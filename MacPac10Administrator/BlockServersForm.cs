using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac10.Administration
{
    public partial class BlockServersForm : Form
    {

        public BlockServersForm()
        {
            InitializeComponent();
        }

        public string Server
        {
            get { return this.txtServer.Text; }
            set { this.txtServer.Text = value; }
        }

        public string Database
        {
            get { return this.txtDatabase.Text; }
            set { this.txtDatabase.Text = value; }
        }

        public string IISServers
        {
            get { return this.txtIISServers.Text.Replace("\r\n", ","); }
            set { this.txtIISServers.Text = value.Replace(",", "\r\n"); }
        }

        private void BlockServersForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.grpServers.Enabled = false;
                this.btnOK.Enabled = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Server == "")
                {
                    MessageBox.Show("Server is required information.", LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.txtServer.Focus();
                }
                else if (this.Database == "")
                {
                    MessageBox.Show("Database is required information.", LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.txtDatabase.Focus();
                }
                else if (NetworkDB.ConnectionInfoIsValid(this.Server, this.Database))
                {
                    //Retrieve current metadata from selected database
                    this.IISServers = NetworkDB.GetBlockedServers(this.Server, this.Database);
                    this.grpServers.Enabled = true;
                    this.btnOK.Enabled = true;
                    //Set input location to end of current text
                    this.txtIISServers.Select(txtIISServers.Text.Length, 0);
                    this.txtIISServers.Focus();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

    }
}