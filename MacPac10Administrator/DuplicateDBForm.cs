using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac10.Administration
{
    public partial class DuplicateDBForm : Form
    {
        public DuplicateDBForm()
        {
            InitializeComponent();
        }

        public string SourceServer
        {
            get { return this.txtSourceServer.Text; }
            set { this.txtSourceServer.Text = value; }
        }

        public string SourceDatabase
        {
            get { return this.txtSourceDatabase.Text; }
            set { this.txtSourceDatabase.Text = value; }
        }

        public string DestinationServer
        {
            get { return this.txtDestServer.Text; }
            set { this.txtDestServer.Text = value; }
        }

        public string DestinationDatabase
        {
            get { return this.txtDestDatabase.Text; }
            set { this.txtDestDatabase.Text = value; }
        }

        public string DestLoginID
        {
            get { return this.txtDestLoginID.Text; }
            set { this.txtDestLoginID.Text = value; }
        }

        public string DestPassword
        {
            get { return this.txtDestPassword.Text; }
            set { this.txtDestPassword.Text = value; }
        }
    }
}