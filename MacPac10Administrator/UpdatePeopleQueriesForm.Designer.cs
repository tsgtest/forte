namespace LMP.MacPac10.Administration
{
    partial class UpdatePeopleQueriesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabProperties = new System.Windows.Forms.TabPage();
            this.gbProperties = new System.Windows.Forms.GroupBox();
            this.lblPassword2 = new System.Windows.Forms.Label();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUID = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUID = new System.Windows.Forms.TextBox();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.lblServer = new System.Windows.Forms.Label();
            this.tabManual = new System.Windows.Forms.TabPage();
            this.gbManual = new System.Windows.Forms.GroupBox();
            this.lblWorking = new System.Windows.Forms.Label();
            this.txtConnection = new LMP.Controls.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabProperties.SuspendLayout();
            this.gbProperties.SuspendLayout();
            this.tabManual.SuspendLayout();
            this.gbManual.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(222, 276);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(77, 27);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(305, 276);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 27);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabProperties);
            this.tabControl1.Controls.Add(this.tabManual);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(378, 257);
            this.tabControl1.TabIndex = 0;
            // 
            // tabProperties
            // 
            this.tabProperties.BackColor = System.Drawing.SystemColors.Control;
            this.tabProperties.Controls.Add(this.gbProperties);
            this.tabProperties.Location = new System.Drawing.Point(4, 27);
            this.tabProperties.Name = "tabProperties";
            this.tabProperties.Padding = new System.Windows.Forms.Padding(3);
            this.tabProperties.Size = new System.Drawing.Size(370, 226);
            this.tabProperties.TabIndex = 0;
            this.tabProperties.Text = "Properties";
            // 
            // gbProperties
            // 
            this.gbProperties.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbProperties.BackColor = System.Drawing.SystemColors.Control;
            this.gbProperties.Controls.Add(this.lblPassword2);
            this.gbProperties.Controls.Add(this.txtPassword2);
            this.gbProperties.Controls.Add(this.lblPassword);
            this.gbProperties.Controls.Add(this.lblUID);
            this.gbProperties.Controls.Add(this.txtPassword);
            this.gbProperties.Controls.Add(this.txtUID);
            this.gbProperties.Controls.Add(this.txtDatabase);
            this.gbProperties.Controls.Add(this.txtServer);
            this.gbProperties.Controls.Add(this.lblDatabase);
            this.gbProperties.Controls.Add(this.lblServer);
            this.gbProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbProperties.Location = new System.Drawing.Point(3, 3);
            this.gbProperties.Name = "gbProperties";
            this.gbProperties.Size = new System.Drawing.Size(364, 220);
            this.gbProperties.TabIndex = 0;
            this.gbProperties.TabStop = false;
            this.gbProperties.Text = "Connection String Properties";
            // 
            // lblPassword2
            // 
            this.lblPassword2.AutoSize = true;
            this.lblPassword2.Location = new System.Drawing.Point(5, 180);
            this.lblPassword2.Name = "lblPassword2";
            this.lblPassword2.Size = new System.Drawing.Size(105, 15);
            this.lblPassword2.TabIndex = 8;
            this.lblPassword2.Text = "Re-enter Password:";
            this.lblPassword2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(118, 177);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(226, 22);
            this.txtPassword2.TabIndex = 9;
            this.txtPassword2.UseSystemPasswordChar = true;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(51, 142);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(59, 15);
            this.lblPassword.TabIndex = 6;
            this.lblPassword.Text = "&Password:";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblUID
            // 
            this.lblUID.AutoSize = true;
            this.lblUID.Location = new System.Drawing.Point(62, 105);
            this.lblUID.Name = "lblUID";
            this.lblUID.Size = new System.Drawing.Size(48, 15);
            this.lblUID.TabIndex = 4;
            this.lblUID.Text = "&User ID:";
            this.lblUID.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(118, 139);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(226, 22);
            this.txtPassword.TabIndex = 7;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtUID
            // 
            this.txtUID.Location = new System.Drawing.Point(118, 102);
            this.txtUID.Name = "txtUID";
            this.txtUID.Size = new System.Drawing.Size(226, 22);
            this.txtUID.TabIndex = 5;
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(118, 65);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(226, 22);
            this.txtDatabase.TabIndex = 3;
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(118, 28);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(226, 22);
            this.txtServer.TabIndex = 1;
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Location = new System.Drawing.Point(53, 68);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(57, 15);
            this.lblDatabase.TabIndex = 2;
            this.lblDatabase.Text = "&Database:";
            this.lblDatabase.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(42, 31);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(68, 15);
            this.lblServer.TabIndex = 0;
            this.lblServer.Text = "&SQL Server:";
            this.lblServer.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabManual
            // 
            this.tabManual.BackColor = System.Drawing.SystemColors.Control;
            this.tabManual.Controls.Add(this.gbManual);
            this.tabManual.Location = new System.Drawing.Point(4, 27);
            this.tabManual.Name = "tabManual";
            this.tabManual.Padding = new System.Windows.Forms.Padding(3);
            this.tabManual.Size = new System.Drawing.Size(370, 226);
            this.tabManual.TabIndex = 1;
            this.tabManual.Text = "Manual Mode";
            // 
            // gbManual
            // 
            this.gbManual.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbManual.BackColor = System.Drawing.SystemColors.Control;
            this.gbManual.Controls.Add(this.txtConnection);
            this.gbManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbManual.Location = new System.Drawing.Point(3, 3);
            this.gbManual.Name = "gbManual";
            this.gbManual.Size = new System.Drawing.Size(364, 222);
            this.gbManual.TabIndex = 9;
            this.gbManual.TabStop = false;
            this.gbManual.Text = "Enter ODBC Connection String:";
            // 
            // lblWorking
            // 
            this.lblWorking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblWorking.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblWorking.Font = new System.Drawing.Font("Arial Unicode MS", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorking.Location = new System.Drawing.Point(19, 279);
            this.lblWorking.Name = "lblWorking";
            this.lblWorking.Size = new System.Drawing.Size(123, 24);
            this.lblWorking.TabIndex = 3;
            this.lblWorking.Text = "Working...";
            this.lblWorking.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtConnection
            // 
            this.txtConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConnection.IsDirty = true;
            this.txtConnection.Location = new System.Drawing.Point(10, 22);
            this.txtConnection.Multiline = true;
            this.txtConnection.Name = "txtConnection";
            this.txtConnection.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtConnection.Size = new System.Drawing.Size(345, 164);
            this.txtConnection.SupportingValues = "";
            this.txtConnection.TabIndex = 0;
            this.txtConnection.Tag2 = null;
            this.txtConnection.Value = "";
            // 
            // UpdatePeopleQueriesForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(399, 315);
            this.Controls.Add(this.lblWorking);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdatePeopleQueriesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update People Query Connection Strings";
            this.Load += new System.EventHandler(this.UpdatePeopleQueriesForm_Load);
            this.Shown += new System.EventHandler(this.UpdatePeopleQueriesForm_Shown);
            this.tabControl1.ResumeLayout(false);
            this.tabProperties.ResumeLayout(false);
            this.gbProperties.ResumeLayout(false);
            this.gbProperties.PerformLayout();
            this.tabManual.ResumeLayout(false);
            this.gbManual.ResumeLayout(false);
            this.gbManual.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabProperties;
        private System.Windows.Forms.GroupBox gbProperties;
        private System.Windows.Forms.Label lblPassword2;
        private System.Windows.Forms.TextBox txtPassword2;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblUID;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUID;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TabPage tabManual;
        private System.Windows.Forms.GroupBox gbManual;
        private Controls.TextBox txtConnection;
        private System.Windows.Forms.Label lblWorking;
    }
}