﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac.Administration
{
    public partial class EditRibbonXMLForm : Form
    {
        string m_xXML;
        Form m_oParent;

        public EditRibbonXMLForm(string xXML, Form oParent)
        {
            InitializeComponent();
            this.m_xXML = xXML;
            this.m_oParent = oParent;
        }

        private void EditRibbonXMLForm_Load(object sender, EventArgs e)
        {
            this.txtXML.Text = this.m_xXML;
            this.Top = this.m_oParent.Top;
            this.Height = this.m_oParent.Height;
        }

        public string XML
        {
            get { return this.txtXML.Text; }
        }
    }
}
