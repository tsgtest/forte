using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac10.Administration
{
    public partial class SyncManagerForm : Form
    {

        public SyncManagerForm()
        {
            InitializeComponent();
        }

        public string Server
        {
            get { return this.txtServer.Text; }
            set { this.txtServer.Text = value; }
        }

        public string Database
        {
            get { return this.txtDatabase.Text; }
            set { this.txtDatabase.Text = value; }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Server == "")
                {
                    MessageBox.Show("Server is required information.", LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.txtServer.Focus();
                }
                else if (this.Database == "")
                {
                    MessageBox.Show("Database is required information.", LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.txtDatabase.Focus();
                }
                else if (NetworkDB.ConnectionInfoIsValid(this.Server, this.Database))
                {
                    //Retrieve current metadata from selected database
                    DataTable oDT = NetworkDB.GetOffices(this.txtServer.Text, this.txtDatabase.Text);
                   
                    this.grpServers.Enabled = true;
                    this.btnOK.Enabled = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}