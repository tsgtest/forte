﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac.Administration
{
    public partial class ResetPublishingInProgressKeyForm : Form
    {
        public ResetPublishingInProgressKeyForm()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            using (LMP.Data.NetworkConnection oCn = new LMP.Data.NetworkConnection(this.txtServer.Text, this.txtDatabase.Text))
            {
                oCn.ConnectToDB();

                OleDbCommand oCmd = new OleDbCommand();
                oCmd.Connection = oCn.ConnectionObject;
                oCmd.CommandText = "UPDATE [dbo].[Metadata] SET Value=0 WHERE Name='PublishInProgress'";
                oCmd.CommandType = CommandType.Text;

                oCmd.ExecuteNonQuery();
            }
        }
    }
}
