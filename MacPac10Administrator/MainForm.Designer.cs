namespace LMP.MacPac10.Administration
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFile_UpdateNormalTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_NetworkDB_CreateNew = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_NetworkDB_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_NetworkDB_CreateCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_NetworkDB_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMain_NetworkDB_GetSharedSegments = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_NetworkDB_DownloadData = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_NetworkDB_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMain_NetworkDB_BlockServers = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_NetworkDB_Backup = new System.Windows.Forms.ToolStripMenuItem();
            this.editMinimumRequiredClientVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_NetworkDB_ResetPublishingInProgressKey = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_LocalDB = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_LocalDB_Publish = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMain_LocalDB_CreateUserCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain_LocalDB_Backup = new System.Windows.Forms.ToolStripMenuItem();
            this.compactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUtilities = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUtilities_BuildCache = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUtilities_RemoveAuthorXML = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUtilities_ExecuteSQL = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUtilities_PeopleUpdater = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUtilities_UserContentManager = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUtilities_EditRibbonXML = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUtilities_CreateLocalDatabases = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scPanels = new System.Windows.Forms.SplitContainer();
            this.treeCategories = new Infragistics.Win.UltraWinTree.UltraTree();
            this.ssMain = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.mnuUtilities_UpdateQueries = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scPanels)).BeginInit();
            this.scPanels.Panel1.SuspendLayout();
            this.scPanels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeCategories)).BeginInit();
            this.ssMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.databaseToolStripMenuItem,
            this.mnuMain_LocalDB,
            this.mnuUtilities,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(836, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile_UpdateNormalTemplate,
            this.toolStripMenuItem4,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // mnuFile_UpdateNormalTemplate
            // 
            this.mnuFile_UpdateNormalTemplate.Name = "mnuFile_UpdateNormalTemplate";
            this.mnuFile_UpdateNormalTemplate.Size = new System.Drawing.Size(234, 22);
            this.mnuFile_UpdateNormalTemplate.Text = "Update Firm &Normal Template";
            this.mnuFile_UpdateNormalTemplate.Click += new System.EventHandler(this.mnuFile_UpdateNormalTemplate_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(231, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuMain_NetworkDB_CreateNew,
            this.mnuMain_NetworkDB_Update,
            this.mnuMain_NetworkDB_CreateCopy,
            this.mnuMain_NetworkDB_Sep1,
            this.mnuMain_NetworkDB_GetSharedSegments,
            this.mnuMain_NetworkDB_DownloadData,
            this.mnuMain_NetworkDB_Sep2,
            this.mnuMain_NetworkDB_BlockServers,
            this.mnuMain_NetworkDB_Backup,
            this.editMinimumRequiredClientVersionToolStripMenuItem,
            this.mnuMain_NetworkDB_ResetPublishingInProgressKey});
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.databaseToolStripMenuItem.Text = "&Network Database";
            // 
            // mnuMain_NetworkDB_CreateNew
            // 
            this.mnuMain_NetworkDB_CreateNew.Name = "mnuMain_NetworkDB_CreateNew";
            this.mnuMain_NetworkDB_CreateNew.Size = new System.Drawing.Size(284, 22);
            this.mnuMain_NetworkDB_CreateNew.Text = "Create...";
            this.mnuMain_NetworkDB_CreateNew.Click += new System.EventHandler(this.mnuMain_NetworkDB_CreateNew_Click);
            // 
            // mnuMain_NetworkDB_Update
            // 
            this.mnuMain_NetworkDB_Update.Name = "mnuMain_NetworkDB_Update";
            this.mnuMain_NetworkDB_Update.Size = new System.Drawing.Size(284, 22);
            this.mnuMain_NetworkDB_Update.Text = "Update Structure...";
            this.mnuMain_NetworkDB_Update.Click += new System.EventHandler(this.mnuMain_NetworkDB_Update_Click);
            // 
            // mnuMain_NetworkDB_CreateCopy
            // 
            this.mnuMain_NetworkDB_CreateCopy.Name = "mnuMain_NetworkDB_CreateCopy";
            this.mnuMain_NetworkDB_CreateCopy.Size = new System.Drawing.Size(284, 22);
            this.mnuMain_NetworkDB_CreateCopy.Text = "Duplicate...";
            this.mnuMain_NetworkDB_CreateCopy.Visible = false;
            this.mnuMain_NetworkDB_CreateCopy.Click += new System.EventHandler(this.mnuMain_NetworkDB_CreateCopy_Click);
            // 
            // mnuMain_NetworkDB_Sep1
            // 
            this.mnuMain_NetworkDB_Sep1.Name = "mnuMain_NetworkDB_Sep1";
            this.mnuMain_NetworkDB_Sep1.Size = new System.Drawing.Size(281, 6);
            // 
            // mnuMain_NetworkDB_GetSharedSegments
            // 
            this.mnuMain_NetworkDB_GetSharedSegments.Name = "mnuMain_NetworkDB_GetSharedSegments";
            this.mnuMain_NetworkDB_GetSharedSegments.Size = new System.Drawing.Size(284, 22);
            this.mnuMain_NetworkDB_GetSharedSegments.Text = "Get Shared Segments...";
            this.mnuMain_NetworkDB_GetSharedSegments.Visible = false;
            this.mnuMain_NetworkDB_GetSharedSegments.Click += new System.EventHandler(this.mnuMain_NetworkDB_GetSharedSegments_Click);
            // 
            // mnuMain_NetworkDB_DownloadData
            // 
            this.mnuMain_NetworkDB_DownloadData.Name = "mnuMain_NetworkDB_DownloadData";
            this.mnuMain_NetworkDB_DownloadData.Size = new System.Drawing.Size(284, 22);
            this.mnuMain_NetworkDB_DownloadData.Text = "Download Data...";
            this.mnuMain_NetworkDB_DownloadData.Click += new System.EventHandler(this.mnuMain_NetworkDB_DownloadData_Click);
            // 
            // mnuMain_NetworkDB_Sep2
            // 
            this.mnuMain_NetworkDB_Sep2.Name = "mnuMain_NetworkDB_Sep2";
            this.mnuMain_NetworkDB_Sep2.Size = new System.Drawing.Size(281, 6);
            // 
            // mnuMain_NetworkDB_BlockServers
            // 
            this.mnuMain_NetworkDB_BlockServers.Name = "mnuMain_NetworkDB_BlockServers";
            this.mnuMain_NetworkDB_BlockServers.Size = new System.Drawing.Size(284, 22);
            this.mnuMain_NetworkDB_BlockServers.Text = "Enable/Disable Synchronization...";
            this.mnuMain_NetworkDB_BlockServers.Click += new System.EventHandler(this.mnuMain_NetworkDB_BlockServers_Click);
            // 
            // mnuMain_NetworkDB_Backup
            // 
            this.mnuMain_NetworkDB_Backup.Name = "mnuMain_NetworkDB_Backup";
            this.mnuMain_NetworkDB_Backup.Size = new System.Drawing.Size(284, 22);
            this.mnuMain_NetworkDB_Backup.Text = "Backup...";
            this.mnuMain_NetworkDB_Backup.Visible = false;
            this.mnuMain_NetworkDB_Backup.Click += new System.EventHandler(this.mnuMain_NetworkDB_Backup_Click);
            // 
            // editMinimumRequiredClientVersionToolStripMenuItem
            // 
            this.editMinimumRequiredClientVersionToolStripMenuItem.Name = "editMinimumRequiredClientVersionToolStripMenuItem";
            this.editMinimumRequiredClientVersionToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.editMinimumRequiredClientVersionToolStripMenuItem.Text = "Edit Minimum Required Client Version...";
            this.editMinimumRequiredClientVersionToolStripMenuItem.Visible = false;
            // 
            // mnuMain_NetworkDB_ResetPublishingInProgressKey
            // 
            this.mnuMain_NetworkDB_ResetPublishingInProgressKey.Name = "mnuMain_NetworkDB_ResetPublishingInProgressKey";
            this.mnuMain_NetworkDB_ResetPublishingInProgressKey.Size = new System.Drawing.Size(284, 22);
            this.mnuMain_NetworkDB_ResetPublishingInProgressKey.Text = "Reset Publishing In Progress Key...";
            this.mnuMain_NetworkDB_ResetPublishingInProgressKey.Click += new System.EventHandler(this.mnuMain_NetworkDB_ResetPublishingInProgressKey_Click);
            // 
            // mnuMain_LocalDB
            // 
            this.mnuMain_LocalDB.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuMain_LocalDB_Publish,
            this.toolStripMenuItem3,
            this.mnuMain_LocalDB_CreateUserCopy,
            this.mnuMain_LocalDB_Backup,
            this.compactToolStripMenuItem});
            this.mnuMain_LocalDB.Name = "mnuMain_LocalDB";
            this.mnuMain_LocalDB.Size = new System.Drawing.Size(98, 20);
            this.mnuMain_LocalDB.Text = "&Local Database";
            // 
            // mnuMain_LocalDB_Publish
            // 
            this.mnuMain_LocalDB_Publish.Name = "mnuMain_LocalDB_Publish";
            this.mnuMain_LocalDB_Publish.Size = new System.Drawing.Size(218, 22);
            this.mnuMain_LocalDB_Publish.Text = "&Publish...";
            this.mnuMain_LocalDB_Publish.Click += new System.EventHandler(this.mnuMain_LocalDB_Publish_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(215, 6);
            // 
            // mnuMain_LocalDB_CreateUserCopy
            // 
            this.mnuMain_LocalDB_CreateUserCopy.Name = "mnuMain_LocalDB_CreateUserCopy";
            this.mnuMain_LocalDB_CreateUserCopy.Size = new System.Drawing.Size(218, 22);
            this.mnuMain_LocalDB_CreateUserCopy.Text = "&Create Distributable Copy...";
            this.mnuMain_LocalDB_CreateUserCopy.Click += new System.EventHandler(this.mnuMain_LocalDB_CreateUserCopy_Click);
            // 
            // mnuMain_LocalDB_Backup
            // 
            this.mnuMain_LocalDB_Backup.Name = "mnuMain_LocalDB_Backup";
            this.mnuMain_LocalDB_Backup.Size = new System.Drawing.Size(218, 22);
            this.mnuMain_LocalDB_Backup.Text = "&Backup...";
            this.mnuMain_LocalDB_Backup.Click += new System.EventHandler(this.mnuMain_LocalDB_Backup_Click);
            // 
            // compactToolStripMenuItem
            // 
            this.compactToolStripMenuItem.Name = "compactToolStripMenuItem";
            this.compactToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.compactToolStripMenuItem.Text = "C&ompact";
            this.compactToolStripMenuItem.Click += new System.EventHandler(this.compactToolStripMenuItem_Click);
            // 
            // mnuUtilities
            // 
            this.mnuUtilities.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuUtilities_BuildCache,
            this.mnuUtilities_RemoveAuthorXML,
            this.mnuUtilities_ExecuteSQL,
            this.mnuUtilities_PeopleUpdater,
            this.mnuUtilities_UpdateQueries,
            this.mnuUtilities_UserContentManager,
            this.mnuUtilities_EditRibbonXML,
            this.mnuUtilities_CreateLocalDatabases});
            this.mnuUtilities.Name = "mnuUtilities";
            this.mnuUtilities.Size = new System.Drawing.Size(58, 20);
            this.mnuUtilities.Text = "&Utilities";
            // 
            // mnuUtilities_BuildCache
            // 
            this.mnuUtilities_BuildCache.Name = "mnuUtilities_BuildCache";
            this.mnuUtilities_BuildCache.Size = new System.Drawing.Size(238, 22);
            this.mnuUtilities_BuildCache.Text = "&Build Cache...";
            this.mnuUtilities_BuildCache.Click += new System.EventHandler(this.mnuUtilities_BuildCache_Click);
            // 
            // mnuUtilities_RemoveAuthorXML
            // 
            this.mnuUtilities_RemoveAuthorXML.Name = "mnuUtilities_RemoveAuthorXML";
            this.mnuUtilities_RemoveAuthorXML.Size = new System.Drawing.Size(238, 22);
            this.mnuUtilities_RemoveAuthorXML.Text = "Remove Author XML";
            this.mnuUtilities_RemoveAuthorXML.Visible = false;
            this.mnuUtilities_RemoveAuthorXML.Click += new System.EventHandler(this.mnuUtilities_RemoveAuthorXML_Click);
            // 
            // mnuUtilities_ExecuteSQL
            // 
            this.mnuUtilities_ExecuteSQL.Name = "mnuUtilities_ExecuteSQL";
            this.mnuUtilities_ExecuteSQL.Size = new System.Drawing.Size(238, 22);
            this.mnuUtilities_ExecuteSQL.Text = "&Execute SQL...";
            this.mnuUtilities_ExecuteSQL.Click += new System.EventHandler(this.mnuUtilities_ExecuteSQL_Click);
            // 
            // mnuUtilities_PeopleUpdater
            // 
            this.mnuUtilities_PeopleUpdater.Name = "mnuUtilities_PeopleUpdater";
            this.mnuUtilities_PeopleUpdater.Size = new System.Drawing.Size(238, 22);
            this.mnuUtilities_PeopleUpdater.Text = "&People Updater...";
            this.mnuUtilities_PeopleUpdater.Click += new System.EventHandler(this.mnuUtilities_PeopleUpdater_Click);
            // 
            // mnuUtilities_UserContentManager
            // 
            this.mnuUtilities_UserContentManager.Name = "mnuUtilities_UserContentManager";
            this.mnuUtilities_UserContentManager.Size = new System.Drawing.Size(238, 22);
            this.mnuUtilities_UserContentManager.Text = "&User Content Manager...";
            this.mnuUtilities_UserContentManager.Click += new System.EventHandler(this.mnuUtilities_UserContentManager_Click);
            // 
            // mnuUtilities_EditRibbonXML
            // 
            this.mnuUtilities_EditRibbonXML.Name = "mnuUtilities_EditRibbonXML";
            this.mnuUtilities_EditRibbonXML.Size = new System.Drawing.Size(238, 22);
            this.mnuUtilities_EditRibbonXML.Text = "Edit Ribbon XML...";
            this.mnuUtilities_EditRibbonXML.Click += new System.EventHandler(this.mnuUtilities_EditRibbonXML_Click);
            // 
            // mnuUtilities_CreateLocalDatabases
            // 
            this.mnuUtilities_CreateLocalDatabases.Name = "mnuUtilities_CreateLocalDatabases";
            this.mnuUtilities_CreateLocalDatabases.Size = new System.Drawing.Size(238, 22);
            this.mnuUtilities_CreateLocalDatabases.Text = "Create &Local Databases...";
            this.mnuUtilities_CreateLocalDatabases.Click += new System.EventHandler(this.mnuUtilities_CreateLocalDatabases_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchToolStripMenuItem,
            this.contentToolStripMenuItem,
            this.indexToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.Visible = false;
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.searchToolStripMenuItem.Text = "&Search...";
            // 
            // contentToolStripMenuItem
            // 
            this.contentToolStripMenuItem.Name = "contentToolStripMenuItem";
            this.contentToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.contentToolStripMenuItem.Text = "&Content...";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.indexToolStripMenuItem.Text = "&Index...";
            // 
            // scPanels
            // 
            this.scPanels.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scPanels.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scPanels.IsSplitterFixed = true;
            this.scPanels.Location = new System.Drawing.Point(0, 28);
            this.scPanels.Name = "scPanels";
            // 
            // scPanels.Panel1
            // 
            this.scPanels.Panel1.BackColor = System.Drawing.Color.White;
            this.scPanels.Panel1.Controls.Add(this.treeCategories);
            // 
            // scPanels.Panel2
            // 
            this.scPanels.Panel2.BackColor = System.Drawing.Color.White;
            this.scPanels.Size = new System.Drawing.Size(836, 573);
            this.scPanels.SplitterDistance = 182;
            this.scPanels.SplitterWidth = 3;
            this.scPanels.TabIndex = 8;
            // 
            // treeCategories
            // 
            appearance1.BackColor = System.Drawing.Color.WhiteSmoke;
            appearance1.BackColor2 = System.Drawing.Color.LightGray;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            this.treeCategories.Appearance = appearance1;
            this.treeCategories.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.treeCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeCategories.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeCategories.FullRowSelect = true;
            this.treeCategories.HideSelection = false;
            this.treeCategories.Location = new System.Drawing.Point(0, 0);
            this.treeCategories.Name = "treeCategories";
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            appearance2.FontData.Name = "Tahoma";
            appearance2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            _override1.NodeAppearance = appearance2;
            _override1.NodeDoubleClickAction = Infragistics.Win.UltraWinTree.NodeDoubleClickAction.None;
            _override1.NodeSpacingAfter = 0;
            _override1.NodeSpacingBefore = 6;
            appearance3.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            _override1.SelectedNodeAppearance = appearance3;
            this.treeCategories.Override = _override1;
            this.treeCategories.ShowLines = false;
            this.treeCategories.ShowRootLines = false;
            this.treeCategories.Size = new System.Drawing.Size(182, 573);
            this.treeCategories.TabIndex = 5;
            this.treeCategories.UpdateMode = Infragistics.Win.UltraWinTree.UpdateMode.OnActiveNodeChange;
            this.treeCategories.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeCategories_AfterSelect);
            this.treeCategories.BeforeSelect += new Infragistics.Win.UltraWinTree.BeforeNodeSelectEventHandler(this.treeCategories_BeforeSelect);
            // 
            // ssMain
            // 
            this.ssMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.ssMain.Location = new System.Drawing.Point(0, 608);
            this.ssMain.Name = "ssMain";
            this.ssMain.Size = new System.Drawing.Size(836, 22);
            this.ssMain.TabIndex = 9;
            this.ssMain.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // mnuUtilities_UpdateQueries
            // 
            this.mnuUtilities_UpdateQueries.Name = "mnuUtilities_UpdateQueries";
            this.mnuUtilities_UpdateQueries.Size = new System.Drawing.Size(238, 22);
            this.mnuUtilities_UpdateQueries.Text = "Set People &Query Connection...";
            this.mnuUtilities_UpdateQueries.Click += new System.EventHandler(this.mnuUtilities_UpdateQueries_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(836, 630);
            this.Controls.Add(this.ssMain);
            this.Controls.Add(this.scPanels);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(763, 564);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Forte Administration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.scPanels.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scPanels)).EndInit();
            this.scPanels.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeCategories)).EndInit();
            this.ssMain.ResumeLayout(false);
            this.ssMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_NetworkDB_CreateCopy;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.SplitContainer scPanels;
        private Infragistics.Win.UltraWinTree.UltraTree treeCategories;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_NetworkDB_CreateNew;
        private System.Windows.Forms.ToolStripSeparator mnuMain_NetworkDB_Sep1;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_LocalDB;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_LocalDB_CreateUserCopy;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_LocalDB_Backup;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_LocalDB_Publish;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_NetworkDB_DownloadData;
        private System.Windows.Forms.ToolStripMenuItem mnuFile_UpdateNormalTemplate;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.StatusStrip ssMain;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities_RemoveAuthorXML;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_NetworkDB_Update;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities_BuildCache;
        private System.Windows.Forms.ToolStripSeparator mnuMain_NetworkDB_Sep2;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_NetworkDB_BlockServers;
        private System.Windows.Forms.ToolStripMenuItem compactToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities_ExecuteSQL;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities_PeopleUpdater;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_NetworkDB_GetSharedSegments;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities_EditRibbonXML;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_NetworkDB_Backup;
        private System.Windows.Forms.ToolStripMenuItem editMinimumRequiredClientVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuMain_NetworkDB_ResetPublishingInProgressKey;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities_CreateLocalDatabases;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities_UserContentManager;
        private System.Windows.Forms.ToolStripMenuItem mnuUtilities_UpdateQueries;
    }
}

