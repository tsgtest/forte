using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ADOX;
using System.Data.OleDb;
using LMP.Data;
using ADODB;
using Microsoft.CSharp;

namespace LMP.MacPac10.Administration
{
    public partial class UpdatePeopleQueriesForm : Form
    {
        const string xConnectionStringFormat = "ODBC;Driver=SQL Server;Server={0};Database={1};UID={2};PWD={3}";
        
        public UpdatePeopleQueriesForm()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string xMsg = "";
            if (this.tabControl1.SelectedTab == this.tabProperties)
            {
                if (txtServer.Text == "")
                {
                    xMsg = "SQL Server is required";
                    txtServer.Focus();
                }
                else if (txtDatabase.Text == "")
                {
                    xMsg = "Database is required";
                    txtDatabase.Focus();
                }
                else if (txtUID.Text == "")
                {
                    xMsg = "User ID is required";
                    txtUID.Focus();
                }
                else if (txtPassword.Text == "")
                {
                    xMsg = "Password is required";
                    txtPassword.Focus();
                }
                else if (txtPassword2.Text != txtPassword.Text)
                {
                    xMsg = "Re-entered password does not match";
                    txtPassword.Focus();
                }
                if (xMsg != "")
                {
                    MessageBox.Show(xMsg, "Update Connection String", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = System.Windows.Forms.DialogResult.None;
                    return;
                }

            }
            else
            {
                if (txtConnection.Text == "")
                {
                    xMsg = "Connection String is required";
                    txtConnection.Focus();
                }
            }
            lblWorking.Visible = true;
            this.Refresh();
            UpdateQueries();
            lblWorking.Visible = false;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Hide();
        }
        private void UpdateQueries()
        {

            DateTime t1 = DateTime.Now;
            string xCnnTemplate = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Jet OLEDB:System Database={1};User ID={2};Password={3};";
            ADODB.Connection oCnn = new ADODB.Connection();
            ADOX.CatalogClass oCat = new ADOX.CatalogClass();
            try
            {
                oCnn.ConnectionString = string.Format(xCnnTemplate, LocalConnection.ConnectionObject.DataSource, LMP.Data.Application.PublicDataDirectory + "\\Forte.mdw",
                    "MacPacPersonnel", "fish4mill");
                oCnn.Open();

                oCat.ActiveConnection = oCnn;
                int iCount = 0;
                DateTime t2 = DateTime.Now;
                List<Procedure> oProcs = new List<Procedure>();
                int iProcCount = oCat.Procedures.Count;
                for (int p = 0; p < iProcCount; p++)
                {
                    oProcs.Add(oCat.Procedures[p]);
                }
                LMP.Benchmarks.Print(t2, "Populate List oProcs (" + iProcCount.ToString() + ")");
                foreach  (Procedure oProc in oProcs)
                {
                    DateTime t0 = DateTime.Now;
                    string xQueryName = oProc.Name;
                    ADODB.Command oCmd = null;
                    try
                    {
                        oCmd = (ADODB.Command)oProc.Command;
                    }
                    catch
                    {
                    }
                    if (oCmd == null)
                    {
                        continue;
                    }
                    object oValue = null;
                    try
                    {
                        oValue = oCmd.Properties["Jet OLEDB:ODBC Pass-Through Statement"].Value;
                    }
                    catch { }
                    if (oValue != null && (bool)oValue == true)
                    {
                        try
                        {
                            oCmd.Properties["Jet OLEDB:Pass Through Query Connect String"].Value = BuildConnectionString();
                            oProc.Command = oCmd;
                            iCount++;
                        }
                        catch
                        { }
                    }
                    LMP.Benchmarks.Print(t0, xQueryName);
                    System.Windows.Forms.Application.DoEvents();
                }
                string xMsg = "";
                switch (iCount)
                {
                    case 0:
                        xMsg = "No pass-through queries found.";
                        break;
                    case 1:
                        xMsg = "One pass-through query updated.";
                        break;
                    default:
                        xMsg = iCount.ToString() + " pass-through queries updated.";
                        break;
                }
                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);

            }
            finally
            {
                oCat = null;
                oCnn.Close();
                oCnn = null;
                LMP.Benchmarks.Print(t1);
            }
        }
        private string BuildConnectionString()
        {
            string xString = "";
            if (this.tabControl1.SelectedTab == this.tabManual)
                xString = txtConnection.Text;
            else
                xString = string.Format(xConnectionStringFormat, this.txtServer.Text, this.txtDatabase.Text, this.txtUID.Text, this.txtPassword.Text);
            return xString;
        }

        private void UpdatePeopleQueriesForm_Load(object sender, EventArgs e)
        {
            this.lblWorking.Visible = false;
            this.tabControl1.SelectedTab = this.tabProperties;
            this.txtConnection.Text = string.Format(xConnectionStringFormat, "<SERVERNAME>", "<DATABASE>", "<UID>", "<PWD>");
        }

        private void UpdatePeopleQueriesForm_Shown(object sender, EventArgs e)
        {
            this.txtServer.Focus();
        }
    }
}