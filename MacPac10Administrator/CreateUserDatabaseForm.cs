using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace LMP.MacPac10.Administration
{
    public partial class CreateUserDatabaseForm : Form
    {
        string m_xInitialTargetPath = "";
        string m_xInitialServer = "";
        string m_xInitialDatabase = "";
        bool m_bLocalPublicMode = false;

        public CreateUserDatabaseForm()
        {
            InitializeComponent();
        }

        public CreateUserDatabaseForm(string xInitialTargetPath, string xInitialServer, string xInitialDatabase) : this()
        {
            DBLocation = m_xInitialTargetPath = xInitialTargetPath;
            Server = m_xInitialServer = xInitialServer;
            Database = m_xInitialDatabase = xInitialDatabase;
            this.ServerApplicationName = "ForteSyncServer";
        }

        public CreateUserDatabaseForm(string xInitialTargetPath, bool bCreateFortePublic) : this()
        {
            DBLocation = m_xInitialTargetPath = xInitialTargetPath;
            m_bLocalPublicMode = bCreateFortePublic;
        }

        public string DBLocation
        {
            get { return this.txtLocation.Text; }
            set { this.txtLocation.Text = value; }
        }

        public string Server
        {
            get { return this.txtServer.Text; }
            set { this.txtServer.Text = value; }
        }

        public string Database
        {
            get { return this.txtDatabase.Text; }
            set { this.txtDatabase.Text = value; }
        }

        public string IISServerIPAddress
        {
            get { return this.txtIISServerIPAddress.Text; }
            set { this.txtIISServerIPAddress.Text = value; }
        }

        public string ServerApplicationName
        {
            get { return this.txtServerAppName.Text; }
            set { this.txtServerAppName.Text = value; }
        }


        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog oDlg = new FolderBrowserDialog())
            {
                oDlg.SelectedPath = this.m_xInitialTargetPath;
                oDlg.Description = "Target Folder:";
                DialogResult iChoice = oDlg.ShowDialog(this);
                if (iChoice == DialogResult.OK)
                {
                    this.txtLocation.Text = oDlg.SelectedPath;
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //default to keeping the dialog visible
            this.DialogResult = DialogResult.None;

            //get target user db full name
            string xUserDB = DBLocation + @"\" + LMP.Data.Application.UserDBName;
            //GLOG 8252
            if (m_bLocalPublicMode)
                xUserDB = DBLocation + @"\" + "ForteLocalPublic.mdb";

            if (string.IsNullOrEmpty(this.txtLocation.Text))
            {
                MessageBox.Show("Location is required information.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            //GLOG 8220
            //GLOG 8252
            else if (!m_bLocalPublicMode && (!LMP.MacPac.MacPacImplementation.IsToolkit && !LMP.MacPac.MacPacImplementation.IsFullLocal) &&
                string.IsNullOrEmpty(this.txtServer.Text))
            {
                MessageBox.Show("Server name or address is required information.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            //GLOG 8220
            //GLOG 8252
            else if (!m_bLocalPublicMode && (!LMP.MacPac.MacPacImplementation.IsToolkit && !LMP.MacPac.MacPacImplementation.IsFullLocal) &&
                string.IsNullOrEmpty(this.txtDatabase.Text))
            {
                MessageBox.Show("Database name is required information.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            //GLOG 8220
            //GLOG 8252
            else if (!m_bLocalPublicMode && (!LMP.MacPac.MacPacImplementation.IsToolkit && !LMP.MacPac.MacPacImplementation.IsFullLocal) &&
                string.IsNullOrEmpty(this.txtIISServerIPAddress.Text))
            {
                MessageBox.Show("Synch server uri is required information.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            // Confirm that the folder exists since it 
            // may be entered or modify by the user manually.
            else if (!Directory.Exists(DBLocation))
            {
                MessageBox.Show("The folder " + DBLocation + " does not exist.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            else if (File.Exists(xUserDB))
            {
                DialogResult iChoice = MessageBox.Show(
                    "A Forte database already exists at this location. Replace the existing database?",
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);

                if (iChoice == DialogResult.Yes)
                    this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                //this.Close();
            }
        }

        private void CreateUserDatabaseForm_Load(object sender, EventArgs e)
        {
            //GLOG 7898 (dm) - hide server controls in Forte Tools
            //GLOG 8220: Hide Server Controls for Local mode
            if (m_bLocalPublicMode || LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                this.Height = this.Height - this.groupBox1.Height;
                this.groupBox1.Visible = false;
                this.gbServers.Visible = false;
            }       
        }
    }
}