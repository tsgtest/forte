﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac.Administration
{
    public partial class BuildCacheForm : Form
    {
        public BuildCacheForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// returns the IP address entered in the form
        /// </summary>
        public string ServerIP
        {
            get { return this.txtIISServer.Text; }
        }

        /// <summary>
        /// returns the server application name
        /// entered in the form
        /// </summary>
        public string ServerApplicationName
        {
            get { return this.txtServerApplicationName.Text; }
        }
    }
}
