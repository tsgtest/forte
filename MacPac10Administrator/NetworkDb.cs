using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using LMP.MacPac;
using LMP.MacPac.Sync;
using System.Data;
using LMP.Data;
using System.Windows.Forms;
using System.Data.OleDb;

namespace LMP.MacPac10.Administration
{
    public static class NetworkDB
    {
        const string mpSuperUserID = "mp10SuperUser";
        const string mpSuperUserPassword = "superfish4wade";
        #region *************************constructor*************************
        /// <summary>
		/// private constructor - prevent instance creation
		/// </summary>
		static NetworkDB()
		{
        }
        #endregion
        #region *************************methods*************************
        /// <summary>
        /// Calls private functions that create mp10 network db and its data objects
        /// </summary>
        public static void CreateNetworkDB(string xServer, string xDatabase, string xLoginID, string xPassword)
        {

            //open network db form, 
            //which displays data objects as they are created
            NetworkDBProgress oForm = new NetworkDBProgress();

            //create new db
            if (CreateNetworkDB(xServer, xDatabase, xLoginID, xPassword, oForm))
            {
                //create Forte data objects
                CreateNetworkDBObjects(xServer, xDatabase, xLoginID, xPassword, oForm);
            }
        }
        /// <summary>
        /// Calls private functions that update Forte network db structure and objects-
        /// does not add/update/delete records -
        /// also updates the local admin db if necessary to
        /// guarantee that the appropriate structure is consistent
        /// between databases
        /// </summary>
        public static void UpdateNetworkDB(string xServer, string xDatabase, string xLoginID, string xPassword)
        {
            //update local admin database
            LMP.Data.Application.UpdateLocalDBStructureIfNecessary(true);

            //update network database
            UpdateNetworkDBObjects(xServer, xDatabase, xLoginID, xPassword);

            MessageBox.Show("The structure of the " + LMP.ComponentProperties.ProductName + 
                " network database has been updated to include all changes defined in the file '" +
                LMP.Data.Application.AppDirectory + @"\UpdateNetworkDB.sql'.  You may now publish any changes to the network database.",
                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static bool ConnectionInfoIsValid(string xServer, string xDatabase)
        {

            //create connection string from parameters
             string xConnectionString = "Persist Security Info=false;Integrated Security=false;Connect Timeout=15;" +
                "database=master" + ";server=" + xServer + @";user id=" + mpSuperUserID +
                ";password=" + mpSuperUserPassword;

            //connection object
             using (SqlConnection oCnn = new SqlConnection(xConnectionString))
             {
                 try
                 {
                     oCnn.Open();
                 }
                 catch (System.Exception oE)
                 {
                     if (oE.Message.Contains("An error has occurred while establishing a connection to the server"))
                     {
                         throw new LMP.Exceptions.DBConnectionException(
                            LMP.Resources.GetLangString("Error_ServerNotAvailable") + xServer, oE);
                     }
                     else
                     {
                         throw new LMP.Exceptions.DBConnectionException("Could not establish the database connection.", oE);

                     }
                 }

                 using (SqlCommand oCmd = new SqlCommand())
                 {
                     oCmd.Connection = oCnn;

                     if (!DatabaseExists(oCmd, xDatabase))
                     {
                         MessageBox.Show("The database '" + xDatabase + "' " + "does not exist. Please enter a valid database name.",
                             LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                         return false;
                     }
                 }
             }
             return true;
        }

        #endregion
        #region *************************private methods*************************

        /// <summary>
        /// Creates Forte network db
        /// 3="oForm"></param>
        /// </summary>
        private static bool CreateNetworkDB(string xServer, string xDatabase, 
            string xLoginID, string xPassword, NetworkDBProgress oForm)
        {
            string xConnectionString = "";

            //create connection string from parameters
            //xConnectionString = "Persist Security Info=false;Integrated Security=false;" +
            //    "database=master;server=" + xServer + @";Network Library=DBMSSOCN;user id=" + xLoginID +
            //    ";password=" + xPassword;

            //xConnectionString = "Data Source=" + xServer + ";Network Library=DBMSSOCN;Initial Catalog=master;User ID=" +
            //    xLoginID + ";Password=" + xPassword;

            xConnectionString = "Persist Security Info=false;Integrated Security=false;" +
                "database=master" + ";server=" + xServer + @";user id=" + xLoginID +
                ";password=" + xPassword;


            //connection object
            using (SqlConnection oCnn = new SqlConnection(xConnectionString))
            {   
                try
                {
                    oCnn.Open();
                }
                catch (System.Exception oE)
                {
                    if (oE.Message.Contains("An error has occurred while establishing a connection to the server"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_ServerNotAvailable") + xServer, oE);
                    }
                    else if (oE.Message.Contains("Login failed for user"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_LoginInvalid") + xLoginID, oE);
                    }
                    else
                    {
                        // GLOG : 4885 : JSW
                        // remove sql admin password from string displayed in error message
                        int iPos = xConnectionString.IndexOf(";password");
                        string xConnectionStringNoPWord = xConnectionString.Substring(0, iPos); 
                        throw new LMP.Exceptions.DBConnectionException(
                            LMP.Resources.GetLangString("Error_CouldNotConnectToDB") + xConnectionStringNoPWord, oE);

                    }
                }

                using (SqlCommand oCmd = new SqlCommand())
                {
                    oCmd.Connection = oCnn;

                    if (DatabaseExists(oCmd, xDatabase))
                    {
                        MessageBox.Show("Database '" + xDatabase + "' " + LMP.Resources.GetLangString("Msg_DatabaseAlreadyExists"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    //get sql script file for creating Forte
                    string xCreateDBFile = LMP.Data.Application.AppDirectory + @"\CreateForte.sql";

                    //create file
                    FileInfo oFile = new FileInfo(xCreateDBFile);

                    //load script
                    string xSQLScript = oFile.OpenText().ReadToEnd();

                    //replace every reference to mp10 with xDataBase
                    xSQLScript = xSQLScript.Replace("mp10", xDatabase);

                    oForm.Show();

                    //create array of queries from script
                    //it's in transact sql, which the command object 
                    //cannot process, so the "GO" lines are removed
                    string[] xCmds = xSQLScript.Replace("GO", "~").Split(new Char[] { '~' });

                    oForm.AddRow(LMP.Resources.GetLangString("Msg_CreatingNetworkDB"));
                    oForm.Refresh();

                    try
                    {

                        //loop through queries and execute
                        foreach (string xCmd in xCmds)
                        {
                            oCmd.CommandText = xCmd;
                            oCmd.ExecuteNonQuery();
                        }
                    }
                    catch (System.Exception oE)
                    {
                        // GLOG : 4885 : JSW
                        // remove sql admin password from string displayed in error message
                        int iPos = xConnectionString.IndexOf(";password");
                        string xConnectionStringNoPWord = xConnectionString.Substring(0, iPos);
                        throw new LMP.Exceptions.ServerException(
                           LMP.Resources.GetLangString("Error_CouldNotCreateDBObject") + oCmd.CommandText, oE);
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Creates Forte db objects
        /// <param name="oForm"></param>
        /// </summary>
        private static void CreateNetworkDBObjects(string xServer, string xDatabase,
            string xLoginID, string xPassword, NetworkDBProgress oForm)
        {
            string xCnn = "";

            //create connection string from parameters
            xCnn = "Persist Security Info=false;Integrated Security=false;" +
                "database=" + xDatabase + ";server=" + xServer + @";user id=" + xLoginID +
                ";password=" + xPassword;

            //connection object
            using (SqlConnection oCnn = new SqlConnection(xCnn))
            {
                //get sql script for creating Forte data objects
                string xCreateDBObjectsFile = LMP.Data.Application.AppDirectory +
                    @"\CreateForteObjects.sql";

                //load new text into file
                FileInfo oFile = new FileInfo(xCreateDBObjectsFile);

                //load new script
                string xSQLScript = oFile.OpenText().ReadToEnd();

                //replace every reference to mp10 with xDataBase
                xSQLScript = xSQLScript.Replace("[mp10]", "[" + xDatabase + "]");

                //JTS 9/18/09: replace references in the defdb property of the sp_addlogin commands
                xSQLScript = xSQLScript.Replace("N'mp10'", "N'" + xDatabase + "'");

                try
                {
                    oCnn.Open();
                }
                catch (System.Exception oE)
                {
                    if (oE.Message.Contains("An error has occurred while establishing a connection to the server"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_ServerNotAvailable") + xServer, oE);
                    }
                    else if (oE.Message.Contains("Login failed for user"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_LoginInvalid") + xLoginID, oE);
                    }
                    else
                    {
                        // GLOG : 4885 : JSW
                        // remove sql admin password from string displayed in error message
                        int iPos = xCnn.IndexOf(";password");
                        string xCnnNoPWord = xCnn.Substring(0, iPos); 
                        throw new LMP.Exceptions.DBConnectionException(
                            LMP.Resources.GetLangString("Error_CouldNotConnectToDB") + xCnnNoPWord, oE);

                    }
                }

                using (SqlCommand oCmd = new SqlCommand())
                {
                    oCmd.Connection = oCnn;

                    int iStartPos;
                    int iEndPos;
                    string xDataObjectName;
                    string xDescription;
                    

                    //create array of queries from script
                    //it's in transact sql, which the command object 
                    //cannot process, so the "GO" lines are removed
                    string[] xCmds = xSQLScript.Replace("GO", "~").Split(new Char[] { '~' });
                    int i = 1;

                    // GLOG : 3502 : JAB
                    // Get the total items so that we can display the progress status 
                    // as a ratio of current items complete to total items ie, 
                    // CurrentItem/TotalItem.

                    int iTotalObjects = 0;

                    foreach (string xCmd in xCmds)
                    {
                        if ((xCmd.Contains("IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[")
                            // Ignore Foreign Key and Constraint entries
                            && !xCmd.Contains("type = 'F'") && !xCmd.Contains("type = 'C'"))
                            || (xCmd.Contains("CREATE USER") || xCmd.Contains("sp_grantdbaccess")))
                        {
                            iTotalObjects++;
                        }
                    }

                    bool bDataObject = false;
                    try
                    {
                        foreach (string xCmd in xCmds)
                        {

                            oCmd.CommandText = xCmd;
                            if ((xCmd.Contains("IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[")
                                // Ignore Foreign Key and Constraint entries
                                && !xCmd.Contains("type = 'F'") && !xCmd.Contains("type = 'C'"))
                                || (xCmd.Contains("CREATE USER") || xCmd.Contains("sp_grantdbaccess")))
                            {
                                bDataObject = true;
                            }
                            else
                            {
                                bDataObject = false;
                            }

                            if (bDataObject == true)
                            {

                                if (xCmd.Contains("CREATE USER") || xCmd.Contains("sp_grantdbaccess"))
                                {
                                    xDescription = "Creating user ";

                                    if (xCmd.Contains("mp10SuperUser"))
                                        xDataObjectName = "mp10SuperUser";
                                    else
                                        xDataObjectName = "mp10User";
                                }
                                else
                                {
                                    iStartPos = xCmd.IndexOf("IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[");

                                    //reset start position to beginning of data object name
                                    iStartPos = xCmd.IndexOf(".[", iStartPos) + 2;
                                    //set end position
                                    iEndPos = xCmd.IndexOf("]'", iStartPos);

                                    //get object name
                                    xDataObjectName = xCmd.Substring(iStartPos, iEndPos - iStartPos);

                                    //create description according to data object type
                                    if (xDataObjectName.Substring(0, 2) == "sp")
                                        xDescription = LMP.Resources.GetLangString("Msg_CreatingFunction");
                                    else
                                        xDescription = LMP.Resources.GetLangString("Msg_CreatingTable");
                                }

                                //add new row to listbox
                                oForm.AddRow(xDescription + xDataObjectName + ".");
                                oForm.UpdateProgressLabel("Executing command " + i.ToString() + " of " + iTotalObjects.ToString());
                                i++;

                                oForm.Refresh();
                                System.Windows.Forms.Application.DoEvents();

                            }
                            try
                            {
                                oCmd.ExecuteNonQuery();
                                System.Windows.Forms.Application.DoEvents();
                            }
                            catch (System.Exception oE)
                            {
                                if (oCmd.CommandText.Contains("CREATE LOGIN") || oCmd.CommandText.Contains("sp_addlogin"))
                                {
                                    //login already created for this server,
                                    //continue on to next command
                                    continue;
                                }
                                else
                                {
                                    throw new LMP.Exceptions.ServerException(
                                        LMP.Resources.GetLangString("Error_CouldNotCreateDBObject") + oCmd.CommandText, oE);
                                }
                            }
                        }
                        try
                        {
                            //JTS 12/09/08: Conform Server People structure table to Local DB
                            oForm.AddRow(LMP.Resources.GetLangString("Msg_UpdatingPeopleTables"));
                            LMP.Data.Application.UpdateNetworkDBPeopleObjects(xServer, xDatabase, 
                                LMP.Data.LocalConnection.ConnectionObject);
                        }
                        catch (System.Exception oE)
                        {
                            throw new LMP.Exceptions.ServerException(LMP.Resources.GetLangString("Error_CouldNotUpdateNetworkPeopleTable"), oE);
                        }

                        oForm.AddRow(LMP.ComponentProperties.ProductName + " network database was successfully created.");
                        oForm.Refresh();
                    }
                    catch (System.Exception oE)
                    {
                        oForm.btnOK.Text = "OK";

                        //add new row to listbox
                        oForm.AddRow(oE.Message);
                        oForm.Refresh();

                        throw new LMP.Exceptions.ServerException(
                            LMP.Resources.GetLangString("Error_CouldNotCreateNetworkDB"), oE);
                    }
                    finally
                    {
                        oCnn.Close();
                        oForm.btnOK.Enabled = true;
                    }
                }
            }
        }

        /// <summary>
        /// updates the network db
        /// <param name="oForm"></param>
        /// </summary>
        private static void UpdateNetworkDBObjects(string xServer,
            string xDatabase, string xLoginID, string xPassword)
        {
            //********************************************
            //NOTE: for this method to execute correctly,
            //the UpdateLocalDB.sql file must have entries
            //whose IDs are sequential
            //********************************************
            string xCnn = "";

            Trace.WriteNameValuePairs("xServer", xServer, "xDatabase", xDatabase,
                "xLoginID", xLoginID);

            //create connection string from parameters
            xCnn = "Persist Security Info=false;Integrated Security=false;" +
                "database=" + xDatabase + ";server=" + xServer + @";user id=" + xLoginID +
                ";password=" + xPassword;

            //connection object
            using (SqlConnection oCnn = new SqlConnection(xCnn))
            {
                //get sql script for creating Forte data objects
                string xCreateDBObjectsFile = LMP.Data.Application.AppDirectory +
                    @"\UpdateNetworkDB.sql";

                Trace.WriteNameValuePairs("xCreateDBObjectsFile", xCreateDBObjectsFile);

                //load new text into file
                FileInfo oFile = new FileInfo(xCreateDBObjectsFile);

                string xSQLScript = null;

                //load new script
                using (StreamReader oReader = oFile.OpenText())
                {
                    xSQLScript = oReader.ReadToEnd();
                }

                //replace every reference to mp10 with xDataBase
                xSQLScript = xSQLScript.Replace("[mp10]", "[" + xDatabase + "]");

                Trace.WriteNameValuePairs("xSQLScript", xSQLScript.Substring(0, 25) + "...");

                try
                {
                    oCnn.Open();
                }
                catch (System.Exception oE)
                {
                    if (oE.Message.Contains("An error has occurred while establishing a connection to the server"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_ServerNotAvailable") + xServer, oE);
                    }
                    else if (oE.Message.Contains("Login failed for user"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_LoginInvalid") + xLoginID, oE);
                    }
                    else
                    {
                        // GLOG : 4885 : JSW
                        // remove sql admin password from string displayed in error message
                        int iPos = xCnn.IndexOf(";password");
                        string xCnnNoPWord = xCnn.Substring(0, iPos); 
                        throw new LMP.Exceptions.DBConnectionException(
                            LMP.Resources.GetLangString("Error_CouldNotConnectToDB") + xCnnNoPWord, oE);

                    }
                }

                string xIDs = GetMetaDataValue(oCnn, "DBStructureUpdateIDs");

                //largest ID executed
                string xLastID = "";

                if (xIDs == "")
                    xLastID = "0";
                else if (xIDs.Contains("|"))
                {
                    //we're using the old format, which holds
                    //every ID executed - get the last one only
                    xIDs = xIDs.TrimEnd('|');
                    string[] aIDs = xIDs.Split('|');
                    xLastID = aIDs[aIDs.Length - 1];
                }
                else
                {
                    xLastID = xIDs;
                }

                Trace.WriteNameValuePairs("xLastID", xLastID);

                using (SqlCommand oCmd = new SqlCommand())
                {
                    oCmd.Connection = oCnn;

                    int iStartPos;
                    int iEndPos;

                    //create array of queries from script
                    //it's in transact sql, which the command object 
                    //cannot process, so the "GO" lines are removed
                    string[] xCmds = xSQLScript.Replace("\r\n", "")
                        .Replace("GO", "~").Split(new Char[] { '~' },
                        StringSplitOptions.RemoveEmptyEntries);

                    bool bDataObject = false;
                    try
                    {
                        foreach (string xCmd in xCmds)
                        {
                            Trace.WriteNameValuePairs("xCmd", xCmd);

                            if (xCmd.Trim() == "")
                                continue;

                            string xModCmd = xCmd;

                            //parse out command ID
                            int iPos = xModCmd.IndexOf(".");

                            if (iPos == -1)
                            {
                                //something's wrong - no ID specified -
                                throw new LMP.Exceptions.DataException(
                                    LMP.Resources.GetLangString("Error_InvalidCommandID") + xModCmd);
                            }

                            //get ID
                            string xID = xModCmd.Substring(0, iPos);

                            //check if ID has already been executed on this database
                            bool bAlreadyExecuted = int.Parse(xID) <= int.Parse(xLastID);

                            //skip if this command has already been executed on this database
                            if (bAlreadyExecuted)
                                continue;

                            xModCmd = xModCmd.Substring(iPos + 1);

                            oCmd.CommandText = xModCmd;
                            if ((xModCmd.Contains("IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[")
                                // Ignore Foreign Key and Constraint entries
                                && !xModCmd.Contains("type = 'F'") && !xModCmd.Contains("type = 'C'"))
                                || (xModCmd.Contains("CREATE USER") || xModCmd.Contains("sp_grantdbaccess")))
                            {
                                bDataObject = true;
                            }
                            else
                            {
                                bDataObject = false;
                            }

                            if (bDataObject)
                            {
                                iStartPos = xModCmd.IndexOf("IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[");

                                //reset start position to beginning of data object name
                                iStartPos = xModCmd.IndexOf(".[", iStartPos) + 2;
                                //set end position
                                iEndPos = xModCmd.IndexOf("]'", iStartPos);
                            }
                            try
                            {
                                oCmd.ExecuteNonQuery();

                                //set greatest id - note that this assumes that
                                //the commands will have sequential IDs
                                xLastID = xID.Trim();

                                System.Windows.Forms.Application.DoEvents();
                            }
                            catch (System.Exception oE)
                            {
                                if (oCmd.CommandText.Contains("CREATE LOGIN") || oCmd.CommandText.Contains("sp_addlogin"))
                                {
                                    //login already created for this server,
                                    //continue on to next command
                                    continue;
                                }
                                else
                                {
                                    throw new LMP.Exceptions.ServerException(
                                        LMP.Resources.GetLangString("Error_CouldNotCreateDBObject") + oCmd.CommandText, oE);
                                }
                            }
                        }
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.ServerException(
                            LMP.Resources.GetLangString("Error_CouldNotUpdateNetworkDB"), oE);
                    }
                    finally
                    {
                        //write list of executed command
                        //IDs back to the metadata table
                        SetMetaDataValue(oCnn, "DBStructureUpdateIDs", xLastID);
                        Trace.WriteNameValuePairs("xLastID", xLastID);
                        oCnn.Close();
                    }
                }
            }
        }
        private static object GetScalar(SqlConnection oCnn, string xSQL)
        {
            if ((oCnn.State & ConnectionState.Open) == 0)
            {
                try
                {
                    //open connection
                    oCnn.Open();
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
            }

            using (SqlCommand oCmd = new SqlCommand())
            {
                try
                {
                    oCmd.CommandText = xSQL;
                    oCmd.CommandType = CommandType.Text;
                    oCmd.Connection = oCnn;
                    object oRet = oCmd.ExecuteScalar();
                    if (oRet != null)
                        return oRet;
                    else
                        return null;
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SQLStatementException(
                        LMP.Resources.GetLangString("Error_SQLStatementFailed") + xSQL, oE);
                }
            }
        }
        public static DataTable GetOffices(string xServer, string xDatabase)
        {
            string xCnn = "";

            //create connection string from parameters
            xCnn = "Persist Security Info=false;Integrated Security=false;" +
                "database=" + xDatabase + ";server=" + xServer + @";user id=" + mpSuperUserID +
                ";password=" + mpSuperUserPassword;

            //connection object
            using (SqlConnection oCnn = new SqlConnection(xCnn))
            {
                using (SqlCommand oCmd = new SqlCommand())
                {
                    try
                    {
                        oCmd.CommandText = "SELECT DisplayName, ID FROM Offices WHERE UsageState=1";
                        oCmd.CommandType = CommandType.Text;
                        oCmd.Connection = oCnn;
                        DataTable oDT = new DataTable();
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCmd);
                        oAdapter.Fill(oDT);

                        return oDT;
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.SQLStatementException(
                            LMP.Resources.GetLangString("Error_SQLStatementFailed") + oCmd.CommandText, oE);
                    }
                }
            }
        }
        private static string GetMetaDataValue(SqlConnection oCnn, string xKey)
        {
            if ((oCnn.State & ConnectionState.Open) == 0)
            {
                try
                {
                    //open connection
                    oCnn.Open();
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
            }

            using (SqlCommand oCmd = new SqlCommand())
            {
                try
                {
                    oCmd.CommandText = "SELECT [Value] FROM Metadata WHERE [Name] = '" + xKey + "';";
                    oCmd.CommandType = CommandType.Text;
                    oCmd.Connection = oCnn;
                    object oRet = oCmd.ExecuteScalar();
                    if (oRet != null)
                        return (string)oRet;
                    else
                        return "";
                }
                catch
                {
                    return "";
                }
            }
        }
        private static void SetMetaDataValue(SqlConnection oCnn, string xName, string xValue)
        {
            if ((oCnn.State & ConnectionState.Open) == 0)
            {
                //open connection
                oCnn.Open();
            }

            using (SqlCommand oCmd = new SqlCommand())
            {
                oCmd.CommandText = "UPDATE Metadata SET [Value] = '" + xValue
                    + "' WHERE [Name]='" + xName + "'";
                oCmd.CommandType = CommandType.Text;
                oCmd.Connection = oCnn;
                int iNumRowsAffected = oCmd.ExecuteNonQuery();
                if (iNumRowsAffected == 0)
                {
                    oCmd.CommandText = "INSERT INTO Metadata ([Name], [Value]) Values('" +
                        xName + "', '" + xValue + "');";
                    iNumRowsAffected = oCmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        ///  Checks if user already exists in db
        /// <returns>bool</returns>
        /// </summary>
        private static bool DatabaseExists(SqlCommand oCmd, string xDatabase)
        {
            try
            {
                //determine if user is already in database.
                oCmd.CommandText = "SELECT Count(*) FROM sys.databases WHERE name = '" + xDatabase + "'";

                Object oResult = oCmd.ExecuteScalar();
                if (Convert.ToInt32(oResult) != 0)
                    return true;
                else
                {
                    return false;
                }
            }
            catch (System.Exception)
            {
                //temporary workaround
                oCmd.CommandText = "SELECT Count(*) FROM dbo.sysdatabases WHERE name = '" + xDatabase + "'";

                Object oResult = oCmd.ExecuteScalar();
                if (Convert.ToInt32(oResult) != 0)
                    return true;
                else
                {
                    return false;
                }
            }
        }
        public static string GetBlockedServers(string xServer, string xDatabase)
        {
            string xCnn = "";

            //create connection string from parameters
            xCnn = "Persist Security Info=false;Integrated Security=false;" +
                "database=" + xDatabase + ";server=" + xServer + @";user id=" + mpSuperUserID +
                ";password=" + mpSuperUserPassword;

            //connection object
            using (SqlConnection oCnn = new SqlConnection(xCnn))
            {
                try
                {
                    oCnn.Open();
                }
                catch (System.Exception oE)
                {
                    if (oE.Message.Contains("An error has occurred while establishing a connection to the server"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_ServerNotAvailable") + xServer, oE);
                    }
                    else if (oE.Message.Contains("Login failed for user"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_LoginInvalid"), oE);
                    }
                    else
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                            LMP.Resources.GetLangString("Error_CouldNotConnectToDB") + xCnn, oE);

                    }
                }
                return GetMetaDataValue(oCnn, "BlockedServers");
            }
            
        }
        public static void SetBlockedServers(string xServer, string xDatabase, string xValue)
        {
            string xCnn = "";

            //create connection string from parameters
            xCnn = "Persist Security Info=false;Integrated Security=false;" +
                "database=" + xDatabase + ";server=" + xServer + @";user id=" + mpSuperUserID +
                ";password=" + mpSuperUserPassword;

            //connection object
            using (SqlConnection oCnn = new SqlConnection(xCnn))
            {
                try
                {
                    oCnn.Open();
                }
                catch (System.Exception oE)
                {
                    if (oE.Message.Contains("An error has occurred while establishing a connection to the server"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_ServerNotAvailable") + xServer, oE);
                    }
                    else if (oE.Message.Contains("Login failed for user"))
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                           LMP.Resources.GetLangString("Error_LoginInvalid"), oE);
                    }
                    else
                    {
                        throw new LMP.Exceptions.DBConnectionException(
                            LMP.Resources.GetLangString("Error_CouldNotConnectToDB") + xCnn, oE);

                    }
                }
                SetMetaDataValue(oCnn, "BlockedServers", xValue);
            }
        }
        #endregion
    }
}
