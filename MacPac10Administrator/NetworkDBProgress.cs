using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac10.Administration
{
    public partial class NetworkDBProgress : Form
    {
        public NetworkDBProgress()
        {
            InitializeComponent();
        }
        public void AddRow(string xDataObject)
        {
            this.lstDataObjects.Items.Add(xDataObject);
            this.lstDataObjects.SelectedIndex = this.lstDataObjects.Items.Count - 1;
            Application.DoEvents();
        }

        public void UpdateProgressLabel(string xText)
        {
            this.lblProgress.Text = xText;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}