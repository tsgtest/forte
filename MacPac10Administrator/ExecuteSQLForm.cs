﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac10.Administration
{
    public partial class ExecuteSQLForm : Form
    {
        public ExecuteSQLForm()
        {
            InitializeComponent();
        }

        public string SQL
        {
            get { return this.txtSQL.Text; }
        }

        private void txtSQL_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.SQL.ToUpper().Trim().StartsWith("SELECT "))
                {
                    MessageBox.Show("SELECT statements can't be executed by this function.",
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);

                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.ShowInEnglish(oE);
            }
        }
    }
}
