﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac.Administration
{
    public partial class EditMinimumRequiredClientVersionForm : Form
    {
        string m_xMinReqClientVersion;

        public EditMinimumRequiredClientVersionForm(string xMinRequiredClientVersion)
        {
            InitializeComponent();
            this.m_xMinReqClientVersion = xMinRequiredClientVersion;
        }

        private void EditMinimumRequiredClientVersionForm_Load(object sender, EventArgs e)
        {
            this.txtVersion.Text = m_xMinReqClientVersion;
        }

        public string MinimumRequiredClientVersion
        {
            get { return this.txtVersion.Text; }
        }
    }
}
