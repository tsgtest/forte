using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac10.Administration
{
    public partial class CreateNetworkDBForm : Form
    {
        
        public CreateNetworkDBForm()
        {
            InitializeComponent();
        }

        public string Server
        {
            get { return this.txtServer.Text; }
            set { this.txtServer.Text = value; }
        }

        public string Database
        {
            get { return this.txtDatabase.Text; }
            set { this.txtDatabase.Text = value; }
        }

        public string LoginID
        {
            get { return this.txtLoginID.Text; }
            set { this.txtLoginID.Text = value; }
        }

        public string Password
        {
            get { return this.txtPassword.Text; }
            set { this.txtPassword.Text = value; }
        }
    }
}