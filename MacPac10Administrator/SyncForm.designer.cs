namespace LMP.MacPac10.Administration
{
    partial class SyncForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.grpDB = new System.Windows.Forms.GroupBox();
            this.cmbType = new LMP.Controls.ComboBox();
            this.lblType = new System.Windows.Forms.Label();
            this.txtIISServer = new System.Windows.Forms.TextBox();
            this.lblIISServer = new System.Windows.Forms.Label();
            this.grpIISServer = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtServerAppName = new System.Windows.Forms.TextBox();
            this.grpDB.SuspendLayout();
            this.grpIISServer.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(158, 272);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(77, 27);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(241, 272);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 27);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(99, 33);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(197, 22);
            this.txtServer.TabIndex = 1;
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(99, 72);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(197, 22);
            this.txtDatabase.TabIndex = 3;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(12, 37);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(43, 15);
            this.lblServer.TabIndex = 0;
            this.lblServer.Text = "&Server:";
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Location = new System.Drawing.Point(12, 75);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(57, 15);
            this.lblDatabase.TabIndex = 2;
            this.lblDatabase.Text = "&Database:";
            // 
            // grpDB
            // 
            this.grpDB.Controls.Add(this.txtDatabase);
            this.grpDB.Controls.Add(this.txtServer);
            this.grpDB.Controls.Add(this.lblDatabase);
            this.grpDB.Controls.Add(this.lblServer);
            this.grpDB.Location = new System.Drawing.Point(12, 14);
            this.grpDB.Name = "grpDB";
            this.grpDB.Size = new System.Drawing.Size(312, 122);
            this.grpDB.TabIndex = 0;
            this.grpDB.TabStop = false;
            this.grpDB.Text = "Network Database";
            // 
            // cmbType
            // 
            this.cmbType.AllowEmptyValue = false;
            this.cmbType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbType.Borderless = false;
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbType.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbType.IsDirty = false;
            this.cmbType.LimitToList = true;
            this.cmbType.ListName = "";
            this.cmbType.Location = new System.Drawing.Point(91, 319);
            this.cmbType.MaxDropDownItems = 8;
            this.cmbType.Name = "cmbType";
            this.cmbType.SelectedIndex = -1;
            this.cmbType.SelectedValue = null;
            this.cmbType.SelectionLength = 0;
            this.cmbType.SelectionStart = 0;
            this.cmbType.Size = new System.Drawing.Size(218, 23);
            this.cmbType.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbType.SupportingValues = "";
            this.cmbType.TabIndex = 3;
            this.cmbType.Tag2 = null;
            this.cmbType.Value = "";
            this.cmbType.Visible = false;
            // 
            // lblType
            // 
            this.lblType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(24, 324);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(63, 15);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "&Sync Type:";
            this.lblType.Visible = false;
            // 
            // txtIISServer
            // 
            this.txtIISServer.Location = new System.Drawing.Point(108, 28);
            this.txtIISServer.Name = "txtIISServer";
            this.txtIISServer.Size = new System.Drawing.Size(188, 22);
            this.txtIISServer.TabIndex = 1;
            // 
            // lblIISServer
            // 
            this.lblIISServer.AutoSize = true;
            this.lblIISServer.Location = new System.Drawing.Point(12, 31);
            this.lblIISServer.Name = "lblIISServer";
            this.lblIISServer.Size = new System.Drawing.Size(51, 15);
            this.lblIISServer.TabIndex = 0;
            this.lblIISServer.Text = "&Address:";
            // 
            // grpIISServer
            // 
            this.grpIISServer.Controls.Add(this.label1);
            this.grpIISServer.Controls.Add(this.txtServerAppName);
            this.grpIISServer.Controls.Add(this.lblIISServer);
            this.grpIISServer.Controls.Add(this.txtIISServer);
            this.grpIISServer.Location = new System.Drawing.Point(12, 144);
            this.grpIISServer.Name = "grpIISServer";
            this.grpIISServer.Size = new System.Drawing.Size(312, 112);
            this.grpIISServer.TabIndex = 1;
            this.grpIISServer.TabStop = false;
            this.grpIISServer.Text = "IIS Server";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Application &Name:";
            // 
            // txtServerAppName
            // 
            this.txtServerAppName.Location = new System.Drawing.Point(108, 66);
            this.txtServerAppName.Name = "txtServerAppName";
            this.txtServerAppName.Size = new System.Drawing.Size(188, 22);
            this.txtServerAppName.TabIndex = 3;
            // 
            // SyncForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(335, 313);
            this.Controls.Add(this.grpIISServer);
            this.Controls.Add(this.grpDB);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.lblType);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SyncForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Publish";
            this.grpDB.ResumeLayout(false);
            this.grpDB.PerformLayout();
            this.grpIISServer.ResumeLayout(false);
            this.grpIISServer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.GroupBox grpDB;
        private LMP.Controls.ComboBox cmbType;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.TextBox txtIISServer;
        private System.Windows.Forms.Label lblIISServer;
        private System.Windows.Forms.GroupBox grpIISServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtServerAppName;
    }
}