using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac10.Administration
{
    public partial class SyncForm : Form
    {
        public enum SyncTypes
        {
            Full = 1,
            Incremental = 2
        }

        public SyncForm()
        {
            InitializeComponent();
            this.cmbType.SetList(new string[,] 
                { { "Full", "Full" }, { "Incremental", "Incremental" } });
            this.cmbType.SelectedIndex = 1;
            //Display Server IP field if registry key isn't set
            this.IISServerAddress = LMP.Registry.GetLocalMachineValue(LMP.Data.ForteConstants.mpSyncRegKey, "ServerIP");
            this.IISServerApplicationName = LMP.Registry.GetLocalMachineValue(LMP.Data.ForteConstants.mpSyncRegKey, "ServerApplicationName");
            if (string.IsNullOrEmpty(this.IISServerApplicationName))
                this.IISServerApplicationName = "ForteSyncServer";
        }

        public string Server
        {
            get { return this.txtServer.Text; }
            set { this.txtServer.Text = value; }
        }

        public string Database
        {
            get { return this.txtDatabase.Text; }
            set { this.txtDatabase.Text = value; }
        }

        public string IISServerAddress
        {
            get { return this.txtIISServer.Text; }
            set { this.txtIISServer.Text = value; }
        }

        public string IISServerApplicationName
        {
            get { return this.txtServerAppName.Text; }
            set { this.txtServerAppName.Text = value; }
        }

        public SyncTypes SyncType
        {
            get
            {
                if (this.cmbType.Text == "Full")
                    return SyncTypes.Full;
                else
                    return SyncTypes.Incremental;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtServer.Text == "")
                {
                    MessageBox.Show("Server is required information", 
                        LMP.ComponentProperties.ProductName, 
                        MessageBoxButtons.OK, 
                        MessageBoxIcon.Exclamation);
                    this.txtServer.Focus();
                    return;
                }
                else if (this.txtDatabase.Text == "")
                {
                    MessageBox.Show("Database is required information", 
                        LMP.ComponentProperties.ProductName, 
                        MessageBoxButtons.OK, 
                        MessageBoxIcon.Exclamation);
                    this.txtDatabase.Focus();
                    return;
                }
                else if (this.txtIISServer.Text == "")
                {
                    MessageBox.Show("IIS Server IP is required information", 
                        LMP.ComponentProperties.ProductName, 
                        MessageBoxButtons.OK, 
                        MessageBoxIcon.Exclamation);
                    this.txtIISServer.Focus();
                    return;
                }

                
                //JTS 12/13/10: Don't stop process if only
                //unable to update sticky registry value
                try
                {
                    //Set ServerIP registry value
                    LMP.Registry.SetLocalMachineValue(
                        LMP.Data.ForteConstants.mpSyncRegKey,
                        "ServerIP", this.IISServerAddress);

                    //Set Server Application Name registry value
                    LMP.Registry.SetLocalMachineValue(
                        LMP.Data.ForteConstants.mpSyncRegKey,
                        "ServerApplicationName", this.IISServerApplicationName);
                }
                catch { }
                this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}