namespace LMP.MacPac10.Administration
{
    partial class DuplicateDBForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSourceDatabase = new System.Windows.Forms.Label();
            this.lblSourceServer = new System.Windows.Forms.Label();
            this.txtSourceDatabase = new System.Windows.Forms.TextBox();
            this.txtSourceServer = new System.Windows.Forms.TextBox();
            this.grpSource = new System.Windows.Forms.GroupBox();
            this.grpDestination = new System.Windows.Forms.GroupBox();
            this.grpLogon = new System.Windows.Forms.GroupBox();
            this.lblDestPassword = new System.Windows.Forms.Label();
            this.lblDestLoginID = new System.Windows.Forms.Label();
            this.txtDestLoginID = new System.Windows.Forms.TextBox();
            this.txtDestPassword = new System.Windows.Forms.TextBox();
            this.txtDestDatabase = new System.Windows.Forms.TextBox();
            this.txtDestServer = new System.Windows.Forms.TextBox();
            this.lblDestServer = new System.Windows.Forms.Label();
            this.lblDestDatabase = new System.Windows.Forms.Label();
            this.grpSource.SuspendLayout();
            this.grpDestination.SuspendLayout();
            this.grpLogon.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(141, 371);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(77, 25);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(224, 371);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 25);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblSourceDatabase
            // 
            this.lblSourceDatabase.AutoSize = true;
            this.lblSourceDatabase.Location = new System.Drawing.Point(22, 72);
            this.lblSourceDatabase.Name = "lblSourceDatabase";
            this.lblSourceDatabase.Size = new System.Drawing.Size(57, 15);
            this.lblSourceDatabase.TabIndex = 2;
            this.lblSourceDatabase.Text = "&Database:";
            // 
            // lblSourceServer
            // 
            this.lblSourceServer.AutoSize = true;
            this.lblSourceServer.Location = new System.Drawing.Point(22, 37);
            this.lblSourceServer.Name = "lblSourceServer";
            this.lblSourceServer.Size = new System.Drawing.Size(43, 15);
            this.lblSourceServer.TabIndex = 0;
            this.lblSourceServer.Text = "&Server:";
            // 
            // txtSourceDatabase
            // 
            this.txtSourceDatabase.Location = new System.Drawing.Point(83, 68);
            this.txtSourceDatabase.Name = "txtSourceDatabase";
            this.txtSourceDatabase.Size = new System.Drawing.Size(198, 22);
            this.txtSourceDatabase.TabIndex = 3;
            // 
            // txtSourceServer
            // 
            this.txtSourceServer.Location = new System.Drawing.Point(83, 33);
            this.txtSourceServer.Name = "txtSourceServer";
            this.txtSourceServer.Size = new System.Drawing.Size(198, 22);
            this.txtSourceServer.TabIndex = 1;
            // 
            // grpSource
            // 
            this.grpSource.Controls.Add(this.txtSourceDatabase);
            this.grpSource.Controls.Add(this.txtSourceServer);
            this.grpSource.Controls.Add(this.lblSourceServer);
            this.grpSource.Controls.Add(this.lblSourceDatabase);
            this.grpSource.Location = new System.Drawing.Point(12, 12);
            this.grpSource.Name = "grpSource";
            this.grpSource.Size = new System.Drawing.Size(298, 114);
            this.grpSource.TabIndex = 0;
            this.grpSource.TabStop = false;
            this.grpSource.Text = "Source";
            // 
            // grpDestination
            // 
            this.grpDestination.Controls.Add(this.grpLogon);
            this.grpDestination.Controls.Add(this.txtDestDatabase);
            this.grpDestination.Controls.Add(this.txtDestServer);
            this.grpDestination.Controls.Add(this.lblDestServer);
            this.grpDestination.Controls.Add(this.lblDestDatabase);
            this.grpDestination.Location = new System.Drawing.Point(12, 132);
            this.grpDestination.Name = "grpDestination";
            this.grpDestination.Size = new System.Drawing.Size(297, 222);
            this.grpDestination.TabIndex = 1;
            this.grpDestination.TabStop = false;
            this.grpDestination.Text = "Destination";
            // 
            // grpLogon
            // 
            this.grpLogon.Controls.Add(this.lblDestPassword);
            this.grpLogon.Controls.Add(this.lblDestLoginID);
            this.grpLogon.Controls.Add(this.txtDestLoginID);
            this.grpLogon.Controls.Add(this.txtDestPassword);
            this.grpLogon.Location = new System.Drawing.Point(8, 104);
            this.grpLogon.Name = "grpLogon";
            this.grpLogon.Size = new System.Drawing.Size(281, 111);
            this.grpLogon.TabIndex = 4;
            this.grpLogon.TabStop = false;
            this.grpLogon.Text = "Logon to SQL Server";
            // 
            // lblDestPassword
            // 
            this.lblDestPassword.AutoSize = true;
            this.lblDestPassword.Location = new System.Drawing.Point(14, 75);
            this.lblDestPassword.Name = "lblDestPassword";
            this.lblDestPassword.Size = new System.Drawing.Size(59, 15);
            this.lblDestPassword.TabIndex = 2;
            this.lblDestPassword.Text = "&Password:";
            // 
            // lblDestLoginID
            // 
            this.lblDestLoginID.AutoSize = true;
            this.lblDestLoginID.Location = new System.Drawing.Point(14, 36);
            this.lblDestLoginID.Name = "lblDestLoginID";
            this.lblDestLoginID.Size = new System.Drawing.Size(50, 15);
            this.lblDestLoginID.TabIndex = 0;
            this.lblDestLoginID.Text = "&Login ID:";
            // 
            // txtDestLoginID
            // 
            this.txtDestLoginID.Location = new System.Drawing.Point(75, 32);
            this.txtDestLoginID.Name = "txtDestLoginID";
            this.txtDestLoginID.Size = new System.Drawing.Size(197, 22);
            this.txtDestLoginID.TabIndex = 1;
            // 
            // txtDestPassword
            // 
            this.txtDestPassword.AcceptsReturn = true;
            this.txtDestPassword.Location = new System.Drawing.Point(75, 72);
            this.txtDestPassword.Name = "txtDestPassword";
            this.txtDestPassword.PasswordChar = '*';
            this.txtDestPassword.Size = new System.Drawing.Size(197, 22);
            this.txtDestPassword.TabIndex = 3;
            // 
            // txtDestDatabase
            // 
            this.txtDestDatabase.Location = new System.Drawing.Point(83, 63);
            this.txtDestDatabase.Name = "txtDestDatabase";
            this.txtDestDatabase.Size = new System.Drawing.Size(198, 22);
            this.txtDestDatabase.TabIndex = 3;
            // 
            // txtDestServer
            // 
            this.txtDestServer.Location = new System.Drawing.Point(83, 29);
            this.txtDestServer.Name = "txtDestServer";
            this.txtDestServer.Size = new System.Drawing.Size(198, 22);
            this.txtDestServer.TabIndex = 1;
            // 
            // lblDestServer
            // 
            this.lblDestServer.AutoSize = true;
            this.lblDestServer.Location = new System.Drawing.Point(22, 32);
            this.lblDestServer.Name = "lblDestServer";
            this.lblDestServer.Size = new System.Drawing.Size(43, 15);
            this.lblDestServer.TabIndex = 0;
            this.lblDestServer.Text = "&Server:";
            // 
            // lblDestDatabase
            // 
            this.lblDestDatabase.AutoSize = true;
            this.lblDestDatabase.Location = new System.Drawing.Point(22, 67);
            this.lblDestDatabase.Name = "lblDestDatabase";
            this.lblDestDatabase.Size = new System.Drawing.Size(57, 15);
            this.lblDestDatabase.TabIndex = 2;
            this.lblDestDatabase.Text = "&Database:";
            // 
            // DuplicateDBForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 408);
            this.Controls.Add(this.grpDestination);
            this.Controls.Add(this.grpSource);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DuplicateDBForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Duplicate";
            this.grpSource.ResumeLayout(false);
            this.grpSource.PerformLayout();
            this.grpDestination.ResumeLayout(false);
            this.grpDestination.PerformLayout();
            this.grpLogon.ResumeLayout(false);
            this.grpLogon.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblSourceDatabase;
        private System.Windows.Forms.Label lblSourceServer;
        private System.Windows.Forms.TextBox txtSourceDatabase;
        private System.Windows.Forms.TextBox txtSourceServer;
        private System.Windows.Forms.GroupBox grpSource;
        private System.Windows.Forms.GroupBox grpDestination;
        private System.Windows.Forms.TextBox txtDestDatabase;
        private System.Windows.Forms.TextBox txtDestServer;
        private System.Windows.Forms.Label lblDestServer;
        private System.Windows.Forms.Label lblDestDatabase;
        private System.Windows.Forms.Label lblDestPassword;
        private System.Windows.Forms.Label lblDestLoginID;
        private System.Windows.Forms.TextBox txtDestPassword;
        private System.Windows.Forms.TextBox txtDestLoginID;
        private System.Windows.Forms.GroupBox grpLogon;
    }
}