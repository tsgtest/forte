using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using Infragistics.Win.UltraWinTree;
using LMP.Administration.Controls;
using LMP.MacPac.Sync;
using LMP.Data;
using LMP.Administration;
using Word = Microsoft.Office.Interop.Word;
using ADOX;
using System.Runtime.InteropServices;
using System.Collections; //GLOG 8220
using System.Diagnostics; //GLOG 8034

namespace LMP.MacPac10.Administration
{
    delegate void BuildCacheAsyncHandler(string xServerIP, string xServerApplicationName);

    public partial class MainForm : Form
    {
        private static bool bUserContentManagerStartComplete = true; //GLOG 8034
        private static System.Diagnostics.Process m_oUserContentProcess; //GLOG 8034
        private const int SW_SHOWNORMAL = 1;
        #region *********************static members*********************
        [STAThread]
        static void Main()
        {
            try
            {
                //GLOG 8289: These need to appear before any DoEvents calls
                System.Windows.Forms.Application.EnableVisualStyles();
                System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
                //GLOG : 8224 : JSW
                //load only if user has admin access
                try
                {
                    bool bHasAdminAccess;
                    if (LMP.Data.Application.AdminDBExists)
                    {
                        LMP.Data.LocalConnection.ConnectToDB(true);
                        //GLOG 8289: For Forte Local, if User has Admin Access (or Admin Group is empty)
                        //make sure People record has been imported from Public People to allow login
                        if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                        {
                            //test that person exists
                            Person oPerson = Person.PersonFromSystemUserName(System.Environment.UserName);
                            if (oPerson == null)
                            {
                                //If no record in Public DB, copy from People DB if Admin Access is allowed
                                Person oPersonImport = PublicPersons.ItemFromSystemUserName(System.Environment.UserName);
                                if (oPersonImport != null && LMP.Data.Person.HasAdminAccess(oPersonImport.ID1, 0, true))
                                {
                                    //no person record having that system user name -
                                    //attempt to copy person from People table in FortePublic.mdb
                                    try
                                    {
                                        LocalPersons oLocalPersons = new LocalPersons();
                                        oLocalPersons.CopyPublicUser(oPersonImport.ID1, false, false);

                                        //test that person was copied
                                        oPerson = Person.PersonFromSystemUserName(System.Environment.UserName);
                                    }
                                    catch (LMP.Exceptions.NotInCollectionException)
                                    {
                                        oPerson = null;
                                    }
                                }
                            }
                            if (oPerson != null)
                                bHasAdminAccess = LMP.Data.Person.HasAdminAccess(oPerson.ID1, 0, true);
                            else
                                bHasAdminAccess = false;
                        }
                        else if (!LMP.Data.Person.HasAdminAccess(System.Environment.UserName))
                            bHasAdminAccess = false;
                        else
                            bHasAdminAccess = true;
                    }
                    else
                        bHasAdminAccess = false;

                    if (bHasAdminAccess == false)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Error_MethodNotUsableUnlessAdmin"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                        return;
                    }
                }
                catch { }


                System.Windows.Forms.Application.Run(new MainForm());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************constructors*********************
        public MainForm()
        {
            InitializeComponent();
            try
            {
                //login to the data component
                bool bRet = LMP.Data.Application.Login(System.Environment.UserName, true);

                //update local admin database
                LMP.Data.Application.UpdateLocalDBStructureIfNecessary(true);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DBConnectionException(
                    LMP.Resources.GetLangString("Error_CouldNotLoginToAdminDB"), oE);
            }
        }
        #endregion
        #region *********************event handlers*********************
        //GLOG 8281
        const string FORTE_LOCAL_USER_DB_CREATION_SCRIPT = "CreateFullLocalUserDB.sql";
        const string FORTE_LOCAL_PUBLIC_DB_CREATION_SCRIPT = "CreateFullLocalPublicDB.sql";
        const string FORTE_LOCAL_PEOPLE_DB_CREATION_SCRIPT = "CreateFullLocalPeopleDB.sql";
        const string FORTE_USER_CONTENT_MGR_EXE = "ForteUserContentManager.exe"; //GLOG 8034
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {

                LoadCategories();
                this.mnuUtilities_EditRibbonXML.Visible = LMP.Utility.InDevelopmentMode;
                UltraTreeNode oTreeNode = this.treeCategories.Nodes[0];
                oTreeNode.Selected = true;
				
				//GLOG : 7998 : ceh
                //show-hide appropriate items
                if (LMP.MacPac.MacPacImplementation.IsToolkit)
                {
                    this.fileToolStripMenuItem.Visible = false;
                    this.mnuUtilities.Visible = false;
                    this.databaseToolStripMenuItem.Visible = false;
                    this.mnuMain_LocalDB_Publish.Visible = false;
                    this.toolStripMenuItem3.Visible = false;
                    this.mnuUtilities_CreateLocalDatabases.Visible = false; //GLOG 8252
                    this.mnuUtilities_UserContentManager.Visible = false; //GLOG 8034
                }
                else if (LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                {
                    this.databaseToolStripMenuItem.Visible = false;
                    this.mnuMain_LocalDB_Publish.Visible = false;
                    this.toolStripMenuItem3.Visible = false;
                    this.mnuMain_LocalDB_CreateUserCopy.Text = "Create ForteLocal.mdb";
                    this.mnuUtilities_BuildCache.Visible = false;
                    this.mnuUtilities_CreateLocalDatabases.Visible = false; //GLOG 8252
                    this.mnuUtilities_UserContentManager.Visible = false; //GLOG 8034
                }
                //GLOG 8220: Removed from Development Mode
                else
                {
                    //GLOG 8281: Don't show item if creation scripts are not present
                    this.mnuUtilities_CreateLocalDatabases.Visible = (System.IO.File.Exists(LMP.Data.Application.AppDirectory + "\\" + FORTE_LOCAL_PEOPLE_DB_CREATION_SCRIPT)
                        && System.IO.File.Exists(LMP.Data.Application.AppDirectory + "\\" + FORTE_LOCAL_PUBLIC_DB_CREATION_SCRIPT));
                    this.mnuUtilities_UserContentManager.Visible = System.IO.File.Exists(LMP.Data.Application.AppDirectory + "\\" + FORTE_USER_CONTENT_MGR_EXE); //GLOG 8034
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// ensure current usercontrol is valid before permitting navigation to another control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void treeCategories_BeforeSelect(object sender, Infragistics.Win.UltraWinTree.BeforeSelectEventArgs e)
        {
            try
            {
                if (this.treeCategories.SelectedNodes.Count == 0)
                    return;
                UltraTreeNode oNode = this.treeCategories.SelectedNodes[0];
                if (oNode.Tag is UserControl)
                {
                    if (!((LMP.Administration.Controls.AdminManagerBase)oNode.Tag).IsValid)
                        e.Cancel = true;
                    else
                        //notify admin base class control will be unloaded
                        ((AdminManagerBase)(Control)oNode.Tag)
                                .OnBeforeControlUnloaded(this.treeCategories, new System.EventArgs());
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeCategories_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                LoadControl(e.NewSelections[0]);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMain_NetworkDB_CreateNew_Click(object sender, EventArgs e)
        {
            try
            {
                //prompt for db parameters
                using (CreateNetworkDBForm oForm = new CreateNetworkDBForm())
                {
                    DialogResult iChoice = oForm.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();

                    if (iChoice == DialogResult.OK)
                    {
                        //create new db
                        NetworkDB.CreateNetworkDB(oForm.Server, oForm.Database, oForm.LoginID, oForm.Password);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMain_NetworkDB_Update_Click(object sender, EventArgs e)
        {
            try
            {
                //prompt for db parameters
                using (CreateNetworkDBForm oForm = new CreateNetworkDBForm())
                {
                    oForm.Text = "Update Network DB Structure";
                    DialogResult iChoice = oForm.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();

                    if (iChoice == DialogResult.OK)
                    {
                        //create new db
                        NetworkDB.UpdateNetworkDB(oForm.Server, oForm.Database, oForm.LoginID, oForm.Password);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMain_NetworkDB_BlockServers_Click(object sender, EventArgs e)
        {
            try
            {
                using (BlockServersForm oForm = new BlockServersForm())
                {
                    DialogResult iChoice = oForm.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();
                    if (iChoice == DialogResult.OK)
                    {
                        NetworkDB.SetBlockedServers(oForm.Server, oForm.Database, oForm.IISServers);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                SaveCurrentRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMain_LocalDB_Backup_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();
                //prompt for db parameters
                SaveFileDialog oDlg = new SaveFileDialog();
                oDlg.OverwritePrompt = true;
                oDlg.InitialDirectory = LMP.Data.Application.WritableDBDirectory; //JTS 3/28/13
                oDlg.Filter = "MS Access Databases (.mdb)|*.mdb";
                DialogResult iChoice = oDlg.ShowDialog();
                System.Windows.Forms.Application.DoEvents();

                if (iChoice == DialogResult.OK)
                {
                    //get admin db full name
                    string xAdminDB = LMP.Data.Application.WritableDBDirectory + @"\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13

                    //backup
                    File.Copy(xAdminDB, oDlg.FileName);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMain_LocalDB_CreateUserCopy_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();

                //GLOG 8281
                if (LMP.MacPac.MacPacImplementation.IsFullLocal && !System.IO.File.Exists(LMP.Data.Application.AppDirectory + "\\" + FORTE_LOCAL_USER_DB_CREATION_SCRIPT))
                {
                    MessageBox.Show("Missing file '" + FORTE_LOCAL_USER_DB_CREATION_SCRIPT + "'. This must exist in " + LMP.Data.Application.AppDirectory + 
                        " in order to create ForteLocal.mdb.", LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                //prompt for location
                CreateUserDatabaseForm oDlg = new CreateUserDatabaseForm(
                    LMP.Data.Application.WritableDBDirectory, "", ""); //JTS 3/28/13

                DialogResult iChoice = oDlg.ShowDialog(this);
                System.Windows.Forms.Application.DoEvents();

                if (iChoice == DialogResult.OK)
                {
                    //GLOG 8220
                    string xAdminDir = LMP.MacPac.MacPacImplementation.IsFullLocal ? LMP.Data.Application.PublicDataDirectory : LMP.Data.Application.WritableDBDirectory;
                    //get admin db full name
                    string xAdminDB = xAdminDir + @"\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13
                    string xUserDB = oDlg.DBLocation + @"\" + LMP.Data.Application.UserDBName;
                    string xSegmentsDB = oDlg.DBLocation + @"\ForteSegments.mdb";

                    //copy admin db to Forte.mdb and overwrite if this file exists.
                    File.Copy(xAdminDB, xUserDB, true);

                    //get whether or not to split the user db
                    string xSplitUserDB =  !LMP.MacPac.MacPacImplementation.IsFullLocal ? LMP.Registry.GetMacPac10Value("SplitUserDB") : ""; //GLOG 8220

                    if (xSplitUserDB == "1")
                    {
                        if (File.Exists(xSegmentsDB))
                            File.Delete(xSegmentsDB);

                        //create segments db
                        ADOX.CatalogClass oCat = new ADOX.CatalogClass();

                        object oDB = oCat.Create("Provider=" + LocalConnection.ConnectionProvider + ";" +
                               "Data Source=" + xSegmentsDB + ";" +
                               "Jet OLEDB:Engine Type=5");

                        //JTS 2/18/16: These lines ensures new db doesn't remain open
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oCat.Tables);
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oCat.ActiveConnection);
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oCat);

                        //add segments table segments db except the segments table
                        //GLOG 15758
                        using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                            @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xSegmentsDB +
                            ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //JTS 3/28/13
                            @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                        {
                            oLocalDBCnn.Open();

                            using (OleDbCommand oCommand = oLocalDBCnn.CreateCommand())
                            {
                                oCommand.CommandText = @"SELECT * INTO Segments  FROM Segments IN '" + xUserDB + "';";
                                oCommand.Connection = oLocalDBCnn;
                                oCommand.ExecuteNonQuery();
                            }

                            oLocalDBCnn.Close();
                        }
                    }

                    //open connection to local db
                    using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                        @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xUserDB +
                        ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //JTS 3/28/13
                        @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                    {
                        oLocalDBCnn.Open();
                        using (OleDbCommand oCommand = oLocalDBCnn.CreateCommand())
                        {
                            if (xSplitUserDB == "1")
                            {
                                //delete segments table - link will be established on session start
                                oCommand.CommandText = "DROP TABLE Segments;";
                                oCommand.Connection = oLocalDBCnn;
                                oCommand.ExecuteNonQuery();
                            }

                            int iNumRowsAffected = 0;
                            try
                            {
                                //Clear records that don't belong
                                oCommand.CommandText = "DELETE * FROM Metadata WHERE [Name] <> 'TagPrefixID' AND [Name] <> 'TaskPaneSegmentTypes' AND [Name] <> 'EncryptionPassword' AND [Name] <> 'DBStructureUpdateIDs' AND [Name] <> 'CustomCode';";
                                oCommand.CommandType = CommandType.Text;
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch { }
                            try
                            {
                                oCommand.CommandText = "UPDATE Metadata SET [Value] = 'false' WHERE [Name] = 'IsAdminDb';";
                                oCommand.CommandType = CommandType.Text;
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                                if (iNumRowsAffected == 0)
                                {
                                    // There was no record to update. Add a record.
                                    oCommand.CommandText = "Insert into Metadata([Name], [Value]) VALUES('IsAdminDb', 'false')";
                                    iNumRowsAffected = oCommand.ExecuteNonQuery();
                                }
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            //GLOG 8220: No Network DB info for Local mode
                            if (!LMP.MacPac.MacPacImplementation.IsFullLocal)
                            {
                                try
                                {
                                    //set DefaultServer key in DB Metadata table
                                    oCommand.CommandText = "UPDATE Metadata SET [Value]='" + oDlg.Server + "' WHERE [Name]='DefaultServer'";
                                    iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    if (iNumRowsAffected == 0)
                                    {
                                        // There was no record to update. Add a record.
                                        oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('DefaultServer', '" + oDlg.Server + "')";
                                        iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LMP.Error.Show(ex);
                                }
                                //set DefaultDatabase key in DB Metadata table
                                try
                                {
                                    oCommand.CommandText = "UPDATE Metadata SET [Value] = '" + oDlg.Database + "' WHERE [Name] = 'DefaultDatabase'";
                                    iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    if (iNumRowsAffected == 0)
                                    {
                                        // There was no record to update. Add a record.
                                        oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('DefaultDatabase', '" + oDlg.Database + "')";
                                        iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LMP.Error.Show(ex);
                                }
                                try
                                {
                                    //set sync server uri key in DB Metadata table
                                    oCommand.CommandText = "UPDATE Metadata SET [Value]='" + oDlg.IISServerIPAddress + "' WHERE [Name]='IISServerIPAddress'";
                                    iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    if (iNumRowsAffected == 0)
                                    {
                                        // There was no record to update. Add a record.
                                        oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('IISServerIPAddress', '" + oDlg.IISServerIPAddress + "')";
                                        iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LMP.Error.Show(ex);
                                }
                                try
                                {
                                    //set sync server application name in DB Metadata table
                                    oCommand.CommandText = "UPDATE Metadata SET [Value]='" + oDlg.ServerApplicationName + "' WHERE [Name]='ServerApplicationName'";
                                    iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    if (iNumRowsAffected == 0)
                                    {
                                        // There was no record to update. Add a record.
                                        oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('ServerApplicationName', '" + oDlg.ServerApplicationName + "')";
                                        iNumRowsAffected = oCommand.ExecuteNonQuery();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LMP.Error.Show(ex);
                                }
                            }
                            else //GLOG 8220: Clear DBStructureUpdateIDs value for ForteLocal.mdb
                            {
                                try
                                {
                                    //set date created
                                    oCommand.CommandText = "UPDATE Metadata SET [Value]='' WHERE [Name]='DBStructureUpdateIDs'";
                                    oCommand.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    LMP.Error.Show(ex);
                                }
                            }
                            try
                            {
                                //set date created
                                oCommand.CommandText = "UPDATE Metadata SET [Value]='" +
                                    String.ConvertDateToISO8601(LMP.Data.Application.GetCurrentEditTime())
                                    + "' WHERE [Name]='DateCreated'";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                                if (iNumRowsAffected == 0)
                                {
                                    // There was no record to update. Add a record.
                                    oCommand.CommandText = "INSERT INTO Metadata([Name], [Value]) VALUES('DateCreated', '"
                                        + String.ConvertDateToISO8601(LMP.Data.Application.GetCurrentEditTime()) + "')";
                                    iNumRowsAffected = oCommand.ExecuteNonQuery();
                                }
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                //clear out people table -
                                //related records will be cleared automatically
                                //due to referential integrity
                                oCommand.CommandText = "DELETE * FROM People";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            //clear out UserSegments, VariableSets, FavoritePeople, Deletions and Permissions, 
                            //since these aren't set for referential integrity
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM VariableSets";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM UserSegments";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM Permissions";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM FavoritePeople";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM Deletions";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                                {
                                    oCommand.CommandText = "DELETE * FROM KeySets";
                                }
                                else
                                {
                                    //Remove user-related Keyset records
                                    oCommand.CommandText = "DELETE * FROM KeySets WHERE OwnerID1 > 0";
                                }
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            //GLOG 8220:  Remove Public tables from ForteLocal.mdb
                            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                            {

                                string[] aPublicTables = { "GroupAssignments", "PeopleGroups", "People", "Offices", "Assignments", "Counties", "States", "Countries", 
                                                            "Couriers", "Courts",  "ExternalConnections", "Folders", "Jurisdictions4", "Jurisdictions3", "Jurisdictions2", "Jurisdictions1", 
                                                            "Jurisdictions0", "AddressFormats", "Addresses", "Lists", "Restrictions", "Segments", "Translations", "ValueSets" };
                                foreach (string xTable in aPublicTables)
                                {
                                    try
                                    {
                                        //Remove any constraints to allow dropping tables
                                        //GLOG 8282
                                        ArrayList aRelations = LMP.Data.SimpleDataCollection.GetArray(oLocalDBCnn, "SELECT DISTINCT szRelationship FROM Msysrelationships WHERE szObject = '" + xTable + "'");
                                        foreach (object oItem in aRelations)
                                        {
                                            string xRelName = ((object[])oItem)[0].ToString();
                                            oCommand.CommandText = "ALTER TABLE " + xTable + " DROP CONSTRAINT [" + xRelName + "]";
                                            try
                                            {
                                                oCommand.ExecuteNonQuery();
                                            }
                                            catch { }
                                        }
                                        //if (xTable.ToUpper() == "KEYSETS")
                                        //{
                                        //    oCommand.CommandText = "SELECT * INTO KeysetsPrivate FROM KeySets";
                                        //    oCommand.ExecuteNonQuery();
                                        //}
                                        //Don't delete People - just remove constraints
                                        if (xTable.ToUpper() != "PEOPLE")
                                        {
                                            oCommand.CommandText = "DROP TABLE " + xTable;
                                            oCommand.ExecuteNonQuery();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LMP.Error.Show(ex);
                                    }
                                }
                                //Make sure all objects required by queries are created
                                LMP.Data.LocalConnection.RefreshPublicDBLinksIfNecessary(oLocalDBCnn, LMP.Data.Application.PublicDataDirectory + @"\Forte.mdw", true);
                                LMP.Data.Application.ApplyStructureUpdatesToDB(xUserDB, FORTE_LOCAL_USER_DB_CREATION_SCRIPT);
                            }

                        }

                        oLocalDBCnn.Close();

                        //GLOG : 8883 : JSW
                        if (!Registry.Is64Bit())
                            LMP.Data.Application.CompactJetDB(xUserDB);

                    }
                    if (xSplitUserDB == "1")
                    {
                        MessageBox.Show("The user databases '" + xUserDB + "' and '" + xSegmentsDB + "' can now be deployed.",
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("The user database '" + xUserDB + "' can now be deployed.",
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuUtilities_CreateLocalDatabases_Click(object sender, EventArgs e)
        {
            //GLOG 8252: Create new Local Public and Local People databases
            //starting with data in current ForteAdmin.mdb
            try
            {
                SaveCurrentRecord();

                if (!System.IO.File.Exists(LMP.Data.Application.AppDirectory + "\\" + FORTE_LOCAL_PUBLIC_DB_CREATION_SCRIPT))
                {
                    MessageBox.Show("Missing file '" + FORTE_LOCAL_PUBLIC_DB_CREATION_SCRIPT+ "'. This file must exist in " + LMP.Data.Application.AppDirectory +
                        " in order to create the Forte Local databases.", LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (!System.IO.File.Exists(LMP.Data.Application.AppDirectory + "\\" + FORTE_LOCAL_PEOPLE_DB_CREATION_SCRIPT))
                {
                    MessageBox.Show("Missing file '" + FORTE_LOCAL_PEOPLE_DB_CREATION_SCRIPT + "'. This file must exist in " + LMP.Data.Application.AppDirectory +
                        " in order to create the Forte Local databases.", LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                //prompt for location
                CreateUserDatabaseForm oDlg = new CreateUserDatabaseForm(
                    LMP.Data.Application.WritableDBDirectory, true); //JTS 3/28/13

                DialogResult iChoice = oDlg.ShowDialog(this);
                System.Windows.Forms.Application.DoEvents();

                if (iChoice == DialogResult.OK)
                {
                    string xAdminDir = LMP.Data.Application.WritableDBDirectory;
                    string xAdminDB = xAdminDir + @"\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13
                    string xPublicDB = oDlg.DBLocation + @"\ForteLocalPublic.mdb";
                    string xPeopleDB = oDlg.DBLocation + @"\ForteLocalPeople.mdb";

                    if (File.Exists(xPeopleDB))
                        File.Delete(xPeopleDB);

                    //create segments db
                    ADOX.CatalogClass oCat = new ADOX.CatalogClass();

                    object oDB = oCat.Create("Provider=" + LocalConnection.ConnectionProvider + ";" +
                           "Data Source=" + xPeopleDB + ";" +
                           "Jet OLEDB:Engine Type=5");

                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oCat.Tables);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oCat.ActiveConnection);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oCat);

                    //add People and AttyLicenses table only to new database
                    using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                        @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xPeopleDB +
                        ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //JTS 3/28/13
                        @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                    {
                        oLocalDBCnn.Open();

                        using (OleDbCommand oCommand = oLocalDBCnn.CreateCommand())
                        {
                            oCommand.CommandText = @"SELECT * INTO People FROM People IN '" + xAdminDB + "';";
                            oCommand.Connection = oLocalDBCnn;
                            oCommand.ExecuteNonQuery();

                            oCommand.CommandText = @"SELECT * INTO AttyLicenses FROM AttyLicenses IN '" + xAdminDB + "';";
                            oCommand.Connection = oLocalDBCnn;
                            oCommand.ExecuteNonQuery();

                            oCommand.CommandText = @"SELECT * INTO Metadata FROM Metadata IN '" + xAdminDB + "';";
                            oCommand.Connection = oLocalDBCnn;
                            oCommand.ExecuteNonQuery();

                            try
                            {
                                //Clear all MetaData records except PUPOptions
                                oCommand.CommandText = "DELETE * FROM Metadata WHERE [Name] <> 'PUPOptions'";
                                oCommand.CommandType = CommandType.Text;
                                int iDeleted = oCommand.ExecuteNonQuery(); //GLOG 8282
                            }
                            catch { }
                        }
                        oLocalDBCnn.Close();
                    }
                    LMP.Data.Application.ApplyStructureUpdatesToDB(xPeopleDB, FORTE_LOCAL_PEOPLE_DB_CREATION_SCRIPT);
                    LMP.Data.Application.CompactJetDB(xPeopleDB);

                    if (File.Exists(xPublicDB))
                        File.Delete(xPublicDB);

                    //copy admin db to Forte.mdb and overwrite if this file exists.
                    File.Copy(xAdminDB, xPublicDB, true);

                    //open connection to local public db
                    using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                        @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xPublicDB +
                        ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //JTS 3/28/13
                        @"\Forte.mdw;User ID=MacPacPersonnel;Password=fish4mill"))
                    {
                        oLocalDBCnn.Open();
                        using (OleDbCommand oCommand = oLocalDBCnn.CreateCommand())
                        {
                            int iNumRowsAffected = 0;
                            try
                            {
                                //Clear records that don't belong
                                oCommand.CommandText = "DELETE * FROM Metadata WHERE [Name] <> 'TagPrefixID' AND [Name] <> 'TaskPaneSegmentTypes' AND [Name] <> 'EncryptionPassword' AND [Name] <> 'DBStructureUpdateIDs' AND [Name] <> 'CustomCode';";
                                oCommand.CommandType = CommandType.Text;
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch { }
                            try
                            {
                                oCommand.CommandText = "UPDATE Metadata SET [Value] = 'true' WHERE [Name] = 'IsAdminDb';";
                                oCommand.CommandType = CommandType.Text;
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                                if (iNumRowsAffected == 0)
                                {
                                    // There was no record to update. Add a record.
                                    oCommand.CommandText = "Insert into Metadata([Name], [Value]) VALUES('IsAdminDb', 'true')";
                                    iNumRowsAffected = oCommand.ExecuteNonQuery();
                                }
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                //set date created
                                oCommand.CommandText = "UPDATE Metadata SET [Value]='' WHERE [Name]='DBStructureUpdateIDs'";
                                oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            //GLOG 8282: Need to remove constraint from GroupAssignments,
                            //since People records are now in a separate database
                            try
                            {
                                //Remove any constraints to allow dropping tables
                                ArrayList aRelations = LMP.Data.SimpleDataCollection.GetArray(oLocalDBCnn, "SELECT DISTINCT szRelationship FROM Msysrelationships WHERE szObject = 'GroupAssignments' AND szReferencedObject='People'");
                                foreach (object oItem in aRelations)
                                {
                                    string xRelName = ((object[])oItem)[0].ToString();
                                    oCommand.CommandText = "ALTER TABLE GroupAssignments DROP CONSTRAINT [" + xRelName + "]";
                                    try
                                    {
                                        oCommand.ExecuteNonQuery();
                                    }
                                    catch { }
                                }
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                //clear out people table -
                                //related records will be cleared automatically
                                //due to referential integrity
                                oCommand.CommandText = "DELETE * FROM People";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            //clear out UserSegments, VariableSets, FavoritePeople, Deletions and Permissions, 
                            //since these aren't set for referential integrity
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM VariableSets";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM UserSegments";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM FavoritePeople";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                oCommand.CommandText = "DELETE * FROM Deletions";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }
                            try
                            {
                                //Remove user-related Keyset records
                                oCommand.CommandText = "DELETE * FROM KeySets WHERE OwnerID1 > 0";
                                iNumRowsAffected = oCommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                LMP.Error.Show(ex);
                            }

                            oLocalDBCnn.Close();
                        }
                        LMP.Data.Application.ApplyStructureUpdatesToDB(xPublicDB, FORTE_LOCAL_PUBLIC_DB_CREATION_SCRIPT);

                        //GLOG : 8883 : JSW
                        if (!Registry.Is64Bit())
                            LMP.Data.Application.CompactJetDB(xPublicDB);
                    }
                    MessageBox.Show("The local databases '" + xPublicDB + "' and '" + xPeopleDB + "' can now be deployed.",
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMain_NetworkDB_CreateCopy_Click(object sender, EventArgs e)
        {
            try
            {
                ////prompt for db parameters
                //using (DuplicateDBForm oForm = new DuplicateDBForm())
                //{
                //    DialogResult iChoice = oForm.ShowDialog();
                //    System.Windows.Forms.Application.DoEvents();

                //    if (iChoice == DialogResult.OK)
                //    {
                //        ////create connection data object to hold all source connection data
                //        ////create new db
                //        //NetworkDB.CreateNetworkDB(oForm.DestinationServer, oForm.DestinationDatabase,
                //        //    oForm.DestLoginID, oForm.DestPassword);

                //        //copy db from source to destination
                //        using (AdminSyncClient oSync = new AdminSyncClient())
                //        {
                //            oSync.CopyNetworkDB(oForm.SourceServer, oForm.SourceDatabase,
                //                oForm.DestinationServer, oForm.DestinationDatabase,
                //                oForm.DestLoginID, oForm.DestPassword);
                //        }
                //    }
                //}
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMain_NetworkDB_DownloadData_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();
                //prompt for network db
                using (SyncForm oDlg = new SyncForm())
                {
                    DialogResult iChoice = oDlg.ShowDialog(this);
                    System.Windows.Forms.Application.DoEvents();

                    if (iChoice == DialogResult.OK)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        bool bSuccess = false;

                        if (oDlg.SyncType == SyncForm.SyncTypes.Full)
                        {
                            //force a full sync by clearing out the
                            //appropriate metadata LastSync field
                            ClearLastSyncField("Down", oDlg.Server, oDlg.Database);
                        }

                        //execute admin sync with specified db
                        using (AdminSyncClient oSync = new AdminSyncClient(
                            oDlg.IISServerAddress, oDlg.IISServerApplicationName))
                        {
                            bSuccess = oSync.SyncDown(oDlg.Server, oDlg.Database);
                        }
                        if (bSuccess)
                        {
                            //force data to refresh by clearing out references
                            //to user controls in each category node tag
                            foreach (UltraTreeNode oNode in this.treeCategories.Nodes)
                                oNode.Tag = null;
                            MessageBox.Show("Admin data downloaded from " +
                                oDlg.Server + @"\" + oDlg.Database + ".",
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMain_LocalDB_Publish_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();

                //prompt for network db
                using (SyncForm oDlg = new SyncForm())
                {
                    DialogResult iChoice = oDlg.ShowDialog(this);
                    System.Windows.Forms.Application.DoEvents();

                    if (iChoice == DialogResult.OK)
                    {
                        string xSuccess = null;
                        //verify that db exists and that server is valid
                        if (NetworkDB.ConnectionInfoIsValid(oDlg.Server, oDlg.Database))
                        {
                            //execute admin sync with specified db
                            using (AdminSyncClient oSync = new AdminSyncClient(
                                oDlg.IISServerAddress, oDlg.IISServerApplicationName))
                            {
                                xSuccess = oSync.SyncUp(oDlg.Server, oDlg.Database);
                            }
                            if (xSuccess != null)
                            {
                                string xBakDB = LMP.Data.Application.WritableDBDirectory + "\\ForteAdmin" + xSuccess + ".mdb"; //JTS 3/28/13
                                File.Copy(LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.AdminDBName, xBakDB); //JTS 3/28/13

                                MessageBox.Show("Admin data published to " + oDlg.Server +
                                    @"\" + oDlg.Database + ".  Please note that your local admin db has been backed up to " + 
                                    xBakDB + ".", LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuFile_UpdateNormalTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateNormalTemplate();
            }
            catch (System.Exception oE)
            {
                LMP.Error.ShowInEnglish(oE);
            }
        }
        private void mnuUtilities_RemoveAuthorXML_Click(object sender, EventArgs e)
        {
            try
            {
                OleDbDataAdapter a = new OleDbDataAdapter(
                    "SELECT ID, XML FROM Segments", LMP.Data.LocalConnection.ConnectionObject);
                OleDbCommandBuilder b = new OleDbCommandBuilder(a);
                DataTable dt = new DataTable();
                a.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    string x = r["XML"].ToString();
                    string xPattern = "Authors=\"¬Authors&amp;gt;.*?¬/Authors&amp;gt;\" | Authors=\"&amp;lt;Authors&amp;gt;.*?&amp;lt;/Authors&amp;gt;\"";
                    string x2 = Regex.Replace(x, xPattern, "");
                    r["XML"] = x2;
                }
                a.Update(dt);
                MessageBox.Show("Author XML has been removed from all segments.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.Exit();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuUtilities_BuildCache_Click(object sender, EventArgs e)
        {
            try
            {
                LMP.MacPac.Administration.BuildCacheForm oForm = new LMP.MacPac.Administration.BuildCacheForm();

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    System.Windows.Forms.Application.DoEvents();

                    MessageBox.Show("Building a cache on a " + LMP.ComponentProperties.ProductName + " server can take several minutes.  You will be alerted when the cache has been built.",
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    System.Windows.Forms.Application.DoEvents();

                    //build cache asynchronously
                    AdminSyncClient oSync = new AdminSyncClient(oForm.ServerIP, oForm.ServerApplicationName);
                    BuildCacheAsyncHandler oHandler = new BuildCacheAsyncHandler(oSync.BuildCache);
                    AsyncCallback oCallback = new AsyncCallback(DisplayCachedBuiltMessage);
                    oHandler.BeginInvoke(oForm.ServerIP, oForm.ServerApplicationName, oCallback, oHandler);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuUtilities_ExecuteSQL_Click(object sender, EventArgs e)
        {
            try
            {
                ExecuteSQLForm oForm = new ExecuteSQLForm();

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    DialogResult iRet = MessageBox.Show(
                        "Are you sure you want to execute this sql statement?  If so, a backup of your database will be created in case of corruption or a loss of data integrity.",
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);

                    System.Windows.Forms.Application.DoEvents();

                    if (iRet == DialogResult.Yes)
                    {
                        //back up db
                        string xDB = LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13
                        string xBackupDB = xDB + "." + DateTime.Now.Ticks.ToString();
                        File.Copy(xDB, xBackupDB);

                        //execute sql
                        LMP.Data.SimpleDataCollection.ExecuteAction(oForm.SQL);

                        MessageBox.Show("The command has successfully executed and a backup database can be found at '" + xBackupDB + "'.",
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Question);

                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.ShowInEnglish(oE);
            }

        }
        private void mnuUtilities_PeopleUpdater_Click(object sender, EventArgs e)
        {
            try
            {
                string xSourceServer = null;
                string xSourceDatabase = null;
                string xTargetServer = null;
                string xTargetDatabase = null;
                bool bPublishAfterUpdate = false;
                bool bUpdateLicenses = false;
                string xIISServer = null;
                string xServerApplicationName = null;
                bool bError = false;

                //get pup options from db metadata
                string xOptions = LMP.Data.Application.GetMetadata("PUPOptions");

                if (xOptions != "")
                {
                    string[] aOptions = xOptions.Split('|');

                    xSourceServer = aOptions[0];
                    xSourceDatabase = aOptions[1];
                    xTargetServer = aOptions[2];
                    xTargetDatabase = aOptions[3];
                    bPublishAfterUpdate = Boolean.Parse(aOptions[4]);
                    bUpdateLicenses = Boolean.Parse(aOptions[5]);

                    try
                    {
                        xIISServer = aOptions[6];
                        xServerApplicationName = aOptions[7];
                    }
                    catch
                    {
                        //IIS server options not included -
                        //this may be the case with values stored
                        //in macpac versions that did not expose
                        //IIS server options -
                        //use reg value and "ForteSyncServer"
                        xIISServer = LMP.Registry.GetLocalMachineValue(ForteConstants.mpSyncRegKey, "ServerIP");
                        xServerApplicationName = "ForteSyncServer";
                    }
                }

                PUPForm oForm = new PUPForm();
                oForm.SourceServer = xSourceServer;
                oForm.SourceDatabase = xSourceDatabase;
                oForm.TargetServer = xTargetServer;
                oForm.TargetDatabase = xTargetDatabase;
                oForm.PublishAfterUpdate = bPublishAfterUpdate;
                oForm.UpdateLicenses = bUpdateLicenses;
                oForm.IISServer = xIISServer;
                oForm.ServerApplicationName = xServerApplicationName;

                DialogResult iRes = oForm.ShowDialog();
                System.Windows.Forms.Application.DoEvents();

                if (iRes == DialogResult.OK)
                {
                    //create options string
                    StringBuilder oSB = new StringBuilder();
                    oSB.AppendFormat("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}",
                        oForm.SourceServer, oForm.SourceDatabase, oForm.TargetServer,
                        oForm.TargetDatabase, oForm.PublishAfterUpdate, oForm.UpdateLicenses,
                        oForm.IISServer, oForm.ServerApplicationName);
                    LMP.Data.Application.SetMetadata("PUPOptions", oSB.ToString());

                    //execute PUP
                    LMP.Data.PUP.Execute(oForm.UpdateLicenses, oForm.SourceServer, oForm.SourceDatabase, out bError, true); //GLOG 8149

                    //GLOG 8149: Don't attempt publish if error occurred during queries
                    if (!bError && oForm.PublishAfterUpdate)
                    {
                        try
                        {
                            //perform people sync with network db- this
                            //will deploy the latest people detail
                            LMP.MacPac.Sync.AdminSyncClient oSync = new LMP.MacPac.Sync.AdminSyncClient(
                                oForm.IISServer, oForm.ServerApplicationName);

                            oSync.SyncPeopleUp(oForm.TargetServer, oForm.TargetDatabase);
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        }
                    }

                    //GLOG 8149
                    if (!bError)
                    {
                        //update people list if necessary
                        UltraTreeNode oNode = this.treeCategories.Nodes["  People"];
                        if (oNode.Tag is UserControl)
                        {
                            //people list has been shown - force refresh list
                            oNode.Tag = "PeopleManager";

                            if (this.treeCategories.SelectedNodes[0] == oNode)
                            {
                                //people manager is currently visible - reload
                                LoadControl(oNode);
                            }
                        }

                        //display completion message
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_PupComplete"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.ShowInEnglish(oE);
            }
        }
        private void DisplayCachedBuiltMessage(IAsyncResult oRes)
        {
            ((BuildCacheAsyncHandler)oRes.AsyncState).EndInvoke(oRes);
            MessageBox.Show("The cache was successfully built.", LMP.ComponentProperties.ProductName, 
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

         // GLOG : 3410 : JAB
        // Adding Compact functionality to the administrator.
        private void compactToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //GLOG 15818
            bool bSuccess = false;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                System.Windows.Forms.Application.DoEvents();

                // Logout from the db to provide CompactAdminDB access.
                LMP.Data.Application.Logout();

                LMP.Data.Application.CompactAdminDB();
                //GLOG 15818
                bSuccess = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                try
                {
                    Cursor.Current = Cursors.Default;

                    // Log back into the db.
                    LMP.Data.Application.Login(System.Environment.UserName, true);
                    //GLOG 15818: Only display if there was no error
                    if (bSuccess)
                    {
                        MessageBox.Show("The " + LMP.Data.Application.AdminDBName + " has been compacted.",
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
        }
       #endregion
        #region *********************private methods*********************
        private void LoadControl(UltraTreeNode oCategoryNode)
        {
            Control oCtl;
            //freeze the window
            LMP.WinAPI.LockWindowUpdate(this.Handle.ToInt32());
            //remove the previous control, if any
            this.scPanels.Panel2.Controls.Clear();
            if (oCategoryNode.Tag is UserControl)
                //the control has already been created - just show
                oCtl = (Control)oCategoryNode.Tag;
            else
            {
                //control has not yet been created - 
                //use reflection to create instance
                string xClassName = oCategoryNode.Tag.ToString();
                Assembly oA = Assembly.GetAssembly(
                    typeof(LMP.Administration.Controls.GroupsManager));
                oCtl = (Control)oA.CreateInstance("LMP.Administration.Controls." +
                    xClassName);

                //add control to node tag for future use
                oCategoryNode.Tag = oCtl;
            }

            //add control to collection
            this.scPanels.Panel2.Controls.Add(oCtl);

            //fill panel with control
            oCtl.Dock = DockStyle.Fill;

            //notify admin base class control has been loaded
            ((AdminManagerBase)oCtl).OnAfterControlLoaded(oCtl, new System.EventArgs());

            //select the loaded control
            oCtl.Focus();

            //unfreeze window
            LMP.WinAPI.LockWindowUpdate(0);

        }
        private void LoadCategories()
        {
            TreeNodesCollection oNodes = this.treeCategories.Nodes;
            UltraTreeNode oNode;
			
			//GLOG : 7998 : ceh
            if (LMP.MacPac.MacPacImplementation.IsToolkit)
            {
                oNode = oNodes.Add("  Address  Formats");
                oNode.Tag = "AddressFormatsManager";
                oNode = oNodes.Add("  Application  Settings");
                oNode.Tag = "AppSettingsManager";
                oNode = oNodes.Add("  Date Formats");
                oNode.Tag = "DateFormatsManager";
                oNode = oNodes.Add("  Interrogatories");
                oNode.Tag = "InterrogatoriesManager";
                oNode = oNodes.Add("  Lists");
                oNode.Tag = "ListsManager";
                oNode = oNodes.Add("  Offices");
                oNode.Tag = "OfficesManager";
                oNode = oNodes.Add("  Supported Languages");
                oNode.Tag = "SupportedLanguagesManager";                
            }
            else
            {
                oNode = oNodes.Add("  Address  Formats");
                oNode.Tag = "AddressFormatsManager";
                oNode = oNodes.Add("  Application  Settings");
                oNode.Tag = "AppSettingsManager";
                oNode = oNodes.Add("  Author Preferences");
                oNode.Tag = "AuthorPreferencesManager";
                oNode = oNodes.Add("  Contacts"); //GLOG : 8031 : ceh
                oNode.Tag = "CountryAliasManager"; //GLOG : 8031 : ceh
                oNode = oNodes.Add("  Counties/States/Countries");
                oNode.Tag = "LocationsManager";
                oNode = oNodes.Add("  Couriers");
                oNode.Tag = "CouriersManager";
                oNode = oNodes.Add("  Courts");
                oNode.Tag = "CourtsManager";
                oNode = oNodes.Add("  Custom People Fields");
                oNode.Tag = "CustomPeopleFieldsManager";
                oNode = oNodes.Add("  Date Formats");
                oNode.Tag = "DateFormatsManager";
                oNode = oNodes.Add("  External Connections");
                oNode.Tag = "ExternalConnectionsManager";
                oNode = oNodes.Add("  Groups");
                oNode.Tag = "GroupsManager";
                oNode = oNodes.Add("  Interrogatories");
                oNode.Tag = "InterrogatoriesManager";
                oNode = oNodes.Add("  Legacy Documents");
                oNode.Tag = "LegacyDocumentsManager";
                oNode = oNodes.Add("  Lists");
                oNode.Tag = "ListsManager";
                oNode = oNodes.Add("  Offices");
                oNode.Tag = "OfficesManager";
                oNode = oNodes.Add("  People");
                oNode.Tag = "PeopleManager";
                oNode = oNodes.Add("  Supported Languages");
                oNode.Tag = "SupportedLanguagesManager";
            }
        }
        /// <summary>
        /// copies the XML of the administrator's normal.dot to the
        /// Normal.dot record in the Segments table
        /// </summary>
        private void UpdateNormalTemplate()
        {
            try
            {
                //get normal.dot xml
                string xXML = LMP.Forte.MSWord.WordApp.GetNormalTemplateXML();
                //update xml in normal.dot record of Segments table
                //GLOG 6673:  Make sure Date in SQL string is US Format
                SimpleDataCollection.ExecuteAction("UPDATE Segments SET XML='" + xXML +
                    "', LastEditTime=#" + LMP.Data.Application.GetCurrentEditTime(true) + "# WHERE Name='****MP10NormalTemplate****';");
                MessageBox.Show("The firm Normal template has been refreshed. Publish your local database for changes to take effect.",
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordDocException(
                    LMP.Resources.GetLangString("Error_CouldNotUpdateNormalTemplate"), oE);
            }
        }
        /// <summary>
        /// clears the last sync up/down fields in the metadata table-
        /// clearing this row forces a full sync instead of an incremental sync
        /// </summary>
        /// <param name="xUpOrDown"></param>
        private void ClearLastSyncField(string xUpOrDown, string xServer, string xDatabase)
        {
            string xLastSyncField = "LastSync" + xUpOrDown + "|" + xServer + "|" + xDatabase;

            string xAdminDB = LMP.Data.Application.WritableDBDirectory + @"\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13

            //open connection to local admin db
            using (OleDbConnection oLocalDBCnn = new OleDbConnection(
                @"Provider=" + LocalConnection.ConnectionProvider + ";Data Source=" + xAdminDB +
                ";Jet OLEDB:System database=" + LMP.Data.Application.PublicDataDirectory + //JTS 3/28/13
                @"\Forte.mdw;User ID=mp10User;Password=Sackett@4437"))
            {
                oLocalDBCnn.Open();

                SimpleDataCollection.ExecuteAction(oLocalDBCnn,
                    "DELETE * FROM Metadata WHERE Name='" + xLastSyncField + "';");

                oLocalDBCnn.Close();
            }
        }
        private void SaveCurrentRecord()
        {
            if (this.scPanels.Panel2.Controls.Count > 0)
            {
                //ensure that current record is saved if necessary
                Control oCtl = this.scPanels.Panel2.Controls[0];
                ((LMP.Administration.Controls.AdminManagerBase)oCtl).SaveCurrentRecord();
            }
        }
        #endregion

        private void mnuMain_NetworkDB_GetSharedSegments_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();

                //prompt for network db
                using (SyncForm oDlg = new SyncForm())
                {
                    DialogResult iChoice = oDlg.ShowDialog(this);
                    System.Windows.Forms.Application.DoEvents();

                    if (iChoice == DialogResult.OK)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        bool bSuccess = false;

                        //execute admin sync with specified db
                        using (AdminSyncClient oSync = new AdminSyncClient(
                            oDlg.IISServerAddress, oDlg.IISServerApplicationName))
                        {
                            bSuccess = oSync.UpdateSharedSegments(oDlg.Server, oDlg.Database);
                        }
                        if (bSuccess)
                        {
                            MessageBox.Show("Shared segments have been downloaded " +
                                oDlg.Server + @"\" + oDlg.Database + ".",
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuUtilities_EditRibbonXML_Click(object sender, EventArgs e)
        {
            try
            {
                string xXML = SimpleDataCollection.GetScalar("SELECT XML FROM Segments WHERE Name='****RibbonXML****'").ToString();

                LMP.MacPac.Administration.EditRibbonXMLForm oForm = new LMP.MacPac.Administration.EditRibbonXMLForm(xXML, this);

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    SimpleDataCollection.ExecuteAction("UPDATE Segments SET XML='" + oForm.XML + "', LastEditTime = #" + DateTime.Now.ToUniversalTime().ToString() + "# WHERE Name='****RibbonXML****'");
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuMain_NetworkDB_Backup_Click(object sender, EventArgs e)
        {
            try
            {
                //LMP.MacPac.Administration.EditMinimumRequiredClientVersionForm oForm = new LMP.MacPac.Administration.EditMinimumRequiredClientVersionForm();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuMain_NetworkDB_ResetPublishingInProgressKey_Click(object sender, EventArgs e)
        {
            try
            {
                LMP.MacPac.Administration.ResetPublishingInProgressKeyForm oForm = new LMP.MacPac.Administration.ResetPublishingInProgressKeyForm();
                oForm.ShowDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG 8034: Launch Forte User Content Manager.exe
        private void mnuUtilities_UserContentManager_Click(object sender, EventArgs e)
        {
            try
            {
                bool bDelay = false;

                if (bUserContentManagerStartComplete)
                {
                    // Lock out reentry into this method.
                    bUserContentManagerStartComplete = false;

                    try
                    {
                        if (m_oUserContentProcess == null || (m_oUserContentProcess != null && m_oUserContentProcess.HasExited))
                        {
                            m_oUserContentProcess = System.Diagnostics.Process.Start(
                                new ProcessStartInfo(LMP.Data.Application.AppDirectory +
                                @"\" + FORTE_USER_CONTENT_MGR_EXE));

                            // GLOG : 2386 : JAB
                            // Delay granting access to the handler of the Administrator startup. This allows the
                            // Administrator to complete its startup.
                            bDelay = true;
                        }
                        else
                        {
                            if (m_oUserContentProcess != null && !m_oUserContentProcess.HasExited)
                            {
                                IntPtr hWnd = m_oUserContentProcess.MainWindowHandle;
                                // Normalize administrator app window just in case it was minimized.
                                Normalize(hWnd.ToInt32());

                                // Bring the administrator app into focus.
                                SetForegroundWindow(hWnd);
                            }
                        }

                    }
                    catch (System.Exception oE)
                    {
                        //Ignore error caused by answering No to Windows security prompt
                        if (oE.Message != "The operation was canceled by the user")
                            throw oE;
                    }

                    // Delay access to the handler of the Administrator startup by 3 seconds.
                    if (bDelay)
                    {
                        System.Threading.Thread.Sleep(3000);
                        bDelay = false;
                    }

                    // Grant access to the handler of the Administrator.
                    bUserContentManagerStartComplete = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Show the specified window.
        /// </summary>
        /// <param name="handle"></param>
        /// GLOG 8034
        private static void Normalize(Int32 handle)
        {
            if (!IsWindowMaximized(handle))
            {
                // If the window is minimized (or if it is in a Normal state)
                // make the window state Normal.
                ShowWindow(handle, SW_SHOWNORMAL);
            }
        }

        /// <summary>
        /// Determine if the specified window is maximized.
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        /// GLOG 8034
        private static bool IsWindowMaximized(Int32 handle)
        {
            return (IsZoomed(handle) != 0);
        }

        /// <summary>
        /// p/invoke to determine if the specified window minimized or maximized. 
        /// </summary>
        /// <param name="hwnd"></param>
        /// <returns></returns>
        /// GLOG 8034
        [DllImport("user32")]
        private static extern int IsZoomed(int hwnd);

        /// <summary>
        /// p/invoke to show the specified window.
        /// </summary>
        /// <param name="hwnd">windowhandle</param>
        /// <param name="nCmdShow">parameter to set a special state</param>
        /// <returns></returns>
        /// GLOG 8034
        [DllImport("user32")]
        private static extern int ShowWindow(int hwnd, int nCmdShow);

        /// <summary>
        /// p/invoke to bring the specified window into focus.
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        /// GLOG 8034
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        private void MainForm_Shown(object sender, EventArgs e)
        {
            //Allow manual sizing after displayed
            this.AutoSize = false;
        }


        private void mnuUtilities_UpdateQueries_Click(object sender, EventArgs e)
        {
            UpdatePeopleQueriesForm oForm = new UpdatePeopleQueriesForm();
            oForm.ShowDialog(this);
        }
    }
}
