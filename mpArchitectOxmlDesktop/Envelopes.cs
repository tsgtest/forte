using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;
using LMP.Data;
using MSWord = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Oxml.Word
{
    public class XmlEnvelopes : LMP.Architect.Oxml.XmlEnvelopes
    {
        public void InsertXML(string xXML, XmlSegment oParent, MSWord.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                XmlSegmentInsertion.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    iIntendedUse, mpObjectTypes.Envelopes); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }

        //public override bool PrepareForFinish()
        //{
        //    //get recipients value here, as variables will be deleted
        //    XmlVariable oRecips = this.Variables.ItemFromName("Recipients");
        //    //GLOG 7901: Get value from Detail control to ensure Details in Saved Data
        //    //that are not defined for this variable are not included in XML
        //    string xValue = "";
        //    if (oRecips.ControlType == mpControlTypes.DetailGrid || oRecips.ControlType == mpControlTypes.DetailList)
        //    {
        //        //Use only XML Details defined for this variable
        //        Controls.IControl oCtl;
        //        if (oRecips.AssociatedControl != null)
        //        {
        //            oCtl = oRecips.AssociatedControl;
        //        }
        //        else
        //            oCtl = oRecips.CreateAssociatedControl(null);

        //        oCtl.Value = oRecips.Value;
        //        xValue = oCtl.Value;
        //        oCtl = null;
        //    }
        //    else
        //    {
        //        xValue = oRecips.Value;
        //    }
        //    //GLOG 7879 : get USPS value
        //    bool bUSPS = false;

        //    try
        //    {
        //        bUSPS = (this.Variables.ItemFromName("USPSStandards").Value.ToLower() == "true");
        //    }
        //    catch { }

        //    //write recips to temp file - to be used in ExecutePostFinish
        //    File.WriteAllText(Path.GetTempPath() + "\\mpEnvelopesRecipsTmp.txt", bUSPS.ToString() + "|" + xValue); //GLOG 7901

        //    Word.Range oRecipsLoc = this.PrimaryRange;
        //    object oDir = Word.WdCollapseDirection.wdCollapseEnd;
        //    oRecipsLoc.Collapse(ref oDir);
        //    object oRng = oRecipsLoc;

        //    oRecipsLoc.Bookmarks.Add("RecipientsLocation", ref oRng);

        //    return base.PrepareForFinish();
        //}

        //public override bool ExecutePostFinish()
        //{
        //    //get bookmark of segment
        //    Word.Bookmark oSegmentBmk = this.PrimaryBookmark;

        //    //determine if the envelope is supposed to be in a new doc - 
        //    //if so we modify the call to merge below to improve performance
        //    int iStartRngIndex = oSegmentBmk.Range.Start;
        //    int iEndRngIndex = oSegmentBmk.Range.End;
        //    bool bInNewDoc = (iStartRngIndex == 0 && iEndRngIndex == this.ForteDocument.WordDocument.Content.End - 1);

        //    StringBuilder oSB = new StringBuilder();

        //    //convert xml to address -
        //    //replace xml end tags with char 11
        //    string xText = File.ReadAllText(Path.GetTempPath() + "\\mpEnvelopesRecipsTmp.txt");

        //    //GLOG 7879 : Parse USPSFormat and Recipients
        //    int iPos1 = xText.IndexOf("|");

        //    string xRecips = xText.Substring(iPos1 + 1);
        //    bool bUSPS = xText.Substring(0, iPos1).ToLower() == "true"; ;

        //    File.Delete(Path.GetTempPath() + "\\mpEnvelopesRecipsTmp.txt");
        //    string xRecipsMod = xRecips.Replace("\r\n", "\v");
        //    xRecipsMod = xRecipsMod.Replace("\r", "\v");

        //    //cycle through Index attributes, pulling out each address xml string
        //    int i = 0;

        //    iPos1 = 0;
        //    int iPos2 = 0;
        //    bool bFound = false;
        //    do
        //    {
        //        i++;
        //        iPos1 = 0;
        //        iPos2 = 0;
        //        bFound = false;

        //        do
        //        {
        //            iPos1 = xRecipsMod.IndexOf("Index='" + i + "'", iPos2);

        //            if (iPos1 == -1)
        //            {
        //                iPos1 = xRecipsMod.IndexOf("Index=\"" + i + "\"", iPos2);
        //            }

        //            if (iPos1 > -1)
        //            {
        //                iPos1 = xRecipsMod.IndexOf(">", iPos1) + 1;
        //                iPos2 = xRecipsMod.IndexOf("<", iPos1);
        //                oSB.AppendFormat("{0}\v", xRecipsMod.Substring(iPos1, iPos2 - iPos1));
        //                bFound = true;
        //            }
        //        } while (iPos1 > -1);

        //        //GLOG : 7614 : CEH
        //        if (oSB.Length != 0)
        //            oSB.Remove(oSB.Length - 1, 1);

        //        oSB.Append("\r\n");
        //    } while (bFound);


        //    xRecips = oSB.ToString();

        //    xRecips = LMP.String.RestoreXMLChars(xRecips);

        //    //trim trailing hard-return
        //    if (!string.IsNullOrEmpty(xRecips))
        //        xRecips = xRecips.Substring(0, xRecips.Length - 2);

        //    //GLOG : 7918 : ceh - Only delete bookmarks within the envelope mseg.
        //    //Alternatively, we could keep the 'if' block and add an argument to 
        //    //not include property tags in DeleteBookmarksAndDocVars.  This will
        //    //involve bookmarks starting with mpp & mpc prefixes, as well as their 
        //    //corresponding variables (mpo + id)

        //    //GLOG 7874:  Make sure no mSEG bookmarks remain
        //    //if (bInNewDoc)
        //    //{
        //    //    //JTS: If new document, clear out all variables and bookmarks
        //    //    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
        //    //    oConvert.DeleteBookmarksAndDocVars(this.ForteDocument.WordDocument);
        //    //}
        //    //else
        //    //{
        //        //JTS: If not new document, just delete bookmarks for Envelope
        //        foreach (Word.Bookmark oBmk in this.Bookmarks)
        //        {
        //            //GLOG 7916 (dm) - delete doc vars as well as booknmarks
        //            LMP.Forte.MSWord.WordDoc.DeleteAssociatedDocVars_Bmk(oBmk.Range, null, null, null);
        //            object oRef = oBmk;
        //            this.ForteDocument.WordDocument.Bookmarks[ref oRef].Delete();
        //        }
        //    //}

        //    //GLOG 7879 : apply USPS formatting if necessary
        //    if (bUSPS)
        //        xRecips = LMP.String.GetUSPSFormat(xRecips);

        //    //execute mail merge using source doc and modified recipients
        //    object oBmkName = "RecipientsLocation";

        //    LMP.Forte.MSWord.WordDoc.MergeEnvelopeRecipients(
        //        this.ForteDocument.WordDocument.Bookmarks.get_Item(
        //        ref oBmkName).Range, xRecips, bInNewDoc);

        //    return base.ExecutePostFinish();
        //}
    }
}
