﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using MSWord = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Oxml.Word
{

    public class XmlForteDocument : LMP.Architect.Oxml.XmlForteDocument
    {

        private MSWord.Document m_oWordDocument;
        public XmlForteDocument() { }
        public XmlForteDocument(MSWord.Document oWordDocument)
        {
            if (oWordDocument != null)
            {
                m_oWordDocument = oWordDocument;
                base.WPDocument = this.GetWPDocument();
            }
        }
        /// <summary>
        /// returns the Word document that is associated with this MacPac document
        /// </summary>
        public MSWord.Document WordDocument
        {
            get { return m_oWordDocument; }
        }
             
        /// <summary>
        /// Attach the current document to the specified template
        /// </summary>
        /// <param name="xNewTemplate"></param>
        /// <param name="bForceUpdate"></param>
        public void AttachToTemplate(string xNewTemplate, bool bForceUpdate)
        {
            if (xNewTemplate != "" && xNewTemplate != null)
            {
                //If no path specified, used MacPac Templates directory
                if (Path.GetDirectoryName(xNewTemplate) == string.Empty)
                {
                    //Use default templates location
                    xNewTemplate = Data.Application.TemplatesDirectory + @"\" + @xNewTemplate;
                }
                
                //GLOG 3088: Do nothing if template is already attached
                MSWord.Template oCurrentTemplate = (MSWord.Template)m_oWordDocument.get_AttachedTemplate();
                if (xNewTemplate.ToUpper() == oCurrentTemplate.FullName.ToUpper()
                    && !bForceUpdate)
                    return;

                if (System.IO.File.Exists(xNewTemplate))
                {
                    object oTemplate = (object)xNewTemplate;
                    try
                    {
                        m_oWordDocument.set_AttachedTemplate(ref oTemplate);
                    }
                    catch (System.Exception oE)
                    {
                        //attempt to find a variant that will work - .dot or .dotx
                        string xVariantTemplate = null;

                        if (xNewTemplate.EndsWith(".dotx"))
                            xVariantTemplate = xNewTemplate.Substring(0, xNewTemplate.Length - 1);
                        else if (xNewTemplate.EndsWith(".dot"))
                            xVariantTemplate = xNewTemplate + "x";
                        else
                            throw oE;

                        if (File.Exists(xVariantTemplate))
                        {
                            try
                            {
                                oTemplate = (object)xVariantTemplate;

                                //attempt to attach to the variant template
                                m_oWordDocument.set_AttachedTemplate(ref oTemplate);
                            }
                            catch
                            {
                                throw oE;
                            }
                        }
                        else
                            //variant doesn't exist - raise original error
                            throw oE;

                    }

                    //Update styles from template
                    m_oWordDocument.UpdateStyles();
                }
                else
                {
                    throw new LMP.Exceptions.FileException(
                        LMP.Resources.GetLangString(
                        "Error_FileDoesNotExist") + xNewTemplate);
                }
            }
        }
        
        private WordprocessingDocument GetWPDocument()
        {
            string xDocXml = m_oWordDocument.WordOpenXML;
            return GetWPDocument(xDocXml);
        }
 
    }
}
