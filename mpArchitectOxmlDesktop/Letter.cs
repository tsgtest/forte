using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using LMP.Data;
using LMP.Architect.Oxml.Word;
using MSWord = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Oxml.Word
{
    public class XmlLetter : LMP.Architect.Oxml.XmlLetter, IWordInsertableSegment
    {
        public void InsertXML(string xXML, XmlSegment oParent, MSWord.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                XmlSegmentInsertion.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                    iIntendedUse, mpObjectTypes.Letter); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }

    public class XmlLetterSignatures : LMP.Architect.Oxml.XmlLetterSignatures
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.LetterSignature; }
        }
    }

    public class XmlLetterSignature : LMP.Architect.Oxml.XmlLetterSignature
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.LetterSignatures; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.LetterSignature; }
        }
    }

    public class XmlLetterSignatureNonTable : LMP.Architect.Oxml.XmlLetterSignatureNonTable, IWordInsertableSegment
    {
        public void InsertXML(string xXML, XmlSegment oParent, MSWord.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                XmlSegmentInsertion.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, false,
                    iIntendedUse, mpObjectTypes.LetterSignatureNonTable); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }

    public class XmlLetterhead : LMP.Architect.Oxml.XmlLetterhead
    {
        // GLOG : 3135 : JAB
        // Provide the ability for the letterhead to update its content.
        public void RefreshContent()
        {
            XmlPrefill oPrefill = this.CreatePrefill();
            XmlSegment.Replace(this.ID, this.ID, this.Parent, this.ForteDocument, oPrefill, null, false);
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// When authors are updated, the letterhead is replaced with the default 
        /// letterhead. This method provides a mechanism by with editted content
        /// can restore their non default letterhead.
        /// </summary>
        /// <param name="xNewID"></param>
        public void RefreshContent(string xNewID)
        {
            XmlPrefill oPrefill = this.CreatePrefill();
            XmlSegment.Replace(xNewID, this.ID, this.Parent, this.ForteDocument, oPrefill, null, false);
        }
    }
}
