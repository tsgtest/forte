﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMP.Data;
using LMP.Architect.Oxml;
using LMP.Architect.Base;
using MSWord = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Oxml.Word
{
    public class XmlSegmentInsertion
    {
        public static XmlSegment oNewSegment
        {
            private set;
            get;
        }

        /// <summary>
        /// inserts the specified XML at the specified location -
        /// executes a generic targeted insertion, inserting 
        /// headers/footers/textframes independently
        /// </summary>
        /// <param name="xXML">xml to insert</param>
        /// <param name="oLocation">location to begin insertion</param>
        /// <param name="bInsertHeadersFooters">if true, will insert header/footer xml</param>
        internal static void InsertXML(string xXML, MSWord.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType,
            LMP.Forte.MSWord.mpSectionOnePageSetupOptions iDoSectionOnePageSetup, bool bTargeted,
            mpSegmentIntendedUses iIntendedUse, mpObjectTypes iObjectType) //GLOG 6983
        {
            DateTime t0 = DateTime.Now;
            //bool bConvertedXML = false;

            Trace.WriteNameValuePairs("iHeaderFooterInsertionType",
                iHeaderFooterInsertionType.ToString(),
                "iPageSetupExecutionType", iDoSectionOnePageSetup);

            //parse segment xml
            string xBodyXML;
            string xSectionsXML;
            string xPageSetupValues;
            string xCompatOptions;

            //disable Word event handlers
            //GLOG 7048 (dm) - no need to create ForteDocument now that IgnoreWordXMLEvents is static
            //ForteDocument oMPDoc = new ForteDocument(oLocation.Document);
            //GLOG 7404 (dm) - get current value before disabling
            XmlForteDocument.WordXMLEvents iEvents = XmlForteDocument.IgnoreWordXMLEvents;
            XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;

            //Make sure MP Schema is attached before inserting
            LMP.Forte.MSWord.WordDoc.AddMP10SchemaReference(oLocation.Document); //GLOG 7048 (dm)

            //GLOG 6983: No longer determined Object Type from XML
            short shObjectTypeID = (short)iObjectType;
            //get targeted xml from specified XML string
            LMP.Architect.Oxml.XmlSegment.
                ParseSegmentXML(xXML, out xBodyXML, out xSectionsXML, out xPageSetupValues, out xCompatOptions);

            if (bTargeted)
            {
                short shIntendedUse = (short)iIntendedUse;
                //insert xml using the 'targeted' technique
                LMP.Forte.MSWord.WordDoc.InsertTargetedSegmentXML(oLocation, xBodyXML, xSectionsXML,
                    xPageSetupValues, xCompatOptions, iHeaderFooterInsertionType, iDoSectionOnePageSetup,
                    shObjectTypeID, shIntendedUse);
            }
            else
            {
                //GLOG 6983: ObjectType now passed as a parameter
                LMP.Forte.MSWord.mpPleadingCollectionTypes iCollectionType;
                switch (iObjectType)
                {
                    case mpObjectTypes.PleadingCounsels:
                        iCollectionType = LMP.Forte.MSWord.mpPleadingCollectionTypes.PleadingCounsels;
                        break;
                    case mpObjectTypes.PleadingCaptions:
                        iCollectionType = LMP.Forte.MSWord.mpPleadingCollectionTypes.PleadingCaptions;
                        break;
                    case mpObjectTypes.PleadingSignatures:
                        iCollectionType = LMP.Forte.MSWord.mpPleadingCollectionTypes.PleadingSignatures;
                        break;
                    default:
                        iCollectionType = LMP.Forte.MSWord.mpPleadingCollectionTypes.None;
                        break;
                }

                //insert xml in one shot
                LMP.Forte.MSWord.WordDoc.InsertSegmentXML(oLocation, xXML, xPageSetupValues, xCompatOptions,
                    iDoSectionOnePageSetup, iCollectionType);
            }

            //            if (bConvertedXML)
            //            {
            //#if StripTags
            //                //Force removal of all tags to simulate InsertXML with CustomXML patch
            //                object oUrn = "urn-legalmacpac-data/10";

            //                Word.XMLSchemaReference oSchema = null;

            //                try
            //                {
            //                    oSchema = oLocation.Document.XMLSchemaReferences.get_Item(ref oUrn);
            //                }
            //                catch { }

            //                if (oSchema != null)
            //                    oSchema.Delete();
            //#endif
            //                LMP.Forte.MSWord.WordApp.EnsureAttachedSchema();

            //                //LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
            //                //oConvert.ConvertBookmarksToTags(oLocation.Document, true);
            //            }

            //re-enable Word event handlers
            //GLOG 7404 (dm) - restore starting value
            LMP.Architect.Oxml.Word.XmlForteDocument.IgnoreWordXMLEvents = iEvents;

            LMP.Benchmarks.Print(t0, bTargeted ? "Targeted" : "Not Targeted");
        }

        /// <summary>
        /// inserts the specified segment at the specified location of the specified document
        /// </summary>
        /// <param name="iDefinitionID">the id of the segment definition</param>
        /// <param name="oLocation">the location to insert the segment</param>
        /// <param name="iInsertionOptions">options available for segment insertion</param>
        /// <param name="oMPDocument">MacPac document representing the Word document into which the segment will be inserted.</param>
        /// <param name="oPrefill">the prefill used to prefill segment variables</param>
        /// <param name="bInsertXMLOnly"></param>
        /// <param name="bTargetXMLInsertion"></param>
        /// <param name="xXML">the XML inserted - if empty, the XML of the segment definition is used</param>
        /// <returns></returns>
        public static XmlSegment Create(string xID, XmlSegment oParent, LMP.Architect.Oxml.Word.XmlForteDocument oMPDocument, XmlPrefill oPrefill, string xBodyText)
        {
            return Create(xID, oParent, oMPDocument, oPrefill, xBodyText, null, null, XmlSegment.OxmlCreationMode.Default, false);
        }
        //GLOG 6021
        public static XmlSegment Create(string xID, XmlSegment oParent, LMP.Architect.Oxml.Word.XmlForteDocument oMPDocument,
            XmlPrefill oPrefill, string xBodyText, CollectionTableStructure oChildStructure, string xTemplate)
        {
            return Create(xID, oParent, oMPDocument, oPrefill, xBodyText, oChildStructure, xTemplate, XmlSegment.OxmlCreationMode.Default, false);
        }
        public static XmlSegment Create(string xID, XmlSegment oParent, LMP.Architect.Oxml.Word.XmlForteDocument oMPDocument,
            XmlPrefill oPrefill, string xBodyText, CollectionTableStructure oChildStructure, string xTemplate, XmlSegment.OxmlCreationMode iMode)
        {
            return Create(xID, oParent, oMPDocument, oPrefill, xBodyText, oChildStructure, xTemplate, iMode, false);
        }
        /// <summary>
        /// inserts the specified XmlSegment at the specified location of the specified document
        /// </summary>
        /// <param name="iDefinitionID">the id of the XmlSegment definition</param>
        /// <param name="oLocation">the location to insert the XmlSegment</param>
        /// <param name="iInsertionOptions">options available for XmlSegment insertion</param>
        /// <param name="oForteDocument">MacPac document representing the Word document into which the XmlSegment will be inserted.</param>
        /// <param name="oPrefill">the prefill used to prefill XmlSegment variables</param>
        /// <param name="bInsertXMLOnly"></param>
        /// <param name="bTargetXMLInsertion"></param>
        /// <param name="InsertingInHeaderFooter">Set to true by ContentManager when inserting in a Header/Footer</param>
        /// <param name="xXML">the XML inserted - if empty, the XML of the XmlSegment definition is used</param>
        /// <returns></returns>
        public static XmlSegment Create(string xID, XmlSegment oParent, XmlForteDocument oForteDocument, XmlPrefill oPrefill, string xBodyText,
            CollectionTableStructure oChildStructure, string xTemplate, XmlSegment.OxmlCreationMode iMode, bool bSuppressDesignErrors)
        {

            if (oForteDocument == null)
            {
                oForteDocument = new XmlForteDocument(null);
            }

            //GLOG 8462
            XmlSegment oSegment = LMP.Architect.Oxml.XmlSegment.CreateOxmlSegment(xID, oParent, oForteDocument, oPrefill, xBodyText, xTemplate, oChildStructure, iMode, LMP.Forte.MSWord.WordApp.Version, bSuppressDesignErrors);
            return oSegment;
        }
        public static void InsertAtRange(XmlSegment oSegment, MSWord.Range oRng)
        {
            DateTime t0 = DateTime.Now;

            string xXML = oSegment.FlatOxml;
            oRng.Collapse();
            string xBookmarkName = "zzmpInsertHere";
            oRng.Bookmarks.Add(xBookmarkName);
            object oBreak = MSWord.WdBreakType.wdSectionBreakNextPage;
            //GLOG 15923: Insert Section Break to retain Headers and Footers for insertion
            oRng.InsertBreak(ref oBreak);
            oRng.EndOf();
            oRng.InsertXML(xXML); // InsertFile(xFileName);
            object oBmkName = xBookmarkName;
            oRng = oRng.Document.Bookmarks[ref oBmkName].Range;
            oRng.Characters.First.Delete();
            try
            {
                oRng.Document.Bookmarks[ref oBmkName].Delete();
            }
            catch { }
            LMP.Benchmarks.ElapsedTime(t0);
        }
        /// <summary>
        /// creates the segment as a word document
        /// </summary>
        public static MSWord.Document OpenInWord(XmlSegment oSegment)
        {
            DateTime t0 = DateTime.Now;

            string xName = System.Guid.NewGuid().ToString() + ".docx";

            //add doc var for later retrieval of Oxml (used to build doc editor tree)
            oSegment.ForteDocument.WDSetDocVarValue(oSegment.WPDoc, "ForteTempFile", System.IO.Path.GetTempPath() + xName);
            
            string xFileName = oSegment.WriteToTempDirectory(xName);
            XmlSegmentInsertion.oNewSegment = oSegment;
            MSWord.Document oDoc = LMP.Forte.MSWord.WordApp.CreateDocument(xFileName, true, false);

            LMP.Benchmarks.Print(t0);
            //GLOG 8656: Clean up temp doc unless overridden in registry
            if (LMP.Registry.GetMacPac10Value("PreserveOxmlTempDocs") != "1")
            {
                try
                {
                    System.IO.File.Delete(xFileName);
                }
                catch { }
            }
            return oDoc;
        }
    }
}
