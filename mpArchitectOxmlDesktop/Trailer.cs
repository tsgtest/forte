﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMP.Data;
using MSWord = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Oxml.Word
{
    public class XmlTrailer : LMP.Architect.Oxml.XmlTrailer, IWordInsertableSegment
    {
        public void InsertXML(string xXML, XmlSegment oParent, MSWord.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                XmlSegmentInsertion.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    this.ForteDocument.Mode == XmlForteDocument.Modes.Design ? 
                        LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty
                        : LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, true,
                    iIntendedUse, mpObjectTypes.Trailer); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
}
