using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using LMP.Data;
using MSWord = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Oxml.Word
{
    public class XmlPleading : LMP.Architect.Oxml.XmlPleading, IWordInsertableSegment
    {
        public void InsertXML(string xXML, XmlSegment oParent, MSWord.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                XmlSegmentInsertion.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
                    iIntendedUse, mpObjectTypes.Pleading); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }

    //GLOG 7154
    public class XmlLitigationBack : LMP.Architect.Oxml.XmlLitigationBack
    {
    }
    
    public class XmlPleadingSignatures : XmlCollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingSignature; }
        }
    }

    public class XmlPleadingCounsels : XmlCollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingCounsel; }
        }
    }

    public class XmlPleadingCaptions : XmlCollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingCaption; }
        }
    }

    public class XmlPleadingSignature : XmlCollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.PleadingSignatures; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingSignature; }
        }
    }

    public class XmlPleadingCounsel : XmlCollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.PleadingCounsels; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingCounsel; }
        }
    }

    public class XmlPleadingCaption : XmlCollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.PleadingCaptions; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingCaption; }
        }

    }

    public class XmlPleadingPaper : XmlPaper
    {
        public enum mpPleadingPaperAlignmentScopes
        {
            Paragraph = 0,
            Selection = 1,
            ToSignature = 2,
            PleadingBody = 3
        }

    }
    public class XmlPleadingSignatureNonTable : LMP.Architect.Oxml.XmlPleadingSignatureNonTable
    {
    }

    public class XmlPleadingCoverPage : LMP.Architect.Oxml.XmlPleadingCoverPage
    {
    }
}
