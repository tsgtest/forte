﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;
using LMP.Data;
using MSWord = Microsoft.Office.Interop.Word;

namespace LMP.Architect.Oxml.Word
{
    public class XmlLabels: LMP.Architect.Oxml.XmlLabels
    {
        public void InsertXML(string xXML, XmlSegment oParent, MSWord.Range oLocation,
            LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
            LMP.Data.mpSegmentIntendedUses iIntendedUse)
        {
            try
            {
                XmlSegmentInsertion.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
                    LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
                    iIntendedUse, mpObjectTypes.Labels); //GLOG 6983
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }

    }
}
