﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMP.Architect.Oxml.Word
{
    public abstract class XmlAdminSegment: LMP.Architect.Oxml.XmlAdminSegment, IWordInsertableSegment
    {
        public virtual void InsertXML(string xXML, XmlSegment oParent, Microsoft.Office.Interop.Word.Range oLocation, fCOM.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted, Data.mpSegmentIntendedUses iIntendedUse)
        {
            throw new NotImplementedException();
        }
    }
}
