﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace LMP.Grail
{
    public class Client
    {
        public string Name{ get; set; }
        public string Number { get; set; }
        public string ParentID { get; set; }
        internal GrailBackend GrailStore { get; set; }

        internal Client(string xName, string xNumber, string xParentID, GrailBackend oGrailStore)
        {
            Name = xName;
            Number = xNumber;
            ParentID = xParentID;
            GrailStore = oGrailStore;
        }

        public Matters GetMatters(string xMatterFilter)
        {
            return this.GrailStore.GetMatters(this, xMatterFilter, SortField.Name);
        }
    }

    public class Matter
    {
        public string Name { get; internal set; }
        public string Number { get; internal set; }
        //public Client Client { get; internal set; }
        public string ClientID { get; internal set; }
        private GrailBackend GrailStore;

        internal Matter(string xName, string xNumber, string xClientID, GrailBackend oStore)
        {
            Name = xName;
            Number = xNumber;
            ClientID = xClientID;
            GrailStore = oStore;
        }

        public string GetData()
        {
            return this.GrailStore.GetData(this.Number);
        }
    }

    public class Clients : NameObjectCollectionBase
    {
        #region************************************fields************************************
        private StringDictionary m_oClientNumbersByName;
        private DataTable m_oDT;
        private GrailBackend m_oGrailStore;
        #endregion
        #region************************************constructors************************************
        public Clients()
        {
            m_oClientNumbersByName = new StringDictionary();
            m_oDT = new DataTable();
            m_oDT.Columns.Add("ClientName");
            m_oDT.Columns.Add("Number");
        }
        #endregion
        #region************************************properties************************************
        internal GrailBackend GrailStore
        {
            get { return m_oGrailStore; }
        }

        public Client this[string xNumber]
        {
            get { return this.ItemFromNumber(xNumber); }
        }

        public Client this[int iIndex]
        {
            get { return this.ItemFromIndex(iIndex); }
        }
        #endregion
        #region************************************methods************************************
        public Client Add(string xName, string xNumber, string xParentID, GrailBackend oGrailStore)
        {
            Client oClient = new Client(xName, xNumber, xParentID, oGrailStore);
            this.BaseAdd(xName, oClient);
            m_oClientNumbersByName.Add(xName, xNumber);
            m_oDT.Rows.Add(xName, xNumber);
            m_oGrailStore = oGrailStore;
            return oClient;
        }

        public Client ItemFromNumber(string xNumber)
        {
            return (Client)this.BaseGet(xNumber);
        }

        public Client ItemFromIndex(int iIndex)
        {
            return (Client)this.BaseGet(iIndex);
        }

        public Client ItemFromName(string xName)
        {
            string xNumber = m_oClientNumbersByName[xName];

            return ItemFromNumber(xNumber);
        }

        public void Remove(int iIndex)
        {
            this.BaseRemoveAt(iIndex);
        }

        public void Remove(string xNumber)
        {
            this.BaseRemove(xNumber);
        }

        public DataTable ToDataTable()
        {
            return m_oDT;
        }
        #endregion
    }

    public class Matters : NameObjectCollectionBase
    {
        #region************************************fields************************************
        private StringDictionary m_oMatterNumbersByName;
        private GrailBackend m_oGrailStore;
        private DataTable m_oDT;
        #endregion
        #region************************************constructors************************************
        public Matters(Client oClient)
        {
            m_oMatterNumbersByName = new StringDictionary();
            this.Client = oClient;
            m_oGrailStore = oClient.GrailStore;
            m_oDT = new DataTable();
            m_oDT.Columns.Add("MatterName");
            m_oDT.Columns.Add("Number");
            
        }

        public Matters(GrailBackend oStore)
        {
            m_oMatterNumbersByName = new StringDictionary();
            m_oDT = new DataTable();
            m_oDT.Columns.Add("MatterName");
            m_oDT.Columns.Add("Number");
        }
        #endregion
        #region************************************properties************************************
        public Matter this[string xNumber]
        {
            get { return this.ItemFromNumber(xNumber); }
        }
        public Matter this[int iIndex]
        {
            get { return this.ItemFromIndex(iIndex); }
        }
        public Client Client { get; private set; }
        #endregion
        #region************************************methods************************************
        public Matter Add(string xName, string xNumber, string xClientID)
        {
            Matter oMatter = new Matter(xName, xNumber, xClientID, m_oGrailStore);
            this.BaseAdd(xName, oMatter);
            m_oDT.Rows.Add(xName, xNumber);
            m_oMatterNumbersByName.Add(xName, xNumber);
            return oMatter;
        }

        public Matter ItemFromNumber(string xNumber)
        {
            return (Matter)this.BaseGet(xNumber);
        }

        public Matter ItemFromIndex(int iIndex)
        {
            return (Matter)this.BaseGet(iIndex);
        }

        public Matter ItemFromName(string xName)
        {
            string xNumber = m_oMatterNumbersByName[xName];
            return ItemFromNumber(xNumber);
        }

        public void Remove(int iIndex)
        {
            this.BaseRemoveAt(iIndex);
        }

        public void Remove(string xNumber)
        {
            this.BaseRemove(xNumber);
        }

        public DataTable ToDataTable()
        {
            return m_oDT;
        }
        #endregion
    }
}
