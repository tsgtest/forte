using System;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using LMP.Data;
using System.Collections;
using LMP.Controls;

namespace LMP.Architect.Base
{
	//TODO: create derivations of EventArgs for appropriate delegates below
	public delegate void AfterDefaultAuthorSetHandler(object sender, EventArgs e);
	public delegate void AfterDefaultValuesSetHandler(object sender, EventArgs e);
	public delegate void BeforeSegmentSetupHandler(object sender, EventArgs e);
	public delegate void SegmentGeneratedHandler(object sender, EventArgs e);
    public delegate void AfterAuthorUpdatedHandler(object sender, EventArgs e);
    public delegate void SegmentDeletedHandler(object sender, SegmentEventArgs oArgs);
    public delegate void SegmentAddedHandler(object sender, SegmentEventArgs oArgs);
    public delegate void ContactsAddedHandler(object sender, EventArgs e);
    public delegate void AfterAllVariablesVisitedHandler(object sender, EventArgs e);
    public delegate void CIDialogReleasedHandler(object sender, EventArgs e);
    public delegate void BeforeSegmentReplacedByActionHandler(object sender, SegmentEventArgs oArgs);
    public delegate void PaperTrayRequestedHandler(object sender, PaperTrayEventArgs e);
    public delegate void ActivationStateChangedHandler(object sender, ActivationStateChangedEventArgs e);
    public delegate void BeforeCollectionTableStructureInsertedHandler(object sender, SegmentEventArgs e);
    public delegate void AfterCollectionTableStructureInsertedHandler(object sender, SegmentEventArgs e);

    public class PaperTrayEventArgs : System.EventArgs
    {
        public int PaperTrayID;
        public PaperTrayEventArgs(int PaperTrayID)
        {
            PaperTrayID = this.PaperTrayID;
        }
    }
	public class SegmentValidationChangeEventArgs
	{
		bool SegmentValid;
        bool SegmentComplete;

		public SegmentValidationChangeEventArgs(bool bSegmentValid, bool bSegmentComplete)
		{
			this.SegmentValid = bSegmentValid;
            this.SegmentComplete = bSegmentComplete;
		}
	}
    public class ActivationStateChangedEventArgs{
        public bool Activated;

        public ActivationStateChangedEventArgs(bool bActivated){
            this.Activated = bActivated;
        }
    }

    /// <summary>
    /// contains properties that define the arguments for the
    /// SegmentDeleted and SegmentAdded events
    /// </summary>
    public class SegmentEventArgs
    {
        private Segment m_oSeg;

        public SegmentEventArgs(Segment oSeg)
        {
            m_oSeg = oSeg;
        }

        public Segment Segment
        {
            get { return m_oSeg; }
            set { m_oSeg = value; }
        }
    }

    public enum mpRelineFormatOptions
    {
        RelineFormatNone = 0,
        RelineBold = 1,
        RelineItalic = 2,
        RelineUnderline = 4,
        RelineUnderlineLast = 8,
        SpecialBold = 16,
        SpecialItalic = 32,
        SpecialUnderline = 64,
        SpecialUnderlineLast = 128,
        RemoveUnderlineLast = 256,
    }

    /// <summary>
	/// contains the members that define a MacPac segment
	/// </summary>
 
	abstract public class Segment
	{
        #region *********************constants*********************
        public static int UNSAVED_ID = 0;
        //GLOG 2543 (dm) - modified template to make DefaultDoubleClickLocation variable
        //GLOG 8376: Include MaxAuthors in template
        public const string ADMIN_SEG_OBJECT_DATA_TEMPLATE = "SegmentID={0}|ObjectTypeID={1}|Name={2}|DisplayName={3}|AuthorNodeUILabel=|MaxAuthors={9}|AuthorsHelpText=|HelpText={4}|IntendedUse={5}|ShowSegmentDialog={6}|DialogCaption={7}|MenuInsertionOptions=144|DefaultMenuInsertionBehavior=0|DefaultDragLocation=16|DefaultDragBehavior=0|DefaultDoubleClickLocation={8}|DefaultDoubleClickBehavior=0|L0=0|L1=0|L2=0|L3=0|L4=0|AuthorControlProperties=|ShowChooser=|LinkAuthorsToParentDef=|TranslationID=0|ShowCourtChooser=|CourtChooserUILabel=|CourtChooserControlProperties=|SupportedLanguages=|Culture=|";

        //user segments now have the same object data template as admin segments
        //public const string USER_SEG_OBJECT_DATA_TEMPLATE = "SegmentID={0}|ObjectTypeID={1}|Name={2}|DisplayName={3}|AuthorNodeUILabel=|MaxAuthors=|AuthorsHelpText=|HelpText={4}|IntendedUse={5}|ShowSegmentDialog={6}|DialogCaption={7}|MenuInsertionOptions=144|DefaultMenuInsertionBehavior=0|DefaultDragLocation=16|DefaultDragBehavior=0|DefaultDoubleClickLocation=16|DefaultDoubleClickBehavior=0|L0=0|L1=0|L2=0|L3=0|L4=0|AuthorControlProperties=|ShowChooser=|LinkAuthorsToParentDef=|TranslationID=0|ShowCourtChooser=|CourtChooserUILabel=|CourtChooserControlProperties=|";
        public const string USER_SEG_OBJECT_DATA_TEMPLATE = "SegmentID={0}|ObjectTypeID={1}|Name={2}|DisplayName={3}|MaxAuthors=0|HelpText={4}|IntendedUse={5}";
        //public const string NEW_SEGMENT_BMK_XML = "<aml:annotation aml:id=\"9999\" w:type=\"Word.Bookmark.Start\" w:name=\"mpNewSegment\" w:displacedBySDT=\"prev\"/><aml:annotation aml:id=\"9999\" w:type=\"Word.Bookmark.End\" w:displacedBySDT=\"prev\"/>";
        public const string NEW_SEGMENT_BMK_XML = "<aml:annotation aml:id=\"9999\" w:type=\"Word.Bookmark.Start\" w:name=\"mpNewSegment\"/><aml:annotation aml:id=\"9999\" w:type=\"Word.Bookmark.End\"/>";
        public const string NEW_SEGMENT_BMK_OPENXML = "<w:bookmarkStart w:id=\"9999\" w:name=\"mpNewSegment\" w:displacedByCustomXml=\"prev\"/><w:bookmarkEnd w:id=\"9999\" w:displacedByCustomXml=\"prev\"/>";
        #endregion
        #region *********************enumerations*********************
        public enum Events
        {
            AfterDefaultAuthorSet = 1,
            AfterDefaultValuesSet = 2,
            BeforeSegmentXMLInsert = 3,
            AfterSegmentXMLInsert = 4,
            AfterSegmentGenerated = 5,
            AfterSegmentValid = 6,
            AfterAuthorUpdated = 7,
            AfterLanguageUpdated = 8,
            BeforeSegmentFinish = 9 //GLOG 8512
        }

        [Flags]
        public enum InsertionLocations
        {
            InsertAtStartOfCurrentSection = 1,
            InsertAtEndOfCurrentSection = 2,
            InsertAtStartOfAllSections = 4,
            InsertAtEndOfAllSections = 8,
            InsertAtSelection = 16,
            InsertAtStartOfDocument = 32,
            InsertAtEndOfDocument = 64,
            InsertInNewDocument = 128,
            Default = 256,
            //GLOG 5723
            InsertBelowTOC = 512,
            InsertAboveTOA = 1024,
            //GLOG 8248
            InsertAtSelectionAsPlainText = 2048
        }

        [Flags]
        public enum InsertionBehaviors
        {
            Default = 0,
            InsertInSeparateNextPageSection = 1,
            KeepExistingHeadersFooters = 2,
            RestartPageNumbering = 4,
            InsertInSeparateContinuousSection = 8,
        }

        public enum Status : int
        {
            Unknown = 0,
            BoilerplateInserted = 1,
            DefaultAuthorsSet = 2,
            VariableDefaultValuesSet = 4,
            Finished = 8
        }
        public enum RefreshTypes
        {
            All = 0,
            NoChildSegments = 1,
            ChildSegmentsOnly = 2
        }

        public enum LinkAuthorsToParentOptions
        {
            Never = 0,
            FirstChildOfType = 1,
            Always = 2
        }

        #endregion
        #region *********************fields*********************
        private LinkAuthorsToParentOptions m_iLinkAuthorsToParentDef =
            LinkAuthorsToParentOptions.Never;
        private string m_xSupportedLanguages = "1033";
        int m_iNextObjectDatabaseID = 1;
        string m_xRecreateRedirectID = "";
        private string m_xAuthorsNodeUILabel = "Author"; //GLOG 6653
        private string m_xCourtChooserLabel = "Court";
        private InsertionLocations m_ilDefaultDragLocation = InsertionLocations.InsertAtSelection;
        private InsertionLocations m_ilDefaultDoubleClickLocation = InsertionLocations.InsertAtSelection;
        private bool m_bPromptForMissingAuthors = true;
        private int m_iMaxAuthors = 1; //GLOG 8053 (dm)
        private CollectionTableStructure m_oCollectionTableStructure = null;
        private string m_xInvalidPreferenceVariables = "";
        #endregion
        #region *********************constructors*********************
        protected Segment()
        {
            //GLOG 8228: Initialize property values
            this.FullTagID = "";
            this.LinkAuthorsToParent = false;
            this.TranslationID = 0;
            this.DefaultWrapperID = 0;
            this.IsTransparent = false;
            this.IsTransparentDef = false;
            this.DisplayWizard = false;
            this.WordTemplate = "";
            this.AllowContactUpdating = false;
            //GLOG 6653
            this.AuthorsNodeUILabel = LMP.Resources.GetLangString("Msg_Author").Replace(":", "");
            this.DefaultTrailerID = 0;
            this.RecreateRedirectID = "";
            this.TagPrefixID = "";
        }
        #endregion
        #region *********************properties*********************
        public int ID1 { get; set; }
        public int ID2 { get; set; }
        /// <summary>
        /// returns the string ID
        /// </summary>
        public string ID
        {
            get { return ID1.ToString() + "." + ID2.ToString(); }
        }
        public Prefill Prefill { get; set; }
        public bool OverrideStaticCreationCondition { get; set; }
        public string FullTagID { get; set; }
        public string DisplayName { get; set; }
        public int TranslationID { get; set; }
        public string AuthorControlProperties { get; set; }
        public bool PromptForMissingAuthors
        {
            get { return m_bPromptForMissingAuthors; }
            set { m_bPromptForMissingAuthors = value; }
        }
        public bool AllowContactUpdating { get; set; }
        /// <summary>
        /// returns the unqualified tag id of the segment
        /// </summary>
        public string TagID
        {
            get
            {
                int iPos = this.FullTagID.LastIndexOf('.');
                if (iPos > -1)
                {
                    return this.FullTagID.Substring(iPos + 1);
                }
                else
                    return this.FullTagID;
            }
        }
        /// <summary>
        /// returns true iff the segment requires an author
        /// </summary>
        public bool DefinitionRequiresAuthor
        {
            get
            {
                string xXML = this.Definition.XML.ToUpper();

                //GLOG item #4390 - dcf
                //return true for answer files that specify
                //MaxAuthors > 0
                return (xXML.Contains("[LEADAUTHOR") || xXML.Contains("[AUTHOR") ||
                    xXML.Contains("[ADDITIONALAUTHORS") || xXML.Contains("[PARENT::LEADAUTHOR") ||
                    xXML.Contains("[PARENT::AUTHOR") || xXML.Contains("[PARENT::ADDITIONALAUTHORS")) ||
                    (this.Definition.IntendedUse == mpSegmentIntendedUses.AsAnswerFile &&
                    !xXML.Contains("|MaxAuthors=0|"));
            }
        }
        /// <summary>
        /// gets the definition of the segment as an ISegmentDef
        /// </summary>
        public ISegmentDef Definition { get; set; }
        /// <summary>
        /// returns true iff the segment has a definition-
        /// segments can have definitions only when the segment
        /// is being used on the client site where it was created
        /// </summary>
        public bool HasDefinition
        {
            get { return this.Definition != null; }
        }
        /// <summary>
        /// returns true iff this segment's authors collection
        /// should be kept synched with its parent's authors collection
        /// </summary>
        public bool LinkAuthorsToParent { get; set; }

        public LinkAuthorsToParentOptions LinkAuthorsToParentDef
        {
            get { return m_iLinkAuthorsToParentDef; }
            set { m_iLinkAuthorsToParentDef = value; }
        }
        public string RecreateRedirectID
        {
            get { return m_xRecreateRedirectID; }
            set { m_xRecreateRedirectID = value; }
        }
        public int DefaultWrapperID { get; set; }
        public bool DisplayWizard { get; set; }
        /// <summary>
        /// determines whether segment will appear in the Document Editor
        /// when it's not top-level - this is the runtime value, which
        /// is based on a number of factors, including IsTransparentDef
        /// </summary>
        public bool IsTransparent { get; set; }

        /// <summary>
        /// determines whether segment will appear in the Document Editor
        /// when it's not top-level - this is the definition value
        /// </summary>
        public bool IsTransparentDef { get; set; }
        public string WordTemplate { get; set; }
        public string RequiredStyles { get; set; }
        public mpSegmentIntendedUses IntendedUse { get; set; }
        /// <summary>
        /// gets/sets the name of the segment
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// gets/sets L0
        /// </summary>
        public int L0 { get; set; }
        /// <summary>
        /// gets/sets L0
        /// </summary>
        public int L1 { get; set; }
        /// <summary>
        /// gets/sets L0
        /// </summary>
        public int L2 { get; set; }
        /// <summary>
        /// gets/sets L0
        /// </summary>
        public int L3 { get; set; }
        /// <summary>
        /// gets/sets L0
        /// </summary>
        public int L4 { get; set; }
        /// <summary>
        /// gets/sets insertion options
        /// </summary>
        public InsertionLocations MenuInsertionOptions { get; set; }

        /// <summary>
        /// gets/sets insertion behaviors
        /// </summary>
        public InsertionBehaviors DefaultMenuInsertionBehavior { get; set; }

        /// <summary>
        /// gets/sets default drag location -- defaults to 16
        /// </summary>
        public InsertionLocations DefaultDragLocation
        {
            get { return m_ilDefaultDragLocation; }
            set { m_ilDefaultDragLocation = value; }
        }

        /// <summary>
        /// gets/sets drag insertion locations
        /// </summary>
        public InsertionBehaviors DefaultDragBehavior { get; set; }

        /// <summary>
        /// gets/sets double click locations - defaults to 16
        /// </summary>
        public InsertionLocations DefaultDoubleClickLocation
        {
            get { return m_ilDefaultDoubleClickLocation; }
            set { m_ilDefaultDoubleClickLocation = value; }
        }

        /// <summary>
        /// gets/sets double click behaviors
        /// </summary>
        public InsertionBehaviors DefaultDoubleClickBehavior { get; set; }

        /// <summary>
        /// gets/sets the text for the author node in the UI
        /// </summary>
        public string AuthorsNodeUILabel //GLOG 6653
        {
            //GLOG 8025 - changed to manual get/set for default value
            get { return m_xAuthorsNodeUILabel; }
            set { m_xAuthorsNodeUILabel = value; }
        }
        public string AuthorsHelpText { get; set; }
        /// <summary>
        /// gets/sets Protected Form password
        /// </summary>
        public string ProtectedFormPassword { get; set; }
        /// gets/sets help text
        /// </summary>
        public string HelpText { get; set; }
        /// <summary>
        /// Gets/sets whether Jurisdiction control is displayed
        /// </summary>
        public bool ShowCourtChooser { get; set; }
        /// <summary>
        /// Gets/sets label for Jurisdiction control
        /// </summary>
        public string CourtChooserUILabel
        {
            get { return m_xCourtChooserLabel; }
            set { m_xCourtChooserLabel = value; }
        }
        /// <summary>
        /// gets/sets help text for Jurisdiction control
        /// </summary>
        public string CourtChooserHelpText { get; set; }
        /// <summary>
        /// gets/sets configuration string for Jurisdiction control
        /// </summary>
        public string CourtChooserControlProperties { get; set; }
        public bool ShowSegmentDialog { get; set; }
        public string DialogTabCaptions { get; set; }
        public string DialogCaption { get; set; }
        public int DefaultTrailerID { get; set; }

        public string SupportedLanguages
        {
            get { return m_xSupportedLanguages; }
            set { m_xSupportedLanguages = value; }
        }

        public bool SupportsMultipleLanguages
        {
            get
            {
                string[] aLanguages = SupportedLanguages.Split(
                    LMP.StringArray.mpEndOfValue);
                return (aLanguages.Length > 1);
            }
        }

        /// <summary>
        /// returns the id of the client to whom this segment belongs
        /// </summary>
        public string TagPrefixID { get; set; }

        public bool BelongsToCurrentClient
        {
            get
            {
                //1-3-11 (dm) - modified to always accept tag with internal prefix
                string xCurClient = LMP.Data.Application.CurrentClientTagPrefixID;
                return ((this.TagPrefixID == xCurClient) || (this.TagPrefixID == "") ||
                    (this.TagPrefixID == LMP.Data.ForteConstants.mpInternalTagPrefixID) ||
                    (xCurClient == LMP.Data.ForteConstants.mpInternalTagPrefixID));
            }
        }
        /// <summary>
        /// returns the creation status of the segment
        /// </summary>
        public Status CreationStatus { get; set; }
        /// <summary>
        /// sets/gets whether or not the segment
        /// should show a chooser in the UI
        /// </summary>
        public bool ShowChooser { get; set; }

        /// <summary>
		/// sets/gets the maximum number of authors allowed for this segment
        /// GLOG 8053 (dm) - reversed auto-implementation to
        /// restore default value for this property
        /// </summary>
        public int MaxAuthors
        {
            get { return m_iMaxAuthors; }
            set {m_iMaxAuthors = value;}
        }
        /// <summary>
        /// gets/sets the object type ID of the segment
        /// </summary>
        public mpObjectTypes TypeID { get; set; }
        public ArrayList GetContainingSegmentIDs()
        {
            return Data.Application.GetContainingSegmentIDs(this.ID);
        }

        //GLOG 3689: This list is stored in Segment object, because variables 
        //collection may be refreshed and recreated during insertion
        public string InvalidPreferenceVariables
        {
            get { return m_xInvalidPreferenceVariables; }
            set { m_xInvalidPreferenceVariables = value; }
        }

        /// <summary>
        /// gets/sets a unique id for variables/blocks - added in 10.2 -
        /// used to restore variable/block definition in the event
        /// that doc vars are stripped
        /// </summary>
        public int NextObjectDatabaseID
        {
            get { return m_iNextObjectDatabaseID; }
            set { m_iNextObjectDatabaseID = Math.Max(m_iNextObjectDatabaseID, value); }
        }
        public CollectionTableStructure ChildCollectionTableStructure
        {
            get
            {
                if (m_oCollectionTableStructure == null)
                {
                    m_oCollectionTableStructure = new CollectionTableStructure();
                    this.GetChildCollectionTableStructure(ref m_oCollectionTableStructure);
                }
                return m_oCollectionTableStructure;
            }
        }
        protected virtual void GetChildCollectionTableStructure(ref CollectionTableStructure oStructure)
        {
        }
        #endregion
        #region *********************methods*********************
        public virtual bool PrepareForFinish()
        {
            return true;
        }

        #endregion
        public virtual bool ExecutePostFinish()
        {
            return true;
        }
    }
    
    /// <summary>
    /// defines a collection of Segments -
    /// contains methods to manage the collection-
    /// created by Dan Fisherman - 11/01/05
    /// </summary>
    public class Segments : NameObjectCollectionBase, IEnumerable
    {
    }

    /// <summary>
    /// contains the information of the 
    /// collection tables of a segment
    /// </summary>
    public class CollectionTableStructure
    {
        [Flags]
        public enum ItemInsertionLocations
        {
            Unspecified = 0,
            EndOfTable = 1,
            CurrentRow = 2,
            LeftCell = 4,
            RightCell = 8
        }
        private OrderedDictionary m_oDictionary;

        public CollectionTableStructure()
        {
            m_oDictionary = new OrderedDictionary();
        }

        public Prefill this[string xCollectionItemID]
        {
            get { return (Prefill)m_oDictionary[xCollectionItemID]; }
        }

        public Prefill this[int iIndex]
        {
            get { return (Prefill)m_oDictionary[iIndex]; }
        }
        
        public int Count
        {
            get { return m_oDictionary.Count; }
        }
        public OrderedDictionary Dictionary
        {
            get { return m_oDictionary; }
        }
        /// <summary>
        /// Tests for existence of child of specified type in the collection
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        public bool ContainsChildOfType(mpObjectTypes iType)
        {
            //GLOG 8373
            foreach (string xKey in m_oDictionary.Keys)
            {
                string[] xProps = xKey.Split('|');
                //GLOG 8431: ID is now 2nd item
                string xChildID = xProps[1];
                //GLOG 6966: Make sure string can be parsed as a Double even if
                //the Decimal separator is something other than '.' in Regional Settings
                if (xChildID.EndsWith(".0"))
                    xChildID = xChildID.Replace(".0", "");
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                //GLOG 8373: Avoid error if child segment has been deleted
                AdminSegmentDef oDef = null;

                try
                {
                    oDef = (AdminSegmentDef)oDefs.ItemFromID((int)double.Parse(xChildID));
                }
                catch
                {
                    continue;
                }
                if (oDef != null && oDef.TypeID == iType)
                    //Collection contains child of this type
                    return true;
            }
            return false;

        }

        /// <summary>
        /// Get list of child segments matching object type
        /// </summary>
        /// <param name="oSegs"></param>
        /// <param name="oType"></param>
        /// <returns></returns>
        public List<string> GetSegmentsOfSameType(Segment[] oSegs, mpObjectTypes oType)
        {
            //GLOG 7848
            List<string> aVals = new List<string>();
            for (int i = 0; i < oSegs.GetLength(0); i++)
            {
                if (oSegs[i].TypeID == oType)
                {
                    string xID = oSegs[i].ID;
                    if (xID.EndsWith(".0"))
                        xID = xID.Replace(".0", "");
                    aVals.Add(xID);
                }
            }
            return aVals;
        }
        /// <summary>
        /// Get list of Child Structure items matching object type
        /// </summary>
        /// <param name="oType"></param>
        /// <returns></returns>
        public List<string> GetStructureItemsOfSameType(mpObjectTypes oType)
        {
            //GLOG 7848
            List<string> aVals = new List<string>();

            //Check dictionary items - TypeID is 2nd part of Key
            foreach (string xKey in m_oDictionary.Keys)
            {
                string[] aParts = xKey.Split('|');
                if (aParts.GetLength(0) > 2 && aParts[2] == ((int)oType).ToString())
                {
                    string xID = aParts[1];
                    if (xID.EndsWith(".0"))
                        xID = xID.Replace(".0", "");
                    aVals.Add(xID);
                }
            }
            return aVals;
        }
    }
}
