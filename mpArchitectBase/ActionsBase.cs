using System;
using System.Collections;
using System.Windows.Forms;
using System.Text;

namespace LMP.Architect.Base
{
	public delegate void ActionsDirtiedEventHandler(object sender, System.EventArgs e);
	public delegate void ActionDirtiedEventHandler(object sender, System.EventArgs e);

	/// <summary>
	/// defines the abstract ActionBase class, from which
	/// VariableAction, ControlAction, and SegmentAction are derived
	/// </summary>
	public abstract class ActionBase
	{
		public event ActionDirtiedEventHandler ActionDirtied;
        #region *********************enumerations*********************
        public enum mpMethodTypes
        {
            NET = 1,
            COM = 2
        }
        #endregion
        #region *********************fields*********************
        protected string m_xID = "";
        private int m_iExecutionIndex = 0;
        private string m_xExecutionCondition = "";
        private string m_xParameters = "";
        private bool m_bIsDirty = false;
        #endregion
        #region *********************constructors*********************
        internal protected ActionBase() { }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// VariableActions have non-persistent IDs - they are used
        /// for reference at run-time, but are not otherwise necessary
        /// </summary>
        public string ID
        {
            get { return m_xID; }
        }
        public int ExecutionIndex
        {
            get { return m_iExecutionIndex; }
            set
            {
                if (m_iExecutionIndex != value)
                {
                    m_iExecutionIndex = value;
                    this.IsDirty = true;
                }
            }
        }
        public string ExecutionCondition
        {
            get { return m_xExecutionCondition; }
            set
            {
                if (m_xExecutionCondition != value)
                {
                    m_xExecutionCondition = value;
                    this.IsDirty = true;
                }
            }
        }
        public string Parameters
        {
            get { return m_xParameters; }
            set
            {
                if (m_xParameters != value)
                {
                    m_xParameters = value;
                    this.IsDirty = true;
                }
            }
        }
        public bool IsDirty
        {
            get { return m_bIsDirty; }
            set
            {
                //notify if value is changing from false to true-
                //ie if going from clean to dirty - skip if there
                //are no subscribers
                if (!m_bIsDirty && value == true)
                {
                    m_bIsDirty = true;
                    if (this.ActionDirtied != null)
                    {
                        this.ActionDirtied(this, new EventArgs());

                        //reset flag after firing event
                        m_bIsDirty = false;
                    }
                }
                else
                    m_bIsDirty = value;
            }
        }
        public virtual bool IsValid { get { return true; } }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// returns the action as an array
        /// </summary>
        /// <returns></returns>
        public virtual string[] ToArray(bool bIncludeID)
        {
            return null;
        }

        /// <summary>
        /// returns the action as an array
        /// </summary>
        /// <returns></returns>
        public string[] ToArray()
        {
            return this.ToArray(false);
        }

        /// <summary>
        /// sets the ID of the action to a new ID
        /// </summary>
        public void SetID()
        {
            if (m_xID == "")
                m_xID = System.Guid.NewGuid().ToString();
        }

        /// <summary>
        /// sets the ID of the action to the specified ID
        /// </summary>
        /// <param name="ID"></param>
        public void SetID(string ID)
        {
            m_xID = ID;
        }

        /// <summary>
        /// returns the action as a delimited string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Join("�", (string[])this.ToArray());
        }

        /// <summary>
        /// executes the action
        /// </summary>
        public virtual void Execute() { }

        /// <summary>
        /// returns true iff execution condition is met or if there is no execution condition
        /// </summary>
        /// <returns></returns>
        public virtual bool ExecutionIsSpecified() { return true; }

        /// <summary>
        /// executes the specified VBA command string on the specified bookmark
        /// </summary>
        /// <param name="xParameters">bookmark, command string</param>
        protected virtual void ExecuteOnBookmark(string xParameters){}

        /// <summary>
        /// executes the specified VBA command string 
        /// on the associated Word tags of the specified Block
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xParameters"></param>
        protected virtual void ExecuteOnBlock(string xParameters){}

        /// <summary>
        /// GLOG 7229: executes the specified VBA command string 
        /// on the primary range of the specified Segment(s)
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xParameters"></param>
        protected virtual void ExecuteOnSegment(string xParameters) { }

        /// <summary>
        /// executes the specified VBA command string on the document
        /// </summary>
        /// <param name="xParameters">command string</param>
        protected virtual void ExecuteOnDocument(string xParameters){}

        /// <summary>
        /// executes the specified VBA command string on the Word application
        /// </summary>
        /// <param name="xParameters">command string</param>
        protected virtual void ExecuteOnApplication(string xParameters){}

        /// <summary>
        /// replaces all instances of the specified
        /// segment with the specified segment
        /// </summary>
        /// <param name="xParameters"></param>
        protected virtual void ReplaceSegment(string xParameters){}

        /// <summary>
        /// runs the specified macro
        /// </summary>
        /// <param name="xParameters">xMacroName, xArgs</param>
        protected virtual void RunMacro(string xParameters){}

        /// <summary>
        /// runs the specified method
        /// </summary>
        /// <param name="xParameters"></param>
        protected virtual void RunMethod(string xParameters){}

        /// <summary>
        /// sets Word document variable
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">variable name, expression</param>
        protected virtual void SetDocVarValue(string xValue, string xParameters){}

        /// <summary>
        /// sets Word custom document property
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">variable name, expression</param>
        protected virtual void SetDocPropValue(string xValue, string xParameters){}

        /// <summary>
        /// sets preference or user application setting for user or lead author
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">key name, key type, expression</param>
        protected virtual void SetAsDefaultValue(string xValue, string xParameters){}

        #endregion
    }

	/// <summary>
	/// defines the abstract ActionsCollection class, from which
	/// VariableActions, ControlActions, and SegmentActions are derived
	/// </summary>
	public abstract class ActionsCollectionBase
	{
        #region *********************fields*********************
        private ArrayList m_oActions = new ArrayList();
        #endregion
        #region *********************events*********************
        public event LMP.Architect.Base.ActionsDirtiedEventHandler ActionsDirtied;
        #endregion
        #region *********************constructors*********************
        public ActionsCollectionBase()
        {
        }
        #endregion
        #region *********************properties*********************
        public ActionBase this[int iIndex]
        {
            get { return this.ItemFromIndex(iIndex); }
        }
        public int Count
        {
            get
            {
                return m_oActions.Count;
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// returns a new ActionBase
        /// </summary>
        /// <returns></returns>
        protected ActionBase Create()
        {
            try
            {
                ////save current edits to collection
                //this.UpdateCollectionWithCurrentAction();

                //request the new object of the appropriate type -
                //GetNewActionInstance is abstract, and hence
                //implemented in each derived class
                ActionBase oAction = this.GetNewActionInstance();
                oAction.ActionDirtied += new LMP.Architect.Base.ActionDirtiedEventHandler(OnActionDirtied);

                //flag new action so it can be added to internal collection
                oAction.IsDirty = true;
                return oAction;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                    "Error_ActionCreationFailed"), oE);
            }
        }

        /// <summary>
        /// returns the variable ActionBase with the specified index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public ActionBase ItemFromIndex(int iIndex)
        {
            Trace.WriteNameValuePairs("iIndex", iIndex);

            ////save current edits to the collection
            //this.UpdateCollectionWithCurrentAction();

            //check for valid supplied index
            if (iIndex < 0 || iIndex > m_oActions.Count - 1)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") +
                    iIndex.ToString());
            }

            //get the array that defines data for the ActionBase
            string[] aAction = (string[])m_oActions[iIndex];

            //get an object from the array - set as current object
            ActionBase oAction = GetActionFromArray(aAction);

            //subscribe to notifications that this action has been dirtied (edited)
            oAction.ActionDirtied += new LMP.Architect.Base.ActionDirtiedEventHandler(OnActionDirtied);
            return oAction;
        }

        /// <summary>
        /// deletes the specified variable ActionBase
        /// </summary>
        /// <param name="iIndex"></param>
        public virtual void Delete(int iIndex)
        {
            try
            {
                Trace.WriteNameValuePairs("iIndex", iIndex);

                ////save current edits to collection
                //this.UpdateCollectionWithCurrentAction();

                //test for valid index
                if (iIndex < 0 || iIndex > m_oActions.Count - 1)
                {
                    throw new LMP.Exceptions.NotInCollectionException(
                        LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") +
                        iIndex.ToString());
                }

                //remove from array list
                m_oActions.RemoveAt(iIndex);

                //adjust all execution indices from deletion index to last index
                for (int i = iIndex; i < m_oActions.Count; i++)
                {
                    //reassign execution index
                    ((string[])m_oActions[i])[1] = (i + 1).ToString();
                }

                //alert subscribers that collection has changed
                if (this.ActionsDirtied != null)
                    this.ActionsDirtied(this, new EventArgs());
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                    "Error_ActionDeletionFailed") + iIndex.ToString(), oE);
            }
        }

        /// <summary>
        /// sets the internal storage for this collection
        /// from a delimited string - should be called only 
        /// by the Variables class -
        ///this is needed because variable actions are stored
        ///as an index in the variable array - it's faster than
        ///having to retrieve and parse the data a second time from 
        ///the object data attribute
        /// </summary>
        /// <param name="oActions"></param> 
        public void SetFromString(string xActions)
        {
            try
            {
                Trace.WriteNameValuePairs("xActions", xActions);

                //clear internal storage
                m_oActions.Clear();

                if (xActions != "")
                {
                    //convert actions string into array of ActionBase def strings
                    string[] aActionStrings = xActions.Split(LMP.StringArray.mpEndOfRecord);
                    //cycle through ActionBase def strings, converting each to an array
                    //and adding to the sorted list
                    for (int i = 0; i <= aActionStrings.GetUpperBound(0); i++)
                    {
                        //get ActionBase def string - add an ID to beginning -
                        //this will be the ID of the ActionBase - we need IDs only
                        //at run time (it's better not to take up space in document)
                        string xAction = System.Guid.NewGuid().ToString() +
                            LMP.StringArray.mpEndOfSubValue + aActionStrings[i];

                        //split into array
                        string[] aAction = xAction.Split(LMP.StringArray.mpEndOfSubValue);

                        if (aAction.GetUpperBound(0) != 5)
                            //bad ActionBase string
                            throw new LMP.Exceptions.DataException(
                                LMP.Resources.GetLangString(
                                "Error_InvalidVariableActionDefinition" + xAction));

                        //add to array list at position specified by ExecutionIndex (array index 1)
                        m_oActions.Insert(Convert.ToInt32(aAction[1]) - 1, aAction);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotPopulateActionsCollection"), oE);
            }
        }

        /// <summary>
        /// returns a VariableAction populated with data supplied by specified array
        /// </summary>
        /// <param name="aNewValues"></param>
        /// <returns></returns>
        protected virtual ActionBase GetActionFromArray(string[] aNewValues) { return null; }
        /// <summary>
        /// derived class will implement to return a new instance of the individual
        /// </summary>
        /// <returns></returns>
        protected virtual ActionBase GetNewActionInstance() { return null; }
        /// <summary>
        /// executes the appropriate actions
        /// </summary>
        public virtual void Execute()
        {
            //cycle through segment actions, executing actions in the collection
            for (int i = 0; i < this.Count; i++)
            {
                ActionBase oAction = (ActionBase)this[i];

                try
                {
                    oAction.Execute();
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.ActionException(
                        LMP.Resources.GetLangString("Error_CouldNotExecuteAction") +
                        oAction.ExecutionIndex.ToString(), oE);
                }
            }
        }

        /// <summary>
        /// Moves up the ExecutionIndex of the action
        /// </summary>
        public void MoveUp(ActionBase oAction)
        {
            try
            {
                //move up if necessary
                if (oAction.ExecutionIndex > 1)
                {
                    //get array of values
                    string[] aNewValues = oAction.ToArray(true);

                    //the execution index has changed-
                    //remove from current array position
                    m_oActions.RemoveAt(oAction.ExecutionIndex - 1);

                    //re-insert at next index up
                    m_oActions.Insert(oAction.ExecutionIndex - 2, aNewValues);

                    //increment execution index of supplied var
                    oAction.ExecutionIndex = oAction.ExecutionIndex - 1;

                    ////we need to update execution indexes
                    for (int iCounter = 0; iCounter < m_oActions.Count; iCounter++)
                        //reassign execution index
                        ((string[])m_oActions[iCounter])[1] = (iCounter + 1).ToString();


                    //alert subscribers that collection has changed
                    if (this.ActionsDirtied != null)
                        this.ActionsDirtied(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotMoveUpAction") +
                    oAction.ExecutionIndex.ToString(), oE);
            }

        }

        /// <summary>
        /// Moves down the ExecutionIndex of the action
        /// </summary>
        public void MoveDown(ActionBase oAction)
        {
            try
            {
                //move down if necessary
                if (oAction.ExecutionIndex < m_oActions.Count)
                {
                    //get array of values
                    string[] aNewValues = oAction.ToArray(true);

                    //the execution index has changed-
                    //remove from current array position
                    m_oActions.RemoveAt(oAction.ExecutionIndex - 1);

                    //re-insert at next index down
                    m_oActions.Insert(oAction.ExecutionIndex, aNewValues);

                    ////we need to update execution indexes
                    for (int iCounter = 0; iCounter < m_oActions.Count; iCounter++)
                        //reassign execution index
                        ((string[])m_oActions[iCounter])[1] = (iCounter + 1).ToString();


                    //alert subscribers that collection has changed
                    if (this.ActionsDirtied != null)
                        this.ActionsDirtied(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotMoveDownAction") +
                    oAction.ExecutionIndex.ToString(), oE);
            }
        }

        /// <summary>
        /// returns the collection of actions as a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder oSB = new StringBuilder();

            //cycle through actions, appending each to strin
            for (int i = 0; i < this.Count; i++)
            {
                oSB.AppendFormat("{0}{1}", this[i].ToString(),
                    LMP.StringArray.mpEndOfRecord.ToString());
            }

            //trim trailing record delimiter
            return oSB.ToString(0, Math.Max(oSB.Length - 1, 0));
        }

        #endregion
        #region *********************private members*********************
        /// <summary>
        /// saves the specified ActionBase to internal storage
        /// </summary>
        private void SaveAction(ActionBase oAction)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                //end if no current action or current action has no edits
                if (oAction == null || !oAction.IsDirty)
                    return;

                //ensure valid data
                if (!oAction.IsValid)
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidActionDefinition") + oAction.ToString());

                //IDs are assigned only when item is saved
                bool bIsNew = (oAction.ID == "");

                if (bIsNew)
                    //get ID for item
                    oAction.SetID();

                //if execution index is not specified, set to last in collection
                if (oAction.ExecutionIndex == 0)
                    oAction.ExecutionIndex = m_oActions.Count + 1;

                //get array of values
                string[] aNewValues = oAction.ToArray(true);

                if (bIsNew)
                {
                    //VariableAction is new-
                    //insert into array list
                    int iExecutionIndex = oAction.ExecutionIndex;

                    //if execution index is not specified, set to last in collection
                    if (iExecutionIndex == 0)
                        iExecutionIndex = m_oActions.Count + 1;

                    if (iExecutionIndex > m_oActions.Count)
                        //specified execution index is greater 
                        //than the last execution index - add
                        //to end of list
                        m_oActions.Add(aNewValues);
                    else
                    {
                        //insert at position specified by ExecutionIndex
                        m_oActions.Insert(iExecutionIndex - 1, aNewValues);

                        //adjust all appropriate execution indices
                        for (int i = iExecutionIndex; i < m_oActions.Count; i++)
                            ((string[])m_oActions[i])[1] = (i + 1).ToString();
                    }
                }
                else
                {
                    //save existing ActionBase - cycle through actions
                    //looking for the ActionBase we want to save
                    for (int i = 0; i < m_oActions.Count; i++)
                    {
                        string[] aOldValues = (string[])m_oActions[i];

                        if (aOldValues[0] == oAction.ID)
                        {
                            //we've found the ActionBase in the collection-
                            //replace the item with the updated array
                            m_oActions[i] = aNewValues;

                            break;
                        }
                    }
                }

                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotUpdateCollection"), oE);
            }
        }
        /// <summary>
        /// handles notification from action that it has been dirtied
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnActionDirtied(object sender, EventArgs e)
        {
            //save action
            SaveAction((ActionBase)sender);

            if (ActionsDirtied != null)
                //notify subscribers that the collection has been edited
                ActionsDirtied(this, new EventArgs());
        }
        #endregion
    }
}
