using System;
using System.Text;
using XML= System.Xml;
using LMP.Compression;

namespace LMP.Architect
{
	public enum mpContentStringTypes: byte
	{
		DocContent = 1,
		Prefill = 2
	}

	public enum mpContentStringObjectDetails: byte
	{
		ObjectID = 1,
		ObjectTypeID = 2,
		DisplayName = 3,
		CollectionIndex = 4
	}

	public enum mpContentStringAuthorDetails: byte
	{
		ID = 1,
		DisplayName = 2,
		IsLeadAuthor = 3
	}

	/// <summary>
	/// contains the methods and properties that
	/// manage the XML content string of an MPObject - 
	/// created by Doug	 Miller 10/28/04
	/// </summary>
	public class ContentString
	{
		private XML.XmlDocument m_oXML = null;
		private MPObject m_oParentMPObject = null;
		private mpContentStringTypes m_iType = mpContentStringTypes.DocContent;

		#region *********************constructors*********************
		internal ContentString(mpContentStringTypes iType, MPObject oMPObject)
		{
			m_oXML = new XML.XmlDocument();
			m_iType = iType;
			m_oParentMPObject = oMPObject;
		}
		#endregion

		#region *********************properties*********************
		internal mpContentStringTypes ContentStringType
		{
			get{return m_iType;}
			set{m_iType = value;}
		}

		public MPObject ParentMPObject
		{
			get{return m_oParentMPObject;}
			set{m_oParentMPObject = value;}
		}

		public string Text
		{
			get{return m_oXML.OuterXml;}
			set
			{
				if (value != this.Text)
				{
					m_oXML.LoadXml(value);

					if ((value != "") && (this.Text == ""))
					{
						//specified xml is invalid
						throw new LMP.Exceptions.XMLException(LMP.Resources
							.GetLangString("Error_InvalidContentString") + value);
					}
				}
			}
		}
		#endregion

		#region *********************methods*********************
		/// <summary>
		/// loads the content string for this object
		/// </summary>
		internal void Load()
		{
			//load content from parent's content string if parent exists
			MPObject oParentMPObjectParent = m_oParentMPObject.Parent;
			if (oParentMPObjectParent != null)
			{
				//parent exists - get node in parent that
				//contains data for this object
				ContentString oContent = oParentMPObjectParent.DocumentContent
					.GetSubObjectContent(m_oParentMPObject);
				if (oContent != null)
					m_oXML.LoadXml(oContent.Text);
			}
			else
			{
				//parent does not exist - load from associated Word Doc
				LoadFromWordDoc();
			}
		}

		/// <summary>
		/// returns the content string of the specified subobject
		/// </summary>
		/// <param name="oSubObject"></param>
		/// <returns></returns>
		public ContentString GetSubObjectContent(MPObject oSubObject)
		{
			if (this.Text != "")
			{
				//build an XPath statement to search for an existing child of this type
				StringBuilder oSB = new StringBuilder("child::MPObject[attribute::ObjID=");
				oSB.AppendFormat("{0} and attribute::ColIndex={1}]",
					(short) oSubObject.TypeDefinition.ObjectID,
					oSubObject.CollectionIndex);
				string xXPathQuery = oSB.ToString();

				if (Trace.IsOn)
					Trace.WriteNameValuePairs("xXPathQuery", xXPathQuery);

				XML.XmlNodeList oNodeList = m_oXML.DocumentElement.SelectNodes(xXPathQuery);

				if (oNodeList.Count > 0)
				{
					//there is a node for this object with specified object id and
					//collection index - return it as a content string
					ContentString oContent = new ContentString(this.ContentStringType, oSubObject);
					oContent.Text = oNodeList.Item(0).OuterXml;
					return oContent;
				}
			}
			return null;
		}

		/// <summary>
		/// returns the authors of the content string as an authors collection
		/// </summary>
		/// <returns></returns>
		public LMP.Architect.Authors GetAuthors()
		{
			//build where filter for people collection
			StringBuilder oSB = new StringBuilder();
			short i = 1;
			string xID;
			string xLeadID = "";

			//cycle through each author in content string -
			//null value returned if author at that index does not exist
			do
			{
				xID = GetAuthorDetail(i, mpContentStringAuthorDetails.ID);
				if (xID != null)
				{
					//get if lead author
					if (GetAuthorDetail(i, mpContentStringAuthorDetails.IsLeadAuthor) == "-1")
						xLeadID = xID;
					//split id
					string[] xIDs = xID.Split('.');
					//append to WHERE condition
					oSB.AppendFormat(" OR (ID1={0} AND ID2={1})", xIDs[0], xIDs[1]);
					//increment index
					i++;
				}
			} while (xID != null);

			//if there are no authors in content string, return null
			if (oSB.Length == 0)
				return null;

			//trim preceding "OR"
			oSB.Remove(0, 4);

			//get specified people - ToArray() is more efficient than .Count here
			//because the resulting internal array will be used by ItemFromIndex(),
			//saving us an additional query
			string xWhere = oSB.ToString();
			if (Trace.IsOn)
				Trace.WriteNameValuePairs("xWhere", xWhere);
			LMP.Data.Persons oPersons = new LMP.Data.Persons(xWhere);
			Array oArray = oPersons.ToArray();

			//add each person to the authors collection
			Authors oAuthors = new Authors(m_oParentMPObject);
			LMP.Data.Person oPerson;
			for (i=1; i<oArray.Length + 1; i++)
			{
				oPerson = oPersons.ItemFromIndex(i);
				oAuthors.Add(oPerson);
				if (oPerson.ID == xLeadID)
					//set as lead
					oAuthors.SetLeadAuthor(oPerson);
			}

			return oAuthors;
		}

		/// <summary>
		/// returns the value of the specified author detail - returns
		/// null if the specified author index is not part of content string
		/// </summary>
		/// <param name="shAuthorIndex"></param>
		/// <param name="bytDetail"></param>
		/// <returns></returns>
		public string GetAuthorDetail(short shAuthorIndex, mpContentStringAuthorDetails bytDetail)
		{
			if (Trace.IsOn)
				Trace.WriteNameValuePairs("shAuthorIndex", shAuthorIndex, "bytDetail", bytDetail);

			//no content string
			if (m_oXML.DocumentElement == null)
				return null;

			//get specified author node
			string xXPathQuery = System.String.Concat("child::Authors/child::Author[position()=",
				shAuthorIndex, ']');

			XML.XmlNodeList oNodeList = m_oXML.DocumentElement.SelectNodes(xXPathQuery);

			if (oNodeList.Count > 0)
			{
				//get value of specified attribute
				string xProp = "";
				switch (bytDetail)
				{
					case mpContentStringAuthorDetails.DisplayName:
						xProp = "DisplayName";
						break;
					case mpContentStringAuthorDetails.ID:
						xProp = "ID";
						break;
					case mpContentStringAuthorDetails.IsLeadAuthor:
						xProp = "Lead";
						break;
				}

				string xValue = oNodeList.Item(0).Attributes.GetNamedItem(xProp).Value;

				//replace problematic characters
				xValue.Replace("&amp11;", "\v");

				return xValue;
			}
			else
				//node does not exist
				return null;
		}

		/// <summary>
		/// returns the number of sub objects of the specified ObjectID
		/// </summary>
		/// <param name="shObjectID"></param>
		/// <returns></returns>
		public int CountSubObjects(LMP.Data.mpObjects shObjectID)
		{
			if (this.Text != "")
			{
				//use an XPath statement to search for existing children with the specified ObjectID
				string xXPathQuery = System.String.Concat("child::MPObject[attribute::ObjID=",
					(short) shObjectID, ']');
				if (Trace.IsOn)
					Trace.WriteNameValuePairs("xXPathQuery", xXPathQuery);
				XML.XmlNodeList oNodeList = m_oXML.DocumentElement.SelectNodes(xXPathQuery);

				//return number of found nodes
				return oNodeList.Count;
			}
			else
				//content string is empty
				return 0;
		}

		/// <summary>
		/// if the parent MPObject is a subObject, writes the content string
		/// to the parent's content string, else writes the content string
		/// to doc variables of the associated Word document
		/// </summary>
		/// <param name="bDefaultsOnly"></param>
		public void Update(bool bDefaultsOnly)
		{
			if (this.ContentStringType == mpContentStringTypes.Prefill)
			{
				throw new LMP.Exceptions.ActionException(LMP.Resources
					.GetLangString("Error_SaveReadOnlyContentString"));
			}

			if (this.Text == "")
			{
				//create new top level node
				LMP.Data.ObjectTypeDef oDef = m_oParentMPObject.TypeDefinition;
				CreateNew(oDef.DisplayName, oDef.ID, oDef.ObjectID,
					m_oParentMPObject.CollectionIndex);
			}

			//save author info
			SaveAuthors();

			//save doc props
			SaveDocProps(bDefaultsOnly);

			MPObject oParentMPObjectParent = m_oParentMPObject.Parent;
			if (oParentMPObjectParent == null)
				//save to the word document
				SaveToWordDoc();
			else
				//save content string as a node in the parent's content string
				oParentMPObjectParent.DocumentContent.SaveSubObject(this);
		}

		/// <summary>
		/// adds the specified content string as a child xml node - replaces the
		/// existing node, if one exists - a node exists if it has the same
		/// object id and collection index as the supplied XML string
		/// </summary>
		/// <param name="oContentString"></param>
		public void SaveSubObject(ContentString oContentString)
		{
			if (oContentString.ContentStringType == mpContentStringTypes.Prefill)
			{
				throw new LMP.Exceptions.ActionException(LMP.Resources
					.GetLangString("Error_SaveReadOnlyContentString"));
			}

			if (Trace.IsOn)
				Trace.WriteNameValuePairs("oContentString.Text", oContentString.Text);

			if (this.Text == "")
			{
				//create new top level node
				LMP.Data.ObjectTypeDef oDef = m_oParentMPObject.TypeDefinition;
				CreateNew(oDef.DisplayName, oDef.ID, oDef.ObjectID,
					m_oParentMPObject.CollectionIndex);
			}

			//see if there is a node already for the supplied content string - we're
			//looking for a node with same ObjectID and CollectionIndex attributes;
			//use an XPath statement to search for an existing child of this type
			StringBuilder oSB = new StringBuilder("child::MPObject[attribute::ObjID=");
			string xObjID = oContentString.GetObjectDetail
				(mpContentStringObjectDetails.ObjectID);
			string xColIndex = oContentString.GetObjectDetail
				(mpContentStringObjectDetails.CollectionIndex);
			oSB.AppendFormat("{0} and attribute::ColIndex={1}]", xObjID, xColIndex);
			string xXPathQuery = oSB.ToString();

			if (Trace.IsOn)
				Trace.WriteNameValuePairs("xXPathQuery", xXPathQuery);

			XML.XmlNodeList oNodeList = m_oXML.DocumentElement.SelectNodes(xXPathQuery);

			//if there is a node for this content string xml, edit the existing node
			if (oNodeList.Count > 0)
				m_oXML.DocumentElement.RemoveChild(oNodeList.Item(0));

			//find end of parent tag
			int lClosingTagPos = this.Text.LastIndexOf("</MPObject>");
			int lAltClosingTagPos = this.Text.LastIndexOf("/>"); 
			string xLeft;
			if (lClosingTagPos > lAltClosingTagPos)
				//the closing tag is the longhand </MPObject>
				xLeft = this.Text.Substring(0, lClosingTagPos);
			else
				//the closing tag is the shorthand "/>"
				xLeft = this.Text.Substring(0, lClosingTagPos) + '>';

			//insert child before closing tag
			m_oXML.LoadXml(System.String.Concat(xLeft, oContentString.Text, "</MPObject>"));
		}

		/// <summary>
		/// returns the value of the specified object detail
		/// </summary>
		/// <param name="bytDetail"></param>
		/// <returns></returns>
		public string GetObjectDetail(mpContentStringObjectDetails bytDetail)
		{
			//no content string
			if (m_oXML.DocumentElement == null)
				return null;

			string xXPathQuery = null;
			switch (bytDetail)
			{
				case mpContentStringObjectDetails.ObjectID:
					xXPathQuery = "attribute::ObjID";
					break;
				case mpContentStringObjectDetails.ObjectTypeID:
					xXPathQuery = "attribute::ObjTypeID";
					break;
				case mpContentStringObjectDetails.DisplayName:
					xXPathQuery = "attribute::ObjTypeName";
					break;
				case mpContentStringObjectDetails.CollectionIndex:
					xXPathQuery = "attribute::ColIndex";
					break;
			}

			XML.XmlNodeList oNodeList = m_oXML.DocumentElement.SelectNodes(xXPathQuery);
			if (oNodeList.Count > 0)
				return oNodeList.Item(0).InnerText;
			else
				return null;
		}

		/// <summary>
		/// returns the value of the specified subobject detail -
		/// returns null if subobject is not part of content string
		/// </summary>
		/// <param name="shObjectID"></param>
		/// <param name="shCollectionIndex"></param>
		/// <param name="bytDetail"></param>
		/// <returns></returns>
		public string GetSubObjectDetail(LMP.Data.mpObjects shObjectID, short shCollectionIndex,
			mpContentStringObjectDetails bytDetail)
		{
			//no content string
			if (m_oXML.DocumentElement == null)
				return null;

			//get specified sub object - sub objects are specified by Object ID and Collection Index
			StringBuilder oSB = new StringBuilder("child::MPObject[attribute::ObjID=");
			oSB.AppendFormat("{0} and attribute::ColIndex={1}]", (short) shObjectID,
				shCollectionIndex);
			string xXPathQuery = oSB.ToString();
			if (Trace.IsOn)
				Trace.WriteNameValuePairs("xXPathQuery", xXPathQuery);
			XML.XmlNodeList oNodeList = m_oXML.DocumentElement.SelectNodes(xXPathQuery);

			if (oNodeList.Count > 0)
			{
				//sub object found - get requested attribute
				XML.XmlNode oNode = oNodeList.Item(0);
				switch (bytDetail)
				{
					case mpContentStringObjectDetails.ObjectID:
						return oNode.Attributes.GetNamedItem("ObjID").Value;
					case mpContentStringObjectDetails.ObjectTypeID:
						return oNode.Attributes.GetNamedItem("ObjTypeID").Value;
					case mpContentStringObjectDetails.DisplayName:
						return oNode.Attributes.GetNamedItem("ObjTypeName").Value;
					case mpContentStringObjectDetails.CollectionIndex:
						return oNode.Attributes.GetNamedItem("ColIndex").Value;
					default:
						return null;
				}
			}
			else
				//sub object not found - return null
				return null;
		}

		/// <summary>
		/// returns the value of the specified property - returns
		/// null if property is not part of content string
		/// </summary>
		/// <param name="xPropertyName"></param>
		/// <returns></returns>
		public string GetPropertyValue(string xPropertyName)
		{
			if (Trace.IsOn)
				Trace.WriteNameValuePairs("xPropertyName", xPropertyName);

			if (this.Text == "")
				return null;

			//get XML element
			XML.XmlNodeList oNodeList = m_oXML.SelectNodes(System.String.Concat
				("MPObject/DocProperties/", xPropertyName));
			
			if (oNodeList.Count > 0)
			{
				//get node value
				string xPropertyValue = oNodeList.Item(0).InnerXml;

				//restore problematic characters
				xPropertyValue = xPropertyValue.Replace("&amp11;", "\v");

				return xPropertyValue;
			}
			else
				//node does not exist
				return null;
		}

		#endregion

		#region *********************private methods*********************

		/// <summary>
		/// loads the content string from associated Word Doc -
		/// gets the portion of the content string pertaining
		/// to the first instance of this object
		/// </summary>
		private void LoadFromWordDoc()
		{
			Word.Document oDoc = m_oParentMPObject.WordDocument;
			if (oDoc == null)
			{
				//there's no Word document
				throw new LMP.Exceptions.WordDocException(LMP.Resources
					.GetLangString("Error_InvalidWordDoc"));
			}

			//cycle through doc variables of form zzmpContentString#,
			//appending to content string
			string xVar;
			string xValue;
			StringBuilder oSB = new StringBuilder();
			int i = 1;
			do
			{
				xVar = "zzmpContentString" + i.ToString();

				mpCOM.cWordDocumentClass oMPDoc = new mpCOM.cWordDocumentClass();
				xValue = oMPDoc.GetDocumentVariableValue(oDoc, xVar, false);
				oSB.Append(xValue);
				i++;
			} while (xValue != null);

			string xContent = oSB.ToString();

			if (xContent != "")
			{
				//decompress
				try
				{
					xContent = Decompress(xContent);
				}
				catch (System.Exception e)
				{
					if (Trace.IsOn)
						Trace.WriteNameValuePairs("xContent", xContent);
					throw new LMP.Exceptions.CompressionException(LMP.Resources
						.GetLangString("Error_DecompressContentString"), e);
				}

				XML.XmlDocument oXML = new XML.XmlDocument();
				oXML.LoadXml(xContent);

				//get only that portion of the content that pertains to
				//the first instance of the object - use an XPath statement
				//to search for an existing child of this type
				oSB = new StringBuilder("child::MPObject[attribute::ObjID=");
				oSB.AppendFormat("{0} and attribute::ColIndex={1}]",
					(short) m_oParentMPObject.TypeDefinition.ObjectID,
					m_oParentMPObject.CollectionIndex);
				string xXPathQuery = oSB.ToString();

				if (Trace.IsOn)
					Trace.WriteNameValuePairs("xXPathQuery", xXPathQuery);

				XML.XmlNodeList oNodeList = oXML.SelectNodes(xXPathQuery);

				if (oNodeList.Count > 0)
					//there is a node for this object
					this.Text = oNodeList.Item(0).OuterXml;
				else
					this.Text = xContent;
			}

			if (Trace.IsOn)
				Trace.WriteNameValuePairs("xContent", xContent);
		}

		/// <summary>
		/// saves the supplied authors xml to the authors sub node -
		/// replaces the existing node, if one exists
		/// </summary>
		private void SaveAuthors()
		{
			if (m_oXML.DocumentElement == null)
			{
				throw new LMP.Exceptions.XMLException(LMP.Resources
					.GetLangString("Error_NoRootXMLElement"));
			}

			//get authors xml
			string xAuthorsXML = m_oParentMPObject.Authors.XML;
			if (Trace.IsOn)
				Trace.WriteNameValuePairs("xAuthorsXML", xAuthorsXML);

			//use an XPath statement to search for an existing authors node
			XML.XmlNodeList oNodeList = m_oXML.DocumentElement
				.SelectNodes("child::Authors");

			//remove node if it exists
			if (oNodeList.Count > 0)
				m_oXML.DocumentElement.RemoveChild(oNodeList.Item(0));

			//insert node before closing tag
			int iClosingTagPos = this.Text.LastIndexOf("</MPObject>");
			string xNew = this.Text.Insert(iClosingTagPos, xAuthorsXML);
			this.Text = xNew;
		}

		/// <summary>
		/// saves the object's doc props - replaces existing doc props node if one exists
		/// </summary>
		/// <param name="bPropsWithLinkedValuesOnly"></param>
		private void SaveDocProps(bool bPropsWithLinkedValuesOnly)
		{
			XML.XmlNode oNode = null;

			if (m_oXML.DocumentElement == null)
			{
				throw new LMP.Exceptions.XMLException(LMP.Resources
					.GetLangString("Error_NoRootXMLElement"));
			}

			//use an XPath statement to search for an existing doc props node
			XML.XmlNodeList oNodeList = m_oXML.DocumentElement
				.SelectNodes("child::DocProperties");

			//remove node if it exists
			if (oNodeList.Count > 0)
				m_oXML.DocumentElement.RemoveChild(oNodeList.Item(0));

			//add doc props node
			try
			{
				oNode = m_oXML.CreateNode(XML.XmlNodeType.Element, "DocProperties", "");
				m_oXML.DocumentElement.AppendChild(oNode);
			}
			catch (System.Exception e)
			{
				//error occurred while creating node
				throw new LMP.Exceptions.XMLNodeException(LMP.Resources
					.GetLangString("Error_CreateXMLNode" + "DocProperties"), e);
			}

			int iCount = m_oParentMPObject.DocProperties.Count;

			for(int i = 1; i <= iCount; i++)
			{
				DocProperty oDocProp = m_oParentMPObject.DocProperties.ItemFromIndex(i);

				if(!bPropsWithLinkedValuesOnly || oDocProp.Definition.LinkedValue1 != "")
				{
					//either we're saving all props or this
					//prop has a linked value - save

					XML.XmlNode oPropNode = null;

					try
					{
						oPropNode = m_oXML.CreateNode(XML.XmlNodeType.Element, oDocProp.Name,"");
						oNode.AppendChild(oPropNode);
					}
					catch(System.Exception oE)
					{
						throw new LMP.Exceptions.XMLNodeException(
							LMP.Resources.GetLangString("Error_CreateXMLNode") + oDocProp.Name, oE);
					}

					oPropNode.InnerText = oDocProp.Value;
				}
			}
		}

		/// <summary>
		/// creates a new content string with the specified attributes
		/// </summary>
		/// <param name="xObjectTypeName"></param>
		/// <param name="iObjectTypeID"></param>
		/// <param name="shObjectID"></param>
		/// <param name="shCollectionIndex"></param>
		private void CreateNew(string xObjectTypeName, int iObjectTypeID,
			LMP.Data.mpObjects shObjectID, short shCollectionIndex)
		{
			if (Trace.IsOn)
			{
				Trace.WriteNameValuePairs("xObjectTypeName", xObjectTypeName,
					"iObjectTypeID", iObjectTypeID, "shObjectID", shObjectID,
					"shCollectionIndex", shCollectionIndex);
			}

			StringBuilder oSB = new StringBuilder();
			oSB.AppendFormat("<MPObject ObjTypeName=\"{0}\" ObjTypeID=\"{1}\"",
				xObjectTypeName, iObjectTypeID);
			oSB.AppendFormat(" ColIndex=\"{0}\" ObjID=\"{1}\"></MPObject>",
				shCollectionIndex, (short) shObjectID);
			m_oXML.LoadXml(oSB.ToString());
		}

		/// <summary>
		/// writes variables to Word document
		/// </summary>
		private void SaveToWordDoc()
		{
			Word.Document oDoc = m_oParentMPObject.WordDocument;
			if (oDoc == null)
			{
				//there's no Word document
				throw new LMP.Exceptions.WordDocException(LMP.Resources
					.GetLangString("Error_InvalidWordDoc"));
			}
			
			//compress string
			string xRemaining = Compress(this.Text);

			string xNextPortion;
			object xVar;
			int iChars;
			int i = 0;
			while (xRemaining != "")
			{
				//add next 65K to next Word doc var in sequence
				iChars = System.Math.Min(xRemaining.Length, 50000);
				xNextPortion = xRemaining.Substring(0, iChars);

				//set doc variable
				i++;
				xVar = System.String.Concat("zzmpContentString", i);
				oDoc.Variables.Item(ref xVar).Value = xNextPortion;

				//trim stored portion
				xRemaining = xRemaining.Substring(iChars);
			}
		}

		private string Compress(string xText)
		{
			UnicodeEncoding oE = new UnicodeEncoding();
			byte[] bufIn = oE.GetBytes(xText);
			byte[] bufOut = new byte[4096];

			Deflater dflt = new Deflater(Deflater.BEST_COMPRESSION, true);
			dflt.SetInput(bufIn);
			dflt.Finish();
			dflt.Deflate(bufOut);

			string xC = oE.GetString(bufOut);
			return xC.TrimEnd('\0');
		}

		private string Decompress(string xText)
		{
			UnicodeEncoding oE = new UnicodeEncoding();
			byte[] bufIn = oE.GetBytes(xText);
			byte[] bufOut = new byte[4096];

			Inflater inflt = new Inflater(true);
			inflt.SetInput(bufIn);
			inflt.Inflate(bufOut);

			string xC = oE.GetString(bufOut);
			return xC.TrimEnd('\0');
		}

		#endregion

	}
}
