using System;
using System.Windows.Forms;
using LMP.Data;
using System.IO;
using System.Xml;

namespace LMP.Architect.Base
{
    public delegate void CurrentSegmentChangedEventHandler(object sender, CurrentSegmentSwitchedEventArgs args);

	/// <summary>
	/// contains the members of a MacPac document -
	/// a Word document created by MacPac.
	/// </summary>
	public abstract class ForteDocument
	{
        public enum Types
        {
            NonMacPac = 0,
            MacPac7 = 1,
            MacPac8 = 2,
            MacPac85 = 3,
            MacPac9 = 4,
            MacPac10 = 5
        }

        public enum RefreshScopes
        {
            WholeDocument = 0,
            StandaloneElementsOnly = 1
        }

        public enum RefreshOptions
        {
            None = 0,
            RefreshTagsWithIntegerIndexes = 1,
            RefreshTagsWithGUIDIndexes = 2
        }

        public enum TagTypes : byte
        {
            None = 0,
            Segment = 1,
            Variable = 2,
            Block = 3,
            DeletedBlock = 4,
            SubVariable = 5,
            Mixed = 6
        }

        public enum Modes
        {
            Edit = 1,
            Design = 2,
            Preview = 3
        }

        [Flags]
        public enum WordXMLEvents
        {
            None = 0,
            XMLSelectionChange = 1,
            XMLBeforeDelete = 2,
            XMLAfterInsert = 4,
            All = 7
        }
    }
}
