using System;
using LMP.Data;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Reflection;
using LMP.Controls;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.Architect.Base
{
    public class UserVariable: Variable
	{
    }

	public class AdminVariable: Variable
	{

    }

    /// <summary>
	/// contains the methods and properties that define a
	/// MacPac Variable - created by Daniel Fisherman 09/20/05
	/// </summary>
	public abstract class Variable
	{
		#region *********************constants*********************
		#endregion
		#region *********************enumerations*********************
		public enum VariableTypes
		{
			Admin = 1,
			User = 2
		}

        [Flags]
        public enum ControlHosts
        {
            None = 0,
            DocumentEditor = 1,
            AuthorPreferenceManager = 2,
            Wizard = 4
        }
        public enum Levels
        {
            Basic = 1,
            Advanced = 2
        }
        public enum PrefillOverrideTypes
        {
            Never = 0,
            Always = 1,
            ForSameType = 2,
            SameSegmentID = 3
        }
		#endregion
        #region *********************fields*********************
        protected string m_xID = "";
        protected string m_xName = "";
        protected string m_xDisplayName = "";
        protected int m_iTranslationID = 0;
        protected int m_iExecutionIndex;
        protected LMP.Controls.IControl m_oAssocCtl;
        protected string m_xDefaultValue = "";
        protected string m_xValueSourceExpression = "";
        protected string m_xReserved;
        protected mpControlTypes m_bytControlType = mpControlTypes.Textbox;
        protected string m_xControlProperties = "";
        protected string m_xValidationCondition = "";
        protected string m_xValidationResponse = "";
        protected string m_xDescription = "";
        protected string m_xSegmentName = "";
        protected string m_xTagID = "";
        protected string m_xValue = null;
        protected string m_xHotkey;
        protected bool m_bValueIsValid = false;
        protected bool m_bValidityTested = false;
        //protected VariableActions m_oActions = null;
        //protected ControlActions m_oControlActions;
        protected bool m_bIsDirty;
        protected string m_xReserveValue = "";
        protected string m_xSecondaryDefaultValue = null;
        protected bool m_bRequestCIForEntireSegment = true;
        protected ciContactTypes m_iCIContactType;
        protected string m_xCIDetailTokenString;
        protected string m_xCIEntityName;
        protected ciAlerts m_iCIAlerts = ciAlerts.ciAlert_None;
        protected Variable.ControlHosts m_iControlHosts = Variable.ControlHosts.DocumentEditor;
        protected Variable.Levels m_iVarLevel = Levels.Basic;
        //protected Segment m_oSegment;
        protected ForteDocument m_oMPDocument;
        protected string m_xTagParentPartNumbers = "1";
        protected ForteDocument.TagTypes m_bytTagType = ForteDocument.TagTypes.Variable;
        protected string m_xDisplayValue = "";
        protected bool m_bMustVisit = false;
        protected PrefillOverrideTypes m_iAllowPrefillOverride = PrefillOverrideTypes.Always;
        protected bool m_bMultipleValues = false;
        protected string m_xAssociatedPrefillNames = "";
        protected int m_iTabNumber = 1;
        //protected Variable m_oUISourceVariable = null;
        protected bool m_bHasInitializedValue = false;
        protected string m_xTagPrefixID = "";
        protected int m_iObjectDatabaseID = 0;

        //GLOG 2645
        protected string m_xRuntimeControlValues = "";
        //GLOG 1408
        protected string m_xNavigationTag = "";
        #endregion
        #region *********************properties*********************
        public string ID
        {
            get { return m_xID; }
            set { this.SetStringPropertyValue(ref m_xID, value); }
        }
        public string Name
        {
            get { return m_xName; }
            set { this.SetStringPropertyValue(ref m_xName, value, 255); }
        }
        public string DisplayName
        {
            get { return m_xDisplayName; }
            set { this.SetStringPropertyValue(ref m_xDisplayName, value, 50); }
        }
        public int TranslationID
        {
            get { return m_iTranslationID; }
            set { this.SetIntPropertyValue(ref m_iTranslationID, value); }
        }
        public int ExecutionIndex
        {
            get { return m_iExecutionIndex; }
            set { this.SetIntPropertyValue(ref m_iExecutionIndex, value); }
        }
        public string DefaultValue
        {
            get { return m_xDefaultValue; }
            set { this.SetStringPropertyValue(ref m_xDefaultValue, value, 0); }
        }
        public string SecondaryDefaultValue
        {
            get { return m_xSecondaryDefaultValue; }
            set { this.SetStringPropertyValue(ref m_xSecondaryDefaultValue, value, 0); }
        }
        public string ValueSourceExpression
        {
            get { return m_xValueSourceExpression; }
            set { this.SetStringPropertyValue(ref m_xValueSourceExpression, value); }
        }
        /// <summary>
        /// gets/sets the control associated with this variable
        /// </summary>
        public LMP.Controls.IControl AssociatedControl
        {
            get { return m_oAssocCtl; }
            set { m_oAssocCtl = value; }
        }
        /// <summary>
        /// this property was originallly AlwaysUpdateDefaultValue 
        /// which is now obsolete - we're keeping the property in 
        /// case we need one later
        /// </summary>
        protected string Reserved
        {
            get { return m_xReserved; }
            set { this.SetStringPropertyValue(ref m_xReserved, value); }
        }
        public mpControlTypes ControlType
        {
            get { return m_bytControlType; }
            set
            {
                if (m_bytControlType != value)
                {
                    m_bytControlType = value;
                    this.IsDirty = true;
                }
            }
        }
        public string ControlProperties
        {
            get { return m_xControlProperties; }
            set { this.SetStringPropertyValue(ref m_xControlProperties, value, 0); }
        }
        /// <summary>
        /// GLOG 2645: ObjectData property to hold values used to configure associate control at runtime
        /// </summary>
        public string RuntimeControlValues
        {
            get { return m_xRuntimeControlValues; }
        }
        public string ValidationCondition
        {
            get { return m_xValidationCondition; }
            set { this.SetStringPropertyValue(ref m_xValidationCondition, value); }
        }
        public string ValidationResponse
        {
            get { return m_xValidationResponse; }
            set { this.SetStringPropertyValue(ref m_xValidationResponse, value); }
        }
        public string HelpText
        {
            get { return m_xDescription; }
            set { this.SetStringPropertyValue(ref m_xDescription, value, 0); }
        }
        public string SegmentName
        {
            get { return m_xSegmentName; }
            set { this.SetStringPropertyValue(ref m_xSegmentName, value, 0); } //GLOG 8371
        }
        public string TagID
        {
            get { return m_xTagID; }
            set { this.SetStringPropertyValue(ref m_xTagID, value, 0); }
        }
        public string Hotkey
        {
            get
            {
                if (m_xHotkey != null)
                    return m_xHotkey.ToUpper();
                else
                    return null;
            }

            set { this.SetStringPropertyValue(ref m_xHotkey, value, 1); }
        }
        public string TagParentPartNumbers
        {
            get { return m_xTagParentPartNumbers; }
            set { this.SetStringPropertyValue(ref m_xTagParentPartNumbers, value); }
        }
        /// <summary>
        /// get/set whether to allow a prefill value with 
        /// the same name to change the value of the variable
        /// </summary>
        public PrefillOverrideTypes AllowPrefillOverride
        {
            set
            {
                if (m_iAllowPrefillOverride != value)
                {
                    m_iAllowPrefillOverride = value;
                    this.IsDirty = true;
                }
            }
            get { return m_iAllowPrefillOverride; }
        }
        /// <summary>
        /// get/set list of Prefill Property names
        /// that can be used to set this variable
        /// </summary>
        public string AssociatedPrefillNames
        {
            set
            {
                if (m_xAssociatedPrefillNames != value)
                {
                    m_xAssociatedPrefillNames = value;
                    this.IsDirty = true;
                }
            }
            get { return m_xAssociatedPrefillNames; }
        }
        /// <summary>
        /// Dialog tab on which to place this variable
        /// </summary>
        public int TabNumber
        {
            set
            {
                if (m_iTabNumber != value)
                {
                    m_iTabNumber = value;
                    this.IsDirty = true;
                }
            }
            get { return m_iTabNumber; }
        }
        /// <summary>
        /// get/set whether individual values
        /// are distributed among multiple tags
        /// </summary>
        public bool IsMultiValue
        {
            set
            {
                if (m_bMultipleValues != value)
                {
                    m_bMultipleValues = value;
                    this.IsDirty = true;
                }
            }
            get { return m_bMultipleValues; }
        }

        protected internal void ForceValueRefresh()
        {
            //modified 5/19/11 (dm)
            m_xValue = null;
            //m_xValue = "";
        }
        public bool HasInitializedValue
        {
            get { return m_bHasInitializedValue; }
        }
        /// <summary>
        /// gets/sets the value of the variable when either
        /// a programmatic deletion has occurred (e.g. during delete scope deletion)
        /// or when the variable is not mapped to an mVar
        /// </summary>
        public string ReserveValue
        {
            get { return m_xReserveValue; }
            set
            {
                //GLOG 7921/7947 (dm) - this code was causing the value to be lost
                //on refresh and seems to no longer be necessary
                //if (!(this.HasDistributedSegmentVariableActions() &&
                //    this.Segment is IStaticDistributedSegment))
                //{
                m_xReserveValue = value.Replace("\v", "\r\n");
                //}
            }
        }

        public bool IsDirty
        {
            get { return m_bIsDirty; }
            set { m_bIsDirty = value; }
        }
        /// <summary>
        /// returns true if the variable has valid data
        /// </summary>
        public bool HasValidDefinition
        {
            get
            {
                return this.ID != "" && this.Name != "" &&
                    this.DisplayName != "" && this.ExecutionIndex != 0;
            }
        }
        /// <summary>
        /// returns the type of variable
        /// </summary>
        public VariableTypes Type
        {
            get
            {
                if (this.ID.StartsWith("U"))
                    return VariableTypes.User;
                else
                    return VariableTypes.Admin;
            }
        }
        /// <summary>
        /// gets/sets whether the call to CI will
        /// populate the values of all CI-enabled 
        /// variables or just this one
        /// </summary>
        public bool RequestCIForEntireSegment
        {
            get { return m_bRequestCIForEntireSegment; }
            set
            {
                if (m_bRequestCIForEntireSegment != value)
                {
                    m_bRequestCIForEntireSegment = value;
                    this.IsDirty = true;
                }
            }
        }
        /// <summary>
        /// gets/sets the CI contact type linked to this variable
        /// </summary>
        public ciContactTypes CIContactType
        {
            get { return m_iCIContactType; }
            set
            {
                if (m_iCIContactType != value)
                {
                    m_iCIContactType = value;
                    this.IsDirty = true;
                }
            }
        }
        /// <summary>
        /// gets/sets the name of the CI entity
        /// associated with this variable
        /// </summary>
        public string CIEntityName
        {
            get { return m_xCIEntityName; }
            set
            {
                if (m_xCIEntityName != value)
                {
                    m_xCIEntityName = value;
                    this.IsDirty = true;
                }
            }
        }
        /// <summary>
        /// gets/sets the CI detail token string used
        /// to populate the value of this variable
        /// </summary>
        public string CIDetailTokenString
        {
            get { return m_xCIDetailTokenString; }
            set
            {
                if (m_xCIDetailTokenString != value)
                {
                    m_xCIDetailTokenString = value;
                    this.IsDirty = true;
                }
            }
        }
        /// <summary>
        /// gets/sets the CI alerts to display when
        /// retrieving contacts for this variable
        /// </summary>
        public ciAlerts CIAlerts
        {
            get { return m_iCIAlerts; }
            set
            {
                if (m_iCIAlerts != value)
                {
                    m_iCIAlerts = value;
                    this.IsDirty = true;
                }
            }
        }
        public ForteDocument.TagTypes TagType
        {
            get { return m_bytTagType; }
            set { m_bytTagType = value; }
        }
        /// <summary>
        /// gets/sets the UI hosts where the 
        /// variable control will be displayed
        /// </summary>
        public ControlHosts DisplayIn
        {
            get { return m_iControlHosts; }
            set
            {
                m_iControlHosts = value;
                this.m_bIsDirty = true;
            }
        }
        /// <summary>
        /// gets/sets the level of the variable -
        /// either basic or advanced
        /// </summary>
        public Variable.Levels DisplayLevel
        {
            get { return m_iVarLevel; }
            set
            {
                m_iVarLevel = value;
                this.m_bIsDirty = true;
            }
        }
        /// <summary>
        /// sets the DisplayValue of the variable
        /// usually an expression that can be evaluated
        /// to show the actual text value of variable
        /// XML
        /// </summary>
        public string DisplayValue
        {
            get { return m_xDisplayValue; }
            set
            {
                m_xDisplayValue = value;
                this.m_bIsDirty = true;
            }
        }
        /// <summary>
        /// returns the id of the client to whom this variable belongs
        /// </summary>
        public string TagPrefixID
        {
            get { return m_xTagPrefixID; }
            set { m_xTagPrefixID = value; }
        }

        public bool BelongsToCurrentClient
        {
            get
            {
                //1-3-11 (dm) - modified to always accept tag with internal prefix
                string xCurClient = LMP.Data.Application.CurrentClientTagPrefixID;
                return ((this.TagPrefixID == xCurClient) || (this.TagPrefixID == "") ||
                    (this.TagPrefixID == LMP.Data.ForteConstants.mpInternalTagPrefixID) ||
                    (xCurClient == LMP.Data.ForteConstants.mpInternalTagPrefixID));
            }
        }
        //GLOG 1408
        public string NavigationTag
        {
            get { return m_xNavigationTag; }
            set
            {
                if (m_xNavigationTag != value)
                {
                    m_xNavigationTag = value;
                    m_bIsDirty = true;
                }
            }
        }

        /// <summary>
        /// returns the internal value of this variable
        /// </summary>
        public string ValueNotFromSource
        {
            get { return m_xValue; }
        }
        /// <summary>
        /// returns the variable/block id of this block - added for 10.2 -
        /// used to recover the definition in the event that doc vars are stripped
        /// </summary>
        public int ObjectDatabaseID
        {
            get { return m_iObjectDatabaseID; }
            set { m_iObjectDatabaseID = value; }
        }
        /// <summary>
        /// Returns true if Default Value or Action Parameter contains Author fieldcode
        /// </summary>

        /// <summary>
        /// returns true if this variable has more than one tag
        /// </summary>


        /// <summary>
        /// returns TRUE if this variable is a component of a master detail variable 
        /// </summary>
        public bool IsDetailSubComponent
        {
            get
            {
                return (((this.ValueSourceExpression == "[DetailValue]") ||
                        (this.ValueSourceExpression == "[DetailValue]" + "__" + this.Name)) &&
                        (this.ControlType != mpControlTypes.DetailGrid) &&
                        (this.ControlType != mpControlTypes.DetailList));
            }
        }
        #endregion
        #region*********************private members*************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xVar"></param>
        /// <param name="xVal"></param>
        /// <param name="iMaxLength">maximum length of string - specify 0 for an unlimited length</param>
        protected void SetStringPropertyValue(ref string xVar, string xVal, int iMaxLength)
        {
            if (xVar != xVal)
            {
                if (iMaxLength > 0 && xVal.Length > iMaxLength)
                {
                    throw new LMP.Exceptions.DataException(
                        Resources.GetLangString("Error_PropertyCharacterOverflow"));
                }

                xVar = xVal;
                this.IsDirty = true;
            }
        }
        /// <summary>
        /// sets the value of the specified string variable to the specified value, with a maximum length of 255 characters.
        /// </summary>
        /// <param name="xVar"></param>
        /// <param name="xVal"></param>
        protected void SetStringPropertyValue(ref string xVar, string xVal)
        {
            SetStringPropertyValue(ref xVar, xVal, 255);
        }
        /// <summary>
        /// sets the value of the specified integer variable to the specified value
        /// </summary>
        /// <param name="xVar"></param>
        /// <param name="xVal"></param>
        protected void SetIntPropertyValue(ref int iVar, int iVal)
        {
            if (iVar != iVal)
            {
                iVar = iVal;
                this.IsDirty = true;
            }
        }
        /// <summary>
        /// sets the value of the specified byte variable to the specified value
        /// </summary>
        /// <param name="xVar"></param>
        /// <param name="xVal"></param>
        protected void SetBytePropertyValue(ref byte byVar, byte byVal)
        {
            if (byVar != byVal)
            {
                byVar = byVal;
                this.IsDirty = true;
            }
        }
        /// <summary>
        /// sets the value of the specified short variable to the specified value
        /// </summary>
        /// <param name="xVar"></param>
        /// <param name="xVal"></param>
        protected void SetShortPropertyValue(ref short shVar, short shVal)
        {
            if (shVar != shVal)
            {
                shVar = shVal;
                this.IsDirty = true;
            }
        }
        /// <summary>
        /// sets the value of the specified boolean variable to the specified value
        /// </summary>
        /// <param name="xVar"></param>
        /// <param name="xVal"></param>
        protected void SetBoolPropertyValue(ref bool bVar, bool bVal)
        {
            if (bVar != bVal)
            {
                bVar = bVal;
                this.IsDirty = true;
            }
        }
        /// <summary>
        /// returns the collection of variable actions 
        /// assigned to this variable as a delimited string
        /// </summary>
        /// <returns></returns>
        protected string[] GetDetailGridFieldNames()
        {
            int iOffset = 0;
            int iPos = this.ControlProperties.IndexOf("ConfigString=") + 13;
            string xConfig = this.ControlProperties.Substring(iPos);
            string[] aConfig = xConfig.Split(LMP.StringArray.mpEndOfSubValue);
            xConfig = aConfig[0];
            aConfig = xConfig.Split(LMP.StringArray.mpEndOfSubField);
            int iFields = aConfig.Length / 5;
            string[] aFields = new string[iFields];
            for (int i = 0; i < iFields; i++)
            {
                aFields[i] = aConfig[iOffset + 1];
                iOffset = iOffset + 5;
            }
            return aFields;
        }
        #endregion
    }

	/// <summary>
	/// contains the methods and properties that manage
	/// a collection of MacPac variables - created by Daniel Fisherman 09/20/05
	/// </summary>
	public class Variables
	{
	}
}
