using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Base
{
    public class Letter : AdminSegment
    {
    }

    public class LetterSignatures : CollectionTable
    {
    }

    public class LetterSignature : CollectionTableItem
    {
    }

    public class LetterSignatureNonTable : AdminSegment
    {
    }

    public class Letterhead : Paper
    {
    }
}
