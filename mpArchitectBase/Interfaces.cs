using System;
using System.Collections.Generic;
using System.Text;

namespace LMP.Architect.Base
{
    public interface ISingleInstanceSegment
    {
    }

    public interface ILitigationAddOnSegment
    {
    }

    /// <summary>
    /// interface designation that a segment is created
    /// after the user clicks the finish button - not
    /// like segments that are created as the user
    /// enters values in the document editor - 
    /// Bates Labels is an example of a static creation segment
    /// </summary>
    public interface IStaticCreationSegment
    {
    }

    /// <summary>
    /// interface designation that segment distributes
    /// detail across table celss only when the user clicks finish
    /// </summary>
    public interface IStaticDistributedSegment
    {
    }
}
