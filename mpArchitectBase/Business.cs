using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Base
{
    public class Business : AdminSegment
    {
    }

    public class BusinessSignatures : CollectionTable
    {
    }

    public class BusinessSignature : CollectionTableItem
    {
    }

    public class BusinessSignatureNonTable : AdminSegment
    {
    }

    public class BusinessTitlePage : AdminSegment, ISingleInstanceSegment
    {
        //#region ISingleInstanceSegment Members

        //Segments ISingleInstanceSegment.GetExistingSegments(int iSectionIndex)
        //{
        //    throw new NotImplementedException();
        //}

        //#endregion
    }
}
