using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using LMP.DMS;

namespace LMP.Architect.Base
{
    public static class DMS
    {
        static DMSBackend m_oDMS = null;
        static DMS()
        {
            DMSBackendType iDMS = DMSBackendType.Windows;
            //Get DMS type from Firm Application Settings
            LMP.Data.FirmApplicationSettings oSettings = new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);
            try
            {
                int iTest = oSettings.DMSType;
                iDMS = (DMSBackendType)iTest;
            }
            catch 
            {
                iDMS = DMSBackendType.Windows;
            }
            switch (iDMS)
            {
                case DMSBackendType.Windows:
                    m_oDMS = new WindowsBackend();
                    break;
                //TODO: Fill in other backend types
                default:
                    m_oDMS = new WindowsBackend();
                    break;
            }
            m_oDMS.ProductName = LMP.ComponentProperties.ProductName;
        }
        public static DMSBackend ProfileObject
        {
            get { return m_oDMS; }
        }
        /// <summary>
        /// Returns true of activedocument is profiled in DMS
        /// </summary>
        public static bool IsProfiled
        {
            //JTS 12/16/10: If not Profiled, GetProfileValue might return null instead of ""
            get { return m_oDMS.DMSType != DMSBackendType.Windows
                && !string.IsNullOrEmpty(GetProfileValue("DocNumber")); }
        }
        /// <summary>
        /// Return 
        /// </summary>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        public static string GetProfileValue(string xPropName)
        {
            return GetProfileValue(xPropName, "");
        }
        public static string GetProfileValue(string xPropName, string xDateFormat)
        {
            string xValue = "";
            try
            {
                switch (xPropName.ToUpper())
                {
                    case "DOCNUMBER":
                        xValue = m_oDMS.DocNumber;
                        break;
                    case "VERSION":
                        xValue = m_oDMS.Version;
                        break;
                    case "LIBRARY":
                        xValue = m_oDMS.Library;
                        break;
                    case "CLIENTID":
                        xValue = m_oDMS.ClientID;
                        break;
                    case "CLIENTNAME":
                        xValue = m_oDMS.ClientName;
                        break;
                    case "MATTERID":
                        xValue = m_oDMS.MatterID;
                        break;
                    case "MATTERNAME":
                        xValue = m_oDMS.MatterName;
                        break;
                    case "AUTHORID":
                        xValue = m_oDMS.AuthorID;
                        break;
                    case "AUTHORNAME":
                        xValue = m_oDMS.AuthorName;
                        break;
                    case "TYPISTID":
                        xValue = m_oDMS.TypistID;
                        break;
                    case "TYPISTNAME":
                        xValue = m_oDMS.TypistName;
                        break;
                    case "ABSTRACT":
                        xValue = m_oDMS.Abstract;
                        break;
                    case "DOCNAME":
                        xValue = m_oDMS.DocName;
                        break;
                    case "DOCTYPEDESCRIPTION":
                        xValue = m_oDMS.DocTypeDescription;
                        break;
                    case "DOCTYPEID":
                        xValue = m_oDMS.DocTypeID;
                        break;
                    case "FILENAME":
                        xValue = m_oDMS.FileName;
                        break;
                    case "GROUPNAME":
                        xValue = m_oDMS.GroupName;
                        break;
                    case "PATH":
                        xValue = m_oDMS.Path;
                        break;
                    case "REVISIONDATE":
                        xValue = m_oDMS.RevisionDate;
                        //Format Date
                        if (xDateFormat != "")
                        {
                            try
                            {
                                DateTime oDT = DateTime.Parse(xValue);
                                if (oDT != null)
                                {
                                    xValue = oDT.ToString(xDateFormat);
                                }
                            }
                            catch { }
                        }
                        break;
                    case "CREATIONDATE":
                        xValue = m_oDMS.CreationDate;
                        //Format Date
                        if (xDateFormat != "")
                        {
                            try
                            {
                                DateTime oDT = DateTime.Parse(xValue);
                                if (oDT != null)
                                {
                                    xValue = oDT.ToString(xDateFormat);
                                }
                            }
                            catch { }
                        }
                        break;
                    case "CUSTOM1":
                        xValue = m_oDMS.Custom1;
                        break;
                    case "CUSTOM2":
                        xValue = m_oDMS.Custom2;
                        break;
                    case "CUSTOM3":
                        xValue = m_oDMS.Custom3;
                        break;
                    default:
                        //Get any other profile property using DMS-specific field ID
                        xValue = m_oDMS.GetProfileInfo(xPropName);
                        break;
                }
            }
            catch
            {
            }
            return xValue;
        }
    }
}
