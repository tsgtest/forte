using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect.Base
{
    public class Pleading : AdminSegment
    {
    }

    //GLOG 7154
    public class LitigationBack : AdminSegment
    {
    }
    
    public class PleadingSignatures : CollectionTable
    {
    }

    public class PleadingCounsels : CollectionTable
    {
    }

    public class PleadingCaptions : CollectionTable
    {
    }

    public class PleadingSignature : CollectionTableItem
    {
    }

    public class PleadingCounsel : CollectionTableItem
    {
    }

    public class PleadingCaption : CollectionTableItem
    {

    }

    public class PleadingPaper : Paper
    {
    }
    public class Sidebar : AdminSegment, IDocumentStamp, ISingleInstanceSegment
    {
        //#region ISingleInstanceSegment Members

        //Segments ISingleInstanceSegment.GetExistingSegments(int iSectionIndex)
        //{
        //    throw new NotImplementedException();
        //}

        //#endregion
    }

    public class PleadingSignatureNonTable : AdminSegment
    {
    }

    public class PleadingCoverPage : AdminSegment
    {
    }
}
