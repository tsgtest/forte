using System;

namespace LMP.Architect.Base
{
	public delegate void ValidationChangedHandler(object sender, ValidationChangedEventArgs e);

	public class ValidationChangedEventArgs: EventArgs
	{
		public bool Validated;

		public ValidationChangedEventArgs(bool bValidated)
		{
			this.Validated = bValidated;
		}
	}

}
