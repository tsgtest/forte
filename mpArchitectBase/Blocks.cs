using System;
using LMP.Data;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace LMP.Architect.Base
{
    /// <summary>
    /// contains properties that define the arguments for the BlockDeleted and BlockAdded events
    /// </summary>
    //public class BlockEventArgs
    //{
    //    private Block m_oBlock;

    //    public BlockEventArgs(Block oBlock)
    //    {
    //        m_oBlock = oBlock;
    //    }

    //    public Block Block
    //    {
    //        get { return m_oBlock; }
    //        set { m_oBlock = value; }
    //    }
    //}

    ///// <summary>
    ///// declares the delegate for a BlockDeleted event handler
    ///// </summary>
    //public delegate void BlockDeletedHandler(object sender, BlockEventArgs oArgs);

    ///// <summary>
    ///// declares the delegate for a BlockAdded event handler
    ///// </summary>
    //public delegate void BlockAddedHandler(object sender, BlockEventArgs oArgs);

    ///// <summary>
    ///// declares the delegate for a BlockDefinitionChanged event handler
    ///// </summary>
    //public delegate void BlockDefinitionChangedHandler(object sender, BlockEventArgs oArgs);

    /// <summary>
    /// contains the methods and properties that define a MacPac Block
    /// </summary>
	public abstract class Block
	{
        private string m_xTagID;
        private string m_xName;
        private string m_xDisplayName;
        private int m_iTranslationID = 0;
        private bool m_bIsBody;
        private bool m_bShowInTree;
        private string m_xDescription = "";
        private Variable.Levels m_iBlockLevel = Variable.Levels.Basic;
        private bool m_bIsDirty;
        private string m_xSegmentName = "";
        private string m_xTagParentPartNumber = "1";
        private string m_xHotkey = "";
        private string m_xTagPrefixID = "";
        private int m_iObjectDatabaseID = 0;
        //GLOG 2165
        private string m_xStartingText = "BeginTypingHere";

        #region *********************properties*********************
        public bool IsDirty
        {
            get { return m_bIsDirty; }
            set { m_bIsDirty = value; }
        }

        public string TagID
        {
            get { return m_xTagID; }
            set
            {
                if (m_xTagID != value)
                {
                    //GLOG 8894: Remove this limitation
                    //if (value.Length > 255)
                    //{
                    //    throw new LMP.Exceptions.DataException(
                    //        Resources.GetLangString("Error_PropertyCharacterOverflow"));
                    //}
                    m_xTagID = value;
                    m_bIsDirty = true;
                }

            }
        }

        public string Name
        {
            get { return m_xName; }
            set
            {
                if (m_xName != value)
                {
                    if (value.Length > 255)
                    {
                        throw new LMP.Exceptions.DataException(
                            Resources.GetLangString("Error_PropertyCharacterOverflow"));
                    }
                    m_xName = value;
                    m_bIsDirty = true;
                }

            }
        }

        public string DisplayName
        {
            get { return m_xDisplayName; }
            set
            {
                if (m_xDisplayName != value)
                {
                    if (value.Length > 255)
                    {
                        throw new LMP.Exceptions.DataException(
                            Resources.GetLangString("Error_PropertyCharacterOverflow"));
                    }
                    m_xDisplayName = value;
                    m_bIsDirty = true;
                }

            }
        }
        public string StartingText
        {
            //GLOG 2165
            get { return m_xStartingText; }
            set
            {
                if (m_xStartingText != value)
                {
                    if (value.Length > 255)
                    {
                        throw new LMP.Exceptions.DataException(
                            Resources.GetLangString("Error_PropertyCharacterOverflow"));
                    }
                    m_xStartingText = value;
                    m_bIsDirty = true;
                }
            }
        }
        public string Hotkey
        {
            get { return m_xHotkey.ToUpper(); }
            set
            {
                if (m_xHotkey != value)
                {
                    if (value.Length > 2)
                    {
                        throw new LMP.Exceptions.DataException(
                            Resources.GetLangString("Error_PropertyCharacterOverflow"));
                    }
                    m_xHotkey = value;
                    m_bIsDirty = true;
                }

            }
        }

        public int TranslationID
        {
            get { return m_iTranslationID; }
            set
            {
                if (m_iTranslationID != value)
                {
                    m_iTranslationID = value;
                    m_bIsDirty = true;
                }
            }
        }

        public bool IsBody
        {
            get { return m_bIsBody; }
            set
            {
                if (m_bIsBody != value)
                {
                    m_bIsBody = value;
                    m_bIsDirty = true;
                }
            }
        }

        public bool ShowInTree
        {
            get { return m_bShowInTree; }
            set
            {
                if (m_bShowInTree != value)
                {
                    m_bShowInTree = value;
                    m_bIsDirty = true;
                }
            }
        }

        public string HelpText
        {
            get { return m_xDescription; }
            set
            {
                if (m_xDescription != value)
                {
                    m_xDescription = value;
                    m_bIsDirty = true;
                }

            }
        }

        /// <summary>
        /// Allow the designer to determine where the block is shown, ie either 
        /// in the basic or in the advanced (More...) section.
        /// </summary>
        public LMP.Architect.Base.Variable.Levels DisplayLevel
        {
            get
            {
                return m_iBlockLevel;
            }

            set
            {
                m_iBlockLevel = value;
                m_bIsDirty = true;
            }
        }

        public string SegmentName
        {
            get { return m_xSegmentName; }
            set
            {
                if (m_xSegmentName != value)
                {
                    m_xSegmentName = value;
                    m_bIsDirty = true;
                }

            }
        }

        public string TagParentPartNumber
        {
            get { return m_xTagParentPartNumber; }
            set
            {
                if (m_xTagParentPartNumber != value)
                {
                    m_xTagParentPartNumber = value;
                    m_bIsDirty = true;
                }

            }
        }

        //return true if the block has valid data
        public bool HasValidDefinition
        {
            get
            {
                return this.Name != "" && this.DisplayName != "";
            }
        }

        /// <summary>
        /// returns the id of the client to whom this block belongs
        /// </summary>
        public string TagPrefixID
        {
            get { return m_xTagPrefixID; }
            set { m_xTagPrefixID = value; }
        }

        public bool BelongsToCurrentClient
        {
            get
            {
                //1-3-11 (dm) - modified to always accept tag with internal prefix
                string xCurClient = LMP.Data.Application.CurrentClientTagPrefixID;
                return ((this.TagPrefixID == xCurClient) || (this.TagPrefixID == "") ||
                    (this.TagPrefixID == LMP.Data.ForteConstants.mpInternalTagPrefixID) ||
                    (xCurClient == LMP.Data.ForteConstants.mpInternalTagPrefixID));
            }
        }

        /// <summary>
        /// returns the variable/block id of this block - added for 10.2 -
        /// used to recover the definition in the event that doc vars are stripped
        /// </summary>
        public int ObjectDatabaseID
        {
            get { return m_iObjectDatabaseID; }
            set { m_iObjectDatabaseID = value; }
        }
        #endregion
        #region *********************methods*********************
        public string[] ToArray()
        {
            return ToArray(false);
        }

        /// <summary>
        /// returns the persistent values of the block
        /// as an array - the persistent values are those
        /// that are stored in the Word Tag. SegmentName and
        /// TagID are added to the array at run time
        /// </summary>
        /// <param name="bPersistentValuesOnly"></param>
        /// <returns></returns>
        public string[] ToArray(bool bPersistentValuesOnly)
        {
            string[] aDef = null;
            int iOffset = 0;

            if (bPersistentValuesOnly)
            {
                //GLOG 2165
                aDef = new string[10];
            }
            else
            {
                //GLOG 2165
                aDef = new string[14];

                //populate the non-persistent indices
                aDef[0] = this.IsDirty.ToString();
                aDef[1] = this.SegmentName;
                aDef[2] = this.TagID;
                aDef[3] = this.TagParentPartNumber;
                iOffset = 4;
            }

            aDef[iOffset] = this.Name;
            aDef[iOffset + 1] = this.DisplayName;
            aDef[iOffset + 2] = this.TranslationID.ToString();
            aDef[iOffset + 3] = this.ShowInTree.ToString();
            aDef[iOffset + 4] = this.IsBody.ToString();
            aDef[iOffset + 5] = this.HelpText;
            aDef[iOffset + 6] = this.Hotkey;
            aDef[iOffset + 7] = ((int)this.DisplayLevel).ToString();
            //GLOG 2165: New property
            aDef[iOffset + 8] = this.StartingText;
            //10.2
            aDef[iOffset + 9] = this.ObjectDatabaseID.ToString();
            return aDef;
        }
        /// <summary>
        /// returns the block as a delimited string -
        /// includes both persistent and non-persistent values
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Join("|", this.ToArray());
        }
        /// <summary>
        /// returns the block as a delimited string - 
        /// includes persistent values only if specified
        /// </summary>
        /// <param name="bPersistentValuesOnly"></param>
        /// <returns></returns>
        public string ToString(bool bPersistentValuesOnly)
        {
            return string.Join("|", this.ToArray(bPersistentValuesOnly));
        }
        #endregion
    }

	/// <summary>
	/// contains the methods and properties that manage
	/// a collection of MacPac blocks
	/// </summary>
    public abstract class Blocks
    {
    }
}
