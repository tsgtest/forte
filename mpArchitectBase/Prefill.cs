using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace LMP.Architect.Base
{
    public class Prefill
    {
        #region *********************fields*********************
        protected NameValueCollection m_oValues = new NameValueCollection();
        protected List<string[]> m_aAuthors = null;
        public const char mpPrefillFieldDelimiter = LMP.StringArray.mpEndOfElement;
        public const char mpPrefillValueDelimiter = LMP.StringArray.mpEndOfRecord;
        public const char mpPrefillAuthorsSeparator = '|';
        public const char mpPrefillAuthorsValueSeparator = LMP.StringArray.mpEndOfValue;
        protected string m_xName = "";
        protected string m_xSegmentID = "";
        protected LMP.Data.mpObjectTypes m_iSegmentType = 0;
        protected string m_xVarSetID = "";
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// gets/sets the name of the Prefill
        /// </summary>
        public string Name
        {
            get { return m_xName; }
            set { m_xName = value; }
        }
        /// <summary>
        /// returns the value of the element with the specified name
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public string this[string xName]
        {
            get{return m_oValues[xName];}
        }
        /// <summary>
        /// returns the ID of Segment associated with VariableSet or ContentString
        /// </summary>
        public string SegmentID
        {
            //GLOG 3475
            get { return m_xSegmentID; }
            set { m_xSegmentID = value; }
        }
        /// <summary>
        /// returns the ID of the VariableSet or ContentString
        /// </summary>
        public string VariableSetID
        {
            //GLOG 3475
            get { return m_xVarSetID; }
            set { m_xVarSetID = value; }
        }
        /// <summary>
        /// Return ObjectType of source Segment
        /// </summary>
        public LMP.Data.mpObjectTypes SegmentType
        {
            get
            {
                if (m_iSegmentType == 0)
                {
                    //Get Segment Type from Segment ID if not previously set
                    if (m_xSegmentID != "")
                    {
                        if (!m_xSegmentID.Contains(".") || m_xSegmentID.EndsWith(".0"))
                        {
                            //AdminSegment
                            //GLOG 4834: Don't use Trim or TrimEnd to remove ".0", because it will
                            //also remove any other zeros from start or end of ID
                            string xAdminID = m_xSegmentID.Replace(".0", "");
                            int iAdminID = Int32.Parse(xAdminID);
                            LMP.Data.AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs();
                            try
                            {
                                LMP.Data.AdminSegmentDef oDef = (LMP.Data.AdminSegmentDef)oDefs.ItemFromID(iAdminID);
                                m_iSegmentType = oDef.TypeID;
                            }
                            catch
                            {
                                //No match found, return Generic Architect type
                                m_iSegmentType = LMP.Data.mpObjectTypes.Architect;
                            }
                        }
                        else
                        {
                            //UserSegment
                            m_iSegmentType = LMP.Data.mpObjectTypes.UserSegment;
                        }
                    }
                    else
                        //return Generic Architect type
                        m_iSegmentType = LMP.Data.mpObjectTypes.Architect;
                }
                return m_iSegmentType;
            }
        }
        /// <summary>
        /// returns the value of the element with the specified index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public string this[int iIndex]
        {
            get {return m_oValues[iIndex];}
        }

        public string ItemName(int iIndex)
        {
            return m_oValues.GetKey(iIndex);
        }
        /// <summary>
        /// returns the number of elements in the prefill
        /// </summary>
        public int Count
        {
            get {return m_oValues.Count;}
        }
        /// <summary>
        /// Returns List of Author string arrays
        /// containing ID, LeadAuthor, FullName and BarID
        /// </summary>
        /// <returns></returns>
        public List<string[]> Authors
        {
            get
            {
                if (m_aAuthors == null)
                {
                    m_aAuthors = new List<string[]>();
                    string xAuthors = m_oValues["Authors"];

                    //GLOG 6113 (dm) - snapshot prefill prepends the segment name and
                    //ancestry, except for the top-level, to all keys, including Authors -
                    //try qualified key name
                    if (string.IsNullOrEmpty(xAuthors))
                    {
                        string xPrefill = this.ToString();
                        int iPos = xPrefill.IndexOf(".Authors" + mpPrefillValueDelimiter);
                        if (iPos > -1)
                        {
                            int iPos2 = xPrefill.LastIndexOf(mpPrefillFieldDelimiter,
                                iPos) + 1;
                            xAuthors = m_oValues[xPrefill.Substring(iPos2,
                                (iPos - iPos2)) + ".Authors"];
                        }
                    }

                    if (!string.IsNullOrEmpty(xAuthors))
                    {
                        string[] aAuthors = xAuthors.Split(mpPrefillAuthorsSeparator);
                        for (int i = 0; i <= aAuthors.GetUpperBound(0); i++)
                        {
                            if (aAuthors[i] != "")
                            {
                                string[] aValues;
                                //Prefill might use old format with only Author ID
                                if (aAuthors[i].IndexOf(mpPrefillAuthorsValueSeparator) > -1)
                                {
                                    aValues = aAuthors[i].Split(mpPrefillAuthorsValueSeparator);
                                }
                                else
                                {
                                    aValues = new string[] { aAuthors[i] };
                                }
                                m_aAuthors.Add(aValues);
                            }
                        }
                    }
                    else
                    {

                    }
                }
                return m_aAuthors;
            }
        }
        public bool ContainsUpdatableContacts
        {
            get
            {
                string xContent = this.ToString();
                return xContent.Contains("UNID=");
            }
        }
        /// <summary>
        /// Returns Author information in AuthorSelector XML format
        /// </summary>
        /// <param name="iMaxAuthors"></param>
        /// <returns></returns>
        public string AuthorsXML(int iMaxAuthors)
        {
            string xAuthorXML = "";
            for (int i = 0; i < this.Authors.Count && i < iMaxAuthors; i++)
            {
                string xID = "";
                string xIsLead = "";
                string xFullName = "";
                string xBarID = "";
                string[] aAuthors = this.Authors[i];
                xID = aAuthors[0];
                try
                {
                    xIsLead = aAuthors[1];
                    xFullName = aAuthors[2];
                    xBarID = aAuthors[3];
                }
                catch { }
                //GLOG 3719: Needed to replace xID with xIsLead for correct evaluation of LeadAuthor
                //GLOG 5034: Replace any reserved XML characters in Author Name or Bar ID
                xAuthorXML += string.Concat("<Author Lead=",
                    xIsLead == "-1" ? "\"-1\"" : "\"0\"", " ID=\"",
                    xID, "\" FullName=\"", LMP.String.ReplaceXMLChars(xFullName, true), "\" BarID=\"", LMP.String.ReplaceXMLChars(xBarID, true), "\"></Author>"); //GLOG 6065
            }
            return xAuthorXML;
        }

        #endregion
        #region *********************methods********************
        /// <summary>
        /// sets the value of the specified element to the specified value
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        public void Set(string xName, string xValue)
        {
            m_oValues.Set(xName, xValue);
        }
        /// <summary>
        /// deletes the element with the specified name
        /// </summary>
        /// <param name="xName"></param>
        public void Delete(string xName)
        {
            m_oValues.Remove(xName);
        }
        /// <summary>
        /// returns the prefill as a delimited string of name/value pairs
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder oSB = new StringBuilder();

            //cycle through all prefill elements,
            //adding each to the string
            for (int i = 0; i < m_oValues.Count; i++)
                oSB.AppendFormat("{0}" + mpPrefillValueDelimiter.ToString() + 
                    "{1}" + mpPrefillFieldDelimiter.ToString(), 
                    m_oValues.Keys[i], m_oValues[i]);

            if (oSB.Length > 0)
            {
                // Remove final delimiter
                oSB.Remove(oSB.Length - 1, 1);
            }

            return oSB.ToString();
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// Populate name/value pairs by parsing input string
        /// </summary>
        /// <param name="xContentString"></param>
        protected void LoadValues(string xContentString)
        {
            string [] aVars = xContentString.Split(mpPrefillFieldDelimiter);

            //cycle through all name/value pairs in the content string
            for (int i = 0; i < aVars.Length; i++)
            {
                if (aVars[i] != "")
                {
                    string[] aVals = aVars[i].Split(mpPrefillValueDelimiter);

                    //get name
                    string xProposedName = aVals[0];
                    int j = 0;

                    //index name if an item with that
                    //name already exists
                    while (this[xProposedName] != null)
                    {
                        //increment index
                        j++;
                        int iPos = aVals[0].LastIndexOf('.');

                        //build new name
                        xProposedName = string.Concat(aVals[0].Substring(0, iPos + 1),
                            j.ToString(), ".", aVals[0].Substring(iPos + 1));
                    }

                    this.Set(xProposedName, aVals[1]);
                }
            }
        }
        protected void CreatePrefill(string xContentString, string xName, string xSegmentID, string xVariableSetID)
        {
            string xPwd = LMP.Data.Application.EncryptionPassword;
            xContentString = LMP.String.Decrypt(xContentString, ref xPwd);
            this.Name = xName;
            this.VariableSetID = xVariableSetID;
            //GLOG 3475: Add Admin ID2 if not included
            //GLOG 8971
            if (!string.IsNullOrEmpty(xSegmentID) && !xSegmentID.Contains("."))
                xSegmentID = xSegmentID + ".0";
            this.SegmentID = xSegmentID;
            if (!string.IsNullOrEmpty(xContentString))
                LoadValues(xContentString);
        }
        #endregion
    }
}
