using System;
using System.Collections;
using System.Collections.Specialized;
using LMP.Data;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace LMP.Architect.Base
{
    /// <summary>
    /// contains properties that define the arguments for the AuthorAdded event
    /// </summary>
    public class AuthorAddedEventArgs
    {
        private short m_shNewIndex = 0;

        public AuthorAddedEventArgs(short shNewIndex)
        {
            m_shNewIndex = shNewIndex;
        }

        public short NewIndex
        {
            get { return m_shNewIndex; }
            set { m_shNewIndex = value; }
        }
    }

	/// <summary>
	/// contains properties that define the arguments for the LeadAuthorChanged event
	/// </summary>
	public class LeadAuthorChangedEventArgs
	{
		private string m_xNewLeadAuthorID = null;

		public LeadAuthorChangedEventArgs(string xNewLeadAuthorID)
		{
			m_xNewLeadAuthorID = xNewLeadAuthorID;
		}

		public string NewLeadAuthorID
		{
			get{return m_xNewLeadAuthorID;}
			set{m_xNewLeadAuthorID = value;}
		}
	}

	/// <summary>
	/// contains properties that define the 
	/// arguments for the AuthorsReindexedEventArgs event - none
	/// </summary>
	public class AuthorsReindexedEventArgs{}
		

	/// <summary>
	/// declares the delegate for an LeadAuthorChanged event handler
	/// </summary>
	public delegate void LeadAuthorChangedHandler(object AuthorsCollection, 
		LeadAuthorChangedEventArgs oArgs);

	/// <summary>
	/// declares the delegate for an AuthorAdded event handler
	/// </summary>
	public delegate void AuthorAddedHandler(object AuthorsCollection, 
		AuthorAddedEventArgs oArgs);

	/// <summary>
	/// declares the delegate for an AuthorAdded event handler
	/// </summary>
	public delegate void AuthorsReindexedHandler(object AuthorsCollection, 
		AuthorsReindexedEventArgs oArgs);
	
	/// <summary>
	/// contains the methods and properties that define 
	/// a collection of MacPac Authors -
	/// created by Daniel Fisherman, 10/04
	/// </summary>
	public abstract class Authors
	{
		protected Author m_oLead = null;

		//we define both a hashtable and an ArrayList because
		//none of the .NET collections allow us to call by
		//key and index, as well as allow us to move authors
		//by index - the best collection is the NameObjectCollectionBase, 
		//but there was no way to move authors around using this class
		protected Hashtable m_oHashtable = new Hashtable();
		protected ArrayList m_oAuthorIndexArray = new ArrayList();
		#region *********************properties*********************
		public int Count
		{
			get
			{
				return m_oAuthorIndexArray.Count;
			}
		}

		protected Hashtable InternalHashtable
		{
			get{return m_oHashtable;}
			set{m_oHashtable = value;}
		}

		protected ArrayList InternalArray
		{
			get{return m_oAuthorIndexArray;}
			set{m_oAuthorIndexArray = value;}
		}

		#endregion
		#region *********************methods*********************
        protected abstract void RaiseAuthorAddedEventIfNecessary();
        protected abstract void RaiseAuthorsReindexedEvent();
        protected abstract void RaiseAuthorsReindexedEventIfNecessary();
        protected abstract void RaiseLeadAuthorChangedEventIfNecessary(Author oNewLeadAuthor);

        //GLOG item #8027 - switched first param from Person to string - dcf
        protected abstract Person RaisePromptToCopyPublicPersonIfNecessary(string xPersonID, LocalPersons oLocalPersons, bool bIsLegacy);

        /// <summary>
        /// adds a person to the collection of Authors. Raises
        ///an error if the person's IsAuthor property is False
        /// </summary>
        /// <param name="oAuthor">Person to add to author collection</param>
        public void Add(Person oPerson)
        {
            this.Add(oPerson, "");
        }
        public void Add(Person oPerson, string xBarID)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("oPerson.ID", oPerson.ID);

            if (!oPerson.IsAuthor)
                //person is not an author - can't add to collection - alert
                throw new LMP.Exceptions.ActionException(
                    string.Concat(LMP.Resources.GetLangString("Error_CouldNotAddAuthor"),
                    "(", oPerson.ID, ")"));

            Author oAuthor = new Author(oPerson);
            oAuthor.BarID = xBarID;
            try
            {
                m_oHashtable.Add(oAuthor.ID, oAuthor);
                m_oAuthorIndexArray.Add(oAuthor);
            }
            catch (System.ArgumentException)
            {
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.AlreadyInCollectionException(
                    LMP.Resources.GetLangString("Error_AuthorAlreadyAdded"), oE);
            }

            if (this.Count == 1)
            {
                //this is the only author in the 
                //collection - set as the lead author
                m_oLead = oAuthor;

                //TODO: deal with this line - currently a holdover from pre-IBF Forte versionm
                //this.Segment.Status = this.Parent.Status | LMP.Architect.Api.mpObjectStatus.AuthorsSet;
            }

            RaiseAuthorAddedEventIfNecessary();

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// removes the author at the specified index in the collection
        /// </summary>
        /// <param name="shIndex">index to remove</param>
        public void Remove(short shIndex)
        {
            Trace.WriteNameValuePairs("shIndex", shIndex);

            //internal collection is 0 based, but this class is 1-based - adjust index
            shIndex--;

            if (this.Count == 1)
                //can't delete last author
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CantDeleteSoleAuthor"));

            //get author
            Author oAuthor = (Author)m_oAuthorIndexArray[shIndex];

            //get if author to delete is the lead author
            bool bIsLead = m_oLead.Equals(oAuthor);

            m_oAuthorIndexArray.RemoveAt(shIndex);
            m_oHashtable.Remove(oAuthor.ID);

            //notify subscribers that authors have been reindexed
            RaiseAuthorsReindexedEvent();

            if (bIsLead)
            {
                //set new lead author to first author in collection
                m_oLead = (Author)m_oAuthorIndexArray[0];

                //notify subscribers that lead author has been changed
                RaiseLeadAuthorChangedEventIfNecessary(m_oLead);
            }
        }

        /// <summary>
        /// moves the author at the specified index to the position
        ///in the collection specified by the destination index
        /// </summary>
        /// <param name="shAuthorIndex"></param>
        /// <param name="shDestinationIndex"></param>
        public void Move(short shAuthorIndex, short shDestinationIndex)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("shAuthorIndex", shAuthorIndex,
                "shDestinationIndex", shDestinationIndex);

            if (shAuthorIndex == shDestinationIndex)
                //nothing to move - exit
                return;

            //internal collection is 0-based, class is 1-based - adjust
            shAuthorIndex--;
            shDestinationIndex--;

            //get the person to be moved
            Author oAuthor = (Author)m_oAuthorIndexArray[shAuthorIndex];

            //remove from collection
            m_oAuthorIndexArray.RemoveAt(shAuthorIndex);

            //add to new index in collection
            if (shAuthorIndex > shDestinationIndex)
                m_oAuthorIndexArray.Insert(shDestinationIndex, oAuthor);
            else
                m_oAuthorIndexArray.Insert(shDestinationIndex, oAuthor);

            RaiseAuthorsReindexedEvent();

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns the author with the specified index in the collection
        /// </summary>
        /// <param name="shIndex"></param>
        /// <returns></returns>
        public Author ItemFromIndex(short shIndex)
        {
            Trace.WriteNameValuePairs("shIndex", shIndex);

            return (Author)m_oAuthorIndexArray[shIndex - 1];
        }

        /// <summary>
        /// returns the author with the specified ID
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public Author ItemFromID(string xID)
        {
            Trace.WriteNameValuePairs("xID", xID);

            if (xID.IndexOf('.') == -1)
                //only ID1 was passed in - add ID2
                xID += ".0";

            Author oAuthor = null;
            try
            {
                oAuthor = (Author)m_oHashtable[xID];
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xID, oE);
            }

            return oAuthor;
        }

        /// <summary>
		/// returns the lead author of the collection
		/// </summary>
		/// <returns></returns>
		public Author GetLeadAuthor()
		{
			if(m_oLead == null)
				//set first author in the collection as the lead author
				if(m_oAuthorIndexArray.Count > 0)
					m_oLead = (Author)m_oAuthorIndexArray[0];

			if(m_oLead == null)
				Trace.WriteInfo("m_oLead is nothing=True");
			else
				Trace.WriteNameValuePairs("m_oLead.ID",m_oLead.ID);

			return m_oLead;
		}

        /// <summary>
        /// sets oAuthor as the lead author.  if oAuthor is not in authors
        /// collection, adds oAuthor and deletes current lead author
        /// </summary>
        /// <param name="oPerson"></param>
        public void SetLeadAuthor(Author oAuthor)
        {
            if (oAuthor == null)
                return;

            Trace.WriteNameValuePairs("oAuthor.ID", oAuthor.ID);

            //get author with specified ID
            Author oNewAuthor = null;
            try
            {
                oNewAuthor = (Author)m_oHashtable[oAuthor.ID];
            }
            catch { }

            if (oNewAuthor == null)
            {
                //add person to author collection - we're not using
                //this.Add because this would fire AuthorAdded event
                oNewAuthor = oAuthor;
                m_oHashtable.Add(oNewAuthor.ID, oNewAuthor);
                m_oAuthorIndexArray.Add(oNewAuthor);

                //remove current lead author from collection
                if (m_oLead != null)
                {
                    m_oHashtable.Remove(m_oLead.ID);
                    m_oAuthorIndexArray.Remove(m_oLead);
                }
            }

            if (m_oLead == null)
            {
                //set new lead author
                m_oLead = oNewAuthor;
                //notify subscribers that lead has changed
                RaiseLeadAuthorChangedEventIfNecessary(m_oLead);
            }
            else if (m_oLead.ID != oNewAuthor.ID)
            {
                //set new lead author
                m_oLead = oNewAuthor;

                //notify subscribers that lead has changed
                RaiseLeadAuthorChangedEventIfNecessary(m_oLead);
            }
        }

        /// <summary>
        /// sets the author at the specified ID as the lead author
        /// </summary>
        /// <param name="shIndex"></param>
        public void SetLeadAuthor(short shIndex)
        {
            Trace.WriteNameValuePairs("shIndex", shIndex);

            //adjust for 0-based array
            shIndex--;

            if ((shIndex > this.InternalArray.Count - 1) || shIndex < 0)
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") +
                    shIndex.ToString());

            //get author at specified index
            Author oAuthor = (Author)this.InternalArray[shIndex];

            //check if author at index is the lead author
            if (!oAuthor.Equals(m_oLead))
            {
                //author is not lead author - 
                //set author as lead author
                m_oLead = oAuthor;

                //notify subscribers that lead has changed
                RaiseLeadAuthorChangedEventIfNecessary(m_oLead);
            }
        }

        /// <summary>
        /// sets this authors collection to the specified set of authors
        /// </summary>
        /// <param name="oAuthors"></param>
        public void SetAuthors(Authors oAuthors)
        {
            m_oLead = null;

            this.InternalArray = (ArrayList)oAuthors.InternalArray.Clone();
            this.InternalHashtable = (Hashtable)oAuthors.InternalHashtable.Clone();
            this.SetLeadAuthor(oAuthors.GetLeadAuthor());
        }

        /// <summary>
        /// sets this authors collection to the specified set of people
        /// </summary>
        /// <param name="oPeople"></param>
        public void SetAuthors(LocalPersons oPeople)
        {
            this.InternalArray.Clear();
            this.InternalHashtable.Clear();

            if (oPeople != null)
            {
                //add each person in the collection to 
                //the collection of Authors
                for (int i = 1; i <= oPeople.Count; i++)
                {
                    this.Add(oPeople.ItemFromIndex(i));
                }
            }
        }

        /// <summary>
        /// removes all authors from collection
        /// </summary>
        public void RemoveAll()
        {
            m_oHashtable.Clear();
            m_oAuthorIndexArray.Clear();
        }

        /// <summary>
        /// returns a delimited string of the values of the specified property for all authors
        /// </summary>
        /// <returns></returns>
        public string ToString(string xPropertyName, string xDelimiter)
        {
            string xPropString = null;

            //get mpData assembly
            Assembly oAss = Assembly.LoadFrom(
                LMP.Data.Application.AppDirectory + @"\mpData.dll");

            //get info for specified property
            PropertyInfo oPropInfo =
                oAss.GetType("LMP.Data.Person").GetProperty(xPropertyName);

            //cycle through authors, adding value 
            //of specified property to delimited string
            foreach (Author oAuthor in this.InternalArray)
            {
                Person oPerson = oAuthor.PersonObject;
                xPropString += string.Concat(oPropInfo.GetValue(oPerson, null), xDelimiter);
            }

            //trim trailing delimiter
            xPropString = xPropString.Substring(0, xPropString.Length - xDelimiter.Length);
            return xPropString;
        }

        public override string ToString()
        {
            return this.XML;
        }

        /// <summary>
        /// gets/sets an XML string representing this collection
        /// </summary>
        /// <returns></returns>
        public string XML
        {
            get
            {
                string xAuthorXML = null;
                string xLeadAuthorID = null;

                if (this.Count > 0)
                    xLeadAuthorID = this.GetLeadAuthor().ID;

                //cycle through authors, creating XML elements
                //with attributes for display name and id
                foreach (Author oAuthor in this.InternalArray)
                {
                    //GLOG 5034: Replace any reserved XML characters in Author Name or Bar ID
                    //GLOG 6065: Also replace double quotes
                    xAuthorXML += string.Concat("<Author Lead=",
                        oAuthor.ID == xLeadAuthorID ? "\"-1\"" : "\"0\"", " ID=\"",
                        oAuthor.ID, "\" FullName=\"", LMP.String.ReplaceXMLChars(oAuthor.FullName, true), "\" BarID=\"", LMP.String.ReplaceXMLChars(oAuthor.BarID, true), "\"></Author>");
                }

                return string.Concat("<Authors>", xAuthorXML, "</Authors>");
            }

            set
            {
                DateTime t0 = DateTime.Now;

                XmlNodeList oNodes = null;
                XmlDocument oXML = new XmlDocument();
                //GLOG 4413: Make sure LocalPersons collection includes Active Office records for Favorite people
                LocalPersons oPersons = new LocalPersons(mpPeopleListTypes.AllPeople,
                    LMP.Data.Application.User.ID, mpTriState.Undefined, mpTriState.True, 0, "",
                    UsageStates.OfficeActive);

                //clear out internal storage
                this.InternalArray = new ArrayList();
                this.InternalHashtable = new Hashtable();

                string xXML = value;

                string xLeadAuthorID = "";
                if (m_oLead != null)
                    xLeadAuthorID = m_oLead.ID;

                if (string.IsNullOrEmpty(value) || value.Substring(0, 9).ToUpper() != "<AUTHORS>")
                    //<Authors> document element is missing - add it
                    xXML = "<Authors>" + xXML + "</Authors>";

                //load the XML string into the parser - create a document element
                oXML.LoadXml(xXML);

                //get all author nodes in XML
                oNodes = oXML.DocumentElement.SelectNodes("child::Author");

                short shIndex = 0;
                //DialogResult iImportAuthors = DialogResult.None; //GLOG 8198

                //add author for each author xml node
                foreach (XmlNode oNode in oNodes)
                {
                    string xFullName = oNode.Attributes.GetNamedItem("FullName").Value;

                    //legacy document authors have IDs and no full name
                    bool bIsLegacyAuthor = (oNode.Attributes.GetNamedItem("ID").Value != "") && (xFullName == "");

                    //get person
                    LMP.Data.Person oPerson = null;
                    string xPersonID = oNode.Attributes.GetNamedItem("ID").Value;
                    try
                    {
                        oPerson = oPersons.ItemFromID(xPersonID);
                    }
                    catch { }

                    if (oPerson == null && !string.IsNullOrEmpty(xPersonID) && !xPersonID.StartsWith("-")
                        && (xPersonID.EndsWith(".0") || !xPersonID.Contains(".")))
                    {
                        //GLOG 8198: Make sure only ID1 part is passed to function
                        xPersonID = xPersonID.Replace(".0", "");
                        //Author is public person not in current local list
                        //GLOG item #8027 - switched first param from Person to string - dcf
                        oPerson = RaisePromptToCopyPublicPersonIfNecessary(xPersonID, oPersons, bIsLegacyAuthor);

                    }

                    Author oAuthor = null;
                    if (oPerson != null)
                    {
                        //make the person the author
                        oAuthor = new Author(oPerson);
                        try
                        {
                            oAuthor.BarID = oNode.Attributes.GetNamedItem("BarID").Value;
                        }
                        catch { }
                    }
                    else
                    {
                        //could not get a person for this author -
                        //set current user as author if FUllName isn't available
                        oAuthor = new Author();
                        oAuthor.ID = oNode.Attributes.GetNamedItem("ID").Value;
                        oAuthor.FullName = oNode.Attributes.GetNamedItem("FullName").Value;
                        try
                        {
                            oAuthor.BarID = oNode.Attributes.GetNamedItem("BarID").Value;
                        }
                        catch { }

                        if (oAuthor.ID != "" && oAuthor.FullName == "")
                        {
                            //this data came from the doc analyzer - set current user as author
                            oAuthor = new Author(LMP.Data.Application.User.PersonObject);
                        }
                    }
                    //add to internal storage structures
                    this.InternalArray.Add(oAuthor);
                    this.InternalHashtable.Add(oAuthor.ID, oAuthor);

                    shIndex++;

                    if ((oNode.Attributes.GetNamedItem("Lead").Value == "-1"))
                    {
                        //set as lead author
                        //SetLeadAuthor(oAuthor);
                        m_oLead = oAuthor;
                    }
                }

                RaiseAuthorsReindexedEventIfNecessary();

                LMP.Benchmarks.Print(t0);
            }
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Refresh the authors people with the current information.
        /// </summary>
        public void Refresh()
        {
            // Setting the xml reloads people and updates each author.
            this.XML = this.XML;
        }
        #endregion
    }
}
