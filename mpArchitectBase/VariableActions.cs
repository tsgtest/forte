using System;
using System.Collections;
using System.Reflection;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using LMP.Data;

namespace LMP.Architect.Base
{
    /// <summary>
    /// declares the delegate for a SegmentRefreshedByAction event handler
    /// </summary>
    public delegate void SegmentRefreshedByActionHandler(object sender, SegmentEventArgs oArgs);
    public delegate void SegmentDistributedSectionsHandler(object sender, SegmentEventArgs oArgs);

	public class VariableAction:ActionBase
	{
    }
	
	/// <summary>
	/// Summary description for VariableActions.
	/// </summary>
	public class VariableActions: ActionsCollectionBase
	{
	}
}
