using System;
using System.Collections;
using System.Text;

namespace LMP.Architect.Base
{
	/// <summary>
	/// contains the members that define a SegmentAction -
	/// an action that is executed when a segment event occurs - 
	/// created by Daniel Fisherman 10/06/05
	/// </summary>
	public class SegmentAction: ActionBase
	{
    }

	public class SegmentActions: ActionsCollectionBase
	{
    }
}
