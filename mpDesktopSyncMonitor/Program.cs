using System;
using System.Threading;
using System.Diagnostics;
using Microsoft.Win32;


namespace mpDesktopSyncMonitor
{
    public class Program
    {
        private static string m_xProcess;
        private static int m_iSleepTime = 1800000; //GLOG 5993
        private static int m_iCounter = 0;
        private const string mpDesktopSyncProcessName = "fDesktopSyncProcess";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //GLOG 4863: Exit if there is already an instance of the monitor running in this session
            Process oProcess = Process.GetCurrentProcess();
            int iSession = oProcess.SessionId;
            Process[] aProcesses = Process.GetProcessesByName("fDesktopSyncMonitor");
            for (int i = 0; i < aProcesses.Length; i++)
            {
                if ((aProcesses[i].Id != oProcess.Id) && (aProcesses[i].SessionId == iSession))
                    return;
            }

            Program oProgram = new Program();
            oProgram.Start();
        }

        public void Start()
        {
            RegistryKey oKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\The Sackett Group\Deca");
            
            //GLOG 6505:  Do nothing if MacPac Registry key doesn't exist
            if (oKey == null)
                //GLOG : 8666 : jsw
                {
                    oKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64);
                    oKey = oKey.OpenSubKey(@"SOFTWARE\The Sackett Group\Deca");
                }

            if (oKey == null)
                return;

            string xRootDirectory = oKey.GetValue("RootDirectory").ToString();
            oKey.Close();
            xRootDirectory = EvaluateEnvironmentVariables(xRootDirectory);

            //GLOG 5993: Interval configurable in Registry - specify as number of minutes
            oKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\The Sackett Group\Deca\Sync");

            string xInterval = "";
            //GLOG 6505: Sync subkey may not exist - avoid unhandled exception
            if (oKey != null)
            {
                try
                {
                    xInterval = oKey.GetValue("MonitorInterval").ToString();
                }
                catch { }
                finally
                {
                    oKey.Close();
                }
            }
            if (!string.IsNullOrEmpty(xInterval))
            {
                try
                {
                    int iInterval = Int32.Parse(xInterval);
                    //Convert to milliseconds
                    m_iSleepTime = iInterval * 60000;
                    //minimum 1 minute interval
                    if (m_iSleepTime < 1800000)
                        m_iSleepTime = 1800000;
                }
                catch
                {
                    m_iSleepTime = 1800000;
                }
            }
            m_xProcess = xRootDirectory + @"\bin\fDesktopSyncProcess.exe";
            ThreadStart oStart = new ThreadStart(LaunchSyncProcess);
            Thread oThread = new Thread(oStart);
            oThread.Start();
        }

        /// <summary>
        /// returns the specified string after evaluating
        /// its environment variables
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        private string EvaluateEnvironmentVariables(string xText)
        {

            //evaluate path for environment variables
            int iPos = xText.IndexOf('%');
            while (iPos > -1)
            {
                int iPos2 = xText.IndexOf('%', iPos + 1);

                string xEnvVar = xText.Substring(iPos + 1, iPos2 - iPos - 1);

                //evaluate
                xEnvVar = System.Environment.GetEnvironmentVariable(xEnvVar);

                xText = xText.Substring(0, iPos) + xEnvVar + xText.Substring(iPos2 + 1);

                iPos = xText.IndexOf('%', iPos + 1);
            }

            return xText;
        }

        private void LaunchSyncProcess()
        {
            while (true)
            {
                RegistryKey oKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\The Sackett Group\Deca\Sync");
                string xSyncExecuting = "0";
                //GLOG 6505
                if (oKey != null)
                {
                    try
                    {
                        xSyncExecuting = oKey.GetValue("Executing").ToString();
                    }
                    catch { }
                    finally
                    {
                        oKey.Close();
                    }
                }
                if (xSyncExecuting != "1")
                {
                    bool bIsRunning = false;
                    //GLOG 3505: GetProcessesByName may return processes running in a different
                    //Terminal Server session, so check SessionID of each Process to see
                    //if there is a match for the Current Process's SessionID (will be 0 in non-TS environment)
                    int iSession = System.Diagnostics.Process.GetCurrentProcess().SessionId;
                    System.Diagnostics.Process[] aProcesses = Process.GetProcessesByName(mpDesktopSyncProcessName);
                    //Only run if not currently syncing and Desktop Process is not already running
                    for (int i = 0; i < aProcesses.Length; i++)
                    {
                        if (aProcesses[i].SessionId == iSession)
                        {
                            //Process is already running in this session
                            bIsRunning = true;
                            break;
                        }
                    }
                    if (!bIsRunning)
                    {
                        Process oProc = new Process();
                        oProc.StartInfo.FileName = m_xProcess;
                        oProc.StartInfo.UseShellExecute = false;
                        //sync is not running - launch sync
                        oProc.Start();
                        //System.Diagnostics.Process.Start(m_xProcess);
                    }
                }
                int iSleep = m_iSleepTime;
                if (m_iCounter < 10)
                {
                    //JTS 9/5/12: For first 10 executions, wait 1 minute between,
                    //then every 1/2 hour, or whatever interval specified in registry
                    m_iCounter++;
                    iSleep = 60000;
                    
                }
                Thread.Sleep(iSleep); //GLOG 5993
            }
        }
    }
}