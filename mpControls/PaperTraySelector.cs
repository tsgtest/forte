﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;

namespace LMP.Controls
{
    public class PaperTraySelector: LMP.Controls.ComboBox
    {
        //GLOG 4752
        static string[] m_arTrays = null;
        static int[] m_arIDs = null;
        static string m_xPrinterName = "";
        public PaperTraySelector()
        {
        }

        public override string Value
        {
            get
            {
                //GLOG 4752
                if (m_arIDs != null)
                    return m_arIDs[Math.Max(this.SelectedIndex, 0)].ToString();
                else
                    return null;
            }
            set
            {
                base.Value = value;
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // PaperTraySelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Name = "PaperTraySelector";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public override void ExecuteFinalSetup()
        {
            //GLOG 4752: Use Base wrapper function to retrieve Bin information
            string xCurPrinter = "";
            try
            {
                xCurPrinter = LMP.Forte.MSWord.Application.GetActivePrinterName();
            }
            catch { }

            if (string.IsNullOrEmpty(xCurPrinter) ||  m_xPrinterName != xCurPrinter)
            {
                LMP.Forte.MSWord.Application.GetPrinterBinNamesAndIDs(ref m_arTrays, ref m_arIDs);
                m_xPrinterName = xCurPrinter;
            }

            if (m_arTrays != null)
            {
                //setup the paper tray combo
                string[,] aDetail = new string[m_arTrays.Length, 2];
                for (int i = 0; i < m_arTrays.Length; i++)
                {
                    aDetail[i, 0] = m_arTrays[i];
                    aDetail[i, 1] = m_arIDs[i].ToString();
                }

                this.SetList(aDetail);

                if (m_arTrays.GetUpperBound(0) >= 0)
                {
                    this.SelectedIndex = 0;
                }
            }
            base.ExecuteFinalSetup();
        }
    }
}
