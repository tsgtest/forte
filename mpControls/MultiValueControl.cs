using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Text;
using LMP.Data;
using LMP.Controls;
using System.Xml;
using System.Reflection;

namespace LMP.Controls
{
    /// <summary>
	/// Defines a MacPac MultiValue input control.
	/// Created by Jeffrey Sweetland 09/08
	/// </summary>
	public partial class MultiValueControl : System.Windows.Forms.UserControl, IControl
	{
        #region *********************fields*********************
		private const int mpMaxDetailFields = 1;
		private const int mpRowHeight = 225;
		private const int mpGridLineHeight = 25;

		private bool m_bControlIsLoaded = false;
		private IControl m_oControl = null;
		private bool m_bHidingControl = false;
        private System.Drawing.Color m_oFieldColor = System.Drawing.SystemColors.Window;
		private string [] m_xValueArray = null;
		private bool m_bValueDirty = false;
		private string m_xCounterScheme = "Value %0 of %1";
		private Control m_oPopupSenderCtl = null;
        private bool m_bPopupVisible = false;

		//index of the item in m_oDetailArray that is currently selected
		private int m_iCurValueIndex = -1;
        private int m_iNumValues = 1;
        private int m_iMinIndex = 0;
        private int m_iCounterIndex = 0;

        private bool m_bIsDirty;
        private object m_oTag2;
        private bool m_bShortCutKeyPressed = false;
        private mpMultiValueControlTypes m_iControlType = mpMultiValueControlTypes.TextBox;
        private string m_xList = "";
        private int m_iNumLines;
        private string m_xTrueString = "";
        private string m_xFalseString = "";
        private int m_iMaxDropDownItems = 6;
        private bool m_bAllowEmptyValue = false;
        private string m_xSeparator = "1310";
        private int m_iListRows = 6;
        private bool m_bForceCaps = false;
        private string m_xSupportingValues = "";
        #endregion

        #region *********************enumerations*********************
        public enum mpMultiValueControlTypes : byte
        {
            TextBox = 1,
            ComboBox = 2,
            ListBox = 3,
            MultilineCombo = 4,
            BooleanCombo = 5
        }

        public enum mpMultiValueFieldSelection : byte
        {
            WholeField = 0,
            StartOfField = 1,
            EndOfField = 2
        }
        #endregion

        #region *********************constructors*********************
        public MultiValueControl(LMP.Data.mpControlTypes iControlType)
		{
			InitializeComponent();
            switch (iControlType)
            {
                case mpControlTypes.Textbox:
                case mpControlTypes.MultilineTextbox:
                    this.ControlType = mpMultiValueControlTypes.TextBox;
                    break;
                case mpControlTypes.MultilineCombo:
                    //make base control transparent, so that opening multiline combo list
                    //won't extend visible control
                    base.BackColor = Color.Transparent;
                    this.ControlType = mpMultiValueControlTypes.MultilineCombo;
                    break;
                case mpControlTypes.Combo:
                    this.ControlType = mpMultiValueControlTypes.ComboBox;
                    break;
                case mpControlTypes.DropdownList:
                    this.ControlType = mpMultiValueControlTypes.ListBox;
                    break;
                case mpControlTypes.Checkbox:
                    this.ControlType = mpMultiValueControlTypes.BooleanCombo;
                    break;
                default:
                    break;
            }
        }

		#endregion

		#region *********************properties*********************
        /// <summary>
        /// Get/Set number of values supported
        /// </summary>
        public int NumberOfValues
        {
            get { return m_iNumValues; }
            set 
            { 
                m_iNumValues = value;
                if (m_iNumValues < 1)
                    m_iNumValues = 1;
                if (m_bControlIsLoaded)
                {
                    SetArraySize(m_iNumValues);
                    UpdateMaxEntitiesView();
                    UpdateCount();
                    SetControlSize();
                    this.Refresh();
                }
            }
        }
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets the configuration string defining the sub-controls to be displayed.")]
        public mpMultiValueControlTypes ControlType
        {
            get { return m_iControlType; }
            set { m_iControlType = value; }
        }
        private bool ValueDirty
		{
			get{return m_bValueDirty;}
			set{m_bValueDirty = value;}
		}

        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets the text to display in the Counter panel.")]
        public string CounterScheme
		{
			get{return m_xCounterScheme;}
			set{m_xCounterScheme = value;}
		}
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets the font used in the control.")]
        public override Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
				if(m_oControl != null)
					((Control)m_oControl).Font = value;
			}
		}
        [DescriptionAttribute("Gets/Sets the name of the list that displays in the dropdown portion of the control.")]
        public string ListName
        {
            get { return m_xList; }
            set
            {
                m_xList = value;
                if (!this.DesignMode)
                {
                    if (m_oControl is LMP.Controls.ComboBox)
                        ((ComboBox)m_oControl).ListName = m_xList;
                    else if (m_oControl is LMP.Controls.MultilineCombo)
                        ((MultilineCombo)m_oControl).ListName = m_xList;
                }
            }
        }
        /// <summary>
        /// gets/sets the display text to substitute for "True" in BooleanComboBox
        /// </summary>
        public string TrueString
        {
            get { return m_xTrueString; }
            set 
            { 
                m_xTrueString = value;
                if (m_bControlIsLoaded && m_oControl is BooleanComboBox)
                    ((BooleanComboBox)m_oControl).TrueString = m_xTrueString;
            }
        }

        /// <summary>
        /// gets/sets the display text to substitute for "False" in BooleanComboBox
        /// </summary>
        public string FalseString
        {
            get { return m_xFalseString; }
            set 
            { 
                m_xFalseString = value;
                if (m_bControlIsLoaded && m_oControl is BooleanComboBox)
                    ((BooleanComboBox)m_oControl).FalseString = m_xFalseString;  //GLOG 7736
            }
        }

        /// <summary>
        /// gets/sets the number of lines displayed in MultiLineTextBox control
        /// </summary>
        public int NumberOfLines
        {
            get { return m_iNumLines; }
            set
            {
                m_iNumLines = value;

                if (m_bControlIsLoaded && m_oControl is LMP.Controls.MultilineTextBox)
                {
                    ((LMP.Controls.MultilineTextBox)m_oControl).NumberOfLines = m_iNumLines;
                    SetControlSize();
                }
            }
        }
        /// <summary>
        /// Get/Set Number of Items to display in ComboBox dropdown list
        /// </summary>
        public int MaxDropDownItems
        {
            get { return m_iMaxDropDownItems; }
            set
            {
                m_iMaxDropDownItems = value;
                if (m_bControlIsLoaded && m_oControl is LMP.Controls.ComboBox)
                {
                    ((LMP.Controls.ComboBox)m_oControl).MaxDropDownItems = m_iMaxDropDownItems;
                }
            }

        }
        /// <summary>
        /// Get/Set whether Empty Value will be allowed in Dropdown List control
        /// </summary>
        public bool AllowEmptyValue
        {
            get { return m_bAllowEmptyValue; }
            set
            {
                m_bAllowEmptyValue = value;
                if (m_bControlIsLoaded && m_oControl is LMP.Controls.ComboBox && 
                    ((LMP.Controls.ComboBox)m_oControl).LimitToList)
                {
                    ((LMP.Controls.ComboBox)m_oControl).AllowEmptyValue = m_bAllowEmptyValue;
                }
            }

        }
        /// <summary>
        /// Get/Set Number of Items to display in MultiLineCombo dropdown list
        /// </summary>
        public int ListRows
        {
            get { return m_iListRows; }
            set
            {
                m_iListRows = value;
                if (m_bControlIsLoaded && m_oControl is LMP.Controls.MultilineCombo)
                {
                    ((LMP.Controls.MultilineCombo)m_oControl).ListRows = m_iListRows;
                }
            }
        }
        /// <summary>
        /// Get/Set separator between values in MultiLineCombo
        /// </summary>
        public string Separator
        {
            get { return m_xSeparator; }
            set
            {
                m_xSeparator = value;
                if (m_bControlIsLoaded && m_oControl is LMP.Controls.MultilineCombo)
                {
                    ((LMP.Controls.MultilineCombo)m_oControl).Separator = m_xSeparator;
                }
            }
        }
        /// <summary>
        /// Get/Set whether Uppercase text is forced in MultiLineCombo
        /// </summary>
        public bool ForceCaps
        {
            get { return m_bForceCaps; }
            set
            {
                m_bForceCaps = value;
                if (m_bControlIsLoaded && m_oControl is LMP.Controls.MultilineCombo)
                {
                    ((LMP.Controls.MultilineCombo)m_oControl).ForceCaps = m_bForceCaps;
                }
            }

        }
        #endregion

		#region *********************methods*********************
        /// <summary>
        /// Set text of interface items
        /// </summary>
        private void SetupResourceStrings()
        {
            LMP.Trace.WriteInfo("SetupResourceStrings");
            tbtnPrev.ToolTipText = LMP.Resources.GetLangString("Prompt_PreviousEntry") + " [PgUp]";
            tbtnNext.ToolTipText = LMP.Resources.GetLangString("Prompt_NextEntry") + " [PgDn]";
            tddbStatus.ToolTipText = LMP.Resources.GetLangString("Prompt_CurrentEntry");
            if (!this.DesignMode)
            {
                mnuContextMenu_ClearField.Text = LMP.Resources.GetLangString("Menu_MultiValue_Clear");
                mnuContextMenu_ApplyToAll.Text = LMP.Resources.GetLangString("Menu_MultiValue_ApplyToAll");
            }
        }

		/// <summary>
		/// resets all detail fields
		/// </summary>
		public void ClearValue()
		{
            LMP.Trace.WriteInfo("ClearValue");
            if (m_oControl is BooleanComboBox)
                m_oControl.Value = "False";
            else if (m_oControl != null)
                m_oControl.Value = "";
            this.IsDirty = true;
		}

		#endregion

		#region *********************internal procedures*********************
		/// <summary>
		/// saves the current detail
		/// </summary>
		private void SaveCurValue()
		{
			if(!this.ValueDirty)
				return;

			m_xValueArray[m_iCurValueIndex] = m_oControl.Value;

			this.ValueDirty = false;
            this.IsDirty = true;

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
		}

		/// <summary>
		/// positions/sizes all subcontrols
		/// </summary>
		private void SetupControl()
		{
            LMP.Trace.WriteInfo("SetupControl");

			if(!m_bControlIsLoaded || m_bHidingControl)
				return;

			DateTime t0 = DateTime.Now;

            CreateControl(this.ControlType);
            this.UpdateMaxEntitiesView();

            SetControlSize();

			if(!this.DesignMode)
				LMP.Benchmarks.Print(t0, this.GetType().FullName);
		}

		/// <summary>
		/// creates the specified control
		/// </summary>
		/// <param name="bytType"></param>
		/// <param name="xFieldName"></param>
		/// <param name="xCaption"></param>
		/// <param name="xCIScheme"></param>
		/// <param name="xSuppValue"></param>
		private void CreateControl(mpMultiValueControlTypes bytType)
		{
			DateTime t0 = DateTime.Now;
            Control oCtl = null;
			switch(bytType)
			{
				case mpMultiValueControlTypes.BooleanCombo:
				{
                    oCtl = this.CreateBooleanCombo();
                    break;
				}
				case mpMultiValueControlTypes.ComboBox:
				{
					oCtl = this.CreateCombo(false);
					break;
				}
				case mpMultiValueControlTypes.ListBox:
				{
					oCtl = this.CreateCombo(true);
					break;
				}
				case mpMultiValueControlTypes.TextBox:
				{
					oCtl = this.CreateTextBox();
					break;
				}
				case mpMultiValueControlTypes.MultilineCombo:
				{
					oCtl = this.CreateMultilineCombo();
					break;
				}
				default:
				{
					break;
				}
			}

			//get the index and name that the control will have
			oCtl.Name = "Control1";
            oCtl.Width = pnlFields.Width;
			oCtl.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            oCtl.BackColor = this.BackColor;
            oCtl.ForeColor = this.ForeColor;
            oCtl.Font = this.Font;
            //assign the control to the detail panel
			this.pnlFields.Controls.Add(oCtl);
            oCtl.Top = 0;
            oCtl.TabStop = true;
            oCtl.Visible = true;
            oCtl.BringToFront();
            if (!this.DesignMode)
            {
                oCtl.TabIndex = 0;
            }
            else
            {
                oCtl.Enabled = false;
            }

			m_oControl = (IControl)oCtl;
		
			if(!this.DesignMode)
				LMP.Benchmarks.Print(t0, oCtl.GetType().FullName);
		}

        /// <summary>
        /// Resizes control to accommodate configured subcontrol
        /// </summary>
        private void SetControlSize()
        {
            LMP.Trace.WriteInfo("SetControlSize");
            Control oCtl = (Control)m_oControl;
            int iHeight = oCtl.Height;
            // Resize to accommodate subcontrol
            if (tsActions.Visible)
            {
                this.Height = oCtl.Top + iHeight + tsActions.Height;
                this.pnlFields.Height = this.Height - this.tsActions.Height;
                this.tsActions.Top = this.Height - this.tsActions.Height;
            }
            else
            {
                this.Height = oCtl.Top + iHeight;
                this.pnlFields.Height = this.Height;
            }
        }
        /// <summary>
        /// Creates a MultilineCombo for use as a sub control
        /// </summary>
        /// <param name="xListName"></param>
        /// <returns></returns>
        private LMP.Controls.MultilineCombo CreateMultilineCombo()
		{
			MultilineCombo oMLC = new MultilineCombo(this);
            if (!this.DesignMode)
            {
                oMLC.ListName = this.ListName;
                oMLC.ForceCaps = m_bForceCaps;
                oMLC.ListRows = m_iListRows;
                oMLC.Separator = m_xSeparator;
            }
            oMLC.ListRows = 10;
            oMLC.Height = 30;
			oMLC.ShowBorder = false;
            if (this.DesignMode)
                oMLC.Enabled = false;
			//sink events
			oMLC.TextChanged += new EventHandler(HandleTextChanged);
			oMLC.GotFocus += new EventHandler(HandleGotFocus);
			oMLC.KeyDown += new KeyEventHandler(HandleKeyDown);
			oMLC.MouseDown += new MouseEventHandler(HandleMouseDown);
            oMLC.TabPressed += new TabPressedHandler(HandleTabPressed);
            oMLC.KeyReleased += new KeyEventHandler(HandleKeyReleased);
            oMLC.ListVisibleChanged += new EventHandler(oMLC_ListVisibleChanged);
            return oMLC;
		}
        /// <summary>
        /// Dropdown list of Multiline combo is shown or hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oMLC_ListVisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (((MultilineCombo)sender).Visible)
                    m_bPopupVisible = true;
                else
                    m_bPopupVisible = false;
            }
            catch { }
        }

		/// <summary>
		/// creates a BooleanCombo for use as a sub control
		/// </summary>
		/// <returns></returns>
		private BooleanComboBox CreateBooleanCombo()
		{
			BooleanComboBox oChk = new BooleanComboBox();
            if (!string.IsNullOrEmpty(m_xTrueString))
                oChk.TrueString = m_xTrueString;
            if (!string.IsNullOrEmpty(m_xFalseString))
                oChk.FalseString = m_xFalseString;
            oChk.Borderless = true;
            oChk.ValueChanged += new ValueChangedHandler(HandleTextChanged);
            oChk.GotFocus += new EventHandler(HandleGotFocus);
            oChk.KeyDown += new KeyEventHandler(HandleKeyDown);
            oChk.MouseDown += new MouseEventHandler(HandleMouseDown);
            oChk.TabPressed +=new TabPressedHandler(HandleTabPressed);
            oChk.KeyReleased += new KeyEventHandler(HandleKeyReleased);
            return oChk;
		}

		/// <summary>
		/// creates a combobox for use as a sub control
		/// </summary>
		/// <param name="xListName"></param>
		/// <returns></returns>
		private LMP.Controls.ComboBox CreateCombo(bool bLimitToList)
		{
			LMP.Controls.ComboBox oCombo = new ComboBox();

			oCombo.Borderless = true;
            if (!this.DesignMode)
            {
                oCombo.LimitToList = bLimitToList;
                oCombo.MaxDropDownItems = m_iMaxDropDownItems;
                oCombo.ListName = this.ListName;
                oCombo.Borderless = false;
                if (bLimitToList)
                {
                    oCombo.AllowEmptyValue = m_bAllowEmptyValue;
                }
            }
            else
            {
                oCombo.Enabled = false;
            }
            //oCombo.Value = "";

			//sink events
			oCombo.ValueChanged += new ValueChangedHandler(HandleTextChanged);
			oCombo.GotFocus += new EventHandler(HandleGotFocus);
			oCombo.KeyDown += new KeyEventHandler(HandleKeyDown);
			oCombo.MouseDown += new MouseEventHandler(HandleMouseDown);
            oCombo.TabPressed += new TabPressedHandler(HandleTabPressed);
            oCombo.KeyReleased += new KeyEventHandler(HandleKeyReleased);
			return oCombo;
		}

		/// <summary>
		/// creates a texbox for use as a subControl
		/// </summary>
		/// <param name="bytNumLines"></param>
		/// <returns></returns>
		private LMP.Controls.TextBox CreateTextBox()
		{
            LMP.Controls.TextBox oTB = null;
            if (m_iNumLines > 1)
            {
                oTB = new LMP.Controls.MultilineTextBox();
                ((MultilineTextBox)oTB).NumberOfLines = m_iNumLines;
            }
            else
                oTB = new LMP.Controls.TextBox();

            if (this.DesignMode)
                oTB.Enabled = false;

			//sink events
			oTB.TextChanged += new EventHandler(HandleTextChanged);
			oTB.GotFocus += new EventHandler(HandleGotFocus);
			oTB.KeyDown += new KeyEventHandler(HandleKeyDown);
			oTB.MouseDown += new MouseEventHandler(HandleMouseDown);
            oTB.TabPressed += new TabPressedHandler(HandleTabPressed);
            oTB.KeyReleased += new KeyEventHandler(HandleKeyReleased);
            return oTB;
		}

        void HandleKeyReleased(object sender, KeyEventArgs e)
        {
            //Don't raise event if Ctrl ShortCut was used to prevent toggling hotkey mode
            if (this.KeyReleased != null && !m_bShortCutKeyPressed)
                this.KeyReleased(this, e);
        }

        private void SetupScrollButtons()
        {
            this.tbtnNext.Enabled = m_iCounterIndex < m_iNumValues && m_iNumValues > 1;
            this.tbtnPrev.Enabled = m_iCounterIndex > 1;
        }

        /// <summary>
        /// Tracks current and total number of details, 
        /// replacing {0} and {1} tokens in CounterScheme
        /// </summary>
		private void UpdateCount()
		{
			
            int iIndex = 0;
			int iTotal = 0;
			string xLabel = "";

			if(m_iCurValueIndex > -1)
			{
				//there's a selected item
				iIndex = m_iCurValueIndex + 1;
				iTotal = m_iNumValues;
			}
			else
			{
				//no selected item
				iTotal = m_iNumValues;
			}

			Trace.WriteNameValuePairs("iIndex", iIndex, "iTotal", iTotal);

			//set count and scroll bar position
			if(iIndex > 0 && iTotal > 0)
			{
                xLabel = this.CounterScheme;
                xLabel = xLabel.Replace("%0", iIndex.ToString());
                xLabel = xLabel.Replace("%1", iTotal.ToString());
			}
			else
				xLabel = "";
			this.tddbStatus.Text = "" + xLabel;
            SetupScrollButtons();
            this.tddbStatus.ShowDropDownArrow = (iTotal > 1);
		}
        /// <summary>
        /// Change size of value array
        /// </summary>
        /// <param name="iValues"></param>
        private void SetArraySize(int iValues)
        {
            if (m_xValueArray == null)
            {
                m_xValueArray = new string[iValues];
                for (int i = 0; i < iValues; i++)
                    m_xValueArray[i] = "";
            }
            else if (m_xValueArray.GetLength(0) != iValues)
            {
                //Create new array of correct size and copy any existing elements
                string[] xTempArray = new string[iValues];
                for (int i = 0; i < iValues; i++)
                {
                    if (i < m_xValueArray.GetLength(0))
                        xTempArray[i] = m_xValueArray[i];
                    else
                        xTempArray[i] = "";
                }
                m_xValueArray = xTempArray;
            }

        }
		/// <summary>
		/// sets values of subcontrols based on the current detail
		/// </summary>
		private void FillValue()
		{
			LMP.Trace.WriteNameValuePairs("m_iCurDetailIndex", m_iCurValueIndex);

            if (m_xValueArray != null)
            {
                bool bIsDirty = this.IsDirty;
                if ((m_iCurValueIndex > -1) && (m_iCurValueIndex < m_xValueArray.GetLength(0)))
                {
                    m_oControl.Value = m_xValueArray[m_iCurValueIndex];

                    this.ValueDirty = false;
                }
                this.IsDirty = bIsDirty;
            }
		}

		/// <summary>
		/// moves the focus to the item with the
		/// specified index or offset from the current item-
		/// does not add a new detail if current item
		/// is the last item
		/// </summary>
		/// <param name="iIndex"></param>
		/// <param name="bRelativeIndex"></param>
		/// <param name="bytSelect"></param>
		private void MoveToDetail(int iIndex, bool bRelativeIndex, mpMultiValueFieldSelection bytSelect)
		{
            LMP.Trace.WriteNameValuePairs("iIndex", iIndex, "bRelativeIndex", bRelativeIndex,
                "bytSelect", bytSelect);
            if (m_iCounterIndex == 0)
                return;
            else if (m_iCounterIndex == 1 && iIndex < 0)
                return;

            if (bRelativeIndex)
                //the index specified is relative to the current detail
                m_iCounterIndex = m_iCounterIndex + iIndex;
            else
            {
                //move to the specified detail
                m_iCounterIndex = iIndex;
            }
            SaveCurValue();
            m_iCurValueIndex = m_iCounterIndex - 1;
            FillValue();
            UpdateCount();
			switch(bytSelect)
			{
				case mpMultiValueFieldSelection.StartOfField:
				{
					if(this.ActiveControl is TextBox)
					{
						TextBox oTB = (TextBox)this.ActiveControl;
						oTB.SelectionStart = 0;
						oTB.SelectionLength = 0;
					}
					else if(this.ActiveControl is ComboBox)
					{
						ComboBox oCombo = (ComboBox)this.ActiveControl;
						oCombo.SelectionStart = 0;
						oCombo.SelectionLength = 0;
					}
					else if(this.ActiveControl is MultilineCombo)
					{
						MultilineCombo oMLC = (MultilineCombo) this.ActiveControl;
						oMLC.SelectionStart = 0;
						oMLC.SelectionLength = 0;
					}
					break;
				}
				case mpMultiValueFieldSelection.EndOfField:
				{
					if(this.ActiveControl is TextBox)
					{
						TextBox oTB = (TextBox)this.ActiveControl;
						oTB.SelectionStart = oTB.TextLength;
						oTB.SelectionLength = 0;
					}
					else if(this.ActiveControl is ComboBox)
					{
						ComboBox oCombo = (ComboBox)this.ActiveControl;
						oCombo.SelectionStart = oCombo.Text.Length;
						oCombo.SelectionLength = 0;
					}
					else if(this.ActiveControl is MultilineCombo)
					{
						MultilineCombo oMLC = (MultilineCombo) this.ActiveControl;
						oMLC.SelectionStart = oMLC.Text.Length;
						oMLC.SelectionLength = 0;
					}
					break;
				}
			}
		}

        private void UpdateMaxEntitiesView()
        {
            if (m_iNumValues > 1)
            {
                this.tsActions.Visible = true;
                // Enable the controls depending on if the number of entries is reached.
                this.tbtnPrev.Enabled = m_iCounterIndex > 1;
                this.tbtnNext.Enabled = m_iCounterIndex < m_iNumValues;
            }
            else
            {
                this.tsActions.Visible = false;
            }
            this.mnuContextMenu_Sep1.Visible = m_iNumValues > 1;
            this.mnuContextMenu_ApplyToAll.Visible = m_iNumValues > 1;
        }
        /// <summary>
		/// returns true iff control is a multiline control
		/// </summary>
		/// <param name="oCtl"></param>
		/// <returns></returns>
		private bool IsMultilineControl(Control oCtl)
		{
			if(oCtl is MultilineCombo)
				return true;
			else if(oCtl is TextBox)
			    return ((TextBox)oCtl).Multiline == true;
			else
				return false;
		}

        /// <summary>
        /// Move to detail selected from dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tddbStatus_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                this.MoveToDetail(this.tddbStatus.DropDownItems.IndexOf(e.ClickedItem) + 1,
                    false, mpMultiValueFieldSelection.WholeField);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// Populate dropdown with list of Detail items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tddbStatus_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                LMP.Trace.WriteInfo("tddbStatus_DropDownOpening");
                SaveCurValue();
                this.tddbStatus.DropDownItems.Clear();
                if (m_xValueArray.GetLength(0) == 0)
                    return;

                //assign array to detail list
                foreach (string xVal in m_xValueArray)
                {
                    this.tddbStatus.DropDownItems.Add(xVal);
                }

                if (m_iCounterIndex <= this.tddbStatus.DropDownItems.Count)
                    //select the current detail in the list
                    this.tddbStatus.DropDownItems[m_iCounterIndex - 1].Select();
                else
                    this.tddbStatus.DropDownItems[0].Select();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

		#endregion

		#region *********************event handlers*********************
        /// <summary>
        /// Control has gotten focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MultiValueControl_Enter(object sender, EventArgs e)
        {
            try
            {
                LMP.Trace.WriteInfo("MultiValueControl_Enter");
                if (!this.DesignMode)
                {
                    if (m_bPopupVisible)
                        return;
                    //((Control)m_oControl).Focus();
                    if (m_iCurValueIndex > -1)
                        FillValue();
                }
                base.OnGotFocus(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Focus has moved off of Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MultiValueControl_Leave(object sender, EventArgs e)
        {
            try
            {
                LMP.Trace.WriteInfo("MultiValueControl_Leave");
                //Displaying popup list will change focus
                if (m_bPopupVisible)
                    return;
                SaveCurValue();
                if (!this.DesignMode)
                {
                    ((Control)m_oControl).TabStop = true;
                }
                base.OnLostFocus(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

		/// <summary>
		/// handles the mouse down event for sub controls
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleMouseDown(object sender, MouseEventArgs e)
		{
            LMP.Trace.WriteInfo("HandleMouseDown");
            try
			{
				if(e.Button == MouseButtons.Right)
				{
					m_oPopupSenderCtl = (Control) sender;
					this.mnuPopup.Show(m_oPopupSenderCtl, 
						new Point(m_oPopupSenderCtl.Left + e.X, e.Y));
				}
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		/// <summary>
		/// handles the TextChanged event sub controls
		/// </summary>
		private void HandleTextChanged(object sender, EventArgs e)
		{
            LMP.Trace.WriteInfo("HandleTextChanged");
            try
			{
				Control oCtl = (Control) sender;
                this.ValueDirty = true;
                this.IsDirty = true;
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		/// <summary>
		/// handles the GotFocus event for sub controls
		/// </summary>
		private void HandleGotFocus(object sender, EventArgs e)
		{
            try
			{
                Control oCtl = (Control)sender;
				//ensure that content of subcontrol is selected
				LMP.Dialog.EnsureSelectedContent(oCtl, false, false);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

        /// <summary>
		/// handles the KeyDown event sub controls
		/// </summary>
		private void HandleKeyDown(object sender, KeyEventArgs e)
		{
			try
			{

			    bool bNoKeyQualifiers = !e.Shift && !e.Alt && !e.Control;
                m_bShortCutKeyPressed = false;
                LMP.Trace.WriteNameValuePairs("sender", ((Control)sender).Name, "key", e.KeyCode, 
                    "bNoKeyQualifiers", bNoKeyQualifiers);
				if(e.KeyCode == Keys.PageUp && bNoKeyQualifiers)
				{
                    if (tbtnPrev.Enabled)
                    {
                        e.Handled = true;
                        this.MoveToDetail(-1, true, mpMultiValueFieldSelection.WholeField);
                    }
				}
				else if(e.KeyCode == Keys.PageDown && bNoKeyQualifiers)
				{
                    if (tbtnNext.Enabled)
                    {
                        e.Handled = true;
                        this.MoveToDetail(1, true, mpMultiValueFieldSelection.WholeField);
                    }
				}

                if (this.KeyPressed != null)
                    this.KeyPressed(this, e);

			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Control has been resized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void MultiValueControl_Resize(object sender, System.EventArgs e)
		{
			try
			{
                LMP.Trace.WriteInfo("Detail_Resize");
                //if(m_bControlIsLoaded && !m_bPopupVisible)
                //    SetupControl();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Control is loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void MultiValueControl_Load(object sender, System.EventArgs e)
		{
            try
            {
                LMP.Trace.WriteInfo("Detail_Load");

                this.SuspendLayout();

                SetupResourceStrings();
                if (this.DesignMode)
                    ExecuteFinalSetup();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
		}
        /// <summary>
        /// Mouse button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void pnlFields_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
				this.mnuPopup.Show(this.pnlFields,new Point(e.X, e.Y));
		}
        /// <summary>
        /// Clears contents of selected detail field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuContextMenu_ClearField_Click(object sender, System.EventArgs e)
		{
			try
			{
				m_oPopupSenderCtl.Text = null;
                this.Refresh();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Move to previous detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnPrev_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_iCounterIndex > m_iMinIndex)
                {
                    MoveToDetail(-1, true, mpMultiValueFieldSelection.WholeField);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Move to next detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_iCounterIndex < m_iNumValues)
                {
                    MoveToDetail(1, true, mpMultiValueFieldSelection.WholeField);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
		#endregion

		#region *********************protected members*********************		
		protected override bool IsInputKey(Keys keyData)
		{
            if (keyData == Keys.Tab || keyData == Keys.Return)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
		/// processes tab and shift-tab key messages -
		/// this method will be executed only when the
		/// message pump is broken, ie when the hosing
		/// form's ProcessDialogKey method is not run.
		/// this seems to happen when when tabbing from
		/// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (m.WParam.ToInt32() == (int)Keys.Return)
            {
                // Process Enter here, because KeyDown doesn't get raised for this
                // in TaskPane
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        #endregion

		#region *********************IControl members*********************
		public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        /// <summary>
        /// 
        /// </summary>
        public void ExecuteFinalSetup()
		{
            try
			{
                LMP.Trace.WriteInfo("ExecuteFinalSetup");
				m_bControlIsLoaded = true;
				SetupControl();
                m_iCurValueIndex = 0;
                m_iCounterIndex = 1;
                FillValue();
                UpdateMaxEntitiesView();
                UpdateCount();
                if (!this.DesignMode)
                    ((Control)m_oControl).Focus();
            }
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

        [Browsable(false)]
        [DescriptionAttribute("Gets/Sets the selected value.")]
		public string Value
		{
			get
			{
                SaveCurValue();
                //Return delimited list of values
				string xValString = "";
                if (m_xValueArray != null)
                {
                    for (int i = 0; i <= m_xValueArray.GetUpperBound(0); i++)
                    {
                        xValString += m_xValueArray[i];
                        if (i < m_xValueArray.GetUpperBound(0))
                            xValString += StringArray.mpEndOfValue;
                    }
                }
                return xValString;
			}
			set
			{
                //parse values from delimited string
                string xTempVal = value;
                if (!string.IsNullOrEmpty(xTempVal))
                {
                    m_xValueArray = xTempVal.Split(StringArray.mpEndOfValue);
                    m_iNumValues = m_xValueArray.GetLength(0);
                }
                else
                {
                    SetArraySize(m_iNumValues);
                }
                FillValue();
                UpdateMaxEntitiesView();
                SetControlSize();
                this.IsDirty = false;
			}
		}
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the Dirty flag indicating the value has changed.")]
		public bool IsDirty
		{
			set{m_bIsDirty = value;}
			get{return m_bIsDirty;}
		}
        [Browsable(false)]
        [DescriptionAttribute("Secondary user-defined object associated with the control.")]
        //TODO: what is this used for?
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        /// <summary>
        /// Handle Tab input in sub-control fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleTabPressed(object sender, TabPressedEventArgs e)
        {
            try
            {
                if (this.TabPressed != null)
                    this.TabPressed(this, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
		#endregion

        private void mnuContextMenu_ApplyToAll_Click(object sender, EventArgs e)
        {
            if (m_xValueArray.GetLength(0) > 1)
            {
                for (int i = 0; i <= m_xValueArray.GetUpperBound(0); i++)
                {
                    m_xValueArray[i] = m_oControl.Value;
                }
                this.ValueDirty = false;
                this.IsDirty = true;
            }
        }
    }

}
