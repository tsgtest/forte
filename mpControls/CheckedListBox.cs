using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace LMP.Controls
{
    abstract public class CheckedListBox : System.Windows.Forms.CheckedListBox, IControl
    {
        #region *********************fields*********************
        private Hashtable m_oHashtable = new Hashtable();
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private bool m_bDelimitedMode = false;
        private char m_cSep;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructors*********************
        public CheckedListBox(object[,] aTwoColumnBitwiseList)
            : base()
        {
            ConfigureCheckBoxAppearance();

            //add items to list and hashtable
            InitHashTableAndList(aTwoColumnBitwiseList);
            
        }
        public CheckedListBox(object[,] oTwoColumnList, char cSep)
            : base()
        {
            ConfigureCheckBoxAppearance();
            
            //set module level sep variable
            m_cSep = cSep;

            //add items to list and hashtable
            InitHashTableAndList(oTwoColumnList);
            
            //set delimited mode var to true
            m_bDelimitedMode = true;
        }
        #endregion
        #region *********************properties*******************************
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }

        /// <summary>
        /// gets/sets the value of the control
        /// </summary>
        public string Value
        {
            get
            {
                if (m_bDelimitedMode)
                    return GetDelimitedValueString();
                else
                {
                    int iValue = 0;

                    //cycle through checked items, adding
                    //the appropriate integer to the value
                    for (int i = 0; i < this.CheckedItems.Count; i++)
                        iValue += (int)m_oHashtable[this.CheckedItems[i]];

                    return iValue.ToString();
                }
            }
            set
            {
                if (m_bDelimitedMode)
                {
                    if (value != "")
                        SetCheckedItemsFromValue(value);
                }
                else
                {
                    if (value != "0")
                    {
                        //check each item whose bit field is 
                        //contained in the supplied control value
                        for (int i = 0; i < m_oHashtable.Count; i++)
                            this.SetItemChecked(i, (Int32.Parse(value) &
                                (int)Math.Pow(2, i)) == (int)Math.Pow(2, i));
                    }
                }
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnClick(EventArgs e)
        {
            this.m_bIsDirty = true;
            base.OnClick(e);

            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (this.KeyPressed != null)
                this.KeyPressed(this, e);

            base.OnKeyDown(e);
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);

            base.OnKeyUp(e);
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// parses delimited string into a two column array
        /// </summary>
        /// <param name="xSep"></param>
        /// <returns></returns>
        private string[,] ParseDelimitedString(string xInput, char cSep)
        {
            string[] xParse = xInput.Split(cSep);
            string[,] xTemp = new string[(xParse.GetLength(0)) / 2, 2];

            int iCounter = 0;
            for (int i = 0; i < xTemp.GetLength(0); i++)
            {
                xTemp[iCounter, 0] = xParse[i];
                xTemp[iCounter, 1] = xParse[i + 1];
                if (i % 2 != 0)
                    iCounter++;
            }
            return xTemp;
        }
        /// <summary>
        /// sets up checkbox appearance properties
        /// </summary>
        private void ConfigureCheckBoxAppearance()
        {
            this.CheckOnClick = true;
            this.ThreeDCheckBoxes = true;
            this.BorderStyle = BorderStyle.None;
        }
        /// <summary>
        /// check each item whose value equals
        /// corresponding member of parsed supplied value
        /// </summary>
        /// <param name="value"></param>
        private void SetCheckedItemsFromValue(string xValue)
        {
            string[] xSplitValue = xValue.Split(m_cSep);

            for (int i = 0; i < Items.Count; i++)
            {
                //use each checkbox item as key, get hashtable value
                //then see whether this value exists in the supplied
                //value array.  Set each item checked if it exists, unchecked
                //if it does not
                string xFormat = m_oHashtable[Items[i]].ToString();
                bool bChecked = false;
                for (int j = 0; j < xSplitValue.GetLength(0); j++)
                {
                    if (xFormat == xSplitValue[j])
                    {
                        bChecked = true;
                        break;
                    }
                }
                //set checked state for each item
                SetItemChecked(i, bChecked);
            }
        }
        /// <summary>
        /// fills hashtable and list - hashtable 
        /// will allow us to retrieve the value associated
        /// with each insert location string
        /// </summary>
        /// <param name="oTwoColumnList"></param>
        private void InitHashTableAndList(object[,] oTwoColumnList)
        {
            if (m_oHashtable.Count == 0)
                for (int i = 0; i <= oTwoColumnList.GetUpperBound(0); i++)
                {
                    m_oHashtable.Add(oTwoColumnList[i, 0], oTwoColumnList[i, 1]);
                    this.Items.Add(oTwoColumnList[i, 0]);
                }
            else
                for (int i = 0; i <= oTwoColumnList.GetUpperBound(0); i++)
                    this.Items.Add(oTwoColumnList[i, 0]);
        }
        /// <summary>
        /// constructs delimited string from checked items collection
        /// </summary>
        /// <returns></returns>
        private string GetDelimitedValueString()
        {
            StringBuilder oSB = new StringBuilder();
            //cycle through checked items, building
            //the delimited string
            for (int i = 0; i < this.CheckedItems.Count; i++)
                oSB.AppendFormat("{0}{1}", m_oHashtable[this.CheckedItems[i].ToString()], m_cSep.ToString());

            return oSB.ToString(0, Math.Max((oSB.Length - 1), 0));
        }
        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CheckedListBox
            // 
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //this.BackColor = System.Drawing.Color.Transparent;
            this.ResumeLayout(false);

        }
    }
}
