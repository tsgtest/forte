using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LMP.Controls;

namespace LMP.Controls
{
    public partial class MultiValueControl : System.Windows.Forms.UserControl, IControl
    {
        private System.Windows.Forms.Panel pnlFields;
        private System.Windows.Forms.ContextMenu mnuPopup;
        private System.Windows.Forms.MenuItem mnuContextMenu_ClearField;
        private System.Windows.Forms.MenuItem mnuContextMenu_Sep1;
        private ToolStrip tsActions;
        private ToolStripButton tbtnPrev;
        private ToolStripButton tbtnNext;
        private ToolStripSeparator tSep1;
        private ToolStripSeparator tSep2;
        private ToolStripDropDownButton tddbStatus;

        #region Component Designer generated code
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiValueControl));
            this.pnlFields = new System.Windows.Forms.Panel();
            this.mnuPopup = new System.Windows.Forms.ContextMenu();
            this.mnuContextMenu_ClearField = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_Sep1 = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_ApplyToAll = new System.Windows.Forms.MenuItem();
            this.tsActions = new System.Windows.Forms.ToolStrip();
            this.tbtnPrev = new System.Windows.Forms.ToolStripButton();
            this.tSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNext = new System.Windows.Forms.ToolStripButton();
            this.tSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.tddbStatus = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFields
            // 
            this.pnlFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFields.BackColor = System.Drawing.SystemColors.Window;
            this.pnlFields.Location = new System.Drawing.Point(0, 0);
            this.pnlFields.Name = "pnlFields";
            this.pnlFields.Size = new System.Drawing.Size(171, 127);
            this.pnlFields.TabIndex = 3;
            this.pnlFields.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlFields_MouseDown);
            // 
            // mnuPopup
            // 
            this.mnuPopup.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContextMenu_ClearField,
            this.mnuContextMenu_Sep1,
            this.mnuContextMenu_ApplyToAll});
            // 
            // mnuContextMenu_ClearField
            // 
            this.mnuContextMenu_ClearField.Index = 0;
            this.mnuContextMenu_ClearField.Text = "[Clear Field]";
            this.mnuContextMenu_ClearField.Click += new System.EventHandler(this.mnuContextMenu_ClearField_Click);
            // 
            // mnuContextMenu_Sep1
            // 
            this.mnuContextMenu_Sep1.Index = 1;
            this.mnuContextMenu_Sep1.Text = "-";
            // 
            // mnuContextMenu_ApplyToAll
            // 
            this.mnuContextMenu_ApplyToAll.Index = 2;
            this.mnuContextMenu_ApplyToAll.Text = "[Apply To All]";
            this.mnuContextMenu_ApplyToAll.Click += new System.EventHandler(this.mnuContextMenu_ApplyToAll_Click);
            // 
            // tsActions
            // 
            this.tsActions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsActions.AutoSize = false;
            this.tsActions.Dock = System.Windows.Forms.DockStyle.None;
            this.tsActions.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsActions.GripMargin = new System.Windows.Forms.Padding(0);
            this.tsActions.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnPrev,
            this.tSep1,
            this.tbtnNext,
            this.tSep2,
            this.tddbStatus});
            this.tsActions.Location = new System.Drawing.Point(0, 127);
            this.tsActions.Name = "tsActions";
            this.tsActions.Size = new System.Drawing.Size(171, 22);
            this.tsActions.TabIndex = 105;
            this.tsActions.Text = "toolStrip1";
            // 
            // tbtnPrev
            // 
            this.tbtnPrev.AutoSize = false;
            this.tbtnPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnPrev.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnPrev.Image = ((System.Drawing.Image)(resources.GetObject("tbtnPrev.Image")));
            this.tbtnPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnPrev.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnPrev.Name = "tbtnPrev";
            this.tbtnPrev.Size = new System.Drawing.Size(20, 22);
            this.tbtnPrev.Text = "<";
            this.tbtnPrev.Click += new System.EventHandler(this.tbtnPrev_Click);
            // 
            // tSep1
            // 
            this.tSep1.AutoSize = false;
            this.tSep1.Name = "tSep1";
            this.tSep1.Size = new System.Drawing.Size(3, 22);
            // 
            // tbtnNext
            // 
            this.tbtnNext.AutoSize = false;
            this.tbtnNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNext.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnNext.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNext.Image")));
            this.tbtnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNext.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnNext.Name = "tbtnNext";
            this.tbtnNext.Size = new System.Drawing.Size(20, 22);
            this.tbtnNext.Text = ">";
            this.tbtnNext.Click += new System.EventHandler(this.tbtnNext_Click);
            // 
            // tSep2
            // 
            this.tSep2.AutoSize = false;
            this.tSep2.Name = "tSep2";
            this.tSep2.Size = new System.Drawing.Size(3, 22);
            // 
            // tddbStatus
            // 
            this.tddbStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tddbStatus.Image = ((System.Drawing.Image)(resources.GetObject("tddbStatus.Image")));
            this.tddbStatus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tddbStatus.Name = "tddbStatus";
            this.tddbStatus.ShowDropDownArrow = false;
            this.tddbStatus.Size = new System.Drawing.Size(73, 19);
            this.tddbStatus.Text = "Item ## of ##";
            this.tddbStatus.ToolTipText = "Item # of #";
            this.tddbStatus.DropDownOpening += new System.EventHandler(this.tddbStatus_DropDownOpening);
            this.tddbStatus.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tddbStatus_DropDownItemClicked);
            // 
            // MultiValueControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.pnlFields);
            this.Controls.Add(this.tsActions);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MultiValueControl";
            this.Size = new System.Drawing.Size(173, 149);
            this.Load += new System.EventHandler(this.MultiValueControl_Load);
            this.Enter += new System.EventHandler(this.MultiValueControl_Enter);
            this.Leave += new System.EventHandler(this.MultiValueControl_Leave);
            this.Resize += new System.EventHandler(this.MultiValueControl_Resize);
            this.tsActions.ResumeLayout(false);
            this.tsActions.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private MenuItem mnuContextMenu_ApplyToAll;
    }
}
