﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMP.Controls
{
    public class SortedListBox : ListBox
    {
        protected override System.Windows.Forms.CreateParams CreateParams
        {
            get
            {
                var returnvalue = base.CreateParams;
                returnvalue.Style |= 0x2; //Add LBS_SORT
                returnvalue.Style ^= 128; //Remove LBS_USETABSTOPS
                return returnvalue;
            }
        }
    }
}
