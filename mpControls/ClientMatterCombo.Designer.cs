namespace LMP.Controls
{
    partial class ClientMatterCombo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientMatterCombo));
            this.axClientMatterComboBox1 = new AxMPCM.AxClientMatterComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.axClientMatterComboBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // axClientMatterComboBox1
            // 
            this.axClientMatterComboBox1.Enabled = true;
            this.axClientMatterComboBox1.Location = new System.Drawing.Point(0, 0);
            this.axClientMatterComboBox1.Name = "axClientMatterComboBox1";
            this.axClientMatterComboBox1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axClientMatterComboBox1.OcxState")));
            this.axClientMatterComboBox1.Size = new System.Drawing.Size(260, 155);
            this.axClientMatterComboBox1.TabIndex = 0;
            // 
            // axClientMatterComboBox1
            // 
            this.axClientMatterComboBox1.Enabled = true;
            this.axClientMatterComboBox1.Location = new System.Drawing.Point(80, 50);
            this.axClientMatterComboBox1.Name = "axClientMatterComboBox1";
            this.axClientMatterComboBox1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axClientMatterComboBox1.OcxState1")));
            this.axClientMatterComboBox1.Size = new System.Drawing.Size(259, 155);
            this.axClientMatterComboBox1.TabIndex = 0;
            this.axClientMatterComboBox1.ClientMatterSelected += new System.EventHandler(this.axClientMatterComboBox1_ClientMatterSelected);
            // 
            // ClientMatterCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.axClientMatterComboBox1);
            this.Name = "ClientMatterCombo";
            this.Size = new System.Drawing.Size(377, 162);
            ((System.ComponentModel.ISupportInitialize)(this.axClientMatterComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axClientMatterComboBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxMPCM.AxClientMatterComboBox axClientMatterComboBox1;
    }
}
