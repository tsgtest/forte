using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class DataGridView : System.Windows.Forms.DataGridView, IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructor*********************
        public DataGridView()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************properties*********************
        #endregion
        #region *********************methods*********************
        // GLOG : 6027 : CEH
        /// <summary>
        /// Searches provided DataGridView for the search string 
        /// and returns the index of the matching cell- returns
        /// -1 if no match is found.
        /// </summary>
        /// <param name="xSearchString"></param>
        /// <param name="oGrid"></param>
        /// <param name="xColName"></param>
        /// <param name="bCaseSensitive"></param>
        /// <param name="bUnOrdered"></param>
        /// <returns></returns>
        public int FindRecord(string xSearchString, string xColName, bool bCaseSensitive, bool bUnOrdered)
        {
            if (!bUnOrdered)
                return this.FindRecord(xSearchString, xColName, bCaseSensitive, 0, this.Rows.Count - 1);
            else
                return this.FindRecordUnOrdered(xSearchString, xColName, bCaseSensitive);
        }
        /// <summary>
        /// Searches provided DataGridView for the search string 
        /// and returns the index of the matching cell- returns
        /// -1 if no match is found.
        /// </summary>
        /// <param name="xSearchString"></param>
        /// <param name="oGrid"></param>
        /// <param name="xColName"></param>
        /// <param name="bCaseSensitive"></param>
        /// <returns></returns>
        public int FindRecord(string xSearchString, string xColName, bool bCaseSensitive)
        {
            return this.FindRecord(xSearchString, xColName, bCaseSensitive, 0, this.Rows.Count - 1);
        }
        /// <summary>
        /// Searches provided DataGridView for the search string 
        /// and returns the index of the matching cell- returns
        /// -1 if no match is found.
        /// </summary>
        /// <param name="xSearchString"></param>
        /// <param name="oGrid"></param>
        /// <param name="xColName"></param>
        /// <param name="bCaseSensitive"></param>
        /// <param name="iCellCount"></param>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        private int FindRecord(string xSearchString, string xColName, bool bCaseSensitive, int iLeft, int iRight)
        {
            if (iRight < iLeft)
                //No matches found
                return -1;

            //Get the index of the midpoint of the current
            //range of cells.
            int iMid = ((iRight - iLeft) / 2) + iLeft;

            //Get the text of the midpoint cell.
            string xCurCellText = this.Rows[iMid].Cells[xColName].FormattedValue.ToString();

            if (bCaseSensitive && xCurCellText.StartsWith(xSearchString))
            {
                //Case sensative match found

                //Find the first instance of the string
                do
                {
                    if (iMid == 0)
                        //If the first cell contains the string,
                        //stop testing.
                        return iMid;

                    //Get the text of the previous cell.
                    iMid--;
                    xCurCellText = this.Rows[iMid].Cells[xColName].FormattedValue.ToString();
                }
                while (xCurCellText.StartsWith(xSearchString));

                //Return the index of the first match.
                return iMid + 1;
            }

            else if (!bCaseSensitive && xCurCellText.ToUpper().StartsWith(xSearchString.ToUpper()))
            {
                //Non-case sensative match found

                //Find the first instance of the string
                do
                {
                    if (iMid == 0)
                        //The first cell contains the string,
                        //stop testing.
                        return iMid;

                    //Get the text of the previous cell.
                    iMid--;
                    xCurCellText = this.Rows[iMid].Cells[xColName].FormattedValue.ToString();
                }
                while (xCurCellText.ToUpper().StartsWith(xSearchString.ToUpper()));

                //Return the index of the first match.
                return iMid + 1;
            }

            else
            {
                //Compare the search string with the text 
                //of the current cell.
                int iCompare = xSearchString.CompareTo(xCurCellText);

                if (iCompare < 0)
                    //Search string is before the current 
                    //cell text alphabetically
                    return FindRecord(xSearchString, xColName, bCaseSensitive, iLeft, iMid - 1);

                else
                    //Search string is after the current 
                    //cell text alphabetically
                    return FindRecord(xSearchString, xColName, bCaseSensitive, iMid + 1, iRight);

            }
        }

        // GLOG : 6027 : CEH
        /// <summary>
        /// Searches provided DataGridView for the search string 
        /// and returns the index of the matching cell- returns
        /// -1 if no match is found.
        /// </summary>
        /// <param name="xSearchString"></param>
        /// <param name="oGrid"></param>
        /// <param name="xColName"></param>
        /// <param name="bCaseSensitive"></param>
        /// <param name="iCellCount"></param>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        private int FindRecordUnOrdered(string xSearchString, string xColName, bool bCaseSensitive)
        {
            if (this.Rows.Count == 0)
                //No matches found
                return -1;

            for (int i = 0; i < this.Rows.Count; i++)
            {
                //get text of cell #i
                string xCurCellText = this.Rows[i].Cells[xColName].FormattedValue.ToString();

                if (bCaseSensitive && xCurCellText.StartsWith(xSearchString))
                    //Case sensitive match found
                    //Return the index of the first match.
                    return i;

                else if (!bCaseSensitive && xCurCellText.ToUpper().StartsWith(xSearchString.ToUpper()))
                    //match found
                    //Return the index of the first match.
                    return i;
            }

            //not found
            return -1;
        }

        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event EnterKeyPressedHandler EnterKeyPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }
        public string Value
        {
            get
            {
                //replace reserved chars with escape sequences
                return null;
            }
            set { }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            int iQualifierKeys;
            bool bShiftPressed;
            bool bEnterKeyPressed = LMP.OS.EnterKeyPressed(m, out iQualifierKeys);
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (bEnterKeyPressed)
            {
                if (this.EnterKeyPressed != null)
                    this.EnterKeyPressed(this, new EnterKeyPressedEventArgs(iQualifierKeys));
                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnCellValueChanged(DataGridViewCellEventArgs e)
        {
            base.OnCellValueChanged(e);

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());

        }
        #endregion
    }
}
