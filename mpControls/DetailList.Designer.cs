using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Xml;

namespace LMP.Controls
{
    public partial class DetailList : System.Windows.Forms.UserControl, IControl
    {
        private System.Windows.Forms.Panel pnlItems;
        private System.Windows.Forms.VScrollBar vsbCurrentIndex;
        private System.Windows.Forms.ContextMenu mnuPopup;
        private System.Windows.Forms.MenuItem mnuContextMenu_DeleteDetail;
        private System.Windows.Forms.MenuItem mnuContextMenu_DeleteAll;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem mnuContextMenu_RefreshDetail;
        private System.Windows.Forms.MenuItem mnuContextMenu_RefreshAll;
        private System.Windows.Forms.MenuItem mnuContextMenu_MoveUp;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem mnuContextMenu_MoveDown;
        private System.ComponentModel.Container components = null;

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetailList));
            this.vsbCurrentIndex = new System.Windows.Forms.VScrollBar();
            this.pnlItems = new System.Windows.Forms.Panel();
            this.mnuPopup = new System.Windows.Forms.ContextMenu();
            this.mnuContextMenu_MoveUp = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_MoveDown = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_DeleteDetail = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_DeleteAll = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_Cut = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_Copy = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_Paste = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_RefreshDetail = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_RefreshAll = new System.Windows.Forms.MenuItem();
            this.tsActions = new System.Windows.Forms.ToolStrip();
            this.tlblStatus = new System.Windows.Forms.ToolStripLabel();
            this.tbtnContacts = new System.Windows.Forms.ToolStripButton();
            this.tSep5 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.tSep4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnMoveDown = new System.Windows.Forms.ToolStripButton();
            this.tSep3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnMoveUp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // vsbCurrentIndex
            // 
            this.vsbCurrentIndex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vsbCurrentIndex.LargeChange = 1;
            this.vsbCurrentIndex.Location = new System.Drawing.Point(365, 0);
            this.vsbCurrentIndex.Maximum = 0;
            this.vsbCurrentIndex.Name = "vsbCurrentIndex";
            this.vsbCurrentIndex.Size = new System.Drawing.Size(18, 141);
            this.vsbCurrentIndex.TabIndex = 106;
            this.vsbCurrentIndex.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vsbFirstIndex_Scroll);
            // 
            // pnlItems
            // 
            this.pnlItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlItems.BackColor = System.Drawing.Color.White;
            this.pnlItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlItems.ForeColor = System.Drawing.Color.Black;
            this.pnlItems.Location = new System.Drawing.Point(0, 0);
            this.pnlItems.Name = "pnlItems";
            this.pnlItems.Size = new System.Drawing.Size(365, 141);
            this.pnlItems.TabIndex = 103;
            this.pnlItems.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlItems_Paint);
            // 
            // mnuPopup
            // 
            this.mnuPopup.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContextMenu_MoveUp,
            this.mnuContextMenu_MoveDown,
            this.menuItem2,
            this.mnuContextMenu_DeleteDetail,
            this.mnuContextMenu_DeleteAll,
            this.menuItem5,
            this.mnuContextMenu_Cut,
            this.mnuContextMenu_Copy,
            this.mnuContextMenu_Paste,
            this.menuItem4,
            this.mnuContextMenu_RefreshDetail,
            this.mnuContextMenu_RefreshAll});
            this.mnuPopup.Popup += new System.EventHandler(this.mnuPopup_Popup);
            // 
            // mnuContextMenu_MoveUp
            // 
            this.mnuContextMenu_MoveUp.Index = 0;
            this.mnuContextMenu_MoveUp.Shortcut = System.Windows.Forms.Shortcut.ShiftF11;
            this.mnuContextMenu_MoveUp.Text = "Move &Up";
            this.mnuContextMenu_MoveUp.Click += new System.EventHandler(this.mnuContextMenu_MoveUp_Click);
            // 
            // mnuContextMenu_MoveDown
            // 
            this.mnuContextMenu_MoveDown.Index = 1;
            this.mnuContextMenu_MoveDown.Shortcut = System.Windows.Forms.Shortcut.ShiftF12;
            this.mnuContextMenu_MoveDown.Text = "Move Do&wn";
            this.mnuContextMenu_MoveDown.Click += new System.EventHandler(this.mnuContextMenu_MoveDown_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 2;
            this.menuItem2.Text = "-";
            // 
            // mnuContextMenu_DeleteDetail
            // 
            this.mnuContextMenu_DeleteDetail.Index = 3;
            this.mnuContextMenu_DeleteDetail.Text = "&Delete Item...";
            this.mnuContextMenu_DeleteDetail.Click += new System.EventHandler(this.mnuContextMenu_DeleteDetail_Click);
            // 
            // mnuContextMenu_DeleteAll
            // 
            this.mnuContextMenu_DeleteAll.Index = 4;
            this.mnuContextMenu_DeleteAll.Text = "De&lete All...";
            this.mnuContextMenu_DeleteAll.Click += new System.EventHandler(this.mnuContextMenu_DeleteAll_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 5;
            this.menuItem5.Text = "-";
            // 
            // mnuContextMenu_Cut
            // 
            this.mnuContextMenu_Cut.Index = 6;
            this.mnuContextMenu_Cut.Text = "Cut";
            this.mnuContextMenu_Cut.Click += new System.EventHandler(this.mnuContextMenu_Cut_Click);
            // 
            // mnuContextMenu_Copy
            // 
            this.mnuContextMenu_Copy.Index = 7;
            this.mnuContextMenu_Copy.Text = "Copy";
            this.mnuContextMenu_Copy.Click += new System.EventHandler(this.mnuContextMenu_Copy_Click);
            // 
            // mnuContextMenu_Paste
            // 
            this.mnuContextMenu_Paste.Index = 8;
            this.mnuContextMenu_Paste.Text = "&Paste";
            this.mnuContextMenu_Paste.Click += new System.EventHandler(this.mnuContextMenu_Paste_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 9;
            this.menuItem4.Text = "-";
            // 
            // mnuContextMenu_RefreshDetail
            // 
            this.mnuContextMenu_RefreshDetail.Index = 10;
            this.mnuContextMenu_RefreshDetail.Text = "&Refresh Item";
            this.mnuContextMenu_RefreshDetail.Click += new System.EventHandler(this.mnuContextMenu_RefreshDetail_Click);
            // 
            // mnuContextMenu_RefreshAll
            // 
            this.mnuContextMenu_RefreshAll.Index = 11;
            this.mnuContextMenu_RefreshAll.Text = "Re&fresh All";
            this.mnuContextMenu_RefreshAll.Click += new System.EventHandler(this.mnuContextMenu_RefreshAll_Click);
            // 
            // tsActions
            // 
            this.tsActions.AutoSize = false;
            this.tsActions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tsActions.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsActions.GripMargin = new System.Windows.Forms.Padding(0);
            this.tsActions.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblStatus,
            this.tbtnContacts,
            this.tSep5,
            this.tbtnDelete,
            this.tSep4,
            this.tbtnAdd,
            this.toolStripSeparator1,
            this.tbtnMoveDown,
            this.tSep3,
            this.tbtnMoveUp,
            this.toolStripSeparator2});
            this.tsActions.Location = new System.Drawing.Point(0, 141);
            this.tsActions.Name = "tsActions";
            this.tsActions.Size = new System.Drawing.Size(383, 22);
            this.tsActions.TabIndex = 106;
            this.tsActions.Text = "toolStrip1";
            // 
            // tlblStatus
            // 
            this.tlblStatus.Name = "tlblStatus";
            this.tlblStatus.Size = new System.Drawing.Size(69, 19);
            this.tlblStatus.Text = "Item ## of ##";
            // 
            // tbtnContacts
            // 
            this.tbtnContacts.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnContacts.AutoSize = false;
            this.tbtnContacts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnContacts.Image = ((System.Drawing.Image)(resources.GetObject("tbtnContacts.Image")));
            this.tbtnContacts.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnContacts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnContacts.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnContacts.Name = "tbtnContacts";
            this.tbtnContacts.Size = new System.Drawing.Size(23, 19);
            this.tbtnContacts.Click += new System.EventHandler(this.tbtnContacts_Click);
            // 
            // tSep5
            // 
            this.tSep5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tSep5.AutoSize = false;
            this.tSep5.Name = "tSep5";
            this.tSep5.Size = new System.Drawing.Size(3, 22);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnDelete.AutoSize = false;
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(23, 19);
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // tSep4
            // 
            this.tSep4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tSep4.AutoSize = false;
            this.tSep4.Name = "tSep4";
            this.tSep4.Size = new System.Drawing.Size(3, 22);
            // 
            // tbtnAdd
            // 
            this.tbtnAdd.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnAdd.AutoSize = false;
            this.tbtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("tbtnAdd.Image")));
            this.tbtnAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAdd.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnAdd.Name = "tbtnAdd";
            this.tbtnAdd.Size = new System.Drawing.Size(23, 19);
            this.tbtnAdd.Click += new System.EventHandler(this.tbtnAdd_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 22);
            // 
            // tbtnMoveDown
            // 
            this.tbtnMoveDown.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnMoveDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("tbtnMoveDown.Image")));
            this.tbtnMoveDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnMoveDown.Name = "tbtnMoveDown";
            this.tbtnMoveDown.Size = new System.Drawing.Size(23, 19);
            this.tbtnMoveDown.Text = "toolStripButton2";
            this.tbtnMoveDown.Click += new System.EventHandler(this.tbtnMoveDown_Click);
            // 
            // tSep3
            // 
            this.tSep3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tSep3.AutoSize = false;
            this.tSep3.Name = "tSep3";
            this.tSep3.Size = new System.Drawing.Size(3, 22);
            // 
            // tbtnMoveUp
            // 
            this.tbtnMoveUp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnMoveUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("tbtnMoveUp.Image")));
            this.tbtnMoveUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnMoveUp.Name = "tbtnMoveUp";
            this.tbtnMoveUp.Size = new System.Drawing.Size(23, 19);
            this.tbtnMoveUp.Text = "toolStripButton1";
            this.tbtnMoveUp.Click += new System.EventHandler(this.tbtnMoveUp_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 22);
            // 
            // DetailList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tsActions);
            this.Controls.Add(this.vsbCurrentIndex);
            this.Controls.Add(this.pnlItems);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DetailList";
            this.Size = new System.Drawing.Size(383, 163);
            this.Load += new System.EventHandler(this.DetailList_Load);
            this.VisibleChanged += new System.EventHandler(this.DetailList_VisibleChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DetailList_Paint);
            this.Enter += new System.EventHandler(this.DetailList_Enter);
            this.Leave += new System.EventHandler(this.DetailList_Leave);
            this.tsActions.ResumeLayout(false);
            this.tsActions.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private ToolStrip tsActions;
        private ToolStripButton tbtnContacts;
        private ToolStripSeparator tSep5;
        private ToolStripButton tbtnDelete;
        private ToolStripSeparator tSep4;
        private ToolStripButton tbtnAdd;
        private ToolStripSeparator tSep3;
        private ToolStripLabel tlblStatus;
        private ToolStripButton tbtnMoveUp;
        private ToolStripButton tbtnMoveDown;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator2;
        private MenuItem mnuContextMenu_Paste;
        private MenuItem menuItem5;
        private MenuItem mnuContextMenu_Cut;
        private MenuItem mnuContextMenu_Copy;
    }
}
