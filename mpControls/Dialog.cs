using System;
using System.Windows.Forms;
using System.Drawing;
using LMP.Controls;
using System.Text.RegularExpressions;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP
{
	/// <summary>
	/// Contains low level Dialog functions
	/// </summary>
	public sealed class Dialog
	{
		/// <summary>
		/// private constructor - prevent instance creation
		/// </summary>
		private Dialog(){}
		public static void EnsureSelectedContent(Control oCtl, 
			bool bLastCharOnly, bool bForceSelection)
		{
			//TODO: implement conditional to select only when 
			//tabbing in or if bForceSelect == true -
			//implement IsPressed(9) as disjunct
			switch(oCtl.GetType().Name.ToUpper())
			{
				case "LISTBOX":
				{
					//select first list item
					LMP.Controls.ListBox oLst = (LMP.Controls.ListBox)oCtl;
					if(oLst.SelectedIndex < 0)
						oLst.SelectedIndex = 0;
					break;
				}
				case "ComboBox":
				{
					LMP.Controls.ComboBox oCombo = (LMP.Controls.ComboBox) oCtl;
					if(bLastCharOnly)
					{
						//select last char
						oCombo.SelectionStart = oCombo.Text.Length;
						oCombo.SelectionLength = 1;
					}
					else
					{
						oCombo.SelectionStart = 0;
						oCombo.SelectionLength = oCombo.Text.Length;
					}
					break;
				}
				case "TEXTBOX":
				{
					LMP.Controls.TextBox oTB = (LMP.Controls.TextBox) oCtl;
					if(bLastCharOnly)
					{
						//select last char
						oTB.SelectionStart = oTB.Text.Length;
						oTB.SelectionLength = 1;
					}
					else
					{
						oTB.SelectionStart = 0;
						oTB.SelectionLength = oTB.Text.Length;
					}
					break;
				}
				case "MPMULTILINECOMBO":
				{
					//TODO: implement this block
					break;
				}
				case "SALUTATIONCOMBO":
				{
					//TODO: implement this block
					break;
				}
				case "CLIENTMATTERCOMBO":
				{
					//TODO: implement this block
					break;
				}
				default:
				{
					break;
				}
			}
		}
        /// <summary>
        /// returns True if the key is defined
        /// by MacPac to be an input key - this
        /// is necessary because the base.InputKey()
        /// method is problematic, possible due to IBF
        /// </summary>
        /// <param name="KeyData"></param>
        /// <returns></returns>
        public static bool IsMacPacDefinedInputKey(Keys KeyData)
      {
            const int KEY_EQUALSIGN = 187;
            const int KEY_OPENBRACKET = 219;
            const int KEY_CLOSEBRACKET = 221;

            int iAsc = (int)KeyData;
            return (iAsc >= 65 && iAsc <= 90) || (iAsc >= 65601 && iAsc <= 65626) || (iAsc == 65725) ||
                (iAsc == KEY_EQUALSIGN) || (iAsc == KEY_OPENBRACKET) || (iAsc == KEY_CLOSEBRACKET);
        }
		/// <summary>
		/// returns the CI Detail for the specified contact and field
		/// </summary>
		/// <param name="oContact"></param>
		/// <param name="xField"></param>
		/// <returns></returns>
		public static string GetCIDetail(ICContact oContact, string xField)
		{
			if(oContact == null)
				return null;

            LMP.Trace.WriteNameValuePairs("xField", xField);

            //GLOG : 8031 : ceh
            //fill in CoreAddress token
			string xDetail = xField.Replace("<COREADDRESS>", LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));
			//fill in other details
            xDetail = oContact.GetDetail(xDetail, true);
			
			return xDetail;
		}
		/// <summary>
		/// returns the CI Detail for the contact with the specified UNID and field 
		/// </summary>
		/// <param name="xUNID"></param>
		/// <param name="xField"></param>
		/// <returns></returns>
		public static string GetCIDetail(string xUNID, string xField, ICSession oCISession)
		{
            Trace.WriteNameValuePairs("xUNID", xUNID, "xField", xField);

			//get contact from UNID
			ICContact oC = oCISession.GetContactFromUNID(xUNID, 
				ciRetrieveData.ciRetrieveData_All, ciAlerts.ciAlert_None);

			if(oC == null)
				//no contact with specified UNID
				throw new LMP.Exceptions.UNIDException(
					LMP.Resources.GetLangString("Error_InvalidCIUNID") + xUNID);

			//return detail
			return GetCIDetail(oC, xField);
		}
        /// <summary>
        /// returns true iff the specified text contains
        /// a MacPac reserved character - i.e. those characters
        /// that the application uses, like delimiters
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        internal static bool ContainsMacPacReservedChars(string xText)
        {
            return Regex.IsMatch(xText, @"[\|ޤ���]");
        }
        /// <summary>
        /// removes MacPac reserved characters from the specified text
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        internal static string RemoveMacPacReservedChars(string xText)
        {
            return Regex.Replace(xText, @"[\|ޤ���]", "");
        }
        /// <summary>
        /// Displays VB-Style InputBox to prompt for text
        /// </summary>
        /// <param name="xTitle"></param>
        /// <param name="xPrompt"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        public static DialogResult InputBox(string xTitle, string xPrompt, ref string xValue)
        {
            Form oForm = new Form();
            Label oLabel = new Label();
            System.Windows.Forms.TextBox oText = new System.Windows.Forms.TextBox();
            Button btnOK = new Button();
            Button btnCancel = new Button();

            oForm.Text = xTitle;
            oLabel.Text = xPrompt;
            oText.Text = xValue;

            btnOK.Text = LMP.Resources.GetLangString("Lbl_OK");
            btnCancel.Text = LMP.Resources.GetLangString("Lbl_Cancel");
            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;

            oLabel.SetBounds(9, 20, 372, 13);
            oText.SetBounds(12, 36, 372, 20);
            btnOK.SetBounds(228, 72, 75, 23);
            btnCancel.SetBounds(309, 72, 75, 23);

            oLabel.AutoSize = true;
            oText.Anchor = oText.Anchor | AnchorStyles.Right;
            btnOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            oForm.ClientSize = new Size(396, 107);
            oForm.Controls.AddRange(new Control[] { oLabel, oText, btnOK, btnCancel });
            oForm.ClientSize = new Size(Math.Max(300, oLabel.Right + 10), oForm.ClientSize.Height);
            oForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            oForm.StartPosition = FormStartPosition.CenterScreen;
            oForm.MinimizeBox = false;
            oForm.MaximizeBox = false;
            oForm.AcceptButton = btnOK;
            oForm.CancelButton = btnCancel;

            DialogResult dialogResult = oForm.ShowDialog();
            xValue = oText.Text;
            return dialogResult;
        }
    }
}