using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.Text;
using System.Data;
using LMP.Data;

namespace LMP.Controls
{
    public partial class ListBox : System.Windows.Forms.ListBox, IControl  
    {
        #region *********************fields*********************
        private List m_oList;
		private string m_xList = "";
        private int m_iSelectedIndex = -1;
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private string m_xDisplayMember;
        private string m_xValueMember;
        private bool m_bAllowEmptyValue;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructors*********************
        public ListBox()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************properties*******************************
        [DescriptionAttribute("Gets/Sets the name of the list that displays in the dropdown portion of the control.")]
        public string ListName
        {
            get { return m_xList; }
            set
            {
                m_xList = value;
                if (!this.DesignMode)
                    SetList(value);
            }
        }
        /// <summary>
        /// gets/sets the list items of the control as a string
        /// </summary>
        [BrowsableAttribute(false)]
        public string ListString
        {
            get
            {
                string xList = "";
                DataTable oDT = (DataTable)base.DataSource;

                if (oDT == null)
                    return "";

                //populate by cycling through list items
                for (int i = 0; i < oDT.Rows.Count; i++)
                {
                    for (int j = 0; j < oDT.Columns.Count; j++)
                        xList = xList + oDT.Rows[i][j].ToString() + "|";
                }

                return xList;
            }
        }
        /// <summary>
        /// gets the list items of the control as an array
        /// </summary>
        //[BrowsableAttribute(false)]
        public string[,] ListArray
        {
            get
            {
                DataTable oDT = (DataTable)base.DataSource;

                if (oDT == null)
                    return null;

                //create a new string array
                string[,] aList = new string[oDT.Rows.Count, oDT.Columns.Count];

                //populate by cycling through list items
                for (int i = 0; i < oDT.Rows.Count; i++)
                {
                    for (int j = 0; j < oDT.Columns.Count; j++)
                        aList[i, j] = oDT.Rows[i][j].ToString();
                }

                return aList;
            }
        }
        [DescriptionAttribute("Gets/Sets the backcolor of the control.")]
        public override System.Drawing.Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        [DescriptionAttribute("Gets/Sets the font of the control.")]
        public override Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the index of the selected value.")]
        public override int SelectedIndex
        {
            get { return base.SelectedIndex; }
            set
            {
                m_iSelectedIndex = value;
                base.SelectedIndex = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the display member of the list.")]
        public new string DisplayMember
        {
            get { return base.DisplayMember; }
            set
            {
                m_xDisplayMember = value;
                base.DisplayMember = value;
            }
        }

        [DescriptionAttribute("Gets/Sets the display member of the list.")]
        public new string ValueMember
        {
            get { return base.ValueMember; }
            set
            {
                m_xValueMember = value;
                base.ValueMember = value;
            }
        }
        
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public new Object SelectedValue
        {
            get { return base.SelectedValue; }
            set
            {
                base.SelectedValue = value;
            }
        }
        [DescriptionAttribute("Gets/sets whether empty value is allowed.")]
        public bool AllowEmptyValue
        {
            get { return m_bAllowEmptyValue; }
            set { m_bAllowEmptyValue = value; }
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event EnterKeyPressedHandler EnterPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }

        /// <summary>
        /// gets/sets the value of the control
        /// </summary>
        public string Value
        {
            get
            {
                if (base.SelectedValue != null)
                    return base.SelectedValue.ToString();
                else
                    return "";
            }
            set
            {
                base.Text = value;
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            int iQualifierKeys;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);
            bool bEnterPressed = LMP.OS.EnterKeyPressed(m, out iQualifierKeys);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (bEnterPressed)
            {
                if (this.EnterPressed != null)
                    this.EnterPressed(this, new EnterKeyPressedEventArgs(iQualifierKeys));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnClick(EventArgs e)
        {
            this.m_bIsDirty = true;
            base.OnClick(e);

            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (this.KeyPressed != null)
                this.KeyPressed(this, e);

            base.OnKeyDown(e);
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);

            base.OnKeyUp(e);
        }
        #endregion
        #region *********************private members*********************
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// overloaded method takes ArrayList for listitems
        /// </summary>
        /// <param name="aList"></param>
        public void SetList(ArrayList aList)
        {
            string[,] aTemp = new string[aList.Count, 2];
            for (int i = 0; i < aList.Count; i++)
            {
                //create string array and call set list to load
                if (((Object[])aList[i]).Length > 1)
                    aTemp[i, 0] = ((Object[])aList[i])[1].ToString();
                else
                    //If Array only has 1 dimension, use same text for display and valuemember
                    aTemp[i, 0] = ((Object[])aList[i])[0].ToString();
                aTemp[i, 1] = ((Object[])aList[i])[0].ToString();
            }
            SetList(aTemp);
        }
        /// <summary>
        /// populates the combo list with items in the specified System.Array
        /// </summary>
        /// <param name="aList"></param>
        public void SetList(Array aList)
        {
            int iNumCols = aList.GetUpperBound(1);

            string[,] aTemp = new string[aList.GetUpperBound(0) + 1, 2];

            if (iNumCols == 0)
            {
                for (int i = 0; i <= aList.GetUpperBound(0); i++)
                {
                    //create string array and call set list to load
                    aTemp[i, 0] = aList.GetValue(i).ToString();
                    aTemp[i, 1] = aList.GetValue(i).ToString();
                }
            }
            else if (iNumCols == 1)
            {
                for (int i = 0; i <= aList.GetUpperBound(0); i++)
                {
                    //populate temp array row
                    aTemp[i, 0] = aList.GetValue(i, 0).ToString();
                    aTemp[i, 1] = aList.GetValue(i, 1).ToString();
                }
            }

            SetList(aTemp);
        }
        /// <summary>
        /// displays the items in the specified array
        /// </summary>
        /// <param name="aListItems"></param>
        public void SetList(string[,] aListItems)
        {
            DataTable oDT = new DataTable();

            //add columns to data table
            for (int i = 0; i <= aListItems.GetUpperBound(1); i++)
                oDT.Columns.Add("Value" + (i+1).ToString());

            if (oDT.Columns.Count == 1)
            {
                //set column as both display and value
                base.DisplayMember = oDT.Columns[0].Caption;
                oDT.Columns.Add("Value2");
                base.ValueMember = oDT.Columns[1].Caption;
            }
            else if (oDT.Columns.Count == 2)
            {
                //set first column for display, second column for value
                base.DisplayMember = oDT.Columns[0].Caption;
                base.ValueMember = oDT.Columns[1].Caption;
            }
            else
            {
                //invalid list
                throw new LMP.Exceptions.ListException(
                    LMP.Resources.GetLangString("Error_InvalidComboboxListArray"));
            }

            //cycle through list item array, adding rows to data table
            for (int i = 0; i <= aListItems.GetUpperBound(0); i++)
            {
                DataRow oRow = oDT.Rows.Add();
                for (int j = 0; j <= aListItems.GetUpperBound(1); j++)
                {
                    //Replace line breaks in display column with printable characters
                    if (base.DisplayMember == oDT.Columns[j].Caption)
                    {
                        oRow[j] = aListItems[i, j].Replace("\r\n", "...");
                    }
                    else
                    {
                        oRow[j] = aListItems[i, j];
                    }
                }
            }

            //bind list to combobox
            base.DataSource = oDT;
            this.Invalidate();
        }
        public void SetList(string xListName)
        {
            //TODO: settle on an ITEM_SEP
            const char ITEM_SEP = '~';
            const char NAME_VALUE_SEP = ';';

            if (this.DesignMode || xListName == null || xListName == "")
                return;

            //if string does not have pipes,
            //assume it is a list name
            if (xListName.IndexOf(ITEM_SEP) == -1)
            {
                this.SetListName(xListName);
                return;
            }

            //otherwise, split into an array
            string[] aList = xListName.Split(ITEM_SEP);
            string[,] aList2Dimensions = new string[aList.Length, 2];
            string xValueMember = "";
            string xDisplayMember = "";

            //cycle through array
            for (int i = 0; i < aList.Length; i++)
            {
                //display member and value member
                //are separated by a semicolon
                //split the array item into two strings
                int iPos = aList[i].IndexOf(NAME_VALUE_SEP);
                if (aList[i] != "")
                {
                    if (aList[i].IndexOf(";") > 0)
                    {
                        xDisplayMember = aList[i].Substring(0, iPos);
                        xValueMember = aList[i].Substring(iPos + 1);
                    }
                    else
                    {
                        xDisplayMember = aList[i];
                        xValueMember = "";
                    }
                }

                //add values to aList2Dimensions
                aList2Dimensions[i, 0] = xDisplayMember;
                aList2Dimensions[i, 1] = xValueMember;
            }

            this.SetList(aList2Dimensions);

        }
        /// <summary>
        /// stores/loads the list with the specified name,
        /// based on the list load mode
        /// </summary>
        /// <param name="xListName"></param>
        private void SetListName(string xListName)
        {
            if (this.DesignMode || xListName == null || xListName == "")
                return;

            Lists oLists = new Lists();
            List oList = null;

            try
            {
                oList = (List)oLists.ItemFromName(xListName);
            }
            catch (System.Exception oE)
            {
                //invalid list
                throw new LMP.Exceptions.ListException(
                    LMP.Resources.GetLangString("Error_ListNotFound") + xListName, oE);
            }

            SetList(oList);
        }
        /// <summary>
        /// stores/loads the specified list
        /// </summary>
        /// <param name="oList"></param>
        public void SetList(List oList)
        {
            if (oList == null)
                throw new LMP.Exceptions.NullReferenceException(
                    LMP.Resources.GetLangString("Error_NullListToCombo"));

            m_oList = oList;
            LoadList();
        }
        /// <summary>
        /// loads the cached list into the combo
        /// </summary>
        public void LoadList()
        {
            DataTable oDT = m_oList.ItemDataTable;
            if (oDT.Columns.Count == 0)
            {
                //invalid list
                throw new LMP.Exceptions.ListException(
                    LMP.Resources.GetLangString("Error_NoListColumns") + m_oList.Name);
            }
            else
            {
                //Can't use DataTable as the DataSource, since we need to modify display column
                ArrayList oArray = m_oList.ItemArray;
                SetList(oArray);
            }
        }
        /// <summary>
        /// clears data bound list
        /// </summary>
        public void ClearList()
        {
            base.DataSource = null;
            base.Items.Clear();
            this.Invalidate();
        }
        protected override void OnValidating(CancelEventArgs e)
        {

            if (this.SelectedValue!=null && this.AllowEmptyValue)
                e.Cancel = false;
            else
                base.OnValidating(e);
        }
        #endregion
    }
}
