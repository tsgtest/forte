using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinTree;

namespace LMP.Controls
{
    public class TreeView: Infragistics.Win.UltraWinTree.UltraTree, IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        private string m_xValue;
        public event CIRequestedHandler CIRequested;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }
        public string Value
        {
            get { return m_xValue; }
            set{m_xValue = value;}
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************static methods*********************
        public static void SetAppearance(UltraTree oTreeView,
            System.Drawing.Color oBackcolor, GradientStyle oGradient)
        {
            // This methods centralizes the modification of the treeview appearance and allows for a single
            // db read for the user's backcolor and gradient style setting.

            // Set the backcolor
            oTreeView.Appearance.BackColor = oBackcolor;

            //set the gradient according to the user's preference.
            oTreeView.Appearance.BackGradientStyle = oGradient;
        }
        #endregion
        #region *********************protected members*********************
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            try
            {
                if (e.KeyValue == 18)
                    e.SuppressKeyPress = true;
                else if (e.KeyCode == Keys.Tab)
                {
                    if (this.TabPressed != null)
                        this.TabPressed(this, new TabPressedEventArgs(e.Shift));
                }
                else
                    base.OnKeyDown(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
		/// <summary>
		/// ensures that a processed tab (processed in ProcessKeyMessage)
		/// won't continue to be processed
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
		protected override bool ProcessKeyEventArgs(ref Message m)
		{
			if(m.WParam.ToInt32() == (int) Keys.Tab)
				return true;
			else
				return base.ProcessKeyEventArgs (ref m);
		}
		/// <summary>
		/// processes tab and shift-tab key messages -
		/// this method will be executed only when the
		/// message pump is broken, ie when the hosing
		/// form's ProcessDialogKey method is not run.
		/// this seems to happen when when tabbing from
		/// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        #endregion

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TreeView
            // 
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
