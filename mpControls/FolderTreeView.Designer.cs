namespace LMP.Controls
{
    partial class FolderTreeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderTreeView));
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Fuchsia;
            this.ilImages.Images.SetKeyName(0, "");
            this.ilImages.Images.SetKeyName(1, "");
            this.ilImages.Images.SetKeyName(2, "");
            this.ilImages.Images.SetKeyName(3, "SavedData.bmpTransparent.bmp");
            this.ilImages.Images.SetKeyName(4, "ParagraphTextSegment");
            this.ilImages.Images.SetKeyName(5, "");
            this.ilImages.Images.SetKeyName(6, "");
            this.ilImages.Images.SetKeyName(7, "DataInterview");
            this.ilImages.Images.SetKeyName(8, "DataPacket");
            this.ilImages.Images.SetKeyName(9, "UserStylesheet.bmp");
            this.ilImages.Images.SetKeyName(10, "UserPrefill.bmp");
            this.ilImages.Images.SetKeyName(11, "UserAnswerFileSegment.bmp");
            this.ilImages.Images.SetKeyName(12, "UserDocument.ico");
            this.ilImages.Images.SetKeyName(13, "UserParagraphText");
            this.ilImages.Images.SetKeyName(14, "SentenceTextSegment");
            this.ilImages.Images.SetKeyName(15, "UserSentenceText");
            this.ilImages.Images.SetKeyName(16, "SearchResults");
            this.ilImages.Images.SetKeyName(17, "Bullet");
            this.ilImages.Images.SetKeyName(18, "Transparent");
            this.ilImages.Images.SetKeyName(19, "WordDocument");
            this.ilImages.Images.SetKeyName(20, "ExternalContent");
            this.ilImages.Images.SetKeyName(21, "XLDocument");
            this.ilImages.Images.SetKeyName(22, "MasterDataForm");
            // 
            // FolderTreeView
            // 
            this.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HideSelection = false;
            this.ImageList = this.ilImages;
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.Override = _override1;
            this.AfterActivate += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.FolderTreeView_AfterActivate);
            this.AfterCheck += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.FolderTreeView_AfterCheck);
            this.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.FolderTreeView_AfterSelect);
            this.BeforeCollapse += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.FolderTreeView_BeforeCollapse);
            this.BeforeExpand += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.FolderTreeView_BeforeExpand);
            this.Scroll += new Infragistics.Win.UltraWinTree.TreeScrollEventHandler(this.FolderTreeView_Scroll);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FolderTreeView_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ilImages;




    }
}
