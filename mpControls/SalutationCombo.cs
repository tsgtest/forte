using System;
using System.Collections.Generic;
using System.Collections; 
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinEditors;
using System.Xml; 
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.Controls
{
    public partial class SalutationCombo : UserControl, IControl, ICIable
    {
        #region *********************fields*********************
        private string m_xSchema;
        private bool m_bAllowDropdown = false;
        private mpSalSelectionTypes m_iSelectionType;
        private bool m_bIsDirty;
        private object m_oTag2;
        private int m_iDefaultListIndex;
        private int m_iCurrentIndex = -1;
        private bool m_bCanRequestCI;
        private ArrayList m_aContacts;
        private string m_xContactSource = "";

        private const char ITEM_SEP = LMP.StringArray.mpEndOfSubField;

        #endregion

        #region *********************enumerations*********************
        public enum mpSalSelectionTypes
        {
            Content = 0,
            EndOfSalutation = 1,
            StartOfSalutation = 2
        }
        #endregion

        #region *********************constructors*********************
        public SalutationCombo()
        {
            InitializeComponent();
            SetupControl();
        }

		#endregion

		#region *********************properties*********************

        public override bool AutoSize
        {
            get
            {
                return base.AutoSize;
            }
            set
            {
                base.AutoSize = value;
                this.ultraComboEditor1.AutoSize = value;
            }
        }
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets whether dropdown list enabled for Salutation Combo.")]
        public bool AllowDropdown
        {
            get{return m_bAllowDropdown;}
            set {m_bAllowDropdown = value;}
        }
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets how control text is selected when control gains focus.")]
        public mpSalSelectionTypes SelectionType
        {
            get { return m_iSelectionType; }
            set { m_iSelectionType = value; }
        }
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets the salutation string ")]
        public string Schema
        {
            set
            {
                if (m_xSchema != value)
                {
                    m_xSchema = value;
                    if (this.Contacts != null)
                        FillSchema();
                }
            }
            get
            {
                return m_xSchema;
            }
        }

        public override System.Drawing.Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
                this.ultraComboEditor1.BackColor = value;
            }
        }
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets the font used in the control.")]
        public override Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
                this.ultraComboEditor1.Font = value;
			}
		}
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets the default list item selected in the dropdown when it is populated.")]
        public int DefaultListIndex
        {
            get { return m_iDefaultListIndex; }
            set
            {
                int iRows = this.ultraComboEditor1.Items.Count - 1;
                //ensure value to be set is valid, i.e less than number of rows
                if (value > iRows)
                    m_iDefaultListIndex = 0;
                else
                    m_iDefaultListIndex = value;
            }
        }
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets whether control can request CI session.")]
        public bool CanRequestCI
        {
            get { return m_bCanRequestCI; }
            set { m_bCanRequestCI = value; }
        }
        /// <summary>
        /// hold Contacts collection instance
        /// </summary>
        private ArrayList Contacts
        {
            get { return m_aContacts; }
            set {m_aContacts = value; }
        }
        #endregion

		#region *********************methods*********************
        /// <summary>
        /// sets default appearance, loads properties
        /// </summary>
        private void SetupControl()
        {
            this.Height = 22;
        }
        /// <summary>
        /// overloaded method - suppresses dropdown if
        /// there is only one row to be loaded into the schema
        /// dropdown
        /// </summary>
        public void ShowDropdown()
        {
            bool bShow;

            //if data table contains only one item, do not show dropdown button
            //no matter what the AllowDropdown property is

            DataTable oDT = (DataTable)ultraComboEditor1.DataSource;

            if (oDT == null)
                bShow = false;
            else
            {
                if (oDT.Rows.Count < 2)
                    bShow = false;
                else if (m_aContacts != null)
                {
                    bShow = m_aContacts.Count > 0;
                    foreach (string xContact in m_aContacts)
                        //Don't show dropdown if there are manually entered contacts
                        if (xContact == "0" || xContact == "")
                        {
                            bShow = false;
                            break;
                        }
                }
                else
                    bShow = false;
            }
            ShowDropdown(bShow);
        }
        /// <summary>
        /// hides/displays combo dropdown button depending on value of 
        /// AllowDropdown property
        /// </summary>
        /// <param name="bShow"></param>
        public void ShowDropdown(bool bShow)
        {
            //AllowDropdown property trumps bShow == true
            if (bShow && this.AllowDropdown == false)
                bShow = false;

            //configure dropdown button based on AllowDropdown property of control
            Infragistics.Win.ButtonDisplayStyle oBDS = new Infragistics.Win.ButtonDisplayStyle();
            if (bShow && this.AllowDropdown)
                oBDS = Infragistics.Win.ButtonDisplayStyle.Always;
            else
                oBDS = Infragistics.Win.ButtonDisplayStyle.Never;

            this.ultraComboEditor1.DropDownButtonDisplayStyle = oBDS;
        }
        /// <summary>
        /// overloaded method takes ArrayList for listitems
        /// </summary>
        /// <param name="aList"></param>
        public void SetList(ArrayList aList)
        {
            string[] aTemp = new string[aList.Count];
            for (int i = 0; i < aList.Count; i++)
            {
                //create string array and call set list to load
                aTemp[i] = (aList[i]).ToString();
            }
            SetList(aTemp);
        }
        /// <summary>
        /// populates the combo list with items in the specified System.Array
        /// </summary>
        /// <param name="aList"></param>
        public void SetList(Array aList)
        {
            string[,] aTemp = new string[aList.GetUpperBound(0) + 1, 1];

            for (int i = 0; i <= aList.GetUpperBound(0); i++)
            {
                //populate temp array row
                aTemp[i, 0] = aList.GetValue(i, 0).ToString();
            }

            SetList(aTemp);
        }
        /// <summary>
        /// displays the items in the specified array
        /// </summary>
        /// <param name="aListItems"></param>
        public void SetList(string[] aListItems)
        {
            //all is well, create DataTable and load items
            DataTable oDT = new DataTable();

            //add column to data table
            oDT.Columns.Add("Column1");

            if (oDT.Columns.Count == 1)
            {
                //set column as both display and value
                this.ultraComboEditor1.DisplayMember = oDT.Columns[0].Caption;
                this.ultraComboEditor1.ValueMember = oDT.Columns[0].Caption;
            }
            else
            {
                //invalid list
                throw new LMP.Exceptions.ListException(
                    LMP.Resources.GetLangString("Error_InvalidComboboxListArray"));
            }

            //cycle through list item array, adding rows to data table
            for (int i = 0; i <= aListItems.GetUpperBound(0); i++)
            {
                DataRow oRow = oDT.Rows.Add();
                oRow[0] = aListItems[i];
            }

            //bind list to combobox
            this.ultraComboEditor1.DataSource = oDT;

            //set list index

            if (m_iCurrentIndex == -1)
                this.ultraComboEditor1.SelectedIndex = this.DefaultListIndex;
            else
                this.ultraComboEditor1.SelectedIndex = m_iCurrentIndex;

            //GLOG 3267: Make sure Control Value reflects latest change
            this.ultraComboEditor1.Text = aListItems[m_iCurrentIndex];
            this.Invalidate();
        }
        /// <summary>
        /// creates array from delimited list, calls overloaded method to load list
        /// </summary>
        /// <param name="xDelimitedList"></param>
        public void SetList(string xDelimitedList)
        {
            if (this.DesignMode || xDelimitedList == null || xDelimitedList == "")
                return;

            //otherwise, split into an array
            string[] aList = xDelimitedList.Split(ITEM_SEP);
            this.SetList(aList);
        }
        /// <summary>
        /// constructs array of filled schema items for each retrieved contact
        /// </summary>
        private void FillSchema()
        {
            DateTime t0 = DateTime.Now;

            LMP.Trace.WriteInfo("FillSchema");

            if (this.Schema == null || this.Schema == "")
                return;

            LMP.Data.Application.StartCIIfNecessary();
            ArrayList oFilledList = new ArrayList();
            int iCount = 1;
            int iCICount = Contacts.Count;
            string xSalPrefix = null;
            string xSalSuffix = null;
            bool bInvalidUnid = false;

            //create array of CI tokens from schema string
            string[,] aCITokens = FillCITokenArray();
            int iTokens = aCITokens.GetUpperBound(0);

            //convert schema string to array
            string[] xSchemaToFill = this.Schema.Split(ITEM_SEP);
            
            //cycle through contacts collection, adding detail for each to schema tokens
            foreach (string xUNID in this.Contacts)
            {
                string xField = "";
                string xCIToken = "";
                string xCIValue = "";
                string xScheme = "";
                string xSep = "";

                //cycle through schema array and fill with detail for designated contact
                for (int i = 0; i <= xSchemaToFill.GetUpperBound(0); i++)
                {
                    xScheme = xSchemaToFill[i];

                    if (xScheme.IndexOf("<") > -1 && xScheme.IndexOf(">") > -1)
                    {
                        //capture and remove Salutation prefix and suffix from schema
                        //these are the prefix and suffix of entire salutation, ie. "Dear" and ":"
                        xSalPrefix = xScheme.Substring(0, xScheme.IndexOf("<"));
                        xSalSuffix = xScheme.Substring(xScheme.LastIndexOf(">") + 1);

                        if (xSalPrefix != "")
                            xScheme = xScheme.Replace(xSalPrefix, "");
                        if (xSalSuffix != "")
                            xScheme = xScheme.Replace(xSalSuffix, "");

                        //Get CI fields from token array & use to retrieve CI values
                        for (int j = 0; j <= iTokens; j++)
                        {
                            xCIToken = aCITokens[j, 0];
                            xField = aCITokens[j, 1];
                            try
                            {
                                xCIValue = Dialog.GetCIDetail(xUNID, xField, LMP.Data.Application.CISession);
                            }
                            catch 
                            {
                                //If UNID is invalid (source Contact has been deleted or originated on a different system)
                                //handle as though there were a manually entered Contact
                                bInvalidUnid = true;
                                break;
                            }

                            //for each token value pair, fill the modified schema string with retrieved detail
                            //GLOG : 7836 : ceh - only remove extra space when token was present
                            if (xScheme.Contains(xCIToken))
                            {
                                xScheme = xScheme.Replace(xCIToken, xCIValue);

                                //remove extra spaces resulting from empty CI value
                                if (xCIValue == "" || xCIValue == null)
                                    xScheme = xScheme.Replace(" ", "");
                            }
                        }

                        //for the first contact, add new members to filled dropdown list
                        //thereafter, modify existing list members, i.e. "Dear XXX:" becomes "Dear XXX and YYY:"
                        //each member corresponds to a member of the filled dropdown list members

                        if (iCount == 1)
                        {
                            oFilledList.Add(xSalPrefix + xScheme);
                            if (iCount == iCICount)
                                oFilledList[i] = oFilledList[i] + xSalSuffix;
                        }
                        else
                        {
                            //set appropriate separator depending on number of contacts
                            //and the actual contact we're working with
                            if (iCICount > 2 && iCount <= iCICount - 1)
                                xSep = ", ";
                            else if (iCount == iCICount)
                                xSep = " and ";

                            //for the last contact, append the captured suffix to the end of the string
                            string xSuffix = "";

                            if (iCount == iCICount)
                                xSuffix = xSalSuffix;

                            //modify the list member = previous value + sep + filled schema for the 
                            //designated contact, plus suffix if contact is the last in the collection
                            oFilledList[i] = oFilledList[i] + xSep + xScheme + xSuffix;
                        }
                    }
                    else if (iCount == 1)
                    {
                        //Add item with no CI tokens
                        oFilledList.Add(xScheme);
                    }
                }
                iCount++;
            }
            
            if (!bInvalidUnid)
                //load filled schema lists to dropdown list
                SetList(oFilledList);

            //configure dropdown depending on number of list members
            ShowDropdown(oFilledList.Count > 1 && !bInvalidUnid);

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// set contacts property for control
        /// calls FillSchema to fills dropdown with list of filled schema items
        /// </summary>
        /// <param name="oContacts"></param>
        /// <param name="bAppend"></param>
        public void SetContacts(ICContacts oNewContacts, ciContactTypes iContactType, bool bAppend)
        {
            //return if no contacts passed in
            if (oNewContacts == null || oNewContacts.Count() == 0)
                return;

            //create new contacts collection if
            //we're not appending, or if there's
            //no existing collection
            if (!bAppend || this.Contacts == null)
                this.Contacts = new ArrayList();

            //cycle through all new contacts,
            //adding each to end of existing collection
            for (int i = 1; i <= oNewContacts.Count(); i++)
            {
                //get contact
                object oIndex = (object)i;
                ICContact oContact = oNewContacts.Item(oIndex);

                //add to collection only if contact is the same type as requested
                if (oContact.AddressTypeName.ToLower() == iContactType.ToString())
                {
                    this.Contacts.Add(oContact.UNID);
                }
                oContact = null;
            }

            //fill dropdown with list schema items filled with detail for each contact
            FillSchema();
        }
        /// <summary>
        /// Release the Contacts object
        /// </summary>
        public void ClearContacts()
        {
            if (m_aContacts != null)
            {
                m_aContacts = null;
            }
        }
        #endregion

        #region *********************procedures*********************
        /// <summary>
        /// fills array containing MacPac CI field token, CI field string 
        /// </summary>
        /// <returns>returns number of field, ie. number of CI tokens in salutation schema</returns>
        private string[,] FillCITokenArray()
        {
            //create array of CI tokens in schema, and field names parsed from tokens
            int iTokens = LMP.String.CountChrs(this.Schema, "<");
            string[,]aValues = new string[iTokens, 2];
            string xSchema = this.Schema;
            string xTokenValue = xSchema;

            //parse schema, populate array with token name, CI field name, buffer for value
            for (int i = 0; i < iTokens; i++)
            {
                //parse CI token
                xTokenValue = xSchema.Substring(xSchema.IndexOf("<"));
                xTokenValue = xTokenValue.Substring(0, xTokenValue.IndexOf(">") + 1);
                aValues[i, 0] = xTokenValue;
                aValues[i, 1] = xTokenValue;

                //parse CI field name - format as CI field string, i.e. <FIELDNAME>
                //xTokenValue = xTokenValue.Substring(xTokenValue.IndexOf("_") + 1);
                //xTokenValue = xTokenValue.Substring(0, xTokenValue.IndexOf(">"));
                //aValues[i, 1] = "<" + xTokenValue.ToUpper() + ">";

                xSchema = xSchema.Substring(xSchema.IndexOf(">") + 1);
            }
            return aValues;
        }
       
        #endregion

        #region *********************internal procedures*********************
        #endregion

        #region *********************helper classes*********************
        /// <summary>
        /// helper class defining SchemaItem type which can hold name value pairs
        /// or UNID string plus array lists containing SchemaItems
        /// </summary>
        private class SchemaItem
        {
            public string UNID;
            public string CIToken;
            public string CIValue = "";
            public ArrayList TokenArray = new ArrayList();

            public SchemaItem(string xUNID, ArrayList aTokenArray)
            {
                this.UNID = xUNID;
                this.TokenArray  = aTokenArray;
            }
            public SchemaItem(ArrayList aTokenArray)
            {
                this.TokenArray = aTokenArray;
            }
            public SchemaItem(string xCIToken, string xCIValue)
            {
                this.CIToken  = xCIToken;
                this.CIValue = xCIValue;
            }
        }
        #endregion

        #region *********************event handlers*********************
        /// <summary>
        /// Track the last selected dropdown item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraComboEditor1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                m_iCurrentIndex = ultraComboEditor1.SelectedIndex;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles various resizing issues
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SalutationCombo_Resize(object sender, EventArgs e)
        {
            //this.ultraComboEditor1.Width = this.Width;
        }
        /// <summary>
        /// assigns IsDirty flag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraComboEditor1_TextChanged(object sender, EventArgs e)
        {
            this.IsDirty = true;

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }
        /// <summary>
        /// handles selection of text based on SelectionType property
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraComboEditor1_AfterEnterEditMode(object sender, System.EventArgs e)
        {
            try
            {
                switch (this.SelectionType)
                {
                    case mpSalSelectionTypes.Content:
                        ultraComboEditor1.SelectAll();
                        break;
                    case mpSalSelectionTypes.EndOfSalutation:
                        //select directly before the ':' or ','
                        //if those are the trailing chars
                        string xChr = ultraComboEditor1.Text;
                        if (xChr.Length != 0)
                            xChr = xChr.Substring(xChr.Length - 1, 1);
                        ultraComboEditor1.SelectionLength = 0;
                        if (xChr == ":" || xChr == ",")
                            ultraComboEditor1.Editor.SelectionStart = ultraComboEditor1.Text.Length - 1;
                        else
                            ultraComboEditor1.Editor.SelectionStart = ultraComboEditor1.Text.Length;
                        break;
                    case mpSalSelectionTypes.StartOfSalutation:
                    default:
                        ultraComboEditor1.Editor.SelectionStart = 0;
                        ultraComboEditor1.Editor.SelectionLength = 0;
                        break;
                }
            }

            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires CIRequested event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraComboEditor1_AfterDropDown(object sender, System.EventArgs e)
        {
            try
            {
                //do not raise event if control is configured not to allow CI
                if (this.CanRequestCI == false)
                    return;
                if (this.CIRequested != null)
                    this.CIRequested(this, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void ultraComboEditor1_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Tab)
            {
                //Handle tab key
                bool bShift = e.Shift;
                if (this.TabPressed != null)
                {
                    this.TabPressed(this, new TabPressedEventArgs(bShift));
                    //Suppress to avoid duplicate events
                    e.SuppressKeyPress = true;
                }
            }
            else
            {
                if (this.KeyPressed != null)
                    this.KeyPressed(this, e);
            }
            
            base.OnKeyDown(e);
        }

        private void ultraComboEditor1_KeyUp(object sender, KeyEventArgs e)
        {
            base.OnKeyUp(e);

            if (this.KeyReleased != null)
                this.KeyReleased(this, e);
        }
        private void SalutationCombo_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Rectangle oRect = new Rectangle(0, 0, this.Width - 1, this.Height - 1);
                e.Graphics.DrawRectangle(new Pen(Brushes.LightBlue), oRect);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void SalutationCombo_Leave(object sender, EventArgs e)
        {
            base.OnLostFocus(e);
        }

        private void SalutationCombo_Enter(object sender, EventArgs e)
        {
            base.OnGotFocus(e);
        }
        #endregion

        #region *********************protected members*********************
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        /// <summary>
        /// support for tabbing for MacPac controls
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab || keyData == Keys.Return)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (m.WParam.ToInt32() == (int)Keys.Return)
            {
                // Process Enter here, because KeyDown doesn't get raised for this
                // in TaskPane
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        #endregion

        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        /// <summary>
        /// 
        /// </summary>
        public void ExecuteFinalSetup()
        {
            try
            {
                LMP.Trace.WriteInfo("ExecuteFinalSetup");
                //m_bControlIsLoaded = true;
                SetupControl();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get
            {
                return (string)this.ultraComboEditor1.Text;
            }
            set
            {
                try
                {

                    if (value == "" || value == null)
                        ultraComboEditor1.SelectedIndex = 0;
                    else
                        ultraComboEditor1.Text = value;
                    this.IsDirty = true;

                    //configure dropdown
                    ShowDropdown();
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the Dirty flag indicating the value has changed.")]
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get 
            { 
                return m_bIsDirty; 
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Secondary user-defined object associated with the control.")]
        //TODO: what is this used for?
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        /// <summary>
        /// GLOG 2645: Stores the Current List Index and Contacts List between sessions
        /// </summary>
        public string SupportingValues
        {
            get 
            {
                string xTemp = "";
                xTemp = m_iCurrentIndex.ToString() + StringArray.mpEndOfSubValue;
                if (m_aContacts != null && m_aContacts.Count > 0)
                {
                    foreach (string xID in m_aContacts)
                    {
                        xTemp = xTemp + xID + StringArray.mpEndOfSubField;
                    }
                }
                return xTemp;
            }
            set 
            {
                try
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        //Parse DefaultListIndex and Contacts from value
                        string[] aItems = value.Split(StringArray.mpEndOfSubValue);
                        if (aItems.GetUpperBound(0) >= 0)
                            m_iCurrentIndex = Int32.Parse(aItems[0]);
                        //Get list of UNIDs only if Contacts Array is not already set -
                        //this might have been set by the ContactSource property
                        if (aItems.GetUpperBound(0) >= 1 && aItems[1] != "" && m_aContacts == null)
                        {
                            m_aContacts = new ArrayList();
                            string[] aUnids = aItems[1].Split(StringArray.mpEndOfSubField);
                            for (int i = 0; i < aUnids.GetLength(0); i++)
                            {
                                if (!string.IsNullOrEmpty(aUnids[i]))
                                    m_aContacts.Add(aUnids[i]);
                            }
                            FillSchema();
                        }
                    }
                }
                catch (System.Exception oE)
                {
                    throw oE;
                }
            }
        }
        /// <summary>
        /// GLOG 2645: Extracts list of Contact UNIDs from Detail XML
        /// </summary>
        public string ContactSource
        {
            set
            {
                if (this.DesignMode)
                    return;

                try
                {
                    if (value.Trim() == "")
                    {
                        m_xContactSource = "";
                        m_aContacts = null;
                        ShowDropdown(false);
                    }
                    else if (m_xContactSource != value)
                    {
                        bool bManualEntry = false;
                        bool bContactsChanged = false; //GLOG 4833
                        ArrayList aContacts = new ArrayList();
                        XmlDocument oXML = new System.Xml.XmlDocument();
                        oXML.LoadXml(string.Concat("<zzmpD>", value, "</zzmpD>"));

                        string xFieldName = oXML.DocumentElement.ChildNodes[0].Name;
                        //Get all nodes matching first name so there's one for each UNID
                        XmlNodeList oNodeList = oXML.DocumentElement.SelectNodes(xFieldName);
                        foreach (XmlNode oNode in oNodeList)
                        {
                            //get UNID, if one exists
                            string xUNID = "";
                            try
                            {
                                xUNID = oNode.Attributes.GetNamedItem("UNID").Value;
                            }
                            catch { }

                            if (xUNID == "" || xUNID == "0")
                            {
                                aContacts.Add("0");
                                bManualEntry = true;
                            }
                            else
                                aContacts.Add(xUNID);
                        }
                        //GLOG 4833: Don't repopulate list unless underlying
                        //Contact UNIDs have changed
                        if (m_aContacts != null)
                        {
                            if (aContacts.Count != m_aContacts.Count)
                                bContactsChanged = true;
                            else
                            {
                                for (int i = 0; i < aContacts.Count; i++)
                                {
                                    if (aContacts[i].ToString() != m_aContacts[i].ToString())
                                    {
                                        bContactsChanged = true;
                                        break;
                                    }

                                }
                            }
                        }
                        else
                            bContactsChanged = true;
                        m_aContacts = aContacts;
                        m_xContactSource = value;
                        //If any entries are not from CI, disable dropdown
                        if (!bManualEntry)
                        {
                            if (bContactsChanged)
                                FillSchema();
                        }
                        else
                            ShowDropdown(false);
                    }
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.XMLException(LMP.Resources.GetLangString("Error_InvalidDetailXML"), oE);
                }
            }
        }
        #endregion

        #region *********************ICIable members*********************
        public event CIRequestedHandler CIRequested;
        public bool IsSubscribedToCI
        {
            get
            {
                if ((System.Delegate)this.CIRequested != null)
                {
                    return this.CIRequested.GetInvocationList().Length > 0;
                }
                else
                {
                    return false;
                }
            }
        }
        public int ContactCount
        {
            get { return m_aContacts.Count; }
        }

        #endregion
    }
}
