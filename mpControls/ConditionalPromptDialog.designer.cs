namespace LMP.Controls
{
    partial class ConditionalPromptDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConditionalPromptDialog));
            this.lblPrompt = new System.Windows.Forms.Label();
            this.chkSkipPrompt = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.pbExclamation = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbExclamation)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPrompt
            // 
            this.lblPrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrompt.Location = new System.Drawing.Point(59, 17);
            this.lblPrompt.Name = "lblPrompt";
            this.lblPrompt.Size = new System.Drawing.Size(317, 60);
            this.lblPrompt.TabIndex = 0;
            this.lblPrompt.Text = "PROMPT TEXT";
            this.lblPrompt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkSkipPrompt
            // 
            this.chkSkipPrompt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkSkipPrompt.AutoSize = true;
            this.chkSkipPrompt.Location = new System.Drawing.Point(19, 93);
            this.chkSkipPrompt.Name = "chkSkipPrompt";
            this.chkSkipPrompt.Size = new System.Drawing.Size(125, 17);
            this.chkSkipPrompt.TabIndex = 1;
            this.chkSkipPrompt.Text = "Do not prompt again.";
            this.chkSkipPrompt.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(301, 89);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // pbExclamation
            // 
            this.pbExclamation.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbExclamation.BackgroundImage")));
            this.pbExclamation.Location = new System.Drawing.Point(12, 12);
            this.pbExclamation.Name = "pbExclamation";
            this.pbExclamation.Size = new System.Drawing.Size(41, 39);
            this.pbExclamation.TabIndex = 3;
            this.pbExclamation.TabStop = false;
            // 
            // ConditionalPromptDialog
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(388, 117);
            this.Controls.Add(this.pbExclamation);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.chkSkipPrompt);
            this.Controls.Add(this.lblPrompt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConditionalPromptDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GenericPrompt";
            ((System.ComponentModel.ISupportInitialize)(this.pbExclamation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPrompt;
        private System.Windows.Forms.CheckBox chkSkipPrompt;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.PictureBox pbExclamation;
    }
}