using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using LMP.Data;

namespace LMP.Controls
{
    public class LocationsDropdown:System.Windows.Forms.ComboBox, LMP.Controls.IControl
    {
        #region *********************enums*********************
        public enum mpLocationFields
        {
            Name = 1,
            ID = 2
        }
        #endregion
        #region *********************fields*********************
        private mpLocationTypes m_iLocationType = mpLocationTypes.Countries;
        private int m_iLocationParentID;
        private bool m_bProgrammaticallySettingValue;
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private bool m_bNavigating = false;
        private bool m_bMatching = false;
        private bool m_bPopulatingList = false;
        private string m_xSupportingValues = "";
        //GLOG 3563
        private string m_xTextValue = "";
        #endregion
        #region *********************constructors*********************
        public LocationsDropdown()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************properties*********************
        [Description("Gets/sets the type of locations displayed in the list.")]
        public mpLocationTypes LocationType
        {
            get { return m_iLocationType; }
            set
            {
                m_iLocationType = value;
            }
        }
        [Description("Get/sets the ID of the parent location, which filters the list accordingly.")]
        public int LocationParentID
        {
            get { return m_iLocationParentID; }
            set
            {
                if (m_iLocationParentID != value)
                {
                    m_iLocationParentID = value;

                    if (m_iLocationParentID != 0)
                    {
                        //we're changing the parent-
                        //change the list
                        RefreshList();
                    }
                }
            }
        }
        [Description("Get/sets the Location field that will be used for the value of the control.")]
        public mpLocationFields ValueField
        {
            get
            {
                if (this.ValueMember == "Name")
                    return mpLocationFields.Name;
                else
                    return mpLocationFields.ID;
            }
            set
            {
                if (value == mpLocationFields.Name)
                    this.ValueMember = "Name";
                else
                    this.ValueMember = "ID";
            }
        }
        #endregion
        #region *********************private methods*********************
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LocationsDropdown
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Size = new System.Drawing.Size(121, 21);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.LocationsDropdown_Validating);
            this.Resize += new System.EventHandler(this.LocationsDropdown_Resize);
            this.SelectedIndexChanged += new System.EventHandler(this.LocationsDropdown_SelectedIndexChanged);
            this.Leave += new System.EventHandler(this.LocationsDropdown_Leave);
            this.Enter += new System.EventHandler(this.LocationsDropdown_Enter);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LocationsDropdown_KeyUp);
            this.SelectedValueChanged += new System.EventHandler(this.LocationsDropdown_SelectedValueChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LocationsDropdown_KeyDown);
            this.TextChanged += new System.EventHandler(this.LocationsDropdown_TextChanged);
            this.ResumeLayout(false);

        }
        /// <summary>
        /// refreshes the list displayed in the control
        /// </summary>
        private void RefreshList()
        {
            m_bPopulatingList = true;
            try
            {
                List oList = (List)(new Lists()).Create();

                switch (m_iLocationType)
                {
                    //GLOG 5439: Sort list in alpha order by Name
                    case mpLocationTypes.Countries:
                        oList.SQL = "SELECT Name, ID FROM Countries ORDER BY Name";
                        break;
                    case mpLocationTypes.States:
                        //if (this.LocationParentID != 0)
                        oList.SQL = "SELECT Name, ID FROM States WHERE CountryID=" + this.LocationParentID + " ORDER BY Name";
                        break;
                    case mpLocationTypes.Counties:
                        oList.SQL = "SELECT Name, ID FROM Counties WHERE StateID=" + this.LocationParentID + " ORDER BY Name";
                        break;
                }

                this.DisplayMember = "Name";
                if (this.ValueField == mpLocationFields.Name)
                    this.ValueMember = "Name";
                else
                    this.ValueMember = "ID";

                this.DataSource = oList.ItemDataTable;
                this.Invalidate();
                this.SelectedIndex = -1;
                this.Text = "";
            }
            finally
            {
                m_bPopulatingList = false;
            }
        }
        /// <summary>
        /// autocompletes the combo entry
        /// </summary>
        private bool MatchEntry(string xEntry)
        {
            try
            {
                if (this.Items.Count == 0)
                    return false;
                else if (string.IsNullOrEmpty(xEntry))
                {
                    this.SelectedIndex = -1;
                    return false;
                }

                m_bMatching = true;

                //get the number of characters in the text portion
                int iCharCount = xEntry.Length;

                //find a match in the list
                int iMatchIndex = this.FindString(xEntry);

                if (iMatchIndex > -1)
                {
                    //GLOG 3662: Force update of Selected Index if necessary
                    //Otherwise text won't get updated if matching entry is first in list
                    m_bProgrammaticallySettingValue = true;
                    if (this.SelectedIndex == iMatchIndex)
                        this.SelectedIndex = -1;
                    //there is a match - select the list item
                    this.SelectedIndex = iMatchIndex;
                    System.Diagnostics.Debug.WriteLine(this.SelectedIndex.ToString());
                    //select untyped characters
                    this.SelectionStart = iCharCount;
                    this.SelectionLength = this.Text.Length - iCharCount;
                }
                else
                {
                    //remove last char - attempt to match
                    this.Text = this.Text.Substring(0, this.Text.Length - 1);
                    MatchEntry(this.Text);
                }
            }
            finally
            {
                m_bMatching = false;
                m_bProgrammaticallySettingValue = false;
            }

            return false;
        }
        private bool MatchEntry()
        {
            return MatchEntry(this.Text);
        }

        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public virtual void ExecuteFinalSetup()
        {
            RefreshList();
        }
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public virtual string Value
        {
            get
            {
                //refresh selectedindex property with stored value
                //this is a bug in windows.system.forms.combobox
                if (this.SelectedIndex == -1)
                    this.SelectedIndex = this.SelectedIndex;

                //get accessibility object value - try block was added 8/19/08 due to
                //error in Word 2003 only when closing document with LocationsDropdown
                //selected
                string xAccValue = null;
                try
                {
                    xAccValue = this.AccessibilityObject.Value;
                }
                catch { }

                if (this.ValueField == mpLocationFields.Name && !string.IsNullOrEmpty(xAccValue))
                    //SelectedValue won't work here if selected item was matched from keyboard instead of clicked
                    return this.AccessibilityObject.Value;
                else if (this.ValueField == mpLocationFields.ID && this.SelectedValue != null)
                    return this.SelectedValue.ToString();
                else if (this.ValueField == mpLocationFields.Name)
                    return "";
                else
                    return "0";
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    //clear selected text
                    this.SelectedIndex = -1;
                    this.Text = "";
                    this.IsDirty = true;
                }
                else
                {
                    m_bProgrammaticallySettingValue = true;
                    if (this.ValueField == mpLocationFields.ID)
                        this.SelectedValue = value;
                    else
                    {
                        this.Text = value;
                    }
                    m_bProgrammaticallySettingValue = false;
                }
                //GLOG 3563
                m_xTextValue = this.Text;
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }

            //TODO: this needs to be implemented
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************event handlers*********************
        private void LocationsDropdown_Validating(object sender, CancelEventArgs e)
        {
            //ensure that there is a match with a list item
            int iMatchIndex = this.FindStringExact(this.Text);

            if (iMatchIndex == -1 && (this.Text != ""))
            {
                //there is no match
                e.Cancel = true;

                //select text
                this.SelectionStart = 0;
                this.SelectionLength = this.Text.Length;

                //alert
                MessageBox.Show(LMP.Resources.GetLangString("Msg_LimitedToList"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //ensure that match is selected
                this.SelectedIndex = iMatchIndex;
            }

        }
        private void LocationsDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.IsDirty = true;
            //GLOG 3563
            m_xTextValue = this.Text;
        }
        private void LocationsDropdown_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //remove MacPac reserved characters from control -
                //we do this here, as opposed to a KeyDown-type
                //event so that we can capture pasting of reserved characters
                if (Dialog.ContainsMacPacReservedChars(this.Text))
                {
                    //get current insertion point position
                    int iSelStart = this.SelectionStart;

                    //remove reserved chars
                    this.Text = Dialog.RemoveMacPacReservedChars(this.Text);

                    //return insertion point
                    this.SelectionStart = iSelStart - 1;
                }

                //don't execute handler code if this handler
                //was triggered by programmatically 
                //setting the value of the handler or loading the list - it
                //should only run when text is changed by user
                if (m_bProgrammaticallySettingValue || m_bPopulatingList)
                    return;

                //need to reference the accessibility object instead of the text
                //property because of bug in System.Windows.Forms.ComboBox -
                //see KB article #814346
                if (string.IsNullOrEmpty(this.Text))
                    this.SelectedIndex = -1;
                else if (!m_bMatching && !m_bNavigating && this.AccessibilityObject.Value != null)
                    this.MatchEntry(this.AccessibilityObject.Value);

                this.IsDirty = true;
                //GLOG 3563
                m_xTextValue = this.Text;
                if (this.ValueChanged != null)
                    this.ValueChanged(this, new System.EventArgs());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void LocationsDropdown_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.IsDirty = true;
                //GLOG 3563
                m_xTextValue = this.Text;

                if (this.ValueChanged != null)
                    this.ValueChanged(this, new System.EventArgs());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void LocationsDropdown_Resize(object sender, EventArgs e)
        {
            try
            {
                this.Height = 21;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void LocationsDropdown_Enter(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && this.Visible)
                {
                    //GLOG 3563: Make sure text is blank if value hasn't been explicitly set
                    this.Text = m_xTextValue;
                    Dialog.EnsureSelectedContent(this, false, false);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void LocationsDropdown_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (this.DesignMode)
                    return;

                if ((e.KeyCode == Keys.Back ||
                    e.KeyCode == Keys.Left ||
                    e.KeyCode == Keys.Right ||
                    e.KeyCode == Keys.Up ||
                    e.KeyCode == Keys.Delete ||
                    e.KeyCode == Keys.Down ||
                    e.KeyCode == Keys.PageUp ||
                    e.KeyCode == Keys.PageDown ||
                    e.KeyCode == Keys.Home ||
                    e.KeyCode == Keys.End ||
                    e.KeyCode == Keys.Shift ||
                    e.KeyCode == Keys.Control ||
                    e.KeyCode == Keys.Alt))
                {
                    m_bNavigating = true;
                }
                else
                {
                    m_bNavigating = false;
                }

                if (this.KeyPressed != null)
                    //raise keypressed event
                    this.KeyPressed(this, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void LocationsDropdown_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.KeyReleased != null)
                //raise keyreleased event
                this.KeyReleased(this, e);
        }
        private void LocationsDropdown_Leave(object sender, EventArgs e)
        {
        }
        #endregion
    }
}
