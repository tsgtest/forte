using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class ManagerBase : UserControl
    {

        public ManagerBase()
        {
            InitializeComponent();
        }

        public virtual void SaveCurrentRecord()
        {
        }
    }
}
