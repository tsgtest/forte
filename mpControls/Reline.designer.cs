using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LMP.Controls;

namespace LMP.Controls
{
    public partial class Reline : System.Windows.Forms.UserControl, IControl
    {
        private LMP.Controls.MultilineTextBox txtReline;
        private System.Windows.Forms.ContextMenu mnuReline;
        private System.Windows.Forms.MenuItem mnuRelineClear;
        private System.Windows.Forms.MenuItem mnuRelinePaste;
        private System.Windows.Forms.ContextMenu mnuSpecial;
        private System.Windows.Forms.MenuItem mnuSpecialDeleteAll;
        private System.Windows.Forms.MenuItem mnuSpecialClearRow;
        private System.Windows.Forms.MenuItem mnuSpecialDeleteRow;
        private System.Windows.Forms.MenuItem mnuSpecialInsertRow;

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reline));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Label");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Value");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.mnuReline = new System.Windows.Forms.ContextMenu();
            this.mnuRelineClear = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuRelineCut = new System.Windows.Forms.MenuItem();
            this.mnuRelineCopy = new System.Windows.Forms.MenuItem();
            this.mnuRelinePaste = new System.Windows.Forms.MenuItem();
            this.mnuSpecial = new System.Windows.Forms.ContextMenu();
            this.mnuSpecialClearRow = new System.Windows.Forms.MenuItem();
            this.mnuSpecialDeleteRow = new System.Windows.Forms.MenuItem();
            this.mnuSpecialDeleteAll = new System.Windows.Forms.MenuItem();
            this.mnuSpecialInsertRow = new System.Windows.Forms.MenuItem();
            this.mnuSpecialAddRow = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.mnuSpecialCut = new System.Windows.Forms.MenuItem();
            this.mnuSpecialCopy = new System.Windows.Forms.MenuItem();
            this.mnuSpecialPaste = new System.Windows.Forms.MenuItem();
            this.mnuSpecialPrefillSeparator = new System.Windows.Forms.MenuItem();
            this.tsOptions = new System.Windows.Forms.ToolStrip();
            this.tbtnReline = new System.Windows.Forms.ToolStripButton();
            this.tsep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnSpecial = new System.Windows.Forms.ToolStripButton();
            this.tsep2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnUnderlineLast = new System.Windows.Forms.ToolStripButton();
            this.tsep6 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnUnderline = new System.Windows.Forms.ToolStripButton();
            this.tsep5 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnItalic = new System.Windows.Forms.ToolStripButton();
            this.tsep4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnBold = new System.Windows.Forms.ToolStripButton();
            this.tsep3 = new System.Windows.Forms.ToolStripSeparator();
            this.tlblSpacer = new System.Windows.Forms.ToolStripLabel();
            this.grdSpecial = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtReline = new LMP.Controls.MultilineTextBox();
            this.tsOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSpecial)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuReline
            // 
            this.mnuReline.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuRelineClear,
            this.menuItem1,
            this.mnuRelineCut,
            this.mnuRelineCopy,
            this.mnuRelinePaste});
            this.mnuReline.Popup += new System.EventHandler(this.mnuReline_Popup);
            // 
            // mnuRelineClear
            // 
            this.mnuRelineClear.Index = 0;
            this.mnuRelineClear.Text = "[Clear]";
            this.mnuRelineClear.Click += new System.EventHandler(this.mnuRelineClear_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 1;
            this.menuItem1.Text = "-";
            // 
            // mnuRelineCut
            // 
            this.mnuRelineCut.Index = 2;
            this.mnuRelineCut.Text = "[Cut]";
            this.mnuRelineCut.Click += new System.EventHandler(this.mnuRelineCut_Click);
            // 
            // mnuRelineCopy
            // 
            this.mnuRelineCopy.Index = 3;
            this.mnuRelineCopy.Text = "[Copy]";
            this.mnuRelineCopy.Click += new System.EventHandler(this.mnuRelineCopy_Click);
            // 
            // mnuRelinePaste
            // 
            this.mnuRelinePaste.Index = 4;
            this.mnuRelinePaste.Text = "[Paste]";
            this.mnuRelinePaste.Click += new System.EventHandler(this.mnuRelinePaste_Click);
            // 
            // mnuSpecial
            // 
            this.mnuSpecial.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSpecialClearRow,
            this.mnuSpecialDeleteRow,
            this.mnuSpecialDeleteAll,
            this.mnuSpecialInsertRow,
            this.mnuSpecialAddRow,
            this.menuItem2,
            this.mnuSpecialCut,
            this.mnuSpecialCopy,
            this.mnuSpecialPaste,
            this.mnuSpecialPrefillSeparator});
            this.mnuSpecial.Popup += new System.EventHandler(this.mnuSpecial_Popup);
            // 
            // mnuSpecialClearRow
            // 
            this.mnuSpecialClearRow.Index = 0;
            this.mnuSpecialClearRow.Text = "[Clear Row]";
            this.mnuSpecialClearRow.Click += new System.EventHandler(this.mnuSpecialClearRow_Click);
            // 
            // mnuSpecialDeleteRow
            // 
            this.mnuSpecialDeleteRow.Index = 1;
            this.mnuSpecialDeleteRow.Text = "[Delete Row]";
            this.mnuSpecialDeleteRow.Click += new System.EventHandler(this.mnuSpecialDeleteRow_Click);
            // 
            // mnuSpecialDeleteAll
            // 
            this.mnuSpecialDeleteAll.Index = 2;
            this.mnuSpecialDeleteAll.Text = "[Delete All]";
            this.mnuSpecialDeleteAll.Click += new System.EventHandler(this.mnuSpecialDeleteAll_Click);
            // 
            // mnuSpecialInsertRow
            // 
            this.mnuSpecialInsertRow.Index = 3;
            this.mnuSpecialInsertRow.Text = "[Insert Row]";
            this.mnuSpecialInsertRow.Click += new System.EventHandler(this.mnuSpecialInsertRow_Click);
            // 
            // mnuSpecialAddRow
            // 
            this.mnuSpecialAddRow.Index = 4;
            this.mnuSpecialAddRow.Text = "[Add Row]";
            this.mnuSpecialAddRow.Click += new System.EventHandler(this.mnuSpecialAddRow_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 5;
            this.menuItem2.Text = "-";
            // 
            // mnuSpecialCut
            // 
            this.mnuSpecialCut.Index = 6;
            this.mnuSpecialCut.Text = "[Cut]";
            this.mnuSpecialCut.Click += new System.EventHandler(this.mnuSpecialCut_Click);
            // 
            // mnuSpecialCopy
            // 
            this.mnuSpecialCopy.Index = 7;
            this.mnuSpecialCopy.Text = "[Copy]";
            this.mnuSpecialCopy.Click += new System.EventHandler(this.mnuSpecialCopy_Click);
            // 
            // mnuSpecialPaste
            // 
            this.mnuSpecialPaste.Index = 8;
            this.mnuSpecialPaste.Text = "[Paste]";
            this.mnuSpecialPaste.Click += new System.EventHandler(this.mnuSpecialPaste_Click);
            // 
            // mnuSpecialPrefillSeparator
            // 
            this.mnuSpecialPrefillSeparator.Index = 9;
            this.mnuSpecialPrefillSeparator.Text = "-";
            this.mnuSpecialPrefillSeparator.Visible = false;
            // 
            // tsOptions
            // 
            this.tsOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsOptions.AutoSize = false;
            this.tsOptions.Dock = System.Windows.Forms.DockStyle.None;
            this.tsOptions.GripMargin = new System.Windows.Forms.Padding(0);
            this.tsOptions.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnReline,
            this.tsep1,
            this.tbtnSpecial,
            this.tsep2,
            this.tbtnUnderlineLast,
            this.tsep6,
            this.tbtnUnderline,
            this.tsep5,
            this.tbtnItalic,
            this.tsep4,
            this.tbtnBold,
            this.tsep3,
            this.tlblSpacer});
            this.tsOptions.Location = new System.Drawing.Point(0, 128);
            this.tsOptions.Name = "tsOptions";
            this.tsOptions.Size = new System.Drawing.Size(317, 22);
            this.tsOptions.TabIndex = 9;
            this.tsOptions.Text = "toolStrip1";
            // 
            // tbtnReline
            // 
            this.tbtnReline.CheckOnClick = true;
            this.tbtnReline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnReline.Image = ((System.Drawing.Image)(resources.GetObject("tbtnReline.Image")));
            this.tbtnReline.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnReline.Name = "tbtnReline";
            this.tbtnReline.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tbtnReline.Size = new System.Drawing.Size(43, 19);
            this.tbtnReline.Text = "Reline";
            this.tbtnReline.CheckedChanged += new System.EventHandler(this.tsTabReline_CheckedChanged);
            // 
            // tsep1
            // 
            this.tsep1.Name = "tsep1";
            this.tsep1.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tsep1.Size = new System.Drawing.Size(6, 22);
            // 
            // tbtnSpecial
            // 
            this.tbtnSpecial.CheckOnClick = true;
            this.tbtnSpecial.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSpecial.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSpecial.Image")));
            this.tbtnSpecial.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSpecial.Name = "tbtnSpecial";
            this.tbtnSpecial.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tbtnSpecial.Size = new System.Drawing.Size(48, 19);
            this.tbtnSpecial.Text = "Special";
            this.tbtnSpecial.CheckedChanged += new System.EventHandler(this.tsTabSpecial_CheckedChanged);
            // 
            // tsep2
            // 
            this.tsep2.Name = "tsep2";
            this.tsep2.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tsep2.Size = new System.Drawing.Size(6, 22);
            // 
            // tbtnUnderlineLast
            // 
            this.tbtnUnderlineLast.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnUnderlineLast.CheckOnClick = true;
            this.tbtnUnderlineLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnUnderlineLast.Image = global::LMP.Controls.Properties.Resources.UnderlineLast;
            this.tbtnUnderlineLast.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnUnderlineLast.Name = "tbtnUnderlineLast";
            this.tbtnUnderlineLast.Size = new System.Drawing.Size(23, 19);
            this.tbtnUnderlineLast.Text = "toolStripButton2";
            this.tbtnUnderlineLast.CheckedChanged += new System.EventHandler(this.tbtnUnderlineLast_CheckedChanged);
            // 
            // tsep6
            // 
            this.tsep6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsep6.Name = "tsep6";
            this.tsep6.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tsep6.Size = new System.Drawing.Size(6, 22);
            // 
            // tbtnUnderline
            // 
            this.tbtnUnderline.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnUnderline.CheckOnClick = true;
            this.tbtnUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnUnderline.Image = global::LMP.Controls.Properties.Resources.Underline;
            this.tbtnUnderline.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnUnderline.Name = "tbtnUnderline";
            this.tbtnUnderline.Size = new System.Drawing.Size(23, 19);
            this.tbtnUnderline.Text = "toolStripButton1";
            this.tbtnUnderline.CheckedChanged += new System.EventHandler(this.tbtnUnderline_CheckedChanged);
            // 
            // tsep5
            // 
            this.tsep5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsep5.Name = "tsep5";
            this.tsep5.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tsep5.Size = new System.Drawing.Size(6, 22);
            // 
            // tbtnItalic
            // 
            this.tbtnItalic.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnItalic.CheckOnClick = true;
            this.tbtnItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnItalic.Image = global::LMP.Controls.Properties.Resources.Italic;
            this.tbtnItalic.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnItalic.Name = "tbtnItalic";
            this.tbtnItalic.Size = new System.Drawing.Size(23, 19);
            this.tbtnItalic.CheckedChanged += new System.EventHandler(this.tbtnItalic_CheckedChanged);
            // 
            // tsep4
            // 
            this.tsep4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsep4.Name = "tsep4";
            this.tsep4.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tsep4.Size = new System.Drawing.Size(6, 22);
            // 
            // tbtnBold
            // 
            this.tbtnBold.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnBold.CheckOnClick = true;
            this.tbtnBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnBold.Image = global::LMP.Controls.Properties.Resources.Bold;
            this.tbtnBold.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnBold.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnBold.Name = "tbtnBold";
            this.tbtnBold.Size = new System.Drawing.Size(23, 19);
            this.tbtnBold.CheckedChanged += new System.EventHandler(this.tbtnBold_CheckedChanged);
            // 
            // tsep3
            // 
            this.tsep3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsep3.Name = "tsep3";
            this.tsep3.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tsep3.Size = new System.Drawing.Size(6, 22);
            // 
            // tlblSpacer
            // 
            this.tlblSpacer.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tlblSpacer.Name = "tlblSpacer";
            this.tlblSpacer.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tlblSpacer.Size = new System.Drawing.Size(0, 19);
            this.tlblSpacer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grdSpecial
            // 
            this.grdSpecial.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.grdSpecial.DisplayLayout.Appearance = appearance1;
            this.grdSpecial.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            ultraGridBand1.ColHeadersVisible = false;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2});
            ultraGridBand1.GroupHeadersVisible = false;
            this.grdSpecial.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.grdSpecial.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.grdSpecial.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.grdSpecial.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.grdSpecial.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grdSpecial.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.grdSpecial.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grdSpecial.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.grdSpecial.DisplayLayout.MaxColScrollRegions = 1;
            this.grdSpecial.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grdSpecial.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.grdSpecial.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.grdSpecial.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.grdSpecial.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.grdSpecial.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.grdSpecial.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.grdSpecial.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.grdSpecial.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.grdSpecial.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.grdSpecial.DisplayLayout.Override.CellAppearance = appearance8;
            this.grdSpecial.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.grdSpecial.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.grdSpecial.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.grdSpecial.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.grdSpecial.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.grdSpecial.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.grdSpecial.DisplayLayout.Override.RowAppearance = appearance11;
            this.grdSpecial.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.grdSpecial.DisplayLayout.Scrollbars = Infragistics.Win.UltraWinGrid.Scrollbars.Vertical;
            this.grdSpecial.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.grdSpecial.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.grdSpecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdSpecial.Location = new System.Drawing.Point(0, 0);
            this.grdSpecial.Name = "grdSpecial";
            this.grdSpecial.Size = new System.Drawing.Size(317, 126);
            this.grdSpecial.TabIndex = 10;
            this.grdSpecial.Text = "ultraGrid1";
            this.grdSpecial.Visible = false;
            this.grdSpecial.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.grdSpecial_AfterCellUpdate);
            this.grdSpecial.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.grdSpecial_CellChange);
            this.grdSpecial.Enter += new System.EventHandler(this.grdSpecial_Enter);
            this.grdSpecial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdSpecial_KeyDown);
            this.grdSpecial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Reline_KeyPress);
            this.grdSpecial.KeyUp += new System.Windows.Forms.KeyEventHandler(this.grdSpecial_KeyUp);
            this.grdSpecial.Leave += new System.EventHandler(this.grdSpecial_Leave);
            // 
            // txtReline
            // 
            this.txtReline.AcceptsReturn = true;
            this.txtReline.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReline.ContextMenu = this.mnuReline;
            this.txtReline.IsDirty = true;
            this.txtReline.Location = new System.Drawing.Point(0, 0);
            this.txtReline.Multiline = true;
            this.txtReline.Name = "txtReline";
            this.txtReline.NumberOfLines = 2;
            this.txtReline.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtReline.Size = new System.Drawing.Size(317, 126);
            this.txtReline.SupportingValues = "";
            this.txtReline.TabIndex = 0;
            this.txtReline.Tag2 = null;
            this.txtReline.Value = "";
            this.txtReline.TextChanged += new System.EventHandler(this.txtReline_TextChanged);
            this.txtReline.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReline_KeyDown);
            this.txtReline.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Reline_KeyPress);
            this.txtReline.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtReline_KeyUp);
            // 
            // Reline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.tsOptions);
            this.Controls.Add(this.grdSpecial);
            this.Controls.Add(this.txtReline);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Reline";
            this.Size = new System.Drawing.Size(317, 150);
            this.Load += new System.EventHandler(this.Reline_Load);
            this.Enter += new System.EventHandler(this.Reline_Enter);
            this.Leave += new System.EventHandler(this.Reline_Leave);
            this.Resize += new System.EventHandler(this.Reline_Resize);
            this.tsOptions.ResumeLayout(false);
            this.tsOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSpecial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private MenuItem mnuSpecialPrefillSeparator;
        private ToolStrip tsOptions;
        private ToolStripButton tbtnReline;
        private ToolStripSeparator tsep1;
        private ToolStripButton tbtnSpecial;
        private ToolStripSeparator tsep2;
        private ToolStripSeparator tsep3;
        private ToolStripButton tbtnBold;
        private ToolStripSeparator tsep4;
        private ToolStripButton tbtnItalic;
        private ToolStripSeparator tsep5;
        private ToolStripButton tbtnUnderline;
        private ToolStripSeparator tsep6;
        private ToolStripButton tbtnUnderlineLast;
        private ToolStripLabel tlblSpacer;
        private MenuItem mnuSpecialAddRow;
        private Infragistics.Win.UltraWinGrid.UltraGrid grdSpecial;
        private MenuItem menuItem1;
        private MenuItem mnuRelineCut;
        private MenuItem mnuRelineCopy;
        private MenuItem menuItem2;
        private MenuItem mnuSpecialPaste;
        private MenuItem mnuSpecialCut;
        private MenuItem mnuSpecialCopy;
    }
}
