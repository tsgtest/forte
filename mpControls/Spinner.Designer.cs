namespace LMP.Controls
{
    partial class Spinner
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.mnuContext_Copy = new System.Windows.Forms.MenuItem();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.textBox1 = new LMP.Controls.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContext_Copy});
            // 
            // mnuContext_Copy
            // 
            this.mnuContext_Copy.Index = 0;
            this.mnuContext_Copy.Text = "Copy";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericUpDown1.Location = new System.Drawing.Point(186, 0);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(0);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(18, 22);
            this.numericUpDown1.TabIndex = 10;
            this.numericUpDown1.TabStop = false;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.IsDirty = false;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Margin = new System.Windows.Forms.Padding(1);
            this.textBox1.Name = "textBox1";
            this.textBox1.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.textBox1.Size = new System.Drawing.Size(185, 22);
            this.textBox1.SupportingValues = "";
            this.textBox1.TabIndex = 9;
            this.textBox1.Tag2 = null;
            this.textBox1.Value = "";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Spinner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.textBox1);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Spinner";
            this.Size = new System.Drawing.Size(203, 22);
            this.Leave += new System.EventHandler(this.Spinner_Leave);
            this.Resize += new System.EventHandler(this.Spinner_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        protected System.Windows.Forms.ContextMenu mnuContext;
        protected System.Windows.Forms.MenuItem mnuContext_Copy;
        private TextBox textBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;


    }
}
