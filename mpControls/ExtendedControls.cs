using System;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;

namespace LMP.Controls
{
	//delegate handles OnPreProcessMessageEvents -
	//returns the preprocessed message
	public delegate string PreProcessMessageEventHandler();

	public interface IMPTreeSubControl
	{
		event PreProcessMessageEventHandler OnPreProcessMessage;
	}

	/// <summary>
	/// Summary description for ExtendedControls.
	/// </summary>
	public class MPTextEditor: TextBox
	{	
		private Infragistics.Win.Misc.UltraButton ultraButton1;
		private UltraTree oTree;
		private UltraTreeNode oOwningNode;

		public MPTextEditor(Infragistics.Win.UltraWinTree.UltraTreeNode oOwningNode)
		{
			this.oTree = oOwningNode.Control;
			this.oOwningNode = oOwningNode;
		}

		private void InitializeComponent()
		{
			this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
			// 
			// ultraButton1
			// 
			this.ultraButton1.Location = new System.Drawing.Point(17, 17);
			this.ultraButton1.Name = "ultraButton1";
			this.ultraButton1.TabIndex = 0;
			this.ultraButton1.Text = "ultraButton1";

		}

		public override bool PreProcessMessage(ref Message msg)
		{
			if(msg.Msg==256 && (
				msg.WParam.ToInt32()==(int) Keys.Tab ||
				msg.WParam.ToInt32() == (int) Keys.Down))
			{
				UltraTreeNode oNextNode = oOwningNode.NextVisibleNode;
					if(oNextNode != null)
						oNextNode.Selected = true;

				return true;
			}
			else if(msg.Msg==256 && (
				msg.WParam.ToInt32()==(int) Keys.Up))
			{
				UltraTreeNode oPrevNode = oOwningNode.PrevVisibleNode;
					if(oPrevNode != null)
						oPrevNode.Selected = true;

				return true;
			}
			else
				return base.PreProcessMessage(ref msg);
		}
	}

	public class TreeMultiLineCombo: LMP.Controls.MultilineCombo
	{
		private UltraTree oTree;
		private UltraTreeNode oOwningNode;

		public TreeMultiLineCombo(Infragistics.Win.UltraWinTree.UltraTreeNode oOwningNode):base()
		{
			this.oTree = oOwningNode.Control;
			this.oOwningNode = oOwningNode;
		}

		public override bool PreProcessMessage(ref Message msg)
		{
			if(msg.Msg==256 && (
				msg.WParam.ToInt32()==(int) Keys.Tab ||
				msg.WParam.ToInt32() == (int) Keys.Down))
			{
				UltraTreeNode oNextNode = oOwningNode.NextVisibleNode;
				if(oNextNode != null)
					oNextNode.Selected = true;

				return true;
			}
			else if(msg.Msg==256 && (
				msg.WParam.ToInt32()==(int) Keys.Up))
			{
				UltraTreeNode oPrevNode = oOwningNode.PrevVisibleNode;
				if(oPrevNode != null)
					oPrevNode.Selected = true;

				return true;
			}
			else
				return base.PreProcessMessage(ref msg);
		}
	}

	public class MPTree: UltraTree 
	{
		public override bool PreProcessMessage(ref System.Windows.Forms.Message msg)
		{
			return true;
		}
	}
}
