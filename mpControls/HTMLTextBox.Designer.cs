namespace LMP.Controls
{
    partial class HTMLTextBox
    {
        private LMP.Controls.MultilineTextBox txtHTML;
        private System.Windows.Forms.Button btnGetHTML;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetHTML = new System.Windows.Forms.Button();
            this.txtHTML = new LMP.Controls.MultilineTextBox();
            this.SuspendLayout();
            // 
            // btnGetHTML
            // 
            this.btnGetHTML.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetHTML.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnGetHTML.Location = new System.Drawing.Point(-1, 158);
            this.btnGetHTML.Name = "btnGetHTML";
            this.btnGetHTML.Size = new System.Drawing.Size(263, 27);
            this.btnGetHTML.TabIndex = 1;
            this.btnGetHTML.TabStop = false;
            this.btnGetHTML.Text = "Get &HTML...";
            this.btnGetHTML.UseVisualStyleBackColor = false;
            this.btnGetHTML.Click += new System.EventHandler(this.btnGetHTML_Click);
            // 
            // txtHTML
            // 
            this.txtHTML.AcceptsReturn = true;
            this.txtHTML.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHTML.IsDirty = false;
            this.txtHTML.Location = new System.Drawing.Point(0, 0);
            this.txtHTML.Multiline = true;
            this.txtHTML.Name = "txtHTML";
            this.txtHTML.NumberOfLines = 2;
            this.txtHTML.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtHTML.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtHTML.Size = new System.Drawing.Size(260, 157);
            this.txtHTML.SupportingValues = "";
            this.txtHTML.TabIndex = 0;
            this.txtHTML.Tag2 = null;
            this.txtHTML.Value = "";
            this.txtHTML.TextChanged += new System.EventHandler(this.txtHTML_TextChanged);
            // 
            // HTMLTextBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Silver;
            this.Controls.Add(this.btnGetHTML);
            this.Controls.Add(this.txtHTML);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "HTMLTextBox";
            this.Size = new System.Drawing.Size(261, 185);
            this.Move += new System.EventHandler(this.HTMLTextBox_Move);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
