using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.Architect
{
    public class Memo : AdminSegment
    {
        public override void InsertXML(string xXML, Segment oParent, Word.Range oLocation, 
            mpCOM.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted)
        {
            try
            {
                Segment.InsertXML(xXML, oLocation,
                    iHeaderFooterInsertionType,
                    mpCOM.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
            }
        }
    }
}
