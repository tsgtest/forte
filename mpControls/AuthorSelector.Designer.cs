namespace LMP.Controls
{
    partial class AuthorSelector : System.Windows.Forms.UserControl, IControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DisplayName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ID");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthorSelector));
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Favorite");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DisplayName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ID");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand3 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn6 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Description");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn7 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BarID");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn8 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ID", 0);
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand4 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn9 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Source");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn10 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ID");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Lead");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name", -1, "ddAuthors");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BarID", -1, "ddBarIDs");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Band 1");
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand5 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 1", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("EMail");
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.cmbAuthor = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ddAuthors = new Infragistics.Win.UltraWinGrid.UltraDropDown();
            this.ddBarIDs = new Infragistics.Win.UltraWinGrid.UltraDropDown();
            this.grdAuthors = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.mnuContext_MoveUp = new System.Windows.Forms.MenuItem();
            this.mnuContext_MoveDown = new System.Windows.Forms.MenuItem();
            this.mnuContext_Sep2 = new System.Windows.Forms.MenuItem();
            this.mnuContext_Delete = new System.Windows.Forms.MenuItem();
            this.mnuContext_DeleteAll = new System.Windows.Forms.MenuItem();
            this.mnuContext_Sep3 = new System.Windows.Forms.MenuItem();
            this.mnuContext_Cut = new System.Windows.Forms.MenuItem();
            this.mnuContext_Copy = new System.Windows.Forms.MenuItem();
            this.mnuContext_Paste = new System.Windows.Forms.MenuItem();
            this.mnuContext_Sep1 = new System.Windows.Forms.MenuItem();
            this.mnuContext_ManagePeople = new System.Windows.Forms.MenuItem();
            this.mnuContext_EditAuthorPreferences = new System.Windows.Forms.MenuItem();
            this.mnuContext_UpdateAuthors = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAuthor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddAuthors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddBarIDs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAuthors)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbAuthor
            // 
            this.cmbAuthor.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbAuthor.DisplayLayout.Appearance = appearance1;
            this.cmbAuthor.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            ultraGridBand1.ColHeadersVisible = false;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2});
            ultraGridBand1.GroupHeadersVisible = false;
            ultraGridBand1.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None;
            ultraGridBand1.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None;
            this.cmbAuthor.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.cmbAuthor.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbAuthor.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbAuthor.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbAuthor.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmbAuthor.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbAuthor.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmbAuthor.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbAuthor.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbAuthor.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbAuthor.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmbAuthor.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.cmbAuthor.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.cmbAuthor.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.cmbAuthor.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.cmbAuthor.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbAuthor.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAuthor.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbAuthor.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmbAuthor.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.cmbAuthor.DisplayLayout.Override.CellPadding = 0;
            this.cmbAuthor.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbAuthor.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmbAuthor.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmbAuthor.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbAuthor.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmbAuthor.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmbAuthor.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.cmbAuthor.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.cmbAuthor.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.cmbAuthor.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbAuthor.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmbAuthor.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbAuthor.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbAuthor.DisplayLayout.TabNavigation = Infragistics.Win.UltraWinGrid.TabNavigation.NextControl;
            this.cmbAuthor.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.cmbAuthor.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbAuthor.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Default;
            this.cmbAuthor.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbAuthor.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            this.cmbAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAuthor.ImageList = this.imageList1;
            this.cmbAuthor.Location = new System.Drawing.Point(0, 0);
            this.cmbAuthor.Name = "cmbAuthor";
            this.cmbAuthor.Size = new System.Drawing.Size(351, 22);
            this.cmbAuthor.TabIndex = 0;
            this.cmbAuthor.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbAuthor_BeforeDropDown);
            this.cmbAuthor.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.cmbAuthor_RowSelected);
            this.cmbAuthor.TextChanged += new System.EventHandler(this.cmbAuthor_TextChanged);
            this.cmbAuthor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbAuthor_KeyDown);
            this.cmbAuthor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbAuthor_KeyUp);
            this.cmbAuthor.Leave += new System.EventHandler(this.cmbAuthor_Leave);
            this.cmbAuthor.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cmbAuthor_MouseDown);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Pen.bmp");
            // 
            // ddAuthors
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ddAuthors.DisplayLayout.Appearance = appearance13;
            ultraGridBand2.ColHeadersVisible = false;
            ultraGridColumn3.Header.VisiblePosition = 0;
            ultraGridColumn4.Header.VisiblePosition = 1;
            ultraGridColumn5.Header.VisiblePosition = 2;
            ultraGridColumn5.Hidden = true;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5});
            ultraGridBand2.GroupHeadersVisible = false;
            ultraGridBand2.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None;
            ultraGridBand2.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None;
            this.ddAuthors.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this.ddAuthors.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ddAuthors.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ddAuthors.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ddAuthors.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ddAuthors.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ddAuthors.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ddAuthors.DisplayLayout.MaxColScrollRegions = 1;
            this.ddAuthors.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ddAuthors.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ddAuthors.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ddAuthors.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ddAuthors.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.ddAuthors.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ddAuthors.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ddAuthors.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ddAuthors.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ddAuthors.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ddAuthors.DisplayLayout.Override.CellAppearance = appearance20;
            this.ddAuthors.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.ddAuthors.DisplayLayout.Override.CellPadding = 0;
            this.ddAuthors.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ddAuthors.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ddAuthors.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ddAuthors.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ddAuthors.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ddAuthors.DisplayLayout.Override.RowAppearance = appearance23;
            this.ddAuthors.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ddAuthors.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ddAuthors.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ddAuthors.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ddAuthors.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ddAuthors.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ddAuthors.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ddAuthors.DisplayLayout.TabNavigation = Infragistics.Win.UltraWinGrid.TabNavigation.NextControl;
            this.ddAuthors.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.ddAuthors.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ddAuthors.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddAuthors.Location = new System.Drawing.Point(3, 142);
            this.ddAuthors.Name = "ddAuthors";
            this.ddAuthors.Size = new System.Drawing.Size(199, 73);
            this.ddAuthors.TabIndex = 1;
            this.ddAuthors.Text = "ultraDropDown1";
            this.ddAuthors.Visible = false;
            this.ddAuthors.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ddAuthors_BeforeDropDown);
            this.ddAuthors.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.ddAuthors_RowSelected);
            // 
            // ddBarIDs
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ddBarIDs.DisplayLayout.Appearance = appearance25;
            ultraGridBand3.ColHeadersVisible = false;
            ultraGridColumn6.Header.VisiblePosition = 0;
            ultraGridColumn7.Header.VisiblePosition = 1;
            ultraGridColumn8.Header.VisiblePosition = 2;
            ultraGridColumn8.Hidden = true;
            ultraGridBand3.Columns.AddRange(new object[] {
            ultraGridColumn6,
            ultraGridColumn7,
            ultraGridColumn8});
            ultraGridBand3.GroupHeadersVisible = false;
            ultraGridBand3.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None;
            ultraGridBand3.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None;
            this.ddBarIDs.DisplayLayout.BandsSerializer.Add(ultraGridBand3);
            this.ddBarIDs.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ddBarIDs.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ddBarIDs.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ddBarIDs.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ddBarIDs.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ddBarIDs.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ddBarIDs.DisplayLayout.MaxColScrollRegions = 1;
            this.ddBarIDs.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ddBarIDs.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ddBarIDs.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ddBarIDs.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ddBarIDs.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.ddBarIDs.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ddBarIDs.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ddBarIDs.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ddBarIDs.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ddBarIDs.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ddBarIDs.DisplayLayout.Override.CellAppearance = appearance32;
            this.ddBarIDs.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.ddBarIDs.DisplayLayout.Override.CellPadding = 0;
            this.ddBarIDs.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ddBarIDs.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ddBarIDs.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ddBarIDs.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ddBarIDs.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ddBarIDs.DisplayLayout.Override.RowAppearance = appearance35;
            this.ddBarIDs.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ddBarIDs.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ddBarIDs.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ddBarIDs.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ddBarIDs.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ddBarIDs.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ddBarIDs.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ddBarIDs.DisplayLayout.TabNavigation = Infragistics.Win.UltraWinGrid.TabNavigation.NextControl;
            this.ddBarIDs.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.ddBarIDs.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ddBarIDs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddBarIDs.Location = new System.Drawing.Point(28, 179);
            this.ddBarIDs.Name = "ddBarIDs";
            this.ddBarIDs.Size = new System.Drawing.Size(199, 73);
            this.ddBarIDs.TabIndex = 2;
            this.ddBarIDs.Text = "ultraDropDown1";
            this.ddBarIDs.Visible = false;
            this.ddBarIDs.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ddBarIDs_BeforeDropDown);
            // 
            // grdAuthors
            // 
            this.grdAuthors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.grdAuthors.DisplayLayout.Appearance = appearance37;
            this.grdAuthors.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            ultraGridBand4.ColHeadersVisible = false;
            ultraGridColumn9.Header.VisiblePosition = 0;
            ultraGridColumn9.Hidden = true;
            ultraGridColumn10.Header.VisiblePosition = 1;
            ultraGridColumn10.Hidden = true;
            ultraGridColumn11.Header.VisiblePosition = 2;
            ultraGridColumn11.TabStop = false;
            ultraGridColumn11.Width = 16;
            ultraGridColumn12.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.OnRowActivate;
            ultraGridColumn12.Header.VisiblePosition = 3;
            ultraGridColumn12.ProportionalResize = true;
            ultraGridColumn12.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
            ultraGridColumn12.Width = 244;
            ultraGridColumn13.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.OnRowActivate;
            ultraGridColumn13.Header.VisiblePosition = 4;
            ultraGridColumn13.Width = 70;
            ultraGridColumn14.Header.VisiblePosition = 5;
            ultraGridBand4.Columns.AddRange(new object[] {
            ultraGridColumn9,
            ultraGridColumn10,
            ultraGridColumn11,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14});
            ultraGridBand4.GroupHeadersVisible = false;
            ultraGridColumn15.Header.VisiblePosition = 0;
            ultraGridColumn15.Width = 311;
            ultraGridBand5.Columns.AddRange(new object[] {
            ultraGridColumn15});
            ultraGridBand5.RowLayoutLabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.Left;
            ultraGridBand5.RowLayoutLabelStyle = Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.WithCellData;
            this.grdAuthors.DisplayLayout.BandsSerializer.Add(ultraGridBand4);
            this.grdAuthors.DisplayLayout.BandsSerializer.Add(ultraGridBand5);
            this.grdAuthors.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.grdAuthors.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.grdAuthors.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.grdAuthors.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grdAuthors.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.grdAuthors.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grdAuthors.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.grdAuthors.DisplayLayout.MaxColScrollRegions = 1;
            this.grdAuthors.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grdAuthors.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.ForeColor = System.Drawing.SystemColors.WindowText;
            this.grdAuthors.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.grdAuthors.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.grdAuthors.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.grdAuthors.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.grdAuthors.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.grdAuthors.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.grdAuthors.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.grdAuthors.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.grdAuthors.DisplayLayout.Override.CellAppearance = appearance44;
            this.grdAuthors.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.grdAuthors.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.grdAuthors.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.grdAuthors.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.grdAuthors.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.grdAuthors.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.grdAuthors.DisplayLayout.Override.RowAppearance = appearance47;
            this.grdAuthors.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            appearance48.ForeColor = System.Drawing.SystemColors.WindowText;
            this.grdAuthors.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.grdAuthors.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.grdAuthors.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.grdAuthors.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdAuthors.ImageList = this.imageList1;
            this.grdAuthors.Location = new System.Drawing.Point(0, 0);
            this.grdAuthors.Name = "grdAuthors";
            this.grdAuthors.Size = new System.Drawing.Size(351, 136);
            this.grdAuthors.TabIndex = 3;
            this.grdAuthors.Text = "ultraGrid1";
            this.grdAuthors.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.grdAuthors_AfterCellUpdate);
            this.grdAuthors.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.grdAuthors_CellChange);
            this.grdAuthors.BeforeCellActivate += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.grdAuthors_BeforeCellActivate);
            this.grdAuthors.BeforeCellUpdate += new Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventHandler(this.grdAuthors_BeforeCellUpdate);
            this.grdAuthors.Enter += new System.EventHandler(this.grdAuthors_Enter);
            this.grdAuthors.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdAuthors_KeyDown);
            this.grdAuthors.KeyUp += new System.Windows.Forms.KeyEventHandler(this.grdAuthors_KeyUp);
            this.grdAuthors.MouseClick += new System.Windows.Forms.MouseEventHandler(this.grdAuthors_MouseClick);
            this.grdAuthors.MouseDown += new System.Windows.Forms.MouseEventHandler(this.grdAuthors_MouseDown);
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContext_MoveUp,
            this.mnuContext_MoveDown,
            this.mnuContext_Sep2,
            this.mnuContext_Delete,
            this.mnuContext_DeleteAll,
            this.mnuContext_Sep3,
            this.mnuContext_Cut,
            this.mnuContext_Copy,
            this.mnuContext_Paste,
            this.mnuContext_Sep1,
            this.mnuContext_ManagePeople,
            this.mnuContext_EditAuthorPreferences,
            this.mnuContext_UpdateAuthors});
            this.mnuContext.Popup += new System.EventHandler(this.mnuContext_Popup);
            // 
            // mnuContext_MoveUp
            // 
            this.mnuContext_MoveUp.Index = 0;
            this.mnuContext_MoveUp.Text = "Move Up";
            this.mnuContext_MoveUp.Click += new System.EventHandler(this.mnuContext_MoveUp_Click);
            // 
            // mnuContext_MoveDown
            // 
            this.mnuContext_MoveDown.Index = 1;
            this.mnuContext_MoveDown.Text = "Move Down";
            this.mnuContext_MoveDown.Click += new System.EventHandler(this.mnuContext_MoveDown_Click);
            // 
            // mnuContext_Sep2
            // 
            this.mnuContext_Sep2.Index = 2;
            this.mnuContext_Sep2.Text = "-";
            // 
            // mnuContext_Delete
            // 
            this.mnuContext_Delete.Index = 3;
            this.mnuContext_Delete.Text = "Delete";
            this.mnuContext_Delete.Click += new System.EventHandler(this.mnuContext_Delete_Click);
            // 
            // mnuContext_DeleteAll
            // 
            this.mnuContext_DeleteAll.Index = 4;
            this.mnuContext_DeleteAll.Text = "Delete All";
            this.mnuContext_DeleteAll.Click += new System.EventHandler(this.mnuContext_DeleteAll_Click);
            // 
            // mnuContext_Sep3
            // 
            this.mnuContext_Sep3.Index = 5;
            this.mnuContext_Sep3.Text = "-";
            // 
            // mnuContext_Cut
            // 
            this.mnuContext_Cut.Index = 6;
            this.mnuContext_Cut.Text = "Cut";
            this.mnuContext_Cut.Click += new System.EventHandler(this.mnuContext_Cut_Click);
            // 
            // mnuContext_Copy
            // 
            this.mnuContext_Copy.Index = 7;
            this.mnuContext_Copy.Text = "Copy";
            this.mnuContext_Copy.Click += new System.EventHandler(this.mnuContext_Copy_Click);
            // 
            // mnuContext_Paste
            // 
            this.mnuContext_Paste.Index = 8;
            this.mnuContext_Paste.Text = "Paste";
            this.mnuContext_Paste.Click += new System.EventHandler(this.mnuContext_Paste_Click);
            // 
            // mnuContext_Sep1
            // 
            this.mnuContext_Sep1.Index = 9;
            this.mnuContext_Sep1.Text = "-";
            // 
            // mnuContext_ManagePeople
            // 
            this.mnuContext_ManagePeople.Index = 10;
            this.mnuContext_ManagePeople.Text = "Manage People...";
            this.mnuContext_ManagePeople.Click += new System.EventHandler(this.mnuContext_ManagePeople_Click);
            // 
            // mnuContext_EditAuthorPreferences
            // 
            this.mnuContext_EditAuthorPreferences.Index = 11;
            this.mnuContext_EditAuthorPreferences.Text = "#Edit Author Preferences#";
            this.mnuContext_EditAuthorPreferences.Click += new System.EventHandler(this.mnuContext_EditAuthorPreferences_Click);
            // 
            // mnuContext_UpdateAuthors
            // 
            this.mnuContext_UpdateAuthors.Index = 12;
            this.mnuContext_UpdateAuthors.Text = "#Update Authors#";
            this.mnuContext_UpdateAuthors.Click += new System.EventHandler(this.mnuContext_UpdateAuthors_Click);
            // 
            // AuthorSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.ddBarIDs);
            this.Controls.Add(this.ddAuthors);
            this.Controls.Add(this.cmbAuthor);
            this.Controls.Add(this.grdAuthors);
            this.Name = "AuthorSelector";
            this.Size = new System.Drawing.Size(351, 209);
            this.Load += new System.EventHandler(this.AuthorSelector_Load);
            this.Enter += new System.EventHandler(this.AuthorSelector_Enter);
            this.Leave += new System.EventHandler(this.AuthorSelector_Leave);
            this.Resize += new System.EventHandler(this.AuthorSelector_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.cmbAuthor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddAuthors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddBarIDs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAuthors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraCombo cmbAuthor;
        private Infragistics.Win.UltraWinGrid.UltraDropDown ddAuthors;
        private Infragistics.Win.UltraWinGrid.UltraDropDown ddBarIDs;
        private Infragistics.Win.UltraWinGrid.UltraGrid grdAuthors;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ContextMenu mnuContext;
        private System.Windows.Forms.MenuItem mnuContext_ManagePeople;
        private System.Windows.Forms.MenuItem mnuContext_Delete;
        private System.Windows.Forms.MenuItem mnuContext_DeleteAll;
        private System.Windows.Forms.MenuItem mnuContext_Sep1;
        private System.Windows.Forms.MenuItem mnuContext_MoveUp;
        private System.Windows.Forms.MenuItem mnuContext_MoveDown;
        private System.Windows.Forms.MenuItem mnuContext_Sep2;
        private System.Windows.Forms.MenuItem mnuContext_UpdateAuthors;
        private System.Windows.Forms.MenuItem mnuContext_EditAuthorPreferences;
        private System.Windows.Forms.MenuItem mnuContext_Paste;
        private System.Windows.Forms.MenuItem mnuContext_Sep3;
        private System.Windows.Forms.MenuItem mnuContext_Cut;
        private System.Windows.Forms.MenuItem mnuContext_Copy;
    }
}
