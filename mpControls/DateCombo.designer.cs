namespace LMP.Controls
{
    partial class DateCombo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new LMP.Controls.ComboBox();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.AllowEmptyValue = false;
            this.comboBox1.AutoSize = true;
            this.comboBox1.Borderless = false;
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.comboBox1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.IsDirty = false;
            this.comboBox1.LimitToList = true;
            this.comboBox1.ListName = "";
            this.comboBox1.Location = new System.Drawing.Point(0, 0);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(0);
            this.comboBox1.MaxDropDownItems = 8;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.SelectedIndex = -1;
            this.comboBox1.SelectedValue = null;
            this.comboBox1.SelectionLength = 0;
            this.comboBox1.SelectionStart = 0;
            this.comboBox1.Size = new System.Drawing.Size(210, 23);
            this.comboBox1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.comboBox1.SupportingValues = "";
            this.comboBox1.TabIndex = 2;
            this.comboBox1.Tag2 = null;
            this.comboBox1.Value = "";
            this.comboBox1.ValueChanged += new LMP.Controls.ValueChangedHandler(this.comboBox1_ValueChanged);
            this.comboBox1.KeyReleased += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyReleased);
            this.comboBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyDown);
            // 
            // DateCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.comboBox1);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "DateCombo";
            this.Size = new System.Drawing.Size(210, 23);
            this.Leave += new System.EventHandler(this.DateCombo_Leave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private ComboBox comboBox1;






    }
}
