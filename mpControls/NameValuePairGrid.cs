using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LMP.Data;
using Infragistics.Shared;
using Infragistics.Win;
using XML = System.Xml;
using Infragistics.Win.UltraWinGrid;

namespace LMP.Controls
{
    public partial class NameValuePairGrid : UserControl, IControl
    {
        #region *********************enumerations*********************
        [Flags]
        public enum mpTabStopsConfiguration
        {
            None = 0,
            GridColumn1 = 1,
            GridColumn2 = 2
        }
        //GLOG 4559
        public enum mpNameValueSeparator
        {
            Tab = 0,
            Spaces = 1
        }

        #endregion
        #region *********************fields*********************
        private bool m_bInitialized = false;
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private bool m_bInputIsXML = false;
        private string m_xInitialContent = "";
        private int m_iVisibleRows = 4;
        private bool m_bAllowRowAdd = true;
        private bool m_bAllowRowDelete = true;
        private bool m_bAllowEmptyRow = false;
        private bool m_bRemoveEmptyValues = false;
        private int m_iLabelColumnWidth = 0;
        private bool m_bWrapCellText = false;
        private bool m_bAllowColumnResize = true;
        private mpTabStopsConfiguration m_iTabStopColumns = mpTabStopsConfiguration.GridColumn1 | mpTabStopsConfiguration.GridColumn2;
        private string m_xSupportingValues = "";
        private mpNameValueSeparator m_iNameValueSeparator = mpNameValueSeparator.Tab;
        #endregion
        #region *********************constructor*********************
        public NameValuePairGrid()
        {
            InitializeComponent();
        }
        # endregion
        #region *********************properties*********************
        public string InitialContent
        {
            get { return m_xInitialContent; }
            set { m_xInitialContent = value; }
        }
        public int VisibleRows
        {
            get { return m_iVisibleRows; }
            set
            {
                m_iVisibleRows = value;
                if (m_bInitialized)
                    SetControlHeight();
            }
        }
        public bool AllowAddRow
        {
            get { return m_bAllowRowAdd; }
            set
            {
                m_bAllowRowAdd = value;
                if (m_bInitialized)
                    SetMenus();
            }
        }
        public  bool AllowEmptyRow
        {
            get {return m_bAllowEmptyRow;}
            set
            {
                m_bAllowEmptyRow = value;
            }
        }

        public bool AllowDeleteRow
        {
            get { return m_bAllowRowDelete; }
            set
            {
                m_bAllowRowDelete = value;
                if (m_bInitialized)
                    SetMenus();
            }
        }
        public bool RemoveEmptyValues
        {
            get { return m_bRemoveEmptyValues; }
            set { m_bRemoveEmptyValues = value; }
        }
        public mpTabStopsConfiguration TabStopColumns
        {
            get { return m_iTabStopColumns; }
            set 
            {
                if (m_iTabStopColumns != value)
                {
                    m_iTabStopColumns = value;
                }
            }
        }
        public int LabelColumnWidth
        {
            get { return m_iLabelColumnWidth; }
            set
            {
                if (m_iLabelColumnWidth != value)
                {
                    m_iLabelColumnWidth = value;
                    if (m_bInitialized)
                    {
                        if (m_iLabelColumnWidth > 0)
                        {
                            this.grdNameValuePairs.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
                            this.grdNameValuePairs.DisplayLayout.Bands[0].Columns[0].Width = m_iLabelColumnWidth;
                        }
                        else
                            this.grdNameValuePairs.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
                    }
                }
            }
        }
        public bool WrapCellText
        {
            get { return m_bWrapCellText; }
            set
            {
                if (m_bWrapCellText != value)
                {
                    m_bWrapCellText = value;
                    if (m_bInitialized)
                        SetCellWrapping();
                }
            }
        }
        public bool AllowColumnResize
        {
            get { return m_bAllowColumnResize; }
            set
            {
                if (m_bAllowColumnResize != value)
                {
                    m_bAllowColumnResize = value;
                    if (m_bInitialized)
                    {
                        if (m_bAllowColumnResize)
                            this.grdNameValuePairs.DisplayLayout.Override.AllowColSizing = AllowColSizing.Synchronized;
                        else
                            this.grdNameValuePairs.DisplayLayout.Override.AllowColSizing = AllowColSizing.None;
                    }
                }
            }
        }
        public mpNameValueSeparator NameValueSeparator
        {
            get { return m_iNameValueSeparator; }
            set { m_iNameValueSeparator = value; }
        }
        #endregion
        #region *********************private members*********************
        private void SetMenus()
        {
            this.mnuDateTimeInfoAddRow.Visible = this.AllowAddRow;
            this.mnuDateTimeInfoInsertRow.Visible = this.AllowAddRow;
            this.mnuDateTimeInfoDeleteRow.Visible = this.AllowDeleteRow;
            this.mnuDateTimeInfoDeleteAll.Visible = this.AllowDeleteRow;
            this.mnuDateTimeInfoReloadDefaults.Visible = true;
            this.grdNameValuePairs.ContextMenu = this.mnuDateTimeInfo;
        }
        private void SetCellWrapping()
        {
            if (m_bWrapCellText)
            {
                this.grdNameValuePairs.DisplayLayout.Bands[0].Override.CellMultiLine = DefaultableBoolean.True;
                this.grdNameValuePairs.DisplayLayout.Bands[0].Override.RowSizing = RowSizing.AutoFree;
            }
            else
            {
                this.grdNameValuePairs.DisplayLayout.Bands[0].Override.CellMultiLine = DefaultableBoolean.False;
                this.grdNameValuePairs.DisplayLayout.Bands[0].Override.RowSizing = RowSizing.Default;
            }
        }
        private void SetControlHeight()
        {
            this.Height = (int)(Math.Min(m_iVisibleRows, (String.CountChrs(this.InitialContent, '�') / 2) + 1) * 17.5);
            this.Height = this.grdNameValuePairs.Height;
        }
        /// <summary>
        /// initializes grid
        /// </summary>
        private void UpdateGrid(string xGridValue)
        {
            //get data from grid
            grdNameValuePairs.UpdateData();

            //create data storage
            DataTable oDT = new DataTable("Table");

            oDT.Clear();
            oDT.Columns.Add("Label", typeof(string));
            oDT.Columns.Add("Value", typeof(string));

            //populate from XML
            XML.XmlDocument oXML = new XML.XmlDocument();
            oXML.LoadXml(xGridValue);
            XML.XmlNodeList oNodeList = oXML.DocumentElement.SelectNodes("child::*");

            // Nodes will alternate Labels and Values
            for (int i = 0; i < oNodeList.Count - 1; i = i + 2)
            {
                XML.XmlNode oNode = oNodeList.Item(i);
                string xLabel = oNode.InnerText;
                string xValue = oNode.NextSibling.InnerText;
                //Skip empty row if not allowed
                if (this.AllowEmptyRow || (xLabel != "" || xValue != ""))
                    oDT.Rows.Add(new object[] { xLabel=="{Empty Row}" ? "": xLabel, xValue });
            }
            this.grdNameValuePairs.DataSource = oDT;
            this.grdNameValuePairs.DataBind();
            this.grdNameValuePairs.Refresh();
            if (m_iLabelColumnWidth > 0)
            {
                this.grdNameValuePairs.DisplayLayout.Bands[0].Columns[0].Width = m_iLabelColumnWidth;
                this.grdNameValuePairs.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            }
            else
            {
                this.grdNameValuePairs.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            }
            if (m_bAllowColumnResize)
                this.grdNameValuePairs.DisplayLayout.Override.AllowColSizing = AllowColSizing.Synchronized;
            else
                this.grdNameValuePairs.DisplayLayout.Override.AllowColSizing = AllowColSizing.None;
            SetControlHeight();
            SetCellWrapping();
            //if ((m_iTabStopColumns & mpTabStopsConfiguration.GridColumn1) > 0)
            //    this.grdNameValuePairs.DisplayLayout.Bands[0].Columns[0].TabStop = true;
            //else
            //    this.grdNameValuePairs.DisplayLayout.Bands[0].Columns[0].TabStop = false;

            //if ((m_iTabStopColumns & mpTabStopsConfiguration.GridColumn2) > 0)
            //    this.grdNameValuePairs.DisplayLayout.Bands[0].Columns[1].TabStop = true;
            //else
            //    this.grdNameValuePairs.DisplayLayout.Bands[0].Columns[1].TabStop = false;
        }
        /// <summary>
        /// returns the value of the grid as xml
        /// </summary>
        private string ToXML()
        {
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();
            int iItemCount = 0;

            //get data from grid
            grdNameValuePairs.UpdateData();
            DataTable oDT = (DataTable)this.grdNameValuePairs.DataSource;

            //Don't return any empty rows if none are populated
            if (this.AllValuesEmpty() && this.RemoveEmptyValues)
                return "";

            //cycle through rows
            foreach (DataRow oRow in oDT.Rows)
            {
                string xLabel = oRow["Label"] == null ? "":oRow["Label"].ToString();
                string xValue = oRow["Value"] == null ? "":oRow["Value"].ToString();

                if (!(xLabel == "" && xValue == "") || this.AllowEmptyRow)
                {
                    if (!(xLabel != "" && xValue == "") || !this.RemoveEmptyValues)
                    {
                        //replace reserved characters with escape sequences
                        xLabel = LMP.String.ReplaceXMLChars(xLabel);
                        xValue = LMP.String.ReplaceXMLChars(xValue);
                        if (xLabel == "")
                            xLabel = "{Empty Row}";
                        //row is not empty - add to content string
                        string xFormat = "<Label Index=\"{0}\">{1}</Label><Value Index=\"{0}\">{2}</Value>";
                        oSB.AppendFormat(xFormat, ++iItemCount, xLabel, xValue);
                    }
                }
            }
            return oSB.ToString();
        }

        private string ToDelimitedString()
        {
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();

            //get data from grid
            grdNameValuePairs.UpdateData();
            DataTable oDT = (DataTable)this.grdNameValuePairs.DataSource;


            if (this.AllValuesEmpty() && this.RemoveEmptyValues)
                return "";

            //GLOG 4559
            string xSep = m_iNameValueSeparator == mpNameValueSeparator.Spaces ? "  " : "\t";
            //cycle through rows
            foreach (DataRow oRow in oDT.Rows)
            {
                string xLabel = oRow["Label"] == null ? "" : oRow["Label"].ToString();
                string xValue = oRow["Value"] == null ? "" : oRow["Value"].ToString();

                if (!(xLabel == "" && xValue == "") || this.AllowEmptyRow)
                {
                    if (!(xLabel != "" && xValue == "") || !this.RemoveEmptyValues)
                    {
                        //GLOG 5331: This is not appropriate since non-XML string is being returned
                        ////replace reserved characters with escape sequences
                        //xLabel = LMP.String.ReplaceXMLChars(xLabel);
                        //xValue = LMP.String.ReplaceXMLChars(xValue);

                        string xFormat = "";
                        if (xLabel == "" && xValue == "")
                            xFormat = "\r\n";
                        else
                            //row is not empty - add to content string
                            //GLOG 4559
                            xFormat = "{0}" + xSep + "{1}\r\n";

                        oSB.AppendFormat(xFormat, xLabel, xValue);
                    }
                }
            }
            string xTemp = oSB.ToString();
            //Remove last delimiter
            if (xTemp != "")
                xTemp = xTemp.TrimEnd(new char[] {'\r', '\n'});
            return xTemp;
        }

        /// <summary>
        /// Setup text of interface items
        /// </summary>
        private void SetupResourceStrings()
        {
            if (!this.DesignMode)
            {              
                this.mnuDateTimeInfoClearRow.Text = LMP.Resources.GetLangString("Dialog_Reline_ClearRow");
                this.mnuDateTimeInfoDeleteAll.Text = LMP.Resources.GetLangString("Dialog_Reline_DeleteAll");
                this.mnuDateTimeInfoDeleteRow.Text = LMP.Resources.GetLangString("Dialog_Reline_DeleteRow");
                //GLOG : 6297 : CEH
                this.mnuDateTimeInfoCut.Text = LMP.Resources.GetLangString("Dialog_Reline_Cut");
                this.mnuDateTimeInfoCopy.Text = LMP.Resources.GetLangString("Dialog_Reline_Copy");
                this.mnuDateTimeInfoPaste.Text = LMP.Resources.GetLangString("Dialog_Reline_Paste");
                this.mnuDateTimeInfoInsertRow.Text = LMP.Resources.GetLangString("Dialog_Reline_InsertRow");
                this.mnuDateTimeInfoAddRow.Text = LMP.Resources.GetLangString("Dialog_Reline_AddRow");
                this.mnuDateTimeInfoReloadDefaults.Text = LMP.Resources.GetLangString("Dialog_NameValue_ReloadDefaults");
            }
        }
        ///Gets out of grid if the user is pressing enter 
        /// of the last cell - called only by grdSpecial_KeyDown
        /// </summary>
        private bool EnterOutIfAppropriate()
        {
            bool bEnterHandled = false;
            UltraGridRow oRow = this.grdNameValuePairs.ActiveCell.Row;

            int iCol = this.grdNameValuePairs.ActiveCell.Column.Index;

            //check if in empty new row
            int iRows = ((DataTable)this.grdNameValuePairs.DataSource).Rows.Count;

            if ((oRow.Index == iRows - 1) && (iCol == 1))
            {
                bEnterHandled = !string.IsNullOrEmpty(oRow.Cells[0].Text) 
                    || !string.IsNullOrEmpty(oRow.Cells[1].Text);
            }

            return bEnterHandled;
        }
        /// <summary>
        /// tabs out of grid if the user is tabbing out
        /// of the last cell - called only by grdSpecial_KeyDown
        /// </summary>
        private bool TabOutIfAppropriate()
        {
            bool bTabHandled = false;
            int iCurRow = this.grdNameValuePairs.ActiveCell.Row.Index;
            int iCol = this.grdNameValuePairs.ActiveCell.Column.Index;
            //check if in empty new row
            int iRows = ((DataTable)this.grdNameValuePairs.DataSource).Rows.Count;
            if (this.TabStopColumns == mpTabStopsConfiguration.None || 
                ((iCurRow == iRows - 1) && (iCol == 1)) ||
                ((iCurRow == iRows - 1) && (iCol == 0) && this.TabStopColumns == mpTabStopsConfiguration.GridColumn1) ||
                (iCurRow == 0 && this.AllValuesEmpty() && grdNameValuePairs.ActiveCell.Text == ""))
            {
                //user is in last cell of grid				
                //notify subscribers that the tab was pressed
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(false));

                bTabHandled = true;
            }
            return bTabHandled;
        }
        /// <summary>
        /// Return true if all value cells are empty
        /// </summary>
        /// <returns></returns>
        private bool AllValuesEmpty()
        {
            //get data from grid
            grdNameValuePairs.UpdateData();
            DataTable oDT = (DataTable)this.grdNameValuePairs.DataSource;

            //cycle through rows
            foreach (DataRow oRow in oDT.Rows)
            {
                if (oRow["Value"] != null && oRow["Value"].ToString() != "")
                    return false;
            }
            return true;
        }
        /// <summary>
        /// tabs out of grid if the
        /// user is shift-tabbing out of the first cell-
        /// called only by grdSpecial_KeyDown
        /// </summary>
        private bool ShiftTabOutIfAppropriate()
        {
            bool bTabHandled = false;
            int iCurRow = this.grdNameValuePairs.ActiveCell.Row.Index;
            int iCol = this.grdNameValuePairs.ActiveCell.Column.Index;
            //check if in empty new row
            int iRows = ((DataTable)this.grdNameValuePairs.DataSource).Rows.Count;
            if (this.TabStopColumns == mpTabStopsConfiguration.None || (iCurRow == 0 && iCol == 0) || (iCurRow == 0 && iCol == 1 && this.TabStopColumns == mpTabStopsConfiguration.GridColumn2))
            {
                //user is in first cell of grid				
                //notify subscribers that the tab was pressed
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(true));

                bTabHandled = true;
            }
            return bTabHandled;
        }
        /// <summary>
        /// adjusts selection in grid
        /// </summary>
        private void SetGridSelection()
        {
            //if first row has label but no value, select value cell -
            //otherwise, select label cell
            if (this.grdNameValuePairs.Rows.Count == 0)
                return;

            DataRow oRow = ((DataTable)this.grdNameValuePairs.DataSource).Rows[0];
          
            string xLabel = oRow["Label"].ToString();
            string xValue = oRow["Value"].ToString();
            if ( m_iTabStopColumns == mpTabStopsConfiguration.GridColumn2 || (xLabel != "") && (string.IsNullOrEmpty(xValue) && m_iTabStopColumns != mpTabStopsConfiguration.GridColumn1))
                this.grdNameValuePairs.ActiveCell = this.grdNameValuePairs.Rows[0].Cells["Value"];
            else
                this.grdNameValuePairs.ActiveCell = this.grdNameValuePairs.Rows[0].Cells["Label"];

            this.grdNameValuePairs.PerformAction(UltraGridAction.EnterEditMode);
        }
        /// <summary>
        /// converts to XML and returns the specified delimited string
        /// </summary>
        /// <param name="xDelimitedString"></param>
        /// <returns></returns>
        private string ConvertDelimitedStringToXML(string xDelimitedString)
        {
            if (xDelimitedString == "")
                return "";
            System.Text.StringBuilder oSB = new System.Text.StringBuilder();

            //split into array
            string[] aItems = xDelimitedString.Split('�');
            for (int i = 0; i < aItems.Length; i++)
            {
                //GLOG 5331: Replace any reserved XML characters in Delimited string
                if (i % 2 == 0)
                    //even index - this is a name
                    oSB.AppendFormat("<Label index = '{0}'>{1}</Label>", i/2, LMP.String.ReplaceXMLChars(aItems[i]));
                else
                    //odd index - this is a value
                    oSB.AppendFormat("<Value index = '{0}'>{1}</Value>", (i - 1) / 2, LMP.String.ReplaceXMLChars( aItems[i]));
            }

            return oSB.ToString();
        }
        #endregion
        #region *********************event handlers*********************
        private void grdNameValuePair_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            try
            {
                //if tab is handled in TabOutIfAppropriate function
                //suppress the keystroke - deals with Office 2003 issue
                if (e.KeyCode == Keys.Tab && e.Shift == false)
                {
                    if (TabOutIfAppropriate())
                        e.SuppressKeyPress = true;
                    else
                    {
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell);
                        grdNameValuePairs.SuspendLayout();
                        try
                        {
                            //skip cells not configured for tab stop
                            if (grdNameValuePairs.ActiveCell.Column.Index == 1 && m_iTabStopColumns < mpTabStopsConfiguration.GridColumn2)
                                grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell);
                            else if (grdNameValuePairs.ActiveCell.Column.Index == 0 && m_iTabStopColumns == mpTabStopsConfiguration.GridColumn2)
                                grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell);
                            grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        }
                        catch
                        {
                            grdNameValuePairs.ResumeLayout();
                        }
                        e.SuppressKeyPress = true;
                    }

                }
                else if (e.KeyCode == Keys.Tab && e.Shift == true)
                {
                    if (ShiftTabOutIfAppropriate())
                        e.SuppressKeyPress = true;
                    else
                    {
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        //skip cells not configured for tab stop
                        if (grdNameValuePairs.ActiveCell.Column.Index == 1 && m_iTabStopColumns < mpTabStopsConfiguration.GridColumn2)
                            grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        else if (grdNameValuePairs.ActiveCell.Column.Index == 0 && m_iTabStopColumns == mpTabStopsConfiguration.GridColumn2)
                            grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        e.SuppressKeyPress = true;
                    }
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    if (EnterOutIfAppropriate())
                    {
                        e.SuppressKeyPress = true;
                        this.mnuDateTimeInfoAddRow_Click(this, e);
                    }
                    else
                    {
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell);
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        e.SuppressKeyPress = true;
                    }
                }
                else if (e.KeyCode == Keys.Left)
                {
                    if (grdNameValuePairs.ActiveCell.SelStart == 0)
                    {
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        e.SuppressKeyPress = true;
                    }
                }
                else if (e.KeyCode == Keys.Right)
                {
                    if (grdNameValuePairs.ActiveCell.SelStart == grdNameValuePairs.ActiveCell.Text.Length)
                    {
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell);
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        e.SuppressKeyPress = true;
                    }
                }
                else if (e.KeyCode == Keys.Up)
                {
                    if (grdNameValuePairs.ActiveCell.SelStart == 0)
                    {
                        grdNameValuePairs.PerformAction(UltraGridAction.AboveCell);
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        e.SuppressKeyPress = true;
                    }
                }
                else if (e.KeyCode == Keys.Down)
                {
                    if (grdNameValuePairs.ActiveCell.SelStart == grdNameValuePairs.ActiveCell.Text.Length)
                    {
                        grdNameValuePairs.PerformAction(UltraGridAction.BelowCell);
                        grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        e.SuppressKeyPress = true;
                    }
                }
                else
                {
                    if (this.KeyPressed != null)
                        this.KeyPressed(this, e);
                }

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Clear selected row in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuDateTimeInfoClearRow_Click(object sender, System.EventArgs e)
        {
            try
            {
                //update data set with latest grid values -
                //this is an essential part of refreshing
                this.grdNameValuePairs.UpdateData();

                //clear row
                int iRow = this.grdNameValuePairs.ActiveCell.Row.Index;
                DataTable oDT = (DataTable)this.grdNameValuePairs.DataSource;
                DataRow oRow = oDT.Rows[iRow];
                oRow["Label"] = "";
                oRow["Value"] = "";

                //refresh
                this.grdNameValuePairs.Rows[iRow].Refresh();
                this.grdNameValuePairs.ActiveCell = this.grdNameValuePairs.Rows[iRow].Cells["Label"];
                this.grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        ///// <summary>
        ///// Handle MouseDown event in grid
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void grdNameValuePair_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Button == MouseButtons.Right)
        //            this.mnuDateTimeInfo.Show(this.grdNameValuePairs, new Point(e.X, e.Y));
        //    }
        //    catch (System.Exception oE)
        //    {
        //        LMP.Error.Show(oE);
        //    }
        //}
        private void grdNameValuePair_KeyUp(object sender, KeyEventArgs e)
       {
            if (this.KeyReleased != null)
                //raise keyreleased event
                this.KeyReleased(this, e);
        }
        /// <summary>
        /// Delete selected row in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuDateTimeInfoDeleteRow_Click(object sender, System.EventArgs e)
        {
            try
            {
                //update data set with latest grid values -
                //this is an essential part of refreshing
                this.grdNameValuePairs.UpdateData();

                int iRow = this.grdNameValuePairs.ActiveRow.Index;
                DataTable oDT = (DataTable)this.grdNameValuePairs.DataSource;
                DataRow oRow = oDT.Rows[iRow];
                if (oDT.Rows.Count > 1)
                    //delete row
                    oRow.Delete();
                else
                {
                    //there's only one row in the table - just clear it
                    oRow["Label"] = "";
                    oRow["Value"] = "";
                }
                oDT.AcceptChanges();
                //refresh
                this.grdNameValuePairs.Refresh();
                SetControlHeight();
                iRow = Math.Min(iRow, grdNameValuePairs.Rows.Count - 1);
                this.grdNameValuePairs.ActiveCell = grdNameValuePairs.Rows[iRow].Cells["Label"];
                this.grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Clear all rows in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuDateTimeInfoDeleteAll_Click(object sender, System.EventArgs e)
        {
            try
            {
                //update data set with latest grid values -
                //this is an essential part of refreshing
                this.grdNameValuePairs.UpdateData();

                //clear data table
                DataTable oDT = (DataTable)this.grdNameValuePairs.DataSource;
                oDT.Clear();

                //add one empty row
                oDT.Rows.Add(new object[] { "", "" });
                oDT.AcceptChanges();
                //refresh
                this.grdNameValuePairs.Refresh();
                SetControlHeight();
                this.grdNameValuePairs.ActiveCell = this.grdNameValuePairs.Rows[0].Cells["Label"];
                this.grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Add row to end of grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuDateTimeInfoAddRow_Click(object sender, EventArgs e)
        {
            try
            {
                //insert new row
                DataTable oDT = (DataTable)this.grdNameValuePairs.DataSource;
                // Add new row to end
                oDT.Rows.Add(new object[] { "", "" });
                this.grdNameValuePairs.Refresh();
                SetControlHeight();
                this.grdNameValuePairs.ActiveCell = this.grdNameValuePairs.Rows[oDT.Rows.Count - 1].Cells["Label"];
                this.grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Insert row in the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuDateTimeInfoInsertRow_Click(object sender, System.EventArgs e)
        {
            try
            {
                //insert new row
                this.grdNameValuePairs.UpdateData();
                int iNewRow = grdNameValuePairs.ActiveRow.Index;
                DataTable oDT = (DataTable)this.grdNameValuePairs.DataSource;
                if (iNewRow > oDT.Rows.Count || iNewRow < 0)
                {
                    // Add new row to end
                    oDT.Rows.Add(new object[] { "", "" });
                    iNewRow = oDT.Rows.Count - 1;
                }
                else
                {
                    // Insert before current row
                    DataRow oNewRow = oDT.NewRow();
                    oDT.Rows.InsertAt(oNewRow, iNewRow);
                }
                oDT.AcceptChanges();
                this.grdNameValuePairs.Refresh();
                SetControlHeight();
                this.grdNameValuePairs.ActiveCell = this.grdNameValuePairs.Rows[iNewRow].Cells["Label"];
                this.grdNameValuePairs.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Revert to default name/value pairs for control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuDateTimeInfoReloadDefaults_Click(object sender, EventArgs e)
        {
            try
            {
                string xVal = this.Value;
                xVal = CombineInitialAndEnteredValues(m_xInitialContent, xVal);
                this.Value = xVal;
                this.IsDirty = true;
                SetGridSelection();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Combine the default values and entered values for the grid so that 
        /// when the grid is reset to display the default values the entered
        /// values can be preserved.
        /// </summary>
        /// <param name="xInitialContent"></param>
        /// <param name="xEnteredContent"></param>
        /// <returns></returns>
        private string CombineInitialAndEnteredValues(string xInitialContent, string xEnteredContent)
        {
            char cInitValsFieldDelimiter = '�';

            char cEnteredValsFieldDelimiter = '\t';
            char [] cEnteredValsRecordDelimitier = {'\r', '\n'};

            DataSet dsNameValuePairs = new DataSet();
            DataTable dtNameValuePairs = new DataTable();
            dtNameValuePairs.Columns.Add("Name", typeof(string));
            dtNameValuePairs.Columns.Add("Value", typeof(string));
            dtNameValuePairs.Columns.Add("IsDefault", typeof(bool));
            dsNameValuePairs.Tables.Add(dtNameValuePairs);

            string[] axEnteredRecords = xEnteredContent.Split(cEnteredValsRecordDelimitier, StringSplitOptions.RemoveEmptyEntries);

            string[] axDefaultNameValuePairs = xInitialContent.Split(cInitValsFieldDelimiter);

            for (int i = 0; i < axDefaultNameValuePairs.Length/2; i++)
            {
                DataRow drNameValuePairsRow = dtNameValuePairs.NewRow();
    
                drNameValuePairsRow["Name"] = axDefaultNameValuePairs[2 * i];
                drNameValuePairsRow["Value"] = axDefaultNameValuePairs[2 * i + 1];
                drNameValuePairsRow["IsDefault"] = true;
                dtNameValuePairs.Rows.Add(drNameValuePairsRow);
            }

            for(int i = 0; i < axEnteredRecords.Length; i++)
            {
                string[] axEnteredNameValuePairs = axEnteredRecords[i].Split(cEnteredValsFieldDelimiter);
                string xFilter = string.Format("Name = '{0}' AND IsDefault = 'true'", axEnteredNameValuePairs[0]);

                DataRow [] aoRows = dtNameValuePairs.Select(xFilter);

                if(aoRows.Length == 0)
                {
                    // We've found no unset rows with this field name. Create a row 
                    // to hold the field and its value.
                    DataRow drNameValuePairsRow = dtNameValuePairs.NewRow();
        
                    drNameValuePairsRow["Name"] = axEnteredNameValuePairs[0];
                    drNameValuePairsRow["Value"] = axEnteredNameValuePairs[1];
                    drNameValuePairsRow["IsDefault"] = false;

                    dtNameValuePairs.Rows.Add(drNameValuePairsRow);
                }
                else
                {
                    // We've found a row that is unset with this field name. Change the 
                    // value from its default to the entered value.
                    aoRows[0]["Value"] = axEnteredNameValuePairs[1];
                    aoRows[0]["IsDefault"] = false;
                }
            }

            System.Text.StringBuilder sbInitialValues = new System.Text.StringBuilder();

            foreach(DataRow oRow in dtNameValuePairs.Rows)
            {
                sbInitialValues.Append((string)oRow[0] + cInitValsFieldDelimiter + (string)oRow[1] + cInitValsFieldDelimiter);
            }

            return sbInitialValues.ToString();
        }

        /// <summary>
        /// DateTime grid has received focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdNameValuePair_Enter(object sender, System.EventArgs e)
        {
            try
            {
                //initialize/restore selection
                SetGridSelection();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Control is being loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DateTimeInfo_Load(object sender, EventArgs e)
        {
            try
            {
                SetupResourceStrings();
                if (this.DesignMode)
                    ExecuteFinalSetup();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Contents of DateTime grid have changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdNameValuePair_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    this.IsDirty = true;
                    //notify that value has changed, if necessary
                    if (ValueChanged != null)
                        ValueChanged(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Grid context menu is about to be displayed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuDateTimeInfo_Popup(object sender, EventArgs e)
        {
            int iRow = -1;
            try
            {
                iRow = grdNameValuePairs.ActiveRow.Index;
            }
            catch { }
            try
            {
                // Don't display insert if no row is selected
                if (iRow > -1)
                    mnuDateTimeInfoInsertRow.Visible = this.AllowAddRow;
                else
                    mnuDateTimeInfoInsertRow.Visible = false;

                //GLOG : 6297 : CEH
                //enable/disable copy & cut context menu items
                //GLOG : 7360 : JSW
                //make sure cell is in edit mode, or error will occur on right click.
                if (this.grdNameValuePairs.ActiveCell.IsInEditMode)
                {
                    mnuDateTimeInfoCopy.Enabled = (this.grdNameValuePairs.ActiveCell.SelLength != 0);
                    mnuDateTimeInfoCut.Enabled = (this.grdNameValuePairs.ActiveCell.SelLength != 0);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        private void grdNameValuePair_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    this.IsDirty = true;
                    if (m_bWrapCellText)
                        grdNameValuePairs.UpdateData();
                    //notify that value has changed, if necessary
                    if (ValueChanged != null)
                        ValueChanged(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Called when Cell value changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdNameValuePairs_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            try
            {
                if (m_bWrapCellText)
                    e.Row.PerformAutoSize();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6297 : CEH
        private void mnuDateTimeInfoPaste_Click(object sender, EventArgs e)
        {
            try
            {
                this.grdNameValuePairs.ActiveCell.SelText = Clipboard.GetText();
                this.Refresh();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuDateTimeInfoCut_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.grdNameValuePairs.ActiveCell.SelText, true);
                this.grdNameValuePairs.ActiveCell.SelText = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuDateTimeInfoCopy_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.grdNameValuePairs.ActiveCell.SelText, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }


        #endregion
        #region *********************IControl Members*********************

        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;
        public event EnterKeyPressedHandler EnterKeyPressed;

        public void ExecuteFinalSetup()
        {
            try
            {
                LMP.Trace.WriteInfo("ExecuteFinalSetup");

                this.Value = m_xInitialContent;

                SetControlHeight();
                SetMenus();
                m_bInitialized = true;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ControlEventException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    "NameValuePairGrid", oE);
            }
        }

        [DescriptionAttribute("Gets/Sets the selected value.")]
        [Browsable(false)]
        public string Value
        {
            get
            {
                if (!this.DesignMode && m_bInitialized)
                    if (m_bInputIsXML)
                        return ToXML();
                    else
                        return ToDelimitedString();
                else
                    return "";
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    return;

                string xXML = "";
                //GLOG 4559
                string xSep = m_iNameValueSeparator == mpNameValueSeparator.Spaces ? "  " : "\t";
                if (value.StartsWith("<"))
                    //consider value to be xml
                    xXML = value;
                else
                {
                    xXML = value;

                    //GLOG 4559: If value doesn't contain the defined separator,
                    //it might have been created by a differently configured control
                    //in another pleading type then saved as data.
                    //In this case, replace the alternate separator with that 
                    //currently configured.
                    if (xXML.IndexOf(xSep) == -1)
                    {
                        if (m_iNameValueSeparator == mpNameValueSeparator.Spaces)
                            xXML = xXML.Replace("\t", xSep);
                        else
                            xXML = xXML.Replace("  ", xSep);
                    }
                    //GLOG #4238 - dcf
                    xXML = xXML.Replace("\v", xSep); //GLOG 4559

                    xXML = xXML.Replace("\r\n", "\r");
                    char cPrev = 'x';
                    string xTemp = "";
                    string xCurChars = "";
                    for (int i = 0; i < xXML.Length; i++)
                    {
                        char c = xXML.Substring(i, 1).ToCharArray()[0];
                        if (c == '\r')
                        {
                            //Replace each return except first in series with 2 delimiters
                            if (cPrev == '\r' || i == 0)
                                xCurChars = "��";
                            else
                                xCurChars = "�";
                        }
                        else
                            xCurChars = c.ToString();
                        xTemp = xTemp + xCurChars;
                        cPrev = c;   
                    }
                    xXML = xTemp;
                    //GLOG 4559
                    xXML = xXML.Replace(xSep, "�");
                    //consider value to be delimited string
                    xXML = ConvertDelimitedStringToXML(xXML);
                }

                //value is xml
                XML.XmlDocument oXML = new XML.XmlDocument();
                try
                {
                    oXML.LoadXml("<NameValuePairs>" + xXML + "</NameValuePairs>");
                }
                catch (System.Exception oE)
                {
                    //invalid xml - alert
                    throw new LMP.Exceptions.XMLException(
                        "Error_InvalidDateTimeGridXML" + value, oE);
                }

                UpdateGrid(oXML.OuterXml);

                this.IsDirty = true;
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion*******
        #region *********************protected members*********************
        public event TabPressedHandler TabPressed;
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Return)
                // Need this so that Enter will generate WM_KEYDOWN for EnterKeyPressed function
                return true;
            else if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            int iQualifierKeys;
            bool bShiftPressed;
            bool bEnterKeyPressed = LMP.OS.EnterKeyPressed(m, out iQualifierKeys);
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (bEnterKeyPressed)
            {
                if (this.EnterKeyPressed != null)
                    this.EnterKeyPressed(this, new EnterKeyPressedEventArgs(iQualifierKeys));
                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        #endregion


    }
}
