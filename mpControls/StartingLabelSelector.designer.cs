namespace LMP.Controls
{
    partial class StartingLabelSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdLabels = new LMP.Controls.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grdLabels)).BeginInit();
            this.SuspendLayout();
            // 
            // grdLabels
            // 
            this.grdLabels.AllowUserToAddRows = false;
            this.grdLabels.AllowUserToDeleteRows = false;
            this.grdLabels.AllowUserToResizeColumns = false;
            this.grdLabels.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.grdLabels.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grdLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.grdLabels.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdLabels.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grdLabels.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.grdLabels.ColumnHeadersHeight = 4;
            this.grdLabels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdLabels.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdLabels.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdLabels.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdLabels.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.grdLabels.IsDirty = false;
            this.grdLabels.Location = new System.Drawing.Point(0, 0);
            this.grdLabels.MultiSelect = false;
            this.grdLabels.Name = "grdLabels";
            this.grdLabels.RowHeadersVisible = false;
            this.grdLabels.RowHeadersWidth = 4;
            this.grdLabels.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdLabels.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.grdLabels.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdLabels.Size = new System.Drawing.Size(94, 143);
            this.grdLabels.SupportingValues = "";
            this.grdLabels.TabIndex = 0;
            this.grdLabels.Tag2 = null;
            this.grdLabels.Value = null;
            // 
            // StartingLabelSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.Controls.Add(this.grdLabels);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "StartingLabelSelector";
            this.Size = new System.Drawing.Size(181, 143);
            this.Resize += new System.EventHandler(this.StartingLabelSelector_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.grdLabels)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView grdLabels;
    }
}
