using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace LMP.Controls
{
    public partial class HTMLTextBox : UserControl, IControl
    {
        #region *********************fields*********************
        bool m_bIsDirty = false;
        object m_oTag2;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructor*********************
        public HTMLTextBox()
        {
            InitializeComponent();

            //subscribe to textbox's TabPressed event
            this.txtHTML.TabPressed += new TabPressedHandler(txtHTML_TabPressed);
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// handles button click, calls method to display openFileDialog
        /// and retrieve html text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetHTML_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog oDlg = new OpenFileDialog())
                {
                    oDlg.Filter = "HTML File (*.html)|*.html|Word HTML File (*.mht)|*.mht|HTM File (*.htm)|*.htm|Text file (*.txt)|*.txt";
                    oDlg.Title = "Select File";
                    oDlg.Multiselect = false;

                    DialogResult oRet = oDlg.ShowDialog(this);

                    if (oRet == DialogResult.OK)
                        LoadHTML(oDlg.FileName);
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtHTML_TextChanged(object sender, System.EventArgs e)
        {
            this.m_bIsDirty = true;

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }
        /// <summary>
        /// broadcast that tab was pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void txtHTML_TabPressed(object sender, TabPressedEventArgs e)
        {
            //notify subscribers tab was pressed
            if (this.TabPressed != null)
                this.TabPressed(this, e);
        }
        /// <summary>
        /// Control is moved
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HTMLTextBox_Move(object sender, EventArgs e)
        {
            //This is to ensure subcontrols repaint after being moved
            //out of and back into visible area of tree
            try
            {
                this.btnGetHTML.Visible = false;
                this.txtHTML.Visible = false;
            }
            catch { }
            finally
            {
                this.btnGetHTML.Visible = true;
                this.txtHTML.Visible = true;
            }
        }
        #endregion
        #region*********************methods**************************
        /// <summary>
        /// retrieves html from file stream, sets value property
        /// </summary>
        /// <param name="dlgFileDialog"></param>
        private void LoadHTML(string xFileName)
        {
            //build storage for supported file extensions
            string[] xExt = { ".txt", ".htm", ".html", ".mht" };

            //validate file name string returned by openFileDialog
            bool bContinue = false;

            for (int i = 0; i < xExt.GetLength(0); i++)
            {
                if (xFileName.EndsWith(xExt[i]))
                {
                    bContinue = true;
                    break;
                }
            }

            //filename string does not contain supported extensions - 
            //message user and exit
            if (!bContinue)
            {
                string xMsg = LMP.Resources.GetLangString
                    ("Msg_FileExtensionNotSupportedByHTMLTextBox") + xFileName;

                MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);

                return;
            }

            //read file contents, build string and set control value
            string xText = File.ReadAllText(xFileName);

            //extract HTML
            string xHTML = xText.ToUpper();
            int iStart = Math.Max(xHTML.IndexOf("<HTML"), 0);
            if (iStart > 0)
            {
                int iEnd = xHTML.IndexOf("</HTML>") + 7;
                xText = xText.Substring(iStart, iEnd - iStart);
            }
           this.Value = xText;
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }

        /// <summary>
        /// gets/sets the value of the control
        /// </summary>
        public string Value
        {
            get { return String.ReplaceMPReservedChars(this.txtHTML.Text); }
            set{this.txtHTML.Text = String.RestoreMPReservedChars(value);}
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosting
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        /// <summary>
        /// Determine if the key pressed is recognized for input by the control
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        #endregion
    }
}
