using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win.UltraWinGrid;
using LMP.Data;

namespace LMP.Controls
{
    [Flags]
    public enum mpAuthorColumns : byte
    {
        Pen = 1,
        Name = 2,
        BarID = 4
    }
    public enum mpListTypes : byte
    {
        AllAuthors = 0,
        Attorneys = 1,
        NonAttorneys = 2,
        Admin = 3
    }
    /// <summary>
    /// Summary description for AuthorSelector.
    /// </summary>
    public partial class AuthorSelector : System.Windows.Forms.UserControl, IControl
    {
        #region *********************delegates*********************
        public delegate void UpdatePeopleListEventHandler(Control oControl, ref bool bUseCallBack);
        // GLOG : 3135 : JAB
        // Provide a mechanism to notify that an update of the authors has been requested.
        public delegate void UpdateAuthorsEventHandler(object sender);

        // GLOG : 2547 : JAB
        // Define the delegate to handle requests to edit the authors preferences.
        public delegate void EditAutorPreferencesEventHandler(object sender);
        #endregion
        #region *********************events*********************
        public event ValueChangedHandler ValueChanged;
        public event UpdatePeopleListEventHandler ShowUserPeopleManagerRequested;
        public event UpdateAuthorsEventHandler UpdateAuthorsRequested;

        // GLOG : 2547 : JAB
        // Introduce an event requesting the editting of authors preferences.
        public event EditAutorPreferencesEventHandler EditAuthorPreferencesRequested;
        #endregion
        #region *********************fields*********************
        private const int mpAuthorGridRowHeight = 16;
        private bool m_bInitialized = false;
        private short m_shMaxAuthors = 1;
        private short m_shDropDownRows = 8;
        private short m_shDisplayRows = 4;
        private bool m_bListFiltering = false;
        //private bool m_bAllowMultiselectDialog = false;
        private string m_xListWhereClause = "";
        private bool m_bAllowManualInput = false;
        private bool m_bAllowManualInputLeadAuthor = false;
        private string m_xContentString = "";
        private string m_xSelAuthorName = "";
        private string m_xSelAuthorID = "";
        private string m_xPendingSelectionID = null;
        private bool m_bListLoaded = false;
        private string m_xPrevListWhereClause = "";
        private bool m_bIsDirty;
        private bool m_bIsResetEmtpy;
        private object m_oTag2;
        private string m_xSupportingValues = "";
        private bool m_bIgnoreCellChange;
        private mpAuthorColumns m_iShowColumns = mpAuthorColumns.Name | mpAuthorColumns.Pen;
        private System.Timers.Timer m_oSetSelectedAuthor = new System.Timers.Timer();

        private System.Data.DataSet m_oPeopleDataSet = null;
        private static Image m_oPenBMP;
        private mpListTypes m_iListType = mpListTypes.AllAuthors;
        private int m_iL0 = 0;
        private int m_iL1 = 0;
        private int m_iL2 = 0;
        private int m_iL3 = 0;
        private int m_iL4 = 0;

        // GLOG : 3440 : JAB
        // Introduce a field for the authors drop down row that corresponds
        // to the author in the currently selected authors grid row.
        UltraGridRow m_oCurValidGridRow = null;

        #endregion
        #region *********************constructors*********************
        public AuthorSelector()
        {
            InitializeComponent();
            //stop timer until an author changes
            m_oSetSelectedAuthor.Stop();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
            m_oSetSelectedAuthor.Dispose();
            GC.SuppressFinalize(this);
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// Get/Set Maximum number of authors allowed
        /// </summary>
        public short MaxAuthors
        {
            set
            {
                if (value < 0)
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidMaxAuthorValue"));

                m_shMaxAuthors = Math.Min(value, (short)10);
                LMP.Trace.WriteNameValuePairs("MaxAuthors", m_shMaxAuthors);
                // At runtime, this is handled by ExecuteFinalSetup
                if (this.DesignMode)
                    SetupControlForMaxAuthors();
            }
            get { return m_shMaxAuthors; }
        }

        /// <summary>
        /// Get/Sets whether or not the author list can be filtered
        /// </summary>
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets whether or not the author list can be filtered.")]
        public bool ListFiltering
        {
            get { return m_bListFiltering; }
            set { m_bListFiltering = value; }
        }

        /// <summary>
        /// Get/Sets the filter SQL where clause that filters the author list
        /// </summary>
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets the filter SQL where clause that filters the author list.")]
        public string ListWhereClause
        {
            get { return m_xListWhereClause; }
            set { m_xListWhereClause = value; }
        }

        /// <summary>
        /// Get/Sets whether or not authors must be selected from the dropdown list
        /// </summary>
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets whether or not authors must be selected from the dropdown list.")]
        public bool AllowManualInput
        {
            get { return m_bAllowManualInput; }
            set
            {
                m_bAllowManualInput = value;
                SetupAllowManualInput(value);
            }
        }

        /// <summary>
        /// Get/Sets whether or not the lead author must be selected from the dropdown list
        /// </summary>
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets whether or not the lead author must be selected from the dropdown list.")]
        public bool AllowManualInputLeadAuthor
        {
            get { return m_bAllowManualInputLeadAuthor; }
            set { m_bAllowManualInputLeadAuthor = value; }
        }

        /// <summary>
        /// Get/Sets XML string specifying the authors returned/displayed
        /// </summary>
        [BrowsableAttribute(false)]
        [DescriptionAttribute("Get/Sets XML string specifying the authors returned/displayed.")]
        public string ContentString
        {
            get { return GetContentString(); }
            set
            {
                if (value != GetContentString())
                {
                    m_xContentString = value;
                    ParseContentString(value);
                }
            }

        }

        /// <summary>
        /// Get/Sets the number of rows displayed by the author dropdown
        /// </summary>
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets the number of rows displayed by the author dropdown.")]
        public short DropDownRows
        {
            get { return m_shDropDownRows; }
            set
            {
                if (value != m_shDropDownRows)
                {
                    m_shDropDownRows = Math.Min(value, (short)20);
                    LMP.Trace.WriteNameValuePairs("DropDownRows", m_shDropDownRows);
                    if (!this.DesignMode && m_bInitialized)
                    {
                        if (this.MaxAuthors == 1)
                        {
                            this.cmbAuthor.MaxDropDownItems = m_shDropDownRows;
                        }
                        else
                        {
                            this.ddAuthors.Height = this.ddAuthors.Rows[0].Height * m_shDropDownRows;
                            this.ddBarIDs.Height = this.ddAuthors.Height;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get/Sets the number of rows displayed by the author grid
        /// </summary>
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets the number of rows displayed in the author grid.")]
        public short DisplayRows
        {
            get { return m_shDisplayRows; }
            set
            {
                if (value != m_shDisplayRows)
                {
                    LMP.Trace.WriteNameValuePairs("DisplayRows", m_shDisplayRows);
                    m_shDisplayRows = value;
                    if (this.MaxAuthors > 1 & m_bInitialized)
                    {
                        //Set grid height to smaller of MaxAuthors and DisplayRows
                        this.Height = (mpAuthorGridRowHeight + 4) * Math.Min(this.MaxAuthors, this.m_shDisplayRows) + 1;
                        this.grdAuthors.Height = this.Height - 1;
                    }
                }
            }
        }
        /// <summary>
        /// Get/Sets the delay that occurs before actually registering a user selection
        /// </summary>
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets the delay that occurs before actually registering a user selection.")]
        public double SelectionDelay
        {
            get { return m_oSetSelectedAuthor.Interval; }
            set { m_oSetSelectedAuthor.Interval = value; }
        }

        /// <summary>
        /// Get/Sets the columns of the control that will be displayed
        /// </summary>
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the columns of the control that will be displayed.")]
        public mpAuthorColumns ShowColumns
        {
            get { return m_iShowColumns; }
            set
            {
                if (value != m_iShowColumns)
                {
                    LMP.Trace.WriteNameValuePairs("ShowColumns", m_iShowColumns);
                    m_iShowColumns = value;
                    if (this.MaxAuthors != 1 && m_bInitialized)
                        SetupControlForMaxAuthors();
                }
            }
        }
        /// <summary>
        /// Get/Sets which types of Authors to display in list
        /// </summary>
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets which types of Authors to display in list.")]
        public mpListTypes ListType
        {
            get { return m_iListType; }
            set 
            {
                if (value != m_iListType)
                {
                    m_iListType = value;
                    if (m_bInitialized && !this.DesignMode)
                        RefreshPeopleList();
                }
            }
        }

        /// <summary>
        /// Get/Sets the jurisdiction levels of Licenses to be displayed
        /// </summary>
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the jurisdiction levels of Licenses to be displayed")]
        public int[] Jurisdiction
        {
            get { return new int[] { m_iL0, m_iL1, m_iL2, m_iL3, m_iL4 }; }
            set
            {
                m_iL0 = value[0];
                m_iL1 = value[1];
                m_iL2 = value[2];
                m_iL3 = value[3];
                m_iL4 = value[4];
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// refreshes the dropdown; reinitializes to current author
        /// messages user if former current selection no longer exists in the list
        /// </summary>
        public void RefreshPeopleList()
        {
            this.m_bInitialized = false;

            //reset manual input
            bool bAllowManualInput = m_bAllowManualInput;
            m_bAllowManualInput = true;

            string xSelectedID = m_xSelAuthorID;

            //clear dataset
            m_oPeopleDataSet = null;

            //reload list and stop timer
            this.LoadList();
            m_oSetSelectedAuthor.Stop();

            if (this.MaxAuthors == 1)
            {
                //attempt to reset list to formerly selected author
                if (xSelectedID != "")
                    this.cmbAuthor.Value = xSelectedID;

                //author no longer exists in the collection - message user and 
                //select first item in the list
                if (this.cmbAuthor.SelectedRow == null)
                {
                    string xMsg = LMP.Resources.GetLangString("Msg_AuthorHasBeenDeleted");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    if (cmbAuthor.Rows.Count > 0)
                        this.cmbAuthor.SelectedRow = cmbAuthor.Rows[0];
                }
            }
            else
            {
                DataSet oDS = (DataSet)this.grdAuthors.DataSource;
                bool bLeadDeleted = false;
                //cycle through rows and remove any Authors no longer in the People List
                foreach (DataRow oRow in oDS.Tables["Table"].Rows)
                {
                    string xLead = oRow["Lead"].ToString();
                    string xID = oRow["ID"].ToString();
                    if (!xID.StartsWith("-") && !PersonIDIsInList(xID))
                    {
                        oRow.Delete();
                        if (xLead == "-1")
                            bLeadDeleted = true;
                        this.IsDirty = true;
                    }
                }
                if (bLeadDeleted)
                    oDS.Tables["Table"].Rows[0]["Lead"] = "-1";
                oDS.Tables["Table"].AcceptChanges();
                grdAuthors.Refresh();
            }
            //reset manual input
            m_bAllowManualInput = bAllowManualInput;
        }
        #endregion
        #region *********************internal procedures*********************
        /// <summary>
        /// Configure columns to display in the Grid
        /// </summary>
        private void SetVisibleColumns()
        {
            Infragistics.Win.UltraWinGrid.ColumnsCollection oCols = 
                this.grdAuthors.DisplayLayout.Bands[0].Columns;
            
            oCols["Source"].Hidden = true;
            oCols["ID"].Hidden = true;
            oCols["Lead"].Hidden = !((m_iShowColumns & mpAuthorColumns.Pen) == mpAuthorColumns.Pen);
            oCols["Name"].Hidden = false; // !((m_iShowColumns & mpAuthorColumns.Name) == mpAuthorColumns.Name);
            oCols["BarID"].Hidden = !((m_iShowColumns & mpAuthorColumns.BarID) == mpAuthorColumns.BarID);
        }
        /// <summary>
        /// loads the author list into the dropdown
        /// </summary>
        private void LoadList()
        {
            LMP.Trace.WriteInfo("LoadList");
            int iID1 = 0;
            int iID2 = 0;
            string xSelIDWhereClause = "";
            string xFullWhereClause = "";
            LocalPersons oPeople = null;
            mpTriState iAttorneysOnly = mpTriState.Undefined;
            
            if (this.ListType == mpListTypes.Attorneys)
                iAttorneysOnly = mpTriState.True;
            else if (this.ListType == mpListTypes.NonAttorneys)
                iAttorneysOnly = mpTriState.False;

            //m_oLoadListTimer.Enabled = false;

            DateTime t0 = DateTime.Now;

            //get people in the specified list -
            //since the ListWhereClause can be changed by the user, the
            //list might not have the default author in the list - thus, if there
            //is a pending selection, get this person as part of the list
            if (m_xPendingSelectionID != null)
            {
                LocalPersons.SplitID(m_xPendingSelectionID, out iID1, out iID2);
                xSelIDWhereClause = string.Concat("(ID1=", iID1.ToString(),
                    " AND ID2=", iID2.ToString(), ")");
            }

            if (m_oPeopleDataSet == null || (this.ListWhereClause != m_xPrevListWhereClause))
            {
                //get a new data set
                if (this.ListWhereClause != "")
                {
                    xFullWhereClause = this.ListWhereClause;
                    if (xSelIDWhereClause != "")
                        xFullWhereClause = string.Concat(this.ListWhereClause, " OR ", xSelIDWhereClause);
                }
                else
                {
                    //ListWhereClause is empty
                    xFullWhereClause = "";
                }

                //show only authors that are "office active"
                oPeople = new LocalPersons(mpPeopleListTypes.AllPeople, LMP.Data.Application.User.ID,
                    iAttorneysOnly, mpTriState.True, 0, xFullWhereClause, UsageStates.OfficeActive);

                //alert if no items in the list
                if (oPeople.Count == 0)
                {
                    string xMsg = null;
                    if (this.ListType == mpListTypes.Attorneys)
                    {
                        if (this.AllowManualInput && this.AllowManualInputLeadAuthor)
                            xMsg = LMP.Resources.GetLangString("Msg_NoAttorneysInList_ManualAllowed");
                        else
                            xMsg = LMP.Resources.GetLangString("Msg_NoAttorneysInList");
                    }
                    else
                    {
                        if (this.AllowManualInput && this.AllowManualInputLeadAuthor)
                            xMsg = LMP.Resources.GetLangString("Msg_NoPeopleInList_ManualAllowed");
                        else
                            xMsg = LMP.Resources.GetLangString("Msg_NoPeopleInList");
                    }
                    
                    MessageBox.Show(xMsg,  LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    m_bListLoaded = false;
                    return;
                }

                m_oPeopleDataSet = oPeople.ToDisplayFieldsDataSet(true);
                LMP.Trace.WriteNameValuePairs("oPeople.Count", oPeople.Count);
            }

            if (this.MaxAuthors == 1)
            {
                //load list into author combo
                this.cmbAuthor.DataSource = m_oPeopleDataSet;
                this.cmbAuthor.DataMember = "Table";
            }
            else
            {
                this.ddAuthors.DataSource = m_oPeopleDataSet;
                this.ddAuthors.DataMember = "Table";
            }

            m_bListLoaded = true;

            //select pending item if one exists
            if (m_xPendingSelectionID != null)
            {
                //TODO: implement this section - see corresponding mp10 VB code
            }

            LMP.Benchmarks.Print(t0, "Load List Array");
        }

        /// <summary>
        /// sets the properties of the combo box - must be 
        /// called after load list for property values to stick
        /// </summary>
        private void SetComboProperties()
        {
            LMP.Trace.WriteInfo("SetComboProperties");
            this.cmbAuthor.DisplayLayout.Bands[0].Columns["ID"].Hidden = true;
            this.cmbAuthor.DisplayLayout.Bands[0].Override.RowSpacingBefore = 0;
            this.cmbAuthor.DisplayLayout.Bands[0].Override.RowSpacingAfter = 0;
            this.cmbAuthor.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;

            this.cmbAuthor.ValueMember = "ID";
            this.cmbAuthor.DisplayMember = "DisplayName";

        }

        /// <summary>
        /// Sets the properties of Grid (for MaxAuthors > 1)
        /// </summary>
        private void SetGridProperties()
        {
            LMP.Trace.WriteInfo("SetGridProperties");
            //create storage for grid authors 
            DataSet oDS = new DataSet("AuthorGridData");

            DataTable oDT = new DataTable("Table");
            oDT.Columns.Add("Lead", typeof(string));
            oDT.Columns.Add("Name", typeof(string));
            oDT.Columns.Add("ID", typeof(string));
            oDT.Columns.Add("BarID", typeof(string));
            oDT.Columns.Add("Source", typeof(string));

            oDS.Tables.Add(oDT);

            DataTable oDT2 = new DataTable("Table1");
            oDT2.Columns.Add("Col1");
            oDS.Tables.Add(oDT2);

            this.grdAuthors.DataSource = oDS;
            //this.grdAuthors.DataMember = "Table";
            
            //this.grdAuthors.DisplayLayout.Bands[0] = mpAuthorGridRowHeight;

            Infragistics.Win.UltraWinGrid.ColumnsCollection oCols = this.grdAuthors.DisplayLayout.Bands[0].Columns;

            SetVisibleColumns();
            //ReassignDropDownControls(true);
            grdAuthors.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            grdAuthors.DisplayLayout.Override.DefaultRowHeight = mpAuthorGridRowHeight;
            //set display name column width to be the entire grid
            //minus the Lead and BarID columns
            oCols["Lead"].DefaultCellValue = "0";
            //oCols["Lead"].Width = 16;
            oCols["Lead"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

            if (!this.DesignMode)
            {
                //get bitmap for Pen if not already retrieved
                if (m_oPenBMP == null)
                {
                    m_oPenBMP = imageList1.Images[0];
                }

                Infragistics.Win.ValueList oVL = grdAuthors.DisplayLayout.ValueLists.Add("VLLead");
                //provide translation values for pen column
                Infragistics.Win.ValueListItem oVLItem = oVL.ValueListItems.Add("0");
                oVLItem.Appearance.Image = null;
                oVLItem = oVL.ValueListItems.Add("-1");
                oVLItem.Appearance.Image = m_oPenBMP;
                oVL.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.Picture;
                oCols["Lead"].ValueList = oVL;
                //set up dropdown
                oCols["Name"].ValueList = this.ddAuthors;
                this.ddAuthors.DisplayMember = "DisplayName";
                this.ddAuthors.ValueMember = "DisplayName";


                //this.ddAuthors.Height = this.ddAuthors.RowHeight * this.DropDownRows;
                this.ddAuthors.DataMember = "Table";

                //set up bar id column if necessary
                if (!oCols["BarID"].Hidden)
                {
                    oCols["BarID"].ValueList = this.ddBarIDs;
                    this.ddBarIDs.DisplayMember = "BarID";
                    //don't set ValueMember, since each row will have a different lookup list
                    this.ddBarIDs.ValueMember = null;
                    //this.ddBarIDs.Height = this.ddBarIDs.RowHeight * this.DropDownRows;
                    this.ddBarIDs.DataMember = "Table";
                }


            }
            //grdAuthors.DisplayLayout.Bands[0].MinRows = this.DisplayRows;
            grdAuthors.DisplayLayout.Bands[0].MaxRows = this.MaxAuthors;
            //resize controls
            ResizeControls();
        }

        /// <summary>
        /// sizes child controls based on usercontrol size
        /// </summary>
        private void ResizeControls()
        {
            //Ignore interim states when existing control is added to tree
            if (this.Width <= 0)
                return;

            LMP.Trace.WriteInfo("ResizeControls");
            //only resize after column visibility has been set -
            //this prevents the code from running on the first
            //resize event firing.  We explicity call this method
            //at the end of set grid properties
            if (this.MaxAuthors == 1)
            {
                //this.cmbAuthor.Width = this.Width;
                this.Height = this.cmbAuthor.Height;
                cmbAuthor.DropDownWidth = cmbAuthor.Width;
            }
            else
            {

                //Column widths are set in Designer
                //Name column will expand or contract based on grid width
                this.grdAuthors.Width = this.Width;
                //size the authors dropdown to the width of the
                //control minus the widths of visible columns
                Infragistics.Win.UltraWinGrid.ColumnsCollection oCols =
                    this.grdAuthors.DisplayLayout.Bands[0].Columns;

                //resize BarID column if visible - should
                //be 33% of grid up to a width of 70
                if (!oCols["BarID"].Hidden)
                {
                    //oCols["BarID"].Width = (int)Math.Min(this.grdAuthors.Width * .20, 70);
                    ddBarIDs.Width = oCols["BarID"].Width;
                }

                //resize Display Name column width
                //oCols["Name"].Width = this.Width - 12 - (oCols["Lead"].Width +
                //    (oCols["BarID"].Hidden ? 0 : oCols["BarID"].Width));
                ddAuthors.Width = oCols["Name"].Width;
                //this.grdAuthors.ExtendRightColumn = true;
                //Set grid height to smaller of MaxAuthors and DisplayRows
                this.Height = ((mpAuthorGridRowHeight + 4) * Math.Min(this.MaxAuthors, this.DisplayRows)) + 2;
                this.grdAuthors.Height = this.Height - 1;
            }
        }

        /// <summary>
        /// sets up control for manual input or not
        /// </summary>
        /// <param name="bAllowManualInput"></param>
        private void SetupAllowManualInput(bool bAllowManualInput)
        {
            if (this.MaxAuthors == 1)
            {
                this.cmbAuthor.LimitToList = !bAllowManualInput;
                this.cmbAuthor.AutoEdit = !bAllowManualInput;
                if (bAllowManualInput)
                    this.cmbAuthor.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDown;
                else
                    this.cmbAuthor.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
            }
        }

        /// <summary>
        /// Set Combo or Grid properties depending on MaxAuthors value
        /// </summary>
        private void SetupControlForMaxAuthors()
        {
            LMP.Trace.WriteInfo("SetupControlForMaxAuthors");
            //set properties, both in design and run-time
            if (this.MaxAuthors == 1)
            {
                SetComboProperties();
                this.Height = this.cmbAuthor.Height;
            }
            else
            {
                SetGridProperties();
                //this.Height = this.grdAuthors.Height;
            }

            SetupAllowManualInput(this.AllowManualInput);
            if (this.MaxAuthors == 1)
            {
                this.grdAuthors.Visible = false;
                this.cmbAuthor.Visible = true;
                cmbAuthor.ContextMenu = this.mnuContext;
                this.mnuContext_Delete.Visible = false;
                this.mnuContext_DeleteAll.Visible = false;
                this.mnuContext_MoveUp.Visible = false;
                this.mnuContext_MoveDown.Visible = false;
                this.mnuContext_Sep2.Visible = false;
                this.mnuContext_Sep1.Visible = false;
            }
            else
            {
                this.cmbAuthor.Visible = false;
                this.grdAuthors.Visible = true;
                this.grdAuthors.ContextMenu = this.mnuContext;
                this.mnuContext_Delete.Visible = true;
                this.mnuContext_DeleteAll.Visible = true;
                this.mnuContext_Sep1.Visible = true;
            }
        }

        /// <summary>
        /// returns the content string representing the authors in the control
        /// </summary>
        /// <returns></returns>
        private string GetContentString()
        {
            LMP.Trace.WriteInfo("GetContentString");
            string xXML = "";

            if (this.MaxAuthors == 1)
            {
                //get content from the selected author
                if (m_xSelAuthorID == null || m_xSelAuthorID == "")
                    //the author has been manually input
                    xXML = string.Concat("<Author Lead=\"-1\" ID=\"\" FullName=\"",
                        LMP.String.ReplaceXMLChars(m_xSelAuthorName), "\" BarID=\"\"></Author>");
                else
                    //the author is coming from the list
                    xXML = string.Concat("<Author Lead=\"-1\" ID=\"", m_xSelAuthorID,
                        "\" FullName=\"", m_xSelAuthorName, "\" BarID=\"\"></Author>");
            }
            else
            {
                grdAuthors.UpdateData();
                //get content from authors grid
                System.Text.StringBuilder oSB = new System.Text.StringBuilder();
                string xFormat = "<Author Lead=\"{0}\" ID=\"{1}\" FullName=\"{2}\" BarID=\"{3}\"></Author>";

                DataSet oDS = (DataSet)this.grdAuthors.DataSource;

                //cycle through rows, appending XML
                foreach (DataRow oRow in oDS.Tables["Table"].Rows)
                {
                    string xLead = oRow["Lead"].ToString();
                    string xName = LMP.String.ReplaceXMLChars(oRow["Name"].ToString());
                    string xID = oRow["ID"].ToString();
                    string xBarID = LMP.String.ReplaceXMLChars(oRow["BarID"].ToString());
                    oSB.AppendFormat(xFormat, xLead, xID, xName, xBarID);
                }

                xXML = oSB.ToString();
            }
            LMP.Trace.WriteNameValuePairs("xXML", xXML);
            return xXML;
        }

        //updates the content of the control based on the current content string
        private void ParseContentString(string xXML)
        {
            LMP.Trace.WriteInfo("ParseContentString");

            System.Xml.XmlDocument oXML = new System.Xml.XmlDocument();
            if (xXML == "" || xXML.Substring(0, 9).ToUpper() != "<AUTHORS>")
                //add document element
                oXML.LoadXml(string.Concat("<Authors>", xXML, "</Authors>"));
            else
                oXML.LoadXml(xXML);

            //get all authors
            System.Xml.XmlNodeList oNodes = oXML.DocumentElement.SelectNodes("child::Author");

            if (oNodes.Count == 0)
                return;
            else if (this.MaxAuthors == 1)
            {
                //set combo box content -
                //get the first author
                string xID = oNodes.Item(0).Attributes[1].Value;
                string xName = oNodes.Item(0).Attributes[2].Value;
                //Set Name if ID is valid
                if (xID != null && xID != "" && (this.AllowManualInput || PersonIDIsInList(xID)))
                {
                    //the author has an ID - check if author exists
                    if (!m_bListLoaded)
                    {
                        //add person's name to text portion of dropdown
                        this.cmbAuthor.Text = LMP.String.
                            RestoreXMLChars(oNodes.Item(0).Attributes[2].Value);
                        this.cmbAuthor.Value = xID;
                        this.cmbAuthor.Textbox.SelectionStart = 0;
                        this.cmbAuthor.Textbox.SelectionLength = this.cmbAuthor.Text.Length;

                        m_xSelAuthorID = xID;
                        m_xSelAuthorName = this.cmbAuthor.Text;

                        //set ID as a pending selection - the ID will
                        //be selected after the list is loaded
                        m_xPendingSelectionID = xID;
                    }
                    else
                    {
                        //list is loaded - select
                        this.cmbAuthor.Value = xID;
                        m_xSelAuthorID = xID;
                        //GLOG 3634: Set Text to Name if Manually entered
                        if (this.cmbAuthor.Text == xID && this.AllowManualInput)
                            this.cmbAuthor.Text = xName;
                        //DisplayFullNameInCombo();
                    }
                }
                else
                {
                    //author no longer exists in the collection - message user 
                    string xMsg = LMP.Resources.GetLangString("Msg_AuthorHasBeenDeleted");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            else
            {
                DataSet oDS = (DataSet)this.grdAuthors.DataSource;
                DataTable oDT = oDS.Tables["Table"];

                //delete any existing rows in DataTable
                oDT.Rows.Clear();

                //cycle through author nodes, adding each as row in datatable
                foreach (System.Xml.XmlNode oNode in oNodes)
                {
                    string xID = "";
                    string xLead = "0";
                    string xBarID = "";
                    //stop adding authors if max is reached
                    if (oDT.Rows.Count >= this.MaxAuthors)
                        break;

                    string xFullName = LMP.String
                        .RestoreXMLChars(oNode.Attributes["FullName"].Value);
                    try
                    {
                        xID = oNode.Attributes["ID"].Value;
                        xLead = oNode.Attributes["Lead"].Value;
                        xBarID = oNode.Attributes["BarID"].Value;
                    }
                    catch { }
                    //Add to grid if ID is valid
                    if (this.AllowManualInput || PersonIDIsInList(xID))
                    {
                        oDT.Rows.Add(new object[]{xLead, 
												 xFullName,
												 oNode.Attributes["ID"].Value,
												 xBarID,
												 null});
                    }
                }

                if (oDT.Rows.Count == 0)
                {
                    //author no longer exists in the collection - message user 
                    string xMsg = LMP.Resources.GetLangString("Msg_AuthorHasBeenDeleted");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //add one empty row
                    oDT.Rows.Add(new object[] { "", "" });
                }
                //set whether or not to allow additional authors
                SetAddNew();

                //update grid to reflect DataSet changes
                this.grdAuthors.Refresh();
            }
        }

        /// <summary>
        /// updates the authors in this control based on currently selected author
        /// </summary>
        private void UpdateAuthorsForSelection()
        {
            LMP.Trace.WriteInfo("UpdateAuthorsForSelection");
            if (this.MaxAuthors == 1)
            {
                if (this.cmbAuthor.SelectedRow == null)
                {
                    if (!m_bAllowManualInput)
                    {
                        if (m_xSelAuthorName != "")
                        {
                            //text must match an entry in the list and doesn't
                            MessageBox.Show(
                                LMP.Resources.GetLangString("Message_ManualEntryNotAllowed"),
                                LMP.String.MacPacProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            this.cmbAuthor.Text = m_xSelAuthorName;
                        }
                        return;
                    }
                    //author was manually input
                    if (m_xSelAuthorName != this.cmbAuthor.Text)
                    {
                        //author has changed
                        m_xSelAuthorName = this.cmbAuthor.Text;
                        //Assign random ID to manual author
                        Random oRand = new Random();
                        m_xSelAuthorID = "-" + oRand.Next(9999).ToString();
                        this.IsDirty = true;

                        //notify that value has changed, if necessary
                        if (ValueChanged != null)
                            ValueChanged(this, new EventArgs());
                    }
                }
                else
                {
                    //author was selected from list

                    if (m_xSelAuthorID != this.cmbAuthor.Value.ToString())
                    {
                        //author has changed
                        m_xSelAuthorID = this.cmbAuthor.Value.ToString();
                        //a selection from the list was made - get full name
                        Person oPerson = LocalPersons.GetPersonFromID(m_xSelAuthorID);
                        if (oPerson != null)
                            m_xSelAuthorName = oPerson.FullName;
                        else
                            m_xSelAuthorName = this.cmbAuthor.Text;
                        this.IsDirty = true;
                        if (ValueChanged != null)
                            ValueChanged(this, new EventArgs());

                    }
                }
            }
            else
            {
                // GLOG : 3440 : JAB
                // Validate the selection such that a duplicate entry is not made.
                if (!IsValidSelection())
                {
                    if (this.ddAuthors.SelectedRow != null)
                    {
                        MessageBox.Show( LMP.Resources.GetLangString("Message_DuplicateAuthor"),
                                            LMP.String.MacPacProductName,
                                            MessageBoxButtons.OK, 
                                            MessageBoxIcon.Exclamation);

                        this.ddAuthors.SelectedRow = m_oCurValidGridRow;

                        // Reset the value of the grid cell to its original value.
                        if (this.grdAuthors.ActiveRow != null)
                        {
                            if (m_oCurValidGridRow != null)
                            {
                                this.grdAuthors.ActiveRow.Cells["Name"].Value = m_oCurValidGridRow.Cells["DisplayName"].Value;
                            }
                            else
                            {
                                //GLOG - 3620 - ceh
                                m_bIsResetEmtpy = true;
                                this.grdAuthors.ActiveRow.Cells["Name"].Value = "";
                                m_bIsResetEmtpy = false;
                            }
                        }
                    }
                    return;
                }

                m_oCurValidGridRow = this.ddAuthors.SelectedRow;

                if (this.grdAuthors.ActiveCell.Row.Cells["Name"].Text == "")
                {
                    //the user deleted the name of the author - delete author row
                    DataSet oDS = (DataSet)this.grdAuthors.DataSource;
                    DataRowCollection oRows = oDS.Tables["Table"].Rows;

                    if (oRows.Count > 1)
                    {
                        //get current row
                        int iRow = this.grdAuthors.ActiveCell.Row.Index;
                        bool bHasPen = (this.grdAuthors.ActiveCell.Row.Cells["Lead"].Text.ToString() == "-1");
                        //delete author
                        //this.grdAuthors.DeleteSelectedRows();
                        oRows.RemoveAt(iRow);
                        this.grdAuthors.UpdateData();
                        this.grdAuthors.Refresh();
                        //set pen to first author
                        if (bHasPen)
                            AssignPen(0);

                        //TODO:select correct row after delete
                        if (iRow == oRows.Count - 1)
                            //we're deleting the last row - 
                            //select the new last row
                            this.grdAuthors.Rows[Math.Max(iRow - 1, 0)].Selected = true;
                        else
                            this.grdAuthors.Rows[iRow].Selected = true;
                    }
                    this.IsDirty = true;
                    return;
                }

                //set author info in grid row
                if (this.ddAuthors.SelectedRow != null && this.grdAuthors.ActiveCell.Row.Cells["Name"].Text == this.ddAuthors.SelectedRow.Cells["DisplayName"].Text)
                {
                    //author was selected from list - add ID to ID column
                    this.grdAuthors.ActiveCell.Row.Cells["ID"].Value = this.ddAuthors.SelectedRow.Cells["ID"].Text;
                }
                else
                {
                    //Assign random ID to manual author
                    Random oRand = new Random();
                    //author was manually entered - clear ID column cell
                    this.grdAuthors.ActiveCell.Row.Cells["ID"].Value = "-" + oRand.Next(9999).ToString();
                }

                if (!this.grdAuthors.ActiveCell.Row.Cells["ID"].Text.StartsWith("-"))
                {
                    //author is coming from the author list - 
                    //display full name
                    this.DisplayFullNameInGrid();

                    if (this.grdAuthors.ActiveRow.IsAddRow == true)
                    {
                        //current author has not been saved yet - save
                        this.grdAuthors.UpdateData();

                        SetAddNew();
                    }

                    AssignPenToNewAuthorIfNecessary();
                    FillBarIDs(true);
                    this.IsDirty = true;
                }
                else if (this.AllowManualInput)
                {
                    //author was manually entered
                    if (this.grdAuthors.ActiveRow.IsAddRow == true)
                    {	//current author has not been saved yet - save
                        this.grdAuthors.UpdateData();
                        SetAddNew();
                        this.IsDirty = true;
                    }
                    FillBarIDs(true);

                    if (this.AllowManualInputLeadAuthor)
                        AssignPenToNewAuthorIfNecessary();
                }

                //raise event
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());
            }
        }

        /// <summary>
        /// gives the pen to the newest author if the pen is not assigned to an author
        /// </summary>
        private void AssignPenToNewAuthorIfNecessary()
        {
            LMP.Trace.WriteInfo("AssignPenToNewAuthorIfNecessary");
            DataSet oDS = (DataSet)this.grdAuthors.DataSource;
            DataRowCollection oRows = oDS.Tables["Table"].Rows;

            if (oRows.Count == 0)
                return;
            else if (oRows.Count == 1)
                //this is the only author in the list - give pen
                AssignPen(0);
            else
            {
                //check if another author has the pen
                bool bPenExists = false;

                for (int i = 0; i < oRows.Count; i++)
                {
                    if (oDS.Tables["Table"].Rows[i]["Lead"].ToString() == "-1")
                    {
                        //author has the pen
                        bPenExists = true;
                        break;
                    }
                }

                if (!bPenExists)
                {
                    //give pen to new author
                    AssignPen(oRows.Count - 1);
                    this.IsDirty = true;
                }
            }
        }

        /// <summary>
        /// displays the full name of the selected person in the text portion of the combo
        /// </summary>
        private void DisplayFullNameInCombo()
        {
            LMP.Trace.WriteInfo("DisplayFullNameInCombo");
            if (m_xSelAuthorID != "")
            {
                //a selection from the list was made - get full name
                Person oPerson = LocalPersons.GetPersonFromID(m_xSelAuthorID);

                if (oPerson == null)
                    throw new LMP.Exceptions.PersonIDException(
                        LMP.Resources.GetLangString("Error_InvalidPersonID") +
                        m_xSelAuthorID);
                else
                {
                    //display full name in both controls - only one will be showing, though
                    this.cmbAuthor.Text = oPerson.FullName;
                    m_xSelAuthorName = oPerson.FullName;
                    LMP.Trace.WriteNameValuePairs("m_xSelAuthorName", m_xSelAuthorName);
                }
            }
        }

        /// <summary>
        /// displays the full name of the selected person in the text portion of the current grid row
        /// </summary>
        private void DisplayFullNameInGrid()
        {
            LMP.Trace.WriteInfo("DisplayFullNameInGrid");
            //a selection from the list was made - get full name
            Person oPerson = LocalPersons.GetPersonFromID(this.grdAuthors.ActiveCell.Row.Cells["ID"].Text);

            if (oPerson == null)
                throw new LMP.Exceptions.PersonIDException(
                    LMP.Resources.GetLangString("Error_InvalidPersonID") +
                    m_xSelAuthorID);
            else
            {
                this.grdAuthors.ActiveCell.Row.Cells["Name"].Value = oPerson.FullName;
                LMP.Trace.WriteNameValuePairs("FullName", oPerson.FullName);
            }
        }
        /// <summary>
        /// tabs out if user is shift-tabbing out of the first cell
        /// </summary>
        private bool ShiftTabOutIfAppropriate()
        {
            bool bTabHandled = false;
            if (this.MaxAuthors > 1)
            {
                int iCurRow = this.grdAuthors.ActiveCell.Row.Index;
                int iCol = this.grdAuthors.ActiveCell.Column.Index;
                //check if in empty new row
                int iRows = ((DataSet)this.grdAuthors.DataSource).Tables["Table"].Rows.Count;
                if ((iCurRow == 0) && (iCol < 2))
                {
                    //user is in first cell of grid				
                    //notify subscribers that the tab was pressed
                    if (this.TabPressed != null)
                    {
                        this.TabPressed(this, new TabPressedEventArgs(true));
                        bTabHandled = true;
                    }
                }
            }
            else
            {
                //Tab pressed in cmbAuthor
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(true));
                bTabHandled = true;
            }
            return bTabHandled;
        }

        /// <summary>
        /// tabs out if the user is tabbing out 
        /// of the last cell and the maximum number
        /// of authors has been reached
        /// </summary>
        private bool TabOutIfAppropriate()
        {

            bool bTabHandled = false;
            if (this.MaxAuthors > 1)
            {
                int iCurRow = this.grdAuthors.ActiveCell.Row.Index;
                int iCol = this.grdAuthors.ActiveCell.Column.Index;
                //check if in empty new row
                int iRows = ((DataSet)this.grdAuthors.DataSource).Tables["Table"].Rows.Count;
                bool bInAddNewRow = (this.grdAuthors.CurrentState & Infragistics.Win.UltraWinGrid.UltraGridState.AddRow) ==
                    Infragistics.Win.UltraWinGrid.UltraGridState.AddRow;
                //If nothing has been typed into AddNew row, LastRowInGrid bit will be set
                bool bInEmptyNewRow = bInAddNewRow &&
                            ((this.grdAuthors.CurrentState & Infragistics.Win.UltraWinGrid.UltraGridState.LastRowInGrid)
                            == Infragistics.Win.UltraWinGrid.UltraGridState.LastRowInGrid);
                //In last cell and Max Authors has been reached
                bool bInLastCell = !bInEmptyNewRow && (iCurRow == this.MaxAuthors - 1) && 
                    ((iCol == 1 && (this.ShowColumns < mpAuthorColumns.BarID)) || iCol == 3);
                if (bInEmptyNewRow || bInLastCell)
                {
                    this.grdAuthors.EndUpdate(false);
                    //user is in last cell of grid				
                    //notify subscribers that the tab was pressed
                    if (this.TabPressed != null)
                    {
                        this.TabPressed(this, new TabPressedEventArgs(false));
                        bTabHandled = true;
                    }
                }
            }
            else
            {
                //Tab pressed in cmbAuthor
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(false));
                bTabHandled = true;
            }
            return bTabHandled;

        }

        /// <summary>
        /// sets the lead author to the current row of the grid
        /// </summary>
        /// <param name="iRow"></param>
        private void AssignPen(int iRow)
        {
            LMP.Trace.WriteNameValuePairs("iRow", iRow);
            DataSet oDS = (DataSet)this.grdAuthors.DataSource;
            DataRowCollection oRows = oDS.Tables["Table"].Rows;
            grdAuthors.SuspendLayout();
            //cycle through authors, removing pen
            for (int i = 0; i < oRows.Count; i++)
            {
                oRows[i]["Lead"] = "0";
                grdAuthors.Rows[i].Cells["Lead"].Value = "0";
                //grdAuthors.Rows[i].Cells["Lead"].Appearance.Image = null;
                //grdAuthors.Rows[i].Cells["Lead"].Refresh();
            }

            //give pen to specified author
            oRows[iRow]["Lead"] = "-1";
            grdAuthors.Rows[iRow].Cells["Lead"].Value = "-1";

            grdAuthors.ResumeLayout();
            this.IsDirty = true;
        }

        //sets the AddNew property to true iff 
        //number of authors is less than max authors
        private void SetAddNew()
        {
            DataSet oDS = (DataSet)this.grdAuthors.DataSource;

            if (oDS.Tables["Table"].Rows.Count >= this.MaxAuthors)
            {
                this.grdAuthors.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            }
            else
                this.grdAuthors.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
        }

        /// <summary>
        /// fills the bar id dropdown with the appropriate bar ids
        /// </summary>
        private void FillBarIDs(bool bSetValue)
        {
            LMP.Trace.WriteInfo("FillBarIDs");
            DateTime t0 = DateTime.Now;
            string xID = this.grdAuthors.ActiveCell.Row.Cells["ID"].Text;

            //create data table in data set
            DataSet oDisplayDS = new DataSet();
            DataTable oDT = new DataTable("Table");
            oDisplayDS.Tables.Add(oDT);
            oDT.Columns.Add("Description");
            oDT.Columns.Add("BarID");
            oDT.Columns.Add("ID");

            if (xID != "" && !xID.StartsWith("-"))
            {
                //cycle through all licenses for the specified person,
                //adding each to the newly created data table
                Person oPerson = LMP.Data.LocalPersons.GetPersonFromID(xID);
                AttorneyLicenses oLics = oPerson.GetLicensesForLevel(m_iL0, m_iL1, m_iL2, m_iL3, m_iL4);
                DataSet oDS = oLics.ToDataSet();
                foreach (DataRow oRow in oDS.Tables["Table"].Rows)
                {
                    //Add Description, License and ID to table
                    oDT.Rows.Add(new object[] { oRow.ItemArray[2], oRow.ItemArray[3], oRow.ItemArray[0] });
                }
                //If there's a single matching license, prefill grid with that number
                if (bSetValue)
                {
                    string xBarID = "";
                    try
                    {
                        xBarID = oLics.Default.License;
                    }
                    catch { }
                    grdAuthors.ActiveCell.Row.Cells["BarID"].Value = xBarID;
                }
            }
            //set new dataset as datasource of dropdown
            this.ddBarIDs.DataSource = oDisplayDS;
            this.ddBarIDs.DataMember = "Table";
            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// Set Context Menu text from resource strings
        /// </summary>
        private void SetupMenus()
        {
            this.mnuContext_Delete.Text = LMP.Resources.GetLangString("Menu_AuthorSelector_Delete");
            this.mnuContext_DeleteAll.Text = LMP.Resources.GetLangString("Menu_AuthorSelector_DeleteAll");
            this.mnuContext_MoveUp.Text = LMP.Resources.GetLangString("Menu_AuthorSelector_MoveUp");
            this.mnuContext_MoveDown.Text = LMP.Resources.GetLangString("Menu_AuthorSelector_MoveDown");
            this.mnuContext_ManagePeople.Text = LMP.Resources.GetLangString("Menu_AuthorSelector_ManagePeople");
            // GLOG : 3135 : JAB
            // Provide the text for the update authors menu item.
            this.mnuContext_UpdateAuthors.Text = LMP.Resources.GetLangString("MenuAuthors_Refresh");

            // GLOG : 2547 : JAB
            // Provide the text for the edit author preferences menu item.
            this.mnuContext_EditAuthorPreferences.Text = LMP.Resources.GetLangString("Menu_AuthorSelector_EditAuthorPreferences");
        }
        /// <summary>
        /// True if specified ID is in current list of available authors
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        private bool PersonIDIsInList(string xID)
        {
            for (int i = 0; i < m_oPeopleDataSet.Tables["Table"].Rows.Count; i++)
            {
                if (m_oPeopleDataSet.Tables["Table"].Rows[i]["ID"].ToString() == xID)
                    return true;
            }
            return false;
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// Control is being resized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AuthorSelector_Resize(object sender, System.EventArgs e)
		{
			try
			{
				if(m_bInitialized & !this.DesignMode)
					ResizeControls();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Stop timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void m_oSetSelectedAuthor_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				//stop timer - timer will be started with next change -
				//either from the TextChanged event or ItemChanged event
				m_oSetSelectedAuthor.Stop();
				UpdateAuthorsForSelection();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Selected row in combo has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbAuthor_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                UpdateAuthorsForSelection();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Selected item or text in combo has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbAuthor_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
                UpdateAuthorsForSelection();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		/// <summary>
		/// Key has been pressed in Combo
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void cmbAuthor_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			try
			{
				m_oSetSelectedAuthor.Stop();		
				m_oSetSelectedAuthor.Start();

                if (e.KeyCode == Keys.Tab && e.Shift == false)
                {
                    if (TabOutIfAppropriate())
                    {
                        e.SuppressKeyPress = true;
                    }
                }
                else if (e.KeyCode == Keys.Tab && e.Shift == true)
                {
                    if (ShiftTabOutIfAppropriate())
                        e.SuppressKeyPress = true;
                }
                if (this.KeyPressed != null)
                    this.KeyPressed(this, e);
                base.OnKeyDown(e);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		/// <summary>
		/// Combo has lost focus
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void cmbAuthor_Leave(object sender, System.EventArgs e)
		{
			try
			{
				//DisplayFullNameInCombo();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		/// <summary>
		/// Combo control has been opened
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void cmbAuthor_BeforeDropDown(object sender, CancelEventArgs e)
        {
            LMP.Trace.WriteInfo("cmbAuthor_Open");
            try
            {
                //Disable timer while list is displayed
                m_oSetSelectedAuthor.Stop();
                if (!m_bListLoaded)
                    this.LoadList();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
		/// <summary>
		/// Grid Dropdown list is opening
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void ddAuthors_BeforeDropDown(object sender, CancelEventArgs e)
		{
            LMP.Trace.WriteInfo("ddAuthors_DropDownOpen");
            try
            {
                //resize dropdown width
                if (!grdAuthors.DisplayLayout.Bands[0].Columns["BarID"].Hidden)
                {
                    // dropdowns can't overlap, so minimize BarID width if necessary
                    ddBarIDs.Width = 10;
                }
                //ddAuthors.Left = grdAuthors.Left;

                ddAuthors.Width = grdAuthors.DisplayLayout.Bands[0].Columns["Name"].Width;

                //if list is empty, the display name column won't exist
                try
                {
                    ddAuthors.DisplayLayout.Bands[0].Columns["DisplayName"].Width =
                        ddAuthors.Width;
                }
                catch { }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
		}
		/// <summary>
		/// Contents of grid cell have changed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void grdAuthors_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            LMP.Trace.WriteInfo("grdAuthors_AfterCellUpdate");
            try
            {
                if (e.Cell.Column.Index == 1 && !m_bIgnoreCellChange)
                {
                    m_bIgnoreCellChange = true;
                    UpdateAuthorsForSelection();
                    m_bIgnoreCellChange = false;
                }
                else
                    this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }		

        }
        /// <summary>
        /// Contents of Grid cell are about to change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdAuthors_BeforeCellUpdate(object sender, Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventArgs e)
        {
            LMP.Trace.WriteInfo("grdAuthors_BeforeCellUpdate");
            try
            {
                //GLOG - 3620 - ceh
                if ((e.Cell.Column.Index == 1) && (!m_bIsResetEmtpy))
                {
                    string xName = e.Cell.Text;

                    DataSet oDS = (DataSet)this.grdAuthors.DataSource;
                    if (xName == "" && oDS.Tables["Table"].Rows.Count == 1)
                    {
                        //user deleted the name of the only author - alert and prevent
                        e.Cancel = true;
                        MessageBox.Show(
                            LMP.Resources.GetLangString("Message_AuthorRequired"),
                            LMP.String.MacPacProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else if (!this.AllowManualInput && ((this.ddAuthors.SelectedRow == null) || (xName != this.ddAuthors.SelectedRow.Cells["DisplayName"].Text)))
                    {
                        //text must match an entry in the list and doesn't
                        e.Cancel = true;
                        MessageBox.Show(
                            LMP.Resources.GetLangString("Message_ManualEntryNotAllowed"),
                            LMP.String.MacPacProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else if (xName != "" && !this.AllowManualInputLeadAuthor && (this.ddAuthors.SelectedRow == null ||
                        xName != this.ddAuthors.SelectedRow.Cells["DisplayName"].Text) &&
                        (e.Cell.Row.Cells["Lead"].Value.ToString() == "-1" ||
                        ((DataSet)this.grdAuthors.DataSource).Tables["Table"].Rows.Count == 0))
                    {
                        //this is the lead author row - text must match an entry in the list and doesn't
                        e.Cancel = true;
                        MessageBox.Show(
                            LMP.Resources.GetLangString("Message_LeadAuthorManualEntryNotAllowed"),
                            LMP.String.MacPacProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }		

        }
        private void cmbAuthor_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point oPoint = new Point(cmbAuthor.Left, e.Y);
                    this.mnuContext.Show(cmbAuthor, oPoint);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// MouseDown event for Grid control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdAuthors_MouseClick(object sender, MouseEventArgs e)
        {
            LMP.Trace.WriteInfo("grdAuthors_MouseDown");
            try
            {
                //show context menu if right click
                if (e.Button == MouseButtons.Right)
                {
                    //Point oPoint = new Point(grdAuthors.Left, e.Y);
                    //this.mnuContext.Show(grdAuthors, oPoint);
                    return;
                }
                
                DataSet oDS = (DataSet)this.grdAuthors.DataSource;
                DataRowCollection oRows = oDS.Tables["Table"].Rows;

                if (oRows.Count == 0 || this.grdAuthors.ActiveCell == null)
                    //no authors exist yet
                    return;

                int iRow = this.grdAuthors.ActiveCell.Row.Index;
                int iCol = this.grdAuthors.ActiveCell.Column.Index;

                if (iRow == -1 || iRow >= oRows.Count)
                    //user clicked on a "non-existent" row
                    return;

                // Lead Author cell was clicked
                if (iCol == 0 && oRows[iRow]["Name"] != null && oRows[iRow]["Name"].ToString() != "")
                {
                    //user clicked in pencil column of an existing author-
                    //give pencil to current author
                    if ((oRows[iRow]["ID"] != null && oRows[iRow]["ID"].ToString() != "")  ||
                        this.AllowManualInputLeadAuthor)
                    {
                        this.AssignPen(iRow);
                    }
                    else
                        //this is a manually input author - don't give pen
                        MessageBox.Show(LMP.Resources.GetLangString(
                            "Message_ManualAuthorCantBeLead"),
                            LMP.String.MacPacProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            
        }
		/// <summary>
		/// Key has been pressed in Grid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void grdAuthors_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
            LMP.Trace.WriteInfo("grdAuthors_KeyDown");
            try
			{
                if (e.KeyCode == Keys.Tab && e.Shift == false)
                {
                    if (TabOutIfAppropriate())
                        e.SuppressKeyPress = true;
                }
                else if (e.KeyCode == Keys.Tab && e.Shift == true)
                {
                    if (ShiftTabOutIfAppropriate())
                        e.SuppressKeyPress = true;
                }
                
                if (this.KeyPressed != null)
                    //raise keypressed event
                    this.KeyPressed(this, e);
                base.OnKeyDown(e);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Bar ID dropdown list in Grid is opening
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddBarIDs_BeforeDropDown(object sender, CancelEventArgs e)
        {
            LMP.Trace.WriteInfo("ddBarIDs_BeforeDropDown");
            try
            {
                FillBarIDs(false);
                //get number of bar ids
                DataSet oDS = (DataSet)this.ddBarIDs.DataSource;
                int iRows = oDS.Tables["Table"].Rows.Count;

                if (iRows == 0)
                    //no bar ids - ensure dropdown won't show -
                    //if nothing is done, a small bar appears
                    e.Cancel = true;
                else
                {
                    //bar ids exist - ensure that 
                    //dropdown is correct width
                    //dropdowns can't overlap, so minimize width of Authors
                    this.ddAuthors.Width = 10;
                    this.ddBarIDs.Width = 180;
                    this.ddBarIDs.DisplayLayout.Bands[0].Columns["BarID"].Width = 70;
                    this.ddBarIDs.DisplayLayout.Bands[0].Columns["Description"].Width = 110;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Selected item from Authors Dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddAuthors_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                if (ddAuthors.IsDroppedDown)
                {
                    m_bIgnoreCellChange = true;
                    UpdateAuthorsForSelection();
                    m_bIgnoreCellChange = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
		/// <summary>
		/// AuthorSelector control is loading
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void AuthorSelector_Load(object sender, System.EventArgs e)
		{
			try
			{
                if (this.DesignMode)
                {
                    SetupMenus();
                    //do only in design mode - at runtime, this is
                    // handled by ExecuteFinalSetup()
                    SetupControlForMaxAuthors();
                    m_bInitialized = true;
                }
                else
                {
                    // GLOG : 3135 : JAB
                    // Provide the text for the update authors menu item.
                    this.mnuContext_UpdateAuthors.Text = LMP.Resources.GetLangString("MenuAuthors_Refresh");

                    // GLOG : 2547 : JAB
                    // Provide the text for the edit author preferences menu item.
                    this.mnuContext_EditAuthorPreferences.Text = LMP.Resources.GetLangString("Menu_AuthorSelector_EditAuthorPreferences");

                }
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.ControlEventException(
					LMP.Resources.GetLangString("Error_CouldNotInitializeAuthorSelector"), oE);
			}
		}
        private void cmbAuthor_KeyUp(object sender, KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);
        }
        private void grdAuthors_KeyUp(object sender, KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (this.KeyReleased != null)
                //raise keyreleased event
                this.KeyReleased(this, e);
        }
        private void AuthorSelector_Leave(object sender, EventArgs e)
        {
            base.OnLostFocus(e);
        }

        private void AuthorSelector_Enter(object sender, EventArgs e)
        {
            if (this.MaxAuthors > 1)
            {
                if (this.TabPressed != null)
                    //Set tab key for cell navigation when sited in Editor tree -
                    //TabPressed event will handle changing focus to next control
                    this.grdAuthors.DisplayLayout.TabNavigation = TabNavigation.NextCell;
                else
                    //Set tab key behavior to navigate controls when not in Editor tree
                    this.grdAuthors.DisplayLayout.TabNavigation = TabNavigation.NextControlOnLastCell;
            }
            base.OnGotFocus(e);
        }

        /// <summary>
        /// handles display of people manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_ManagePeople_Click(object sender, EventArgs e)
        {
            try
            {
                //if bUseCallBack, the subscriber will handle
                //refreshing all instance(s) of the author selector
                //including this one
                bool bUseCallBack = false;
                
                //request that the UserPeopleManager be displayed - 
                //AuthorSelector can't show this control because it
                //is contained in a component not referenced by mpControls
                if (this.ShowUserPeopleManagerRequested != null)
                    this.ShowUserPeopleManagerRequested((Control)this, ref bUseCallBack);

                //if no callback requested, refresh the list internally 
                //even if no changes have been made - 
                //we might in future enhance Persons to notify subscribers
                //that changes have been made to the collection or to members 
                //of the collection 
                if (!bUseCallBack)
                    RefreshPeopleList();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Focus enters Authors grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdAuthors_Enter(object sender, EventArgs e)
        {
            try
            {
                //Switch to edit mode when control gets focus
                if ((grdAuthors.ActiveCell == null) && (this.grdAuthors.Rows.Count > 0))
                    this.grdAuthors.ActiveCell = this.grdAuthors.Rows[0].Cells["Name"];

                this.grdAuthors.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Clear all authors in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_DeleteAll_Click(object sender, EventArgs e)
        {
            try
            {
                //update data set with latest grid values -
                //this is an essential part of refreshing
                this.grdAuthors.UpdateData();

                //clear data table
                DataSet oDS = (DataSet)this.grdAuthors.DataSource;
                DataTable oDT = oDS.Tables["Table"];
                oDT.Clear();

                //add one empty row
                oDT.Rows.Add(new object[] { "", "" });
                oDT.AcceptChanges();
                //refresh
                this.grdAuthors.Refresh();
                this.grdAuthors.ActiveCell = this.grdAuthors.Rows[0].Cells["Name"];
                this.grdAuthors.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Delete selected author from Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                //update data set with latest grid values -
                //this is an essential part of refreshing
                this.grdAuthors.UpdateData();

                int iRow = this.grdAuthors.ActiveRow.Index;
                DataSet oDS = (DataSet)this.grdAuthors.DataSource;
                DataTable oDT = oDS.Tables["Table"];
                DataRow oRow = oDT.Rows[iRow];

                if (oDT.Rows.Count > 1)
                {
                    bool bLeadDeleted = oRow["Lead"].ToString() == "-1";

                    //delete row
                    oRow.Delete();
                    if (bLeadDeleted)
                        oDT.Rows[0]["Lead"] = "-1";
                }
                else
                {
                    //there's only one row in the table - just clear it
                    //update data set with latest grid values -
                    //this is an essential part of refreshing
                    this.grdAuthors.UpdateData();

                    //clear data table
                    oDT.Clear();

                    //add one empty row
                    oDT.Rows.Add(new object[] { "", "" });
                }
                oDT.AcceptChanges();
                //refresh
                this.grdAuthors.Refresh();
                iRow = Math.Min(iRow, grdAuthors.Rows.Count - 1);
                this.grdAuthors.ActiveCell = grdAuthors.Rows[iRow].Cells["Name"];
                this.grdAuthors.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Move selected item up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_MoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                int iRow = this.grdAuthors.ActiveRow.Index;
                if (iRow > -1)
                    MoveItem(iRow, -1);

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Move selected item down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_MoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                int iRow = this.grdAuthors.ActiveRow.Index;
                if (iRow > -1)
                    MoveItem(iRow, 1);

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdAuthors_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                Infragistics.Win.UIElement oUIElement =
                    grdAuthors.DisplayLayout.UIElement.ElementFromPoint(new Point(e.X, e.Y));


                if (oUIElement != null)
                {

                    UltraGridRow oRow =
                    oUIElement.GetContext(typeof(UltraGridRow), true) as UltraGridRow;


                    if (oRow != null)
                    {
                        grdAuthors.ActiveRow = oRow;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Context menu is about to be displayed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_Popup(object sender, EventArgs e)
        {
            try
            {
                if (this.MaxAuthors > 1)
                {
                    //Show/Hide commands
                    int iRow = grdAuthors.ActiveRow.Index;
                    mnuContext_MoveUp.Visible = (iRow > 0);
                    mnuContext_MoveDown.Visible = (iRow < grdAuthors.Rows.Count - 1);
                    mnuContext_Sep2.Visible = (iRow > 0) || (iRow < grdAuthors.Rows.Count - 1);
                    // GLOG : 3135 : JAB
                    // Show the Manage People menu item.
                    mnuContext_ManagePeople.Visible = true;
                }
                else
                {
                    // GLOG : 3135 : JAB
                    // Hide the Manage People menu item.
                    mnuContext_ManagePeople.Visible = true;
                }

                // GLOG : 2547 : JAB
                // Show the Edit Author Preferences menu item.
                this.mnuContext_EditAuthorPreferences.Visible = true;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void MoveItem(int iCurPos, int iRelativeMove)
        {
            //update data set with latest grid values -
            //this is an essential part of refreshing
            this.grdAuthors.UpdateData();
            DataSet oDS = (DataSet)this.grdAuthors.DataSource;
            DataTable oDT = oDS.Tables["Table"];
            if (iRelativeMove == 0)
                //Nothing to do
                return;
            else if (iCurPos == 0 && iRelativeMove < 0)
                //Already at top, can't move up
                return;
            else if (iCurPos == oDT.Rows.Count - 1 && iRelativeMove > 0)
                //already at end, can't move down
                return;

            DataRow oRow = oDT.Rows[iCurPos];
            int iNewPos = iCurPos + iRelativeMove;
            if (iNewPos < 0)
                iNewPos = 0;
            DataRow oNewRow = oDT.NewRow();
            oNewRow.ItemArray = oRow.ItemArray;
            oDT.Rows.Remove(oRow);
            if (iNewPos > oDT.Rows.Count - 1)
                oDT.Rows.Add(oNewRow);
            else
                oDT.Rows.InsertAt(oNewRow, iNewPos);
            oDT.AcceptChanges();
            //refresh
            this.grdAuthors.Refresh();
            this.grdAuthors.ActiveCell = grdAuthors.Rows[iNewPos].Cells["Name"];
            this.grdAuthors.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            this.IsDirty = true;

        }

        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosting
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        /// <summary>
        /// Determine if the key pressed is recognized for input by the control
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
            LMP.Trace.WriteInfo("ExecuteFinalSetup");
            try
            {
                if (!this.DesignMode)
                {
                    m_oPeopleDataSet = null;
                    this.LoadList();
                    m_oSetSelectedAuthor.Interval = 500;
                    m_oSetSelectedAuthor.Elapsed += new System.Timers.ElapsedEventHandler(m_oSetSelectedAuthor_Elapsed);
                    m_oSetSelectedAuthor.Stop();

                    SetupControlForMaxAuthors();
                    m_bInitialized = true;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ControlEventException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeAuthorSelector"), oE);
            }
        }
        /// <summary>
        /// Get/Set formatted string
        /// </summary>
        public string Value
        {
            get { return this.GetContentString(); }
            set { this.ParseContentString(value); }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        //GLOG - 3311 - CEH
        private void grdAuthors_CellChange(object sender, CellEventArgs e)
        {
            try
            {
                this.IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Handle selection of the Update Authors context menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_UpdateAuthors_Click(object sender, EventArgs e)
        {
            try
            {
                // Fire the UpdateAuthorsRequested event.
                if (this.UpdateAuthorsRequested != null)
                {
                    this.UpdateAuthorsRequested(this);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 2547 : JAB
        /// Handle selection of the Edit Author Preferences context menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_EditAuthorPreferences_Click(object sender, EventArgs e)
        {
            try
            {
                // Fire the EditAuthorPreferencesRequested event.
                if (EditAuthorPreferencesRequested != null)
                {
                    EditAuthorPreferencesRequested(this);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 3440 : JAB
        /// Keep track of the current row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdAuthors_BeforeCellActivate(object sender, CancelableCellEventArgs e)
        {
            try
            {
                GetGridRowDropDownRow();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : JAB : 3440
        /// Determine if the selected author has already been added.
        /// </summary>
        private bool IsValidSelection()
        {
            if (this.MaxAuthors == 1)
            {
                return true;
            }

            if (this.ddAuthors.SelectedRow == null)
            {
                return false;
            }

            DataTable dtSelectedAuthors = ((DataSet)this.grdAuthors.DataSource).Tables[0];
            DataRow [] oRows = dtSelectedAuthors.Select("ID = '" + this.ddAuthors.SelectedRow.Cells["ID"].Text + "'");
            return ((oRows.Length == 0) || 
                (oRows.Length > 0 && this.grdAuthors.ActiveRow.Index == dtSelectedAuthors.Rows.IndexOf(oRows[0])));
        }

        /// <summary>
        /// GLOG : JAB : 3440
        /// Get the dropdown's row that corresponds to the current row of the authors grid.
        /// The dropdown's row is used in restoring the current row's value when a
        /// previously-selected author is selected for the current row.
        /// </summary>
        private void GetGridRowDropDownRow()
        {
            m_oCurValidGridRow = null;

            if (this.grdAuthors.ActiveRow != null)
            {
                string xID = this.grdAuthors.ActiveRow.Cells["ID"].Value as string;

                if (xID != null)
                {
                    foreach (UltraGridRow oUGRow in this.ddAuthors.Rows)
                    {
                        string xDropDownID = oUGRow.Cells["ID"].Value as string;

                        if (xDropDownID == xID)
                        {
                            m_oCurValidGridRow = oUGRow;
                            break;
                        }
                    }
                }
            }                    
        }
    }
}