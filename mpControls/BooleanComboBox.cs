using System;
using System.Collections.Generic;
using System.Text;
using LMP.Data;

namespace LMP.Controls
{
    public class BooleanComboBox:LMP.Controls.ComboBox, IControl
    {
        #region *********************fields*********************
        private string m_xTrueDisplayText = "True";
        private string m_xFalseDisplayText = "False";
        private bool m_bControlIsLoaded = false;
        #endregion
        #region *********************constructors*********************
        public BooleanComboBox()
        {
            this.LimitToList = true;
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// gets/sets the display text to substitute for "True"
        /// </summary>
        public string TrueString
        {
            get { return m_xTrueDisplayText; }
            set 
            { 
                m_xTrueDisplayText = value;
                if (m_bControlIsLoaded)
                {
                    bool bIsDirty = this.IsDirty;
                    if (!this.DesignMode)
                        this.SetList(new string[,] 
                        { { this.TrueString, "true" }, { this.FalseString, "false" } });
                    this.IsDirty = bIsDirty;
                }
            }
        }

        /// <summary>
        /// gets/sets the display text to substitute for "False"
        /// </summary>
        public string FalseString
        {
            get { return m_xFalseDisplayText; }
            set 
            { 
                m_xFalseDisplayText = value;
                if (m_bControlIsLoaded)
                {
                    bool bIsDirty = this.IsDirty;
                    if (!this.DesignMode)
                        this.SetList(new string[,] 
                        { { this.TrueString, "true" }, { this.FalseString, "false" } });
                    this.IsDirty = bIsDirty;
                }
            }
        }
        public override string Value
        {
            get
            {
                if (this.Text.ToLower() == this.TrueString.ToLower())
                    return "true";
                else
                    return "false";
            }
            set
            {
                //Load list if necessary
                if (!m_bControlIsLoaded)
                    ExecuteFinalSetup();
                base.Value = string.IsNullOrEmpty(value) ? "false" : value;
            }
        }
        #endregion
        #region *********************Events*********************
        #endregion
        #region *********************IControl members*********************
        public override void ExecuteFinalSetup()
        {
            if (!this.DesignMode)
                this.SetList(new string[,] 
                { { this.TrueString, "true" }, { this.FalseString, "false" } });
            base.ExecuteFinalSetup();
            m_bControlIsLoaded = true;
        }
        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BooleanComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Name = "BooleanComboBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

    }
}
