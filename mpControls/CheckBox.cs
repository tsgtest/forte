using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public class CheckBox: System.Windows.Forms.CheckBox, IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructors*********************
        public CheckBox()
            : base()
        {
            this.BackColor = System.Drawing.Color.White;
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }
        public string Value
        {
            get { return base.Checked.ToString(); }
            set
            {
                if (value.ToUpper() == "TRUE")
                    base.Checked = true;
                else if (value.ToUpper() == "FALSE")
                    base.Checked = false;
                else
                    //invalid value - alert
                    throw new LMP.Exceptions.ValueException(
                        LMP.Resources.GetLangString("Error_InvalidCheckBoxValue") + value);
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        protected override void OnCheckedChanged(EventArgs e)
        {
            //mark as dirty
            this.IsDirty = true;
            base.OnCheckedChanged(e);

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, e);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (this.KeyPressed != null)
                this.KeyPressed(this, e);

            base.OnKeyDown(e);
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if(this.KeyReleased != null)
                this.KeyReleased(this, e);

            base.OnKeyUp(e);
        }
        #endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CheckBox
            // 
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UseVisualStyleBackColor = false;
            this.ResumeLayout(false);

        }
    }
}
