using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Controls
{
    public partial class Spinner : UserControl, IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;

        private mpMeasurementUnits m_iDisplayUnit;
        private decimal m_decIncrementValue;
        private decimal m_decMaxValue;
        private decimal m_decMinValue;
        private string m_xSymbol;
        private bool m_bAppendSymbol;
        private bool m_bValueProgramaticallyChanged;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************enumerations*********************

        public enum TextBoxSelections
        {
            Start = 0,
            ValueEnd = 1,
            TextEnd = 2,
            SelectAll = 3
        }

        #endregion
        #region *********************constructors*********************
        public Spinner()
        {
            InitializeComponent();
        }
        #endregion
        #region  *********************properties**************************
        [DescriptionAttribute("Gets/Sets decimal places allowed")]
        public int DecimalPlaces
        {
            get { return this.numericUpDown1.DecimalPlaces; }
            set 
            {
                //also suppresses "." in underlying spinner_keydown
                this.numericUpDown1.DecimalPlaces = value; 
            }
        }
        [DescriptionAttribute("Gets/Sets appending symbol to value")]
        public bool AppendSymbol
        {
            get { return m_bAppendSymbol; }
            set
            {
                if (value == m_bAppendSymbol)
                    return;
                
                if (value == true)
                {
                    //append symbol to textbox text
                    if (this.textBox1.Text != null && this.textBox1.Text != "")
                        this.textBox1.Text = this.AddSymbol(this.textBox1.Text);
                }
                else
                {
                    //remove symbol from textbox text
                    if (this.textBox1.Text != null && this.textBox1.Text != "")
                        this.textBox1.Text = RemoveSymbol(this.textBox1.Text);
                }
                m_bAppendSymbol = value;
            }
        }
        [DescriptionAttribute("Gets/Sets display unit type")]
        public mpMeasurementUnits DisplayUnit
        {
            get { return m_iDisplayUnit; }
            set
            {
                m_iDisplayUnit = value;
                SetDisplayUnit();
            }
        }
        [DescriptionAttribute("Gets/Sets maximum value for control")]
        public decimal Maximum
        {
            get { return m_decMaxValue; }
            set
            {
                m_decMaxValue = value;
                this.numericUpDown1.Maximum = value;
            }
        }
        [DescriptionAttribute("Gets/Sets minimum value for control")]
        public decimal Minimum
        {
            get { return m_decMinValue; }
            set
            {
                m_decMinValue = value;
                this.numericUpDown1.Minimum = value;
            }
        }
        [DescriptionAttribute("Gets/Sets increment value for each click of the spinner")]
        public decimal Increment
        {
            get { return m_decIncrementValue; }
            set
            {
                if (value == m_decIncrementValue)
                    return;
                
                //if decimal places == 0 and increment is not a whole number, reset to 1
                if (((long)value != value) && this.DecimalPlaces == 0)
                    value = 1;
                
                m_decIncrementValue = value;
                this.numericUpDown1.Increment = value;

                SetDisplayUnit();
            }
        }
        [DescriptionAttribute("Gets/Sets appending symbol to value")]
        private bool ValueProgramaticallyChanged
        {
            get { return m_bValueProgramaticallyChanged; }
            set { m_bValueProgramaticallyChanged = value; }
        }
        [DescriptionAttribute("Gets/Sets whether to allow user to specify an empty value")]

        #endregion
        #region *********************protected methods**************************
        #endregion
        #region *********************methods**************************
        private void SetDisplayUnit()
        {
            //   get designated symbol
            m_xSymbol = GetSymbol(m_iDisplayUnit);
        }
        /// <summary>
        /// appends symbol if specified, handles flagging
        /// </summary>
        /// <param name="sender"></param>
        private void OnValueChanged(object sender)
        {
            //set flag for _TextChanged event handler 
            this.ValueProgramaticallyChanged = true;

            string xValue = ((NumericUpDown)sender).Value.ToString();

            //add configured symbol if AppendSymbol == true
            if (this.AppendSymbol == true)
                xValue = AddSymbol(xValue);

            this.textBox1.Text = xValue;
            //set control as dirty
            this.IsDirty = true;
            this.ValueProgramaticallyChanged = false;

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }
        #endregion
        #region *********************procedures**************************
        private string GetSymbol(mpMeasurementUnits iUnit)
        {
            switch (iUnit)
            {
                case mpMeasurementUnits.Inches:
                    return ((char)34).ToString();
                case mpMeasurementUnits.Centimeters:
                    return " cm";
                case mpMeasurementUnits.Millimeters:
                    return " mm";
                case mpMeasurementUnits.Points:
                    return " pt";
                case mpMeasurementUnits.Picas:
                    return " pi";
                default:
                    return " pt";
            }
        }
        /// <summary>
        /// appends designated units symbox to designated string
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        private string AddSymbol(string xText)
        {
            string xTemp;
            if (xText == null || xText == "")
                return xText;
            //remove any existing symbol
            xText = RemoveSymbol(xText);

            //append symbol to input string
            xTemp = xText + m_xSymbol;
            return xTemp;
        }

        /// <summary>
        /// evaluates text typed into spinner text box
        /// allows numeric plus appropriate decimal separator
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private bool ValidateInput(string xInput)
        {
            xInput = RemoveSymbol(xInput);

            //if Minimum property greater than zero, do not allow input
            //of minus sign
            if (this.Minimum > -1 && xInput.IndexOf("-") != -1)
                return false;

            //if decimal places == 0 do not allow input of decimal point
            if (this.DecimalPlaces == 0 && xInput.Contains("."))
                return false;
            
            //test for non-numeric input
            if (LMP.String.IsNumericDouble(xInput) == false)
                return false;

            //allow input of single digit if minimum > 9
            if (this.Minimum > 9)
                if (xInput.Length == this.Minimum.ToString().Length - 1)
                    return true;
            
            //allow input of decimal values
            if (this.Minimum.ToString().Contains("."))
                if (xInput.Length < this.Minimum.ToString().Length)
                    return true;
            
            //test whether proposed value is between Mininum and
            //Maximum properties of control
            
            decimal decTest = decimal.Parse(xInput, LMP.Culture.USEnglishCulture);

            if (!(decTest >= this.Minimum && decTest <= this.Maximum))
                return false;
            
            //test whether proposed value exceeds set number of decimal places
            if (this.DecimalPlaces > 0)
            {
                if (decTest != Math.Round(decTest, this.DecimalPlaces))
                    return false;
            }
            
            //value is valid
            return true;
        }
        /// <summary>
        /// removes current units symbol from designated input
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        private string RemoveSymbol(string xInput)
        {
            if (this.AppendSymbol == true)
                //remove symbol if it's there
                xInput = xInput.Replace(this.m_xSymbol, "");

            return xInput;
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
            this.SetDisplayUnit();
        }
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get 
            {
                if (this.textBox1.Text != this.numericUpDown1.Value.ToString())
                {
                    if (this.DecimalPlaces > 0)
                    {
                        string xTest = RemoveSymbol(this.textBox1.Text);
                        int iPos = xTest.IndexOf(".");
                        int iDec = 0;
                        if (iPos == -1)
                        {
                            iDec = 0;
                            if (this.DecimalPlaces > 0)
                                xTest = xTest + ".";
                        }
                        else
                            iDec = xTest.Substring(iPos + 1).Length;
                        //Pad with '0' to specified decimal places
                        int iFill = this.DecimalPlaces - iDec;
                        for (int c = 1; c <= iFill; c++)
                            xTest = xTest + "0";
                        if (xTest.IndexOf(".") == 0)
                            xTest = "0" + xTest;
                        this.textBox1.Text = xTest;
                    }
                }

                if (this.textBox1.Text != this.numericUpDown1.Value.ToString())
                    this.textBox1.Text = this.numericUpDown1.Value.ToString();

                //GLOG item #2965 - always return value in en-US, as this control always
                //returns values, and these values are often used by code
                //that runs in en-US - see calls to ChangeType(), which
                //explicitly run in en-US.  Changing those calls to run in 
                //a different culture won't work, as keyset values could have
                //been saved in a different culture, leading to all kinds of havoc.
                //instead, we standardize all keyset numeric values to English,
                //and avoid those issues.

                //TODO: we might think to add Spinner.ValueIsUSEnglish, which
                //would allow designers to configure the control to return values
                //in the current thread culture.  This could be used when the
                //spinner needs to return a literal numeric string instead of a value -
                //property would be set to False in this case.  currently, we're
                //assuming that all spinners are being used to return numeric values,
                //not literal values - df
                return this.numericUpDown1.Value.ToString(LMP.Culture.USEnglishCulture);
            }
            
            set 
            {
                // GLOG : 2916 : JAB
                // Determine if the decimal value is 0 so that the text box
                // can be filled in appropriately. The check below only checks
                // the value == "0", and would leave the textbox blank if the
                // value were "0.00".
                bool bIsDoubleParsible = true;
                double dNewValue = -1.0;

                try
                {
                    dNewValue = double.Parse(value);
                }
                catch
                {
                    bIsDoubleParsible = false;
                }

                decimal decValue = this.Minimum;

                //handle invalid values by setting value to control minimum val
                if (value == null || value == "" || value == "0" || (bIsDoubleParsible && dNewValue == 0.0))
                {
                    if (bIsDoubleParsible && dNewValue == 0.0)
                    {
                        this.textBox1.Text = value;
                    }
                    else
                    {
                        this.textBox1.Text = decValue.ToString();
                    }
                }
                else
                    try
                    {
                        decValue = Decimal.Parse(value, LMP.Culture.USEnglishCulture);
                    }
                    catch { }

                this.numericUpDown1.Value = decValue;
            }
        }   
        [DescriptionAttribute("Gets/Sets the edited flag.")]
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get {return m_bIsDirty;}
        }
        [DescriptionAttribute("Gets/Sets a second tag.")]
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// intercepts up/down keys when focus is in text box; runs
        /// methods to increment/decrement underlying spinner
        /// whose _OnValueChanged handler assigns spinner value to textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            try
            {
                switch (e.KeyValue)
                {
                    case 40:
                        numericUpDown1.DownButton();
                        //down button behaves differently than up button
                        //therefore always pass value end as selection - 
                        //this will ensure cursor is positioned after the value, 
                        //and not at the end of all the text, including the units suffix
                        PositionCursor(TextBoxSelections.ValueEnd); 
                        break;
                    case 38:
                        numericUpDown1.UpButton();
                        PositionCursor(TextBoxSelections.TextEnd); 
                        break;
                    default:
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);        
            }
        }
        private void textBox1_TabPressed(object sender, TabPressedEventArgs e)
        {
            if (this.TabPressed != null)
                this.TabPressed(this, e);
        }

        /// <summary>
        /// validates input, changes textbox value
        /// this handler deals with manually entered input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //do not execute if text has been changed by numericUpDown_Value changed handler
                if (this.ValueProgramaticallyChanged == true)
                    return;
                
                //text has been deleted - set numericUpDown.Value to 0 or
                //value of non-zero Minimum property
                if (textBox1.Text == "")
                {
                    this.numericUpDown1.Value = Math.Max(this.Minimum, 0);
                    return;
                }

                //allow input of leading negative sign
                if (this.Minimum < 0 && textBox1.Text == "-")
                    return;

                //allow input of leading decimal point
                if ((this.Minimum >= 0 && this.Minimum < 1)&& ((textBox1.Text == ".") && this.DecimalPlaces != 0))
                    return;

                //allow manual input of decimal characters
                if (this.DecimalPlaces > 0 && textBox1.Text.Contains("."))
                {
                    int iDecPos = textBox1.Text.IndexOf(".");
                    int iDecChars = RemoveSymbol(textBox1.Text).Substring(iDecPos + 1).Length;
                    //allow input of decimals up to maximum places
                    if (iDecChars < this.DecimalPlaces)
                        return;
                }
                
                //GLOG : 15848 : ceh - show custom message for incorrect format
                // GLOG : 2920 : JAB
                // Handle blank entry in text box by preventing blank text box entry from being converted to decimal.

                //allow manual input if it could be start of number within bounds
                try
                {
                    if (string.IsNullOrEmpty(RemoveSymbol(textBox1.Text)) || (textBox1.Text.IndexOf(".") == -1 && (System.Convert.ToDecimal(RemoveSymbol(textBox1.Text)) < this.Minimum) && 
                        (System.Convert.ToDecimal(RemoveSymbol(textBox1.Text)) < (this.Maximum / 10))))
                        return;
                }
                catch (System.FormatException oE)
                {
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Error_IncorrectSpinnerFormat"), this.Minimum, this.Maximum),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.numericUpDown1.Value = Math.Max(this.Minimum, 0);
                    return;
                }

                //test for valid input, set numericUpDown value
                if (this.ValidateInput(textBox1.Text) == true)
                {
                    decimal decVal = System.Convert.ToDecimal(RemoveSymbol(textBox1.Text));
                    
                    //if input is less than min, don't assign value
                    //this will allow input if min > 9
                    if (decVal >= this.Minimum)
                    {
                        this.numericUpDown1.Value = System.Convert.ToDecimal(RemoveSymbol(textBox1.Text));
                        OnValueChanged((object)this.numericUpDown1);
                    }
                    //ensure cursor is at end of input
                    PositionCursor(TextBoxSelections.ValueEnd); 
                }
                else
                {
                    //manually entered input is invalid - return display text to 
                    //existing numericUpDown value
                    //set flag so that _TextChanged does not run twice
                    
                    this.ValueProgramaticallyChanged = true;
     
                    string xValue = numericUpDown1.Value.ToString();
                    
                    //add configured symbol if AppendSymbol == true
                    if (this.AppendSymbol == true)
                        xValue = AddSymbol(xValue);
                    
                    textBox1.Text = xValue;

                    PositionCursor(TextBoxSelections.SelectAll); 

                    this.ValueProgramaticallyChanged = false;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// selects designated selection position in text box
        /// </summary>
        /// <param name="textBoxSelections"></param>
        private void PositionCursor(TextBoxSelections textBoxSelection)
        {
            switch (textBoxSelection)
            {
                case TextBoxSelections.Start:
                    textBox1.SelectionStart = 0;
                    textBox1.SelectionLength = 0;
                    break;
                case TextBoxSelections.ValueEnd:
                    if (this.AppendSymbol == true)
                    {
                        int iLength = RemoveSymbol(textBox1.Text).Length;
                        textBox1.SelectionStart = iLength;
                    }
                    else
                        textBox1.SelectionStart = textBox1.Text.Length;
                    break;
                case TextBoxSelections.TextEnd:
                        textBox1.SelectionStart = textBox1.Text.Length;
                    break;
                case TextBoxSelections.SelectAll:
                    textBox1.SelectionStart = 0;
                    textBox1.SelectionLength = textBox1.Text.Length;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// transfers spinner value to textbox for display; appends 
        /// units symbol suffix to value if necessary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                OnValueChanged(sender);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Raise LostFocus event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Spinner_Leave(object sender, EventArgs e)
        {
            // GLOG : 2920 : JAB
            // If the text box entry is empty set the value to 0 prior
            // to leaving the control.
            if (string.IsNullOrEmpty(this.RemoveSymbol(this.textBox1.Text)))
            {
                this.textBox1.Text = "0";
            }
            base.OnLostFocus(e);
        }

        // GLOG : 2920 : JAB
        // Restrict the text entry to numbers, decimal points and keys used
        // in editting the text.
        private void textBox1_KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.OemPeriod &&
                e.KeyCode != Keys.Delete &&
                !char.IsDigit((char)e.KeyValue) &&
                e.KeyCode != Keys.Decimal &&
                e.KeyCode != Keys.Back &&
                e.KeyCode != Keys.Right &&
                e.KeyCode != Keys.Left &&
                e.KeyCode != Keys.Tab)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        //GLOG : 6444 : CEH
        private void mnuContext_Copy_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.textBox1.SelectedText, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuContext_Popup(object sender, EventArgs e)
        {
            try
            {
                //enable/disable context menu items based on selection
                this.mnuContext_Copy.Enabled = (this.textBox1.SelectedText != "");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG #8492
        private void Spinner_Resize(object sender, EventArgs e)
        {
            this.numericUpDown1.Width = 18;
            this.textBox1.Width = this.Width - (this.numericUpDown1.Width + 1);
            this.numericUpDown1.Left = this.textBox1.Width + 1;
            //this.numericUpDown1.Left = this.Width - this.numericUpDown1.Width;
            //this.Refresh();
        }


        #endregion
        #region *********************protected members*********************
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }

        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
    #endregion

    }
}
