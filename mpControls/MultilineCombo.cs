using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Controls
{
    /// <summary>
    /// MultilineCombo class.  Defines a multiline combo control.  
    /// Created by Dan Fisherman 2/05.
    /// </summary>
    public class MultilineCombo : System.Windows.Forms.UserControl, IControl
    {
        #region *********************fields*********************
        private System.Windows.Forms.ContextMenu mnuContext;
        private System.Windows.Forms.MenuItem mnuContext_Clear;
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Panel pnlMLC;
        private LMP.Controls.MultilineTextBox txtMLC;
        private LMP.Controls.ListBox lstMLC;
        private Control m_oTopLevelCtl;
        private List m_oList;
        private object m_oTag2;
        private string m_xList = "";
        private string[,] m_aListArray;
        private string m_xSeparator = "1310";
        private string m_xText = "";
        private bool m_bForceCaps = false;
        private bool m_bEnterHandled = false;
        private bool m_bAllowReservedCharacterInput;
        private bool m_bIsDirty;
        private int m_iListRows = 8;
        private ToolStrip tsMain;
        private ToolStripButton tbtnShowList;
        private bool m_bShowBorder = true;
        private bool m_bHideList = false;
        private MenuItem mnuContext_Paste;
        private MenuItem menuItem1;
        private MenuItem mnuContext_Cut;
        private MenuItem mnuContext_Copy;
        private string m_xSupportingValues = "";
        private bool m_bConstructed = false;
        private int m_iCurrentHeight = 0;   //GLOG : 8532 : ceh
        #endregion
        #region *********************events***************************
        public event BeforeItemAddedHandler BeforeItemAdded;
        public new event System.EventHandler TextChanged;
        public event System.EventHandler ListVisibleChanged;
        #endregion
        #region *********************constructors*********************
        public MultilineCombo(Control ParentControl)
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
            this.txtMLC.BackColor = this.BackColor;
            this.lstMLC.BackColor = this.BackColor;
            this.txtMLC.Font = this.Font;
            this.lstMLC.Font = this.Font;
            this.txtMLC.AcceptsTab = false;
            //set border to none - we will paint
            this.pnlMLC.BorderStyle = BorderStyle.None;
            //this.pnlMLC.Anchor = pnlMLC.Anchor | AnchorStyles.Bottom;
            this.Height = 60;
            m_oTopLevelCtl = ParentControl;
            m_bHideList = true;
            m_bConstructed = true;
        }
        #endregion
        #region *********************properties*********************
        //public Control ParentControl
        //{
        //    get { return m_oTopLevelCtl; }
        //    set { m_oTopLevelCtl = value; }
        //}
        [DescriptionAttribute("Get/set the length of the current text selection.")]
        public int SelectionLength
        {
            get { return this.txtMLC.SelectionLength; }
            set { this.txtMLC.SelectionLength = value; }
        }
        [DescriptionAttribute("Get/set the starting character of the current text selection.")]
        public int SelectionStart
        {
            get { return this.txtMLC.SelectionStart; }
            set { this.txtMLC.SelectionStart = value; }
        }
        [DescriptionAttribute("Get/set the text of the current selection.")]
        public string SelectedText
        {
            get { return this.txtMLC.SelectedText; }
            set { this.txtMLC.SelectedText = value; }
        }
        [DescriptionAttribute("Get/set allow input of reserved characters.")]
        public bool AllowReservedCharacterInput
        {
            get { return m_bAllowReservedCharacterInput; }
            set
            {
                ((LMP.Controls.TextBox)this.txtMLC).AllowReservedCharacterInput = value;
                m_bAllowReservedCharacterInput = value;
            }
        }
        [DescriptionAttribute("Get/set whether to force all text to upper case.")]
        public bool ForceCaps
        {
            get { return m_bForceCaps; }
            set
            {
                m_bForceCaps = value;
                if (value)
                    this.txtMLC.CharacterCasing = CharacterCasing.Upper;
                else
                    this.txtMLC.CharacterCasing = CharacterCasing.Normal;
            }
        }
        [DescriptionAttribute("Get/set name of MacPac list to display in dropdown.")]
        public string ListName
        {
            get { return m_xList; }
            set
            {
                m_xList = value;
                //GLOG 2941 - force reevaluation of expression for next display
                m_oList = null;
            }
        }
        [DescriptionAttribute("Get/set number of rows to display in dropdown.")]
        public int ListRows
        {
            get { return m_iListRows; }
            set { m_iListRows = value; }
        }
        [DescriptionAttribute("Get/Set string array for combo data source.")]
        public string[,] ListArray
        {
            get { return m_aListArray; }
            set { m_aListArray = value; }
        }
        [DescriptionAttribute("Get/set string inserted between list selections.")]
        public string Separator
        {
            get
            {
                if (!this.DesignMode)
                {
                    //translate non-printing characters
                    switch (m_xSeparator)
                    {
                        case "1310":
                            return "\r\n";
                        case "13":
                            return "\r";
                        case "10":
                            return "\n";
                        case "9":
                            return "\t";
                        case "11":
                            return ((char)11).ToString();
                        default:
                            return m_xSeparator;
                    }
                }
                else
                    return m_xSeparator;
            }

            set { m_xSeparator = value; }
        }

        [DescriptionAttribute("Get/set the type of border for the control.")]
        //public new BorderStyle BorderStyle
        //{
        //    get { return this.pnlMLC.BorderStyle; }
        //    set { this.pnlMLC.BorderStyle = value; }
        //}
        public bool ShowBorder
        {
            get { return m_bShowBorder; }
            set { m_bShowBorder = value; }
        }
        [DescriptionAttribute("Get/set text in text portion of control.")]
        public override string Text
        {
            get
            {
                m_xText = this.txtMLC.Text;
                return m_xText;
            }
            set
            {
                m_xText = value;
                this.txtMLC.Text = value;
            }
        }

        //[DescriptionAttribute("Get/set visibility of the vertical scroll bar.")]
        //public bool VerticalScrollBar
        //{
        //    get { return this.txtMLC.ScrollBars == ScrollBars.Vertical; }
        //    set
        //    {
        //        if (value)
        //            this.txtMLC.ScrollBars = ScrollBars.Vertical;
        //        else
        //            this.txtMLC.ScrollBars = ScrollBars.None;
        //    }
        //}
        public override System.Drawing.Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
                this.txtMLC.BackColor = value;
                this.lstMLC.BackColor = value;
            }
        }

        public override Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
                this.txtMLC.Font = value;
                this.lstMLC.Font = value;
            }
        }

        #endregion
        #region *********************methods*********************
        #endregion
        #region *********************internal procedures*********************
        /// <summary>
        /// shows the list
        /// </summary>
        private void ShowList()
        {
            // Exit if no list has been defined
            if (this.ListName == "" && this.ListArray == null)
                return;

            //change anchor so that textbox and button do not resize on height change
            //this.pnlMLC.Anchor = this.pnlMLC.Anchor & ~AnchorStyles.Bottom;

            DateTime t0 = DateTime.Now;
            if (m_oList == null && this.ListName != "" && !this.DesignMode)
            {
                //get list from name
                Lists oLists = new Lists();
                m_oList = (List)oLists.ItemFromName(this.ListName);

                if (m_oList == null)
                {
                    //no list found with this name
                    throw new LMP.Exceptions.ListException(
                        LMP.Resources.GetLangString("Error_ListNotFound") + this.ListName);
                }
                //load list
                this.lstMLC.ListName = this.ListName;

                if (m_oList.ColumnCount > 1)
                {
                    this.lstMLC.DisplayMember = "Value1";
                    this.lstMLC.ValueMember = "Value2";
                }
            }
            else if (this.ListArray != null)
            {
                //load list with designated string array
                if (lstMLC.DataSource == null)
                    SetList(this.ListArray);
            }

            LMP.Benchmarks.Print(t0);

            int iTop = 0;
            int iLeft = 0;

            ////GLOG #4160 - ceh
            ////only necessary the first time to ensure the list
            ////shows completely
            //if (m_bHideList)
            //{
            //    this.HideList();
            //    m_bHideList = false;
            //}

            //remove list from usercontrol container -
            //place in top level control so that 
            //it shows completely, ie not cut off by a container
            this.Controls.Remove(this.lstMLC);

            Control oParent = this.Parent;
            Control oTopLevelCtl = null;

            while (oParent != null)
            {
                oTopLevelCtl = oParent;
                oParent = oParent.Parent;
            }

            //cycle through all container controls-
            //adding top/left to total top/left
            Control oTopCtl = this.m_oTopLevelCtl;
            Control oCtl = this.txtMLC;
            bool bDetailParent = false;

            while (oCtl != null && !oCtl.Equals(oTopCtl))
            {
                iTop += oCtl.Top;
                iLeft += oCtl.Left;
                oCtl = oCtl.Parent;
                //GLOG 15988
                if (oCtl.Name == "Detail")
                {
                    bDetailParent = true;
                }
            }

            if (oCtl != null)
            {
                //position list below textbox
                this.lstMLC.Top = iTop = iTop + this.txtMLC.Height; //GLOG 15988
                this.lstMLC.Left = iLeft - 1;

                this.lstMLC.Height = (this.ListRows + 1) * this.lstMLC.ItemHeight;
            }
            else
            {
                //position list below textbox
                this.lstMLC.Top = this.txtMLC.Height + 27;
                this.lstMLC.Left = this.txtMLC.Left + 10;

                this.lstMLC.Height = (this.ListRows + 1) * this.lstMLC.ItemHeight;
            }

            if (oTopLevelCtl.Name == "TaskPane")
                this.m_oTopLevelCtl.Controls.Add(this.lstMLC);
            else
                oTopLevelCtl.Controls.Add(this.lstMLC);

            this.lstMLC.Visible = true;
            //GLOG 15988: Changing Visible property was also
            //affecting top of list when in a Detail Grid
            this.lstMLC.Top = iTop;

            //GLOG 15988: This is necessary to ensure list
            //isn't cut off by bottom of Detail Grid
            if (bDetailParent && m_bHideList)
            {
                this.HideList();
                m_bHideList = false;
                this.ShowList();
                return;
            }
            this.lstMLC.BringToFront();
            this.lstMLC.Focus();
            if (this.lstMLC.SelectedIndex == -1)
                this.lstMLC.SelectedIndex = 0;

            //notify subscribers that list was shown
            if (this.ListVisibleChanged != null)
                this.ListVisibleChanged(this, new System.EventArgs());
        }
        /// <summary>
        /// displays the items in the specified array
        /// </summary>
        /// <param name="aListItems"></param>
        private void SetList(string[,] aListItems)
        {
            DataTable oDT = new DataTable();

            //add columns to data table
            for (int i = 0; i <= aListItems.GetUpperBound(1); i++)
                oDT.Columns.Add("Column" + i.ToString());

            //cycle through list item array, adding rows to data table
            for (int i = 0; i <= aListItems.GetUpperBound(0); i++)
            {
                DataRow oRow = oDT.Rows.Add();
                for (int j = 0; j <= aListItems.GetUpperBound(1); j++)
                    oRow[j] = aListItems[i, j];
            }

            //load list
            this.lstMLC.DataSource = oDT;

            //show display column only - hide value column
            if (oDT.Columns.Count > 1)
            {
                this.lstMLC.DisplayMember = oDT.Columns[0].Caption;
                this.lstMLC.ValueMember = oDT.Columns[1].Caption;
            }
        }
        /// <summary>
        /// hides the list
        /// </summary>
        private void HideList()
        {
            this.lstMLC.Visible = false;

            //change anchor so that textbox and button do resize with control
            //this.pnlMLC.Anchor = this.pnlMLC.Anchor | AnchorStyles.Bottom;

            //add list back to user control container
            this.m_oTopLevelCtl.Controls.Remove(this.lstMLC);
            this.Controls.Add(this.lstMLC);
            this.txtMLC.Focus();
            this.SendToBack();
            //force borders to repaint
            this.pnlMLC.Invalidate();

            //notify subscribers that list was shown
            if (this.ListVisibleChanged != null)
                this.ListVisibleChanged(this, new System.EventArgs());
        }
        /// <summary>
        /// adds the selected list item to the text portion of the control
        /// </summary>
        private void AddItem()
        //GLOG - 3098 - CEH
        {
            string xItem = "";

            if (this.txtMLC.Text != "")
                this.txtMLC.Text = this.txtMLC.Text + this.Separator;

            if (this.lstMLC.SelectedIndex > -1)
            {
                //at least one item is selected - add
                //string xItem = this.lstMLC.SelectedValue.ToString();
                ListBox.SelectedObjectCollection oCol = this.lstMLC.SelectedItems;

                for (int i = 0; i < oCol.Count; i++)
                {
                    DataRowView oRow = (DataRowView)oCol[i];
                    //GLOG - 3609 - CEH
                    xItem = xItem + oRow[1].ToString();
                    if (i < oCol.Count - 1 && oCol.Count > 1)
                    {
                        xItem = xItem + this.Separator;
                    }
                }
                //notify subscribers, if they exist
                if (this.BeforeItemAdded != null)
                {
                    BeforeItemAddedEventArgs oArgs = new BeforeItemAddedEventArgs();
                    oArgs.Item = xItem;
                    this.BeforeItemAdded(this, oArgs);
                    xItem = oArgs.Item;
                }

                //append text to textbox text
                this.txtMLC.Text = this.txtMLC.Text + xItem;
            }

            //place insertion point after newly inserted text
            this.txtMLC.SelectionStart = this.txtMLC.Text.Length;
            this.txtMLC.ScrollToCaret();
            this.txtMLC.Focus();
        }
        #endregion
        #region *********************event handlers*********************

        private void lstMLC_EnterPressed(object sender, EnterKeyPressedEventArgs e)
        {
            try
            {
                AddItem();
                this.HideList();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void lstMLC_TabPressed(object sender, TabPressedEventArgs e)
        {
            try
            {
                this.HideList();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstMLC_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Escape:
                        this.HideList();
                        e.Handled = true;
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// processes enter key and tab keys when lstKeyDown handler does not fire
        /// this happens in 2003 if focus has moved from textbox to listbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstMLC_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            try
            {
                if (m_bEnterHandled)
                {
                    m_bEnterHandled = false;
                    return;
                }

                if (e.KeyCode == Keys.Enter)
                {
                    AddItem();
                    this.HideList();
                    m_bEnterHandled = true;
                }
                if (e.KeyCode == Keys.Tab)
                {
                    if (this.lstMLC.Focused || this.lstMLC.Visible)
                        this.HideList();
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void lstMLC_Leave(object sender, EventArgs e)
        {
            if (!this.txtMLC.Focused && !this.lstMLC.Focused && !this.tsMain.Focused)
                HideList();
        }

        private void mnuContext_Clear_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.txtMLC.Text = "";
                this.txtMLC.Focus();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6318 : CEH
        private void mnuContext_Paste_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.txtMLC.SelectedText = Clipboard.GetText();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6297 : CEH
        private void mnuContext_Copy_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.txtMLC.SelectedText, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuContext_Cut_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.txtMLC.SelectedText, true);
                //cut
                this.txtMLC.SelectedText = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuContext_Popup(object sender, EventArgs e)
        {
            try
            {
                //GLOG : 6444 : CEH
                //GLOG : 6297 : CEH
                //enable/disable copy & cut context menu items
                mnuContext_Copy.Enabled = (this.txtMLC.SelectedText != "");
                mnuContext_Cut.Enabled = (this.txtMLC.SelectedText != "");
                mnuContext_Clear.Enabled = (this.txtMLC.Text != "");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }


        private void txtMLC_TextChanged(object sender, System.EventArgs e)
        {
            try
            {
                this.IsDirty = true;

                if (this.TextChanged != null)
                    this.TextChanged(this.txtMLC, e);

                //notify that value has changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void MultilineCombo_Leave(object sender, System.EventArgs e)
        {
            try
            {
                if (!this.lstMLC.Focused && this.lstMLC.Visible)
                    this.HideList();
                base.OnLostFocus(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void MultilineCombo_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down && e.Alt == true)
                this.ShowList();
        }
        //adjust button and textbox height - these are not anchored on the bottom
        //because listbox is removed from container when shown
        private void MultilineCombo_Resize(object sender, EventArgs e)
        {
            //GLOG 15988: No longer necessary because textbox is now anchored to Panel container
            //this.txtMLC.Height = this.Height - 4;
            //this.tsMain.Height = this.txtMLC.Height + 2;
            //GLOG : 8532 : ceh
            //if (m_bConstructed && (m_iCurrentHeight != this.Height))
            //{
            //    m_iCurrentHeight = (int)(this.Height * LMP.OS.GetScalingFactor());
            //    this.Height = m_iCurrentHeight;
            //    this.txtMLC.Height = this.Height - 4;
            //}
        }
        private void txtMLC_TabPressed(object sender, TabPressedEventArgs e)
        {
            if (this.TabPressed != null)
                this.TabPressed(this, e);
        }
        private void txtMLC_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.KeyPressed != null)
                this.KeyPressed(this, e);

            base.OnKeyDown(e);
        }
        private void txtMLC_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);
        }
        
        private void pnlMLC_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Rectangle rect = new Rectangle(e.ClipRectangle.X,
                                                e.ClipRectangle.Y,
                                                e.ClipRectangle.Width - 1,
                                                e.ClipRectangle.Height - 1);

                if (m_bShowBorder)
                {
                    e.Graphics.DrawRectangle(Pens.Gray, rect);
                }
                else
                {
                    e.Graphics.DrawRectangle(Pens.Transparent, rect);
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void tbtnShowList_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode)
                {
                    if (this.lstMLC.Visible)
                        HideList();
                    else
                        ShowList();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void lstMLC_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                AddItem();
                this.HideList();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void MultilineCombo_VisibleChanged(object sender, EventArgs e)
        {
            //GLOG 6186: Make sure Dropdown is removed if Control is no longer visible
            try
            {
                if (!this.Visible && lstMLC.Visible)
                    HideList();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }


        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event EnterKeyPressedHandler EnterKeyPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
        }
        public string Value
        {
            get
            {
                return this.txtMLC.Value;
            }
            set
            {
                this.txtMLC.Value = value;
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Return)
                // Need this so that Enter will generate WM_KEYDOWN for EnterKeyPressed function
                return true;
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            int iQualifierKeys;
            bool bEnterKeyPressed = LMP.OS.EnterKeyPressed(m, out iQualifierKeys);
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (bEnterKeyPressed)
            {
                if (this.EnterKeyPressed != null)
                    this.EnterKeyPressed(this, new EnterKeyPressedEventArgs(iQualifierKeys));
                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            this.txtMLC.TabPressed += new TabPressedHandler(txtMLC_TabPressed);
        }
        public override bool Focused
        {
            get
            {
                //return true if any subcontrol has focus
                return this.txtMLC.Focused || this.lstMLC.Focused || this.tsMain.Focused;
            }
        }
        #endregion
        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultilineCombo));
            this.pnlMLC = new System.Windows.Forms.Panel();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tbtnShowList = new System.Windows.Forms.ToolStripButton();
            this.lstMLC = new LMP.Controls.ListBox();
            this.txtMLC = new LMP.Controls.MultilineTextBox();
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.mnuContext_Clear = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuContext_Cut = new System.Windows.Forms.MenuItem();
            this.mnuContext_Copy = new System.Windows.Forms.MenuItem();
            this.mnuContext_Paste = new System.Windows.Forms.MenuItem();
            this.pnlMLC.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMLC
            // 
            this.pnlMLC.AutoSize = true;
            this.pnlMLC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlMLC.Controls.Add(this.tsMain);
            this.pnlMLC.Controls.Add(this.lstMLC);
            this.pnlMLC.Controls.Add(this.txtMLC);
            this.pnlMLC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMLC.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.pnlMLC.Location = new System.Drawing.Point(0, 0);
            this.pnlMLC.Name = "pnlMLC";
            this.pnlMLC.Size = new System.Drawing.Size(311, 150);
            this.pnlMLC.TabIndex = 5;
            this.pnlMLC.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMLC_Paint);
            // 
            // tsMain
            // 
            this.tsMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsMain.AutoSize = false;
            this.tsMain.CanOverflow = false;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnShowList});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(287, 1);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(22, 148);
            this.tsMain.Stretch = true;
            this.tsMain.TabIndex = 8;
            // 
            // tbtnShowList
            // 
            this.tbtnShowList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnShowList.Image = ((System.Drawing.Image)(resources.GetObject("tbtnShowList.Image")));
            this.tbtnShowList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnShowList.Name = "tbtnShowList";
            this.tbtnShowList.Size = new System.Drawing.Size(20, 20);
            this.tbtnShowList.Text = "Show List";
            this.tbtnShowList.Click += new System.EventHandler(this.tbtnShowList_Click);
            // 
            // lstMLC
            // 
            this.lstMLC.AllowEmptyValue = false;
            this.lstMLC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lstMLC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstMLC.FormattingEnabled = true;
            this.lstMLC.IsDirty = false;
            this.lstMLC.ItemHeight = 15;
            this.lstMLC.ListName = "";
            this.lstMLC.Location = new System.Drawing.Point(0, 18);
            this.lstMLC.Name = "lstMLC";
            this.lstMLC.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstMLC.Size = new System.Drawing.Size(311, 32);
            this.lstMLC.SupportingValues = "";
            this.lstMLC.TabIndex = 6;
            this.lstMLC.Tag2 = null;
            this.lstMLC.Value = "";
            this.lstMLC.Visible = false;
            this.lstMLC.TabPressed += new LMP.Controls.TabPressedHandler(this.lstMLC_TabPressed);
            this.lstMLC.EnterPressed += new LMP.Controls.EnterKeyPressedHandler(this.lstMLC_EnterPressed);
            this.lstMLC.DoubleClick += new System.EventHandler(this.lstMLC_DoubleClick);
            this.lstMLC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstMLC_KeyDown);
            this.lstMLC.Leave += new System.EventHandler(this.lstMLC_Leave);
            this.lstMLC.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.lstMLC_PreviewKeyDown);
            // 
            // txtMLC
            // 
            this.txtMLC.AcceptsReturn = true;
            this.txtMLC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMLC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMLC.ContextMenu = this.mnuContext;
            this.txtMLC.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtMLC.IsDirty = true;
            this.txtMLC.Location = new System.Drawing.Point(1, 1);
            this.txtMLC.Multiline = true;
            this.txtMLC.Name = "txtMLC";
            this.txtMLC.NumberOfLines = 2;
            this.txtMLC.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtMLC.Size = new System.Drawing.Size(283, 148);
            this.txtMLC.SupportingValues = "";
            this.txtMLC.TabIndex = 5;
            this.txtMLC.Tag2 = null;
            this.txtMLC.Text = " ";
            this.txtMLC.Value = " ";
            this.txtMLC.TextChanged += new System.EventHandler(this.txtMLC_TextChanged);
            this.txtMLC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMLC_KeyDown);
            this.txtMLC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMLC_KeyUp);
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContext_Clear,
            this.menuItem1,
            this.mnuContext_Cut,
            this.mnuContext_Copy,
            this.mnuContext_Paste});
            this.mnuContext.Popup += new System.EventHandler(this.mnuContext_Popup);
            // 
            // mnuContext_Clear
            // 
            this.mnuContext_Clear.Index = 0;
            this.mnuContext_Clear.Text = "&Clear";
            this.mnuContext_Clear.Click += new System.EventHandler(this.mnuContext_Clear_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 1;
            this.menuItem1.Text = "-";
            // 
            // mnuContext_Cut
            // 
            this.mnuContext_Cut.Index = 2;
            this.mnuContext_Cut.Text = "Cut";
            this.mnuContext_Cut.Click += new System.EventHandler(this.mnuContext_Cut_Click);
            // 
            // mnuContext_Copy
            // 
            this.mnuContext_Copy.Index = 3;
            this.mnuContext_Copy.Text = "Copy";
            this.mnuContext_Copy.Click += new System.EventHandler(this.mnuContext_Copy_Click);
            // 
            // mnuContext_Paste
            // 
            this.mnuContext_Paste.Index = 4;
            this.mnuContext_Paste.Text = "&Paste";
            this.mnuContext_Paste.Click += new System.EventHandler(this.mnuContext_Paste_Click);
            // 
            // MultilineCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.pnlMLC);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MultilineCombo";
            this.Size = new System.Drawing.Size(311, 150);
            this.VisibleChanged += new System.EventHandler(this.MultilineCombo_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MultilineCombo_KeyDown);
            this.Leave += new System.EventHandler(this.MultilineCombo_Leave);
            this.Resize += new System.EventHandler(this.MultilineCombo_Resize);
            this.pnlMLC.ResumeLayout(false);
            this.pnlMLC.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

    }
    public delegate void BeforeItemAddedHandler(object sender, BeforeItemAddedEventArgs e);
    public class BeforeItemAddedEventArgs : System.EventArgs
    {
        public string Item;
    }
}
