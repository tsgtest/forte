namespace LMP.Controls
{
    partial class SalutationCombo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraComboEditor1
            // 
            this.ultraComboEditor1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraComboEditor1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraComboEditor1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button;
            this.ultraComboEditor1.Location = new System.Drawing.Point(2, 2);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(192, 20);
            this.ultraComboEditor1.TabIndex = 1;
            this.ultraComboEditor1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.ultraComboEditor1.SelectionChanged += new System.EventHandler(this.ultraComboEditor1_SelectionChanged);
            this.ultraComboEditor1.AfterDropDown += new System.EventHandler(this.ultraComboEditor1_AfterDropDown);
            this.ultraComboEditor1.AfterEnterEditMode += new System.EventHandler(this.ultraComboEditor1_AfterEnterEditMode);
            this.ultraComboEditor1.TextChanged += new System.EventHandler(this.ultraComboEditor1_TextChanged);
            this.ultraComboEditor1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ultraComboEditor1_KeyDown);
            this.ultraComboEditor1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ultraComboEditor1_KeyUp);
            // 
            // SalutationCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ultraComboEditor1);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "SalutationCombo";
            this.Size = new System.Drawing.Size(196, 25);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SalutationCombo_Paint);
            this.Enter += new System.EventHandler(this.SalutationCombo_Enter);
            this.Leave += new System.EventHandler(this.SalutationCombo_Leave);
            this.Resize += new System.EventHandler(this.SalutationCombo_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;

    }
}
