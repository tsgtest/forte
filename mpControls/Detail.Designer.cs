using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LMP.Controls;

namespace LMP.Controls
{
    public partial class Detail : System.Windows.Forms.UserControl, IControl
    {
        private System.Windows.Forms.Panel pnlFields;
        private System.Windows.Forms.TextBox txtEndDetail;
        private System.Windows.Forms.TextBox txtStartDetail;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.ContextMenu mnuPopup;
        private System.Windows.Forms.MenuItem mnuContextMenu_ClearField;
        private System.Windows.Forms.MenuItem menuItem8;
        private System.Windows.Forms.MenuItem mnuContextMenu_AddDetail;
        private System.Windows.Forms.MenuItem mnuContextMenu_DeleteDetail;
        private System.Windows.Forms.MenuItem mnuContextMenu_DeleteAll;
        private System.Windows.Forms.MenuItem mnuContextMenu_Paste;
        private System.Windows.Forms.MenuItem mnuContextMenu_RefreshDetail;
        private System.Windows.Forms.MenuItem mnuContextMenu_RefreshAll;
        private ToolStrip tsActions;
        private ToolStripButton tbtnPrev;
        private ToolStripButton tbtnNext;
        private ToolStripSeparator tSep1;
        private ToolStripSeparator tSep2;
        private ToolStripSeparator tSep3;
        private ToolStripButton tbtnAdd;
        private ToolStripSeparator tSep4;
        private ToolStripButton tbtnDelete;
        private ToolStripSeparator tSep5;
        private ToolStripButton tbtnContacts;
        private ToolStripDropDownButton tddbStatus;

        #region Component Designer generated code
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Detail));
            this.pnlFields = new System.Windows.Forms.Panel();
            this.txtEndDetail = new System.Windows.Forms.TextBox();
            this.txtStartDetail = new System.Windows.Forms.TextBox();
            this.mnuPopup = new System.Windows.Forms.ContextMenu();
            this.mnuContextMenu_ClearField = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_AddDetail = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_DeleteDetail = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_DeleteAll = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_Cut = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_Copy = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_Paste = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_RefreshDetail = new System.Windows.Forms.MenuItem();
            this.mnuContextMenu_RefreshAll = new System.Windows.Forms.MenuItem();
            this.tsActions = new System.Windows.Forms.ToolStrip();
            this.tbtnPrev = new System.Windows.Forms.ToolStripButton();
            this.tSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNext = new System.Windows.Forms.ToolStripButton();
            this.tSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.tddbStatus = new System.Windows.Forms.ToolStripDropDownButton();
            this.tbtnContacts = new System.Windows.Forms.ToolStripButton();
            this.tSep5 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.tSep4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tSep3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnMoveDown = new System.Windows.Forms.ToolStripButton();
            this.tSep7 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnMoveUp = new System.Windows.Forms.ToolStripButton();
            this.tSep6 = new System.Windows.Forms.ToolStripSeparator();
            this.pnlFields.SuspendLayout();
            this.tsActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFields
            // 
            this.pnlFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFields.BackColor = System.Drawing.SystemColors.Window;
            this.pnlFields.Controls.Add(this.txtEndDetail);
            this.pnlFields.Controls.Add(this.txtStartDetail);
            this.pnlFields.Location = new System.Drawing.Point(0, 0);
            this.pnlFields.Name = "pnlFields";
            this.pnlFields.Size = new System.Drawing.Size(386, 127);
            this.pnlFields.TabIndex = 3;
            this.pnlFields.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlFields_Paint);
            this.pnlFields.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlFields_MouseDown);
            // 
            // txtEndDetail
            // 
            this.txtEndDetail.Location = new System.Drawing.Point(0, 0);
            this.txtEndDetail.Name = "txtEndDetail";
            this.txtEndDetail.Size = new System.Drawing.Size(0, 22);
            this.txtEndDetail.TabIndex = 1;
            this.txtEndDetail.Enter += new System.EventHandler(this.txtEndDetail_Enter);
            // 
            // txtStartDetail
            // 
            this.txtStartDetail.Location = new System.Drawing.Point(0, 0);
            this.txtStartDetail.Name = "txtStartDetail";
            this.txtStartDetail.Size = new System.Drawing.Size(0, 22);
            this.txtStartDetail.TabIndex = 0;
            this.txtStartDetail.Enter += new System.EventHandler(this.txtStartDetail_Enter);
            // 
            // mnuPopup
            // 
            this.mnuPopup.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContextMenu_ClearField,
            this.menuItem8,
            this.mnuContextMenu_AddDetail,
            this.mnuContextMenu_DeleteDetail,
            this.mnuContextMenu_DeleteAll,
            this.menuItem3,
            this.mnuContextMenu_Cut,
            this.mnuContextMenu_Copy,
            this.mnuContextMenu_Paste,
            this.menuItem4,
            this.mnuContextMenu_RefreshDetail,
            this.mnuContextMenu_RefreshAll});
            this.mnuPopup.Popup += new System.EventHandler(this.mnuPopup_Popup);
            // 
            // mnuContextMenu_ClearField
            // 
            this.mnuContextMenu_ClearField.Index = 0;
            this.mnuContextMenu_ClearField.Text = "[Clear Field]";
            this.mnuContextMenu_ClearField.Click += new System.EventHandler(this.mnuContextMenu_ClearField_Click);
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 1;
            this.menuItem8.Text = "-";
            // 
            // mnuContextMenu_AddDetail
            // 
            this.mnuContextMenu_AddDetail.Index = 2;
            this.mnuContextMenu_AddDetail.Text = "[Add Item]";
            this.mnuContextMenu_AddDetail.Click += new System.EventHandler(this.mnuContextMenu_AddDetail_Click);
            // 
            // mnuContextMenu_DeleteDetail
            // 
            this.mnuContextMenu_DeleteDetail.Index = 3;
            this.mnuContextMenu_DeleteDetail.Text = "[Delete Item]";
            this.mnuContextMenu_DeleteDetail.Click += new System.EventHandler(this.mnuContextMenu_DeleteDetail_Click);
            // 
            // mnuContextMenu_DeleteAll
            // 
            this.mnuContextMenu_DeleteAll.Index = 4;
            this.mnuContextMenu_DeleteAll.Text = "[Delete All]";
            this.mnuContextMenu_DeleteAll.Click += new System.EventHandler(this.mnuContextMenu_DeleteAll_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 5;
            this.menuItem3.Text = "-";
            // 
            // mnuContextMenu_Cut
            // 
            this.mnuContextMenu_Cut.Index = 6;
            this.mnuContextMenu_Cut.Text = "[Cut]";
            this.mnuContextMenu_Cut.Click += new System.EventHandler(this.mnuContextMenu_Cut_Click);
            // 
            // mnuContextMenu_Copy
            // 
            this.mnuContextMenu_Copy.Index = 7;
            this.mnuContextMenu_Copy.Text = "[Copy]";
            this.mnuContextMenu_Copy.Click += new System.EventHandler(this.mnuContextMenu_Copy_Click);
            // 
            // mnuContextMenu_Paste
            // 
            this.mnuContextMenu_Paste.Index = 8;
            this.mnuContextMenu_Paste.Text = "[Paste]";
            this.mnuContextMenu_Paste.Click += new System.EventHandler(this.mnuContextMenu_Paste_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 9;
            this.menuItem4.Text = "-";
            // 
            // mnuContextMenu_RefreshDetail
            // 
            this.mnuContextMenu_RefreshDetail.Index = 10;
            this.mnuContextMenu_RefreshDetail.Text = "[Refresh Item]";
            this.mnuContextMenu_RefreshDetail.Click += new System.EventHandler(this.mnuContextMenu_RefreshDetail_Click);
            // 
            // mnuContextMenu_RefreshAll
            // 
            this.mnuContextMenu_RefreshAll.Index = 11;
            this.mnuContextMenu_RefreshAll.Text = "[Refresh All]";
            this.mnuContextMenu_RefreshAll.Click += new System.EventHandler(this.mnuContextMenu_RefreshAll_Click);
            // 
            // tsActions
            // 
            this.tsActions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsActions.AutoSize = false;
            this.tsActions.Dock = System.Windows.Forms.DockStyle.None;
            this.tsActions.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsActions.GripMargin = new System.Windows.Forms.Padding(0);
            this.tsActions.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnPrev,
            this.tSep1,
            this.tbtnNext,
            this.tSep2,
            this.tddbStatus,
            this.tbtnContacts,
            this.tSep5,
            this.tbtnDelete,
            this.tSep4,
            this.tbtnAdd,
            this.tSep3,
            this.tbtnMoveDown,
            this.tSep7,
            this.tbtnMoveUp,
            this.tSep6});
            this.tsActions.Location = new System.Drawing.Point(0, 127);
            this.tsActions.Name = "tsActions";
            this.tsActions.Size = new System.Drawing.Size(386, 22);
            this.tsActions.TabIndex = 105;
            this.tsActions.Text = "toolStrip1";
            // 
            // tbtnPrev
            // 
            this.tbtnPrev.AutoSize = false;
            this.tbtnPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnPrev.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnPrev.Image = ((System.Drawing.Image)(resources.GetObject("tbtnPrev.Image")));
            this.tbtnPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnPrev.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnPrev.Name = "tbtnPrev";
            this.tbtnPrev.Size = new System.Drawing.Size(20, 22);
            this.tbtnPrev.Click += new System.EventHandler(this.tbtnPrev_Click);
            // 
            // tSep1
            // 
            this.tSep1.AutoSize = false;
            this.tSep1.Name = "tSep1";
            this.tSep1.Size = new System.Drawing.Size(3, 22);
            // 
            // tbtnNext
            // 
            this.tbtnNext.AutoSize = false;
            this.tbtnNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnNext.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnNext.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNext.Image")));
            this.tbtnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNext.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnNext.Name = "tbtnNext";
            this.tbtnNext.Size = new System.Drawing.Size(20, 22);
            this.tbtnNext.Text = ">";
            this.tbtnNext.Click += new System.EventHandler(this.tbtnNext_Click);
            // 
            // tSep2
            // 
            this.tSep2.AutoSize = false;
            this.tSep2.Name = "tSep2";
            this.tSep2.Size = new System.Drawing.Size(3, 22);
            // 
            // tddbStatus
            // 
            this.tddbStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tddbStatus.Image = ((System.Drawing.Image)(resources.GetObject("tddbStatus.Image")));
            this.tddbStatus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tddbStatus.Name = "tddbStatus";
            this.tddbStatus.ShowDropDownArrow = false;
            this.tddbStatus.Size = new System.Drawing.Size(73, 19);
            this.tddbStatus.Text = "Item ## of ##";
            this.tddbStatus.ToolTipText = "Item # of #";
            this.tddbStatus.DropDownOpening += new System.EventHandler(this.tddbStatus_DropDownOpening);
            this.tddbStatus.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tddbStatus_DropDownItemClicked);
            // 
            // tbtnContacts
            // 
            this.tbtnContacts.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnContacts.AutoSize = false;
            this.tbtnContacts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnContacts.Image = ((System.Drawing.Image)(resources.GetObject("tbtnContacts.Image")));
            this.tbtnContacts.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnContacts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnContacts.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnContacts.Name = "tbtnContacts";
            this.tbtnContacts.Size = new System.Drawing.Size(23, 19);
            this.tbtnContacts.Click += new System.EventHandler(this.tbtnContacts_Click);
            // 
            // tSep5
            // 
            this.tSep5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tSep5.AutoSize = false;
            this.tSep5.Name = "tSep5";
            this.tSep5.Size = new System.Drawing.Size(3, 22);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnDelete.AutoSize = false;
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(23, 19);
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // tSep4
            // 
            this.tSep4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tSep4.AutoSize = false;
            this.tSep4.Name = "tSep4";
            this.tSep4.Size = new System.Drawing.Size(3, 22);
            // 
            // tbtnAdd
            // 
            this.tbtnAdd.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnAdd.AutoSize = false;
            this.tbtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("tbtnAdd.Image")));
            this.tbtnAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAdd.Margin = new System.Windows.Forms.Padding(0);
            this.tbtnAdd.Name = "tbtnAdd";
            this.tbtnAdd.Size = new System.Drawing.Size(23, 19);
            this.tbtnAdd.Click += new System.EventHandler(this.tbtnAdd_Click);
            // 
            // tSep3
            // 
            this.tSep3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tSep3.AutoSize = false;
            this.tSep3.Name = "tSep3";
            this.tSep3.Size = new System.Drawing.Size(3, 22);
            // 
            // tbtnMoveDown
            // 
            this.tbtnMoveDown.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnMoveDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("tbtnMoveDown.Image")));
            this.tbtnMoveDown.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnMoveDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnMoveDown.Name = "tbtnMoveDown";
            this.tbtnMoveDown.Size = new System.Drawing.Size(23, 19);
            this.tbtnMoveDown.Text = "Move Down";
            this.tbtnMoveDown.Click += new System.EventHandler(this.tbtnMoveDown_Click);
            // 
            // tSep7
            // 
            this.tSep7.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tSep7.Name = "tSep7";
            this.tSep7.Size = new System.Drawing.Size(6, 22);
            // 
            // tbtnMoveUp
            // 
            this.tbtnMoveUp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnMoveUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtnMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("tbtnMoveUp.Image")));
            this.tbtnMoveUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnMoveUp.Name = "tbtnMoveUp";
            this.tbtnMoveUp.Size = new System.Drawing.Size(23, 19);
            this.tbtnMoveUp.Text = "Move Up";
            this.tbtnMoveUp.Click += new System.EventHandler(this.tbtnMoveUp_Click);
            // 
            // tSep6
            // 
            this.tSep6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tSep6.Name = "tSep6";
            this.tSep6.Size = new System.Drawing.Size(6, 22);
            // 
            // Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.pnlFields);
            this.Controls.Add(this.tsActions);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Detail";
            this.Size = new System.Drawing.Size(388, 149);
            this.Load += new System.EventHandler(this.Detail_Load);
            this.Enter += new System.EventHandler(this.Detail_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Detail_KeyDown);
            this.Leave += new System.EventHandler(this.Detail_Leave);
            this.Resize += new System.EventHandler(this.Detail_Resize);
            this.pnlFields.ResumeLayout(false);
            this.pnlFields.PerformLayout();
            this.tsActions.ResumeLayout(false);
            this.tsActions.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private IContainer components;
        private ToolStripButton tbtnMoveUp;
        private ToolStripButton tbtnMoveDown;
        private ToolStripSeparator tSep6;
        private ToolStripSeparator tSep7;
        private MenuItem menuItem3;
        private MenuItem mnuContextMenu_Cut;
        private MenuItem mnuContextMenu_Copy;
    }
}
