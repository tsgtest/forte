 using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using System.Collections;
using XML = System.Xml;

namespace LMP.Controls
{
    public partial class JurisdictionChooser : UserControl, IControl
    {
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        private int m_iL0;
        private int m_iL1;
        private int m_iL2;
        private int m_iL3;
        private int m_iL4;
        private bool m_bLoadingValue;
        private bool m_bHideDisabledLevels = true;
        private mpLocationsLevels m_iFirstLevel = mpLocationsLevels.Zero;
        private mpLocationsLevels m_iLastLevel = mpLocationsLevels.Four;
        private string m_xSupportingValues = "";
        private bool m_bNavigating = false;

        #endregion
        #region *********************enums*********************
        public enum mpLocationsLevels
        {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Four = 4
        }
        #endregion
        #region *********************constructors*********************
        public JurisdictionChooser()
        {
            InitializeComponent();
        }

        #region *********************methods*********************
        /// <summary>
        /// loads level 0 combo
        /// uses ToArray method of Jurisdictions class
        /// clears child combos
        /// </summary>
        internal void LoadLevel0List()
        {
            Jurisdictions oJurs = new Jurisdictions(Jurisdictions.Levels.Zero);
            if (oJurs.Count == 0)
            {
                //clear child lists and text
                Clear(0);
                EnableLists(0, false);
            }
            else
            {
                ArrayList aList = oJurs.ToArrayList();
                this.cmbLevel0.SetList(aList);
            }
        }
        /// loads level 1 combo
        /// uses ToArray method of Jurisdictions class
        /// clears child combos
        internal void LoadLevel1List()
        {
            Jurisdictions oJurs = new Jurisdictions(Jurisdictions.Levels.One, L0 != 0 ? L0 : -1);
            if (oJurs.Count == 0)
            {
                //clear child lists and text
                Clear(1);
                EnableLists(1, false);
                this.L1 = 0;
            }
            else
            {
                EnableLists(1, true);
                ArrayList aList = oJurs.ToArrayList();
                this.cmbLevel1.SetList(aList);
            }
        }
        /// <summary>
        /// loads level 0 combo
        /// uses ToArray method of Jurisdictions class
        /// clears child combos
        /// </summary>
        internal void LoadLevel2List()
        {
            Jurisdictions oJurs = new Jurisdictions(Jurisdictions.Levels.Two, L1 != 0 ? L1 : -1);
            if (oJurs.Count == 0)
            {
                //clear child lists and text
                Clear(2);
                EnableLists(2, false);
                this.L2 = 0;
            }
            else
            {
                EnableLists(2, true);
                ArrayList aList = oJurs.ToArrayList();
                this.cmbLevel2.SetList(aList);
            }
        }
        /// <summary>
        /// loads level 0 combo
        /// uses ToArray method of Jurisdictions class
        /// clears child combos
        /// </summary>
        internal void LoadLevel3List()
        {
            Jurisdictions oJurs = new Jurisdictions(Jurisdictions.Levels.Three, L2 != 0 ? L2 : -1);
            if (oJurs.Count == 0)
            {
                //clear child lists and text
                Clear(3);
                EnableLists(3, false);
                this.L3 = 0; 
            }
            else
            {
                ArrayList aList = oJurs.ToArrayList();
                EnableLists(3, true);
                aList.Insert(0, new object[] { 0, "-None-" });
                this.cmbLevel3.SetList(aList);
            }
        }
        /// <summary>
        /// loads level 0 combo
        /// uses ToArray method of Jurisdictions class
        /// clears child combos
        /// </summary>
        internal void LoadLevel4List()
        {
            Jurisdictions oJurs = new Jurisdictions(Jurisdictions.Levels.Four, L3 != 0 ? L3 : -1);
            if (oJurs.Count == 0)
            {
                //clear child lists and text
                Clear(4);
                EnableLists(4, false);
                this.L4 = 0;
            }
            else
            {
                ArrayList aList = oJurs.ToArrayList();
                EnableLists(4, true);
                aList.Insert(0, new object[] { 0, "-None-" });
                this.cmbLevel4.SetList(aList);
            }
        }
        /// <returns></returns>
        /// <summary>
        /// clears all combos below iTopLevel
        /// </summary>
        internal void Clear(int iTopLevel)
        {
            this.LimitToList = false;
            if (iTopLevel == 0)
            {
                this.cmbLevel0.Text = "";
            }
            if (iTopLevel <= 1)
            {
                this.cmbLevel1.ClearList(); 
            }
            if (iTopLevel <= 2)
            {
                this.cmbLevel2.ClearList();
            }
            if (iTopLevel <= 3)
            {
                this.cmbLevel3.ClearList();
            }
            if (iTopLevel <= 4)
            {
                this.cmbLevel4.ClearList();
            }
            this.LimitToList = true;
        }
        /// <summary>
        /// disables all combos & labels below iTopLevel
        /// </summary>
        internal void EnableLists(int iTopLevel, bool bEnabled)
        {
            this.SuspendLayout();
            try
            {
                if (iTopLevel == 0 && m_iFirstLevel <= mpLocationsLevels.Zero)
                {
                    this.cmbLevel0.Visible = bEnabled;
                    this.txtLevel0Disabled.Visible = !bEnabled;
                    this.lblLevel0.Enabled = bEnabled;
                }
                if (iTopLevel <= 1 && m_iFirstLevel <= mpLocationsLevels.One && m_iLastLevel >= mpLocationsLevels.One)
                {
                    this.cmbLevel1.Visible = bEnabled;
                    this.txtLevel1Disabled.Visible = !bEnabled;
                    this.lblLevel1.Enabled = bEnabled;
                }
                if (iTopLevel <= 2 && m_iFirstLevel <= mpLocationsLevels.Two && m_iLastLevel >= mpLocationsLevels.Two)
                {
                    this.cmbLevel2.Visible = bEnabled;
                    this.txtLevel2Disabled.Visible = !bEnabled;
                    this.lblLevel2.Enabled = bEnabled;
                }
                if (iTopLevel <= 3 && m_iFirstLevel <= mpLocationsLevels.Three && m_iLastLevel >= mpLocationsLevels.Three)
                {
                    this.cmbLevel3.Visible = bEnabled;
                    this.txtLevel3Disabled.Visible = !bEnabled;
                    this.lblLevel3.Enabled = bEnabled;
                }
                if (iTopLevel <= 4 && m_iLastLevel >= mpLocationsLevels.Four)
                {
                    this.cmbLevel4.Visible = bEnabled;
                    this.txtLevel4Disabled.Visible = !bEnabled;
                    this.lblLevel4.Enabled = bEnabled;
                }
            }
            finally
            {
                this.ResumeLayout();
            }
        }
        
        
        /// <summary>
        /// parse delimited string, set L0-L4 properties, refresh lists
        /// </summary>
        /// <param name="value"></param>
        private void SetJurisdictions(string xValue)
        {

            try
            {
                this.SuspendLayout();
                //Value is delimited string
                string[] xValues = xValue.Split(StringArray.mpEndOfRecord);
                int iLimit = xValues.GetUpperBound(0);
                L0 = iLimit > -1 ? Int32.Parse(xValues[0]) : 0;
                L1 = iLimit > 0 ? Int32.Parse(xValues[1]) : 0;
                L2 = iLimit > 1 ? Int32.Parse(xValues[2]) : 0;
                L3 = iLimit > 2 ? Int32.Parse(xValues[3]) : 0;
                L4 = iLimit > 3 ? Int32.Parse(xValues[4]) : 0;

                //do not cascade lists when setting value of control
                m_bLoadingValue = true;
                //load each combo individually when setting control value
                LoadLevel0List();
                LoadLevel1List();
                LoadLevel2List();
                LoadLevel3List();
                LoadLevel4List();
                //setting value property w/ limit to list = true will
                //set each combo's .SelectedIndex property, which will
                //in turn refresh combo text
                this.LimitToList = true;
                this.cmbLevel0.Value = L0.ToString();
                this.cmbLevel1.Value = L1.ToString();
                this.cmbLevel2.Value = L2.ToString();
                this.cmbLevel3.Value = L3.ToString();
                this.cmbLevel4.Value = L4.ToString();
                this.cmbLevel0.SelectionLength = 0;
                this.cmbLevel1.SelectionLength = 0;
                this.cmbLevel2.SelectionLength = 0;
                this.cmbLevel3.SelectionLength = 0;
                this.cmbLevel4.SelectionLength = 0;
                this.txtLevel0Disabled.Text = this.cmbLevel0.Text;
                this.txtLevel1Disabled.Text = this.cmbLevel1.Text;
                this.txtLevel2Disabled.Text = this.cmbLevel2.Text;
                this.txtLevel3Disabled.Text = this.cmbLevel3.Text;
                this.txtLevel4Disabled.Text = this.cmbLevel4.Text;

                //control is dirty
                this.IsDirty = true;
                //reset flag so list refresh will cascade if user
                //chooses a new combo item
                m_bLoadingValue = false;
            }
            finally
            {
                this.ResumeLayout();
                this.Refresh();
            }
        }

    #endregion

        #endregion
        #region *********************properties*********************
        public bool HideDisabledLevels
        {
            get { return m_bHideDisabledLevels; }
            set { m_bHideDisabledLevels = value; }
        }

        /// <summary>
        /// gets/sets first level to display
        /// </summary>
        public mpLocationsLevels FirstLevel
        {
            get { return m_iFirstLevel; }
            set
            {
                if (value > m_iLastLevel)
                    return;
                else
                    m_iFirstLevel = value;
            }
        }
        /// <summary>
        /// gets/sets last level to display
        /// </summary>
        public mpLocationsLevels LastLevel
        {
            get { return m_iLastLevel; }
            set
            {
                if (value < m_iFirstLevel)
                    return;
                else
                    m_iLastLevel = value;
            }
        }
       
        public string Level0Label
        {
            get { return lblLevel0.Text; }
            set 
            { 
                if (!string.IsNullOrEmpty(value))
                    lblLevel0.Text = value; 
            }
        }
        public string Level1Label
        {
            get { return lblLevel1.Text; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    lblLevel1.Text = value;
            }
        }
        public string Level2Label
        {
            get { return lblLevel2.Text; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    lblLevel2.Text = value;
            }
        }
        public string Level3Label
        {
            get { return lblLevel3.Text; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    lblLevel3.Text = value;
            }
        }
        public string Level4Label
        {
            get { return lblLevel4.Text; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    lblLevel4.Text = value;
            }
        }
        /// <summary>
        /// shows/hides level combos based on FirstLevel
        /// and LastLevel property settings
        /// If HideDisabledLevels=false, levels above FirstLevel
        /// will be visible but disabled
        /// </summary>
        private void SetLevelDisplay()
        {
            this.SuspendLayout();
            try
            {
                int iLevels = 0;
                int iOffset = (int)(22 * LMP.OS.GetScalingFactor()); //Spacing for level controls
                int iLabelOffset = (int)(0 * LMP.OS.GetScalingFactor());
                int iCurPos = 0; // cmbLevel0.Top;
                if (m_iFirstLevel <= mpLocationsLevels.Zero)
                {
                    this.cmbLevel0.Top = iCurPos;
                    this.txtLevel0Disabled.Top = iCurPos;
                    this.txtLabel0.Top = iCurPos;
                    this.lblLevel0.Top = this.txtLabel0.Top + 1;
                    this.lblLevel0.Height = this.txtLabel0.Height - 2;
                    this.lblLevel0.Width = this.txtLabel0.Width - 3;

                    if (m_bHideDisabledLevels)
                    {
                        this.lblLevel0.Visible = true;
                        this.cmbLevel0.Visible = true;
                        this.txtLevel0Disabled.Visible = false;
                        this.txtLabel0.Visible = true;
                    }
                    else
                    {
                        this.lblLevel0.Enabled = true;
                        this.cmbLevel0.Visible = true;
                        this.txtLevel0Disabled.Visible = false;
                    }
                    iCurPos = this.cmbLevel0.Top + this.cmbLevel0.Height;
                    iLevels++;
                }
                else
                {
                    if (m_bHideDisabledLevels)
                    {
                        this.lblLevel0.Visible = false;
                        this.cmbLevel0.Visible = false;
                        this.txtLevel0Disabled.Visible = false;
                        this.txtLabel0.Visible = false;
                    }
                    else
                    {
                        this.lblLevel0.Enabled = false;
                        this.cmbLevel0.Visible = false;
                        this.txtLevel0Disabled.Visible = true;
                        this.txtLevel0Disabled.Text = this.cmbLevel0.Text;
                        this.cmbLevel0.SelectionLength = 0;
                        iCurPos = this.cmbLevel0.Top + this.cmbLevel0.Height;
                        iLevels++;
                    }

                }
                if (m_iFirstLevel <= mpLocationsLevels.One && m_iLastLevel >= mpLocationsLevels.One)
                {
                    this.cmbLevel1.Top = iCurPos;
                    this.txtLevel1Disabled.Top = iCurPos;
                    this.txtLabel1.Top = iCurPos;
                    this.lblLevel1.Top = this.txtLabel1.Top + 1;
                    this.lblLevel1.Height = this.txtLabel1.Height - 2;
                    this.lblLevel1.Width = this.txtLabel1.Width - 3;
                    iCurPos = this.cmbLevel1.Top + this.cmbLevel1.Height;
                    if (m_bHideDisabledLevels)
                    {
                        this.lblLevel1.Visible = true;
                        this.cmbLevel1.Visible = true;
                        this.txtLevel1Disabled.Visible = false;
                        this.txtLabel1.Visible = true;
                    }
                    else
                    {
                        this.lblLevel1.Enabled = true;
                        this.cmbLevel1.Visible = true;
                        this.txtLevel1Disabled.Visible = false;
                    }
                    iLevels++;
                }
                else
                {
                    if (m_bHideDisabledLevels || m_iLastLevel < mpLocationsLevels.One)
                    {
                        this.lblLevel1.Visible = false;
                        this.cmbLevel1.Visible = false;
                        this.txtLevel1Disabled.Visible = false;
                        this.txtLabel1.Visible = false;
                    }
                    else
                    {
                        this.lblLevel1.Enabled = false;
                        this.cmbLevel1.Visible = false;
                        this.txtLevel1Disabled.Visible = true;
                        this.txtLevel1Disabled.Text = this.cmbLevel1.Text;
                        this.cmbLevel1.SelectionLength = 0;
                        iCurPos = this.cmbLevel1.Top + this.cmbLevel1.Height;
                        iLevels++;
                    }
                }
                if (m_iFirstLevel <= mpLocationsLevels.Two && m_iLastLevel >= mpLocationsLevels.Two)
                {
                    this.cmbLevel2.Top = iCurPos;
                    this.txtLevel2Disabled.Top = iCurPos;
                    this.txtLabel2.Top = iCurPos;
                    this.lblLevel2.Top = this.txtLabel2.Top + 1;
                    this.lblLevel2.Height = this.txtLabel2.Height - 2;
                    this.lblLevel2.Width = this.txtLabel2.Width - 3;
                    iCurPos = this.cmbLevel2.Top + this.cmbLevel2.Height;
                    if (m_bHideDisabledLevels)
                    {
                        this.lblLevel2.Visible = true;
                        this.cmbLevel2.Visible = true;
                        this.txtLevel2Disabled.Visible = false;
                        this.txtLabel2.Visible = true;
                    }
                    else
                    {
                        this.lblLevel2.Enabled = true;
                        this.cmbLevel2.Visible = true;
                        this.txtLevel2Disabled.Visible = false;
                    }
                    iLevels++;
                }
                else
                {
                    if (m_bHideDisabledLevels || m_iLastLevel < mpLocationsLevels.Two)
                    {
                        this.lblLevel2.Visible = false;
                        this.cmbLevel2.Visible = false;
                        this.txtLevel2Disabled.Visible = false;
                        this.txtLabel2.Visible = false;
                    }
                    else
                    {
                        this.lblLevel2.Enabled = false;
                        this.cmbLevel2.Visible = false;
                        this.txtLevel2Disabled.Visible = true;
                        this.txtLevel2Disabled.Text = this.cmbLevel2.Text;
                        this.cmbLevel2.SelectionLength = 0;
                        iCurPos = this.cmbLevel2.Top + this.cmbLevel2.Height;
                        iLevels++;
                    }
                }
                if (m_iFirstLevel <= mpLocationsLevels.Three && m_iLastLevel >= mpLocationsLevels.Three)
                {
                    this.cmbLevel3.Top = iCurPos;
                    this.txtLevel3Disabled.Top = iCurPos;
                    this.txtLabel3.Top = iCurPos;
                    this.lblLevel3.Top = this.txtLabel3.Top + 1;
                    this.lblLevel3.Height = this.txtLabel3.Height - 2;
                    this.lblLevel3.Width = this.txtLabel3.Width - 3;
                    iCurPos = this.cmbLevel3.Top + this.cmbLevel3.Height;
                    if (m_bHideDisabledLevels)
                    {
                        this.lblLevel3.Visible = true;
                        this.cmbLevel3.Visible = true;
                        this.txtLevel3Disabled.Visible = false;
                        this.txtLabel3.Visible = true;
                    }
                    else
                    {
                        this.lblLevel3.Enabled = true;
                        this.cmbLevel3.Visible = true;
                        this.txtLevel3Disabled.Visible = false;
                    }
                    iLevels++;
                }
                else
                {
                    if (m_bHideDisabledLevels || m_iLastLevel < mpLocationsLevels.Three)
                    {
                        this.lblLevel3.Visible = false;
                        this.txtLevel3Disabled.Visible = false;
                        this.cmbLevel3.Visible = false;
                        this.txtLabel3.Visible = false;
                    }
                    else
                    {
                        this.lblLevel3.Enabled = false;
                        this.cmbLevel3.Visible = false;
                        this.txtLevel3Disabled.Visible = true;
                        this.txtLevel3Disabled.Text = this.cmbLevel3.Text;
                        this.cmbLevel3.SelectionLength = 0;
                        iCurPos = this.cmbLevel3.Top + this.cmbLevel3.Height;
                        iLevels++;
                    }
                }
                if (m_iFirstLevel <= mpLocationsLevels.Four && m_iLastLevel >= mpLocationsLevels.Four)
                {
                    this.cmbLevel4.Top = iCurPos;
                    this.txtLevel4Disabled.Top = iCurPos;
                    this.txtLabel4.Top = iCurPos;
                    this.lblLevel4.Top = this.txtLabel4.Top + 1;
                    this.lblLevel4.Height = this.txtLabel4.Height - 2;
                    this.lblLevel4.Width = this.txtLabel4.Width - 3;
                    iCurPos = this.cmbLevel4.Top + this.cmbLevel4.Height;
                    if (m_bHideDisabledLevels)
                    {
                        this.lblLevel4.Visible = true;
                        this.cmbLevel4.Visible = true;
                        this.txtLevel4Disabled.Visible = false;
                        this.txtLabel4.Visible = true;
                    }
                    iLevels++;
                }
                else
                {
                    this.lblLevel4.Visible = false;
                    this.cmbLevel4.Visible = false;
                    this.txtLevel4Disabled.Visible = true;
                    this.txtLevel4Disabled.Text = this.cmbLevel4.Text;
                    this.txtLabel4.Visible = false;
                }
                //set control height based on visible controls
                this.Height = iCurPos;
            }
            finally
            {
                this.ResumeLayout();
                this.Refresh();
            }
        }
        
        /// <summary>
        /// hold L0 value
        /// </summary>
        internal int L0
        {
            get { return m_iL0; }
            set { m_iL0 = value; }
        }
        /// <summary>
        /// hold L1 value
        /// </summary>
        internal int L1
        {
            get { return m_iL1; }
            set { m_iL1 = value; }
        }
        /// <summary>
        /// hold L2 value
        /// </summary>
        internal int L2
        {
            get { return m_iL2; }
            set { m_iL2 = value; }
        }
        /// <summary>
        /// hold L3 value
        /// </summary>
        internal int L3
        {
            get { return m_iL3; }
            set { m_iL3 = value; }
        }
        /// <summary>
        /// hold L0 value
        /// </summary>
        internal int L4
        {
            get { return m_iL4; }
            set { m_iL4 = value; }
        }
        /// <summary>
        /// internal property sets limit to list prop for all combos
        /// in control
        /// </summary>
        internal bool LimitToList
        {
            set
            {
                this.cmbLevel0.LimitToList = value;
                this.cmbLevel1.LimitToList = value;
                this.cmbLevel2.LimitToList = value;
                this.cmbLevel3.LimitToList = value;
                this.cmbLevel4.LimitToList = value;
            }
        }
        #endregion
        #region *********************events*********************
        /// <summary>
        /// subscribe to underlying combo SelectedValueChanged property
        /// update L0 property, load child combo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLevel0_ValueChanged(object sender, EventArgs e)
        {
            if (m_bLoadingValue)
                return;
            try
            {
                //ensure value property returns selected value int
                this.L0 = ((this.cmbLevel0.Value != "") && (this.cmbLevel0.Value != null)
                    ? Int32.Parse(this.cmbLevel0.Value) : 0);
                this.txtLevel0Disabled.Text = this.cmbLevel0.Text;
                this.IsDirty = true;

                this.SuspendLayout();
                //notify that value has been changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());

                this.LoadLevel1List();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
        }

        /// <summary>
        /// subscribe to underlying combo SelectedValueChanged property
        /// update L0 property, load child combo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLevel1_ValueChanged(object sender, EventArgs e)
        {
            if (m_bLoadingValue)
                return;
            try
            {
                //ensure value property returns selected value int
                L1 = ((this.cmbLevel1.Value != "") && (this.cmbLevel1.Value != null)
                    ? Int32.Parse(this.cmbLevel1.Value) : 0);
                this.SuspendLayout();

                this.txtLevel1Disabled.Text = this.cmbLevel1.Text;
                this.IsDirty = true;

                //notify that value has been changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());

                this.LoadLevel2List();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
        }

        /// <summary>
        /// subscribe to underlying combo SelectedValueChanged property
        /// update L0 property, load child combo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLevel2_ValueChanged(object sender, EventArgs e)
        {
            if (m_bLoadingValue)
                return;
            try
            {
                //ensure value property returns selected value int
                L2 = ((this.cmbLevel2.Value != "") && (this.cmbLevel2.Value != null)
                    ? Int32.Parse(this.cmbLevel2.Value) : 0);
                this.SuspendLayout();
                this.txtLevel2Disabled.Text = this.cmbLevel2.Text;

                this.IsDirty = true;

                //notify that value has been changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());

                this.LoadLevel3List();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
    }

        /// <summary>
        /// subscribe to underlying combo SelectedValueChanged property
        /// update L0 property, load child combo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLevel3_ValueChanged(object sender, EventArgs e)
        {
			//GLOG - 3951 - ceh
            if (m_bLoadingValue || m_bNavigating)
                return;
            try
            {
                //ensure value property returns selected value int
                L3 = ((this.cmbLevel3.Value != "") && (this.cmbLevel3.Value != null)
                    ? Int32.Parse(this.cmbLevel3.Value) : 0);
                this.SuspendLayout();

                this.txtLevel3Disabled.Text = this.cmbLevel3.Text;
                this.IsDirty = true;

                //notify that value has been changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());

                this.LoadLevel4List();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
        }

        /// <summary>
        /// subscribe to underlying combo SelectedValueChanged property
        /// update L0 property, load child combo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLevel4_ValueChanged(object sender, EventArgs e)
        {
            if (m_bLoadingValue)
                return;
            try
            {

                //ensure value property returns selected value int
                L4 = ((this.cmbLevel4.Value != "") && (this.cmbLevel4.Value != null)
                    ? Int32.Parse(this.cmbLevel4.Value) : 0);
                this.SuspendLayout();
                this.txtLevel4Disabled.Text = this.cmbLevel4.Text;

                //notify that value has been changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());

                this.IsDirty = true;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
        }

        #endregion
        #region *********************methods*********************
        /// <summary>
        /// builds value string, called by value get{}
        /// returns delimited string of L0-L4 from level combos
        /// </summary>
        internal new string ToString()
        {
            this.LimitToList = true;
            StringBuilder sB = new StringBuilder();
            sB.Append(this.cmbLevel0.Value);
            sB.Append(StringArray.mpEndOfRecord);
            if (this.cmbLevel1.Value != null)
                sB.Append(L1.ToString());
            else
                sB.Append("0");
            sB.Append(StringArray.mpEndOfRecord);
            if (this.cmbLevel2.Value != null)
                sB.Append(L2.ToString());
            else
                sB.Append("0");
            sB.Append(StringArray.mpEndOfRecord);
            if (this.cmbLevel3.Value != null && this.cmbLevel3.Value != "" && Int32.Parse(this.cmbLevel3.Value) > 0)
                sB.Append(L3.ToString());
            else
                sB.Append("0");
            sB.Append(StringArray.mpEndOfRecord);
            if (this.cmbLevel4.Value != null && this.cmbLevel4.Value != "" && Int32.Parse(this.cmbLevel4.Value) > 0)
                sB.Append(L4.ToString());
            else
                sB.Append("0");
            return sB.ToString();
        }

        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosting
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }

        protected override void OnGotFocus(EventArgs e)
        {
            //line below causes last character of selected text to be hidden
            //this.comboBox1.SelectAll();
            base.OnGotFocus(e);
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;
        public bool m_bInitialized;

        public void ExecuteFinalSetup()
        {
            SetLevelDisplay();
            m_bInitialized = true;
        }
        /// <summary>
        /// value is set in form of delimited string
        /// returns delimited string
        /// </summary>
        public string Value
        {
            get
            {
                return ToString();
            }
            set
            {
                SetJurisdictions(value);
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion

        private void JurisdictionChooser_Enter(object sender, EventArgs e)
        {
            this.SuspendLayout();
            try
            {
                if (m_iFirstLevel != mpLocationsLevels.Zero)
                    this.cmbLevel0.SelectionLength = 0;
                if (m_iFirstLevel != mpLocationsLevels.One)
                    this.cmbLevel1.SelectionLength = 0;
                if (m_iFirstLevel != mpLocationsLevels.Two)
                    this.cmbLevel2.SelectionLength = 0;
                if (m_iFirstLevel != mpLocationsLevels.Three)
                    this.cmbLevel3.SelectionLength = 0;
                if (m_iFirstLevel != mpLocationsLevels.Four)
                    this.cmbLevel4.SelectionLength = 0;
            }
            finally
            {
                this.ResumeLayout();
            }
        }

        private void HandleKeyPressed(object sender, KeyEventArgs e)
        {
            if (this.KeyPressed != null)
                this.KeyPressed(this, e);
            base.OnKeyDown(e);
        }

        private void HandleKeyReleased(object sender, KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);
        }

		//GLOG - 3951 - ceh
        private void JurisdictionChooser_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Back ||
                e.KeyCode == Keys.Left ||
                e.KeyCode == Keys.Right ||
                e.KeyCode == Keys.Up ||
                e.KeyCode == Keys.Delete ||
                e.KeyCode == Keys.Down ||
                e.KeyCode == Keys.PageUp ||
                e.KeyCode == Keys.PageDown ||
                e.KeyCode == Keys.Home ||
                e.KeyCode == Keys.End ||
                e.KeyCode == Keys.Shift ||
                e.KeyCode == Keys.Control ||
                e.KeyCode == Keys.Alt))
            {
                m_bNavigating = true;
            }
            else
            {
                m_bNavigating = false;
            }

        }

    }
}
