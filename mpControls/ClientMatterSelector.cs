﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class ClientMatterSelector : UserControl, IControl
    {
        public ClientMatterSelector()
        {
            InitializeComponent();
        }

        private void btnLookup_Click(object sender, EventArgs e)
        {
            try
            {
                LookupClientMatterNumber();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void txtClientMatter_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    LookupClientMatterNumber();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void LookupClientMatterNumber()
        {
            //LMP.Grail.ClientMatterLookupControl oForm = new
            //    LMP.Grail.ClientMatterLookupControl();

            //if (oForm.ShowDialog() == DialogResult.OK)
            //{
            //    this.txtClientMatter.Text = oForm.Matter.Number;
            //}
        }

        #region ****************************************IControl Members****************************************

        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
        {
            ((IControl)this).IsDirty = false;
        }

        [Browsable(false)]
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get
            {
                return this.txtClientMatter.Text;
            }
            set
            {
                this.txtClientMatter.Text = value;
            }
        }

        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the Dirty flag indicating the value has changed.")]
        public bool IsDirty { get; set; }

        [Browsable(false)]
        [DescriptionAttribute("Secondary user-defined object associated with the control.")]
        public object Tag2 { get; set; }

        [Browsable(false)]
        public string SupportingValues { get; set; }
        #endregion

        private void txtClientMatter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ((IControl)this).IsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
