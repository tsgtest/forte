namespace LMP.Controls
{
    partial class JurisdictionChooser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLevel0 = new System.Windows.Forms.Label();
            this.lblLevel1 = new System.Windows.Forms.Label();
            this.lblLevel2 = new System.Windows.Forms.Label();
            this.lblLevel3 = new System.Windows.Forms.Label();
            this.lblLevel4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbLevel0 = new LMP.Controls.ComboBox();
            this.cmbLevel4 = new LMP.Controls.ComboBox();
            this.cmbLevel1 = new LMP.Controls.ComboBox();
            this.cmbLevel2 = new LMP.Controls.ComboBox();
            this.cmbLevel3 = new LMP.Controls.ComboBox();
            this.txtLabel0 = new System.Windows.Forms.TextBox();
            this.txtLabel1 = new System.Windows.Forms.TextBox();
            this.txtLabel2 = new System.Windows.Forms.TextBox();
            this.txtLabel3 = new System.Windows.Forms.TextBox();
            this.txtLabel4 = new System.Windows.Forms.TextBox();
            this.txtLevel4Disabled = new System.Windows.Forms.TextBox();
            this.txtLevel3Disabled = new System.Windows.Forms.TextBox();
            this.txtLevel2Disabled = new System.Windows.Forms.TextBox();
            this.txtLevel1Disabled = new System.Windows.Forms.TextBox();
            this.txtLevel0Disabled = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLevel0
            // 
            this.lblLevel0.BackColor = System.Drawing.Color.Transparent;
            this.lblLevel0.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel0.Location = new System.Drawing.Point(2, 2);
            this.lblLevel0.Name = "lblLevel0";
            this.lblLevel0.Size = new System.Drawing.Size(49, 20);
            this.lblLevel0.TabIndex = 0;
            this.lblLevel0.Text = "Country:";
            this.lblLevel0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLevel1
            // 
            this.lblLevel1.BackColor = System.Drawing.Color.Transparent;
            this.lblLevel1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel1.Location = new System.Drawing.Point(1, 23);
            this.lblLevel1.Name = "lblLevel1";
            this.lblLevel1.Size = new System.Drawing.Size(65, 21);
            this.lblLevel1.TabIndex = 2;
            this.lblLevel1.Text = "State:";
            this.lblLevel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLevel2
            // 
            this.lblLevel2.BackColor = System.Drawing.Color.Transparent;
            this.lblLevel2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel2.Location = new System.Drawing.Point(2, 50);
            this.lblLevel2.Name = "lblLevel2";
            this.lblLevel2.Size = new System.Drawing.Size(37, 14);
            this.lblLevel2.TabIndex = 4;
            this.lblLevel2.Text = "Court:";
            this.lblLevel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLevel3
            // 
            this.lblLevel3.BackColor = System.Drawing.Color.Transparent;
            this.lblLevel3.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel3.Location = new System.Drawing.Point(2, 72);
            this.lblLevel3.Name = "lblLevel3";
            this.lblLevel3.Size = new System.Drawing.Size(65, 18);
            this.lblLevel3.TabIndex = 6;
            this.lblLevel3.Text = "Jurisdiction:";
            this.lblLevel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLevel4
            // 
            this.lblLevel4.BackColor = System.Drawing.Color.Transparent;
            this.lblLevel4.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel4.Location = new System.Drawing.Point(2, 96);
            this.lblLevel4.Name = "lblLevel4";
            this.lblLevel4.Size = new System.Drawing.Size(48, 14);
            this.lblLevel4.TabIndex = 8;
            this.lblLevel4.Text = "Division:";
            this.lblLevel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(329, 116);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cmbLevel0);
            this.panel2.Controls.Add(this.cmbLevel4);
            this.panel2.Controls.Add(this.lblLevel0);
            this.panel2.Controls.Add(this.cmbLevel1);
            this.panel2.Controls.Add(this.lblLevel1);
            this.panel2.Controls.Add(this.cmbLevel2);
            this.panel2.Controls.Add(this.lblLevel3);
            this.panel2.Controls.Add(this.cmbLevel3);
            this.panel2.Controls.Add(this.lblLevel4);
            this.panel2.Controls.Add(this.lblLevel2);
            this.panel2.Controls.Add(this.txtLabel0);
            this.panel2.Controls.Add(this.txtLabel1);
            this.panel2.Controls.Add(this.txtLabel2);
            this.panel2.Controls.Add(this.txtLabel3);
            this.panel2.Controls.Add(this.txtLabel4);
            this.panel2.Controls.Add(this.txtLevel4Disabled);
            this.panel2.Controls.Add(this.txtLevel3Disabled);
            this.panel2.Controls.Add(this.txtLevel2Disabled);
            this.panel2.Controls.Add(this.txtLevel1Disabled);
            this.panel2.Controls.Add(this.txtLevel0Disabled);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(329, 116);
            this.panel2.TabIndex = 0;
            // 
            // cmbLevel0
            // 
            this.cmbLevel0.AllowEmptyValue = false;
            this.cmbLevel0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLevel0.AutoSize = true;
            this.cmbLevel0.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLevel0.Borderless = false;
            this.cmbLevel0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLevel0.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLevel0.IsDirty = false;
            this.cmbLevel0.LimitToList = true;
            this.cmbLevel0.ListName = "";
            this.cmbLevel0.Location = new System.Drawing.Point(67, 0);
            this.cmbLevel0.MaxDropDownItems = 8;
            this.cmbLevel0.Name = "cmbLevel0";
            this.cmbLevel0.SelectedIndex = -1;
            this.cmbLevel0.SelectedValue = null;
            this.cmbLevel0.SelectionLength = 0;
            this.cmbLevel0.SelectionStart = 0;
            this.cmbLevel0.Size = new System.Drawing.Size(262, 24);
            this.cmbLevel0.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLevel0.SupportingValues = "";
            this.cmbLevel0.TabIndex = 1;
            this.cmbLevel0.Tag2 = null;
            this.cmbLevel0.Value = "";
            this.cmbLevel0.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLevel0_ValueChanged);
            this.cmbLevel0.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HandleKeyPressed);
            this.cmbLevel0.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HandleKeyReleased);
            // 
            // cmbLevel4
            // 
            this.cmbLevel4.AllowEmptyValue = true;
            this.cmbLevel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLevel4.AutoSize = true;
            this.cmbLevel4.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLevel4.Borderless = false;
            this.cmbLevel4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLevel4.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLevel4.IsDirty = false;
            this.cmbLevel4.LimitToList = true;
            this.cmbLevel4.ListName = "";
            this.cmbLevel4.Location = new System.Drawing.Point(67, 91);
            this.cmbLevel4.MaxDropDownItems = 8;
            this.cmbLevel4.Name = "cmbLevel4";
            this.cmbLevel4.SelectedIndex = -1;
            this.cmbLevel4.SelectedValue = null;
            this.cmbLevel4.SelectionLength = 0;
            this.cmbLevel4.SelectionStart = 0;
            this.cmbLevel4.Size = new System.Drawing.Size(262, 24);
            this.cmbLevel4.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLevel4.SupportingValues = "";
            this.cmbLevel4.TabIndex = 9;
            this.cmbLevel4.Tag2 = null;
            this.cmbLevel4.Value = "";
            this.cmbLevel4.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLevel4_ValueChanged);
            this.cmbLevel4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HandleKeyPressed);
            this.cmbLevel4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HandleKeyReleased);
            // 
            // cmbLevel1
            // 
            this.cmbLevel1.AllowEmptyValue = false;
            this.cmbLevel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLevel1.AutoSize = true;
            this.cmbLevel1.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLevel1.Borderless = false;
            this.cmbLevel1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLevel1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLevel1.IsDirty = false;
            this.cmbLevel1.LimitToList = true;
            this.cmbLevel1.ListName = "";
            this.cmbLevel1.Location = new System.Drawing.Point(67, 23);
            this.cmbLevel1.MaxDropDownItems = 8;
            this.cmbLevel1.Name = "cmbLevel1";
            this.cmbLevel1.SelectedIndex = -1;
            this.cmbLevel1.SelectedValue = null;
            this.cmbLevel1.SelectionLength = 0;
            this.cmbLevel1.SelectionStart = 0;
            this.cmbLevel1.Size = new System.Drawing.Size(262, 24);
            this.cmbLevel1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLevel1.SupportingValues = "";
            this.cmbLevel1.TabIndex = 3;
            this.cmbLevel1.Tag2 = null;
            this.cmbLevel1.Value = "";
            this.cmbLevel1.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLevel1_ValueChanged);
            this.cmbLevel1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HandleKeyPressed);
            this.cmbLevel1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HandleKeyReleased);
            // 
            // cmbLevel2
            // 
            this.cmbLevel2.AllowEmptyValue = false;
            this.cmbLevel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLevel2.AutoSize = true;
            this.cmbLevel2.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLevel2.Borderless = false;
            this.cmbLevel2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLevel2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLevel2.IsDirty = false;
            this.cmbLevel2.LimitToList = true;
            this.cmbLevel2.ListName = "";
            this.cmbLevel2.Location = new System.Drawing.Point(67, 46);
            this.cmbLevel2.MaxDropDownItems = 8;
            this.cmbLevel2.Name = "cmbLevel2";
            this.cmbLevel2.SelectedIndex = -1;
            this.cmbLevel2.SelectedValue = null;
            this.cmbLevel2.SelectionLength = 0;
            this.cmbLevel2.SelectionStart = 0;
            this.cmbLevel2.Size = new System.Drawing.Size(262, 24);
            this.cmbLevel2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLevel2.SupportingValues = "";
            this.cmbLevel2.TabIndex = 5;
            this.cmbLevel2.Tag2 = null;
            this.cmbLevel2.Value = "";
            this.cmbLevel2.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLevel2_ValueChanged);
            this.cmbLevel2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HandleKeyPressed);
            this.cmbLevel2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HandleKeyReleased);
            // 
            // cmbLevel3
            // 
            this.cmbLevel3.AllowEmptyValue = true;
            this.cmbLevel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLevel3.AutoSize = true;
            this.cmbLevel3.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLevel3.Borderless = false;
            this.cmbLevel3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLevel3.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLevel3.IsDirty = false;
            this.cmbLevel3.LimitToList = true;
            this.cmbLevel3.ListName = "";
            this.cmbLevel3.Location = new System.Drawing.Point(67, 69);
            this.cmbLevel3.MaxDropDownItems = 8;
            this.cmbLevel3.Name = "cmbLevel3";
            this.cmbLevel3.SelectedIndex = -1;
            this.cmbLevel3.SelectedValue = null;
            this.cmbLevel3.SelectionLength = 0;
            this.cmbLevel3.SelectionStart = 0;
            this.cmbLevel3.Size = new System.Drawing.Size(262, 23);
            this.cmbLevel3.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLevel3.SupportingValues = "";
            this.cmbLevel3.TabIndex = 7;
            this.cmbLevel3.Tag2 = null;
            this.cmbLevel3.Value = "";
            this.cmbLevel3.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLevel3_ValueChanged);
            this.cmbLevel3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HandleKeyPressed);
            this.cmbLevel3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HandleKeyReleased);
            // 
            // txtLabel0
            // 
            this.txtLabel0.BackColor = System.Drawing.SystemColors.Window;
            this.txtLabel0.Enabled = false;
            this.txtLabel0.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLabel0.Location = new System.Drawing.Point(0, 0);
            this.txtLabel0.Margin = new System.Windows.Forms.Padding(0);
            this.txtLabel0.Multiline = true;
            this.txtLabel0.Name = "txtLabel0";
            this.txtLabel0.ReadOnly = true;
            this.txtLabel0.Size = new System.Drawing.Size(68, 23);
            this.txtLabel0.TabIndex = 5;
            this.txtLabel0.TabStop = false;
            // 
            // txtLabel1
            // 
            this.txtLabel1.BackColor = System.Drawing.SystemColors.Window;
            this.txtLabel1.Enabled = false;
            this.txtLabel1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLabel1.Location = new System.Drawing.Point(0, 23);
            this.txtLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.txtLabel1.Multiline = true;
            this.txtLabel1.Name = "txtLabel1";
            this.txtLabel1.ReadOnly = true;
            this.txtLabel1.Size = new System.Drawing.Size(68, 23);
            this.txtLabel1.TabIndex = 6;
            this.txtLabel1.TabStop = false;
            // 
            // txtLabel2
            // 
            this.txtLabel2.BackColor = System.Drawing.SystemColors.Window;
            this.txtLabel2.Enabled = false;
            this.txtLabel2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLabel2.Location = new System.Drawing.Point(0, 46);
            this.txtLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.txtLabel2.Multiline = true;
            this.txtLabel2.Name = "txtLabel2";
            this.txtLabel2.ReadOnly = true;
            this.txtLabel2.Size = new System.Drawing.Size(68, 23);
            this.txtLabel2.TabIndex = 7;
            this.txtLabel2.TabStop = false;
            // 
            // txtLabel3
            // 
            this.txtLabel3.BackColor = System.Drawing.SystemColors.Window;
            this.txtLabel3.Enabled = false;
            this.txtLabel3.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLabel3.Location = new System.Drawing.Point(0, 69);
            this.txtLabel3.Margin = new System.Windows.Forms.Padding(0);
            this.txtLabel3.Multiline = true;
            this.txtLabel3.Name = "txtLabel3";
            this.txtLabel3.ReadOnly = true;
            this.txtLabel3.Size = new System.Drawing.Size(68, 23);
            this.txtLabel3.TabIndex = 8;
            this.txtLabel3.TabStop = false;
            // 
            // txtLabel4
            // 
            this.txtLabel4.BackColor = System.Drawing.SystemColors.Window;
            this.txtLabel4.Enabled = false;
            this.txtLabel4.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLabel4.Location = new System.Drawing.Point(0, 92);
            this.txtLabel4.Margin = new System.Windows.Forms.Padding(0);
            this.txtLabel4.Multiline = true;
            this.txtLabel4.Name = "txtLabel4";
            this.txtLabel4.ReadOnly = true;
            this.txtLabel4.Size = new System.Drawing.Size(68, 23);
            this.txtLabel4.TabIndex = 9;
            this.txtLabel4.TabStop = false;
            // 
            // txtLevel4Disabled
            // 
            this.txtLevel4Disabled.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLevel4Disabled.BackColor = System.Drawing.SystemColors.Window;
            this.txtLevel4Disabled.Enabled = false;
            this.txtLevel4Disabled.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel4Disabled.Location = new System.Drawing.Point(66, 92);
            this.txtLevel4Disabled.Margin = new System.Windows.Forms.Padding(0);
            this.txtLevel4Disabled.Multiline = true;
            this.txtLevel4Disabled.Name = "txtLevel4Disabled";
            this.txtLevel4Disabled.ReadOnly = true;
            this.txtLevel4Disabled.Size = new System.Drawing.Size(263, 23);
            this.txtLevel4Disabled.TabIndex = 14;
            this.txtLevel4Disabled.TabStop = false;
            this.txtLevel4Disabled.Visible = false;
            // 
            // txtLevel3Disabled
            // 
            this.txtLevel3Disabled.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLevel3Disabled.BackColor = System.Drawing.SystemColors.Window;
            this.txtLevel3Disabled.Enabled = false;
            this.txtLevel3Disabled.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel3Disabled.Location = new System.Drawing.Point(66, 69);
            this.txtLevel3Disabled.Margin = new System.Windows.Forms.Padding(0);
            this.txtLevel3Disabled.Multiline = true;
            this.txtLevel3Disabled.Name = "txtLevel3Disabled";
            this.txtLevel3Disabled.ReadOnly = true;
            this.txtLevel3Disabled.Size = new System.Drawing.Size(263, 23);
            this.txtLevel3Disabled.TabIndex = 13;
            this.txtLevel3Disabled.TabStop = false;
            this.txtLevel3Disabled.Visible = false;
            // 
            // txtLevel2Disabled
            // 
            this.txtLevel2Disabled.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLevel2Disabled.BackColor = System.Drawing.SystemColors.Window;
            this.txtLevel2Disabled.Enabled = false;
            this.txtLevel2Disabled.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel2Disabled.Location = new System.Drawing.Point(66, 46);
            this.txtLevel2Disabled.Margin = new System.Windows.Forms.Padding(0);
            this.txtLevel2Disabled.Multiline = true;
            this.txtLevel2Disabled.Name = "txtLevel2Disabled";
            this.txtLevel2Disabled.ReadOnly = true;
            this.txtLevel2Disabled.Size = new System.Drawing.Size(263, 23);
            this.txtLevel2Disabled.TabIndex = 12;
            this.txtLevel2Disabled.TabStop = false;
            this.txtLevel2Disabled.Visible = false;
            // 
            // txtLevel1Disabled
            // 
            this.txtLevel1Disabled.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLevel1Disabled.BackColor = System.Drawing.SystemColors.Window;
            this.txtLevel1Disabled.Enabled = false;
            this.txtLevel1Disabled.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel1Disabled.Location = new System.Drawing.Point(66, 23);
            this.txtLevel1Disabled.Margin = new System.Windows.Forms.Padding(0);
            this.txtLevel1Disabled.Multiline = true;
            this.txtLevel1Disabled.Name = "txtLevel1Disabled";
            this.txtLevel1Disabled.ReadOnly = true;
            this.txtLevel1Disabled.Size = new System.Drawing.Size(263, 23);
            this.txtLevel1Disabled.TabIndex = 11;
            this.txtLevel1Disabled.TabStop = false;
            this.txtLevel1Disabled.Visible = false;
            // 
            // txtLevel0Disabled
            // 
            this.txtLevel0Disabled.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLevel0Disabled.BackColor = System.Drawing.SystemColors.Window;
            this.txtLevel0Disabled.Enabled = false;
            this.txtLevel0Disabled.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel0Disabled.Location = new System.Drawing.Point(66, 0);
            this.txtLevel0Disabled.Margin = new System.Windows.Forms.Padding(0);
            this.txtLevel0Disabled.Multiline = true;
            this.txtLevel0Disabled.Name = "txtLevel0Disabled";
            this.txtLevel0Disabled.ReadOnly = true;
            this.txtLevel0Disabled.Size = new System.Drawing.Size(263, 23);
            this.txtLevel0Disabled.TabIndex = 10;
            this.txtLevel0Disabled.TabStop = false;
            this.txtLevel0Disabled.Visible = false;
            // 
            // JurisdictionChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "JurisdictionChooser";
            this.Size = new System.Drawing.Size(329, 116);
            this.Enter += new System.EventHandler(this.JurisdictionChooser_Enter);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion

        private ComboBox cmbLevel0;
        private System.Windows.Forms.Label lblLevel0;
        private System.Windows.Forms.Label lblLevel1;
        private ComboBox cmbLevel1;
        private System.Windows.Forms.Label lblLevel2;
        private ComboBox cmbLevel2;
        private System.Windows.Forms.Label lblLevel3;
        private ComboBox cmbLevel3;
        private System.Windows.Forms.Label lblLevel4;
        private ComboBox cmbLevel4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtLabel3;
        private System.Windows.Forms.TextBox txtLabel2;
        private System.Windows.Forms.TextBox txtLabel1;
        private System.Windows.Forms.TextBox txtLabel0;
        private System.Windows.Forms.TextBox txtLabel4;
        private System.Windows.Forms.TextBox txtLevel4Disabled;
        private System.Windows.Forms.TextBox txtLevel3Disabled;
        private System.Windows.Forms.TextBox txtLevel2Disabled;
        private System.Windows.Forms.TextBox txtLevel1Disabled;
        private System.Windows.Forms.TextBox txtLevel0Disabled;

    }
}
