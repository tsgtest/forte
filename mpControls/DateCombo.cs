using System;
using System.Collections.Generic;
using System.Collections; 
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using LMP.Data;


namespace LMP.Controls
{

    public partial class DateCombo : UserControl, IControl
    {
        #region *********************fields*******************
        private string m_xFormats;
        private bool m_bIsDirty;
        private string m_xList;
        private DateComboModes m_iMode;
        private const char ITEM_SEP = LMP.StringArray.mpEndOfSubField;
        private string m_xSupportingValues = "";
        private int m_iLCID = 1033;
        #endregion
        #region *********************enumerations*******************
        public enum DateComboModes
        {
            Literal = 0,
            Format = 1
        }
        #endregion
        #region *********************constructors*******************
        public DateCombo()
        {
            InitializeComponent();
        }

        #endregion
        #region *********************properties*******************
        [DescriptionAttribute("Gets/Sets the name of the list that displays in the dropdown portion of the control.")]
        public string ListName
        {
            get { return m_xList; }
            set 
            { 
                m_xList = value;
                //GLOG 7584: Refresh list to reflect change
                if (!this.DesignMode)
                    RefreshList();
            }
        }
        [DescriptionAttribute("Gets/Sets DateComboMode for the control.")]
        public DateComboModes Mode
        {
            get { return m_iMode; }
            set 
            { 
                m_iMode = value;
                //sets limit to list to permit/suppress manual
                //input depending on mode property
                switch (value)
                {
                    case DateComboModes.Literal:
                        this.LimitToList = false;
                        break;
                    case DateComboModes.Format:
                        //TODO: Make this a control property?
                        this.LimitToList = false;
                        break;
                    default:
                        break;
                }
            }
        }
        [DescriptionAttribute("Gets/Sets delimited string of date formats.")]
        public string Formats
        {
            get{return m_xFormats;}
            set 
            {
                m_xFormats = value;
                //GLOG 7584: Refresh list to reflect change
                if (!this.DesignMode)
                    RefreshList();
            }
        }
        [DescriptionAttribute("Gets/Sets the text of the control.")]
        public override string Text
        {
            get { return this.comboBox1.Text; }
            set {this.comboBox1.Text = value;}
        }
        [DescriptionAttribute("Gets the list items of the control as an array")]
        public string[,] ListArray
        {
            get
            {
                return this.comboBox1.ListArray; 
            }
        }
        [DescriptionAttribute("Gets/Sets whether control displays value set lists only")]
        private bool LimitToList
        {
            get { return comboBox1.LimitToList; }
            set {this.comboBox1.LimitToList = value;}
        }
        [DescriptionAttribute("Gets/Sets the backcolor of the control.")]
        public override System.Drawing.Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the font of the control.")]
        public override Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
            }
        }
        [DescriptionAttribute("Gets/Sets whether or not the control displays borders.")]
        public bool Borderless
        {
            get { return comboBox1.Borderless; }
            set
            {
                comboBox1.Borderless = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the number of dropdown rows displayed.")]
        public int MaxDropDownItems
        {
            get { return this.comboBox1.MaxDropDownItems; }
            set { this.comboBox1.MaxDropDownItems = value; }
        }
        [DescriptionAttribute("Gets/Sets the index of the selected value.")]
        public int SelectedIndex
        {
            get { return this.comboBox1.SelectedIndex; }
            set
            {
                this.comboBox1.SelectedIndex = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the length of the selected text.")]
        public int SelectionLength
        {
            get { return this.comboBox1.SelectionLength; }
            set { this.comboBox1.SelectionLength = value; }
        }
        [DescriptionAttribute("Gets/Sets the first selected character.")]
        public int SelectionStart
        {
            get { return this.comboBox1.SelectionStart; }
            set { this.comboBox1.SelectionStart = value; }
        }
        [DescriptionAttribute("Gets/Sets the LCID of the date formats.")]
        public int LCID
        {
            get {return m_iLCID;}
            set
            {
                m_iLCID = value;
                //GLOG 7584: Refresh list to reflect current language
                if (!this.DesignMode)
                {
                    RefreshList();
                }
            }
        }
        #endregion
        #region *********************methods*******************
        /// <summary>
        /// overloaded method takes ArrayList for listitems
        /// </summary>
        /// <param name="aList"></param>
        public void SetList(ArrayList aList)
        {
            this.comboBox1.SetList(aList);
        }
        /// <summary>
        /// populates the combo list with items in the specified System.Array
        /// </summary>
        /// <param name="aList"></param>
        public void SetList(Array aList)
        {
            this.comboBox1.SetList(aList);
        }
        /// <summary>
        /// displays the items in the specified array
        /// </summary>
        /// <param name="aListItems"></param>
        public void SetList(string[,] aListItems)
        {
            this.comboBox1.SetList(aListItems);
        }
        /// <summary>
        /// displays the items in the specified subfield delimited string
        /// </summary>
        /// <param name="aListItems"></param>
        public void SetList(string xListItems)
        {
            this.CreateDateList(xListItems);
        }
        /// <summary>
        /// splits subfield delimited string of date formats,
        /// calls overloaded method to create list, loads combo
        /// </summary>
        /// <param name="xListItems"></param>
        private void CreateDateList(string xListItems)
        {
            //GLOG item #5931 - dcf - the xml quote character
            //equivalents were added to snapshot to address
            //a different issue - they need to be removed here
            xListItems = xListItems.Replace("&quot;", "\"");

            if (xListItems.IndexOf(ITEM_SEP) != -1)
                CreateDateList(xListItems.Split(ITEM_SEP));
            else
                MessageBox.Show(LMP.Resources.GetLangString("Msg_InvalidDateFormatsDelimiter"),
                                LMP.String.MacPacProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);      
        }
        /// <summary>
        /// creates 2 column array, display column = current date in formats specified in
        /// designated array of date formats
        /// </summary>
        /// <param name="aList"></param>
        private void CreateDateList(string[] aList)
        {
            string[,] aDateFormats = new string[aList.GetUpperBound(0) + 1, 2];
            DateTime oDT = DateTime.Now;

            for (int i = 0; i <= aList.GetUpperBound(0); i++)
            {
                aDateFormats[i, 0] = GetFormattedDate(oDT, aList[i]);
                aDateFormats[i, 1] = aList[i];
            }

            SetList(aDateFormats);
        }
        /// <summary>
        /// stores/loads the list with the specified name,
        /// based on the list load mode
        /// </summary>
        /// <param name="xListName"></param>
        private void SetListName(string xListName)
        {
            if (this.DesignMode || xListName == null || xListName == "")
                return;

            Lists oLists = new Lists();
            List oList = null;

            try
            {
                oList = (List)oLists.ItemFromName(xListName);

            }
            catch (System.Exception oE)
            {
                //invalid list
                throw new LMP.Exceptions.ListException(
                    LMP.Resources.GetLangString("Error_ListNotFound") + xListName, oE);
            }

            SetList(oList);
        }
        /// <summary>
        /// stores/loads the specified list
        /// </summary>
        /// <param name="oList"></param>
        public void SetList(List oList)
        {
            this.ClearList();

            DataTable oDT = oList.ItemDataTable;
            
            string[] aList = new string[oDT.Rows.Count];

            for (int i = 0; i < oDT.Rows.Count; i++)
            {
                aList[i] = oDT.Rows[i].ItemArray.GetValue(1).ToString();
            }
            CreateDateList(aList); 
        }
        /// <summary>
        /// clears the dropdown list
        /// </summary>
        public void ClearList()
        {
            this.comboBox1.ClearList();
        }
        #endregion
        #region *********************internal procedures*******************
        /// <summary>
        /// converts date format strings to display text
        /// handles jurat-type dates
        /// </summary>
        /// <param name="oDateTime"></param>
        /// <param name="xDateFormat"></param>
        /// <returns></returns>
        private string GetFormattedDate(DateTime oDateTime, string xDateFormat)
        {
            return LMP.String.EvaluateDateTimeValue(xDateFormat, oDateTime, this.LCID);
        }
        private void RefreshList() //GLOG 7584
        {
            if (!string.IsNullOrEmpty(this.ListName))
                this.SetListName(this.ListName);
            else if (!string.IsNullOrEmpty(this.Formats))
                this.SetList(this.Formats);
        }
        #endregion
        #region *********************procedures*******************
        #endregion
        #region *********************event handlers *******************
        /// <summary>
        /// handler for underlying LMP.Forte.WordMethodsbo ValueChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.IsDirty = true;

                //GLOG : 7176 : JSW
                //if a field code type is selected
                //don't allow user edits
                //this is working while you are in the list,
                //but if you come back in without changing, it's enabled again.
                if (comboBox1.SelectedIndex > -1 && this.Mode == DateComboModes.Format)
                {
                    if (comboBox1.SelectedValue.ToString().Contains("/F"))
                        this.comboBox1.LimitToList = true;
                    else
                       this.comboBox1.LimitToList = false;
                }
                //notify that value has changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        void comboBox1_KeyReleased(object sender, KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);
        }
        void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                //Container is handling tab - don't process further
                //TabPressed event is raise by control only in ProcessKeyMessage
                e.SuppressKeyPress = true;
                e.Handled = true;
                return;
            }
            if (this.KeyPressed != null)
                this.KeyPressed(this, e);
            
            base.OnKeyDown(e);
        }
        /// <summary>
        /// Raise LostFocus event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DateCombo_Leave(object sender, EventArgs e)
        {
            base.OnLostFocus(e);
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// support for tabbing for MacPac controls
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab || keyData == Keys.Return)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (m.WParam.ToInt32() == (int)Keys.Return)
            {
                // Process Enter here, because KeyDown doesn't get raised for this
                // in TaskPane
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        /// <summary>
        /// 
        /// </summary>
        public void ExecuteFinalSetup()
        {
            try
            {
                //GLOG 2915: Sort list items to ensure selected item matches text
                //By default, first item starting with text will be selected, 
                //even if an exact match follows it in the list
                this.comboBox1.SortOrder = SortOrder.Ascending;
                LMP.Trace.WriteInfo("ExecuteFinalSetup");
                //if an LMP list has been specified, load it - otherwise use
                //formats string
                if (!string.IsNullOrEmpty(this.ListName))
                    this.SetListName(this.ListName);
                else
                    this.SetList(this.Formats);

                this.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get
            {
                switch (this.Mode )
                {
                    case DateComboModes.Literal:
                        return comboBox1.Text;
                    case DateComboModes.Format:
                        //Can either be a date format or literal string
                        if (comboBox1.SelectedIndex > -1)
                            return comboBox1.ListArray.GetValue(new int[] { comboBox1.SelectedIndex, 1 }).ToString();
                        else
                            return comboBox1.Text;
                    default:
                        return null;
                }
            }
            set
            {
                switch (this.Mode)
	            {
		            case DateComboModes.Literal:
                        this.comboBox1.Text = value;
                        break;
                    case DateComboModes.Format:
                        this.comboBox1.LimitToList = true;
                        string xOldValue = this.comboBox1.Value;
                        this.comboBox1.SelectedIndex = -1;
                        this.comboBox1.Value = value;
                        if (value == xOldValue)
                        {
                            //there was no real change in value,
                            //so set dirty to false
                            this.IsDirty = false;
                        }

                        //If value contains MMM and doesn't match,
                        //check for identical format using MMMM
                        if (this.comboBox1.SelectedIndex == -1 && value.Contains("MMM"))
                            this.comboBox1.Value = value.Replace("MMM", "MMMM");

                        //If non-matching format contains double-digit day,
                        //check for identical format using single-digit day
                        if (this.comboBox1.SelectedIndex == -1 && value.Contains("dd"))
                            this.comboBox1.Value = value.Replace("dd", "d");

                        //GLOG : 7176 : JSW
                        //if fieldcode, don't allow editing
                        if (this.comboBox1.Value.Contains("/F"))
                            this.comboBox1.LimitToList = true;
                        else
                            this.comboBox1.LimitToList = false;

                        if (this.comboBox1.SelectedIndex == -1)
                        {
                            //check to see if the value is in the list -
                            //sometimes item is not selected even when
                            //it's in the list
                            //Value is not a format from the list
                            this.comboBox1.Text = value;
                        }
                        break;
                    default:
                        break;
	            }
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the Dirty flag indicating the value has changed.")]
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get 
            { 
                return m_bIsDirty; 
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Secondary user-defined object associated with the control.")]
        //TODO: what is this used for?
        public object Tag2
        {
            get { return comboBox1.Tag2; }
            set { this.comboBox1.Tag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion

    }
}
