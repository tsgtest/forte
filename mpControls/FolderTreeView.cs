using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Infragistics.Win.UltraWinTree;
using LMP.Data;
using System.Drawing;
using System.Windows.Forms;

namespace LMP.Controls
{
    public delegate void TreeDoubleClickEventHandler(object sender, EventArgs oArgs);
    //GLOG : 6257 : CEH
    public delegate void TreeKeyDownEventHandler(object sender, KeyEventArgs oArgs);

    public partial class FolderTreeView : Infragistics.Win.UltraWinTree.UltraTree
    {
        #region **********************enums*********************
        [Flags]
        private enum Images
        {
            FolderClosed = 0,
            FolderOpen = 1,
            DocumentSegment = 2,
            Prefill = 3,
            ParagraphTextSegment = 4,
            ComponentSegment = 5,
            StyleSheetSegment = 6,
            AnswerFile = 7,
            DataPacket = 8,
            UserStyleSheet = 9,
            UserPrefill = 10,
            UserAnswerFileSegment = 11,
            UserDocument = 12,
            UserParagraphText = 13,
            SentenceTextSegment = 14,
            UserSentenceText = 15,
            SearchResults = 16,
            Bullet = 17,
            Transparent = 18,
            //GLOG : 8363 : jsw
            WordDocument = 19, 
            ExternalContent = 20,
            XLDocument = 21,
            MasterDataForm = 22
        }
        [Flags]
        public enum mpFolderTreeViewOptions
        {
            UserFolders = 1,
            SharedFolders = 2,
            AdminFolders = 4,
            Segments = 8,
            // GLOG : 3071 : JAB
            // Add option to show UserSegments.
            UserSegments = 16,
            Prefills = 32,
            StyleSheets = 64, //GLOG 6208
            SegmentPackets = 128,
            PreferenceSegmentsOnly = 256,
            DesignerSegments = 512, //GLOG 8413
            MasterData = 1024 //GLOG 8163
        }
        public enum mpFolderTreeViewExpansion
        {
            ExpandNone = 0,
            ExpandTopLevel = 1,
            ExpandAll = 2
        }
        public enum mpFolderTreeViewModes
        {
            FolderTree = 0,
            FindResults = 1,
            AdvancedFindResults = 2 //GLOG : 8348 : jsw
        }

        public enum mpFolderTreeSearchContentTypes
        {
            AllContent = 0,
            Segments = 1,
            SavedData = 2,
            StyleSheets = 3,
            MasterData = 4,
            SegmentPackets = 5
        }

        #endregion
        #region *********************fields*********************
        public event TreeDoubleClickEventHandler TreeDoubleClick;
        //GLOG : 6257 : CEH
        public event TreeKeyDownEventHandler TreeKeyDown;
        //GLOG 8163: Include MasterData by default
        mpFolderTreeViewOptions m_iDisplayOptions = mpFolderTreeViewOptions.AdminFolders | mpFolderTreeViewOptions.UserFolders | mpFolderTreeViewOptions.Segments |
            mpFolderTreeViewOptions.UserSegments | mpFolderTreeViewOptions.StyleSheets | mpFolderTreeViewOptions.SegmentPackets | mpFolderTreeViewOptions.MasterData;
        private int m_iOwnerID = 0;
        private bool m_bControlIsLoaded = false;
        private bool m_bShowCheckboxes = false;
        private ArrayList m_aFolderList = null;
        private ArrayList m_aFolderMemberList = null;
        private bool m_bIsDirty = false;
        private bool m_bDisableExcludedTypes = false;
        private mpFolderTreeViewExpansion m_iExpansion = mpFolderTreeViewExpansion.ExpandAll;
        private string m_xFilterText = "";
        private ArrayList m_aFindResults = null;
        private mpFolderTreeViewModes m_iMode = mpFolderTreeViewModes.FolderTree;
        private bool m_bShowFindPaths = true;
        //GLOG : 8152 : jsw
        private bool m_bShowTopUserFolderNode = false;
        //GLOG : 8380 : jsw
        private bool m_bExcludeNonMacPacTypes = false;
		        //GLOG : 8348 : jsw
        private mpFolderTreeSearchContentTypes m_iSearchContentType = mpFolderTreeSearchContentTypes.AllContent;
        private mpSegmentFindFields m_iSearchFields = 0;
        private int m_iSearchUserID = 0;
        private string m_xSearchFolderList = "";
        private bool m_bIncludeAdminContent = true;
        private bool m_bIncludeUserContent = true;
        private string m_xAdvancedFilterText = "";


        #endregion
        #region *******************Constructors***********************
        public FolderTreeView()
        {
            InitializeComponent();
        }

        public FolderTreeView(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion
        #region *****************Properties******************
        /// <summary>
        /// Get/Set OwnerID for displayed objects
        /// </summary>
        public int OwnerID
        {
            get { return m_iOwnerID; }
            set 
            { 
                m_iOwnerID = value;
                if (!this.DesignMode && m_bControlIsLoaded)
                    SetupComponent();
            }
        }
        /// <summary>
        /// Get/Set which types of objects to display in the tree
        /// </summary>
        public mpFolderTreeViewOptions DisplayOptions
        {
            get { return m_iDisplayOptions; }
            set
            {
                m_iDisplayOptions = value;
                if (!this.DesignMode && m_bControlIsLoaded)
                    SetupComponent();
            }
        }
        public bool DisableExcludedTypes
        {
            get { return m_bDisableExcludedTypes; }
            set
            {
                m_bDisableExcludedTypes = value;
                if (!this.DesignMode && m_bControlIsLoaded)
                    SetupComponent();
            }
        }
        //GLOG : 8380 : jsw
        public bool ExcludeNonMacPacTypes
        {
            get { return m_bExcludeNonMacPacTypes; }
            set
            {
                m_bExcludeNonMacPacTypes = value;
            }
        }
        /// <summary>
        /// Get/Set whether to display checkboxes for nodes
        /// </summary>
        public bool ShowCheckboxes
        {
            get { return m_bShowCheckboxes; }
            set 
            { 
                m_bShowCheckboxes = value;
                if (!this.DesignMode && m_bControlIsLoaded)
                    SetupComponent();
            }

        }
        /// <summary>
        /// Get/Set which nodes will be expanded initially
        /// </summary>
        public mpFolderTreeViewExpansion Expansion
        {
            get { return m_iExpansion; }
            set { m_iExpansion = value; }
        }
        /// <summary>
        /// Get ArrayList of ArrayLists containing Folder ID, Name and Type
        /// </summary>
        public ArrayList FolderList
        {
            get
            {
                if (m_aFolderList == null || this.IsDirty)
                {
                    PopulateFolderList();
                }
                return m_aFolderList;
            }
        }
        /// <summary>
        /// Get ArrayList of ArrayLists containing FolderMember ID, Name and ObjectType
        /// </summary>
        public ArrayList MemberList
        {
            get
            {
                if (m_aFolderMemberList == null || this.IsDirty)
                {
                    PopulateMemberList();
                }
                return m_aFolderMemberList;
            }
        }
        //GLOG : 8348 : jsw
        public mpFolderTreeSearchContentTypes SearchContentType
        {
            get { return m_iSearchContentType; }
            set { m_iSearchContentType = value; }

        }
        //GLOG : 8348 : jsw
        public mpSegmentFindFields SearchFields
        {
            get { return m_iSearchFields; }
            set { m_iSearchFields = value; }

        }       
        //GLOG : 8348 : jsw
        public int SearchUserID
        {
            get { return m_iSearchUserID; }
            set { m_iSearchUserID = value; }

        }     
        //GLOG : 8348 : jsw
        public string SearchFolderList
        {
            get { return m_xSearchFolderList; }
            set { m_xSearchFolderList = value; }
        }    
        //GLOG : 8348 : jsw
        public bool IncludeAdminContentInSearch
        {
            get { return m_bIncludeAdminContent; }
            set { m_bIncludeAdminContent = value; }
        }
        //GLOG : 8348 : jsw
        public bool IncludeUserContentInSearch
        {
            get { return m_bIncludeUserContent; }
            set { m_bIncludeUserContent = value; }
        }      
        /// <summary>
        /// Get delimited list of Segment or Folder IDs
        /// </summary>
        public string Value
        {
            get
            {
                if (this.MemberList.Count > 0)
                {
                    string xTemp = "";
                    //return delimited list of Segment IDs
                    for (int i = 0; i < this.MemberList.Count; i++)
                    {
                        xTemp = xTemp + ((ArrayList)this.MemberList[i])[0].ToString();
                        if (i < this.MemberList.Count - 1)
                            xTemp = xTemp + ",";
                    }
                    return xTemp;
                }
                else if (this.FolderList.Count > 0)
                {
                    string xTemp = "";
                    //return delimited list of Segment IDs
                    for (int i = 0; i < this.FolderList.Count; i++)
                    {
                        xTemp = xTemp + ((ArrayList)this.FolderList[i])[0].ToString();
                        if (i < this.FolderList.Count)
                            xTemp = xTemp + ",";
                    }
                    return xTemp.Substring(0, xTemp.Length - 1);
                }
                else
                {
                    return "";
                }
            }
        }
        public string FilterText
        {
            set
            {
                //GLOG 8163: Don't check previous value - since m_aFindResults is used for both Advanced and standard results,
                //we may need to execute search even if previous find results exist
                //if (m_xFilterText != value)
                //{
                    m_xFilterText = value;
                    if (this.DesignMode)
                        return;
                    try
                    {
                        mpSegmentFindTypes iTypes = 0;
                        //GLOG 6031: Option to include only Segments with Author Preferences
                        if ((m_iDisplayOptions & mpFolderTreeViewOptions.PreferenceSegmentsOnly) == mpFolderTreeViewOptions.PreferenceSegmentsOnly)
                            iTypes = mpSegmentFindTypes.Preferences;
                        else
                        {
                            if ((m_iDisplayOptions & mpFolderTreeViewOptions.Segments) == mpFolderTreeViewOptions.Segments)
                                iTypes = iTypes | mpSegmentFindTypes.AdminSegments;
                            if ((m_iDisplayOptions & mpFolderTreeViewOptions.UserSegments) == mpFolderTreeViewOptions.UserSegments)
                                iTypes = iTypes | mpSegmentFindTypes.UserSegments;
                            if ((m_iDisplayOptions & mpFolderTreeViewOptions.Prefills) == mpFolderTreeViewOptions.Prefills)
                                iTypes = iTypes | mpSegmentFindTypes.Prefills;
                            //GLOG 6208: Include Style Sheets and Segment Packets in results only if specified
                            if ((m_iDisplayOptions & mpFolderTreeViewOptions.UserFolders) == mpFolderTreeViewOptions.UserFolders)
                            {
                                if ((m_iDisplayOptions & mpFolderTreeViewOptions.StyleSheets) == mpFolderTreeViewOptions.StyleSheets)
                                    iTypes = iTypes | mpSegmentFindTypes.UserStyleSheets;
                                if ((m_iDisplayOptions & mpFolderTreeViewOptions.SegmentPackets) == mpFolderTreeViewOptions.SegmentPackets)
                                    iTypes = iTypes | mpSegmentFindTypes.UserSegmentPackets;
                            }
                            if ((m_iDisplayOptions & mpFolderTreeViewOptions.AdminFolders) == mpFolderTreeViewOptions.AdminFolders)
                            {
                                if ((m_iDisplayOptions & mpFolderTreeViewOptions.StyleSheets) == mpFolderTreeViewOptions.StyleSheets)
                                    iTypes = iTypes | mpSegmentFindTypes.StyleSheets;
                                if ((m_iDisplayOptions & mpFolderTreeViewOptions.SegmentPackets) == mpFolderTreeViewOptions.SegmentPackets)
                                    iTypes = iTypes | mpSegmentFindTypes.SegmentPackets;
                            }
                        }

                        m_aFindResults = Data.Application.GetSegmentFindResults(m_xFilterText,
                            Data.Application.User.ID, LMP.Data.mpSegmentFindMode.MatchSubString, iTypes);

                        if (m_aFindResults == null || m_aFindResults.Count == 0)
                        {
                            m_xFilterText = "";
                            m_aFindResults = null;
                            this.Mode = mpFolderTreeViewModes.FolderTree;
                        }
                        else
                        {
                            this.Mode = mpFolderTreeViewModes.FindResults;
                        }
                    }
                    catch { }
                //}
            }
            get { return m_xFilterText; }
        }
        //GLOG : 8348 : jsw
        public string AdvancedFilterText
        {
            set
            {
                m_xAdvancedFilterText = value;
                if (this.DesignMode)
                    return;

                mpSegmentFindTypes iTypes = 0;

                switch (m_iSearchContentType)
                {
                    case mpFolderTreeSearchContentTypes.Segments:
                        if (m_iSearchUserID > 0)
                        {
                            //Only User Content will have an OwnerID
                            iTypes = mpSegmentFindTypes.UserSegments;
                        }
                        else
                        {
                            if (m_bIncludeAdminContent)
                                iTypes = iTypes | mpSegmentFindTypes.AdminSegments;
                            if (m_bIncludeUserContent)
                                iTypes = iTypes | mpSegmentFindTypes.UserSegments;
                        }
                        break;
                    case mpFolderTreeSearchContentTypes.StyleSheets:
                        if (m_iSearchUserID > 0)
                        {
                            //Only User Content will have an OwnerID
                            iTypes = mpSegmentFindTypes.UserStyleSheets;
                        }
                        else
                        {
                            if (m_bIncludeAdminContent)
                                iTypes = iTypes | mpSegmentFindTypes.StyleSheets;
                            if (m_bIncludeUserContent)
                                iTypes = iTypes | mpSegmentFindTypes.UserStyleSheets;
                        }
                        break;
                    case mpFolderTreeSearchContentTypes.SavedData:
                        iTypes = mpSegmentFindTypes.Prefills;
                        break;
                    case mpFolderTreeSearchContentTypes.SegmentPackets:
                        if (m_iSearchUserID > 0)
                        {
                            //Only User Content will have an OwnerID
                            iTypes = mpSegmentFindTypes.UserSegmentPackets;
                        }
                        else
                        {
                            if (m_bIncludeAdminContent)
                                iTypes = iTypes | mpSegmentFindTypes.SegmentPackets;
                            if (m_bIncludeUserContent)
                                iTypes = iTypes | mpSegmentFindTypes.UserSegmentPackets;
                        }

                        break;
                    case mpFolderTreeSearchContentTypes.MasterData:
                        iTypes = mpSegmentFindTypes.MasterData;
                        //GLOG 8163
                        if ((m_iDisplayOptions & mpFolderTreeViewOptions.MasterData) != mpFolderTreeViewOptions.MasterData)
                        {
                            //GLOG 8163: If MasterData not allowed, set dummy text so there will be no matches
                            m_xAdvancedFilterText = "123zzmpzzmpzzmpzzmpzzmpz9999";
                        }
                        break;
                    case mpFolderTreeSearchContentTypes.AllContent:
                        if (m_iSearchUserID > 0)
                        {
                            //Only User Content with have an OwnerID
                            iTypes = mpSegmentFindTypes.UserSegments | mpSegmentFindTypes.UserStyleSheets | mpSegmentFindTypes.UserSegmentPackets | mpSegmentFindTypes.Prefills;
                        }
                        else
                        {
                            if (m_bIncludeUserContent && !m_bIncludeAdminContent)
                                iTypes = mpSegmentFindTypes.UserSegments | mpSegmentFindTypes.UserStyleSheets | mpSegmentFindTypes.UserSegmentPackets | mpSegmentFindTypes.Prefills;
                            if (m_bIncludeAdminContent && !m_bIncludeUserContent)
                            {
                                iTypes = mpSegmentFindTypes.AdminSegments | mpSegmentFindTypes.StyleSheets | mpSegmentFindTypes.SegmentPackets;
                                //GLOG 8163: Display MasterData only if configured
                                if ((m_iDisplayOptions & mpFolderTreeViewOptions.MasterData) == mpFolderTreeViewOptions.MasterData)
                                    iTypes = iTypes | mpSegmentFindTypes.MasterData;
                            }
                        }
                        break;
                }
                m_aFindResults = Data.Application.GetSegmentFindResults(m_xAdvancedFilterText, m_iOwnerID, mpSegmentFindMode.MatchSubString, 
                    iTypes, m_iSearchFields, m_iSearchUserID, m_xSearchFolderList);

                if (m_aFindResults == null || m_aFindResults.Count == 0)
                {
                    m_xAdvancedFilterText = "";
                    m_aFindResults = null;
                    this.Mode = mpFolderTreeViewModes.FolderTree;
                }
                else
                {
                    this.Mode = mpFolderTreeViewModes.AdvancedFindResults;
                }
            }
            get { return m_xAdvancedFilterText; }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        public mpFolderTreeViewModes Mode
        {
            set
            {
                //Don't switch to FindResults mode if no filter specified
                //GLOG : 8348 : jsw
                if ((string.IsNullOrEmpty(m_xFilterText) && value == mpFolderTreeViewModes.FindResults) || 
                    (string.IsNullOrEmpty(m_xAdvancedFilterText) && value == mpFolderTreeViewModes.AdvancedFindResults))
                    value = mpFolderTreeViewModes.FolderTree;

                if (m_iMode != value)
                {
                    m_iMode = value;
                    ExecuteFinalSetup();
                }
            }
            get { return m_iMode; }
        }
        public bool ShowFindPaths
        {
            set { m_bShowFindPaths = value; }
            get { return m_bShowFindPaths; }
        }
        //GLOG : 8152 : jsw
        public bool ShowTopUserFolderNode
        {
            set { m_bShowTopUserFolderNode = value; }
            get { return m_bShowTopUserFolderNode; }
        }
        
        /// <summary>
        /// gets/sets whether the background of the
        /// control should be transparent
        /// </summary>
        public bool TransparentBackground
        {
            get { return this.Appearance.BackColor == System.Drawing.Color.Transparent; }
            set
            {
                if (value)
                    this.Appearance.BackColor = System.Drawing.Color.Transparent;
                else
                    this.Appearance.BackColor = System.Drawing.Color.White;
            }
        }
        #endregion
        #region ******************Methods**********************
        /// <summary>
        /// Setup control after properties have been set
        /// </summary>
        public void ExecuteFinalSetup()
        {
            if (!this.DesignMode)
            {
                SetupComponent();
            }
        }
        /// <summary>
        /// Fills FolderList ArrayList
        /// If ShowCheckboxes==false, list will have at most 1 item with selected folder
        /// </summary>
        private void PopulateFolderList()
        {
            //Clear list
            m_aFolderList = new ArrayList();
            if (m_bShowCheckboxes)
            {
                foreach (UltraTreeNode oNode in this.Nodes)
                {
                    GetCheckedNodes(oNode, true);
                }
            }
            else
            {
                //No folders selected
                if (this.SelectedNodes.Count == 0)
                    return;
                else if (this.SelectedNodes[0].Tag is UserFolder)
                {
                    //Selected node is a UserFolder
                    UserFolder oFolder = (UserFolder)this.SelectedNodes[0].Tag;
                    ArrayList aNew = new ArrayList();
                    m_aFolderList.Add(new ArrayList(new object[] { oFolder.ID, oFolder.Name, mpFolderTypes.User }));
                }
                else if (this.SelectedNodes[0].Tag is Folder)
                {
                    //Selected node is an AdminFolder
                    Folder oFolder = (Folder)this.SelectedNodes[0].Tag;
                    m_aFolderList.Add(new ArrayList(new object[] { oFolder.ID, oFolder.Name, mpFolderTypes.Admin }));
                }
                //GLOG #3331 - account for selection of a folder member
                else if (this.SelectedNodes[0].Tag is FolderMember)
                {
                    //add parent folder
                    UserFolder oFolder = (UserFolder)this.SelectedNodes[0].Parent.Tag;
                    m_aFolderList.Add(new ArrayList(new object[] { oFolder.ID, oFolder.Name, mpFolderTypes.Admin }));
                }
                else
                {
                    //Selected node is not a folder
                    return;
                }
            }
        }
        /// <summary>
        /// Fills MemberList ArrayList
        /// If ShowCheckboxes==false, list will have at most 1 item with selected member
        /// </summary>
        private void PopulateMemberList()
        {
            //Clear list
            m_aFolderMemberList = new ArrayList();
            if (m_bShowCheckboxes)
            {
                foreach (UltraTreeNode oNode in this.Nodes)
                {
                    GetCheckedNodes(oNode, true);
                }
            }
            else
            {
                //No items selected
                if (this.SelectedNodes.Count == 0)
                    return;
                else if (this.SelectedNodes[0].Tag is FolderMember)
                {
                    //Selected node is a UserFolder
                    FolderMember oMember = (FolderMember)this.SelectedNodes[0].Tag;
                    m_aFolderMemberList.Add(new ArrayList(new object[] { oMember.ObjectID1 + "." + oMember.ObjectID2, oMember.Name, oMember.ObjectTypeID }));
                }
                else if (this.SelectedNodes[0].Tag is object[]) //Selected node is a Find Result
                {
                    object[] aMember = (object[])this.SelectedNodes[0].Tag;
                    m_aFolderMemberList.Add(new ArrayList(new object[] { aMember[1], aMember[0], (mpFolderMemberTypes)int.Parse(aMember[2].ToString()) }));
                }
                else
                {
                    //Selected node is not a FolderMember
                    return;
                }
            }
        }
        /// <summary>
        /// Recurse through tree nodes, populating Folder and/or Member List with checked items
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bIncludeChildren"></param>
        private void GetCheckedNodes(UltraTreeNode oNode, bool bIncludeChildren)
        {
            foreach (UltraTreeNode oChild in oNode.Nodes)
            {
                //only included checkbox nodes that are checked
                if (oChild.NodeStyleResolved == NodeStyle.CheckBox &&
                    oChild.CheckedState == System.Windows.Forms.CheckState.Checked)
                {
                    if (this.DisplayOptions < mpFolderTreeViewOptions.Segments)
                    {
                        //Only Folders are displayed, get checked Folders
                        if (oChild.Tag is UserFolder &&
                            ((this.DisplayOptions & mpFolderTreeViewOptions.UserFolders) == mpFolderTreeViewOptions.UserFolders))
                        {
                            //Selected node is a UserFolder
                            UserFolder oFolder = (UserFolder)oChild.Tag;
                            m_aFolderList.Add(new ArrayList(new object[] { oFolder.ID, oFolder.Name, mpFolderTypes.User }));
                        }
                        else if (oChild.Tag is Folder &&
                            ((this.DisplayOptions & mpFolderTreeViewOptions.AdminFolders) == mpFolderTreeViewOptions.UserFolders))
                        {
                            //Selected node is an AdminFolder
                            Folder oFolder = (Folder)oChild.Tag;
                            m_aFolderList.Add(new ArrayList(new object[] { oFolder.ID, oFolder.Name, mpFolderTypes.Admin }));
                        }
                    }
                    else
                    {
                        //Folders and Members are displayed, get checked FolderMembers
                        if (oChild.Tag is FolderMember)
                        {
                            FolderMember oMember = (FolderMember)oChild.Tag;
                            if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet &&
                                ((this.DisplayOptions & mpFolderTreeViewOptions.Prefills) == mpFolderTreeViewOptions.Prefills))
                            {
                                m_aFolderMemberList.Add(new ArrayList(new object[] { oMember.ObjectID1 + "." + oMember.ObjectID2, oMember.Name, oMember.ObjectTypeID }));
                            }
                            else if (oMember.ObjectTypeID == mpFolderMemberTypes.UserSegment &&
                                ((this.DisplayOptions & mpFolderTreeViewOptions.UserFolders) == mpFolderTreeViewOptions.UserFolders))
                            {
                                m_aFolderMemberList.Add(new ArrayList(new object[] { oMember.ObjectID1 + "." + oMember.ObjectID2, oMember.Name, oMember.ObjectTypeID }));
                            }
                            else if (oMember.ObjectTypeID == mpFolderMemberTypes.AdminSegment &&
                                ((this.DisplayOptions & mpFolderTreeViewOptions.AdminFolders) == mpFolderTreeViewOptions.AdminFolders))
                            {
                                m_aFolderMemberList.Add(new ArrayList(new object[] { oMember.ObjectID1 + "." + oMember.ObjectID2, oMember.Name, oMember.ObjectTypeID }));
                            }

                        }
                    }
                }
                if (bIncludeChildren)
                    GetCheckedNodes(oChild, true);
            }
        }
        private void SetupComponent()
        {
            if (!this.DesignMode)
            {
                if (((m_iDisplayOptions & mpFolderTreeViewOptions.AdminFolders) != mpFolderTreeViewOptions.AdminFolders) &&
                    ((m_iDisplayOptions & mpFolderTreeViewOptions.UserFolders) != mpFolderTreeViewOptions.UserFolders) &&
                    ((m_iDisplayOptions & mpFolderTreeViewOptions.SharedFolders) != mpFolderTreeViewOptions.SharedFolders))
                {
                    throw new LMP.Exceptions.PropertyException(LMP.Resources.GetLangString("Error_FolderTree_MustDisplayFolder"));
                }
                //this.Override.NodeAppearance.Image = Images.FolderClosed;
                //this.Override.ExpandedNodeAppearance.Image = Images.FolderOpen;
                this.Override.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
                this.Override.NodeSpacingAfter = 2;
                this.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;
                //this.ShowRootLines = false;
                this.Indent = 3;
                RefreshTopLevelNodes();

                m_bControlIsLoaded = true;
            }
        }
        /// <summary>
        /// refreshes top level nodes in tree
        /// </summary>
        private void RefreshTopLevelNodes()
        {
            this.SuspendLayout();
            //display top level nodes - first clear tree
            this.Nodes.Clear();

            if (this.Mode != mpFolderTreeViewModes.FindResults & this.Mode != mpFolderTreeViewModes.AdvancedFindResults) //GLOG : 8348 : jsw
            {
                if ((this.DisplayOptions & mpFolderTreeViewOptions.UserFolders) == mpFolderTreeViewOptions.UserFolders)
                {
                    //Don't display root node if only a single folder type is being displayed
                    //GLOG : 8152 : jsw
                    //add option to show user folder node
                    if ((this.DisplayOptions & mpFolderTreeViewOptions.AdminFolders) == 0 &&
                        (this.DisplayOptions & mpFolderTreeViewOptions.SharedFolders) == 0 && !this.ShowTopUserFolderNode)
                    {
                        RefreshUserFolderNode(null, true);
                        if (m_iExpansion == mpFolderTreeViewExpansion.ExpandAll)
                            this.ExpandAll(ExpandAllType.Always);
                    }
                    else
                    {
                        //Add Top node of My Folders
                        UltraTreeNode oTreeNode = this.Nodes.Add("0.0", LMP.Resources.GetLangString("Prompt_MyFolders"));
                        oTreeNode.Tag = (object)"0.0";
                        oTreeNode.Override.NodeAppearance.Image = Images.FolderClosed;
                        oTreeNode.Override.ExpandedNodeAppearance.Image = Images.FolderOpen;
                        if (m_iExpansion == mpFolderTreeViewExpansion.ExpandAll)
                            oTreeNode.ExpandAll(ExpandAllType.Always);
                        else if (m_iExpansion == mpFolderTreeViewExpansion.ExpandTopLevel)
                            oTreeNode.Expanded = true;

                        if (oTreeNode.Nodes.Count == 0 && m_iExpansion > 0)
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                        else
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;
                    }
                }

                if ((this.DisplayOptions & mpFolderTreeViewOptions.AdminFolders) == mpFolderTreeViewOptions.AdminFolders)
                {
                    //Don't display root node if only a single folder type is being displayed
                    if ((this.DisplayOptions & mpFolderTreeViewOptions.UserFolders) == 0 &&
                        (this.DisplayOptions & mpFolderTreeViewOptions.SharedFolders) == 0)
                    {

                        RefreshAdminFolderNode(null, true, 0);
                        if (m_iExpansion == mpFolderTreeViewExpansion.ExpandAll)
                            this.ExpandAll(ExpandAllType.Always);
                    }
                    else
                    {
                        //Add Top node of Public Folders
                        UltraTreeNode oTreeNode = this.Nodes.Add("0", LMP.Resources.GetLangString("Prompt_PublicFolders"));
                        oTreeNode.Tag = (object)0;

                        oTreeNode.Override.NodeAppearance.Image = Images.FolderClosed;
                        oTreeNode.Override.ExpandedNodeAppearance.Image = Images.FolderOpen;
                        if (m_iExpansion == mpFolderTreeViewExpansion.ExpandAll)
                            oTreeNode.ExpandAll(ExpandAllType.Always);
                        else if (m_iExpansion == mpFolderTreeViewExpansion.ExpandTopLevel)
                            oTreeNode.Expanded = true;

                        if (oTreeNode.Nodes.Count == 0 && m_iExpansion > 0)
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                        else
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;
                    }
                }


                if ((this.DisplayOptions & mpFolderTreeViewOptions.SharedFolders) == mpFolderTreeViewOptions.SharedFolders)
                {
                    //Don't display root node if only a single folder type is being displayed
                    if ((this.DisplayOptions & mpFolderTreeViewOptions.UserFolders) == 0 &&
                        (this.DisplayOptions & mpFolderTreeViewOptions.AdminFolders) == 0)
                    {

                        RefreshAdminFolderNode(null, true, -2);
                        if (m_iExpansion == mpFolderTreeViewExpansion.ExpandAll)
                            this.ExpandAll(ExpandAllType.Always);
                    }
                    else
                    {
                        //Add Top node of Shared Folders
                        UltraTreeNode oTreeNode = this.Nodes.Add("-2", LMP.Resources.GetLangString("Prompt_SharedFolders"));
                        oTreeNode.Tag = (object)-2;
                        oTreeNode.Override.NodeAppearance.Image = Images.FolderClosed;
                        oTreeNode.Override.ExpandedNodeAppearance.Image = Images.FolderOpen;
                        if (m_iExpansion == mpFolderTreeViewExpansion.ExpandAll)
                            oTreeNode.ExpandAll(ExpandAllType.Always);
                        else if (m_iExpansion == mpFolderTreeViewExpansion.ExpandTopLevel)
                            oTreeNode.Expanded = true;

                        if (oTreeNode.Nodes.Count == 0 && m_iExpansion > 0)
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                        else
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Always;
                    }
                }
            }
            else
            {
                RefreshSearchResults();
            }
            this.ResumeLayout();
        }
        private void RefreshSearchResults()
        {
            if (m_aFindResults != null && m_aFindResults.Count > 0)
            {
                this.SuspendLayout();
                try
                {
                    this.Nodes.Clear();
                    //Add top-level Search Results node
                    UltraTreeNode oTopNode = this.Nodes.Add(
                    "0", LMP.Resources.GetLangString("Prompt_SearchResults"));
                    oTopNode.Tag = (object)"-1";
                    oTopNode.Override.ExpandedNodeAppearance.Image = Images.SearchResults; // .SearchResults;
                    oTopNode.Expanded = true;
                    oTopNode.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                    oTopNode.Override.NodeSpacingBefore = 5;
                    oTopNode.Override.SelectionType = SelectType.None;
                    oTopNode.Override.NodeDoubleClickAction = NodeDoubleClickAction.None;
                    for (int i = 0; i < m_aFindResults.Count; i++)
                    {
                        // Use DisplayName for Node Text, ID for Key and Tag
                        object[] aFindResult = (object[])m_aFindResults[i];
                        string xName = aFindResult[0].ToString();
                        string xID = @aFindResult[1].ToString();
                        mpFolderMemberTypes shMemberType = (mpFolderMemberTypes)(int.Parse(aFindResult[2].ToString()));
                        mpSegmentIntendedUses iUse = (mpSegmentIntendedUses)(int.Parse(aFindResult[3].ToString()));
                        //add node to tree
                        UltraTreeNode oTreeNode = null;
                        switch (shMemberType)
                        {
                            case mpFolderMemberTypes.UserSegment:
                                //UserSegment
                                if ((this.DisplayOptions & mpFolderTreeViewOptions.UserFolders) == mpFolderTreeViewOptions.UserFolders)
                                {
                                    //GLOG 6208: Can be UserSegment or Style Sheet
                                    if ((this.DisplayOptions & mpFolderTreeViewOptions.StyleSheets) == 0 && iUse == mpSegmentIntendedUses.AsStyleSheet)
                                        // DisplayOptions don't include Style Sheets
                                        continue;
                                    else if ((this.DisplayOptions & mpFolderTreeViewOptions.UserSegments) == 0 && iUse != mpSegmentIntendedUses.AsStyleSheet)
                                        // DisplayOptions don't include UserSegments
                                        continue;
                                    oTreeNode = this.Nodes[0].Nodes.Add("U" + xID, xName);
                                    oTreeNode.Tag = aFindResult;
                                    oTreeNode.Override.NodeAppearance.Image = this.GetImageForFolderMember(iUse, mpFolderMemberTypes.UserSegment);
                                    oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImageForFolderMember(iUse, mpFolderMemberTypes.UserSegment);
                                }
                                break;
                            case mpFolderMemberTypes.VariableSet:
                                //VariableSet/Prefill
                                if ((this.DisplayOptions & mpFolderTreeViewOptions.Prefills) == mpFolderTreeViewOptions.Prefills)
                                {
                                    oTreeNode = this.Nodes[0].Nodes.Add("P" + xID, xName);
                                    oTreeNode.Tag = aFindResult;
                                    oTreeNode.Override.NodeAppearance.Image = this.GetImageForFolderMember(iUse, mpFolderMemberTypes.VariableSet);
                                    oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImageForFolderMember(iUse, mpFolderMemberTypes.VariableSet);
                                }
                                break;
                            default:
                                //AdminSegment
                                oTreeNode = this.Nodes[0].Nodes.Add("A" + xID, xName);
                                oTreeNode.Tag = aFindResult;
                                oTreeNode.Override.NodeAppearance.Image = this.GetImageForFolderMember(iUse, mpFolderMemberTypes.AdminSegment);
                                oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImageForFolderMember(iUse, mpFolderMemberTypes.AdminSegment);
                                break;
                        }
                        if (oTreeNode != null)
                        {
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            oTreeNode.Override.CellClickAction = CellClickAction.Default;
                            oTreeNode.Override.NodeDoubleClickAction = NodeDoubleClickAction.None;
                        }
                    }
                    oTopNode.ExpandAll();
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
                finally
                {
                    this.ResumeLayout();
                }
            }
        }
        /// <summary>
        /// Populates User Folder node with child folders and segments
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh"></param>
        private void RefreshUserFolderNode(UltraTreeNode oNode, bool bForceRefresh)
        {
            try
            {
                this.SuspendLayout();
                int iID1, iID2;
                if (oNode == null)
                {
                    if (this.Nodes.Count > 0 && !bForceRefresh)
                        return;
                    else if (bForceRefresh)
                        this.Nodes.Clear();
                    iID1 = 0;
                    iID2 = 0;
                }
                else
                {
                    //node has already been populated
                    if (oNode.HasNodes == true && !bForceRefresh)
                        return;
                    // clear nodes before refreshing
                    if (oNode.HasNodes)
                        oNode.Nodes.Clear();
                    string xParentID;
                    if (oNode.Tag is string)
                    {
                        xParentID = "0.0";
                        iID1 = 0;
                        iID2 = 0;
                    }
                    else if (oNode.Tag is UserFolder)
                    {
                        xParentID = ((UserFolder)oNode.Tag).ID.ToString();
                        LMP.String.SplitID(xParentID, out iID1, out iID2);
                    }
                    else
                        return;
                }
                UserFolders oUserFolders = new UserFolders(this.OwnerID, iID1, iID2);
                ArrayList oDisplay = oUserFolders.ToDisplayArray();
      
                for (int i = 0; i < oDisplay.Count; i++)
                {
                    string xName = ((Object[])oDisplay[i])[0].ToString();
                    string xID = ((Object[])oDisplay[i])[1].ToString();
                    UserFolder oFolder = (UserFolder)oUserFolders.ItemFromID(xID);
                    //add node to tree
                    UltraTreeNode oTreeNode = null;
                    if (oNode == null)
                        //Add at top level
                        oTreeNode = this.Nodes.Add(xID, xName);
                    else
                        //Add as child node
                        oTreeNode = oNode.Nodes.Add(xID, xName);
                    oTreeNode.Override.NodeAppearance.Image = Images.FolderClosed;
                    oTreeNode.Override.ExpandedNodeAppearance.Image = Images.FolderOpen;
                    oTreeNode.Tag = oFolder;
                }
                // GLOG : 3071 : JAB
                // Add case for mpFolderTreeViewOptions.UserSegments.
                if ((this.DisplayOptions & mpFolderTreeViewOptions.Prefills) == mpFolderTreeViewOptions.Prefills ||
                    (this.DisplayOptions & mpFolderTreeViewOptions.Segments) == mpFolderTreeViewOptions.Segments ||
                    (this.DisplayOptions & mpFolderTreeViewOptions.UserSegments) == mpFolderTreeViewOptions.UserSegments ||
                    (this.DisplayOptions & mpFolderTreeViewOptions.StyleSheets) == mpFolderTreeViewOptions.StyleSheets ||
                    (this.DisplayOptions & mpFolderTreeViewOptions.DesignerSegments) == mpFolderTreeViewOptions.DesignerSegments ||
                    (this.DisplayOptions & mpFolderTreeViewOptions.SegmentPackets) == mpFolderTreeViewOptions.SegmentPackets) //GLOG 8413 //GLOG : 8972 : jsw
                {
                    FolderMembers oMembers = new FolderMembers(iID1, iID2);
                    oDisplay = oMembers.ToDisplayArray();
                    int iCount = oDisplay.Count;
                    for (int i = 0; i < iCount; i++)
                    {
                        string xName = ((Object[])oDisplay[i])[0].ToString();
                        string xID = ((Object[])oDisplay[i])[1].ToString();
                        mpFolderMemberTypes shMemberType = (mpFolderMemberTypes)((int)((Object[])oDisplay[i])[2]);
                        mpSegmentIntendedUses shIntendedUse = (mpSegmentIntendedUses)((int)((Object[])oDisplay[i])[3]);
                        //add node to tree
                        if (shMemberType == mpFolderMemberTypes.VariableSet &&
                            ((this.DisplayOptions & mpFolderTreeViewOptions.Prefills) == mpFolderTreeViewOptions.Prefills))
                        {
                            FolderMember oMember = (FolderMember)oMembers.ItemFromID(xID);

                            ISegmentDef oDef = GetSegmentDefFromFolderMember(oMember);

                            //skip this item if there's no definition 
                            //valid segment def associated with it
                            if (oDef == null)
                            {
                                // Variable set with no segment id are valid members to be added to the tree.

                                if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
                                {
                                    VariableSetDef oSetDef = GetVariableSetForFolderMember(oMember);

                                    if (oSetDef.SegmentID != "")
                                    {
                                        // This is not a variable set with no segment id and will not be added
                                        // to the tree.
                                        continue;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }

                            //add node to tree
                            UltraTreeNode oTreeNode = oNode.Nodes.Add("P" + xID, xName);
                            oTreeNode.Tag = oMember;

                            oTreeNode.Override.NodeAppearance.Image = GetImageForFolderMember(oMember);
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            //Display checkboxes if appropriate
                            if (m_bShowCheckboxes)
                                oTreeNode.Override.NodeStyle = NodeStyle.CheckBox;
                            else
                                oTreeNode.Override.NodeStyle = NodeStyle.Standard;
                        }
                        // GLOG : 3071 : JAB
                        // Handle displaying user segments.
                        //GLOG 6208:  Only display Style Sheets and Segment Packets if configured
                        else if (shMemberType == mpFolderMemberTypes.UserSegment &&
                            ((((this.DisplayOptions & mpFolderTreeViewOptions.UserSegments) == mpFolderTreeViewOptions.UserSegments) &&
                            shIntendedUse != mpSegmentIntendedUses.AsStyleSheet && shIntendedUse != mpSegmentIntendedUses.AsAnswerFile) ||
                            (((this.DisplayOptions & mpFolderTreeViewOptions.StyleSheets) == mpFolderTreeViewOptions.StyleSheets) &&
                            shIntendedUse == mpSegmentIntendedUses.AsStyleSheet) || 
                            (((this.DisplayOptions & mpFolderTreeViewOptions.SegmentPackets) == mpFolderTreeViewOptions.SegmentPackets) &&
                            shIntendedUse == mpSegmentIntendedUses.AsAnswerFile)))
                        {
                            //GLOG : 3071 : CEH
                            FolderMember oMember = (FolderMember)oMembers.ItemFromID(xID);

                            ISegmentDef oDef = GetSegmentDefFromFolderMember(oMember);

                            //skip this item if it's a Style Sheet 
                            //GLOG : 7767 : JSW
                            //if StyleSheets is included in the display settings, User StyleSheet members should also display
                            //so I'm remming out the code to exclude them
                            //if (oDef.IntendedUse == mpSegmentIntendedUses.AsStyleSheet)
                            //{
                            //    continue;
                            //}  

                            //add node to tree
                            UltraTreeNode oTreeNode = oNode.Nodes.Add("U" + xID, xName);
                            oTreeNode.Tag = oMembers.ItemFromID(xID);
                            oTreeNode.Override.NodeAppearance.Image = GetImageForFolderMember((FolderMember)oTreeNode.Tag);
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            //Display checkboxes if appropriate
                            if (m_bShowCheckboxes)
                                oTreeNode.Override.NodeStyle = NodeStyle.CheckBox;
                            else
                                oTreeNode.Override.NodeStyle = NodeStyle.Standard;
                        }
                        // GLOG : 6255 : ceh
                        // Handle displaying user segments packets.
                        else if (shMemberType == mpFolderMemberTypes.UserSegmentPacket &&
                            ((((this.DisplayOptions & mpFolderTreeViewOptions.SegmentPackets) == mpFolderTreeViewOptions.SegmentPackets) &&
                            shIntendedUse == mpSegmentIntendedUses.AsAnswerFile)))
                        {
                            //add node to tree
                            UltraTreeNode oTreeNode = oNode.Nodes.Add("U" + xID, xName);
                            oTreeNode.Tag = oMembers.ItemFromID(xID);
                            oTreeNode.Override.NodeAppearance.Image = GetImageForFolderMember((FolderMember)oTreeNode.Tag);
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            //Display checkboxes if appropriate
                            if (m_bShowCheckboxes)
                                oTreeNode.Override.NodeStyle = NodeStyle.CheckBox;
                            else
                                oTreeNode.Override.NodeStyle = NodeStyle.Standard;
                        }
                        //GLOG 6208:  Only display Style Sheets if configured
                        else if (shMemberType == mpFolderMemberTypes.AdminSegment &&
                            ((((this.DisplayOptions & mpFolderTreeViewOptions.Segments) == mpFolderTreeViewOptions.Segments) &&
                            shIntendedUse != mpSegmentIntendedUses.AsStyleSheet && shIntendedUse != mpSegmentIntendedUses.AsAnswerFile) || 
                            (((this.DisplayOptions & mpFolderTreeViewOptions.StyleSheets) == mpFolderTreeViewOptions.StyleSheets) &&
                            shIntendedUse == mpSegmentIntendedUses.AsStyleSheet) ||
                            (((this.DisplayOptions & mpFolderTreeViewOptions.SegmentPackets) == mpFolderTreeViewOptions.SegmentPackets) &&
                            shIntendedUse == mpSegmentIntendedUses.AsAnswerFile)))
                        {
                            //add node to tree
                            UltraTreeNode oTreeNode = oNode.Nodes.Add("U" + xID, xName);
                            oTreeNode.Tag = oMembers.ItemFromID(xID);
                            oTreeNode.Override.NodeAppearance.Image = GetImageForFolderMember((FolderMember)oTreeNode.Tag);
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            //Display checkboxes if appropriate
                            if (m_bShowCheckboxes)
                                oTreeNode.Override.NodeStyle = NodeStyle.CheckBox;
                            else
                                oTreeNode.Override.NodeStyle = NodeStyle.Standard;
                        }
						//GLOG 8413
                        else if (shMemberType == mpFolderMemberTypes.UserSegment &&
                            ((((this.DisplayOptions & mpFolderTreeViewOptions.DesignerSegments) == mpFolderTreeViewOptions.DesignerSegments) &&
                            shIntendedUse != mpSegmentIntendedUses.AsStyleSheet && shIntendedUse != mpSegmentIntendedUses.AsAnswerFile)))
                        {
                            FolderMember oMember = (FolderMember)oMembers.ItemFromID(xID);

                            ISegmentDef oDef = GetSegmentDefFromFolderMember(oMember);

                            if (oDef is UserSegmentDef && oDef.TypeID != mpObjectTypes.UserSegment)
                            {
                                //add node to tree
                                UltraTreeNode oTreeNode = oNode.Nodes.Add("U" + xID, xName);
                                oTreeNode.Tag = oMembers.ItemFromID(xID);
                                oTreeNode.Override.NodeAppearance.Image = GetImageForFolderMember((FolderMember)oTreeNode.Tag);
                                oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                                //Display checkboxes if appropriate
                                if (m_bShowCheckboxes)
                                    oTreeNode.Override.NodeStyle = NodeStyle.CheckBox;
                                else
                                    oTreeNode.Override.NodeStyle = NodeStyle.Standard;
                            }
                        }

                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }
            finally
            {
                this.ResumeLayout();
            }
        }

        private void RefreshAdminFolderNode(UltraTreeNode oNode, bool bForceRefresh)
        {
            RefreshAdminFolderNode(oNode, bForceRefresh, -9999);
        }
        private void RefreshAdminFolderNode(UltraTreeNode oNode, bool bForceRefresh, int iTopLevelParent)
        {
            try
            {
                this.SuspendLayout();
                int iParentID;
                if (oNode == null)
                {
                    //Adding to top level of tree
                    if (this.Nodes.Count > 0 && !bForceRefresh)
                        return;
                    else if (bForceRefresh)
                        this.Nodes.Clear();
                    iParentID = iTopLevelParent;
                }
                else
                {
                    //node has already been populated
                    if (oNode.HasNodes == true && !bForceRefresh)
                        return;
                    // clear nodes before refreshing
                    if (oNode.HasNodes)
                        oNode.Nodes.Clear();
                    if (oNode.Tag is int)
                    {
                        iParentID = (int)oNode.Tag;
                    }
                    else if (oNode.Tag is Folder)
                    {
                        iParentID = ((Folder)oNode.Tag).ID;
                    }
                    else
                        return;
                }
                Folders oFolders = new Folders(this.OwnerID, iParentID);
                ArrayList oDisplay = oFolders.ToDisplayArray();
                for (int i = 0; i < oDisplay.Count; i++)
                {
                    string xName = ((Object[])oDisplay[i])[0].ToString();
                    int iID = (int)(((Object[])oDisplay[i])[1]);
                    Folder oFolder = (Folder)oFolders.ItemFromID(iID);
                    //add node to tree
                    UltraTreeNode oTreeNode = null;
                    if (oNode == null)
                        oTreeNode = this.Nodes.Add(iID.ToString(), xName);
                    else
                        oTreeNode = oNode.Nodes.Add(iID.ToString(), xName);
                    oTreeNode.Override.NodeAppearance.Image = Images.FolderClosed;
                    oTreeNode.Override.ExpandedNodeAppearance.Image = Images.FolderOpen;
                    oTreeNode.Tag = oFolder;
                }
                if (((this.DisplayOptions & mpFolderTreeViewOptions.Segments) == mpFolderTreeViewOptions.Segments) ||
                    ((this.DisplayOptions & mpFolderTreeViewOptions.PreferenceSegmentsOnly) == mpFolderTreeViewOptions.PreferenceSegmentsOnly) ||
                    ((this.DisplayOptions & mpFolderTreeViewOptions.SegmentPackets) == mpFolderTreeViewOptions.SegmentPackets) ||
                    ((this.DisplayOptions & mpFolderTreeViewOptions.StyleSheets) == mpFolderTreeViewOptions.StyleSheets) ||
                    ((this.DisplayOptions & mpFolderTreeViewOptions.MasterData) == mpFolderTreeViewOptions.MasterData)) //GLOG 8163
                {
                    FolderMembers oMembers = new FolderMembers(iParentID);
                    oDisplay = oMembers.ToDisplayArray();
                    int iCount = oDisplay.Count;

                    //GLOG item #4242 - dcf
                    string xTypeExclusions = "," + LMP.Data.Application.GetMetadata("TypeExclusions") + ",";

                    for (int i = 0; i < iCount; i++)
                    {
                        //GLOG 6031: Display only Preference Segments
                        if (((m_iDisplayOptions & mpFolderTreeViewOptions.PreferenceSegmentsOnly) == mpFolderTreeViewOptions.PreferenceSegmentsOnly) &&
                            ((Object[])oDisplay[i])[3].ToString() != "-1")
                            continue;
                        //GLOG 8163: Skip MasterData if not configured
                        if (((m_iDisplayOptions & mpFolderTreeViewOptions.MasterData) != mpFolderTreeViewOptions.MasterData) &&
                            ((Object[])oDisplay[i])[4].ToString() == ((int)mpObjectTypes.MasterData).ToString())
                            continue;

                        bool bIsExcludedType = xTypeExclusions.Contains(
                            "," + ((object[])oDisplay[i])[4].ToString() + ",");

                        if (m_bExcludeNonMacPacTypes)
                        { 
                            //GLOG : 8380 : jsw
                            mpObjectTypes oTypeID = (mpObjectTypes)((Object[])oDisplay[i])[4];

                            if (oTypeID == mpObjectTypes.NonMacPacContent)
                                bIsExcludedType = true;
                        }

                        if (!bIsExcludedType)
                        {
                            string xName = ((Object[])oDisplay[i])[0].ToString();
                            string xID = ((Object[])oDisplay[i])[1].ToString();

                            //add node to tree
                            UltraTreeNode oTreeNode = oNode.Nodes.Add(@"A" + xID, xName);
                            oTreeNode.Tag = oMembers.ItemFromID(xID);
                            oTreeNode.Override.NodeAppearance.Image = GetImageForFolderMember((FolderMember)oTreeNode.Tag);
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            if (this.ShowCheckboxes)
                                oTreeNode.Override.NodeStyle = NodeStyle.CheckBox;
                            else
                                oTreeNode.Override.NodeStyle = NodeStyle.Standard;
                        }
                    }

                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }
            finally
            {
                this.ResumeLayout();
            }
        }
        /// <summary>
        /// Return iSegmentDef object corresponding to FolderMember Node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private ISegmentDef GetSegmentDefFromFolderMember(FolderMember oMember)
        {
            try
            {
                switch (oMember.ObjectTypeID)
                {
                    case mpFolderMemberTypes.AdminSegment:
                        {
                            AdminSegmentDefs oSegs = new AdminSegmentDefs();
                            AdminSegmentDef oSeg = (AdminSegmentDef)oSegs.ItemFromID(oMember.ObjectID1);
                            return (ISegmentDef)oSeg;
                        }
                    case mpFolderMemberTypes.UserSegment:
                        {
                            UserSegmentDefs oSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                            UserSegmentDef oSeg = (UserSegmentDef)oSegs.ItemFromID(oMember.ObjectID1.ToString()
                                + "." + oMember.ObjectID2.ToString());
                            return (ISegmentDef)oSeg;
                        }
                    case mpFolderMemberTypes.VariableSet:
                        {
                            VariableSetDef oSetDef = GetVariableSetForFolderMember(oMember);

                            if (oSetDef.SegmentID == "")
                            {
                                // This is a variable segment with no segment id.
                                return null;
                            }
                            else if (oSetDef.SegmentID.EndsWith(".0") || !oSetDef.SegmentID.Contains("."))
                            {
                                //GLOG 6966: Make sure string can be parsed as a Double even if
                                //the Decimal separator is something other than '.' in Regional Settings
                                string xSegmentID = oSetDef.SegmentID.Replace(".0", "");
                                //is admin segment
                                AdminSegmentDefs oSegs = new AdminSegmentDefs();
                                AdminSegmentDef oSeg = (AdminSegmentDef)oSegs.ItemFromID(
                                    Int32.Parse(xSegmentID));
                                return (ISegmentDef)oSeg;
                            }
                            else
                            {
                                //is user segment
                                UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, LMP.Data.Application.User.ID);
                                UserSegmentDef oDef = (UserSegmentDef) oDefs.ItemFromID(oSetDef.SegmentID);
                                return (ISegmentDef)oDef;
                            }
                        }
                    default:
                        return null;
                }
            }
            catch
            {
                return null;
            }
        }
        private Images GetImageForFolderMember(mpSegmentIntendedUses iUse, mpFolderMemberTypes iMemberType)
        {
            if (iMemberType == mpFolderMemberTypes.VariableSet)
            {
                if (iUse == mpSegmentIntendedUses.AsAnswerFile)
                    return Images.AnswerFile;
                else
                    return Images.Prefill;
            }
            else
            {
                switch (iUse)
                {
                    case mpSegmentIntendedUses.AsDocument:
                        return iMemberType == mpFolderMemberTypes.UserSegment ? Images.UserDocument : Images.DocumentSegment;
                    case mpSegmentIntendedUses.AsDocumentComponent:
                        return Images.ComponentSegment;
                    case mpSegmentIntendedUses.AsParagraphText:
                        return iMemberType == mpFolderMemberTypes.UserSegment ? Images.UserParagraphText : Images.ParagraphTextSegment;
                    case mpSegmentIntendedUses.AsSentenceText:
                        return iMemberType == mpFolderMemberTypes.UserSegment ? Images.UserSentenceText : Images.SentenceTextSegment;
                    case mpSegmentIntendedUses.AsStyleSheet:
                        // GLOG : 3041 : JAB
                        // Return image index for user styles sheet segments.
                        return Images.StyleSheetSegment;
                    case mpSegmentIntendedUses.AsAnswerFile:
                        // GLOG : 3041 : JAB
                        // Return image index for user styles sheet segments.
                        return iMemberType == mpFolderMemberTypes.UserSegment ? Images.UserAnswerFileSegment : Images.AnswerFile;
                    case mpSegmentIntendedUses.AsMasterDataForm:
                        //GLOG 8163
                        return Images.MasterDataForm;
                    default:
                        return Images.DocumentSegment;
                }
            }
        }
        /// <summary>
        /// returns an image for the specified Folder Member
        /// </summary>
        /// <param name="iIntendedUse"></param>
        /// <returns></returns>
        private Images GetImageForFolderMember(FolderMember oMember)
        {
            LMP.Trace.WriteNameValuePairs("oMember.Name", oMember.Name);

            ISegmentDef oDef = GetSegmentDefFromFolderMember(oMember);
            if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
            {
                //Variable Set may reference Data Interview or Standalone segment
                if (oDef == null)
                {
                    // The oDef can be null because this variable set has no segment id.
                    return Images.Prefill;
                }
                else
                {
                    // GLOG : 3176 : JAB
                    // Use the appropriate icon for user answer file segment.
                    if (oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                        return Images.UserAnswerFileSegment;
                    else
                        return Images.UserPrefill;
                }
            }
            else if (oMember.ObjectTypeID == mpFolderMemberTypes.UserSegmentPacket)
            {
                return Images.AnswerFile;
            }
            else
            {
                //GLOG : 8363 : JSW
                string xFullFileName = "";
                string xFileExtension = "";

                if (oDef.TypeID == mpObjectTypes.NonMacPacContent)
                {
                    // The file name of the external content is stored as a non-xml within the def's XML property.
                    xFullFileName = oDef.XML;
                    xFileExtension = System.IO.Path.GetExtension(xFullFileName);

                    if (xFileExtension.StartsWith(".xls") || xFileExtension.StartsWith(".xlt"))
                    {
                        // Excel file extensions get an XL image.
                        return Images.XLDocument;
                    }
                    //GLOG : 8065 : jsw
                    if (xFileExtension.ToLower().StartsWith(".doc") || xFileExtension.ToLower().StartsWith(".dot"))
                    {
                        // Word files extensions get a Word image.
                        return Images.WordDocument;
                    }

                    // All other types of external content files get an external content image.
                    return Images.ExternalContent;
                }

                
                //get icon based on intended use
                switch (oDef.IntendedUse)
                {
                    case mpSegmentIntendedUses.AsDocument:
                        // GLOG : 3176 : JAB
                        // Use appropriate content for user content.
                        //GLOG 8413: Display different image for Designer Segment
                        return oMember.ObjectTypeID == mpFolderMemberTypes.UserSegment && oDef.TypeID == mpObjectTypes.UserSegment ? Images.UserDocument : Images.DocumentSegment;
                    case mpSegmentIntendedUses.AsDocumentComponent:
                        return Images.ComponentSegment;
                    case mpSegmentIntendedUses.AsParagraphText:
                        // GLOG : 3176 : JAB
                        // Use appropriate content for user content.
                        //GLOG 8413: Display different image for Designer Segment
                        return oMember.ObjectTypeID == mpFolderMemberTypes.UserSegment && oDef.TypeID == mpObjectTypes.UserSegment ? Images.UserParagraphText : Images.ParagraphTextSegment;
                    case mpSegmentIntendedUses.AsSentenceText:
                        //GLOG 8413: Display different image for Designer Segment
                        return oMember.ObjectTypeID == mpFolderMemberTypes.UserSegment && oDef.TypeID == mpObjectTypes.UserSegment ? Images.UserSentenceText : Images.SentenceTextSegment;
                    case mpSegmentIntendedUses.AsStyleSheet:
                        // GLOG : 3176 : JAB
                        // Use appropriate content for user content.
                        return oMember.ObjectTypeID == mpFolderMemberTypes.UserSegment ? Images.UserStyleSheet : Images.StyleSheetSegment;
                    case mpSegmentIntendedUses.AsAnswerFile:
                        // GLOG : 3176 : JAB
                        // Use appropriate content for user content.
                        return oMember.ObjectTypeID == mpFolderMemberTypes.UserSegment ? Images.UserAnswerFileSegment : Images.AnswerFile;
                    case mpSegmentIntendedUses.AsMasterDataForm:
                        //GLOG : 8363 : JSW
                        return Images.MasterDataForm;

                    default:
                        return Images.DocumentSegment;
                }
            }
        }
        /// <summary>
        /// Return iSegmentDef object corresponding to FolderMember Node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private VariableSetDef GetVariableSetForFolderMember(FolderMember oMember)
        {
            try
            {
                if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
                {
                    VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                    VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(oMember.ObjectID1.ToString()
                        + "." + oMember.ObjectID2.ToString());
                    return oDef;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        #endregion
        #region *******************Events**********************
        private void FolderTreeView_BeforeExpand(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
        {
            try
            {
                if (this.Mode == mpFolderTreeViewModes.FolderTree)
                {
                    // Expand node if it's an Admin or User folder
                    if (e.TreeNode.Tag is Folder || e.TreeNode.Tag is int)
                        RefreshAdminFolderNode(e.TreeNode, true);
                    else if (e.TreeNode.Tag is UserFolder || e.TreeNode.Tag is string)
                        RefreshUserFolderNode(e.TreeNode, true);
                    else e.Cancel = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        //GLOG : 15806 : jsw
        private void FolderTreeView_BeforeCollapse(object sender, CancelableNodeEventArgs e)
        {
            if (e.TreeNode.Selected == false)
            {
                e.TreeNode.Selected = true;
            }

        }
        /// <summary>
        /// Selection has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FolderTreeView_AfterSelect(object sender, SelectEventArgs e)
        {
            this.IsDirty = true;
        }
        /// <summary>
        /// Checked State of a node has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FolderTreeView_AfterCheck(object sender, NodeEventArgs e)
        {
            this.IsDirty = true;
        }

        private void FolderTreeView_AfterActivate(object sender, NodeEventArgs e)
        {
            try
            {
                //Display path node for Segment in Find Results
                if (this.Mode == mpFolderTreeViewModes.FindResults && m_bShowFindPaths)
                {
                    if (e.TreeNode != null && e.TreeNode.Tag is object[] && e.TreeNode.Nodes.Count == 0)
                    {
                        UltraTreeNode oNode = e.TreeNode;
                        string[] aPaths = new string[0];
                        if (oNode.Key.StartsWith("P"))
                        {
                            aPaths = LMP.Data.UserFolder.GetSegmentFullPaths(oNode.Key, LMP.Data.Application.User.ID);
                        }
                        else if (oNode.Key.StartsWith("U"))
                        {
                            aPaths = LMP.Data.UserFolder.GetSegmentFullPaths(oNode.Key, LMP.Data.Application.User.ID);
                        }
                        else
                        {
                            aPaths = LMP.Data.Folder.GetSegmentPaths(oNode.Key);
                        }
                        foreach (string xSegmentPath in aPaths)
                        {
                            UltraTreeNode oSegmentPathNode = new UltraTreeNode();
                            oSegmentPathNode.Key = oNode.Key + ".Path." + xSegmentPath;
                            oSegmentPathNode.Text = xSegmentPath;
                            oSegmentPathNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            oSegmentPathNode.Enabled = false; //GLOG 6196
                            oNode.Nodes.Add(oSegmentPathNode);
                        }
                        //GLOG 6196: Override appearance of disabled node text
                        //TODO: Is there a way to prevent image being greyed out as well?
                        oNode.Nodes.Override.NodeAppearance.Image = Images.Bullet;
                        oNode.Nodes.Override.NodeAppearance.ImageAlpha = Infragistics.Win.Alpha.Opaque;
                        oNode.Nodes.Override.NodeAppearance.BackColorDisabled = this.Appearance.BackColor;
                        oNode.Nodes.Override.NodeAppearance.ForeColorDisabled = SystemColors.WindowText;
                        oNode.Expanded = true;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void FolderTreeView_DoubleClick(object sender, EventArgs e)
        {
            if (TreeDoubleClick != null)
            {
                TreeDoubleClick(this, new EventArgs());
            }
        }

        //GLOG : 6257 : CEH
        private void FolderTreeView_KeyDown(object sender, KeyEventArgs e)
        {
            if (TreeKeyDown != null)
            {
                TreeKeyDown(this, new KeyEventArgs(e.KeyCode));
            }
        }

        protected override void OnScroll(TreeScrollEventArgs e)
        {
            try
            {
                if (e.ScrollBarType == ScrollBarType.horizontal)
                {
                    UltraTreeNode oNode = this.GetNodeFromPoint(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);
                    if (oNode != null && oNode.Key.Contains(".Path."))
                    {
                        e.ScrollEventArgs.NewValue = e.ScrollEventArgs.OldValue;
                        return;
                    }
                }
                base.OnScroll(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        private void FolderTreeView_Scroll(object sender, TreeScrollEventArgs e)
        {
        }



    }
}
