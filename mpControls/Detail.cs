using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Text;
using LMP.Data;
using LMP.Controls;
using System.Xml;
using System.Reflection;
using TSG.CI;  //GLOG : 8819 : ceh

namespace LMP.Controls
{
    public delegate void DetailAddedHandler(int iIndex);
    public delegate void DetailDeletedHandler(int iIndex);
    public delegate void DetailEditedHandler(int iIndex);
    public delegate void SubControlChangedEventHandler(Control oControl);
    public delegate void SubControlKeyDownEventHandler(Control oControl, Keys oKey);
    public delegate void SubControlMouseDownEventHandler(Control oControl, int x, int y);

    /// <summary>
	/// Defines a MacPac detail control.
	/// Created by Dan Fisherman 2/05.
	/// </summary>
	public partial class Detail : System.Windows.Forms.UserControl, IControl, ICIable
	{
        //events
        public event DetailAddedHandler DetailAdded;
        public event DetailDeletedHandler DetailDeleted;
        public event DetailEditedHandler DetailEdited;
        public event System.EventHandler DetailsCleared;
        public event SubControlChangedEventHandler SubControlChanged;
        public event SubControlKeyDownEventHandler SubControlKeyDown;
        public event SubControlMouseDownEventHandler SubControlMouseDown;

        #region *********************fields*********************
        private const int mpDefLabelWidth = 85;
		private const int mpMaxDetailFields = 12; //GLOG 5134
		private const int mpRowHeight = 225;
		private const int mpGridLineHeight = 25;
        private const string mpDetail_DefaultConfig = "1�FULLNAME�&Name:�True��1�COREADDRESS�&Address:�True�4";

//        private byte m_bytDetailListThreshold = 0;
		private bool m_bControlIsLoaded = false;
		private byte m_bytFieldCount = 0;
		private string m_xConfigString = "";
		private IControl[] m_oControls = null;
		private Label[] m_oLabelControls = null;
        private bool m_bShowCIButton = false;
		private string[] m_aConfig = null;
		private bool m_bHidingControl = false;
		private int m_iLabelColumnWidth = mpDefLabelWidth;
        private System.Drawing.Color m_oFieldColor = System.Drawing.SystemColors.Window;
		private short m_shSelectedRow = 0;
		private ArrayList m_oDetailArray = new ArrayList();
		private DataSet m_oDetailDS = new DataSet("Details");
		private bool m_bDetailDirty = false;
        //private mpDetailButtons m_bytDetailButtons = mpDetailButtons.AddButton | 
        //    mpDetailButtons.DeleteButton | mpDetailButtons.ListButton;
		private string m_xCounterScheme = "Item %0 of %1";
		private Control m_oPopupSenderCtl = null;
        private bool m_bPopupVisible = false;

		//index of the item in m_oDetailArray that is currently selected
		private int m_iCurDetailIndex = -1;
        private int m_iMaxIndex = 0;
        private int m_iMinIndex = 0;
        private int m_iCounterIndex = 0;

		private string m_xContentString = "";
        private bool m_bIsDirty;
        private object m_oTag2;
        private bool m_bSubControlsConfigured = false;
        private bool m_bShortCutKeyPressed = false;
        private int m_iMaxEntities = 0;
        private bool m_bRemoveEmptyFields = true;
        private bool m_bCIRequested = false;
        private int m_iCountBeforeCI = 0;
        private string m_xSupportingValues = "";
        //GLOG 8098
        private string m_xTrueString = "Yes";
        private string m_xFalseString = "No";

        #endregion

        #region *********************enumerations*********************
        public enum mpDetailControlTypes : byte
        {
            TextBox = 1,
            ComboBox = 2,
            ListBox = 3,
            MultilineCombo = 4,
            BooleanCombo = 5
        }

        //TODO: Confirm removal of DetailButtons property
        //[Flags]
        //public enum mpDetailButtons : byte
        //{
        //    ListButton = 1,
        //    AddButton = 2,
        //    DeleteButton = 4
        //}

        public enum mpDetailFieldSelection : byte
        {
            WholeField = 0,
            StartOfField = 1,
            EndOfField = 2
        }
        #endregion

        #region *********************constructors*********************
        public Detail()
		{
			InitializeComponent();
            //make base control transparent, so that opening multiline combo list
            //won't extend visible control
            base.BackColor = Color.Transparent;
        }

		#endregion

		#region *********************properties*********************

        //[CategoryAttribute("Behavior")]
        //[DescriptionAttribute("Get/Sets the maximum number if list items to display in popup menu rather than scrollable list.")]
        //public byte DetailListThreshold
        //{
        //    get{return m_bytDetailListThreshold;}
        //    set{m_bytDetailListThreshold = value;}
        //}

		private bool DetailDirty
		{
			get{return m_bDetailDirty;}
			set{m_bDetailDirty = value;}
		}
        public override bool AutoSize
        {
            get
            {
                return base.AutoSize;
            }
            set
            {
                base.AutoSize = value;
                if (m_bControlIsLoaded)
                    SetControlSize();
            }
        }
        [CategoryAttribute("Behavior")]
        [DescriptionAttribute("Get/Sets the configuration string defining the sub-controls to be displayed.")]
        public string ConfigString
		{
			set
			{
				string xValue;

				//value cannot be empty
				if(value == "")
					xValue = mpDetail_DefaultConfig;
				else
					xValue = value;

				if(xValue != m_xConfigString)
				{
					m_aConfig = xValue.Split(LMP.StringArray.mpEndOfSubField);

					if((m_aConfig.Length % 5 != 0) || (m_aConfig.Length /5 > mpMaxDetailFields))
					{
						//wrong number of array elements - alert
						throw new LMP.Exceptions.DataException(
							LMP.Resources.GetLangString("Error_InvalidDetailConfigString") +
							xValue);
					}
					
					//get fields in detail control - there are
					//five parameters defining a field
					m_bytFieldCount = (byte)(m_aConfig.Length/5);

					m_xConfigString = xValue;

                    m_bSubControlsConfigured = false;

                    if (m_bControlIsLoaded)
					{
						RefreshSubControls();
						SetupSubControls();
					}
				}
			}

			get{return m_xConfigString;}
		}

        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets the text to display in the Counter panel.")]
        public string CounterScheme
		{
			get{return m_xCounterScheme;}
			set{m_xCounterScheme = value;}
		}
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets width of the area allocated to the controls labels.")]
        public int LabelColumnWidth
		{
			get{return m_iLabelColumnWidth;}
			set
			{
                //GLOG : 8532 : ceh
				m_iLabelColumnWidth = (int)(value * LMP.OS.GetScalingFactor());
				if(m_bControlIsLoaded)
					SetupControl();
			}
		}
        /// <summary>
        /// If true, omits empty SubVars from value XML
        /// </summary>
        public bool RemoveEmptyFields
        {
            get { return m_bRemoveEmptyFields; }
            set { m_bRemoveEmptyFields = value; }
        }
        //TODO: Confirm removal of DetailButtons property
        //[CategoryAttribute("Appearance")]
        //[DescriptionAttribute("Get/Sets which action buttons to display.")]
        //public mpDetailButtons DetailButtons
        //{
        //    get{return m_bytDetailButtons;}
        //    set
        //    {
        //        if(value != m_bytDetailButtons)
        //        {
        //            m_bytDetailButtons = value;
        //            if(m_bControlIsLoaded)
        //                SetupControl();
        //        }
        //    }
        //}

        [Browsable(false)]
        [DescriptionAttribute("Gets number the subcontrols that are defined.")]
        public byte FieldCount
		{
			get{return m_bytFieldCount;}
		}
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the XML string defining the detail items returned/displayed.")]
        private string ContentString
		{
			set
			{
				if(value != this.ContentString)
				{
					Trace.WriteNameValuePairs("value", value);
                    //value should contain valid xml
                    if (!(value.Contains("<") && value.Contains(">")))
                    {
                        m_xContentString = "";
                    }
                    else
                    {
                        //Extract just XML portion of string
                        int iStart = value.IndexOf("<");
                        int iEnd = value.LastIndexOf(">") + 1;
                        m_xContentString = value.Substring(iStart, iEnd - iStart);
                    }

					if(!m_bControlIsLoaded)
						return;

					if(!this.DesignMode)
					{
                        PopulateDetails();
					}
				}
			}

			get
			{
				if(!this.DesignMode)
					m_xContentString = ToXML();
                Trace.WriteNameValuePairs("m_xContentString", m_xContentString);
				return m_xContentString;
			}

		}
        
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets background color used for each subcontrol.")]
        public System.Drawing.Color FieldBackColor
		{
			get{return m_oFieldColor;}
			set
			{
				m_oFieldColor = value;
				if(m_bControlIsLoaded)
				{
					foreach(Control oCtl in m_oControls)
						oCtl.BackColor = value;
				}
			}
		}
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets the background color for the control labels.")]
        public new System.Drawing.Color BackColor
        {
            get
            {
                return pnlFields.BackColor;
            }
            set
            {
                this.pnlFields.BackColor = value;
            }
        }
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Get/Sets the font used in the control.")]
        public override Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
				if(m_bControlIsLoaded)
				{
					foreach(Control oCtl in m_oControls)
						oCtl.Font = value;
				}
			}
		}

        public int MaxEntities
        {
            get
            {
                return m_iMaxEntities;
            }

            set
            {
                if (m_iMaxEntities != value)
                {
                    m_iMaxEntities = value;
                    if (m_bControlIsLoaded)
                        UpdateMaxEntitiesView();
                }
            }
        }
        //GLOG 8098: Allow customization of True/False
        //values in BooleanCombo details
        public string TrueString
        {
            get
            {
                return m_xTrueString;
            }
            set
            {
                if (value != "" && m_xTrueString != value)
                {
                    m_xTrueString = value;
                }
            }
        }
        public string FalseString
        {
            get
            {
                return m_xFalseString;
            }
            set
            {
                if (value != "" && m_xFalseString != value)
                {
                    m_xFalseString = value;
                }
            }
        }
        #endregion

		#region *********************methods*********************
        /// <summary>
        /// Set text of interface items
        /// </summary>
        private void SetupResourceStrings()
        {
            LMP.Trace.WriteInfo("SetupResourceStrings");
            tbtnPrev.ToolTipText = LMP.Resources.GetLangString("Prompt_PreviousEntry") + " [PgUp]";
            tbtnNext.ToolTipText = LMP.Resources.GetLangString("Prompt_NextEntry") + " [PgDn]";
            tbtnAdd.ToolTipText = LMP.Resources.GetLangString("Prompt_AddNewEntry") + " [Ctrl+Shift+N]";
            tbtnDelete.ToolTipText = LMP.Resources.GetLangString("Prompt_DeleteCurrentEntry") + " [Ctrl+Shift+Del]";
            tbtnContacts.ToolTipText = LMP.Resources.GetLangString("Prompt_RetrieveContacts") + " [F2]";
            tddbStatus.ToolTipText = LMP.Resources.GetLangString("Prompt_CurrentEntry");
            if (!this.DesignMode)
            {
                mnuContextMenu_AddDetail.Text = LMP.Resources.GetLangString("Menu_Detail_AddItem");
                mnuContextMenu_ClearField.Text = LMP.Resources.GetLangString("Menu_Detail_ClearField");
                //GLOG : 6297 : CEH
                mnuContextMenu_Cut.Text = LMP.Resources.GetLangString("Menu_Detail_Cut");
                mnuContextMenu_Copy.Text = LMP.Resources.GetLangString("Menu_Detail_Copy");
                mnuContextMenu_Paste.Text = LMP.Resources.GetLangString("Menu_Detail_Paste");
                mnuContextMenu_DeleteAll.Text = LMP.Resources.GetLangString("Menu_Detail_DeleteAll");
                mnuContextMenu_DeleteDetail.Text = LMP.Resources.GetLangString("Menu_Detail_DeleteItem");
                mnuContextMenu_RefreshAll.Text = LMP.Resources.GetLangString("Menu_Detail_RefreshAll");
                mnuContextMenu_RefreshDetail.Text = LMP.Resources.GetLangString("Menu_Detail_RefreshItem");
            }
            // GLOG : 3485 : JAB
            // Set up tool tips for the move up/down button
            this.tbtnMoveUp.ToolTipText = LMP.Resources.GetLangString("Prompt_MoveEntryUp");
            this.tbtnMoveDown.ToolTipText = LMP.Resources.GetLangString("Prompt_MoveEntryDown");
        }

        /// <summary>
		/// resets all detail fields
		/// </summary>
		public void ClearDetail()
		{
            LMP.Trace.WriteInfo("ClearDetail");
            foreach(IControl oCtl in m_oControls)
			{
                if (oCtl is BooleanComboBox)
                    oCtl.Value = "False";
                else
                    oCtl.Value = "";
			}
			
			this.DetailDirty = false;
		}

        private void PopulateDetails()
        {
            LMP.Trace.WriteInfo("PopulateDetails");
            //populate detail array
            FillDetailArray();

            if (m_oDetailArray.Count > 0)
            {
                m_iCurDetailIndex = 0;

                //update detail content
                FillDetail();

                m_iMaxIndex = m_oDetailArray.Count;
                m_iCounterIndex = 1;
                //update count
                UpdateCount();

            }
            else
            {
                m_iCurDetailIndex = -1;
                m_iMaxIndex = 1;

                this.DetailDirty = false;
                UpdateCount();
                this.tbtnAdd.Enabled = !this.MaxEntitiesReached;
                ClearDetail();
            }
        }

		/// <summary>
		/// deletes all items
		/// </summary>
		public void DeleteAll()
		{
			m_oDetailArray.Clear();
			m_iCurDetailIndex = -1;

			ClearDetail();

			m_iMaxIndex = 1;

			this.DetailDirty = false;
            this.IsDirty = true;

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());

			UpdateCount();
            this.tbtnAdd.Enabled = !this.MaxEntitiesReached;

			if(DetailsCleared != null)
				DetailsCleared(this, new System.EventArgs());
		}
        private void UpdateMaxEntitiesView()
        {
            int iNumEntries = m_oDetailArray.Count;

            if(MaxEntities == 1)
            {
                // MaxEntities is constrained to 1. Disable navigation, add, deletes, etc.
                this.tbtnPrev.Visible = this.tbtnNext.Visible = false;
                this.tbtnAdd.Visible = false;
                this.tbtnDelete.Visible = true;
                this.tddbStatus.Visible = false;
                this.tSep1.Visible = false;
                this.tSep2.Visible = false;
                this.tSep3.Visible = false;
                this.tSep4.Visible = false;
                this.tSep5.Visible = false;
                this.tSep6.Visible = false;
                this.tSep7.Visible = false;
                this.tbtnMoveUp.Visible = false;
                this.tbtnMoveDown.Visible = false;
            }
            else
            {
                // MaxEntities is either zero (unlimited) or sum number greater than 1. Allow navigation, add, delete etc.
                this.tbtnPrev.Visible = true;
                this.tbtnNext.Visible = true;
                this.tbtnAdd.Visible = true;
                this.tbtnDelete.Visible = true;
                this.tddbStatus.Visible = true;
                this.tSep1.Visible = true;
                this.tSep2.Visible = true;
                this.tSep3.Visible = true;
                this.tSep4.Visible = true;
                this.tSep5.Visible = true;
                this.tSep6.Visible = true;
                this.tSep7.Visible = true;
                this.tbtnMoveUp.Visible = true;
                this.tbtnMoveDown.Visible = true;
            }
            
            // Enable the controls depending on if the number of entries is reached.
            this.tbtnPrev.Enabled = m_iCounterIndex > 1;
            this.tbtnNext.Enabled = m_iCounterIndex < iNumEntries;

            this.tbtnAdd.Enabled = !this.MaxEntitiesReached;
            this.tbtnDelete.Enabled = iNumEntries > 0;

            // GLOG : 3485 : JAB
            // Enable the move up/down buttons appropriately.
            this.UpdateMoveButtonsView();
        }
		#endregion

		#region *********************internal procedures*********************
        /// <summary>
        /// returns true iif the max number of 
        /// entities in the grid has been reached
        /// </summary>
        public bool MaxEntitiesReached
        {
            get
            {
                //max has been reached if control has a max, 
                //and either the currently edited entry is the max,
                //or the number of entries is the max
                return MaxEntities == 0 ? false :
                    (m_iCounterIndex >= MaxEntities || m_oDetailArray.Count >= MaxEntities);
            }
        }
		/// <summary>
		/// populates control from XML
		/// </summary>
		private void FillDetailArray()
		{
            LMP.Trace.WriteInfo("FillDetailArray");
            //clear detail list
			m_oDetailArray.Clear();

			if(m_xContentString != "")
			{

                //repopulate detail array
				XmlDocument oXML = new System.Xml.XmlDocument();
				oXML.LoadXml(string.Concat("<zzmpD>", m_xContentString, "</zzmpD>"));

                XmlNodeList oNodeList;
                int iIndex = 0;
                do
                {
                    iIndex ++;
                    oNodeList = oXML.DocumentElement.SelectNodes(
                        "(/zzmpD/*|/zzmpD/*)[@Index='" + iIndex.ToString() + "']");
                    if (oNodeList.Count > 0)
                    {
                        string[] aVals = new string[m_bytFieldCount + 1];

                        //get UNID, if one exists
                        string xUNID = "";
                        try
                        {
                            xUNID= oNodeList[0].Attributes.GetNamedItem("UNID").Value;
                        }
                        catch { }

                        aVals[0] = xUNID != "" ? xUNID : "0";

                        string xItem = "";

                        for (int i = 0; i < oNodeList.Count; i++)
                        {
                            string xField = oNodeList[i].Name;
                            string xValue = oNodeList[i].InnerText;
                            int iFieldIndex = -1; //GLOG 7895

                            //GLOG : 5686 : CEH
                            //if FieldCount is 1, then all node values go into one field
                            if (this.FieldCount == 1)
                            {
                                if (xValue != "")
                                {
                                    //xItem = xItem != "" ? aVals[1] + ((char)11).ToString() + xValue : xValue;
                                    //GLOG 8238: Lines should be separated by Carriage Return/Linefeed rather than Ascii 11
                                    xItem = xItem != "" ? aVals[1] + "\r\n" + xValue : xValue;
                                    aVals[1] = xItem;
                                }
                            }
                            else
                            {
                                //GLOG 7895: There may be more fields in saved data than are defined for control
                                if (i < m_bytFieldCount && this.GetControlFieldName(i) == xField)
                                    iFieldIndex = i + 1;
                                else
                                {
                                    for (int j = 0; j < m_bytFieldCount; j++)
                                    {
                                        if (this.GetControlFieldName(j) == xField)
                                        {
                                            iFieldIndex = j + 1;
                                            break;
                                        }
                                    }
                                }
                                //GLOG 7895: If there's no match for field name in Detail,
                                //we don't want to add to array
                                if (iFieldIndex > -1)
                                    aVals[iFieldIndex] = xValue;
                            }
                            
                        }

                        //TODO: code to refresh empty fields from UNID
                        //add item array to array of detail
                        m_oDetailArray.Add(aVals);
                    }
                }while (oNodeList.Count > 0);

                this.UpdateMaxEntitiesView();
                //string xFirstField = this.GetControlFieldName(m_oControls[0]);
                
                ////Get list of nodes for first field
                ////The indices used by these will be used to retrieve remaining fields
                //XmlNodeList oNodeList = oXML.DocumentElement.SelectNodes(string.Concat("(/zzmpD/", xFirstField, "|/zzmpD/", xFirstField.ToUpper(), ")"));

                ////cycle through details in XML, adding each to detail array
                //foreach(XmlNode oNode in oNodeList)
                //{
                //    string[] aVals = new string[m_bytFieldCount + 1];

                //    //get UNID, if one exists
                //    string xUNID = oNode.Attributes.GetNamedItem("UNID").Value;
                //    aVals[0] = xUNID != "" ? xUNID : "0";
                    
                //    // Get Index from attribute
                //    string xIndex = oNode.Attributes.GetNamedItem("Index").Value;
                //    aVals[1] = oNode.InnerText;

                //    // Get remaining fields with same index
                //    for(int i = 1; i < m_bytFieldCount; i++)
                //    {
                //        string xField = this.GetControlFieldName(m_oControls[i]);
                //        string xCITokenString = this.GetControlCIScheme(m_oControls[i]);

                //        //select node for named property
                //        XmlNode oFieldNode = oXML.SelectSingleNode(
                //            string.Concat("(/zzmpD/", xField, "|/zzmpD/", xField.ToUpper(), ")[@Index='", xIndex, "']"));

                //        if(oFieldNode != null)
                //            //property is store in XML
                //            aVals[i + 1] = oFieldNode.InnerText;
                //        //TODO: code to refresh details from UNIDs
                //        //else if(xUNID != "0" && xUNID != "" && xCITokenString != "" && oCISession != null)
                //            //property isn't in current XML -
                //            //get from CI
                //        //	aVals[i + 1] = Dialog.GetCIDetail(xUNID, xCITokenString, oCISession);
                //        else
                //            aVals[i + 1] = "";
                //    }

                //    //add item array to array of detail
                //    m_oDetailArray.Add(aVals);
                //}
			}
		}
					
		/// <summary>
		/// returns an XML string representing the detail items in the control
		/// </summary>
		/// <returns></returns>
		private string ToXML()
		{
            SaveCurDetail();
            if(m_oDetailArray.Count == 0)
				return "";

			//cycle through details, creating XML 
			//elements with attribute for CI ID
			XmlDocument oXML = new XmlDocument();

			//create temporary top level node
			oXML.LoadXml("<zzmpD></zzmpD>");

			int i = 0;
			foreach(string[] aVals in m_oDetailArray)
			{
				i++;
                string xUNID = aVals[0] != null ? aVals[0] : "0";

				for(int j = 1; j <= m_bytFieldCount; j++)
				{
                    if (string.IsNullOrEmpty(aVals[j]) && m_bRemoveEmptyFields)
                        //Don't include empty elements in XML
                        continue;
                    XmlNode oFieldNode = oXML.CreateNode(XmlNodeType.Element,
						this.GetControlFieldName(j - 1), "");
					if(aVals[j] == null)
						oFieldNode.InnerText = "";
					else
                        oFieldNode.InnerText = LMP.String.ReplaceXMLChars(aVals[j]);

                    XmlAttribute oA = oXML.CreateAttribute("Index");
                    oA.InnerText = i.ToString();
                    //store Index as attribute of node
                    oFieldNode.Attributes.Append(oA);

                    oA = oXML.CreateAttribute("UNID");
                    oA.InnerText = xUNID;
                    //store UNID as attribute of node
                    oFieldNode.Attributes.Append(oA);

                    oXML.DocumentElement.AppendChild(oFieldNode);
				}
			}

			string xItems = oXML.OuterXml;
			xItems = xItems.Replace("<zzmpD>", "");
			xItems = xItems.Replace("</zzmpD>", "");
			xItems = xItems.TrimEnd('\r', '\n');
			return xItems;
		}

		/// <summary>
		/// returns the field name of the control with the specified index
		/// </summary>
		/// <param name="oCtl"></param>
		/// <returns></returns>
		private string GetControlFieldName(int iIndex)
		{
			return GetControlFieldName(m_oControls[iIndex]);
		}

		/// <summary>
		/// returns the CI scheme of the control with the specified index
		/// </summary>
		/// <param name="oCtl"></param>
		/// <returns></returns>
		private string GetControlCIScheme(int iIndex)
		{
			return GetControlCIScheme(m_oControls[iIndex]);
		}

		/// <summary>
		/// returns the field name of the specified control
		/// </summary>
		/// <param name="oCtl"></param>
		/// <returns></returns>
		private string GetControlFieldName(IControl oICtl)
		{
			if(((Control)oICtl).Tag.ToString() == "")
				return "";

			string xTag = ((Control)oICtl).Tag.ToString();
			int iPos = xTag.IndexOf(LMP.StringArray.mpEndOfSubField);

			if(iPos == 0)
				throw new LMP.Exceptions.ControlTagException(
					LMP.Resources.GetLangString("Error_InvalidControlTag") + xTag);
			
			return xTag.Substring(0, iPos);
		}			
		/// <summary>
		/// returns the CI scheme of the specified control
		/// </summary>
		/// <param name="oCtl"></param>
		/// <returns></returns>
		private string GetControlCIScheme(IControl oICtl)
		{
            if (((Control)oICtl).Tag.ToString() == "")
				return "";

            string xTag = ((Control)oICtl).Tag.ToString();
			int iPos = xTag.IndexOf(LMP.StringArray.mpEndOfSubField);

			if(iPos == 0)
				throw new LMP.Exceptions.ControlTagException(
					LMP.Resources.GetLangString("Error_InvalidControlTag") + xTag);
			
			return xTag.Substring(iPos + 1);
		}

		/// <summary>
		/// returns true iff control is in add mode
		/// </summary>
		private bool InAddMode
		{
			get
			{
				bool bInAddMode = (m_oDetailArray.Count <= m_iCurDetailIndex) ||
					(m_iCurDetailIndex == -1 && this.DetailDirty);

				Trace.WriteNameValuePairs("bInAddMode", bInAddMode);

				return bInAddMode;
			}
		}

		/// <summary>
		/// saves the current detail
		/// </summary>
		private void SaveCurDetail()
		{
			bool bNew = false;

			Trace.WriteNameValuePairs("m_iCurDetailIndex", m_iCurDetailIndex);

			if(m_shSelectedRow < 0)
				return;
			else if(!this.InAddMode && !this.DetailDirty)
				return;
			else if(this.InAddMode && this.DetailDirty)
			{
				//add a row to detail array
				m_oDetailArray.Add(new string[m_bytFieldCount + 1]);

				m_iCurDetailIndex = m_oDetailArray.Count - 1;

				//mark for later that this is a new record
				bNew = true;
			}
			else if(this.InAddMode && !this.DetailDirty)
			{
				//record is empty, nothing to add
				m_iCurDetailIndex--;
                m_iCounterIndex--;

				m_iMaxIndex = m_oDetailArray.Count;
				FillDetail();
				UpdateCount();

				this.DetailDirty = false;
				this.tbtnAdd.Enabled = !this.MaxEntitiesReached;
				return;
			}

			//save detail
			string[] aVals = (string[])m_oDetailArray[m_iCurDetailIndex];

			//update detail array item with control values
			for(int j = 1; j <= m_bytFieldCount; j++)
			{
				aVals[j] = m_oControls[j - 1].Value;
			}

			this.DetailDirty = false;
            this.tbtnAdd.Enabled = !this.MaxEntitiesReached;

			m_iMaxIndex = m_oDetailArray.Count;

			if(bNew)
			{
				//record added - invoke event subscribers
				if(DetailAdded != null)
					DetailAdded(m_iCurDetailIndex);

				m_iCounterIndex = m_iCurDetailIndex + 1;
			}
			else
			{
				//record edited - invoke event subscribers
				if(DetailEdited != null)
					DetailEdited(m_iCurDetailIndex);
			}
            this.IsDirty = true;

            //notify that value has changed, if necessary
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
		}

		/// <summary>
		/// cycle through control array and reload defined CI
		/// properties for item or all based on saved UNIDs
		/// </summary>
		private void RefreshCIValues(int iIndex)
		{
			int iStart;
			int iEnd;
			bool bChanged = false;

			this.SaveCurDetail();
            if (!m_bShowCIButton)
                return;

			if(iIndex == -1)
			{
				//refresh all items
				iStart = 0;
				iEnd = m_oDetailArray.Count - 1;
			}
			else
			{
				//refresh item with specified index
				iStart = iIndex;
				iEnd = iIndex;
			}

			//cycle through specified details
			for(int i = iStart; i <= iEnd; i++)
			{
				bChanged = false;

				//get detail values for item
				string[] aVals = (string[]) m_oDetailArray[i];

				if(aVals[0] != "" && aVals[0] != null && aVals[0] != "0")
				{
					//item has a UNID - get contact
                    ICContact oC = Data.Application.CISession.GetContactFromUNID(aVals[0],
						ciRetrieveData.ciRetrieveData_All, ciAlerts.ciAlert_None);
                    
					if(oC != null)
					{
						//there is a contact - cycle through detail 
						//fields retrieving specified CI value
						for(int j = 1; j <= m_bytFieldCount; j++)
						{
							//get CI tokens
							string xCIScheme = this.GetControlCIScheme(m_oControls[j - 1]);

							if(System.Text.RegularExpressions.Regex.IsMatch(xCIScheme,"<.*>"))
							{
								//CI tokens exist
								string xVal =  Dialog.GetCIDetail(oC, xCIScheme.ToUpper());

								if(aVals[j] != xVal)
								{
									//new value is different
									aVals[j] = xVal;
									bChanged = true;
								}
							}
						}
					}
				}

				if(bChanged)
				{
					FillDetail();

					if(this.DetailEdited != null)
						this.DetailEdited(i);
				}
			}
		}

		/// <summary>
		/// deletes existing detail sub controls, then
		/// recreates them according to the current config string
		/// </summary>
		private void RefreshSubControls()
		{
            LMP.Trace.WriteInfo("RefreshSubControls");
            DateTime t0 = DateTime.Now;

			ClearSubControls();

			int iOffset = 0;

			//create new control array
			m_oControls = new IControl[m_bytFieldCount];
			m_oLabelControls = new Label[m_bytFieldCount];

			for(short sh = 1; sh <= m_bytFieldCount; sh++)
			{
                //create control from configuration parameters
				CreateControl((mpDetailControlTypes)Convert.ToByte(m_aConfig[iOffset]), 
					m_aConfig[iOffset + 1],
					m_aConfig[iOffset + 2], m_aConfig[iOffset + 3],
					m_aConfig[iOffset + 4], sh);

				iOffset = iOffset + 5;
			}

			if(!this.DesignMode)
				LMP.Benchmarks.Print(t0);
		}

		//removes all subcontrol-related controls -
		//called when config string is set
		private void ClearSubControls()
		{
            LMP.Trace.WriteInfo("ClearSubControls");
            if(m_oControls != null)
			{
				foreach(Control oCtl in m_oControls)
					this.pnlFields.Controls.Remove(oCtl);
				m_oControls = null;
			}

			if(m_oLabelControls != null)
			{
				foreach(Label oLbl in m_oLabelControls)
					this.pnlFields.Controls.Remove(oLbl);
				m_oLabelControls = null;
			}

			m_oLabelControls = null;
		}

		/// <summary>
		/// positions/sizes all subcontrols
		/// </summary>
		private void SetupControl()
		{
            LMP.Trace.WriteInfo("SetupControl");
            int iHeight = 0;

			if(!m_bControlIsLoaded || m_bHidingControl)
				return;

			DateTime t0 = DateTime.Now;

            if (m_xConfigString == "")
                this.ConfigString = mpDetail_DefaultConfig;
            else
            {
                //GLOG - 3263 - CEH
                //GLOG - 3613 - CEH
                if (!m_bSubControlsConfigured)
                    SetupSubControls();
                else this.SetControlSize();
            }

			iHeight = this.Height;
            
            this.UpdateMaxEntitiesView();

			if(!this.DesignMode)
				LMP.Benchmarks.Print(t0, this.GetType().FullName);
		}

		private void SetupSubControls()
		{
            LMP.Trace.WriteInfo("SetupSubControls");
            DateTime t0 = DateTime.Now;

			//don't setup if control is not loaded
			if(!m_bControlIsLoaded)
				return;

			//this.pnlFields.Width = this.Width;
            //this.pnlFields.BackColor = this.BackColor;

			int iIndex = 0;

			//create sub controls
			RefreshSubControls();

			//position detail controls
			for(int i = 0; i < m_oControls.Length; i++, iIndex++)
			{
				Control oCtl = (Control) m_oControls[i];

				if(iIndex == 0)
					//position control at the top
					oCtl.Top = 3;
				else
				{
					//position control after previous control
					Control oPrevCtl = (Control) m_oControls[iIndex - 1];
                    int iHeightOffset = Math.Max(oPrevCtl.Height, 13);
                    if (oPrevCtl is LMP.Controls.ComboBox)
                        oCtl.Top = oPrevCtl.Top + iHeightOffset + 3;
                    else if (oCtl is LMP.Controls.MultilineCombo)
                        oCtl.Top = oPrevCtl.Top + iHeightOffset + 3;
                    else
                        oCtl.Top = oPrevCtl.Top + iHeightOffset + 5;
                }

				//position/size control
                if (oCtl is ComboBox){
					oCtl.Left = this.LabelColumnWidth - 1;
                    oCtl.Width = this.pnlFields.Width - (oCtl.Left);
                }
                else if (oCtl is MultilineCombo)
                {
                    oCtl.Left = this.LabelColumnWidth + 1;
                    oCtl.Width = this.pnlFields.Width - (oCtl.Left + 1);
                }
                else
                {
                    oCtl.Left = this.LabelColumnWidth + 3;
                    oCtl.Width = this.pnlFields.Width - (oCtl.Left + 6);
                }

				//set background color and font
				oCtl.Font = this.Font;
                oCtl.TabStop = true;
                oCtl.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
				//position label control
				Label oLbl= (Label) m_oLabelControls[iIndex];
				if(oCtl is ComboBox)
					oLbl.Top = oCtl.Top + 2;
				else
					oLbl.Top = oCtl.Top;

				oLbl.Left = 1;
				oLbl.Height = oCtl.Height;
				oLbl.Width = this.LabelColumnWidth - 2;
				oLbl.TextAlign = ContentAlignment.TopLeft;
                oLbl.DoubleClick += new EventHandler(HandleDoubleClick);
			}

			this.txtStartDetail.TabIndex = 0;
			
			if(m_oControls.Length == 0)
				this.txtEndDetail.TabIndex = 1;
			else
				//set tab index to be after last sub control
				this.txtEndDetail.TabIndex = ((Control)m_oControls[m_bytFieldCount - 1]).TabIndex + 1;

            //if ((this.DetailButtons & mpDetailButtons.DeleteButton) > 0)
            //{
            //    this.tbtnDelete.Visible = true;
            //    this.tSep4.Visible = true;
            //}
            //else
            //{
            //    this.tbtnDelete.Visible = false;
            //    this.tSep4.Visible = false;
            //}

            //if ((this.DetailButtons & mpDetailButtons.AddButton) > 0)
            //{
            //    this.tbtnAdd.Visible = true;
            //    this.tSep3.Visible = true;
            //}
            //else
            //{
            //    this.tbtnAdd.Visible = false;
            //    this.tSep3.Visible = false;
            //}
            if (m_bShowCIButton)
            {
                this.tbtnContacts.Visible = true;
                this.tSep5.Visible = true;
            }
            else
            {
                this.tbtnContacts.Visible = false;
                this.tSep5.Visible = false;
            }

            SetControlSize();
            m_bSubControlsConfigured = true;
			//this.Refresh();
		}

		/// <summary>
		/// creates the specified control
		/// </summary>
		/// <param name="bytType"></param>
		/// <param name="xFieldName"></param>
		/// <param name="xCaption"></param>
		/// <param name="xCIScheme"></param>
		/// <param name="xSuppValue"></param>
		private void CreateControl(mpDetailControlTypes bytType, string xFieldName,
			string xCaption, string xCIEnabled, string xSuppValue, short iIndex)
		{
			DateTime t0 = DateTime.Now;
			Control oCtl = null;
            string xCIScheme = "";

			switch(bytType)
			{
				case mpDetailControlTypes.BooleanCombo:
				{
                    oCtl = this.CreateBooleanCombo();
                    break;
				}
				case mpDetailControlTypes.ComboBox:
				{
					oCtl = this.CreateCombo(false, xSuppValue);
					break;
				}
				case mpDetailControlTypes.ListBox:
				{
					oCtl = this.CreateCombo(true, xSuppValue);
					break;
				}
				case mpDetailControlTypes.TextBox:
				{
					oCtl = this.CreateTextBox(xSuppValue);
					break;
				}
				case mpDetailControlTypes.MultilineCombo:
				{
					oCtl = this.CreateMultilineCombo(xSuppValue);
					break;
				}
				default:
				{
					break;
				}
			}

			//get the index and name that the control will have
			oCtl.Name = "Control" + iIndex.ToString();
			oCtl.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            oCtl.BackColor = this.FieldBackColor;
            oCtl.ForeColor = this.ForeColor;
			//assign the control to the detail panel
			this.pnlFields.Controls.Add(oCtl);

			//create the associated label control
			Label oLabelCtl = new Label();
			oLabelCtl.Name = "LabelControl" + iIndex.ToString();
			this.pnlFields.Controls.Add(oLabelCtl);

			//set properties of label and control
			oLabelCtl.Visible = true;
            oLabelCtl.ForeColor = System.Drawing.Color.Black;
            oLabelCtl.BackColor = System.Drawing.Color.Transparent;

            oLabelCtl.Text = xCaption;
            oLabelCtl.BringToFront();
            if (!this.DesignMode)
            {
                oLabelCtl.TabIndex = 2 * iIndex;
            }


            // CI Detail token must be same as the Field Name
            if (xCIEnabled == "1" || xCIEnabled.ToUpper() == "TRUE")
            {
                xCIScheme = string.Concat("<", xFieldName, ">");
                m_bShowCIButton = true;
            }

            oCtl.Visible = true;
            oCtl.BringToFront();
            if (!this.DesignMode)
            {
                oCtl.TabIndex = 2 * iIndex + 1;
                oCtl.Tag = string.Concat(xFieldName,
                    LMP.StringArray.mpEndOfSubField, xCIScheme);
            }
            else
            {
                oCtl.Enabled = false;
            }

			//add control to control array
			m_oControls[iIndex - 1] = (IControl)oCtl;
			m_oLabelControls[iIndex - 1] = oLabelCtl;
		
			if(!this.DesignMode)
				LMP.Benchmarks.Print(t0, oCtl.GetType().FullName);
		}

        /// <summary>
        /// Resizes control to accommodate all subcontrols if AutoSize property is true
        /// </summary>
        private void SetControlSize()
        {
            LMP.Trace.WriteInfo("SetControlSize");
            if (this.AutoSize)
            {
                Control oLastCtl = (Control)(m_oControls[m_oControls.Length - 1]);
                int iHeight;
                iHeight = oLastCtl.Height;
                // Resize to accommodate subcontrols
                this.Height = oLastCtl.Top + iHeight + 5 + tsActions.Height;
                this.pnlFields.Height = this.Height - this.tsActions.Height;
                this.tsActions.Top = this.Height - this.tsActions.Height;
                //GLOG - 3263 - CEH
                this.pnlFields.Width = this.Width - 2;
                this.tsActions.Width = this.Width - 2;
            }
        }
        /// <summary>
        /// Creates a MultilineCombo for use as a sub control
        /// </summary>
        /// <param name="xListName"></param>
        /// <returns></returns>
        private LMP.Controls.MultilineCombo CreateMultilineCombo(string xListName)
		{
			MultilineCombo oMLC = new MultilineCombo(this);
			if(!this.DesignMode)
				oMLC.ListName = xListName;
            oMLC.ListRows = 10;
            // GLOG : 3201 : JAB
            // Adjust the height of the multiline combo control such 
            // that it always allows for two full lines of text to display.            
            oMLC.Height = 35;
			oMLC.ShowBorder = false;
            if (this.DesignMode)
                oMLC.Enabled = false;
			//sink events
			oMLC.TextChanged += new EventHandler(HandleTextChanged);
			oMLC.GotFocus += new EventHandler(HandleGotFocus);
			oMLC.KeyDown += new KeyEventHandler(HandleKeyDown);
			oMLC.MouseDown += new MouseEventHandler(HandleMouseDown);
            oMLC.TabPressed += new TabPressedHandler(InputField_TabPressed);
            oMLC.KeyReleased += new KeyEventHandler(HandleKeyReleased);
            oMLC.ListVisibleChanged += new EventHandler(oMLC_ListVisibleChanged);
            oMLC.DoubleClick += new EventHandler(HandleDoubleClick);
            return oMLC;
		}
        /// <summary>
        /// Dropdown list of Multiline combo is shown or hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oMLC_ListVisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (((MultilineCombo)sender).Visible)
                    m_bPopupVisible = true;
                else
                    m_bPopupVisible = false;
            }
            catch { }
        }

		/// <summary>
		/// creates a BooleanCombo for use as a sub control
		/// </summary>
		/// <returns></returns>
		private BooleanComboBox CreateBooleanCombo()
		{
			BooleanComboBox oChk = new BooleanComboBox();

            oChk.Borderless = true;
            oChk.TrueString = m_xTrueString; //GLOG 8098
            oChk.FalseString = m_xFalseString; //GLOG 8098
            oChk.ValueChanged += new ValueChangedHandler(HandleTextChanged);
            oChk.GotFocus += new EventHandler(HandleGotFocus);
            oChk.KeyDown += new KeyEventHandler(HandleKeyDown);
            oChk.MouseDown += new MouseEventHandler(HandleMouseDown);
            oChk.TabPressed +=new TabPressedHandler(InputField_TabPressed);
            oChk.KeyReleased += new KeyEventHandler(HandleKeyReleased);
            oChk.DoubleClick += new EventHandler(HandleDoubleClick);
            return oChk;
		}

        void HandleDoubleClick(object sender, EventArgs e)
        {
            try
            {
                base.OnDoubleClick(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

		/// <summary>
		/// creates a combobox for use as a sub control
		/// </summary>
		/// <param name="xListName"></param>
		/// <returns></returns>
		private LMP.Controls.ComboBox CreateCombo(bool bLimitToList, string xListName)
		{
			LMP.Controls.ComboBox oCombo = new ComboBox();

			oCombo.Borderless = true;
            if (!this.DesignMode)
            {
                oCombo.LimitToList = bLimitToList;
                oCombo.MaxDropDownItems = 10;
                oCombo.ListName = xListName;
                oCombo.Borderless = false;
            }
            else
            {
                oCombo.Enabled = false;
            }
            //oCombo.Value = "";

			//sink events
			oCombo.ValueChanged += new ValueChangedHandler(HandleTextChanged);
			oCombo.GotFocus += new EventHandler(HandleGotFocus);
			oCombo.KeyDown += new KeyEventHandler(HandleKeyDown);
			oCombo.MouseDown += new MouseEventHandler(HandleMouseDown);
            oCombo.TabPressed += new TabPressedHandler(InputField_TabPressed);
            oCombo.KeyReleased += new KeyEventHandler(HandleKeyReleased);
            oCombo.DoubleClick += new EventHandler(HandleDoubleClick);
			return oCombo;
		}

		/// <summary>
		/// creates a texbox for use as a subControl
		/// </summary>
		/// <param name="bytNumLines"></param>
		/// <returns></returns>
		private TextBox CreateTextBox(string xNumLines)
		{
			byte bytLines = 1;

			TextBox oTB = new TextBox();
			oTB.BorderStyle = BorderStyle.None;
            oTB.Width = 100;
			//set number of lines in the textbox
			if(xNumLines != null && xNumLines != "")
			{
				try
				{
					bytLines = Convert.ToByte(xNumLines);
				}
				catch(System.Exception oE)
				{
					throw new LMP.Exceptions.ArgumentException(
						LMP.Resources.GetLangString("Error_InvalidTextBoxLinesValue"), oE);
				}
			}

            if (bytLines > 1)
			{
                oTB.AcceptsReturn = true;
                oTB.Multiline = true;
				oTB.Height = bytLines * 13;
			}
            if (this.DesignMode)
                oTB.Enabled = false;

			//sink events
			oTB.TextChanged += new EventHandler(HandleTextChanged);
			oTB.GotFocus += new EventHandler(HandleGotFocus);
			oTB.KeyDown += new KeyEventHandler(HandleKeyDown);
			oTB.MouseDown += new MouseEventHandler(HandleMouseDown);
            oTB.TabPressed += new TabPressedHandler(InputField_TabPressed);
            oTB.EnterKeyPressed += new EnterKeyPressedHandler(ExecuteCodeForEnterPressed);
            oTB.KeyReleased += new KeyEventHandler(HandleKeyReleased);
			oTB.DoubleClick += new EventHandler(HandleDoubleClick);
            return oTB;
		}

        void HandleKeyReleased(object sender, KeyEventArgs e)
        {
            //Don't raise event if Ctrl ShortCut was used to prevent toggling hotkey mode
            if (this.KeyReleased != null && !m_bShortCutKeyPressed)
                this.KeyReleased(this, e);
        }

        private void SetupScrollButtons()
        {
            this.tbtnNext.Enabled = m_iCounterIndex < m_iMaxIndex && m_oDetailArray.Count > 0;
            this.tbtnPrev.Enabled = m_iCounterIndex > 1;
        }

        /// <summary>
        /// Clears current detail and places control in Add mode
        /// </summary>
		private void AddNew(bool bSaveCurrent)
		{
			Trace.WriteNameValuePairs("m_iCurDetailIndex", m_iCurDetailIndex);

            //Save dirty record if necessary
            if (bSaveCurrent)
                SaveCurDetail();

			m_iMaxIndex++;
			m_iCounterIndex = m_iMaxIndex;

			//set index to next available spot
			m_iCurDetailIndex = m_oDetailArray.Count;

            // GLOG : 6717 : CEH
            //don't clear detail & reset focus if MaxEntities has been reached
            //GLOG 7012: MaxEntities will be 0 if unlimited
            if (MaxEntities == 0 || m_oDetailArray.Count < MaxEntities)
            {
                ClearDetail();
                ((Control)m_oControls[0]).Focus();
            }

            // Disable tab stops so focus will leave control if tab is pressed without any changes
            EnableDetailViewTabStops(false, true, false);
			UpdateCount();
			this.DetailDirty = false;
            this.tbtnAdd.Enabled = !this.InAddMode && !this.MaxEntitiesReached;
            this.tbtnDelete.Enabled = true;
            if (MaxEntities == m_oDetailArray.Count)
            {
                //max has been reached
                if (this.TabPressed != null)
                {
                    this.TabPressed(this, new TabPressedEventArgs(false));
                }
            }
            else
            {
                this.UpdateMaxEntitiesView();
            }

            // GLOG : 3485 : JAB
            // Update the move up down button view.
            this.UpdateMoveButtonsView();
		}
        /// <summary>
        /// Tracks current and total number of details, 
        /// replacing {0} and {1} tokens in CounterScheme
        /// </summary>
		private void UpdateCount()
		{
			
            int iIndex = 0;
			int iTotal = 0;
			string xLabel = "";

			if(this.InAddMode)
			{
				//we're in the process of adding a detail -
				//the index is the last item in the array
				iIndex = m_oDetailArray.Count + 1;
				iTotal = iIndex;
			}
			else if(m_iCurDetailIndex > -1)
			{
				//there's a selected item
				iIndex = m_iCurDetailIndex + 1;
				iTotal = m_oDetailArray.Count;
			}
			else
			{
				//no selected item
				iTotal = m_oDetailArray.Count;
			}

			Trace.WriteNameValuePairs("iIndex", iIndex, "iTotal", iTotal);

			//set count and scroll bar position
			if(iIndex > 0 && iTotal > 0)
			{
                xLabel = this.CounterScheme;
                xLabel = xLabel.Replace("%0", iIndex.ToString());
                xLabel = xLabel.Replace("%1", iTotal.ToString());
			}
			else
				xLabel = "";
			this.tddbStatus.Text = "" + xLabel;
            SetupScrollButtons();
            this.tddbStatus.ShowDropDownArrow = (iTotal > 1);
		}


		/// <summary>
		/// sets values of subcontrols based on the current detail
		/// </summary>
		private void FillDetail()
		{
			LMP.Trace.WriteNameValuePairs("m_iCurDetailIndex", m_iCurDetailIndex);

            bool bIsDirty = this.IsDirty;
			if((m_iCurDetailIndex > -1) && (m_iCurDetailIndex < m_oDetailArray.Count))
			{
				//cycle through fields, setting control values
				for(int i=1; i <= m_bytFieldCount; i++)
					m_oControls[i-1].Value = ((string[])m_oDetailArray[m_iCurDetailIndex])[i];

				this.DetailDirty = false;
			}
            this.IsDirty = bIsDirty;
		}

		/// <summary>
		/// enables/disables tab stops in detail view -
		/// allows for cycling through details by tabbing,
		/// as well as tabbing out of the control
		/// </summary>
		/// <param name="bEnable"></param>
		private void EnableDetailViewTabStops(bool bEnableTab, bool bEnableShiftTab, bool bEnableControls)
		{
            LMP.Trace.WriteNameValuePairs("bEnableTab", bEnableTab, "bEnableShiftTab", bEnableShiftTab,
                "bEnableControls", bEnableControls);
            foreach(Control oCtl in m_oControls)
				oCtl.TabStop = bEnableControls;

			this.txtEndDetail.TabStop = bEnableTab;
            this.txtStartDetail.TabStop = bEnableShiftTab;
		}
		/// <summary>
		/// executes actions that must take place when entering a record
		/// </summary>
		private void EnterDetail()
		{
            LMP.Trace.WriteInfo("EnterDetail");
            if(this.DetailDirty)
				SaveCurDetail();

			if(m_iCounterIndex > 1)
			{
				//move to previous item
				m_iCurDetailIndex--;

				//fill controls with data from previous item
				FillDetail();

                m_iCounterIndex--;
                UpdateCount();

				//select last sub control
                ((Control)m_oControls[m_bytFieldCount - 1]).Focus();


			}
			else if(!this.InAddMode)
			{
                //if(m_iCurDetailIndex == 0)
                //    //we're at the top of the list - tab out
                //    SendKeys.Send("+{Tab}");
                //else
			    ((Control)m_oControls[0]).Focus();
			}
		}
        /// <summary>
        /// executes actions that must take place when leaving a record
        /// </summary>
		private void ExitDetail()
		{
            LMP.Trace.WriteInfo("ExitDetail");
            SaveCurDetail();

            if (m_iCounterIndex < m_oDetailArray.Count)
            {
                //move to next detail
                m_iCurDetailIndex++;
                FillDetail();
                m_iCounterIndex++;
                UpdateCount();
                ((Control)m_oControls[0]).Focus();

            }
            else
                this.AddNew(false);
		}

		/// <summary>
		/// executes code that must be run when "Enter" is pressed on the keyboard
		/// </summary>
		private void ExecuteCodeForEnterPressed(object sender, EnterKeyPressedEventArgs e)
		{
            try
            {
                Control oCurCtl = (Control)sender;
                if (!this.IsMultilineControl(oCurCtl))
                {
                    //move to next control-
                    //first enable tab stops so that 'Tab'
                    //tabs to next control
                    this.EnableDetailViewTabStops(true, true, true);
                    this.MoveToNextField(oCurCtl, false);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
		}

		/// <summary>
		/// moves the focus to the next detail -
		/// </summary>
		private void MoveToNextDetail()
		{
            LMP.Trace.WriteInfo("MoveToNextDetail");
            // give focus to picEndDetail
			this.txtEndDetail.Focus();
		}

		/// <summary>
		/// moves the focus to the next field-
		/// if the current focus is the last field, 
		/// moves focus to the first field of the next detail
		/// </summary>
		/// <param name="oControl"></param>
		/// <param name="bFirstChar"></param>
		private void MoveToNextField(Control oControl, bool bFirstChar)
		{
            LMP.Trace.WriteNameValuePairs("oControl", oControl.Name, "bFirstChar", bFirstChar);
            byte bytCurField = Convert.ToByte(oControl.Name.Substring(7));

			if(bytCurField < m_bytFieldCount)
			{
				//current field is not the last field - 
				//move to next field
				Control oCtl = (Control)m_oControls[bytCurField];
				oCtl.Focus();

				if(bFirstChar)
				{
					switch(oCtl.GetType().Name)
					{
						case "TextBox":
							TextBox oTB = (TextBox) oCtl;
                            oTB.SelectionStart = 0;
                            oTB.SelectionLength = 0;
							break;
                        case "ComboBox":
                            ComboBox oCombo = (ComboBox)oCtl;
                            oCombo.SelectionStart = 0;
                            oCombo.SelectionLength = 0;
                            break;
						case "MPMultilineCombo":
							MultilineCombo oMLC = (MultilineCombo) oCtl;
                            oMLC.SelectionStart = 0;
                            oMLC.SelectionLength = 0;
							break;
					}
				}
			}
			else
				//current field is the last field - 
				//move to the next item
				this.MoveToNextDetail();
		}

		/// <summary>
		/// moves the focus to the previous field-
		/// if current focus is the first field,
		/// moves focus to the last field of the previous detail
		/// </summary>
		/// <param name="oControl"></param>
		private void MoveToPreviousField(Control oControl, bool bLastChar)
		{
            LMP.Trace.WriteNameValuePairs("oControl", oControl.Name, "bLastChar", bLastChar);
            byte bytCurField = Convert.ToByte(oControl.Name.Substring(7));
            
			if(bytCurField > 1)
			{
				//current field is not the first field -
				//move to previous field
				Control oCtl = (Control)m_oControls[bytCurField - 2];
				oCtl.Focus();
				if(bLastChar)
				{
					switch(oCtl.GetType().Name)
					{
						case "TextBox":
							TextBox oTB = (TextBox) oCtl;
							oTB.SelectionStart = oTB.Text.Length;
							break;
						case "ComboBox":
							ComboBox oCombo = (ComboBox) oCtl;
							oCombo.SelectionStart = oCombo.Text.Length;
							break;
						case "MPMultilineCombo":
							MultilineCombo oMLC = (MultilineCombo) oCtl;
							oMLC.SelectionStart = oMLC.Text.Length;
							break;
					}
				}
			}
			else
			{
				//current field is the first field-
				//move to previous detail
				this.MoveToDetail(-1, true, true, mpDetailFieldSelection.EndOfField);
			}
		}

		/// <summary>
		/// moves the focus to the item with the
		/// specified index or offset from the current item-
		/// does not add a new detail if current item
		/// is the last item - gives focus to last field
		/// if specified, else gives focus to first field
		/// </summary>
		/// <param name="iIndex"></param>
		/// <param name="bRelativeIndex"></param>
		/// <param name="bLastFieldFocus"></param>
		/// <param name="bytSelect"></param>
		private void MoveToDetail(int iIndex, bool bRelativeIndex, 
			bool bLastFieldFocus, mpDetailFieldSelection bytSelect)
		{
            LMP.Trace.WriteNameValuePairs("iIndex", iIndex, "bRelativeIndex", bRelativeIndex,
                "bLastFieldFocus", bLastFieldFocus, "bytSelect", bytSelect);
            if (m_iCounterIndex == 0)
                return;
            else if (m_iCounterIndex == 1 && iIndex < 0)
                return;

            if (bRelativeIndex)
                //the index specified is relative to the current detail
                m_iCounterIndex = m_iCounterIndex + iIndex;
            else
            {
                //move to the specified detail
                m_iCounterIndex = iIndex;
            }

            // GLOG : 3485 : JAB
            // Update the index the m_iCurDetailIndex to correspond to the counter index.
            this.m_iCurDetailIndex = m_iCounterIndex - 1;

            UpdateDetail();
            //Don't force focus if value is being changed externally
            if (this.Focused)
            {
                if (bLastFieldFocus)
                    ((Control)m_oControls[m_bytFieldCount - 1]).Focus();
                else
                    ((Control)m_oControls[0]).Focus();
            }

			switch(bytSelect)
			{
				case mpDetailFieldSelection.StartOfField:
				{
					if(this.ActiveControl is TextBox)
					{
						TextBox oTB = (TextBox)this.ActiveControl;
						oTB.SelectionStart = 0;
						oTB.SelectionLength = 0;
					}
					else if(this.ActiveControl is ComboBox)
					{
						ComboBox oCombo = (ComboBox)this.ActiveControl;
						oCombo.SelectionStart = 0;
						oCombo.SelectionLength = 0;
					}
					else if(this.ActiveControl is MultilineCombo)
					{
						MultilineCombo oMLC = (MultilineCombo) this.ActiveControl;
						oMLC.SelectionStart = 0;
						oMLC.SelectionLength = 0;
					}
					break;
				}
				case mpDetailFieldSelection.EndOfField:
				{
					if(this.ActiveControl is TextBox)
					{
						TextBox oTB = (TextBox)this.ActiveControl;
						oTB.SelectionStart = oTB.TextLength;
						oTB.SelectionLength = 0;
					}
					else if(this.ActiveControl is ComboBox)
					{
						ComboBox oCombo = (ComboBox)this.ActiveControl;
						oCombo.SelectionStart = oCombo.Text.Length;
						oCombo.SelectionLength = 0;
					}
					else if(this.ActiveControl is MultilineCombo)
					{
						MultilineCombo oMLC = (MultilineCombo) this.ActiveControl;
						oMLC.SelectionStart = oMLC.Text.Length;
						oMLC.SelectionLength = 0;
					}
					break;
				}
			}

            // GLOG : 3485 : JAB
            // Update the view of the move up/down buttons.
            this.UpdateMoveButtonsView();
		}

		/// <summary>
		/// returns true iff the specified control is the last sub control
		/// </summary>
		/// <param name="oCtl"></param>
		/// <returns></returns>
		private bool IsLastSubControl(Control oCtl)
		{
			return oCtl.Name == "Control" + m_bytFieldCount.ToString();
		}

		/// <summary>
		/// returns true iff control is a multiline control
		/// </summary>
		/// <param name="oCtl"></param>
		/// <returns></returns>
		private bool IsMultilineControl(Control oCtl)
		{
			if(oCtl is MultilineCombo)
				return true;
			else if(oCtl is TextBox)
			    return ((TextBox)oCtl).Multiline == true;
			else
				return false;
		}

        /// <summary>
        /// Move to detail selected from dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tddbStatus_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                this.MoveToDetail(this.tddbStatus.DropDownItems.IndexOf(e.ClickedItem) + 1,
                    false, false, mpDetailFieldSelection.WholeField);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// Populate dropdown with list of Detail items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tddbStatus_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                LMP.Trace.WriteInfo("tddbStatus_DropDownOpening");
                SaveCurDetail();
                string[] aVals = null;
                this.tddbStatus.DropDownItems.Clear();
                if (m_oDetailArray.Count == 0)
                    return;

                //assign array to detail list
                foreach (object oDetail in m_oDetailArray)
                {
                    aVals = (string[])oDetail;
                    this.tddbStatus.DropDownItems.Add(aVals[1]);
                }

                if (m_iCounterIndex <= this.tddbStatus.DropDownItems.Count)
                    //select the current detail in the list
                    this.tddbStatus.DropDownItems[m_iCounterIndex - 1].Select();
                else
                    this.tddbStatus.DropDownItems[0].Select();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

		/// <summary>
		/// deletes the current item from the array
		/// </summary>
		private void DeleteCurrentDetail(bool bPrompt)
		{
            LMP.Trace.WriteInfo("DeleteCurrentDetail");

            if (this.InAddMode && !this.DetailDirty)
            {
                //In blank record, move to last populated detail
                if (m_oDetailArray.Count > 0)
                {
                    //This will result in last populated record being displayed
                    SaveCurDetail();
                }
                return;
            }
            
            if (bPrompt)
            {
                DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oChoice != DialogResult.Yes)
                    return;
            }

            if (this.InAddMode)
            {
                // Blank out fields for new entry
                ClearDetail();
                ((Control)m_oControls[0]).Focus();
            }
            else if(m_iCurDetailIndex > -1)
			{
                SaveCurDetail();
                m_oDetailArray.RemoveAt(m_iCurDetailIndex);
				
				int iNumDetails = m_oDetailArray.Count;

				if(m_iCurDetailIndex > iNumDetails - 1)
					m_iCurDetailIndex = iNumDetails - 1;

				ClearDetail();

				if(DetailDeleted != null)
					DetailDeleted(m_iCurDetailIndex);

				FillDetail();
                m_iCounterIndex = m_iCurDetailIndex + 1;
				m_iMaxIndex = iNumDetails;
                ((Control)m_oControls[0]).Focus();
				UpdateCount();

                this.tbtnAdd.Enabled = !this.MaxEntitiesReached;
				this.DetailDirty = false;
                this.IsDirty = true;

                //notify that value has changed, if necessary
                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());
			}

            this.UpdateMaxEntitiesView();
		}

		/// <summary>
		/// draws the gridlines between controls
		/// </summary>
		/// <param name="oG"></param>
        private void PaintGridlines(System.Drawing.Graphics oG)
		{
            try
            {
                using (System.Drawing.Pen oP = new Pen(Brushes.DarkGray, 1))
                {
                    //remove previous gridlines
                    oG.Clear(this.pnlFields.BackColor);

                    //cycle through controls, drawing gridlines between controls
                    for (int i = 0; i < m_oControls.Length - 1; i++)
                    {
                        Control oCtl = (Control)m_oControls[i];
                        Control oNextCtl = (Control)m_oControls[i + 1];

                        int iY = 0;
                        if (oCtl is LMP.Controls.ComboBox)
                            iY = oCtl.Top + oCtl.Height - 2;
                        else
                        {
                            if (oNextCtl is LMP.Controls.ComboBox)
                                iY = oNextCtl.Top - 1;
                            else
                                iY = ((oCtl.Top + oCtl.Height + oNextCtl.Top) / 2);
                        }

                        //set Y pos to be between controls
                        if (oCtl is ComboBox && oNextCtl is ComboBox)
                            iY = iY + 1;
                        else
                            iY = iY + 1;

                        oG.DrawLine(oP, 0, iY, pnlFields.Width, iY);
                    }

                    // Draw Vertical separator
                    oG.DrawLine(oP, this.LabelColumnWidth - 1, 0, 
                        this.LabelColumnWidth - 1, pnlFields.Height);

                    //draw box around fields
                    Rectangle oRect = new Rectangle(0,0,this.Bounds.Width - 3,this.pnlFields.Height - 1);
                    //oRect.Inflate(-1, 0);
                    oG.DrawRectangle(oP, oRect);
                }
            }
            catch { }
		}
		#endregion

		#region *********************event handlers*********************
        /// <summary>
        /// Control has gotten focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Detail_Enter(object sender, EventArgs e)
        {
            try
            {
                LMP.Trace.WriteInfo("Detail_Enter");
                if (!this.DesignMode)
                {
                    if (m_bPopupVisible)
                        return;
                    // Disable txtStartDetail so first subcontrol will get focus
                    EnableDetailViewTabStops(false, false, true);
                    ((Control)m_oControls[0]).Focus();
                    if (m_iCurDetailIndex > -1)
                        FillDetail();
                }
                base.OnGotFocus(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Focus has moved off of Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Detail_Leave(object sender, EventArgs e)
        {
            try
            {
                LMP.Trace.WriteInfo("Detail_Leave");
                //Displaying popup list will change focus
                if (m_bPopupVisible)
                    return;
                SaveCurDetail();
                if (!this.DesignMode)
                {
                    EnableDetailViewTabStops(false, false, false);
                    // Enable only first sub-control
                    ((Control)m_oControls[0]).TabStop = true;
                }
                base.OnLostFocus(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

		/// <summary>
		/// handles the mouse down event for sub controls
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleMouseDown(object sender, MouseEventArgs e)
		{
            LMP.Trace.WriteInfo("HandleMouseDown");
            try
			{
				if(this.SubControlMouseDown != null)
					this.SubControlMouseDown((Control)sender, e.X, e.Y);
				
				if(e.Button == MouseButtons.Right)
				{
					m_oPopupSenderCtl = (Control) sender;
					this.mnuPopup.Show(m_oPopupSenderCtl, 
						new Point(m_oPopupSenderCtl.Left + e.X, e.Y));
				}
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		/// <summary>
		/// handles the TextChanged event sub controls
		/// </summary>
		private void HandleTextChanged(object sender, EventArgs e)
		{
            LMP.Trace.WriteInfo("HandleTextChanged");
            try
			{
				Control oCtl = (Control) sender;
                //Re-enable tab stops if we're entering text in a new blank detail
                if (oCtl.Name == "Control1" && m_iCurDetailIndex > (m_oDetailArray.Count - 1))
                    EnableDetailViewTabStops(true, true, true);

                this.DetailDirty = true;
                this.IsDirty = true;
                this.tbtnAdd.Enabled = !this.MaxEntitiesReached;
				this.UpdateCount();

                //if(this.IsLastSubControl(oCtl) || this.IsMultilineControl(oCtl))
                //{
                //    //Control is last control or multiline control
                //    if(oCtl.Text.Length > 4 && oCtl.Text.Substring(oCtl.Text.Length - 4) == "\r\n\r\n")
                //    {
                //        //control text ends in 2 returns - clear and move to next field
                //        oCtl.Text = oCtl.Text.Substring(0, oCtl.Text.Length - 4);
                //        this.MoveToNextField(oCtl, true);
                //    }
                //}
				
                // GLOG : 3485 : JAB
                // Update the view for cases when we are adding an enty
                if (this.InAddMode)
                {
                    this.UpdateMoveButtonsView();
                }
				if(this.SubControlChanged != null)
					this.SubControlChanged((Control)sender);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		/// <summary>
		/// handles the GotFocus event for sub controls
		/// </summary>
		private void HandleGotFocus(object sender, EventArgs e)
		{
            try
			{
                //GLOG 6071:  Make sure flags are reset after returning from CI
                m_iCountBeforeCI = 0;
                m_bCIRequested = false;
                Control oCtl = (Control)sender;
                LMP.Trace.WriteNameValuePairs("oCtl.Name", oCtl.Name);
			    if(oCtl.Name == "Control" + m_bytFieldCount.ToString() && m_iCurDetailIndex == m_oDetailArray.Count - 1)
				{
                    //the control that has focus is the last subcontrol -
                    //tab will move to a new blank record
                    EnableDetailViewTabStops(true, true, true);
				}
                else if (oCtl.Name == "Control1" && m_iCurDetailIndex < 1)
                {
                    //the control that has focus is the first subcontrol -
                    //shift-tab will move to previous control
                    EnableDetailViewTabStops(true, false, true);
                }
                else
					EnableDetailViewTabStops(true, true, true);

				//ensure that content of subcontrol is selected
				LMP.Dialog.EnsureSelectedContent(oCtl, false, false);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

        /// <summary>
		/// handles the KeyDown event sub controls
		/// </summary>
		private void HandleKeyDown(object sender, KeyEventArgs e)
		{
			try
			{

			    bool bNoKeyQualifiers = !e.Shift && !e.Alt && !e.Control;
                m_bShortCutKeyPressed = false;
                LMP.Trace.WriteNameValuePairs("sender", ((Control)sender).Name, "key", e.KeyCode, 
                    "bNoKeyQualifiers", bNoKeyQualifiers);
                if (e.KeyCode == Keys.Return)
				{
                    int iQualifierKeys = 0;
                    if (e.Shift)
                        iQualifierKeys = iQualifierKeys + (int)Keys.ShiftKey;
                    if (e.Control)
                        iQualifierKeys = iQualifierKeys + (int)Keys.ControlKey;
                    if (e.Alt)
                        iQualifierKeys = iQualifierKeys + (int)Keys.LMenu;
                    if (!this.IsMultilineControl((Control)sender))
                    {
                        e.SuppressKeyPress = true;
                        ExecuteCodeForEnterPressed(sender, new EnterKeyPressedEventArgs(iQualifierKeys));
                        e.Handled = true;
                    }
                }
				else if(e.KeyCode == Keys.Down && bNoKeyQualifiers)
				{

                    if (IsMultilineControl((Control)sender))
                    {
                        string xCtlType = ((Control)sender).GetType().Name;
                        //Move to next field if selection is at end of multiline text
                        if (xCtlType == "TextBox" && 
                            !(this.IsLastSubControl((Control)sender) && 
                            m_iCurDetailIndex == m_oDetailArray.Count - 1))
                        {
                            TextBox oTB = (TextBox)sender;
                            if (oTB.SelectionStart == oTB.Text.Length)
                            {
                                e.Handled = true;
                                this.MoveToNextField((Control)sender, true);
                            }
                        }
                        else if (xCtlType == "MultilineCombo" &&
                            !(this.IsLastSubControl((Control)sender) &&
                            m_iCurDetailIndex == m_oDetailArray.Count - 1))
                        {
                            MultilineCombo oMLC = (MultilineCombo)sender;
                            if (oMLC.SelectionStart == oMLC.Text.Length)
                            {
                                e.Handled = true;
                                this.MoveToNextField((Control)sender, true);
                            }
                        }
                    }
                    else if (!(sender is ComboBox))
                    {
                        this.MoveToNextField((Control)sender, false);
                        e.Handled = true;
                    }
                }
				else if(e.KeyCode == Keys.Up && bNoKeyQualifiers)
				{
                    if (IsMultilineControl((Control)sender))
                    {
                        string xCtlType = ((Control)sender).GetType().Name;

                        if (xCtlType == "TextBox")
                        {
                            TextBox oTB = (TextBox)sender;
                            if (oTB.SelectionStart == 0 && (m_iCurDetailIndex > 0 || this.ActiveControl != m_oControls[0]))
                            {
                                //we're at the beginning of a control that is not the
                                //first control of the first detail
                                e.Handled = true;
                                this.MoveToPreviousField((Control)sender, true);
                            }
                        }
                        else if (xCtlType == "MultilineCombo")
                        {
                            MultilineCombo oMLC = (MultilineCombo)sender;
                            if (oMLC.SelectionStart == 0)
                            {
                                e.Handled = true;
                                this.MoveToPreviousField((Control)sender, true);
                            }
                        }
                    }
                    else if (!(sender is ComboBox))
                    {
                        e.Handled = true;
                        this.MoveToPreviousField((Control)sender, false);
                    }
                }
				else if(e.KeyCode == Keys.Left && bNoKeyQualifiers)
				{
					string xCtlType = ((Control) sender).GetType().Name;

					if(xCtlType == "TextBox")
					{
						TextBox oTB = (TextBox) sender;
						if(oTB.SelectionStart == 0 && (m_iCurDetailIndex > 0 || this.ActiveControl != m_oControls[0]))
						{
							//we're at the beginning of a control that is not the
							//first control of the first detail
							e.Handled = true;
							this.MoveToPreviousField((Control) sender,true);
						}
					}
					else if(xCtlType == "ComboBox")
					{
						ComboBox oCombo = (ComboBox)sender;
						if (oCombo.SelectionStart == 0)
						{
							e.Handled = true;
							this.MoveToPreviousField((Control) sender,true);
						}
                    }
                    else if (xCtlType == "MultilineCombo")
                    {
                        MultilineCombo oMLC = (MultilineCombo)sender;
                        if (oMLC.SelectionStart == 0)
                        {
                            e.Handled = true;
                            this.MoveToPreviousField((Control)sender, true);
                        }
                    }
                    else
                    {
                        e.Handled = true;
                        this.MoveToPreviousField((Control)sender, true);
                    }
				}
				else if(e.KeyCode == Keys.Right && bNoKeyQualifiers)
				{
					string xCtlType = ((Control) sender).GetType().Name;

					if(!(this.IsLastSubControl((Control)sender) && 
						m_iCurDetailIndex == m_oDetailArray.Count - 1))
					{				
						if(xCtlType == "TextBox")
						{
							TextBox oTB = (TextBox) sender;
							if(oTB.SelectionStart == oTB.Text.Length)
							{
								e.Handled = true;
								this.MoveToNextField((Control) sender,true);
							}
							else if(xCtlType == "ComboBox")
							{
								ComboBox oCombo = (ComboBox) sender;
								if(oCombo.SelectionStart == oCombo.Text.Length)
								{
									e.Handled = true;
									this.MoveToNextField((Control) sender,true);
								}
                            }
                            else if (xCtlType == "MultilineCombo")
                            {
                                MultilineCombo oMLC = (MultilineCombo)sender;
                                if (oMLC.SelectionStart == oMLC.Text.Length)
                                {
                                    e.Handled = true;
                                    this.MoveToNextField((Control)sender, true);
                                }
                            }
                            else
                            {
                                e.Handled = true;
                                this.MoveToNextField((Control)sender, true);
                            }
						}
					}
				}
				else if(e.KeyCode == Keys.PageUp && bNoKeyQualifiers)
				{
                    if (tbtnPrev.Enabled)
                    {
                        e.Handled = true;
                        this.MoveToDetail(-1, true, false, mpDetailFieldSelection.WholeField);
                    }
				}
				else if(e.KeyCode == Keys.PageDown && bNoKeyQualifiers)
				{
                    if (tbtnNext.Enabled)
                    {
                        e.Handled = true;
                        this.MoveToNextDetail();
                    }
				}
                else if ((e.KeyCode == Keys.Delete) && e.Control & e.Shift)
                {
                    if (tbtnDelete.Enabled)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                        m_bShortCutKeyPressed = true;
                        DeleteCurrentDetail(true);
                        this.Focus();
                        return;
                    }
                }
                else if ((e.KeyCode == Keys.N) && e.Control && e.Shift)
                {
                    if (tbtnAdd.Enabled)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                        m_bShortCutKeyPressed = true;
                        this.AddNew(true);
                        this.Focus();
                        return;
                    }
                }

                if (this.KeyPressed != null)
                    this.KeyPressed(this, e);

                if (this.SubControlKeyDown != null)
					this.SubControlKeyDown((Control)sender, e.KeyCode);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Control has been resized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void Detail_Resize(object sender, System.EventArgs e)
		{
			try
			{
                LMP.Trace.WriteInfo("Detail_Resize");
                if (m_bControlIsLoaded && !m_bPopupVisible)
                {
                    //GLOG - 3263 - CEH
                    //GLOG - 3613 - CEH
                    SetupControl();
                }
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Control is loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void Detail_Load(object sender, System.EventArgs e)
		{
            try
            {
                LMP.Trace.WriteInfo("Detail_Load");

                this.SuspendLayout();

                SetupResourceStrings();
                if (this.DesignMode)
                    ExecuteFinalSetup();
                else
                    this.pnlFields.DoubleClick += new EventHandler(HandleDoubleClick);

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
		}
		/// <summary>
		/// Add a new detail item and clear fields
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void tbtnAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				AddNew(true);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Navigation via left/right arrows on scrollbar
        /// </summary>
		private void UpdateDetail()
		{
			try
			{
                LMP.Trace.WriteInfo("UpdateDetail");

				if(m_oDetailArray.Count == 0)
					m_iMinIndex = 0;
				else
					m_iMinIndex = 1;

				if(m_iCounterIndex <= m_oDetailArray.Count &&  
					m_iCounterIndex > 0)
				{
					SaveCurDetail();

					m_iCurDetailIndex = m_iCounterIndex - 1;

					FillDetail();
					UpdateCount();

					Trace.WriteNameValuePairs("m_iCurDetailIndex", m_iCurDetailIndex);
				}
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}		

        /// <summary>
        /// Focus has moved to textbox marking end of record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void txtEndDetail_Enter(object sender, System.EventArgs e)
		{
            LMP.Trace.WriteInfo("txtEndDetail_Enter");
            if (!this.DesignMode)
                ExitDetail();
		}
        /// <summary>
        /// Focus has moved to textbox marking start of record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void txtStartDetail_Enter(object sender, System.EventArgs e)
		{
            LMP.Trace.WriteInfo("txtStartDetail_Enter");
            if (!this.DesignMode)
                EnterDetail();
		}

        /// <summary>
        /// Delete item from list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void tbtnDelete_Click(object sender, System.EventArgs e)
		{
            try
			{
                LMP.Trace.WriteInfo("tbtnDelete_Click");
                DeleteCurrentDetail(true);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Mouse button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void pnlFields_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
				this.mnuPopup.Show(this.pnlFields,new Point(e.X, e.Y));
		}
        /// <summary>
        /// Clears contents of selected detail field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuContextMenu_ClearField_Click(object sender, System.EventArgs e)
		{
			try
			{
				m_oPopupSenderCtl.Text = null;
                this.Refresh();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Sets up control to enter a new detail item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuContextMenu_AddDetail_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.AddNew(true);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Deletes the current item from the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuContextMenu_DeleteDetail_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.DeleteCurrentDetail(true);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Clears all detail items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuContextMenu_DeleteAll_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.DeleteAll();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Refresh current item from CI based on UNID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuContextMenu_RefreshDetail_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.RefreshCIValues(m_iCurDetailIndex);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Move to previous detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnPrev_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_iCounterIndex > m_iMinIndex)
                {
                    SaveCurDetail();
                    m_iCounterIndex--;
                    UpdateDetail();

                    // GLOG : 3485 : JAB
                    // Update the move up/down buttons view.
                    UpdateMoveButtonsView();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Move to next detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_iCounterIndex < m_iMaxIndex)
                {
                    SaveCurDetail();
                    m_iCounterIndex++;
                    UpdateDetail();

                    // GLOG : 3485 : JAB
                    // Update the move up/down buttons view.
                    this.UpdateMoveButtonsView();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Retrieve Contacts from CI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnContacts_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_bCIRequested)
                    return;
                try
                {
                    //Clear tooltip to allow CI Window to initialize properly
                    ToolTip t = (ToolTip)this.tsActions.GetType().GetProperty("ToolTip", 
                        BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this.tsActions, null);
                    t.RemoveAll();
                }
                catch { }
                this.tsActions.Enabled = false;
                m_bCIRequested = true;
                m_iCountBeforeCI = m_oDetailArray.Count;
                if (this.CIRequested != null)
                    this.CIRequested(this, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                UpdateMaxEntitiesView();
                this.tsActions.Enabled = true;
            }
        }
        /// <summary>
        /// Refresh all detail items from CI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuContextMenu_RefreshAll_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.Cursor = Cursors.WaitCursor;
				this.RefreshCIValues(-1);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		private void pnlFields_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
            try
            {
                PaintGridlines(e.Graphics);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
		#endregion

		#region *********************protected members*********************		
		protected override bool IsInputKey(Keys keyData)
		{
            if (keyData == Keys.Tab || keyData == Keys.Return)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
		/// processes tab and shift-tab key messages -
		/// this method will be executed only when the
		/// message pump is broken, ie when the hosing
		/// form's ProcessDialogKey method is not run.
		/// this seems to happen when when tabbing from
		/// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (m.WParam.ToInt32() == (int)Keys.Return)
            {
                // Process Enter here, because KeyDown doesn't get raised for this
                // in TaskPane
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        #endregion

		#region *********************IControl members*********************
		public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        /// <summary>
        /// 
        /// </summary>
        public void ExecuteFinalSetup()
		{
            try
			{
                LMP.Trace.WriteInfo("ExecuteFinalSetup");
				m_bControlIsLoaded = true;
				SetupControl();
                PopulateDetails();
                this.UpdateMaxEntitiesView();
                if (!this.DesignMode)
                    ((Control)m_oControls[0]).Focus();
            }
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

        [Browsable(false)]
        [DescriptionAttribute("Gets/Sets the selected value.")]
		public string Value
		{
			get
			{
				return this.ContentString;
			}
			set
			{
                this.ContentString = value;
                //If entries have been added from CI, move to last new item
                if (m_oDetailArray.Count > m_iCountBeforeCI)
                    this.MoveToDetail(m_oDetailArray.Count, false, false, mpDetailFieldSelection.WholeField);

                this.IsDirty = false;
                m_iCountBeforeCI = 0;
                m_bCIRequested = false;
			}
		}
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the Dirty flag indicating the value has changed.")]
		public bool IsDirty
		{
			set{m_bIsDirty = value;}
			get{return m_bIsDirty;}
		}
        [Browsable(false)]
        [DescriptionAttribute("Secondary user-defined object associated with the control.")]
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        /// <summary>
        /// Handle Tab input in sub-control fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputField_TabPressed(object sender, TabPressedEventArgs e)
        {
            try
            {
                Control oCtl = (Control)sender;
                LMP.Trace.WriteNameValuePairs("oCtl.Name", oCtl.Name, "ShiftPressed", e.ShiftPressed);
                if (oCtl.Name == "Control1")
                {
                    if (!e.ShiftPressed)
                    {
                        if (m_oControls.Length > 1)
                        {
                            //move to the next sub-control
                            MoveToNextField(oCtl, false);
                            return;
                        }
                        else if (m_iCurDetailIndex < m_oDetailArray.Count || this.DetailDirty)
                        {
                            //single sub-control - move to next detail
                            MoveToNextDetail();
                            return;
                        }
                    }
                    else if (e.ShiftPressed && m_iCurDetailIndex > 0)
                    {
                        //move to previous detail
                        MoveToPreviousField(oCtl, false);
                        return;
                    }
                }
                else if (IsLastSubControl(oCtl))
                {
                    if (e.ShiftPressed)
                    {
                        // Move to previous sub-control
                        MoveToPreviousField(oCtl, false);
                        e.ShiftPressed = false;
                        return;
                    }
                    else
                    {
                        if (m_iCurDetailIndex < m_oDetailArray.Count || this.DetailDirty)
                        {
                            MoveToNextDetail();
                            return;
                        }
                    }
                }
                else
                {
                    if (e.ShiftPressed)
                        MoveToPreviousField(oCtl, false);
                    else
                        MoveToNextField(oCtl, false);
                    e.ShiftPressed = false;
                    return;
                }
                if (this.TabPressed != null)
                    this.TabPressed(this, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
		#endregion

        #region *********************ICIable members*********************
        public event CIRequestedHandler CIRequested;
        public bool IsSubscribedToCI
        {
            get
            {
                if ((System.Delegate)this.CIRequested != null)
                {
                    return this.CIRequested.GetInvocationList().Length > 0;
                }
                else
                {
                    return false;
                }
            }
        }
        public int ContactCount
        {
            get { return m_oDetailArray.Count; }
        }

        #endregion
        private void Detail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                //Container or subcontrol is handling tab - don't process further
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.F2)
            {
                m_bCIRequested = true;
                m_iCountBeforeCI = m_oDetailArray.Count;
            }
 	        return base.ProcessDialogKey(keyData);
        }

        // GLOG : 3485 : JAB 
        // Move the order of the selected item up.
        private void tbtnMoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.InAddMode)
                {
                    this.SaveCurDetail();
                }
                if (this.m_oDetailArray.Count > 1 && this.m_iCurDetailIndex > 0)
                {
                    object oItemDetail = m_oDetailArray[this.m_iCurDetailIndex];
                    this.m_oDetailArray.RemoveAt(this.m_iCurDetailIndex);
                    this.m_iCurDetailIndex--;
                    this.m_iCounterIndex--;
                    this.m_oDetailArray.Insert(this.m_iCurDetailIndex, oItemDetail);
                    this.DetailDirty = true;
                    this.IsDirty = true;
                    this.UpdateCount();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            UpdateMaxEntitiesView();
        }

        // GLOG : 3485 : JAB 
        // Move the order of the selected item down.
        private void tbtnMoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.m_oDetailArray.Count > 0 && this.m_iCurDetailIndex < this.m_oDetailArray.Count - 1)
                {
                    object oItemDetail = m_oDetailArray[this.m_iCurDetailIndex];
                    this.m_oDetailArray.RemoveAt(this.m_iCurDetailIndex);
                    this.m_iCurDetailIndex++;
                    this.m_iCounterIndex++;
                    this.m_oDetailArray.Insert(this.m_iCurDetailIndex, oItemDetail);
                    this.DetailDirty = true;
                    this.IsDirty = true; 
                    this.UpdateCount();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            UpdateMaxEntitiesView();
        }

        // GLOG : 3485 : JAB
        // Handle the view of the buttons, enabling as appropriate.
        private void UpdateMoveButtonsView()
        {
            if (this.InAddMode && (this.m_oDetailArray.Count>0))
            {
                // Enable the up button on if the detail entry has been modified.
                this.tbtnMoveUp.Enabled = DetailDirty;
            }
            else
            {
                // Enable the up button if the current detail is not the first entry.
                this.tbtnMoveUp.Enabled = (this.m_iCurDetailIndex > 0);
            }

            // Enable the down button if it is not the last entry.
            this.tbtnMoveDown.Enabled = (this.m_iCurDetailIndex < this.m_oDetailArray.Count - 1);
        }

        //GLOG : 6297 : CEH
        private void mnuContextMenu_Paste_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (this.ActiveControl is TextBox)
                {
                    TextBox oTB = (TextBox)this.ActiveControl;
                    oTB.SelectedText = Clipboard.GetText();                    
                }
                else if (this.ActiveControl is ComboBox)
                {
                    ComboBox oCombo = (ComboBox)this.ActiveControl;
                    oCombo.SelectedValue = Clipboard.GetText();
                }
                else if (this.ActiveControl is MultilineCombo)
                {
                    MultilineCombo oMLC = (MultilineCombo)this.ActiveControl;
                    oMLC.SelectedText = Clipboard.GetText();
                }               
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuContextMenu_Copy_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ActiveControl is TextBox)
                {
                    TextBox oTB = (TextBox)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oTB.SelectedText, true);
                }
                else if (this.ActiveControl is ComboBox)
                {
                    ComboBox oCombo = (ComboBox)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oCombo.SelectedValue.ToString(), true);
                }
                else if (this.ActiveControl is MultilineCombo)
                {
                    MultilineCombo oMLC = (MultilineCombo)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oMLC.SelectedText, true);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuContextMenu_Cut_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ActiveControl is TextBox)
                {
                    TextBox oTB = (TextBox)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oTB.SelectedText, true);
                    //cut
                    oTB.SelectedText = "";
                }
                else if (this.ActiveControl is ComboBox)
                {
                    ComboBox oCombo = (ComboBox)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oCombo.SelectedValue.ToString(), true);
                    //cut
                    oCombo.SelectedValue = "";
                }
                else if (this.ActiveControl is MultilineCombo)
                {
                    MultilineCombo oMLC = (MultilineCombo)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oMLC.SelectedText, true);
                    //cut
                    oMLC.SelectedText = "";
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6297 : CEH
        //enable/disable copy & cut context menu items
        private void mnuPopup_Popup(object sender, EventArgs e)
        {
            try
            {
                bool bEnable = false;
                if (this.ActiveControl is TextBox)
                {
                    TextBox oTB = (TextBox)this.ActiveControl;
                    bEnable = (oTB.SelectedText != "");
                }
                else if (this.ActiveControl is ComboBox)
                {
                    ComboBox oCombo = (ComboBox)this.ActiveControl;
                    oCombo.SelectedValue = "";
                    bEnable = (oCombo.SelectedValue.ToString() != "");
                }
                else if (this.ActiveControl is MultilineCombo)
                {
                    MultilineCombo oMLC = (MultilineCombo)this.ActiveControl;
                    bEnable = (oMLC.SelectedText != "");
                }

                this.mnuContextMenu_Copy.Enabled = bEnable;
                this.mnuContextMenu_Cut.Enabled = bEnable;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }

}
