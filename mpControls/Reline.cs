using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LMP.Data;
using Infragistics.Shared;
using Infragistics.Win;
using XML = System.Xml;

namespace LMP.Controls
{
	public enum mpRelineTabs: byte
	{
		Reline = 0,
		Special = 1
	}

	public enum mpRelineStyles: byte
	{
		ShowRelineOnly = 0,
		ShowSpecialOnly = 1,
		ShowBoth = 2
	}

	[Flags]
	public enum mpRelineFormatOptions
	{
		None = 0,
		RelineBold = 1,
		RelineItalic = 2,
		RelineUnderline = 4,
		RelineUnderlineLast = 8,
		SpecialBold = 16,
		SpecialItalic = 32,
		SpecialUnderline = 64,
		SpecialUnderlineLast = 128
	}

	/// <summary>
	/// Summary description for Reline.
	/// </summary>
	public partial class Reline : System.Windows.Forms.UserControl, IControl
	{
        #region *********************fields*********************
        private const int mpRelineGridRowHeight = 16;

		private mpRelineTabs m_iDefaultTab;
		private string m_xSelectedPrefill = "";
		private string m_xCaptionString = "RelineáSpecial";
		private mpRelineStyles m_iStyle;
		private mpRelineFormatOptions m_iFormatValues;
        private mpRelineFormatOptions m_iFormatOptions; 
		private bool m_bInitialized = false;
		private object[] m_oPrefillArray = null;
		private string m_xContentString = "";
		private string m_xSpecialValue = "";
		private int m_iSelStart = -1;
		private int m_iSelLength = 0;
		private bool m_bIsDirty = false;
        private object m_oTag2;
        private bool m_bShortCutKeyPressed = false;
        private string m_xSupportingValues = "";
        private bool m_bRemoveEmptyValues = true; //GLOG 4220
        private int m_iFixedMenuCount = 0; //GLOG 7807
        private int m_iCurrentHeight = 0;
        private bool m_bConstructed = false;

        #endregion
        #region *********************constructor*********************
        public Reline()
		{
			InitializeComponent();
            m_iStyle = mpRelineStyles.ShowRelineOnly;
            m_iFormatOptions = (mpRelineFormatOptions)255;
            m_iDefaultTab = mpRelineTabs.Reline;
            m_bConstructed = true;
        }
        #endregion *************************************************
        #region *********************properties*********************
        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Determines the captions for the tabs.")]
		public string CaptionString
		{
			get{return m_xCaptionString;}
			set
			{
				if (m_xCaptionString != value)
				{
					Trace.WriteNameValuePairs("value", value);

					string[] aCaptions = value.Split(LMP.StringArray.mpEndOfSubField);
					if (aCaptions.Length == 2)
					{
						m_xCaptionString = value;
						this.tbtnReline.Text = aCaptions[0];
                        this.tbtnReline.ToolTipText = aCaptions[0];
						this.tbtnSpecial.Text = aCaptions[1];
                        this.tbtnSpecial.ToolTipText = aCaptions[1];
					}
				}
			}
		}

		[CategoryAttribute("Appearance")]
		[DescriptionAttribute("Determines which tab will display initially.")]
		public mpRelineTabs DefaultTab
		{
			get{return m_iDefaultTab;}
			set
			{
                Trace.WriteNameValuePairs("value", value);

                mpRelineTabs iTab;
                if (m_bInitialized)
                {
                    if (m_iStyle == mpRelineStyles.ShowBoth)
                        //apply specified value
                        iTab = value;
                    else if (m_iStyle == mpRelineStyles.ShowRelineOnly)
                        //only reline tab is available
                        iTab = mpRelineTabs.Reline;
                    else
                        //only Special tab is available
                        iTab = mpRelineTabs.Special;
                }
                else
                    iTab = value;

                //switch tabs if necessary
                if (m_iDefaultTab != iTab)
                {
                    m_iDefaultTab = iTab;
                    DisplayTab(iTab);
                }
			}
		}

		[BrowsableAttribute(false)]
		[DescriptionAttribute("Get/Sets XML string specifying the reline text, Special grid values, format values, and selected Special prefill.")]
		private string ContentString
		{
			get
			{
                if ((!this.DesignMode) && (m_bInitialized))
                {
                    //get current control values
                    m_xContentString = ToXML();
                }
				return m_xContentString;
			}
			set
			{
                //GLOG 6067: Need to remove this check, since value will also include FormatValues,
                //which may have been set to Default value when configuring control properties
                //if (m_xContentString != value)
                //{
                    m_xContentString = value;
                    if (!this.DesignMode)
                    {
                        Trace.WriteNameValuePairs("value", value);

                        //convert delimited string or non-XML string to formatted XML
                        if (m_xContentString.IndexOf("<") != 0)
                        {
                            m_xContentString = StringToRelineXML(value, m_iFormatValues, m_xSelectedPrefill);
                            this.IsDirty = true;
                        }
                        //set control values
                        FromXML();

                        if ((m_bInitialized) && (m_iStyle != mpRelineStyles.ShowRelineOnly))
                        {
                            //reinitialize grid
                            InitializeGrid();

                            //update prefill menu
                            SelectPrefillMenuItem();

                        }
                    }
                }
			//}
		}

		[CategoryAttribute("Appearance")]
		[DescriptionAttribute("Determines which formatting buttons will be displayed.")]
		public mpRelineFormatOptions FormatOptions
		{
			get{return m_iFormatOptions;}
			set
			{
				Trace.WriteNameValuePairs("value", value);
                if (m_iFormatOptions != value)
                {
                    m_iFormatOptions = value;
                    if (m_bInitialized)
                    {
                        ResizeControls();
                        DisplayFormatOptions();
                    }
                }
			}
		}

		[BrowsableAttribute(false)]
		[DescriptionAttribute("Get/Sets the values of the formatting buttons.")]
		public mpRelineFormatOptions FormatValues
		{
			get{return m_iFormatValues;}
			set
			{
				Trace.WriteNameValuePairs("value", value);
                if (m_iFormatValues != value)
                {
                    m_iFormatValues = value;
                    if (!this.DesignMode && m_bInitialized)
                    {
                        this.IsDirty = true;

                        //notify that value has changed, if necessary
                        if (ValueChanged != null)
                            ValueChanged(this, new EventArgs());
                    }
                }
            }
		}

		[CategoryAttribute("Appearance")]
		[DescriptionAttribute("Determines whether to display the standard textbox, Special grid, or both.")]
		public mpRelineStyles Style
		{
			get{return m_iStyle;}
			set
			{
				if (m_iStyle != value)
				{
					Trace.WriteNameValuePairs("value", value);
					m_iStyle = value;

					//resize other controls
                    if (m_bInitialized)
                    {
                        ResizeControls();
                        //ensure that current default tab works for new style
                        DefaultTab = m_iDefaultTab;
                    }

					//initilize grid and prefill menu if necessary
					if ((!this.DesignMode) && (m_bInitialized) &&
						(m_iStyle != mpRelineStyles.ShowRelineOnly)  &&
						(m_oPrefillArray == null))
					{
						InitializeGrid();
						ConfigurePrefillMenu();
					}
				}
			}
		}
        //GLOG 4220
        public bool RemoveEmptyValues
        {
            get { return m_bRemoveEmptyValues; }
            set { m_bRemoveEmptyValues = value; }
        }

		#endregion
		#region *********************private members*********************
		/// <summary>
		/// displays specified tab
		/// </summary>
		/// <param name="iTab"></param>
		private void DisplayTab(mpRelineTabs iTab)
		{
			bool bShowGrid = (iTab == mpRelineTabs.Special);
			this.txtReline.Visible = !bShowGrid;
			this.grdSpecial.Visible = bShowGrid;
			if (m_iStyle == mpRelineStyles.ShowBoth && !this.DesignMode)
			{
				if (bShowGrid)
				{
                    this.tbtnReline.Checked = false;
                    this.tbtnSpecial.Checked = true;
					this.grdSpecial.Focus();

				}
				else
				{
					this.tbtnSpecial.Checked = false;
                    this.tbtnReline.Checked = true;
                    this.txtReline.Focus();
				}
                DisplayFormatOptions();
			}
		}

		/// <summary>
		/// adds available Special prefills to grid's right-click menu
		/// </summary>
		private void ConfigurePrefillMenu()
		{
			int iCount = m_oPrefillArray.Length;
            //GLOG 7807: Get count of fixed menu items
            m_iFixedMenuCount = this.mnuSpecial.MenuItems.Count;
			if (iCount > 0)
			{
				//create handler for click event
				EventHandler oHandler = new EventHandler(this.mnuSpecialPrefillItem_Click);

				//cycle through prefill list, adding to menu
				for (int i = 0; i < iCount; i++)
				{
					//get caption and values
					object[] oValues = (object[]) m_oPrefillArray[i];
					string xCaption = oValues[0].ToString();

					//create menu item
					MenuItem oItem = new MenuItem(xCaption, oHandler);

					//add - note that there are five menu items above the prefill list
                    //GLOG : 6297 : CEH
					this.mnuSpecial.MenuItems.Add(oItem); //GLOG 7807: added to end by default

					//add check mark if item is currently selected
					if (xCaption == m_xSelectedPrefill)
						oItem.Checked = true;
				}

				//add separator above prefill list
				this.mnuSpecialPrefillSeparator.Visible = true;
			}
		}

		/// <summary>
		/// initializes Special grid
		/// </summary>
		private void InitializeGrid()
		{
			if (m_oPrefillArray == null)
			{
				//get list of available prefills
                try
                {
                    List oList = (List)LMP.Data.Application.GetLists(true)
                        .ItemFromName("SpecialRelinePrefills");
                    m_oPrefillArray = oList.ItemArray.ToArray();
                }
                catch 
                {
                    m_oPrefillArray = new List[0]; //GLOG 7807: Handle case where List is not configured
                }
			}

            grdSpecial.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            grdSpecial.DisplayLayout.Override.DefaultRowHeight = 16;
            grdSpecial.ContextMenu = mnuSpecial;

            //create data storage
			DataSet oDS = new DataSet();
			DataTable oDT = new DataTable("Table");
			oDT.Columns.Add("Label", typeof(string));
			oDT.Columns.Add("Value", typeof(string));
			oDS.Tables.Add(oDT);
			this.grdSpecial.DataSource = oDS;
			this.grdSpecial.DataMember = "Table";

			if (m_xSpecialValue != "")
			{
				//populate from XML
				XML.XmlDocument oXML = new XML.XmlDocument();
				oXML.LoadXml(System.String.Concat("<tmpR>", m_xSpecialValue, "</tmpR>"));
				XML.XmlNodeList oNodeList = oXML.DocumentElement.SelectNodes("child::*");
                // Nodes will alternate Labels and Values
				for (int i=0; i < oNodeList.Count-1; i = i+2)
				{
					XML.XmlNode oNode = oNodeList.Item(i);
                    string xLabel = oNode.InnerText;
					string xValue = oNode.NextSibling.InnerText;
					oDT.Rows.Add(new object[]{xLabel, xValue});
				}
				this.grdSpecial.Refresh();
			}
			else if (m_xSelectedPrefill != ""  && m_oPrefillArray.Length > 0)
			{
				//load default prefill
				for (int i=0; i < m_oPrefillArray.Length; i++)
				{
					object[] aPrefill = (object[]) m_oPrefillArray[i];
					if (aPrefill[0].ToString() == m_xSelectedPrefill)
					{
						PrefillGrid(i);
						break;
					}
				}
			}
			else
			{
                if (m_xSelectedPrefill == "" && m_oPrefillArray.Length > 0)
                {
                    //Select first item if no default selected
                    m_xSelectedPrefill = ((object [])m_oPrefillArray[0])[0].ToString();
                    PrefillGrid(0);
                }
                else
                {
                    //add empty row
                    oDT.Rows.Add(new object[] { "", "" });
                    this.grdSpecial.Refresh();
                }
			}
		}

		/// <summary>
		/// populates grid with prefill at iIndex in prefill array
		/// </summary>
		/// <param name="iIndex"></param>
		private void PrefillGrid(int iIndex)
		{
			//clear data table
			this.grdSpecial.UpdateData();
			DataSet oDS = (DataSet) this.grdSpecial.DataSource;
			DataTable oDT = oDS.Tables["Table"];
			oDT.Clear();

			//insert prefill values into data table
			object[] aPrefill = (object[]) m_oPrefillArray[iIndex];
			string[] aRows = aPrefill[1].ToString().Split(LMP.StringArray.mpEndOfField);			
			for (int i=0; i < aRows.Length; i++)
			{
				string xValue = "";
				string[] aValues = aRows[i].Split(LMP.StringArray.mpEndOfValue);
				if (aValues.Length > 1)
					xValue = aValues[1];
				oDT.Rows.Add(new object[]{aValues[0], xValue});
			}

			//refresh grid
			this.grdSpecial.Refresh();
		}
        /// <summary>
        /// Handle defined shortcut key combinations
        /// </summary>
        /// <param name="e"></param>
        private void HandleShortCutKeys(KeyEventArgs e)
        {
            m_bShortCutKeyPressed = false;
            if (e.Control && e.Shift)
            {
                switch (e.KeyCode)
                {
                    case Keys.O:
                        //Toggle Bold
                        if (tbtnBold.Visible)
                        {
                            m_bShortCutKeyPressed = true;
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            tbtnBold.Checked = !tbtnBold.Checked;
                        }
                        break;
                    case Keys.U:
                        //Toggle Underline
                        if (tbtnUnderline.Visible)
                        {
                            m_bShortCutKeyPressed = true;
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            tbtnUnderline.Checked = !tbtnUnderline.Checked;
                        }
                        break;
                    case Keys.I:
                        //Toggle Italics
                        if (tbtnItalic.Visible)
                        {
                            m_bShortCutKeyPressed = true;
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            tbtnItalic.Checked = !tbtnItalic.Checked;
                        }
                        break;
                    case Keys.L:
                        //Toggle Underline Last
                        if (tbtnUnderlineLast.Visible)
                        {
                            m_bShortCutKeyPressed = true;
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            tbtnUnderlineLast.Checked = !tbtnUnderlineLast.Checked;
                        }
                        break;
                    case Keys.D1:
                    case Keys.NumPad1:
                    case Keys.Oem1:
                        //Select 1st tab
                        if (tbtnReline.Visible)
                        {
                            m_bShortCutKeyPressed = true;
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            tbtnReline.Checked = true;
                        }
                        break;
                    case Keys.D2:
                    case Keys.NumPad2:
                    case Keys.Oem2:
                        //Select 2nd tab
                        if (tbtnSpecial.Visible)
                        {
                            m_bShortCutKeyPressed = true;
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            tbtnSpecial.Checked = true;
                        }
                        break;
                }
            }
        }

		/// <summary>
		/// sets property and control values from content string
		/// </summary>
        private void FromXML()
        {
            if (m_xContentString != "")
            {
                int iStandardFormat = 0;
                int iSpecialFormat = 0;
                XML.XmlNode oAtt = null;
                XML.XmlDocument oXML = new XML.XmlDocument();
                oXML.LoadXml(System.String.Concat("<tmpR>", m_xContentString, "</tmpR>"));

                //reline text
                XML.XmlNodeList oNodeList = oXML.DocumentElement.SelectNodes("Standard");
                if (oNodeList.Count > 0)
                {
                    oAtt = oNodeList.Item(0).Attributes.GetNamedItem("Format");
                    if (oAtt != null)
                        iStandardFormat = Int32.Parse(oAtt.Value);
                    oAtt = null;
                    oAtt = oNodeList.Item(0).Attributes.GetNamedItem("Default");
                    if (oAtt != null)
                    {
                        m_xSelectedPrefill = LMP.String.RestoreXMLChars(
                            oAtt.Value);
                    }
                    this.txtReline.Text = LMP.String.RestoreXMLChars(
                        oNodeList.Item(0).InnerText);
                }

                //Special grid
                m_xSpecialValue = "";
                oNodeList = oXML.DocumentElement.SelectNodes("Special");
                if (oNodeList.Count > 0)
                {
                    // Get Format and Default attributes from XML
                    oAtt = null;
                    oAtt = oNodeList.Item(0).Attributes.GetNamedItem("Format");
                    if (oAtt != null)
                        iSpecialFormat = Int32.Parse(oAtt.Value);
                    oAtt = null;
                    oAtt = oNodeList.Item(0).Attributes.GetNamedItem("Default");
                    if (oAtt != null)
                    {
                        m_xSelectedPrefill = LMP.String.RestoreXMLChars(
                            oAtt.Value);
                    }
                    foreach (XML.XmlNode oChild in oNodeList.Item(0).ChildNodes)
                    {
                        //GLOG item #5828 - dcf
                        //m_xSpecialValue = m_xSpecialValue + LMP.String.RestoreXMLChars(oChild.OuterXml);
                        m_xSpecialValue += oChild.OuterXml;
                    }
                }

                //format options
                m_iFormatValues = (mpRelineFormatOptions)(iStandardFormat | iSpecialFormat); //GLOG 6067
                this.DisplayFormatOptions();
            }
        }

        /// <summary>
        /// sets content string with current property and control values
        /// </summary>
        private string ToXML()
        {
            string xSpecial = "";
            string xTemp = "";
            int iItemCount = 0;
            //get data from grid
            if (m_iStyle != mpRelineStyles.ShowRelineOnly)
            {
                grdSpecial.UpdateData();
                System.Text.StringBuilder oSB = new System.Text.StringBuilder();
                DataSet oDS = (DataSet)this.grdSpecial.DataSource;
                bool bEmptySpecial = true;
                //cycle through rows
                foreach (DataRow oRow in oDS.Tables["Table"].Rows)
                {
                    string xLabel = oRow["Label"].ToString();
                    string xValue = oRow["Value"].ToString();
                    if (xValue != "")
                    {
                        // At least one value has been entered
                        bEmptySpecial = false;
                    }
                    //GLOG 4220: Don't include rows with no value entered if RemoveEmptyValues=true
                    if (xLabel == "" && xValue != "" ||(xLabel != "" && (xValue != "" || !this.RemoveEmptyValues)))
                    {
                        //replace reserved characters with escape sequences
                        xLabel = LMP.String.ReplaceXMLChars(xLabel);
                        xValue = LMP.String.ReplaceXMLChars(xValue);

                        //row is not empty - add to content string
                        string xFormat = "<Label Index=\"{0}\">{1}</Label><Value Index=\"{0}\">{2}</Value>";
                        oSB.AppendFormat(xFormat, ++iItemCount, xLabel, xValue);
                    }
                }
                // Append Special only if all values were not blank
                if (!bEmptySpecial)
                {
                    xSpecial = oSB.ToString();
                }
            }
            //Include XML for Standard Reline if appropriate
            if (this.txtReline.Text != "" && this.Style != mpRelineStyles.ShowSpecialOnly)
            {
                string xValue = LMP.String.ReplaceXMLChars(this.txtReline.Text);

                xTemp = System.String.Concat(xTemp, "<Standard Format=\"", (int)m_iFormatValues, "\" Default=\"",
                    m_xSelectedPrefill, "\">", xValue, "</Standard>");
            }

            //Include XML for Special Reline if appropriate
			//GLOG 5547
            if (xSpecial != "" && this.Style != mpRelineStyles.ShowRelineOnly)
            {
                xTemp = System.String.Concat(xTemp, "<Special Format=\"", (int)m_iFormatValues, "\" Default=\"", m_xSelectedPrefill,
                "\">", xSpecial, "</Special>");
            }

            return xTemp;
        }
        /// <summary>
        /// converts delimited string to reline-formatted XML
        /// </summary>
        public static string StringToRelineXML(string xInput, mpRelineFormatOptions iFormatValues, string xDefPrefill)
        {
            string xSpecial = "";
            string xTemp = "";
            int iItemCount = 0;
            string[,] xSpecialTemp = null;
            string xStandard = null;
            mpRelineStyles iStyle = 0;
            
            //parse data from delimited string
            if (xInput.IndexOf("<") == 0)
            {
                //
                return xInput;
            }
            else if (!xInput.Contains("|"))
            {
                iStyle = mpRelineStyles.ShowRelineOnly;
                xStandard = LMP.String.ReplaceXMLChars(xInput);
            }
            else
            {
                //extract first field for standard, load 2 dimensional storage
                //array for special label/value pairs
                iStyle = mpRelineStyles.ShowBoth;
                string[] xParse = xInput.Split('|');

                xStandard = xParse[0];
                xSpecialTemp = new string[(xParse.Length - 1) / 2, 2];
                int iInc = 0;

                for (int i = 1; i <= xParse.GetLength(0) - 1; i++)
                {
                    if (i % 2 != 0)
                        xSpecialTemp[iInc, 0] = xParse[i];
                    else
                    {
                        xSpecialTemp[iInc, 1] = xParse[i];
                        iInc++;
                    }
                }
            }

            //build appropriate XML from storage array
            if (iStyle != mpRelineStyles.ShowRelineOnly)
            {
                System.Text.StringBuilder oSB = new System.Text.StringBuilder();
                bool bEmptySpecial = true;
                //cycle through rows
                for (int i = 0; i < xSpecialTemp.GetLength(0); i++)
                {
                    string xLabel = xSpecialTemp[i, 0].ToString();
                    string xValue = xSpecialTemp[i, 1].ToString();
                    if (xValue != "")
                    {
                        // At least one value has been entered
                        bEmptySpecial = false;
                    }
                    if (xLabel != "" || xValue != "")
                    {
                        //replace reserved characters with escape sequences
                        xLabel = LMP.String.ReplaceXMLChars(xLabel);
                        xValue = LMP.String.ReplaceXMLChars(xValue);

                        //row is not empty - add to content string
                        string xFormat = "<Label Index=\"{0}\">{1}</Label><Value Index=\"{0}\">{2}</Value>";
                        oSB.AppendFormat(xFormat, ++iItemCount, xLabel, xValue);
                    }
                }
                
                // Append Special only if all values were not blank
                if (!bEmptySpecial)
                {
                    xSpecial = oSB.ToString();
                }
            }
            //Include XML for Standard Reline if appropriate
            if (iStyle != mpRelineStyles.ShowSpecialOnly)
            {
                xTemp = System.String.Concat(xTemp, "<Standard Format=\"", (int)iFormatValues, "\" Default=\"",
                    xDefPrefill, "\">", xStandard, "</Standard>");
            }

            //Include XML for Special Reline if appropriate
            if (xSpecial != "" && iStyle != mpRelineStyles.ShowSpecialOnly)
            {
                xTemp = System.String.Concat(xTemp, "<Special Format=\"", (int)iFormatValues, "\" Default=\"", xDefPrefill,
                "\">", xSpecial, "</Special>");
            }

            return xTemp;
        }
        /// <summary>
        /// Setup text of interface items
        /// </summary>
        private void SetupResourceStrings()
        {
            this.tbtnBold.ToolTipText = LMP.Resources.GetLangString("Dialog_RelineBoldTooltip") + " [Ctrl+Shift+O]";
            this.tbtnItalic.ToolTipText = LMP.Resources.GetLangString("Dialog_RelineItalicTooltip") + " [Ctrl+Shift+I]";
            this.tbtnUnderline.ToolTipText = LMP.Resources.GetLangString("Dialog_RelineUnderlineTooltip") + " [Ctrl+Shift+U]";
            this.tbtnUnderlineLast.ToolTipText = LMP.Resources.GetLangString("Dialog_RelineUnderlineLastTooltip") + " [Ctrl+Shift+L]";
            this.tbtnReline.ToolTipText = this.tbtnReline.ToolTipText + " [Ctrl+Shift+1]";
            this.tbtnSpecial.ToolTipText = this.tbtnSpecial.ToolTipText + " [Ctrl+Shift+2]";
            if (!this.DesignMode)
            {
                this.mnuRelineClear.Text = LMP.Resources.GetLangString("Dialog_Reline_Clear");
                //GLOG : 6318 : CEH
                //GLOG : 6297 : CEH
                this.mnuRelinePaste.Text = LMP.Resources.GetLangString("Dialog_Reline_Paste");
                this.mnuRelineCopy.Text = LMP.Resources.GetLangString("Dialog_Reline_Copy");
                this.mnuRelineCut.Text = LMP.Resources.GetLangString("Dialog_Reline_Cut");
                this.mnuSpecialPaste.Text = LMP.Resources.GetLangString("Dialog_Reline_Paste");
                this.mnuSpecialCopy.Text = LMP.Resources.GetLangString("Dialog_Reline_Copy");
                this.mnuSpecialCut.Text = LMP.Resources.GetLangString("Dialog_Reline_Cut");
                this.mnuSpecialClearRow.Text = LMP.Resources.GetLangString("Dialog_Reline_ClearRow");
                this.mnuSpecialDeleteAll.Text = LMP.Resources.GetLangString("Dialog_Reline_DeleteAll");
                this.mnuSpecialDeleteRow.Text = LMP.Resources.GetLangString("Dialog_Reline_DeleteRow");
                this.mnuSpecialInsertRow.Text = LMP.Resources.GetLangString("Dialog_Reline_InsertRow");
                this.mnuSpecialAddRow.Text = LMP.Resources.GetLangString("Dialog_Reline_AddRow");
            }
        }
		/// <summary>
		/// resizes reline textbox and Special grid to reflect form height
		/// </summary>
		private void ResizeControls()
		{
            LMP.Trace.WriteInfo("ResizeControls");
            if (m_iStyle == mpRelineStyles.ShowBoth)
            {
                this.tbtnReline.Visible = true;
                this.tsep1.Visible = true;
                this.tbtnSpecial.Visible = true;
                this.tsep2.Visible = true;
                //this.grdSpecial.Visible = true;
                //this.txtReline.Visible = true;
                this.tsOptions.Visible = true;
            }
            else
            {
                this.tsep1.Visible = false;
                this.tsep2.Visible = false;
                this.tbtnSpecial.Visible = false;
                this.tbtnReline.Visible = false;
                this.grdSpecial.Visible = (m_iStyle == mpRelineStyles.ShowSpecialOnly);
                this.txtReline.Visible = (m_iStyle == mpRelineStyles.ShowRelineOnly);
                if (m_iStyle == mpRelineStyles.ShowRelineOnly)
                {
                    this.tsOptions.Visible = (((m_iFormatOptions & mpRelineFormatOptions.RelineBold) == mpRelineFormatOptions.RelineBold) ||
                        ((m_iFormatOptions & mpRelineFormatOptions.RelineItalic) == mpRelineFormatOptions.RelineItalic) ||
                        ((m_iFormatOptions & mpRelineFormatOptions.RelineUnderline) == mpRelineFormatOptions.RelineUnderline) ||
                        ((m_iFormatOptions & mpRelineFormatOptions.RelineUnderlineLast) == mpRelineFormatOptions.RelineUnderlineLast));
                }
                else
                {
                    this.tsOptions.Visible = (((m_iFormatOptions & mpRelineFormatOptions.SpecialBold) == mpRelineFormatOptions.SpecialBold) ||
                        ((m_iFormatOptions & mpRelineFormatOptions.SpecialItalic) == mpRelineFormatOptions.SpecialItalic) ||
                        ((m_iFormatOptions & mpRelineFormatOptions.SpecialUnderline) == mpRelineFormatOptions.SpecialUnderline) ||
                        ((m_iFormatOptions & mpRelineFormatOptions.SpecialUnderlineLast) == mpRelineFormatOptions.SpecialUnderlineLast));
                }
            }

            //GLOG : 8532 : ceh
            int iHeight = 0;
            if (this.tsOptions.Visible)
                iHeight = this.Height - this.tsOptions.Height;
            else
                iHeight = this.Height;

            this.txtReline.Height = iHeight;

            //adjust options location
            if (this.tsOptions.Visible)
                this.tsOptions.Top = this.txtReline.Top + this.txtReline.Height + (int)(1 * LMP.OS.GetScalingFactor());
                
            //this.grdSpecial.Height = this.txtReline.Height;
            //this.tsOptions.Width = this.Width;
		}
        private void DisplayFormatOptions()
        {
            // Do nothing if toolstrip is not visible
            if (!this.tsOptions.Visible)
                return;
            if (txtReline.Visible == true)
            {
                if ((m_iFormatOptions & mpRelineFormatOptions.RelineBold) == mpRelineFormatOptions.RelineBold)
                {
                    this.tbtnBold.Visible = true;
                    this.tsep3.Visible = true;
                    this.tbtnBold.Checked = (m_iFormatValues & mpRelineFormatOptions.RelineBold) == mpRelineFormatOptions.RelineBold;
                }
                else
                {
                    this.tbtnBold.Visible = false;
                    this.tsep3.Visible = false;
                }

                if ((m_iFormatOptions & mpRelineFormatOptions.RelineItalic) == mpRelineFormatOptions.RelineItalic)
                {
                    this.tbtnItalic.Visible = true;
                    this.tsep4.Visible = true;
                    this.tbtnItalic.Checked = (m_iFormatValues & mpRelineFormatOptions.RelineItalic) == mpRelineFormatOptions.RelineItalic;
                }
                else
                {
                    this.tbtnItalic.Visible = false;
                    this.tsep4.Visible = false;
                }

                if ((m_iFormatOptions & mpRelineFormatOptions.RelineUnderline) == mpRelineFormatOptions.RelineUnderline)
                {
                    this.tbtnUnderline.Visible = true;
                    this.tsep5.Visible = true;
                    this.tbtnUnderline.Checked = (m_iFormatValues & mpRelineFormatOptions.RelineUnderline) == mpRelineFormatOptions.RelineUnderline;
                }
                else
                {
                    this.tbtnUnderline.Visible = false;
                    this.tsep5.Visible = false;
                }

                if ((m_iFormatOptions & mpRelineFormatOptions.RelineUnderlineLast) == mpRelineFormatOptions.RelineUnderlineLast)
                {
                    this.tbtnUnderlineLast.Visible = true;
                    this.tsep6.Visible = true;
                    this.tbtnUnderlineLast.Checked = (m_iFormatValues & mpRelineFormatOptions.RelineUnderlineLast) == mpRelineFormatOptions.RelineUnderlineLast;
                }
                else
                {
                    this.tbtnUnderlineLast.Visible = false;
                    this.tsep6.Visible = false;
                }

            }
            else if (grdSpecial.Visible)
            {
                if ((m_iFormatOptions & mpRelineFormatOptions.SpecialBold) == mpRelineFormatOptions.SpecialBold)
                {
                    this.tbtnBold.Visible = true;
                    this.tsep3.Visible = true;
                    this.tbtnBold.Checked = (m_iFormatValues & mpRelineFormatOptions.SpecialBold) == mpRelineFormatOptions.SpecialBold;
                }
                else
                {
                    this.tbtnBold.Visible = false;
                    this.tsep3.Visible = false;
                }

                if ((m_iFormatOptions & mpRelineFormatOptions.SpecialItalic) == mpRelineFormatOptions.SpecialItalic)
                {
                    this.tbtnItalic.Visible = true;
                    this.tsep4.Visible = true;
                    this.tbtnItalic.Checked = (m_iFormatValues & mpRelineFormatOptions.SpecialItalic) == mpRelineFormatOptions.SpecialItalic;
                }
                else
                {
                    this.tbtnItalic.Visible = false;
                    this.tsep4.Visible = false;
                }
                if ((m_iFormatOptions & mpRelineFormatOptions.SpecialUnderline) == mpRelineFormatOptions.SpecialUnderline)
                {
                    this.tbtnUnderline.Visible = true;
                    this.tsep5.Visible = true;
                    this.tbtnUnderline.Checked = (m_iFormatValues & mpRelineFormatOptions.SpecialUnderline) == mpRelineFormatOptions.SpecialUnderline;
                }
                else
                {
                    this.tbtnUnderline.Visible = false;
                    this.tsep5.Visible = false;
                }

                if ((m_iFormatOptions & mpRelineFormatOptions.SpecialUnderlineLast) == mpRelineFormatOptions.SpecialUnderlineLast)
                {
                    this.tbtnUnderlineLast.Visible = true;
                    this.tsep6.Visible = true;
                    this.tbtnUnderlineLast.Checked = (m_iFormatValues & mpRelineFormatOptions.SpecialUnderlineLast) == mpRelineFormatOptions.SpecialUnderlineLast;
                }
                else
                {
                    this.tbtnUnderlineLast.Visible = false;
                    this.tsep6.Visible = false;
                }

            }
            // Hide label if no buttons are shown
            this.tlblSpacer.Visible = (this.tbtnBold.Visible || this.tbtnItalic.Visible ||
                this.tbtnUnderline.Visible || this.tbtnUnderlineLast.Visible);
        }
		/// <summary>
		/// tabs out of grid if the user is tabbing out
		/// of the last cell - called only by grdSpecial_KeyDown
		/// </summary>
        private bool TabOutIfAppropriate()
        {
            bool bTabHandled = false;
            bool bIsAppropriate = false; //9-2-11 (dm)

            if (this.grdSpecial.ActiveCell == null)
                //9-2-11 (dm) - an object reference error was occurring because there
                //may be mo active cell if you revisit the control, toggle one of the
                //formatting buttons, and press tab
                bIsAppropriate = true;
            else
            {
                //check whether user is in last cell of grid
                int iCurRow = this.grdSpecial.ActiveCell.Row.Index;
                int iCol = this.grdSpecial.ActiveCell.Column.Index;
                //check if in empty new row
                int iRows = ((DataSet)this.grdSpecial.DataSource).Tables["Table"].Rows.Count;
                bIsAppropriate = ((iCurRow == iRows - 1) && (iCol == 1));
            }

            if (bIsAppropriate)
            {
                //notify subscribers that the tab was pressed
                if (this.TabPressed != null)
                {
                    this.TabPressed(this, new TabPressedEventArgs(false));
                    bTabHandled = true;
                }
            }
            return bTabHandled;
        }

		/// <summary>
		/// tabs out of grid if the
		/// user is shift-tabbing out of the first cell-
		/// called only by grdSpecial_KeyDown
		/// </summary>
		private bool ShiftTabOutIfAppropriate()
		{
            bool bTabHandled = false;
            int iCurRow = this.grdSpecial.ActiveCell.Row.Index;
            int iCol = this.grdSpecial.ActiveCell.Column.Index;
            //check if in empty new row
            int iRows = ((DataSet)this.grdSpecial.DataSource).Tables["Table"].Rows.Count;
            if ((iCurRow == 0) && (iCol == 0))
            {
                //user is in first cell of grid				
                //notify subscribers that the tab was pressed
                if (this.TabPressed != null)
                {
                    this.TabPressed(this, new TabPressedEventArgs(true));
                    bTabHandled = true;
                }
            }
            return bTabHandled;
        }

		/// <summary>
		/// adjusts selection in grid
		/// </summary>
		private void SetGridSelection()
		{
            if (m_iSelStart == -1)
            {
                //there's no existing selection - select first row

				//if first row has label but no value, select value cell -
				//otherwise, select label cell
                DataRowCollection oRows = ((DataSet)this.grdSpecial.DataSource).Tables["Table"].Rows;
                if (oRows.Count > 0)
                {
                    DataRow oRow = oRows[0];
                    string xLabel = oRow["Label"].ToString();
                    string xValue = oRow["Value"].ToString();
                    if ((xLabel != "") && (xValue == ""))
                        this.grdSpecial.ActiveCell = this.grdSpecial.Rows[0].Cells["Value"];
                    else
                    {
                        this.grdSpecial.ActiveCell = this.grdSpecial.Rows[0].Cells["Value"];
                    }
                    this.grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                }
			}
            else
            {
                //restore previous selection - grid default behavior is to select
                //entire cell when you reenter
                if (this.grdSpecial.ActiveCell != null)
                {
                    this.grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    //grdSpecial.ActiveCell.SelStart = m_iSelStart;
                    //grdSpecial.ActiveCell.SelLength = m_iSelLength;
                }
            }
		}

		private void SelectPrefillMenuItem()
		{
			for (int i=m_iFixedMenuCount; i < this.mnuSpecial.MenuItems.Count; i++) //GLOG 7807
			{
				MenuItem oItem = this.mnuSpecial.MenuItems[i];
				oItem.Checked = (oItem.Text == m_xSelectedPrefill);
			}
		}

		#endregion
		#region *********************event handlers*********************
		/// <summary>
		/// Handle Resize of subcontrols
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void Reline_Resize(object sender, System.EventArgs e)
		{
			try
			{
                LMP.Trace.WriteInfo("Reline_Resize");
                //GLOG : 8532 : ceh
                if (m_bConstructed && (m_iCurrentHeight != this.Height))
                {
                    m_iCurrentHeight = (int)(this.Height * LMP.OS.GetScalingFactor());
                    this.Height = m_iCurrentHeight;
                }

                if (m_bInitialized)
                    ResizeControls();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		private void grdSpecial_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			try
			{
                //if tab is handled in TabOutIfAppropriate function
                //suppress the keystroke - deals with Office 2003 issue
                if (e.KeyCode == Keys.Tab && e.Shift == false)
                {
                    if (TabOutIfAppropriate())
                    {
                        e.SuppressKeyPress = true;
                    }
                    else
                    {
                        if (this.grdSpecial.ActiveRow.Index < this.grdSpecial.Rows.Count - 1 && this.grdSpecial.ActiveCell.Column.Index == 1)
                        {
                            // Goto the next row
                            this.grdSpecial.ActiveRow = this.grdSpecial.Rows[this.grdSpecial.ActiveRow.Index + 1];
                            this.grdSpecial.ActiveCell = this.grdSpecial.ActiveRow.Cells[1];

                            // Enter edit mode within the cell.
                            grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            e.SuppressKeyPress = true;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Tab && e.Shift == true)
                {
                    if (ShiftTabOutIfAppropriate())
                    {
                        e.SuppressKeyPress = true;
                    }
                    else
                    {
                        if (this.grdSpecial.ActiveRow.Index > 0 && this.grdSpecial.ActiveCell.Column.Index == 1)
                        {
                            // Goto the previous row
                            this.grdSpecial.ActiveRow = this.grdSpecial.Rows[this.grdSpecial.ActiveRow.Index - 1];
                            this.grdSpecial.ActiveCell = this.grdSpecial.ActiveRow.Cells[1];

                            // Enter edit mode within the cell.
                            grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            e.SuppressKeyPress = true;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell);
                    grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    e.SuppressKeyPress = true;
                }
                else
                {
                    HandleShortCutKeys(e);
                    if (this.KeyPressed != null && !m_bShortCutKeyPressed)
                        this.KeyPressed(this, e);
                }

            }
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Clear Reline text 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuRelineClear_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.txtReline.Text = "";
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

        //GLOG : 6318 : CEH
        /// <summary>
        /// Paste text inside Reline Textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuRelinePaste_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtReline.SelectedText = Clipboard.GetText();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6297 : CEH
        private void mnuRelineCopy_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.txtReline.SelectedText, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuRelineCut_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.txtReline.SelectedText, true);
                //cut
                this.txtReline.SelectedText = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuReline_Popup(object sender, EventArgs e)
        {
            try
            {
                //GLOG : 6297 : CEH
                //enable/disable copy & cut context menu items
                mnuRelineCopy.Enabled = (this.txtReline.SelectedText != "");
                mnuRelineCut.Enabled = (this.txtReline.SelectedText != "");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Clear selected row in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuSpecialClearRow_Click(object sender, System.EventArgs e)
		{
			try
			{
				//update data set with latest grid values -
				//this is an essential part of refreshing
				this.grdSpecial.UpdateData();

				//clear row
				int iRow = this.grdSpecial.ActiveCell.Row.Index;
				DataSet oDS = (DataSet) this.grdSpecial.DataSource;
				DataRow oRow = oDS.Tables["Table"].Rows[iRow];
				oRow["Label"] = "";
				oRow["Value"] = "";

				//refresh
				this.grdSpecial.Rows[iRow].Refresh();
				this.grdSpecial.ActiveCell =  this.grdSpecial.Rows[iRow].Cells["Label"];
                this.grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        private void grdSpecial_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.KeyReleased != null && !m_bShortCutKeyPressed)
                //raise keyreleased event
                this.KeyReleased(this, e);
        }

        private void txtReline_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.KeyReleased != null && !m_bShortCutKeyPressed)
                //raise keyreleased event
                this.KeyReleased(this, e);
        }
        /// <summary>
        /// Delete selected row in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuSpecialDeleteRow_Click(object sender, System.EventArgs e)
		{
			try
			{
				//update data set with latest grid values -
				//this is an essential part of refreshing
				this.grdSpecial.UpdateData();

				int iRow = this.grdSpecial.ActiveRow.Index;
				DataSet oDS = (DataSet) this.grdSpecial.DataSource;
				DataTable oDT = oDS.Tables["Table"];
				DataRow oRow = oDT.Rows[iRow];
				if (oDT.Rows.Count > 1)
					//delete row
					oRow.Delete();
				else
				{
					//there's only one row in the table - just clear it
					oRow["Label"] = "";
					oRow["Value"] = "";
				}
                oDT.AcceptChanges();
				//refresh
				this.grdSpecial.Refresh();
                iRow = Math.Min(iRow, grdSpecial.Rows.Count - 1);
				this.grdSpecial.ActiveCell = grdSpecial.Rows[iRow].Cells["Label"];
                this.grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Clear all rows in grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuSpecialDeleteAll_Click(object sender, System.EventArgs e)
		{
			try
			{
				//update data set with latest grid values -
				//this is an essential part of refreshing
				this.grdSpecial.UpdateData();

				//clear data table
				DataSet oDS = (DataSet) this.grdSpecial.DataSource;
				DataTable oDT = oDS.Tables["Table"];
				oDT.Clear();

				//add one empty row
				oDT.Rows.Add(new object[]{"", ""});
                oDT.AcceptChanges();
				//refresh
				this.grdSpecial.Refresh();
                this.grdSpecial.ActiveCell = this.grdSpecial.Rows[0].Cells["Label"];
                this.grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
        }
        /// <summary>
        /// Add row to end of grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSpecialAddRow_Click(object sender, EventArgs e)
        {
            try
            {
                //insert new row
                DataSet oDS = (DataSet)this.grdSpecial.DataSource;
                DataTable oDT = oDS.Tables["Table"];
                // Add new row to end
                oDT.Rows.Add(new object[] { "", "" });
                this.grdSpecial.Refresh();
                this.grdSpecial.ActiveCell = this.grdSpecial.Rows[oDT.Rows.Count - 1].Cells["Label"];
                this.grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Insert row in the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuSpecialInsertRow_Click(object sender, System.EventArgs e)
		{
			try
			{
                //insert new row
                this.grdSpecial.UpdateData();
                int iNewRow = grdSpecial.ActiveRow.Index;
				DataSet oDS = (DataSet) this.grdSpecial.DataSource;
				DataTable oDT = oDS.Tables["Table"];
                if (iNewRow > oDT.Rows.Count || iNewRow < 0)
                {
                    // Add new row to end
                    oDT.Rows.Add(new object[] { "", "" });
                    iNewRow = oDT.Rows.Count - 1;
                }
                else
                {
                    // Insert before current row
                    DataRow oNewRow = oDT.NewRow();
                    oDT.Rows.InsertAt(oNewRow, iNewRow);
                }
                oDT.AcceptChanges();
                this.grdSpecial.Refresh();
				this.grdSpecial.ActiveCell = this.grdSpecial.Rows[iNewRow].Cells["Label"];
                this.grdSpecial.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                this.IsDirty = true;
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Prefill definition has been selected from menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void mnuSpecialPrefillItem_Click(Object sender, EventArgs e)
		{
			try
			{
				//get values for selected prefill
				MenuItem oMenuItem = (MenuItem) sender;
				int iIndex = (oMenuItem.Index - m_iFixedMenuCount); //GLOG 7807

				//populate grid
				PrefillGrid(iIndex);

				//update menu selection
				m_xSelectedPrefill = oMenuItem.Text;
				SelectPrefillMenuItem();

				//select appropriate cell
				m_iSelStart = -1;
				SetGridSelection();
                this.IsDirty = true;
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Special reline grid has lost focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void grdSpecial_Leave(object sender, System.EventArgs e)
		{
			try
			{
				//store current selection
                if (this.grdSpecial.ActiveCell != null && this.grdSpecial.ActiveCell.IsInEditMode)
                {
                    m_iSelStart = this.grdSpecial.ActiveCell.SelStart;
                    m_iSelLength = this.grdSpecial.ActiveCell.SelLength;
                }
                else
                    m_iSelStart = -1;
            }
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Special reline grid has received focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void grdSpecial_Enter(object sender, System.EventArgs e)
		{
			try
			{
                //GLOG 4770: Initialize tab navigation so that Tab will move
                //to next control when in last cell and there is no TabPressed handler
                if (this.TabPressed != null)
                    this.grdSpecial.DisplayLayout.TabNavigation = Infragistics.Win.UltraWinGrid.TabNavigation.NextCell;
                else
                    this.grdSpecial.DisplayLayout.TabNavigation = Infragistics.Win.UltraWinGrid.TabNavigation.NextControlOnLastCell;
                //initialize/restore selection
				SetGridSelection();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Handle tab press in Reline textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtReline_TabPressed(object sender, TabPressedEventArgs e)
        {
            try
            {
                //notify subscribers that the tab was pressed
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(e.ShiftPressed));
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        private void txtReline_KeyDown(object sender, KeyEventArgs e)
        {
            HandleShortCutKeys(e);

            if (this.KeyPressed != null)
                this.KeyPressed(this, e);

        }
        /// <summary>
        /// Control is being loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reline_Load(object sender, EventArgs e)
        {
            try
            {
                SetupResourceStrings();
                if (this.DesignMode)
                    ExecuteFinalSetup();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Value of Reline textbox has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtReline_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    this.IsDirty = true;

                    //notify that value has changed, if necessary
                    if (ValueChanged != null)
                        ValueChanged(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Contents of Special reline grid have changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdSpecial_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    this.IsDirty = true;

                    //notify that value has changed, if necessary
                    if (ValueChanged != null)
                        ValueChanged(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Checked state of Bold format button has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnBold_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    if (tbtnBold.Checked)
                    {
                        if (this.txtReline.Visible)
                            this.FormatValues = this.FormatValues | mpRelineFormatOptions.RelineBold;
                        else
                            this.FormatValues = this.FormatValues | mpRelineFormatOptions.SpecialBold;

                    }
                    else
                    {
                        if (this.txtReline.Visible)
                            this.FormatValues = this.FormatValues & ~mpRelineFormatOptions.RelineBold;
                        else
                            this.FormatValues = this.FormatValues & ~mpRelineFormatOptions.SpecialBold;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// Checked state of Italic format button has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnItalic_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    if (tbtnItalic.Checked)
                    {
                        if (this.txtReline.Visible)
                            this.FormatValues = this.FormatValues | mpRelineFormatOptions.RelineItalic;
                        else
                            this.FormatValues = this.FormatValues | mpRelineFormatOptions.SpecialItalic;

                    }
                    else
                    {
                        if (this.txtReline.Visible)
                            this.FormatValues = this.FormatValues & ~mpRelineFormatOptions.RelineItalic;
                        else
                            this.FormatValues = this.FormatValues & ~mpRelineFormatOptions.SpecialItalic;

                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Checked state of Underline format button has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnUnderline_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    if (tbtnUnderline.Checked)
                    {
                        if (this.txtReline.Visible)
                            this.FormatValues = this.FormatValues | mpRelineFormatOptions.RelineUnderline;
                        else
                            this.FormatValues = this.FormatValues | mpRelineFormatOptions.SpecialUnderline;
                        // Only one underline option can be checked at a time
                        if (this.tbtnUnderlineLast.Checked)
                            this.tbtnUnderlineLast.Checked = false;
                    }
                    else
                    {
                        if (this.txtReline.Visible)
                            this.FormatValues = this.FormatValues & ~mpRelineFormatOptions.RelineUnderline;
                        else
                            this.FormatValues = this.FormatValues & ~mpRelineFormatOptions.SpecialUnderline;

                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Checked state of Underline Last format button has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnUnderlineLast_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    if (tbtnUnderlineLast.Checked)
                    {
                        if (this.txtReline.Visible)
                            this.FormatValues = this.FormatValues | mpRelineFormatOptions.RelineUnderlineLast;
                        else
                            this.FormatValues = this.FormatValues | mpRelineFormatOptions.SpecialUnderlineLast;
                        // Only one underline option can be checked at a time
                        if (this.tbtnUnderline.Checked)
                            this.tbtnUnderline.Checked = false;
                    }
                    else
                    {
                        if (this.txtReline.Visible)
                            this.FormatValues = this.FormatValues & ~mpRelineFormatOptions.RelineUnderlineLast;
                        else
                            this.FormatValues = this.FormatValues & ~mpRelineFormatOptions.SpecialUnderlineLast;

                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void tsTabReline_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.tbtnReline.Checked)
                {
                    DisplayTab(mpRelineTabs.Reline);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tsTabSpecial_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.tbtnSpecial.Checked)
                {
                    DisplayTab(mpRelineTabs.Special);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }


        /// <summary>
        /// Special context menu is about to be displayed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSpecial_Popup(object sender, EventArgs e)
        {
            int iRow = -1;
            try
            {
                iRow = grdSpecial.ActiveRow.Index;
            }
            catch {}
            try
            {
                // Don't display insert if no row is selected
                if (iRow > -1)
                    mnuSpecialInsertRow.Visible = true;
                else
                    mnuSpecialInsertRow.Visible = false;

                //GLOG : 6297 : CEH
                //enable/disable copy & cut context menu items
                //GLOG 7807: Only display in appropriate mode/trap for unexpected errors
                try
                {
                    if (this.grdSpecial.ActiveCell != null && this.grdSpecial.ActiveCell.IsInEditMode)
                    {
                        mnuSpecialCopy.Enabled = (this.grdSpecial.ActiveCell.SelText != null && this.grdSpecial.ActiveCell.SelText != "");
                        mnuSpecialCut.Enabled = (this.grdSpecial.ActiveCell.SelText != null && this.grdSpecial.ActiveCell.SelText != "");
                        mnuSpecialPaste.Enabled = true;
                    }
                    else
                    {
                        mnuSpecialCopy.Enabled = false;
                        mnuSpecialCut.Enabled = false;
                        mnuSpecialPaste.Enabled = false;
                    }
                }
                catch
                {
                    mnuSpecialCopy.Enabled = false;
                    mnuSpecialCut.Enabled = false;
                    mnuSpecialPaste.Enabled = false;
                }

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        //GLOG : 6297 : CEH
        private void mnuSpecialPaste_Click(object sender, EventArgs e)
        {
            try
            {
                this.grdSpecial.ActiveCell.SelText = Clipboard.GetText();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuSpecialCopy_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.grdSpecial.ActiveCell.SelText, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuSpecialCut_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.grdSpecial.ActiveCell.SelText, true);
                //cut
                this.grdSpecial.ActiveCell.SelText = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }


        private void grdSpecial_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!this.DesignMode && m_bInitialized)
                {
                    this.IsDirty = true;

                    //notify that value has changed, if necessary
                    if (ValueChanged != null)
                        ValueChanged(this, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
		#region *********************IControl members*********************
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;
        public event EnterKeyPressedHandler EnterKeyPressed;

        public void ExecuteFinalSetup()
		{
			try
			{
                LMP.Trace.WriteInfo("ExecuteFinalSetup");
                ResizeControls();
                DisplayFormatOptions();
                if (!this.DesignMode)
				{
					if (m_iStyle != mpRelineStyles.ShowRelineOnly)
					{
						//inititialize Special grid
						InitializeGrid();
						ConfigurePrefillMenu();
					}
                    if (m_iStyle == mpRelineStyles.ShowBoth)
                    {
                        if (this.DefaultTab == mpRelineTabs.Reline)
                        {
                            this.DisplayTab(mpRelineTabs.Reline);
                            this.tbtnReline.CheckState = CheckState.Checked;
                        }
                        else
                        {
                            this.DisplayTab(mpRelineTabs.Special);
                            this.tbtnSpecial.CheckState = CheckState.Checked;
                        }
                    }
				}
				m_bInitialized = true;
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.ControlEventException(
					LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
					"Reline Control", oE);
			}
		}
		[DescriptionAttribute("Gets/Sets the selected value.")]
		public string Value
		{
			get
			{
                return this.ContentString;
			}
			set
			{
                this.ContentString = value;
                this.IsDirty = true;
			}
		}
		public bool IsDirty
		{
			set{m_bIsDirty = value;}
			get{return m_bIsDirty;}
		}
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
		#region *********************protected members*********************
		public event TabPressedHandler TabPressed;
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Return)
                // Need this so that Enter will generate WM_KEYDOWN for EnterKeyPressed function
                return true;
            else if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
		/// <summary>
		/// processes tab and shift-tab key messages -
		/// this method will be executed only when the
		/// message pump is broken, ie when the hosing
		/// form's ProcessDialogKey method is not run.
		/// this seems to happen when when tabbing from
		/// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            int iQualifierKeys;
            bool bShiftPressed;
            bool bEnterKeyPressed = LMP.OS.EnterKeyPressed(m, out iQualifierKeys);
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));
                //return that key was processed
                return true;
            }
            else if (bEnterKeyPressed)
            {
                if (this.EnterKeyPressed != null)
                    this.EnterKeyPressed(this, new EnterKeyPressedEventArgs(iQualifierKeys));
                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        protected override void OnCreateControl()
		{
            base.OnCreateControl();
            //handle tab pressed event of constituent textbox
			this.txtReline.TabPressed += new TabPressedHandler(txtReline_TabPressed);
		}
		#endregion

        private void Reline_Enter(object sender, EventArgs e)
        {
            base.OnGotFocus(e);
        }

        private void Reline_Leave(object sender, EventArgs e)
        {
            base.OnLostFocus(e);
        }

        /// <summary>
        /// GLOG 8839: Allow entering non-breaking hyphen directly in control using Ctrl+Shift+- shortcut
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reline_KeyPress(object sender, KeyPressEventArgs e)
        {
            //standard control behavior is to insert Ascii 31 (optional hyphen)
            if (e.KeyChar == 31 && ModifierKeys == (Keys.Control | Keys.Shift))
            {
                //replace with Unicode 8209 to display in control
                e.KeyChar = (char)8209;
            }
        }

    }
}
