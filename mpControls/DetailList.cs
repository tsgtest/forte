using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Xml;
using System.Reflection;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.Controls
{
	/// <summary>
	/// MacPac detail list control - handles 
	/// lists of recipients, e.g. cc and bcc - 
	/// created by Daniel Fisherman 03/05
	/// </summary>
	public partial class DetailList : System.Windows.Forms.UserControl, IControl,ICIable
	{
        #region *********************fields*********************
        private int m_iRowsPerItem = 1;
        private int m_iVisibleRows = 4;
        private int m_iFirstIndex = 0;
		private bool m_bShowGridlines = false;
        private bool m_bShowCIButton = false;
		private bool m_bAutoSize = false;
		private float m_fHeight = 0;
        private bool m_bControlIsLoaded = false;
		private bool m_bScrolling = false;
		private ArrayList m_aEntries = new ArrayList();
		private bool m_bTabPressed = false;
		private int m_iCurIndex = 0;
		private string m_xCIToken;
        private string m_xFieldName = "Item";
		private bool m_bStopScrollBarUpdates = false;
		private string m_xContentString = "";
        private string m_xCounterScheme = "Item %0 of %1";
        private Control m_oLastActiveControl = null;

		public event DetailAddedHandler DetailAdded;
		public event DetailDeletedHandler DetailDeleted;
		public event DetailEditedHandler DetailEdited;
		public event System.EventHandler DetailCleared;
		public event SubControlChangedEventHandler SubControlChanged;
		public event SubControlKeyDownEventHandler SubControlKeyDown;
		public event SubControlMouseDownEventHandler SubControlMouseDown;
		
		private bool m_bIsDirty = false;
        private bool m_bDetailDirty = false;
        private object m_oTag2;
        private Control m_oLastTabControl;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************constructors*********************
        public DetailList()
		{
			InitializeComponent();
            //this.Height = 58;
		}
		#endregion
		#region *********************properties*********************
		[DescriptionAttribute("Gets/sets the CI detail token detail for added CI contacts.")]
		public string CIToken
		{
			get{return m_xCIToken;}
			set
            {
                // If CI Token is specified, make that the XML Node name matches
                m_xCIToken = value;
                if (value != "")
                {
                    m_xFieldName = m_xCIToken.Replace("<", "").Replace(">", "");
                    m_bShowCIButton = true;
                }
                else
                {
                    m_xFieldName = "Item";
                    m_bShowCIButton = false;
                }
            }
		}

        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Gets/sets the number of rows per detail item.")]
        public int RowsPerItem
        {
            get { return m_iRowsPerItem; }
            set
            {
                // Property can not be changed at runtime
                if (m_bControlIsLoaded && !this.DesignMode)
                    return;
                // Always 1 if AutoSize==true
                if (m_bAutoSize)
                    value = 1;

                // Check withing min/max bounds
                if (value > 10)
                    value = 10;
                else if (value < 1)
                    value = 1;

                if (m_iRowsPerItem != value)
                {
                    m_iRowsPerItem = value;

                    //set up if control has already been setup
                    if (m_bControlIsLoaded)
                        this.SetupControl();
                }
            }
        }

        [CategoryAttribute("Appearance")]
        [DescriptionAttribute("Gets/sets the number of rows displayed at once.")]
		public int VisibleRows
		{
			get{return m_iVisibleRows;}
			set
			{
                // Property can not be changed at runtime
                if (m_bControlIsLoaded && !this.DesignMode)
                    return;
                // Check withing min/max bounds
                if (value > 20)
                    value = 30;
                else if (value < 1)
                    value = 1;
                if (m_iVisibleRows != value)
                {
                    //Can not be less the height of a single item
                    m_iVisibleRows = Math.Max(value, m_iRowsPerItem);

                    //set up if control has already been setup
                    if (m_bControlIsLoaded)
                        this.SetupControl();
                }
			}
		}
		[DescriptionAttribute("Gets/sets whether or not a row will automatically resize to fit it's contents.")]
		public override bool AutoSize
		{
			get{return m_bAutoSize;}
			set
			{
                // Property can not be changed at runtime
                if (m_bControlIsLoaded && !this.DesignMode)
                    return;
                m_bAutoSize = value;
                m_iRowsPerItem = 1;
				//set up if control has already been setup
				if(m_bControlIsLoaded)
					this.SetupControl();
			}
		}
        [Browsable(false)]
        [DescriptionAttribute("Gets/sets the string that populates this control.")]
		private string ContentString
		{
			get{return ToXML();}
			set
			{
				if(this.DesignMode)
					return;

				if(value != m_xContentString)
				{
					Trace.WriteNameValuePairs("value", value);


                    //convert delimited string or non-XML string to formatted XML
                    if (!string.IsNullOrEmpty(value) && value.IndexOf("<") != 0)
                    {
                        m_xContentString = DelimitedStringToXML(value);
                        this.IsDirty = true;
                    }
                    else
                    {
                        m_xContentString = value;
                        this.IsDirty = true;
                    }

					if(!m_bControlIsLoaded)
						return;

					if(!this.DesignMode)
					{
						//populate array
						this.FillDetailArray();

						//update detail content
						this.RefreshDisplay();

					}
				}
			}
		}

		[DescriptionAttribute("Gets/sets the token string that determines the format of the counter.")]
		public string CounterScheme
		{
			get{return m_xCounterScheme;}
			set{m_xCounterScheme = value;}
		}

		[DescriptionAttribute("Gets/sets whether rows will be separated by gridlines.")]
		public bool ShowGridlines
		{
			get{return m_bShowGridlines;}
			set
			{
				if(m_bShowGridlines != value)
				{
                    // Property can not be changed at runtime
                    if (m_bControlIsLoaded && !this.DesignMode)
                        return;
                    m_bShowGridlines = value;
                    if (m_bControlIsLoaded)
					    this.SetupControl();
				}
			}
		}

        private bool DetailDirty
        {
			get{return m_bDetailDirty;}
			set{m_bDetailDirty = value;}
        }
        #endregion
		#region *********************methods*********************
        // GLOG : 3485 : JAB
        // Update the view of the move up/down buttons.
        private void UpdateMoveButtonsView()
        {
            this.tbtnMoveUp.Enabled = (this.m_iCurIndex > 0 && m_iCurIndex < m_aEntries.Count);
            this.tbtnMoveDown.Enabled = (this.m_iCurIndex < this.m_aEntries.Count - 1);
        }
        //GLOG 3954: Add New toolbar button clicked
        private void tbtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                MoveToAddRow();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void MoveToAddRow()
        {
            MoveToRow(Math.Max(m_aEntries.Count - m_iCurIndex, 1), true);
        }
		#endregion
		#region *********************internal procedures*********************
        /// <summary>
        /// Set text of interface items
        /// </summary>
        private void SetupResourceStrings()
        {
            LMP.Trace.WriteInfo("SetupResourceStrings");
            tbtnAdd.ToolTipText = LMP.Resources.GetLangString("Prompt_AddNewEntry") + " [Ctrl+Shift+N]";
            tbtnDelete.ToolTipText = LMP.Resources.GetLangString("Prompt_DeleteCurrentEntry") + " [Ctrl+Shift+Del]";
            tbtnContacts.ToolTipText = LMP.Resources.GetLangString("Prompt_RetrieveContacts") + " [F2]";
            if (!this.DesignMode)
            {
                mnuContextMenu_MoveDown.Text = LMP.Resources.GetLangString("Menu_Detail_MoveItemDown");
                mnuContextMenu_MoveUp.Text = LMP.Resources.GetLangString("Menu_Detail_MoveItemUp");
                mnuContextMenu_DeleteAll.Text = LMP.Resources.GetLangString("Menu_Detail_DeleteAll");
                mnuContextMenu_DeleteDetail.Text = LMP.Resources.GetLangString("Menu_Detail_DeleteItem");
                mnuContextMenu_RefreshAll.Text = LMP.Resources.GetLangString("Menu_Detail_RefreshAll");
                mnuContextMenu_RefreshDetail.Text = LMP.Resources.GetLangString("Menu_Detail_RefreshItem");
            }

            // GLOG : 3485 : JAB
            // Set up tool tips for the move up/down button
            this.tbtnMoveUp.ToolTipText = LMP.Resources.GetLangString("Prompt_MoveEntryUp");
            this.tbtnMoveDown.ToolTipText = LMP.Resources.GetLangString("Prompt_MoveEntryDown");
        }

		/// <summary>
		/// sets up control
		/// </summary>
		private void SetupControl()
		{
			DateTime t0 = DateTime.Now;

			TextBox oRow = null;
			ControlCollection oCtls = this.pnlItems.Controls;
			oCtls.Clear();
			//create row controls to fill up panel
            for (int i = 1; i <= (m_iVisibleRows / m_iRowsPerItem); i++)
            {
				oRow = this.CreateRow();
            }
            this.Height = oRow.Top + oRow.Height + tsActions.Height + 8;
			this.RefreshDisplay();
//            this.pnlItems.Height = this.Height - this.tsActions.Height;
//            this.vsbFirstIndex.Height = this.pnlItems.Height;

			if(!this.DesignMode)
                this.tlblStatus.Text = "";

            this.tbtnContacts.Visible = m_bShowCIButton;
            this.tSep5.Visible = m_bShowCIButton;

            if(!this.DesignMode)
				LMP.Benchmarks.Print(t0);
		}
        /// <summary>
        /// returns the XML version of the supplied delimited string
        /// </summary>
        /// <param name="xValueString"></param>
        /// <returns></returns>
        private string DelimitedStringToXML(string xDelimitedString)
        {
            string xXMLPart = "";
            string xDelimitedPart = "";
            int iXMLStart = xDelimitedString.IndexOf('<');
            if (iXMLStart == 0)
                //String is already XML
                return xDelimitedString;
            else if (iXMLStart > -1)
            {
                //String contains XML appended to non-XML value
                xXMLPart = xDelimitedString.Substring(iXMLStart);
                xDelimitedPart = xDelimitedString.Substring(0, iXMLStart);
            }
            else
                //string contains no XML
                xDelimitedPart = xDelimitedString;

            string[] aItems = xDelimitedPart.Split('|');

            //cycle through string items, creating XML 
            //elements with attribute for CI ID
            XmlDocument oXML = new XmlDocument();

            //create temporary top level node
            oXML.LoadXml("<zzmpD></zzmpD>");

            int i = 0;

            foreach (string aItem in aItems)
            {
                i++;

                //create detail node
                XmlNode oDetailNode = oXML.CreateNode(
                    XmlNodeType.Element, m_xFieldName, "");

                oDetailNode.InnerText =
                    LMP.String.ReplaceXMLChars(aItem);

                // Create Index and UNID attributes of Detail
                XmlAttribute oA = oXML.CreateAttribute("Index");
                oA.InnerText = i.ToString();
                oDetailNode.Attributes.Append(oA);

                oA = oXML.CreateAttribute("UNID");
                oA.InnerText = "0";
                oDetailNode.Attributes.Append(oA);

                //add detail node
                oXML.DocumentElement.AppendChild(oDetailNode);
            }

            string xDetails = oXML.OuterXml;
            xDetails = xDetails.Replace("<zzmpD>", "");
            xDetails = xDetails.Replace("</zzmpD>", "");
            xDetails = xDetails.TrimEnd('\r', '\n');
            //Append original XML Portion
            if (xXMLPart != "")
                xDetails = xDetails + xXMLPart;
            return xDetails;
        }
		/// <summary>
		/// paints the gridlines between rows
		/// </summary>
		/// <param name="oG"></param>
		private void PaintGridlines(Graphics oG)
		{
			int iY = 0;
			//remove previous gridlines
			oG.Clear(this.pnlItems.BackColor);

			if(this.ShowGridlines)
			{
				//cycle through controls that should have values,
				//as well as the 'add new' row, drawing gridlines between controls
				for(int i = 0; i <= m_aEntries.Count - m_iFirstIndex; i++)
				{
					TextBox oTB = null;

					try
					{
						oTB = (TextBox) this.pnlItems.Controls[i];
					}
					catch
					{
						//there is no such textbox - stop drawing
						break;
					}

					//set line position to be between controls
					if(this.RowsPerItem == 1)
						iY = oTB.Top + oTB.Height + 3;
					else
						iY = oTB.Top + oTB.Height + 1;

                    if (iY > this.pnlItems.Height - 5)
                        //the position to draw the line is below the
                        //visible portion of the panel - stop drawing
                        break;
                    else
                        //draw the line
                        oG.DrawLine(new Pen(Brushes.Gainsboro, 1),
                            0, iY, this.pnlItems.Width, iY);
				}
			}
		}

		/// <summary>
		/// creates a TextBox for the control
		/// </summary>
		private TextBox CreateRow()
		{
			//add TextBox of specified height
			TextBox oRow = new TextBox();
				
			if(m_fHeight == 0)
			{
				//measure height of text, given current font
				Graphics oG = oRow.CreateGraphics();
				m_fHeight = oG.MeasureString("Ap", oRow.Font).Height;
			}

			this.pnlItems.Controls.Add(oRow);
            oRow.BackColor = pnlItems.BackColor;
            oRow.ForeColor = pnlItems.ForeColor;
			oRow.BorderStyle = BorderStyle.None;

			oRow.Width = this.pnlItems.Width;

			//set multiline
			oRow.Multiline = this.RowsPerItem > 1 || this.AutoSize;

			//resize TextBox appropriately - min height
			//has to be the lines/item * the height per row
			oRow.Height = Math.Max((int) (m_fHeight * this.RowsPerItem + 5), 20);

			//reposition control
			int i = this.pnlItems.Controls.Count - 1;

			if(i == 0)
				if(this.RowsPerItem == 1)
					oRow.Top = 1;
				else
					oRow.Top = 0;
			else
			{
				TextBox oPrevTB = (TextBox) this.pnlItems.Controls[i - 1];
				if(m_bShowGridlines)
					oRow.Top = oPrevTB.Top + oPrevTB.Height + 6;
				else
					oRow.Top = oPrevTB.Top + oPrevTB.Height + 2;
			}
			oRow.Left = 2;

			oRow.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;

			//sink event to resize TextBoxes
			oRow.TextChanged +=new EventHandler(HandleTextChanged);
			oRow.KeyDown += new KeyEventHandler(HandleKeyDown);
			oRow.Validating += new CancelEventHandler(HandleValidating);
			oRow.TabPressed += new TabPressedHandler(HandleTabPressed);
            oRow.PreviewKeyDown += new PreviewKeyDownEventHandler(HandlePreviewKeyDown);
			oRow.Leave += new EventHandler(HandleLeave);
			oRow.GotFocus += new EventHandler(HandleGotFocus);
			oRow.MouseDown += new MouseEventHandler(HandleMouseDown);
            oRow.EnterKeyPressed += new EnterKeyPressedHandler(ExecuteCodeForEnterPressed);
            oRow.KeyReleased += new KeyEventHandler(HandleKeyReleased);
            oRow.DoubleClick += new EventHandler(HandleDoubleClick);
            if (this.DesignMode)
                oRow.Enabled = false;
			return oRow;
		}
        private void ExecuteCodeForTabPressed(Control oSender, bool bShift)
        {

            // Tab press has already been processed, so don't duplicate
            if (m_oLastTabControl == oSender && oSender.Text != "")
                return;

            LMP.Trace.WriteInfo("ExecuteCodeForTabPressed");

            //determine if we're in the 'add new' row
            int iCurRowIndex = this.pnlItems.Controls.IndexOf(oSender);
            bool bInAddNew = iCurRowIndex + m_iFirstIndex >= m_aEntries.Count;

            if (iCurRowIndex == 0 && m_iCurIndex == 0 && (bShift || oSender.Text == ""))
            {
                //tab occurred in first row - tab out of control
                foreach (Control oCtl in this.pnlItems.Controls)
                    oCtl.TabStop = false;
                m_bTabPressed = true;
                return;
            }
            else if (bInAddNew && oSender.Text == "" && !bShift)
            {
                //tab occurred in empty 'add new' row - tab out of control
                foreach (Control oCtl in this.pnlItems.Controls)
                    oCtl.TabStop = false;
                m_bTabPressed = true;
                return;
            }
            // Mark this control as tab processor
            m_oLastTabControl = oSender;
            // Mark as handled so it's not duplicated by another event
            if (!bShift && this.MoveToAddRowIfAppropriate((TextBox)oSender))
            {
                m_bTabPressed = false;
            }
            else if (bShift)
            {
                m_bTabPressed = false;
                this.MoveToRow(-1, true);
            }
            else
            {
                m_bTabPressed = false;
                this.MoveToRow(1, true);
            }
        
        }
		/// <summary>
		/// updates the appropriate array entry 
		/// with the value in the specified DetailRow
		/// </summary>
		/// <param name="oRow"></param>
		private void UpdateEntry(TextBox oRow)
		{
			if(this.DesignMode)
				return;
            if (oRow == null || !this.DetailDirty)
                return;
            this.DetailDirty = false;
			//get index of current text box
			int iCurTextBoxIndex = this.pnlItems.Controls.IndexOf(oRow);

			//get index of entry
			int iEntryIndex = m_iFirstIndex + iCurTextBoxIndex;

			if(iEntryIndex < 0)
				return;
			
			if(oRow.Text == null || oRow.Text == "")
			{
				if(m_iFirstIndex + iCurTextBoxIndex <= m_aEntries.Count - 1)
				{
					//delete array entry for this item 
					m_aEntries.RemoveAt(iEntryIndex);

					//update display to reflect deletion
					this.RefreshDisplay();

					//raise event if necessary
					if(this.DetailDeleted != null)
						this.DetailDeleted((short) iEntryIndex);
				}
			}
			else
			{
				if(iEntryIndex > m_aEntries.Count - 1)
				{
					//there is no array entry for this item - add
					m_aEntries.Add(new DetailItem(oRow.Text));

					//raise event if necessary
					if(this.DetailAdded != null)
						this.DetailAdded((short) (m_aEntries.Count - 1));
				}
				else
				{	
					DetailItem oItem = (DetailItem) m_aEntries[iEntryIndex];
					if(oItem.Detail != oRow.Text)
					{
						//text is different - assign text to this array entry -
						//clear any UNID out, as once text changes, the contact is different
						oItem.Detail = oRow.Text;
						oItem.UNID = "0";
						m_aEntries[iEntryIndex] = oItem;

						//raise event if necessary
						if(this.DetailEdited != null)
							this.DetailEdited((short) iEntryIndex);
					}
				}
			}

            //raise event that value was changed
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());

			//set scroll bar max to number of entries
			this.vsbCurrentIndex.Maximum = Math.Max(m_aEntries.Count, 0);

			//force a repaint of gridlines
			this.pnlItems.Refresh();
		}

		/// <summary>
		/// sets the counter label when focus is on the control
		/// </summary>
		private void SetCounter(int iCurIndex)
		{
			string xLabel;
			int iTotal;
            int iCurrent;

			if(this.CounterScheme == "")
			{
                this.tlblStatus.Text = "";
                return;
			}

			//set counter
			if(iCurIndex > -1 && this.vsbCurrentIndex.Maximum > -1)
			{
                iCurrent = iCurIndex + 1;
                if(iCurIndex >= m_aEntries.Count)
					//in 'add new' row - need to temporarily include this entry in total
					iTotal = m_aEntries.Count + 1;
				else
					iTotal = m_aEntries.Count;
                xLabel = this.CounterScheme;
                xLabel = xLabel.Replace("%0", iCurrent.ToString());
                xLabel = xLabel.Replace("%1", iTotal.ToString());
			}
			else
				xLabel = "";

            this.tlblStatus.Text = xLabel;
		}

		/// <summary>
		/// refreshes all TextBox info, size, and layout
		/// </summary>
		private void RefreshDisplay()
		{
			DateTime t0 = DateTime.Now;

			int iSelStart = 0;
			int iSelLen = 0;
			bool bAllTextSelected = false;
            int iScrollStart = m_iFirstIndex;
			TextBox oCurRow = null;

			if(!this.DesignMode)
			{
				//get currently selected text
				oCurRow = (TextBox) this.ActiveControl;
				if(oCurRow != null)
				{
					iSelStart = oCurRow.SelectionStart;
					iSelLen = oCurRow.SelectionLength;
					bAllTextSelected = (iSelLen == oCurRow.Text.Length);
				}
			}

			//position/size controls
			for(int i = 0; (i < this.pnlItems.Controls.Count); i++)
			{
				TextBox oRow = (TextBox) this.pnlItems.Controls[i];

				//set text for control
				if(m_aEntries.Count > i + m_iFirstIndex)
					oRow.Text = ((DetailItem) m_aEntries[i + m_iFirstIndex]).Detail;
				else
					oRow.Text = "";

				//resize control
				Graphics oG = oRow.CreateGraphics();

				//get current height
				int iCurHeight = oRow.Height;

				//measure string - we append a character because
				//MeasureString does not include the height of a blank
				//line in it's value
				SizeF oSize = oG.MeasureString(oRow.Text + "p", oRow.Font);

				oRow.Multiline = this.RowsPerItem > 1 || this.AutoSize;

				//resize TextBox appropriately - min height
				//has to be the lines/item * the height per row
				oRow.Height = Math.Max((int) (oSize.Height + 5), 
					Math.Max((int) (m_fHeight * this.RowsPerItem + 5), 20));

				//reposition control
				if(i == 0)
					if(this.RowsPerItem == 1)
						oRow.Top = 1;
					else
						oRow.Top = 0;
				else
				{
					TextBox oPrevRow = (TextBox) this.pnlItems.Controls[i - 1];
                    if(this.ShowGridlines)
                        oRow.Top = oPrevRow.Top + oPrevRow.Height + 6;
                    else
						oRow.Top = oPrevRow.Top + oPrevRow.Height + 2;
				}
			}
			//force a repaint to draw gridlines
			this.pnlItems.Invalidate();

            //setup scrollbar
            m_bStopScrollBarUpdates = true;
            this.vsbCurrentIndex.Maximum = m_aEntries.Count;
            //GLOG 6779: If number of entries has changed, make sure m_iCurIndex is within limits
            m_iCurIndex = Math.Min(m_iCurIndex, this.vsbCurrentIndex.Maximum);
            this.vsbCurrentIndex.Value = m_iCurIndex;
            m_iFirstIndex = iScrollStart;
            m_bStopScrollBarUpdates = false;
			//select current row
			int iControlIndex = Math.Max(m_iCurIndex - m_iFirstIndex, 0);
			iControlIndex = Math.Min(iControlIndex, this.pnlItems.Controls.Count - 1);

			((TextBox) this.pnlItems.Controls[iControlIndex]).Focus();
			if(oCurRow != null)
			{
				//reselect text that was selected before the refresh
				TextBox oNewRow = (TextBox) this.ActiveControl;
				oNewRow.SelectionStart = iSelStart;
				if(bAllTextSelected)
					oNewRow.SelectionLength = oNewRow.Text.Length;
				else
					oNewRow.SelectionLength = iSelLen;
			}

			if(!this.DesignMode)
				LMP.Benchmarks.Print(t0);
		}

		/// <summary>
		/// moves the cursor the specified number of rows
		/// </summary>
		/// <param name="iOffset"></param>
		private void MoveToRow(int iOffset, bool bSelect)
		{
			ControlCollection oCtls = this.pnlItems.Controls;
            TextBox oCurRow = null;
            if (m_oLastActiveControl == null)
                oCurRow = (TextBox) this.pnlItems.Controls[0];
            else
			    oCurRow = (TextBox) m_oLastActiveControl;


			//get current row and selected char position
			int iCurRow = oCtls.IndexOf(oCurRow);
			int iSelStart = oCurRow.SelectionStart;

			if(iCurRow + iOffset > oCtls.Count - 1)
			{
				if(m_iCurIndex >= m_aEntries.Count && oCurRow.Text == "")
					return;

				//save current row
				this.UpdateEntry(oCurRow);

                m_iFirstIndex += iOffset;
                RefreshDisplay();
                //focus and select
                TextBox oNewRow = (TextBox)oCtls[oCtls.Count - 1];
                oNewRow.Focus();
                vsbCurrentIndex.Value = m_iCurIndex;
                SetCounter(m_iCurIndex);
                if (bSelect)
                {
                    oNewRow.SelectionStart = 0;
                    oNewRow.SelectionLength = oNewRow.Text.Length;
                }
                else
                {
                    oNewRow.SelectionStart = iSelStart;
                    oNewRow.SelectionLength = 0;
                }
                m_oLastTabControl = null;
            }
			else if(iCurRow + iOffset < 0 && this.vsbCurrentIndex.Value > 0)
			{
				//save current row
				this.UpdateEntry(oCurRow);
                //scroll up
                m_iFirstIndex +=iOffset;
                RefreshDisplay();
                //focus and select
                TextBox oNewRow = (TextBox)oCtls[0];
                oNewRow.Focus();
                SetCounter(m_iCurIndex);
                if (bSelect)
                {
                    oNewRow.SelectionStart = 0;
                    oNewRow.SelectionLength = oNewRow.Text.Length;
                }
                else
                {
                    oNewRow.SelectionStart = iSelStart;
                    oNewRow.SelectionLength = 0;
                }
                m_oLastTabControl = null;
            }
			else
			{
				//get new row - first possible row is 0, 
				//last possible row is last index in collection
				int iNewRow = Math.Min(Math.Max(iCurRow + iOffset, 0), 
					this.pnlItems.Controls.Count - 1);

                if(iNewRow != iCurRow)
				{
                    //GLOG 5136: save current row
                    this.UpdateEntry(oCurRow);
                    //select new row
					TextBox oRow = (TextBox) this.pnlItems.Controls[iNewRow];
					oRow.Focus();
                    //vsbFirstIndex.Value = m_iCurIndex;
                    if (bSelect)
                    {
                        oRow.SelectionStart = 0;
                        oRow.SelectionLength = oRow.Text.Length;
                    }
                    else
                    {
                        oRow.SelectionStart = iSelStart;
                        oRow.SelectionLength = 0;
                    }
                }
			}

            // GLOG : 3485 : JAB
            // Update the view of the move buttons.
            this.UpdateMoveButtonsView();
		}
        private void DeleteEntry()
        {
            if (m_aEntries.Count > 0)
            {
                int iDelIndex = m_iCurIndex;

                //get current row
                TextBox oRow = (TextBox)this.pnlItems.Controls[
                    iDelIndex - this.vsbCurrentIndex.Value];


                if (iDelIndex == m_aEntries.Count)
                {
                    //user has added text, but not yet hit return to add to array
                    //clear this row's text
                    TextBox oDelRow = (TextBox)this.pnlItems.Controls[
                        this.pnlItems.Controls.Count - 1];
                    oDelRow.Text = "";
                }
                else
                    //delete entry from array
                    m_aEntries.RemoveAt(iDelIndex);

                this.RefreshDisplay();

                //raise event if necessary
                if (this.DetailDeleted != null)
                    this.DetailDeleted((short)iDelIndex);

                Dialog.EnsureSelectedContent(oRow, false, false);
            }
        }
		/// <summary>
		/// returns an XML string representing the details in the control
		/// </summary>
		/// <returns></returns>
		private string ToXML()
		{
            //Save current entry if necessary
            if (this.DetailDirty && m_oLastActiveControl != null)
                UpdateEntry((TextBox)m_oLastActiveControl);

            if(m_aEntries.Count == 0)
				return "";

			//cycle through detail, creating XML 
			//elements with attribute for CI ID
			XmlDocument oXML = new XmlDocument();

			//create temporary top level node
			oXML.LoadXml("<zzmpD></zzmpD>");

			int i = 0;

            foreach(DetailItem oItem in m_aEntries)
			{
				i++;

				//create detail node
				XmlNode oDetailNode = oXML.CreateNode(XmlNodeType.Element,
					m_xFieldName, "");

                if (oItem.Detail == null)
                    oDetailNode.InnerText = "";
                else
                    oDetailNode.InnerText = 
                        LMP.String.ReplaceXMLChars(oItem.Detail);

                // Create Index and UNID attributes of Detail
                XmlAttribute oA = oXML.CreateAttribute("Index");
                oA.InnerText = i.ToString();
                oDetailNode.Attributes.Append(oA);

                oA = oXML.CreateAttribute("UNID");
                oA.InnerText = oItem.UNID != null ? oItem.UNID : "0";
                oDetailNode.Attributes.Append(oA);

				//add detail node
				oXML.DocumentElement.AppendChild(oDetailNode);
			}

			string xDetails = oXML.OuterXml;
			xDetails = xDetails.Replace("<zzmpD>", "");
			xDetails = xDetails.Replace("</zzmpD>", "");
			xDetails = xDetails.TrimEnd('\r', '\n');
            //GLOG 7365
            m_xContentString = xDetails;
			return xDetails;
		}

		/// <summary>
		/// populates control from XML
		/// </summary>
		private void FillDetailArray()
		{
			//clear detail list
			m_aEntries.Clear();

            if (m_xContentString != "")
            {
                //repopulate detail array
                XmlDocument oXML = new System.Xml.XmlDocument();
                oXML.LoadXml(string.Concat("<zzmpD>", m_xContentString, "</zzmpD>"));

                //select detail nodes in XML
                XmlNodeList oNodeList = oXML.DocumentElement.SelectNodes(
                    "(/zzmpD/" + m_xFieldName + "|/zzmpD/" +  m_xFieldName.ToUpper() + ")");

                //cycle through details in XML, adding each to detail array
                foreach (XmlNode oNode in oNodeList)
                {
                    string xDetail = LMP.String.RestoreXMLChars(oNode.InnerText);

                    //get UNID, if one exists
                    string xUNID = oNode.Attributes.GetNamedItem("UNID").Value;
                    if (xDetail == "")
                    {
                        //TODO: code to refresh details from UNIDs
                        //property isn't in current XML -
                        //get from CI
                        //if ((xUNID != "") && (xUNID != "0"))
                        //    xDetail = Dialog.GetCIDetail(xUNID, this.CIToken, oCISession);
                    }

                    //add detail to array of details
                    m_aEntries.Add(new DetailItem(xUNID, xDetail));
                }
            }
		}
        private void ScrollTextIntoView(TextBox oRow)
        {
            if (!m_bAutoSize || m_bScrolling)
                return;
            
            //scroll down if some of the selected row is not visible
            if (oRow.Top + oRow.Height > this.pnlItems.Height)
            {
                int iOffset = oRow.Top - Math.Min(oRow.Top - 1, (this.pnlItems.Height - oRow.Height));
                oRow.Top -= iOffset;
                //move all TextBoxes above resized TextBox
                foreach (TextBox oDetailRow in this.pnlItems.Controls)
                {
                    if (pnlItems.Controls.IndexOf((Control)oDetailRow) < pnlItems.Controls.IndexOf((Control)oRow))
                        oDetailRow.Top -= (iOffset);
                }
                this.pnlItems.Invalidate();
            }
            else if (oRow.Top < 0)
            {
                oRow.Top = 1;
                int iOffset = Math.Min(oRow.Height, this.pnlItems.Height);
                //move all TextBoxes above resized TextBox
                foreach (TextBox oDetailRow in this.pnlItems.Controls)
                {
                    if (pnlItems.Controls.IndexOf((Control)oDetailRow) > pnlItems.Controls.IndexOf((Control)oRow))
                    {
                        oDetailRow.Top = iOffset;
                        iOffset += oDetailRow.Height;
                    }
                }
                this.pnlItems.Invalidate();
            }

        }
		/// <summary>
		/// resizes the specified row to display contents
		/// </summary>
		/// <param name="oRow"></param>
		private void ResizeDetailRow(TextBox oRow)
		{
			try
			{
				Graphics oG = oRow.CreateGraphics();

				//get current height
				int iCurHeight = oRow.Height;

				//measure string - we append a character because
				//MeasureString does not include the height of a blank
				//line in it's value
				SizeF oSize = oG.MeasureString(oRow.Text + "p", oRow.Font);

				//resize TextBox appropriately - min height
				//has to be the lines/item * the height per row
				oRow.Height = Math.Max((int) (oSize.Height + 5), 
					Math.Max((int) (m_fHeight * this.RowsPerItem + 5 ), 20));

				//get amount of increased height
				int iHeightDelta = oRow.Height - iCurHeight;

                if (oRow.Top + oRow.Height > pnlItems.Height)
                {
                    //Move up Top of textbox to acommodate text
                    oRow.Top = oRow.Top - (iHeightDelta + 2);
                    //move all TextBoxes above resized TextBox
                    foreach (TextBox oDetailRow in this.pnlItems.Controls)
                    {
                        if (pnlItems.Controls.IndexOf((Control)oDetailRow)  < pnlItems.Controls.IndexOf((Control)oRow))
                            oDetailRow.Top -= (iHeightDelta + 2);
                    }
                }
                else
                {
                    //move all TextBoxes below resized TextBox
                    foreach (TextBox oDetailRow in this.pnlItems.Controls)
                    {
                        if (oDetailRow.Top > oRow.Top)
                            oDetailRow.Top += iHeightDelta;
                    }
                }
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		/// <summary>
		/// moves to 'Add Row' row if currently in a filled last row
		/// called from TabPressed and MoveRow - returns true if moved, else false
		/// </summary>
		/// <param name="oCurRow"></param>
		private bool MoveToAddRowIfAppropriate(TextBox oCurRow)
		{
			ControlCollection oCtls = this.pnlItems.Controls;

			if(oCurRow.Equals(oCtls[oCtls.Count - 1]) && oCurRow.Text != "")
			{
				//in the last TextBox, which was not empty - 
				//simulate adding a new row and scrolling down -
				//save current entry
				this.UpdateEntry(oCurRow);

				this.vsbCurrentIndex.Value = this.vsbCurrentIndex.Maximum;
                m_iFirstIndex = this.vsbCurrentIndex.Maximum - (oCtls.Count - 1);
                RefreshDisplay();
				//oCtls[oCtls.Count - 1].Text = "";
				oCtls[oCtls.Count - 1].Focus();
                if (oCtls[oCtls.Count - 1].Text == "")
                {
                    m_oLastTabControl = oCtls[oCtls.Count - 1];
                }
                else
                {
                    m_oLastTabControl = null;
                }
				//return true if moved to 'Add Row'
				return true;
			}
			else
				return false;
		}
		/// <summary>
		/// moves the current entry up a row
		/// </summary>
		private void MoveCurrentEntryUp()
		{
            if(m_iCurIndex > 0)
			{
                this.UpdateEntry((TextBox)this.ActiveControl);
                //entry is not last entry - get entry and next entry
				DetailItem oEntry1 = (DetailItem) m_aEntries[m_iCurIndex];
				DetailItem oEntry2 = (DetailItem) m_aEntries[m_iCurIndex - 1];

				//switch entries
				m_aEntries[m_iCurIndex] = oEntry2;
				m_aEntries[m_iCurIndex - 1] = oEntry1;

				//refresh
				this.RefreshDisplay();

				//select moved row
				this.MoveToRow(-1, false);
                this.IsDirty = true;
			}

            // GLOG : 3485 : JAB
            this.UpdateMoveButtonsView();
		}
		/// <summary>
		/// moves the current entry down a row
		/// </summary>
		private void MoveCurrentEntryDown()
		{
			if(m_iCurIndex < m_aEntries.Count - 1)
			{
                this.UpdateEntry((TextBox)this.ActiveControl);
                //entry is not last entry - get entry and next entry
				DetailItem oEntry1 = (DetailItem) m_aEntries[m_iCurIndex];
				DetailItem oEntry2 = (DetailItem) m_aEntries[m_iCurIndex + 1];

				//switch entries
				m_aEntries[m_iCurIndex] = oEntry2;
				m_aEntries[m_iCurIndex + 1] = oEntry1;

				//refresh
				this.RefreshDisplay();

				//select moved row
				this.MoveToRow(1, false);
                this.IsDirty = true;
			}

            // GLOG : 3485 : JAB
            this.UpdateMoveButtonsView();
		}

		/// <summary>
		/// code that should be called whenever a row gets focus
		/// </summary>
		private void OnRowGotFocus(TextBox oRow)
		{
			if(this.DesignMode)
				return;
			m_iCurIndex = m_iFirstIndex + this.pnlItems.Controls.IndexOf(oRow);
			if(m_iCurIndex > m_aEntries.Count)
			{
				//this textbox is not associated with an entry, nor is it the 'add new' row -
				//select 'add new' row - ensure index is at least zero, if called after
                //deletion of all detail rows
				int iAddRowIndex = Math.Max(0, m_aEntries.Count - m_iFirstIndex);
				m_iCurIndex = m_aEntries.Count;
				this.pnlItems.Controls[iAddRowIndex].Focus();
			}
			else
				//select text in row
				LMP.Dialog.EnsureSelectedContent(oRow, false, false);
            vsbCurrentIndex.Value = m_iCurIndex;
			oRow = (TextBox) this.ActiveControl;
            m_oLastActiveControl = this.ActiveControl;
            //Ensure all text is visible
            ScrollTextIntoView(oRow);
			this.SetCounter(m_iCurIndex);

            // GLOG : 3485 : JAB
            // Update the view of the move buttons
            this.UpdateMoveButtonsView();
		}
		/// <summary>
		/// cycle through control array and reload defined CI
		/// properties for item or all based on saved UNIDs
		/// </summary>
		private void RefreshCIValues(int iIndex)
		{
			int iStart;
			int iEnd;
			bool bChanged = false;

			if(this.CIToken == "")
				return;

			this.UpdateEntry((TextBox) this.ActiveControl);

			if(iIndex == -1)
			{
				//refresh all details
				iStart = 0;
				iEnd = m_aEntries.Count - 1;
			}
			else
			{
				//refresh detail with specified index
				iStart = iIndex;
				iEnd = iIndex;
			}

			bChanged = false;

			//cycle through specified details
			for(int i = iStart; i <= iEnd; i++)
			{
                DetailItem oItem = null;
                try
                {
				    //get detail
				    oItem = (DetailItem) m_aEntries[i];
                }
                catch {}
                //exit if oItem is null - there's nothing to refresh
                if (oItem == null)
                    break;

				if(oItem.UNID != null && oItem.UNID != "" && oItem.UNID != "0")
				{
					//recipient has a UNID - get contact
					ICContact oC = LMP.Data.Application.CISession.GetContactFromUNID(oItem.UNID,
						ciRetrieveData.ciRetrieveData_All, ciAlerts.ciAlert_None);

					if(oC != null)
					{
						//there is a contact - get CI detail for detail field
						string xVal = Dialog.GetCIDetail(oC, this.CIToken.ToUpper());

						if(oItem.Detail != xVal)
						{
							//new value is different
							oItem.Detail = xVal;
							bChanged = true;

							if(this.DetailEdited != null)
								this.DetailEdited((short) i);
						}
					}
				}
			}

			if(bChanged)
				this.RefreshDisplay();
		}
		#endregion
		#region *********************helper classes*********************

		private class DetailItem
		{
			public string UNID;
			public string Detail;

			public DetailItem(string xUNID, string xDetail)
			{
				this.UNID = xUNID;
				this.Detail = xDetail;
			}
			public DetailItem(string xDetail)
			{
				this.UNID = "0";
				this.Detail = xDetail;
			}
		}
		#endregion
		#region *********************event handlers*********************
        private void DetailList_Load(object sender, EventArgs e)
        {
            try
            {
                LMP.Trace.WriteInfo("DetailList_Load");
                SetupResourceStrings();
                if (this.DesignMode)
                    ExecuteFinalSetup();
                else
                {
                    this.pnlItems.MouseDown +=new MouseEventHandler(HandleMouseDown);
                    this.pnlItems.DoubleClick += new EventHandler(HandleDoubleClick);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

            // GLOG : 3485 : JAB
            // Set the appropriate view for the move buttons.
            UpdateMoveButtonsView();
        }

        void HandleDoubleClick(object sender, EventArgs e)
        {
            try
            {
                base.OnDoubleClick(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void DetailList_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
            //try
            //{
            //    this.PaintGridlines(e.Graphics);
            //}
            //catch (System.Exception oE)
            //{
            //    LMP.Error.Show(oE);
            //}
		}

		private void HandleTextChanged(object sender, EventArgs e)
		{
			try
			{
                this.IsDirty = true;
                this.DetailDirty = true;
                m_oLastTabControl = null;
                if(!this.AutoSize)
					return;

				ResizeDetailRow((TextBox) sender);

				if(this.SubControlChanged != null)
					this.SubControlChanged((Control) sender);
			}			
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		private void HandleKeyDown(object sender, KeyEventArgs e)
		{
			try
			{
                LMP.Trace.WriteInfo("HandleKeyDown");

                
                TextBox oRow = (TextBox)sender;

                //suppress return key if autosize is not on
				e.Handled = ((e.KeyCode == Keys.Return) && (!this.AutoSize));

                if (e.KeyCode != Keys.Tab)
                    m_oLastTabControl = null;

                if (e.KeyCode == Keys.Return)
                {
                    e.Handled = true;
                    if (oRow.Multiline == false)
                        this.MoveToRow(1, false);
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (oRow.Text == "")
                    {
                        e.Handled = true;
                        this.MoveToRow(-1, false);
                    }
                }
                else if (e.KeyCode == Keys.Up)
                {
                    //if cursor is in first row, move to previous row
                    if (oRow.Text.IndexOf("\r\n", 0, oRow.SelectionStart) == -1)
                    {
                        //there are no returns up to the start of the selection -
                        //thus, we're in the first line
                        e.Handled = true;
                        this.MoveToRow(-1, false);
                    }
                }
                else if (e.KeyCode == Keys.Down)
                {
                    //if cursor is in last row, move to next row
                    if (oRow.Text.IndexOf("\r\n", oRow.SelectionStart + oRow.SelectionLength) == -1)
                    {
                        //there are no returns after the selection -
                        //thus, we're in the last line
                        e.Handled = true;
                        this.MoveToRow(1, false);
                    }
                }
                else if (e.KeyCode == Keys.Left &&
                    oRow.SelectionStart == 0 && oRow.SelectionLength == 0)
                {
                    //arrow left when cursor is at start of textbox
                    e.Handled = true;
                    this.MoveToRow(-1, false);
                }
                else if (e.KeyCode == Keys.Right && oRow.SelectionStart == oRow.Text.Length)
                {	//arrow right when cursor is at end of textbox
                    this.MoveToRow(1, false);
                    e.Handled = true;
                }
                else
                    if (e.KeyCode == Keys.F11 && e.Shift)
                    {
                        //Shift+PageUp
                        this.MoveCurrentEntryUp();
                        e.SuppressKeyPress = true;
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.F12 && e.Shift)
                    {
                        //Shift+PageDown
                        this.MoveCurrentEntryDown();
                        e.SuppressKeyPress = true;
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Delete && e.Control && e.Shift)
                    {
                        DeleteEntry();
                        e.SuppressKeyPress = true;
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.N && e.Control && e.Shift)
                    {
                        //GLOG 3954: Ctrl+Shift+N Moves to Add New row
                        MoveToAddRow();
                        e.SuppressKeyPress = true;
                        e.Handled = true;
                    }

                if (this.KeyPressed != null)
                    this.KeyPressed(this, e);

                if (this.SubControlKeyDown != null)
					this.SubControlKeyDown((Control) sender, e.KeyCode);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

        void HandleKeyReleased(object sender, KeyEventArgs e)
        {
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);
        }
        private void HandleValidating(object sender, CancelEventArgs e)
		{
            try
            {
                LMP.Trace.WriteInfo("HandleValidating");

                TextBox oRow = (TextBox)sender;
                ControlCollection oCtls = this.pnlItems.Controls;

                //cancel focus move if the tab key was pressed in the 
                //last textbox and that textbox had text in it - under
                //these conditions the oRow_TabPressed handler will handle the
                //tab key to simulate adding a new row
                if (oRow.Equals(oCtls[oCtls.Count - 1]) && oRow.Text != "" && m_bTabPressed)
                    e.Cancel = true;
                m_bTabPressed = false;
                m_oLastTabControl = null;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
		}

		private void HandleTabPressed(object sender, TabPressedEventArgs e)
		{
            try
            {
                // Don't process if it's a duplicate event
                if (m_oLastTabControl != (Control)sender)
                {
                    LMP.Trace.WriteInfo("HandleTabPressed");
                    ExecuteCodeForTabPressed((Control)sender, e.ShiftPressed);
                    // If we've gotten here, tab out of control
                    if (this.TabPressed != null && m_bTabPressed)
                        this.TabPressed(this, e);
                }
                m_oLastTabControl = null;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
		}

		private void HandleLeave(object sender, EventArgs e)
		{
			try
			{
				this.UpdateEntry((TextBox) sender);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		private void HandleGotFocus(object sender, EventArgs e)
		{
			try
			{
                this.OnRowGotFocus((TextBox) sender);
                m_oLastTabControl = null;
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

        /// <summary>
        /// executes code that must be run when "Enter" is pressed on the keyboard
        /// </summary>
        private void ExecuteCodeForEnterPressed(object sender, EnterKeyPressedEventArgs e)
        {
            try
            {
                TextBox oCurCtl = (TextBox)sender;
                if (oCurCtl.Multiline)
                {
                    // Enter key doesn't work when textbox is on the TaskPane,
                    // so insert the return directly
                    int iSelStart = ((TextBox)oCurCtl).SelectionStart;
                    ((TextBox)oCurCtl).SelectedText = "\r\n";
                    ((TextBox)oCurCtl).SelectionStart = iSelStart + 2;
                    ((TextBox)oCurCtl).SelectionLength = 0;
                }
                else
                    this.MoveToRow(1, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void vsbFirstIndex_ValueChanged(object sender, System.EventArgs e)
		{
            //try
            //{
            //    m_bScrolling = true;
            //    if(!m_bStopScrollBarUpdates)
            //    {
            //        RefreshDisplay();
            //        //if(this.ActiveControl != null)
            //        //{
            //        //    TextBox oRow = (TextBox) this.ActiveControl;
            //        //    this.OnRowGotFocus(oRow);
            //        //}
            //    }
            //    m_bScrolling = false;
            //}
            //catch(System.Exception oE)
            //{
            //    LMP.Error.Show(oE);
            //}
		}

		private void pnlItems_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			try
			{
				this.PaintGridlines(e.Graphics);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		private void DetailList_Leave(object sender, System.EventArgs e)
		{
			try
			{
                LMP.Trace.WriteInfo("DetailList_Leave");
                //if (!this.DesignMode)
                //{
                //    foreach (Control oCtl in this.pnlItems.Controls)
                //        oCtl.PreviewKeyDown += null;
                //}
                base.OnLostFocus(e);

			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		private void HandleMouseDown(object sender, MouseEventArgs e)
		{
			try
			{
				if(e.Button == MouseButtons.Right)
					this.mnuPopup.Show((Control) sender, new Point(e.X, e.Y));

				if(this.SubControlMouseDown != null)
					this.SubControlMouseDown((Control) sender, e.X, e.Y);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		private void mnuContextMenu_MoveUp_Click(object sender, System.EventArgs e)
		{
			try
			{
				MoveCurrentEntryUp();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
}

		private void mnuContextMenu_MoveDown_Click(object sender, System.EventArgs e)
		{
			try
			{
				MoveCurrentEntryDown();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		private void mnuContextMenu_DeleteDetail_Click(object sender, System.EventArgs e)
		{
			try
			{
                DeleteEntry();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		private void mnuContextMenu_DeleteAll_Click(object sender, System.EventArgs e)
		{
			try
			{
				//delete all entries
				m_aEntries.Clear();

				//reset curIndex variable
                m_iCurIndex = 0;
                m_iFirstIndex = 0;
                //refresh list
				this.RefreshDisplay();

				//select first row
				this.pnlItems.Controls[0].Focus();

				//raise event if necessary
				if(this.DetailCleared != null)
					this.DetailCleared(sender, e);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		private void mnuContextMenu_RefreshDetail_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.RefreshCIValues(m_iCurIndex);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		private void mnuContextMenu_RefreshAll_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.RefreshCIValues(-1);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

		private void DetailList_Enter(object sender, System.EventArgs e)
		{
			try
			{
                LMP.Trace.WriteInfo("DetailList_Enter");
                if (!this.DesignMode)
                {
                    //ensure that all rows have their tab stop enabled-
                    //tab stops are disabled in the TabPressed event handler
                    foreach (Control oCtl in this.pnlItems.Controls)
                    {
                        oCtl.TabStop = true;
                    }
                    if (m_oLastActiveControl != null)
                    {
                        this.ActiveControl = m_oLastActiveControl;
                        m_oLastActiveControl.Focus();
                    }
                    else
                    {
                        pnlItems.Controls[0].Focus();
                        this.ActiveControl = pnlItems.Controls[0];
                        m_oLastActiveControl = pnlItems.Controls[0];
                        m_iCurIndex = 0;
                    }
                    m_oLastTabControl = null;
                    //SendKeys.SendWait("+");
                    
                }
                base.OnGotFocus(e);
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Sometimes TabPressed event for Textbox isn't raised, so check for Tab in PreviewKeyDown instead
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandlePreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            try
            {
                // Don't process if it's a duplicate event
                if (m_oLastTabControl != (Control)sender)
                {
                    LMP.Trace.WriteInfo("HandlePreviewKeyDown");
                    if (e.KeyCode == Keys.Tab)
                    {
                        ExecuteCodeForTabPressed((Control)sender, e.Shift);
                        e.IsInputKey = !m_bTabPressed;
                        //GLOG 5136: Prevent duplicate event
                        m_oLastTabControl = (Control)sender;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Retrieve Contacts from CI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnContacts_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    //Clear tooltip to allow CI Window to initialize properly
                    ToolTip t = (ToolTip)this.tsActions.GetType().GetProperty("ToolTip", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this.tsActions, null);
                    t.RemoveAll();
                }
                catch { }
                this.tsActions.Enabled = false;
                if (this.CIRequested != null)
                    this.CIRequested(this, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.tsActions.Enabled = true;
            }
        }

        private void DetailList_VisibleChanged(object sender, EventArgs e)
        {

            //if (this.Visible && m_bControlIsLoaded && !this.DesignMode)
            //{
            //    foreach (Control oCtl in this.pnlItems.Controls)
            //    {
            //        oCtl.PreviewKeyDown += new PreviewKeyDownEventHandler(HandlePreviewKeyDown);
            //    }
            //}
            //else if (!this.Visible && m_bControlIsLoaded && !this.DesignMode)
            //{
            //    foreach (Control oCtl in this.pnlItems.Controls)
            //    {
            //        oCtl.PreviewKeyDown += null;
            //    }
            //}
        }

        private void vsbFirstIndex_Scroll(object sender, ScrollEventArgs e)
        {
            try
            {
                m_bScrolling = true;
                if (!m_bStopScrollBarUpdates)
                {
                    if (e.NewValue < 0 || e.NewValue > vsbCurrentIndex.Maximum)
                    {
                        e.NewValue = e.OldValue;
                        return;
                    }
                    else if (e.NewValue < e.OldValue)
                    {
                        // scrolling up
                        if (e.NewValue < m_iFirstIndex)
                        {
                            m_iFirstIndex = e.NewValue;
                            m_iCurIndex = e.NewValue;
                            RefreshDisplay();
                        }
                        else
                        {
                            MoveToRow(-1, true);
                        }
                        SetCounter(m_iCurIndex);
                    }
                    else if (e.NewValue > e.OldValue)
                    {
                        // scrolling down
                        if (e.NewValue >= m_iFirstIndex + this.pnlItems.Controls.Count)
                        {
                            m_iFirstIndex = e.NewValue - (this.pnlItems.Controls.Count - 1);
                            m_iCurIndex = e.NewValue;
                            RefreshDisplay();
                        }
                        else
                        {
                            MoveToRow(1, true);
                        }
                        SetCounter(m_iCurIndex);
                    }
                }
                m_bScrolling = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                // GLOG : 3104 : JAB
                // Prompt user to confirm delete.
                DialogResult oChoice = MessageBox.Show(LMP.Resources.GetLangString("Message_DeleteEntry"),
                                                        LMP.String.MacPacProductName,
                                                        MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Question);

                if (oChoice != DialogResult.Yes)
                    return;

                DeleteEntry();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // GLOG : 3485 : JAB
        // Move the item up.
        private void tbtnMoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                this.MoveCurrentEntryUp();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // GLOG : 3485 : JAB
        // Move the item down.
        private void tbtnMoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                this.MoveCurrentEntryDown();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6297 : CEH
        private void mnuContextMenu_Paste_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ActiveControl is TextBox)
                {
                    TextBox oTB = (TextBox)this.ActiveControl;
                    oTB.SelectedText = Clipboard.GetText();
                }
                else if (this.ActiveControl is ComboBox)
                {
                    ComboBox oCombo = (ComboBox)this.ActiveControl;
                    oCombo.SelectedValue = Clipboard.GetText();
                }
                else if (this.ActiveControl is MultilineCombo)
                {
                    MultilineCombo oMLC = (MultilineCombo)this.ActiveControl;
                    oMLC.SelectedText = Clipboard.GetText();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuContextMenu_Copy_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ActiveControl is TextBox)
                {
                    TextBox oTB = (TextBox)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oTB.SelectedText, true);
                }
                else if (this.ActiveControl is ComboBox)
                {
                    ComboBox oCombo = (ComboBox)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oCombo.SelectedValue.ToString(), true);
                }
                else if (this.ActiveControl is MultilineCombo)
                {
                    MultilineCombo oMLC = (MultilineCombo)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oMLC.SelectedText, true);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuContextMenu_Cut_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ActiveControl is TextBox)
                {
                    TextBox oTB = (TextBox)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oTB.SelectedText, true);
                    //cut
                    oTB.SelectedText = "";
                }
                else if (this.ActiveControl is ComboBox)
                {
                    ComboBox oCombo = (ComboBox)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oCombo.SelectedValue.ToString(), true);
                    //cut
                    oCombo.SelectedValue = "";
                }
                else if (this.ActiveControl is MultilineCombo)
                {
                    MultilineCombo oMLC = (MultilineCombo)this.ActiveControl;
                    //copy to clipboard
                    Clipboard.SetDataObject(oMLC.SelectedText, true);
                    //cut
                    oMLC.SelectedText = "";
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuPopup_Popup(object sender, EventArgs e)
        {
            try
            {
                bool bEnable = false;
                if (this.ActiveControl is TextBox)
                {
                    TextBox oTB = (TextBox)this.ActiveControl;
                    bEnable = (oTB.SelectedText != "");
                }
                else if (this.ActiveControl is ComboBox)
                {
                    ComboBox oCombo = (ComboBox)this.ActiveControl;
                    oCombo.SelectedValue = "";
                    bEnable = (oCombo.SelectedValue.ToString() != "");
                }
                else if (this.ActiveControl is MultilineCombo)
                {
                    MultilineCombo oMLC = (MultilineCombo)this.ActiveControl;
                    bEnable = (oMLC.SelectedText != "");
                }

                this.mnuContextMenu_Copy.Enabled = bEnable;
                this.mnuContextMenu_Cut.Enabled = bEnable;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
		#region *********************protected members*********************
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab || keyData == Keys.Return)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null && m_bTabPressed)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (m.WParam.ToInt32() == (int)Keys.Return)
            {
                // Process Enter here, because KeyDown doesn't get raised for this
                // in TaskPane
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        #endregion
		#region *********************IControl members*********************
		public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
		{
            m_bControlIsLoaded = true;
            FillDetailArray();
            SetupControl();
        }
        [CategoryAttribute("Data")]
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get
            {
                return this.ContentString;
            }
            set
            {
                this.ContentString = value;
                //this.IsDirty = false;
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the Dirty flag indicating the value has changed.")]
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return m_bIsDirty; }
        }
        [Browsable(false)]
        [DescriptionAttribute("Secondary user-defined object associated with the control.")]
        //TODO: what is this used for?
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************ICIable members*********************
        public event CIRequestedHandler CIRequested;
        public bool IsSubscribedToCI
        {
            get
            {
                if ((System.Delegate)this.CIRequested != null)
                {
                    return this.CIRequested.GetInvocationList().Length > 0;
                }
                else
                {
                    return false;
                }
            }
        }
        public int ContactCount
        {
            get
            {
                return m_aEntries.Count;
            }
        }
        #endregion
	}
}
