using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class StartingLabelSelector : UserControl, IControl 
    {
        #region *********************fields*******************
        private int m_iColumns;
        private int m_iRows;
        private bool m_bIsDirty;
        private bool m_bAlternatingRowBackColor;
        private string m_xLabelType;
        private string m_xValue = "1";
        //this aspect ratio is 8.5/11, classic paper size
        private decimal m_decAspect = .772M;
        private string m_xSupportingValues = "";
        #endregion
        #region *********************enumerations*******************
        ////////public enum LabelType
        ////////{
        ////////    Avery_5160,
        ////////    Avery_5161,
        ////////    Avery_5162,
        ////////    Custom
        ////////}
        #endregion
        #region *********************constructors*******************
        public StartingLabelSelector()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************properties*******************
        [DescriptionAttribute("Gets/Sets the number of columns representing a label type")]
        public int Columns
        {
            get { return m_iColumns; }
            set { m_iColumns = value; }
        }
        [DescriptionAttribute("Gets/Sets the number of rows representing a label type")]
        public int Rows
        {
            get { return m_iRows; }
            set { m_iRows = value; }
        }
        [DescriptionAttribute("Gets/Sets the label type")]
        public string LabelType
        {
            get { return m_xLabelType; }
            set { m_xLabelType = value; }
        }
        [DescriptionAttribute("Gets/Sets the label type")]
        public bool AlternatingRowBackColor
        {
            get { return m_bAlternatingRowBackColor; }
            set { m_bAlternatingRowBackColor = value; }
        }
        #endregion
        #region *********************methods*******************
        /// <summary>
        /// configures the grid to represent label sheet of nn columns and nn rows
        /// </summary>
        private void SetupGrid()
        {
            DataTable oDT = new DataTable();
            string[] xRow = new string[this.Columns];
            int iWidth = grdLabels.Width;

            //create the columns
            for (int i = 0; i < this.Columns; i++)
            {
                oDT.Columns.Add("Column"+ i.ToString());
                xRow[i] = string.Empty;
            }
            
            //create the rows
            for (int i = 0; i < this.Rows; i++)
                oDT.Rows.Add(xRow);

            //set the row height, backcolor via a RowTemplate 
            int iHeight = (grdLabels.Height / oDT.Rows.Count);
            DataGridViewRow row = this.grdLabels.RowTemplate; 
            ////row.DefaultCellStyle.BackColor = Color.Bisque; 
            
            //alternate row color if specified
            if (!this.AlternatingRowBackColor)
                grdLabels.AlternatingRowsDefaultCellStyle.BackColor =
                    grdLabels.DefaultCellStyle.BackColor;

            row.Height = iHeight;

            //adjust the height slightly to account for the grid dividers
            Height = Height + 4;
            
            //create the grid structure
            this.grdLabels.DataSource = oDT;

            //set the column width
            for (int i = 0; i < grdLabels.Columns.Count; i++)
            {
                grdLabels.Columns[i].Width = iWidth / grdLabels.Columns.Count;
            }
        }
        /// <summary>
        /// selects the grid cell corresponding to the value
        /// </summary>
        private void SelectStartingLabelCell()
        {
            int iCell;
            int iRow;
            if (Int32.Parse(m_xValue) == 1)
            {
                iCell = 0;
                iRow = 0;
            }
            else
            {
                iCell = Int32.Parse(m_xValue) % grdLabels.Columns.Count;
                iRow = (Int32.Parse(m_xValue) - iCell) / grdLabels.Columns.Count;
                if (grdLabels.Rows.Count == 1)
                    iRow = 0;
            }
            grdLabels.CurrentCell = grdLabels.Rows[iRow].Cells[iCell];
        }
        #endregion
        #region *********************internal procedures*******************
        #endregion
        #region *********************procedures*******************
        #endregion
        #region *********************event handlers *******************
        /// <summary>
        /// sets control value based on row/cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdLabels_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            int iVal = (e.RowIndex * grdLabels.Columns.Count) + e.ColumnIndex + 1;
            this.Value = iVal.ToString();
            this.IsDirty = true;
        }
        /// <summary>
        /// process tab press - enables task pane navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void grdLabels_TabPressed(object sender, TabPressedEventArgs e)
        {
            const int WM_KEYDOWN = 256;
            const int KEY_TAB = 9;
            //const int KEY_SHIFT = 16;

            Message oMsg = new Message();
            oMsg.Msg = WM_KEYDOWN;
            oMsg.WParam = (IntPtr)KEY_TAB;
            
            //if (e.ShiftPressed)
            //    oMsg.WParam = oMsg.WParam;

            this.ProcessKeyMessage(ref oMsg);
        }
        /// <summary>
        /// this handler will adjust the width according to the 
        /// module level variable m_decAspect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartingLabelSelector_Resize(object sender, EventArgs e)
        {
            grdLabels.Width = (int)((decimal)this.Height * m_decAspect);
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// support for tabbing for MacPac controls
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab || keyData == Keys.Return)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (m.WParam.ToInt32() == (int)Keys.Return)
            {
                // Process Enter here, because KeyDown doesn't get raised for this
                // in TaskPane
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        /// <summary>
        /// 
        /// </summary>
        public void ExecuteFinalSetup()
        {
            try
            {
                //don't attempt setup if invalide row or column value
                if (this.Columns == 0 || this.Rows == 0)
                    return;
                
                SetupGrid();
                SelectStartingLabelCell();
                //subscribe to CellEnter event - handler will set control value
                //when user navigates through grid
                this.grdLabels.CellEnter += 
                    new System.Windows.Forms.DataGridViewCellEventHandler(this.grdLabels_CellEnter);
                this.grdLabels.TabPressed += new TabPressedHandler(grdLabels_TabPressed);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public string Value
        {
            get
            {
                return m_xValue;
            }
            set
            {
                m_xValue = value;
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Get/Sets the Dirty flag indicating the value has changed.")]
        public bool IsDirty
        {
            set 
            { 
                m_bIsDirty = value;
            }
            get 
            { 
                return m_bIsDirty; 
            }
        }
        [Browsable(false)]
        [DescriptionAttribute("Secondary user-defined object associated with the control.")]
        //TODO: what is this used for?
        public object Tag2
        {
            get { return this.grdLabels.Tag2; }
            set { this.grdLabels.Tag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
    }
}
