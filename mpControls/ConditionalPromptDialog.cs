using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class ConditionalPromptDialog : Form
    {
        string m_xRegistryValue = "";

        /// <summary>
        /// Display standard dialog with prompt text and a checkbox
        /// </summary>
        /// <param name="xPrompt"></param>
        /// <param name="xCheckboxText"></param>
        public ConditionalPromptDialog()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Conditionally display dialog if configured Reg key is not set to "1" to skip
        /// </summary>
        /// <returns></returns>
        public DialogResult ShowDialog(string xPrompt, string xCheckboxText, string xRegistryValue)
        {
            this.lblPrompt.Text = xPrompt;
            this.chkSkipPrompt.Text = xCheckboxText;
            m_xRegistryValue = xRegistryValue;
            this.Text = LMP.ComponentProperties.ProductName;
            bool bShow = (Registry.GetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, m_xRegistryValue) != "1");
            if (bShow)
                return base.ShowDialog();
            else
                return DialogResult.Abort;
        }
        /// <summary>
        /// Return true if 'Skip Prompt' checkbox checked
        /// </summary>
        public bool Checked
        {
            get { return this.chkSkipPrompt.Checked; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //Set registry value to skip this prompt in the future
            if (this.chkSkipPrompt.Checked)
                Registry.SetMacPac10Value(m_xRegistryValue, "1");
        }
    }
}