using System;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Controls
{
	/// <summary>
	/// Summary description for Chooser.
	/// </summary>
	public class Chooser:LMP.Controls.ComboBox, IControl
	{
        #region *********************fields*********************
        private mpObjectTypes m_iTargetObjectType;
        private mpObjectTypes m_iAssignedObjectType;
        private int m_iTargetObjectID;
        private string m_xSegmentsList = "";
        private bool m_bIncludeNoneOption = false;
        #endregion
        #region ******************delegates*********************
        public delegate void CtrlRightArrowPressedHandler(object sender);
        public delegate void CtrlLeftArrowPressedHandler(object sender);
        #endregion
        #region *********************events*********************
        public event CtrlRightArrowPressedHandler OnCtrlRightArrowPressed;
        public event CtrlLeftArrowPressedHandler OnCtrlLeftArrowPressed;
        #endregion
        #region *********************constructors*********************
        public Chooser()
            : base()
        {
            this.KeyDown += new KeyEventHandler(Chooser_KeyDown);
            //GLOG 4548: Use timer delay to allow extended matching from keyboard
            this.UseTimer = true;
        }
        #endregion
        #region *********************properties*********************
        public mpObjectTypes TargetObjectType
        {
            get { return m_iTargetObjectType; }
            set { m_iTargetObjectType = value; }
        }
        public mpObjectTypes AssignedObjectType
        {
            get { return m_iAssignedObjectType; }
            set { m_iAssignedObjectType = value; }
        }
        public int TargetObjectID
        {
            get { return m_iTargetObjectID; }
            set { m_iTargetObjectID = value; }
        }
        public string SegmentsList
        {
            get { return m_xSegmentsList; }
            set { m_xSegmentsList = value; }
        }
        public bool IncludeNoneOption
        {
            get { return m_bIncludeNoneOption; }
            set { m_bIncludeNoneOption = value; }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// loads the appropriate assignments into the chooser
        /// </summary>
        /// <param name="iTargetObjectType"></param>
        /// <param name="iTargetObjectID"></param>
        /// <param name="iAssignedObjectType"></param>
        private void LoadAssignments(mpObjectTypes iTargetObjectType, int iTargetObjectID,
            mpObjectTypes iAssignedObjectType)
        {
            Assignments oAssignments = null;

           Trace.WriteNameValuePairs("iTargetObjectType", iTargetObjectType,
                "iTargetObjectID", iTargetObjectID,
                "iAssignedObjectType", iAssignedObjectType);

            //set fields
            this.m_iTargetObjectType = iTargetObjectType;
            this.m_iTargetObjectID = iTargetObjectID;
            this.m_iAssignedObjectType = iAssignedObjectType;

            //get assignments
            oAssignments = new Assignments(iTargetObjectType,
                iTargetObjectID, iAssignedObjectType);

            //if there are no assignments, load segments
            //of assigned object type instead; otherwise
            //bind assignments to data
            if (oAssignments.Count == 0)
                LoadSegments(iAssignedObjectType);
            else
            {
                System.Collections.ArrayList aList = oAssignments.ToArrayList();
                if (m_bIncludeNoneOption)
                {
                    //Add None option to top of list
                    object [] oNone = new object[2];
                    oNone[0] = "0";
                    oNone[1] = LMP.Resources.GetLangString("Dialog_Chooser_NoneText");
                    aList.Insert(0, oNone);
                }
                this.SetList(aList);
            }

            this.LimitToList = true;
        }
        private void LoadSegments(string xInputList)
        {
            Trace.WriteNameValuePairs("xInputList", xInputList);

            if (m_bIncludeNoneOption)
                xInputList = LMP.Resources.GetLangString("Dialog_Chooser_NoneText") + ";0~" + xInputList;
            this.SetList(xInputList);

            //temporary only - so that we won't get errors in the UI
            this.LimitToList = true;
        }
        /// <summary>
        /// loads all segments of the 
        /// specified type into the chooser
        /// </summary>
        /// <param name="iTargetObjectType"></param>
        /// <param name="iTargetObjectID"></param>
        /// <param name="iAssignedObjectType"></param>
        private void LoadSegments(mpObjectTypes iObjectType)
        {
            Trace.WriteNameValuePairs("iObjectType", iObjectType);

            //get assignments
            //GLOG 15736: If no specific assignments, limit collection to folders for which user has permission
            AdminSegmentDefs oDefs = new AdminSegmentDefs(iObjectType, LMP.Data.Application.User.ID.ToString(), 0, 0, 0, 0, 0, true);

            System.Collections.ArrayList aList = oDefs.ToArrayList(0, 1);
            if (m_bIncludeNoneOption)
            {
                //Add None option to top of list
                object [] oNone = new object[2];
                oNone[0] = "0";
                oNone[1] = LMP.Resources.GetLangString("Dialog_Chooser_NoneText");
                aList.Insert(0, oNone);
            }
            //bind to data - select ID and DisplayName columns
            this.SetList(aList);

            //temporary only - so that we won't get errors in the UI
            this.LimitToList = true;
        }
        #endregion
        #region *********************event handlers*********************
        void Chooser_KeyDown(object sender, KeyEventArgs e)
        {
            // Fire an event for the control-left arrow or 
            // control-right arrow key stroke.
            if (e.Control)
            {
                if (e.KeyCode == Keys.Left)
                {
                    OnCtrlLeftArrowPressed(sender);
                    e.Handled = true;
                    return;
                }

                if (e.KeyCode == Keys.Right)
                {
                    OnCtrlRightArrowPressed(sender);
                    e.Handled = true;
                    return;
                }
            }
        }
        #endregion
        #region *********************private methods*********************
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Chooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Name = "Chooser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        #region *********************IControl members*********************
        public override void ExecuteFinalSetup()
        {
            //GLOG 3668: Don't reload list if it's already been set
            if (!base.ListLoaded)
            {
                if (this.SegmentsList != "")
                    //load Segments from delimited list
                    this.LoadSegments(this.SegmentsList);
                else if (this.TargetObjectID != 0)
                    //load assigned segments of the specified type
                    this.LoadAssignments(this.TargetObjectType, this.TargetObjectID,
                        this.AssignedObjectType);
                else
                    //load all segments of the specified type
                    this.LoadSegments(this.AssignedObjectType);
            }
            base.ExecuteFinalSetup();
        }
        #endregion
    }
}
