using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace LMP.Controls
{
    public partial class DatePicker : DateTimePicker, IControl
    {
        #region********************constructors**********************
        public DatePicker()
        {
            InitializeComponent();
        }

        #endregion
        #region********************fields****************************
        private string m_xCustomFormat = "";
        private bool m_bShowCheckbox;
        private bool m_bShowUpDown;
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private string m_xSupportingValues = "";

        #endregion
        #region********************properties************************
        public string CustomDateFormat
        {
            get { return m_xCustomFormat; }
            set { m_xCustomFormat = value; }
        }
        /// <summary>
        /// gets/sets custom format to be returned by control
        /// </summary>
        public bool ShowPickerCheckbox
        {
            get { return m_bShowCheckbox; }
            set { m_bShowCheckbox = value; }
        }
        /// <summary>
        /// gets/sets custom format to be returned by control
        /// </summary>
        public bool ShowPickerUpDown
        {
            get { return m_bShowUpDown; }
            set { m_bShowUpDown = value; }
        }

        #endregion
        #region********************events****************************
        #endregion
        #region********************methods***************************
        /// <summary>
        /// sets options for calendar appearance based on property settings
        /// </summary>
        private void ConfigureDatePicker()
        {
            base.ShowCheckBox = this.ShowPickerCheckbox;
            base.ShowUpDown = this.ShowPickerUpDown;

            if (this.CustomDateFormat != "")
            {
                base.Format = DateTimePickerFormat.Custom;
                base.CustomFormat = this.CustomDateFormat;
            }
        }
        #endregion
        #region********************private functions*****************
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event EnterKeyPressedHandler EnterPressed;
        public new event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;
        /// <summary>
        /// configures calendar based on property settings
        /// </summary>
        public void ExecuteFinalSetup()
        {
            try
            {
                ConfigureDatePicker();
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.ControlEventException(
                   LMP.Resources.GetLangString("Error_CouldNotInitializeDatePicker"), oE);
            }
        }
        
        /// <summary>
        /// gets/sets the value of the control
        /// </summary>
        public new string Value
        {
            get
            {
                if (this.CustomDateFormat != "")
                    return base.Value.ToString(this.CustomDateFormat);
                else
                    return base.Value.ToString();
            }
            set
            {
                try
                {
                    if (value != "")
                        base.Value  = DateTime.Parse(value);
                }
                catch 
                {
                    //variable contains invalid data - message user we can't initialize the picker
                    string xMsg = LMP.Resources.GetLangString("Msg_UnrecognizedDateFormatInVariable");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName, 
                        MessageBoxButtons.OK, MessageBoxIcon.Information);   
                }
            }
        }
        public bool IsDirty
        {
            set { m_bIsDirty = value; }
            get { return  m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosing
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            int  iQualifierKeys;
            bool bTabPressed = LMP.OS.TabKeyPressed(m, out bShiftPressed);
            bool bEnterPressed = LMP.OS.EnterKeyPressed(m, out iQualifierKeys);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (bEnterPressed)
            {
                if (this.EnterPressed != null)
                    this.EnterPressed(this, new EnterKeyPressedEventArgs(iQualifierKeys));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        /// <summary>
        /// base notification of value changed
        /// </summary>
        /// <param name="eventargs"></param>
        protected override void OnValueChanged(EventArgs eventargs)
        {
            this.IsDirty = true;
        }
        #endregion
    }
}
