using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using LMP.Controls;
using LMP.Data;

namespace LMP.Controls
{
    public partial class PeopleDropdown : LMP.Controls.ComboBox
    {
        #region *********************fields*********************
        private mpFilterOptions m_iFilterOption;
        private string m_xWhereClause = "";
        #endregion
        #region *********************enumerations*********************
        public enum mpFilterOptions
        {
            AllPeople = 0,
            Authors = 1,
            Attorneys = 2,
            Users = 3
        }
        #endregion
        #region *********************constructors*********************
        public PeopleDropdown()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************properties*******************************
        /// gets/sets the ID of the person
        /// </summary>
        public override string Value
        {
            //GLOG 8629: Needs to have a Value property for correct Editor functionality
            get 
            { 
                return base.Value; 
            }
            set 
            {
                if (base.Value != value)
                {
                    base.Value = value;
                    base.IsDirty = true;
                }
            }
        }
        /// <summary>
        /// gets/sets the options for filtering the list
        /// </summary>
        public mpFilterOptions FilterOptions
        {
            get { return m_iFilterOption; }
            set { m_iFilterOption = value; }
        }
        /// <summary>
        /// gets/sets where clause for list filtering
        /// </summary>
        public string WhereClause
        {
            get{ return m_xWhereClause; }
            set{ m_xWhereClause = value; }
        }
        ///// <summary>
        ///// gets/sets max dropdown rows displayed
        ///// </summary>
        //public  int MaxDropDownItems
        //{
        //    get { return base.MaxDropDownItems; }
        //    set { base.MaxDropDownItems = value; }
        //}
        #endregion
        #region *********************private methods*********************
        /// <summary>
        /// loads specified filtered people list into base combo
        /// </summary>
        private void LoadPeopleList()
        {
            try
            {
                //get filtering options from control FilterOptions property
                mpTriState tsIsAttorney = this.FilterOptions == 
                    mpFilterOptions.Attorneys ? mpTriState.True : mpTriState.Undefined;
                mpTriState tsIsAuthor = this.FilterOptions == 
                    mpFilterOptions.Authors ? mpTriState.True : mpTriState.Undefined;

                //if filteroptions = users, prepend userID filter to where clause
                StringBuilder oSB = new StringBuilder();

                string xFormat = "{0}";
                string xUserClause = "UserID <> ''";
                string xWhereClause = this.WhereClause;

                if (FilterOptions == mpFilterOptions.Users && 
                    xWhereClause != "")
                {
                    if (xWhereClause != "")
                    {
                        xFormat = "{0} AND {1}";
                        oSB.AppendFormat(xFormat, xUserClause, xWhereClause);
                    }
                    else
                        oSB.AppendFormat(xFormat, xUserClause);
                }
                else
                    oSB.AppendFormat(xFormat, WhereClause);

                string xWhere = oSB.ToString();

                //retrieve filtered persons
                LocalPersons oPersons = new LocalPersons(mpPeopleListTypes.AllPeople, 0, tsIsAttorney, 
                    tsIsAuthor, 0, xWhere);

                //retrieve array with DisplayName, ID fields
                //GLOG 8629: Get ID2 for private authors
                Array aPersons = oPersons.ToArray(5, 0, 1);

                //convert to string array for loading into combo
                string[,] oList = new string[oPersons.Count, 2];

                for (int i = 0; i < aPersons.GetLength(0) ; i++)
                {
                    oList[i, 0] = ((Array)aPersons.GetValue(i)).GetValue(0).ToString();
                    //GLOG 8629: Include both ID1 and ID2 parts of ID
                    oList[i, 1] = ((Array)aPersons.GetValue(i)).GetValue(1).ToString() + "." + ((Array)aPersons.GetValue(i)).GetValue(2).ToString();
                }

                //load base combo with returned list
                base.SetList(oList);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// loads people list after all properties are set
        /// </summary>
        public override void ExecuteFinalSetup()
        {
            try
            {
                base.LimitToList = true;
                this.LoadPeopleList();
                //set initial value if any
                base.ExecuteFinalSetup();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ControlEventException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializePeopleDropdown"), oE);
            }
        }
        #endregion
        #region *********************event handlers*********************
        #endregion
    }
}
