﻿namespace LMP.Controls
{
    partial class ClientMatterSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtClientMatter = new System.Windows.Forms.TextBox();
            this.btnLookup = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtClientMatter
            // 
            this.txtClientMatter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtClientMatter.Location = new System.Drawing.Point(0, 0);
            this.txtClientMatter.Name = "txtClientMatter";
            this.txtClientMatter.Size = new System.Drawing.Size(239, 20);
            this.txtClientMatter.TabIndex = 0;
            this.txtClientMatter.TextChanged += new System.EventHandler(this.txtClientMatter_TextChanged);
            this.txtClientMatter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtClientMatter_KeyDown);
            // 
            // btnLookup
            // 
            this.btnLookup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLookup.Location = new System.Drawing.Point(238, -1);
            this.btnLookup.Name = "btnLookup";
            this.btnLookup.Size = new System.Drawing.Size(25, 20);
            this.btnLookup.TabIndex = 1;
            this.btnLookup.Text = "...";
            this.btnLookup.UseVisualStyleBackColor = true;
            this.btnLookup.Click += new System.EventHandler(this.btnLookup_Click);
            // 
            // ClientMatterSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnLookup);
            this.Controls.Add(this.txtClientMatter);
            this.Name = "ClientMatterSelector";
            this.Size = new System.Drawing.Size(262, 22);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtClientMatter;
        private System.Windows.Forms.Button btnLookup;
    }
}
