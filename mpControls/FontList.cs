using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Text;
using System.Drawing.Text;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Controls
{
    public partial class FontList : LMP.Controls.ComboBox
    {
    #region*********************fields*******************************
        private NotInListBehaviors m_iNotInList;

    #endregion
    #region*********************enumerations*************************
        private enum NotInListBehaviors
        {
            RaiseError = 1,
            SelectFirstInList = 2,
            DoNotSelectFont = 3
        }

    #endregion
    #region*********************constructors*************************
        public FontList()
        {
            InitializeComponent();
        }
    #endregion
    #region*********************properties***************************
        /// <summary>
        /// get/sets notinlist behaviors that determine how list reacts
        /// to values not contained in font list
        /// </summary>
        public int OnNotInList
        {
            get { return (int)m_iNotInList; }
            set { m_iNotInList = (NotInListBehaviors)value; }
        }
        /// <summary>
        /// gets/sets combo value
        /// </summary>
        public override string Value
        {
            get
            {
               return base.Value;
            }
            set
            {
                //check if proposed name is in font list
                //set display according to OnNotInList property value
                if (!FontNameExistsInList(value.ToString()))
                {
                    switch (m_iNotInList)
                    {
                        case NotInListBehaviors.SelectFirstInList:
                            base.SelectedIndex = 0; 
                            break;
                        case NotInListBehaviors.RaiseError:
                            string xMsg = LMP.Resources.GetLangString("Error_SpecifiedFontNameDoesNotExist");
                            MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            base.SelectedIndex = -1;
                            break;
                        case NotInListBehaviors.DoNotSelectFont:
                        default:
                            base.SelectedIndex = -1;
                            break;
                    }
                }
                else
                    base.Value = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool FontNameExistsInList(string xName)
        {
            //GLOG 5908: InstalledFontCollection does not include PostScript,
            //Raster, or non-Microsoft OpenType fonts - use Screen.Fonts in VB instead

            //InstalledFontCollection oFonts = new InstalledFontCollection();
            //FontFamily[] oFamilies = oFonts.Families;

            ////look for the proposed name in fonts collection
            //for (int i = 0; i < oFamilies.Length; i++)
            //{
            //    if (xName == oFamilies[i].Name)
            //        return true;
            //}

            object[] oFonts = (object[])LMP.Forte.MSWord.Application.ScreenFonts();
            for (int i = 0; i < oFonts.Length; i++)
            {
                if (xName == oFonts[i].ToString())
                    return true;
            }
            return false;
        }
 
    #endregion
    #region*********************events*******************************
    #endregion
    #region*********************methods******************************
        /// <summary>
        /// loads font list from existing system fonts
        /// </summary>
        private void LoadFontList()
        {
            //GLOG 5908: InstalledFontCollection does not include PostScript,
            //Raster, or non-Microsoft OpenType fonts - use Screen.Fonts in VB instead

            //InstalledFontCollection oFonts = new InstalledFontCollection();
            //FontFamily[] oFamilies = System.Drawing.FontFamily.Families; // oFonts.Families;
            //string[,] xFontList = new string[oFamilies.Length, 2];
            ////underlying combo requires a 2 col list, so duplicate both
            //for (int i = 0; i < oFamilies.Length; i++)
            //{
            //    xFontList[i, 0] = oFamilies[i].Name;
            //    xFontList[i, 1] = oFamilies[i].Name;
            //}
            object[] oFonts = (object[])LMP.Forte.MSWord.Application.ScreenFonts();
            Array.Sort(oFonts);
            string[,] xFontList = new string[oFonts.Length, 2];

            //underlying combo requires a 2 col list, so duplicate both
            for (int i = 0; i < oFonts.Length; i++)
            {
                xFontList[i, 0] = oFonts[i].ToString();
                xFontList[i, 1] = oFonts[i].ToString();
            }

            //load base combo with font list
            base.SetList(xFontList);
        }

    #endregion
    #region*********************private functions********************
    #endregion
    #region*********************protected members********************
        /// <summary>
        /// loads people list after all properties are set
        /// </summary>
        public override void ExecuteFinalSetup()
        {
            try
            {
                base.LimitToList = true;
                this.LoadFontList();
                base.ExecuteFinalSetup();
                //set initial value if any
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ControlEventException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeFontDropdown"), oE);
            }
        }

    #endregion
   }
}
