namespace LMP.Controls
{
    partial class NameValuePairGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;      
        private Infragistics.Win.UltraWinGrid.UltraGrid grdNameValuePairs;
        private System.Windows.Forms.ContextMenu mnuDateTimeInfo;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoClearRow;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoDeleteRow;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoDeleteAll;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoInsertRow;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoAddRow;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoPrefillSeparator;
     

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Label");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Value");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this.grdNameValuePairs = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.mnuDateTimeInfo = new System.Windows.Forms.ContextMenu();
            this.mnuDateTimeInfoClearRow = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoDeleteRow = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoDeleteAll = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoCut = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoCopy = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoPaste = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoPrefillSeparator2 = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoInsertRow = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoAddRow = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoPrefillSeparator = new System.Windows.Forms.MenuItem();
            this.mnuDateTimeInfoReloadDefaults = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grdNameValuePairs)).BeginInit();
            this.SuspendLayout();
            // 
            // grdNameValuePairs
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.Color.Transparent;
            this.grdNameValuePairs.DisplayLayout.Appearance = appearance1;
            this.grdNameValuePairs.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            ultraGridBand1.ColHeadersVisible = false;
            ultraGridColumn1.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            ultraGridColumn1.CellDisplayStyle = Infragistics.Win.UltraWinGrid.CellDisplayStyle.PlainText;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.Width = 14;
            ultraGridColumn2.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            ultraGridColumn2.CellDisplayStyle = Infragistics.Win.UltraWinGrid.CellDisplayStyle.PlainText;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.Width = 20;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2});
            ultraGridBand1.GroupHeadersVisible = false;
            ultraGridBand1.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
            ultraGridBand1.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
            ultraGridBand1.Override.RowSizingAutoMaxLines = 5;
            this.grdNameValuePairs.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.grdNameValuePairs.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.grdNameValuePairs.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.grdNameValuePairs.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.grdNameValuePairs.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grdNameValuePairs.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.grdNameValuePairs.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grdNameValuePairs.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.grdNameValuePairs.DisplayLayout.MaxColScrollRegions = 1;
            this.grdNameValuePairs.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grdNameValuePairs.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.grdNameValuePairs.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.grdNameValuePairs.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.grdNameValuePairs.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.grdNameValuePairs.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Synchronized;
            this.grdNameValuePairs.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.grdNameValuePairs.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.DarkGray;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.grdNameValuePairs.DisplayLayout.Override.CellAppearance = appearance8;
            this.grdNameValuePairs.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.grdNameValuePairs.DisplayLayout.Override.CellPadding = 0;
            this.grdNameValuePairs.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.CellsOnly;
            this.grdNameValuePairs.DisplayLayout.Override.DefaultRowHeight = 16;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.grdNameValuePairs.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.grdNameValuePairs.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.grdNameValuePairs.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.grdNameValuePairs.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.DarkGray;
            this.grdNameValuePairs.DisplayLayout.Override.RowAppearance = appearance11;
            this.grdNameValuePairs.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.grdNameValuePairs.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.grdNameValuePairs.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.grdNameValuePairs.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this.grdNameValuePairs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdNameValuePairs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdNameValuePairs.Location = new System.Drawing.Point(0, 0);
            this.grdNameValuePairs.Name = "grdNameValuePairs";
            this.grdNameValuePairs.Size = new System.Drawing.Size(166, 74);
            this.grdNameValuePairs.TabIndex = 11;
            this.grdNameValuePairs.Text = "ultraGrid1";
            this.grdNameValuePairs.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.grdNameValuePairs_InitializeRow);
            this.grdNameValuePairs.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.grdNameValuePair_CellChange);
            this.grdNameValuePairs.Enter += new System.EventHandler(this.grdNameValuePair_Enter);
            this.grdNameValuePairs.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdNameValuePair_KeyDown);
            this.grdNameValuePairs.KeyUp += new System.Windows.Forms.KeyEventHandler(this.grdNameValuePair_KeyUp);
            // 
            // mnuDateTimeInfo
            // 
            this.mnuDateTimeInfo.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuDateTimeInfoClearRow,
            this.mnuDateTimeInfoDeleteRow,
            this.mnuDateTimeInfoDeleteAll,
            this.menuItem1,
            this.mnuDateTimeInfoCut,
            this.mnuDateTimeInfoCopy,
            this.mnuDateTimeInfoPaste,
            this.mnuDateTimeInfoPrefillSeparator2,
            this.mnuDateTimeInfoInsertRow,
            this.mnuDateTimeInfoAddRow,
            this.mnuDateTimeInfoPrefillSeparator,
            this.mnuDateTimeInfoReloadDefaults});
            this.mnuDateTimeInfo.Popup += new System.EventHandler(this.mnuDateTimeInfo_Popup);
            // 
            // mnuDateTimeInfoClearRow
            // 
            this.mnuDateTimeInfoClearRow.Index = 0;
            this.mnuDateTimeInfoClearRow.Text = "[Clear Row]";
            this.mnuDateTimeInfoClearRow.Click += new System.EventHandler(this.mnuDateTimeInfoClearRow_Click);
            // 
            // mnuDateTimeInfoDeleteRow
            // 
            this.mnuDateTimeInfoDeleteRow.Index = 1;
            this.mnuDateTimeInfoDeleteRow.Text = "[Delete Row]";
            this.mnuDateTimeInfoDeleteRow.Click += new System.EventHandler(this.mnuDateTimeInfoDeleteRow_Click);
            // 
            // mnuDateTimeInfoDeleteAll
            // 
            this.mnuDateTimeInfoDeleteAll.Index = 2;
            this.mnuDateTimeInfoDeleteAll.Text = "[Delete All]";
            this.mnuDateTimeInfoDeleteAll.Click += new System.EventHandler(this.mnuDateTimeInfoDeleteAll_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 3;
            this.menuItem1.Text = "-";
            // 
            // mnuDateTimeInfoCut
            // 
            this.mnuDateTimeInfoCut.Index = 4;
            this.mnuDateTimeInfoCut.Text = "[Cut]";
            this.mnuDateTimeInfoCut.Click += new System.EventHandler(this.mnuDateTimeInfoCut_Click);
            // 
            // mnuDateTimeInfoCopy
            // 
            this.mnuDateTimeInfoCopy.Index = 5;
            this.mnuDateTimeInfoCopy.Text = "[Copy]";
            this.mnuDateTimeInfoCopy.Click += new System.EventHandler(this.mnuDateTimeInfoCopy_Click);
            // 
            // mnuDateTimeInfoPaste
            // 
            this.mnuDateTimeInfoPaste.Index = 6;
            this.mnuDateTimeInfoPaste.Text = "[Paste]";
            this.mnuDateTimeInfoPaste.Click += new System.EventHandler(this.mnuDateTimeInfoPaste_Click);
            // 
            // mnuDateTimeInfoPrefillSeparator2
            // 
            this.mnuDateTimeInfoPrefillSeparator2.Index = 7;
            this.mnuDateTimeInfoPrefillSeparator2.Text = "-";
            // 
            // mnuDateTimeInfoInsertRow
            // 
            this.mnuDateTimeInfoInsertRow.Index = 8;
            this.mnuDateTimeInfoInsertRow.Text = "[Insert Row]";
            this.mnuDateTimeInfoInsertRow.Click += new System.EventHandler(this.mnuDateTimeInfoInsertRow_Click);
            // 
            // mnuDateTimeInfoAddRow
            // 
            this.mnuDateTimeInfoAddRow.Index = 9;
            this.mnuDateTimeInfoAddRow.Text = "[Add Row]";
            this.mnuDateTimeInfoAddRow.Click += new System.EventHandler(this.mnuDateTimeInfoAddRow_Click);
            // 
            // mnuDateTimeInfoPrefillSeparator
            // 
            this.mnuDateTimeInfoPrefillSeparator.Index = 10;
            this.mnuDateTimeInfoPrefillSeparator.Text = "-";
            // 
            // mnuDateTimeInfoReloadDefaults
            // 
            this.mnuDateTimeInfoReloadDefaults.Index = 11;
            this.mnuDateTimeInfoReloadDefaults.Text = "[Reload Defaults]";
            this.mnuDateTimeInfoReloadDefaults.Click += new System.EventHandler(this.mnuDateTimeInfoReloadDefaults_Click);
            // 
            // NameValuePairGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.grdNameValuePairs);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "NameValuePairGrid";
            this.Size = new System.Drawing.Size(166, 74);
            this.Load += new System.EventHandler(this.DateTimeInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdNameValuePairs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem mnuDateTimeInfoReloadDefaults;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoPaste;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoPrefillSeparator2;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoCut;
        private System.Windows.Forms.MenuItem mnuDateTimeInfoCopy;
    }
}
