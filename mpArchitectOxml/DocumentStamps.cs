﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMP.Architect.Oxml
{
    public class XmlSidebar : XmlAdminSegment, LMP.Architect.Base.IDocumentStamp, ISingleInstanceSegment
    {
        #region ISingleInstanceSegment Members

        public XmlSegments GetExistingSegments(int iSection)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
    public class XmlDisclaimerSidebar : XmlSidebar
    {
    }
    public class XmlLogoSidebar : XmlSidebar
    {
    }
    public class XmlDisclaimerText : XmlAdminSegment
    {
    }
}
