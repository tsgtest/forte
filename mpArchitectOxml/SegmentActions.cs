using System;
using System.Collections;
using System.Text;

namespace LMP.Architect.Oxml
{
	/// <summary>
	/// contains the members that define a SegmentAction -
	/// an action that is executed when a segment event occurs - 
	/// created by Daniel Fisherman 10/06/05
	/// </summary>
	public class XmlSegmentAction: XmlActionBase
	{
		#region *********************fields*********************
		XmlSegment.Events m_oEvent = XmlSegment.Events.AfterDefaultValuesSet;
		XmlVariableActions.Types m_oType = XmlVariableActions.Types.InsertText;
		#endregion
		#region *********************constructors*********************
		internal XmlSegmentAction(XmlSegment oSegment)
		{
            m_oSegment = oSegment;
            m_oForteDocument = oSegment.ForteDocument;
            //m_oDocument = oSegment.ForteDocument.WordDocument;
		}
		#endregion
		#region *********************properties*********************
		/// <summary>
		/// SegmentActions have non-persistent IDs - they are used
		/// for reference at run-time, but are not otherwise necessary
		/// </summary>
		public XmlSegment.Events Event
		{
			get{return m_oEvent;}
			set
			{
				if(m_oEvent != value)
				{
					m_oEvent = value;}
				this.IsDirty = true;
			}
		}
		public XmlVariableActions.Types Type
		{
			get{return m_oType;}
			set
			{
				if(m_oType != value)
				{
					m_oType = value;
					this.IsDirty = true;
				}
			}
		}
		public override bool IsValid
		{
			get
			{
				//SegmentActions, as currently defined, always have valid data
				return true;
			}
		}
		#endregion
		#region *********************methods*********************
		/// <summary>
		/// returns the SegmentAction as an array
		/// </summary>
		/// <returns></returns>
		public override string[] ToArray(bool bIncludeID)
		{
			string[] aAction;

			if(bIncludeID)
			{
				aAction = new string[6];
				aAction[0] = this.ID;
				aAction[1] = this.ExecutionIndex.ToString();
				aAction[2] = this.ExecutionCondition;
				aAction[3] = ((int)this.Event).ToString();
				aAction[4] = ((int)this.Type).ToString();
				aAction[5] = this.Parameters;
			}
			else
			{
				aAction = new string[5];
				aAction[0] = this.ExecutionIndex.ToString();
				aAction[1] = this.ExecutionCondition;
				aAction[2] = ((int)this.Event).ToString();
				aAction[3] = ((int)this.Type).ToString();
				aAction[4] = this.Parameters;
			}

			return aAction;
		}
			
		/// <summary>
		/// executes this action
		/// </summary>
		public override void Execute()
		{
            DateTime t0 = DateTime.Now;

            string xParameters = this.Parameters;

            Trace.WriteNameValuePairs("xParameters", xParameters);

            //exit if condition for insertion is not satisfied
            if (!ExecutionIsSpecified())
                return;
            try
            {
                switch (m_oType)
                {
                    case XmlVariableActions.Types.ExecuteOnBookmark:
                        ExecuteOnBookmark(xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnBlock:
                        ExecuteOnBlock(xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnDocument:
                        ExecuteOnDocument(xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnApplication:
                        ExecuteOnApplication(xParameters);
                        break;
                    case XmlVariableActions.Types.ReplaceSegment:
                        ReplaceSegment(xParameters);
                        break;
                    //case XmlVariableActions.Types.InsertBoilerplateAtBookmark:
                    //TODO: implement or remove?
                    //break;
                    case XmlVariableActions.Types.InsertBoilerplateAtLocation:
                        //TODO: implement or remove?
                        break;
                    case XmlVariableActions.Types.SetVariableValue:
                        SetVariableValue("", xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnPageSetup:
                        ExecuteOnPageSetup(xParameters);
                        break;
                    case XmlVariableActions.Types.SetDocVarValue:
                        SetDocVarValue("", xParameters);
                        break;
                    case XmlVariableActions.Types.SetDocPropValue: //GLOG 7495
                        SetDocPropValue("", xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnStyle:
                        ExecuteOnStyle(xParameters);
                        break;
                    case XmlVariableActions.Types.RunMethod:
                        RunMethod(xParameters);
                        break;
                    case XmlVariableActions.Types.RunMacro:
                        RunMacro(xParameters);
                        break;
                    case XmlVariableActions.Types.SetAsDefaultValue:
                        SetAsDefaultValue("", xParameters);
                        break;
                    case XmlVariableActions.Types.UpdateMemoTypePeople:
                        //TODO: needs to be implemented
                        break;
                    //GLOG 3583
                    case XmlVariableActions.Types.UnderlineToLongest:
                        UnderlineTagToLongest(null, xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnSegment: //GLOG 7229
                        ExecuteOnSegment(xParameters);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                //action failed
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_SegmentActionFailed") +
                    this.ToString(), oE);
            }

            LMP.Benchmarks.Print(t0,
                Convert.ToString(m_oType), "Parameters=" + xParameters);
        }

		/// <summary>
		/// returns true iff execution condition is met or if there is no execution condition
		/// </summary>
		/// <returns></returns>
		public override bool ExecutionIsSpecified()
		{
            string xExecutionCondition = this.ExecutionCondition;

            Trace.WriteNameValuePairs("xExecutionCondition", xExecutionCondition);

            //return true if no execution condition
            if (xExecutionCondition == "")
                return true;

            //evaluate resulting execution condition
            string xRet = XmlExpression.Evaluate(xExecutionCondition,
                m_oSegment, m_oForteDocument);

            bool bRet = false;

            try
            {
                //convert to boolean
                bRet = String.ToBoolean(xRet);
            }
            catch
            {
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidExecutionCondition") +
                    xExecutionCondition);
            }
            return bRet;
        }

		#endregion	
		#region *********************private members*********************
		#endregion
	}

	public class XmlSegmentActions: XmlActionsCollectionBase
	{
		#region *********************fields*********************
        XmlSegment m_oSegment;
		#endregion
		#region *********************constructors*********************
		internal XmlSegmentActions(XmlSegment oSegment):base()
		{
            //set fields
            m_oSegment = oSegment;
            string xActionDefs = m_oSegment.GetPropertyValue("SegmentActionDefinitions");

            //populate collection
            this.SetFromString(xActionDefs);
		}
		#endregion
		#region *********************properties*********************
        public new XmlSegmentAction this[int iIndex]
        {
            get { return (XmlSegmentAction)base.ItemFromIndex(iIndex); }
        }
        public new XmlSegmentAction ItemFromIndex(int iIndex)
        {
            return (XmlSegmentAction)base.ItemFromIndex(iIndex);
        }
        #endregion
		#region *********************methods*********************
		public new XmlSegmentAction Create()
		{
			return (XmlSegmentAction) base.Create();
		}
		/// <summary>
		/// saves the specified action to the Word XML Tag
		/// </summary>
		public void Save()
		{
            //DateTime t0 = DateTime.Now;

            //string xSegmentActions = this.SegmentActionsToString();

            //if (!m_oSegment.Nodes.NodeExists(m_oSegment.FullTagID))
            //{
            //    //can't save to specified tag - alert
            //    throw new LMP.Exceptions.WordXmlNodeException(
            //        LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + m_oSegment.FullTagID);
            //}

            //string xObjectData = m_oSegment.Nodes.GetItemObjectData(m_oSegment.FullTagID);
            //string xNewObjectData = "";
            //const byte mpLenVarDef = 25;

            ////search for existing segment actions definition in tag
            //int iPos1 = xObjectData.IndexOf("SegmentActionDefinitions=");

            //if(iPos1 > -1)
            //{
            //    //segment actions definition already exists - 
            //    //replace with updated definition
            //    int iPos2 = xObjectData.IndexOf("|", iPos1 + 1);

            //    xNewObjectData = xObjectData.Substring(0, iPos1 + mpLenVarDef) + 
            //        xSegmentActions + xObjectData.Substring(iPos2);
            //}
            //else
            //{
            //    //add variable definition to object data
            //    xNewObjectData = xObjectData + "SegmentActionDefinitions=" + xSegmentActions + "|";
            //}

            ////set object data for specified Node
            //m_oSegment.Nodes.SetItemObjectData(m_oSegment.FullTagID, xNewObjectData);

            ////remove dirt for each action in collection
            //for(int i= 0; i < this.Count; i++)
            //    this[i].IsDirty = false;

            //LMP.Benchmarks.Print(t0);
		}

		/// <summary>
		/// deletes the segment action at the specified index
		/// </summary>
		/// <param name="iIndex"></param>
		public override void Delete(int iIndex)
		{
			//execute base delete
			base.Delete(iIndex);

			//save deletion - we need this in this actions collection, 
			//but not in VariableActions or ControlActions because those
			//classes are stored with the variable.  The ActionsDirtied
			//event tells the variable that it needs saving.
			this.Save();
		}

		/// <summary>
		/// executes the actions defined to execute 
		/// for the specified event
		/// </summary>
		/// <param name="iEventType"></param>
		public void Execute(XmlSegment.Events iEventType)
		{
			//cycle through segment actions, executing those that 
			//are defined to execute for the specified event type -
			//the actions will automatically get executed in their
			//specified execution order, given their storage position
			for(int i=0; i<this.Count; i++)
			{
				XmlSegmentAction oAction = this[i];

				if(oAction.Event == iEventType)
				{
                    //end execution if specified
                    if ((oAction.Type == XmlVariableActions.Types.EndExecution) &&
                        (oAction.ExecutionIsSpecified()))
                        return;

                    //GLOG 5944 (dm) - skip the SetVariableValue action when running
                    //AfterSegmentXMLInsert actions during child structure insertion -
                    //this will prevent prefill values from being overwritten
                    //GLOG 6414 (dm) - moved into ActionsBase.SetVariableValue() for
                    //broader applicability
                    //if (m_oSegment.ForteDocument.ChildStructureInsertionInProgess &&
                    //    (iEventType == Segment.Events.AfterSegmentXMLInsert) &&
                    //    (oAction.Type == XmlVariableActions.Types.SetVariableValue))
                    //    return;

                    try
					{
						oAction.Execute();
					}
					catch(System.Exception oE)
					{
						throw new LMP.Exceptions.ActionException(
							LMP.Resources.GetLangString("Error_CouldNotExecuteSegmentAction") + 
							iEventType.ToString(), oE);
					}
				}
			}
		}

        /// <summary>
        /// returns a clone of this segment action
        /// </summary>
        /// <param name="oSourceAction">the segment action to be cloned</param>
        /// <returns></returns>
        public XmlSegmentAction Clone(XmlSegmentAction oSourceAction)
        {
            //create new variable action
            XmlSegmentAction oNewAction = this.Create();

            //populate with values from source action
            oNewAction.Event = oSourceAction.Event;
            oNewAction.Type = oSourceAction.Type;
            oNewAction.ExecutionCondition = oSourceAction.ExecutionCondition;
            oNewAction.Parameters = oSourceAction.Parameters;

            return oNewAction;
        }

		#endregion
		#region *********************private members*********************
        /// <summary>
        /// returns a VariableAction populated with data supplied by specified array
        /// </summary>
        /// <param name="aNewValues"></param>
        /// <returns></returns>
        protected override LMP.Architect.Base.ActionBase GetActionFromArray(string[] aNewValues)
        {
            XmlSegmentAction oAction = null;

            try
            {
                oAction = new XmlSegmentAction(m_oSegment);
                oAction.SetID(aNewValues[0]);
                oAction.ExecutionIndex = Convert.ToInt32(aNewValues[1]);
                oAction.ExecutionCondition = aNewValues[2].ToString();
                oAction.Event = (XmlSegment.Events)Convert.ToInt32(aNewValues[3]);
                oAction.Type = (XmlVariableActions.Types)Convert.ToInt32(aNewValues[4]);
                oAction.Parameters = aNewValues[5].ToString();
                oAction.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_InvalidSegmentActionDefinition" +
                    string.Join(LMP.StringArray.mpEndOfSubValue.ToString(), aNewValues)), oE);
            }

            return oAction;
        }
        protected override LMP.Architect.Base.ActionBase GetNewActionInstance()
        {
            return new XmlSegmentAction(m_oSegment);
        }

		/// <summary>
		/// returns the collection of segment actions 
		/// assigned to this segment as a delimited string
		/// </summary>
		/// <returns></returns>
		private string SegmentActionsToString()
		{
			//get actions string
			string xActions = "";

			//cycle through actions, converting object to string
			//and adding it to the actions string
			for(int i=0; i<this.Count; i++)
			{
				XmlSegmentAction oAction = this[i];

				//convert object to string
				StringBuilder oSB = new StringBuilder();
				oSB.AppendFormat("{0}�{1}�{2}�{3}�{4}", oAction.ExecutionIndex,
					oAction.ExecutionCondition, (int) oAction.Event,
					(int) oAction.Type, oAction.Parameters);

				//add to actions string
				xActions += oSB.ToString();

				//add record separator if not last action
				if(i < this.Count - 1)
					xActions += StringArray.mpEndOfRecord;
			}

			return xActions;
		}
		#endregion
	}
}
