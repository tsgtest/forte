using System;
using System.Collections;

namespace LMP.Architect.Oxml
{
	public class XmlControlAction: XmlActionBase
	{
		#region *********************fields*********************
		XmlControlActions.Events m_oEvent = XmlControlActions.Events.ValueChanged;
		XmlControlActions.Types m_oType = XmlControlActions.Types.Visible;
        private XmlVariable m_oVariable;
        private XmlAuthors m_oAuthors;
		#endregion
		#region *********************constructors*********************
        internal XmlControlAction(XmlVariable oVariable)
            : base()
        {
            m_oVariable = oVariable;
            m_oSegment = m_oVariable.Segment;
            m_oForteDocument = oVariable.ForteDocument;
        }
        internal XmlControlAction(XmlAuthors oAuthors)
            : base()
        {
            m_oAuthors = oAuthors;
            m_oSegment = oAuthors.Segment;
            m_oForteDocument = m_oSegment.ForteDocument;
        }
        public XmlControlAction(XmlSegment oSegment)
            : base()
        {
            m_oSegment = oSegment;
            m_oForteDocument = oSegment.ForteDocument;
        }
        #endregion
		#region *********************properties*********************
		public XmlControlActions.Types Type
		{
			get{return m_oType;}
			set
			{
				if(m_oType != value)
				{
					m_oType = value;
					this.IsDirty = true;
				}
			}
		}
		public XmlControlActions.Events Event
		{
			get{return m_oEvent;}
			set
			{
				if(m_oEvent != value)
				{
					m_oEvent = value;
					this.IsDirty = true;
				}
			}
		}
		public override bool IsValid
		{
			get
			{
				//as currently defined, a control action always has valid data
				return true;
			}
		}
        public XmlVariable ParentVariable
        {
            get { return m_oVariable; }
        }
        public XmlAuthors ParentAuthors
        {
            get
            {
                if (m_oVariable != null)
                    return m_oVariable.Segment.Authors;
                else
                    return m_oAuthors;
            }
        }
        public XmlSegment ParentSegment
        {
            get
            {
                if (m_oVariable != null)
                    return m_oVariable.Segment;
                else if (m_oAuthors != null)
                    return m_oAuthors.Segment;
                else
                    return m_oSegment;
            }
        }

		#endregion
		#region *********************methods*********************
		/// <summary>
		/// returns the ControlAction as an array
		/// </summary>
		/// <returns></returns>
		public override string[] ToArray(bool bIncludeID)
		{
			string[] aAction;

			if(bIncludeID)
			{
				aAction = new string[6];
				aAction[0] = this.ID;
				aAction[1] = this.ExecutionIndex.ToString();
				aAction[2] = this.ExecutionCondition;
				aAction[3] = ((int) this.Event).ToString();
				aAction[4] = ((int) this.Type).ToString();
				aAction[5] = this.Parameters;
			}
			else
			{
				aAction = new string[5];
				aAction[0] = this.ExecutionIndex.ToString();
				aAction[1] = this.ExecutionCondition;
				aAction[2] = ((int) this.Event).ToString();
				aAction[3] = ((int) this.Type).ToString();
				aAction[4] = this.Parameters;
			}
			return aAction;
		}
			
		/// <summary>
		/// executes this action
		/// </summary>
		public override void Execute()
		{
            DateTime t0 = DateTime.Now;

            string xParameters = this.Parameters;
            string xValue = m_oVariable.Value;

            Trace.WriteNameValuePairs("xParameters", xParameters);

            //exit if condition for insertion is not satisfied
            if (!ExecutionIsSpecified())
                return;

            if (xParameters != "")
            {
                string xParamValue = xValue;

                //evaluate [MyValue] first
                xParameters = XmlFieldCode.EvaluateMyValue(xParameters, xValue);
            }

            try
            {
                switch (m_oType)
                {
                    case XmlControlActions.Types.RunMethod:
                        RunMethod(xParameters);
                        break;
                    default:
                        // Other types handled by Document Editor
                        throw new LMP.Exceptions.NotImplementedException(
                        LMP.Resources.GetLangString(
                        "Error_ControlActionExecuteMethodNotImplemented"));
                }
            }
            catch (System.Exception oE)
            {
                //action failed
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_ControlActionExecutionFailed") +
                    m_oVariable.Name, oE);
            }

            LMP.Benchmarks.Print(t0,
                Convert.ToString(m_oType), "Value=" + xValue, "Parameters=" + xParameters);

        }

        public override bool ExecutionIsSpecified()
        {
            return ExecutionIsSpecified(null);
        }
		/// <summary>
		/// returns true iff execution condition is met or if there is no execution condition
		/// </summary>
		/// <returns></returns>
		public bool ExecutionIsSpecified(string xVarValue)
		{
            string xExecutionCondition = this.ExecutionCondition;

            Trace.WriteNameValuePairs("xExecutionCondition", xExecutionCondition);

            //return true if no execution condition
            if (xExecutionCondition == "")
                return true;

            string xMyValue = null;

            if (xVarValue != null)
            {
                xMyValue = xVarValue;
            }
            if (m_oVariable != null)
            {
                //evaluate the execution condition with MyValue if the 
                //control action belongs to a variable - authors don't
                //have values so MyValue cna't be used with control actions
                //belonging to authors -
                //determine if there is a [MyValue] code in the execution condition string
                bool bMyValueExists = xExecutionCondition.ToUpper().IndexOf("MYVALUE") > -1;

                if (bMyValueExists)
                {
                    xMyValue = m_oVariable.Value;
                }
            }

            //evaluate condition
            if (xMyValue != null)
            {
                //GLOG 2151: Mark any reserved expression characters that are part of value
                //so they will be evaluated as the literal characters
                xMyValue = XmlExpression.MarkLiteralReservedCharacters(xMyValue);
                //replace all [MyValue] codes with control value
                xExecutionCondition = XmlFieldCode.EvaluateMyValue(xExecutionCondition, xMyValue);
            }

            //evaluate resulting execution condition
            string xRet = XmlExpression.Evaluate(xExecutionCondition,
                m_oSegment, m_oForteDocument);

            bool bRet = false;

            try
            {
                //convert to boolean
                bRet = String.ToBoolean(xRet);
            }
            catch
            {
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidExecutionCondition") +
                    xExecutionCondition);
            }
            return bRet;
        }

        //public override bool ExecutionIsSpecifiedOLD()
        //{
        //    string xExecutionCondition = this.ExecutionCondition;

        //    Trace.WriteNameValuePairs("xExecutionCondition", xExecutionCondition);

        //    //return true if no execution condition
        //    if (xExecutionCondition == "")
        //        return true;

        //    if (m_oVariable != null)
        //    {
        //        //evaluate the execution condition with MyValue if the 
        //        //control action belongs to a variable - authors don't
        //        //have values so MyValue cna't be used with control actions
        //        //belonging to authors
        //        //determine if there is a [MyValue] code in the execution condition string
        //        bool bMyValueExists = xExecutionCondition.IndexOf(Expression.mpMyValueCode) > -1;

        //        //evaluate condition
        //        if (bMyValueExists)
        //            //replace all [MyValue] codes with control value
        //            xExecutionCondition = xExecutionCondition.Replace(
        //                Expression.mpMyValueCode, m_oVariable.Value.ToString());
        //    }

        //    //evaluate resulting execution condition
        //    string xRet = Expression.Evaluate(xExecutionCondition,
        //        m_oSegment, this.m_oVariable.ForteDocument);

        //    bool bRet = false;

        //    try
        //    {
        //        //convert to boolean
        //        bRet = String.ToBoolean(xRet);
        //    }
        //    catch
        //    {
        //        throw new LMP.Exceptions.ArgumentException(
        //            LMP.Resources.GetLangString("Error_InvalidExecutionCondition") +
        //            xExecutionCondition);
        //    }
        //    return bRet;
        //}

		#endregion
		#region *********************private members*********************
		#endregion
	}
	
	/// <summary>
	/// Summary description for ControlActions.
	/// </summary>
	public class XmlControlActions: XmlActionsCollectionBase
	{
		#region *********************enumerations*********************
		public enum Events: byte
		{
			FirstDisplay = 1,
			LostFocus = 2,
			GotFocus = 3,
 			ValueChanged = 4
           //TODO: we might need to reinstate these events, but
            //they are difficult to work with, and of questionable use -
            //thus, they've been at least temporarily removed
			//Click = 5,
			//DoubleClick = 6
		}
		
		public enum Types: byte
		{
			Visible = 1,
			Enable = 2,
			ChangeLabel = 3,
			SetVariableValue = 4,
			RunMethod = 5,
			EndExecution = 6,
			SetDefaultValue = 7,
			SelectDialogText = 8,
			Validate = 9,
			SetFocus = 10,
			SetControlProperty = 11,
			SelectTag = 12,
			ExecuteControlActionGroup = 13,
            DisplayMessage = 14,
            RefreshControl = 15
		}

		#endregion
		#region *********************fields*********************
        private XmlVariable m_oVariable;
        private XmlAuthors m_oAuthors;
        private XmlSegment m_oSegment;
        #endregion
		#region *********************constructors*********************
		public XmlControlActions(XmlVariable oVar):base()
		{
            m_oVariable = oVar; 
		}
		public XmlControlActions(XmlAuthors oAuthors):base()
		{
            m_oAuthors = oAuthors; 
		}
        public XmlControlActions(XmlSegment oSegment)
            : base()
        {
            m_oSegment = oSegment;
        }

		#endregion
		#region *********************properties*********************
        public new XmlControlAction this[int iIndex]
        {
            get { return (XmlControlAction)this.ItemFromIndex(iIndex); }
        }
        public new XmlControlAction ItemFromIndex(int iIndex)
        {
            return (XmlControlAction)base.ItemFromIndex(iIndex);
        }
        #endregion
		#region *********************methods*********************
		public new XmlControlAction Create()
		{
			return (XmlControlAction) base.Create();
		}

        //execute all actions
        public override void Execute()
        {
            throw new LMP.Exceptions.NotImplementedException(
                LMP.Resources.GetLangString(
                "Error_ControlActionsExecuteMethodNotImplemented"));
        }
        /// <summary>
        /// returns a clone of this control action
        /// </summary>
        /// <param name="oSourceAction">the control action to be cloned</param>
        /// <returns></returns>
        public XmlControlAction Clone(XmlControlAction oSourceAction)
        {
            //create new control action
            XmlControlAction oNewAction = this.Create();

            //populate with values from source action
            oNewAction.Event = oSourceAction.Event;
            oNewAction.Type = oSourceAction.Type;
            oNewAction.ExecutionCondition = oSourceAction.ExecutionCondition;
            oNewAction.Parameters = oSourceAction.Parameters;

            return oNewAction;
        }
        #endregion
		#region *********************private members*********************
        /// <summary>
        /// returns a ControlAction populated with data supplied by specified array
        /// </summary>
        /// <param name="aNewValues"></param>
        /// <returns></returns>
        protected override LMP.Architect.Base.ActionBase GetActionFromArray(string[] aNewValues)
        {
            XmlControlAction oAction = null;

            try
            {
                if (m_oVariable != null)
                    oAction = new XmlControlAction(m_oVariable);
                else if (m_oAuthors != null)
                    oAction = new XmlControlAction(m_oAuthors);
                else
                    oAction = new XmlControlAction(m_oSegment);

                oAction.SetID(aNewValues[0]);
                oAction.ExecutionIndex = Convert.ToInt32(aNewValues[1]);
                oAction.ExecutionCondition = aNewValues[2].ToString();
                oAction.Event = (XmlControlActions.Events)Convert.ToInt32(aNewValues[3]);
                oAction.Type = (XmlControlActions.Types)Convert.ToInt32(aNewValues[4]);
                oAction.Parameters = aNewValues[5].ToString();
                oAction.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_InvalidControlActionDefinition" +
                    string.Join(LMP.StringArray.mpEndOfSubValue.ToString(), aNewValues)), oE);
            }

            return oAction;
        }
        protected override LMP.Architect.Base.ActionBase GetNewActionInstance()
        {
            if (m_oVariable != null)
                return new XmlControlAction(m_oVariable);
            else if (m_oAuthors != null)
                return new XmlControlAction(m_oAuthors);
            else
                return new XmlControlAction(m_oSegment);
        }
		#endregion
	}
}
