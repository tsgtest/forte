using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;
using LMP.Data;

namespace LMP.Architect.Oxml
{
    public class XmlEnvelopes : XmlAdminSegment, LMP.Architect.Base.IStaticDistributedSegment
    {
        public override bool PrepareForFinish()
        {
            //Run delayed actions to complete document
            this.CreationStatus = Status.Finished;
            for (int v = 0; v < this.Variables.Count; v++)
            {
                XmlVariable oVar = this.Variables[v];
                bool bDo = false;
                for (int i = 0; i < oVar.VariableActions.Count; i++)
                {
                    switch (oVar.VariableActions[i].Type)
                    {
                        case XmlVariableActions.Types.SetupLabelTable:
                        case XmlVariableActions.Types.SetupDistributedSections:
                        case XmlVariableActions.Types.InsertDetail:
                            bDo = true;
                            break;
                    }
                    if (bDo)
                        break;
                }
                if (bDo)
                    oVar.VariableActions.Execute();
            }
            return base.PrepareForFinish();
        }
    }
}
