using System;
using WinForms = System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using LMP.Controls;
using System.IO;
using System.Collections;

namespace LMP.Architect.Oxml
{
	/// <summary>
	/// contains methods and functions that get/set Word environment
	/// </summary>
	public class Environment
	{
		private fCOM.cEnvironment m_oEnv = null;
		private Word.Document m_oDoc;
		private string m_xPWD = "";
		private Word.WdProtectionType m_iProtectionType;

		#region *********************constructors*********************
		public Environment(Word.Document oDocument)
		{
			m_oDoc = oDocument;
		}
		#endregion

		#region *********************methods*********************
		/// <summary>
		/// saves current Word environment
		/// </summary>
		public void SaveState()
		{
			m_oEnv = new fCOM.cEnvironment();

            XmlForteDocument.WordXMLEvents oEvents = XmlForteDocument.IgnoreWordXMLEvents;
            XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;

            try
            {
                m_oEnv.SaveState(m_oDoc);
            }
            catch (System.Exception oE)
            {
                //show message that save state failed - don't 
                //prevent the rest of the code from executing
                LMP.Exceptions.WordDocException oNewException = new LMP.Exceptions.WordDocException(
                    LMP.Resources.GetLangString("Error_SaveStateFailed"), oE);

                LMP.Error.Show(oNewException);
            }
            finally
            {

                m_iProtectionType = m_oDoc.ProtectionType;
                XmlForteDocument.IgnoreWordXMLEvents = oEvents;
            }
		}

		/// <summary>
		/// restores original Word environment, 
		/// clears undo and displays main story
		/// </summary>
		public void RestoreState()
		{
            if (LMP.Architect.Base.Application.CurrentWordVersion != 12)
            {
                RestoreState(false, false, false);
            }
            else
            {
                RestoreState(false, false, false);
            }
		}

		/// <summary>
		/// restores original Word environment as specified
		/// </summary>
		/// <param name="bSelect"></param>
		/// <param name="bClearUndo"></param>
		/// <param name="bSeekMain"></param>
		/// <param name="bApplicationStateOnly"></param>
		public void RestoreState(bool bSelect,bool bSeekMain, bool bApplicationStateOnly)
		{
            if (m_oEnv == null)
			{
				//no state has been saved
				throw new LMP.Exceptions.NullReferenceException(LMP.Resources
					.GetLangString("Error_NoSavedEnvironmentState"));
			}

            //GLOG 7915 (dm) - disable events as already done in SaveState() and SetExecutionState()
            XmlForteDocument.WordXMLEvents oEvents = XmlForteDocument.IgnoreWordXMLEvents;
            XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;

            //restore state
            try
            {
                if (LMP.Data.Application.User.FirmSettings.ClearUndoStack)
                {
                    m_oEnv.RestoreState(m_oDoc, bSelect, true,
                     bSeekMain, bApplicationStateOnly);
                }
                else
                {
                    m_oEnv.RestoreState(m_oDoc, bSelect, false,
                        bSeekMain, bApplicationStateOnly);
                }
            }
            catch (System.Exception oE)
            {
                //show message that save state failed - don't 
                //prevent the rest of the code from executing
                //GLOG 3625: Should not use Error.Show here
                throw new LMP.Exceptions.WordDocException(
                    LMP.Resources.GetLangString("Error_RestoreStateFailed"), oE);
            }
            finally
            {

                //restore original protection status
                RestoreProtection();
                XmlForteDocument.IgnoreWordXMLEvents = oEvents;
            }
		}

		/// <summary>
		/// sets Word application environment
		/// for MacPac end-user functions as specified
		/// </summary>
		/// <param name="iShowAll"></param>
		/// <param name="iShowHiddenText"></param>
		/// <param name="iShowBookmarks"></param>
		/// <param name="iShowFieldCodes"></param>
		/// <param name="iShowXMLMarkup"></param>
		/// <param name="bUnprotect"></param>
		public void SetExecutionState(LMP.mpTriState iShowAll,
			LMP.mpTriState iShowHiddenText, LMP.mpTriState iShowBookmarks,
			LMP.mpTriState iShowFieldCodes, LMP.mpTriState iShowXMLMarkup,
			bool bUnprotect)
		{
			//unprotect if specified
			if (bUnprotect)
				UnprotectDocument();

            XmlForteDocument.WordXMLEvents oEvents = XmlForteDocument.IgnoreWordXMLEvents;
            XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;

            try
            {
                m_oEnv.SetExecutionState(m_oDoc, (int)iShowAll, (int)iShowHiddenText,
                    (int)iShowBookmarks, (int)iShowFieldCodes, (int)iShowXMLMarkup,
                    (int)LMP.mpTriState.Undefined);
            }
            finally
            {
                XmlForteDocument.IgnoreWordXMLEvents = oEvents;
            }
		}
		#endregion

		#region *********************private members*********************
        //GLOG : 6421 : CEH
        /// <summary>
		/// restores original protection status
		/// </summary>
		public void RestoreProtection()
		{
			if (m_iProtectionType != Word.WdProtectionType.wdNoProtection
                && m_oDoc.ProtectionType == Microsoft.Office.Interop.Word.WdProtectionType.wdNoProtection)
			{
				//clear out dummy password
				if (m_xPWD.ToString() == "zzmpDummy")
					m_xPWD = "";

				//reprotect
				object oTrueArg = true;
				object oFalseArg = false;
                object oPwd = m_xPWD;
				m_oDoc.Protect(m_iProtectionType, ref oTrueArg, 
					ref oPwd, ref oFalseArg, ref oFalseArg);
			}
		}

        //GLOG : 6421 : CEH
		/// <summary>
		/// ensures that document is unprotected
		/// </summary>
		public void UnprotectDocument()
		{
            ArrayList oPasswords = new ArrayList();

            //Forms Workflow standard passwords
            const string xWorkflowPwd1 = "aln777x";
            const string xWorkflowPwd2 = "aln888x";
            
            //get Firm App Settings
            LMP.Data.FirmApplicationSettings oFirmAppSettings =
                new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);

            //get protection type
            m_iProtectionType = m_oDoc.ProtectionType;

            if (m_iProtectionType != Word.WdProtectionType.wdNoProtection)
			{

                //dummy password will force error if doc is password protected
				m_xPWD = "zzmpDummy";

                //add dummy & Wordflow passwords
                oPasswords.Add(m_xPWD);
                oPasswords.Add(xWorkflowPwd1);
                oPasswords.Add(xWorkflowPwd2);

                //add firm default passwords if any
                try
                {
                    //load array of passwords
                    if (oFirmAppSettings.ProtectedDocumentsPassword != "")
                    {
                        //get firm default passwords
                        string[] axDocumentProtectionPasswords = oFirmAppSettings.ProtectedDocumentsPassword.Split(LMP.StringArray.mpEndOfSubField);

                        //get list of passwords without description field
                        for (int i = 0; i < axDocumentProtectionPasswords.Length; i = i + 2)
                        {
                            oPasswords.Add(axDocumentProtectionPasswords[i]);
                        }
                    }
                }
                catch { }


                //cycle through passwords, return if successful
                //set module level password used for restoring protection
                foreach (string xPWD in oPasswords)
                {
                    try
                    {
                        object oPWD = xPWD;
                        m_oDoc.Unprotect(ref oPWD);
                        m_xPWD = xPWD;
                        return;
                    }
                    catch
                    {
                    }
                }

                ////TODO: prompt for user password
                ////Make this an additional parameter of SetExecutionState?
                //for (int i = 1; i <=5; i++)
                //{
                //    string xUserPwd = "";
                //    //prompt for user password
                //    if (i == 1)
                //        xMsg = LMP.Resources
                //            .GetLangString("Prompt_ProtectedDocPassword");
                //    else
                //        xMsg = LMP.Resources
                //            .GetLangString("Prompt_InvalidProtectedDocPassword");

                //    try
                //    {
                //        xUserPwd = GetUserInput();
                //        m_oDoc.Unprotect(ref xUserPwd);
                //        m_xPWD = xUserPwd;
                //        return;
                //    }
                //    catch
                //    {
                //    }
                //}
                //throw exception
				throw new LMP.Exceptions.PasswordException(LMP.Resources
					.GetLangString("Error_InvalidProtectedDocPassword"));
			}
		}
		#endregion

		#region *********************static members*********************
		/// <summary>
		/// returns true if document is protected -
		/// displays message if specified
		/// </summary>
		/// <param name="oDocument"></param>
		/// <param name="bAlert"></param>
		/// <returns></returns>
		public static bool DocumentIsProtected(Word.Document oDocument, bool bAlert)
		{
            if (oDocument == null)
                return false;

            if (oDocument.ProtectionType == Word.WdProtectionType.wdNoProtection)
				return false;

			if (bAlert)
			{
				WinForms.MessageBox.Show(LMP.Resources.GetLangString
					("Msg_ProtectedDocument"), LMP.ComponentProperties.ProductName,
					WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Exclamation);
			}

			return true;
		}

		/// <summary>
		/// returns true if the current environment meets the
		/// supplied conditions - displays alert if appropriate
		/// </summary>
		/// <param name="bRequireActiveDoc"></param>
		/// <param name="bAllowProtectedDoc"></param>
		/// <param name="bAllowPrintPreview"></param>
		/// <returns></returns>
		public static bool EnvironmentIsExecutable(bool bRequireActiveDoc,
			bool bAllowProtectedDoc, bool bAllowPrintPreview)
		{
			Word.Application oApp = LMP.Architect.Base.Application.CurrentWordApp;

			if (bRequireActiveDoc && (oApp.Windows.Count == 0))
			{
				//no active document
				WinForms.MessageBox.Show(LMP.Resources.GetLangString
					("Msg_PleaseOpenDocument"), WinForms.Application.ProductName,
					WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Exclamation);
				return false;
			}
			else if ((!bAllowProtectedDoc) &&
				(DocumentIsProtected(oApp.ActiveDocument, false)))
			{
				//doc is protected
				WinForms.MessageBox.Show(LMP.Resources.GetLangString
					("Msg_ProtectedDocument"), WinForms.Application.ProductName,
					WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Exclamation);
				return false;
			}
			else if ((!bAllowPrintPreview) &&
				(oApp.ActiveWindow.View.Type == Word.WdViewType.wdPrintPreview))
			{
				//prompt user to switch to normal view
				WinForms.DialogResult oChoice = WinForms.MessageBox.Show(LMP.Resources
					.GetLangString("Prompt_SwitchToNormalView"),
					WinForms.Application.ProductName, WinForms.MessageBoxButtons.YesNo,
					WinForms.MessageBoxIcon.Exclamation);
				if (oChoice == WinForms.DialogResult.Yes)
					oApp.ActiveWindow.View.Type = Word.WdViewType.wdNormalView;
				else
					return false;
			}

			//environment meets specified conditions
			return true;
		}
        public static bool DocumentHasRevisions(Word.Document oDocument, bool bAlert)
        {
            //4-18-11 (dm) - RefreshTags has been enhanced to exclude deleted tags
            return false;

            ////JTS 1/19/11: Return whether document contains revision marks,
            ////optionally alerting user that function is unavailable
            ////TODO: Ideally, we would want to be able to Refresh Tags, while ignoring
            ////those XML Tags that are inside Deletion marks.  This is a stopgap to prevent
            ////running functions that might result in errors when deleted tags are present
            //if (oDocument == null)
            //    return false;

            //if (oDocument.Revisions.Count == 0)
            //    return false;

            //if (bAlert)
            //{
            //    WinForms.MessageBox.Show(LMP.Resources.GetLangString
            //        ("Msg_DocumentRevisionsExist"), LMP.ComponentProperties.ProductName,
            //        WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Exclamation);
            //}

            //return true;
        }

        /// <summary>
        /// returns true if .docx file is in Word 2003 compatibility mode - 
        /// alerts user that there's no content control support if specified
        /// </summary>
        /// <param name="oDocument"></param>
        /// <param name="bAlert"></param>
        /// <returns></returns>
        public static bool DocxIsIn2003CompatibilityMode(Word.Document oDocument, bool bAlert)
        {
            if (oDocument == null)
                return false;

            //GLOG 6567 (dm) - this function was previously returning False
            //unconditionally in Word 2007, because we were under the false impression
            //that Word 2007 supported content controls in this state
            fCOM.cConvert oCOMConvert = new fCOM.cConvert();
            if (!oCOMConvert.DocxIsIn2003CompatibilityMode(oDocument))
                return false;

            if (bAlert)
            {
                //7-27-11 (dm) - offer option to convert instead of just disallowing
                string xMsg = "Msg_CompatibilityModeDoesNotSupportContentControls";
                if (LMP.Architect.Base.Application.CurrentWordVersion == 12)
                    xMsg = xMsg + "_2007";
                WinForms.DialogResult iResult = WinForms.MessageBox.Show(LMP.Resources.GetLangString
                    (xMsg), LMP.ComponentProperties.ProductName,
                    WinForms.MessageBoxButtons.YesNo, WinForms.MessageBoxIcon.Question);
                if (iResult == WinForms.DialogResult.Yes)
                {
                    oDocument.Convert();
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// returns true if .doc is in compatibility mode that does not support XML Tags - 
        /// alerts user if specified
        /// </summary>
        /// <param name="oDocument"></param>
        /// <param name="bAlert"></param>
        /// <returns></returns>
        public static bool BinaryDocDoesNotSupportXMLTags(Word.Document oDocument, bool bAlert)
        {
            if (oDocument == null)
                return false;

            if (LMP.fCOMObjects.cWordDoc.GetDocumentFileFormat(oDocument) != 1)
                return false;

            if (LMP.Architect.Base.Application.CurrentWordVersion > 14 && LMP.fCOMObjects.cWordDoc.GetDocumentCompatibility(oDocument) < 12 && bAlert)
            {
                WinForms.DialogResult iResult = WinForms.MessageBox.Show(LMP.Resources.GetLangString("Msg_DocAutomationNotSupportedInWord2013"),
                    LMP.ComponentProperties.ProductName, WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Information);
                return true;
            }
            return LMP.Architect.Base.Application.CurrentWordVersion > 14;
        }
        /// <summary>
        /// returns true if default save format does not support XML Tags - 
        /// alerts user if specified
        /// </summary>
        /// <param name="oDocument"></param>
        /// <param name="bAlert"></param>
        /// <returns></returns>
        public static bool SaveFormatDoesNotSupportXMLTags(Word.Document oDocument, bool bAlert)
        {
            if (oDocument == null)
                return false;

            if (LMP.fCOMObjects.cWordDoc.GetDocumentFileFormat(oDocument) != 1)
                return false;

            if (LMP.Architect.Base.Application.CurrentWordVersion > 14 && LMP.fCOMObjects.cWordDoc.GetDocumentCompatibility(oDocument) < 12 && bAlert)
            {
                WinForms.DialogResult iResult = WinForms.MessageBox.Show(LMP.Resources.GetLangString("Msg_NewDocAutomationNotSupportedInWord2013"),
                    LMP.ComponentProperties.ProductName, WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Information);
                return true;
            }
            return LMP.Architect.Base.Application.CurrentWordVersion > 14;
        }
        /// <summary>
        /// returns true if document is legacy MacPac 8.0, 8.5 or 9 
        /// </summary>
        /// <param name="oDocument"></param>
        /// <param name="bAlert"></param>
        /// <returns></returns>
        public static bool DocumentIsLegacyMacPac(Word.Document oDocument)
        {
            string xMPVersion = "";
            string xRestarted = "";

            //get Word document variables for MacPac 9, 8.5 & 8.0
            fCOM.cWordDocument oMPDoc = LMP.fCOMObjects.cWordDoc;

            xMPVersion = oMPDoc.GetDocumentVariableValue(oDocument, "zzmpFixed_MacPacVersion", false);
            xRestarted = oMPDoc.GetDocumentVariableValue(oDocument, "Restarted", false);

            if ((xMPVersion == "9.0") || (xMPVersion == "97") || (xRestarted == "True"))
                return true;
            else
                return false;
        }

        /// <summary>
        /// returns true if document is non-Forte document
        /// </summary>
        /// <param name="oDocument"></param>
        /// <param name="bAlert"></param>
        /// <returns></returns>
        public static bool DocumentIsNonForteDocument(Word.Document oDocument, bool bAlert)
        {
            string xValue = null;

            fCOM.cWordDocument oMPDoc = LMP.fCOMObjects.cWordDoc;

            //check for 'zzmp10TP' variable value
            xValue = oMPDoc.GetDocumentVariableValue(oDocument, "zzmp10TP", false);

            if (xValue == null)
            {
                if (bAlert)
                {
                    //prompt if user hasn't turned warning off
                    ConditionalPromptDialog oDlg = new ConditionalPromptDialog();
                    oDlg.ShowDialog(LMP.Resources.GetLangString("Prompt_NonForteDocumentOpened"),
                        LMP.Resources.GetLangString("Prompt_DoNotPromptAgainSimple"), "SkipNonForteDocumentOpenedPrompt");
                }
                return true;
            }

            return false;
        }
        #endregion
	}
}
