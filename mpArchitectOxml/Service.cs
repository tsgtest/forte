using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using LMP.Data;

namespace LMP.Architect.Oxml
{
    public class XmlService : XmlAdminSegment
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
        //            iIntendedUse, mpObjectTypes.Service); //GLOG 6983
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
    }
    public class XmlServiceList : XmlAdminSegment
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
        //            iIntendedUse, mpObjectTypes.ServiceList); //GLOG 6983
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
    }
    public class XmlServiceListSeparatePage : XmlAdminSegment //GLOG 6094
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
        //            iIntendedUse, mpObjectTypes.ServiceListSeparatePage); //GLOG 6983
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
    }
}
