using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using LMP.Data;

namespace LMP.Architect.Oxml
{
    public class XmlAgreement : XmlAdminSegment
    {
    }

    public class XmlAgreementSignatures : XmlCollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.AgreementSignature; }
        }
    }

    public class XmlAgreementSignature : XmlCollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.AgreementSignatures; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.AgreementSignature; }
        }
    }

    public class XmlAgreementSignatureNonTable : XmlAdminSegment
    {
    }

    public class XmlAgreementTitlePage : XmlAdminSegment, ISingleInstanceSegment
    {
        #region ISingleInstanceSegment Members

        public XmlSegments GetExistingSegments(int iSection)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
