using System;
using System.Collections.Generic;
using System.Text;

namespace LMP.Architect.Oxml
{
    public interface ISingleInstanceSegment : LMP.Architect.Base.ISingleInstanceSegment
    {
        XmlSegments GetExistingSegments(int iSection);
    }
}
