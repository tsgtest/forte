using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using LMP.Data;
using DocumentFormat.OpenXml.Packaging;

namespace LMP.Architect.Oxml
{
    public class XmlSavedContent : XmlUserSegment
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, 
        //    Word.Range oLocation, LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, 
        //    bool bTargeted, mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation, iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_IfEmpty, bTargeted,
        //            mpSegmentIntendedUses.AsDocument, mpObjectTypes.SavedContent); //GLOG 6983
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
    }
    public class XmlUserSegment : XmlSegment
    {
        #region *********************fields*********************
        #endregion
		#region *********************constructors*********************
        public XmlUserSegment(): base() { }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// gets/sets the definition of the segment
        /// </summary>
        public new UserSegmentDef Definition
        {
            get { return (UserSegmentDef)base.Definition; }
            internal set { base.Definition = value; }
        }
        #endregion
        #region *********************methods*********************
        protected override void Initialize(string xTagID, XmlSegment oParent)
        {
            Initialize(xTagID, oParent, null);
        }
        protected override void Initialize(string xTagID, XmlSegment oParent, RawSegmentPart oRawSegmentPart)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                base.Initialize(xTagID, oParent);

                if(oRawSegmentPart == null)
                    oRawSegmentPart = this.RawSegmentParts[0];

                string xObjectData = oRawSegmentPart.GetSegmentObjectDataString();

                //get segment properties
                string xID = Query.GetmSEGObjectDataValue(xObjectData, "SegmentID");
                if (xID.Contains("."))
                {
                    int iPos = xID.IndexOf(".");
                    this.ID1 = int.Parse(xID.Substring(0, iPos));
                    this.ID2 = int.Parse(xID.Substring(iPos + 1));
                }
                else
                {
                    this.ID1 = int.Parse(Query.GetmSEGObjectDataValue(xObjectData, "SegmentID"));
                }

                this.TypeID = (mpObjectTypes)Int32.Parse(Query.GetmSEGObjectDataValue(xObjectData, "ObjectTypeID").ToString());
                this.Name = Query.GetmSEGObjectDataValue(xObjectData, "Name");
                this.DisplayName = Query.GetmSEGObjectDataValue(xObjectData, "DisplayName");

                //these are optional values - ignore missing values
                string xShowChooser = Query.GetmSEGObjectDataValue(xObjectData, "ShowChooser");
                if (!string.IsNullOrEmpty(xShowChooser))
                    this.ShowChooser = Boolean.Parse(xShowChooser);

                string xMaxAuthors = Query.GetmSEGObjectDataValue(xObjectData, "MaxAuthors");
                if (!string.IsNullOrEmpty(xMaxAuthors))
                    this.MaxAuthors = Int32.Parse(xMaxAuthors);

                //GLOG 6653
                string xAuthorNodeUILabel = Query.GetmSEGObjectDataValue(xObjectData, "AuthorNodeUILabel");
                if (xAuthorNodeUILabel == "1")
                {
                    this.AuthorsNodeUILabel = "Author";
                }
                else if (xAuthorNodeUILabel == "2")
                {
                    this.AuthorsNodeUILabel = "Attorney";
                }
                else if (!string.IsNullOrEmpty(xAuthorNodeUILabel))
                {
                    this.AuthorsNodeUILabel = xAuthorNodeUILabel;
                }
                else
                {
                    this.AuthorsNodeUILabel = "Author";
                }

                string xAuthorsHelpText = Query.GetmSEGObjectDataValue(xObjectData, "AuthorsHelpText");
                if (!string.IsNullOrEmpty(xAuthorsHelpText))
                    this.AuthorsHelpText = xAuthorsHelpText;

                string xProtectedFormPassword = Query.GetmSEGObjectDataValue(xObjectData, "ProtectedFormPassword");
                if (!string.IsNullOrEmpty(xProtectedFormPassword))
                    this.ProtectedFormPassword = xProtectedFormPassword;

                string xAuthorControlProperties = Query.GetmSEGObjectDataValue(xObjectData, "AuthorControlProperties");
                if (!string.IsNullOrEmpty(xAuthorControlProperties))
                    this.AuthorControlProperties = xAuthorControlProperties;

                string xTranslationID = Query.GetmSEGObjectDataValue(xObjectData, "TranslationID");
                if (!string.IsNullOrEmpty(xTranslationID))
                    this.TranslationID = int.Parse(xTranslationID);

                string xDefaultWrapperID = Query.GetmSEGObjectDataValue(xObjectData, "DefaultWrapperID");
                if (!string.IsNullOrEmpty(xDefaultWrapperID))
                    this.DefaultWrapperID = int.Parse(xDefaultWrapperID);

                string xIsTransparentDef = Query.GetmSEGObjectDataValue(xObjectData, "IsTransparentDef");
                if (!string.IsNullOrEmpty(xIsTransparentDef))
                    this.IsTransparentDef = bool.Parse(xIsTransparentDef);

                string xDisplayWizard = Query.GetmSEGObjectDataValue(xObjectData, "DisplayWizard");
                if (!string.IsNullOrEmpty(xDisplayWizard))
                    this.DisplayWizard = bool.Parse(xDisplayWizard);

                string xWordTemplate = Query.GetmSEGObjectDataValue(xObjectData, "WordTemplate");
                if (!string.IsNullOrEmpty(xWordTemplate))
                    this.WordTemplate = xWordTemplate;

                string xRequiredStyles = Query.GetmSEGObjectDataValue(xObjectData, "RequiredStyles");
                if (!string.IsNullOrEmpty(xRequiredStyles))
                    this.RequiredStyles = xRequiredStyles;

                string xRecreateRedirectID = Query.GetmSEGObjectDataValue(xObjectData, "RecreateRedirectID");
                if (!string.IsNullOrEmpty(xRecreateRedirectID))
                    this.RecreateRedirectID = xRecreateRedirectID;

                //transparent if defined as such, not top-level, and the only sibling of its type
                if (!this.IsTopLevel)
                {
                    if ((this.Parent.IsTransparentDef == true) &&
                        (this.Parent.IsTransparent == false))
                        //if parent that is normally transparent is showing,
                        //so should child that is normally transparent -
                        //this comes into play when a pleading caption is
                        //copied from a pleading into a blank document
                        this.IsTransparent = false;
                    else if (this.IsOnlyInstanceOfType)
                        this.IsTransparent = this.IsTransparentDef;
                    else
                    {
                        //ensure that no siblings of the same type are transparent
                        XmlSegments oSiblings = this.Parent.Segments;
                        for (int i = 0; i < oSiblings.Count; i++)
                        {
                            XmlSegment oSibling = oSiblings[i];
                            if ((oSibling.TypeID == this.TypeID) &&
                                (oSibling.FullTagID != this.FullTagID))
                                oSibling.IsTransparent = false;
                        }
                    }
                }

                //primary if no other instances or if already has been
                //established as primary -
                //GLOG item 2250 (2/10/09 dm) - this property was previously limited
                //to collection table items, but now applies to all segments

                string xIsPrimary = Query.GetmSEGObjectDataValue(xObjectData, "IsPrimary");
                if (!string.IsNullOrEmpty(xIsPrimary))
                    this.IsPrimary = bool.Parse(xIsPrimary);
                else if (this.IsOnlyInstanceOfType)
                    this.IsPrimary = true;
                else
                    this.IsPrimary = false;
                
                this.MaxAuthors = 0;

                string xLinkAuthors = Query.GetmSEGObjectDataValue(xObjectData, "LinkAuthorsToParentDef");
                if (!string.IsNullOrEmpty(xLinkAuthors))
                    this.LinkAuthorsToParentDef = ((LinkAuthorsToParentOptions) int.Parse(xLinkAuthors));

                //get LinkAuthorsToParent runtime value
                this.LinkAuthorsToParent = ((this.LinkAuthorsToParentDef ==
                    LinkAuthorsToParentOptions.Always) || ((this.LinkAuthorsToParentDef ==
                    LinkAuthorsToParentOptions.FirstChildOfType) && this.IsPrimary));

                string xAuthorControlActions = Query.GetmSEGObjectDataValue(xObjectData, "AuthorControlActions");
                if (!string.IsNullOrEmpty(xAuthorControlActions))
                    this.Authors.ControlActions.SetFromString(xAuthorControlActions);

                string xL0 = Query.GetmSEGObjectDataValue(xObjectData, "L0");
                string xL1 = Query.GetmSEGObjectDataValue(xObjectData, "L1");
                string xL2 = Query.GetmSEGObjectDataValue(xObjectData, "L2");
                string xL3 = Query.GetmSEGObjectDataValue(xObjectData, "L3");
                string xL4 = Query.GetmSEGObjectDataValue(xObjectData, "L4");

                if (!string.IsNullOrEmpty("L0") && !string.IsNullOrEmpty("L1") && !string.IsNullOrEmpty("L2")
                    && !string.IsNullOrEmpty("L3") && !string.IsNullOrEmpty("L4"))
                {
                    //all levels exist - get levels
                    this.L0 = Int32.Parse(xL0);
                    this.L1 = Int32.Parse(xL1);
                    this.L2 = Int32.Parse(xL2);
                    this.L3 = Int32.Parse(xL3);
                    this.L4 = Int32.Parse(xL4);
                }
                else if (!string.IsNullOrEmpty("L0") || !string.IsNullOrEmpty("L1") || !string.IsNullOrEmpty("L2")
                    || !string.IsNullOrEmpty("L3") || !string.IsNullOrEmpty("L4"))
                    //some levels exist, while others don't - alert
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString("Error_MissingSegmentLevelsProperty"));

                //GLOG 3413: Test for valid integer value in ObjectData before attempting to set these
                int iTest = 0;
                if (Int32.TryParse(Query.GetmSEGObjectDataValue(xObjectData, "MenuInsertionOptions"), out iTest))
                    this.MenuInsertionOptions = (InsertionLocations)iTest;
                if (Int32.TryParse(Query.GetmSEGObjectDataValue(xObjectData, "DefaultMenuInsertionBehavior"), out iTest))
                    this.DefaultMenuInsertionBehavior = (InsertionBehaviors)iTest;
                if (Int32.TryParse(Query.GetmSEGObjectDataValue(xObjectData, "DefaultDragLocation"), out iTest))
                    this.DefaultDragLocation = (InsertionLocations)iTest;
                if (Int32.TryParse(Query.GetmSEGObjectDataValue(xObjectData, "DefaultDragBehavior"), out iTest))
                    this.DefaultDragBehavior = (InsertionBehaviors)iTest;
                if (Int32.TryParse(Query.GetmSEGObjectDataValue(xObjectData, "DefaultDoubleClickLocation"), out iTest))
                    this.DefaultDoubleClickLocation = (InsertionLocations)iTest;
                if (Int32.TryParse(Query.GetmSEGObjectDataValue(xObjectData, "DefaultDoubleClickBehavior"), out iTest))
                    this.DefaultDoubleClickBehavior = (InsertionBehaviors)iTest;
                if (Int32.TryParse(Query.GetmSEGObjectDataValue(xObjectData, "IntendedUse"), out iTest))
                    this.IntendedUse = (mpSegmentIntendedUses)iTest;

                this.HelpText = Query.GetmSEGObjectDataValue(xObjectData, "HelpText");

                //get jurisdiction control properties
                string xShowCourtChooser = Query.GetmSEGObjectDataValue(xObjectData, "ShowCourtChooser");
                if (!string.IsNullOrEmpty(xShowCourtChooser))
                    this.ShowCourtChooser = bool.Parse(xShowCourtChooser);

                string xCourtChooserUILabel = Query.GetmSEGObjectDataValue(xObjectData, "CourtChooserUILabel");
                if (!string.IsNullOrEmpty(xCourtChooserUILabel))
                    this.CourtChooserUILabel = xCourtChooserUILabel;

                string xCourtChooserHelpText = Query.GetmSEGObjectDataValue(xObjectData, "CourtChooserHelpText");
                if (!string.IsNullOrEmpty(xCourtChooserHelpText))
                    this.CourtChooserHelpText = xCourtChooserHelpText;

                string xCourtChooserControlProperties = Query.GetmSEGObjectDataValue(xObjectData, "CourtChooserControlProperties");
                if (!string.IsNullOrEmpty(xCourtChooserControlProperties))
                    this.CourtChooserControlProperties = xCourtChooserControlProperties;

                string xCourtChooserControlActions = Query.GetmSEGObjectDataValue(xObjectData, "CourtChooserControlActions");
                if (!string.IsNullOrEmpty(xCourtChooserControlActions))
                    this.CourtChooserControlActions.SetFromString(xCourtChooserControlActions);

                //get dialog options
                string xShowSegmentDialog = Query.GetmSEGObjectDataValue(xObjectData, "ShowSegmentDialog");
                if (!string.IsNullOrEmpty(xShowSegmentDialog))
                    this.ShowSegmentDialog = bool.Parse(xShowSegmentDialog);

                string xDialogCaption = Query.GetmSEGObjectDataValue(xObjectData, "DialogCaption");
                if (!string.IsNullOrEmpty(xDialogCaption))
                    this.DialogCaption = xDialogCaption;

                string xDialogTabCaptions = Query.GetmSEGObjectDataValue(xObjectData, "DialogTabCaptions");
                if (!string.IsNullOrEmpty(xDialogTabCaptions))
                    this.DialogTabCaptions = xDialogTabCaptions;

                string xDefaultTrailerID = Query.GetmSEGObjectDataValue(xObjectData, "DefaultTrailerID");
                if (!string.IsNullOrEmpty(xDefaultTrailerID))
                    this.DefaultTrailerID = Int32.Parse(xDefaultTrailerID);

                //get tag prefix id
                this.TagPrefixID = LMP.String.GetTagPrefixID(xObjectData);

                if (this.Definition == null)
                {
                    try
                    {
                        //get the segment def for this segment - one may not exist, e.g. when a segment has not yet been saved
                        //GLOG 4292: ID2 may not be 0 if this is a UserSegment created by Content Designer
                        this.Definition = (UserSegmentDef)XmlSegment.GetDefFromID(this.ID1 + "." + this.ID2);
                    }

                    catch { }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeSegment") + this.ID, oE);
            }

            LMP.Benchmarks.Print(t0, this.FullTagID);
        }
        #endregion
    }
}
