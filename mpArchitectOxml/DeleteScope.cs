﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using A = DocumentFormat.OpenXml.Drawing;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;

namespace LMP.Architect.Oxml
{
    class DeleteScope
    {
        internal enum DeleteScopeTypes
        {
            Target = 0,
            WithPrecedingSpace = 1,
            WithTrailingSpace = 2,
            Paragraph = 3,
            Cell = 4,
            CellAndPreviousCell = 5,
            Row = 6,
            Table = 7
        }

        internal enum DeletedBlockProperties
        {
            ParentSegment = 0,
            ReinsertionLocation = 1,
            NativeToTable = 2,
            ParentBlock = 3
        }

        internal enum ReinsertionLocations
        {
            Before = 0,
            After = 1
        }

        /// <summary>
        /// deletes expanded scope of specified mVar -
        /// returns TRUE if expanded scope was deleted and
        /// mVar replaced with mDel -
        /// returns FALSE if only text of mVar was deleted
        /// because expanded scope is inappropriate in current context
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="oTargetCC"></param>
        /// <param name="iScope"></param>
        /// <returns></returns>
        internal static bool Delete(XmlVariable oVar, SdtElement oTargetCC,
            DeleteScopeTypes iScope, ref List<string> oPartsUpdated)
        {
            XmlSegment oSegment = oVar.Segment;
            WordprocessingDocument oDoc = oSegment.WPDoc;

            //delete the doc vars for any associated sub vars
            Query.DeleteAssociatedDocVars(oDoc, oTargetCC, "mSubVar", "", "");

            //get content of cc
            Paragraph oParentPara = null;
            OpenXmlElement oContent = null;
            if (oTargetCC is SdtBlock)
                oContent = oTargetCC.Elements<SdtContentBlock>().FirstOrDefault();
            else if (oTargetCC is SdtRun)
            {
                oContent = oTargetCC.Elements<SdtContentRun>().FirstOrDefault();
                oParentPara = oTargetCC.Ancestors<Paragraph>().FirstOrDefault();
            }
            else if (oTargetCC is SdtCell)
                oContent = oTargetCC.Elements<SdtContentCell>().FirstOrDefault();

            if (oContent == null)
            {
                oContent = oTargetCC.Elements<SdtContentRun>().FirstOrDefault();
                oParentPara = oTargetCC.Ancestors<Paragraph>().FirstOrDefault();
            }

            //delete mSubVars
            foreach (SdtElement oCC in oContent.Descendants<SdtElement>())
            {
                if (Query.GetBaseName(oCC) == "mSubVar")
                    oCC.Remove();
            }

            //delete runs
            foreach (Run oRun in oContent.Descendants<Run>())
                oRun.Remove();

            //exit if tag is not in a table and the scope calls for one
            Table oTable = oTargetCC.Ancestors<Table>().FirstOrDefault();
            bool bisInTable = oTable != null;
            if ((iScope > DeleteScopeTypes.Paragraph) && !bisInTable)
               return false;
            
            //get parent mSEG
            SdtElement omSEG = Query.GetParentSegmentContentControl(oTargetCC);

            //get range corresponding to scope
            OpenXmlElement oTargetScope = null;
            TableCell oPrevCell = null;

            switch (iScope)
            {
                case DeleteScopeTypes.Paragraph:
                    //exit if this is the only paragraph in the mSEG
                    if (omSEG.Descendants<Paragraph>().Count() < 2)
                        return false;

                    //Exit if this is only paragraph in cell
                    if (bisInTable)
                    {
                        int iParaCount = 0;
                        if (oTargetCC is SdtCell)
                        {
                            iParaCount = oTargetCC.Descendants<TableCell>().FirstOrDefault().Descendants<Paragraph>().Count();
                        }
                        else
                        {
                            iParaCount = oTargetCC.Ancestors<TableCell>().FirstOrDefault().Descendants<Paragraph>().Count();
                        }
                        if (iParaCount < 2)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //Exit if deleting paragraph would result in merging 2 tables
                        Table oPrevTable = oTargetCC.PreviousSibling() as Table;
                        Table oNextTable = oTargetCC.NextSibling() as Table;
                        if (oPrevTable != null && oNextTable != null)
                        {
                            return false;
                        }
                    }

                    if (oParentPara != null)
                        oTargetScope = oParentPara;
                    else
                        oTargetScope = oTargetCC;
                    break;
                case DeleteScopeTypes.Cell:
                case DeleteScopeTypes.CellAndPreviousCell:
                    TableCell oCell = oTargetCC.Ancestors<TableCell>().FirstOrDefault();
                    //Previous cell in same row required for this delete scope
                    if (oCell.PreviousSibling<TableCell>() == null)
                        return false;
                    if (iScope == DeleteScopeTypes.CellAndPreviousCell &&
                        oCell.PreviousSibling<TableCell>().PreviousSibling<TableCell>() == null)
                        return false;
                    oTargetScope = oCell;
                    if (iScope == DeleteScopeTypes.CellAndPreviousCell)
                        oPrevCell = oTargetScope.PreviousSibling<TableCell>();
                    break;
                case DeleteScopeTypes.Row:
                    //If only one Row, delete entire table
                    if (oTable.Descendants<TableRow>().Count() == 1)
                    {
                        oTargetScope = oTable;
                    }
                    else
                    {
                        oTargetScope = oTargetCC.Ancestors<TableRow>().FirstOrDefault();
                    }
                    break;
                case DeleteScopeTypes.Table:
                    oTargetScope = oTable;
                    break;
                case DeleteScopeTypes.WithPrecedingSpace:

                    if (oTargetCC.PreviousSibling() != null && oTargetCC.PreviousSibling() is Run)
                    {
                        Run oRun = oTargetCC.PreviousSibling<Run>();
                        Text oText = oRun.GetFirstChild<Text>();

                        if (oText.Text.EndsWith(" "))
                        {
                            oText.Text = oText.Text.Substring(0, oText.Text.Length - 1);
                        }
                    }
                    //Just clear text and return
                    return true;
                case DeleteScopeTypes.WithTrailingSpace:

                    if (oTargetCC.NextSibling() != null && oTargetCC.NextSibling() is Run)
                    {
                        Run oRun = oTargetCC.NextSibling<Run>();
                        Text oText = oRun.GetFirstChild<Text>();

                        if (oText.Text.StartsWith(" "))
                        {
                            oText.Text = oText.Text.Substring(1);
                        }
                    }
                    //Just clear text and return
                    return true;
            }

            //insert mDel/move others as necessary -
            //adjust oVar's Associated, Containing, and TagType properties
            ReinsertionLocations iReinsertionLocation = InsertmDels(oSegment, omSEG,
                oTargetScope, oTargetCC, oVar.Name, oVar.ObjectDatabaseID.ToString(), iScope);


            //remove embedded variables from collection or just adjust
            //their associated ccs when there are others outside the scope
            foreach (SdtElement oCC in oTargetScope.Descendants<SdtElement>())
            {
                if (Query.GetBaseName(oCC) == "mVar")
                {
                    string xName = Query.GetAttributeValue(oDoc, oCC, "TagID");
                    if (xName != oVar.Name)
                    {
                        XmlVariable oEmbeddedVar = oSegment.Variables.ItemFromName(xName);
                        if (oEmbeddedVar.AssociatedContentControls.Count() == 1)
                            //this is the only associated cc - delete variable
                            oSegment.Variables.Delete(xName);
                        else
                        {
                            //No need to maintain Associated CCs since property is dynamic
                        }
                    }
                }
            }
            if (oPrevCell != null && iScope == DeleteScopeTypes.CellAndPreviousCell)
            {
                foreach (SdtElement oCC in oPrevCell.Descendants<SdtElement>())
                {
                    if (Query.GetBaseName(oCC) == "mVar")
                    {
                        string xName = Query.GetAttributeValue(oDoc, oCC, "TagID");
                        if (xName != oVar.Name)
                        {
                            XmlVariable oEmbeddedVar = oSegment.Variables.ItemFromName(xName);
                            if (oEmbeddedVar.AssociatedContentControls.Count() == 1)
                                //this is the only associated cc - delete variable
                                oSegment.Variables.Delete(xName);
                            else
                            {
                                //No need to maintain Associated CCs since property is dynamic
                            }
                        }
                    }
                }

            }
            //GLOG 8698: Can't save deletion xml until mDels have been removed
            string xTargetWordOpenXML = "";
            switch (iScope)
            {
                case DeleteScopeTypes.Paragraph:
                    xTargetWordOpenXML = oTargetScope.OuterXml;
                    break;
                case DeleteScopeTypes.Cell:
                case DeleteScopeTypes.CellAndPreviousCell:
                    xTargetWordOpenXML = GetTableCellDeletionXML(oTargetScope, iScope);
                    break;
                case DeleteScopeTypes.Row:
                    //If only one Row, delete entire table
                    if (oTargetScope is Table)
                    {
                        xTargetWordOpenXML = oTargetScope.OuterXml;
                    }
                    else
                    {
                        xTargetWordOpenXML = GetTableRowDeletionXML(oTargetScope);
                    }
                    break;
                case DeleteScopeTypes.Table:
                    xTargetWordOpenXML = oTargetScope.OuterXml;
                    break;
            }
            //construct scope 'xml' - only needs to be done once per part
            string xPart = Query.GetAttributeValue(oDoc, omSEG, "PartNumber");
            if (!oPartsUpdated.Contains(xPart))
            {
                string xScope = "DeletedTag=" + oVar.Name + "¦";

                //append w:document node
                string xDocument = oDoc.MainDocumentPart.Document.OuterXml;
                int iPos = xDocument.IndexOf("<w:body");
                xDocument = xDocument.Substring(0, iPos);
                xScope += xDocument;

                xScope += GetEmbeddedContentXML(oTargetScope, oSegment);
                //Also get embedded content for previous cell
                if (oPrevCell != null && iScope == DeleteScopeTypes.CellAndPreviousCell)
                {
                    xScope += GetEmbeddedContentXML(oPrevCell, oSegment);
                }



                //append scope itself and add separator
                xScope += "<w:body>" + xTargetWordOpenXML + "</w:body>" + "|";

                //replace opening brackets as expected by backend
                xScope = xScope.Replace('<', '¬');

                //update mSEG attribute
                string xDeletedScopes = Query.GetAttributeValue(oDoc, omSEG, "DeletedScopes");
                DeleteDefinition(oDoc, ref xDeletedScopes, oVar.Name);
                Query.SetAttributeValue(oDoc, omSEG, "DeletedScopes", xDeletedScopes + xScope, false, "");

                //add part to list
                oPartsUpdated.Add(xPart);
            }
            else
            {
                //delete associated doc var - we only need the one associated with
                //the content control that's been saved to the xml
                string xTag = Query.GetTag(oTargetCC);
                Query.DeleteAssociatedDocVars(oDoc, xTag);
            }

            //delete scope
            if (iScope == DeleteScopeTypes.CellAndPreviousCell || iScope == DeleteScopeTypes.Cell)
            {
                int iSpan = 0;
                TableCell oRemainingCell = null;
                GridSpan oSpan = null;
                if (iScope == DeleteScopeTypes.CellAndPreviousCell)
                {
                    oRemainingCell = oPrevCell.PreviousSibling<TableCell>();
                    oSpan = oPrevCell.TableCellProperties.GridSpan;
                    if (oSpan != null)
                        iSpan += oSpan.Val.Value;
                    else
                        iSpan += 1;
                }
                if (oRemainingCell == null)
                {
                    oRemainingCell = oTargetScope.PreviousSibling<TableCell>();
                }
                oSpan = ((TableCell)oTargetScope).TableCellProperties.GridSpan;
                if (oSpan != null)
                {
                    iSpan += oSpan.Val.Value;
                }
                else
                {
                    iSpan += 1;
                }
                oTargetScope.Remove();
                if (oPrevCell != null && iScope == DeleteScopeTypes.CellAndPreviousCell)
                    oPrevCell.Remove();
                //Adjust GridSpan value of remaining cell
                oSpan = oRemainingCell.TableCellProperties.GridSpan;
                if (oSpan == null)
                    oSpan = oRemainingCell.TableCellProperties.AppendChild<GridSpan>(new GridSpan() {Val = iSpan+ 1});
                else
                    oSpan.Val.Value += iSpan;
            }
            else
            {
                oTargetScope.Remove();
            }

            return true;
        }
        internal static string GetTableRowDeletionXML(OpenXmlElement oRow)
        {
            Table oTable = (Table)oRow.Ancestors<Table>().FirstOrDefault().Clone();
            string xRowXML = oRow.OuterXml;
            oTable.RemoveAllChildren<TableRow>();
            oTable.AppendChild<TableRow>(new TableRow(xRowXML));
            return oTable.OuterXml;
        }
        internal static string GetTableCellDeletionXML(OpenXmlElement oCell, DeleteScopeTypes iScope)
        {
            //Start with full Table XML and remove portions unrelated to Row or Cell
            Table oTable = (Table)oCell.Ancestors<Table>().FirstOrDefault().Clone();
            TableRow oRow = (TableRow)oCell.Ancestors<TableRow>().FirstOrDefault();
            TableCell[] oCells = oRow.Descendants<TableCell>().ToArray();
            TableCell oCell1 = null;
            TableCell oCell2 = null;
            int iGridStart = 0;
            int iGridEnd = 0;
            int iLastSpan = 0;
            //Determine number of GridColumns defined for table
            //and which GridColumns relate to selected cell(s)
            for (int i = 0; i < oCells.Count(); i++)
            {
                GridSpan oGridSpan = oCells[i].TableCellProperties.GridSpan;
                iGridStart = iGridEnd + 1;
                if (oGridSpan != null)
                {
                    iLastSpan = oGridSpan.Val.Value;
                }
                else
                {
                    iLastSpan = 1;
                }
                iGridEnd += iLastSpan;
                if (oCells[i] == oCell)
                {
                    if (iScope == DeleteScopeTypes.CellAndPreviousCell)
                    {
                        iGridStart -= iLastSpan;
                        oCell1 = oCells[i-1];
                        oCell2 = oCells[i];
                    }
                    else
                    {
                        oCell1 = oCells[i];
                    }
                }

            }
            oTable.RemoveAllChildren<TableRow>();
            TableRow oNewRow = oTable.AppendChild<TableRow>(new TableRow(oRow.OuterXml));
            oNewRow.RemoveAllChildren();
            oNewRow.AppendChild<TableCell>(new TableCell(oCell1.OuterXml));
            if (oCell2 != null)
            {
                oNewRow.AppendChild<TableCell>(new TableCell(oCell2.OuterXml));
            }
            GridColumn[] oGridCols = oTable.Descendants<TableGrid>().FirstOrDefault().Descendants<GridColumn>().ToArray();
            for (int i = iGridStart - 1; i > 0; i--)
            {
                oGridCols[i-1].Remove();
            }
            oGridCols = oTable.Descendants<TableGrid>().FirstOrDefault().Descendants<GridColumn>().ToArray();
            for (int i = oGridCols.Count() - 1; i > iGridEnd - iGridStart; i--)
            {
                oGridCols[i].Remove();
            }
            return oTable.OuterXml;
        }
        /// <summary>
        /// deletes scope of specified mBlock
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="oTargetCC"></param>
        /// <returns></returns>
        internal static bool Delete(XmlBlock oBlock, SdtElement oTargetCC)
        {
            XmlSegment oSegment = oBlock.Segment;
            WordprocessingDocument oDoc = oSegment.WPDoc;
            DeleteScopeTypes iScope = DeleteScopeTypes.Paragraph;

            //get content of cc
            Paragraph oParentPara = null;
            if (oTargetCC is SdtRun)
            {
                oParentPara = oTargetCC.Ancestors<Paragraph>().First();
            }

            //exit if tag is not in a table and the scope calls for one
            bool bisInTable = oTargetCC.Ancestors<Table>().Count() > 0;

            //get parent mSEG
            SdtElement omSEG = Query.GetParentSegmentContentControl(oTargetCC);

            //get range corresponding to scope
            OpenXmlElement oTargetScope = null;
            //exit if this is the only paragraph in the mSEG
            if (omSEG.Descendants<Paragraph>().Count() < 2)
                return false;

            //TODO: exit if this is the only paragraph in a cell
            //TODO: exit if deleting will cause the merger of two tables

            if (oParentPara != null)
                oTargetScope = oParentPara;
            else
                oTargetScope = oTargetCC;

            //insert mDel/move others as necessary -
            //adjust oVar's Associated, Containing, and TagType properties
            ReinsertionLocations iReinsertionLocation = InsertmDels(oSegment, omSEG,
                oTargetScope, oTargetCC, oBlock.Name, oBlock.ObjectDatabaseID.ToString(), iScope);

            //remove embedded variables from collection or just adjust
            //their associated ccs when there are others outside the scope
            foreach (SdtElement oCC in oTargetScope.Descendants<SdtElement>())
            {
                if (Query.GetBaseName(oCC) == "mVar")
                {
                    string xName = Query.GetAttributeValue(oDoc, oCC, "TagID");
                    XmlVariable oEmbeddedVar = oSegment.Variables.ItemFromName(xName);
                    if (oEmbeddedVar.AssociatedContentControls.Count() == 1)
                        //this is the only associated cc - delete variable
                        oSegment.Variables.Delete(xName);
                }
            }

            //construct scope 'xml' - only needs to be done once per part
            string xPart = Query.GetAttributeValue(oDoc, omSEG, "PartNumber");
            string xScope = "DeletedTag=" + oBlock.Name + "¦";

            //append w:document node
            string xDocument = oDoc.MainDocumentPart.Document.OuterXml;
            int iPos = xDocument.IndexOf("<w:body");
            xDocument = xDocument.Substring(0, iPos);
            xScope += xDocument;

            xScope += GetEmbeddedContentXML(oTargetScope, oSegment);
            //TODO: add embedded content

            //TODO: modify scope xml to ensure correct cell width -
            //if the columns of a table are irregular, the xml for a single cell
            //will indicate only the width of the entire table

            //append scope itself and add separator
            xScope += "<w:body>" + oTargetScope.OuterXml + "</w:body>" + "|";

            //replace opening brackets as expected by backend
            xScope = xScope.Replace('<', '¬');

            //update mSEG attribute
            string xDeletedScopes = Query.GetAttributeValue(oDoc, omSEG, "DeletedScopes");
            DeleteDefinition(oDoc, ref xDeletedScopes, oBlock.Name);
            Query.SetAttributeValue(oDoc, omSEG, "DeletedScopes", xDeletedScopes + xScope, false, "");

            //delete scope
            oTargetScope.Remove();

            return true;
        }

        /// <summary>
        /// returns a list of the tags of the mDels that are inside
        /// the specified content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        internal static List<string> GetChildmDels(WordprocessingDocument oWPDoc, SdtElement oCC)
        {
            return GetChildmDels(oWPDoc, oCC, -1, true, false, "");
        }
        internal static List<string> GetChildmDels(WordprocessingDocument oWPDoc, SdtElement oCC, string xExcludeSegmentTagID)
        {
            return GetChildmDels(oWPDoc, oCC, -1, true, false, xExcludeSegmentTagID);
        }
        internal static List<string> GetChildmDels(WordprocessingDocument oWPDoc, SdtElement oCC, int iReinsertionLocation, bool bDelete, bool bExcludeNativeToTable, string xExcludeSegmentTagID)
        {
            if (oCC == null)
                return null;
            List<string> oList = null;
            string xExcludeSegmentIDs = xExcludeSegmentTagID;
            //If Excluding mDels for a specific Segment, also exclude Child Segments
            if (xExcludeSegmentTagID != "")
            {
                //JTS: replaced linq query to optimize speed
                //GLOG 8484: Avoid error on non-Forte Content Controls
                SdtElement[] amSegs = oCC.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mps")).ToArray();
                //var omSegs = from cc in oCC.Descendants<SdtElement>()
                //             where (from t in cc.Elements<SdtProperties>().FirstOrDefault().Elements<Tag>()
                //                    where t.Val.Value.Substring(0, 3) == "mps"
                //                    select t).Any()
                //             select cc;
                foreach (var omSeg in amSegs)
                {
                    string xTagID = Query.GetAttributeValue(oWPDoc, omSeg, "TagID");
                    if (!xExcludeSegmentIDs.Contains(xTagID))
                    {
                        xExcludeSegmentIDs = xExcludeSegmentIDs + "|" + xTagID;
                    }
                }
            }
            //JTS: replaced linq query to optimize speed
            //GLOG 8484: Avoid error on non-Forte Content Controls
            SdtElement[] amDels = oCC.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mpd")).ToArray();
            //var omDels = from cc in oCC.Descendants<SdtElement>()
            //             where (from t in cc.Elements<SdtProperties>().FirstOrDefault().Elements<Tag>()
            //                    where t.Val.Value.Substring(0, 3) == "mpd"
            //                    select t).Any()
            //             select cc;
            if (amDels.GetLength(0) > 0)
            {
                oList = new List<string>();
                foreach (SdtElement omDel in amDels)
                {
                    string xObjectData = Query.GetAttributeValue(oWPDoc, omDel, "ObjectData");
                    string[] aProps = xObjectData.Split('|');
                    bool bNativeToTable = aProps.Length > 2 && aProps[2] == "1";
                    if ((iReinsertionLocation == -1 || iReinsertionLocation == Int32.Parse(aProps[1])) && !xExcludeSegmentIDs.Contains(aProps[0]) && !(bExcludeNativeToTable && bNativeToTable))
                    {
                        oList.Add(Query.GetTag(omDel));
                        if (bDelete)
                        {
                            //GLOG 8490:  If mDel contains a Paragraph, that needs to remain as a child of the Parent node after deletion
                            Paragraph omDelPara = omDel.Descendants<Paragraph>().FirstOrDefault();
                            if (omDelPara != null)
                            {
                                omDel.InsertBeforeSelf(omDelPara.CloneNode(true));
                            }
                            omDel.Remove();
                        }
                    }
                }
            }
            return oList;

        }
        internal static List<string> GetSiblingmDels(SdtElement oCC, ReinsertionLocations iLoc)
        {
            return GetSiblingmDels(oCC, iLoc, true);
        }
        /// <summary>
        /// Get List of Tags for mDels immediately following or preceding current mDel
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="iLoc"></param>
        /// <param name="bDelete"></param>
        /// <returns></returns>
        internal static List<string> GetSiblingmDels(SdtElement oCC, ReinsertionLocations iLoc, bool bDelete)
        {
            List<string> oSiblings = new List<string>();

            OpenXmlElement oParent = oCC.Parent;
            //GLOG 8659: Make sure selected mDels have same parent element as oCC
            //(for instance if parent is TableCell, don't select mDels from other paragraphs in the TableCell)
            //GLOG 8484: Avoid error on non-Forte Content Controls
            SdtElement[] omDels = oParent.Descendants<SdtElement>().Where(cc => cc.Parent == oParent && Query.GetTag(cc).StartsWith("mpd")).ToArray();
            //var omDels = from cc in oParent.Descendants<SdtElement>()
            //             where (from t in cc.Elements<SdtProperties>().FirstOrDefault().Elements<Tag>()
            //                    where t.Val.Value.Substring(0, 3) == "mpd"
            //                    select t).Any()
            //             select cc;
            foreach (SdtElement omDel in omDels)
            {
                if ((omDel.IsBefore(oCC) && (iLoc == ReinsertionLocations.Before)) || (omDel.IsAfter(oCC) && (iLoc == ReinsertionLocations.After)))
                {
                    oSiblings.Add(Query.GetTag(omDel));
                    if (bDelete)
                    {
                        //GLOG 8490:  If mDel contains a Paragraph, that needs to remain as a child of the Parent node after deletion
                        Paragraph omDelPara = omDel.Descendants<Paragraph>().FirstOrDefault();
                        if (omDelPara != null)
                        {
                            omDel.InsertBeforeSelf(omDelPara.CloneNode(true));
                        }
                        omDel.Remove();
                    }
                }
            }
            return oSiblings;
        }
        internal static void RestoreChildmDels(WordprocessingDocument oDoc, SdtElement oCC, List<string> omDels)
        {
            SdtElement omDel = null;
            foreach (string xTag in omDels)
            {
                string xObjectData = Query.GetAttributeValue(oDoc, xTag, "ObjectData");
                if (xObjectData != "")
                {
                    string[] aProps = xObjectData.Split('|');
                    ReinsertionLocations iReinsertLoc = (ReinsertionLocations)Enum.Parse(typeof(ReinsertionLocations), aProps[1]);
                    SdtRun oDelCC = new SdtRun(new SdtProperties(new Tag() { Val = xTag }, new ShowingPlaceholder()), new SdtContentRun());
                    if (omDel != null)
                    {
                        //Insert after previous mDel
                        omDel = omDel.Parent.InsertAfter<SdtRun>(oDelCC, omDel);
                    }
                    else if (iReinsertLoc == ReinsertionLocations.Before)
                    {
                        if (oCC is SdtRow)
                        {
                            //Insert at start of first cell
                            TableCell oCell = oCC.Descendants<TableCell>().First<TableCell>();
                            omDel = oCell.PrependChild<SdtRun>(oDelCC);
                            //oCC.Descendants<SdtContentRow>().First<SdtContentRow>().PrependChild<SdtRun>(oDelCC);
                        }
                        else
                        {
                            Paragraph oPara = oCC.Ancestors<Paragraph>().FirstOrDefault();
                            if (oPara != null)
                            {
                                Run oRun = oPara.Descendants<Run>().FirstOrDefault();
                                if (oRun != null)
                                {
                                    omDel = oRun.InsertBeforeSelf<SdtRun>(oDelCC);
                                }
                                else
                                {
                                    ParagraphProperties oProps = oPara.Descendants<ParagraphProperties>().FirstOrDefault();
                                    if (oProps != null)
                                    {
                                        oProps.InsertAfterSelf<SdtRun>(oDelCC);
                                    }
                                    else
                                        oPara.PrependChild<SdtRun>(oDelCC);
                                }
                                    
                            }
                            else
                            {
                                OpenXmlElement oContent = oCC.Descendants().Where(o => o.LocalName == "sdtContent").First();
                                //GLOG 8723: If child Paragraph exists, insert mDel as child of that
                                Paragraph oChildPara = oContent.Descendants<Paragraph>().FirstOrDefault();
                                if (oChildPara != null)
                                {
                                    if (oChildPara.Descendants<ParagraphProperties>().FirstOrDefault() != null)
                                    {
                                        omDel = oChildPara.Descendants<ParagraphProperties>().FirstOrDefault().InsertAfterSelf<SdtRun>(oDelCC);
                                    }
                                    else
                                        omDel = oChildPara.PrependChild<SdtRun>(oDelCC);
                                }
                                else
                                {
                                    omDel = oContent.PrependChild<SdtRun>(oDelCC);
                                }
                            }
                        }
                    }
                    else
                    {
                        //JTS 4/21/16: If Content Control contains paragraph, append mDel to that
                        if (oCC.Descendants<Paragraph>().Count() > 0)
                            omDel = oCC.Descendants<Paragraph>().Last<Paragraph>().AppendChild<SdtRun>(oDelCC);
                        else
                        {
                            //GLOG 8679: mDel should be child of sdtContent
                            OpenXmlElement oContent = oCC.Descendants().Where(o => o.LocalName == "sdtContent").First();
                            omDel = oContent.AppendChild<SdtRun>(oDelCC);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// returns a pipe-delimited string containing the doc vars associated with
        /// the content controls embedded in the DeletedScopes attribute
        /// </summary>
        /// <param name="xDeletedScopes"></param>
        /// <returns></returns>
        internal static string GetAssociatedDocVars(string xDeletedScopes)
        {
            string xVars = "";

            string xSearch = "¬w:tag w:val=";
            int iPos = xDeletedScopes.IndexOf(xSearch);
            while (iPos > -1)
            {
                iPos += xSearch.Length + 1;
                int iPos2 = xDeletedScopes.IndexOf('\"', iPos);
                string xTag = xDeletedScopes.Substring(iPos, iPos2 - iPos);
                xVars += "|mpo" + xTag.Substring(3, 8);
                iPos = xDeletedScopes.IndexOf(xSearch, iPos);
            }
            xVars.TrimStart('|');

            return xVars;
        }

        internal static List<SdtElement> Restore(XmlVariable oVar, List<SdtElement> omSEGList, DeleteScopeTypes iScope)
        {
            List<SdtElement> omVarList = new List<SdtElement>();

            XmlSegment oSegment = oVar.Segment;
            WordprocessingDocument oDoc = oSegment.WPDoc;

            foreach (SdtElement omSEG in omSEGList)
            {
                //in multivalue scenario, there may be a mix of mSEGs and mVars
                //in the containing content controls collection
                if (Query.GetBaseName(omSEG) == "mSEG")
                {
                    //get insertion xml
                    string xBodyXml = "";
                    string xEmbeddedXml = "";
                    string xFootnotesXml = "";
                    string xEndnotesXml = "";
                    string xCommentsXml = ""; //GLOG 15921
                    string xXML = Query.GetAttributeValue(oDoc, omSEG, "DeletedScopes");
                    xXML = xXML.Replace('¬', '<');

                    OpenXmlPart oPart = Query.GetContainingOpenXmlPart(omSEG, oDoc);

                    GetInsertionXml(xXML, oVar.Name, ref xBodyXml, ref xEmbeddedXml, ref xFootnotesXml, ref xEndnotesXml, ref xCommentsXml, oPart, oDoc, oSegment.ForteDocument);

                    //get target mDels
                    //JTS: replaced linq query to optimize speed
                    //GLOG 8484: Avoid error on non-Forte Content Controls
                    SdtElement[] amDels = omSEG.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mpd") && Query.GetAttributeValue(oDoc, Query.GetTag(cc), "TagID") == oVar.Name &&
                                         Query.GetAttributeValue(oDoc, Query.GetTag(cc), "ObjectData").StartsWith(oSegment.TagID + "|")).ToArray();
                    //var omDels = from cc in omSEG.Descendants<SdtElement>()
                    //             where (from pr in cc.Elements<SdtProperties>()
                    //                    where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpd") && Query.GetAttributeValue(oDoc, o.Val.Value, "TagID") == oVar.Name &&
                    //                        Query.GetAttributeValue(oDoc, o.Val.Value, "ObjectData").StartsWith(oSegment.TagID + "|")).Any()
                    //                    select pr).Any()
                    //             select cc;

                    //cycle through mDels, calling RestoreScope for each
                    foreach (SdtElement omDel in amDels)
                    {
                        //TODO: do we need to remove any text from mDel?
                        //restore scope
                        SdtElement omVar = RestoreScope(oVar, omDel, iScope, xBodyXml, xEmbeddedXml, xFootnotesXml, xEndnotesXml, xCommentsXml, oPart);
                        omVarList.Add(omVar);
                    }
                    string xDeletedScopes = Query.GetAttributeValue(oDoc, omSEG, "DeletedScopes");
                    //Remove Variable definition from Deleted Scopes and delete old doc variable
                    DeleteDefinition(oDoc, ref xDeletedScopes, oVar.Name);
                    Query.SetAttributeValue(oDoc, omSEG, "DeletedScopes", xDeletedScopes, false, "");
                }
                else
                {
                    //Multi-value variable may have mixture of mVars and mSEGs
                    //Make sure original mVars remain in list
                    omVarList.Add(omSEG);
                }
            }
            //Update TagType
            oVar.TagType = Base.ForteDocument.TagTypes.Variable;
            //return a list of inserted mVars
            return omVarList;
        }
        /// <summary>
        /// Parses DeleteScope XML to extract XML for various parts related to Variable or Block
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="xVarName"></param>
        /// <param name="xBodyXml"></param>
        /// <param name="xEmbeddedXml"></param>
        /// <param name="xNotesXml"></param>
        /// <param name="xEndnotesXML"></param>
        internal static void GetInsertionXml(string xXML, string xVarName, ref string xBodyXml, ref string xEmbeddedXml, ref string xFootnotesXml, ref string xEndnotesXML, 
            ref string xCommentsXml, OpenXmlPart oPart, WordprocessingDocument oDoc, XmlForteDocument oForteDoc)
        {
            int iPos = xXML.IndexOf("DeletedTag=" + xVarName + "¦");
            if (iPos > -1)
            {
                int iPos2 = xXML.IndexOf("|DeletedTag=", iPos);
                if (iPos2 == -1)
                    iPos2 = xXML.Length - 1;
                xXML = xXML.Substring(iPos, iPos2 - iPos);
                Query.GetEmbeddedPartsXml(xXML, ref xBodyXml, ref xEmbeddedXml, ref xFootnotesXml, ref xEndnotesXML, ref xCommentsXml, oPart, oDoc, oForteDoc);
            }
            else
                return;
        }
        internal static bool Restore(XmlBlock oBlock, SdtElement omSEG)
        {
            SdtElement omBlock = null;
            XmlSegment oSegment = oBlock.Segment;
            WordprocessingDocument oDoc = oSegment.WPDoc;

            //get insertion xml
            //get insertion xml
            string xBodyXml = "";
            string xEmbeddedXml = "";
            string xFootnotesXml = "";
            string xEndnotesXml = "";
            string xCommentsXml = ""; //GLOG 15921
            string xXML = Query.GetAttributeValue(oDoc, omSEG, "DeletedScopes");
            xXML = xXML.Replace('¬', '<');

            OpenXmlPart oPart = Query.GetContainingOpenXmlPart(omSEG, oDoc);

            GetInsertionXml(xXML, oBlock.Name, ref xBodyXml, ref xEmbeddedXml, ref xFootnotesXml, ref xEndnotesXml, ref xCommentsXml, oPart, oDoc, oSegment.ForteDocument); //GLOG 15921

            //get target mDels
            //JTS: replaced linq query to optimize speed
            //GLOG 8484: Avoid error on non-Forte Content Controls
            SdtElement[] amDels = omSEG.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mpd") && Query.GetAttributeValue(oDoc, Query.GetTag(cc), "TagID") == oBlock.Name &&
                     Query.GetAttributeValue(oDoc, Query.GetTag(cc), "ObjectData").StartsWith(oSegment.TagID + "|")).ToArray();

            //var omDels = from cc in omSEG.Descendants<SdtElement>()
            //                where (from pr in cc.Elements<SdtProperties>()
            //                    where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpd") && Query.GetAttributeValue(oDoc, o.Val.Value, "TagID") == oBlock.Name &&
            //                        Query.GetAttributeValue(oDoc, o.Val.Value, "ObjectData").StartsWith(oSegment.TagID + "|")).Any()
            //                    select pr).Any()
            //                select cc;

            //cycle through mDels, calling RestoreScope for each
            foreach (SdtElement omDel in amDels)
            {
                //TODO: do we need to remove any text from mDel?

                //restore scope
                omBlock = RestoreScope(oBlock, omDel, xBodyXml, xEmbeddedXml, xFootnotesXml, xEndnotesXml, xCommentsXml, oPart);
            }
            if (omBlock != null)
            {
                string xDeletedScopes = Query.GetAttributeValue(oDoc, omSEG, "DeletedScopes");
                //Remove Variable definition from Deleted Scopes and delete old doc variable
                DeleteDefinition(oDoc, ref xDeletedScopes, oBlock.Name);
                Query.SetAttributeValue(oDoc, omSEG, "DeletedScopes", xDeletedScopes, false, "");
            }
            //return a list of inserted mVars
            return omBlock != null;
        }

        private static SdtElement RestoreScope(XmlVariable oVar, SdtElement omDel, DeleteScopeTypes iScope, string xBodyXml,
            string xEmbeddedXml, string xFootnoteRefXml, string xEndnoteRefXml, string xCommentsRefXml, OpenXmlPart oPart)
        {
            SdtElement omVar = null;
            XmlSegment oSegment = oVar.Segment;
            WordprocessingDocument oDoc = oSegment.WPDoc;

            string xBodyDocXml = String.GetMinimalOpeningXML(true) + "<w:body>" + xBodyXml + "</w:body>" + String.GetMinimalClosingXML(true);
            XmlDocument oXDoc = new XmlDocument();
            oXDoc.LoadXml(xBodyDocXml);
            XmlNamespaceManager oNSM = new XmlNamespaceManager(oXDoc.NameTable);
            oNSM.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            XmlNode oBodyNode = oXDoc.SelectSingleNode("descendant::w:body", oNSM);

            //GLOG 8726: Make sure there is no mpNewSegment bookmark in insertion XML
            string xBmkID = "";
            foreach (XmlNode oChildNode in oBodyNode)
            {
                if (oChildNode.Name == "w:bookmarkStart")
                {
                    if (oChildNode.Attributes.GetNamedItem("w:name").Value == "mpNewSegment")
                    {
                        xBmkID = oChildNode.Attributes.GetNamedItem("w:id").Value;
                        oBodyNode.RemoveChild(oChildNode);
                        break;
                    }
                }
            }
            if (xBmkID != "")
            {
                foreach (XmlNode oChildNode in oBodyNode)
                {
                    if (oChildNode.Name == "w:bookmarkEnd")
                    {
                        if (oChildNode.Attributes.GetNamedItem("w:id").Value == xBmkID)
                        {
                            oBodyNode.RemoveChild(oChildNode);
                            break;
                        }
                    }
                }

            }
            //get reinsertion location
            string xObjectData = Query.GetAttributeValue(oDoc, omDel, "ObjectData");
            string[] oProps = xObjectData.Split('|');

            ReinsertionLocations iReinsertLoc = (ReinsertionLocations)Enum.Parse(typeof(ReinsertionLocations), oProps[1]);

            OpenXmlElement oParentVar = Query.GetNearestAncestorContentControl(omDel, "mVar");
            OpenXmlElement oParentBlock = Query.GetNearestAncestorContentControl(omDel, "mBlock");
            Table oTable = omDel.Ancestors<Table>().FirstOrDefault();
            
            bool bNativeToTable = oProps[2] == "1" ? true : false;
            if (!bNativeToTable && oTable != null)
            {
                //GLOG 8875: NativeToTable property may not have been set properly when saving design if mDel is in last paragraph of segment.
                //If mDel is in last paragraph of mSEG and mSEG TagID matches TagID of parent segment for mDel,
                //We want to make sure Restored scope stays within table belonging to this mSEG.
                SdtElement oParentmSEG = Query.GetNearestAncestorContentControl(omDel, "mSEG");
                OpenXmlElement oLastPara = oParentmSEG.Descendants<Paragraph>().LastOrDefault();

                if (oLastPara != null && oLastPara.Contains(omDel) &&  Query.GetAttributeValue(oDoc, oParentmSEG, "TagID")  == oSegment.TagID)
                {
                    bNativeToTable = true;
                }
            }

            OpenXmlElement oParent = null;
            if ((iScope == DeleteScopeTypes.Row || iScope == DeleteScopeTypes.Cell || iScope == DeleteScopeTypes.CellAndPreviousCell) &&
                oTable == null)
            {
                iScope = DeleteScopeTypes.Table;
            }
            //If mDel is not native to table, insert above or below table depending on ReinsertionLocation
            if (oTable != null && iScope == DeleteScopeTypes.Paragraph && !bNativeToTable)
            {
                oParent = oTable;
            }
            else if (iScope == DeleteScopeTypes.Row)
            {
                oParent = omDel.Ancestors<TableRow>().FirstOrDefault();
            }
            else if (iScope == DeleteScopeTypes.CellAndPreviousCell || iScope == DeleteScopeTypes.Cell)
            {
                if (omDel is SdtCell)
                {
                    omDel = XmlForteDocument.ConvertSdtCellToSdtBlock((SdtCell)omDel);
                }
                oParent = omDel.Ancestors<TableCell>().FirstOrDefault();
            }
            else if (omDel is SdtRun)
            {
                oParent = omDel.Ancestors<Paragraph>().FirstOrDefault();
            }
            else
            {
                oParent = omDel;
            }
            if (oParentVar is SdtBlock && (iScope <= DeleteScopeTypes.Paragraph || iScope == DeleteScopeTypes.Table))
            {
                oParent = oParentVar;
            }
            else if (oParentVar is SdtRun && iScope < DeleteScopeTypes.Paragraph)
            {
                oParent = oParentVar;
            }
            else if (oParentVar is SdtRun && iScope == DeleteScopeTypes.Paragraph)
            {
                oParent = oParentVar.Ancestors<Paragraph>().First();
            }
            if (oParentBlock != null && oParentBlock.Descendants<Paragraph>().Contains(oParent))
            {
                if (iReinsertLoc == ReinsertionLocations.Before && oParent is Paragraph && oParent.PreviousSibling() != null && !(oParent.PreviousSibling() is Paragraph))
                {
                    oParent = oParentBlock;
                }
                else if (iReinsertLoc == ReinsertionLocations.After && oParent is Paragraph && oParent.NextSibling() != null && !(oParent.NextSibling() is Paragraph))
                {
                    oParent = oParentBlock;
                }

            }
            List<string> omDels = null;
            if (oParent != null)
            {
                omDels = GetSiblingmDels(omDel, iReinsertLoc);
                //GLOG 15761: Top-level element may be w:tbl, w:sdt or w:p.
                //Skip over any additional elements in the Delete Scope XML (such as bookmarks) prior to one of those types.
                XmlNode oFirstNode = oBodyNode.FirstChild;
                while (oFirstNode != null && oFirstNode.Name != "w:sdt" && oFirstNode.Name != "w:p" && oFirstNode.Name != "w:tbl")
                {
                    oFirstNode = oFirstNode.NextSibling;
                }

                if (oFirstNode.Name == "w:tbl")
                {
                    //Create Table Object from Deletion XML
                    Table oDelTable = new Table(oFirstNode.OuterXml);
                    if (iScope == DeleteScopeTypes.Table)
                    {
                        //GLOG 8697: Handle Table Delete Scope
                        if (iReinsertLoc == ReinsertionLocations.After)
                            oParent = oParent.InsertAfterSelf<Table>(new Table(oDelTable.OuterXml));
                        else
                            oParent = oParent.InsertBeforeSelf<Table>(new Table(oDelTable.OuterXml));
                    }
                    else if (iScope == DeleteScopeTypes.Row)
                    {
                        //Get Deleted Row
                        GridColumn[] aGridCols = oTable.Descendants<TableGrid>().FirstOrDefault().Descendants<GridColumn>().ToArray();
                        TableRow oDelRow = oDelTable.Descendants<TableRow>().FirstOrDefault();
                        TableCell[] aDelCells = oDelTable.Descendants<TableCell>().ToArray();
                        int iRowSpan = 0;
                        int iStartSpan = 0;

                        for (int c = 0; c < aDelCells.GetLength(0); c++)
                        {
                            iStartSpan = iRowSpan + 1;
                            TableCell oNewCell = aDelCells[c];
                            GridSpan oSpan = oNewCell.TableCellProperties.GridSpan;
                            if (oSpan != null)
                            {
                                iRowSpan += oSpan.Val.Value;
                            }
                            else
                            {
                                iRowSpan++;
                            }
                            if (c == aDelCells.GetUpperBound(0) && iRowSpan < aGridCols.GetLength(0))
                            {
                                //GLOG 15924: Number of columns may not have been the same when delete scope was created
                                //Make sure sum of row GridSpans matches total for table
                                int iLastSpan = aGridCols.GetLength(0) - (iStartSpan - 1);
                                if (oSpan == null)
                                {
                                    oSpan = new GridSpan() { Val = iLastSpan };
                                    oNewCell.TableCellProperties.AppendChild(oSpan);
                                }
                                else
                                {
                                    oSpan.Val = iLastSpan;
                                }
                            }
                        }
                        //GLOG 8876
                        if (iReinsertLoc == ReinsertionLocations.After)
                            oParent = oParent.InsertAfterSelf<TableRow>(new TableRow(oDelRow.OuterXml));
                        else
                            oParent = oParent.InsertBeforeSelf<TableRow>(new TableRow(oDelRow.OuterXml));

                    }
                    else if (iScope == DeleteScopeTypes.Cell || iScope == DeleteScopeTypes.CellAndPreviousCell)
                    {
                        //JTS 1/28/16: To get proper cell widths there are 3 components that need to be adjusted:
                        // - Table Object: TableGrid.GridColumns
                        // - Cell Object: Cell Width
                        // - Cell Object: Gridspan - indicates how many of Gridcolumns are spanned by the cell
                        TableCell oInitCell = (TableCell)oParent;
                        //GridColumn objects
                        TableGrid oGrid = oTable.Descendants<TableGrid>().FirstOrDefault();
                        GridColumn[] aGridCols = oGrid.Descendants<GridColumn>().ToArray();
                        GridColumn[] aDelCols = oDelTable.Descendants<GridColumn>().ToArray();
                        //TableCell objects from Deleted XML
                        TableCell[] aDelCells = oDelTable.Descendants<TableCell>().ToArray();
                        TableCell[] oCells = oInitCell.Ancestors<TableRow>().FirstOrDefault<TableRow>().Descendants<TableCell>().ToArray();
                        int iLastCol = 0;
                        int iStartCol = 0;
                        int iGridCols = aGridCols.Count();
                        //Get first and last initial GridColumn spanned by Parent cell
                        foreach (TableCell oCell in oCells)
                        {
                            GridSpan oSpan = oCell.TableCellProperties.GridSpan;
                            iStartCol = iLastCol + 1;
                            if (oSpan != null)
                            {
                                iLastCol += oSpan.Val.Value;
                            }
                            else
                            {
                                iLastCol++;
                            }
                            if (oCell == oInitCell)
                                break;
                        }
                        //Get starting cell width - inserted cells will be created within this width
                        string sLastWidth = oInitCell.TableCellProperties.TableCellWidth.Width.Value;
                        //Does starting cell include a GridSpan?
                        GridSpan oGridSpan = oInitCell.TableCellProperties.GridSpan;
                        int iGridSpan = 1;
                        if (oGridSpan != null)
                        {
                            iGridSpan = oInitCell.TableCellProperties.GridSpan.Val.Value;
                        }
                        int iNewCellsWidth = 0;
                        int iNewCellCount = 0;
                        //Add new cells after parent cell
                        foreach (TableCell oNewCell in aDelCells)
                        {
                            iNewCellCount++;
                            //Get Column width from GridColumns of Table XML 
                            string sWidth = aDelCells[iNewCellCount - 1].TableCellProperties.TableCellWidth.Width.Value;
                            if (oNewCell.Parent is SdtContentCell)
                            {
                                SdtCell oCellCC = oNewCell.Ancestors<SdtCell>().First();
                                oParent = oParent.InsertAfterSelf<SdtCell>(new SdtCell(oCellCC.OuterXml));
                            }
                            else
                            {
                                oParent = oParent.InsertAfterSelf<TableCell>(new TableCell(oNewCell.OuterXml));
                            }
                            iNewCellsWidth += Int32.Parse(sWidth);
                            //Reduce GridSpan value of initial cell if spanning multiple grid columns
                            if (iGridSpan > 1)
                            {
                                iGridSpan--;
                                iLastCol--;
                            }
                        }
                        //Reduce width of initial cell by total of widths of inserted cells
                        oInitCell.TableCellProperties.TableCellWidth.Width.Value = (Int32.Parse(sLastWidth) - iNewCellsWidth).ToString();
                        //If inserted cells have increased number of GridColumns,
                        //append GridColumns from XML to Table object
                        int iOrigCols = iGridCols;
                        if (iLastCol + iNewCellCount > iGridCols)
                        {
                            int iNewGridColsWidth = 0;
                            for (int d = aDelCols.Count() - 1; d >= 0; d--)
                            {
                                iNewGridColsWidth += Int32.Parse(aDelCols[d].Width.Value);
                                aGridCols[iLastCol - 1].InsertAfterSelf<GridColumn>((GridColumn)aDelCols[d].Clone());
                                iGridCols++;
                            }
                            aGridCols[iLastCol - 1].Width.Value = (Int32.Parse(aGridCols[iLastCol - 1].Width.Value) - iNewGridColsWidth).ToString();
                        }
                        //Refresh GridColumn collection
                        aGridCols = oGrid.Descendants<GridColumn>().ToArray();
                        iGridSpan = (iGridCols - iNewCellCount) - (iStartCol - 1);
                        //Adjust Parent cell GridSpan value - remove if necessary
                        if (iGridSpan > 1)
                        {
                            if (oGridSpan == null)
                            {
                                oGridSpan = new GridSpan();
                                oInitCell.TableCellProperties.AppendChild<GridSpan>(oGridSpan);
                            }
                            oGridSpan.Val = iGridSpan;
                        }
                        else if (oGridSpan != null)
                        {
                            oGridSpan.Remove();
                        }
                        TableRow[] oRows = oInitCell.Ancestors<Table>().FirstOrDefault<Table>().Descendants<TableRow>().ToArray();
                        //Need to adjust GridSpan value for any cells in other rows that overlap with initial cell
                        foreach (TableRow oRow in oRows)
                        {
                            TableCell[] oRowCells = oRow.Descendants<TableCell>().ToArray();
                            //Skip row containing Parent cell
                            if (oRowCells.Contains(oInitCell))
                                continue;

                            int iGridCount = 0;
                            
                            int iColCount =  oRowCells.Count();
                            
                            if (iColCount == iGridCols)
                                continue;
                            
                            int[] aRowSpan = new int[iColCount];
                            int iRowSpan = 0;
                            for (int c = 0; c < iColCount; c++)
                            {
                                TableCell oRowCell = oRowCells[c];
                                GridSpan oCellSpan = oRowCell.TableCellProperties.GridSpan;
                                int iSpan = 1;
                                if (oCellSpan != null)
                                {
                                    iSpan = oCellSpan.Val.Value;
                                }
                                aRowSpan[c] = iSpan;
                                iRowSpan += iSpan;
                            }
                            for (int c = 0; c < iColCount; c++)
                            {
                                TableCell oRowCell = oRowCells[c];
                                int iSpan = aRowSpan[c];
                                if (iGridCount + iSpan >= iStartCol)
                                {
                                    //int iCellWidth = Int32.Parse(oRowCell.TableCellProperties.TableCellWidth.Width.Value);
                                    GridSpan oCellSpan = oRowCell.TableCellProperties.GridSpan;
                                    int iCurSpan = aGridCols.Count() - (iRowSpan - iSpan);
                                    if (oCellSpan == null)
                                    {
                                        oCellSpan = new GridSpan() { Val = iCurSpan };
                                        oRowCell.TableCellProperties.AppendChild(oCellSpan);
                                    }
                                    else
                                    {
                                        oCellSpan.Val.Value = iCurSpan;
                                    }
                                    //oRowCell.TableCellProperties.TableCellWidth.Width.Value = (iCellWidth + iNewCellsWidth).ToString();
                                    break;
                                }
                                iGridCount += iSpan;
                            }
                        }
                    }
                    if (oParent is SdtCell)
                    {
                        //Last restored cell was part of an SdtCell - get mVar from Parent Row
                        omVar = oParent.Ancestors<TableRow>().First().Descendants<SdtElement>().Where(cc => Query.GetAttributeValue(oDoc, cc, "Name") == oVar.Name && Query.GetBaseName(cc) == "mVar").FirstOrDefault();
                    }
                    else
                    {
                        omVar = oParent.Descendants<SdtElement>().Where(cc => Query.GetAttributeValue(oDoc, cc, "Name") == oVar.Name).FirstOrDefault();
                    }
                }
                else if (iReinsertLoc == ReinsertionLocations.Before)
                {
                    if (oFirstNode.Name == "w:sdt")
                    {
                        oParent = oParent.InsertBeforeSelf<SdtBlock>(new SdtBlock(oFirstNode.OuterXml));
                        omVar = (SdtElement)oParent;
                    }
                    else
                    {

                        oParent = oParent.InsertBeforeSelf<Paragraph>(new Paragraph(oFirstNode.OuterXml));
                        omVar = oParent.Descendants<SdtElement>().Where(cc => Query.GetAttributeValue(oDoc, cc, "Name") == oVar.Name).FirstOrDefault();
                    }
                }
                else if (iReinsertLoc == ReinsertionLocations.After)
                {
                    if (oFirstNode.Name == "w:sdt")
                    {
                        oParent = oParent.InsertAfterSelf<SdtBlock>(new SdtBlock(oFirstNode.OuterXml));
                        omVar = (SdtElement)oParent;
                    }
                    else
                    {
                        oParent = oParent.InsertAfterSelf<Paragraph>(new Paragraph(oFirstNode.OuterXml));
                        omVar = oParent.Descendants<SdtElement>().Where(cc => Query.GetAttributeValue(oDoc, cc, "Name") == oVar.Name).FirstOrDefault();
                    }
                }
                if (omVar != null)
                {
                    //Assign mVar Content Control new Tag
                    Query.ReTagContentControl(omVar, oDoc);

                    if (iScope >= DeleteScopeTypes.Paragraph && oParent != null)
                    {
                        //Retag other content controls in restored range
                        foreach (SdtElement oCC in oParent.Descendants<SdtElement>().Where(cc => Query.GetTag(cc) != "").ToArray<SdtElement>())
                        {
                            Query.ReTagContentControl(oCC, oDoc);
                        }
                    }
                    //Delete mDel, Doc Variable and bookmark
                    Query.DeleteAssociatedDocVars(oDoc, omDel);
                    Query.DeleteBookmark("_" + Query.GetTag(omDel), oDoc);
                    if (omDel is SdtCell)
                    {
                        //GLOG 8876: Move child cell into to parent row
                        TableCell omDelCell = omDel.Descendants<TableCell>().FirstOrDefault();
                        if (omDelCell != null)
                        {
                            omDel.InsertBeforeSelf(omDelCell.CloneNode(true));
                        }
                    }
                    else
                    {
                        //GLOG 8490:  If mDel contains a Paragraph, that needs to remain as a child of the Parent node after deletion
                        Paragraph omDelPara = omDel.Descendants<Paragraph>().FirstOrDefault();
                        if (omDelPara != null)
                        {
                            omDel.InsertBeforeSelf(omDelPara.CloneNode(true));
                        }
                    }
                    omDel.Remove();
                    RestoreChildmDels(oDoc, omVar, omDels);
                    Query.RestoreEmbeddedParts(xEmbeddedXml, xFootnoteRefXml, xEndnoteRefXml, xCommentsRefXml, oPart, oDoc, oSegment.ForteDocument);
                }
            }
            return omVar;
        }

        private static SdtElement RestoreScope(XmlBlock oBlock, SdtElement omDel, string xBodyXML,
            string xEmbeddedXML, string xFootnoteRefXML, string xEndnoteRefXML, string xCommentsRefXml, OpenXmlPart oPart)
        {
            SdtElement omBlock = null;
            XmlSegment oSegment = oBlock.Segment;
            WordprocessingDocument oDoc = oSegment.WPDoc;

            string xBodyDocXml = String.GetMinimalOpeningXML(true) + "<w:body>" + xBodyXML + "</w:body>" + String.GetMinimalClosingXML(true);
            XmlDocument oXDoc = new XmlDocument();
            oXDoc.LoadXml(xBodyDocXml);
            XmlNamespaceManager oNSM = new XmlNamespaceManager(oXDoc.NameTable);
            oNSM.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            XmlNode oBodyNode = oXDoc.SelectSingleNode("descendant::w:body", oNSM);

            //get reinsertion location
            string xObjectData = Query.GetAttributeValue(oDoc, omDel, "ObjectData");
            string[] oProps = xObjectData.Split('|');
            ReinsertionLocations iReinsertLoc = (ReinsertionLocations)Enum.Parse(typeof(ReinsertionLocations), oProps[1]);

            OpenXmlElement oParentVar = Query.GetNearestAncestorContentControl(omDel, "mVar");
            OpenXmlElement oParentBlock = Query.GetNearestAncestorContentControl(omDel, "mBlock");
            Table oTable = omDel.Ancestors<Table>().FirstOrDefault();
            OpenXmlElement oParent = null;
            if (omDel is SdtRun)
            {
                if (oParentVar != null && oParentVar is SdtBlock)
                    oParent = oParentVar;
                else
                    oParent = omDel.Ancestors<Paragraph>().FirstOrDefault();
            }
            else if (omDel is SdtBlock)
            {
                oParent = omDel;
            }
            if (oParentBlock != null && oParentBlock.Descendants<Paragraph>().Contains(oParent))
            {
                if (iReinsertLoc == ReinsertionLocations.Before && oParent is Paragraph && oParent.PreviousSibling() != null && !(oParent.PreviousSibling() is Paragraph))
                {
                    oParent = oParentBlock;
                }
                else if (iReinsertLoc == ReinsertionLocations.After && oParent is Paragraph && oParent.NextSibling() != null && !(oParent.NextSibling() is Paragraph))
                {
                    oParent = oParentBlock;
                }

            }
            if (oParent != null)
            {
                //GLOG 15761: Top-level element may be w:sdt or w:p.
                //Skip over any additional elements in the Delete Scope XML (such as bookmarks) prior to one of those types.
                XmlNode oFirstNode = oBodyNode.FirstChild;
                while (oFirstNode != null && oFirstNode.Name != "w:sdt" && oFirstNode.Name != "w:p")
                {
                    oFirstNode = oFirstNode.NextSibling;
                }
                //GLOG 8631: Need to handle differently depending on whether Block has a parent paragraph
                if (iReinsertLoc == ReinsertionLocations.Before)
                {
                    if (oFirstNode.Name == "w:sdt")
                    {
                        oParent = oParent.InsertBeforeSelf<SdtBlock>(new SdtBlock(oFirstNode.OuterXml));
                        omBlock = (SdtElement)oParent;
                    }
                    else
                    {

                        oParent = oParent.InsertBeforeSelf<Paragraph>(new Paragraph(oFirstNode.OuterXml));
                        omBlock = oParent.Descendants<SdtElement>().Where(cc => Query.GetAttributeValue(oDoc, cc, "Name") == oBlock.Name).FirstOrDefault();
                    }
                }
                else if (iReinsertLoc == ReinsertionLocations.After)
                {
                    if (oFirstNode.Name == "w:sdt")
                    {
                        oParent = oParent.InsertAfterSelf<SdtBlock>(new SdtBlock(oFirstNode.OuterXml));
                        omBlock = (SdtElement)oParent;
                    }
                    else
                    {
                        oParent = oParent.InsertAfterSelf<Paragraph>(new Paragraph(oFirstNode.OuterXml));
                        omBlock = oParent.Descendants<SdtElement>().Where(cc => Query.GetAttributeValue(oDoc, cc, "Name") == oBlock.Name).FirstOrDefault();
                    }
                }
            }
            if (omBlock != null)
            {
                //Assign mVar Content Control new Tag
                Query.ReTagContentControl (omBlock, oDoc);
                foreach (SdtElement oCC in omBlock.Descendants<SdtElement>().Where(cc => Query.GetTag(cc) != "").ToArray<SdtElement>())
                {
                    //Retag other content controls in scope
                    Query.ReTagContentControl(oCC, oDoc);
                }
                //Delete mDel and Doc Variable
                Query.DeleteAssociatedDocVars(oDoc, omDel);
                //GLOG 8490:  If mDel contains a Paragraph, that needs to remain as a child of the Parent node after deletion
                Paragraph omDelPara = omDel.Descendants<Paragraph>().FirstOrDefault();
                if (omDelPara != null)
                {
                    omDel.InsertBeforeSelf(omDelPara.CloneNode(true));
                }
                omDel.Remove();
                Query.RestoreEmbeddedParts(xEmbeddedXML, xFootnoteRefXML, xEndnoteRefXML, xCommentsRefXml, oPart, oDoc, oSegment.ForteDocument); //GLOG 15921
            }
            return omBlock;
        }
        /// <summary>
        ///  inserts mDels to replace target content control and all existing
        ///  mDels in the scope, preserving the sequence in the document
        /// </summary>
        /// <returns></returns>
        private static ReinsertionLocations InsertmDels(XmlSegment oSegment, SdtElement omSEG,
            OpenXmlElement oTargetScope, SdtElement oTargetCC, string xTargetName, string xObjectDBID,
            DeleteScopeTypes iScope)
        {
            //if the insertion location contains blockable tags, mDel is inserted inside
            //innermost tag to preserve blockability -
            //mDel object data structure: "tag id of parent segment|reinsertion location|native
            //to table|tag id of parent block" -
            //the last two properties indicate whether the scope for an mDel at the start of a table
            //or block should be reinserted inside the table/block or above it
            WordprocessingDocument oDoc = oSegment.WPDoc;

            //TEST CODE
            //List<Paragraph> oParas = omSEG.Descendants<Paragraph>().ToList();
            //Paragraph oPara = oTargetCC.Descendants<Paragraph>().First();

            //get reinsertion location - mDel is generally inserted in the subsequent paragraph
            ReinsertionLocations iReinsertionLocation = ReinsertionLocations.Before;
            bool bParaScopeInCell = (iScope == DeleteScopeTypes.Paragraph) &&
                (oTargetScope.Ancestors<Table>().Count() > 0);
            Paragraph oLastPara = null;
            if (oTargetScope is Paragraph)
                oLastPara = (Paragraph)oTargetScope;
            else
                oLastPara = oTargetScope.Descendants<Paragraph>().LastOrDefault();
            if ((oLastPara == omSEG.Descendants<Paragraph>().Last()) ||
                ((iScope == DeleteScopeTypes.Cell) || (iScope == DeleteScopeTypes.CellAndPreviousCell)))
                //insert before if scope is the last paragraph of the segment or is one of the cell types
                iReinsertionLocation = ReinsertionLocations.After;
            else if (bParaScopeInCell)
            {
                //insert before if scope is the last paragraph of the cell
                TableCell oCell = oTargetScope.Ancestors<TableCell>().First();
                //GLOG 8504
                if (oTargetScope.Descendants<Paragraph>().LastOrDefault() == oCell.Descendants<Paragraph>().LastOrDefault())
                    iReinsertionLocation = ReinsertionLocations.After;
            }

            //get target paragraph
            //TODO: is the all of the validation we do in COM still necessary? We shouldn't be in this method
            //at all if segment only has one paragraph, reinsertion location set above should reflect availabilty
            //of an existing previous or next pargagraph, and we don't need to worry about staying inside a bookmark -
            //so we shouldn't ever need to add a new paragraph
            OpenXmlElement oNode = null;
            Paragraph omDelTargetPara = null;
            if (iReinsertionLocation == ReinsertionLocations.Before)
            {
                oNode = oTargetScope.NextSibling();
                if (oNode is Paragraph)
                    omDelTargetPara = (Paragraph)oNode;
                else
                {
                    oNode = oTargetScope.NextSibling().Where(x => x.Descendants<Paragraph>().Count() > 0).First();
                    omDelTargetPara = oNode.Descendants<Paragraph>().First();
                }
            }
            else
            {
                oNode = oTargetScope.PreviousSibling();
                if (oNode is Paragraph)
                    omDelTargetPara = (Paragraph)oNode;
                else if (oNode is TableCell)
                {
                    if (iScope == DeleteScopeTypes.CellAndPreviousCell)
                    {
                        oNode = oNode.PreviousSibling<TableCell>();
                    }
                    omDelTargetPara = oNode.Descendants<Paragraph>().Last();
                }
                else
                {
                    oNode = oTargetScope.PreviousSibling().Where(x => x.Descendants<Paragraph>().Count() > 0).Last();
                    omDelTargetPara = oNode.Descendants<Paragraph>().Last();
                }
            }

            //create tag for new mDel
            string xTag = "mpd" + Query.GenerateDocVarID(oDoc);

            //append object database id
            xObjectDBID = new string('0', 6 - xObjectDBID.Length) + xObjectDBID;
            xTag += xObjectDBID + new string('0', 22);

            //get attributes of new mDel
            bool bNativeToTable = (iScope > DeleteScopeTypes.Paragraph) || bParaScopeInCell;
            string xParentBlock = "";
            SdtElement oBlock = Query.GetNearestAncestorContentControl(oTargetCC, "mBlock");
            if (oBlock != null)
                xParentBlock = Query.GetAttributeValue(oDoc, oBlock, "TagID");
            string xObjectData = oSegment.TagID + '|' + ((System.Int32)iReinsertionLocation).ToString() +
                '|' + Convert.ToInt32(bNativeToTable).ToString() + '|' + xParentBlock;

            //create doc var and set attributes
            Query.SetAttributeValue(oDoc, xTag, "TagID", xTargetName, false, "");
            Query.SetAttributeValue(oDoc, xTag, "ObjectData", xObjectData, false, "");

            List<string> omDelsBefore = new List<string>();
            List<string> omDelsAfter = new List<string>();

            //get tags of existing mDels in scope, inserting tag of new mDel
            //at correct location in the string -
            //TODO: do we need to account for target cc's location relevant
            //to existing mDels?  The only scenario I can envision where this
            //might come into play would be where there are paragraphs scope mDels
            //inside a larger target scope, and these would be outside the scope
            //when reinserted in any case
            bool bNativeToBlock = false;
            string xExistingTag = "";
            //GLOG 15766:  Need to save Descendants to an array, otherwise loop
            //will exit as soon as first matching CC is deleted
            SdtElement[] oCCs = oTargetScope.Descendants<SdtElement>().ToArray();
            foreach (SdtElement oCC in oCCs)
            {
                xExistingTag = Query.GetTag(oCC);
                if (Query.GetBaseName(xExistingTag) == "mDel")
                {
                    //determine whether mDel is native to block that's getting deleted
                    xObjectData = Query.GetAttributeValue(oDoc, xExistingTag, "ObjectData");
                    string[] oProps = xObjectData.Split('|');
                    if (oProps.Length > 2)
                        bNativeToBlock = (oProps[(System.Int32)(DeletedBlockProperties.ParentBlock)] == xTargetName);
                    if (!bNativeToBlock)
                    {
                        if ((ReinsertionLocations)System.Int32.Parse(oProps[
                            (System.Int32)DeletedBlockProperties.ReinsertionLocation]) == ReinsertionLocations.Before)
                            //insert before the mDel representing the target
                            omDelsBefore.Add(xExistingTag);
                        else
                            //insert after the mDel representing the target
                            omDelsAfter.Add(xExistingTag);

                        //delete existing mDel
                        oCC.Remove();
                    }
                }
            }

            //add new mDel to list
            omDelsBefore.Add(xTag);

            //add 'after' items to list
            for (int i = 0; i < omDelsAfter.Count(); i++)
                omDelsBefore.Add(omDelsAfter[i]);    

            //TODO: if target paragraph is inside a block mDel, convert the mDel to inline
            SdtElement oParentCC = omDelTargetPara.Ancestors<SdtElement>().First();
            //GLOG 8484: Avoid error on non-Forte Content Controls
            if (Query.GetTag(oParentCC).StartsWith("mpd"))
            {
                xExistingTag = Query.GetTag(oParentCC);
            }

            //insert mDels
            for (int i = 0; i < omDelsBefore.Count(); i++)
            {
                int iOffset = i;
                //JTS 1/7/16:  If Para has a ParagraphProperties child, insert mDels after that
                if (omDelTargetPara.Descendants<ParagraphProperties>().Count() > 0)
                    iOffset++;

                //create
                string xItem = omDelsBefore[i];
                SdtRun omDel = CreatemDel(xItem);


                //insert
                if (iReinsertionLocation == ReinsertionLocations.Before)
                    omDelTargetPara.InsertAt(omDel, iOffset);
                else
                    omDelTargetPara.AppendChild(omDel);
                //update reinsertion location - this is the
                //only attribute that may have changed
                xObjectData = Query.GetAttributeValue(oDoc, xItem, "ObjectData");
                string[] oProps = xObjectData.Split('|');
                oProps[(System.Int32)DeletedBlockProperties.ReinsertionLocation] = ((System.Int32)iReinsertionLocation).ToString();
                xObjectData = string.Join("|", oProps);
                Query.SetAttributeValue(oDoc, xItem, "ObjectData", xObjectData, false, "");
            }

            return iReinsertionLocation;
        }

        /// <summary>
        /// creates a new mDel
        /// </summary>
        /// <param name="xTag"></param>
        /// <returns></returns>
        private static SdtRun CreatemDel(string xTag)
        {
            SdtRun omDel = new SdtRun();
            SdtProperties oProps = new SdtProperties();
            Tag oTag = new Tag() { Val = xTag };
            oProps.AppendChild(oTag);
            oProps.AppendChild(new ShowingPlaceholder());
            omDel.AppendChild(oProps);
            omDel.AppendChild(new SdtContentRun());
            return omDel;
        }

        /// <summary>
        /// deletes scope xml for specified variable
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xDeletedScopes"></param>
        /// <param name="xVariableName"></param>
        private static void DeleteDefinition(WordprocessingDocument oDoc, ref string xDeletedScopes,
            string xVariableName)
        {
            int iPos = xDeletedScopes.IndexOf("DeletedTag=" + xVariableName + "¦");
            if (iPos > -1)
            {
                //get scope
                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>|", iPos) + 10;
                string xScope = xDeletedScopes.Substring(iPos, iPos2 - iPos);

                //delete scope
                xDeletedScopes = xDeletedScopes.Substring(0, iPos) + xDeletedScopes.Substring(iPos2);

                //delete associated doc vars
                iPos = xScope.IndexOf("¬w:tag w:val=");
                while (iPos > -1)
                {
                    iPos += 14;
                    iPos2 = xScope.IndexOf("\"", iPos);
                    string xTag = xScope.Substring(iPos, iPos2 - iPos);
                    Query.DeleteAssociatedDocVars(oDoc, xTag);
                    iPos = xScope.IndexOf("¬w:tag w:val=", iPos);
                }
            }
        }
        public static string GetEmbeddedContentXML(OpenXmlElement oTarget, XmlSegment oSegment)
        {
            string xValue = "";
            string xRel = "";
            string xEmbed = "";
            WordprocessingDocument oDoc = oSegment.WPDoc;
            OpenXmlPart oPart = null;
            //Locate all r:blip nodes marking embedded images
            var oBlips = oTarget.Descendants<A.Blip>();
            foreach (A.Blip oBlip in oBlips)
            {
                xRel = "";
                xEmbed = "";
                if (oPart == null)
                    oPart = Query.GetContainingOpenXmlPart(oTarget, oDoc);

                string xRelID = oBlip.Embed.Value;
                IdPartPair ipp = oPart.Parts.FirstOrDefault(p => p.RelationshipId == xRelID);
                if (ipp != null)
                {
                    string xURI = ipp.OpenXmlPart.Uri.OriginalString;
                    string xType = ipp.OpenXmlPart.RelationshipType;
                    string xContentType = ipp.OpenXmlPart.ContentType;
                    string xImage = "";
                    using (Stream s = ipp.OpenXmlPart.GetStream(FileMode.Open, FileAccess.Read))
                    {
                        byte[] aImage = new byte[s.Length];
                        s.Read(aImage, 0, (int)s.Length);
                        xImage = Convert.ToBase64String(aImage);
                    }
                    //Build XML definitions based on Part properties
                    xRel = string.Format("<Relationship Id=\"{0}\" Type=\"{1}\" Target=\"{2}\"/>", xRelID, xType, xURI.Replace("/word/", ""));
                    xEmbed = string.Format("<pkg:part pkg:name=\"{0}\" pkg:contentType=\"{1}\" pkg:compression=\"store\"><pkg:binaryData>{2}" +
                        "</pkg:binaryData></pkg:part>", xURI, xContentType, xImage);
                    xValue += xRel + xEmbed;
                }

            }
            var oFNRefs = oTarget.Descendants<FootnoteReference>();
            xRel = "";
            xEmbed = "";
            string xPkgXML = "";
            string xNotesXML = "";
            OpenXmlPart oNotesPart = null;
            foreach (FootnoteReference oNoteRef in oFNRefs)
            {
                if (oPart == null)
                    oPart = Query.GetContainingOpenXmlPart(oTarget, oDoc);
                long lNoteID = oNoteRef.Id.Value;
                if (oNotesPart == null)
                {
                    oNotesPart = oDoc.MainDocumentPart.GetPartsOfType<FootnotesPart>().FirstOrDefault();
                    string xURI = oNotesPart.Uri.OriginalString;
                    string xType = oNotesPart.RelationshipType;
                    string xContentType = oNotesPart.ContentType;
                    string xRelID = oDoc.MainDocumentPart.GetIdOfPart(oNotesPart);

                    //Build XML definitions based on Part properties
                    xRel = string.Format("<Relationship Id=\"{0}\" Type=\"{1}\" Target=\"{2}\" />", xRelID, xType, xURI.Replace("/word/", ""));
                    xPkgXML = string.Format("<pkg:part pkg:name=\"{0}\" pkg:contentType=\"{1}\"><pkg:xmlData>{2}" +
                        "</pkg:xmlData></pkg:part>", xURI, xContentType, oNotesPart.RootElement.OuterXml);
                }
                //Find w:footnote node for each footnote in Target
                Footnote oNote = oNotesPart.RootElement.Descendants<Footnote>().FirstOrDefault(n => n.Id.Value == lNoteID);
                if (oNote != null)
                {
                    xNotesXML = xNotesXML + oNote.OuterXml;
                }
            }
            if (xPkgXML != "" && xNotesXML != "")
            {
                //Package XML will contain all notes, not just those in target range.
                //Replace footnote nodes with XML that includes only relevant footnotes.
                int iStart = xPkgXML.IndexOf("<w:footnote w:id=");
                if (iStart != -1)
                {
                    int iEnd = xPkgXML.LastIndexOf("</w:footnotes>");
                    if (iEnd != -1)
                    {
                        xEmbed = xPkgXML.Substring(0, iStart) + xNotesXML + xPkgXML.Substring(iEnd);
                    }
                }
                xValue += xRel + xEmbed;
            }
            var oENRefs = oTarget.Descendants<EndnoteReference>();
            xRel = "";
            xEmbed = "";
            xPkgXML = "";
            xNotesXML = "";
            oNotesPart = null;
            foreach (EndnoteReference oNoteRef in oENRefs)
            {
                if (oPart == null)
                    oPart = Query.GetContainingOpenXmlPart(oTarget, oDoc);
                long lNoteID = oNoteRef.Id.Value;
                if (oNotesPart == null)
                {
                    oNotesPart = oDoc.MainDocumentPart.GetPartsOfType<EndnotesPart>().FirstOrDefault();
                    string xURI = oNotesPart.Uri.OriginalString;
                    string xType = oNotesPart.RelationshipType;
                    string xContentType = oNotesPart.ContentType;
                    string xRelID = oDoc.MainDocumentPart.GetIdOfPart(oNotesPart);
                    xRel = string.Format("<Relationship Id=\"{0}\" Type=\"{1}\" Target=\"{2}\" />", xRelID, xType, xURI.Replace("/word/", ""));

                    xPkgXML = string.Format("<pkg:part pkg:name=\"{0}\" pkg:contentType=\"{1}\"><pkg:xmlData>{2}" +
                        "</pkg:xmlData></pkg:part>", xURI, xContentType, oNotesPart.RootElement.OuterXml);
                }
                Endnote oNote = oNotesPart.RootElement.Descendants<Endnote>().FirstOrDefault(n => n.Id.Value == lNoteID);
                if (oNote != null)
                {
                    xNotesXML = xNotesXML + oNote.OuterXml;
                }
            }
            if (xPkgXML != "" && xNotesXML != "")
            {
                //Package XML will contain all notes, not just those in target range.
                //Replace endnote nodes with XML that includes only relevant endnotes.
                int iStart = xPkgXML.IndexOf("<w:endnote w:id=");
                if (iStart != -1)
                {
                    int iEnd = xPkgXML.LastIndexOf("</w:endnotes>");
                    if (iEnd != -1)
                    {
                        xEmbed = xPkgXML.Substring(0, iStart) + xNotesXML + xPkgXML.Substring(iEnd);
                    }
                }
                xValue += xRel + xEmbed;
            }
            //GLOG 15921
            var oCommentRefs = oTarget.Descendants<CommentReference>();
            xRel = "";
            xEmbed = "";
            xPkgXML = "";
            xNotesXML = "";
            oNotesPart = null;
            foreach (CommentReference oNoteRef in oCommentRefs)
            {
                if (oPart == null)
                    oPart = Query.GetContainingOpenXmlPart(oTarget, oDoc);
                long lNoteID = long.Parse(oNoteRef.Id.Value);
                if (oNotesPart == null)
                {
                    oNotesPart = oDoc.MainDocumentPart.GetPartsOfType<WordprocessingCommentsPart>().FirstOrDefault();
                    string xURI = oNotesPart.Uri.OriginalString;
                    string xType = oNotesPart.RelationshipType;
                    string xContentType = oNotesPart.ContentType;
                    string xRelID = oDoc.MainDocumentPart.GetIdOfPart(oNotesPart);
                    xRel = string.Format("<Relationship Id=\"{0}\" Type=\"{1}\" Target=\"{2}\" />", xRelID, xType, xURI.Replace("/word/", ""));

                    xPkgXML = string.Format("<pkg:part pkg:name=\"{0}\" pkg:contentType=\"{1}\"><pkg:xmlData>{2}" +
                        "</pkg:xmlData></pkg:part>", xURI, xContentType, oNotesPart.RootElement.OuterXml);
                }
                Comment oNote = oNotesPart.RootElement.Descendants<Comment>().FirstOrDefault(n => n.Id.Value == lNoteID.ToString());
                if (oNote != null)
                {
                    xNotesXML = xNotesXML + oNote.OuterXml;
                }
            }
            if (xPkgXML != "" && xNotesXML != "")
            {
                //Package XML will contain all notes, not just those in target range.
                //Replace endnote nodes with XML that includes only relevant endnotes.
                int iStart = xPkgXML.IndexOf("<w:endnote w:id=");
                if (iStart != -1)
                {
                    int iEnd = xPkgXML.LastIndexOf("</w:endnotes>");
                    if (iEnd != -1)
                    {
                        xEmbed = xPkgXML.Substring(0, iStart) + xNotesXML + xPkgXML.Substring(iEnd);
                    }
                }
                xValue += xRel + xEmbed;
            }
            return xValue;
        }
    }
}
