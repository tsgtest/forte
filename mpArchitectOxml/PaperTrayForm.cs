﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.Architect.Oxml
{
    public partial class PaperTrayForm : Form
    {
        private object[] m_oTrays;
        private short[] m_shIDs;
        public PaperTrayForm(string xSegmentTypeName)
        {
            InitializeComponent();
            //GLOG 4752: Use Base function to retrieve bin information
            LMP.fCOMObjects.GetPrinterBinNamesAndIDs(ref m_oTrays, ref m_shIDs);
            //setup the paper tray combo
            foreach (object oTray in m_oTrays)
            {
                this.lstPaperTray.Items.Add(oTray.ToString());
            }

            if (m_oTrays.GetUpperBound(0) >= 0)
            {
                this.lstPaperTray.SelectedIndex = 0;
            }

            this.lblPaperTray.Text = "Select Paper Tray for " + xSegmentTypeName + ":";
        }

        /// <summary>
        /// returns the selected paper tray
        /// </summary>
        public int PaperTray
        {
            get
            {
                return (int)m_shIDs[this.lstPaperTray.SelectedIndex];
            }
        }
    }
}
