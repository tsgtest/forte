﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMP.Data; //GLOG 6983

namespace LMP.Architect.Oxml
{
    public class XmlTOA : XmlAdminSegment, LMP.Architect.Base.ILitigationAddOnSegment, ISingleInstanceSegment
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation,
        //            iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
        //            iIntendedUse, mpObjectTypes.TOA); //GLOG 6983

        //        //GLOG 4780: We can't assume that first Tag in section is TOA mSEG -
        //        //it could be the opening mSEG of a Parent Segment.  Look for mBlock tag
        //        //that is child of innermost mSEG

        //        Word.Range oTOALocation = null;

        //        int iFirstTOASection = oLocation.Sections.First.Index; //GLOG 7553 (dm)

        //        //1-27-11 (dm) - condition on save format, not xml format
        //        //if (LMP.String.IsWordOpenXML(xXML))
        //        if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
        //        {
        //            object oIndex = 1;
        //            //GLOG 4881: 10.2
        //            //find location for TOA field codes -
        //            //first check for block
        //            Word.ContentControl oCC = oLocation.Sections.First.Range.ContentControls.get_Item(ref oIndex);
        //            while (LMP.String.GetBoundingObjectBaseName(oCC.Tag) == "mSEG" && oCC.Range.ContentControls.Count > 0)
        //                oCC = oCC.Range.ContentControls.get_Item(ref oIndex);

        //            if (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mBlock")
        //            {
        //                Word.ContentControl oParentCC = oCC.ParentContentControl;
        //                Word.ContentControl oBlockCC = null;
        //                oCC = null;
        //                for (int i = 1; i <= oParentCC.Range.ContentControls.Count; i++)
        //                {
        //                    object oBIndex = i;
        //                    oBlockCC = oParentCC.Range.ContentControls.get_Item(ref oBIndex);
        //                    if (LMP.String.GetBoundingObjectBaseName(oBlockCC.Tag) == "mBlock")
        //                    {
        //                        oCC = oBlockCC;
        //                        break;
        //                    }
        //                }
        //            }
        //            //GLOG 4780: Make sure we didn't get mBlock located somewhere else in document
        //            if (oCC == null || oCC.Range.Sections.First.Index != iFirstTOASection)
        //                //insert at start of segment
        //                oCC = oLocation.Sections.First.Range.ContentControls.get_Item(ref oIndex);
        //            oTOALocation = oCC.Range;
        //        }
        //        else
        //        {
        //            //find location for TOA field codes -
        //            //first check for block
        //            Word.XMLNode oNode = oLocation.Sections.First.Range.XMLNodes[1];
        //            while (oNode.BaseName == "mSEG" && oNode.HasChildNodes)
        //                oNode = oNode.ChildNodes[1];

        //            if (oNode.BaseName != "mBlock")
        //                oNode = oNode.ParentNode.SelectSingleNode(
        //                "x:mBlock", "xmlns:x='urn-legalmacpac-data/10'", true);

        //            //GLOG 4780: Make sure we didn't get mBlock located somewhere else in document
        //            if (oNode == null || oNode.Range.Sections.First.Index != iFirstTOASection)
        //                //insert at start of segment
        //                oNode = oLocation.Sections.First.Range.XMLNodes[1];
        //            oTOALocation = oNode.Range;
        //        }
        //        int iLastTOASection = oLocation.Sections.Last.Index;

        //        if (iLastTOASection < oLocation.Document.Sections.Count)
        //        {
        //            //set next section to start page numbering at one
        //            Word.PageNumbers oPgNum = oLocation.Document.Sections[iLastTOASection + 1]
        //                .Footers[Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers;
        //            oPgNum.RestartNumberingAtSection = true;
        //            oPgNum.StartingNumber = 1;

        //            oPgNum = oLocation.Document.Sections[iLastTOASection + 1]
        //                .Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers;
        //            oPgNum.RestartNumberingAtSection = true;
        //            oPgNum.StartingNumber = 1;
        //        }

        //        //GLOG 7553 (dm) - continue page numbering from TOC
        //        Word.Bookmark oTOC = null;
        //        object oBmkName = "mpTableOfContents";
        //        try
        //        {
        //            oTOC = oLocation.Document.Bookmarks.get_Item(ref oBmkName);
        //        }
        //        catch { }
        //        if ((oTOC != null) && (oTOC.Range.Sections.Last.Index == iFirstTOASection - 1))
        //        {
        //            Word.PageNumbers oPgNumTOA = oLocation.Document.Sections[iFirstTOASection]
        //                .Footers[Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers;
        //            oPgNumTOA.RestartNumberingAtSection = false;
        //            oPgNumTOA = oLocation.Document.Sections[iFirstTOASection]
        //                .Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers;
        //            oPgNumTOA.RestartNumberingAtSection = false;
        //        }

        //        InsertTOA(oTOALocation);
        //        //DeleteExistingInstance(null);
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
        //private void InsertTOA(Word.Range oLocation)
        //{
        //    //insert new toa
        //    LMP.Forte.MSWord.Pleading oPleading = new LMP.Forte.MSWord.Pleading();
        //        oPleading.InsertTOAFieldCodes(oLocation,true);
        //}
        //public XmlSegments GetExistingSegments(Word.Section oSection)
        //{
        //    //get existing segment of this type
        //    XmlSegments oSegs = null;

        //    ////GLOG - 2476 - ceh
        //    try
        //    {
        //        //get from location
        //        oSegs = XmlSegment.GetSegments(this.ForteDocument, this.Definition.TypeID, 1);
        //    }
        //    catch { }
        //    return oSegs;
        //}

        //public void DeleteExistingInstance(Word.Section oSection)
        //{
        //    //delete existing TOA
        //    XmlSegment oExistingTOA = null;
        //    if (oExistingTOA != null)
        //    {
        //        try
        //        {
        //            this.ForteDocument.DeleteSegment(oExistingTOA);
        //        }
        //        catch (System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.SegmentException(
        //                LMP.Resources.GetLangString("Error_CouldNotRemoveSegment"), oE);
        //        }
        //    }
        //}
        #region ISingleInstanceSegment Members

        public XmlSegments GetExistingSegments(int iSection)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
