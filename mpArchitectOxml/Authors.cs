using System;
using System.Collections;
using System.Collections.Specialized;
using LMP.Data;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace LMP.Architect.Oxml
{
	/// <summary>
	/// contains the methods and properties that define 
	/// a collection of MacPac Authors -
	/// created by Daniel Fisherman, 10/04
	/// </summary>
	public class XmlAuthors : LMP.Architect.Base.Authors
	{
		private XmlSegment m_oSegment = null;

		//we define both a hashtable and an ArrayList because
		//none of the .NET collections allow us to call by
		//key and index, as well as allow us to move authors
		//by index - the best collection is the NameObjectCollectionBase, 
		//but there was no way to move authors around using this class
        private XmlControlActions m_oControlActions;
        public event LMP.Architect.Base.AuthorAddedHandler AuthorAdded = null;
        public event LMP.Architect.Base.LeadAuthorChangedHandler LeadAuthorChanged = null;
        public event LMP.Architect.Base.AuthorsReindexedHandler AuthorsReindexed = null;
		#region *********************constructors*********************
		public XmlAuthors(XmlSegment oSegment)
		{
			m_oSegment = oSegment;
            m_oControlActions = new XmlControlActions(this);
		}
		#endregion
		#region *********************properties*********************
		/// <summary>
		/// sets/gets the parent MPObject for this authors collection
		/// </summary>
		public XmlSegment Segment
		{
			get{return m_oSegment;}
			set{m_oSegment = value;}
		}

        public XmlControlActions ControlActions
        {
            get { return m_oControlActions; }
        }
		#endregion
		#region *********************methods*********************
        protected override void RaiseAuthorAddedEventIfNecessary()
        {
            //raise event
            if (AuthorAdded != null)
                AuthorAdded(this, new LMP.Architect.Base.AuthorAddedEventArgs((short)this.Count));
        }
        protected override void RaiseAuthorsReindexedEvent()
        {
            AuthorsReindexed(m_oSegment, new LMP.Architect.Base.AuthorsReindexedEventArgs());
        }
        protected override void RaiseAuthorsReindexedEventIfNecessary()
        {
            if (AuthorsReindexed != null)
                AuthorsReindexed(m_oSegment, new LMP.Architect.Base.AuthorsReindexedEventArgs());
        }
        protected override void RaiseLeadAuthorChangedEventIfNecessary(Author oNewLeadAuthor)
        {
            if (LeadAuthorChanged != null)
                LeadAuthorChanged(m_oSegment,
                    new LMP.Architect.Base.LeadAuthorChangedEventArgs(oNewLeadAuthor.ID));
        }
        //GLOG item #8027 - switched first param from Person to string - dcf
        protected override Person RaisePromptToCopyPublicPersonIfNecessary(string xPersonID1, LocalPersons oLocalPersons, bool bIsLegacy)
        {
            return null;
        }
		#endregion
	}
}
