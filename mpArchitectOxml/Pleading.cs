using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using LMP.Data;

namespace LMP.Architect.Oxml
{
    public class XmlPleading : XmlAdminSegment
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation,
        //            iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
        //            iIntendedUse, mpObjectTypes.Pleading); //GLOG 6983
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
    }

    //GLOG 7154
    public class XmlLitigationBack : XmlAdminSegment
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation,
        //            iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, true,
        //            iIntendedUse, mpObjectTypes.LitigationBack);
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
    }
    
    public class XmlPleadingSignatures : XmlCollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingSignature; }
        }
    }

    public class XmlPleadingCounsels : XmlCollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingCounsel; }
        }
    }

    public class XmlPleadingCaptions : XmlCollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingCaption; }
        }
    }

    public class XmlPleadingSignature : XmlCollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.PleadingSignatures; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingSignature; }
        }
    }

    public class XmlPleadingCounsel : XmlCollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.PleadingCounsels; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingCounsel; }
        }
    }

    public class XmlPleadingCaption : XmlCollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.PleadingCaptions; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.PleadingCaption; }
        }

    }

    public class XmlPleadingPaper : XmlPaper
    {
        public enum mpPleadingPaperAlignmentScopes
        {
            Paragraph = 0,
            Selection = 1,
            ToSignature = 2,
            PleadingBody = 3
        }

        /// <summary>
        /// aligns text with pleading paper
        /// </summary>
        //public static void AlignText(Word.Range oRng)
        //{
        //    try
        //    {
        //        //GLOG 6967 (dm) - if necessary, revert to Word 2010 compatibility mode and
        //        //suppress extra space at top
        //        Word.Document oDoc = oRng.Document;
        //        if (LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(oDoc) > 14)
        //        {
        //            LMP.fWordObjects.cWord14.SetCompatibilityMode(oDoc, 14);
        //            oDoc.Compatibility[Word.WdCompatibility.wdSuppressTopSpacing] = true;
        //        }

        //        LMP.Forte.MSWord.PleadingPaper oPPaper = new LMP.Forte.MSWord.PleadingPaper();
        //        oPPaper.AlignText(oRng);
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.MethodExecutionException(
        //            LMP.Resources.GetLangString("Error_CouldNotAlignTextWithPleadingPaper"), oE);
        //    }
        //}

        ///// <summary>
        ///// returns the range corresponding to the body of the pleading
        ///// </summary>
        ///// <returns></returns>
        //public static Word.Range GetBodyFromSelection(XmlForteDocument oMPDoc)
        //{
        //    XmlSegment oSegment = oMPDoc.GetTopLevelSegmentFromSelection();

        //    if (oSegment != null && oSegment.TypeID == mpObjectTypes.Pleading)
        //        //we've found the pleading - get the body
        //        return oSegment.GetBody();
        //    else
        //        return null;
        //}

        ///// <summary>
        ///// if there's existing pleading paper in oForteDoc, inserts a new
        ///// instance of the same pleading paper in the current section
        ///// </summary>
        ///// <param name="oForteDoc"></param>
        ///// <returns></returns>
        //public static XmlSegment InsertExistingInCurrentSection(XmlForteDocument oForteDoc, XmlSegment oParent)
        //{
        //    XmlSegment oNew = null;

        //    //GLOG 7217 (dm) - get target section
        //    int iSection = Application.CurrentWordApp.Selection.Sections[1].Index;

        //    //if TOC has inserted a new section at the start of the document, the
        //    //header/footer tags in the node store will no longer be valid
        //    //GLOG 4780: Also the case if TOA is being inserted an selection
        //    if (iSection < oForteDoc.WordDocument.Sections.Count)
        //    {
        //        oForteDoc.Refresh(XmlForteDocument.RefreshOptions
        //            .RefreshTagsWithGUIDIndexes, false);

        //        //GLOG 7256 (dm) - reset oParent to a current object
        //        if (oParent != null)
        //            oParent = oForteDoc.FindSegment(oParent.FullTagID);
        //    }

        //    //look for existing pleading paper
        //    XmlSegment oExisting = XmlSegment.FindSegment(LMP.Data.mpObjectTypes.PleadingPaper,
        //        oForteDoc.Segments);
        //    if (oExisting != null)
        //    {
        //        //insert using existing values
        //        XmlPrefill oPrefill = new XmlPrefill(oExisting);
        //        oNew = XmlSegment.Insert(oExisting.ID, oParent, InsertionLocations.InsertAtSelection,
        //            InsertionBehaviors.Default, oForteDoc, oPrefill, true, "");

        //        //GLOG 3026 (dm) - ensure that child segments match
        //        int iExistingCount = oExisting.Segments.Count;
        //        int iNewCount = oNew.Segments.Count;

        //        //update if count doesn't match
        //        bool bUpdateRequired = (iExistingCount != iNewCount);

        //        //if count matches, compare ids
        //        if (!bUpdateRequired)
        //        {
        //            for (int i = 0; i < iExistingCount; i++)
        //            {
        //                if (oExisting.Segments[i].ID != oNew.Segments[i].ID)
        //                {
        //                    bUpdateRequired = true;
        //                    break;
        //                }
        //            }
        //        }

        //        if (bUpdateRequired)
        //        {
        //            //delete design children
        //            XmlSegments oSegs = oNew.Segments;
        //            for (int i = 0; i < oSegs.Count; i++)
        //                oSegs.ForteDocument.DeleteSegment(oSegs[i]);

        //            //GLOG 7217 (dm) - select start of target section - the entire document
        //            //was selected after deleting a None sidebar, resulting in the entire
        //            //doc getting wiped out below at the start on InsertTargetedSegmentXML()
        //            Word.Range oRange = oSegs.ForteDocument.WordDocument.Sections[iSection].Range;
        //            object oStart = Word.WdCollapseDirection.wdCollapseStart;
        //            oRange.Collapse(ref oStart);
        //            oRange.Select();

        //            //insert existing children
        //            oSegs = oExisting.Segments;
        //            for (int i = 0; i < oSegs.Count; i++)
        //            {
        //                XmlPrefill oChildPrefill = new XmlPrefill(oSegs[i]);
        //                oNew = XmlSegment.Insert(oSegs[i].ID, oNew, InsertionLocations.InsertAtSelection,
        //                    InsertionBehaviors.Default, oForteDoc, oChildPrefill, true, "");
        //            }
        //        }
        //    }

        //    return oNew;
        //}
    }
    public class XmlPleadingSignatureNonTable : XmlAdminSegment
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation,
        //            iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, false,
        //            iIntendedUse, mpObjectTypes.PleadingSignatureNonTable); //GLOG 6983
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
    }

    public class XmlPleadingCoverPage : XmlAdminSegment
    {
        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation,
        //            iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_True, bTargeted,
        //            iIntendedUse, mpObjectTypes.PleadingCoverPage); //GLOG 6983
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}
    }
}
