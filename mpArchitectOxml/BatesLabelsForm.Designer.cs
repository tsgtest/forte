﻿namespace LMP.Architect.Oxml
{
    partial class XmlBatesLabelsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblStartRow = new System.Windows.Forms.Label();
            this.lblStartCol = new System.Windows.Forms.Label();
            this.spnStartRow = new LMP.Controls.Spinner();
            this.spnStartCol = new LMP.Controls.Spinner();
            this.cmbLabelDirection = new System.Windows.Forms.ComboBox();
            this.lblLabelDirection = new System.Windows.Forms.Label();
            this.cmbPosition = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtConfidentialPhrase = new LMP.Controls.TextBox();
            this.lblConfidentialPhrase = new System.Windows.Forms.Label();
            this.txtStartNumber = new LMP.Controls.TextBox();
            this.lblStartNumber = new System.Windows.Forms.Label();
            this.lblBatesSuffix = new System.Windows.Forms.Label();
            this.txtNumLabels = new LMP.Controls.TextBox();
            this.txtBatesSuffix = new LMP.Controls.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBatesPrefix = new System.Windows.Forms.Label();
            this.txtBatesPrefix = new LMP.Controls.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbHorizAlignment = new System.Windows.Forms.ComboBox();
            this.cmbVertAlignment = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.spnTopMargin = new LMP.Controls.Spinner();
            this.spnDigits = new LMP.Controls.Spinner();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grpCharacter = new System.Windows.Forms.GroupBox();
            this.fntFontName = new System.Windows.Forms.ComboBox();
            this.chkUnderline = new LMP.Controls.CheckBox();
            this.lblFont = new System.Windows.Forms.Label();
            this.chkBold = new LMP.Controls.CheckBox();
            this.chkAllCaps = new LMP.Controls.CheckBox();
            this.lblFontSize = new System.Windows.Forms.Label();
            this.chkSmallCaps = new LMP.Controls.CheckBox();
            this.cmbFontSize = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpCharacter.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(6, 6);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(360, 366);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblStartRow);
            this.tabPage1.Controls.Add(this.lblStartCol);
            this.tabPage1.Controls.Add(this.spnStartRow);
            this.tabPage1.Controls.Add(this.spnStartCol);
            this.tabPage1.Controls.Add(this.cmbLabelDirection);
            this.tabPage1.Controls.Add(this.lblLabelDirection);
            this.tabPage1.Controls.Add(this.cmbPosition);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtConfidentialPhrase);
            this.tabPage1.Controls.Add(this.lblConfidentialPhrase);
            this.tabPage1.Controls.Add(this.txtStartNumber);
            this.tabPage1.Controls.Add(this.lblStartNumber);
            this.tabPage1.Controls.Add(this.lblBatesSuffix);
            this.tabPage1.Controls.Add(this.txtNumLabels);
            this.tabPage1.Controls.Add(this.txtBatesSuffix);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.lblBatesPrefix);
            this.tabPage1.Controls.Add(this.txtBatesPrefix);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(352, 340);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Details";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lblStartRow
            // 
            this.lblStartRow.Location = new System.Drawing.Point(13, 273);
            this.lblStartRow.Name = "lblStartRow";
            this.lblStartRow.Size = new System.Drawing.Size(88, 14);
            this.lblStartRow.TabIndex = 14;
            this.lblStartRow.Text = "Starting &Row:";
            // 
            // lblStartCol
            // 
            this.lblStartCol.Location = new System.Drawing.Point(13, 237);
            this.lblStartCol.Name = "lblStartCol";
            this.lblStartCol.Size = new System.Drawing.Size(88, 13);
            this.lblStartCol.TabIndex = 12;
            this.lblStartCol.Text = "Starting Co&lumn:";
            // 
            // spnStartRow
            // 
            this.spnStartRow.AppendSymbol = false;
            this.spnStartRow.BackColor = System.Drawing.Color.Transparent;
            this.spnStartRow.DecimalPlaces = 0;
            this.spnStartRow.DisplayUnit = LMP.Data.mpMeasurementUnits.Inches;
            this.spnStartRow.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spnStartRow.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnStartRow.IsDirty = true;
            this.spnStartRow.Location = new System.Drawing.Point(165, 269);
            this.spnStartRow.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.spnStartRow.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnStartRow.Name = "spnStartRow";
            this.spnStartRow.Size = new System.Drawing.Size(156, 22);
            this.spnStartRow.SupportingValues = "";
            this.spnStartRow.TabIndex = 15;
            this.spnStartRow.Tag2 = null;
            this.spnStartRow.Value = "1";
            // 
            // spnStartCol
            // 
            this.spnStartCol.AppendSymbol = false;
            this.spnStartCol.BackColor = System.Drawing.Color.Transparent;
            this.spnStartCol.DecimalPlaces = 0;
            this.spnStartCol.DisplayUnit = LMP.Data.mpMeasurementUnits.Inches;
            this.spnStartCol.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spnStartCol.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnStartCol.IsDirty = true;
            this.spnStartCol.Location = new System.Drawing.Point(165, 233);
            this.spnStartCol.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.spnStartCol.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnStartCol.Name = "spnStartCol";
            this.spnStartCol.Size = new System.Drawing.Size(156, 22);
            this.spnStartCol.SupportingValues = "";
            this.spnStartCol.TabIndex = 13;
            this.spnStartCol.Tag2 = null;
            this.spnStartCol.Value = "1";
            this.spnStartCol.Load += new System.EventHandler(this.spnStartCol_Load);
            // 
            // cmbLabelDirection
            // 
            this.cmbLabelDirection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbLabelDirection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbLabelDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLabelDirection.FormattingEnabled = true;
            this.cmbLabelDirection.Items.AddRange(new object[] {
            "Across Then Down",
            "Down Then Across"});
            this.cmbLabelDirection.Location = new System.Drawing.Point(224, 347);
            this.cmbLabelDirection.Name = "cmbLabelDirection";
            this.cmbLabelDirection.Size = new System.Drawing.Size(116, 21);
            this.cmbLabelDirection.TabIndex = 31;
            this.cmbLabelDirection.Visible = false;
            // 
            // lblLabelDirection
            // 
            this.lblLabelDirection.Location = new System.Drawing.Point(119, 350);
            this.lblLabelDirection.Name = "lblLabelDirection";
            this.lblLabelDirection.Size = new System.Drawing.Size(88, 20);
            this.lblLabelDirection.TabIndex = 30;
            this.lblLabelDirection.Text = "&Fill Direction:";
            this.lblLabelDirection.Visible = false;
            // 
            // cmbPosition
            // 
            this.cmbPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPosition.Items.AddRange(new object[] {
            "Above",
            "Below"});
            this.cmbPosition.Location = new System.Drawing.Point(165, 198);
            this.cmbPosition.Name = "cmbPosition";
            this.cmbPosition.Size = new System.Drawing.Size(156, 21);
            this.cmbPosition.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Confidential Phrase &Position:";
            // 
            // txtConfidentialPhrase
            // 
            this.txtConfidentialPhrase.IsDirty = true;
            this.txtConfidentialPhrase.Location = new System.Drawing.Point(165, 164);
            this.txtConfidentialPhrase.Name = "txtConfidentialPhrase";
            this.txtConfidentialPhrase.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtConfidentialPhrase.Size = new System.Drawing.Size(156, 20);
            this.txtConfidentialPhrase.SupportingValues = "";
            this.txtConfidentialPhrase.TabIndex = 9;
            this.txtConfidentialPhrase.Tag2 = null;
            this.txtConfidentialPhrase.Value = "";
            // 
            // lblConfidentialPhrase
            // 
            this.lblConfidentialPhrase.Location = new System.Drawing.Point(13, 167);
            this.lblConfidentialPhrase.Name = "lblConfidentialPhrase";
            this.lblConfidentialPhrase.Size = new System.Drawing.Size(106, 15);
            this.lblConfidentialPhrase.TabIndex = 8;
            this.lblConfidentialPhrase.Text = "&Confidential Phrase:";
            // 
            // txtStartNumber
            // 
            this.txtStartNumber.IsDirty = true;
            this.txtStartNumber.Location = new System.Drawing.Point(165, 28);
            this.txtStartNumber.Name = "txtStartNumber";
            this.txtStartNumber.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtStartNumber.Size = new System.Drawing.Size(156, 20);
            this.txtStartNumber.SupportingValues = "";
            this.txtStartNumber.TabIndex = 1;
            this.txtStartNumber.Tag2 = null;
            this.txtStartNumber.Value = "";
            // 
            // lblStartNumber
            // 
            this.lblStartNumber.Location = new System.Drawing.Point(13, 31);
            this.lblStartNumber.Name = "lblStartNumber";
            this.lblStartNumber.Size = new System.Drawing.Size(101, 17);
            this.lblStartNumber.TabIndex = 0;
            this.lblStartNumber.Text = "&Starting Number:";
            // 
            // lblBatesSuffix
            // 
            this.lblBatesSuffix.Location = new System.Drawing.Point(13, 133);
            this.lblBatesSuffix.Name = "lblBatesSuffix";
            this.lblBatesSuffix.Size = new System.Drawing.Size(91, 13);
            this.lblBatesSuffix.TabIndex = 6;
            this.lblBatesSuffix.Text = "Bates &Suffix:";
            // 
            // txtNumLabels
            // 
            this.txtNumLabels.IsDirty = true;
            this.txtNumLabels.Location = new System.Drawing.Point(165, 62);
            this.txtNumLabels.Name = "txtNumLabels";
            this.txtNumLabels.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtNumLabels.Size = new System.Drawing.Size(156, 20);
            this.txtNumLabels.SupportingValues = "";
            this.txtNumLabels.TabIndex = 3;
            this.txtNumLabels.Tag2 = null;
            this.txtNumLabels.Value = "";
            // 
            // txtBatesSuffix
            // 
            this.txtBatesSuffix.IsDirty = true;
            this.txtBatesSuffix.Location = new System.Drawing.Point(165, 130);
            this.txtBatesSuffix.Name = "txtBatesSuffix";
            this.txtBatesSuffix.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtBatesSuffix.Size = new System.Drawing.Size(156, 20);
            this.txtBatesSuffix.SupportingValues = "";
            this.txtBatesSuffix.TabIndex = 7;
            this.txtBatesSuffix.Tag2 = null;
            this.txtBatesSuffix.Value = "";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "&Number Of Labels:";
            // 
            // lblBatesPrefix
            // 
            this.lblBatesPrefix.Location = new System.Drawing.Point(13, 99);
            this.lblBatesPrefix.Name = "lblBatesPrefix";
            this.lblBatesPrefix.Size = new System.Drawing.Size(91, 13);
            this.lblBatesPrefix.TabIndex = 4;
            this.lblBatesPrefix.Text = "Bates &Prefix:";
            // 
            // txtBatesPrefix
            // 
            this.txtBatesPrefix.IsDirty = true;
            this.txtBatesPrefix.Location = new System.Drawing.Point(165, 96);
            this.txtBatesPrefix.Name = "txtBatesPrefix";
            this.txtBatesPrefix.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtBatesPrefix.Size = new System.Drawing.Size(156, 20);
            this.txtBatesPrefix.SupportingValues = "";
            this.txtBatesPrefix.TabIndex = 5;
            this.txtBatesPrefix.Tag2 = null;
            this.txtBatesPrefix.Value = "";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.grpCharacter);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(352, 340);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Format";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbHorizAlignment);
            this.groupBox1.Controls.Add(this.cmbVertAlignment);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.spnTopMargin);
            this.groupBox1.Controls.Add(this.spnDigits);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(11, 162);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 157);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Position";
            // 
            // cmbHorizAlignment
            // 
            this.cmbHorizAlignment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbHorizAlignment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbHorizAlignment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHorizAlignment.FormattingEnabled = true;
            this.cmbHorizAlignment.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.cmbHorizAlignment.Location = new System.Drawing.Point(154, 52);
            this.cmbHorizAlignment.Name = "cmbHorizAlignment";
            this.cmbHorizAlignment.Size = new System.Drawing.Size(159, 21);
            this.cmbHorizAlignment.TabIndex = 11;
            // 
            // cmbVertAlignment
            // 
            this.cmbVertAlignment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbVertAlignment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbVertAlignment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVertAlignment.FormattingEnabled = true;
            this.cmbVertAlignment.Items.AddRange(new object[] {
            "Top",
            "Center",
            "Bottom"});
            this.cmbVertAlignment.Location = new System.Drawing.Point(154, 25);
            this.cmbVertAlignment.Name = "cmbVertAlignment";
            this.cmbVertAlignment.Size = new System.Drawing.Size(159, 21);
            this.cmbVertAlignment.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(211, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Digits";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(11, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Label Sheet Top &Margin:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(11, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "&Pad Number To:";
            // 
            // spnTopMargin
            // 
            this.spnTopMargin.AppendSymbol = false;
            this.spnTopMargin.BackColor = System.Drawing.Color.Transparent;
            this.spnTopMargin.DecimalPlaces = 2;
            this.spnTopMargin.DisplayUnit = LMP.Data.mpMeasurementUnits.Inches;
            this.spnTopMargin.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spnTopMargin.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnTopMargin.IsDirty = true;
            this.spnTopMargin.Location = new System.Drawing.Point(154, 116);
            this.spnTopMargin.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spnTopMargin.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            131072});
            this.spnTopMargin.Name = "spnTopMargin";
            this.spnTopMargin.Size = new System.Drawing.Size(51, 22);
            this.spnTopMargin.SupportingValues = "";
            this.spnTopMargin.TabIndex = 16;
            this.spnTopMargin.Tag2 = null;
            this.spnTopMargin.Value = "0.5";
            // 
            // spnDigits
            // 
            this.spnDigits.AppendSymbol = false;
            this.spnDigits.BackColor = System.Drawing.Color.Transparent;
            this.spnDigits.DecimalPlaces = 0;
            this.spnDigits.DisplayUnit = LMP.Data.mpMeasurementUnits.Inches;
            this.spnDigits.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spnDigits.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnDigits.IsDirty = true;
            this.spnDigits.Location = new System.Drawing.Point(154, 86);
            this.spnDigits.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.spnDigits.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnDigits.Name = "spnDigits";
            this.spnDigits.Size = new System.Drawing.Size(51, 22);
            this.spnDigits.SupportingValues = "";
            this.spnDigits.TabIndex = 13;
            this.spnDigits.Tag2 = null;
            this.spnDigits.Value = "4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Label Ali&gnment (Row):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Lab&el Alignment (Column):";
            // 
            // grpCharacter
            // 
            this.grpCharacter.Controls.Add(this.fntFontName);
            this.grpCharacter.Controls.Add(this.chkUnderline);
            this.grpCharacter.Controls.Add(this.lblFont);
            this.grpCharacter.Controls.Add(this.chkBold);
            this.grpCharacter.Controls.Add(this.chkAllCaps);
            this.grpCharacter.Controls.Add(this.lblFontSize);
            this.grpCharacter.Controls.Add(this.chkSmallCaps);
            this.grpCharacter.Controls.Add(this.cmbFontSize);
            this.grpCharacter.Location = new System.Drawing.Point(11, 17);
            this.grpCharacter.Name = "grpCharacter";
            this.grpCharacter.Size = new System.Drawing.Size(330, 135);
            this.grpCharacter.TabIndex = 40;
            this.grpCharacter.TabStop = false;
            this.grpCharacter.Text = "Character";
            // 
            // fntFontName
            // 
            this.fntFontName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.fntFontName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fntFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fntFontName.FormattingEnabled = true;
            this.fntFontName.Items.AddRange(new object[] {
            "Arial",
            "Courier",
            "Times New Roman"});
            this.fntFontName.Location = new System.Drawing.Point(51, 25);
            this.fntFontName.Name = "fntFontName";
            this.fntFontName.Size = new System.Drawing.Size(168, 21);
            this.fntFontName.TabIndex = 40;
            // 
            // chkUnderline
            // 
            this.chkUnderline.AutoSize = true;
            this.chkUnderline.BackColor = System.Drawing.Color.White;
            this.chkUnderline.IsDirty = false;
            this.chkUnderline.Location = new System.Drawing.Point(199, 92);
            this.chkUnderline.Name = "chkUnderline";
            this.chkUnderline.Size = new System.Drawing.Size(71, 17);
            this.chkUnderline.SupportingValues = "";
            this.chkUnderline.TabIndex = 7;
            this.chkUnderline.Tag2 = null;
            this.chkUnderline.Text = "&Underline";
            this.chkUnderline.UseVisualStyleBackColor = true;
            this.chkUnderline.Value = "False";
            // 
            // lblFont
            // 
            this.lblFont.Location = new System.Drawing.Point(11, 28);
            this.lblFont.Name = "lblFont";
            this.lblFont.Size = new System.Drawing.Size(31, 20);
            this.lblFont.TabIndex = 0;
            this.lblFont.Text = "&Font:";
            // 
            // chkBold
            // 
            this.chkBold.AutoSize = true;
            this.chkBold.BackColor = System.Drawing.Color.White;
            this.chkBold.IsDirty = false;
            this.chkBold.Location = new System.Drawing.Point(199, 69);
            this.chkBold.Name = "chkBold";
            this.chkBold.Size = new System.Drawing.Size(47, 17);
            this.chkBold.SupportingValues = "";
            this.chkBold.TabIndex = 5;
            this.chkBold.Tag2 = null;
            this.chkBold.Text = "&Bold";
            this.chkBold.UseVisualStyleBackColor = true;
            this.chkBold.Value = "False";
            // 
            // chkAllCaps
            // 
            this.chkAllCaps.AutoSize = true;
            this.chkAllCaps.BackColor = System.Drawing.Color.White;
            this.chkAllCaps.IsDirty = false;
            this.chkAllCaps.Location = new System.Drawing.Point(51, 69);
            this.chkAllCaps.Name = "chkAllCaps";
            this.chkAllCaps.Size = new System.Drawing.Size(64, 17);
            this.chkAllCaps.SupportingValues = "";
            this.chkAllCaps.TabIndex = 4;
            this.chkAllCaps.Tag2 = null;
            this.chkAllCaps.Text = "&All Caps";
            this.chkAllCaps.UseVisualStyleBackColor = true;
            this.chkAllCaps.Value = "False";
            // 
            // lblFontSize
            // 
            this.lblFontSize.Location = new System.Drawing.Point(225, 28);
            this.lblFontSize.Name = "lblFontSize";
            this.lblFontSize.Size = new System.Drawing.Size(30, 20);
            this.lblFontSize.TabIndex = 2;
            this.lblFontSize.Text = "&Size:";
            // 
            // chkSmallCaps
            // 
            this.chkSmallCaps.AutoSize = true;
            this.chkSmallCaps.BackColor = System.Drawing.Color.White;
            this.chkSmallCaps.IsDirty = false;
            this.chkSmallCaps.Location = new System.Drawing.Point(51, 92);
            this.chkSmallCaps.Name = "chkSmallCaps";
            this.chkSmallCaps.Size = new System.Drawing.Size(81, 17);
            this.chkSmallCaps.SupportingValues = "";
            this.chkSmallCaps.TabIndex = 6;
            this.chkSmallCaps.Tag2 = null;
            this.chkSmallCaps.Text = "S&mall Caps:";
            this.chkSmallCaps.UseVisualStyleBackColor = true;
            this.chkSmallCaps.Value = "False";
            // 
            // cmbFontSize
            // 
            this.cmbFontSize.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbFontSize.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFontSize.FormattingEnabled = true;
            this.cmbFontSize.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14"});
            this.cmbFontSize.Location = new System.Drawing.Point(260, 25);
            this.cmbFontSize.Name = "cmbFontSize";
            this.cmbFontSize.Size = new System.Drawing.Size(53, 21);
            this.cmbFontSize.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(287, 379);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 41;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(206, 379);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 27);
            this.btnOK.TabIndex = 40;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // BatesLabelsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 415);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BatesLabelsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Bates Labels";
            this.Load += new System.EventHandler(this.BatesLabelsForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpCharacter.ResumeLayout(false);
            this.grpCharacter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lblStartRow;
        private System.Windows.Forms.Label lblStartCol;
        private System.Windows.Forms.Label lblLabelDirection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblConfidentialPhrase;
        private System.Windows.Forms.Label lblStartNumber;
        private System.Windows.Forms.Label lblBatesSuffix;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBatesPrefix;
        private System.Windows.Forms.Label lblFontSize;
        private System.Windows.Forms.Label lblFont;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox grpCharacter;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        internal LMP.Controls.Spinner spnStartRow;
        internal LMP.Controls.Spinner spnStartCol;
        internal System.Windows.Forms.ComboBox cmbLabelDirection;
        internal System.Windows.Forms.ComboBox cmbPosition;
        internal LMP.Controls.TextBox txtConfidentialPhrase;
        internal LMP.Controls.TextBox txtStartNumber;
        internal LMP.Controls.TextBox txtNumLabels;
        internal LMP.Controls.TextBox txtBatesSuffix;
        internal LMP.Controls.TextBox txtBatesPrefix;
        internal System.Windows.Forms.ComboBox cmbFontSize;
        internal LMP.Controls.CheckBox chkUnderline;
        internal LMP.Controls.CheckBox chkBold;
        internal LMP.Controls.CheckBox chkAllCaps;
        internal LMP.Controls.CheckBox chkSmallCaps;
        internal LMP.Controls.Spinner spnTopMargin;
        internal LMP.Controls.Spinner spnDigits;
        internal System.Windows.Forms.ComboBox cmbVertAlignment;
        internal System.Windows.Forms.ComboBox cmbHorizAlignment;
        internal System.Windows.Forms.ComboBox fntFontName;

    }
}