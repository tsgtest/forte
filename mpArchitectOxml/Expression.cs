using System;
using System.Text.RegularExpressions;
using LMP.Data;

namespace LMP.Architect.Oxml
{

	/// <summary>
	/// contains the methods and properties that 
	/// define the MacPac expression parser - 
	/// created by Dan Fisherman 9/04
	/// </summary>
	public class XmlExpression
	{

		#region *********************enumerations*********************
		public enum OperatorPrecedenceLevels: byte
		{
			Level1 = 1,
			Level2 = 2,
			Level3 = 3
		}

		public enum SyntaxErrors: byte
		{
			MissingSpace = 1,
			UnmatchedParen = 2,
			InvalidOperator = 3,
			InvalidOperands = 4,
			InvalidFieldCodeSyntax = 5,
            InvalidPreferenceExpression = 6
		}

        public enum PreferenceCategories : byte
        {
            Author = 1,
            User = 2,
            All = 3
        }
        #endregion

		#region *********************constants*********************
		public const char mpExpressionOpenParen = '{';
		public const char mpExpressionCloseParen = '}';
		public const char mpExpressionOperatorTag = '^';
		public const string mpPersistOverPrefillSwitch = "/~";
		public const string mpOrdinalSuffix = "[OrdinalSuffix]";
		public const string mpIndefiniteArticle = "[USStateIndefiniteArticle]";
		public const string mpMyValueCode = "[MyValue]";
		public const byte mpTempLiteralOpenBracketASCII = 178;
		public const byte mpTempLiteralCloseBracketASCII = 179;
		public const byte mpTempLiteralCarrotASCII = 177;
		public const byte mpTempLiteralOpenBraceASCII = 171;
		public const byte mpTempLiteralCloseBraceASCII = 187;
		public const byte mpTempLiteralBackslashASCII = 185;
        //GLOG 5344
        public const byte mpTempLiteralUnderscoreASCII = 172;
		public const short mpTabControlID = -1;

		//supported FieldCodes and operators
        //GLOG : 3129 : JAB
        // Modify the field code strings such that all field codes begin 
        // with an open square bracket ('[').
        //GLOG 7561 (dm) - added [AdditionalAuthorsOfficeAddress__*]
        //GLOG 15838: [XMLFormat__*__*__*]
        public const string mpSupportedFieldCodes = @"\[\\EXEC|\[SEGMENTLANGUAGENAME|\[SEGMENTLANGUAGEID|\[SEGMENTINSERTIONCOMPLETE|\[SUBSTRINGBEFORE|\[SUBSTRINGBEFORELAST|\[SUBSTRINGAFTER|\[SUBSTRINGAFTERLAST|\[MYTAGVALUE|\[MYVALUE|\[LEADAUTHOROFFICE__.*|\[AUTHORSOFFICE__.*|\[AUTHOROFFICE\d*__.*|\[LEADAUTHOR__.*|\[AUTHORS__.*|\[AUTHOR\d*__.*|\[PERSON\d.*__.*|\[USER__.*|\[USEROFFICE__.*|\[AUTHORPREFERENCE\d.*__.*|\[TYPEPREFERENCE__.*|\[OBJECTPREFERENCE__.*|\[APPLICATIONPREFERENCE__.*|\[USERSETTING__.*|\[FIRMSETTING__.*|\[OFFICE-\d.*__.*|\[OFFICE[^-].*__.*|\[COURT__.*|\[COURIER\d.*__.*|\[DOCUMENTVARIABLE__.*|\[STYLE.*__.*|\[PAGESETUP\d.*__.*|\[BOOKMARK__.*|\[LEVEL\d.*__.*|\[VARIABLE__.*|\[CHILDSEGMENTID__.*|\[CHAR__.*|\[UCHAR__.*|\[SUBVAR__.*|\[LEADAUTHORPREFERENCE__.*|\[AUTHORSCOUNT|\[BOILERPLATE__.*|\[OBJECTPROPERTY__.*|\[EXEC.*|\[TODAY|\[EXISTS__.*|\[DOCUMENT__.*|\[WORDAPPLICATION__.*|\[PROPERTYOTHEROBJECT__.*|\[USSTATEINDEFINITEARTICLE|\[ORDINALSUFFIX|\[CM__.*|\[USEROFFICEADDRESS__.*|\[AUTHOROFFICEADDRESS\d.*__.*|\[LEADAUTHOROFFICEADDRESS__.*|\[COURTADDRESS__.*|\[OFFICEADDRESS-\d.*__.*|\[AUTHORSOFFICEADDRESS__.*|\[PERSONOFFICE\d.*__.*|\[PERSONOFFICEADDRESS\d.*__.*|\[WORDBUILTINDOCPROPERTY__.*|\[WORDCUSTOMDOCPROPERTY__.*|\[LEADAUTHORPARENTPREFERENCE__.*|\[AUTHORPARENTPREFERENCE\d.*__.*|\[FUNCTION__.*|\[EXTERNALDATA.*__.*|\[LISTLOOKUP.*__.*|\[EMPTY|\[CIDETAIL__.*|\[RESERVEVALUE|\[TAGVALUE$|\[XMLEXTRACT__.*__.*|\[REPLACE__.*__.*__.*|\[FORMATDATETIME__.*|\[CONVERTTOXML__.*|\[SEGMENTID__.*|\[SEGMENTNAME__.*|\[UPPERCASE__.*|\[LOWERCASE__.*|\[COUNT__.*__.*|\[DATEFORMAT__.*|\[NULL|\[TAGVALUE__.*|\[ADDITIONALAUTHORS__.*|\[ADDITIONALAUTHORSOFFICE__.*|\[ADDITIONALAUTHORSOFFICEADDRESS__.*|\[ADDITIONALAUTHORSOFFICEADDRESS\d.*__.*|\[TRUNCATE__.*__.*|\[SEGMENTLEVEL__.*|\[PREVIOUSLEADAUTHOR__.*|\[PREVIOUSLEADAUTHOROFFICE__.*|\[PREVIOUSLEADAUTHORPREFERENCE__.*|\[PREVIOUSLEADAUTHOROFFICEADDRESS__.*|\[PREVIOUSLEADAUTHORPARENTPREFERENCE__.*|\[PREVIOUSAUTHORSOFFICE__.*|\[PREVIOUSAUTHOROFFICE\d*__.*|\[PREVIOUSAUTHORS__.*|\[PREVIOUSAUTHOR\d*__.*|\[PREVIOUSAUTHOROFFICEADDRESS\d.*__.*|\[PREVIOUSAUTHORPREFERENCE\d.*__.*|\[STARTSWITH__.*__.*|\[ENDSWITH__.*__.*|\[COUNTENTITIES__.*|\[NUMBERTEXT__.*|\[CONVERT.*__.*__.*|\[USPSFORMAT__.*|\[EXTRACTENTITIES__.*__.*|\[CHOOSE__.*|\[COUNTRYNAME__.*|\[STATENAME__.*__.*|\[COUNTYNAME__.*__.*|\[DETAILVALUE$|\[RELINEVALUE$|\[DETAILVALUE__.*|\[RELINEVALUE__.*|\[SEGMENTINDEX|\[LOCATIONID__.*|\[DMS__.*|\[TAGEXPANDEDVALUE$|\[TAGEXPANDEDVALUE__.*|\[ADD__.*__.*|\[SUBTRACT__.*__.*|\[MULTIPLY__.*__.*|\[DIVIDE__.*__.*|\[MODULUS__.*__.*|\[TOPSEGMENTID|\[TOPSEGMENTNAME|\[ISNUMERIC__.*|\[DATEDIFF__.*__.*__.*|\[DATEADD__.*__.*__.*|\[DATESUBTRACT__.*__.*__.*|\[XMLFORMAT__.*__.*__.*";
        //old-format FieldCodes that are different from current format
        public const string mpSupportedFieldCodesOLD = @"\[LEADAUTHOROFFICE_.*|\[AUTHORSOFFICE_.*|\[AUTHOROFFICE\d*_.*|\[LEADAUTHOR_.*|\[AUTHORS_.*|\[AUTHOR\d*_.*|\[PERSON\d.*_.*|\[USER_.*|\[USEROFFICE_.*|\[AUTHORPREFERENCE\d.*_.*|\[TYPEPREFERENCE_.*|\[OBJECTPREFERENCE_.*|\[APPLICATIONPREFERENCE_.*|\[USERSETTING_.*|\[FIRMSETTING_.*|\[OFFICE-\d.*_.*|\[OFFICE[^-].*_.*|\[COURT_.*|\[COURIER\d.*_.*|\[DOCUMENTVARIABLE_.*|\[STYLE.*_.*|\[PAGESETUP\d.*_.*|\[BOOKMARK_.*|\[LEVEL\d.*_.*|\[VARIABLE_.*|\[CHILDSEGMENTID_.*|\[CHAR_.*|\[UCHAR_.*|\[SUBVAR_.*|\[LEADAUTHORPREFERENCE_.*|\[BOILERPLATE_.*|\[OBJECTPROPERTY_.*|\[EXISTS_.*|\[DOCUMENT_.*|\[WORDAPPLICATION_.*|\[PROPERTYOTHEROBJECT_.*|\[CM_.*|\[USEROFFICEADDRESS_.*|\[AUTHOROFFICEADDRESS\d.*_.*|\[LEADAUTHOROFFICEADDRESS_.*|\[OFFICEADDRESS-\d.*_.*|\[AUTHORSOFFICEADDRESS_.*|\[PERSONOFFICE\d.*_.*|\[PERSONOFFICEADDRESS\d.*_.*|\[WORDBUILTINDOCPROPERTY_.*|\[WORDCUSTOMDOCPROPERTY_.*|\[LEADAUTHORPARENTPREFERENCE_.*|\[AUTHORPARENTPREFERENCE\d.*_.*|\[FUNCTION_.*|\[EXTERNALDATA.*_.*|\[LISTLOOKUP.*_.*|\[CIDETAIL_.*|\[XMLEXTRACT.*_.*|\[REPLACE_.*_.*_.*|\[FORMATDATETIME_.*|\[CONVERTTOXML_.*|\[SEGMENTID_.*|\[SEGMENTNAME_.*|\[UPPERCASE_.*|\[LOWERCASE_.*|\[COUNT_.*_.*|\[DATEFORMAT_.*|\[TAGVALUE_.*|\[ADDITIONALAUTHORS_.*|\[ADDITIONALAUTHORSOFFICE_.*|\[ADDITIONALAUTHORSOFFICEADDRESS\d.*_.*|\[TRUNCATE_.*_.*|\[SEGMENTLEVEL_.*|\[PREVIOUSLEADAUTHOR_.*|\[PREVIOUSLEADAUTHOROFFICE_.*|\[PREVIOUSLEADAUTHORPREFERENCE_.*|\[PREVIOUSLEADAUTHOROFFICEADDRESS_.*|\[PREVIOUSLEADAUTHORPARENTPREFERENCE_.*|\[PREVIOUSAUTHORSOFFICE_.*|\[PREVIOUSAUTHOROFFICE\d*_.*|\[PREVIOUSAUTHORS_.*|\[PREVIOUSAUTHOR\d*_.*|\[PREVIOUSAUTHOROFFICEADDRESS\d.*_.*|\[PREVIOUSAUTHORPREFERENCE\d.*_.*|\[STARTSWITH_.*_.*|\[ENDSWITH_.*_.*|\[COUNTENTITIES_.*|\[NUMBERTEXT_.*|\[CONVERT.*_.*_.*|\[USPSFORMAT_.*|\[EXTRACTENTITIES_.*_.*|\[CHOOSE_.*|\[COUNTRYNAME_.*|\[STATENAME_.*_.*|\[COUNTYNAME_.*_.*|\[DETAILVALUE_.*|\[RELINEVALUE_.*|\[LOCATIONID_.*|\[DMS_.*|\[TOPSEGMENTID\]|\[TOPSEGMENTNAME\]|\[XMLFORMAT_.*_.*_.*";
        public const string mpSupportedOperators = @"|^=|^!=|^GT|^LT|^GTE|^LTE|^AND|^OR|^LIKE|^?|^:|";
        //GLOG 5384: Added FormatDateTime to list of Fieldcodes not requiring a Segment object
        public const string mpNonSegmentFieldCodes = "|USERSETTING|FIRMSETTING|OFFICE||COURIER|LEVEL|CHAR|UCHAR|TODAY|ORDINALSUFFIX|CM|USEROFFICEADDRESS|OFFICEADDRESS|PERSONOFFICE|PERSONOFFICEADDRESS|FUNCTION|EXTERNALDATA|SUBSTRINGBEFORE|SUBSTRINGBEFORELAST|SUBSTRINGAFTER|SUBSTRINGAFTERLAST|LISTLOOKUP|EMPTY|XMLEXTRACT|REPLACE|CONVERTTOXML|SEGMENTID|SEGMENTNAME|UPPERCASE|LOWERCASE|COUNT|NULL|TRUNCATE|STARTSWITH|ENDSWITH|COUNTENTITIES|NUMBERTEXT|CONVERT|USPSFORMAT|EXTRACTENTITIES|CHOOSE|COUNTRYNAME|STATENAME|COUNTYNAME|LOCATIONID|ADD|SUBTRACT|MULTIPLY|DIVIDE|MODULUS|FORMATDATETIME|ISNUMERIC|DATEDIFF|DATEADD|DATESUBTRACT|XMLFORMAT";
        #endregion

		#region *********************constructors*********************
		public XmlExpression()
		{
		}
		#endregion
		#region *********************methods*********************
		/// <summary>
		/// returns true iff the specified expression contains one
		/// of the five user codes - User, TypePreference, ObjectPreference, 
		/// ApplicationPreference, and FirmSetting
		/// </summary>
		/// <param name="xExp">a Macpac expression</param>
		/// <returns></returns>
		public static bool ContainsUserCode(string xExp)
		{
			if(xExp == null || xExp == "")
				return false;

            //GLOG 3627: Use Uppercase for comparison
            xExp = xExp.ToUpper();

            return (xExp.Contains("[USER_") || xExp.Contains("[TYPEPREFERENCE_") ||
                xExp.Contains("[OBJECTPREFERENCE_") || xExp.Contains("[APPLICATIONPREFERENCE_") ||
                xExp.Contains("[FIRMSETTING_"));
		}

        /// <summary>
        /// returns True iff the specified expression
        /// contains a preference field code
        /// </summary>
        /// <param name="xExp"></param>
        /// <returns></returns>
        public static bool ContainsPreferenceCode(string xExp)
        {
            return ContainsPreferenceCode(xExp, PreferenceCategories.All);
        }

        /// <summary>
        /// returns True iff the specified expression contains
        /// a preference field code belonging to the specified category
        /// </summary>
        /// <param name="xExp"></param>
        /// <param name="iCategory"></param>
        /// <returns></returns>
        public static bool ContainsPreferenceCode(string xExp,
            XmlExpression.PreferenceCategories iCategory)
        {
            DateTime t0 = DateTime.Now;

            if (xExp == null || xExp == "")
                return false;

            bool bIsMatch = false;
            //JTS 2/10/09: Need to convert to uppercase for corrrect comparison
            string xExpUpper = xExp.ToUpper();
            if (iCategory == PreferenceCategories.Author || iCategory == PreferenceCategories.All)
            {
                if (xExpUpper.Contains("[AUTHORPREFERENCE_") || xExpUpper.Contains("[LEADAUTHORPREFERENCE_") ||
                    xExpUpper.Contains("[AUTHORPARENTPREFERENCE") || xExpUpper.Contains("[LEADAUTHORPARENTPREFERENCE_"))
                    bIsMatch = true;
            }

            if (!bIsMatch && (iCategory == PreferenceCategories.User || iCategory == PreferenceCategories.All))
            {
                if (xExpUpper.Contains("[TYPEPREFERENCE_") || xExpUpper.Contains("[OBJECTPREFERENCE_") ||
                    xExpUpper.Contains("[APPLICATIONPREFERENCE_"))
                    bIsMatch = true;
            }

            LMP.Benchmarks.Print(t0, xExp);
            return bIsMatch;
        }

        ///// <summary>
        ///// returns True iff the specified expression contains
        ///// a preference field code belonging to the specified category
        ///// </summary>
        ///// <param name="xExp"></param>
        ///// <param name="iCategory"></param>
        ///// <returns></returns>
        //public static bool ContainsPreferenceCode(string xExp,
        //    Expression.PreferenceCategories iCategory)
        //{
        //    DateTime t0 = DateTime.Now;

        //    if (xExp == null || xExp == "")
        //        return false;

        //    string xPattern = @"(?i)";

        //    if (iCategory != PreferenceCategories.Author)
        //        xPattern += @".*\[TYPEPREFERENCE.*\].*|.*\[OBJECTPREFERENCE.*\].*|.*\[APPLICATIONPREFERENCE.*\].*";

        //    if (iCategory == PreferenceCategories.All)
        //        xPattern += "|";

        //    if (iCategory != PreferenceCategories.User)
        //        xPattern += @".*\[AUTHORPREFERENCE.*\].*|.*\[LEADAUTHORPREFERENCE.*\].*|.*\[AUTHORPARENTPREFERENCE.*\].*|.*\[LEADAUTHORPARENTPREFERENCE.*\].*";

        //    bool bIsMatch = Regex.IsMatch(xExp, xPattern, RegexOptions.IgnorePatternWhitespace);

        //    LMP.Benchmarks.Print(t0, xExp);
        //    return bIsMatch;
        //}
        /// <summary>
        /// Replaces reserved expression characters preceded by backslash
        /// with non-reserved temporary characters
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        private static string ReplaceReservedCharacters(string xInput)
        {
            if (xInput == null)
                return null;
            
            string xExpTemp = xInput;

            xExpTemp = xExpTemp.Replace(@"\" + XmlFieldCode.mpFieldCodeStartTag, ((char)mpTempLiteralOpenBracketASCII).ToString());
            xExpTemp = xExpTemp.Replace(@"\" + XmlFieldCode.mpFieldCodeEndTag, ((char)mpTempLiteralCloseBracketASCII).ToString());
            xExpTemp = xExpTemp.Replace(@"\" + mpExpressionOperatorTag, ((char)mpTempLiteralCarrotASCII).ToString());
            xExpTemp = xExpTemp.Replace(@"\" + mpExpressionOpenParen, ((char)mpTempLiteralOpenBraceASCII).ToString());
            xExpTemp = xExpTemp.Replace(@"\" + mpExpressionCloseParen, ((char)mpTempLiteralCloseBraceASCII).ToString());
            xExpTemp = xExpTemp.Replace(@"\\", ((char)mpTempLiteralBackslashASCII).ToString());
            //GLOG 5344: Include Underscore character
            xExpTemp = xExpTemp.Replace(@"\" + "_", ((char)mpTempLiteralUnderscoreASCII ).ToString());

            return xExpTemp;
        }
        /// <summary>
        /// Replaces predefined temporary characters with corresponding Literal reserved character
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        private static string RestoreReservedCharacters(string xInput)
        {
            if (xInput == null)
                return null;

            string xExpTemp = xInput;
            xExpTemp = xExpTemp.Replace(((char)mpTempLiteralOpenBracketASCII).ToString(), XmlFieldCode.mpFieldCodeStartTag.ToString());
            xExpTemp = xExpTemp.Replace(((char)mpTempLiteralCloseBracketASCII).ToString(), XmlFieldCode.mpFieldCodeEndTag.ToString());
            xExpTemp = xExpTemp.Replace(((char)mpTempLiteralCarrotASCII).ToString(), mpExpressionOperatorTag.ToString());
            xExpTemp = xExpTemp.Replace(((char)mpTempLiteralOpenBraceASCII).ToString(), mpExpressionOpenParen.ToString());
            xExpTemp = xExpTemp.Replace(((char)mpTempLiteralCloseBraceASCII).ToString(), mpExpressionCloseParen.ToString());
            xExpTemp = xExpTemp.Replace(((char)mpTempLiteralBackslashASCII).ToString(), @"\");
            //GLOG 5344
            xExpTemp = xExpTemp.Replace(((char)mpTempLiteralUnderscoreASCII).ToString(), @"_");
            return xExpTemp;
        }
        /// <summary>
        /// Mark reserved expression characters in a string with preceding backslash
        /// so they'll be interpreted as literals during evaluation
        /// </summary>
        /// <param name="xInput"></param>
        /// <returns></returns>
        public static string MarkLiteralReservedCharacters(string xInput)
        {
            if (xInput == null)
                return null;

            string xExpTemp = xInput;

            //GLOG 5344: Add underscore to list of reserved characters
            char[] aChars = new char[] { '\\', '[', ']', '^', '{', '}' , '_'};

            //Insert backslash before each reserved character
            foreach (char c in aChars)
            {
                xExpTemp = xExpTemp.Replace(c.ToString(), @"\" + c.ToString());
            }
            return xExpTemp;
        }
        /// <summary>
		/// returns the column 2 value of the first value
		/// set row that matches the supplied value
		/// </summary>
		/// <param name="xValue"></param>
		/// <param name="iValueSetID"></param>
		/// <param name="oMPObject"></param>
		/// <returns></returns>
		public static string Lookup(string xValue, int iListID, XmlSegment oSegment, XmlForteDocument oForteDoc)
		{
			string xRet = "";

			Trace.WriteNameValuePairs("xValue", xValue, "iListID", iListID);

			//evaluate expression
			string xExpEval = Evaluate(xValue, oSegment, oForteDoc);

			//get value set as a list
			Lists oLists = LMP.Data.Application.GetLists(false);
			
			List oList = (List) oLists.ItemFromID(iListID);

			byte bytSearchCol = 1;
			byte bytRetCol = 2;

			xRet = Evaluate(oList.Lookup(xValue, bytSearchCol, bytRetCol), oSegment, oForteDoc);

			return xRet;
		}
		/// <summary>
		/// returns True iff the syntax in the specified expression is valid
		///check for matched parens
		///check for proper field code syntax
		///check for spaces around operators
		///check that each ^ represents an operator
		///check for 2 operands for each operator [all operators are binary operators]
		/// </summary>
		/// <param name="xExp">MacPac expression whose syntax is to be tested</param>
		/// <param name="iErrPos">Position of syntax error</param>
		/// <param name="iErrType">Type of syntax error</param>
		/// <returns></returns>
        public static bool SyntaxIsValid(string xExpression, out int iErrPos, out XmlExpression.SyntaxErrors bytErrType)
        {
            //TODO: Turn off automatic support for old fieldcodes at some point
            return SyntaxIsValid(xExpression, out iErrPos, out bytErrType, true);
        }
		public static bool SyntaxIsValid(string xExpression, out int iErrPos, out XmlExpression.SyntaxErrors bytErrType, bool bAllowOldFormat)
		{
			//set default error type and position - might be changed below
			bytErrType = 0;
			iErrPos = -1;

            ////convert reserved characters that should be interpreted as literals 
            ////(ie those preceded by '\') to a non-reserved character-
            //string xExpTemp = "";
            //xExpTemp = xExpTemp.Replace(@"\" + FieldCode.mpFieldCodeStartTag, ((char) mpTempLiteralOpenBracketASCII).ToString());
            //xExpTemp = xExpTemp.Replace(@"\" + FieldCode.mpFieldCodeEndTag, ((char) mpTempLiteralCloseBracketASCII).ToString());
            //xExpTemp = xExpTemp.Replace(@"\" + mpExpressionOperatorTag, ((char) mpTempLiteralCarrotASCII).ToString());
            //xExpTemp = xExpTemp.Replace(@"\" + mpExpressionOpenParen, ((char) mpTempLiteralOpenBraceASCII).ToString());
            //xExpTemp = xExpTemp.Replace(@"\" + mpExpressionCloseParen, ((char) mpTempLiteralCloseBraceASCII).ToString());
            //xExpTemp = xExpTemp.Replace(@"\\", ((char) mpTempLiteralBackslashASCII).ToString());

			//check for spaces around operators
			if(!OperatorsHaveSpace(xExpression, out iErrPos))
			{
				bytErrType = XmlExpression.SyntaxErrors.InvalidOperator;
				return false;
			}
    
			//check for unmatched parens
			if(!ParensMatch(xExpression, out iErrPos))
			{
				bytErrType = XmlExpression.SyntaxErrors.UnmatchedParen;
				return false;
			}
		    
			//check for invalid operators
			if(!OperatorsAreValid(xExpression, out iErrPos))
			{
				bytErrType = XmlExpression.SyntaxErrors.InvalidOperator;
				return false;
			}

			//check for invalid field code syntax
			if(!FieldCodesAreValid(xExpression, out iErrPos, bAllowOldFormat))
			{
				bytErrType = XmlExpression.SyntaxErrors.InvalidFieldCodeSyntax;
				return false;
			}
		    
            if(PreferenceFieldCodesNotAlone(xExpression, out iErrPos))
			{
				bytErrType = XmlExpression.SyntaxErrors.InvalidPreferenceExpression;
				return false;
			}
			//if we got here, all is ok
			return true;
		}

		/// <summary>
		/// evaluates the supplied expression- does this by
		/// parsing expression into operator/operands units, then reiterating-
		/// assumes that all operators are binary
		/// </summary>
		/// <param name="xExp"></param>
		/// <param name="oMPObject"></param>
		/// <param name="bResetExpressionCache"></param>
		/// <returns></returns>
        public static string Evaluate(string xExpression, XmlSegment oSegment, XmlForteDocument oForteDoc)
        {
            //TODO: turn off automatic support for old-format fieldcodes at some point
            return Evaluate(xExpression, oSegment, oForteDoc, true, null);
        }
        public static string Evaluate(string xExpression, XmlSegment oSegment, XmlForteDocument oForteDoc, bool bAllowOldFormat)
        {
            return Evaluate(xExpression, oSegment, oForteDoc, bAllowOldFormat, null);
        }
        
        public static string Evaluate(string xExpression, XmlSegment oSegment, XmlForteDocument oForteDoc,
            bool bAllowOldFormat, FieldCodeExternalEvaluationRequiredHandler oExternalEvalRequiredDelegate)
		{
            DateTime t0 = DateTime.Now;

            //return if expression is empty
            if ((xExpression == "") || (xExpression == null))
                return "";

			string xExpTemp = xExpression;
			string xEvaluated = "";

			Trace.WriteNameValuePairs("xExpression", xExpression);

			//exit function if there is nothing to evaluate
            //GLOG 5344: Add Underscore character as marker to evaluate expression
            string xTags = string.Concat(@"\", XmlFieldCode.mpFieldCodeStartTag, @"|\",
                mpExpressionOperatorTag, @"|\", mpExpressionOpenParen, "|",
				XmlFieldCode.mpFieldCodeEndTag, "|", mpExpressionCloseParen, "|", XmlFieldCode.mpFieldCodeParamSepOLD);

			if(!Regex.IsMatch(xExpTemp, xTags))
				//nothing to evaluate - return expression
				return xExpTemp;

			//temporarily convert reserved characters that should be
			//interpreted as literals (ie those preceded by '\')
			//to a non-reserved character- we'll return these characters
			//to their literal when evaluating the binary expression
            xExpTemp = XmlExpression.ReplaceReservedCharacters(xExpTemp);

            // Replace underscore in character fieldcode so that this does not interfere when nested
            xExpTemp = xExpTemp.Replace("[Char__", "[Char~");
            if (bAllowOldFormat)
                xExpTemp = xExpTemp.Replace("[Char_", "[Char~");
            //set evaluated version of expression
			string xExpEval = EvaluateFieldCodes(xExpTemp, oSegment, oForteDoc, 
                bAllowOldFormat, oExternalEvalRequiredDelegate);
            //restore underscores in Char fieldcodes            
            xExpEval = xExpEval.Replace("[Char~", "[Char__");

			xExpEval = EvaluateUSStateIndefiniteArticles(xExpEval);
			xExpEval = EvaluateOrdinalSuffixes(xExpEval);

			Trace.WriteNameValuePairs("xExpEval", xExpEval);
			
			//evaluate resulting expression - first evaluate expressions in parens
			xExpTemp = xExpEval;

			int iParenOpenPos = xExpTemp.IndexOf(mpExpressionOpenParen);

			while(iParenOpenPos > -1)
			{
				//there's an opening Brace - find closing Brace
				int iCurPos = iParenOpenPos;
				int iNumUnMatchedBraces = 1;
				int iNumBraces = 1;
				int iStartPos = -1;
				int iEndPos = -1;

				while(iNumUnMatchedBraces > 0 || (iStartPos == -1 && iEndPos == -1))
				{
					//see if next Brace is an open or close Brace
					iStartPos = xExpTemp.IndexOf(mpExpressionOpenParen, iCurPos + 1);
					iEndPos = xExpTemp.IndexOf(mpExpressionCloseParen, iCurPos + 1);

                    if (iEndPos == -1 || (iEndPos < iCurPos))
                    {
                        //there is an unmatched Brace
                        if (oForteDoc.Mode == XmlForteDocument.Modes.Design)
                        {
                            throw new LMP.Exceptions.ExpressionException(
                                LMP.Resources.GetLangString(
                                "Error_InvalidExpressionBracketMismatch" + xExpression));
                        }
                        else
                        {
                            //GLOG #4216 - dcf
                            //we're not designing the document,
                            //so just consider the expression to be text
                            return xExpression;
                        }
                    }
                    else if (iStartPos == -1 || (iStartPos > iEndPos))
                    {
                        //next Brace is closing Brace-
                        //decrement unmatched Brace count
                        iNumUnMatchedBraces--;
                        iCurPos = iEndPos;
                        iNumBraces++;
                    }
                    else
                    {
                        //next Brace is opening Brace-
                        //increment unmatched Brace count
                        iNumUnMatchedBraces++;
                        iCurPos = iStartPos;
                        iNumBraces++;
                    }
				}

				// if we got here, all open Braces have a close Brace counterpart
				// and we have a starting and ending position of the expression-
				// get the expression
				string xClause = xExpTemp.Substring(iParenOpenPos + 1, iEndPos - iParenOpenPos - 1);

				Trace.WriteNameValuePairs("xClause", xClause);

				if(xClause.IndexOf(mpExpressionOpenParen) > -1 || HasOperator(xClause))
				{
					//evaluate complex expression by iterating
					xEvaluated = Evaluate(xClause, oSegment, oForteDoc);
				}
				else
				{
					//phrase does not need further evaluation-
					//remove braces if they exist
                    //GLOG #3465: Added criterion that clause can't be empty -
                    //remmed statement below is the old one
                    if (xClause != "" && xClause.Substring(0, 1) == mpExpressionOpenParen.ToString() &&
                        xClause.Substring(xClause.Length - 1, 1) == mpExpressionCloseParen.ToString())
                        xClause = xClause.Substring(3, xClause.Length - 1);

                    //if(xClause.Substring(0, 1) == mpExpressionOpenParen.ToString() &&
                    //    xClause.Substring(xClause.Length - 1, 1) == mpExpressionCloseParen.ToString())
                    //    xClause = xClause.Substring(3, xClause.Length - 1);

					xEvaluated = xClause;
				}

				//replace field code clause with value
				xExpEval = xExpEval.Replace(string.Concat(mpExpressionOpenParen,
					xClause, mpExpressionCloseParen), xEvaluated);

				//get position of next open paren
				iParenOpenPos = xExpTemp.ToUpper().IndexOf(mpExpressionOpenParen, iParenOpenPos + 1);

				Trace.WriteNameValuePairs("xExpEval", xExpEval);
			}

			//expression has no more braces, evaluate from left to right
			//according to order of precedence - operators are grouped
			//into 2 groups of precedence - AND and OR are low,
			//=,<>,>, >=,<,<=, and LIKE are high
			for(byte bytLevel  = (byte) XmlExpression.OperatorPrecedenceLevels.Level1; 
				bytLevel <= (byte) XmlExpression.OperatorPrecedenceLevels.Level3; bytLevel++)
			{
				while(HasOperator(xExpEval, (XmlExpression.OperatorPrecedenceLevels) bytLevel))
				{
					//more of the expression to parse - get next
					//sub expression whose operator is in this
					//precedence level
					string xFirstSubExp = GetFirstSubExpression(xExpEval, 
						(XmlExpression.OperatorPrecedenceLevels) bytLevel);

					//replace first sub expression with evaluation
					xExpEval = xExpEval.Replace(xFirstSubExp, 
						EvaluateOperation(xFirstSubExp, bAllowOldFormat).ToString());

					Trace.WriteNameValuePairs("xFirstSubExp", xFirstSubExp);
				}
			}

            if (xExpEval != "" && xExpEval != null)
            {
                //evaluate any remaining character codes - we leave this to last because
                //we trim white space when evaluating expressions - character codes are also
                //evaluated in evalutate operation
                xExpEval = EvaluateCharacterCodes(xExpEval, bAllowOldFormat);

                //return any remaining temporary characters to their appropriate literal
                xExpEval = RestoreReservedCharacters(xExpEval);

                if (xExpEval.Substring(0, 1) == '"'.ToString() &&
                    xExpEval.Substring(xExpEval.Length - 1, 1) == '"'.ToString())
                    //expression has enclosing quotes - trim
                    xExpEval = xExpEval.Substring(1, xExpEval.Length - 2);
            }

			LMP.Benchmarks.Print(t0, xExpression + " ; " + xExpEval);

			return xExpEval;
		}
		/// <summary>
		/// TRUE if xExp contains field code requiring update of doc prop
		/// values and/or execution of actions upon specified author change;
		/// -1 = update all author fields, 0 = update lead author only,
		/// 1 = update first author, 2 = update second author, etc.
		/// </summary>
		/// <param name="Expression"></param>
		/// <param name="shAuthorIndex"></param>
		/// <param name="shParentAuthorIndex"></param>
		/// <returns></returns>
		public static bool ContainsAuthorCode(string xExpression, short shAuthorIndex, short shParentAuthorIndex)
		{
            DateTime t0 = DateTime.Now;

            bool bRes = false;
			if(xExpression == "" || xExpression == null)
				return false;

			Trace.WriteNameValuePairs("xExpression", xExpression, "shAuthorIndex", shAuthorIndex, 
				"shParentAuthorIndex", shParentAuthorIndex);

			switch(shAuthorIndex)
			{
				case -2:
					//no author-related fields
					break;
				case -1:
				{
					//all author related fields
                    //GLOG 7561 (dm) - we were missing AuthorOffice and AuthorOfficeAddress for specific author
                    if (Regex.IsMatch(xExpression.ToUpper(), @"(?i).*\[LEADAUTHOR.*\].*|.*\[AUTHORS.*\].*|.*\[AUTHOR\d.*\]|.*\[ADDITIONALAUTHORS.*\].*|.*\[PARENT::LEADAUTHOR.*\].*|.*\[PARENT::AUTHORS.*\].*|.*\[PARENT::ADDITIONALAUTHORS.*\].*|.*\[PARENT::AUTHOR\d.*\]|.*\[AUTHOROFFICE\d.*\]|.*\[AUTHOROFFICEADDRESS\d.*\]|.*\[PARENT::AUTHOROFFICE\d.*\]|.*\[PARENT::AUTHOROFFICEADDRESS\d.*\]")) 
                        bRes = true;
					break;
				}
				case 0:
				{
                    if (Regex.IsMatch(xExpression.ToUpper(), @"(?i).*\[LEADAUTHOR.*\].*|.*\[AUTHORS.*\].*|.*\[ADDITIONALAUTHORS.*\].*|.*\[PARENT::LEADAUTHOR.*\].*|.*\[PARENT::AUTHORS.*\].*|.*\[PARENT::ADDITIONALAUTHORS.*\].*"))
						bRes = true;
					break;
				}
				default:
				{
					//all fields related to the specified author
					if(Regex.IsMatch(xExpression.ToUpper(), string.Concat(
						@"(?i).*\[AUTHOR",shAuthorIndex.ToString() ,
						@".*\].*|.*\[AUTHORS",
                        @".*\].*|.*\[ADDITIONALAUTHORS",
                        @".*\].*|.*\[AUTHOROFFICE", shAuthorIndex.ToString(), 
						@".*\].*|.*\[AUTHORPREFERENCE", shAuthorIndex.ToString(), 
						@".*\].*|.*\[AUTHORPARENTPREFERENCE", shAuthorIndex.ToString(), 
						@".*\].*")))
                        bRes = true;

					break;
				}
			}

            if (!bRes)
            {
                switch (shParentAuthorIndex)
                {
                    case -2:
                        //no author-related fields
                        break;
                    case -1:
                        {
                            //all author related fields
                            if (Regex.IsMatch(xExpression, @"(?i).*\[Parent::LeadAuthor.*\].*|.*\[Parent::Authors.*\].*"))
                                bRes = true;
                            break;
                        }
                    case 0:
                        {
                            if (Regex.IsMatch(xExpression, @"(?i).*\[Parent::LeadAuthor.*\].*|.*\[Parent::Author.*\].*"))
                                bRes = true;
                            break;
                        }
                    default:
                        {
                            //all fields related to the specified author
                            if (Regex.IsMatch(xExpression, string.Concat(
                                @"(?i).*\[Parent::Author", shAuthorIndex.ToString(),
                                @".*\].*|.*\[Parent::Authors",
                                @".*\].*|.*\[Parent::AuthorOffice", shAuthorIndex.ToString(),
                                @".*\].*|.*\[Parent::AuthorPreference", shAuthorIndex.ToString(),
                                @".*\].*|.*\[Parent::AuthorParentPreference", shAuthorIndex.ToString(),
                                @".*\].*")))
                                bRes = true;
                            break;
                        }
                }
            }

            LMP.Benchmarks.Print(t0, xExpression);
			return bRes;
		}

		//true iff xExp contains a "Variable" field code
		public static bool ContainsVariableCode(string xExpression)
		{
            DateTime t0 = DateTime.Now;

			if(xExpression == "" || xExpression == null)
				return false;

			Trace.WriteNameValuePairs("xExpression", xExpression);

            //JTS 2/19/09: Use correct case for comparison
            bool bIsMatch = xExpression.ToUpper().Contains("[VARIABLE_");

            LMP.Benchmarks.Print(t0);

            return bIsMatch;
		}
		#endregion

		#region *********************private members*********************
		/// <summary>
		/// returns True iff the expression has a recognized operator with the specified level-
		/// if no level is supplied, returns True iff the expression has a recognized operator
		/// </summary>
		/// <param name="xExp"></param>
		/// <param name="bytLevel"></param>
		/// <returns></returns>
        /// <summary>
        /// returns True iff the expression has a recognized operator with the specified level-
        /// if no level is supplied, returns True iff the expression has a recognized operator
        /// </summary>
        /// <param name="xExp"></param>
        /// <param name="bytLevel"></param>
        /// <returns></returns>
        private static bool HasOperator(string xExpression, XmlExpression.OperatorPrecedenceLevels bytLevel)
        {
            string[] aOps = null;
            bool bIsMatch = false;

            //search for different operators based on precedence level -
            //default case is all operators
            switch (bytLevel)
            {
                case XmlExpression.OperatorPrecedenceLevels.Level1:
                    {
                        aOps = new string[] {"^=", "^!=", "^GT", "^LT", "^GTE", 
                            "^LTE", "^LIKE"};
                        break;
                    }
                case XmlExpression.OperatorPrecedenceLevels.Level2:
                    {
                        aOps = new string[] { "^AND", "^OR" };
                        break;
                    }
                case XmlExpression.OperatorPrecedenceLevels.Level3:
                    {
                        aOps = new string[] { "^?" };
                        break;
                    }
                default:
                    {
                        aOps = new string[] {"^=", "^!=", "^GT", "^LT", "^GTE", 
                            "^LTE", "^LIKE", "^AND", "^OR", "^?"};
                        break;
                    }
            }
            foreach (string xOp in aOps)
            {
                if (xExpression.Contains(xOp))
                {
                    bIsMatch = true;
                    break;
                }
            }
            return bIsMatch;
        }

		private static bool HasOperator(string xExpression)
		{
			return HasOperator(xExpression, 0);
		}

		/// <summary>
		/// returns a string that replaces all [CHAR]
		/// field codes with the appropriate characters
		/// </summary>
		/// <param name="xExp"></param>
		/// <returns></returns>
		public static string EvaluateCharacterCodes(string xExpression, bool bAllowOldFormat)
		{
            int iCodeOffset = 7;
            string xMod = xExpression;

			Trace.WriteNameValuePairs("xExpression", xExpression);

			//exit if expression is empty
			if(xMod == null || xMod == "")
				return xMod;

            int iCodeStart = xMod.ToUpper().IndexOf("[CHAR__");
            if (iCodeStart == -1 && bAllowOldFormat)
            {
                //Old format fieldcode with single underscore separator
                iCodeStart = xMod.ToUpper().IndexOf("[CHAR_");
                if (iCodeStart > -1)
                    iCodeOffset = 6;
            }
            while (iCodeStart > -1)
            {
                //Locate closing bracket
                int iCodeEnd = xMod.IndexOf("]", iCodeStart) + 1;
                string xCode = xMod.Substring(iCodeStart, iCodeEnd - iCodeStart);
				//get the key code of the field code
				string xKeyCode = xMod.Substring(iCodeStart + iCodeOffset, (iCodeEnd - 1) - (iCodeStart + iCodeOffset));
				int iKeyCode = 0;

				//convert to int - alert if not convertible
				try
				{
					iKeyCode = System.Convert.ToInt32(xKeyCode);
				}
				catch(System.Exception oE)
				{
					throw new LMP.Exceptions.FieldCodeException(
						LMP.Resources.GetLangString("Error_FieldCode_Char") + xExpression,oE);
				}

				//replace the field code with the character having the specified key code
				xMod = xMod.Replace(xCode, ((char) iKeyCode).ToString());
                //Search for additional character codes
                iCodeStart = xMod.ToUpper().IndexOf("[CHAR__");
                if (iCodeStart == -1 && bAllowOldFormat)
                {
                    iCodeStart = xMod.ToUpper().IndexOf("[CHAR_");
                    if (iCodeStart > -1)
                        iCodeOffset = 6;
                }
                else
                    iCodeOffset = 7;
            }
			return xMod;
		}

		/// <summary>
		/// returns the evaluation of the specified logical expression - 
		/// all evaluations are case-insensitive
		/// </summary>
		/// <param name="xExp">a MacPac expression</param>
		/// <returns></returns>
		private static object EvaluateOperation(string xExpression, bool bAllowOldFormat)
		{
			DateTime t0 = DateTime.Now;

			string xOp = null;
			int iPos1 = 0;
			string xTempExp = null;
			object oRet = null;

			xTempExp = xExpression;

            ////remove encapsulating brackets if they exist
            //if (ContainsEncapsulatingBrackets(xTempExp))
            //{
            //    xTempExp = xExpression.TrimStart(FieldCode.mpFieldCodeStartTag);
            //    xTempExp = xTempExp.TrimEnd(FieldCode.mpFieldCodeEndTag);
            //}

			//parse into operator and field 
			//codes get position of operator
            string[] aOperators = new string[] {"^=", "^!=", "^GTE", "^LTE", "^GT", "^LT", "^AND",
                "^OR", "^LIKE", "^?"};
			
            foreach (string xOpTest in aOperators)
            {
                int iPos = xExpression.IndexOf(xOpTest);
                if (iPos > -1)
                {
                    xOp = xOpTest;
                    iPos1 = iPos;
                    break;
                }
            }

            if (xOp == null)
				//no matching operator - alert
				throw new LMP.Exceptions.FieldCodeException(
					LMP.Resources.GetLangString("Error_InvalidOperator") + xExpression);

			//get operands
			string xOperand1 = xTempExp.Substring(0,iPos1);

            //trim last trailing space
            if (xOperand1.EndsWith(" "))
                xOperand1 = xOperand1.Substring(0, xOperand1.Length - 1);

			string xOperand2 = xTempExp.Substring(iPos1 + xOp.Length);
            if (xOperand2.StartsWith(" "))
                xOperand2 = xOperand2.Substring(1);
            
            string xOperand3 = null;

			iPos1 = xOperand2.IndexOf("^:");
			if(iPos1 > 0)
			{
				//IF operator - there is a third operand
				int iPos3 = iPos1 + xOp.Length + 1;
				if(iPos3 < xOperand2.Length)
					xOperand3 = xOperand2.Substring(iPos1 + xOp.Length + 1);
				else
					//third operand is empty
					xOperand3 = "";

				xOperand2 = xOperand2.Substring(0,iPos1);

                //trim last trailing space of second operand
                if (xOperand2.EndsWith(" "))
                    xOperand2 = xOperand2.Substring(0, xOperand2.Length - 1);
            }

			//evaluate character codes
			xOperand1 = EvaluateCharacterCodes(xOperand1, bAllowOldFormat);
			xOperand2 = EvaluateCharacterCodes(xOperand2, bAllowOldFormat);
			xOperand3 = EvaluateCharacterCodes(xOperand3, bAllowOldFormat);

            xOperand1 = RestoreReservedCharacters(xOperand1);
            xOperand2 = RestoreReservedCharacters(xOperand2);
            xOperand3 = RestoreReservedCharacters(xOperand3);
			Trace.WriteNameValuePairs("xOp", xOp, "xOperand1", xOperand1, "xOperand2", xOperand2);

			//return evaluation
			double dblOpnd1 = 0;
			double dblOpnd2 = 0;

			switch(xOp)
			{
				case "^=":
				{
					try
					{
						//attempt to convert these to doubles -
						//if successful, treat operands as numeric
						dblOpnd1 = System.Convert.ToDouble(xOperand1);
						dblOpnd2 = System.Convert.ToDouble(xOperand2);
						oRet = (dblOpnd1 == dblOpnd2);
					}
					catch
					{
						//operands are not numeric - treat as strings
						xOperand1 = xOperand1.ToUpper();
						xOperand2 = xOperand2.ToUpper();
						oRet = (xOperand1 == xOperand2);
					}

					break;
				}
				case "^!=":
				{
					try
					{
						//attempt to convert these to doubles -
						//if successful, treat operands as numeric
						dblOpnd1 = System.Convert.ToDouble(xOperand1);
						dblOpnd2 = System.Convert.ToDouble(xOperand2);
						oRet = (dblOpnd1 != dblOpnd2);
					}
					catch
					{
						//operands are not numeric - treat as strings
						xOperand1 = xOperand1.ToUpper();
						xOperand2 = xOperand2.ToUpper();
						oRet = (xOperand1 != xOperand2);
					}
					break;
				}
				case "^GT":
				{
					try
					{
						//attempt to convert these to doubles -
						//if successful, treat operands as numeric
						dblOpnd1 = System.Convert.ToDouble(xOperand1);
						dblOpnd2 = System.Convert.ToDouble(xOperand2);
						oRet = (dblOpnd1 > dblOpnd2);
					}
					catch
					{
						//operands are not numeric - treat as strings
						xOperand1 = xOperand1.ToUpper();
						xOperand2 = xOperand2.ToUpper();
						oRet = xOperand1.CompareTo(xOperand2) > 0;
					}
					break;
				}
				case "^LT":
				{
					try
					{
						//attempt to convert these to doubles -
						//if successful, treat operands as numeric
						dblOpnd1 = System.Convert.ToDouble(xOperand1);
						dblOpnd2 = System.Convert.ToDouble(xOperand2);
						oRet = (dblOpnd1 < dblOpnd2);
					}
					catch
					{
						//operands are not numeric - treat as strings
						xOperand1 = xOperand1.ToUpper();
						xOperand2 = xOperand2.ToUpper();
						oRet = xOperand1.CompareTo(xOperand2) < 0;
					}
					break;
				}
				case "^GTE":
				{
					try
					{
						//attempt to convert these to doubles -
						//if successful, treat operands as numeric
						dblOpnd1 = System.Convert.ToDouble(xOperand1);
						dblOpnd2 = System.Convert.ToDouble(xOperand2);
						oRet = (dblOpnd1 >= dblOpnd2);
					}
					catch
					{
						//operands are not numeric - treat as strings
						xOperand1 = xOperand1.ToUpper();
						xOperand2 = xOperand2.ToUpper();
						oRet = xOperand1.CompareTo(xOperand2) >= 0;
					}
					break;
				}
				case "^LTE":
				{
					try
					{
						//attempt to convert these to doubles -
						//if successful, treat operands as numeric
						dblOpnd1 = System.Convert.ToDouble(xOperand1);
						dblOpnd2 = System.Convert.ToDouble(xOperand2);
						oRet = (dblOpnd1 <= dblOpnd2);
					}
					catch
					{
						//operands are not numeric - treat as strings
						xOperand1 = xOperand1.ToUpper();
						xOperand2 = xOperand2.ToUpper();
						oRet = xOperand1.CompareTo(xOperand2) <= 0;
					}
					break;
				}
				case "^AND":
				{
					//convert operands to booleans
					bool b1 = false;
					bool b2 = false;

					try
					{
						b1 = String.ToBoolean(xOperand1);
						b2 = String.ToBoolean(xOperand2);
					}
					catch(System.Exception oE)
					{
						//can't be converted - raise error
						throw new LMP.Exceptions.ExpressionException(
							LMP.Resources.GetLangString("Error_InvalidExpression") + xExpression, oE);
					}
					oRet = b1 && b2;
					break;
				}
				case "^OR":
				{
					//convert operands to booleans
					bool b1 = false;
					bool b2 = false;

					try
					{
						b1 = String.ToBoolean(xOperand1);
						b2 = String.ToBoolean(xOperand2);
					}
					catch(System.Exception oE)
					{
						//can't be converted - raise error
						throw new LMP.Exceptions.ExpressionException(
							LMP.Resources.GetLangString("Error_InvalidExpression") + xExpression, oE);
					}
					oRet = b1 || b2;
					break;
				}
				case "^LIKE":
				{
					//ignore case in match
					oRet = Regex.IsMatch(xOperand1, "(?i)" + xOperand2);
					break;
				}
				case "^?":
				{
					//convert operand to boolean
					bool b1 = false;
                    if (string.IsNullOrEmpty(xOperand1))
                    {
                        b1 = false;
                    }
                    else
                    {
                        try
                        {
                            //convert to bool - any non-zero value is true
                            b1 = String.ToBoolean(xOperand1);
                        }
                        catch (System.Exception oE)
                        {
                            //can't be converted - raise error
                            throw new LMP.Exceptions.ExpressionException(
                                LMP.Resources.GetLangString("Error_InvalidExpression") + xExpression, oE);
                        }
                    }
					if(b1)
					{
						oRet = xOperand2 == null ? "" : xOperand2;
					}
					else
					{
						oRet = xOperand3 == null ? "" : xOperand3;
					}
					break;
				}
			}

			LMP.Benchmarks.Print(t0, xExpression);
			return oRet;
		}

        /// <summary>
        /// returns the specified expression with all field codes evaluated
        /// </summary>
        /// <param name="xExp">field code expression</param>
        /// <param name="oMPObject">Segment to be used to evaluate the expression</param>
        /// <param name="oForteDoc">MacPac document to be used to evaluate the expression</param>
        /// <returns></returns>
		private static string EvaluateFieldCodes(string xExpression, XmlSegment oSegment, XmlForteDocument oForteDoc, bool bAllowOldFormat,
            FieldCodeExternalEvaluationRequiredHandler oExternalEvalRequiredDelegate)
		{
            try
            {
                int iStartPos = -1;
                int iEndPos = -1;

                Trace.WriteNameValuePairs("xExpression", xExpression);

                //store original expression in variable
                //that will be getting evaluated
                string xExpEval = xExpression;

                //find opening field code bracket
                int iStartTagPos = xExpEval.ToUpper().IndexOf(XmlFieldCode.mpFieldCodeStartTag);

                while (iStartTagPos > -1)
                {
                    //there's an opening bracket - find closing bracket
                    int iCurPos = iStartTagPos;
                    int iNumUnmatchedBrackets = 1;
                    int iNumBrackets = 1;

                    while (iNumUnmatchedBrackets > 0 || (iStartPos == -1 && iEndPos == -1))
                    {
                        //see if next bracket is an open or close bracket
                        iStartPos = xExpEval.IndexOf(XmlFieldCode.mpFieldCodeStartTag.ToString(), iCurPos + 1);
                        iEndPos = xExpEval.IndexOf(XmlFieldCode.mpFieldCodeEndTag.ToString(), iCurPos + 1);

                        if (iEndPos == -1 || (iEndPos < iCurPos))
                        {
                            //there is an unmatched bracket
                            if (oForteDoc.Mode == XmlForteDocument.Modes.Design)
                            {
                                throw new LMP.Exceptions.ExpressionException(LMP.Resources.GetLangString(
                                    "Error_InvalidExpressionBracketMismatch") + xExpression);
                            }
                            else
                            {
                                //GLOG #4216 - dcf
                                //we're not designing the document -
                                //consider the expression as text -
                                return xExpression;
                            }
                        }
                        else if (iStartPos == -1 || (iStartPos > iEndPos))
                        {
                            //next bracket is closing bracket-
                            //decrement unmatched bracket count
                            iNumUnmatchedBrackets = iNumUnmatchedBrackets - 1;
                            iCurPos = iEndPos;
                            iNumBrackets = iNumBrackets + 1;
                        }
                        else
                        {
                            //next bracket is opening bracket-
                            //increment unmatched bracket count
                            iNumUnmatchedBrackets = iNumUnmatchedBrackets + 1;
                            iCurPos = iStartPos;
                            iNumBrackets = iNumBrackets + 1;
                        }
                    }

                    //if we got here, all open brackets have a close bracket counterpart
                    //and we have a starting and ending position of the field code-
                    string xFieldCode = xExpEval.Substring(iStartTagPos, iEndPos - iStartTagPos + 1);
                    string xCodeWithoutBrackets = xFieldCode.Substring(1, xFieldCode.Length - 2);

                    Trace.WriteNameValuePairs("xFieldCode", xFieldCode);

                    string xFieldCodeMod = xFieldCode;

                    if (xCodeWithoutBrackets.IndexOf(XmlFieldCode.mpFieldCodeStartTag.ToString()) > -1)
                    {
                        //there's nested field code - evaluate by iterating
                        xFieldCodeMod = EvaluateFieldCodes(xCodeWithoutBrackets, oSegment, oForteDoc,
                            bAllowOldFormat, oExternalEvalRequiredDelegate);

                        //reattach brackets
                        xFieldCodeMod = string.Concat(XmlFieldCode.mpFieldCodeStartTag.ToString(),
                            xFieldCodeMod, XmlFieldCode.mpFieldCodeEndTag.ToString());
                    }

                    string xFldCodeModUpper = xFieldCodeMod.ToUpper();

                    if (xFldCodeModUpper != mpIndefiniteArticle.ToUpper() &&
                        xFldCodeModUpper != mpOrdinalSuffix.ToUpper() &&
                        xFldCodeModUpper != mpMyValueCode.ToUpper() &&
                        //GLOG 5551: Avoid error when FieldCode length is less than Substring length
                        (xFldCodeModUpper.Length > 5 && xFldCodeModUpper.Substring(1, 4) != "EXEC") &&
                        (xFldCodeModUpper.Length > 6 && xFldCodeModUpper.Substring(1, 5) != @"\EXEC") &&
                        (xFldCodeModUpper.Length > 5 && xFldCodeModUpper.Substring(1, 4) != "CHAR") &&
                        !(xFldCodeModUpper.StartsWith("[__")) &&
                        !(xFldCodeModUpper.StartsWith("[_") || !bAllowOldFormat))
                    {
                        //field code is not a formatting code, USStateIndefiniteArticle,
                        //OrdinalSuffix, exec code or character code - we evaluate these
                        //either before or after - get value of field code
                        xFieldCodeMod = XmlFieldCode.GetValue(xFieldCodeMod, oSegment, oForteDoc, 
                            bAllowOldFormat,oExternalEvalRequiredDelegate);

                        //GLOG 2151: Replace any reserved operators in evaluated FieldCode with 
                        //Temporary markers, to be switched back at end of Expression.Evaluate
                        xFieldCodeMod = MarkLiteralReservedCharacters(xFieldCodeMod);
                        xFieldCodeMod = ReplaceReservedCharacters(xFieldCodeMod);

                        //replace field code with value
                        xExpEval = xExpEval.Replace(xFieldCode, xFieldCodeMod);
                    }

                    //look for next start tag
                    if (xExpEval == "" || (iStartTagPos + 1 > xExpEval.Length))
                        iStartTagPos = -1;
                    else
                    {
                        try
                        {
                            iStartTagPos = xExpEval.IndexOf(XmlFieldCode.mpFieldCodeStartTag, iStartTagPos + 1);
                        }
                        catch
                        {
                            iStartTagPos = -1;
                        }
                    }
                }

                Trace.WriteNameValuePairs("xExpEval", xExpEval);
                return xExpEval;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.FieldCodeException(
                    LMP.Resources.GetLangString("Error_InvalidExpression") + xExpression, oE);
            }
		}

		/// <summary>
		/// modifies the supplied expression with all indefinite article field codes evaluated - 
		/// works only where the word following the indefinite article is a U.S. state.
		/// </summary>
		/// <param name="xExp">a MacPac expression to be evaluated</param>
		/// <returns></returns>
		private static string EvaluateUSStateIndefiniteArticles(string xExpression)
		{
            string xExpTemp = xExpression;

			Trace.WriteNameValuePairs("xExpression", xExpression);

			int iPos1 = xExpTemp.IndexOf(mpIndefiniteArticle);

			while(iPos1 > -1)
			{
				//evaluate the next found indefinite
				//article- get end of the field code
				string xNextWord = null;
				string xArticle = null;

				int iPos2 = iPos1 + 26;

				string xRemainingExp = xExpTemp.Substring(iPos2).TrimStart();

				Trace.WriteNameValuePairs("xRemainingExp", xRemainingExp);

				//get end of next word - we're expecting this to be a U.S. state
				iPos2 = xRemainingExp.IndexOf(' ');

				if(iPos2 > 0)
					xNextWord = xRemainingExp.Substring(0,iPos2);
				else
					//no trailing space - this must be the end of the expression
					xNextWord = xRemainingExp.Trim();

				Trace.WriteNameValuePairs("xNextWord", xNextWord);

				//get article for US State
				switch(xNextWord.ToUpper())
				{
					case "ALABAMA": 
					case "ALASKA": 
					case "ARIZONA": 
					case "ARKANSAS": 
					case "IDAHO": 
					case "ILLINOIS": 
					case "INDIANA":
					case "IOWA": 
					case "OHIO":
					case "OKLAHOMA": 
					case "OREGON":
						xArticle = "an";
						break;
					default:
						xArticle = "a";
						break;
				}

				if(xExpTemp.Substring(iPos1, 26) == mpIndefiniteArticle.ToUpper())
				{
					//field code was specified as upper case -
					//return value upper case
					xArticle = xArticle.ToUpper();
				}

				//substitute article for field code
				xExpTemp = string.Concat(xExpTemp.Substring(0, iPos1),
					xArticle, xExpTemp.Substring(iPos1 + 26));

				Trace.WriteNameValuePairs("xExpTemp", xExpTemp);
				
				//search for next indefinite article
				iPos1 = xExpTemp.IndexOf(mpIndefiniteArticle.ToUpper());
			}
			return xExpTemp;
		}

		/// <summary>
		/// modifies xExp by evaluating the supplied ordinal suffix
		/// </summary>
		/// <param name="xExp">MacPac expression to be evaluated</param>
		/// <returns></returns>
		private static string EvaluateOrdinalSuffixes(string xExpression)
		{
            string xExp = xExpression;

			Trace.WriteNameValuePairs("xExpression", xExpression);

			int iPos1 = xExp.ToUpper().IndexOf(mpOrdinalSuffix.ToUpper());

			if(iPos1 == 0)
				throw new LMP.Exceptions.ExpressionException(
					LMP.Resources.GetLangString("Error_CantEvaluateOrdinalSuffixCode") + xExp);

			while(iPos1 > -1)
			{
				//evaluate the next found ordinal-
				//get beginning of previous word - we're expecting this to be numeric
				int iPos2 = xExp.LastIndexOf(' ', iPos1 - 1);
				string xNum = null;
				int iNum = 0;

				if(iPos2 > -1)
					xNum = xExp.Substring(iPos2, iPos1 - iPos2).Trim();
				else
					//no trailing space - this must be the end of the expression
					xNum = xExp.Substring(0, iPos1 - 1);

				Trace.WriteNameValuePairs("xNum", xNum);

				//ensure that captured string is numeric
				try
				{
					iNum = System.Convert.ToInt32(xNum);
				}
				catch(System.Exception oE)
				{
					throw new LMP.Exceptions.ExpressionException(
						LMP.Resources.GetLangString(
						"Error_CantEvaluateOrdinalSuffixCode") + xExp,oE);
				}

				//get the suffix
				string xSuffix = LMP.Number.GetOrdinalSuffix(iNum);

				if(xExp.Substring(iPos1, 15) == mpOrdinalSuffix.ToUpper())
				{
					//field code was specified as upper case -
					//return value upper case
					xSuffix = xSuffix.ToUpper();
				}

				//substitute suffix for field code
				xExp = string.Concat(xExp.Substring(0, iPos1), 
					xSuffix, xExp.Substring(iPos1 + 15));

				Trace.WriteNameValuePairs("xExp", xExp);

				//search for next ordinal
				iPos1 = xExp.ToUpper().IndexOf(mpOrdinalSuffix.ToUpper());
			}
			return xExp;
		}

		/// <summary>
		/// returns the first binary expression in the supplied expression
		/// </summary>
		/// <param name="xExp"></param>
		/// <param name="bytLevel"></param>
		/// <returns></returns>
		private static string GetFirstSubExpression(string xExpression, XmlExpression.OperatorPrecedenceLevels bytLevel)
		{
            string xOp = "";
			int iPos1 = -1;
			int iPos2 = -1;

			Trace.WriteNameValuePairs("xExpression", xExpression);

			if(bytLevel == XmlExpression.OperatorPrecedenceLevels.Level2)
			{
				while((xOp != "^AND" && xOp != "^OR") || xOp == "")
				{
					//operator is not in this precendence
					//level - find next operator
					iPos1 = xExpression.IndexOf(" ^", iPos1 + 1);

					if(iPos1 == -1)
					{
						//no more operators - return entire expression
						return xExpression;
					}
					else
					{
						//get end position of operator
						iPos2 = xExpression.IndexOf(" ", iPos1 + 1);

						//parse operator from expression
						if(iPos2 == -1)
							xOp = xExpression.Substring(iPos1).Trim().ToUpper();
						else
							xOp = xExpression.Substring(iPos1, iPos2 - iPos1).Trim().ToUpper();
					}
				}
			}
			else if(bytLevel == XmlExpression.OperatorPrecedenceLevels.Level1)
			{
				string xOps = "^= ^!= ^GTE ^GT ^LTE ^LT ^LIKE";
				while(xOps.IndexOf(xOp) == -1 || xOp == "")
				{
					//operator is not in this precendence level - find next operator
					iPos1 = xExpression.IndexOf("^", iPos1 + 1);

					if(iPos1 == -1)
						//no more operators - return entire expression
						return xExpression;
					else
					{
						//get end position of operator
						iPos2 = xExpression.IndexOf(" ", iPos1 + 1);

						//parse operator from expression
                        if (iPos2 == -1)
                            xOp = xExpression.Substring(iPos1).Trim().ToUpper();
                        else
                            xOp = xExpression.Substring(iPos1, iPos2 - iPos1).Trim().ToUpper();
					}
				}
			}
			else
			{
				while(xOp == "" || (xOp != "^? " && !Regex.IsMatch(xOp, @"\^\? .* \^:")))
				{
					//operator is not in this precendence level -
					//only ^? is - find next operator
					iPos1 = xExpression.IndexOf("^", iPos1 + 1);

					if(iPos1 ==-1)
					{
						//no more operators - return entire expression
						return xExpression;
					}
					else
					{
						//get end position of operator
						iPos2 = xExpression.IndexOf("^: ", iPos1 + 1);
                        
						if(iPos2 == -1)
						{
							//no third operand exists - end of operator is a space
							iPos2 = xExpression.IndexOf(" ", iPos1 + 1);

							//parse operator from expression
							if(iPos2 ==0)
								xOp = xExpression.Substring(iPos1).Trim().ToUpper();
							else
								xOp = xExpression.Substring(iPos1, iPos2 - iPos1).Trim().ToUpper();
						}
						else
							xOp = xExpression.Substring(iPos1, iPos2 - iPos1 + "^:".Length).Trim().ToUpper();
					}
				}
			}

            //get the start of the next operator
            if (iPos2 > -1)
			    iPos2 = xExpression.IndexOf(" ^", iPos2 + 1);

			if(iPos2 == -1)
				//there is no next operator - end of the
				//expression is the end of the sub expression
				iPos2 = xExpression.Length;

			//get start of the previous operator
			int iPos0 = xExpression.LastIndexOf(" ^", iPos1 - 1);

			if(iPos0 > -1)
			{
				//find end of the previous operator
				iPos0 = xExpression.IndexOf(" ", iPos0 + 1);
				
				//get beginning of expression - after space
				iPos0++;
			}
			else
				//there is no previous operator - start of expression
				//is the start of the sub expression
				iPos0 = 0;
			return xExpression.Substring(iPos0, iPos2 - iPos0);
		}

		/// <summary>
		/// returns True iff all field codes in the supplied expression have valid syntax and are supported
		/// </summary>
		/// <param name="xExp">MacPac expression to be tested</param>
		/// <param name="iErrPos">Start position of invalid field code</param>
		/// <returns></returns>
        private static bool FieldCodesAreValid(string xExpression, out int iErrPos)
        {
            return FieldCodesAreValid(xExpression, "", out iErrPos, false);
        }
        /// <summary>
        /// returns True iff all field codes in the supplied expression have valid syntax and are supported
        /// </summary>
        /// <param name="xExp">MacPac expression to be tested</param>
        /// <param name="iErrPos">Start position of invalid field code</param>
        /// <param name="bAllowOld">Allow Old format field codes (single '_' separator)</param>
        /// <returns></returns>
        private static bool FieldCodesAreValid(string xExpression, out int iErrPos, bool bAllowOldFormat)
        {
            return FieldCodesAreValid(xExpression, "", out iErrPos, bAllowOldFormat);
        }
        /// <summary>
        /// returns True iff all field codes in the supplied expression have valid syntax and are supported
        /// </summary>
        /// <param name="xExp">MacPac expression to be tested</param>
        /// <param name="iErrPos">Start position of invalid field code</param>
        /// <param name="xParentExpression">The expression containing the expression that is being evaluated</param>
        /// <param name="bAllowOld">Allow Old format field codes (single '_' separator)</param>
        /// <returns></returns>
        private static bool FieldCodesAreValid(string xExpression, string xParentExpression, out int iErrPos, bool bAllowOldFormat)
        {
			string xCurTag = "";
			string xFieldCode = "";
			int iCurPos = 0;
			int iPos = 0;

			//Start with inner fieldcode if nested
            int iStartTagPos = xExpression.IndexOf(XmlFieldCode.mpFieldCodeStartTag);
			int iEndTagPos = xExpression.IndexOf(XmlFieldCode.mpFieldCodeEndTag);

			//assign default err pos value - might be changed below
			iErrPos = -1;

			if(iStartTagPos + iEndTagPos == -2)
				//no field code markers
				return true;
			else if(iStartTagPos == -1)
			{
				//there must be an end tag but no start tag
				iErrPos = iEndTagPos;
				return false;
			}
			else if(iEndTagPos == -1)
			{
				//there must be a start tag but no end tag
				iErrPos = iStartTagPos;
				return false;
			}

			while(iStartTagPos + iEndTagPos > -2)
			{
				//find the next field code tag
				if((iStartTagPos < iEndTagPos && iStartTagPos > -1) || (iEndTagPos == -1))
				{
					//next field code tag is start tag
					if(xCurTag == XmlFieldCode.mpFieldCodeStartTag.ToString())
					{
                        //Must be a nested fieldcode
                        //Find associated end tag
                        string xNestedExpression = "";
                        int iNestedLevels = 1;
                        int iNestedEndTagPos = iEndTagPos;
                        int iNestedStartTagPos = iStartTagPos;
                        int iNextStartTagPos = -1;
                        do
                        {
                            // Search for Start tags between current start tag and first end tag
                            iNextStartTagPos = xExpression.IndexOf(XmlFieldCode.mpFieldCodeStartTag.ToString(), iNestedStartTagPos + 1,
                                iEndTagPos - iNestedStartTagPos);
                            if (iNextStartTagPos > -1)
                            {
                                iNestedStartTagPos = iNextStartTagPos;
                                iNestedLevels++;
                            }
                        }
                        while (iNextStartTagPos > -1);
                        // Find matching end tags for nested fieldcodes
                        // Current iEndTagPos will belong to innermost fieldcode,
                        // so start there are work outward
                        for (int i = 1; i <= iNestedLevels; i++)
                        {
                            xNestedExpression = xExpression.Substring(iNestedStartTagPos,
                                iNestedEndTagPos - iNestedStartTagPos + 1);
                            try
                            {
                                if (FieldCodesAreValid(xNestedExpression, xExpression, out iErrPos, bAllowOldFormat))
                                {
                                    // Fill space currently occupied by nested Fieldcode with '1's
                                    // So that parent expression can be validated and length remains constant
                                    // Digit is used to match '\d*' or '.*' in RegEx pattern
                                    xExpression = xExpression.Substring(0, iNestedStartTagPos) + new string('1', xNestedExpression.Length) +
                                        xExpression.Substring(iNestedEndTagPos + 1);
                                    // Find next Start Tag working backward
                                    iNestedStartTagPos = xExpression.LastIndexOf(XmlFieldCode.mpFieldCodeStartTag.ToString(),
                                        iNestedStartTagPos - 1);
                                    // Find next End Tag working forward
                                    iNestedEndTagPos = xExpression.IndexOf(XmlFieldCode.mpFieldCodeEndTag.ToString(),
                                        iNestedEndTagPos + 1);
                                    if (iNestedEndTagPos == -1)
                                    {
                                        // Did not find matching end tag for all levels
                                        iErrPos = iNestedStartTagPos;
                                        return false;
                                    }
                                    else
                                    {
                                        iEndTagPos = iNestedEndTagPos;
                                        iCurPos = iNestedStartTagPos;
                                        //GLOG 7399: Test for additional nested FieldCodes within same outer FieldCode
                                        do
                                        {
                                            // Search for Start tags between current start tag and first end tag
                                            iNextStartTagPos = xExpression.IndexOf(XmlFieldCode.mpFieldCodeStartTag.ToString(), iNestedStartTagPos + 1,
                                                iEndTagPos - iNestedStartTagPos);
                                            if (iNextStartTagPos > -1)
                                            {
                                                iNestedStartTagPos = iNextStartTagPos;
                                                iNestedLevels++;
                                            }
                                        }
                                        while (iNextStartTagPos > -1);

                                    }
                                }
                                else
                                    return false;
                            }
                            catch
                            {
                                // Add correct offset for position relative to full string
                                iErrPos = iErrPos + iNestedStartTagPos;
                                return false;
                            }
                        }
					}
					else
					{
						xCurTag = XmlFieldCode.mpFieldCodeStartTag.ToString();
						iCurPos = iStartTagPos;
                    }

                    //get next start tag
                    iStartTagPos = xExpression.IndexOf(XmlFieldCode.mpFieldCodeStartTag.ToString(), iCurPos + 1);
                }
				else if((iStartTagPos > iEndTagPos) || (iStartTagPos == -1))
				{
					//next field code tag is an end tag
					if(xCurTag == XmlFieldCode.mpFieldCodeStartTag.ToString())
					{
						//current tag is start tag - get field code
						xFieldCode = xExpression.Substring(iCurPos + 1, iEndTagPos - iCurPos - 1);

                        if (xFieldCode.ToUpper().StartsWith("AUTHORS_") ||
                            xFieldCode.ToUpper().StartsWith("ADDITIONALAUTHORS_") ||
                            xFieldCode.ToUpper().StartsWith("AUTHORSOFFICE_") ||
                            xFieldCode.ToUpper().StartsWith("ADDITIONALAUTHORSOFFICE_"))
                        {
                            //Authors field code may contain nested expressions
                            if (!OperatorsAreValid(xFieldCode, out iPos))
                            {
                                iErrPos = iCurPos + iPos;
                                return false;
                            }
                            if (!ParensMatch(xFieldCode, out iPos))
                            {
                                iErrPos = iCurPos + iPos;
                                return false;
                            }
                            iPos = xFieldCode.IndexOf(XmlFieldCode.mpFieldCodeStartTag);
                            if (iPos > -1)
                            {
                                //set error position to be the found FieldCode.mpFieldCodeStartTag
                                iErrPos = iCurPos + iPos;
                                return false;
                            }

                            iPos = xFieldCode.IndexOf(XmlFieldCode.mpFieldCodeEndTag);
                            if (iPos > -1)
                            {
                                //set error position to be the found FieldCode.mpFieldCodeEndTag
                                iErrPos = iCurPos + iPos;
                                return false;
                            }
                        }
                        else
                        {
                        //check for reserved characters in field code
						iPos = xFieldCode.IndexOf("^");
						if(iPos > -1)
						{
							//set error position to be the found "^"
							iErrPos = iCurPos + iPos;
							return false;
						}

						iPos = xFieldCode.IndexOf(XmlFieldCode.mpFieldCodeStartTag);
						if(iPos > -1)
						{
							//set error position to be the found FieldCode.mpFieldCodeStartTag
							iErrPos = iCurPos + iPos;
							return false;
						}

						iPos = xFieldCode.IndexOf(XmlFieldCode.mpFieldCodeEndTag);
						if(iPos > -1)
						{
							//set error position to be the found FieldCode.mpFieldCodeEndTag
							iErrPos = iCurPos + iPos;
							return false;
						}

						iPos = xFieldCode.IndexOf(mpExpressionOpenParen);
						if(iPos > -1)
						{
							//set error position to be the found mpExpressionOpenParen
							iErrPos = iCurPos + iPos;
							return false;
						}

						iPos = xFieldCode.IndexOf(mpExpressionCloseParen);
						if(iPos > -1)
						{
							//set error position to be the found mpExpressionCloseParen
							iErrPos = iCurPos + iPos;
							return false;
						}
                        }
                        //skip supported field code validation if the field
                        //code has the form "[_XXX], which indicates a reference to the 
                        //parent field code, and the code is nested in the Authors or AdditionalAuthors field codes
                        string xUpperParent = xParentExpression.ToUpper();
                        //GLOG 3911: Parent expression might also contain a reference target
                        if (!(xFieldCode.StartsWith("_") &&
                            (xUpperParent.StartsWith("[AUTHORS_") ||
                            xUpperParent.StartsWith("[ADDITIONALAUTHORS_") ||
                            xUpperParent.StartsWith("[AUTHORSOFFICE_") ||
                            xUpperParent.StartsWith("[ADDITIONALAUTHORSOFFICE_") ||
                            xUpperParent.Contains("::AUTHORS_") ||
                            xUpperParent.Contains("::ADDITIONALAUTHORS_") ||
                            xUpperParent.Contains("::AUTHORSOFFICE_") ||
                            xUpperParent.Contains("::ADDITIONALAUTHORSOFFICE_"))))
                        {
                            //check if field code is supported
                            if (!FieldCodeIsSupported(xFieldCode) && !(bAllowOldFormat && FieldCodeIsSupportedOLD(xFieldCode)))
                            {
                                //set error position to be the found end brace
                                iErrPos = iCurPos;
                                return false;
                            }
                        }

						xCurTag = XmlFieldCode.mpFieldCodeEndTag.ToString();
						iCurPos = iEndTagPos;
					}
					else
					{
						//can't have two start tags in a row
						iErrPos = iEndTagPos;
						return false;
					}

					//get next end tag
					iEndTagPos = xExpression.IndexOf(XmlFieldCode.mpFieldCodeEndTag.ToString(),iCurPos + 1);
				}
			}
			
			return true;
		}

        private static bool PreferenceFieldCodesNotAlone(string xExpression, out int iErrPos)
        {
            //validate existence of preference expression -
            //author prefs are excluded because they are not set automatically
            bool bPrefCodeNotAlone = (Regex.IsMatch(xExpression,
                @".+\[.*ObjectPreference.*\]|\[.*ObjectPreference.*\].+",
                RegexOptions.IgnoreCase) || Regex.IsMatch(xExpression,
                @".+\[.*TypePreference.*\]|\[.*TypePreference.*\].+",
                RegexOptions.IgnoreCase) || Regex.IsMatch(xExpression,
                @".+\[.*ApplicationPreference.*\]|\[.*ApplicationPreference.*\].+",
                RegexOptions.IgnoreCase));

            if (bPrefCodeNotAlone)
                iErrPos = Regex.Match(xExpression, @"\[.*Preference.*\]").Index;
            else
                iErrPos = 0;

            return bPrefCodeNotAlone;
        }

		/// <summary>
		/// returns True iff specified FieldCode is a legitimate FieldCode
		/// </summary>
		/// <param name="xFieldCode"></param>
		/// <returns></returns>
        //private static bool FieldCodeIsSupported(string xFieldCode)
        //{
        //    return FieldCodeIsSupported(xFieldCode, false);
        //}
        internal static bool FieldCodeIsSupported(string xFieldCode)
        {
            int iPos = xFieldCode.IndexOf("::");

            if(iPos > -1)
            {
                //trim reference qualifier
                xFieldCode = xFieldCode.Substring(iPos + 2);
            }

            if (xFieldCode.Length >= 6 && xFieldCode.Substring(0, 6).ToUpper() == "PARENT")
                xFieldCode = xFieldCode.Substring(0, 6);

            // GLOG : 3129 : JAB
            // All field codes must start with an open square bracket. If
            // this bracket is not present, introduce it.
            if (!xFieldCode.StartsWith("["))
            {
                xFieldCode = "[" + xFieldCode;
            }

            return Regex.IsMatch(xFieldCode, 
                "(?i)" + mpSupportedFieldCodes, RegexOptions.Singleline);
        }

        internal static bool FieldCodeIsSupportedOLD(string xFieldCode)
        {
            int iPos = xFieldCode.IndexOf("::");

            if (iPos > -1)
            {
                //trim reference qualifier
                xFieldCode = xFieldCode.Substring(iPos + 2);
            }

            if (xFieldCode.Length >= 6 && xFieldCode.Substring(0, 6).ToUpper() == "PARENT")
                xFieldCode = xFieldCode.Substring(0, 6);

            // GLOG : 3129 : JAB
            // All field codes must start with an open square bracket. If
            // this bracket is not present, introduce it.
            if (!xFieldCode.StartsWith("["))
            {
                xFieldCode = "[" + xFieldCode;
            }

            return Regex.IsMatch(xFieldCode, 
                    "(?i)" + mpSupportedFieldCodesOLD, RegexOptions.Singleline);
        }
        //internal static bool FieldCodeIsSupported(string xFieldCode)
        //{
        //    // GLOG : 3129 : JAB
        //    // All field codes must start with an open square bracket. If
        //    // this bracket is not present, introduce it.

        //    if (!xFieldCode.StartsWith("["))
        //    {
        //        xFieldCode = "[" + xFieldCode;
        //    }

        //    if (xFieldCode.Length >= 6 && xFieldCode.Substring(0, 6).ToUpper() == "PARENT")
        //        //trim PARENT keyword before searching
        //        return Regex.IsMatch(xFieldCode.Substring(6), "(?i)" + mpSupportedFieldCodes, RegexOptions.Singleline);
        //    else
        //        return Regex.IsMatch(xFieldCode, "(?i)" + mpSupportedFieldCodes, RegexOptions.Singleline);
        //}

        //internal static bool FieldCodeIsSupportedOLD(string xFieldCode)
        //{
        //    // GLOG : 3129 : JAB
        //    // All field codes must start with an open square bracket. If
        //    // this bracket is not present, introduce it.

        //    if (!xFieldCode.StartsWith("["))
        //    {
        //        xFieldCode = "[" + xFieldCode;
        //    }

        //    if (xFieldCode.Length >= 6 && xFieldCode.Substring(0, 6).ToUpper() == "PARENT")
        //        //trim PARENT keyword before searching
        //        return Regex.IsMatch(xFieldCode.Substring(6), "(?i)" + mpSupportedFieldCodesOLD, RegexOptions.Singleline);
        //    else
        //        return Regex.IsMatch(xFieldCode, "(?i)" + mpSupportedFieldCodesOLD, RegexOptions.Singleline);
        //}
        /// <summary>
		/// returns True iff all parens match up - ie there is a
		/// closing paren for each opening paren and visa versa
		/// </summary>
		/// <param name="xExp"></param>
		/// <param name="iErrPos"></param>
		/// <returns></returns>
		private static bool ParensMatch(string xExpression, out int iErrPos)
		{
			int iCurPos = -1;
			int iBraceMismatchCount = 0;

			iErrPos = -1;

			int iOpenBracePos = xExpression.IndexOf(mpExpressionOpenParen.ToString());
			int iCloseBracePos = xExpression.IndexOf(mpExpressionCloseParen.ToString());

			while(iOpenBracePos + iCloseBracePos > -2)
			{
				//find the next brace
				if((iOpenBracePos < iCloseBracePos && iOpenBracePos > -1) || (iCloseBracePos == -1))
				{
					//next brace is opening brace
					iCurPos = iOpenBracePos;
					iBraceMismatchCount++;
				}
				else if((iOpenBracePos > iCloseBracePos) || (iOpenBracePos == -1))
				{
					//next brace is closing brace
					iCurPos = iCloseBracePos;
					iBraceMismatchCount--;
				}

				if(iBraceMismatchCount < 0)
				{
					//more closing braces than opening braces - something's wrong
					iErrPos = iCurPos;
					return false;
				}

				iOpenBracePos = xExpression.IndexOf(mpExpressionOpenParen, iCurPos + 1);
				iCloseBracePos = xExpression.IndexOf(mpExpressionCloseParen, iCurPos + 1);
			}

			if(iBraceMismatchCount != 0)
			{
				//braces don't match up - something's wrong
				iErrPos = xExpression.Length;
				return false;
			}
			else
				return true;
		}

		/// <summary>
		/// returns True iff operators all have space around them
		/// </summary>
		/// <param name="xExp"></param>
		/// <param name="iErrPos"></param>
		/// <returns></returns>
		private static bool OperatorsHaveSpace(string xExpression, out int iErrPos)
		{
			iErrPos = -1;
			int iPos = xExpression.IndexOf("^");

			while(iPos > -1)
			{
				if(iPos == 0)
				{
					//operator can't start an expression
					iErrPos = 1;
					return false;
				}

				//check for either a space before or a "\"
				string xChr = xExpression.Substring(iPos - 1, 1);

				if(xChr != " " && xChr != "\\")
				{
					//operator doesn't have a space or "\"
					iErrPos = iPos;
					return false;
				}

				iPos = xExpression.IndexOf("^", iPos + 1);
			}

			return true;
		}

		/// <summary>
		/// returns True iff the operators in the specified expression are valid
		/// </summary>
		/// <param name="xExp"></param>
		/// <param name="iErrPos"></param>
		/// <returns></returns>
		private static bool OperatorsAreValid(string xExpression, out int iErrPos)
		{
			int iPosOp0End = -1;
			int iPosOp1End = -1;
			string xOperand1 = "";
			string xOperand2 = "";
			int iPosOp1Start = xExpression.IndexOf("^");
			int iPosOp2Start = -1;
			string xOp = "";

			iErrPos = -1;

			while(iPosOp1Start > -1)
			{
				//check for a "\" before the "^"
				string xChr = xExpression.Substring(iPosOp1Start - 1, 1);

				//if character is not "\", the ^ is the
				//beginning of an operator - check validity
				if(xChr != "\\")
				{
					iPosOp1End = xExpression.IndexOf(" ", iPosOp1Start);
					if(iPosOp1End == -1)
					{
                        iPosOp1End = xExpression.Length;
					}

					xOp = xExpression.Substring(iPosOp1Start, iPosOp1End - iPosOp1Start);

					if(mpSupportedOperators.IndexOf("|" + xOp + "|") == -1)
					{
						iErrPos = iPosOp1Start;
						return false;
					}
				}
				else
				{
					//check that there are two operands for this operator-
					//find the start of the next operator
					iPosOp2Start = xExpression.IndexOf("^", iPosOp1End);

					if(xOperand1 == "")
						xOperand1 = xExpression.Substring(iPosOp0End + 1 ,iPosOp1Start - 1).Trim();

					if(xOperand1 == "")
					{
						//no operand 1
						iErrPos = iPosOp1Start;
						return false;
					}
					else
					{
						if(iPosOp2Start == -1)
						{
							//there is no next operator-
							//check from end of operator to end of expression
							xOperand2 = xExpression.Substring(iPosOp1End + 1).Trim();
						}
						else
						{
							//there is a next operator - get operand 2
							xOperand2 = xExpression.Substring(iPosOp1End + 1, 
								iPosOp2Start - iPosOp1End - 1).Trim();
						}

						if(xOperand2 == "" || xOperand2 == mpExpressionOpenParen.ToString() || 
							xOperand2 == mpExpressionCloseParen.ToString())
						{
							iErrPos = iPosOp1End;
							return false;
						}
						else
						{
							//get ready for next iteration of loop
							xOperand1 = xOperand2;
							iPosOp0End = iPosOp1End;
						}
					}
				}

				iPosOp1Start = xExpression.IndexOf("^", iPosOp1Start + 1);
			}

			return true;
		}


		#endregion
	}
}	
	

