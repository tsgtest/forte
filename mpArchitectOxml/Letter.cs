using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using LMP.Data;

namespace LMP.Architect.Oxml
{
    public class XmlLetter : XmlAdminSegment
    {
    }

    public class XmlLetterSignatures : XmlCollectionTable
    {
        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.LetterSignature; }
        }
    }

    public class XmlLetterSignature : XmlCollectionTableItem
    {
        public override mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.LetterSignatures; }
        }

        public override mpObjectTypes ItemType
        {
            get { return mpObjectTypes.LetterSignature; }
        }
    }

    public class XmlLetterSignatureNonTable : XmlAdminSegment
    {
    }

    public class XmlLetterhead : XmlPaper
    {
        // GLOG : 3135 : JAB
        // Provide the ability for the letterhead to update its content.
        public void RefreshContent()
        {
            XmlPrefill oPrefill = this.CreatePrefill();
            XmlSegment.Replace(this.ID, this.ID, this.Parent, this.ForteDocument, oPrefill, null, false);
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// When authors are updated, the letterhead is replaced with the default 
        /// letterhead. This method provides a mechanism by with editted content
        /// can restore their non default letterhead.
        /// </summary>
        /// <param name="xNewID"></param>
        public void RefreshContent(string xNewID)
        {
            XmlPrefill oPrefill = this.CreatePrefill();
            XmlSegment.Replace(xNewID, this.ID, this.Parent, this.ForteDocument, oPrefill, null, false);
        }
    }
}
