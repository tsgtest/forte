using System;
using LMP.Data;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Linq;

namespace LMP.Architect.Oxml
{
    /// <summary>
    /// contains properties that define the arguments for the BlockDeleted and BlockAdded events
    /// </summary>
    public class BlockEventArgs
    {
        private XmlBlock m_oBlock;

        public BlockEventArgs(XmlBlock oBlock)
        {
            m_oBlock = oBlock;
        }

        public XmlBlock Block
        {
            get { return m_oBlock; }
            set { m_oBlock = value; }
        }
    }

    /// <summary>
    /// declares the delegate for a BlockDeleted event handler
    /// </summary>
    public delegate void BlockDeletedHandler(object sender, BlockEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a BlockAdded event handler
    /// </summary>
    public delegate void BlockAddedHandler(object sender, BlockEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a BlockDefinitionChanged event handler
    /// </summary>
    public delegate void BlockDefinitionChangedHandler(object sender, BlockEventArgs oArgs);

    /// <summary>
    /// contains the methods and properties that define a MacPac Block
    /// </summary>
	public class XmlBlock : LMP.Architect.Base.Block
	{
		#region *********************constants*********************
		#endregion
		#region *********************fields*********************
        private XmlSegment m_oSegment;
        private XmlForteDocument m_oForteDocument;
		#endregion
		#region *********************constructors*********************
		internal XmlBlock(XmlSegment oParent)
		{
			m_oSegment = oParent;
		}
        internal XmlBlock(XmlForteDocument oParent)
        {
            m_oForteDocument = oParent;
        }
        #endregion
		#region *********************properties*********************
        public SdtElement AssociatedContentControl { set; get; }
        /// <summary>
		/// returns the Word.XMLNode that is
		/// associated with this block
		/// </summary>
        //public Word.XMLNode AssociatedWordTag
        //{
        //    get
        //    {
        //        if(this.TagID != "")
        //        {
        //            NodeStore oNodes = this.Parent.Nodes;
        //            string xElementName = oNodes.GetItemElementName(this.TagID);
        //            if(xElementName == "mBlock")
        //                return oNodes.GetWordTags(this.TagID)[0];
        //            else
        //                return null;
        //        }
        //        else
        //            return null;
        //    }
        //}

        /// <summary>
        /// returns the content control that is
        /// associated with this block
        /// </summary>
        //public Word.ContentControl AssociatedContentControl
        //{
        //    get
        //    {
        //        if (this.TagID != "")
        //        {
        //            NodeStore oNodes = this.Parent.Nodes;
        //            string xElementName = oNodes.GetItemElementName(this.TagID);
        //            if (xElementName == "mBlock")
        //                return oNodes.GetContentControls(this.TagID)[0];
        //            else
        //                return null;
        //        }
        //        else
        //            return null;
        //    }
        //}

        public XmlSegment Segment
        {
            get{return m_oSegment;}
        }

        public XmlForteDocument ForteDocument
        {
            get
            {
                return this.Segment != null ?
                    m_oSegment.ForteDocument : m_oForteDocument;
            }
        }

        /// <summary>
        /// returns the blocks collection to which this block belongs
        /// </summary>
        public XmlBlocks Parent
        {
            get
            {
                ////return the segment blocks collection
                ////if this block is not standalone, else
                ////the document's blocks collection
                //if (this.IsStandalone)
                //    return this.ForteDocument.Blocks;
                //else
                    return this.Segment.Blocks;
            }
        }

        /// <summary>
        /// returns true iff the block is a standalone block
        /// </summary>
        public bool IsStandalone
        {
            get { return this.Segment == null; }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// inserts mBlock word tag at current selection
        /// </summary>
        /// <param name="range"></param>
        //public Word.XMLNode InsertAssociatedTag(string xTagID, string xObjectData)
        //{
        //    try
        //    {
        //        //get tag for new mBlock tag
        //        xTagID = this.Name;
        //        string xFullTagID = this.m_oSegment.FullTagID + "." + xTagID;
        //        this.TagID = xFullTagID;

        //        //10.2 (dm) assign object database id
        //        this.ObjectDatabaseID = this.m_oSegment.NextObjectDatabaseID;

        //        string xDef = this.ToString();
        //        xObjectData = (xDef).Substring(xDef.IndexOf("|" + this.Name) + 1);

        //        //insert mBlock tag
        //        Word.XMLNode oNewNode = oDoc.InsertmBlockAtSelection(xTagID, xObjectData);

        //        //add tag to collection
        //        m_oSegment.ForteDocument.Tags.Insert(oNewNode, false);

        //        return oNewNode;
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString(
        //            "Error_CouldNotInsertWordTag"), oE);
        //    }

        //}
 
        /// <summary>
        /// inserts mBlock content control at current selection
        /// </summary>
        /// <param name="range"></param>
        //public Word.ContentControl InsertAssociatedContentControl(string xTagID, string xObjectData)
        //{
        //    try
        //    {
        //        //get tag id for new mBlock tag
        //        xTagID = this.Name;
        //        string xFullTagID = this.m_oSegment.FullTagID + "." + xTagID;
        //        this.TagID = xFullTagID;

        //        //get object data
        //        string xDef = this.ToString();
        //        xObjectData = (xDef).Substring(xDef.IndexOf("|" + this.Name) + 1);

        //        //insert mBlock tag
        //        Word.ContentControl oCC = oDoc.InsertmBlockAtSelection_CC(xTagID, xObjectData);

        //        //add tag to collection
        //        m_oSegment.ForteDocument.Tags.Insert_CC(oCC, false);

        //        return oCC;
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString(
        //            "Error_CouldNotInsertWordTag"), oE);
        //    }

        //}
        #endregion
	}

	/// <summary>
	/// contains the methods and properties that manage
	/// a collection of MacPac blocks
	/// </summary>
    public class XmlBlocks : LMP.Architect.Base.Blocks
    {
		#region *********************fields*********************
        private Hashtable m_oDefsByName;
        private Hashtable m_oDefsByTagID;
        private ArrayList m_oDefsByIndex;
        private XmlSegment m_oSegment;
        private XmlForteDocument m_oForteDocument;
        #endregion
		#region *********************constructors*********************
        /// <summary>
        /// creates a blocks collection belonging
        /// to the specified segment
        /// </summary>
        /// <param name="oParent"></param>
        internal XmlBlocks(XmlSegment oParent)
		{
			m_oSegment = oParent;
            this.WPDoc = oParent.WPDoc;

            //populate internal storage
            PopulateStorage();
        }
		#endregion
        #region *********************properties*********************
        public WordprocessingDocument WPDoc { private set; get; }

        public XmlBlock this[int iIndex]
        {
            get { return ItemFromIndex(iIndex); }
        }
        public int Count
        {
            get
            {
                return m_oDefsByName.Count;
            }
        }
        public XmlSegment Segment
        {
            get { return m_oSegment; }
        }
        public XmlForteDocument ForteDocument
        {
            get
            {
                return m_oSegment != null ?
                    m_oSegment.ForteDocument : m_oForteDocument;
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// returns a new block
        /// </summary>
        /// <returns></returns>
        public XmlBlock CreateBlock()
        {
            XmlBlock oBlock = new XmlBlock(this.Segment);
            oBlock.SegmentName = this.Segment.FullTagID;
            return oBlock;
        }

        public XmlBlock ItemFromIndex(int iIndex)
        {
            if (iIndex > m_oDefsByIndex.Count - 1 || iIndex < 0)
            {
                //index is not in range - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemNotInCollection"));
            }

            //get the block at the specified index
            XmlBlock oBlock = (XmlBlock)m_oDefsByIndex[iIndex];

            return oBlock;
        }

        public XmlBlock ItemFromName(string xName)
        {
            XmlBlock oBlock = null;

            Trace.WriteNameValuePairs("xName", xName);

            try
            {
                oBlock = (XmlBlock)m_oDefsByName[xName];
            }
            catch { }

            if (oBlock == null)
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString(
                    "Error_ItemWithIDNotInCollection") + xName);
            }

            return oBlock;
        }

        /// <summary>
        /// returns the block whose associated tag is the specified Word tag
        /// </summary>
        /// <param name="oWordTag"></param>
        /// <returns></returns>
        //public XmlBlock ItemFromAssociatedTag(Word.XMLNode oWordTag)
        //{
        //    XmlBlock oBlock = null;

        //    if (oWordTag.BaseName != "mBlock")
        //    {
        //        //mBlock tags are required for this function
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString("Error_BlockTagRequired"));
        //    }

        //    //get tag id of specified Word tag
        //
        //    string xTagID = oDoc.GetFullTagID(oWordTag);

        //    try
        //    {
        //        //get block from hashtable that maps 
        //        //selection tag names to blocks
        //        oBlock = (XmlBlock)this.m_oDefsByTagID[xTagID];
        //    }
        //    catch { }

        //    if (oBlock == null)
        //        return null;
        //    else
        //        return oBlock;
        //}

        /// <summary>
        /// returns the block whose associated tag is the specified content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        //public XmlBlock ItemFromAssociatedContentControl(Word.ContentControl oCC)
        //{
        //    XmlBlock oBlock = null;

        //    string xTag = oCC.Tag;
        //    if (LMP.String.GetBoundingObjectBaseName(xTag) != "mBlock")
        //    {
        //        //mBlock tags are required for this function
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString("Error_BlockTagRequired"));
        //    }

        //    //get tag id of specified Word tag
        //
        //    string xTagID = oDoc.GetFullTagID_CC(oCC);

        //    try
        //    {
        //        //get block from hashtable that maps 
        //        //selection tag names to blocks
        //        oBlock = (XmlBlock)this.m_oDefsByTagID[xTagID];
        //    }
        //    catch { }

        //    if (oBlock == null)
        //        return null;
        //    else
        //        return oBlock;
        //}

        /// <summary>
        /// saves the specified block to the Word XML Tag
        /// </summary>
        /// <param name="oBlock"></param>
        public void Save(XmlBlock oBlock)
        {
            //DateTime t0 = DateTime.Now;

            //try
            //{
            //    Trace.WriteNameValuePairs("oBlock.Name", oBlock.Name);

            //    //save only if variable is dirty
            //    if (!oBlock.IsDirty)
            //        return;

            //    if (!oBlock.HasValidDefinition)
            //        throw new LMP.Exceptions.DataException(
            //            LMP.Resources.GetLangString(
            //            "Error_InvalidBlockDefinition") + string.Join("|", oBlock.ToArray()));

            //    if (this.BlockExists(oBlock.Name))
            //    {
            //        if (!this.Nodes.NodeExists(oBlock.TagID))
            //        {
            //            //can't save to specified tag - alert
            //            throw new LMP.Exceptions.WordXmlNodeException(
            //                LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + oBlock.TagID);
            //        }

            //        //get new definition 
            //        string xBlock = oBlock.ToString(true);

            //        //set object data for specified Node
            //        this.Nodes.SetItemObjectData(oBlock.TagID, xBlock);

            //        //raise event
            //        this.ForteDocument.RaiseBlockDefinitionChangedEvent(oBlock);
            //    }
            //    else
            //    {
            //        //block is not yet in the collection - this method assumes that
            //        //that the definition for the new block is already in the
            //        //Word XMLNode and the ForteDocument.Tags collection -
            //        //it also assumes that all of the properties of the new block
            //        //have been set - all of this is handled by the front-end

            //        //refresh node store
            //        if (m_oSegment != null)
            //            m_oSegment.RefreshNodes();
            //        else
            //            m_oForteDocument.RefreshOrphanNodes();

            //        //add to internal arrays
            //        m_oDefsByName.Add(oBlock.Name, oBlock);
            //        m_oDefsByTagID.Add(oBlock.TagID, oBlock);
            //        m_oDefsByIndex.Add(oBlock);

            //        //raise event
            //        this.ForteDocument.RaiseBlockAddedEvent(oBlock);
            //    }

            //    //remove dirt
            //    oBlock.IsDirty = false;
            //}
            //catch (System.Exception oE)
            //{
            //    throw new LMP.Exceptions.VariableException(
            //        LMP.Resources.GetLangString("Error_CantSaveBlock") + oBlock.Name, oE);
            //}
            //finally
            //{
            //    LMP.Benchmarks.Print(t0);
            //}
        }

        /// <summary>
        /// returns true if the block with the specified name is in the collection
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public bool BlockExists(string xName)
        {
            XmlBlock oBlock = null;

            Trace.WriteNameValuePairs("xName", xName);

            try
            {
                oBlock = (XmlBlock)m_oDefsByName[xName];
            }
            catch { }

            return (oBlock != null);
        }

        /// <summary>
        /// deletes the specified block and its associated tag
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="bDeleteAssociatedTag"></param>
        public void Delete(XmlBlock oBlock)
        {
            Delete(oBlock, true);
        }

        /// <summary>
        /// deletes the specified block and its associated tag if specified
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="bDeleteAssociatedTag"></param>
        public void Delete(XmlBlock oBlock, bool bDeleteAssociatedTag)
        {
            //DateTime t0 = DateTime.Now;

            //Trace.WriteNameValuePairs("oBlock.Name", oBlock.Name);

            //if (!this.Nodes.NodeExists(oBlock.TagID))
            //{
            //    //can't find specified tag - alert
            //    throw new LMP.Exceptions.WordXmlNodeException(
            //        LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + oBlock.TagID);
            //}

            ////delete tag
            //if (bDeleteAssociatedTag)
            //{
            //    //XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
            //    ////JTS 5/10/10: 10.2
            //    //if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
            //    //{
            //    //    //GLOG 6326 (dm) - delete associated doc var and bookmark
            //    //    Word.ContentControl oCC = oBlock.AssociatedContentControl;
            //    //    LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oCC.Tag,
            //    //        this.ForteDocument.WordDocument, false);

            //    //    oCC.Delete(false);
            //    //}
            //    //else
            //    //{
            //    //    //GLOG 6326 (dm) - delete associated doc var and bookmark
            //    //    Word.XMLNode oTag = oBlock.AssociatedWordTag;
            //    //    Word.XMLNode oReserved = oTag.SelectSingleNode("@Reserved", "", true);
            //    //    if (oReserved != null)
            //    //    {
            //    //        LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oReserved.NodeValue,
            //    //            this.ForteDocument.WordDocument, false);
            //    //    }

            //    //    oTag.Delete();
            //    //}
            //    //XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.None;
            //}

            ////delete the block from the internal arrays
            //DeleteFromInternalArrays(oBlock);

            ////JTS 5/10/10: Not required when being called from RefreshVariables
            //if (bDeleteAssociatedTag)
            //    //GLOG item #4424 and #4425 - dcf
            //    this.ForteDocument.RefreshTags(this.ForteDocument.Mode != XmlForteDocument.Modes.Design);

            ////refresh node store
            //if (m_oSegment != null)
            //    m_oSegment.RefreshNodes();
            //else
            //    this.ForteDocument.RefreshOrphanNodes();

            ////raise event
            //this.ForteDocument.RaiseBlockDeletedEvent(oBlock);

            //LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns a block populated with the supplied definition values
        /// </summary>
        /// <param name="xBlockDef"></param>
        /// <returns></returns>
        public XmlBlock GetBlockFromDefinition(string xBlockDef)
        {
            //trim tag prefix id - added 8/7/08 for GLOG 2937
            int iPos = xBlockDef.IndexOf(LMP.String.mpTagPrefixIDSeparator);
            if (iPos != -1)
            {
                xBlockDef = xBlockDef.Substring(iPos +
                    LMP.String.mpTagPrefixIDSeparator.Length);
            }

            //convert definition to array
            string[] aBlockDef = xBlockDef.Split('|');

            //get Block from array
            return GetBlockFromArray(aBlockDef);
        }
        /// <summary>
        /// gets the definition of the specified block from the deleted
        /// scopes attribute of the specified mSEG
        /// </summary>
        /// <param name="oVar"></param>
        private string GetBlockObjectDataFromDeletedScopes(WordprocessingDocument oDoc,
            SdtElement omSEG, string xBlockName)
        {
            string xDeletedScopes = Query.GetAttributeValue(oDoc, omSEG, "DeletedScopes");
            int iPos1 = xDeletedScopes.IndexOf("DeletedTag=" + xBlockName + "�");
            if (iPos1 != -1)
            {
                int iPos2 = xDeletedScopes.IndexOf("�/w:body>|", iPos1 + 1) + 10;
                string xScope = xDeletedScopes.Substring(iPos1, iPos2 - iPos1);
                iPos1 = xScope.IndexOf("�w:tag w:val=");
                while (iPos1 != -1)
                {
                    iPos1 += 14;
                    iPos2 = xScope.IndexOf('\"', iPos1);
                    string xTag = xScope.Substring(iPos1, iPos2 - iPos1);
                    if (xTag.StartsWith("mpb")) //Limit to mBlocks
                    {
                        string xDef = Query.GetAttributeValue(oDoc, xTag, "ObjectData");
                        xDef = xDef.Trim('|');
                        string[] aDef = xDef.Split('|');
                        if ((aDef.Length > 1) && ((aDef[0] == xBlockName) || (aDef[0].EndsWith(LMP.String.mpTagPrefixIDSeparator + xBlockName))))
                            return xDef;
                        else
                            iPos1 = xScope.IndexOf("�w:tag w:val=", iPos2);
                    }
                    else
                    {
                        iPos1 = xScope.IndexOf("�w:tag w:val=", iPos2);
                    }
                }
            }

            return null;
        }

        #endregion
        #region *********************private members*********************
        private void PopulateStorage()
        {
            DateTime t0 = DateTime.Now;

            m_oDefsByName = new Hashtable();
            m_oDefsByIndex = new ArrayList();
            m_oDefsByTagID = new Hashtable();

            foreach (RawSegmentPart oPart in m_oSegment.RawSegmentParts)
            {
                SdtElement omSEG = oPart.ContentControl;
                //GLOG 8484: Avoid error on non-Forte Content Controls
                SdtElement[] omBlocks = omSEG.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mpb")).ToArray();
                //var omBlocks = from cc in omSEG.Descendants<SdtElement>()
                //               where (from pr in cc.Elements<SdtProperties>()
                //                      where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpb")).Any()
                //                      select pr).Any()
                //               select cc;
                foreach (SdtElement omBlock in omBlocks)
                {
                    //exclude mBlocks contained in child mSEGs
                    if (Query.GetParentSegmentContentControl(omBlock) == omSEG)
                    {
                        string xObjectData = Query.GetAttributeValue(this.WPDoc, omBlock, "ObjectData");
                        if (xObjectData.StartsWith("|"))
                            //trim preceding pipe
                            xObjectData = xObjectData.Substring(1);
                        if (xObjectData.EndsWith("|"))
                            //trim trailing pipe
                            xObjectData = xObjectData.Substring(0, xObjectData.Length - 1);

                        //get and trim tag prefix id
                        string xTagPrefixID = "";
                        int iPos = xObjectData.IndexOf(LMP.String.mpTagPrefixIDSeparator);
                        if (iPos != -1)
                        {
                            xTagPrefixID = xObjectData.Substring(0, iPos);
                            xObjectData = xObjectData.Substring(iPos +
                                LMP.String.mpTagPrefixIDSeparator.Length);
                        }

                        string[] aDef = xObjectData.Split('|');
                        XmlBlock oBlock = this.GetBlockFromArray(aDef);

                        //set runtime properties
                        oBlock.AssociatedContentControl = omBlock;
                        string xTagID = Query.GetAttributeValue(this.WPDoc, omBlock, "TagID");
                        oBlock.TagID = m_oSegment.FullTagID + "." + xTagID;
                        oBlock.SegmentName = m_oSegment.Name;
                        oBlock.TagParentPartNumber = Query.GetAttributeValue(this.WPDoc, omSEG, "PartNumber");
                        oBlock.TagPrefixID = xTagPrefixID;
                        oBlock.IsDirty = false;

                        //10.2 (dm) - set next object database id for segment
                        m_oSegment.NextObjectDatabaseID = oBlock.ObjectDatabaseID + 1;

                        //add to internal arrays
                        m_oDefsByName.Add(oBlock.Name, oBlock);
                        m_oDefsByTagID.Add(oBlock.TagID, oBlock);
                        m_oDefsByIndex.Add(oBlock);
                    }
                }
                //GLOG 8484: Avoid error on non-Forte Content Controls
                SdtElement[] omDels = omSEG.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mpd")).ToArray();
                //var omDels = from cc in omSEG.Descendants<SdtElement>()
                //               where (from pr in cc.Elements<SdtProperties>()
                //                      where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpd")).Any()
                //                      select pr).Any()
                //               select cc;
                foreach (SdtElement omDel in omDels)
                {
                    //exclude mBlocks contained in child mSEGs
                    if (Query.GetParentSegmentContentControl(omDel) == omSEG)
                    {
                        string xmDelObjectData = Query.GetAttributeValue(this.WPDoc, omDel, "ObjectData");
                        string[] aProps = xmDelObjectData.Split('|');
                        if (aProps[0] == m_oSegment.TagID)
                        {
                            //get variable
                            string xName = Query.GetAttributeValue(this.WPDoc, omDel, "TagID");

                            if (m_oDefsByName[xName] == null)
                            {
                                string xObjectData = this.GetBlockObjectDataFromDeletedScopes(this.WPDoc, omSEG, xName);
                                if (xObjectData != null)
                                {
                                    if (xObjectData.StartsWith("|"))
                                        //trim preceding pipe
                                        xObjectData = xObjectData.Substring(1);
                                    if (xObjectData.EndsWith("|"))
                                        //trim trailing pipe
                                        xObjectData = xObjectData.Substring(0, xObjectData.Length - 1);

                                    //get and trim tag prefix id
                                    string xTagPrefixID = "";
                                    int iPos = xObjectData.IndexOf(LMP.String.mpTagPrefixIDSeparator);
                                    if (iPos != -1)
                                    {
                                        xTagPrefixID = xObjectData.Substring(0, iPos);
                                        xObjectData = xObjectData.Substring(iPos +
                                            LMP.String.mpTagPrefixIDSeparator.Length);
                                    }
                                    string[] aDef = xObjectData.Split('|');
                                    XmlBlock oBlock = this.GetBlockFromArray(aDef);

                                    //set runtime properties
                                    oBlock.AssociatedContentControl = omDel;
                                    string xTagID = Query.GetAttributeValue(this.WPDoc, omDel, "TagID");
                                    oBlock.TagID = m_oSegment.FullTagID + "." + xTagID;
                                    oBlock.SegmentName = m_oSegment.Name;
                                    oBlock.TagParentPartNumber = Query.GetAttributeValue(this.WPDoc, omSEG, "PartNumber");
                                    oBlock.TagPrefixID = xTagPrefixID;
                                    oBlock.IsDirty = false;

                                    //10.2 (dm) - set next object database id for segment
                                    m_oSegment.NextObjectDatabaseID = oBlock.ObjectDatabaseID + 1;

                                    //add to internal arrays
                                    m_oDefsByName.Add(oBlock.Name, oBlock);
                                    m_oDefsByTagID.Add(oBlock.TagID, oBlock);
                                    m_oDefsByIndex.Add(oBlock);
                                }
                                else
                                {
                                    //mDel not associated with a Block
                                    continue;
                                }
                            }
                            else
                                //There should not be more than one content control associated with a block
                                continue;
                        }
                    }
                }
            }
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns a block populated with the supplied definition values
        /// </summary>
        /// <param name="aDef"></param>
        /// <returns></returns>
        private XmlBlock GetBlockFromArray(string[] aDef)
        {
            DateTime t0 = DateTime.Now;

            XmlBlock oBlock = null;
            if (m_oSegment != null)
                oBlock = new XmlBlock(m_oSegment);
            else
                oBlock = new XmlBlock(m_oForteDocument);

            try
            {
                oBlock.Name = aDef[0].ToString();
                oBlock.DisplayName = aDef[1].ToString();
                oBlock.TranslationID = Convert.ToInt32(aDef[2]);
                oBlock.ShowInTree = Convert.ToBoolean(aDef[3].ToString());
                oBlock.IsBody = Convert.ToBoolean(aDef[4].ToString());
                oBlock.HelpText = aDef[5].ToString();
                oBlock.Hotkey = aDef[6].ToString();

                // Since this is a newly introduced property it may not be
                // persisted in the xml and the array of properties string 
                // may not contain this property.

                if(aDef.Length < 8)
                {
                    // This property is not in the xml. Set the value accordingly such that
                    // it produces the behaviour exhibitted prior to the introduction of 
                    // this enhancement, ie, show this block in the advanced section.
                    oBlock.DisplayLevel = LMP.Architect.Base.Variable.Levels.Advanced;
                }
                else
                {
                    // Use the value persisted in the xml.

                    try
                    {
                        oBlock.DisplayLevel = (LMP.Architect.Base.Variable.Levels)Convert.ToInt32(aDef[7]);
                    }
                    catch
                    {
                        // Previously this was a boolean. Make a best effort attempt to preserve the
                        // the boolean setting:
                        //      True => Place this block in the basic section. 
                        //      False => place this in the advanced section.
                        //
                        // Revert to the behavior prior to this enhancement should there be an error.
                        // This is also necessary since the type of the Block.Display has changed after
                        // being released.

                        try
                        {
                            bool bShowBlockInBasicSection = Convert.ToBoolean(aDef[7]);
                            oBlock.DisplayLevel = bShowBlockInBasicSection ? 
                                LMP.Architect.Base.Variable.Levels.Basic : LMP.Architect.Base.Variable.Levels.Advanced;
                        }
                        catch
                        {
                            // Revert to the behavior prior to this enhancement should there be an error.
                            oBlock.DisplayLevel = LMP.Architect.Base.Variable.Levels.Advanced;
                        }
                    }
                }
                //GLOG 2165: Set Starting Text if it exists in XML
                if (aDef.Length > 8)
                {
                    oBlock.StartingText = aDef[8].ToString();
                }

                //10.2
                if (aDef.Length > 9 && !string.IsNullOrEmpty(aDef[9]))
                    oBlock.ObjectDatabaseID = Int32.Parse(aDef[9].ToString());
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString(
                        "Error_InvalidBlockDefinition") + oBlock.TagID, oE);
            }

            LMP.Benchmarks.Print(t0);
            return oBlock;
        }

        /// <summary>
        /// deletes the specified block from the internal arrays
        /// </summary>
        /// <param name="oVar"></param>
        private void DeleteFromInternalArrays(XmlBlock oBlock)
        {
            //delete from internal arrays
            m_oDefsByIndex.Remove(oBlock);
            m_oDefsByName.Remove(oBlock.Name);
            m_oDefsByTagID.Remove(oBlock.TagID);
        }
        #endregion
    }
}
