using System;
using Word = Microsoft.Office.Interop.Word;
using System.Xml;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using LMP.Data;
using System.Collections;
using LMP.Controls;

namespace LMP.Architect.Oxml
{
    public class CurrentSegmentSwitchedEventArgs : EventArgs
    {
        public XmlSegment OldSegment;
        public XmlSegment NewSegment;

        public CurrentSegmentSwitchedEventArgs(XmlSegment oOldSegment, XmlSegment oNewSegment)
        {
            this.OldSegment = oOldSegment;
            this.NewSegment = oNewSegment;
        }
    }

	/// <summary>
	/// Summary description for Application.
	/// </summary>
	public class Application
	{
        #region *********************static fields*********************
        private static Word.Application m_oWordApp = null;
        private static int m_iWordVersion = 0;
        #endregion
        #region *********************constructors*********************
        private Application() { }
        #endregion
        #region *********************static properties*********************
        public static Word.Application CurrentWordApp
		{
			get{return m_oWordApp;}
			set{m_oWordApp = value;}
		}
        public static int CurrentWordVersion
        {
            get
            {
                if (m_iWordVersion == 0)
                {
                    //GLOG 5443 (dm) - this may be called before we have Word app
                    if (m_oWordApp == null)
                        m_oWordApp = LMP.fCOMObjects.cWordApp.CurrentWordApplication();

                    //get Word major version number as integer
                    string xVersion = CurrentWordApp.Version;
                    m_iWordVersion = int.Parse(xVersion.Substring(0, xVersion.IndexOf('.')));
                }
                return m_iWordVersion;
            }
		}
		public static LMP.Data.User CurrentUser
		{
			get{return LMP.Data.Application.User;}
        }
        #endregion
        #region *********************static methods*********************
        /// <summary>
        /// returns the datetime format pattern
        /// for the specified Word.Range
        /// </summary>
        /// <param name="oRange"></param>
        /// <returns></returns>
        public static string GetDateFormatPattern(string xDateString)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                string xPattern = "";
                string xCulture = "";

                xDateString = xDateString.Trim();

                //DateTimeFormatInfo object, with info from current culture
                DateTimeFormatInfo o = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat;

                //current culture value
                xCulture = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();

                DateTime oDate;
                //GLOG 4959: If text contains underline characters, use current date
                //for comparison against configured patterns
                if (xDateString.Contains("__"))
                    oDate = DateTime.Now;
                else
                {
                    bool bIsDate = DateTime.TryParse(xDateString, out oDate);


                    if (!bIsDate)
                        //return empty string
                        return string.Empty;
                }
                //array to store datetime patterns from config file
                string[] aPatternsFromConfig = GetPatternsFromConfig(xCulture);

                //array of patterns from current culture 
                string[] aCulturePatterns = o.GetAllDateTimePatterns();

                int iConfigCount = 0;
                int iCultureCount = aCulturePatterns.GetUpperBound(0) + 1;

                if (aPatternsFromConfig != null)
                    iConfigCount = aPatternsFromConfig.GetUpperBound(0) + 1;

                int iCount = iConfigCount + iCultureCount;

                //create new array containing patterns from config and patterns from culture.
                string[] aAllPatterns = new string[iCount];

                //copy culture patterns array into all patterns array
                aCulturePatterns.CopyTo(aAllPatterns, 0);

                //add patterns from config file
                for (int i = 0; i < iConfigCount; i++)
                {
                    aAllPatterns[i + iCultureCount] = aPatternsFromConfig[i];
                }

                CultureInfo oCI = new CultureInfo(xCulture);
                DateTimeFormatInfo oDTFI = oCI.DateTimeFormat;

                //Set patterns with format char
                oDTFI.SetAllDateTimePatterns(aAllPatterns, 'd');

                //all xInput style formats available from config and culture 
                string[] aAllFormats = oDate.GetDateTimeFormats('d', oDTFI);

                //find a match for xInput format and get the pattern
                for (int i = 0; i < aAllFormats.Length; i++)
                {
                    if (xDateString.ToUpper() == aAllFormats[i].ToUpper())
                    {
                        xPattern = aAllPatterns[i];
                        break;
                    }
                }

                LMP.Benchmarks.Print(t0);

                return xPattern;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.FileException(LMP.Resources
                    .GetLangString("Error_InvalidDateFormat"), oE);
            }

        }
        /// <summary>
        /// Tests if input will work as a Date format string
        /// </summary>
        /// <param name="xFormat"></param>
        /// <returns></returns>
        public static bool IsValidDateFormat(string xFormat)
        {
            if (xFormat.ToUpper().EndsWith("/J"))
                //InsertDate Action will handle Jurat formats
                return true;

            //Apply input format to current date
            DateTime oDT = DateTime.Now;

            //remove literal text
            const string QUOTE = "\"";
            while (xFormat.Contains(QUOTE))
            {
                //get first quote position
                int iPos1 = xFormat.IndexOf(QUOTE);
                if (iPos1 > -1)
                {
                    //quote exists - get second quote position
                    int iPos2 = xFormat.IndexOf(QUOTE, iPos1 + 1);

                    if (iPos2 > -1)
                        //parse out literal text
                        xFormat = xFormat.Substring(0, iPos1) + xFormat.Substring(iPos2 + 1);
                    else
                        //replace stray quote
                        xFormat = xFormat.Replace("\"", "");
                }
            }
            //suffix is different in VBA and .NET
            xFormat = xFormat.Replace("am/pm", "tt");

            //6-10-11 (dm) - some strings will cause DateTime.ToString() to err
            string xFormattedDate = null;
            try
            {
                xFormattedDate = oDT.ToString(xFormat);
            }
            catch 
            {
                return false;
            }

            if (xFormattedDate == xFormat)
                //If Formatted Date is identical to Format, date is literal text
                return false;

            //GLOG 4959: Leave underscores in place.  If xFormat matches pattern defined in DB
            //return true, otherwise true as literal text.
            ////Strip out underscores, since these aren't recognized by system as date components
            //xFormattedDate = xFormattedDate.Replace("_", "");
            DateTime oDTTest = DateTime.MinValue;

            //Attempt to apply adjusted format to a date
            //TODO: Since date is eventually formatted on the VB/COM side
            //it seems like that's where we should be testing for validity
            DateTime.TryParse(xFormattedDate, out oDTTest);

            //Value will have changed if Format is valid
            if (oDTTest != DateTime.MinValue)
                return true;
            else
            {
                //GLOG 2647: return true if Format corresponds to a date format defined in DB
                ArrayList oFormatsArray = SimpleDataCollection
                    .GetArray("spDateFormats", null);
                foreach (object[] oFormat in oFormatsArray)
                {
                    //GLOG 5447:  Trim white space from both values being compared,
                    //in case Expression has appended extra spaces
                    string xTest = oFormat[0].ToString().Trim();
                    if (xTest == xFormat.Trim())
                        return true;
                }
               return false;
            }
        }
        /// <summary>
        /// returns a new control of the specified type
        /// </summary>
        /// <param name="iControlType"></param>
        /// <returns></returns>
        public static Control CreateControl(mpControlTypes iControlType, Control oParentControl)
        {
            Control oCtl = null;

            switch (iControlType)
            {
                case mpControlTypes.Textbox:
                    return new LMP.Controls.TextBox();
                case mpControlTypes.MultilineTextbox:
                    return new LMP.Controls.MultilineTextBox();
                case mpControlTypes.Combo:
                    oCtl = new LMP.Controls.ComboBox();
                    ((LMP.Controls.ComboBox)oCtl).LimitToList = false;
                    return oCtl;
                case mpControlTypes.DropdownList:
                    oCtl = new LMP.Controls.ComboBox();
                    ((LMP.Controls.ComboBox)oCtl).LimitToList = true;
                    return oCtl;
                case mpControlTypes.Checkbox:
                    return new LMP.Controls.BooleanComboBox();
                case mpControlTypes.AuthorSelector:
                    return new LMP.Controls.AuthorSelector();
                case mpControlTypes.List:
                    return new LMP.Controls.ListBox();
                case mpControlTypes.MultilineCombo:
                    return new LMP.Controls.MultilineCombo(oParentControl);
                case mpControlTypes.DetailGrid:
                    return new LMP.Controls.Detail();
                case mpControlTypes.RelineGrid:
                    return new LMP.Controls.Reline();
                case mpControlTypes.SalutationCombo:
                    return new LMP.Controls.SalutationCombo();
                case mpControlTypes.DetailList:
                    return new LMP.Controls.DetailList();
                case mpControlTypes.ClientMatterSelector:
                    //return new LMP.Controls.ClientMatterSelector();
                case mpControlTypes.DateCombo:
                    return new LMP.Controls.DateCombo();
                case mpControlTypes.Spinner:
                    return new LMP.Controls.Spinner();
                case mpControlTypes.SegmentChooser:
                    return new LMP.Controls.Chooser();
                case mpControlTypes.PeopleDropdown:
                    return new LMP.Controls.PeopleDropdown();
                case mpControlTypes.Calendar:
                    return new LMP.Controls.DatePicker();
                case mpControlTypes.FontList:
                    return new LMP.Controls.FontList();
                case mpControlTypes.LocationsDropdown:
                    return new LMP.Controls.LocationsDropdown();
                case mpControlTypes.NameValuePairGrid:
                    return new LMP.Controls.NameValuePairGrid();
                case mpControlTypes.JurisdictionChooser:
                    return new LMP.Controls.JurisdictionChooser();
                case mpControlTypes.StartingLabelSelector:
                    return new LMP.Controls.StartingLabelSelector();
                case mpControlTypes.PaperTraySelector:
                    return new LMP.Controls.PaperTraySelector();
                default:
                    return null;
            }
        }
        /// <summary>
        /// sets .Saved=true for the template with the specified name,
        /// if that template is currently open
        /// </summary>
        /// <param name="xTemplate"></param>
        public static void UndirtyTemplateIfNecessary(string xTemplate)
        {
            //if no path specified, use MacPac Templates directory
            if (System.IO.Path.GetDirectoryName(xTemplate) == string.Empty)
            {
                //Use default templates location
                xTemplate = Data.Application.TemplatesDirectory + @"\" + @xTemplate;
            }

            object oTemplateName = xTemplate;
            Word.Template oTemplate = null;
            try
            {
                oTemplate = LMP.fCOMObjects.cWordApp.CurrentWordApplication()
                    .Templates.get_Item(ref oTemplateName);
            }
            catch { }

            if (oTemplate != null)
            {
                oTemplate.Saved = true;
            }
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// gets datetime patterns for culture from 
        /// xml file and returns values in array
        /// </summary>
        /// <param name="xCulture"></param>
        /// <returns></returns>
        private static string[] GetPatternsFromConfig(string xCulture)
        {
            LMP.Data.FirmApplicationSettings oFirmAppSettings = 
                new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);

            string[] aDateFormats = LMP.Data.Application.GetDateFormats();

            //GLOG item #4003
            return aDateFormats;

            //if (aDateFormats != null)
            //{
            //    for (int iRecordIndex = 0; iRecordIndex < aDateFormats.Length; iRecordIndex++)
            //    {
            //        string xCustomDateFormatsRecord = aDateFormats[iRecordIndex];

            //        if (xCustomDateFormatsRecord.StartsWith(xCulture))
            //        {
            //            return xCustomDateFormatsRecord.Substring(
            //                xCustomDateFormatsRecord.IndexOf('|')).Split('|');
            //        }
            //    }
            //}
        }
        #endregion
    }
}
