using System;
using System.Collections;
using System.Reflection;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Linq;
using LMP.Data;

namespace LMP.Architect.Oxml
{
    /// <summary>
    /// declares the delegate for a SegmentRefreshedByAction event handler
    /// </summary>
    public delegate void SegmentRefreshedByActionHandler(object sender, SegmentEventArgs oArgs);
    public delegate void SegmentDistributedSectionsHandler(object sender, SegmentEventArgs oArgs);

	public class XmlVariableAction:XmlActionBase
	{
		#region *********************fields*********************
		int m_iLookupListID = 0;
		XmlVariableActions.Types m_oType = XmlVariableActions.Types.InsertText;
		private XmlVariable m_oVariable;
		#endregion
        #region *********************enumerations*********************
        public enum AlternateFormatTypes
        {
            None = 0,
            Columns = 1, 
            Table = 2
        }
        #endregion
		#region *********************constructors*********************
		internal XmlVariableAction(XmlVariable oVariable)
            :base()
		{
			m_oVariable = oVariable;
			base.m_oSegment = m_oVariable.Segment;
            base.m_oForteDocument = oVariable.ForteDocument;
            //base.m_oDocument = oVariable.ForteDocument.WordDocument;
		}
		#endregion
		#region *********************properties*********************
		/// <summary>
		/// gets/sets the ID of the list used to map
        /// the variable value to another value
		/// </summary>
		public int LookupListID
		{
			get{return m_iLookupListID;}
			set
			{
				if(m_iLookupListID != value)
				{
					m_iLookupListID = value;
					this.IsDirty = true;
				}
			}
		}

		public XmlVariableActions.Types Type
		{
			get{return m_oType;}
			set
			{
				if(m_oType != value)
				{
					m_oType = value;
					this.IsDirty = true;
				}
			}
		}
	
		public override bool IsValid
		{
			get
			{
				return true;
			}
		}

		#endregion
		#region *********************methods*********************
		/// <summary>
		/// returns the VariableAction as an array
		/// </summary>
		/// <returns></returns>
		public override string[] ToArray(bool bIncludeID)
		{
			string[] aAction;

			if(bIncludeID)
			{
				aAction = new string[6];
				aAction[0] = this.ID;
				aAction[1] = this.ExecutionIndex.ToString();
				aAction[2] = this.LookupListID.ToString();
				aAction[3] = this.ExecutionCondition;
				aAction[4] = ((int)this.Type).ToString();
				aAction[5] = this.Parameters;
			}
			else
			{
				aAction = new string[5];
				aAction[0] = this.ExecutionIndex.ToString();
				aAction[1] = this.LookupListID.ToString();
				aAction[2] = this.ExecutionCondition;
				aAction[3] = ((int) this.Type).ToString();
				aAction[4] = this.Parameters;
			}

			return aAction;
		}

        /// <summary>
        /// executes this action
        /// </summary>
        public override void Execute()
        {
            this.Execute(false);
        }

        /// <summary>
        /// executes this action
        /// </summary>
        /// <param name="bIgnoreDeleteScope">
        /// if true, expanded scope will not be deleted when value is empty
        /// </param>
        public void Execute(bool bIgnoreDeleteScope)
		{
			DateTime t0 = DateTime.Now;

			string xParameters = this.Parameters;
			string xValue = m_oVariable.Value;

			Trace.WriteNameValuePairs("xParameters", xParameters);

            //exit if condition for insertion is not satisfied
			if(!ExecutionIsSpecified())
				return;

			//get mapped value if mapping domain
			//(i.e. value set) is specified
			if(this.LookupListID > 0)
				xValue = LMP.Architect.Oxml.XmlExpression.Lookup(
					xValue, this.LookupListID, m_oSegment, m_oForteDocument);

            if(xParameters != "")
			{
                string xParamValue = xValue;

                //GLOG 2151: Mark any reserved Expression characters before evaluating
                //They will treated as the literal characters by the actions
                xParamValue = XmlExpression.MarkLiteralReservedCharacters(xParamValue);

				//evaluate [MyValue] first
                xParameters = XmlFieldCode.EvaluateMyValue(xParameters, xParamValue);
			}

            //7-21-11 (dm) - disable events here rather than in the individual actions -
            //events were disabled for only a portion of each action, which didn't include
            //the call to SetDynamicEditingEnvironment, a potential trigger - additionally,
            //IgnoreWordXMLEvents was being set to None at the end of each action,
            //regardless of the starting value
            XmlForteDocument.WordXMLEvents oEvents = XmlForteDocument.IgnoreWordXMLEvents;
            XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;

            try
            {
                switch (m_oType)
                {
                    case XmlVariableActions.Types.InsertText:
                        InsertText(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case XmlVariableActions.Types.InsertDetail:
                        InsertDetail(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case XmlVariableActions.Types.InsertDetailIntoTable:
                        InsertDetailIntoTable(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case XmlVariableActions.Types.InsertReline:
                        InsertReline(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case XmlVariableActions.Types.InsertTextAsTable:
                        InsertTextAsTable(xValue, xParameters,
                            bIgnoreDeleteScope);
                        break;
                    case XmlVariableActions.Types.ExecuteOnBookmark:
                        ExecuteOnBookmark(xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnDocument:
                        ExecuteOnDocument(xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnApplication:
                        ExecuteOnApplication(xParameters);
                        break;
                    case XmlVariableActions.Types.ReplaceSegment:
                        ReplaceSegment(xParameters);
                        break;
                    case XmlVariableActions.Types.IncludeExcludeText:
                        IncludeExcludeText(xValue, xParameters,
                            bIgnoreDeleteScope);
                        break;
                    //GLOG - 3395 - CEH
                    case XmlVariableActions.Types.IncludeExcludeBlocks:
                        IncludeExcludeBlocks(xValue, xParameters,
                            bIgnoreDeleteScope);
                        break;
                    case XmlVariableActions.Types.SetVariableValue:
                        SetVariableValue(xValue, xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnPageSetup:
                        ExecuteOnPageSetup(xParameters);
                        break;
                    case XmlVariableActions.Types.SetDocVarValue:
                        SetDocVarValue(xValue, xParameters);
                        break;
                    case XmlVariableActions.Types.SetDocPropValue: //GLOG 7495
                        SetDocPropValue(xValue, xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnStyle:
                        ExecuteOnStyle(xParameters);
                        break;
                    case XmlVariableActions.Types.InsertCheckbox:
                        InsertCheckbox(xValue, xParameters);
                        break;
                    case XmlVariableActions.Types.RunMethod:
                        RunMethod(xParameters);
                        break;
                    case XmlVariableActions.Types.RunMacro:
                        RunMacro(xParameters);
                        break;
                    case XmlVariableActions.Types.InsertDate:
                        InsertDate(xValue, xParameters,
                            bIgnoreDeleteScope);
                        break;
                    case XmlVariableActions.Types.SetAsDefaultValue:
                        SetAsDefaultValue(xValue, xParameters);
                        break;
                    case XmlVariableActions.Types.InsertSegment:
                        InsertSegment(xValue, xParameters, bIgnoreDeleteScope);
                        break;
                    case XmlVariableActions.Types.ExecuteOnTag:
                        ExecuteOnTag(m_oVariable, xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnSegment: //GLOG 7229
                        ExecuteOnSegment(xParameters);
                        break;
                    case XmlVariableActions.Types.ExecuteOnBlock:
                        ExecuteOnBlock(xParameters);
                        break;
                    case XmlVariableActions.Types.RunVariableActions:
                        RunVariableActions(xParameters);
                        break;
                    case XmlVariableActions.Types.SetupLabelTable:
                        SetupLabelTable(xValue, xParameters);
                        break;
                    case XmlVariableActions.Types.SetupDetailTable:
                        SetupDistributedDetailTable(xValue, xParameters);
                        break;
                    case XmlVariableActions.Types.SetupDistributedSections:
                        SetupDistributedSegmentSections(xValue, xParameters);
                        break;
                    //case XmlVariableActions.Types.InsertBarCode:
                    //    InsertBarCode(xParameters);
                    //    break;
                    case XmlVariableActions.Types.ApplyStyleSheet:
                        ApplyStyleSheet(xValue, xParameters);
                        break;
                    case XmlVariableActions.Types.SetPaperSource:
                        SetPaperSource(xValue, xParameters);
                        break;
                    //GLOG 3583
                    case XmlVariableActions.Types.UnderlineToLongest:
                        UnderlineTagToLongest(m_oVariable, xParameters);
                        break;
                    case XmlVariableActions.Types.InsertTOA:
                        InsertTOA(xValue, xParameters);
                        break;
                }

                //cycle through associated tags, executing embedded commands
                //TODO: OpenXML rewrite
                //if (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                //{
                //    //xml tags
                //    Word.XMLNode[] aNodes = this.m_oVariable.AssociatedWordTags;
                //    if (aNodes.Length > 0)
                //    {
                //        foreach (Word.XMLNode oNode in aNodes)
                //            LMP.Forte.MSWord.WordDoc.ExecuteEmbeddedCommands(oNode.Range);
                //    }
                //}
                //else
                //{
                    //content controls
                foreach (SdtElement oCC in m_oVariable.AssociatedContentControls)
                {
                    ExecuteEmbeddedCommands(oCC);
                }

                //}
            }
            catch (System.Exception oE)
            {
                //action failed
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_VariableActionFailed") +
                    m_oVariable.Name, oE);
            }
            finally
            {
                //7-21-11 (dm) - restore event handling
                XmlForteDocument.IgnoreWordXMLEvents = oEvents;
            }

			LMP.Benchmarks.Print(t0, 
				Convert.ToString(m_oType), "Variable=" + this.m_oVariable.Name, "Value=" + xValue, "Parameters=" + xParameters);
		}

		/// <summary>
		/// returns true iff execution condition is met or if there is no execution condition
		/// </summary>
		/// <returns></returns>
		public override bool ExecutionIsSpecified()
		{
			string xExecutionCondition = this.ExecutionCondition;

			Trace.WriteNameValuePairs("xExecutionCondition", xExecutionCondition);

			//return true if no execution condition
            if (xExecutionCondition == "")
            {
                ////execute master detail actions only after the segment has been created
                //if (m_oVariable.IsMasterDetailVariable && this.Type == XmlVariableActions.Types.RunVariableActions)
                //{
                //    return m_oVariable.Segment.CreationStatus == Segment.Status.Finished;
                //}
                //else
                //{
                    return true;
                //}
            }

			//determine if there is a [MyValue] code in the execution condition string
			bool bMyValueExists = xExecutionCondition.IndexOf(XmlExpression.mpMyValueCode) > -1;

			//evaluate condition
			if(bMyValueExists)
				//replace all [MyValue] codes with control value
                xExecutionCondition = XmlFieldCode.EvaluateMyValue(
                    xExecutionCondition, m_oVariable.Value.ToString());

			//evaluate resulting execution condition
			string xRet = XmlExpression.Evaluate(xExecutionCondition, 
                m_oSegment, this.m_oVariable.ForteDocument);

			bool bRet = false;

			try
			{
				//convert to boolean
				bRet = String.ToBoolean(xRet);
			}
			catch
			{
				throw new LMP.Exceptions.ArgumentException(
					LMP.Resources.GetLangString("Error_InvalidExecutionCondition") + 
					xExecutionCondition);
			}
			return bRet;
		}
		#endregion
		#region *********************private members*********************
        /// <summary>
        /// inserts xValue at tag containing the definition for this variable -
        /// if xDelimReplacement is not supplied, replaces char 10/13 with char 11 -
        /// xExp overrides xValue if supplied -
        /// applies formatting per any embedded formatting tokens
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">xDelimReplacement, shUnderlineLength, iDeleteScope, xExp</param>
        private void InsertText(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
        {
            string xDelimReplacement = "";
            short shUnderlineLength = 0;
            DeleteScope.DeleteScopeTypes iDeleteScope = DeleteScope.DeleteScopeTypes.Target;
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 4)
                //4 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xDelimReplacement = XmlExpression.Evaluate(aParams[0],
                        m_oSegment, m_oForteDocument);
                    if (xDelimReplacement == "Null")
                        //keyword specifies that line breaks should simply be removed
                        xDelimReplacement = "";
                    else if (xDelimReplacement == "")
                        xDelimReplacement = "\n";

                    shUnderlineLength = short.Parse(XmlExpression.Evaluate(aParams[1],
                        m_oSegment, m_oForteDocument));
                    iDeleteScope = (DeleteScope.DeleteScopeTypes)System.Convert.ToInt32(
                        XmlExpression.Evaluate(aParams[2], m_oSegment, m_oForteDocument));
                    xExpression = aParams[3];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xDelimReplacement",
                xDelimReplacement, "shUnderlineLength", shUnderlineLength, "iDeleteScope",
                iDeleteScope, "xExpression", xExpression);

            if (xExpression != "")
            {
                xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);
            }

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //Word.View oView = m_oForteDocument.WordDocument.ActiveWindow.View;
            //int iShowTags = oView.ShowXMLMarkup;

            //m_oForteDocument.SetDynamicEditingEnvironment();

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph))
                iDeleteScope = DeleteScope.DeleteScopeTypes.Target;

            List<SdtElement> oCCList = m_oVariable.ContainingContentControls;

            string xTagPrefix = Query.GetTag(oCCList[0]).Substring(0, 3);
            bool bIsSegNode = (xTagPrefix == "mps");

            //exit if value is empty and scope is already deleted
            if (bIsSegNode && (xValue == "") && !bIgnoreDeleteScope)
                return;

            bool bExpandedScope = ((iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph) &&
                (bIsSegNode || ((xValue == "") && (shUnderlineLength == 0))));

            //set other argument values - THIS BLOCK MAY NEED WORK
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is XmlCollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }

            //Word.Range oInsertionLocation = null;
            //LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
            object missing = System.Reflection.Missing.Value;

            //insert text
            try
            {
                //Doug (11/25/15) - this is where Restore() would be called - remmed out for now
                //so as no to break anything
                if (bIsSegNode)
                    oCCList = DeleteScope.Restore(m_oVariable, oCCList, iDeleteScope);

                InsertSingleValueText(oCCList, xValue, xDelimReplacement, shUnderlineLength, iDeleteScope,
                    xSegmentXML, m_oVariable.Name, ref bIgnoreDeleteScope, m_oVariable.IsMultiValue);

            }
            catch (System.Exception oE)
            {
                //rethrow
                throw oE;
            }
            if (bExpandedScope)
                m_oSegment.Refresh(Base.Segment.RefreshTypes.NoChildSegments);

        }

        private void InsertDetailXML(List<SdtElement> oCCList, string[] aValues, string xEntityDelimiter,
            short iUnderlineLength, DeleteScope.DeleteScopeTypes iDeleteScope, string xSegmentXml, string xVariableName,
            ref bool bIgnoreDeleteScope)
        {
            List<string> oPartsUpdated = new List<string>();
            bool bEmptyValues = true;
            if (aValues != null)
            {
                for (int i = 0; i < aValues.GetLength(0); i++)
                {
                    if (aValues[i] != "")
                    {
                        bEmptyValues = false;
                        break;
                    }
                }
            }
            //GLOG 15777: If any item in array is non-empty, need to restore CCs for this variable
            //GLOG 15791
            if ((!bEmptyValues || bIgnoreDeleteScope) && (m_oVariable.IsMultiValue || Query.GetBaseName(oCCList[0]) == "mSEG"))
            {
                oCCList = DeleteScope.Restore(m_oVariable, oCCList, iDeleteScope);
            }
            for (int i = 0; i < oCCList.Count; i++)
            {
                SdtElement oCC = oCCList[i];
                string xTagPrefix = Query.GetTag(oCC).Substring(0, 3);
                //GLOG 8897
                if ((aValues == null || aValues.GetLength(0) == 0 || (aValues.GetUpperBound(0) >= i && aValues[i] == "")) && iUnderlineLength == 0 &&
                    (iDeleteScope != DeleteScope.DeleteScopeTypes.Target) && !bIgnoreDeleteScope)
                {
                    if (xTagPrefix != "mps")
                    {
                        DeleteScope.Delete(m_oVariable, oCC, iDeleteScope, ref oPartsUpdated);
                    }
                }
                else
                {
                    List<string> omDels = DeleteScope.GetChildmDels(this.m_oForteDocument.WPDocument, oCC);
                    if (aValues != null)
                    {
                        if (aValues.GetLength(0) > oCCList.Count)
                        {
                            //insert
                            this.m_oForteDocument.SetDetail(oCC, aValues, xEntityDelimiter);
                        }
                        else if (i <= aValues.GetUpperBound(0))
                        {
                            string[] aValue = new string[] { aValues[i] };
                            this.m_oForteDocument.SetDetail(oCC, aValue, xEntityDelimiter);
                        }

                        //add space if specified and needed
                        if (iDeleteScope == DeleteScope.DeleteScopeTypes.WithPrecedingSpace)
                        {
                            //get previous adjacent run if one exists -
                            //will be null if previous sibling is not a run
                            Run oAdjacentRun = oCC.PreviousSibling() as Run;

                            if (oAdjacentRun != null)
                            {
                                //get text element of run
                                Text oText = oAdjacentRun.GetFirstChild<Text>();

                                //add space to end of text if space is missing
                                if (!oText.Text.EndsWith(" "))
                                    oText.Text = oAdjacentRun.InnerText + " ";
                            }
                        }
                        else if (iDeleteScope == DeleteScope.DeleteScopeTypes.WithTrailingSpace)
                        {
                            //get subsequent adjacent run if one exists -
                            //will be null if subsequent sibling is not a run
                            Run oAdjacentRun = oCC.NextSibling() as Run;

                            if (oAdjacentRun != null)
                            {
                                //get text element of run
                                Text oText = oAdjacentRun.GetFirstChild<Text>();

                                //add space to end of text if space is missing
                                if (!oText.Text.StartsWith(" "))
                                    oText.Text = " " + oAdjacentRun.InnerText;
                            }
                        }

                        //        //execute instructions embedded into the text
                        //        ExecuteEmbeddedCommands oRng
                    }
                    else if (iUnderlineLength > 0)
                    {
                        //insert underline
                        if (iUnderlineLength == -1)
                        {
                            //insert underlined tab
                            this.m_oForteDocument.SetText(oCC, "\t");
                            SdtContentRun o = oCC.GetFirstChild<SdtContentRun>();
                            Run oRun = o.GetFirstChild<Run>();
                            RunProperties oRunProps = oRun.GetFirstChild<RunProperties>();
                        }
                        else
                        {
                            //insert the specified number of hard spaces, then underline
                            this.m_oForteDocument.SetText(oCC, new string((char)160, iUnderlineLength));
                        }
                    }
                    if (omDels != null)
                    {
                        DeleteScope.RestoreChildmDels(m_oSegment.WPDoc, oCC, omDels);
                    }
                }
            }
        }
        private void InsertSingleValueText(List<SdtElement> oCCList, string xValue, string xDelimiterReplacement,
            short iUnderlineLength, DeleteScope.DeleteScopeTypes iDeleteScope, string xSegmentXml, string xVariableName,
            ref bool bIgnoreDeleteScope)
        {
            InsertSingleValueText(oCCList, xValue, xDelimiterReplacement, iUnderlineLength, iDeleteScope, xSegmentXml,
                xVariableName, ref bIgnoreDeleteScope, false);
        }
        private void InsertSingleValueText(List<SdtElement> oCCList, string xValue, string xDelimiterReplacement,
            short iUnderlineLength, DeleteScope.DeleteScopeTypes iDeleteScope, string xSegmentXml, string xVariableName,
            ref bool bIgnoreDeleteScope, bool bIsMultiValue)
        {
            List<string> oPartsUpdated = new List<string>();
            string[] aValues = null;
            //GLOG 15834: MultiValue items will be separated by '�'
            if (bIsMultiValue)
            {
                aValues = xValue.Split(StringArray.mpEndOfValue);
            }
            for (int iIndex = 0; iIndex < oCCList.Count; iIndex++)
            {
                SdtElement oCC = oCCList[iIndex];
                if (aValues != null)
                {
                    if (iIndex <= aValues.GetUpperBound(0))
                    {
                        xValue = aValues[iIndex];
                    }
                    else
                    {
                        xValue = "";
                    }
                }
                if (string.IsNullOrEmpty(xValue) && iUnderlineLength == 0 &&
                    (iDeleteScope != DeleteScope.DeleteScopeTypes.Target) && !bIgnoreDeleteScope)
                {
                    DeleteScope.Delete(m_oVariable, oCC, iDeleteScope, ref oPartsUpdated);
                }
                else
                {
                    List<string> omDels = DeleteScope.GetChildmDels(m_oForteDocument.WPDocument, oCC);
                    if (!string.IsNullOrEmpty(xValue))
                    {
                        //replace returns with specified delimiter
                        xValue = xValue.Replace("\v", xDelimiterReplacement);
                        xValue = xValue.Replace("\r\n", xDelimiterReplacement);

                        //insert
                        this.m_oForteDocument.SetText(oCC, xValue);

                        //add space if specified and needed
                        if (iDeleteScope == DeleteScope.DeleteScopeTypes.WithPrecedingSpace)
                        {
                            //get previous adjacent run if one exists -
                            //will be null if previous sibling is not a run
                            Run oAdjacentRun = oCC.PreviousSibling() as Run;

                            if (oAdjacentRun != null)
                            {
                                //get text element of run
                                Text oText = oAdjacentRun.GetFirstChild<Text>();

                                //add space to end of text if space is missing
                                if(!oText.Text.EndsWith(" "))
                                    oText.Text = oAdjacentRun.InnerText + " ";
                            }
                        }
                        else if (iDeleteScope == DeleteScope.DeleteScopeTypes.WithTrailingSpace)
                        {
                            //get subsequent adjacent run if one exists -
                            //will be null if subsequent sibling is not a run
                            Run oAdjacentRun = oCC.NextSibling() as Run;

                            if (oAdjacentRun != null)
                            {
                                //get text element of run
                                Text oText = oAdjacentRun.GetFirstChild<Text>();

                                //add space to end of text if space is missing
                                if (!oText.Text.StartsWith(" "))
                                    oText.Text = " " + oAdjacentRun.InnerText;
                            }
                        }

                    //        //execute instructions embedded into the text
                    //        ExecuteEmbeddedCommands oRng
                    }
                    else if (iUnderlineLength > 0)
                    {
                        //insert underline
                        if (iUnderlineLength == -1)
                        {
                            //insert underlined tab
                            this.m_oForteDocument.SetText(oCC, "\t");
                            SdtContentRun o = oCC.GetFirstChild<SdtContentRun>();
                            Run oRun = o.GetFirstChild<Run>();
                            RunProperties oRunProps = oRun.GetFirstChild<RunProperties>();
                        }
                        else
                        {
                            //insert the specified number of hard spaces, then underline
                            this.m_oForteDocument.SetText(oCC, new string((char)160, iUnderlineLength));
                        }
                    }
                    else
                    {
                        this.m_oForteDocument.SetText(oCC, "");
                    }
                    if (omDels != null)
                    {
                        DeleteScope.RestoreChildmDels(m_oSegment.WPDoc, oCC, omDels);
                    }
                }
            }

        }
        private string[] GetReLineSubVarXML(string xText, string xLabelText, string xSpecialDelimiter, string xSpecialSeparator, ref short shFormat, bool bFormatSmartQuotes)
        {
            string xStandardXML = "";
            string xSpecialXML = "";
            string xPrefill = "";
            string xID = "";
            string xTag = "";
            bool bBold = false;
            bool bItalic = false;
            bool bUnderline = false;

            XmlDocument oXMLSource = new System.Xml.XmlDocument();
            //Convert Reline Control Value to XML Nodes
            oXMLSource.LoadXml(string.Concat("<zzmpD>", xText, "</zzmpD>"));

            //select detail nodes in XML
            XmlNodeList oNodeList = oXMLSource.DocumentElement.SelectNodes("/zzmpD/*");
            ArrayList aDocVars = new ArrayList();
            XmlNode oAtt = null;

            Run oStandardRun = new Run();
            Run oSpecialRun = new Run();
            foreach (XmlNode oNode in oNodeList)
            {
                switch (oNode.Name.ToUpper())
                {
                    case "STANDARD":
                        xStandardXML = oNode.InnerText;
                        //JTS NOTE:  API code would get appropriate SmartQuotes setting from Word.
                        //Since that's not available here, we're just defaulting to using Smart Quotes.
                        //We may want to add a new Admin option to control this
                        if (bFormatSmartQuotes)
                        {
                            xStandardXML = LMP.String.ReplaceQuotesWithSmartQuotes(xStandardXML);
                            xStandardXML = LMP.String.RestoreXMLChars(xStandardXML);
                        }

                        //If standard Re line is not blank, build appropriate XML
                        if (xStandardXML != "")
                        {
                            oAtt = null;
                            try
                            {
                                oAtt = oNode.Attributes.GetNamedItem("Format");
                            }
                            catch { }
                            if (oAtt != null)
                                shFormat = (short)(shFormat | short.Parse(oAtt.Value));
                            LMP.Controls.mpRelineFormatOptions iFormat = (LMP.Controls.mpRelineFormatOptions)shFormat;
                            // Get selected formatting options to build appropriate w:rPr tags
                            bBold = (iFormat & LMP.Controls.mpRelineFormatOptions.RelineBold) == LMP.Controls.mpRelineFormatOptions.RelineBold;
                            bItalic = (iFormat & LMP.Controls.mpRelineFormatOptions.RelineItalic) == LMP.Controls.mpRelineFormatOptions.RelineItalic;
                            bUnderline = (iFormat & LMP.Controls.mpRelineFormatOptions.RelineUnderline) == Controls.mpRelineFormatOptions.RelineUnderline;

                            Run oLabelRun = new Run();
                            if (xLabelText != "")
                            {
                                oLabelRun = new Run(new Text(xLabelText) { Space = SpaceProcessingModeValues.Preserve });
                                //Bold and Italic get applied to label
                                if (bBold || bItalic)
                                {
                                    RunProperties oProps = new RunProperties();
                                    if (bBold)
                                        oProps.Append(new Bold());
                                    if (bItalic)
                                        oProps.Append(new Italic());
                                    oLabelRun.PrependChild<RunProperties>(oProps);
                                }
                            }
                            //Create separate Text elements for each line, separated by Breaks
                            xStandardXML = xStandardXML.Replace("\r\n", "\v");
                            string[] aLines = xStandardXML.Split('\v');
                            Run oTextRun = new Run();
                            for (int i = 0; i < aLines.GetLength(0); i++)
                            {
                                oTextRun.AppendChild<Text>(new Text(aLines[i]));
                                if (i < aLines.GetUpperBound(0))
                                {
                                    oTextRun.AppendChild<Break>(new Break());
                                }
                            }
                            //Bold, Italic or Underline get applied to text
                            if (bBold || bItalic || bUnderline)
                            {
                                RunProperties oProps = new RunProperties();
                                if (bBold)
                                    oProps.Append(new Bold());
                                if (bItalic)
                                    oProps.Append(new Italic());
                                if (bUnderline)
                                    oProps.Append(new Underline() { Val = UnderlineValues.Single });
                                oTextRun.PrependChild<RunProperties>(oProps);
                            }
                            xID = Query.GenerateDocVarID(this.m_oSegment.WPDoc);
                            xTag = "mpu" + xID + new string('0', 28);
                            //Create Doc var for subvar
                            Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "Name", oNode.Name, false, "");
                            Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "ObjectData", string.Format("��Format={0}�Default={1}", shFormat.ToString(), xPrefill), false, "");

                            if (oLabelRun.HasChildren)
                                oStandardRun.AppendChild<Run>(oLabelRun);

                            //Use Doc Var ID for bookmark Id to avoid conflicts
                            oStandardRun.Append(new SdtRun(new SdtProperties(new Tag { Val = xTag }), new SdtContentRun(new BookmarkStart { Id = xID, Name = "_" + xTag }, oTextRun,
                                new BookmarkEnd { Id = xID })));
                        }
                        break;
                    case "SPECIAL":
                        xSpecialXML = oNode.InnerXml;
                        XmlDocument oSpecialXML = new XmlDocument();
                        oSpecialXML.LoadXml(string.Concat("<zzmpS>", xSpecialXML, "</zzmpS>"));
                        XmlNodeList oSpecialNodes = oSpecialXML.SelectNodes("/zzmpS[Value!='']");
                        // Only build special XML if all values are not blank
                        if (oSpecialNodes.Count > 0)
                        {
                            Run oDetailsRun = new Run();
                            oAtt = null;
                            try
                            {
                                oAtt = oNode.Attributes.GetNamedItem("Format");
                            }
                            catch { }
                            if (oAtt != null)
                                shFormat = (short)(shFormat | short.Parse(oAtt.Value));
                            LMP.Controls.mpRelineFormatOptions iFormat = (LMP.Controls.mpRelineFormatOptions)shFormat;
                            // Get selected formatting options to build appropriate w:rPr tags
                            bBold = ((iFormat & LMP.Controls.mpRelineFormatOptions.SpecialBold) == LMP.Controls.mpRelineFormatOptions.SpecialBold);
                            bItalic = (iFormat & LMP.Controls.mpRelineFormatOptions.SpecialItalic) == LMP.Controls.mpRelineFormatOptions.SpecialItalic;
                            bUnderline = (iFormat & LMP.Controls.mpRelineFormatOptions.SpecialUnderline) == Controls.mpRelineFormatOptions.SpecialUnderline;

                            Run oLabelRun = new Run();
                            if (xLabelText != "" && !oStandardRun.HasChildren)
                            {
                                //Label hasn't already been inserted
                                oLabelRun = new Run(new Text(xLabelText) { Space = SpaceProcessingModeValues.Preserve });
                                //Bold and Italic get applied to label
                                if (bBold || bItalic)
                                {
                                    RunProperties oLabelProps = new RunProperties();
                                    if (bBold)
                                        oLabelProps.Append(new Bold());
                                    if (bItalic)
                                        oLabelProps.Append(new Italic());
                                    oLabelRun.PrependChild<RunProperties>(oLabelProps);
                                }
                            }

                            oSpecialNodes = oSpecialXML.SelectNodes("/zzmpS/*");
                            RunProperties oProps = new RunProperties();
                            //Bold, Italic or Underline get applied to text
                            if (bBold || bItalic || bUnderline)
                            {
                                if (bBold)
                                    oProps.Append(new Bold());
                                if (bItalic)
                                    oProps.Append(new Italic());
                                if (bUnderline)
                                    oProps.Append(new Underline() { Val = UnderlineValues.Single });
                            }
                            for (int i = 0; i < oSpecialNodes.Count; i++)
                            {
                                XmlNode oSpecialNode = oSpecialNodes[i];
                                if (oSpecialNode.Name.ToUpper() == "LABEL" || oSpecialNode.Name.ToUpper() == "VALUE")
                                {
                                    string xSubVarText = oSpecialNode.InnerText;
                                    if (bFormatSmartQuotes)
                                    {
                                        xSubVarText = LMP.String.ReplaceQuotesWithSmartQuotes(xSubVarText);
                                        LMP.String.ReplaceXMLChars(xSubVarText);
                                    }
                                    string xCurDelimiter = "";
                                    if (oSpecialNode.Name.ToUpper() == "LABEL")
                                        xCurDelimiter = xSpecialSeparator;
                                    else
                                        xCurDelimiter = xSpecialDelimiter;

                                    Run oTextRun = new Run();
                                    oTextRun.AppendChild<RunProperties>((RunProperties)oProps.Clone());
                                    oTextRun.AppendChild<Text>(new Text(xSubVarText));

                                    Run oDelimRun = new Run();
                                    oDelimRun.AppendChild<RunProperties>((RunProperties)oProps.Clone());
                                    switch (xCurDelimiter)
                                    {
                                        case ("\r"):
                                            oDelimRun.AppendChild<Paragraph>(new Paragraph());
                                            break;
                                        case ("\v"):
                                            oDelimRun.AppendChild<Break>(new Break());
                                            break;
                                        case ("\r\t"):
                                            oDelimRun.Append(new Paragraph(), new Run(new TabChar()));
                                            break;
                                        default:
                                            //GLOG 15829: Allow for spaces in separator
                                            oDelimRun.AppendChild<Text>(new Text(xCurDelimiter) { Space = SpaceProcessingModeValues.Preserve });
                                            break;
                                    }

                                    xID = Query.GenerateDocVarID(this.m_oSegment.WPDoc);
                                    xTag = "mpu" + xID + new string('0', 28);
                                    //Create Doc var for subvar
                                    Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "Name", oSpecialNode.Name, false, "");
                                    Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "ObjectData", string.Format("Index={0}���", (i + 1).ToString()), false, "");
                                    if (i == 0)
                                    {
                                        //add temp id for post-injunction retagging
                                        Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "TempID", "1", false, "");
                                    }

                                    //Use Doc Var ID for bookmark Id to avoid conflicts
                                    //SdtRun oSubRun = new SdtRun(new SdtProperties(new Tag { Val = xTag }), new SdtContentRun(new BookmarkStart { Id = xID, Name = "_" + xTag }, oTextRun,
                                    //    new BookmarkEnd { Id = xID }));
                                    SdtRun oSubRun = new SdtRun(new SdtProperties(new Tag { Val = xTag }), new SdtContentRun(new BookmarkStart { Id = xID, Name = "_" + xTag }, oTextRun,
                                        new BookmarkEnd { Id = xID }));
                                    oDetailsRun.AppendChild<SdtRun>(oSubRun);
                                    if (i < oSpecialNodes.Count - 1)
                                        oDetailsRun.AppendChild<Run>(oDelimRun);
                                }
                            }
                            if (oDetailsRun.HasChildren)
                            {
                                xID = Query.GenerateDocVarID(this.m_oSegment.WPDoc);
                                xTag = "mpu" + xID + new string('0', 28);
                                //Create Doc var for Special Re Line subvar
                                Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "Name", oNode.Name, false, "");
                                Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "ObjectData", string.Format("��Format={0}�Default={1}", shFormat.ToString(), xPrefill), false, "");
                                //add temp id for post-injunction retagging
                                Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "TempID", "2", false, "");
                                SdtContentRun oSpecialContent = new SdtContentRun(new BookmarkStart { Id = xID, Name = "_" + xTag });
                                for (int i = 0; i < oDetailsRun.ChildElements.Count; i++)
                                {
                                    oSpecialContent.Append((OpenXmlElement)(oDetailsRun.ChildElements[i].Clone()));
                                }
                                oSpecialContent.Append(new BookmarkEnd { Id = xID });

                                oSpecialRun.Append(new SdtRun(new SdtProperties(new Tag { Val = xTag }),  oSpecialContent));
                                if (oLabelRun.HasChildren)
                                    oSpecialRun.PrependChild<Run>(oLabelRun);
                                LMP.Trace.WriteNameValuePairs("oSpecialRun", oSpecialRun.InnerXml);
                            }
                        }
                        break;
                }
            }
            //GLOG 15829: Tab character needs to be in a separate run
            oSpecialRun.InnerXml = oSpecialRun.InnerXml.Replace("\t</w:t>", "</w:t></w:r><w:r><w:tab/>");
            if (oStandardRun.HasChildren && oSpecialRun.HasChildren)
            {
                return new string[] { oStandardRun.InnerXml, oSpecialRun.InnerXml };
            }
            else if (oStandardRun.HasChildren)
            {
                return new string[] { oStandardRun.InnerXml };
            }
            else if (oSpecialRun.HasChildren)
            {
                return new string[] { oSpecialRun.InnerXml };
            }
            else
                return null;
        }
        private string[] GetDetailSubVarXML(string xText, string xTemplate, string xDetailItemSeparator, bool bFormatSmartQuotes)
        {
            string xSubVarText = "";
            string xID = "";
            string xTag = "";
            string xPrecedingText = "";
            ArrayList aTemplateElements = new ArrayList();
            int iIndex = 0;
            string xItemSeparator = "";
            ArrayList aDetailXML = new ArrayList();

            XmlDocument oXMLSource = new System.Xml.XmlDocument();
            //Convert Reline Control Value to XML Nodes
            oXMLSource.LoadXml(string.Concat("<zzmpD>", xText, "</zzmpD>"));

            //select detail nodes in XML
            XmlNodeList oNodeList = oXMLSource.DocumentElement.SelectNodes("/zzmpD/*");
            ArrayList aDocVars = new ArrayList();

            if (xTemplate != "")
            {
                int iPos = xTemplate.IndexOf("[SubVar_", StringComparison.CurrentCultureIgnoreCase);
                if (iPos > 0)
                {
                    xPrecedingText = xTemplate.Substring(0, iPos);
                }
                while (iPos > -1)
                {
                    int iEnd = xTemplate.IndexOf("]", iPos);
                    if (iEnd > -1)
                    {
                        int iStart = iPos + 8;
                        string xCurElement = xTemplate.Substring(iStart, iEnd - iStart);
                        xCurElement = xCurElement.TrimStart('_');
                        iPos = xTemplate.IndexOf("[SubVar_", iEnd, StringComparison.CurrentCultureIgnoreCase);
                        string xSep = "";
                        if (iPos > iEnd + 1)
                        {
                            xSep = xTemplate.Substring(iEnd + 1, iPos - (iEnd + 1));
                        }
                        else if (iEnd + 1 < xTemplate.Length)
                        {
                            xSep = xTemplate.Substring(iEnd + 1);
                        }

                        aTemplateElements.Add(new string[] { xCurElement, xSep });
                    }
                }
            }
            iIndex = -1;
            int iCurIndex = -1;
            do
            {
                foreach (XmlNode oNode in oNodeList)
                {
                    iCurIndex = Int32.Parse(oNode.Attributes.GetNamedItem("Index").Value);
                    if (iCurIndex > iIndex)
                    {
                        break;
                    }
                }
                if (iCurIndex > iIndex)
                {
                    iIndex = iCurIndex;

                    XmlNodeList oElementNodes = oXMLSource.DocumentElement.SelectNodes("//zzmpD/*[@Index=" + iCurIndex.ToString() + "]");
                    Run oDetailRun = new Run();
                    int iElements = aTemplateElements.Count;
                    for (int iCounter = 0; iCounter < Math.Max(iElements, 1); iCounter++)
                    {
                        if (iCounter == 0)
                        {
                            xItemSeparator = xPrecedingText;
                        }
                        else
                        {
                            xItemSeparator = xDetailItemSeparator;
                        }
                        foreach (XmlNode oNode in oElementNodes)
                        {
                            if (xTemplate == "" || oNode.Name.ToUpper() == ((string[])aTemplateElements[iCounter])[0].ToUpper())
                            {
                                xSubVarText = oNode.InnerText;
                                //GLOG 8772: Skip empty nodes
                                if (string.IsNullOrEmpty(xSubVarText))
                                {
                                    continue;
                                }
                                xSubVarText = xSubVarText.Replace("\r\n", "\r");
                                xSubVarText = xSubVarText.Replace("\r", "\v");
                                if (bFormatSmartQuotes)
                                    xSubVarText = String.ReplaceQuotesWithSmartQuotes(xSubVarText);
                                xSubVarText = String.RestoreXMLChars(xSubVarText);
                                string xNodeName = oNode.Name;
                                if (xTemplate != "")
                                {
                                    xNodeName = ((string[])aTemplateElements[iCounter])[0];
                                }

                                string xUNID = "0";
                                try
                                {
                                    xUNID = oNode.Attributes.GetNamedItem("UNID").Value;
                                }
                                catch { }
                                if (xItemSeparator != "")
                                {
                                    Run oDelimRun = new Run();
                                    switch (xItemSeparator)
                                    {
                                        case ("\r"):
                                            oDelimRun.AppendChild<Paragraph>(new Paragraph());
                                            break;
                                        case ("\v"):
                                            oDelimRun.AppendChild<Break>(new Break());
                                            break;
                                        case ("\r\t"):
                                            oDelimRun.Append(new Paragraph(), new Run(new TabChar()));
                                            break;
                                        default:
                                            oDelimRun.AppendChild<Text>(new Text(xItemSeparator));
                                            break;
                                    }
                                    oDetailRun.AppendChild<Run>(oDelimRun);
                                }

                                //Create separate Text elements for each line, separated by Breaks
                                string[] aLines = xSubVarText.Split('\v');
                                Run oTextRun = new Run();
                                for (int i = 0; i < aLines.GetLength(0); i++)
                                {
                                    oTextRun.AppendChild<Text>(new Text(aLines[i]));
                                    if (i < aLines.GetUpperBound(0))
                                    {
                                        oTextRun.AppendChild<Break>(new Break());
                                    }
                                }

                                xID = Query.GenerateDocVarID(this.m_oSegment.WPDoc);
                                xTag = "mpu" + xID + new string('0', 28);
                                //Create Doc var for subvar
                                Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "Name", xNodeName, false, "");
                                Query.SetAttributeValue(this.m_oSegment.WPDoc, xTag, "ObjectData", string.Format("Index={0}�UNID={1}��", iCurIndex.ToString(), xUNID), false, "");

                                //Use Doc Var ID for bookmark Id to avoid conflicts
                                //SdtRun oSubRun = new SdtRun(new SdtProperties(new Tag { Val = xTag }), new SdtContentRun(new BookmarkStart { Id = xID, Name = "_" + xTag }, oTextRun,
                                //    new BookmarkEnd { Id = xID }));
                                SdtRun oSubRun = new SdtRun(new SdtProperties(new Tag { Val = xTag }), new SdtContentRun(new BookmarkStart { Id = xID, Name = "_" + xTag }, oTextRun,
                                    new BookmarkEnd { Id = xID }));
                                oDetailRun.AppendChild<SdtRun>(oSubRun);
                                if (xTemplate != "")
                                    break;
                                else
                                    xItemSeparator = xDetailItemSeparator;
                            }
                            else
                            {
                                continue;
                            }

                        }
                    }
                    oDetailRun.InnerXml = oDetailRun.InnerXml.Replace("\t", "<w:r><w:tab/></w:r>");
                    aDetailXML.Add(oDetailRun.InnerXml);
                }
                else
                {
                    iCurIndex = -1;
                }

            } while (iCurIndex > -1);

            return (string[])aDetailXML.ToArray(typeof(string));
        }
         
        private string GetFormattedDateOXML(string xFormat, bool bAsField, string xPrefix, short LCID)
        {
            string xValue = "";
            //Different am/pm token for .NET and Word fields
            string xFormatNET = xFormat.Replace("am/pm", "tt");
            System.Globalization.CultureInfo oCulture = null;
            //Get date display value
            if (LCID != 0)
            {
                oCulture = new System.Globalization.CultureInfo(LCID);
                xValue = DateTime.Now.ToString(xFormatNET, oCulture);
            }
            else
            {
                //No LCID specified, use Language at location
                xValue = DateTime.Now.ToString(xFormatNET);
            }
            if (!string.IsNullOrEmpty(xFormat))
            {
                if (bAsField)
                {
                    //Build XML for field
                    Run oFldRun = new Run();
                    //Add Prefix text if configured
                    if (!string.IsNullOrEmpty(xPrefix))
                    {
                        oFldRun.Append(new Run(new Text(xPrefix) { Space = SpaceProcessingModeValues.Preserve }));
                    }
                    //Build Word fieldcode XML
                    oFldRun.Append(new Run(new FieldChar() { FieldCharType = FieldCharValues.Begin }),
                        new Run(new FieldCode("DATE \\@ \"" + xFormat + "\"") { Space = SpaceProcessingModeValues.Preserve }),
                        new Run(new FieldChar() { FieldCharType = FieldCharValues.Separate }),
                        new Run(new Text(xValue)),
                        new Run(new FieldChar() { FieldCharType = FieldCharValues.End }));
                    return oFldRun.InnerXml;
                }
                else
                {
                    //strip single and double quotes
                    xFormat = xFormat.Replace("\"", "'");
                    xFormat = xFormat.Replace("'", "");
                    //Return as date text string
                    if (xFormat.IndexOf("%") > -1 || xFormat.IndexOf("/J") > -1)
                    {
                        //Jurat date format
                        xValue = xFormat.Replace("/J", "").TrimEnd(' ');
                        string xMonth = DateTime.Now.ToString("MMMM", oCulture);
                        string xYear = DateTime.Now.ToString("yyyy", oCulture);
                        string xDay = "";
                        int iDay = 0;
                        if (xValue.IndexOf("%") > -1)
                        {
                            string xDayFormat = xValue.Substring(0, xValue.IndexOf("%"));
                            xDayFormat = xDayFormat.Substring(xDayFormat.IndexOf(" ") + 1);
                            xDay = DateTime.Now.ToString("dd", oCulture);
                            if (Int32.TryParse(xDay, out iDay))
                            {
                                if (iDay > 3 && iDay < 20)
                                    xDay = iDay.ToString() + "th";
                                else
                                {
                                    switch (iDay % 10)
                                    {
                                        case 1:
                                            xDay = iDay.ToString() + "st";
                                            break;
                                        case 2:
                                            xDay = iDay.ToString() + "nd";
                                            break;
                                        case 3:
                                            xDay = iDay.ToString() + "rd";
                                            break;
                                        default:
                                            xDay = iDay.ToString() + "th";
                                            break;
                                    }
                                }
                            }
                            xValue = xValue.Replace(xDayFormat + "%", xDay);
                        }
                        xValue = xValue.Replace("MMMM", xMonth);
                        xValue = xValue.Replace("yyyy", xYear);
                    }
                    return xPrefix + xValue;
                }
            }
            else
                return "";
        }
        /// <summary>
        /// inserts the specified SubVar-related detail
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void InsertDetail(string xValue, string xParameters, bool bIgnoreDeleteScope)
        {
            DateTime t0 = DateTime.Now;

            //GLOG 7824: Run this action for Labels and Envelopes in Design only
            //GLOG 15786: For labels and envelopes, only run this action 
            //when finishing segment after all other variable actions have run
            if (m_oSegment is LMP.Architect.Base.IStaticDistributedSegment && 
                (m_oSegment.CreationMode != XmlSegment.OxmlCreationMode.StaticFinish || m_oSegment.CreationStatus != Base.Segment.Status.Finished))
            {
                return;
            }
            string xDetailItemSeparator = "";
            string xEntitySeparator = "";
            string xTemplate = "";
            string[] aInsertXML = new string[0];
            string xSegmentXML = "";
            short shUnderlineLength = 0;
            int iThreshold = 0;
            object missing = System.Reflection.Missing.Value;
            AlternateFormatTypes iAlternateFormat = AlternateFormatTypes.None;
            XmlDocument oXMLSource = new System.Xml.XmlDocument();
            string xExpression = "";
            XmlVariable oVarSource = null;
            DeleteScope.DeleteScopeTypes iDeleteScope = 0;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length < 7)
                //7 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xDetailItemSeparator = XmlExpression.Evaluate(aParams[0],
                        m_oSegment, m_oForteDocument);

                    xEntitySeparator = XmlExpression.Evaluate(aParams[1],
                        m_oSegment, m_oForteDocument);

                    xTemplate = XmlExpression.Evaluate(aParams[2], m_oSegment, m_oForteDocument);

                    iAlternateFormat = (AlternateFormatTypes)Int32.Parse(aParams[3]);

                    iThreshold = Int32.Parse(aParams[4]);

                    //reset threshold to impossible value if zero
                    iThreshold = iThreshold == 0 ? iThreshold = -9999 : iThreshold;

                    shUnderlineLength = short.Parse(XmlExpression.Evaluate(aParams[5],
                        m_oSegment, m_oForteDocument));

                    iDeleteScope = (DeleteScope.DeleteScopeTypes)System.Convert.ToInt32(
                        XmlExpression.Evaluate(aParams[6], m_oSegment, m_oForteDocument));
                    if (aParams.GetUpperBound(0) > 6)
                        xExpression = aParams[7];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                Trace.WriteNameValuePairs("xValue", xValue, "xDetailItemSeparator",
                    xDetailItemSeparator, "xEntitySeparator", xEntitySeparator, "iDeleteScope",
                    iDeleteScope, "xTemplate", xTemplate, "DetailType", iAlternateFormat, "Split Threshold", iThreshold,
                    "xExpression", xExpression);

                //Use a different variable or expression as source of value
                if (xExpression != "")
                {
                    //TODO: Originally, this paramater was a Variable name
                    //In order not to break existing segments, we check
                    //first to see if non-expression parameter is a valid variable name
                    //This can be removed, once any existing segments have been adjusted
                    if (!xExpression.Contains("["))
                    {
                        //get target segment(s)
                        XmlSegment[] oTargets = m_oSegment.GetReferenceTargets(ref xExpression);
                        if (oTargets != null)
                        {
                            foreach (XmlSegment oTarget in oTargets)
                            {
                                //get target variable
                                oVarSource = oTarget.GetVariable(xExpression);

                                //if variable exists, set value
                                if (oVarSource != null)
                                {
                                    xValue = oVarSource.Value;
                                    break;
                                }
                            }
                        }
                        if (oVarSource == null)
                        {
                            xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);
                        }
                    }
                    else
                        xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);
                }

                //exit if value of assignment is mpUndefined
                if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                    return;

                //Convert xValue to XML Nodes, convert to insertable XML string
                try
                {
                    //get detail and entity separators
                    ModifyItemSeparatorsIfNecessary(aParams, ref xEntitySeparator, ref xDetailItemSeparator);

                    //Template is token reserved for empty value
                    //GLOG 2083: Recognize both old and new Fieldcode format
                    if (xTemplate.ToUpper() == "[SUBVAR_EMPTY]" || xTemplate.ToUpper() == "[SUBVAR__EMPTY]")
                        xValue = "";

                    if (!string.IsNullOrEmpty(xValue) && !xValue.StartsWith("<"))
                    {
                        string xXMLPart = "";
                        string xDelimPart = "";
                        int iXMLStart = xValue.IndexOf('<');
                        //GLOG 5034: Check whether '<' is start of appended XML portion, 
                        //or is instead part of text within delimited values
                        if (iXMLStart > -1 && xValue.EndsWith(">"))
                        {
                            //XML Value has been appended to delimited values -
                            //Convert Delimited Portion only
                            xXMLPart = xValue.Substring(iXMLStart);
                            xDelimPart = xValue.Substring(0, iXMLStart);
                        }
                        else
                            xDelimPart = xValue;
                        //specified value is a pipe delimited string -
                        //create xml from string
                        int i = 0;
                        string[] aValues = xDelimPart.Split('|');
                        string xSubVarName = "";
                        if (xTemplate != "")
                        {
                            //Get SubVar name from Template if defined
                            int iStart = xTemplate.IndexOf("__") + 2;
                            //Check for old format fieldcode
                            if (iStart == 1)
                                iStart = xTemplate.IndexOf("_") + 1;

                            int iEnd = xTemplate.IndexOf("]");
                            if (iStart > 0 && iEnd > -1 && (iEnd > iStart))
                                xSubVarName = xTemplate.Substring(iStart, iEnd - iStart);
                        }
                        if (xSubVarName == "")
                            xSubVarName = "FULLNAME";
                        StringBuilder oSB = new StringBuilder();
                        foreach (string aValue in aValues)
                        {
                            i++;
                            //GLOG 5034:  Replace any reserved XML characters within Detail values
                            oSB.AppendFormat("<{0} Index=\"{1}\" UNID=\"0\">{2}</{0}>",
                                xSubVarName, i.ToString(), LMP.String.ReplaceXMLChars(aValue)); //GLOG 6000
                        }
                        xValue = oSB.ToString();
                        //Reappend original XML portion
                        if (xXMLPart != "")
                            xValue = xValue + xXMLPart;
                    }


                    if (xValue != "")
                    {
                        aInsertXML = GetDetailSubVarXML(xValue, xTemplate, xDetailItemSeparator, true);
                    }
                    else
                    {
                        if (iDeleteScope == DeleteScope.DeleteScopeTypes.Target)
                            aInsertXML = new string[] { "" };
                        else
                            aInsertXML = new string[0];
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.XMLException(
                        LMP.Resources.GetLangString("Error_InvalidDetailXML") +
                        xParameters, oE);
                }
            }

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph))
                iDeleteScope = DeleteScope.DeleteScopeTypes.Target;

            //JTS 8/28/15: TODO - Deal with adding/removing table format
            if (iAlternateFormat == AlternateFormatTypes.Table &&
                iThreshold > 0 && m_oVariable.IsMultiValue)
            {
                int iDetailCount = 0;
                if (aInsertXML != null && aInsertXML.GetLength(0) > 0)
                    iDetailCount = aInsertXML.Count();
                bool bRefresh = m_oForteDocument.SetupDistributedDetailTable(m_oVariable.AssociatedContentControls[0], m_oSegment.WPDoc, iDetailCount, 1, false, 0, 0, 0, iThreshold);
            }
            //else if (iAlternateFormat == AlternateFormatTypes.Columns && iThreshold > 0 &&
            //    m_oVariable.IsMultiValue)
            //{
            //    int iDetailCount = 1;
            //    if (aXML != null)
            //        iDetailCount = aXML.Count;
            //    bool bRefresh = false;
            //    int iLength = 0;

            //    iLength = m_oVariable.AssociatedContentControls.Count;

            //    if (iDetailCount >= iThreshold && iLength == 1)
            //    {
            //        //restore delete scope before setting up columns - GLOG 2953 (8/19/08)
            //        if (bIsSegNode)
            //        {
            //            object oSegNodesArray = null;

            //            oSegNodesArray = m_oVariable.ContainingContentControls;

            //            oMPDoc.InsertText_RestoreScope(m_oDocument, ref oSegNodesArray, "", "",
            //                0, iDeleteScope, xSegmentXML, m_oVariable.Name, oInsertionLocation,
            //                ref xVariablesInScope, ref oTags);
            //            RefreshVariables(xVariablesInScope, false);
            //            m_oVariable.TagType = ForteDocument.TagTypes.Variable;
            //        }

            //        oMPDoc.SetupDistributedDetailColumns_CC(
            //            m_oVariable.AssociatedContentControls[0],
            //            LMP.Forte.MSWord.mpDetailColumnActions.ActionSplitCell);
            //        bRefresh = true;
            //    }
            //    else if (iDetailCount < iThreshold && iLength == 2)
            //    {
            //        oMPDoc.SetupDistributedDetailColumns_CC(
            //            m_oVariable.AssociatedContentControls[0],
            //            LMP.Forte.MSWord.mpDetailColumnActions.ActionJoinCells);
            //        bRefresh = true;
            //    }
            //    //GLOG 5027: RefreshTags should never run when saving Design
            //    if (this.m_oForteDocument.Mode != XmlForteDocument.Modes.Design && bRefresh)
            //    {
            //        //Refresh since Nodes were added or removed
            //        m_oForteDocument.RefreshTags();
            //        m_oSegment.RefreshNodes();
            //    }
            //}

            List<SdtElement> oCCList = m_oVariable.ContainingContentControls;
            string xTagPrefix = Query.GetTag(oCCList[0]).Substring(0, 3);
            bool bIsSegNode = (xTagPrefix == "mps");

            //exit if value is empty and scope is already deleted
            bool bIsEmpty = (aInsertXML.GetLength(0) == 0);
            if (bIsSegNode && bIsEmpty && !bIgnoreDeleteScope &&
                (!m_oVariable.IsMultiValue  || m_oVariable.TagType != Base.ForteDocument.TagTypes.Segment)) //GLOG 8461
                return;

            bool bExpandedScope = false; 

            //GLOG 15777: Mark for Expanded Scope if any CC in Multi-value variable will Delete or Restore Delete Scope
            if (m_oVariable.IsMultiValue && iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph)
            {
                for (int i = 0; i < oCCList.Count; i++)
                {
                    string xCCTag = Query.GetTag(oCCList[i]).Substring(0, 3);
                    if (xCCTag == "mps" && i <= aInsertXML.GetUpperBound(0) && aInsertXML[i] != "")
                    {
                        bExpandedScope = true;
                        break;
                    }
                    if (xCCTag != "mps" && i <= aInsertXML.GetUpperBound(0) && aInsertXML[i] == "")
                    {
                        bExpandedScope = true;
                        break;
                    }
                }
            }
            else if (!m_oVariable.IsMultiValue)
            {
                bExpandedScope = ((iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph) &&
                 (bIsSegNode || (bIsEmpty && (shUnderlineLength == 0))));
            }
            //get segment xml
            if (bExpandedScope)
            {
                if ((m_oSegment is XmlCollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }
            if (m_oVariable.IsMultiValue && aInsertXML.GetLength(0) == 1 && oCCList.Count > 1)
            {
                //If there's a single value with multiple tags (as with full page of labels)
                //pad array with retagged duplicates of the first item
                int iTagCount = oCCList.Count;
                List<string> oItemCopies = new List<string>();
                oItemCopies.AddRange(aInsertXML);
                for (int i = 1; i < iTagCount; i++)
                {
                    oItemCopies.AddRange(GetDetailSubVarXML(xValue, xTemplate, xDetailItemSeparator, true));
                }
                aInsertXML = oItemCopies.ToArray();
            }

            //insert xml
            try
            {
                //GLOG 15777: Restoration of Delete Scope now handled by InsertDetailXML
                //if (bIsSegNode)
                //    oCCList = DeleteScope.Restore(m_oVariable, oCCList, iDeleteScope);
                InsertDetailXML(oCCList, aInsertXML, xEntitySeparator, shUnderlineLength, iDeleteScope,
                        xSegmentXML, m_oVariable.Name, ref bIgnoreDeleteScope);
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            if (bExpandedScope)
                m_oSegment.Refresh(Base.Segment.RefreshTypes.NoChildSegments);
        }
        /// <summary>
        /// inserts the specified SubVar-related detail into a table
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void InsertDetailIntoTable(string xValue, string xParameters, bool bIgnoreDeleteScope)
        {
        }

        /// <summary>
        /// Sets up label table with appropriate cells populated with tags
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void SetupLabelTable(string xValue, string xParameters)
        {
            //GLOG 15786: For labels and envelopes, only run this action 
            //when finishing segment after all other variable actions have run
            if (m_oSegment is LMP.Architect.Base.IStaticDistributedSegment &&
                (m_oSegment.CreationMode != XmlSegment.OxmlCreationMode.StaticFinish || m_oSegment.CreationStatus != Base.Segment.Status.Finished))
            {
                //don't run any setup of sections for envelopes, as the new (10.5.1) functionality
                //uses Word's merge feature to create the appropriate number of envelope sections
                return;
            }
            DateTime t0 = DateTime.Now;

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            int iStartPos = 1;
            bool bFullPageOfSame = false;
            int iLabelCount = 0;
            string xVariableTarget = "";

            XmlVariable oVarTarget = null;


            object missing = System.Reflection.Missing.Value;
            XmlDocument oXMLSource = new System.Xml.XmlDocument();

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 3)
                //3 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVariableTarget = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    try
                    {
                        iStartPos = int.Parse(XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    }
                    catch
                    {
                        //Default to 1
                        iStartPos = 1;
                    }
                    if (aParams[2] != "")
                    {
                        try
                        {
                            bFullPageOfSame = bool.Parse(XmlExpression.Evaluate(aParams[2], m_oSegment, m_oForteDocument));
                        }
                        catch
                        {
                            bFullPageOfSame = false;
                        }
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                Trace.WriteNameValuePairs("xValue", xValue, "xVariableTarget",
                    xVariableTarget, "iStartPos", iStartPos, "bFullPageOfSame", bFullPageOfSame);
            }
            if (xVariableTarget != "")
            {
                //Different variable holds the target tag
                XmlSegment[] oTargets = m_oSegment.GetReferenceTargets(ref xVariableTarget);
                if (oTargets != null)
                {
                    foreach (XmlSegment oTarget in oTargets)
                    {
                        //get target variable
                        oVarTarget = oTarget.GetVariable(xVariableTarget);

                        //stop after finding match
                        if (oVarTarget != null)
                        {
                            break;
                        }
                    }
                }
                if (oVarTarget == null)
                {
                    return;
                }
            }
            else
                oVarTarget = m_oVariable;

            if (bFullPageOfSame)
            {
                //Marker for full page of same
                iLabelCount = -1;
            }
            else
                iLabelCount = String.GetXMLEntityCount(xValue, null);

            //Setup table for labels
            try
            {
                //10.2 - JTS 4/2/10
                SdtElement oCC = oVarTarget.AssociatedContentControls[0];

                //disable event handler that prevents deletion of mDel tags
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                this.m_oForteDocument.SetupLabelTable(oCC, m_oSegment.WPDoc, iLabelCount, iStartPos);

            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// Sets up service list table with appropriate cells populated with tags
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void SetupDistributedDetailTable(string xValue, string xParameters)
        {
            DateTime t0 = DateTime.Now;

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            int iDetailCount = 0;
            int iStartPos = 1;
            int iHeaderRows = 0;
            int iMaxRows = 0;
            int iMinRows = 0;
            bool bFullRow = false;
            string xVariableTarget = "";

            XmlVariable oVarTarget = null;

            object missing = System.Reflection.Missing.Value;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 6)
                //6 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVariableTarget = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    iStartPos = Int32.Parse(XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    bFullRow = bool.Parse(XmlExpression.Evaluate(aParams[2], m_oSegment, m_oForteDocument));
                    iHeaderRows = Int32.Parse(XmlExpression.Evaluate(aParams[3], m_oSegment, m_oForteDocument));
                    if (aParams[4] != "")
                        iMinRows = Int32.Parse(XmlExpression.Evaluate(aParams[4], m_oSegment, m_oForteDocument));
                    if (aParams[5] != "")
                        iMaxRows = Int32.Parse(XmlExpression.Evaluate(aParams[5], m_oSegment, m_oForteDocument));
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                Trace.WriteNameValuePairs("xVariableTarget", xVariableTarget, "xValue", xValue,
                    "iStartPos", iStartPos, "bFullRow", bFullRow, "iHeaderRows", iHeaderRows);
            }

            if (xVariableTarget != "")
            {
                //Different variable holds the target tag
                XmlSegment[] oTargets = m_oSegment.GetReferenceTargets(ref xVariableTarget);
                if (oTargets != null)
                {
                    foreach (XmlSegment oTarget in oTargets)
                    {
                        //get target variable
                        oVarTarget = oTarget.GetVariable(xVariableTarget);

                        //stop after finding match
                        if (oVarTarget != null)
                        {
                            break;
                        }
                    }
                }
                if (oVarTarget == null)
                {
                    return;
                }

                //GLOG 3456 (dm) - if this an mDel, reinsert the table before proceeding
                if (oVarTarget.TagType == XmlForteDocument.TagTypes.DeletedBlock)
                {
                    //TODO: Deal with deleted block
                    ////get names of subvariables from RunVariableActions parameter
                    //string[] aSubVariables = null;
                    //for (int i = 0; i < m_oVariable.VariableActions.Count; i++)
                    //{
                    //    XmlVariableAction oAction = m_oVariable.VariableActions[i];
                    //    if (oAction.Type == XmlVariableActions.Types.RunVariableActions)
                    //    {
                    //        string[] aRVParams = oAction.Parameters.Split(
                    //            LMP.StringArray.mpEndOfSubField);
                    //        aSubVariables = aRVParams[0].Split(',');
                    //        break;
                    //    }
                    //}

                    //if (aSubVariables != null)
                    //{
                    //    //reinsert the table by executing the target variable's actions
                    //    //with the parameter to leave the delete scope when value is empty
                    //    oVarTarget.VariableActions.Execute(true);

                    //    //temporarily clear the value of the master variable
                    //    XmlVariable oMasterVariable = m_oSegment.Variables.ItemFromName(
                    //        m_oVariable.Name);
                    //    oMasterVariable.SetValue("", false);

                    //    //execute actions of other variables to clear values
                    //    foreach (string xSubVariable in aSubVariables)
                    //    {
                    //        if (xSubVariable != oVarTarget.Name)
                    //        {
                    //            XmlVariable oSubVariable = null;
                    //            try
                    //            {
                    //                oSubVariable = m_oSegment.Variables.ItemFromName(
                    //                    xSubVariable);
                    //            }
                    //            catch { }
                    //            if (oSubVariable != null)
                    //                oSubVariable.VariableActions.Execute(true);
                    //        }
                    //    }

                    //    //restore value of master variable
                    //    oMasterVariable.SetValue(xValue, false);
                    //}
                }
            }
            else
                oVarTarget = m_oVariable;

            //Get count of Detail entities
            iDetailCount = String.GetXMLEntityCount(xValue, null);

            //Setup table for detail
            try
            {
                //disable event handler that prevents deletion of mDel tags
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                DateTime t2 = DateTime.Now;
                bool bRefreshNeeded = false;
                //content controls
                SdtElement oCC = oVarTarget.AssociatedContentControls[0];
                bRefreshNeeded = m_oForteDocument.SetupDistributedDetailTable(oCC, m_oSegment.WPDoc, iDetailCount, iStartPos, bFullRow, iHeaderRows, iMinRows, iMaxRows, -9999);
                LMP.Benchmarks.Print(t2, "SetupDistributedDetailTable");

                if (bRefreshNeeded)
                {
                    //TODO: Update just ContainingContentControls for variable?
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                //hide tags if these were hidden before running this action
                //reenable event handler
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;

                LMP.Benchmarks.Print(t0);
            }
        }
        private void SetupDistributedSegmentSections(string xValue, string xParameters)
        {
            //GLOG 15786: For labels and envelopes, only run this action 
            //when finishing segment after all other variable actions have run
            if (m_oSegment is LMP.Architect.Base.IStaticDistributedSegment &&
                (m_oSegment.CreationMode != XmlSegment.OxmlCreationMode.StaticFinish || m_oSegment.CreationStatus != Base.Segment.Status.Finished))
            {
                //don't run any setup of sections for envelopes, as the new (10.5.1) functionality
                //uses Word's merge feature to create the appropriate number of envelope sections
                return;
            }

            //GLOG 6687:  Don't ever run this action in Design Mode
            if (m_oForteDocument.Mode == XmlForteDocument.Modes.Design)
                return;

            DateTime t0 = DateTime.Now;

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            string xVariableName = "";
            XmlVariable oVarSource = null;
            int iDetailCount = 0;
            int iMaxSections = 0;

            object missing = System.Reflection.Missing.Value;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameter are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVariableName = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    if (aParams[1] != "")
                        iMaxSections = Int32.Parse(XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                Trace.WriteNameValuePairs("xExpression", xVariableName);
            }
            if (xVariableName != "")
            {
                //Different variable holds the target tag
                XmlSegment[] oTargets = m_oSegment.GetReferenceTargets(ref xVariableName);
                if (oTargets != null)
                {
                    foreach (XmlSegment oTarget in oTargets)
                    {
                        //get target variable
                        oVarSource = oTarget.GetVariable(xVariableName);

                        //stop after finding match
                        if (oVarSource != null)
                        {
                            xValue = oVarSource.Value;
                            break;
                        }
                    }
                }
                if (oVarSource == null)
                {
                    return;
                }
            }

            //Get last Index to determine count of details in Value
            iDetailCount = String.GetXMLEntityCount(xValue, "");

            //Limit Sections based on parameter
            if (iMaxSections > 0)
                iDetailCount = Math.Min(iDetailCount, iMaxSections);

            SdtElement oSegCC = m_oSegment.RawSegmentParts[0].ContentControl;

            //Setup table for detail
            try
            {
                //Each section with have a separate SectionProperties node
                SectionProperties[] aSecProps = Query.GetSectionPropertiesForContentControl(oSegCC, m_oSegment.WPDoc);

                //JTS (11/14/08): Don't do anything if there are already the correct number of sections
                if (aSecProps.Count() != Math.Max(iDetailCount, 1))
                {
                    XmlSegment.AddOrRemoveSections(m_oSegment, iDetailCount);
                    this.m_oForteDocument.RaiseSegmentDistributedSectionsChangedEvent(m_oSegment);
                }

            }
            catch (System.Exception oE)
            {
                throw oE;
            }

        }
        /// <summary>
        /// returns the xml
        /// </summary>
        /// <param name="xTemplate"></param>
        /// <returns></returns>
        private string GetTableRowXML(string xSource, string xTemplate, int iMinRows)
        {
            if (xSource == "" && iMinRows < 1)
                return "";

            string xTableXML = "";
            //GLOG 2083: Conform all fieldcodes to old format
            xTemplate.Replace("[SubVar__", "[SubVar_");
            //get subvars in template
            MatchCollection oSubVars = Regex.Matches(xTemplate, @"\[SubVar_.*?\]");

            //get names of fields in source that should be retrieved
            string[] aFields = new string[oSubVars.Count];

            int iCounter = 0;

            //cycle through subvars, adding field names to array for later use
            foreach (Match oSubVar in oSubVars)
            {
                aFields[iCounter] = oSubVar.Value.Substring(
                    8, oSubVar.Value.Length - 9);
                iCounter++;
            }

            string xOpeningXML = LMP.String.GetMinimalOpeningXML(false);

            //Get proper namespace prefix for MacPac 
            string xNS = LMP.String.GetNamespacePrefix(
                xOpeningXML, LMP.Data.ForteConstants.MacPacNamespace);

            XmlDocument oXMLSource = new System.Xml.XmlDocument();
            oXMLSource.LoadXml("<zzmpD>" + xSource + "</zzmpD>");

            //select detail nodes in XML
            XmlNodeList oSourceXMLNodes = oXMLSource.DocumentElement.SelectNodes("/zzmpD/*");

            if (oSourceXMLNodes.Count > 0 || iMinRows > 0)
            {
                int iNumEntities = 0;

                //count entities in source
                if (oSourceXMLNodes.Count > 0)
                {
                    iNumEntities = int.Parse(oSourceXMLNodes[
                        oSourceXMLNodes.Count - 1].Attributes["Index"].Value);
                }

                //encapsulate template with row/cell xml
                xTemplate = "<w:tr><w:tc><w:tcPr><w:tcW w:w=\"2952\" w:type=\"dxa\"/></w:tcPr><w:p>" + 
                    xTemplate + "</w:p></w:tc></w:tr>";

                //add appropriate xml for cell breaks
                xTemplate = xTemplate.Replace(@"\a", "</w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2952\" w:type=\"dxa\"/></w:tcPr><w:p>");
                
                //replace linebreaks with proper xml
                xTemplate = xTemplate.Replace(@"\v", "<w:br/>");
                xTemplate = xTemplate.Replace(@"\r\n", "\r");
                xTemplate = xTemplate.Replace(@"\r", "<w:/p><w:p>");

                //cycle through entities, creating xml 
                //from source data and template
                for (int i = 1; i <= Math.Max(iNumEntities, iMinRows); i++)
                {
                    string xEntityXML = xTemplate;

                    //cycle through fields, getting the 
                    //appropriate data from the source -
                    //then replace corresponding text in template
                    foreach (string xField in aFields)
                    {
                        //Set defaults for empty fields
                        string xUNID = "0";
                        string xName = xField;
                        string xIndex = i.ToString();
                        string xText = "";

                        StringBuilder oSB = new StringBuilder();
                        if (i <= iNumEntities)
                        {
                            XmlNode oNode = oXMLSource.SelectSingleNode(
                                @"(/zzmpD/" + xField + "|/zzmpD/" + xField.ToUpper() + ")[@Index = '" + i.ToString() + "']");
                            if (oNode != null)
                            {
                                xUNID = oNode.Attributes["UNID"].Value;
                                xName = oNode.Name;
                                xIndex = oNode.Attributes["Index"].Value;
                                xText = oNode.InnerText.Replace("\r\n", "\v");
                                xText = LMP.String.ReplaceXMLChars(xText);
                            }

                            // Build WordML string for mSubVar Tag
                            oSB.AppendFormat("<{0}:mSubVar Name=\"{1}\" ObjectData=\"Index={2}�UNID={3}��\"><w:r><w:t>{4}</w:t></w:r></{0}:mSubVar>",
                                xNS, xName, xIndex, xUNID, xText);

                        }
                        else
                        {
                            // Leave cells blank
                            oSB.Append("");
                        }
                        //replace subvar text in template
                        xEntityXML = xEntityXML.Replace("[SubVar_" + xField + "]", oSB.ToString());
                        //Remove any linebreaks at the start a cell (due to empty fields)
                        xEntityXML = xEntityXML.Replace("<w:p><w:br/>", "<w:p> ");
                    }

                    xTableXML += xEntityXML;
                }

            }
            return xTableXML;
        }

        /// <summary>
        /// returns detail item or entity item separators
        /// </summary>
        /// <param name="xEntitySeparator"></param>
        /// <param name="xDetailItemSeparator"></param>
        protected internal void ModifyItemSeparatorsIfNecessary(string[] aParams, ref string xEntitySeparator, 
            ref string xDetailItemSeparator)
        {
            if (xDetailItemSeparator == "Null")
                //keyword specifies that line breaks should simply be removed
                xDetailItemSeparator = "";
            else if (xDetailItemSeparator == "")
                xDetailItemSeparator = "\v";

            if (xEntitySeparator == "Null")
                //keyword specifies that line breaks should simply be removed
                xEntitySeparator = "";
            else if (xEntitySeparator == "")
                xEntitySeparator = "\r";

            // Item separator can't be Chr(13) if Entity separator is Chr(11), because 
            // mSubVar tag can't contain a partial paragraph
            if (xEntitySeparator == "\v" && xDetailItemSeparator == "\r")
                xDetailItemSeparator = "\v";

        }
        /// <summary>
        /// inserts the specified SubVar-related reline
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        private void InsertReline(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
        {
            string xSpecialSeparator = "";
            string xSpecialDelimiter = "";
            string xStandardDelimiter = "";
            string xLabelText = "";
            string[] aInsertXML = null;
            short shFormat = 0;
            short shUnderlineLength = 0;
            LMP.Controls.mpRelineFormatOptions iDefOptions = 0;
            DeleteScope.DeleteScopeTypes iDeleteScope = 0;
            object missing = System.Reflection.Missing.Value;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length < 5)
                //5 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    //Label text preceding Re line
                    xLabelText = aParams[0];
                    if (xLabelText == "Null")
                        xLabelText = "";
                    else if (xLabelText == "")
                        xLabelText = "Re:[Char_9]";
                    xLabelText = XmlExpression.Evaluate(xLabelText, m_oSegment, m_oForteDocument);

                    //Separator character(s) between Standard and Special Relines
                    xStandardDelimiter = aParams[1];
                    if (xStandardDelimiter == "Null")
                        xStandardDelimiter = "";
                    else if (xStandardDelimiter == "")
                        xStandardDelimiter = "[Char_13][Char_9]";
                    xStandardDelimiter = XmlExpression.Evaluate(xStandardDelimiter,
                        m_oSegment, m_oForteDocument);

                    //Character(s) separating labels and values in Special Re line
                    xSpecialSeparator = aParams[2];
                    if (xSpecialSeparator == "Null")
                        xSpecialSeparator = "";
                    else if (xSpecialSeparator == "")
                        xSpecialSeparator = "[Char_9]:[Char_9]";
                    xSpecialSeparator = XmlExpression.Evaluate(xSpecialSeparator,
                        m_oSegment, m_oForteDocument);

                    //Character(s) at end of each Special line
                    xSpecialDelimiter = aParams[3];
                    if (xSpecialDelimiter == "Null")
                        xSpecialDelimiter = "";
                    else if (xSpecialDelimiter == "")
                        xSpecialDelimiter = "[Char_11]";
                    xSpecialDelimiter = XmlExpression.Evaluate(xSpecialDelimiter,
                        m_oSegment, m_oForteDocument);

                    iDeleteScope = (DeleteScope.DeleteScopeTypes)System.Convert.ToInt32(
                        XmlExpression.Evaluate(aParams[4], m_oSegment, m_oForteDocument));

                    iDefOptions = (LMP.Controls.mpRelineFormatOptions)Int32.Parse(XmlExpression.Evaluate(aParams[5],
                        m_oSegment, m_oForteDocument));
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
                Trace.WriteNameValuePairs("xValue", xValue, "xStandardDelimiter", xStandardDelimiter, "xSpecialSeparator",
                    xSpecialSeparator, "xSpecialDelimiter", xSpecialDelimiter, "iDeleteScope",
                    iDeleteScope, "xLabelText", xLabelText);

                //exit if value of assignment is mpUndefined
                if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                    return;

            }
            if (xValue.IndexOf('<') != 0)
            {
                //Make sure string is in XML form
                xValue = LMP.Controls.Reline.StringToRelineXML(xValue, iDefOptions, "");
            }

            try
            {
                aInsertXML = GetReLineSubVarXML(xValue, xLabelText, xSpecialDelimiter, xSpecialSeparator, ref shFormat, true);
            }
            catch (System.Exception oE)
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_InvalidDetailXML") +
                    xParameters, oE);
            }

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph))
                iDeleteScope = DeleteScope.DeleteScopeTypes.Target;

            List<SdtElement> oCCList = m_oVariable.ContainingContentControls;

            string xTagPrefix = Query.GetTag(oCCList[0]).Substring(0, 3);
            bool bIsSegNode = (xTagPrefix == "mps");

            //exit if value is empty and scope is already deleted
            if (bIsSegNode && (xValue == "") && !bIgnoreDeleteScope)
                return;

            //exit if value is empty and scope is already deleted
            bool bIsEmpty = aInsertXML == null;
            if (bIsSegNode && bIsEmpty && !bIgnoreDeleteScope)
                return;

            bool bExpandedScope = ((iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph) &&
                (bIsSegNode || (bIsEmpty && (shUnderlineLength == 0))));

            //set other argument values
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is XmlCollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }
            LMP.Architect.Base.mpRelineFormatOptions iRelineFormat = (LMP.Architect.Base.mpRelineFormatOptions)shFormat;
            //GLOG 3510: Indicate for InsertXML action to remove underline from 
            //last line if there is text immediately following mVar which has been included
            //in Underline Last
            if ((iRelineFormat & LMP.Architect.Base.mpRelineFormatOptions.RelineUnderline) == 0 &&
                (iRelineFormat & LMP.Architect.Base.mpRelineFormatOptions.RelineUnderlineLast) == 0)
                iRelineFormat = iRelineFormat | LMP.Architect.Base.mpRelineFormatOptions.RemoveUnderlineLast;

            //disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            //insert
            try
            {
                //GLOG 15777: Restoration of Delete Scope now handled by InsertDetailXML
                //if (bIsSegNode)
                //    oCCList = DeleteScope.Restore(m_oVariable, oCCList, iDeleteScope);
                InsertDetailXML(oCCList, aInsertXML, xStandardDelimiter, shUnderlineLength, iDeleteScope,
                        xSegmentXML, m_oVariable.Name, ref bIgnoreDeleteScope);
            }
            catch (System.Exception oE)
            {
                //rethrow
                throw oE;
            }
            if (bExpandedScope)
                m_oSegment.Refresh(Base.Segment.RefreshTypes.NoChildSegments);
        }

        /// <summary>
        /// if xValue is TRUE, inserts secondary default value at tag containing the
        /// definition for this variable; if there's no secondary default value,
        /// reinserts stored prior value; if xValue is FALSE, deletes specified scope -
        /// applies formatting per any embedded formatting tokens
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">iDeleteScope</param>
		private void IncludeExcludeText(string xValue, string xParameters, bool bIgnoreDeleteScope)
		{
            DateTime t0 = DateTime.Now;

            string xDelimReplacement = "";
            string xExpression = "";
            DeleteScope.DeleteScopeTypes iDeleteScope = 0;
            //return;
            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length < 1 || aParams.Length > 3)
                //3 parameters are used, but also allow 1 or 2 for backward compatibility
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    iDeleteScope = (DeleteScope.DeleteScopeTypes)int.Parse(
                        XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument));

                    //get delimiter replacement
                    if (aParams.Length > 1)
                    {
                        xDelimReplacement = XmlExpression.Evaluate(aParams[1],
                            m_oSegment, m_oForteDocument);
                    }
                    if (xDelimReplacement.ToUpper() == "NULL")
                        //keyword specifies that line breaks should simply be removed
                        xDelimReplacement = "";
                    else if (xDelimReplacement == "")
                        xDelimReplacement = "\v";

                    if (aParams.Length > 2)
                        xExpression = aParams[2];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "iDeleteScope", iDeleteScope,
                "xExpression", xExpression);

            if (xExpression != "")
                xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph))
                iDeleteScope = DeleteScope.DeleteScopeTypes.Target;

            List<SdtElement> oCCList = m_oVariable.ContainingContentControls;
            string xTagPrefix = Query.GetTag(oCCList[0]).Substring(0, 3);
            bool bIsSegNode = (xTagPrefix == "mps");

            bool bInclude = ((!string.IsNullOrEmpty(xValue) && xValue.ToUpper() != "NULL" &&
                String.ToBoolean(xValue)) || bIgnoreDeleteScope);
            object missing = System.Reflection.Missing.Value;

            //get text to insert
            string xText = "";
            if (bInclude && !bIgnoreDeleteScope)
            {
                xText = m_oVariable.SecondaryDefaultValue;
                if (xText != null && xText != "")
                    xText = XmlExpression.Evaluate(xText, m_oSegment, m_oForteDocument);
                else
                    xText = m_oVariable.ReserveValue;

                //this is a de facto exclude
                if (xText == "")
                    bInclude = false;
            }

            //exit if set to exclude and scope is already deleted
            if (bIsSegNode && !bInclude)
                return;

            //determine whether this is an expanded delete scope scenario
            bool bExpandedScope = ((iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph) &&
                (bIsSegNode || !bInclude));
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is XmlCollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }

            //run action
            try
            {
                if (bInclude)
                {
                    if (bIsSegNode)
                        oCCList = DeleteScope.Restore(m_oVariable, oCCList, iDeleteScope);

                    InsertSingleValueText(oCCList, xText, xDelimReplacement, 0, iDeleteScope,
                        xSegmentXML, m_oVariable.Name, ref bIgnoreDeleteScope);
                }
                else
                {
                    if (!bIsSegNode)
                    {
                        //Set all CCs to empty text
                        InsertSingleValueText(oCCList, "", xDelimReplacement, 0, iDeleteScope, xSegmentXML,
                            m_oVariable.Name, ref bIgnoreDeleteScope);
                    }
                }
            }
            catch (System.Exception oE)
            {
                //rethrow
                throw oE;
            }
            if (bExpandedScope)
                m_oSegment.Refresh(Base.Segment.RefreshTypes.NoChildSegments);
            LMP.Benchmarks.Print(t0, this.m_oVariable.Name);
        }

        /// <summary>
        /// if xValue is TRUE, includes specified block -
        /// if xValue is FALSE, deletes specified block
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">xBlockName</param>
        private void IncludeExcludeBlocks(string xValue, string xParameters, bool bIgnoreDeleteScope)
        {
            string xBlockName = "";
            string xExpression = "";
            string xName = "";
            string[] aNames = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1 && aParams.Length != 2)
                //2 parameters are used, but als allow 1 for backward compatibility
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //GLOG - 3395 - CEH
                    //Allow for more multiple Block names
                    //get parameters
                    xName = aParams[0];
                    //Can be list of Block names
                    aNames = xName.Split(',');

                    if (aParams.Length == 2)
                        xExpression = aParams[1];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
                "xExpression", xExpression);

            if (xExpression != "")
                xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //standalone variables cannot have expanded delete scopes
            if (m_oSegment == null)
                return;

            //set other argument values
            bool bInclude = string.IsNullOrEmpty(xValue) ? false : (bIgnoreDeleteScope || String.ToBoolean(xValue));
            string xSegmentXML = "";
            if ((m_oSegment is XmlCollectionTable) && (!m_oSegment.IsTopLevel))
            {
                //for pleading tables, use xml of parent
                if (m_oSegment.Parent.HasDefinition)
                    xSegmentXML = m_oSegment.Parent.Definition.XML;
            }
            else if (m_oSegment.HasDefinition)
                xSegmentXML = m_oSegment.Definition.XML;

            //GLOG - 3395 - CEH
            //Cycle through each block name
            foreach (string xItem in aNames)
            {
                string[] aBlocks;
                //GLOG 6266: Support * wildcard for Block names
                if (xItem.Contains("*"))
                {
                    string xMatchingBlocks = "";
                    //Check for both existing and deleted Blocks that match name
                    //Check only Blocks that are not currently deleted
                    for (int b = 0; b < m_oSegment.Blocks.Count; b++)
                    {
                        XmlBlock oBlock = m_oSegment.Blocks[b];
                        if (oBlock.Name.ToUpper().Contains(xItem.Replace("*", "").ToUpper()))
                        {
                            xMatchingBlocks = xMatchingBlocks + oBlock.Name + ",";
                        }
                    }
                    xMatchingBlocks = xMatchingBlocks.TrimEnd(',');
                    aBlocks = xMatchingBlocks.Split(',');
                }
                else
                {
                    aBlocks = new string[] { xItem };
                }
                foreach (string xVar in aBlocks)
                {
                    xBlockName = XmlExpression.Evaluate(xVar, m_oSegment,
                        m_oForteDocument);

                    //get tag type
                    string xTagID = m_oSegment.FullTagID + '.' + xBlockName;
                    XmlBlock oBlock = m_oSegment.Blocks.ItemFromName(xVar);
                    string xTagType = Query.GetBaseName(oBlock.AssociatedContentControl);
                    //exit if nothing to do
                    if ((bInclude && (xTagType == "mBlock")) ||
                        ((!bInclude) && (xTagType == "mDel")))
                        break;

                    SdtElement oCC = null;

                    if (bInclude)
                    {
                        //JTS 11/9/16
                        //GLOG 8484: Avoid error on non-Forte Content Controls
                        oCC = oBlock.AssociatedContentControl.Ancestors<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mps")).FirstOrDefault();
                        //var omSEGs = from cc in oBlock.AssociatedContentControl.Ancestors<SdtElement>()
                        //             where (from pr in cc.Elements<SdtProperties>()
                        //                    where pr.Descendants<Tag>().Where(t => t.Val.Value.StartsWith("mps")).Any()
                        //                    select pr).Any()
                        //             select cc;
                        //oCC = omSEGs.FirstOrDefault<SdtElement>();

                    }
                    else
                    {
                        //get mBlock
                        oCC = oBlock.AssociatedContentControl;
                    }

                    //run action
                    try
                    {
                        if (bInclude)
                        {
                            bool bRestored = DeleteScope.Restore(oBlock, oCC);

                        }
                        else
                        {

                            DeleteScope.Delete(oBlock, oCC);
                        }
                        //GLOG 8466: Refresh Segment since blocks and variables may 
                        //have been added or deleted with current block
                        m_oSegment.Refresh(Base.Segment.RefreshTypes.NoChildSegments);
                    }
                    catch (System.Exception oE)
                    {
                        //rethrow
                        throw oE;
                    }

                }
            }
        }
 
        /// <summary>
        /// inserts date/time at tag containing the definition for this variable
        /// if xFormat is a valid date format, current date will be inserted,
        /// and if xFormat contains /F switch, inserts date/time as a field.
        /// If xFormat is not a date format, it will be inserted as literal text using InsertText
        /// </summary>
        /// <param name="xFormat"></param>
        /// <param name="xParameters">shUnderlineLength, iDeleteScope, xPrefix, xExp</param>
        private void InsertDate(string xFormat, string xParameters,
            bool bIgnoreDeleteScope)
        {
            DateTime t0 = DateTime.Now;

            bool bAsField = false;
            short shUnderlineLength = 0;
            DeleteScope.DeleteScopeTypes iDeleteScope = 0;
            string xPrefix = "";
            string xExpression = "";
            string xDate = "";
            short shLCID = 0;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 4)
                //4 parameters are required - but 4 are also accepted for backward compatibility
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    shUnderlineLength = short.Parse(XmlExpression.Evaluate(aParams[0],
                        m_oSegment, m_oForteDocument));
                    iDeleteScope = (DeleteScope.DeleteScopeTypes)System.Convert.ToInt32(
                        XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    xPrefix = XmlExpression.Evaluate(aParams[2], m_oSegment, m_oForteDocument);
                    xExpression = aParams[3];
                    if (m_oSegment.SupportsMultipleLanguages)
                    {
                        shLCID = (short)m_oSegment.Culture;

                        if (shLCID <= 0)
                        {
                            //undefined LCID - segment culture 
                            //is a macpac "bilingual" language -
                            //parse LCID from 3rd character on of culture id
                            try
                            {
                                string xCulture = Math.Abs(m_oSegment.Culture).ToString().Substring(2);
                                shLCID = short.Parse(xCulture);
                            }
                            catch
                            {
                                throw new LMP.Exceptions.CultureIDException(
                                    LMP.Resources.GetLangString("Error_InvalidBilingualID") + " (" + shLCID.ToString() + ")");
                            }
                        }
                    }
                    else
                    {
                        //GLOG 4757: If Language control is not enabled, 
                        //set shLCID to 0 to use language setting at Tag location
                        shLCID = 0;
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xFormat", xFormat, "shUnderlineLength",
                shUnderlineLength, "iDeleteScope", iDeleteScope, "xPrefix",
                xPrefix, "xExpression", xExpression, "xDate", xDate, "shLCID", shLCID);

            if (xExpression != "")
                xFormat = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xFormat == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //m_oForteDocument.SetDynamicEditingEnvironment();

            //handle field switch
            int iPos = xFormat.ToUpper().IndexOf(" /F");
            if (iPos != -1)
            {
                bAsField = true;
                xFormat = xFormat.Remove(iPos, 3);
            }

            //standalone variables cannot have expanded delete scopes - adjust if necessary
            if ((m_oSegment == null) && (iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph))
                iDeleteScope = DeleteScope.DeleteScopeTypes.Target;

            List<SdtElement> oCCList = m_oVariable.ContainingContentControls;

            //GLOG 15773
            string xTagPrefix = Query.GetTag(oCCList[0]).Substring(0, 3);
            bool bIsSegNode = (xTagPrefix == "mps");

            //exit if value is empty and scope is already deleted
            if (bIsSegNode && (xFormat == "") && !bIgnoreDeleteScope)
                return;

            bool bExpandedScope = ((iDeleteScope >= DeleteScope.DeleteScopeTypes.Paragraph) &&
                (bIsSegNode || ((xFormat == "") && (shUnderlineLength == 0))));

            //set other argument values
            string xSegmentXML = "";
            if (bExpandedScope)
            {
                if ((m_oSegment is XmlCollectionTable) && (!m_oSegment.IsTopLevel))
                {
                    //for pleading tables, use xml of parent
                    if (m_oSegment.Parent.HasDefinition)
                        xSegmentXML = m_oSegment.Parent.Definition.XML;
                }
                else if (m_oSegment.HasDefinition)
                    xSegmentXML = m_oSegment.Definition.XML;
            }
            //Word.Range oInsertionLocation = null;
            object missing = System.Reflection.Missing.Value;

            //disable event handler that prevents deletion of mDel tags
            //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            //insert text
            try
            {
                if (bIsSegNode)
                    oCCList = DeleteScope.Restore(m_oVariable, oCCList, iDeleteScope);

                if (LMP.Architect.Base.Application.IsValidDateFormat(xFormat))
                {

                    string xDateXML = GetFormattedDateOXML(xFormat, bAsField, xPrefix, shLCID);

                    InsertSingleValueText(oCCList, xDateXML, "", shUnderlineLength, iDeleteScope,
                        xSegmentXML, m_oVariable.Name, ref bIgnoreDeleteScope);
                }
                else
                    InsertSingleValueText(oCCList, xFormat, "", shUnderlineLength, iDeleteScope,
                        xSegmentXML, m_oVariable.Name, ref bIgnoreDeleteScope);
            }
            catch (System.Exception oE)
            {
                //rethrow
                throw oE;
            }
            if (bExpandedScope)
                m_oSegment.Refresh(Base.Segment.RefreshTypes.NoChildSegments);

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// inserts specified text in a table with the specified number of columns
        /// at the tag containing the definition for this variable
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">xItemSeparator, shUnderlineLength, iDeleteScope, shNumCols, xExp</param>
        private void InsertTextAsTable(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
        {
        }

        /// <summary>
        /// inserts a checked or unchecked macrobutton that runs zzmpToggleCheckbox
        /// </summary>
        /// <param name="xValue"></param>
        private void InsertCheckbox(string xValue, string xParameters)
        {
            string xFont = "Wingdings";
            //string xCheckedASCII = "";
            //string xUnCheckedASCII = "";
            string xChar = "";

            Trace.WriteNameValuePairs("xValue", xValue);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            xChar = (xValue.ToUpper() == "TRUE") ? ((char)253).ToString() : ((char)168).ToString();
            
            List<SdtElement> oCCList = m_oVariable.ContainingContentControls;

            try
            {
                for (int i = 0; i < oCCList.Count; i++)
                {
                    //GLOG 5802:  Don't run action if Checkbox is already correct value - check stated may have already been set by macrobutton
                    if (xValue.ToUpper() != m_oVariable.GetValueFromSource(oCCList[i]).ToUpper())
                        m_oForteDocument.SetCheckbox(oCCList[i], xChar, xFont);
                }
            }
            catch (System.Exception oE)
            {
                //rethrow
                throw oE;
            }
            finally
            {
                //ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
            }
        }

        /// <summary>
        /// InsertSegment Variable Action
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        /// <param name="bIgnoreDeleteScope"></param>
        private void InsertSegment(string xValue, string xParameters,
            bool bIgnoreDeleteScope)
        {
        }
        private void ApplyStyleSheet(string xValue, string xParameters)
        {
            //GLOG 3220
            string xExpression = "";
            LMP.Data.AdminSegmentDef oDef = null;

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length > 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xExpression = aParams[0];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }
            Trace.WriteNameValuePairs("xValue", xValue, "xExpression", xExpression);

            if (xExpression != "")
                xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            if (xValue != "" && xValue != "0")
            {
                //Get Integer portion of Admin Segment ID
                if (xValue.EndsWith(".0"))
                    xValue = xValue.Substring(0, xValue.Length - 2);
                //Style Sheet Segment ID must be a numeric
                int iTest = 0;
                if (!Int32.TryParse(xValue, out iTest))
                    return;

                //validate proposed Segment ID
                oDef = GetAdminSegmentDefinition(xValue);

                //Not a valid Segment ID
                if (oDef == null)
                    return;

                //TODO: OpenXML rewrite
                m_oForteDocument.UpdateStylesFromXML(oDef.XML, m_oSegment.WPDoc);
            }
        }
        private void InsertTOA(string xValue, string xParameters)
        {
        }
        #endregion
        #region *********************protected members*********************
        /// <summary>
        /// validates conditions necessary for the insertion of a segment
        /// called by InsertSegment variable action
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="xBmk"></param>
        /// <returns></returns>
        //protected bool ValidateSegmentInsertionRange(LMP.Data.AdminSegmentDef oDef, Word.Range oRange)
        //{
        //    string xMsg = null;

        //    //validate the oDef insertionlocation option - bitwise comparison since
        //    //MenuInsertionOptions property value is a bitwise sum of several values
        //    int iInsertOpt = oDef.MenuInsertionOptions;

        //    if ((iInsertOpt & (int)XmlSegment.InsertionLocations.InsertAtSelection) !=
        //        (int)XmlSegment.InsertionLocations.InsertAtSelection)
        //    {
        //        //segment to be inserted must have MenuInsertionOptions set to Selection
        //        xMsg = LMP.Resources.GetLangString(
        //            "Msg_SegmentInsertionInvalidSegmentInsertionLocationOption");
        //        MessageBox.Show(xMsg, LMP.String.MacPacProductName,
        //            MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //        return false;
        //    }

        //    if (oRange == null)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        //bookmark exists - validate insertion range
        //        xMsg = null;
        //        bool bValidLocation = false;
        //        LMP.Forte.MSWord.TagInsertionValidityStates iValid = LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoParentTag;
        //        LMP.Forte.MSWord.WordDoc oMPDoc = new LMP.Forte.MSWord.WordDoc();
        //        Word.XMLNode oParentTag = null;
        //        Word.ContentControl oParentCC = null;
        //        //GLOG 5219: Content Control support
        //        if (Application.CurrentWordVersion == 11)
        //        {
        //            //Range.XMLParentNode can lead to access violation exceptions
        //            //in Word 2003 - uses selection object instead
        //            Word.Range oSelRng = Application.CurrentWordApp.Selection.Range;
        //            oRange.Select();
        //            oParentTag = Application.CurrentWordApp.Selection.XMLParentNode;
        //            oSelRng.Select();
        //            iValid = oMPDoc.ValidateTagInsertion(
        //                oRange, oParentTag, LMP.Forte.MSWord.TagTypes.Segment);
        //        }
        //        else if (this.m_oForteDocument.FileFormat == mpFileFormats.Binary)
        //        {
        //            oParentTag = oRange.XMLParentNode;
        //        }
        //        else
        //        {
        //            oParentCC = oRange.ParentContentControl;
        //        }

        //        if (this.m_oForteDocument.FileFormat == mpFileFormats.Binary)
        //            iValid = oMPDoc.ValidateTagInsertion(
        //                oRange, oParentTag, LMP.Forte.MSWord.TagTypes.Segment);
        //        else
        //            iValid = oMPDoc.ValidateContentControlInsertion(
        //                oRange, oParentCC, LMP.Forte.MSWord.TagTypes.Segment);

        //        switch (iValid)
        //        {
        //            case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoContainingSegmentExists:
        //                //selection is physically outside the story's existing segment tags
        //                xMsg = LMP.Resources.GetLangString("Msg_SegmentInsertionInvalidVariableOrSegmentLocation");
        //                break;
        //            case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoParentTag:
        //                //selection contains top level segment
        //                xMsg = LMP.Resources.GetLangString("Msg_SegmentInsertionInvalidSelectionContainsTopLevelSegment");
        //                break;
        //            case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidOutsideStorySegment:
        //                //selection is physically outside the story's existing segment tags
        //                xMsg = LMP.Resources.GetLangString("Msg_SegmentInsertionInvalidVariableOrSegmentLocation");
        //                break;
        //            case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidParentTag:
        //                //selection is nested inside mVar or msubVar
        //                xMsg = LMP.Resources.GetLangString("Msg_SegmentInsertionInvalidVariableParentTag");
        //                break;
        //            case LMP.Forte.MSWord.TagInsertionValidityStates.Valid:
        //            default:
        //                bValidLocation = true;
        //                break;
        //        }

        //        //message if child segment insertion range not allowed
        //        if (bValidLocation == false)
        //        {
        //            xMsg = xMsg.Replace("\\r", "\r");
        //            MessageBox.Show(xMsg, LMP.String.MacPacProductName, MessageBoxButtons.OK,
        //                            MessageBoxIcon.Warning);
        //            return false;
        //        }
        //    }
        //    return true;
        //}
        /// <summary>
        /// inserts Segment of designated ID into designated range
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters"></param>
        //private XmlSegment InsertSegmentInRange(string xSegmentID, Word.Range oRange)
        //{
        //
        //    Trace.WriteNameValuePairs("xSegmentID", xSegmentID);


        //    if (xSegmentID == "" || xSegmentID == null)
        //        return null;

        //    //validate proposed Segment ID
        //    LMP.Data.AdminSegmentDef oDef =
        //        GetAdminSegmentDefinition(xSegmentID);

        //    if (oDef == null)
        //    {
        //        return null;
        //    }

        //    //validate insertion conditions - insertion location property for segment, 
        //    //and selected range in target doc
        //    if (!ValidateSegmentInsertionRange(oDef, oRange))
        //        return null;

        //    oRange.Select();

        //    XmlPrefill oPrefill = null;

        //    object oInd = (object)"mpNewSegment";
        //    bool bReturnBookmark = false;

        //    if (this.m_oDocument.Bookmarks.Exists("mpNewSegment"))
        //    {
        //        this.m_oDocument.Bookmarks.get_Item(ref oInd).Delete();
        //        bReturnBookmark = true;
        //    }
        //    //TODO:  may want to make InsertionBehavior be a parameter
        //    XmlSegment oInsertedSeg = XmlSegment.Insert(xSegmentID
        //        , m_oSegment, XmlSegment.InsertionLocations.InsertAtSelection,
        //        XmlSegment.InsertionBehaviors.KeepExistingHeadersFooters,
        //        m_oForteDocument, oPrefill, true, null);

        //    //return mpNewSegmentBookmark if it existed before
        //    if (bReturnBookmark)
        //    {
        //        object oSegBKRange = (object)this.m_oForteDocument.Segments[0]
        //                .PrimaryRange.Characters.First;

        //        Word.Bookmark oSegBmk = m_oDocument.Bookmarks.Add("mpNewSegment", ref oSegBKRange);
        //        oSegBmk.End = oSegBmk.End - 1;
        //    }
        //    return oInsertedSeg;
        //}

        #endregion
    }
	
	/// <summary>
	/// Summary description for VariableActions.
	/// </summary>
	public class XmlVariableActions: XmlActionsCollectionBase
	{
        #region *********************enumerations*********************
        public enum Types
		{
			InsertText = 1,
			ExecuteOnBookmark = 2,
			ExecuteOnDocument = 3,
			ExecuteOnApplication = 4,
			IncludeExcludeText = 5,
			InsertCheckbox = 6,
			SetVariableValue = 7,
			RunMacro = 8,
			RunMethod = 9,
			InsertBoilerplateAtTag = 10,
			InsertBoilerplateAtLocation = 11,
			SetDocVarValue = 12,
			SetAsDefaultValue = 13,
			ExecuteOnStyle = 14,
			ExecuteOnPageSetup = 15,
			InsertTextAsTable = 16,
			InsertDate = 17,
			UpdateMemoTypePeople = 18,
			EndExecution = 19,
            ReplaceSegment = 20,
            InsertDetail = 21,
            InsertReline = 22,
            InsertSegment = 23,
            InsertDetailIntoTable = 24,
            ExecuteOnTag = 25,
            RunVariableActions = 26,
            IncludeExcludeBlocks = 27,
            SetupLabelTable = 28,
            InsertBarCode = 29,
            SetupDetailTable = 30,
            SetupDistributedSections = 31,
            ExecuteOnBlock = 32,
            ApplyStyleSheet = 33,
            SetPaperSource = 34,
            UnderlineToLongest = 35,
            InsertTOA = 36,
            SetDocPropValue = 37, //GLOG 7495
            ExecuteOnSegment = 38 //GLOG 7229
        }
        #endregion
        #region *********************fields*********************
        private XmlVariable m_oVariable;
		#endregion
        #region *********************events*********************
        #endregion
		#region *********************constructors*********************
		public XmlVariableActions(XmlVariable oVariable):base()
		{
			m_oVariable = oVariable;
		}
		#endregion
		#region *********************properties*********************
        public new XmlVariableAction this[int iIndex]
        {
            get { return (XmlVariableAction)this.ItemFromIndex(iIndex); }
        }
        public new XmlVariableAction ItemFromIndex(int iIndex)
        {
            return (XmlVariableAction)base.ItemFromIndex(iIndex);
        }
		#endregion
		#region *********************methods*********************
		public new XmlVariableAction Create()
		{
			return (XmlVariableAction) base.Create();
		}
        /// <summary>
        /// returns a clone of this variable action
        /// </summary>
        /// <param name="oSourceAction">the variable action to be cloned</param>
        /// <returns></returns>
        public XmlVariableAction Clone(XmlVariableAction oSourceAction)
        {
            //create new variable action
            XmlVariableAction oNewAction = this.Create();

            //populate with values from source action
            oNewAction.Type = oSourceAction.Type;
            oNewAction.LookupListID = oSourceAction.LookupListID;
            oNewAction.ExecutionCondition = oSourceAction.ExecutionCondition;
            oNewAction.Type = oSourceAction.Type;
            oNewAction.Parameters = oSourceAction.Parameters;

            return oNewAction;
        }

        /// <summary>
        /// executes actions
        /// </summary>
        /// <param name="bIgnoreDeleteScope">
        /// if true, expanded scopes will not be deleted when value is empty
        /// </param>
        public void Execute(bool bIgnoreDeleteScope)
        {
            XmlSegment oSegment = m_oVariable.Segment;
            if (oSegment is LMP.Architect.Base.IStaticCreationSegment && !oSegment.OverrideStaticCreationCondition)
            {
                //don't execute actions
            }
            else
            {
                //cycle through actions in collection and execute
                for (int i = 0; i < this.Count; i++)
                {
                    XmlVariableAction oAction = this[i];

                    //end execution if specified
                    if ((oAction.Type == Types.EndExecution) &&
                        (oAction.ExecutionIsSpecified()))
                        return;

                    try
                    {
                        oAction.Execute(bIgnoreDeleteScope);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.ActionException(
                            LMP.Resources.GetLangString("Error_CouldNotExecuteAction") +
                            oAction.ExecutionIndex.ToString(), oE);
                    }
                }
            }
        }

        /// <summary>
        /// executes actions
        /// </summary>
        public override void Execute()
        {
            this.Execute(false);
        }
        #endregion
		#region *********************private members*********************
        /// <summary>
        /// returns a VariableAction populated with data supplied by specified array
        /// </summary>
        /// <param name="aNewValues"></param>
        /// <returns></returns>
        protected override LMP.Architect.Base.ActionBase GetActionFromArray(string[] aNewValues)
        {
            XmlVariableAction oAction = null;

            try
            {
                oAction = new XmlVariableAction(m_oVariable);
                oAction.SetID(aNewValues[0]);
                oAction.ExecutionIndex = Convert.ToInt32(aNewValues[1]);
                oAction.LookupListID = Convert.ToInt32(aNewValues[2]);
                oAction.ExecutionCondition = aNewValues[3].ToString();
                oAction.Type = (XmlVariableActions.Types)Convert.ToInt32(aNewValues[4]);
                oAction.Parameters = aNewValues[5].ToString();
                oAction.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_InvalidVariableActionDefinition" +
                    string.Join(LMP.StringArray.mpEndOfSubValue.ToString(), aNewValues)), oE);
            }

            return oAction;
        }
        protected override LMP.Architect.Base.ActionBase GetNewActionInstance()
        {
            return new XmlVariableAction(m_oVariable);
        }
		#endregion
	}
}
