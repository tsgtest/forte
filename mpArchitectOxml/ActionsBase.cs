using System;
using System.Collections;
using System.Windows.Forms;
using System.Text;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Collections.Generic;

namespace LMP.Architect.Oxml
{
	/// <summary>
	/// defines the abstract ActionBase class, from which
	/// VariableAction, ControlAction, and SegmentAction are derived
	/// </summary>
	public abstract class XmlActionBase : LMP.Architect.Base.ActionBase
	{
        protected XmlForteDocument m_oForteDocument;
        protected XmlSegment m_oSegment;
        protected const int mpNoDeleteScopeLocationException = -2146823664;

        public XmlActionBase() { }

        #region***************************constants***************************************
        public const string mpCodeStartTag = @"[EXEC";
		public const string mpCodeEndTag = @"[\EXEC]";

        #endregion
        #region **************************methods**********************************
        /// <summary>
        /// sets the value of another variable belonging to this segment
        /// or a related segment
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">xVariableName, xExp</param>
        protected void SetVariableValue(string xValue, string xParameters)
        {
            //GLOG 6414 (dm) - skip during child structure insertion
            if (m_oForteDocument.ChildStructureInsertionInProgess)
                return;

            string xName = "";
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    xExpression = aParams[1];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName",
                xName, "xExpression", xExpression);

            if (xExpression != "")
                xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //get target segment(s)
            XmlSegment[] oTargets = m_oSegment.GetReferenceTargets(ref xName);
            if (oTargets != null)
            {
                foreach (XmlSegment oTarget in oTargets)
                {
                    //get target variable
                    XmlVariable oVar = oTarget.GetVariable(xName);

                    //if variable exists, set value
                    if (oVar != null)
                    {
                        if (oVar.Value != xValue)
                        {
                            oVar.SetValue(xValue);
                            //GLOG 3144: Handle special caption borders, since Editor event not raised here
                            //GLOG 4282: Run for any Collection Table item
                            if (oVar.Segment is XmlCollectionTableItem)
                                oVar.Segment.UpdateTableBorders();
                        }
                    }
                }
            }
        }
        /// <summary>
        /// sets preference or user application setting for user or lead author
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">key name, key type, expression</param>
        protected override void SetAsDefaultValue(string xValue, string xParameters)
        {
            string xName = "";
            LMP.Data.mpKeySetTypes iType = LMP.Data.mpKeySetTypes.UserTypePref;
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 3)
                //3 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    iType = (LMP.Data.mpKeySetTypes)System.Convert.ToByte
                        (XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument));
                    xExpression = aParams[2];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            if (iType == LMP.Data.mpKeySetTypes.FirmApp)
            {
                //firm app settings are read-only
                throw new LMP.Exceptions.KeySetException(LMP.Resources
                    .GetLangString("Error_FirmAppKeyReadOnly"));
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
                "iType", iType, "xExpression", xExpression);

            if (xExpression != "")
                xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            //entity is user or lead author
            string xEntityID = "";
            if (iType == LMP.Data.mpKeySetTypes.AuthorPref)
                try
                {
                    //get lead author id
                    xEntityID = m_oSegment.Authors.GetLeadAuthor().ID;
                }
                catch
                {
                    //no lead author
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
                }
            else
                //get user id
                xEntityID = LMP.Data.LocalPersons.GetFormattedID(LMP.Data.Application.User.ID);

            //set scope based on type
            int iScope = 0;
            if ((iType == LMP.Data.mpKeySetTypes.AuthorPref) ||
                (iType == LMP.Data.mpKeySetTypes.UserObjectPref))
                //TODO: this is temporary only - we need to implement
                //this for user segments as well
                //TODO: implement for 2-field scope id
                iScope = m_oSegment.ID1;
            else if (iType == LMP.Data.mpKeySetTypes.UserTypePref)
                iScope = (int)m_oSegment.TypeID;

            //set key value
            LMP.Data.KeySet.SetKeyValue(xName, xValue, iType, iScope.ToString(),
                xEntityID, 0, m_oSegment.Culture);
        }
        /// <summary>
        /// runs the variable actions of another variable belonging to this segment
        /// or a related segment
        /// </summary>
        /// <param name="xParameters">xVariableName</param>
        protected void RunVariableActions(string xParameters)
        {
            string xName = "";
            string[] aNames = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    //Can be list of Variable names
                    aNames = xName.Split(',');
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xName", xName);

            foreach (string xVar in aNames)
            {
                string xVarRef = xVar;
                //get target segment(s)
                XmlSegment[] oTargets = m_oSegment.GetReferenceTargets(ref xVarRef);
                if (oTargets != null)
                {
                    foreach (XmlSegment oTarget in oTargets)
                    {
                        //get target variable
                        XmlVariable oVar = oTarget.GetVariable(xVarRef.Trim());

                        //if variable exists, run actions
                        if (oVar != null)
                            oVar.VariableActions.Execute();
                    }
                }
            }
        }
        /// <summary>
        /// returns AdminSegment definition for specified segment ID
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        protected LMP.Data.AdminSegmentDef GetAdminSegmentDefinition(string xInsertedSegmentID)
        {

            string xMsg = null;

            //validate xSegmentID - message user and return is no def exists
            LMP.Data.AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs();
            LMP.Data.AdminSegmentDef oDef = null;

            try
            {
                oDef = (LMP.Data.AdminSegmentDef)oDefs
                    .ItemFromID(Int32.Parse(xInsertedSegmentID));
            }
            catch { }

            if (oDef == null)
            {
                xMsg = LMP.Resources.GetLangString("Msg_SegmentDefinitionDoesNotExist");
                Error.Show(new LMP.Exceptions.SegmentDefinitionException(xMsg + xInsertedSegmentID + "."));
                return null;
            }
            return oDef;
        }

        /// <summary>
        /// used to refesh variables collection after tags have been added or deleted
        /// as the result of the reinsertion or deletion of an expanded delete scope
        /// </summary>
        /// <param name="xVariablesAdded"></param>
        protected void RefreshVariables(string xVariablesAdded)
        {
            //when action is executed while assigning default values, the segment
            //has not yet have been added to.ForteDocument.Segments, so can't be
            //refreshed in the XML delete/insert event handlers in MacPac10 - refresh here
            if (m_oSegment.CreationStatus != XmlSegment.Status.Finished)
                m_oSegment.Refresh();

            //initialize other variables in scope
            if ((xVariablesAdded != "") && (xVariablesAdded != null))
            {
                string[] aVars = xVariablesAdded.Split(LMP.StringArray.mpEndOfField);
                for (int i = 0; i < aVars.Length; i++)
                {
                    m_oSegment.Variables.ItemFromName(aVars[i]).AssignDefaultValue();
                }
            }
        }
        /// <summary>
        /// executes the specified VBA command string on the specified bookmark
        /// </summary>
        /// <param name="xParameters">bookmark, command string</param>
        protected override void ExecuteOnBookmark(string xParameters)
        {
            string xBookmark = "";
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xBookmark = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    xCommandString = XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xBookmark", xBookmark, "xCommandString", xCommandString);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");
            try
            {
                //get bookmark element with specified name
                //get all top level mps ccs in document body
                var bmks = from bmk in m_oSegment.WPDoc.MainDocumentPart.Document.Body.Descendants<BookmarkStart>()
                           where bmk.GetAttribute("name", "http://schemas.openxmlformats.org/wordprocessingml/2006/main").Value == xBookmark
                           select bmk;

                string xID = bmks.First().GetAttribute("id", "http://schemas.openxmlformats.org/wordprocessingml/2006/main").Value.ToString();

                var bmkEnds = from bmk in m_oSegment.WPDoc.MainDocumentPart.Document.Body.Descendants<BookmarkEnd>()
                           where bmk.GetAttribute("id", "http://schemas.openxmlformats.org/wordprocessingml/2006/main").Value == xID
                           select bmk;
                //Execute command on all runs between Bookmark Start and Bookmark End
                var oRuns = from run in m_oSegment.WPDoc.MainDocumentPart.Document.Body.Descendants<Run>() where run.IsAfter(bmks.First()) && run.IsBefore(bmkEnds.First()) select run;
                for (int i = 0; i < oRuns.Count(); i++)
                {
                    ExecuteVBAStatementOnBaseElements(oRuns.ElementAt(i), xCommandString);
                }
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Bookmark - " + this.Parameters);
            }
        }

        /// <summary>
        /// executes the specified VBA command string 
        /// on the associated Word tags of the specified Block
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xParameters"></param>
        protected override void ExecuteOnBlock(string xParameters)
        {
            XmlBlock oBlock = null;
            string xCommandString = "";
            string xName = "";
            string xBlockName = "";
            string[] aNames = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    xName = aParams[1];
                    //GLOG 6266: Support comma-delimited list of Block names or Wildcard patterns
                    aNames = xName.Split(',');
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
                foreach (string xItem in aNames)
                {
                    string[] aBlocks;
                    //GLOG 6266: Support * wildcard for Block names
                    if (xItem.Contains("*"))
                    {
                        string xMatchingBlocks = "";
                        //Check only Blocks that are not currently deleted and match name
                        for (int i = 0; i < m_oSegment.Blocks.Count; i++)
                        {
                            if ((xItem.Contains("*") && m_oSegment.Blocks[i].Name.ToUpper().Contains(xItem.ToUpper().Replace("*", ""))) ||
                                (m_oSegment.Blocks[i].Name.ToUpper() == xItem.ToUpper()))
                            {
                                string xBlockTagID = m_oSegment.Blocks[i].TagID;
                                xBlockTagID = xBlockTagID.Substring(xBlockTagID.LastIndexOf(".") + 1);
                                xMatchingBlocks = xMatchingBlocks + xBlockTagID + ",";
                            }
                        }
                        xMatchingBlocks = xMatchingBlocks.TrimEnd(',');
                        aBlocks = xMatchingBlocks.Split(',');
                    }
                    else
                    {
                        aBlocks = new string[] { xItem };
                    }
                    foreach (string xVar in aBlocks)
                    {
                        xBlockName = XmlExpression.Evaluate(xVar, m_oSegment, m_oForteDocument);
                        if (xBlockName != "" && xCommandString != "")
                        {
                            //get the specified block - we'll
                            //be acting on this block
                            oBlock = m_oSegment.Blocks.ItemFromName(xBlockName);
                            Trace.WriteNameValuePairs("oBlock.Name", oBlock.Name,
                                "xCommandString", xCommandString, "xBlockName", xBlockName);
                            //replace "^=" with "=" -
                            //this will probably be a common mistake,
                            //so we'll just convert it without alerting
                            //the user
                            xCommandString = xCommandString.Replace("^=", "=");
                            try
                            {
                                //cycle through each CC, executing the
                                //instruction on the target element
                                SdtElement oCC = oBlock.AssociatedContentControl;
                                ExecuteVBAStatementOnBaseElements(oCC, xCommandString);
                            }
                            catch
                            {
                                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                                    "Error_CouldNotExecuteVariableAction") + "Execute On Block - " + this.Parameters);
                            }
                        }

                    }
                }
            }
        }
        /// <summary>
        /// executes the specified VBA command string 
        /// on the associated Word tags of the specified Variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xParameters"></param>
        protected void ExecuteOnTag(XmlVariable oVar, string xParameters)
        {
            string xCommandString = "";
            string xVariableName = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    xVariableName = XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("oVar.Name", oVar.Name,
                "xCommandString", xCommandString, "xVariableName", xVariableName);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            if (xVariableName != "")
            {
                //get the specified variable - we'll
                //be acting on this variable
                oVar = m_oSegment.Variables.ItemFromName(xVariableName);
            }

            try
            {
                //cycle through each CC, executing the
                //instruction on the target element
                foreach (SdtElement oCC in oVar.AssociatedContentControls)
                {
                    ExecuteVBAStatementOnBaseElements(oCC, xCommandString);
                }
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Content Control - " + this.Parameters);
            }
        }

        /// <summary>
        /// executes the specified VBA statement on the specified base element
        /// </summary>
        /// <param name="oBaseElement"></param>
        /// <param name="xCommandString"></param>
        private void ExecuteVBAStatementOnBaseElements(OpenXmlElement oBaseElement, string xCommandString)
        {
            OpenXmlElement[] oTargetElements = null;
            string xObjectRefString = "";
            if (xCommandString.StartsWith("[") && xCommandString.EndsWith("]"))
            {
                xCommandString = xCommandString.Substring(1, xCommandString.Length - 2);
            }
            if (xCommandString.StartsWith("EXEC "))
            {
                xCommandString = xCommandString.Substring(5);
            }
            //split on semicolumn for multiple commands
            string[] aCommands = xCommandString.Split(';');

            //cycle & execute each command
            foreach (string xCmd in aCommands)
            {
                //parse command into property name, value and object reference strings

                //get assignment position
                int iAssignPos = xCmd.IndexOf("=");

                //search left of assignment only
                int iPos = xCmd.Substring(0, iAssignPos != -1 ? iAssignPos : xCmd.Length)
                                .LastIndexOf(".");
                bool bExecutingOnRow = xCmd.ToLower().Contains("rows.item(") && !xCmd.ToLower().Contains(".cells.item(");
                if (iPos == -1)
                {
                    if (iAssignPos == -1)
                        //no object reference, no assignment - just method
                        oTargetElements[0] = oBaseElement;
                    else
                    {
                        //parse single property string from vba command string
                        xObjectRefString = xCmd.Substring(0, iAssignPos).Trim();

                        //get target element from base element and object reference string
                        oTargetElements = GetTargetElements(oBaseElement, xObjectRefString);
                    }
                }
                else
                {
                    //parse object reference string from vba command string
                    xObjectRefString = xCmd.Substring(0, iPos).Trim();

                    //get target element from base element and object reference string
                    oTargetElements = GetTargetElements(oBaseElement, xObjectRefString);
                }

                //parse property or method from vba command string
                string xMemberString = xCmd.Substring(iPos == 0 ? 0 : iPos + 1);

                //get assignment position in xMemberString if any
                iAssignPos = xMemberString.IndexOf("=");


                //cycle & execute each command
                foreach (OpenXmlElement oElement in oTargetElements)
                {
                    if (iAssignPos == -1)
                    {
                        //execute method
                        this.m_oForteDocument.RunVBAMethod(oElement, xMemberString);
                    }
                    else
                    {
                        //set property - parse property name and value
                        string xPropName = xMemberString.Substring(0, iAssignPos).Trim();
                        string xPropValue = xMemberString.Substring(iAssignPos + 1).Trim();

                        this.m_oForteDocument.SetPropertyValue(oElement, xPropName, xPropValue, bExecutingOnRow);
                    }
                }   
            }

        }

        /// <summary>
        /// returns the target element from the specified
        /// command and base content control
        /// </summary>
        /// <param name="oBaseParent"></param>
        /// <param name="xCmd"></param>
        private OpenXmlElement[] GetTargetElements(OpenXmlElement oBaseParent, string xCmd)
        {

            //create array with Base Parent
            OpenXmlElement[] oTempTargetArray = { oBaseParent };

            if (xCmd == "")
            {
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                    xCmd);
            }

           //split VBA string into array of method/property calls
            string[] aObjectProps = xCmd.Split('.');

            //cycle through array
            for (int i = 0; i <= aObjectProps.GetUpperBound(0); i++)
            {
                switch (aObjectProps[i].ToLower())
                {
                    case "bottommargin":
                    case "topmargin":
                    case "leftmargin":
                    case "rightmargin":
                    case "papersize":
                    case "verticalalignment":
                        break;
                    case "languageid":
                        oTempTargetArray = this.m_oForteDocument.GetTargetLanguageID(oTempTargetArray);
                        break;
                    case "para":
                    case "paragraphformat":
                        oTempTargetArray = this.m_oForteDocument.GetTargetParagraphFormat(oTempTargetArray);
                        break;
                    case "paragraphs":
                        oTempTargetArray = this.m_oForteDocument.GetTargetParagraphs(oTempTargetArray, aObjectProps, ref i);
                        break;
                    case "tables":
                        oTempTargetArray = this.m_oForteDocument.GetTargetTables(oTempTargetArray, aObjectProps, ref i);
                        break;
                    case "rows":
                        oTempTargetArray = this.m_oForteDocument.GetTargetRows(oTempTargetArray, aObjectProps, ref i);
                        break;
                    case "fields":
                        oTempTargetArray = this.m_oForteDocument.GetTargetFields(oTempTargetArray, aObjectProps, ref i);
                        break;
                    case "range":
                        oTempTargetArray = this.m_oForteDocument.GetTargetRange(oTempTargetArray, aObjectProps, ref i);
                        break;
                    case "font":
                    case "bold": //GLOG 
                    case "italic":
                    case "underline":
                        oTempTargetArray = this.m_oForteDocument.GetTargetRunProperties(oTempTargetArray);
                        break;
                    case "style":
                        //Just return current element
                        oTempTargetArray = new OpenXmlElement[] {oBaseParent};  // this.m_oForteDocument.GetTargetStyle(oTempTargetArray);
                        break;
                    case "text":
                        oTempTargetArray = this.m_oForteDocument.GetTargetText(oTempTargetArray);
                        break;
                    case "sections":
                        oTempTargetArray = this.m_oForteDocument.GetTargetSections(oTempTargetArray, aObjectProps, ref i);
                        break;
                    case "pagesetup":
                        //JTS 12/15/15: If accessing through Sections collection, appropriate target will already be set
                        if (xCmd.ToLower().Contains("sections"))
                            break;
                        else
                            oTempTargetArray = this.m_oForteDocument.GetTargetSections(oTempTargetArray, aObjectProps, ref i);
                        break;
                    case "borders":
                        oTempTargetArray = this.m_oForteDocument.GetTargetBorders(oTempTargetArray, aObjectProps, ref i);
                        break;
                    case "shading":
                        oTempTargetArray = this.m_oForteDocument.GetTargetShading(oTempTargetArray);
                        break;
                    default:
                        throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                            "Error_CouldNotExecuteVariableAction") + "GetTargetElement - " + this.Parameters);
                }
            }

            return oTempTargetArray;
        }


        /// <summary>
        /// executes the specified VBA command string on the specified page setup object -
        /// if iSection = -1, applies to entire document; if 0, applies to the last section
        /// </summary>
        /// <param name="xParameters">section index, command string</param>
        protected void ExecuteOnPageSetup(string xParameters)
        {

            short shSection = 0;
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    shSection = short.Parse(aParams[0]);
                    xCommandString = XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("shSection", shSection, "xCommandString", xCommandString);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            short shStartSection = shSection;
            short shEndSection = shSection;

            Body oMainDocument = m_oSegment.WPDoc.MainDocumentPart.Document.Body;

            //get section count
            int iSecCount = oMainDocument.Descendants().OfType<SectionProperties>().Count();

            //JTS 12/15/15
            if (shSection == 0)
            {
                //Execute on last section only
                shStartSection = (short)iSecCount;
                shEndSection = (short)iSecCount;
            }
            else if (shSection == -1)
            {
                //Execute on all sections in Parent Segment
                shStartSection = 1;
                shEndSection = (short)iSecCount;
            }

            try
            {
                for (short sec = shStartSection; sec <= shEndSection; sec++)
                {
                    //pass sectionproperties objects
                    SectionProperties oSecProp = oMainDocument.Descendants().OfType<SectionProperties>().ElementAt(sec -1);
                    ExecuteVBAStatementOnBaseElements(oSecProp, xCommandString);
                }
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Page Setup - " + this.Parameters);
            }

        }


        /// <summary>
        /// executes the specified VBA command string 
        /// on xxx
        /// </summary>
        /// <param name="xParameters">xxx</param>
        protected override void ExecuteOnApplication(string xParameters)
        {
            string xMsg = LMP.Resources.GetLangString("Error_ActionNotImplemented");
            Error.Show(new LMP.Exceptions.UnsupportedActionException(xMsg));
            //MessageBox.Show(xMsg, LMP.String.MacPacProductName, 
            //                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// executes the specified VBA command string 
        /// on the associated Segment of the specified Variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xParameters"></param>
        protected override void ExecuteOnSegment(string xParameters)
        {
            string xCommandString = "";
            string xSegmentID = "";
            XmlSegment[] aSegments = null;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                    xSegmentID = XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xCommandString", xCommandString, "xSegmentID", xSegmentID);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            if (xSegmentID != "")
            {
                if (aParams[1].ToUpper().Contains("CHILD"))
                {
                    //Target Segment is a child of this segment
                    aSegments = XmlSegment.FindSegments(xSegmentID, m_oSegment.Segments);
                }
                else
                {
                    //Search entire document for matching segment
                    aSegments = XmlSegment.FindSegments(xSegmentID, m_oForteDocument.Segments);
                }
            }
            else
            {
                aSegments = new XmlSegment[] { m_oSegment };
            }
            try
            {
                foreach (XmlSegment oSegment in aSegments)
                {
                    SdtElement oCC = oSegment.RawSegmentParts[0].ContentControl;
                    ExecuteVBAStatementOnBaseElements(oCC, xCommandString);
                }
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Segment - " + this.Parameters);
            }

        }
        /// <summary>
        /// executes the specified command on the specified style
        /// </summary>
        /// <param name="xParameters">style name, command string</param>
        protected void ExecuteOnStyle(string xParameters)
        {
            string xStyle = "";
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);

            try
            {
                //get parameters
                xStyle = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                xCommandString = XmlExpression.Evaluate(aParams[1], m_oSegment, m_oForteDocument);
            }
            catch (System.Exception oE)
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                    xParameters, oE);
            }

            Trace.WriteNameValuePairs("xStyle", xStyle, "xCommandString", xCommandString);

            if (xStyle == "" || xCommandString == "")
            {
                //one of the parameters is invalid
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                    xParameters);
            }

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            try
            {
                //GLOG 8673: Disregard StylesWithEffectsPart - only used by Word 2010
                Styles oStyles = m_oSegment.WPDoc.MainDocumentPart.StyleDefinitionsPart.Styles;
                if (oStyles == null)
                    return;
                string xs = oStyles.InnerXml;
                //Locate Style node corresponding to named style
                //JTS: If for some reason style definition exists more than once in XML, only last instance will apply
                Style oStyle = oStyles.Elements<Style>().Where(s => s.StyleName.Val.Value.ToUpper() == xStyle.ToUpper()).LastOrDefault();
                if (oStyle != null)
                {
                    try
                    {
                        ExecuteVBAStatementOnBaseElements(oStyle, xCommandString);
                        oStyles.Save();
                    }
                    catch
                    {
                        throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                            "Error_CouldNotExecuteVariableAction") + "Execute On Style - " + this.Parameters);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Style - " + this.Parameters);
            }
        }

        /// <summary>
        /// executes the specified VBA command string on the document
        /// </summary>
        /// <param name="xParameters">command string</param>
        protected override void ExecuteOnDocument(string xParameters)
        {
            string xCommandString = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xCommandString = XmlExpression.Evaluate(aParams[0], m_oSegment, m_oForteDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xCommandString", xCommandString);

            //replace "^=" with "=" -
            //this will probably be a common mistake,
            //so we'll just convert it without alerting
            //the user
            xCommandString = xCommandString.Replace("^=", "=");

            try
            {
                ExecuteVBAStatementOnBaseElements(m_oSegment.WPDoc.MainDocumentPart.Document, xCommandString);
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute On Document - " + this.Parameters);
            }
            finally
            {
            }
        }

        /// <summary>
        /// executes embedded VBA Commands on the associated 
        /// Word tag range
        /// </summary>
        /// <param name="oBaseElement"></param>
        protected void ExecuteEmbeddedCommands(SdtElement oCC)
        {
            string xInstructions = "";
            string xText = "";
            OpenXmlElement oExecRun = null;
            int iEndStartTagPos = 0;
            int iSwitchPos = 0;
            int iCurPos = 0;
            int iNumUnMatchedSETCodes = 0;
            int iNumSETCodes = 0;
            int iStartTagPosTemp = 0;
            int iEndTagPos = 0;
            string xEXECode = "";
            string xSwitch = "";

            try
            {
                //find the first SET code in oCC range
                Text oText = oCC.Descendants<Text>().Where(t => t.Text.Contains(mpCodeStartTag)).FirstOrDefault();

                if (oText != null)
                {
                    Run oRun = (Run)oText.Parent;

                    //get text string
                    xText = oText.Text;

                    //get start of Set code
                    int iStartTagPos = xText.ToUpper().IndexOf(mpCodeStartTag);

                    //cycle through SET codes
                    //get actual range scope & exec code
                    while (iStartTagPos >= 0)
                    {
                        List<OpenXmlElement> oCodeScope = new List<OpenXmlElement>(); ;

                        //get end of start tag
                        iEndStartTagPos = xText.ToUpper().IndexOf("]", iStartTagPos);

                        //reset at start of loop
                        iEndTagPos = -1;

                        //get exec code
                        xEXECode = xText.Substring(iStartTagPos, iEndStartTagPos - iStartTagPos + 1);

                        //get switches - look for first '/' and
                        //take everything after it
                        iSwitchPos = xEXECode.IndexOf(" /");

                        if (iSwitchPos >= 0)
                        {
                            //create new run elements out of start & end tag
                            Query.SplitRun(oRun, new int[] { iStartTagPos, iEndStartTagPos + 1 });

                            //get new run containing only Exec code
                            oExecRun = (Run)oCC
                                            .Descendants<Text>()
                                            .Where(t => t.Text.Contains(mpCodeStartTag))
                                            .FirstOrDefault()
                                            .Parent;

                            xText = oExecRun.InnerText;
                            
                            //modify code scope based on switch   
                            xSwitch = xEXECode.Substring(iSwitchPos, (xEXECode.Length - 1) - iSwitchPos)
                                            .Trim().ToUpper();

                            switch (xSwitch)
                            {
                                case "/P":
                                    //get run elements from start of code to end of paragraph
                                    foreach (Run oR in oExecRun.ElementsAfter())
                                        oCodeScope.Add(oR);
                                    break;
                                case "/S":
                                    //get run elements from start of paragraph to beginning of code
                                    foreach (Run oRR in oExecRun.ElementsBefore())
                                        oCodeScope.Add(oRR);
                                    break;
                                case "/G":
                                    //todo? - get all paragraphs in range?
                                    //get the parent paragraph
                                    Paragraph oPara = oExecRun.Ancestors<Paragraph>().FirstOrDefault();
                                    oCodeScope.Add(oPara);
                                    break;
                                case "/L":
                                case "/N":
                                default:
                                    //do nothing
                                    break;
                            }

                            //apply switch to code scope
                            //ApplySwitchToCodeScope oCodeScope, xSwitch, iStartTagPos, iEndStartTagPos,xEXECCode
                        }
                        else
                        {
                            //get end of that code range from [\EXEC]
                            //need to account for nested codes
                            xSwitch = "";
                            iCurPos = iEndStartTagPos;
                            iNumUnMatchedSETCodes = 1;
                            iNumSETCodes = 1;

                            iStartTagPosTemp = iStartTagPos;

                            while (iNumUnMatchedSETCodes > 0 || (iStartTagPos == -1 && iEndTagPos == -1))  //(iNumUnMatchedSETCodes > 0 || (iStartTagPos == 0 && iEndTagPos == 0))
                            {
                                //see if next tag is a Start or End tag
                                iStartTagPosTemp = xText.IndexOf(mpCodeStartTag, iCurPos + 1);
                                iEndTagPos = xText.IndexOf(mpCodeEndTag, iCurPos + 1);

                                if (iEndTagPos == -1 || (iEndTagPos < iCurPos))  //(iEndTagPos == 0 || (iEndTagPos < iCurPos))
                                    //unmatch tag found, throw error
                                    throw new LMP.Exceptions.ArgumentException(
                                            LMP.Resources.GetLangString("Error_InvalidEXECCode") + xEXECode);
                                else if (iStartTagPosTemp == -1 || (iStartTagPosTemp > iEndTagPos))  //(iStartTagPosTemp == 0 || (iStartTagPosTemp > iEndTagPos))
                                {
                                    //next tag is End tag
                                    //decrement unmatched tag count
                                    iNumUnMatchedSETCodes = iNumUnMatchedSETCodes - 1;
                                    iCurPos = iEndTagPos;
                                    iNumSETCodes = iNumSETCodes + 1;
                                }
                                else
                                {
                                    //next tag is Start tag
                                    //increment unmatched tag count
                                    iNumUnMatchedSETCodes = iNumUnMatchedSETCodes + 1;
                                    iCurPos = iStartTagPosTemp;
                                    iNumSETCodes = iNumSETCodes + 1;
                                }
                            }

                            iEndTagPos = iCurPos;

                            int iStart = iStartTagPos;
                            int iEnd = 0;

                            if (iEndTagPos >= xText.Length)
                                iEnd = iEndTagPos;
                            else
                                iEnd = iEndTagPos + mpCodeEndTag.Length;
                            
                            //create new run elements out of start & end tag
                            Query.SplitRun(oRun, new int[] { iStart, iEnd });
                            
                            //get new run containing only Exec code
                            oExecRun = (Run)oCC
                                            .Descendants<Text>()
                                            .Where(t => t.Text.Contains(mpCodeStartTag))
                                            .FirstOrDefault()
                                            .Parent;

                            //insert into array
                            oCodeScope.Add(oExecRun);

                            xText = oExecRun.InnerText;
                        }
                        
                        //format scope
                        if (xSwitch == "/L" || xSwitch == "/N")
                        {
                            //do nothing
                        }
                        else
                        {
                            //remove code brackets if necessary
                            if (xEXECode.Substring(0,1) == "[" &&
                                xEXECode.Substring(xEXECode.Length - 1, 1) == "]")
	                        {
		                        xEXECode = xEXECode.Substring(1, xEXECode.Length - 2);
	                        }

                            //remove any switches
                            xEXECode = xEXECode.Replace("/L","");
                            xEXECode = xEXECode.Replace("/P","");
                            xEXECode = xEXECode.Replace("/S","");
                            xEXECode = xEXECode.Replace("/N","");
                            xEXECode = xEXECode.Replace("/G","");
    
                            if (xEXECode.Substring(0,5) == "EXEC ")
	                        {
                                //get instructions from EXEC code
		                        xInstructions = xEXECode.Substring(4).Trim();
	                        }
                            else
	                        {
                                xInstructions = xEXECode;
	                        }
    
                            //get instructions
                            string[] aInstructions = xInstructions.Split(';');

                            //cycle through instructions, parsing/executing each
                            for (int i = 0; i < aInstructions.Length; i++)
                            {
                                string xCommandString = aInstructions[i];

                                if (xCommandString != "")
	                            {
		                            //replace all shorthand with proper syntax
                                    switch (xCommandString.ToUpper())
	                                {
                                        case "B":
                                            xCommandString = "Font.Bold=True";
                                            break;
                                        case "I":
                                            xCommandString = "Font.Italic=True";
                                            break;
                                        case "U":
                                            xCommandString = "Font.Underline=True";
                                            break;
                                        case "S":
                                            xCommandString = "Font.SmallCaps=True";
                                            break;
                                        case "A":
                                            xCommandString = "Font.AllCaps=True";
                                            break;
		                                default:
                                            if (String.IsNumericInt32(xCommandString))
	                                        {
                                                //shorthand for font.size
                                                //Ignore size values outside of allowed range
                                                if (System.Convert.ToInt32(xCommandString) >= 1 &&
                                                    System.Convert.ToInt32(xCommandString) <= 1638)
                                                    xCommandString = "Font.Size=" + xCommandString;
                                                else
                                                    continue;
	                                        }
                                            break;
	                                }

                                    //execute on run Elements
                                    foreach (OpenXmlElement oElement in oCodeScope)
                                    {
                                        ExecuteVBAStatementOnBaseElements(oElement, xCommandString);
                                    }
	                            }                                
                            }
                        }
                        //cleanup
                        if (iSwitchPos >= 0)
                        {
                            oExecRun.Remove();
                            ////remove end tag if existing
                            //xText = xText.Substring(0, xText.IndexOf("]") - 3);

                            ////remove start tag
                            //xText = xText.Substring(mpCodeStartTag.Length + 1);
                        }
                        else
                        {
                            //remove end tag if existing
                            if (iEndTagPos >= 0)
                                xText = xText.Substring(0, xText.Length - mpCodeEndTag.Length);    

                        //remove start tag
                        xText = xText.Substring(xText.IndexOf("]") + 1);

                            //set Text element of Exec run
                            oExecRun.Elements<Text>().FirstOrDefault().Text = xText;
                        }

                        //find next SET code in oCC range
                        oText = oCC.Descendants<Text>().Where(t => t.Text.Contains(mpCodeStartTag)).FirstOrDefault();

                        if (oText != null)
                        {
                            oRun = (Run)oText.Parent;

                            //get text string
                            xText = oText.Text;

                            //get start of Set code
                            iStartTagPos = xText.ToUpper().IndexOf(mpCodeStartTag);
                        }
                        else
                            iStartTagPos = -1;
                    }   //loop while iStartTagPos >= 0
                }
            }
            catch
            {
                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                    "Error_CouldNotExecuteVariableAction") + "Execute Embedded Commands on " + this.Parameters);
            }
        }

        protected void SetPaperSource(string xValue, string xParameters)
        {
        }
        /// <summary>
        /// Formats all tags for the specified Variable
        /// to Add/Remove Underlining to length of longest line
        /// </summary>
        /// <param name="oSourceVar"></param>
        /// <param name="xParameters"></param>
        protected void UnderlineTagToLongest(XmlVariable oSourceVar, string xParameters)
        {
        }
        /// <summary>
        /// sets Word document variable
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">variable name, expression</param>
        protected override void SetDocVarValue(string xValue, string xParameters)
        {
            string xName = "";
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    xExpression = aParams[1];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
                "xExpression", xExpression);

            //raise error if variable name is empty
            if (xName == "")
            {
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
            }

            if (xExpression != "")
                xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;
            this.m_oForteDocument.WDSetDocVarValue(m_oSegment.WPDoc, xName, xValue);
        }

        /// <summary>
        /// sets Word custom document property
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xParameters">variable name, expression</param>
        protected override void SetDocPropValue(string xValue, string xParameters)
        {
            //GLOG 7495: Set Built-in or Custom Document Property
            string xName = "";
            string xExpression = "";

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xName = aParams[0];
                    xExpression = aParams[1];
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xValue", xValue, "xName", xName,
                "xExpression", xExpression);

            //raise error if variable name is empty
            if (xName.ToString() == "")
            {
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
            }

            if (xExpression != "")
                xValue = XmlExpression.Evaluate(xExpression, m_oSegment, m_oForteDocument);

            //exit if value of assignment is mpUndefined
            if (xValue == ((int)LMP.mpTriState.Undefined).ToString())
                return;

            if (xValue.Length > 255)
            {
                //Text Doc Properties allow maximum of 255 characters
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters);
            }
            //First try accessing as built-in property
            if (this.m_oForteDocument.WDIsCorePropertyName(xName))
            {
                this.m_oForteDocument.WDSetCoreProperty(m_oSegment.WPDoc, xName, xValue);
            }
            else if (this.m_oForteDocument.WDIsAppPropertyName(xName))
            {
                this.m_oForteDocument.WDSetAppProperty(m_oSegment.WPDoc, xName, xValue);
            }
            else
            {
                //not a built-in property
                this.m_oForteDocument.WDSetCustomProperty(m_oSegment.WPDoc, xName, xValue);
            }
        }
        /// <summary>
        /// replaces all instances of the specified
        /// segment with the specified segment
        /// </summary>
        /// <param name="xParameters"></param>
        protected override void ReplaceSegment(string xParameters)
        {

            //GLOG 6687:  Don't ever run this action in Design Mode
            if (m_oForteDocument.Mode == XmlForteDocument.Modes.Design)
                return;

            string xExistingSegmentID = null;
            string xNewSegmentID = null;
            bool bUsePrefill = false;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            //GLOG 3247: 3 parameters are supported, but still allow 2 for older content
            if (aParams.Length < 2)
                //2 parameters are required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xExistingSegmentID = XmlExpression.Evaluate(
                        aParams[0], m_oSegment, m_oForteDocument);
                    xNewSegmentID = XmlExpression.Evaluate(
                        aParams[1], m_oSegment, m_oForteDocument);
                    if (aParams.Length > 2)
                        bool.TryParse(XmlExpression.Evaluate(aParams[2], m_oSegment, m_oForteDocument), out bUsePrefill);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xExistingSegmentID", xExistingSegmentID,
                "xNewSegmentID", xNewSegmentID);

            //GLOG 3499 - strip fixed admin segment id2 before comparing ids
            xExistingSegmentID = xExistingSegmentID.Replace(".0", "");
            xNewSegmentID = xNewSegmentID.Replace(".0", "");

            if (xExistingSegmentID == "" || xNewSegmentID == "" || xExistingSegmentID == xNewSegmentID)
                //the new segment is the same as the old segment -
                //replacement is superfluous
                return;

            //GLOG 3719 (dm) - added support for comma delimited list of ids and
            //specific handling for switching psigs between table and non-table format
            xExistingSegmentID = xExistingSegmentID.Replace(" ", "");
            string[] aExistingIDs = xExistingSegmentID.Split(',');
            xNewSegmentID = xNewSegmentID.Replace(" ", "");
            string[] aNewIDs = xNewSegmentID.Split(',');
            if (aExistingIDs.Length != aNewIDs.Length)
            {
                //one-to-one mapping is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                    xParameters);
            }

            //get the object type of first segment on each list
            LMP.Data.mpObjectTypes oExistingType =
                LMP.Data.Application.GetSegmentTypeID(System.Int32.Parse(aExistingIDs[0]));
            LMP.Data.mpObjectTypes oNewType =
                LMP.Data.Application.GetSegmentTypeID(System.Int32.Parse(aNewIDs[0]));

            //if (oExistingType != Data.mpObjectTypes.Letterhead && oExistingType != Data.mpObjectTypes.PleadingPaper)
            //{
            //    //TODO: Only run this action for Paper types
            //    return;
            //}
            //TODO for OXML*****************************************************
            //if (((oExistingType == LMP.Data.mpObjectTypes.PleadingSignature) &&
            //    (oNewType == LMP.Data.mpObjectTypes.PleadingSignatureNonTable)) ||
            //    ((oExistingType == LMP.Data.mpObjectTypes.PleadingSignatureNonTable) &&
            //    (oNewType == LMP.Data.mpObjectTypes.PleadingSignature)))
            //{
            //    //call dedicated method to switch between table and non-table format
            //    this.ChangePleadingSignatureFormat((oExistingType ==
            //        LMP.Data.mpObjectTypes.PleadingSignatureNonTable), aExistingIDs,
            //        aNewIDs, bUsePrefill);
            //    return;
            //}

            for (int i = 0; i < aExistingIDs.Length; i++)
            {
                //GLOG 3564: Get Parent of matching segment for use with Replace
                //This allows action to work on Segments that are not a child of this segment
                XmlSegment oParent = null;
                XmlPrefill oPrefill = null;
                XmlSegment[] aSegments;
                //Since Trailer and DraftStamp won't have a Parent Segment
                //set search scope to entire document, otherwise limit to children of current Segment
                if (oExistingType == LMP.Data.mpObjectTypes.Trailer ||
                    oExistingType == LMP.Data.mpObjectTypes.DraftStamp)
                {
                    aSegments = XmlSegment.FindSegments(aExistingIDs[i], m_oForteDocument.Segments);
                }
                else
                {
                    aSegments = XmlSegment.FindSegments(aExistingIDs[i], m_oSegment.Segments);
                }
                if (aSegments.Length > 0)
                {
                    if (bUsePrefill)
                    {
                        //GLOG 3247: Get existing values to prefill new segment
                        oPrefill = new XmlPrefill(aSegments[0]);
                    }
                    oParent = aSegments[0].Parent;
                }
                //replace the segment with the new one
                XmlSegment.Replace(aNewIDs[i], aExistingIDs[i], oParent, m_oForteDocument, oPrefill, null, false);
                //oParent.Refresh();
                //GLOG 4845: This is no longer necessary, because Replace should preserve index
                ////GLOG 4519: If replacing Collection item, Refresh ForteDocument so that editor tree
                ////reflects correct order of segments in document
                //if (oParent != null && oParent.IsTransparent) //GLOG 4584
                //    m_oForteDocument.Refresh(ForteDocument.RefreshOptions.None, false);

                ////notify UI that containing Segment has been refreshed
                //m_oForteDocument.RaiseSegmentRefreshedByActionEvent(oParent);
            }
        }

        #endregion
		#region *********************private members*********************
        #endregion
	}

	/// <summary>
	/// defines the abstract ActionsCollection class, from which
	/// VariableActions, ControlActions, and SegmentActions are derived
	/// </summary>
	public abstract class XmlActionsCollectionBase : LMP.Architect.Base.ActionsCollectionBase
	{
        //#region *********************fields*********************
        //private ArrayList m_oActions = new ArrayList();
        //#endregion
        //#region *********************events*********************
        //internal event LMP.Architect.Base.ActionsDirtiedEventHandler ActionsDirtied;
        //#endregion
        //#region *********************constructors*********************
        //public XmlActionsCollectionBase()
        //{
        //}
        //#endregion
        //#region *********************properties*********************
        //public XmlActionBase this[int iIndex]
        //{
        //    get{return this.ItemFromIndex(iIndex);}
        //}
        //public int Count
        //{
        //    get
        //    {
        //        return m_oActions.Count;
        //    }
        //}
        //#endregion
        //#region *********************methods*********************
        ///// <summary>
        ///// returns a new ActionBase
        ///// </summary>
        ///// <returns></returns>
        //protected XmlActionBase Create()
        //{
        //    try
        //    {
        //        ////save current edits to collection
        //        //this.UpdateCollectionWithCurrentAction();

        //        //request the new object of the appropriate type -
        //        //GetNewActionInstance is abstract, and hence
        //        //implemented in each derived class
        //        XmlActionBase oAction = this.GetNewActionInstance();
        //        oAction.ActionDirtied += new LMP.Architect.Base.ActionDirtiedEventHandler(OnActionDirtied);
				
        //        //flag new action so it can be added to internal collection
        //        oAction.IsDirty = true;
        //        return oAction;
        //    }
        //    catch(System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString(
        //            "Error_ActionCreationFailed"), oE);
        //    }
        //}

        ///// <summary>
        ///// returns the variable ActionBase with the specified index
        ///// </summary>
        ///// <param name="iIndex"></param>
        ///// <returns></returns>
        //public XmlActionBase ItemFromIndex(int iIndex)
        //{
        //    Trace.WriteNameValuePairs("iIndex", iIndex);

        //    ////save current edits to the collection
        //    //this.UpdateCollectionWithCurrentAction();

        //    //check for valid supplied index
        //    if(iIndex < 0 || iIndex > m_oActions.Count - 1)
        //    {
        //        throw new LMP.Exceptions.NotInCollectionException(
        //            LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + 
        //            iIndex.ToString());
        //    }

        //    //get the array that defines data for the ActionBase
        //    string[] aAction = (string[]) m_oActions[iIndex];

        //    //get an object from the array - set as current object
        //    XmlActionBase oAction = GetActionFromArray(aAction);

        //    //subscribe to notifications that this action has been dirtied (edited)
        //    oAction.ActionDirtied += new LMP.Architect.Base.ActionDirtiedEventHandler(OnActionDirtied);
        //    return oAction;
        //}

        ///// <summary>
        ///// deletes the specified variable ActionBase
        ///// </summary>
        ///// <param name="iIndex"></param>
        //public virtual void Delete(int iIndex)
        //{
        //    try
        //    {
        //        Trace.WriteNameValuePairs("iIndex", iIndex);

        //        ////save current edits to collection
        //        //this.UpdateCollectionWithCurrentAction();

        //        //test for valid index
        //        if(iIndex < 0 || iIndex > m_oActions.Count - 1)
        //        {
        //            throw new LMP.Exceptions.NotInCollectionException(
        //                LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + 
        //                iIndex.ToString());
        //        }

        //        //remove from array list
        //        m_oActions.RemoveAt(iIndex);

        //        //adjust all execution indices from deletion index to last index
        //        for(int i=iIndex; i<m_oActions.Count; i++)
        //        {
        //            //reassign execution index
        //            ((string[]) m_oActions[i])[1] = (i + 1).ToString();
        //        }
			
        //        //alert subscribers that collection has changed
        //        if(this.ActionsDirtied != null)
        //            this.ActionsDirtied(this, new EventArgs());
        //    }
        //    catch(System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString(
        //            "Error_ActionDeletionFailed") + iIndex.ToString(), oE);
        //    }
        //}

        ///// <summary>
        ///// sets the internal storage for this collection
        ///// from a delimited string - should be called only 
        ///// by the Variables class -
        /////this is needed because variable actions are stored
        /////as an index in the variable array - it's faster than
        /////having to retrieve and parse the data a second time from 
        /////the object data attribute
        ///// </summary>
        ///// <param name="oActions"></param> 
        //public void SetFromString(string xActions)
        //{
        //    try
        //    {
        //        Trace.WriteNameValuePairs("xActions", xActions);

        //        //clear internal storage
        //        m_oActions.Clear();

        //        if(xActions != "")
        //        {
        //            //convert actions string into array of ActionBase def strings
        //            string[] aActionStrings = xActions.Split(LMP.StringArray.mpEndOfRecord);
        //            //cycle through ActionBase def strings, converting each to an array
        //            //and adding to the sorted list
        //            for(int i=0; i <= aActionStrings.GetUpperBound(0); i++)
        //            {
        //                //get ActionBase def string - add an ID to beginning -
        //                //this will be the ID of the ActionBase - we need IDs only
        //                //at run time (it's better not to take up space in document)
        //                string xAction = System.Guid.NewGuid().ToString() + 
        //                    LMP.StringArray.mpEndOfSubValue + aActionStrings[i];

        //                //split into array
        //                string[] aAction = xAction.Split(LMP.StringArray.mpEndOfSubValue);

        //                if(aAction.GetUpperBound(0) != 5)
        //                    //bad ActionBase string
        //                    throw new LMP.Exceptions.DataException(
        //                        LMP.Resources.GetLangString(
        //                        "Error_InvalidVariableActionDefinition" + xAction));

        //                //add to array list at position specified by ExecutionIndex (array index 1)
        //                m_oActions.Insert(Convert.ToInt32(aAction[1]) - 1, aAction);
        //            }
        //        }
        //    }
        //    catch(System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString(
        //            "Error_CouldNotPopulateActionsCollection"), oE);
        //    }
        //}

        ///// <summary>
        ///// returns a VariableAction populated with data supplied by specified array
        ///// </summary>
        ///// <param name="aNewValues"></param>
        ///// <returns></returns>
        //protected abstract XmlActionBase GetActionFromArray(string[] aNewValues);
        ///// <summary>
        ///// derived class will implement to return a new instance of the individual
        ///// </summary>
        ///// <returns></returns>
        //protected abstract XmlActionBase GetNewActionInstance();
        ///// <summary>
        ///// executes the appropriate actions
        ///// </summary>
        //public virtual void Execute()
        //{
        //    //cycle through segment actions, executing actions in the collection
        //    for(int i=0; i<this.Count; i++)
        //    {
        //        XmlActionBase oAction = (XmlActionBase) this[i];

        //        try
        //        {
        //            oAction.Execute();
        //        }
        //        catch(System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.ActionException(
        //                LMP.Resources.GetLangString("Error_CouldNotExecuteAction") + 
        //                oAction.ExecutionIndex.ToString(), oE);
        //        }
        //    }
        //}

        ///// <summary>
        ///// Moves up the ExecutionIndex of the action
        ///// </summary>
        //public void MoveUp(XmlActionBase oAction)
        //{
        //    try
        //    {
        //        //move up if necessary
        //        if (oAction.ExecutionIndex > 1)
        //        {
        //            //get array of values
        //            string[] aNewValues = oAction.ToArray(true);

        //            //the execution index has changed-
        //            //remove from current array position
        //            m_oActions.RemoveAt(oAction.ExecutionIndex - 1);

        //            //re-insert at next index up
        //            m_oActions.Insert(oAction.ExecutionIndex - 2, aNewValues);

        //            //increment execution index of supplied var
        //            oAction.ExecutionIndex = oAction.ExecutionIndex - 1;

        //            ////we need to update execution indexes
        //            for (int iCounter = 0; iCounter < m_oActions.Count; iCounter++)
        //                //reassign execution index
        //                ((string[])m_oActions[iCounter])[1] = (iCounter + 1).ToString();


        //            //alert subscribers that collection has changed
        //            if (this.ActionsDirtied != null)
        //                this.ActionsDirtied(this, new EventArgs());
        //        }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString("Error_CouldNotMoveUpAction") +
        //            oAction.ExecutionIndex.ToString(), oE);
        //    }

        //}

        ///// <summary>
        ///// Moves down the ExecutionIndex of the action
        ///// </summary>
        //public void MoveDown(XmlActionBase oAction)
        //{
        //    try
        //    {
        //        //move down if necessary
        //        if (oAction.ExecutionIndex < m_oActions.Count)
        //        {
        //            //get array of values
        //            string[] aNewValues = oAction.ToArray(true);

        //            //the execution index has changed-
        //            //remove from current array position
        //            m_oActions.RemoveAt(oAction.ExecutionIndex - 1);

        //            //re-insert at next index down
        //            m_oActions.Insert(oAction.ExecutionIndex, aNewValues);

        //            ////we need to update execution indexes
        //            for (int iCounter = 0; iCounter < m_oActions.Count; iCounter++)
        //                //reassign execution index
        //                ((string[])m_oActions[iCounter])[1] = (iCounter + 1).ToString();


        //            //alert subscribers that collection has changed
        //            if (this.ActionsDirtied != null)
        //                this.ActionsDirtied(this, new EventArgs());
        //        }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString("Error_CouldNotMoveDownAction") +
        //            oAction.ExecutionIndex.ToString(), oE);
        //    }
        //}

        ///// <summary>
        ///// returns the collection of actions as a string
        ///// </summary>
        ///// <returns></returns>
        //public override string ToString()
        //{
        //    StringBuilder oSB = new StringBuilder();

        //    //cycle through actions, appending each to strin
        //    for (int i = 0; i < this.Count; i++)
        //    {
        //        oSB.AppendFormat("{0}{1}", this[i].ToString(),
        //            LMP.StringArray.mpEndOfRecord.ToString());
        //    }

        //    //trim trailing record delimiter
        //    return oSB.ToString(0, Math.Max(oSB.Length - 1,0));
        //}

        //#endregion
        //#region *********************private members*********************
        ///// <summary>
        ///// saves the specified ActionBase to internal storage
        ///// </summary>
        //private void SaveAction(XmlActionBase oAction)
        //{
        //    DateTime t0 = DateTime.Now;

        //    try
        //    {
        //        //end if no current action or current action has no edits
        //        if(oAction  == null || !oAction.IsDirty)
        //            return;

        //        //ensure valid data
        //        if(!oAction.IsValid)
        //            throw new LMP.Exceptions.DataException(
        //                LMP.Resources.GetLangString(
        //                "Error_InvalidActionDefinition") + oAction.ToString());

        //        //IDs are assigned only when item is saved
        //        bool bIsNew = (oAction.ID == "");

        //        if(bIsNew)
        //            //get ID for item
        //            oAction.SetID();
				
        //        //if execution index is not specified, set to last in collection
        //        if(oAction.ExecutionIndex == 0)
        //            oAction.ExecutionIndex = m_oActions.Count + 1;

        //        //get array of values
        //        string[] aNewValues = oAction.ToArray(true);
			
        //        if(bIsNew)
        //        {
        //            //VariableAction is new-
        //            //insert into array list
        //            int iExecutionIndex = oAction.ExecutionIndex;
			
        //            //if execution index is not specified, set to last in collection
        //            if(iExecutionIndex == 0)
        //                iExecutionIndex = m_oActions.Count + 1;

        //            if(iExecutionIndex > m_oActions.Count)
        //                //specified execution index is greater 
        //                //than the last execution index - add
        //                //to end of list
        //                m_oActions.Add(aNewValues);
        //            else
        //            {
        //                //insert at position specified by ExecutionIndex
        //                m_oActions.Insert(iExecutionIndex - 1, aNewValues);

        //                //adjust all appropriate execution indices
        //                for(int i=iExecutionIndex; i < m_oActions.Count; i++)
        //                    ((string[]) m_oActions[i])[1] = (i + 1).ToString();
        //            }
        //        }
        //        else
        //        {
        //            //save existing ActionBase - cycle through actions
        //            //looking for the ActionBase we want to save
        //            for(int i=0; i < m_oActions.Count; i++)
        //            {
        //                string[] aOldValues = (string[]) m_oActions[i];

        //                if(aOldValues[0] == oAction.ID)
        //                {
        //                    //we've found the ActionBase in the collection-
        //                    //replace the item with the updated array
        //                        m_oActions[i] = aNewValues;

        //                    break;
        //                }
        //            }
        //        }

        //        LMP.Benchmarks.Print(t0);
        //    }
        //    catch(System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.ActionException(
        //            LMP.Resources.GetLangString(
        //            "Error_CouldNotUpdateCollection"), oE);
        //    }
        //}
        ///// <summary>
        ///// handles notification from action that it has been dirtied
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void OnActionDirtied(object sender, EventArgs e)
        //{
        //    //save action
        //    SaveAction((XmlActionBase)sender);

        //    if(ActionsDirtied != null)
        //        //notify subscribers that the collection has been edited
        //        ActionsDirtied(this, new EventArgs());
        //}
        //#endregion
	}
}
