﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.CustomProperties;
using DocumentFormat.OpenXml.VariantTypes;
using System.Text.RegularExpressions;

namespace LMP.Architect.Oxml
{
    class Query
    {
        private struct HeaderFooterElements
        {
            public SdtElement CC;
            public bool InHeader;
            public HeaderFooterValues HFType;
            public int Section;
        }
        public enum VariableProperties
        {
            ID = 1,
            Name = 2,
            DisplayName = 3,
            TranslationID = 4,
            ExecutionIndex = 5,
            DefaultValue = 6,
            ValueSourceExpression = 7,
            ReserveValue = 8,
            Reserved = 9,
            ControlType = 10,
            ControlProperties = 11,
            ValidationCondition = 12,
            ValidationResponse = 13,
            Description = 14,
            VariableActionsToString = 15,
            ControlActionsToString = 16,
            RequestCIForEntireSegment = 17,
            CIContactType = 18,
            CIDetailTokenString = 19,
            CIAlerts = 20,
            DisplayIn = 21,
            DisplayLevel = 22,
            DisplayValue = 23,
            Hotkey = 24,
            MustVisit = 25,
            AllowPrefillOverride = 26,
            DistributedDetail = 27,
            AssociatedPrefillNames = 28,
            TabNumber = 29,
            CIEntityName = 30,
            RuntimeControlValues = 31,
            NavigationTag = 32,
            ObjectDatabaseID = 33
        }

        public enum BlockProperties
        {
            Name = 1,
            DisplayName = 2,
            TranslationID = 3,
            ShowInTree = 4,
            IsBody = 5,
            Description = 6,
            Hotkey = 7,
            DisplayLevel = 8,
            StartingText = 9,
            ObjectDatabaseID = 10
        }

        public enum SubVariableProperties
        {
            Index = 1,
            UNID = 2,
            Format = 3,
            Default = 4
        }

        /// <summary>
        /// returns the value of the specified tag attribute
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xTag"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        public static string GetAttributeValue(WordprocessingDocument oDoc, string xTag, string xName)
        {
            //get settings part
            Settings oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            string xValue = "";
            if (xName != "DeletedScopes")
            {
                //attribute is in mpo-prefixed variable
                //JTS 11/8/16: reworked for speed
                DocumentVariable oDocVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == "mpo" + xTag.Substring(3, 8)).FirstOrDefault();
                if (oDocVar != null)
                    xValue = oDocVar.Val.Value;
                //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                //               where dv.Name == "mpo" + xTag.Substring(3, 8)
                //               select dv;
                //if (oDocVars.Count() > 0)
                //    xValue = oDocVars.First().Val.Value;
                if (xValue == "")
                    return "";
                int iPos = xValue.IndexOf("ÌÍ");
                string xTagID = xValue.Substring(0, iPos);
                string xAttributes = xValue.Substring(iPos + 2);
                if (xName == "TagID" || xName == "Name")
                    //these attributes are always at start of string, unlabeled and unencrypted
                    return xTagID;
                else
                {
                    //search for specified attribute in encrypted portion of value
                    xAttributes = "ÌÍ" + LMP.String.Decrypt(xAttributes) + "ÌÍ";
                    iPos = xAttributes.IndexOf("ÌÍ" + xName + "=");
                    if (iPos >= 0)
                    {
                        iPos = iPos + xName.Length + 3;
                        int iPos2 = xAttributes.IndexOf("ÌÍ", iPos);
                        //GLOG : 15819 : ceh - ensure variable formatting is preserved
                        //return xAttributes.Substring(iPos, iPos2 - iPos);
                        return xAttributes.Substring(iPos, iPos2 - iPos).Replace("_x000d__x000a_", "\r\n");
                    }
                }
            }
            else
            {
                //attribute is in mpd-prefixed variable
                string xVar = "mpd" + xTag.Substring(3, 8) + "01";
                //JTS 11/8/16: reworked for speed
                DocumentVariable oDocVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                if (oDocVar != null)
                    xValue = oDocVar.Val.Value;
                //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                //               where dv.Name == xVar
                //               select dv;
                //if (oDocVars.Count() > 0)
                //    xValue = oDocVars.First().Val.Value;

                //there can be multiple mpd doc vars
                string xDeletedScopes = "";
                int iIndex = 1;
                while (xValue != "")
                {
                    xDeletedScopes += xValue;
                    iIndex++;
                    xVar = xVar.Substring(0, 11);
                    if (iIndex < 10)
                        xVar += "0";
                    xVar += iIndex.ToString();
                    xValue = "";
                    oDocVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                    if (oDocVar != null)
                        xValue = oDocVar.Val.Value;
                    //if (oDocVars.Count() > 0)
                    //    xValue = oDocVars.First().Val.Value;
                }
                return LMP.String.Decrypt(xDeletedScopes);
            }
            return "";
        }

        /// <summary>
        /// returns the value of the specified tag attribute
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="oCC"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        public static string GetAttributeValue(WordprocessingDocument oDoc, SdtElement oCC, string xName)
        {
            string xTag = GetTag(oCC);
            return GetAttributeValue(oDoc, xTag, xName);
        }

        /// <summary>
        /// sets the value of the specified tag attribute
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="oCC"></param>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        /// <param name="xPassword"></param>
        public static void SetAttributeValue(WordprocessingDocument oDoc, SdtElement oCC, string xName,
            string xValue, bool bEncrypt, string xPassword)
        {
            string xTag = GetTag(oCC);
            SetAttributeValue(oDoc, xTag, xName, xValue, bEncrypt, xPassword);
        }

        /// <summary>
        /// sets the value of the specified tag attribute
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="oCC"></param>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        /// <param name="xPassword"></param>
        public static void SetAttributeValue(WordprocessingDocument oDoc, string xTag, string xName,
            string xValue, bool bEncrypt, string xPassword)
        {
            DocumentVariable oVar = null;
            string xExistingPassword = "";
            string xVarValue = "";

            //get settings part
            Settings oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            if (xName != "DeletedScopes")
            {
                string xBaseName = GetBaseName(xTag);

                //attribute is in mpo-prefixed variable
                //JTS 11/8/16: reworked for speed
                oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == "mpo" + xTag.Substring(3, 8)).FirstOrDefault();
                //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                //               where dv.Name == "mpo" + xTag.Substring(3, 8)
                //               select dv;
                //if (oDocVars.Count() > 0)
                //    oVar = oDocVars.First();

                if (oVar == null)
                {
                    //create variable
                    oVar = oSettings.Elements<DocumentVariables>().FirstOrDefault().AppendChild(
                        new DocumentVariable() { Name = "mpo" + xTag.Substring(3, 8), Val = "ÌÍ" });
                }

                xVarValue = oVar.Val.Value;

                int iPos = xVarValue.IndexOf("ÌÍ");

                if (xName == "TagID" || xName == "Name")
                    //these attributes are always at start of string, unlabeled and unencrypted
                    xVarValue = xValue + xVarValue.Substring(iPos);
                else
                {
                    //search for specified attribute in encrypted portion of value
                    string xTagID = xVarValue.Substring(0, iPos);
                    string xAttributes = LMP.String.Decrypt(xVarValue.Substring(iPos + 2), ref xExistingPassword);
                    if (xAttributes == "")
                        xAttributes = "ÌÍ";
                    else
                        xAttributes = "ÌÍ" + xAttributes + "ÌÍ";
                    iPos = xAttributes.IndexOf("ÌÍ" + xName + "=");
                    if (iPos > -1)
                    {
                        //existing attribute found
                        iPos += xName.Length + 3;
                        int iPos2 = xAttributes.IndexOf("ÌÍ", iPos);
                        xAttributes = xAttributes.Substring(2, iPos - 2) + xValue +
                            xAttributes.Substring(iPos2, xAttributes.Length - iPos2 - 2);
                    }
                    else
                        //add attribute
                        xAttributes = xAttributes.Substring(2) + xName + "=" + xValue;

                    //encrypt
                    if (bEncrypt && ((xBaseName != "mDel") && (xBaseName != "mSubVar")))
                    {
                        if (xPassword == "")
                        {
                            //no supplied password
                            if (xExistingPassword != "")
                                //use existing password
                                xPassword = xExistingPassword;
                            else
                                //use current client's password
                                xPassword = LMP.Data.Application.EncryptionPassword;
                        }
                        xAttributes = LMP.String.Encrypt(xAttributes, xPassword);
                    }

                    //recombine with tag id
                    xVarValue = xTagID + "ÌÍ" + xAttributes;
                }

                //write to doc var
                oVar.Val.Value = xVarValue;
            }
            else
            {
                //attribute is in mpd-prefixed variable
                string xVar = "";
                //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                //               where dv.Name == xVar
                //               select dv;

                //JTS 11/8/16: reworked for speed
                xVar = "mpd" + xTag.Substring(3, 8) + "01";

                oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                //encrypt value
                if (bEncrypt)
                {
                    if (xPassword == "")
                    {
                        //no supplied password - attempt to get existing one
                        //if (oDocVars.Count() > 0)
                        //    oVar = oDocVars.First();

                        if (oVar != null)
                        {
                            xVarValue = LMP.String.Decrypt(oVar.Val.Value, ref xExistingPassword);
                        }
                        if (xExistingPassword != "")
                            //use existing password
                            xPassword = xExistingPassword;
                        else
                            //use current client's password
                            xPassword = LMP.Data.Application.EncryptionPassword;
                    }

                    xValue = LMP.String.Encrypt(xValue, xPassword);
                }
                //create/set variables
                int iIndex = 0;
                while (xValue != "")
                {
                    oVar = null;
                    iIndex++;
                    xVar = "mpd" + xTag.Substring(3, 8) +
                        new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();
                    xVarValue = xValue.Substring(0, Math.Min(65000, xValue.Length));
                    oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();

                    //if (oDocVars.Count() > 0)
                    //    oVar = oDocVars.First();
                    if (oVar == null)
                    {
                        oVar = oSettings.Elements<DocumentVariables>().FirstOrDefault().AppendChild(new DocumentVariable() { Name = xVar, Val = xVarValue });
                    }
                    else
                        oVar.Val.Value = xVarValue;
                    if (xValue.Length > 65000)
                        xValue = xValue.Substring(65000);
                    else
                        xValue = "";
                }

                //delete any remaining unused variables
                oVar = null;
                iIndex++;
                xVar = "mpd" + xTag.Substring(3, 8) +
                    new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();

                oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                //if (oDocVars.Count() > 0)
                //    oVar = oDocVars.First();
                while (oVar != null)
                {
                    oVar.Remove();
                    oVar = null;
                    iIndex++;
                    xVar = "mpd" + xTag.Substring(3, 8) +
                        new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();
                    oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                    //if (oDocVars.Count() > 0)
                    //    oVar = oDocVars.First();
                }
            }
            oSettings.Save();
        }

        /// <summary>
        /// returns the value of the specified variable property
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xObjectData"></param>
        /// <param name="xVariableName"></param>
        /// <param name="iProp"></param>
        /// <returns></returns>
        public static string GetmVarObjectDataValue(string xObjectData, string xVariableName, VariableProperties iProp)
        {
            //get the variable definition
            string xDef = GetVariableDefinition(xObjectData, xVariableName);
            if (xDef == "")
                return "";
            else if (xDef.EndsWith(StringArray.mpEndOfField.ToString()))
                xDef = xDef.Substring(0, xDef.Length - 1);

            //newer ObjectData items might not exist in previously-created docs
            string[] aProps = xDef.Split(StringArray.mpEndOfValue);
            int iIndex = (int)iProp - 1;
            if (iIndex <= aProps.Length)
                return aProps[iIndex];
            else
                return "";
        }

        /// <summary>
        /// extracts the definition for xVariableName from xObjectData
        /// </summary>
        /// <param name="xObjectData"></param>
        /// <param name="xVariableName"></param>
        /// <returns></returns>
        private static string GetVariableDefinition(string xObjectData, string xVariableName)
        {
            string xDef = "";
            int iPos = 0;
            while (xDef == "")
            {
                iPos = xObjectData.IndexOf(StringArray.mpEndOfValue + xVariableName + StringArray.mpEndOfValue, iPos);
                if (iPos < 0)
                    return "";
                int iPos2 = xObjectData.LastIndexOf("VariableDefinition=", iPos);
                if (iPos2 < 0)
                    return "";
                iPos2 += 19;
                int iPos3 = xObjectData.IndexOf(StringArray.mpEndOfField, iPos2);
                if (iPos3 < 0)
                    return "";
                string xTest = xObjectData.Substring(iPos2, (iPos3 - iPos2) + 1);
                if (LMP.String.GetInstancePosition(xTest, "¦", (int)VariableProperties.Name - 1) == iPos - iPos2)
                    //definition is valid
                    xDef = xTest;
                else
                    //try again
                    iPos = iPos + 1;
            }

            return xDef;
        }

        /// <summary>
        /// returns the value of the specified segment property
        /// </summary>
        /// <param name="xObjectData"></param>
        /// <param name="xProperty"></param>
        /// <returns></returns>
        public static string GetmSEGObjectDataValue(string xObjectData, string xProperty)
        {
            return LMP.Conversion.GetmSEGObjectDataValue(xObjectData, xProperty);
        }

        /// <summary>
        /// returns the value of the specified block property
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xObjectData"></param>
        /// <param name="xVariableName"></param>
        /// <param name="iProp"></param>
        /// <returns></returns>
        public static string GetmBlockObjectDataValue(string xObjectData, BlockProperties iProp)
        {
            //strip tag prefix id
            string xDef = xObjectData;
            int iPos = xDef.IndexOf(LMP.String.mpTagPrefixIDSeparator);
            if (iPos != -1)
                xDef = xDef.Substring(iPos + LMP.String.mpTagPrefixIDSeparator.Length);

            //newer ObjectData items might not exist in previously-created docs
            string[] aProps = xDef.Split(StringArray.mpEndOfField);
            int iIndex = (int)iProp - 1;
            if (iIndex <= aProps.Length)
                return aProps[iIndex];
            else
                return "";
        }

        /// <summary>
        /// returns the value of the specified subvariable property
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xObjectData"></param>
        /// <param name="xVariableName"></param>
        /// <param name="iProp"></param>
        /// <returns></returns>
        public static string GetmSubVarObjectDataValue(string xObjectData, SubVariableProperties iProp)
        {
            //newer ObjectData items might not exist in previously-created docs
            string[] aProps = xObjectData.Split(StringArray.mpEndOfValue);
            int iIndex = (int)iProp - 1;
            if (iIndex <= aProps.Length)
                return aProps[iIndex];
            else
                return "";
        }

        /// <summary>
        /// returns the Forte base name of the specified content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        public static string GetBaseName(SdtElement oCC)
        {
            string xTag = GetTag(oCC);
            return GetBaseName(xTag);
        }

        /// <summary>
        /// returns the Forte base name of the specified content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        public static string GetBaseName(string xTag)
        {
            if (xTag.Length < 3)
                return "";
            switch (xTag.Substring(0, 3))
            {
                case "mps":
                    return "mSEG";
                case "mpv":
                    return "mVar";
                case "mpb":
                    return "mBlock";
                case "mpd":
                    return "mDel";
                case "mpu":
                    return "mSubVar";
                case "mpp":
                    return "mDocProps";
                case "mpc":
                    return "mSecProps";
                default:
                    return "";
            }
        }

        /// <summary>
        /// returns the appropriate prefix for the specified Forte content control type
        /// </summary>
        /// <param name="xNBaseName"></param>
        /// <returns></returns>
        public static string GetTagPrefix(string xBaseName)
        {
            switch (xBaseName)
            {
                case "mSEG":
                    return "mps";
                case "mVar":
                    return "mpv";
                case "mBlock":
                    return "mpb";
                case "mDel":
                    return "mpd";
                case "mSubVar":
                    return "mpu";
                case "mDocProps":
                    return "mpp";
                case "mSecProps":
                    return "mpc";
                default:
                    return "";
            }
        }

        /// <summary>
        /// returns a list of SdtElements that contains
        /// all top level mSeg content controls
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static List<RawSegmentPart> GetTopLevelRawSegmentParts(WordprocessingDocument oDoc, string xCheckID)
        {
            DateTime t0 = DateTime.Now;
            if (!string.IsNullOrEmpty(xCheckID) && xCheckID.EndsWith(".0"))
            {
                xCheckID = xCheckID.Replace(".0", "");
            }

            //create empty list
            //JTS 9/12/16:  Get parts in order they appear in document, not sorted by TagID
            List<RawSegmentPart> oTopLevelmSegs = new List<RawSegmentPart>();
            //SortedSet<RawSegmentPart> oTopLevelmSegs = new SortedSet<RawSegmentPart>(new RawSegmentPart.RawSegmentTagIDComparer());

            //get all top level mps ccs in document body
            //JTS 7/6/16: Content Control may not be direct child of Body (such as when its a collection table item).
            //Look for Descendant Content Controls that don't have a parent content control
            //JTS 11/8/16: reworked for speed
            //GLOG 8484: Avoid error on non-Forte Content Controls
            SdtElement[] ccs = oDoc.MainDocumentPart.Document.Body.Descendants<SdtElement>().Where<SdtElement>(cce => cce.Ancestors<SdtElement>().Count() == 0 &&
                                   Query.GetTag(cce).StartsWith("mps")).ToArray<SdtElement>();

            //var ccs = from cc in oDoc.MainDocumentPart.Document.Body.Descendants<SdtElement>().Where<SdtElement>(cce => cce.Ancestors<SdtElement>().Count() == 0) // Elements()
            //          where (
            //              from pr in cc.Elements<SdtProperties>()
            //              where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mps")).Any()
            //              select pr).Any()
            //          select cc;

            //cycle through each cc adding to the list
            //those that are not linked to a parent
            foreach (SdtElement cc in ccs)
            {
                //GLOG 8712: Ignore content controls in mc:Fallback node
                if (cc.Ancestors<AlternateContentFallback>().FirstOrDefault() != null)
                    continue;
                RawSegmentPart oSeg = new RawSegmentPart(oDoc, cc);
                if (!string.IsNullOrEmpty(xCheckID) && oSeg.GetSegmentPropertyValue("SegmentID") != xCheckID)
                {
                    continue;
                }
                string xParentTagID = oSeg.GetSegmentPropertyValue("ParentTagID");

                //add to list if there is no parent id
                //GLOG 15951: Also add if SegmentID matches xCheckID, since ParentTagID may have been preset in a Component
                //segment for insertion into Parent segment in word document
                if (string.IsNullOrEmpty(xParentTagID) || oSeg.GetSegmentPropertyValue("SegmentID") == xCheckID)
                {
                    oTopLevelmSegs.Add(oSeg);
                }
            }


            //get header parts
            //GLOG 15834: HeaderParts may appear in random order - retrieve in order HeaderReferences appear in the main document 
            List<HeaderPart> headerParts = new List<HeaderPart>();
            HeaderReference[] aHR = oDoc.MainDocumentPart.Document.Descendants<HeaderReference>().ToArray();
            foreach (HeaderReference oHR in aHR)
            {
                OpenXmlPart oPart = oDoc.MainDocumentPart.GetPartById(oHR.Id);
                if (oPart != null)
                {
                    headerParts.Add((HeaderPart)oPart);
                }
            }
            //HeaderPart[] headerParts = oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>().ToArray();

            foreach (HeaderPart h in headerParts)
            {
                //get header mseg ccs
                //GLOG 8484: Avoid error on non-Forte Content Controls
                List<SdtElement> headerMSegs = h.Header.Descendants<SdtElement>().Where<SdtElement>(cce => Query.GetTag(cce).StartsWith("mps")).ToList<SdtElement>();
                //SdtElement[] headerMSegs = h.Header.Descendants<SdtElement>().Where<SdtElement>(cce => cce.Ancestors<SdtElement>().Count() == 0 &&
                //                   cce.Elements<SdtProperties>().First().Elements<Tag>().First().Val.Value.StartsWith("mps")).ToArray<SdtElement>();
                //var headerMSegs = from cc in h.Header.Descendants<SdtElement>().Where<SdtElement>(cce => cce.Ancestors<SdtElement>().Count() == 0) //GLOG 8519
                //                  where (
                //                      from pr in cc.Elements<SdtProperties>()
                //                      where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mps")).Any()
                //                      select pr).Any()
                //                  select cc;

                //cycle through each cc adding to the list
                //those that are not linked to a parent
                foreach (SdtElement cc in headerMSegs)
                {
                    //GLOG 8712: Ignore content controls in mc:Fallback node
                    if (cc.Ancestors<AlternateContentFallback>().FirstOrDefault() != null)
                        continue;
                    if (cc.Ancestors<SdtElement>().Count() == 0)
                    {
                        RawSegmentPart oSeg = new RawSegmentPart(oDoc, cc);
                        if (!string.IsNullOrEmpty(xCheckID) && oSeg.GetSegmentPropertyValue("SegmentID") != xCheckID)
                        {
                            continue;
                        }

                        string xParentTagID = oSeg.GetSegmentPropertyValue("ParentTagID");

                        //add to list if there is no parent id
                        //GLOG 15951
                        if (string.IsNullOrEmpty(xParentTagID) || oSeg.GetSegmentPropertyValue("SegmentID") == xCheckID)
                        {
                            oTopLevelmSegs.Add(oSeg);
                        }
                    }
                    else if (cc.Ancestors<Picture>().Count() > 0)
                    {
                        //Also include mSEGs in textboxes that are anchored to a top-level mSEG of the same Segment
                        Picture oPicture = cc.Ancestors<Picture>().First();
                        SdtElement oAnchor = oPicture.Ancestors<SdtElement>().FirstOrDefault();
                        if (oAnchor != null && oPicture.Descendants<SdtElement>().First() == cc)
                        {
                            RawSegmentPart oSeg = new RawSegmentPart(oDoc, cc);
                            if (!string.IsNullOrEmpty(xCheckID) && oSeg.GetSegmentPropertyValue("SegmentID") != xCheckID)
                            {
                                continue;
                            }
                            RawSegmentPart oParentSeg = new RawSegmentPart(oDoc, oAnchor);
                            string xParentTagID = oSeg.GetSegmentPropertyValue("ParentTagID");
                            //GLOG 15951
                            if ((string.IsNullOrEmpty(xParentTagID) || oSeg.GetSegmentPropertyValue("SegmentID") == xCheckID) && oSeg.TagID == oParentSeg.TagID)
                            {
                                oTopLevelmSegs.Add(oSeg);
                            }
                        }
                    }
                }
            }

            //get footer parts
            //GLOG 15834: FooterParts may appear in random order - retrieve in order HeaderReferences appear in the main document 
            List<FooterPart> footerParts = new List<FooterPart>();
            FooterReference[] aFR = oDoc.MainDocumentPart.Document.Descendants<FooterReference>().ToArray();
            foreach (FooterReference oFR in aFR)
            {
                OpenXmlPart oPart = oDoc.MainDocumentPart.GetPartById(oFR.Id);
                if (oPart != null)
                {
                    footerParts.Add((FooterPart)oPart);
                }
            }
            //FooterPart[] footerParts = oDoc.MainDocumentPart.GetPartsOfType<FooterPart>().ToArray();

            foreach (FooterPart f in footerParts)
            {
                //get footer mseg ccs
                //GLOG 8484: Avoid error on non-Forte Content Controls
                List<SdtElement> footerMSegs = f.Footer.Descendants<SdtElement>().Where<SdtElement>(cce => Query.GetTag(cce).StartsWith("mps")).ToList<SdtElement>();
                //SdtElement[] footerMSegs = f.Footer.Descendants<SdtElement>().Where<SdtElement>(cce => cce.Ancestors<SdtElement>().Count() == 0 &&
                //                   cce.Elements<SdtProperties>().First().Elements<Tag>().First().Val.Value.StartsWith("mps")).ToArray<SdtElement>();
                //var footerMSegs = from cc in f.Footer.Descendants<SdtElement>().Where<SdtElement>(cce => cce.Ancestors<SdtElement>().Count() == 0) //GLOG 8519
                //                  where (
                //                      from pr in cc.Elements<SdtProperties>()
                //                      where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mps")).Any()
                //                      select pr).Any()
                //                  select cc;

                //cycle through each cc adding to the list
                //those that are not linked to a parent
                foreach (SdtElement cc in footerMSegs)
                {
                    //GLOG 8712: Ignore content controls in mc:Fallback node
                    if (cc.Ancestors<AlternateContentFallback>().FirstOrDefault() != null)
                        continue;
                    if (cc.Ancestors<SdtElement>().Count() == 0)
                    {
                        RawSegmentPart oSeg = new RawSegmentPart(oDoc, cc);
                        if (!string.IsNullOrEmpty(xCheckID) && oSeg.GetSegmentPropertyValue("SegmentID") != xCheckID)
                        {
                            continue;
                        }
                        string xParentTagID = oSeg.GetSegmentPropertyValue("ParentTagID");

                        //add to list if there is no parent id
                        //GLOG 15951
                        if (string.IsNullOrEmpty(xParentTagID) || oSeg.GetSegmentPropertyValue("SegmentID") == xCheckID)
                        {
                            oTopLevelmSegs.Add(oSeg);
                        }
                    }
                    else if (cc.Ancestors<Picture>().Count() > 0)
                    {
                        //Also include mSEGs in textboxes that are anchored to a top-level mSEG of the same Segment
                        Picture oPicture = cc.Ancestors<Picture>().First();
                        SdtElement oAnchor = oPicture.Ancestors<SdtElement>().FirstOrDefault();
                        if (oAnchor != null && oPicture.Descendants<SdtElement>().First() == cc)
                        {
                            RawSegmentPart oSeg = new RawSegmentPart(oDoc, cc);
                            if (!string.IsNullOrEmpty(xCheckID) && oSeg.GetSegmentPropertyValue("SegmentID") != xCheckID)
                            {
                                continue;
                            }
                            RawSegmentPart oParentSeg = new RawSegmentPart(oDoc, oAnchor);
                            string xParentTagID = oSeg.GetSegmentPropertyValue("ParentTagID");
                            //GLOG 15951
                            if ((string.IsNullOrEmpty(xParentTagID) || oSeg.GetSegmentPropertyValue("SegmentID") == xCheckID) && oSeg.TagID == oParentSeg.TagID)
                            {
                                oTopLevelmSegs.Add(oSeg);
                            }
                        }
                    }
                }
            }

            LMP.Benchmarks.Print(t0);

            return oTopLevelmSegs; //.ToList<RawSegmentPart>();
        }

        /// <summary>
        /// returns a list of SdtElements that contains
        /// all top level mSeg content controls
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static List<RawSegmentPart> GetChildRawSegmentParts(RawSegmentPart oParent)
        {
            DateTime t0 = DateTime.Now;

            //create empty list
            //JTS 9/12/16:  Get parts in order they appear in document, not sorted by TagID
            List<RawSegmentPart> oChildSegments = new List<RawSegmentPart>();
            //SortedSet<RawSegmentPart> oChildSegments = new SortedSet<RawSegmentPart>(new RawSegmentPart.RawSegmentTagIDComparer());
            //GLOG 8911: If CC is not in main body, don't check for Descendant CCs
            //They will be added when Headers and Footers are checked
            if (oParent.ContentControl.Ancestors<Document>().Count() > 0)
            {
                //get only those descendants whose closest mps ancestor is the parent -
                //start with all enclosed descendants
                //JTS 11/8/16: reworked for speed
                //GLOG 8484: Avoid error on non-Forte Content Controls
                SdtElement[] ccs = oParent.ContentControl.Descendants<SdtElement>().Where<SdtElement>(cce => Query.GetTag(cce).StartsWith("mps")).ToArray<SdtElement>();
                //var ccs = from cc in oParent.ContentControl.Descendants()
                //          where (
                //              from pr in cc.Elements<SdtProperties>()
                //              where (pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mps")).Any())
                //              select pr).Any()
                //          select cc;

                //cycle through each descendant adding to the list only
                //those whose immediate mSEG parent is the segment passed in
                foreach (SdtElement cc in ccs)
                {

                    //GLOG 8712: Ignore content controls in mc:Fallback node
                    if (cc.Ancestors<AlternateContentFallback>().FirstOrDefault() != null)
                        continue;

                    if (cc.Ancestors<SdtElement>().First() == oParent.ContentControl)
                    {
                        //Don't include Content Controls that are part of the same segment
                        //(e.g., Textboxes inside the Pleading Paper header)
                        if (GetAttributeValue(oParent.WPDocument, cc, "TagID") != oParent.TagID)
                            oChildSegments.Add(new RawSegmentPart(oParent.WPDocument, cc));
                    }
                }
            }
            //get header parts
            //GLOG 15834: HeaderParts may appear in random order - retrieve in order HeaderReferences appear in the main document 
            List<HeaderPart> headerParts = new List<HeaderPart>();
            HeaderReference[] aHR = oParent.WPDocument.MainDocumentPart.Document.Descendants<HeaderReference>().ToArray();
            foreach (HeaderReference oHR in aHR)
            {
                OpenXmlPart oPart = oParent.WPDocument.MainDocumentPart.GetPartById(oHR.Id);
                if (oPart != null)
                {
                    headerParts.Add((HeaderPart)oPart);
                }
            }
            //var headerParts = oParent.WPDocument.MainDocumentPart.GetPartsOfType<HeaderPart>();

            List<RawSegmentPart> oChildHeaders = new List<RawSegmentPart>();
            foreach (HeaderPart h in headerParts)
            {
                //get header mseg ccs
                //GLOG 8484: Avoid error on non-Forte Content Controls
                SdtElement[] headerMSegs = h.Header.Descendants<SdtElement>().Where<SdtElement>(cce => Query.GetTag(cce).StartsWith("mps")).ToArray<SdtElement>();
                //var headerMSegs = from cc in h.Header.Descendants()
                //                  where (
                //                      from pr in cc.Elements<SdtProperties>()
                //                      where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mps")).Any()
                //                      select pr).Any()
                //                  select cc;
                //cycle through each cc adding to the list
                //those that are either another part of the passed in segment
                //or a segment linked to the parent (i.e. and external child)
                foreach (SdtElement cc in headerMSegs)
                {
                    //GLOG 8712: Ignore content controls in mc:Fallback node
                    if (cc.Ancestors<AlternateContentFallback>().FirstOrDefault() != null)
                        continue;
                    RawSegmentPart oSeg = new RawSegmentPart(oParent.WPDocument, cc);
                    //Don't include Content Controls that are part of the same segment
                    if (oSeg.TagID != oParent.TagID)
                    {
                        string xParentTagID = oSeg.GetSegmentPropertyValue("ParentTagID");
                        bool bExistingChild = false;
                        foreach (RawSegmentPart oExisting in oChildSegments)
                        {
                            //GLOG 8712:  Headers and Footer of Admin Segments
                            //Inserted in UserSegments may not have ParentTagID set -
                            //Check if thie RawSegmentPart corresponds to one already
                            //found and add if there is a match
                            if (oSeg.TagID == oExisting.TagID)
                            {
                                bExistingChild = true;
                                break;
                            }
                        }

                        //add to list if there is no parent id
                        bool bAdd = false;
                        if (xParentTagID == oParent.TagID || bExistingChild)
                        {
                            bAdd = true;
                        }
                        else
                        {
                            //GLOG 8519: Part may not be external child, but descendant of different RawSegmentPart
                            SdtElement oParentSeg = Query.GetParentSegmentContentControl(oSeg.ContentControl);
                            if (oParentSeg != null && (Query.GetAttributeValue(oParent.WPDocument, oParentSeg, "TagID") == oParent.TagID))
                            {
                                bAdd = true;
                            }
                        }
                        if (bAdd)
                        {
                            oChildSegments.Add(oSeg);
                        }
                    }
                }
            }
            //get footer parts
            //GLOG 15834: FooterParts may appear in random order - retrieve in order HeaderReferences appear in the main document 
            List<FooterPart> footerParts = new List<FooterPart>();
            FooterReference[] aFR = oParent.WPDocument.MainDocumentPart.Document.Descendants<FooterReference>().ToArray();
            foreach (FooterReference oHR in aFR)
            {
                OpenXmlPart oPart = oParent.WPDocument.MainDocumentPart.GetPartById(oHR.Id);
                if (oPart != null)
                {
                    footerParts.Add((FooterPart)oPart);
                }
            }
            //var footerParts = oParent.WPDocument.MainDocumentPart.GetPartsOfType<FooterPart>();

            foreach (FooterPart f in footerParts)
            {
                //get footer mseg ccs
                //GLOG 8484: Avoid error on non-Forte Content Controls
                SdtElement[] footerMSegs = f.Footer.Descendants<SdtElement>().Where<SdtElement>(cce => Query.GetTag(cce).StartsWith("mps")).ToArray<SdtElement>();
                //var footerMSegs = from cc in f.Footer.Descendants()
                //                  where (
                //                      from pr in cc.Elements<SdtProperties>()
                //                      where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mps")).Any()
                //                      select pr).Any()
                //                  select cc;

                //cycle through each cc adding to the list
                //those that are either another part of the passed in segment
                //or a segment linked to the parent (i.e. and external child)
                foreach (SdtElement cc in footerMSegs)
                {
                    //GLOG 8712: Ignore content controls in mc:Fallback node
                    if (cc.Ancestors<AlternateContentFallback>().FirstOrDefault() != null)
                        continue;
                    RawSegmentPart oSeg = new RawSegmentPart(oParent.WPDocument, cc);
                    //Don't include Content Controls that are part of the same segment
                    if (oSeg.TagID != oParent.TagID)
                    {
                        string xParentTagID = oSeg.GetSegmentPropertyValue("ParentTagID");

                        bool bExistingChild = false;
                        foreach (RawSegmentPart oExisting in oChildSegments)
                        {
                            //GLOG 8712:  Headers and Footer of Admin Segments
                            //Inserted in UserSegments may not have ParentTagID set -
                            //Check if thie RawSegmentPart corresponds to one already
                            //found and add if there is a match
                            if (oSeg.TagID == oExisting.TagID)
                            {
                                bExistingChild = true;
                                break;
                            }
                        }

                        //add to list if there is no parent id
                        if (xParentTagID == oParent.TagID || bExistingChild)
                        {
                            oChildSegments.Add(oSeg);
                        }
                        else
                        {
                            //GLOG 8519: Part may not be external child, but descendant of different RawSegmentPart
                            SdtElement oParentSeg = Query.GetParentSegmentContentControl(oSeg.ContentControl);
                            if (oParentSeg != null && (Query.GetAttributeValue(oParent.WPDocument, oParentSeg, "TagID") == oParent.TagID))
                            {
                                oChildSegments.Add(oSeg);
                            }
                        }
                    }
                }
            }

            LMP.Benchmarks.Print(t0);

            return oChildSegments; //.ToList<RawSegmentPart>();
        }
        //GLOG 15834: Ensure list of SdtElements follows order of appearance in document
        public static List<SdtElement> SortCCList(List<SdtElement> oList, WordprocessingDocument oDoc)
        {
            List<SdtElement> oMainList = new List<SdtElement>();
            List<SdtElement> oHFList = new List<SdtElement>();
            List<HeaderFooterElements> oHFElements = new List<HeaderFooterElements>();
            foreach (SdtElement oCC in oList)
            {
                Document oMain = oCC.Ancestors<Document>().FirstOrDefault();
                Header oHeader = oCC.Ancestors<Header>().FirstOrDefault();
                Footer oFooter = oCC.Ancestors<Footer>().FirstOrDefault();
                if (oMain != null)
                {
                    //For MainDocument sort in order of location
                    int iIndex = oMainList.Count;
                    for (int c = 0; c < oMainList.Count; c++)
                    {
                        if (oCC.IsBefore(oMainList[c]))
                        {
                            iIndex = c;
                            break;
                        }
                    }
                    oMainList.Insert(iIndex, oCC);
                }
                else if (oHeader != null || oFooter != null)
                {
                    //Sort items in headers and footer based on section then location
                    OpenXmlPart oPart = null;
                    if (oHeader != null)
                        oPart = oHeader.HeaderPart;
                    else
                        oPart = oFooter.FooterPart;

                    int iSection = GetContentControlSection(oCC, oDoc);
                    HeaderFooterValues iType = GetHeaderFooterType(oPart, oDoc);
                    HeaderFooterElements oNewHF = new HeaderFooterElements();
                    oNewHF.CC = oCC;
                    oNewHF.Section = iSection;
                    oNewHF.HFType = iType;
                    oNewHF.InHeader = oHeader != null;
                    int iIndex = oHFElements.Count;
                    for (int c = 0; c < oHFElements.Count; c++)
                    {
                        HeaderFooterElements oExistingHF = oHFElements[c];
                        if (oNewHF.Section < oExistingHF.Section)
                        {
                            iIndex = c;
                            break;
                        }
                        else if (oNewHF.Section == oExistingHF.Section)
                        {
                            //Order for CCs in the same section:
                            //- First Page Header
                            //- Primary Header
                            //- Even Pages Header
                            //- First Page Footer
                            //- Primary Footer
                            //- Even Pages Footer
                            if (oNewHF.InHeader && !oExistingHF.InHeader)
                            {
                                iIndex = c;
                                break;
                            }
                            else if (oNewHF.InHeader == oExistingHF.InHeader)
                            {
                                if (oNewHF.HFType == oExistingHF.HFType)
                                {
                                    if (oCC.IsBefore(oHFList[c]))
                                    {
                                        iIndex = c;
                                        break;
                                    }
                                }
                                else if (oNewHF.HFType > oExistingHF.HFType)
                                {
                                    iIndex = c;
                                    break;
                                }
                            }
                        }
                    }
                    oHFElements.Insert(iIndex, oNewHF);
                }
            }
            List<SdtElement> oSortedList = new List<SdtElement>();
            oSortedList.AddRange(oMainList);
            foreach (HeaderFooterElements oElements in oHFElements)
            {
                oSortedList.Add(oElements.CC);
            }
            return oSortedList;

        }
        public static int GetContentControlSection(SdtElement oCC, WordprocessingDocument oDoc)
        {
            List<SectionProperties> oSecList = oDoc.MainDocumentPart.Document.Descendants<SectionProperties>().ToList();
            //If there's only 1 section, don't check further
            if (oSecList.Count < 2)
                return 1;
            Document oMain = oCC.Ancestors<Document>().FirstOrDefault();
            if (oMain != null)
            {
                for (int c = 1; c <= oSecList.Count; c++)
                {
                    if (oCC.IsBefore(oSecList[c - 1]))
                        return c;
                }
            }
            else
            {
                Header oHeader = oCC.Ancestors<Header>().FirstOrDefault();
                Footer oFooter = oCC.Ancestors<Footer>().FirstOrDefault();
                OpenXmlPart oPart = null;
                if (oHeader != null)
                    oPart = oHeader.HeaderPart;
                else if (oFooter != null)
                    oPart = oFooter.FooterPart;
                string xRelId = oDoc.MainDocumentPart.GetIdOfPart((OpenXmlPart)oPart);
                for (int c = 1; c <= oSecList.Count; c++)
                {
                    if (oPart is HeaderPart)
                    {
                        HeaderReference oHR = oSecList[c - 1].Descendants<HeaderReference>().Where(h => h.Id == xRelId).FirstOrDefault();
                        if (oHR != null)
                        {
                            return c;
                        }
                    }
                    else if (oPart is FooterPart)
                    {
                        FooterReference oFR = oSecList[c - 1].Descendants<FooterReference>().Where(f => f.Id == xRelId).FirstOrDefault();
                        if (oFR != null)
                        {
                            return c;
                        }
                    }
                }
            }
            return 0;
        }
        public static string GetAuthors(XmlSegment oSegment)
        {
            return Query.GetAttributeValue(oSegment.WPDoc, oSegment.RawSegmentParts[0].ContentControl, "Authors");
        }

        /// <summary>
        /// changes the doc var ids for all content controls in the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        internal static void RegenerateDocVarIDS(WordprocessingDocument oDoc)
        {
            RegenerateDocVarIDs(oDoc, null);
        }
        internal static void RestoreXMLCharsInDocVars(WordprocessingDocument oDoc)
        {
            DateTime t0 = DateTime.Now;
            Settings oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            DocumentVariables oVars = oSettings.GetFirstChild<DocumentVariables>();
            foreach (DocumentVariable oVar in oVars)
            {
                if (oVar.Name.Value.StartsWith("mpo"))
                {
                    oVar.Val.Value = LMP.String.RestoreXMLChars(oVar.Val.Value, true);
                }
            }
            oSettings.Save();
            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// changes the doc var ids of all content controls in the specified document and
        /// ensures that the new ids haven't already been used in specified validation document
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="oValidationDoc"></param>
        internal static void RegenerateDocVarIDs(WordprocessingDocument oDoc, WordprocessingDocument oValidationDoc)
        {
            //JTS 11/1/16: This has been restructured so that InnerXML of each story only needs to be read and written once
            DateTime t0 = DateTime.Now;
            //get target settings part
            Settings oSettings = null;
            DocumentVariables oVars = null;
            oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            oVars = oSettings.GetFirstChild<DocumentVariables>();
            string xVarsXML = oVars.InnerXml;

            //get validation settings part
            Settings oValidationSettings = null;
            DocumentVariables oValidationVars = null;
            string xValidationXML = "";
            if (oValidationDoc != null)
            {
                oValidationSettings = oValidationDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
                oValidationVars = oValidationSettings.GetFirstChild<DocumentVariables>();
                xValidationXML = oValidationVars.InnerXml;
            }

            //cycle through content controls in main document
            string xBodyXML = oDoc.MainDocumentPart.Document.InnerXml;
            bool bDo = false;
            //JTS 11/9/16: Save array to avoid re-evaluating each loop
            SdtElement[] oCCs = oDoc.MainDocumentPart.Document.Descendants<SdtElement>().ToArray();
            foreach (SdtElement occ in oCCs)
            {
                //GLOG 8484
                string xOldTag = Query.GetTag(occ); // occ.Descendants<Tag>().FirstOrDefault().Val.Value;
                if (xOldTag != "")
                {
                    string xOldID = xOldTag.Substring(3, 8);
                    string xNewID = GenerateNewDocVarID(xOldID, xVarsXML, xValidationXML);
                    if (xNewID != "")
                    {
                        bDo = true;
                        //construct new tag
                        string xNewTag = xOldTag.Substring(0, 3) + xNewID + xOldTag.Substring(11);
                        xBodyXML = xBodyXML.Replace(xOldTag + '\"', xNewTag + '\"');
                        if (xVarsXML.Contains('\"' + "mpo" + xOldID))
                            xVarsXML = xVarsXML.Replace('\"' + "mpo" + xOldID, '\"' + "mpo" + xNewID);
                        if (xVarsXML.Contains('\"' + "mpd" + xOldID))
                            xVarsXML = xVarsXML.Replace('\"' + "mpd" + xOldID, '\"' + "mpd" + xNewID);
                    }
                }
            }
            if (bDo)
            {
                //Content Controls were retagged in this story
                oDoc.MainDocumentPart.Document.InnerXml = xBodyXML;
            }

            //cycle through content controls in headers
            //JTS 11/9/16: Save array to avoid re-evaluating each loop
            HeaderPart[] oHeaders = oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>().ToArray();
            foreach (HeaderPart oHeader in oHeaders)
            {
                bDo = false;
                string xHeaderXML = oHeader.Header.InnerXml;
                foreach (SdtElement occ in oHeader.Header.Descendants<SdtElement>())
                {
                    //GLOG 8484
                    string xOldTag = Query.GetTag(occ); // occ.Descendants<Tag>().FirstOrDefault().Val.Value;
                    if (xOldTag != "")
                    {
                        string xOldID = xOldTag.Substring(3, 8);
                        string xNewID = GenerateNewDocVarID(xOldID, xVarsXML, xValidationXML);
                        if (xNewID != "")
                        {
                            bDo = true;
                            //construct new tag
                            string xNewTag = xOldTag.Substring(0, 3) + xNewID + xOldTag.Substring(11);
                            xHeaderXML = xHeaderXML.Replace(xOldTag + '\"', xNewTag + '\"');
                            if (xVarsXML.Contains('\"' + "mpo" + xOldID))
                                xVarsXML = xVarsXML.Replace('\"' + "mpo" + xOldID, '\"' + "mpo" + xNewID);
                            if (xVarsXML.Contains('\"' + "mpd" + xOldID))
                                xVarsXML = xVarsXML.Replace('\"' + "mpd" + xOldID, '\"' + "mpd" + xNewID);
                        }
                    }
                }
                if (bDo)
                {
                    //Content Controls were retagged in this story
                    oHeader.Header.InnerXml = xHeaderXML;
                    oHeader.RootElement.Save();
                }
            }

            //cycle through content controls in footers
            //JTS 11/9/16: Save array to avoid re-evaluating each loop
            FooterPart[] oFooters = oDoc.MainDocumentPart.GetPartsOfType<FooterPart>().ToArray();
            foreach (FooterPart oFooter in oFooters)
            {
                string xFooterXML = oFooter.Footer.InnerXml;
                bDo = false;
                foreach (SdtElement occ in oFooter.Footer.Descendants<SdtElement>())
                {
                    //GLOG 8484
                    string xOldTag = Query.GetTag(occ); // occ.Descendants<Tag>().FirstOrDefault().Val.Value;
                    if (xOldTag != "")
                    {
                        string xOldID = xOldTag.Substring(3, 8);
                        string xNewID = GenerateNewDocVarID(xOldID, xVarsXML, xValidationXML);
                        if (xNewID != "")
                        {
                            bDo = true;
                            //construct new tag
                            string xNewTag = xOldTag.Substring(0, 3) + xNewID + xOldTag.Substring(11);
                            xFooterXML = xFooterXML.Replace(xOldTag + '\"', xNewTag + '\"');
                            if (xVarsXML.Contains('\"' + "mpo" + xOldID))
                                xVarsXML = xVarsXML.Replace('\"' + "mpo" + xOldID, '\"' + "mpo" + xNewID);
                            if (xVarsXML.Contains('\"' + "mpd" + xOldID))
                                xVarsXML = xVarsXML.Replace('\"' + "mpd" + xOldID, '\"' + "mpd" + xNewID);
                        }
                    }
                }
                if (bDo)
                {
                    //Content Controls were retagged in this story
                    oFooter.Footer.InnerXml = xFooterXML;
                    oFooter.RootElement.Save();
                }
            }
            oVars.InnerXml = xVarsXML;
            oSettings.Save();
            LMP.Benchmarks.Print(t0);
        }
        private static string GenerateNewDocVarID(string xOldID, string xVarsXML, string xValidationXML)
        {
            //JTS 11/1/16: Rewritten to just return New ID, not do any replacement
            string xNewID = "";
            while (xNewID == "")
            {
                xNewID = LMP.Conversion.GenerateDocVarID();
                if (xNewID == xOldID)
                {
                    xNewID = "";
                    continue;
                }

                //check that we haven't already used this id in this doc
                if (xVarsXML.Contains("w:name=\"mpo" + xNewID + "\""))
                    xNewID = "";

                //check that name isn't in use in validation document
                if (xValidationXML != "" && xNewID != "" && xValidationXML.Contains("w:name=\"mpo" + xNewID + "\""))
                    xNewID = "";
            }
            return xNewID;
        }

        /// <summary>
        /// returns a new doc var id, ensuring that it doesn't conflict with an existing one
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static string GenerateDocVarID(WordprocessingDocument oDoc)
        {
            //get settings part
            Settings oSettings = null;
            oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;

            string xID = "";
            while (xID == "")
            {
                //generate
                xID = LMP.Conversion.GenerateDocVarID();

                //verify that we haven't already used this id
                var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                               where dv.Name == "mpo" + xID
                               select dv;
                if (oDocVars.Count() > 0)
                    xID = "";
            }

            return xID;
        }

        /// <summary>
        /// generates new GUID in the format used for Forte TagIDs
        /// </summary>
        /// <param name="bUpperCase"></param>
        /// <returns></returns>
        public static string GenerateGUID(bool bUpperCase)
        {
            Guid oGUID = System.Guid.NewGuid();
            string xNewGUID = oGUID.ToString();
            if (bUpperCase)
                xNewGUID = xNewGUID.ToUpper();
            xNewGUID = "{" + xNewGUID + "}";
            return xNewGUID;
        }

        public static string ReindexSegment(WordprocessingDocument oDoc, string xTagID,
            string xParentID, bool bGUIDIndexing)
        {
            string xNewTagID;
            int iPos = xTagID.LastIndexOf('_');
            if (iPos > -1)
                //strip index
                xNewTagID = xTagID.Substring(0, iPos + 1);
            else
                //add separator
                xNewTagID = xTagID + '_';

            if (bGUIDIndexing)
                //generate GUID
                xNewTagID = xNewTagID + GenerateGUID(true).Replace("-", "");
            else
            {
                //TODO: assign new integer index
            }

            //get doc vars
            Settings oSettings = null;
            oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            DocumentVariables oVars = oSettings.GetFirstChild<DocumentVariables>();

            //update tag id itself
            //JTS 11/9/16
            DocumentVariable[] oDocVars = oVars.Elements<DocumentVariable>().Where(dv => dv.Val.Value.StartsWith(xTagID + "ÌÍ")).ToArray();
            //var oDocVars = from dv in oVars.Elements<DocumentVariable>()
            //               where dv.Val.Value.StartsWith(xTagID + "ÌÍ")
            //               select dv;
            foreach (DocumentVariable dv in oDocVars)
            {
                string xValue = dv.Val.Value;
                dv.Val.Value = xNewTagID + "ÌÍ" + xValue.Substring(xTagID.Length + 2);
            }

            //update other references to tag id
            string xXML = oVars.InnerXml;

            //parent tag id property of children
            xXML = xXML.Replace("|ParentTagID=" + xTagID + "|", "|ParentTagID=" + xNewTagID + "|");

            //object data of mDels
            xXML = xXML.Replace("ÌÍObjectData=" + xTagID + "|", "ÌÍObjectData=" + xNewTagID + "|");

            oVars.InnerXml = xXML;
            oSettings.Save();
            return xNewTagID;
        }
        public static void CopyDocumentVariables(WordprocessingDocument oDoc, WordprocessingDocument oDocTarget)
        {
            string xDVXml = "";
            //get settings part
            Settings oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            DocumentVariables oVars = oSettings.Descendants<DocumentVariables>().FirstOrDefault<DocumentVariables>();
            if (oVars != null)
            {
                xDVXml = oVars.InnerXml;
            }
            oSettings = oDocTarget.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            oVars = oSettings.Descendants<DocumentVariables>().FirstOrDefault<DocumentVariables>();
            if (oVars == null && xDVXml != "")
            {
                oVars = new DocumentVariables();
                oSettings.AppendChild(oVars);
            }
            if (xDVXml != "")
            {
                oVars.InnerXml = oVars.InnerXml + xDVXml;
            }
            oSettings.Save();
        }
        /// <summary>
        /// encrypts/decrypts all doc vars associated with Forte content as appropriate
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="bOn"></param>
        /// <param name="xPassword"></param>
        public static void SetDocumentVariableEncryption(WordprocessingDocument oDoc, bool bOn, string xPassword)
        {
            DateTime t0 = DateTime.Now;
            //cycle through content controls in main document
            //GLOG 8484: Avoid error on non-Forte Content Controls
            SdtElement[] oCCs = oDoc.MainDocumentPart.Document.Descendants<SdtElement>().Where(cc => Query.GetTag(cc) != "").ToArray();
            foreach (SdtElement occ in oCCs)
            {
                string xTag = Query.GetTag(occ);
                SetAttributesEncryption(oDoc, xTag, bOn, xPassword);
            }

            //cycle through content controls in headers
            foreach (HeaderPart oHeader in oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>())
            {
                //JTS 11/9/16
                //GLOG 8484: Avoid error on non-Forte Content Controls
                oCCs = oHeader.Header.Descendants<SdtElement>().Where(cc => Query.GetTag(cc) != "").ToArray();
                foreach (SdtElement occ in oCCs)
                {
                    string xTag = Query.GetTag(occ);
                    SetAttributesEncryption(oDoc, xTag, bOn, xPassword);
                }
            }

            //cycle through content controls in footers
            foreach (FooterPart oFooter in oDoc.MainDocumentPart.GetPartsOfType<FooterPart>())
            {
                //JTS 11/9/16
                //GLOG 8484: Avoid error on non-Forte Content Controls
                oCCs = oFooter.Footer.Descendants<SdtElement>().Where(cc => Query.GetTag(cc) != "").ToArray();
                foreach (SdtElement occ in oCCs)
                {
                    string xTag = Query.GetTag(occ);
                    SetAttributesEncryption(oDoc, xTag, bOn, xPassword);
                }
            }
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// encrypts/decrypts all doc vars associated with the specified tag as appropriate
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xTag"></param>
        /// <param name="bOn"></param>
        /// <param name="xPassword"></param>
        public static void SetAttributesEncryption(WordprocessingDocument oDoc, string xTag, bool bOn, string xPassword)
        {
            DocumentVariable oVar = null;
            string xExistingPassword = "";
            string xVarValue = "";
            string xVar = "";
            bool bEncrypted = false;

            //get element
            string xElement = GetBaseName(xTag);

            //mDels and mSubVars ahould not be encrypted
            if ((xElement == "") || (xElement == "mDel") || (xElement == "mSubVar"))
                return;

            //get settings part
            Settings oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;

            //JTS 11/8/16: reworked for speed
            //get mpo-prefixed variable
            xVar = "mpo" + xTag.Substring(3, 8);
            oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();

            //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
            //               where dv.Name == xVar
            //               select dv;

            ////get mpo-prefixed variable
            //xVar = "mpo" + xTag.Substring(3, 8);
            ////if (oDocVars.Count() > 0)
            //oVar = oDocVars.FirstOrDefault();

            if (oVar != null)
            {
                //portion before separator is unencrypted
                xVarValue = oVar.Val.Value;
                int iPos = xVarValue.IndexOf("ÌÍ") + 2;
                string xUnencryptedPortion = xVarValue.Substring(0, iPos);
                string xEncryptedPortion = xVarValue.Substring(iPos);
                bEncrypted = LMP.String.IsEncrypted(xEncryptedPortion);

                if (bOn && !bEncrypted)
                {
                    //no supplied password - use current client's password
                    if (xPassword == "")
                        xPassword = LMP.Data.Application.EncryptionPassword;

                    //encrypt
                    xEncryptedPortion = LMP.String.Encrypt(xEncryptedPortion, xPassword);
                }
                else if (bEncrypted && !bOn)
                {
                    //unencrypt
                    xEncryptedPortion = LMP.String.Decrypt(xEncryptedPortion, ref xExistingPassword);
                }

                //recombine
                xVarValue = xUnencryptedPortion + xEncryptedPortion;

                //write to doc var
                oVar.Val.Value = xVarValue;
            }

            //get mpd-prefixed variable
            if (xElement == "mSEG")
            {
                string xDeletedScopes = "";
                xVarValue = "";
                int iIndex = 1;
                xVar = "mpd" + xTag.Substring(3, 8) + "01";
                oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                if (oVar != null)
                    xVarValue = oVar.Val.Value;
                //if (oDocVars.Count() > 0)
                //    xVarValue = oDocVars.First().Val.Value;
                if (xVarValue != "")
                {
                    //there can be multiple mpd doc vars
                    while (xVarValue != "")
                    {
                        xDeletedScopes += xVarValue;
                        iIndex++;
                        xVar = xVar.Substring(0, 11);
                        if (iIndex < 10)
                            xVar += "0";
                        xVar += iIndex.ToString();
                        xVarValue = "";
                        oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                        if (oVar != null)
                            xVarValue = oVar.Val.Value;
                        //if (oDocVars.Count() > 0)
                        //    xVarValue = oDocVars.First().Val.Value;
                    }

                    //encrypt/decrypt
                    bEncrypted = LMP.String.IsEncrypted(xDeletedScopes);
                    if (bOn && !bEncrypted)
                    {
                        //no supplied password - use current client's password
                        if (xPassword == "")
                            xPassword = LMP.Data.Application.EncryptionPassword;

                        //encrypt
                        xDeletedScopes = LMP.String.Encrypt(xDeletedScopes, xPassword);
                    }
                    else if (bEncrypted && !bOn)
                    {
                        //unencrypt
                        xDeletedScopes = LMP.String.Decrypt(xDeletedScopes, ref xExistingPassword);
                    }
                    //create/set variables
                    iIndex = 0;
                    while (xDeletedScopes != "")
                    {
                        oVar = null;
                        iIndex++;
                        xVar = "mpd" + xTag.Substring(3, 8) +
                            new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();
                        xVarValue = xDeletedScopes.Substring(0, Math.Min(65000, xDeletedScopes.Length));
                        oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                        //if (oDocVars.Count() > 0)
                        //    oVar = oDocVars.First();
                        if (oVar == null)
                        {
                            oVar = oSettings.Elements<DocumentVariables>().FirstOrDefault().AppendChild(new DocumentVariable() { Name = xVar, Val = xVarValue });
                        }
                        else
                            oVar.Val.Value = xVarValue;
                        if (xDeletedScopes.Length > 65000)
                            xDeletedScopes = xDeletedScopes.Substring(65000);
                        else
                            xDeletedScopes = "";
                    }
                    //delete any remaining unused variables
                    oVar = null;
                    iIndex++;
                    xVar = "mpd" + xTag.Substring(3, 8) +
                        new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();
                    oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                    //if (oDocVars.Count() > 0)
                    //    oVar = oDocVars.First();
                    while (oVar != null)
                    {
                        oVar.Remove();
                        oVar = null;
                        iIndex++;
                        xVar = "mpd" + xTag.Substring(3, 8) +
                            new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();
                        oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                        //if (oDocVars.Count() > 0)
                        //    oVar = oDocVars.First();
                    }
                }
            }
            oSettings.Save();
        }
        /// <summary>
        /// encrypts/decrypts all doc vars associated with the specified tag as appropriate
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xTag"></param>
        /// <param name="bOn"></param>
        /// <param name="xPassword"></param>
        public static void SetDocVarAttributesEncryption(WordprocessingDocument oDoc, bool bOn, string xPassword)
        {
            string xExistingPassword = "";
            string xVarValue = "";
            bool bEncrypted = false;
            DateTime t0 = DateTime.Now;
            //get settings part
            Settings oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;

            DocumentVariable[] oDocVars = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name.Value.StartsWith("mpo") || (dv.Name.Value.StartsWith("mpd") && dv.Name.Value.EndsWith("01"))).ToArray<DocumentVariable>();

            foreach (DocumentVariable oVar in oDocVars)
            {
                //portion before separator is unencrypted
                if (oVar.Name.Value.StartsWith("mpo"))
                {
                    xVarValue = oVar.Val.Value;
                    int iPos = xVarValue.IndexOf("ÌÍ") + 2;
                    string xUnencryptedPortion = xVarValue.Substring(0, iPos);
                    string xEncryptedPortion = xVarValue.Substring(iPos);
                    bEncrypted = LMP.String.IsEncrypted(xEncryptedPortion);

                    if (bOn && !bEncrypted)
                    {
                        //JTS 11/2/16: Don't encrypt mDel or mSubVar - anything else will match at least one of these criteria
                        if (xUnencryptedPortion == "" || xEncryptedPortion.Contains("VariableDefinition=") || xEncryptedPortion.Contains("SegmentID=") ||
                            xEncryptedPortion.Contains("True|") || xEncryptedPortion.Contains("False|"))
                        {
                            //no supplied password - use current client's password
                            if (xPassword == "")
                                xPassword = LMP.Data.Application.EncryptionPassword;

                            //encrypt
                            xEncryptedPortion = LMP.String.Encrypt(xEncryptedPortion, xPassword);
                        }
                        else
                            continue;
                    }
                    else if (bEncrypted && !bOn)
                    {
                        //unencrypt
                        xEncryptedPortion = LMP.String.Decrypt(xEncryptedPortion, ref xExistingPassword);
                    }

                    //recombine
                    xVarValue = xUnencryptedPortion + xEncryptedPortion;

                    //write to doc var
                    oVar.Val.Value = xVarValue;
                }
                else
                {
                    //Variable is first of a set of mpd variables
                    string xDeletedScopes = "";
                    int iIndex = 1;
                    string xVar = oVar.Name.Value;
                    string xID = xVar.Substring(3, 8);
                    xVarValue = oVar.Val.Value;
                    DocumentVariable oDelVar = null;
                    if (xVarValue != "")
                    {
                        //there can be multiple mpd doc vars
                        while (xVarValue != "")
                        {
                            xDeletedScopes += xVarValue;
                            iIndex++;
                            xVar = xVar.Substring(0, 11);
                            if (iIndex < 10)
                                xVar += "0";
                            xVar += iIndex.ToString();
                            xVarValue = "";
                            oDelVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                            if (oDelVar != null)
                            {
                                xVarValue = oDelVar.Val.Value;
                            }
                        }

                        //encrypt/decrypt
                        bEncrypted = LMP.String.IsEncrypted(xDeletedScopes);
                        if (bOn && !bEncrypted)
                        {
                            //no supplied password - use current client's password
                            if (xPassword == "")
                                xPassword = LMP.Data.Application.EncryptionPassword;

                            //encrypt
                            xDeletedScopes = LMP.String.Encrypt(xDeletedScopes, xPassword);
                        }
                        else if (bEncrypted && !bOn)
                        {
                            //unencrypt
                            xDeletedScopes = LMP.String.Decrypt(xDeletedScopes, ref xExistingPassword);
                        }
                        oDelVar = null;
                        //create/set variables
                        iIndex = 0;
                        while (xDeletedScopes != "")
                        {
                            oDelVar = null;
                            iIndex++;
                            xVar = "mpd" + xID +
                                new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();
                            xVarValue = xDeletedScopes.Substring(0, Math.Min(65000, xDeletedScopes.Length));
                            oDelVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                            if (oDelVar == null)
                            {
                                oDelVar = oSettings.Elements<DocumentVariables>().FirstOrDefault().AppendChild(new DocumentVariable() { Name = xVar, Val = xVarValue });
                            }
                            else
                                oDelVar.Val.Value = xVarValue;
                            if (xDeletedScopes.Length > 65000)
                                xDeletedScopes = xDeletedScopes.Substring(65000);
                            else
                                xDeletedScopes = "";
                        }
                        //delete any remaining unused variables
                        oDelVar = null;
                        iIndex++;
                        xVar = "mpd" + xID + new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();
                        oDelVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                        while (oDelVar != null)
                        {
                            oDelVar.Remove();
                            oDelVar = null;
                            iIndex++;
                            xVar = "mpd" + xID +
                                new string('0', 2 - iIndex.ToString().Length) + iIndex.ToString();
                            oDelVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                        }
                    }
                }
            }
            oSettings.Save();
            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// Returns SectionProperties elements for sections spanned by Content Control
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static SectionProperties[] GetSectionPropertiesForContentControl(SdtElement oCC, WordprocessingDocument oDoc)
        {
            SectionProperties[] aPr = oDoc.MainDocumentPart.Document.Descendants<SectionProperties>().ToArray<SectionProperties>();
            int iDocCount = aPr.Count();
            OpenXmlElement oParent = oCC.Parent;

            int iStartSection = oParent.ElementsBefore().Where(o => o.Descendants<SectionProperties>().Count() > 0).Count() + 1;

            OpenXmlElement[] oAfter = oParent.ElementsAfter().ToArray<OpenXmlElement>();
            int iEndSection = iDocCount - (oParent.ElementsAfter().Where(o => o.Descendants<SectionProperties>().Count() > 0).Count());
            SectionProperties[] aSecs = new SectionProperties[(iEndSection - iStartSection) + 1];
            for (int i = 0; i <= iEndSection - iStartSection; i++)
            {
                aSecs[i] = aPr[iStartSection + (i - 1)];
            }
            return aSecs;
        }
        /// <summary>
        /// gets the infornmation necessary to find the specified content control
        /// via the Word API
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xTag"></param>
        /// <param name="iCCIndex"></param>
        /// <param name="iStoryType">where xShapeName != "", this will be the anchor's story</param>
        /// <param name="iSectionIndex"></param>
        /// <param name="xShapeName"></param>
        public static void GetContentControlLocation(WordprocessingDocument oDoc, string xTag,
            ref int iCCIndex, ref int iStoryType, ref int iSectionIndex, ref string xShapeName)
        {
            iStoryType = 1;
            OpenXmlElement oStory = null;

            //JTS 11/9/16: reworked for speed

            //GLOG 8484: Avoid error on non-Forte Content Controls
            SdtElement oCC = oDoc.MainDocumentPart.Document.Descendants<SdtElement>().Where(cc => Query.GetTag(cc) == xTag).FirstOrDefault();
            //var ccs = from cc in oDoc.MainDocumentPart.Document.Descendants<SdtElement>()
            //          where (from t in cc.Descendants<Tag>()
            //                 where t.Val.Value == xTag
            //                 select t).Any()
            //          select cc;

            if (oCC != null)
            {
                if (oCC.Ancestors<TextBoxContent>().Count() > 0)
                {
                    oStory = oCC.Ancestors<TextBoxContent>().First();
                    xShapeName = oStory.Ancestors<DocumentFormat.OpenXml.Vml.Shape>().First().GetAttribute("id", "").Value;
                }
                else
                    oStory = oDoc.MainDocumentPart.Document;
                //GLOG 8484: Avoid error on non-Forte Content Controls
                System.Collections.Generic.List<string> oTags = (from cc in oStory.Descendants<SdtElement>().Where(cce => Query.GetTag(cce) != "")
                                                                 select cc.Descendants<Tag>().First().Val.Value).ToList();
                iCCIndex = oTags.IndexOf(xTag) + 1;
            }
            SectionProperties[] aSections = oDoc.MainDocumentPart.Document.Descendants<SectionProperties>().ToArray();
            if (iCCIndex == 0)
            {
                foreach (HeaderPart oHeader in oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>())
                {
                    //GLOG 8484: Avoid error on non-Forte Content Controls
                    oCC = oHeader.Header.Descendants<SdtElement>().Where(cc => Query.GetTag(cc) == xTag).FirstOrDefault();
                    //ccs = from cc in oHeader.Header.Descendants<SdtElement>()
                    //      where (from t in cc.Descendants<Tag>()
                    //             where t.Val.Value == xTag
                    //             select t).Any()
                    //      select cc;

                    if (oCC != null)
                    {
                        //get story type
                        string xRelID = oDoc.MainDocumentPart.GetIdOfPart(oHeader);
                        HeaderReference oHR = (from hr in oDoc.MainDocumentPart.Document.Descendants<HeaderReference>()
                                               where hr.Id.Value == xRelID
                                               select hr).First();
                        if (oHR.Type == "first")
                            //first page
                            iStoryType = 10;
                        else if (oHR.Type == "even")
                            //even
                            iStoryType = 6;
                        else
                            //primary
                            iStoryType = 7;

                        //GLOG 15834
                        for (int i = 0; i < aSections.Count(); i++)
                        {
                            if (oHR.Parent == aSections[i])
                            {
                                iSectionIndex = i + 1;
                                break;
                            }
                        }
                        //JTS 6/6/17: This expression was not returning the expected results
                        ////get section index
                        //iSectionIndex = oHR.Parent.ElementsBefore().Where(o => o.Descendants<SectionProperties>().Count() > 0).Count() + 1;

                        //get container
                        //JTS 11/9/16
                        oStory = oCC.Ancestors<TextBoxContent>().FirstOrDefault();
                        if (oStory != null)
                        {
                            xShapeName = oStory.Ancestors<DocumentFormat.OpenXml.Vml.Shape>().First().GetAttribute("id", "").Value;
                        }
                        else
                        {
                            oStory = oHeader.Header;
                        }

                        //get content control index
                        //GLOG 8484: Avoid error on non-Forte Content Controls
                        System.Collections.Generic.List<string> oTags = (from cc in oStory.Descendants<SdtElement>().Where(cce => Query.GetTag(cce) != "")
                                                                         select cc.Descendants<Tag>().First().Val.Value).ToList();
                        iCCIndex = oTags.IndexOf(xTag) + 1;
                        break;
                    }
                }
            }

            if (iCCIndex == 0)
            {
                foreach (FooterPart oFooter in oDoc.MainDocumentPart.GetPartsOfType<FooterPart>())
                {
                    //GLOG 8484: Avoid error on non-Forte Content Controls
                    oCC = oFooter.Footer.Descendants<SdtElement>().Where(cc => Query.GetTag(cc) == xTag).FirstOrDefault();
                    //ccs = from cc in oFooter.Footer.Descendants<SdtElement>()
                    //      where (from t in cc.Descendants<Tag>()
                    //             where t.Val.Value == xTag
                    //             select t).Any()
                    //      select cc;

                    if (oCC != null)
                    {
                        //get story type
                        string xRelID = oDoc.MainDocumentPart.GetIdOfPart(oFooter);
                        FooterReference oHR = (from hr in oDoc.MainDocumentPart.Document.Descendants<FooterReference>()
                                               where hr.Id.Value == xRelID
                                               select hr).First();
                        if (oHR.Type == "first")
                            //first page
                            iStoryType = 11;
                        else if (oHR.Type == "even")
                            //even
                            iStoryType = 8;
                        else
                            //primary
                            iStoryType = 9;

                        //GLOG 15834
                        for (int i = 0; i < aSections.Count(); i++)
                        {
                            if (oHR.Parent == aSections[i])
                            {
                                iSectionIndex = i + 1;
                                break;
                            }
                        }
                        //JTS 6/6/17: This expression was not returning the expected results
                        ////get section index
                        //iSectionIndex = oHR.Parent.ElementsBefore().Where(o => o.Descendants<SectionProperties>().Count() > 0).Count() + 1;

                        //get container
                        //JTS 11/9/16
                        oStory = oCC.Ancestors<TextBoxContent>().FirstOrDefault();
                        if (oStory != null)
                        {
                            xShapeName = oStory.Ancestors<DocumentFormat.OpenXml.Vml.Shape>().First().GetAttribute("id", "").Value;
                        }
                        else
                        {
                            oStory = oFooter.Footer;
                        }

                        //get content control index
                        //GLOG 8484: Avoid error on non-Forte Content Controls
                        System.Collections.Generic.List<string> oTags = (from cc in oStory.Descendants<SdtElement>().Where(cce => Query.GetTag(cce) != "")
                                                                         select cc.Descendants<Tag>().First().Val.Value).ToList();
                        iCCIndex = oTags.IndexOf(xTag) + 1;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// deletes mSEG content controls and rebookmarks
        /// </summary>
        /// <param name="oDoc"></param>
        public static void DeleteSegmentContentControls(WordprocessingDocument oDoc)
        {
            DateTime t0 = DateTime.Now;
            //set counter for bookmark ID - set high and decrement, as bookmark order
            //is determined by ID when start position is the same
            //JTS 11/01/16: Increased starting ID substantially -
            //It's possible a complex Segment might have more than 1000 bookmarks, especially if there are References.
            //Less likely there could be more than 900000.  Ideally, we might want to check InnerXML of each story 
            //to ensure there are no conflicts, but I assume this would impact performance
            int iBmkID = 999999;

            //JTS 11/9/16: reworked for speed
            //GLOG 8484: Avoid error on non-Forte Content Controls
            SdtElement[] ccs = oDoc.MainDocumentPart.Document.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mps")).Reverse().ToArray();

            //var ccs = from cc in oDoc.MainDocumentPart.Document.Descendants<SdtElement>().Reverse()
            //          where (from t in cc.Descendants<Tag>()
            //                 where t.Val.Value.Substring(0, 3) == "mps"
            //                 select t).Any()
            //          select cc;
            foreach (SdtElement occ in ccs)
                DeleteSegmentContentControl(oDoc, occ, ref iBmkID);

            //headers
            foreach (HeaderPart oHeader in oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>())
            {

                //GLOG 8484: Avoid error on non-Forte Content Controls
                ccs = oHeader.Header.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mps")).Reverse().ToArray();
                //ccs = from cc in oHeader.Header.Descendants<SdtElement>().Reverse()
                //      where (from t in cc.Descendants<Tag>()
                //             where t.Val.Value.Substring(0, 3) == "mps"
                //             select t).Any()
                //      select cc;
                foreach (SdtElement occ in ccs)
                    DeleteSegmentContentControl(oDoc, occ, ref iBmkID);
            }

            //footers
            foreach (FooterPart oFooter in oDoc.MainDocumentPart.GetPartsOfType<FooterPart>())
            {
                //GLOG 8484: Avoid error on non-Forte Content Controls
                ccs = oFooter.Footer.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mps")).Reverse().ToArray();
                //ccs = from cc in oFooter.Footer.Descendants<SdtElement>().Reverse()
                //      where (from t in cc.Descendants<Tag>()
                //             where t.Val.Value.Substring(0, 3) == "mps"
                //             select t).Any()
                //      select cc;
                foreach (SdtElement occ in ccs)
                    DeleteSegmentContentControl(oDoc, occ, ref iBmkID);
            }
            LMP.Benchmarks.Print(t0);
        }
        public static bool DeleteBookmark(string xBookmarkName, WordprocessingDocument oDoc)
        {
            BookmarkStart oBmk = oDoc.MainDocumentPart.Document.Descendants<BookmarkStart>().Where(b => b.Name.Value.ToUpper() == xBookmarkName.ToUpper()).FirstOrDefault();
            if (oBmk != null)
            {
                return DeleteBookmark(oBmk);
            }

            //headers
            foreach (HeaderPart oHeader in oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>())
            {
                //GLOG 8649
                oBmk = oHeader.Header.Descendants<BookmarkStart>().Where(b => b.Name.Value.ToUpper() == xBookmarkName.ToUpper()).FirstOrDefault();
                if (oBmk != null)
                {
                    return DeleteBookmark(oBmk);
                }
            }

            //footers
            foreach (FooterPart oFooter in oDoc.MainDocumentPart.GetPartsOfType<FooterPart>())
            {
                //GLOG 8649
                oBmk = oFooter.Footer.Descendants<BookmarkStart>().Where(b => b.Name.Value.ToUpper() == xBookmarkName.ToUpper()).FirstOrDefault();
                if (oBmk != null)
                {
                    return DeleteBookmark(oBmk);
                }
            }
            return false;
        }

        public static bool DeleteBookmark(BookmarkStart oStartElement)
        {
            if (oStartElement == null)
                return false;

            OpenXmlElement oParent = oStartElement.Parent;
            if (oParent == null)
                return false;

            string xID = oStartElement.Id;
            oStartElement.Remove();
            BookmarkEnd oEndElement = null;
            while (oEndElement == null && oParent != null)
            {
                oEndElement = oParent.Descendants<BookmarkEnd>().Where(x => x.Id == xID).FirstOrDefault();
                oParent = oParent.Parent;
            }
            if (oEndElement != null)
            {
                oEndElement.Remove();
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// deletes mSEG content controls and rebookmarks
        /// </summary>
        /// <param name="oDoc"></param>
        public static void DeleteSegmentBookmarks(WordprocessingDocument oDoc)
        {
            DateTime t0 = DateTime.Now;
            BookmarkStart[] oBmks = oDoc.MainDocumentPart.Document.Descendants<BookmarkStart>().Where(b => b.Name.Value.StartsWith("_mps")).ToArray<BookmarkStart>();
            foreach (BookmarkStart oBmk in oBmks)
                DeleteBookmark(oBmk);

            //headers
            foreach (HeaderPart oHeader in oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>())
            {
                oBmks = oHeader.Header.Descendants<BookmarkStart>().Where(b => b.Name.Value.StartsWith("_mps")).ToArray<BookmarkStart>();
                foreach (BookmarkStart oBmk in oBmks)
                    DeleteBookmark(oBmk);
            }

            //footers
            foreach (FooterPart oFooter in oDoc.MainDocumentPart.GetPartsOfType<FooterPart>())
            {
                oBmks = oFooter.Footer.Descendants<BookmarkStart>().Where(b => b.Name.Value.StartsWith("_mps")).ToArray<BookmarkStart>();
                foreach (BookmarkStart oBmk in oBmks)
                    DeleteBookmark(oBmk);
            }
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// deletes mSEG content control and reboomarks
        /// </summary>
        /// <param name="omSeg"></param>
        /// <param name="iBmkID"></param>
        private static void DeleteSegmentContentControl(WordprocessingDocument oDoc, SdtElement omSeg, ref int iBmkID)
        {
            //GLOG 8484: Skip non-Forte Content Controls
            if (Query.GetTag(omSeg) == "")
                return;
            //do children first
            //JTS 11/9/16: reworked for speed
            //JTS: Reverse order to process innermost CCs first
            //GLOG 8484: Avoid error on non-Forte Content Controls
            SdtElement[] ccs = omSeg.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mps")).Reverse().ToArray();

            //var ccs = from cc in omSeg.Descendants<SdtElement>().Reverse()
            //          where (from t in cc.Descendants<Tag>()
            //                 where t.Val.Value.Substring(0, 3) == "mps"
            //                 select t).Any()
            //          select cc;
            foreach (SdtElement occ in ccs)
                DeleteSegmentContentControl(oDoc, occ, ref iBmkID);
            //delete existing bookmark
            string xBmk = "_" + Query.GetTag(omSeg);
            //Corresponding bookmark may not be child of CC or immediate Parent,
            //so search from top-most ancestor
            BookmarkStart oStart = omSeg.Ancestors().Last().Descendants<BookmarkStart>().Where(x => x.Name == xBmk).FirstOrDefault();
            BookmarkEnd oEnd = null;
            if (oStart != null)
            {
                DeleteBookmark(oStart);
            }
            bool bIsCollectionTableItem = Query.IsCollectionTableItem(oDoc, omSeg);
            bool bIsCollectionTable = Query.IsCollectionTable(oDoc, omSeg); //GLOG 8486
            string xObjectData = Query.GetAttributeValue(oDoc, omSeg, "ObjectData");
            bool bSideBySide = Query.GetmSEGObjectDataValue(xObjectData, "AllowSideBySide").ToUpper() == "TRUE";

            //GLOG 8474: Find child sdtContent regardless of type
            OpenXmlElement oCCContent = omSeg.Descendants().Where(e => e.LocalName == "sdtContent").First();
            //rebookmark
            if (omSeg is SdtRow)
            {
                Table oTable = omSeg.Ancestors<Table>().First();
                TableRow oLastRow = oTable.Descendants<TableRow>().Last();
                TableRow oRow = omSeg.Descendants<TableRow>().First();
                int iCells = oRow.Descendants<TableCell>().Count() - 1;
                int iRows = oTable.Descendants<TableRow>().Count();
                oStart = new BookmarkStart() { ColumnFirst = 0, ColumnLast = iCells }; //, DisplacedByCustomXml = DisplacedByCustomXmlValues.Next };
                oStart.Name = xBmk;
                oStart.Id = iBmkID.ToString();
                oEnd = new BookmarkEnd();
                oEnd.Id = oStart.Id;
                if (bIsCollectionTable)
                {
                    //GLOG 15808: make sure start and end of bookmark are both within table
                    oTable.Descendants<TableCell>().First().PrependChild(oStart);
                    oTable.Descendants<TableCell>().Last().AppendChild(oEnd);

                    //GLOG 8486: If Collection table, wrap bookmark around table
                    //oTable.InsertBeforeSelf(oStart);
                    //oTable.InsertAfterSelf(oEnd);
                }
                else
                {
                    omSeg.InsertBeforeSelf(oStart);
                    //GLOG 8486: For Collection table items, if last row insert bookmarkend after table
                    //otherwise insert after current row
                    if (iRows > 1 && oRow.IsBefore(oLastRow))
                    {
                        oRow.Descendants<TableCell>().Last().AppendChild(oEnd);
                        //oRow.InsertAfterSelf(oEnd);
                    }
                    else
                    {
                        if (bIsCollectionTableItem)
                        {
                            //GLOG 15808: make sure start and end of bookmark are both within table
                            oTable.Descendants<TableCell>().Last().AppendChild(oEnd);
                        }
                        else
                        {
                            oTable.InsertAfterSelf(oEnd);
                        }
                    }
                }
                //if (iRows > 1)
                //{
                //    omSeg.Ancestors<Table>().First().AppendChild(oEnd);
                //}
                //else
                //{
                //    omSeg.InsertAfterSelf(oEnd);
                //}
            }
            else
            {
                TableRow oRow = omSeg.Ancestors<TableRow>().FirstOrDefault();
                Table oTable = omSeg.Descendants<Table>().FirstOrDefault();
                //GLOG 8484
                Paragraph oPara = omSeg.Descendants<Paragraph>().FirstOrDefault();
                if (oRow != null)
                {
                    int iFirstCell = 0;
                    int iLastCell = 0;
                    TableCell[] oCells = oRow.Descendants<TableCell>().ToArray<TableCell>();
                    if (oCells.Count() > 1 && bIsCollectionTableItem && bSideBySide)
                    {
                        //If mSEG is not in first cell, bookmark last cell of row
                        if (!omSeg.IsBefore(oCells[1]))
                        {
                            iFirstCell = oCells.Count() - 1;
                            iLastCell = iFirstCell;
                        }
                    }
                    oStart = new BookmarkStart() { ColumnFirst = iFirstCell, ColumnLast = iLastCell, DisplacedByCustomXml = DisplacedByCustomXmlValues.Next };
                }
                else
                {
                    oStart = new BookmarkStart(); // { DisplacedByCustomXml = DisplacedByCustomXmlValues.Next };

                }
                oStart.Name = xBmk;
                oStart.Id = iBmkID.ToString();
                oEnd = new BookmarkEnd();
                oEnd.Id = oStart.Id;
                //GLOG 8467
                if (oPara != null && oPara.InnerText == "     " && oCCContent.Descendants<Paragraph>().Count() == 1)
                {
                    //Clear out placeholder spaces
                    oPara.RemoveAllChildren<Run>();
                }
                if (bIsCollectionTable)
                {
                    //GLOG 15808: make sure start and end of bookmark are both within table
                    oTable.Descendants<TableCell>().First().PrependChild(oStart);
                    oTable.Descendants<TableCell>().Last().AppendChild(oEnd);
                    //GLOG 8486: If Collection table, wrap bookmark around table
                    //oTable.InsertBeforeSelf(oStart);
                    //oTable.InsertAfterSelf(oEnd);
                }
                else
                {
                    //Wrap Start and End bookmark around elements of SdtContent
                    oCCContent.PrependChild(oStart);
                    //GLOG 8679: If TextBox content, place bookmarkend before final paragraph
                    if (oCCContent.Ancestors<TextBoxContent>().FirstOrDefault() != null)
                    {
                        Paragraph oLastPara = oCCContent.Descendants<Paragraph>().LastOrDefault();
                        if (oLastPara != null)
                        {
                            SdtElement oParaCC = oLastPara.Ancestors<SdtElement>().FirstOrDefault();
                            //GLOG 8868: If last paragraph is inside variable CC place Bookmark end outside of it
                            if (oParaCC != null && oParaCC != omSeg)
                            {
                                oParaCC.InsertAfterSelf(oEnd);
                            }
                            else
                            {
                                oLastPara.AppendChild(oEnd);
                            }
                        }
                        else
                            oCCContent.AppendChild(oEnd);
                    }
                    else
                    {
                        oCCContent.AppendChild(oEnd);
                    }
                }
            }
            iBmkID++;
            OpenXmlElementList oContent = omSeg.Descendants().Where(x => x.LocalName == "sdtContent").FirstOrDefault().ChildElements;
            foreach (OpenXmlElement oChild in oContent)
            {
                OpenXmlElement oNewChild = (OpenXmlElement)oChild.Clone();
                omSeg.Parent.InsertBefore(oNewChild, omSeg);
            }
            omSeg.Remove();
        }

        public static SdtElement GetParentSegmentContentControl(SdtElement oCC)
        {
            //JTS 11/9/16
            //GLOG 8484: Avoid error on non-Forte Content Controls
            return oCC.Ancestors<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mps")).FirstOrDefault();
            //var ccs = from cc in oCC.Ancestors<SdtElement>()
            //          where (from pr in cc.Elements<SdtProperties>()
            //                 where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mps")).Any()
            //                 select pr).Any()
            //          select cc;
            //if (ccs.Count() > 0)
            //    return ccs.First();
            //else
            //    return null;
        }

        /// <summary>
        /// deletes the doc vars associated with all content controls contained in oNode
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="oNode"></param>
        public static void DeleteAssociatedDocVars(WordprocessingDocument oDoc, OpenXmlElement oNode)
        {
            DeleteAssociatedDocVars(oDoc, oNode, "", "", "");
        }
        /// <summary>
        /// deletes the doc vars associated with all content controls contained in oNode -
        /// limits to or skips element and/or tags as specified
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="oNode"></param>
        /// <param name="xLimitToElement"></param>
        /// <param name="xSkipElement"></param>
        /// <param name="xSkipTags"></param>
        public static void DeleteAssociatedDocVars(WordprocessingDocument oDoc, OpenXmlElement oNode,
            string xLimitToElement, string xSkipElement, string xSkipTags)
        {
            foreach (SdtProperties oProps in oNode.Descendants<SdtProperties>())
            {
                //GLOG 8484: Avoid error on non-Forte Content Controls
                Tag oTag = oProps.Elements<Tag>().FirstOrDefault();
                if (oTag != null)
                {
                    string xTag = oTag.Val.Value;
                    string xElement = GetBaseName(xTag);
                    if (((xLimitToElement == "") || (xElement == xLimitToElement)) &&
                        (xElement != xSkipElement) && (xSkipTags.IndexOf(xTag) == -1))
                        DeleteAssociatedDocVars(oDoc, xTag);
                }
            }
        }

        /// <summary>
        /// deletes the doc vars associated with the specified tag
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xTag"></param>
        public static void DeleteAssociatedDocVars(WordprocessingDocument oDoc, string xTag)
        {
            string xVar = "";

            //get settings part
            Settings oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            //set query
            //JTS 11/9/16: reworked for speed
            xVar = "mpo" + xTag.Substring(3, 8);
            DocumentVariable oDocVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
            //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
            //               where dv.Name == xVar
            //               select dv;

            //delete mpo variable
            if (oDocVar != null)
                oDocVar.Remove();

            //delete mpd variables
            if (GetBaseName(xTag) == "mSEG")
            {
                int iIndex = 1;
                DocumentVariable oVar = null;
                string xVarRoot = "mpd" + xTag.Substring(3, 8);
                xVar = xVarRoot + "01";
                //JTS 11/9/16
                oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                while (oVar != null)
                {
                    string xValue = oVar.Val.Value;
                    oVar.Remove();

                    //delete the doc vars for the ccs in the deleted scopes xml
                    if (LMP.String.IsEncrypted(xValue))
                        xValue = LMP.String.Decrypt(xValue);

                    string xDeletedVars = DeleteScope.GetAssociatedDocVars(xValue);
                    if (xDeletedVars != "")
                    {
                        string[] oAssocVars = xDeletedVars.Split('|');
                        for (int i = 0; i < oAssocVars.GetLength(0); i++)
                        {
                            xVar = oAssocVars[i];
                            DocumentVariable oAssocVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                            if (oAssocVar != null)
                                oAssocVar.Remove();
                        }
                    }

                    //get next
                    iIndex++;
                    xVar = xVarRoot;
                    if (iIndex < 10)
                        xVar += "0";
                    xVar += iIndex.ToString();
                    oVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// returns the nearest ancestor content control of the specified type
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="xBaseName"></param>
        /// <returns></returns>
        public static SdtElement GetNearestAncestorContentControl(OpenXmlElement oNode, string xBaseName)
        {
            if (xBaseName == "")
            {
                if (oNode.Ancestors<SdtElement>().Count() > 0)
                    return oNode.Ancestors<SdtElement>().First();
            }
            else
            {
                string xPrefix = GetTagPrefix(xBaseName);
                //JTS 11/9/16
                return oNode.Ancestors<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith(xPrefix)).FirstOrDefault();

                //var oCCs = from cc in oNode.Ancestors<SdtElement>()
                //           where (
                //               from pr in cc.Elements<SdtProperties>()
                //               where (pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith(xPrefix)).Any())
                //               select pr).Any()
                //           select cc;
                //if (oCCs.Count() > 0)
                //    return oCCs.First();
            }

            //none found
            return null;
        }

        /// <summary>
        /// returns the tag of the specified content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        public static string GetTag(SdtElement oCC)
        {
            try
            {
                return oCC.Elements<SdtProperties>().First().Elements<Tag>().First().Val.Value;
            }
            catch
            {
                //GLOG 8484: May not be Forte Content Control
                return "";
            }
        }
        public static void ClearPlaceholderText(SdtElement oCC)
        {
            //Remove <w:showingPlcHdr/> attribute if present
            try
            {
                oCC.Elements<SdtProperties>().First<SdtProperties>().RemoveAllChildren<ShowingPlaceholder>();
            }
            catch { }
        }
        public static void CopyAndReTagHeadersFooters(SectionProperties oCurProp, SectionProperties oSrcProps, WordprocessingDocument oDoc)
        {
            oCurProp.RemoveAllChildren<HeaderReference>();
            oCurProp.RemoveAllChildren<FooterReference>();

            foreach (HeaderReference oHR in oSrcProps.Descendants<HeaderReference>())
            {
                HeaderPart oSourceHeader = (HeaderPart)oDoc.MainDocumentPart.GetPartById(oHR.Id);

                if (oSourceHeader != null)
                {
                    HeaderPart oHeaderPart = oDoc.MainDocumentPart.AddNewPart<HeaderPart>();
                    string xPartId = oDoc.MainDocumentPart.GetIdOfPart(oHeaderPart);
                    HeaderReference oRef = new HeaderReference() { Id = xPartId, Type = oHR.Type };
                    oCurProp.Append(oRef);
                    oHeaderPart.Header = new Header(oSourceHeader.Header.OuterXml);
                    Header oHeader = oHeaderPart.Header;
                    List<SdtElement> oHeaderCCs = oHeader.Descendants<SdtElement>().ToList();
                    oHeaderCCs.Reverse();
                    foreach (SdtElement oHeaderCC in oHeaderCCs)
                    {
                        Query.ReTagContentControl(oHeaderCC, oDoc, true);
                    }
                    foreach (ImagePart oImage in oSourceHeader.ImageParts)
                    {
                        string xId = oSourceHeader.GetIdOfPart(oImage);
                        oHeaderPart.CreateRelationshipToPart(oImage, xId);
                    }
                    oHeader.Save();
                }
            }
            foreach (FooterReference oFR in oSrcProps.Descendants<FooterReference>())
            {
                FooterPart oSourceFooter = (FooterPart)oDoc.MainDocumentPart.GetPartById(oFR.Id);
                if (oSourceFooter != null)
                {
                    FooterPart oFooterPart = oDoc.MainDocumentPart.AddNewPart<FooterPart>();
                    string xPartId = oDoc.MainDocumentPart.GetIdOfPart(oFooterPart);
                    FooterReference oRef = new FooterReference() { Id = xPartId, Type = oFR.Type };
                    oCurProp.Append(oRef);
                    oFooterPart.Footer = new Footer(oSourceFooter.Footer.OuterXml);
                    Footer oFooter = oFooterPart.Footer;
                    List<SdtElement> oFooterCCs = oFooter.Descendants<SdtElement>().ToList();
                    oFooterCCs.Reverse();
                    foreach (SdtElement oFooterCC in oFooterCCs)
                    {
                        Query.ReTagContentControl(oFooterCC, oDoc, true);
                    }
                    foreach (ImagePart oImage in oSourceFooter.ImageParts)
                    {
                        string xId = oSourceFooter.GetIdOfPart(oImage);
                        oFooterPart.CreateRelationshipToPart(oImage, xId);
                    }
                    oFooter.Save();
                }
            }
        }
        //GLOG 15826
        public static void ReTagAndRenameBlock(SdtElement oCC, XmlSegment oParent, WordprocessingDocument oDoc)
        {
            //GLOG 8678: Blocks can't have multiple tags, so rename duplicates
            Query.ReTagContentControl(oCC, oDoc);
            string xObjectData = Query.GetAttributeValue(oDoc, oCC, "ObjectData");
            string xBlockName = Query.GetmBlockObjectDataValue(xObjectData, Query.BlockProperties.Name);
            string xBaseName = xBlockName;
            int b = 1;
            int iUnderScore = xBlockName.IndexOf('_');
            if (iUnderScore > -1)
            {
                //If Name already ends with number, increment for new name
                string xNum = xBlockName.Substring(iUnderScore + 1);
                if (int.TryParse(xNum, out b))
                {
                    b = b + 1;
                    xBaseName = xBlockName.Substring(0, iUnderScore);
                }
            }
            string xNewBlockName = xBaseName;
            do
            {
                xNewBlockName = xBaseName + "_" + b++.ToString();
            } while (oParent.Blocks.BlockExists(xNewBlockName));
            Query.SetAttributeValue(oDoc, oCC, "Name", xNewBlockName, false, "");
            xObjectData = xObjectData.Replace(xBlockName + "|", xNewBlockName + "|");
            Query.SetAttributeValue(oDoc, oCC, "ObjectData", xObjectData, false, "");
        }
        public static void ReTagContentControl(SdtElement oCC, WordprocessingDocument oDoc)
        {
            //Delete associated Bookmark by default
            ReTagContentControl(oCC, oDoc, true);
        }
        public static void ReTagContentControl(SdtElement oCC, WordprocessingDocument oDoc, bool bDeleteBookmark)
        {
            string xx = oCC.InnerXml;
            string xOldTag = GetTag(oCC);
            //GLOG 8484: May not be Forte Content Control
            if (xOldTag == "")
                return;

            string xOldID = xOldTag.Substring(3, 8);
            string xNewID = GenerateDocVarID(oDoc);
            string xNewTag = xOldTag.Substring(0, 3) + xNewID + xOldTag.Substring(11);
            oCC.InnerXml = oCC.InnerXml.Replace(xOldTag, xNewTag);
            string xVar = "";

            //get settings part
            Settings oSettings = oDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            //set query
            //JTS 11/9/16: reworked for speed
            xVar = "mpo" + xOldID;
            DocumentVariable oDocVar = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Name == xVar).FirstOrDefault();

            //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
            //               where dv.Name == xVar
            //               select dv;
            string xValue = "";
            if (oDocVar != null)
                xValue = oDocVar.Val.Value;
            if (xValue == "")
                return;
            xVar = "mpo" + xNewID;
            oSettings.Elements<DocumentVariables>().FirstOrDefault().AppendChild(
                new DocumentVariable() { Name = xVar, Val = xValue });
            oSettings.Save();
            BookmarkStart bmk = oDoc.MainDocumentPart.RootElement.Descendants<BookmarkStart>().Where<BookmarkStart>(b => b.Name.Value == "_" + xOldTag).FirstOrDefault();
            if (bmk != null)
            {
                string xOldBmkID = bmk.Id.Value;
                BookmarkEnd bmkEnd = oDoc.MainDocumentPart.RootElement.Descendants<BookmarkEnd>().Where<BookmarkEnd>(b => b.Id.Value == xOldBmkID).FirstOrDefault();
                if (bDeleteBookmark)
                {
                    bmk.Remove();
                    if (bmkEnd.Count() > 0)
                        bmkEnd.Remove();
                }
                else
                {
                    //Don't change ID for BookmarkStart element unless corresponding BookmarkEnd element has been found
                    if (bmkEnd != null)
                    {
                        //Assign Bookmark new unique ID using generated Doc Var ID
                        bmk.Id.Value = xNewID;
                        bmkEnd.Id.Value = xNewID;
                    }
                }

            }
        }
        public static void SplitRun(Run r, int[] positions)
        {
            //get existing run text
            string text = r.Descendants<Text>().FirstOrDefault().Text;

            //initialize split text array
            var splitText = new string[positions.Length + 1];

            //initialize start pos
            int startPos = 0;

            //fill splitText array
            for (int i = 0; i < positions.Length + 1; i++)
            {
                if (i != positions.Length)
                {
                    splitText[i] = text.Substring(startPos, positions[i] - startPos);
                    startPos = positions[i];
                }
                else
                    splitText[i] = text.Substring(startPos, text.Length - startPos);
            }

            //split run & replace text, starting from last
            //allows usage of InsertAfterSelf method
            for (int i = splitText.Length - 1; i >= 0; i--)
            {
                Text t = new Text(splitText[i]);
                t.Space = SpaceProcessingModeValues.Preserve;

                if (i == 0)
                {
                    //replace existing run text
                    r.RemoveAllChildren<Text>();
                    r.Append(t);
                }
                else
                {
                    //clone, append text & insert
                    Run newRun = (Run)r.CloneNode(true);
                    newRun.RemoveAllChildren<Text>();
                    newRun.Append(t);
                    r.InsertAfterSelf(newRun);
                }
            }

        }

        /// <summary>
        /// returns Text length of the specified Element
        /// </summary>
        /// <returns></returns>
        static int GetRunLength(OpenXmlElement e)
        {
            return e.Descendants<Text>().FirstOrDefault().ToString().Length;
        }
        /// <summary>
        /// Returns type of Header or Footer (first, odd, even)
        /// </summary>
        /// <param name="oHF"></param>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static HeaderFooterValues GetHeaderFooterType(OpenXmlPart oHF, WordprocessingDocument oDoc)
        {
            if (oHF is HeaderPart)
            {
                string xRelID = oDoc.MainDocumentPart.GetIdOfPart(oHF);

                HeaderReference oRef = oDoc.MainDocumentPart.Document.Body.Descendants<HeaderReference>().Where(h => h.Id == xRelID).FirstOrDefault<HeaderReference>();
                if (oRef != null)
                {
                    return oRef.Type;
                }
                else
                    return (HeaderFooterValues)(-1);

            }
            else if (oHF is FooterPart)
            {
                string xRelID = oDoc.MainDocumentPart.GetIdOfPart(oHF);
                FooterReference oRef = oDoc.MainDocumentPart.Document.Body.Descendants<FooterReference>().Where(f => f.Id == xRelID).FirstOrDefault<FooterReference>();
                if (oRef != null)
                {
                    return oRef.Type;
                }
                else
                    return (HeaderFooterValues)(-1);
            }
            return (HeaderFooterValues)(-1);
        }
        /// <summary>
        /// Return HeaderPart of the specified Type in the specified Section of oDoc
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="iSection"></param>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static HeaderPart GetHeaderPartByType(HeaderFooterValues iType, int iSection, WordprocessingDocument oDoc)
        {
            SectionProperties[] oSecProps = oDoc.MainDocumentPart.Document.Body.Descendants<SectionProperties>().ToArray<SectionProperties>();
            if (iSection < 1 || iSection > oSecProps.Count())
                return null;
            else
            {
                SectionProperties oSecPr = oSecProps[iSection - 1];
                HeaderReference oRef = oSecPr.Descendants<HeaderReference>().Where(h => h.Type == iType).FirstOrDefault<HeaderReference>();
                if (oRef != null)
                {
                    return (HeaderPart)oDoc.MainDocumentPart.GetPartById(oRef.Id);
                }
                else
                    return null;

            }

        }
        /// <summary>
        /// Return FooterPart of the specified Type in the specified Section of oDoc
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="iSection"></param>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static FooterPart GetFooterPartByType(HeaderFooterValues iType, int iSection, WordprocessingDocument oDoc)
        {
            SectionProperties[] oSecProps = oDoc.MainDocumentPart.Document.Body.Descendants<SectionProperties>().ToArray<SectionProperties>();
            if (iSection < 1 || iSection > oSecProps.Count())
                return null;
            else
            {
                SectionProperties oSecPr = oSecProps[iSection - 1];
                FooterReference oRef = oSecPr.Descendants<FooterReference>().Where(f => f.Type == iType).FirstOrDefault<FooterReference>();
                if (oRef != null)
                {
                    return (FooterPart)oDoc.MainDocumentPart.GetPartById(oRef.Id);
                }
                else
                    return null;

            }
        }
        /// <summary>
        /// Returns OpenXMLPart containing an particular Content Control
        /// </summary>
        /// <param name="oElement"></param>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static OpenXmlPart GetContainingOpenXmlPart(OpenXmlElement oElement, WordprocessingDocument oDoc)
        {
            //TODO: Is there a way to get to this more directly without cycling through all Parts?
            if (oDoc.MainDocumentPart.RootElement.Descendants().Contains(oElement))
                return oDoc.MainDocumentPart;

            foreach (HeaderPart oHeader in oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>())
            {
                if (oHeader.RootElement != null && oHeader.RootElement.Descendants().Contains(oElement))
                    return oHeader;
            }
            foreach (FooterPart oFooter in oDoc.MainDocumentPart.GetPartsOfType<FooterPart>())
            {
                if (oFooter.RootElement != null && oFooter.RootElement.Descendants().Contains(oElement))
                    return oFooter;
            }
            return null;

        }
        public static void CopyStyles(WordprocessingDocument oSource, WordprocessingDocument oTarget, bool bInUseOnly, bool bOverwriteExisting, bool bIncludeDocDefaults)
        {
            DateTime t0 = DateTime.Now;
            try
            {
                //JTS: If StylesWithEffects part exists, copy from that as well
                List<Styles> oSourceList = new List<Styles>();
                List<Styles> oTargetList = new List<Styles>();
                if (oSource.MainDocumentPart.StylesWithEffectsPart != null && oTarget.MainDocumentPart.StylesWithEffectsPart != null)
                {
                    oSourceList.Add(oSource.MainDocumentPart.StylesWithEffectsPart.Styles);
                    oTargetList.Add(oTarget.MainDocumentPart.StylesWithEffectsPart.Styles);
                }
                if (oSource.MainDocumentPart.StyleDefinitionsPart != null && oTarget.MainDocumentPart.StyleDefinitionsPart != null)
                {
                    oSourceList.Add(oSource.MainDocumentPart.StyleDefinitionsPart.Styles);
                    oTargetList.Add(oTarget.MainDocumentPart.StyleDefinitionsPart.Styles);
                }

                if (oSourceList.Count == 0)
                    return;

                List<string> oStylesInUse = new List<string>();
                if (bInUseOnly)
                {
                    string[] aStyles = Query.GetStylesInUse(oSource);
                    oStylesInUse.AddRange(aStyles);
                    //Nothing to do
                    if (oStylesInUse.Count() == 0)
                        return;
                }

                for (int i = 0; i < oSourceList.Count; i++)
                {
                    Styles oSourceStyles = oSourceList[i];
                    Styles oTargetStyles = oTargetList[i];
                    foreach (OpenXmlElement oChild in oSourceStyles)
                    {
                        if (oChild is Style)
                        {
                            Style oSty = (Style)oChild;
                            //Check for existance of style name in Target
                            //GLOG 8605: For built-in styles, StyleID can vary by language, but Name will be consistent, so match that instead
                            Style oTargetSty = oTargetStyles.Descendants<Style>().Where(s => s.StyleName.Val.Value == oSty.StyleName.Val.Value).FirstOrDefault();
                            if (oTargetSty == null || bOverwriteExisting)
                            {
                                if (oTargetSty == null)
                                {
                                    if (!bInUseOnly || oStylesInUse.Contains(oSty.StyleName.Val.Value))
                                    {
                                        //If style is missing, add with source properties
                                        oTargetSty = new Style(oSty.OuterXml);
                                        oTargetStyles.AppendChild<Style>(oTargetSty);
                                    }
                                }
                                else if ((bOverwriteExisting && !bInUseOnly) || (bInUseOnly && oStylesInUse.Contains(oSty.StyleName.Val.Value)))
                                {
                                    //Replace existing style with source definition
                                    oTargetSty.InnerXml = oSty.InnerXml;
                                }
                            }
                        }
                        else if (oChild is DocDefaults && !bInUseOnly && bOverwriteExisting && bIncludeDocDefaults)
                        {
                            DocDefaults oDefs = (DocDefaults)oChild;
                            DocDefaults oTargetDefs = oTargetStyles.Descendants<DocDefaults>().First();
                            if (oTargetDefs != null)
                            {
                                oTargetDefs.InnerXml = oDefs.InnerXml;
                            }
                        }
                    }
                    oTargetStyles.Save();
                }
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
        }
        public static string[] GetStylesInUse(WordprocessingDocument oDoc)
        {
            List<string> oList = new List<string>();
            GetPartStylesInUse(oDoc.MainDocumentPart, oDoc, ref oList);
            foreach (HeaderPart h in oDoc.MainDocumentPart.HeaderParts)
            {
                GetPartStylesInUse(h, oDoc, ref oList);
            }
            foreach (FooterPart f in oDoc.MainDocumentPart.FooterParts)
            {
                GetPartStylesInUse(f, oDoc, ref oList);
            }
            //GLOG 15921: Include styles in notes and comments parts
            if (oDoc.MainDocumentPart.FootnotesPart != null)
            {
                GetPartStylesInUse(oDoc.MainDocumentPart.FootnotesPart, oDoc, ref oList);
            }
            if (oDoc.MainDocumentPart.EndnotesPart != null)
            {
                GetPartStylesInUse(oDoc.MainDocumentPart.EndnotesPart, oDoc, ref oList);
            }
            if (oDoc.MainDocumentPart.WordprocessingCommentsPart != null)
            {
                GetPartStylesInUse(oDoc.MainDocumentPart.WordprocessingCommentsPart, oDoc, ref oList);
            }
            return oList.ToArray<string>();
        }
        private static void GetPartStylesInUse(OpenXmlPart oPart, WordprocessingDocument oDoc, ref List<string> oList)
        {
            Styles oStyles = oDoc.MainDocumentPart.GetPartsOfType<StylesPart>().First().Styles;
            ParagraphStyleId[] aPStyles = oPart.RootElement.Descendants<ParagraphStyleId>().ToArray<ParagraphStyleId>();
            //Find direct paragraph style formatting
            foreach (ParagraphStyleId oPSty in aPStyles)
            {
                string xID = oPSty.Val;
                Style oStyle = oStyles.Elements<Style>().Where(s => s.StyleId.Value == xID).FirstOrDefault<Style>();
                if (oStyle != null)
                {
                    if (!oList.Contains(oStyle.StyleName.Val))
                        oList.Add(oStyle.StyleName.Val);
                }
            }
            //Find direct character style formatting
            RunStyle[] aRStyles = oPart.RootElement.Descendants<RunStyle>().ToArray<RunStyle>();
            foreach (RunStyle oRSty in aRStyles)
            {
                string xID = oRSty.Val;
                Style oStyle = oStyles.Elements<Style>().Where(s => s.StyleId.Value == xID).FirstOrDefault<Style>();
                if (oStyle != null)
                {
                    if (!oList.Contains(oStyle.StyleName.Val))
                        oList.Add(oStyle.StyleName.Val);
                }
            }
            //Find direct table style formatting
            TableStyle[] aTStyles = oPart.RootElement.Descendants<TableStyle>().ToArray<TableStyle>();
            foreach (TableStyle oTSty in aTStyles)
            {
                string xID = oTSty.Val;
                Style oStyle = oStyles.Elements<Style>().Where(s => s.StyleId.Value == xID).FirstOrDefault<Style>();
                if (oStyle != null)
                {
                    if (!oList.Contains(oStyle.StyleName.Val))
                        oList.Add(oStyle.StyleName.Val);
                }
            }
        }
        /// <summary>
        /// Replaces contents of Styles and StylesWithEffects parts in Target doc
        /// </summary>
        /// <param name="oSource"></param>
        /// <param name="oTarget"></param>
        public static void ReplaceDocStyles(WordprocessingDocument oSource, WordprocessingDocument oTarget)
        {
            DateTime t0 = DateTime.Now;
            StylesPart oPart = oSource.MainDocumentPart.StyleDefinitionsPart;
            if (oPart != null)
            {
                StylesPart oTargetPart = oTarget.MainDocumentPart.StyleDefinitionsPart;
                if (oTargetPart != null)
                {
                    oTargetPart.RootElement.InnerXml = oPart.RootElement.InnerXml;
                    oTargetPart.RootElement.Save();
                }
            }
            oPart = oSource.MainDocumentPart.StylesWithEffectsPart;
            if (oPart != null)
            {
                StylesPart oTargetPart = oTarget.MainDocumentPart.StylesWithEffectsPart;
                if (oTargetPart != null)
                {
                    oTargetPart.RootElement.InnerXml = oPart.RootElement.InnerXml;
                    oTargetPart.RootElement.Save();
                }
            }
            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// Update certain settings in target document with values from template document
        /// </summary>
        /// <param name="oTemplate"></param>
        /// <param name="oTarget"></param>
        public static void UpdateSettingsFromTemplate(WordprocessingDocument oTemplate, WordprocessingDocument oTarget)
        {
            DateTime t0 = DateTime.Now;
            DocumentSettingsPart oSourceSettings = oTemplate.MainDocumentPart.DocumentSettingsPart;
            DocumentSettingsPart oTargetSettings = oTarget.MainDocumentPart.DocumentSettingsPart;
            if (oSourceSettings != null && oTargetSettings != null)
            {
                //w:stylePaneFormatFilter
                StylePaneFormatFilter oSourceFilter = oSourceSettings.RootElement.Descendants<StylePaneFormatFilter>().FirstOrDefault();

                if (oSourceFilter != null)
                {
                    //Update Style Pane options
                    StylePaneFormatFilter oTargetFilter = oTargetSettings.RootElement.Descendants<StylePaneFormatFilter>().FirstOrDefault();
                    if (oTargetFilter == null)
                    {
                        //If no existing StylePane settings, prepend copy of source settings
                        oTargetSettings.RootElement.PrependChild(oSourceFilter.CloneNode(true));
                    }
                    else
                    {
                        oTargetSettings.RootElement.ReplaceChild(oSourceFilter.CloneNode(true), oTargetFilter);
                    }
                }
                //w:zoom
                Zoom oSourceZoom = oSourceSettings.RootElement.Descendants<Zoom>().FirstOrDefault();
                if (oSourceZoom != null)
                {
                    //Update Default Zoom
                    Zoom oTargetZoom = oTargetSettings.RootElement.Descendants<Zoom>().FirstOrDefault();
                    if (oTargetZoom == null)
                    {
                        //If no existing Zoom settings, prepend copy of source settings
                        oTargetSettings.RootElement.PrependChild(oSourceZoom.CloneNode(true));
                    }
                    else
                    {
                        oTargetSettings.RootElement.ReplaceChild(oSourceZoom.CloneNode(true), oTargetZoom);
                    }
                }
                //w:doNotIncludeSubdocsInStats
                DoNotIncludeSubdocsInStats oSourceStats = oSourceSettings.RootElement.Descendants<DoNotIncludeSubdocsInStats>().FirstOrDefault();
                DoNotIncludeSubdocsInStats oTargetStats = oTargetSettings.RootElement.Descendants<DoNotIncludeSubdocsInStats>().FirstOrDefault();
                if (oSourceStats != null)
                {
                    if (oTargetStats == null)
                    {
                        oTargetSettings.RootElement.AppendChild(oSourceStats.CloneNode(true));
                    }
                    else
                    {
                        oTargetSettings.RootElement.ReplaceChild(oSourceStats.CloneNode(true), oTargetStats);
                    }
                }
                else
                {
                    if (oTargetStats != null)
                    {
                        //Missing value is same as False setting
                        oTargetStats.Remove();
                    }
                }
                //GLOG 15881
                //w:embedTrueTypeFonts
                EmbedTrueTypeFonts oSourceEmbed = oSourceSettings.RootElement.Descendants<EmbedTrueTypeFonts>().FirstOrDefault();
                EmbedTrueTypeFonts oTargetEmbed = oTargetSettings.RootElement.Descendants<EmbedTrueTypeFonts>().FirstOrDefault();
                if (oSourceEmbed != null)
                {
                    if (oTargetEmbed == null)
                    {
                        oTargetSettings.RootElement.AppendChild(oSourceEmbed.CloneNode(true));
                    }
                    else
                    {
                        oTargetSettings.RootElement.ReplaceChild(oSourceEmbed.CloneNode(true), oTargetEmbed);
                    }
                }
                else
                {
                    if (oTargetEmbed != null)
                    {
                        //Missing value is same as False setting
                        oTargetEmbed.Remove();
                    }
                }
                //w:saveSubsetFonts
                SaveSubsetFonts oSourceSubset = oSourceSettings.RootElement.Descendants<SaveSubsetFonts>().FirstOrDefault();
                SaveSubsetFonts oTargetSubset = oTargetSettings.RootElement.Descendants<SaveSubsetFonts>().FirstOrDefault();
                if (oSourceSubset != null)
                {
                    if (oTargetSubset == null)
                    {
                        oTargetSettings.RootElement.AppendChild(oSourceSubset.CloneNode(true));
                    }
                    else
                    {
                        oTargetSettings.RootElement.ReplaceChild(oSourceSubset.CloneNode(true), oTargetSubset);
                    }
                }
                else
                {
                    if (oTargetSubset != null)
                    {
                        //Missing value is same as False setting
                        oTargetSubset.Remove();
                    }
                }
                //w:embedSystemFonts
                EmbedSystemFonts oSourceSysFonts = oSourceSettings.RootElement.Descendants<EmbedSystemFonts>().FirstOrDefault();
                EmbedSystemFonts oTargetSysFonts = oTargetSettings.RootElement.Descendants<EmbedSystemFonts>().FirstOrDefault();
                if (oSourceSysFonts != null)
                {
                    if (oTargetSysFonts == null)
                    {
                        oTargetSettings.RootElement.AppendChild(oSourceSysFonts.CloneNode(true));
                    }
                    else
                    {
                        oTargetSettings.RootElement.ReplaceChild(oSourceSysFonts.CloneNode(true), oTargetSysFonts);
                    }
                }
                else
                {
                    if (oTargetSysFonts != null)
                    {
                        //Missing value is same as True setting
                        oTargetSysFonts.Remove();
                    }
                }

            }
            LMP.Benchmarks.Print(t0);
        }
        public static bool IsCollectionTableItem(WordprocessingDocument oDoc, SdtElement oCC)
        {
            if (Query.GetBaseName(oCC) == "mSEG")
            {
                string xObjectData = Query.GetAttributeValue(oDoc, oCC, "ObjectData");
                int iPos = xObjectData.IndexOf("|ObjectTypeID=") + 14;
                if (iPos > 13)
                {
                    int iPos2 = xObjectData.IndexOf("|", iPos);
                    if (iPos2 > -1)
                    {
                        string xObjectTypeID = xObjectData.Substring(iPos, iPos2 - iPos);
                        switch (xObjectTypeID)
                        {
                            case "500":
                            case "501":
                            case "502":
                            case "509":
                            case "513":
                            case "525":
                                return true;
                        }
                    }
                }
            }
            return false;
        }
        public static bool IsCollectionTable(WordprocessingDocument oDoc, SdtElement oCC)
        {
            if (Query.GetBaseName(oCC) == "mSEG")
            {
                string xObjectData = Query.GetAttributeValue(oDoc, oCC, "ObjectData");
                int iPos = xObjectData.IndexOf("|ObjectTypeID=") + 14;
                if (iPos > 13)
                {
                    int iPos2 = xObjectData.IndexOf("|", iPos);
                    if (iPos2 > -1)
                    {
                        string xObjectTypeID = xObjectData.Substring(iPos, iPos2 - iPos);
                        switch (xObjectTypeID)
                        {
                            case "506":
                            case "507":
                            case "508":
                            case "526":
                            case "527":
                            case "528":
                                return true;
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Set Document Compatibility for a specific Word version
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="shVersion"></param>
        public static void SetWordCompatibility(WordprocessingDocument oDoc, short shVersion)
        {
            Settings oSettings = oDoc.MainDocumentPart.DocumentSettingsPart.Settings;
            if (oSettings != null)
            {
                CompatibilitySetting oCompSet = oSettings.Descendants<CompatibilitySetting>().FirstOrDefault();
                if (oCompSet != null)
                {
                    //CompatibilitySetting already exists - update value
                    if (oCompSet.Val.Value == shVersion.ToString())
                    {
                        //Compatibility already matches - do nothing
                        return;
                    }
                    oCompSet.Val = new StringValue(shVersion.ToString());
                }
                else
                {
                    CompatibilitySetting oNewSet = new CompatibilitySetting()
                    {
                        Name = new EnumValue<CompatSettingNameValues>(CompatSettingNameValues.CompatibilityMode),
                        Val = new StringValue(shVersion.ToString()),
                        Uri = new StringValue("http://schemas.microsoft.com/office/word")
                    };
                    Compatibility oCompOld = oSettings.Descendants<Compatibility>().FirstOrDefault();
                    if (oCompOld != null)
                    {
                        //No existing CompatibilitySetting - prepend to Compatibility child
                        oCompOld.PrependChild(oNewSet);
                    }
                    else
                    {
                        //Create new Compatiblity child
                        oSettings.PrependChild(new Compatibility(oNewSet));
                    }
                }
                oSettings.Save();
            }
        }
        public static string GetElementInnerText(OpenXmlElement oElement, bool bTrimDelimiters)
        {
            //InnerText property of SdtElement will not include linebreaks or paragraph marks
            //This function recurses child elements, appending appropriate separator characters
            //between text elements
            string xText = "";
            if (oElement.ChildElements.Count() > 0)
            {
                foreach (OpenXmlElement oChild in oElement.ChildElements)
                {
                    xText += GetElementInnerText(oChild, false);
                }
            }
            else
            {
                xText += oElement.InnerText;
            }
            if (oElement is Paragraph)
            {
                xText += "\r";
            }
            else if (oElement is Break)
            {
                xText += "\v";
            }
            if (bTrimDelimiters)
            {
                xText = xText.TrimEnd('\v', '\r');
            }
            return xText;
        }
        /// <summary>
        /// Attach Forte Document Schema if not currently attached
        /// </summary>
        /// <param name="oDoc"></param>
        public static void AttachForteSchemaIfNecessary(WordprocessingDocument oDoc)
        {
            DateTime t0 = DateTime.Now;
            Settings oSettings = oDoc.MainDocumentPart.DocumentSettingsPart.Settings;
            AttachedSchema oSchema = oSettings.Descendants<AttachedSchema>().Where(v => v.Val.Value == LMP.Data.ForteConstants.MacPacNamespace).FirstOrDefault();
            if (oSchema == null)
            {
                oSettings.AppendChild<AttachedSchema>(new AttachedSchema() { Val = LMP.Data.ForteConstants.MacPacNamespace });
                oSettings.Save();
            }
            LMP.Benchmarks.Print(t0);
        }
        public static string GetStyleProperty(WordprocessingDocument oDoc, string xStyleName, string xVBAProperty)
        {
            Styles oStyles = null;
            try
            {
                if (oDoc.MainDocumentPart.StyleDefinitionsPart != null)
                    oStyles = oDoc.MainDocumentPart.StyleDefinitionsPart.Styles;

                if (oStyles == null)
                    return "";

                DocDefaults oDefaults = oStyles.Descendants<DocDefaults>().FirstOrDefault();
                Style oStyle = oStyles.Descendants<Style>().Where(s => s.StyleName.Val.ToString().ToUpper() == xStyleName.ToUpper()).FirstOrDefault();
                if (xVBAProperty.ToLower().StartsWith("para."))
                    //replace alternate form with full text
                    xVBAProperty = "ParagraphFormat." + xVBAProperty.Substring(5);
                string xProperty = xVBAProperty;
                if (oStyle != null)
                {
                    List<Style> oStyleList = Query.GetBaseStyleList(oStyles, oStyle);
                    foreach (Style oItem in oStyleList)
                    {
                        string xTest = "";
                        int iVal = 0;
                        BasedOn oBase = oItem.BasedOn;
                        if (xVBAProperty.ToLower().StartsWith("font."))
                        {
                            xProperty = xVBAProperty.Substring(5);
                            OpenXmlElement oRunProps = oItem.StyleRunProperties;
                            List<OpenXmlElement> aRunProps = new List<OpenXmlElement>();
                            if (oRunProps != null)
                                aRunProps.Add(oRunProps);
                            if (oBase == null)
                            {
                                OpenXmlElement oDefRunProps = oDefaults.RunPropertiesDefault.RunPropertiesBaseStyle;
                                if (oDefRunProps != null)
                                    aRunProps.Add(oDefRunProps);
                            }
                            if (oBase == null && aRunProps.Count == 0)
                            {
                                //add dummy item so that for loop will execute
                                aRunProps.Add(new StyleRunProperties());
                            }
                            for (int iCount = 0; iCount < aRunProps.Count; iCount++)
                            {
                                OpenXmlElement oRpr = aRunProps[iCount];
                                switch (xProperty.ToLower())
                                {
                                    case "name":
                                    case "nameascii":
                                    case "namebi":
                                    case "namefareast":
                                    case "nameother":
                                        //<w:rFonts>
                                        RunFonts oFonts = oRpr.Descendants<RunFonts>().FirstOrDefault();
                                        string xXml = oStyles.OuterXml;
                                        if (oFonts != null)
                                        {
                                            switch (xProperty.ToLower())
                                            {
                                                case "name":
                                                case "nameascii":
                                                    if (oFonts.Ascii != null)
                                                        return oFonts.Ascii.Value;
                                                    break;
                                                case "namebi":
                                                    if (oFonts.ComplexScript != null)
                                                        return oFonts.ComplexScript.Value;
                                                    break;
                                                case "namefareast":
                                                    if (oFonts.EastAsia != null)
                                                        return oFonts.EastAsia.Value;
                                                    break;
                                                case "nameother":
                                                    if (oFonts.HighAnsi != null)
                                                        return oFonts.HighAnsi.Value;
                                                    break;
                                            }
                                        }
                                        break;
                                    case "size":
                                        //<w:sz>
                                        FontSize oSz = oRpr.Descendants<FontSize>().FirstOrDefault();
                                        if (oSz != null)
                                        {
                                            //Value is in half-points
                                            return (Int32.Parse(oSz.Val.Value) / 2).ToString();
                                        }
                                        break;
                                    case "allcaps":
                                        //<w:caps/>
                                        Caps oCaps = oRpr.Descendants<Caps>().FirstOrDefault();
                                        if (oCaps != null)
                                        {
                                            if (oCaps.Val.Value)
                                                return "-1";
                                            else
                                                return "0";
                                        }
                                        else if (oBase == null && iCount == aRunProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                    case "bold":
                                        //<w:b/>
                                        Bold oBold = oRpr.Descendants<Bold>().FirstOrDefault();
                                        if (oBold != null)
                                        {
                                            if (oBold.Val.Value)
                                                return "-1";
                                            else
                                                return "0";
                                        }
                                        else if (oBase == null && iCount == aRunProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                    case "italic":
                                        //<w:i/>
                                        Italic oItalic = oRpr.Descendants<Italic>().FirstOrDefault();
                                        if (oItalic != null)
                                        {
                                            if (oItalic.Val.Value)
                                                return "-1";
                                            else
                                                return "0";
                                        }
                                        else if (oBase == null && iCount == aRunProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                    case "underline":
                                        //<w:u/>
                                        Underline oUL = oRpr.Descendants<Underline>().FirstOrDefault();
                                        if (oUL != null)
                                        {
                                            return oUL.Val.Value.ToString();
                                        }
                                        else if (oBase == null && iCount == aRunProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                    case "smallcaps":
                                        //<w:smallCaps/>
                                        SmallCaps oSmallCaps = oRpr.Descendants<SmallCaps>().FirstOrDefault();
                                        if (oSmallCaps != null)
                                        {
                                            if (oSmallCaps.Val.Value)
                                                return "-1";
                                            else
                                                return "0";
                                        }
                                        else if (oBase == null && iCount == aRunProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                    case "hidden":
                                        //<w:vanish/>
                                        Vanish oVanish = oRpr.Descendants<Vanish>().FirstOrDefault();
                                        if (oVanish != null)
                                        {
                                            if (oVanish.Val.Value)
                                                return "-1";
                                            else
                                                return "0";
                                        }
                                        else if (oBase == null && iCount == aRunProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                }
                            }
                        }
                        else if (xVBAProperty.ToLower().StartsWith("paragraphformat."))
                        {
                            xProperty = xVBAProperty.Substring(16);
                            List<OpenXmlElement> aParaProps = new List<OpenXmlElement>();
                            OpenXmlElement oPara = oItem.StyleParagraphProperties;
                            if (oPara != null)
                                aParaProps.Add(oPara);
                            if (oBase == null)
                            {
                                OpenXmlElement oRunPara = oDefaults.ParagraphPropertiesDefault.ParagraphPropertiesBaseStyle;
                                if (oRunPara != null)
                                {
                                    aParaProps.Add(oRunPara);
                                }
                            }
                            if (oBase == null && aParaProps.Count == 0)
                            {
                                //add dummy item so that for loop will execute
                                aParaProps.Add(new StyleParagraphProperties());
                            }
                            for (int iProp = 0; iProp < aParaProps.Count; iProp++)
                            {
                                OpenXmlElement oPpr = aParaProps[iProp];
                                switch (xProperty.ToLower())
                                {
                                    case "alignment":
                                        //<w:jc>
                                        Justification oJc = oPpr.Descendants<Justification>().FirstOrDefault();
                                        if (oJc != null)
                                        {
                                            //Convert to wdParagraphAlignment value
                                            switch (oJc.Val.Value)
                                            {
                                                case JustificationValues.Both:
                                                    return "3";
                                                case JustificationValues.Right:
                                                    return "2";
                                                case JustificationValues.Center:
                                                    return "1";
                                                case JustificationValues.Distribute:
                                                    return "4";
                                                case JustificationValues.HighKashida:
                                                    return "7";
                                                case JustificationValues.LowKashida:
                                                    return "8";
                                                case JustificationValues.MediumKashida:
                                                    return "5";
                                                case JustificationValues.ThaiDistribute:
                                                    return "9";
                                                default:
                                                    return "0";
                                            }
                                        }
                                        else if (oBase == null && iProp == aParaProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                    case "linespacing":
                                    case "linespacingrule":
                                    case "spacebefore":
                                    case "spaceafter":
                                        //<w:spacing>
                                        SpacingBetweenLines oSBL = oPpr.Descendants<SpacingBetweenLines>().FirstOrDefault();
                                        if (oSBL != null)
                                        {
                                            switch (xProperty.ToLower())
                                            {
                                                case "linespacing":
                                                    //<w:line>
                                                    xTest = oSBL.Line.Value;
                                                    if (Int32.TryParse(xTest, out iVal))
                                                    {
                                                        return (iVal / 20).ToString();
                                                    }
                                                    else if (oBase == null && iProp == aParaProps.Count - 1)
                                                    {
                                                        //value wasn't found in any base style or docDefaults
                                                        return "0";
                                                    }
                                                    break;
                                                case "linespacingrule":
                                                    //<w:lineRule>
                                                    xTest = oSBL.LineRule.Value.ToString();
                                                    LineSpacingRuleValues oRule;
                                                    if (Enum.TryParse(xTest, out oRule))
                                                    {
                                                        switch (oRule)
                                                        {
                                                            case LineSpacingRuleValues.Auto:
                                                                //auto returns different values depending on linespacing
                                                                string xLine = GetStyleProperty(oDoc, oStyle.StyleName.Val.Value, "paragraphformat.linespacing");
                                                                if (String.IsNumericInt32(xLine))
                                                                {
                                                                    int iLines = Int32.Parse(xLine) * 20;
                                                                    Single sMult = iLines / 240;
                                                                    if (sMult == 1)
                                                                        //single spacing
                                                                        return "0";
                                                                    else if (sMult == 1.5)
                                                                        //1.5 spacing
                                                                        return "1";
                                                                    else if (sMult == 2)
                                                                        //double spacing
                                                                        return "2";
                                                                    else
                                                                        //multiple
                                                                        return "5";
                                                                }
                                                                break;
                                                            case LineSpacingRuleValues.Exact:
                                                                return "4";
                                                            case LineSpacingRuleValues.AtLeast:
                                                                return "3";
                                                        }
                                                    }
                                                    else if (oBase == null && iProp == aParaProps.Count - 1)
                                                    {
                                                        //value wasn't found in any base style or docDefaults
                                                        return "0";
                                                    }
                                                    break;
                                                case "spacebefore":
                                                    //<w:before>
                                                    xTest = oSBL.Before.Value;
                                                    if (Int32.TryParse(xTest, out iVal))
                                                    {
                                                        return (iVal / 20).ToString();
                                                    }
                                                    else if (oBase == null && iProp == aParaProps.Count - 1)
                                                    {
                                                        //value wasn't found in any base style or docDefaults
                                                        return "0";
                                                    }
                                                    break;
                                                case "spaceafter":
                                                    //<w:after>
                                                    xTest = oSBL.After.Value;
                                                    if (Int32.TryParse(xTest, out iVal))
                                                    {
                                                        return (iVal / 20).ToString();
                                                    }
                                                    else if (oBase == null && iProp == aParaProps.Count - 1)
                                                    {
                                                        //value wasn't found in any base style or docDefaults
                                                        return "0";
                                                    }
                                                    break;
                                            }
                                        }
                                        else if (oBase == null && iProp == aParaProps.Count - 1)
                                            return "0";
                                        break;
                                    case "firstlineindent":
                                    case "leftindent":
                                    case "rightindent":
                                    case "hangingindent":
                                        Indentation oInd = oPpr.Descendants<Indentation>().FirstOrDefault();
                                        if (oInd != null)
                                        {
                                            switch (xProperty.ToLower())
                                            {
                                                case "firstlineindent":
                                                    //<w:firstLine>
                                                    //FirstLine value is ignored if there is hanging value, in which case the returned value for FirstLineIndent
                                                    //will depend on the LeftIndent and HangingIndent values
                                                    xTest = GetStyleProperty(oDoc, oStyle.StyleName.Val.Value, "paragraphformat.hangingindent");
                                                    if (Int32.TryParse(xTest, out iVal) && iVal != 0)
                                                    {
                                                        int iHanging = iVal;
                                                        xTest = GetStyleProperty(oDoc, oStyle.StyleName.Val.Value, "paragraphformat.leftindent");
                                                        if (Int32.TryParse(xTest, out iVal))
                                                        {
                                                            return (iVal - iHanging).ToString();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (oInd.FirstLine != null)
                                                        {
                                                            xTest = oInd.FirstLine.Value;
                                                            if (Int32.TryParse(xTest, out iVal))
                                                            {
                                                                return (iVal / 20).ToString();
                                                            }
                                                        }
                                                        else if (oBase == null && iProp == aParaProps.Count - 1)
                                                        {
                                                            //value wasn't found in any base style or docDefaults
                                                            return "0";
                                                        }
                                                    }
                                                    break;
                                                case "hangingindent":
                                                    //Not a property in VBA - only used to adjust leftindent and firstlineindent if necessary
                                                    //<w:hanging>
                                                    if (oInd.Hanging != null)
                                                    {
                                                        xTest = oInd.Hanging.Value;
                                                        if (Int32.TryParse(xTest, out iVal))
                                                        {
                                                            return (iVal / 20).ToString();
                                                        }
                                                    }
                                                    else if (oBase == null && iProp == aParaProps.Count - 1)
                                                    {
                                                        //value wasn't found in any base style or docDefaults
                                                        return "0";
                                                    }
                                                    break;
                                                case "leftindent":
                                                    //<w:left>
                                                    if (oInd.Left != null)
                                                    {
                                                        xTest = oInd.Left.Value;
                                                        int iLeft = 0;
                                                        if (Int32.TryParse(xTest, out iVal))
                                                        {
                                                            iLeft = iVal / 20;
                                                            //returned value will be affected if there is a hanging indent defined
                                                            xTest = GetStyleProperty(oDoc, oStyle.StyleName.Val.Value, "paragraphformat.hangingindent");
                                                            if (Int32.TryParse(xTest, out iVal))
                                                            {
                                                                iLeft = iLeft + iVal;
                                                            }
                                                            return iLeft.ToString();
                                                        }
                                                    }
                                                    else if (oBase == null && iProp == aParaProps.Count - 1)
                                                    {
                                                        //value wasn't found in any base style or docDefaults
                                                        return "0";
                                                    }
                                                    break;
                                                case "rightindent":
                                                    //<w:right>
                                                    if (oInd.Right != null)
                                                    {
                                                        xTest = oInd.Right.Value;
                                                        if (Int32.TryParse(xTest, out iVal))
                                                        {
                                                            return (iVal / 20).ToString();
                                                        }
                                                    }
                                                    else if (oBase == null && iProp == aParaProps.Count - 1)
                                                    {
                                                        //value wasn't found in any base style or docDefaults
                                                        return "0";
                                                    }
                                                    break;
                                            }
                                        }
                                        else if (oBase == null && iProp == aParaProps.Count - 1)
                                        {
                                            //value wasn't found in any base style or docDefaults
                                            return "0";
                                        }
                                        break;
                                    case "keeptogether":
                                        KeepLines oKeepLines = oPpr.Descendants<KeepLines>().FirstOrDefault();
                                        if (oKeepLines != null)
                                        {
                                            if (oKeepLines.Val.Value)
                                                return "-1";
                                            else
                                                return "0";
                                        }
                                        else if (oBase == null && iProp == aParaProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                    case "keepwithnext":
                                        KeepNext oKeepNext = oPpr.Descendants<KeepNext>().FirstOrDefault();
                                        if (oKeepNext != null)
                                        {
                                            if (oKeepNext.Val.Value)
                                                return "-1";
                                            else
                                                return "0";
                                        }
                                        else if (oBase == null && iProp == aParaProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                    case "outlinelevel":
                                        OutlineLevel oLevel = oPpr.Descendants<OutlineLevel>().FirstOrDefault();
                                        if (oLevel != null)
                                        {
                                            //OpenXML value is zero-based
                                            return (oLevel.Val.Value - 1).ToString();
                                        }
                                        else if (oBase == null && iProp == aParaProps.Count - 1)
                                        {
                                            //No more base styles to check
                                            return "0";
                                        }
                                        break;
                                }
                            }
                        }
                        else
                        {
                            switch (xProperty.ToLower())
                            {
                                case "languageid":
                                case "languageidfareast":
                                    //<w:lang>
                                    Languages oLang = oItem.Descendants<Languages>().FirstOrDefault();
                                    if (oLang == null && oBase == null)
                                    {
                                        oLang = oDefaults.Descendants<Languages>().FirstOrDefault();
                                    }
                                    if (oLang != null)
                                    {
                                        if (xProperty.ToLower() == "languageidfareast")
                                            xTest = oLang.EastAsia.Value;
                                        else
                                            xTest = oLang.Val.Value;
                                    }
                                    if (xTest != "")
                                    {
                                        //convert language string to LCID
                                        try
                                        {
                                            return new System.Globalization.CultureInfo(xTest).LCID.ToString();
                                        }
                                        catch { }
                                    }
                                    break;
                                case "namelocal":
                                    //<w:name>
                                    return oItem.StyleName.Val.Value;
                                case "basestyle":
                                    //<w:basedOn>
                                    if (oBase != null)
                                    {
                                        return oBase.Val.Value;
                                    }
                                    break;
                                case "nextparagraphstyle":
                                    //<w:next>
                                    if (oItem.NextParagraphStyle != null)
                                    {
                                        return oItem.NextParagraphStyle.Val.Value;
                                    }
                                    else if (oBase == null)
                                        return oItem.StyleName.Val.Value;
                                    break;
                                case "noproofing":
                                    //<w:noProof/>
                                    NoProof oNoP = oItem.Descendants<NoProof>().FirstOrDefault();
                                    if (oNoP != null)
                                    {
                                        if (oNoP.Val.Value)
                                            return "-1";
                                        else
                                            return "0";
                                    }
                                    else if (oBase == null)
                                        return "0";
                                    break;
                                case "quickstyle":
                                    //<w:qFormat/>
                                    PrimaryStyle oQF = oItem.Descendants<PrimaryStyle>().FirstOrDefault();
                                    if (oQF != null)
                                    {
                                        if (oQF.Val.Value == OnOffOnlyValues.On)
                                            return "-1";
                                        else
                                            return "0";
                                    }
                                    else if (oBase == null)
                                        return "0";
                                    break;
                            }
                        }
                    }
                }
            }
            catch { }

            return "";
        }
        /// <summary>
        /// Return Style object including all properties inherited from base styles
        /// </summary>
        /// <param name="oStyles"></param>
        /// <param name="xStyleName"></param>
        /// <returns></returns>
        public static List<Style> GetBaseStyleList(Styles oStyles, Style oStyle)
        {
            string xStyleID = oStyle.StyleId.Value;
            IEnumerable<Style> oStyleList = StyleChainReverseOrder(oStyles, xStyleID);
            return new List<Style>(oStyleList);
        }
        private static IEnumerable<Style> StyleChainReverseOrder(Styles oStyles, string styleId)
        {
            string xCurrent = styleId;
            while (true)
            {
                Style oStyle = oStyles.Elements<Style>()
                    .Where(s => s.StyleId == xCurrent).FirstOrDefault();
                yield return oStyle;

                if (oStyle.BasedOn != null)
                    xCurrent = oStyle.BasedOn.Val;
                else
                    yield break;
            }
        }

        private static IEnumerable<Style> StyleChain(Styles oStyles, string styleId)
        {
            return StyleChainReverseOrder(oStyles, styleId).Reverse();
        }
        public static void RemoveOrphanedParts(WordprocessingDocument oDoc)
        {
            //GLOG 15756
            HeaderPart[] oHeaderParts = oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>().ToArray();
            for (int i = oHeaderParts.GetUpperBound(0); i >= 0; i--)
            {
                HeaderPart oPart = oHeaderParts[i];
                string xPartID = oDoc.MainDocumentPart.GetIdOfPart(oPart);
                HeaderFooterValues iType = GetHeaderFooterType(oPart, oDoc);
                if (iType < 0)
                {
                    //Header is not related to any existing section
                    oDoc.MainDocumentPart.DeletePart(xPartID);
                }
            }
            FooterPart[] oFooterParts = oDoc.MainDocumentPart.GetPartsOfType<FooterPart>().ToArray();
            for (int i = oFooterParts.GetUpperBound(0); i >= 0; i--)
            {
                FooterPart oPart = oFooterParts[i];
                string xPartID = oDoc.MainDocumentPart.GetIdOfPart(oPart);
                HeaderFooterValues iType = GetHeaderFooterType(oPart, oDoc);
                if (iType < 0)
                {
                    //Footer is not related to any existing section
                    oDoc.MainDocumentPart.DeletePart(xPartID);
                }
            }
        }
        public static string[,] GetCustomPropertiesArray(WordprocessingDocument oDoc)
        {
            //GLOG 15753
            string[,] aValues = null;
            CustomFilePropertiesPart oPart = oDoc.CustomFilePropertiesPart;
            if (oPart != null)
            {
                CustomDocumentProperty[] aProps = oPart.Properties.Descendants<CustomDocumentProperty>().ToArray();
                if (aProps.GetLength(0) > 0)
                {
                    aValues = new string[aProps.GetLength(0), 2];
                    for (int i = 0; i < aProps.GetLength(0); i++)
                    {
                        aValues[i, 0] = aProps[i].Name.Value;
                        aValues[i, 1] = aProps[i].InnerText;
                    }
                }

            }
            return aValues;
        }
        public static void GetEmbeddedPartsXml(string xXML, ref string xBodyXml, ref string xEmbeddedXml, ref string xFootnotesXml, ref string xEndnotesXML,
            ref string xCommentsXml, OpenXmlPart oTargetPart, WordprocessingDocument oTargetDoc, XmlForteDocument oForteDoc)
        {
            //Get XML necessary to recreate Comments, Footnotes, Endnotes and Images
            Random oRand = new Random();

            int iPos = xXML.IndexOf("<w:body>");
            if (iPos > -1)
            {
                iPos = iPos + 8;
                int iPos2 = xXML.IndexOf("</w:body>", iPos);
                xBodyXml = xXML.Substring(iPos, iPos2 - iPos);
                //GLOG 8878: Remove WordML bookmark tags
                xBodyXml = Regex.Replace(xBodyXml, "<aml:annotation.*?>", "");
                if (xBodyXml.IndexOf("<w:drawing>") > -1)
                {
                    //JTS: For some reason these namespaces need to be explicitly referenced or there will be an error attempting 
                    //to set Content Control InnerXml from xBodyXml, even though these are all included in the Document namespaces
                    xBodyXml = xBodyXml.Replace("<w:drawing>", "<w:drawing xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\"" +
                        " xmlns:wp14=\"http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing\"" + 
                        " xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">");
                }
            }
            //Each embed type consists of XML for Relationship and Part
            MatchCollection oMatches = Regex.Matches(xXML, "<Relationship Id=.*?/>");
            foreach (Match oMatch in oMatches)
            {
                string xRel = oMatch.Value;
                iPos = xRel.IndexOf("Target=\"");
                if (iPos > -1)
                {
                    int iPos2 = xRel.IndexOf("\"", iPos + 8);
                    if (iPos > -1)
                    {
                        string xTarget = xRel.Substring(iPos + 8, iPos2 - (iPos + 8));
                        switch (xTarget)
                        {
                            case "footnotes.xml":
                            case "endnotes.xml":
                            case "comments.xml": //GLOG 15921
                                string xType = xTarget.Replace("s.xml", "");
                                Match oNotePart = Regex.Match(xXML, "<pkg:part pkg:name=\"/word/" + xTarget + "\".*?</pkg:part>");
                                string xNotesXml = oNotePart.Value;
                                if (xNotesXml != "")
                                {
                                    MatchCollection oNotes = Regex.Matches(xNotesXml, "<w:" + xType + " w:id=\".*?</w:" + xType + ">");
                                    int iStart = 0;
                                    foreach (Match oNote in oNotes)
                                    {
                                        string xOldId = "";
                                        string xNewId = "";
                                        string xValue = oNote.Value;
                                        int iIdStart = xValue.IndexOf("\"", iStart);
                                        if (iIdStart > -1)
                                        {
                                            int iIdEnd = xValue.IndexOf("\"", iIdStart + 1);
                                            if (iIdEnd > -1)
                                            {
                                                xOldId = xValue.Substring(iIdStart + 1, iIdEnd - (iIdStart + 1));
                                                //Skip Footnote Continuation and Separator
                                                if (Int32.Parse(xOldId) > 0)
                                                {
                                                    //Generate new random ID - 32767 appears to largest value allowed
                                                    xNewId = oRand.Next(10000, 32767).ToString();
                                                }
                                            }
                                            if (xOldId != "" && xNewId != "")
                                            {
                                                //Replace old ID in both Footnote and body
                                                xValue = xValue.Replace(" w:id=\"" + xOldId + "\"", " w:id=\"" + xNewId + "\"");
                                                xNotesXml = xNotesXml.Replace(oNote.Value, xValue);
                                                xBodyXml = xBodyXml.Replace("w:" + xType + "Reference w:id=\"" + xOldId + "\"", "w:" + xType + "Reference w:id=\"" + xNewId + "\"");
                                                //GLOG 15921: Comments also have associated start and end tags
                                                if (xTarget == "comments.xml")
                                                {
                                                    xBodyXml = xBodyXml.Replace("<w:commentRangeStart w:id=\"" + xOldId + "\"", "<w:commentRangeStart w:id=\"" + xNewId + "\"");
                                                    xBodyXml = xBodyXml.Replace("<w:commentRangeEnd w:id=\"" + xOldId + "\"", "<w:commentRangeEnd w:id=\"" + xNewId + "\"");
                                                }
                                                //This ID is different from Relationship ID
                                                //xRel = xRel.Replace(" Id=\"" + xOldId + "\"", " Id=\"" + xNewId + "\"");
                                            }
                                            if (xType == "endnote")
                                            {
                                                xEndnotesXML = xRel + xNotesXml;
                                            }
                                            else if (xType == "footnote")
                                            {
                                                xFootnotesXml = xRel + xNotesXml;
                                            }
                                            else
                                            {
                                                xCommentsXml = xRel + xNotesXml;
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                string xMatch = "<pkg:part[^<]*pkg:name=\"[^<]*" + xTarget + "\".*?</pkg:part>";
                                //JTS: need to use single-line option since image data may contain embedded newline characters
                                Match oEmbedPart = Regex.Match(xXML, xMatch, RegexOptions.Singleline);
                                if (oEmbedPart.Value != "")
                                {
                                    string xValue = oEmbedPart.Value;
                                    string xContentType = "";
                                    string xRelId = "";
                                    string xNewRelId = "";
                                    string xImageData = "";
                                    string xNewTarget = "";

                                    iPos = xRel.IndexOf(" Id=\"");
                                    if (iPos > -1)
                                    {
                                        iPos2 = xRel.IndexOf("\"", iPos + 5);
                                        if (iPos2 > iPos)
                                        {
                                            xRelId = xRel.Substring(iPos + 5, iPos2 - (iPos + 5));
                                        }
                                    }
                                    //Only include parts that correspond to images that are part of the body
                                    if (!xBodyXml.Contains(" r:embed=\"" + xRelId + "\""))
                                        continue;

                                    iPos = xValue.IndexOf(" pkg:contentType=\"");
                                    if (iPos > -1)
                                    {
                                        iPos2 = xValue.IndexOf("\"", iPos + 18);
                                        if (iPos2 > iPos)
                                        {
                                            xContentType = xValue.Substring(iPos + 18, iPos2 - (iPos + 18));
                                        }
                                    }
                                    iPos = xValue.IndexOf("<pkg:binaryData>");
                                    if (iPos != -1)
                                    {
                                        iPos2 = xValue.IndexOf("</pkg:binaryData>");
                                        if (iPos2 > iPos)
                                        {
                                            xImageData = xValue.Substring(iPos + @"<pkg:binaryData>".Length, iPos2 - (iPos + @"<pkg:binaryData>".Length));
                                        }
                                    }
                                    if (oForteDoc.ImageList == null)
                                    {
                                        XmlForteDocument.PopulateImageList(oForteDoc, oTargetDoc);
                                    }
                                    //JTS 1/17/16:
                                    //Name and ID of other images in document may have changed after original image was deleted.
                                    //So we need to check for existence of identical image already in document,
                                    //and adjust insertion XML to use proper ID and Target.
                                    OpenXmlPart oImagePart = null;
                                    foreach (ImageData oData in oForteDoc.ImageList)
                                    {
                                        if (oImagePart == null && oData.Compare(xContentType, xImageData))
                                        {
                                            oImagePart = oData.ImagePart;
                                            xNewTarget = oData.Target;
                                        }
                                        else if (oData.Target == xTarget)
                                        {
                                            //Target name is already in use by different image
                                            //Generate a new name
                                            xNewTarget = "Image" + oRand.Next(1000, 9999).ToString() + xTarget.Substring(xTarget.IndexOf('.'));
                                        }
                                    }

                                    if (oImagePart != null)
                                    {
                                        var oRel = oTargetPart.Parts.FirstOrDefault(r => r.OpenXmlPart == oImagePart);
                                        //Relationship already exists between these two parts, no need to recreate
                                        if (oRel != null)
                                        {
                                            xNewRelId = oRel.RelationshipId;
                                            xRel = "";
                                        }
                                        //No need to recreate Part, even if no relationship currently exists
                                        xValue = "";
                                    }
                                    if (xNewRelId == "")
                                    {
                                        //Original Relationship ID may have been repurposed - if so generate new random ID
                                        var oRel = oTargetPart.Parts.FirstOrDefault(r => r.RelationshipId == xRelId);
                                        if (oRel != null)
                                        {
                                            xNewRelId = "rId" + System.Guid.NewGuid().ToString().Replace("-", "").Substring(0, 16);
                                        }
                                    }
                                    if (xNewRelId != "")
                                    {
                                        xRel = xRel.Replace(" Id=\"" + xRelId + "\"", " Id=\"" + xNewRelId + "\"");
                                        xBodyXml = xBodyXml.Replace("r:embed=\"" + xRelId + "\"", "r:embed=\"" + xNewRelId + "\"");
                                    }
                                    if (xNewTarget != "")
                                    {
                                        xRel = xRel.Replace(" Target=\"" + xTarget + "\"", " Target=\"" + xNewTarget + "\"");
                                        if (oImagePart == null)
                                        {
                                            xValue = xValue.Replace(xTarget, xNewTarget);
                                        }
                                    }
                                    xEmbeddedXml = xEmbeddedXml + xRel + xValue;
                                }
                                break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Restore content in related parts for images, comments, footnotes and endnotes
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="xEmbeddedXml"></param>
        /// <param name="xFootnoteRefXml"></param>
        /// <param name="xEndnoteRefXml"></param>
        public static void RestoreEmbeddedParts(string xEmbeddedXml, string xFootnoteRefXml, string xEndnoteRefXml,
                string xCommentsRefXml, OpenXmlPart oPart, WordprocessingDocument oDoc, XmlForteDocument oForteDoc)
        {
            //GLOG 15921
            if (!string.IsNullOrEmpty(xFootnoteRefXml))
            {
                FootnotesPart oFootPart = oDoc.MainDocumentPart.GetPartsOfType<FootnotesPart>().FirstOrDefault();
                if (oFootPart == null)
                {
                    oFootPart = oDoc.MainDocumentPart.AddNewPart<FootnotesPart>();
                    //JTS 1/14/16:  I don't think the FootnotesPart will ever be missing, since it will have existed
                    //in the original design before block was deleted.  However, if this turns out not always to 
                    //be the case, we'll need to figure out how to create a valid FootnotesPart using the full
                    //content of xFootnoteRefXML
                }
                MatchCollection oFootnotes = Regex.Matches(xFootnoteRefXml, "<w:footnote w:id=\".*?</w:footnote>");
                foreach (Match oFoot in oFootnotes)
                {
                    oFootPart.RootElement.InnerXml = oFootPart.RootElement.InnerXml + oFoot.Value;
                }
                oFootPart.RootElement.Save();
            }
            if (!string.IsNullOrEmpty(xEndnoteRefXml))
            {
                EndnotesPart oEndPart = oDoc.MainDocumentPart.GetPartsOfType<EndnotesPart>().FirstOrDefault();
                if (oEndPart == null)
                {
                    oEndPart = oDoc.MainDocumentPart.AddNewPart<EndnotesPart>();
                    //JTS 1/14/16:  I don't think the EndnotesPart will ever be missing, since it will have existed
                    //in the original design before block was deleted.  However, if this turns out not always to 
                    //be the case, we'll need to figure out how to create a valid EndnotesPart using the full
                    //content of xFootnoteRefXML
                }
                MatchCollection oEndnotes = Regex.Matches(xEndnoteRefXml, "<w:endnote w:id=\".*?</w:endnote>");
                foreach (Match oEnd in oEndnotes)
                {
                    oEndPart.RootElement.InnerXml = oEndPart.RootElement.InnerXml + oEnd.Value;
                }
                oEndPart.RootElement.Save();
            }
            //GLOG 15921
            if (!string.IsNullOrEmpty(xCommentsRefXml))
            {
                WordprocessingCommentsPart oCommentPart = oDoc.MainDocumentPart.GetPartsOfType<WordprocessingCommentsPart>().FirstOrDefault();
                if (oCommentPart == null)
                {
                    oCommentPart = oDoc.MainDocumentPart.AddNewPart<WordprocessingCommentsPart>();
                    oCommentPart.Comments = new Comments();
                }
                MatchCollection oComments = Regex.Matches(xCommentsRefXml, "<w:comment w:id=\".*?</w:comment>");
                foreach (Match oComment in oComments)
                {
                    oCommentPart.RootElement.InnerXml = oCommentPart.RootElement.InnerXml + oComment.Value;
                }
                oCommentPart.RootElement.Save();
            }
            if (!string.IsNullOrEmpty(xEmbeddedXml))
            {

                if (oPart != null)
                {
                    if (oForteDoc.ImageList == null)
                    {
                        XmlForteDocument.PopulateImageList(oForteDoc, oDoc);
                    }
                    MatchCollection oRels = Regex.Matches(xEmbeddedXml, "<Relationship Id=.*?/>");
                    foreach (Match oRel in oRels)
                    {
                        string xRel = oRel.Value;
                        int iPos = xRel.IndexOf("Target=\"");
                        if (iPos > -1)
                        {
                            int iPos2 = xRel.IndexOf("\"", iPos + 8);
                            if (iPos > -1)
                            {
                                string xTarget = xRel.Substring(iPos + 8, iPos2 - (iPos + 8));
                                string xRelID = "";
                                string xContentType = "";
                                iPos = xRel.IndexOf(" Id=\"");
                                if (iPos > -1)
                                {
                                    iPos2 = xRel.IndexOf("\"", iPos + 5);
                                    if (iPos2 > iPos)
                                    {
                                        xRelID = xRel.Substring(iPos + 5, iPos2 - (iPos + 5));
                                    }
                                }
                                //JTS: Need to use Single-line option, since binary data may contain embedded new line characters
                                Match oEmbed = Regex.Match(xEmbeddedXml, "<pkg:part[^<]*pkg:name=\"[^<]*?" + xTarget + "\".*?</pkg:part>", RegexOptions.Singleline);
                                if (oEmbed.Value != "")
                                {
                                    string xValue = oEmbed.Value;
                                    iPos = xValue.IndexOf(" pkg:contentType=\"");
                                    if (iPos > -1)
                                    {
                                        iPos2 = xValue.IndexOf("\"", iPos + 18);
                                        if (iPos2 > iPos)
                                        {
                                            xContentType = xValue.Substring(iPos + 18, iPos2 - (iPos + 18));
                                        }

                                    }
                                    if (xRelID != "")
                                    {
                                        //Check if current image already has relationship to this part
                                        var oRelation = oPart.Parts.FirstOrDefault(p => p.RelationshipId == xRelID && p.OpenXmlPart.ContentType == xContentType);

                                        if (oRelation == null)
                                        {
                                            Uri imageUri = new Uri("/word/" + xTarget, UriKind.Relative);

                                            // Need to create Package directly, rather than using AddImagePart
                                            // Otherwise images are saved in \media instead of \word\media, which 
                                            // will break some COM DeleteScope functions
                                            System.IO.Packaging.PackagePart oPackage = oDoc.Package.CreatePart(imageUri, xContentType);
                                            iPos = xValue.IndexOf("<pkg:binaryData>");
                                            if (iPos != -1)
                                            {
                                                iPos2 = xValue.IndexOf("</pkg:binaryData>");
                                                if (iPos2 > iPos)
                                                {
                                                    string xImageData = xValue.Substring(iPos + @"<pkg:binaryData>".Length, iPos2 - (iPos + @"<pkg:binaryData>".Length));
                                                    //Restore escaped newline characters in XML
                                                    xImageData = xImageData.Replace("_x000d__x000a_", "\n");
                                                    byte[] oBytes = Convert.FromBase64String(xImageData);
                                                    //Write image to package
                                                    oPackage.GetStream().Write(oBytes, 0, oBytes.Length);
                                                }
                                            }
                                            System.IO.Packaging.PackagePart oPackagePart = oPart.OpenXmlPackage.Package.GetPart(new Uri(oPart.Uri.OriginalString, UriKind.Relative));
                                            // URI to the image is relative to releationship document.
                                            oPackagePart.CreateRelationship(
                                                  new Uri(xTarget, UriKind.Relative), System.IO.Packaging.TargetMode.Internal,
                                                  "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", xRelID);

                                            oPart.RootElement.Save();
                                        }
                                    }
                                }
                                else if (xRelID != "")
                                {
                                    //Image Part already exists - add part relationship
                                    foreach (ImageData oData in oForteDoc.ImageList)
                                    {
                                        if (oData.Target == xTarget)
                                        {
                                            oPart.AddPart<ImagePart>((ImagePart)oData.ImagePart, xRelID);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
