using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Architect.Oxml
{
    public class XmlNumberedLabels : XmlAdminSegment
    {
        protected override void InitializeValues(XmlPrefill oPrefill, string xRelativePath, bool bForcePrefillOverride, bool bChildStructureAvailable)
        {
            base.InitializeValues(oPrefill, xRelativePath, bForcePrefillOverride, bChildStructureAvailable);
        }
    }
}
