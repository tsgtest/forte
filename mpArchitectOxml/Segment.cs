using System;
using System.Xml;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using LMP.Data;
using System.Collections;
using LMP.Controls;
using LMP.Architect.Base;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.Architect.Oxml
{
    //TODO: create derivations of EventArgs for appropriate delegates below
    public delegate void AfterDefaultAuthorSetHandler(object sender, EventArgs e);
    public delegate void AfterDefaultValuesSetHandler(object sender, EventArgs e);
    public delegate void BeforeSegmentSetupHandler(object sender, EventArgs e);
    public delegate void SegmentGeneratedHandler(object sender, EventArgs e);
    public delegate void AfterAuthorUpdatedHandler(object sender, EventArgs e);
    public delegate void SegmentDeletedHandler(object sender, SegmentEventArgs oArgs);
    public delegate void SegmentAddedHandler(object sender, SegmentEventArgs oArgs);
    public delegate void ContactsAddedHandler(object sender, EventArgs e);
    public delegate void AfterAllVariablesVisitedHandler(object sender, EventArgs e);
    public delegate void CIDialogReleasedHandler(object sender, EventArgs e);
    public delegate void BeforeSegmentReplacedByActionHandler(object sender, SegmentEventArgs oArgs);
    public delegate void PaperTrayRequestedHandler(object sender, PaperTrayEventArgs e);
    public delegate void ActivationStateChangedHandler(object sender, ActivationStateChangedEventArgs e);
    public delegate void BeforeCollectionTableStructureInsertedHandler(object sender, SegmentEventArgs e);
    public delegate void AfterCollectionTableStructureInsertedHandler(object sender, SegmentEventArgs e);

    public class PaperTrayEventArgs : System.EventArgs
    {
        public int PaperTrayID;
        public PaperTrayEventArgs(int PaperTrayID)
        {
            PaperTrayID = this.PaperTrayID;
        }
    }
    public class SegmentValidationChangeEventArgs
    {
        bool SegmentValid;
        bool SegmentComplete;

        public SegmentValidationChangeEventArgs(bool bSegmentValid, bool bSegmentComplete)
        {
            this.SegmentValid = bSegmentValid;
            this.SegmentComplete = bSegmentComplete;
        }
    }
    public class ActivationStateChangedEventArgs
    {
        public bool Activated;

        public ActivationStateChangedEventArgs(bool bActivated)
        {
            this.Activated = bActivated;
        }
    }

    /// <summary>
    /// contains properties that define the arguments for the
    /// SegmentDeleted and SegmentAdded events
    /// </summary>
    public class SegmentEventArgs
    {
        private XmlSegment m_oSeg;

        public SegmentEventArgs(XmlSegment oSeg)
        {
            m_oSeg = oSeg;
        }

        public XmlSegment Segment
        {
            get { return m_oSeg; }
            set { m_oSeg = value; }
        }
    }

    /// <summary>
    /// contains the members that define a MacPac segment
    /// </summary>
    abstract public class XmlSegment : LMP.Architect.Base.Segment
    {
        #region *********************constants*********************
        //GLOG 15864
        public enum OxmlCreationMode
        {
            Default = 0,
            Finalize = 1,
            Design = 2,
            StaticFinish = 3
        }
        #endregion
        #region *********************enumerations*********************
        #endregion
        #region *********************events*********************
        public event AfterDefaultAuthorSetHandler AfterDefaultAuthorSet;
        public event AfterDefaultValuesSetHandler AfterDefaultValuesSet;
        public event BeforeSegmentSetupHandler BeforeSetup;
        public event SegmentGeneratedHandler Generated;
        public event LMP.Architect.Base.ValidationChangedHandler ValidationChange;
        public event AfterAuthorUpdatedHandler AfterAuthorUpdated;
        public event SegmentDeletedHandler SegmentDeleted;
        public event SegmentAddedHandler SegmentAdded;
        public event ContactsAddedHandler ContactsAdded;
        public event AfterAllVariablesVisitedHandler AfterAllVariablesVisited;
        public event CIDialogReleasedHandler CIDialogReleased;
        //public event ActivationStateChangedHandler ActivationStateChanged;
        //public static event PaperTrayRequestedHandler PaperTrayRequested;
        #endregion
        #region *********************fields*********************
        internal const byte MAX_VARIABLE_ACTIONS = 50;
        private WordprocessingDocument m_oWPDoc = null;
        private MemoryStream m_oStream = null;
        private XmlVariables m_oVariables;
        private XmlBlocks m_oBlocks;
        private XmlSegmentActions m_oSegmentActions;
        private XmlForteDocument m_oForteDocument;
        private XmlSegments m_oChildSegments;
        private XmlSegment m_oParentSegment;
        private XmlAuthors m_oAuthors;
        private XmlAuthors m_oPrevAuthors;
        private XmlSegment.Status m_iStatus;
        private bool m_bSegmentPreviouslyValid;
        internal bool m_bSkipAuthorActions = false;
        internal bool m_bAllowUpdateForAuthor = true;
        private XmlControlActions m_oCourtChooserControlActions;
        private int m_iCulture = LMP.Data.Language.mpUSEnglishCultureID;
        private XmlControlActions m_oLanguageControlActions;
        private bool m_bIsPrimary = false;
        private int m_iNextObjectDatabaseID = 1;
        private XmlSnapshot m_oSnapshot = null;
        #endregion
        #region *********************constructors*********************
        internal XmlSegment()
        {
            //setup authors-
            //create authors collection
            this.PreviousAuthors = new XmlAuthors(this);
            this.Authors = new XmlAuthors(this);
            m_oCourtChooserControlActions = new XmlControlActions(this);
            m_oLanguageControlActions = new XmlControlActions(this);
        }

        ~XmlSegment()
        {
            if (m_oStream != null)
            {
                m_oStream.Close();
                m_oStream.Dispose();
            }
        }
        #endregion
        #region *********************properties*********************
        public OxmlCreationMode CreationMode
        {
            get;
            set;
        }
        /// <summary>
        /// returns the segment's WordprocessingDocument object
        /// </summary>
        public WordprocessingDocument WPDoc
        {
            private set
            {
                m_oWPDoc = value;
            }
            get
            {
                if (m_oWPDoc == null)
                {
                    if (m_oStream != null)
                    {
                        //if stream already exists, reattach to WPDoc
                        m_oWPDoc = WordprocessingDocument.Open(m_oStream, true);
                    }
                    else
                    {
                        GetWPDocAndStream();
                    }
                }
                return m_oWPDoc;
            }
        }
        /// <summary>
        /// returns the segment's memory stream
        /// </summary>
        public MemoryStream Stream
        {
            get
            {
                if (m_oStream == null)
                {
                    GetWPDocAndStream();
                }

                return m_oStream;
            }
        }
        /// <summary>
        /// returns the flat Oxml for the segment
        /// </summary>
        public string FlatOxml
        {
            get
            {
                if (this.WPDoc != null)
                {
                    //Make sure all package changes are reflected in stream
                    FlushBuffer();
                    //GLOG 15850: If non-design document, remove Segment Content Controls from returned XML
                    string xXml = "";
                    if (this.ForteDocument.Mode != Base.ForteDocument.Modes.Design)
                    {
                        MemoryStream oCopyStream = new MemoryStream();
                        m_oStream.WriteTo(oCopyStream);
                        WordprocessingDocument oCopyDoc = WordprocessingDocument.Open(oCopyStream, true);
                        Query.DeleteSegmentContentControls(oCopyDoc);
                        oCopyDoc.Package.Flush();
                        oCopyDoc.Close();
                        xXml = FlatOpc.OpcToTextString(oCopyStream);
                        oCopyStream.Close();
                    }
                    else
                    {
                        xXml = FlatOpc.OpcToTextString(this.Stream);
                    }
                    xXml = LMP.String.RemoveBodyXmlNamespace(xXml);
                    xXml = LMP.String.RemoveXMLWhiteSpace(xXml);
                    //GLOG 8727: This required for correct functioning of CopyStylesFromTemplate
                    xXml = "<?xml version=\"1.0\" standalone=\"yes\"?>\r\n" + xXml;
                    return xXml;
                }
                else
                    return null;
            }
        }
        /// <summary>
        /// returns the segment's raw parts
        /// </summary>
        public List<RawSegmentPart> RawSegmentParts { set; get; }
        /// <summary>
        /// taken on Finish
        /// </summary>
        public XmlSnapshot Snapshot
        {
            get
            {
                try
                {
                    if (m_oSnapshot == null)
                        m_oSnapshot = new XmlSnapshot(this);

                    return m_oSnapshot;
                }
                catch
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// returns the variables of this segment
        /// </summary>
        public XmlVariables Variables
        {
            get
            {
                DateTime t0 = DateTime.Now;

                try
                {
                    if (m_oVariables == null)
                    {
                        m_oVariables = new XmlVariables(this);

                        //subscribe to notifications that variables have been validated
                        m_oVariables.ValidationChanged += new LMP.Architect.Base.ValidationChangedHandler(
                            OnVariablesValidationChanged);
                        m_oVariables.BeforeContactsUpdateEvent += new BeforeContactsUpdateHandler(m_oVariables_BeforeContactsUpdateEvent);
                        //m_oVariables.VariableVisited += new VariableVisitedHandler(OnVariableVisited);
                    }
                    return m_oVariables;
                }
                catch (System.Exception oE)
                {
                }
                finally
                {
                    LMP.Benchmarks.Print(t0, this.Name);
                }
                return null;
            }
            private set { this.m_oVariables = value; }
        }
        /// <summary>
        /// returns true iff the segment has a body block
        /// </summary>
        /// <returns></returns>
        public bool HasBody()
        {
            for (int i = 0; i < this.Blocks.Count; i++)
            {
                if (this.Blocks[i].IsBody)
                {
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// returns true iff the segment requires an author
        /// </summary>
        public new bool DefinitionRequiresAuthor
        {
            get
            {
                string xXML = this.Definition.XML;

                if (String.IsBase64SegmentString(xXML))
                {
                    xXML = XmlSegment.GetFlatXmlFromBase64OpcString(xXML);
                }

                xXML = xXML.ToUpper();

                //GLOG item #4390 - dcf
                //return true for answer files that specify
                //MaxAuthors > 0
                return (xXML.Contains("[LEADAUTHOR") || xXML.Contains("[AUTHOR") ||
                    xXML.Contains("[ADDITIONALAUTHORS") || xXML.Contains("[PARENT::LEADAUTHOR") ||
                    xXML.Contains("[PARENT::AUTHOR") || xXML.Contains("[PARENT::ADDITIONALAUTHORS")) ||
                    (this.Definition.IntendedUse == mpSegmentIntendedUses.AsAnswerFile &&
                    !xXML.Contains("|MaxAuthors=0|"));
            }
        }
        /// <summary>
        /// returns a collection of either basic or advanced variables
        /// </summary>
        /// <param name="oLevel"></param>
        /// <returns></returns>
        public System.Collections.Generic.List<XmlVariable> GetVariablesSubset(XmlVariable.Levels oDisplayLevel)
        {
            System.Collections.Generic.List<XmlVariable> oList = new System.Collections.Generic.List<XmlVariable>();

            for (int i = 0; i < this.Variables.Count; i++)
            {
                XmlVariable oVar = this.Variables[i];
                if (oVar.DisplayLevel == oDisplayLevel)
                    oList.Add(oVar);
            }
            return oList;
        }
        /// <summary>
        /// returns a collection of all variables configured to display in the Wizard
        /// </summary>
        /// <param name="oLevel"></param>
        /// <returns></returns>
        public System.Collections.Generic.List<XmlVariable> GetWizardVariables(bool bIncludeChildren)
        {
            System.Collections.Generic.List<XmlVariable> oList = new System.Collections.Generic.List<XmlVariable>();

            for (int i = 0; i < this.Variables.Count; i++)
            {
                XmlVariable oVar = this.Variables[i];
                if ((oVar.DisplayIn & XmlVariable.ControlHosts.Wizard) == XmlVariable.ControlHosts.Wizard)
                    oList.Add(oVar);
            }
            if (bIncludeChildren)
            {
                // Also include variables from child segments
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].DisplayWizard)
                    {
                        System.Collections.Generic.List<XmlVariable> oChildVars = this.Segments[i].GetWizardVariables(bIncludeChildren);
                        for (int c = 0; c < oChildVars.Count; c++)
                        {
                            oList.Add(oChildVars[c]);
                        }
                    }
                }
            }

            return oList;
        }
        /// <summary>
        /// returns the blocks of this segment
        /// </summary>
        public XmlBlocks Blocks
        {
            get
            {
                if (m_oBlocks == null)
                    m_oBlocks = new XmlBlocks(this);
                return m_oBlocks;
            }
            private set { this.m_oBlocks = value; }
        }
        /// <summary>
        /// returns the actions of this segment
        /// </summary>
        public XmlSegmentActions Actions
        {
            get
            {
                if (m_oSegmentActions == null)
                    m_oSegmentActions = new XmlSegmentActions(this);

                return m_oSegmentActions;
            }
        }
        /// <summary>
        /// returns the parent segment of the this segment
        /// </summary>
        public XmlSegment Parent
        {
            get
            {
                if (m_oParentSegment == null && this.ForteDocument != null)
                {
                    //TODO: get parent segment from document
                    string xTagID = this.FullTagID;
                    int iPos1 = xTagID.LastIndexOf(".");

                    string xParentTagID = "";

                    if (iPos1 > -1)
                    {
                        //parent tag precedes the '.'
                        xParentTagID = xTagID.Substring(0, iPos1);

                        //find parent in segments collection
                        m_oParentSegment = XmlSegment.FindSegment(xParentTagID, this.ForteDocument.Segments);
                    }
                    else
                        m_oParentSegment = null;
                }

                return m_oParentSegment;
            }
            protected set { m_oParentSegment = value; }
        }
        //public string ParentVariableID
        //{
        //get 
        //{
        //    return this.Nodes.GetItemAssociatedParentVariable(this.FullTagID);
        //}
        //set 
        //{
        //    this.Nodes.SetItemAssociatedParentVariable(this.FullTagID, value);
        //}
        //}
        /// <summary>
        /// Parses path to child segment in form x.y.z and returns corresponding Segment object
        /// </summary>
        /// <param name="xPathToSegment"></param>
        /// <returns></returns>
        public XmlSegment GetChildSegmentFromPath(string xPathToSegment)
        {
            XmlSegment oTargetSegment = null;

            // Parse full path into separate elements
            string[] xSegments = xPathToSegment.Split('.');
            XmlSegment oParentSegment = this;
            for (int i = 0; i <= xSegments.GetUpperBound(0); i++)
            {
                try
                {
                    oTargetSegment = null;
                    for (int j = 0; j < oParentSegment.Segments.Count; j++)
                    {
                        // Look for child segment with matching DisplayName
                        if (String.RemoveIllegalNameChars(oParentSegment.Segments.ItemFromIndex(j).DisplayName) == xSegments[i])
                        {
                            oTargetSegment = oParentSegment.Segments.ItemFromIndex(j);
                            break;
                        }
                    }
                    if (oTargetSegment == null)
                    {
                        return null;
                    }
                    else
                    {
                        oParentSegment = oTargetSegment;
                    }
                }
                catch
                {
                    // No matching Segment found
                    return null;
                }
            }
            return oTargetSegment;
        }
        /// <summary>
        /// Parses path to child segment in form x.y.z and returns corresponding Segment object
        /// </summary>
        /// <param name="xPathToSegment"></param>
        /// <returns></returns>
        public XmlSegment GetChildSegmentFromFullTagID(string xFullTagID)
        {
            XmlSegment oTargetSegment = null;

            // Parse full path into separate elements
            string[] xSegments = xFullTagID.Split('.');
            XmlSegment oParentSegment = this;
            for (int i = 0; i <= xSegments.GetUpperBound(0); i++)
            {
                try
                {
                    if (xSegments[i] == oParentSegment.TagID)
                        continue;

                    oTargetSegment = null;
                    for (int j = 0; j < oParentSegment.Segments.Count; j++)
                    {
                        // Look for child segment with matching DisplayName
                        if (String.RemoveIllegalNameChars(oParentSegment.Segments.ItemFromIndex(j).TagID) == xSegments[i])
                        {
                            oTargetSegment = oParentSegment.Segments.ItemFromIndex(j);
                            break;
                        }
                    }
                    if (oTargetSegment == null)
                    {
                        return null;
                    }
                    else
                    {
                        oParentSegment = oTargetSegment;
                    }
                }
                catch
                {
                    // No matching Segment found
                    return null;
                }
            }
            return oTargetSegment;
        }
        /// <summary>
        /// returns the child segments of this segment
        /// </summary>
        public XmlSegments Segments
        {
            get
            {
                //get child segments if not already retrieved
                if (m_oChildSegments == null)
                    GetChildSegments();

                return m_oChildSegments;
            }
            private set { m_oChildSegments = value; }
        }
        /// <summary>
        /// returns the MacPac document containing this segment
        /// </summary>
        public XmlForteDocument ForteDocument
        {
            get { return this.m_oForteDocument; }
            set { m_oForteDocument = value; }
        }
        /// <summary>
        /// returns the Authors collection for this segment
        /// </summary>
        public XmlAuthors Authors
        {
            get { return m_oAuthors; }
            set
            {
                if ((m_oAuthors == null && value != null) || (m_oAuthors.ToString() != value.ToString()))
                {
                    //authors have changed -
                    //set previous authors
                    if (m_oAuthors != null)
                        m_oPrevAuthors.SetAuthors(m_oAuthors);

                    //set new authors
                    m_oAuthors = value;
                }
            }
        }
        /// <summary>
        /// Stores XML of previous Authors collection
        /// </summary>
        public XmlAuthors PreviousAuthors
        {
            get { return m_oPrevAuthors; }
            set { m_oPrevAuthors = value; }
        }
        /// <summary>
        /// returns true if all child segments are valid
        /// and all variables are valid
        /// </summary>
        public bool IsValid
        {
            get
            {
                bool bRet = true;

                DateTime t0 = DateTime.Now;

                //segment can't be valid if the default values
                //have yet to be set
                if (!(this.CreationStatus == Status.Finished ||
                    this.CreationStatus == Status.VariableDefaultValuesSet))
                    return false;

                if (!this.Segments.AreValid)
                    //at least one segment is invalid
                    bRet = false;
                else if (!this.Variables.ValuesAreValid)
                    //at least one variable is invalid
                    bRet = false;

                LMP.Benchmarks.Print(t0, this.Name);

                return bRet;
            }
        }
        public bool ContentIsMissing
        {
            get
            {
                if (this.Segments.ContentIsMissing)
                    return true;
                //TODO: OpenXML rewrite
                //else if (LMP.Forte.MSWord.WordDoc.BookmarkIsMissing((object)this.Bookmarks))
                //    return true;
                else
                    return false;
            }
        }
        /// <summary>
        /// returns true if 'Must Visit' variables of this segment
        /// and all child segments have been visited
        /// </summary>
        public bool AllRequiredValuesVisited
        {
            get
            {
                DateTime t0 = DateTime.Now;

                //segment can't be valid if the default values
                //have yet to be set
                if (!(this.CreationStatus == Status.Finished ||
                    this.CreationStatus == Status.VariableDefaultValuesSet))
                    return false;

                if (!this.Segments.AllRequiredValuesVisited)
                    return false;

                if (!this.Variables.AllRequiredValuesVisited)
                    return false;

                LMP.Benchmarks.Print(t0, this.Name);
                return true;
            }
        }
        /// <summary>
        /// returns true iff this segment is an external child segment
        /// </summary>
        public bool IsExternalChild
        {
            get
            {
                if (this.Parent == null)
                    return false;
                else
                {
                    //GLOG 8519
                    string xObjectData = Query.GetAttributeValue(this.WPDoc, this.RawSegmentParts[0].ContentControl, "ObjectData");

                    //GLOG 4441: Make sure ObjectData is decrypted if necessary
                    xObjectData = String.Decrypt(xObjectData);

                    //true only if ObjectData includes ParentTagID, but value is not empty
                    return xObjectData.Contains("ParentTagID=") &&
                        !xObjectData.Contains("ParentTagID=|") &&
                        !xObjectData.EndsWith("ParentTagID=");
                }
            }
        }
        /// <summary>
        /// returns true iff the segment is a top level segment
        /// </summary>
        public bool IsTopLevel
        {
            get { return this.Parent == null; }
        }
        /// <summary>
        /// gets the CI Contacts used by this segment
        /// </summary>
        /// <summary>
        /// if segment is top-level, returns true if there are no other top-level
        /// segments of the same object type - otherwise, returns true if there are
        /// no siblings of the same object type
        /// </summary>
        public bool IsOnlyInstanceOfType
        {
            get
            {
                //get target segments collection
                XmlSegments oSegments = null;
                if (this.IsTopLevel)
                {
                    if (this.ForteDocument.WPDocument != null)
                    {
                        WordprocessingDocument oDoc = this.ForteDocument.WPDocument;
                        List<RawSegmentPart> oParts = Query.GetTopLevelRawSegmentParts(oDoc, this.ID);
                        if (oParts.Count == 1)
                            return true;

                        foreach (RawSegmentPart oPart in oParts)
                        {
                            //false if there is another top-level segment of the same type
                            if (oPart.GetSegmentPropertyValue("ObjectTypeID") == ((int)this.TypeID).ToString() && oPart.TagID != this.TagID)
                                return false;
                        }
                        return true;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                    oSegments = this.Parent.Segments;

                //look for segment of same type
                for (int i = 0; i < oSegments.Count; i++)
                {
                    XmlSegment oSegment = oSegments[i];
                    if ((oSegment.TypeID == this.TypeID) &&
                        (oSegment.FullTagID != this.FullTagID))
                        return false;
                }

                //no segments of same type found
                return true;
            }
        }
        public string ChildSegmentIDs(bool bIncludeChildren)
        {
            if (this.IntendedUse == mpSegmentIntendedUses.AsStyleSheet ||
                this.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                return "";

            System.Collections.Generic.List<string> aList =
                this.ChildSegmentList(bIncludeChildren);
            string xTemp = "";
            //Return as delimited string
            foreach (string xItem in aList)
            {
                if (xItem != this.ID)
                    xTemp = xTemp + xItem + "|";
            }
            //clean up trailing delimiter
            xTemp = xTemp.TrimEnd('|');
            return xTemp;
        }
        public XmlControlActions CourtChooserControlActions
        {
            get { return m_oCourtChooserControlActions; }
        }
        public XmlControlActions LanguageControlActions
        {
            get { return m_oLanguageControlActions; }
        }
        public int Culture
        {
            get { return m_iCulture; }
            set
            {
                if (this.SupportedLanguages.Contains(value + "�") ||
                    this.SupportedLanguages.Contains("�" + value) ||
                    this.SupportedLanguages == value.ToString())
                {
                    m_iCulture = value;
                }
                else if (this is XmlCollectionTable)
                {
                    //GLOG item #4219 - dcf -
                    //segment is a collection, and collections
                    //don't have supported languages - so just
                    //set the language to allow collection items
                    //to be set to the language of thier parent,
                    //i.e. the collection
                    m_iCulture = value;
                }
            }
        }
        /// <summary>
        /// gets/sets whether this is the primary item among siblings of this type
        /// for purposes of maintaining author linkage and position on the tree
        /// </summary>
        public bool IsPrimary
        {
            get { return m_bIsPrimary; }
            set
            {
                DateTime t0 = DateTime.Now;

                m_bIsPrimary = value;

                //if (m_bIsPrimary)
                //{

                //    //ensure that all siblings are non-primary
                //    XmlSegments oSiblings = null;
                //    if (!this.IsTopLevel)
                //        oSiblings = this.Parent.Segments;
                //    else if (this.ForteDocument != null)
                //        oSiblings = this.ForteDocument.Segments;

                //    if (oSiblings != null)
                //    {
                //        for (int i = 0; i < oSiblings.Count; i++)
                //        {
                //            XmlSegment oSibling = oSiblings[i];
                //            if ((oSibling.TypeID == this.TypeID) &&
                //                (oSibling.FullTagID != this.FullTagID) && oSibling.IsPrimary)
                //            {
                //                oSibling.IsPrimary = false;
                //                if (this.ForteDocument.Mode != XmlForteDocument.Modes.Design)
                //                {
                //                    //TODO: OpenXML rewrite
                //                    //oSibling.Nodes.SetItemObjectDataValue(oSibling.FullTagID,
                //                    //    "IsPrimary", "false");
                //                }
                //                if (oSibling.LinkAuthorsToParentDef ==
                //                    LinkAuthorsToParentOptions.FirstChildOfType)
                //                    oSibling.LinkAuthorsToParent = false;
                //            }
                //        }
                //    }
                //}

                //write runtime value to object data
                if (this.ForteDocument != null && this.ForteDocument.Mode != XmlForteDocument.Modes.Design)
                {
                    this.SetPropertyValue("IsPrimary", this.IsPrimary.ToString().ToLower(), false);
                }

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// Return true if Segment has any nodes in the Document Editor
        /// </summary>
        public bool HasVisibleUIElements
        {
            //GLOG 15907
            get 
            { 
                if (this.IsTopLevel && (this.MaxAuthors > 0 || this.SupportedLanguages.Contains(StringArray.mpEndOfValue) || this.ShowChooser == true || this.ShowCourtChooser == true))
                {
                    //return true if Author, Language, Court or Segment Chooser controls should be displayed
                    return true;
                }
                else
                {
                    //return true if there are any visible variables or blocks
                    for (int i = 0; i < this.Variables.Count; i++)
                    {
                        if ((this.Variables[i].DisplayIn & Variable.ControlHosts.DocumentEditor) == Variable.ControlHosts.DocumentEditor)
                            return true;
                    }
                    for (int i = 0; i < this.Blocks.Count; i++)
                    {
                        if (this.Blocks[i].ShowInTree == true)
                            return true;
                    }
                    for (int i = 0; i < this.Segments.Count; i++)
                    {
                        if (!this.Segments[i].IsTransparent)
                            return true;
                    }
                }
                return false;
            }
        }
        #region Properties direct to/from OpenXml
        //public string AuthorControlProperties
        //{
        //    get { return GetPropertyValue("AuthorControlProperties"); }
        //    set { SetPropertyValue("AuthorControlProperties", value, false); }
        //}
        //public string DisplayName
        //{
        //    get { return GetPropertyValue("DisplayName"); }
        //    set { SetPropertyValue("DisplayName", value, false); }
        //}
        //public string Name
        //{
        //    get { return GetPropertyValue("Name"); }
        //    set { SetPropertyValue("Name", value, false); }
        //}
        //public mpObjectTypes TypeID
        //{
        //    get
        //    {
        //        string xObjectTypeID = GetPropertyValue("ObjectTypeID");
        //        return (mpObjectTypes)int.Parse(xObjectTypeID);
        //    }
        //    set { SetPropertyValue("ObjectTypeID", ((int)value).ToString(), false); }
        //}
        //public int MaxAuthors
        //{
        //    get
        //    {
        //        string xMaxAuthors = GetPropertyValue("MaxAuthors");

        //        if (!string.IsNullOrEmpty(xMaxAuthors))
        //            return int.Parse(xMaxAuthors);
        //        else
        //            return 0;
        //    }
        //    set { SetPropertyValue("MaxAuthors", value.ToString(), false); }
        //}
        //public bool ShowChooser
        //{
        //    get
        //    {
        //        string xShowChooser = GetPropertyValue("ShowChooser");

        //        if (!string.IsNullOrEmpty(xShowChooser))
        //            return bool.Parse(xShowChooser);
        //        else
        //            return false;
        //    }
        //    set { SetPropertyValue("ShowChooser", value.ToString(), false); }
        //}
        //public string AuthorsNodeUILabel
        //{
        //    get { return GetPropertyValue("AuthorsNodeUILabel"); }
        //    set { SetPropertyValue("AuthorsNodeUILabel", value, false); }
        //}
        //public string AuthorsHelpText
        //{
        //    get { return GetPropertyValue("AuthorsHelpText"); }
        //    set { SetPropertyValue("AuthorsHelpText", value, false); }
        //}
        //public string ProtectedFormPassword
        //{
        //    get { return GetPropertyValue("ProtectedFormPassword"); }
        //    set { SetPropertyValue("ProtectedFormPassword", value, false); }
        //}
        //public string TranslationID
        //{
        //    get { return GetPropertyValue("TranslationID"); }
        //    set { SetPropertyValue("TranslationID", value, false); }
        //}
        //public string DefaultWrapperID
        //{
        //    get { return GetPropertyValue("DefaultWrapperID"); }
        //    set { SetPropertyValue("DefaultWrapperID", value, false); }
        //}
        //public bool IsTransparentDef
        //{
        //    get
        //    {
        //        string xIsTransparentDef = GetPropertyValue("IsTransparentDef");

        //        if (!string.IsNullOrEmpty(xIsTransparentDef))
        //            return bool.Parse(xIsTransparentDef);
        //        else
        //            return false;
        //    }
        //    set { SetPropertyValue("IsTransparentDef", value.ToString(), false); }
        //}
        //public bool DisplayWizard
        //{
        //    get
        //    {
        //        string xDisplayWizard = GetPropertyValue("DisplayWizard");

        //        if (!string.IsNullOrEmpty(xDisplayWizard))
        //            return bool.Parse(xDisplayWizard);
        //        else
        //            return false;
        //    }
        //    set { SetPropertyValue("DisplayWizard", value.ToString(), false); }
        //}
        //public string WordTemplate
        //{
        //    get { return GetPropertyValue("WordTemplate"); }
        //    set { SetPropertyValue("WordTemplate", value, false); }
        //}
        //public string RequiredStyles
        //{
        //    get { return GetPropertyValue("RequiredStyles"); }
        //    set { SetPropertyValue("RequiredStyles", value, false); }
        //}
        //public string RecreateRedirectID
        //{
        //    get { return GetPropertyValue("RecreateRedirectID"); }
        //    set { SetPropertyValue("RecreateRedirectID", value, false); }
        //}
        //public bool IsTransparent
        //{
        //    get
        //    {
        //        //transparent if defined as such, not top-level, and the only sibling of its type
        //        if (!this.IsTopLevel)
        //        {
        //            if ((this.Parent.IsTransparentDef) &&
        //                (this.Parent.IsTransparent == false))
        //                //if parent that is normally transparent is showing,
        //                //so should child that is normally transparent -
        //                //this comes into play when a pleading caption is
        //                //copied from a pleading into a blank document
        //                return false;
        //            else if (this.IsOnlyInstanceOfType)
        //                return this.IsTransparentDef;
        //            else
        //            {
        //                //ensure that no siblings of the same type are transparent
        //                XmlSegments oSiblings = this.Parent.Segments;
        //                for (int i = 0; i < oSiblings.Count; i++)
        //                {
        //                    XmlSegment oSibling = oSiblings[i];
        //                    if ((oSibling.TypeID == this.TypeID) &&
        //                        (oSibling.FullTagID != this.FullTagID))
        //                        return false;
        //                }

        //                return true;
        //            }
        //        }
        //        else
        //            return false;
        //    }
        //    set { SetPropertyValue("IsTransparent", value.ToString(), false); }
        //}
        //public bool IsPrimary
        //{
        //    get
        //    {
        //        string xIsPrimary = GetPropertyValue("IsPrimary");

        //        if(!string.IsNullOrEmpty("IsPrimary"))
        //            return bool.Parse(xIsPrimary);
        //        else if (this.IsOnlyInstanceOfType)
        //            return true;
        //        else
        //            return false;
        //    }
        //    set { SetPropertyValue("IsPrimary", value.ToString(), false); }
        //}
        //public LinkAuthorsToParentOptions LinkAuthorsToParentDef
        //{
        //    get
        //    {
        //        string xLinkAuthors = GetPropertyValue("LinkAuthorsToParentDef");
        //        if (!string.IsNullOrEmpty(xLinkAuthors))
        //        {
        //            return ((LinkAuthorsToParentOptions)int.Parse(xLinkAuthors));
        //        }
        //        else
        //            return LinkAuthorsToParentOptions.Never;
        //    }
        //    set { SetPropertyValue("LinkAuthorsToParentDef", ((int)value).ToString(), false); }
        //}        #endregion
        #endregion
        #endregion
        #region *********************methods*********************
        public void ApplyPrefill(XmlPrefill oPrefill)
        {
            //GLOG : 6987 : CEH
            //reset status to pre-Finished & force initialization of Prefill authors
            //GLOG 7826
            XmlSegment.Status iStatus = this.CreationStatus;
            this.CreationStatus = Status.BoilerplateInserted;
            this.InitializeValues(oPrefill, false, true);
            //GLOG 7826: Restore original creation status if greater than current value
            if (iStatus > this.CreationStatus)
                this.CreationStatus = iStatus;
        }

        /// <summary>
        /// returns the value to be displayed 
        /// in the tree for the specified variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string GetVariableUIValue(XmlVariable oVar, string xDocumentValue)
        {
            DateTime t0 = DateTime.Now;

            string m_xEmptyDisplayValue = LMP.Resources.GetLangString("Msg_EmptyValueDisplayValue");
            string xValue = null;

            //check if variable.DisplayValue contains a field code, date format
            //or boolean value, then process accordingly

            if (System.String.IsNullOrEmpty(oVar.DisplayValue))
            {
                xValue = xDocumentValue;
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                xValue = XmlExpression.MarkLiteralReservedCharacters(xValue);
                xValue = XmlExpression.Evaluate(xValue, oVar.Segment, oVar.ForteDocument);

                if (oVar.ControlType == mpControlTypes.Checkbox)
                {
                    if (!oVar.IsMultiValue)
                    {
                        //Display appropriate True/False text based on control properties-
                        BooleanComboBox oBC = (BooleanComboBox)oVar.AssociatedControl;
                        if (oBC == null)
                        {
                            DateTime t1 = DateTime.Now;
                            oBC = (BooleanComboBox)oVar.CreateAssociatedControl(null);
                            LMP.Benchmarks.Print(t1, "Create Combobox");
                        }
                        if (xValue.ToUpper() == "TRUE")
                            xValue = oBC.TrueString;
                        else if (xValue.ToUpper() == "FALSE")
                            xValue = oBC.FalseString;
                    }
                    else
                    {
                        //AssociatedControl is a MultiValueControl configured in BooleanComboBox mode
                        //Display appropriate True/False text based on control properties-
                        MultiValueControl oMVC = (MultiValueControl)oVar.AssociatedControl;
                        if (oMVC == null)
                        {
                            oMVC = (MultiValueControl)oVar.CreateAssociatedControl(null);
                        }
                        xValue = xValue.Replace("true", oMVC.TrueString);
                        xValue = xValue.Replace("false", oMVC.FalseString);
                    }
                }
                else if (oVar.ControlType == mpControlTypes.PaperTraySelector)
                {
                    //GLOG 4752: Access Printer bin information via Base wrapper functions
                    if (!string.IsNullOrEmpty(oVar.Value))
                        xValue = LMP.OS.GetPrinterBinName(short.Parse(oVar.Value));
                }

                if (oVar.IsMultiValue)
                {
                    //Replace delimiter between multiple values;
                    xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
                }
            }
            else
            {
                //evaluate the display value expression using variable value
                try
                {
                    xValue = EvaluateDisplayValue(oVar, xDocumentValue);

                    //TODO: OpenXML rewrite
                    //if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    //{
                    //    //GLOG 3318: reflect use of field code in display value
                    //    if (oVar.ValueSourceExpression.ToUpper().StartsWith("[DATEFORMAT") &&
                    //        oVar.AssociatedWordTags[0].Range.Fields.Count > 0)
                    //        xValue = xValue + " (field code)";
                    //}
                    //else
                    //{
                    //    //GLOG 3318: reflect use of field code in display value
                    //    if (oVar.ValueSourceExpression.ToUpper().StartsWith("[DATEFORMAT") &&
                    //        oVar.AssociatedContentControls[0].Range.Fields.Count > 0)
                    //        xValue = xValue + " (field code)";
                    //}
                }
                catch { }
            }

            if (xValue == null || xValue == "")
                xValue = m_xEmptyDisplayValue;
            else
            {
                //replace non-printing chars with ellipses
                xValue = xValue.Replace("\v", "...");
                xValue = xValue.Replace("\t", "...");
                xValue = xValue.Replace("\r\n", "...");
                xValue = xValue.Replace("\r", "...");
                xValue = xValue.Replace("\n", "...");
                xValue = (oVar.Value == "") ? m_xEmptyDisplayValue : xValue;
            }

            ////get available value node text width
            ////TODO: we may want to enhance this method
            ////to determine how much text to enter into
            ////the node, given how much width is available
            //if (xValue.Length > 35)
            //    //trim string and append ellipse if necessary
            //    xValue = xValue.Substring(0, 35) +
            //        (xValue.EndsWith("...") ? "" : "...");

            LMP.Benchmarks.Print(t0, oVar.DisplayName);

            return xValue;
        }
        /// <summary>
        /// evaluates DisplayValue field code using variable value
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string EvaluateDisplayValue(XmlVariable oVar, string xDocumentValue)
        {
            if (oVar.DisplayValue == null || oVar.DisplayValue == "")
                return "";

            string xVarValue = xDocumentValue;
            //GLOG 2151: Mark reserved expression characters in Value so they'll
            //be treated as the literal characters in Expression.Evaluate
            xVarValue = XmlExpression.MarkLiteralReservedCharacters(xVarValue);
            //pre-evaluate the DisplayValue field code(s), substituting MyValue for oVar.Value
            //string xFieldCode = oVar.DisplayValue.Replace("[MyValue]", xVarValue);
            string xFieldCode = XmlFieldCode.EvaluateMyValue(oVar.DisplayValue, xVarValue);

            if (xFieldCode.Contains("[MyTagValue]"))
            {
                try
                {
                    //TODO: OpenXML rewrite
                    //if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    //{
                    //    Word.XMLNode oWordNode = oVar.AssociatedWordTags[0];
                    //    string xTagValue = oWordNode.Text;
                    //    //GLOG 3318: Get text using original input case if necessary
                    //    if (!string.IsNullOrEmpty(xTagValue) && xTagValue.ToUpper() == xTagValue)
                    //        xTagValue = LMP.Forte.MSWord.WordDoc.GetInputValueIfAllCapped(oWordNode);
                    //    xTagValue = XmlExpression.MarkLiteralReservedCharacters(xTagValue);

                    //    //GLOG 4476 (dm) - if field code is displayed, get result
                    //    if ((!string.IsNullOrEmpty(xTagValue)) &&
                    //        xTagValue.Contains(((char)21).ToString()) &&
                    //        (oWordNode.Range.Fields.Count > 0))
                    //        xTagValue = oWordNode.Range.Fields[1].Result.Text;

                    //    xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                    //}
                    //else
                    //{
                    //    Word.ContentControl oCC = oVar.AssociatedContentControls[0];
                    //    string xTagValue = oCC.Range.Text;

                    //    //GLOG 3318: Get text using original input case if necessary
                    //    if (!string.IsNullOrEmpty(xTagValue) && xTagValue.ToUpper() == xTagValue)
                    //        xTagValue = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCC.Range);
                    //    xTagValue = XmlExpression.MarkLiteralReservedCharacters(xTagValue);

                    //    //GLOG 4476 (dm) - if field code is displayed, get result
                    //    if ((!string.IsNullOrEmpty(xTagValue)) &&
                    //        xTagValue.Contains(((char)21).ToString()) &&
                    //        (oCC.Range.Fields.Count > 0))
                    //        xTagValue = oCC.Range.Fields[1].Result.Text;

                    //    xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                    //}
                }
                catch
                {
                    xFieldCode = xFieldCode.Replace("[MyTagValue]", "");
                }
            }
            //xFieldCode = xFieldCode.Replace("MyValue", xVarValue);
            xFieldCode = XmlFieldCode.EvaluateMyValue(xFieldCode, xVarValue);

            //handle listlookup field code
            if (oVar.DisplayValue.StartsWith("[ListLookup"))
                return XmlExpression.Evaluate(xFieldCode, oVar.Segment, oVar.ForteDocument);

            //check for ^AND operator in compound DisplayValue strings 
            //or "][" indicating multiple field codes to be evaluated -
            //reline control value, which contains nodes for Standard and Special
            //is an example where DisplayValue may contain two field codes

            //***REPLACING THESE CHARACTERS DOESN'T SEEM TO BE NECESSARY, AND DOES NOT WORK CORRECTLY WITH
            //***MORE COMPLEX EXPRESSIONS - LET EXPRESSION.EVALUATE DEAL WITH THIS IN THE NORMAL FASHION
            //xFieldCode = xFieldCode.Replace("^AND", LMP.StringArray.mpEndOfElement.ToString() + "[");
            //xFieldCode = xFieldCode.Replace("][", "]" + LMP.StringArray.mpEndOfElement.ToString() + "[");

            //create array of field codes for further pre-evaluation
            string[] xFieldCodes = xFieldCode.Split(LMP.StringArray.mpEndOfElement);
            StringBuilder oSB = new StringBuilder();

            //build an array list of values returned by Expression.Evaluate
            ArrayList oArrayList = new ArrayList();
            string xTemp = null;
            string[] aTemp = null;

            //for each configured field code, evaluate and return its string value
            //in the case of CI field codes, the string will be delimited by "; "
            //one member for each recipient
            for (int i = 0; i <= xFieldCodes.GetUpperBound(0); i++)
            {
                xTemp = xFieldCodes[i];
                xTemp = xTemp.Replace("; ", ";mp10Tempmp10 ");
                if (xTemp == "[TagValue]" || xTemp == "[MyTagValue]")
                {
                    //TODO: OpenXML rewrite
                    //if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    //{

                    //    xTemp = oVar.AssociatedWordTags[0].Text;
                    //}
                    //else
                    //{
                    //    xTemp = oVar.AssociatedContentControls[0].Range.Text;
                    //}
                }
                else
                {
                    xTemp = XmlExpression.Evaluate(xTemp, oVar.Segment, oVar.ForteDocument);
                }

                if (xTemp == null)
                    xTemp = "";

                //split value and add to arraylist
                xTemp = xTemp.Replace(";mp10Tempmp10 ", ";");
                xTemp = xTemp.Replace("mp10Tempmp10 ", "");
                xTemp = xTemp.Replace("; ", "|");
                aTemp = xTemp.Split('|');
                oArrayList.Add(aTemp);
            }

            //go through the array list of field codes and match field code return values
            //In most cases aTemp will have only a single member.  For CI type field codes,
            //aTemp will have as many members as returned contacts.
            for (int i = 0; i < oArrayList.Count; i++)
            {
                string[] aItems = (string[])oArrayList[i];

                for (int j = 0; j < aItems.Length; j++)
                {
                    //delimit each record w/ semicolon, add
                    //commas between record fields, i.e, name, address; name2, address;
                    string xFormat = (j == aItems.Length - 1) ? "{0}; " : "{0}, ";

                    oSB.AppendFormat(xFormat, aItems[j]);
                }
            }

            string xValue = oSB.ToString();
            if (oVar.IsMultiValue)
            {
                //Replace value separator for display
                xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
            }
            //eliminate double delimiters caused by empty values
            xValue = xValue.Replace(";;", ";");
            xValue = xValue.Replace("; ;", ";");
            xValue = xValue.Replace(",,", ",");
            xValue = xValue.Replace(", ,", ",");

            xValue = xValue.TrimEnd();
            xValue = xValue.TrimEnd(';');
            xValue = xValue.TrimStart(';');
            xValue = xValue.TrimStart(',');
            xValue = xValue.TrimStart();

            return xValue;
        }
        ///// <summary>
        ///// prompts the user to change any data that might
        ///// have been corrupted during document editing - stores
        ///// edited data to appropriate deactivation variable -
        ///// returns true iff user did not indicate any bad tag values
        ///// </summary>
        ///// <returns></returns>
        //public bool ReviewData()
        //{
        //    return ReviewData(null);
        //}

        //public bool ReviewData(string xVarName)
        //{
        //    if(string.IsNullOrEmpty(this.GetDeactivationData()))
        //        return true;

        //    string xMismatchedData = null;

        //    if (!string.IsNullOrEmpty(xVarName))
        //    {
        //        //return variable detail if variable value is mismatched -
        //        //i.e. if the value is different from the last
        //        //deactivation snapshot
        //        xMismatchedData = this.GetMismatchedDeactivatedVariable(xVarName);
        //    }
        //    else
        //    {
        //        //get mismatched variables -
        //        //these are those variables could possibly
        //        //have been corrupted since the last
        //        //deactivation snapshot
        //        xMismatchedData = this.GetMismatchedDeactivatedVariables();
        //    }

        //    //exit if there is no possibly corrupted data
        //    if (string.IsNullOrEmpty(xMismatchedData))
        //        return true;

        //    //prompt user - display only the mismatched data
        //    DataReviewForm oForm = new DataReviewForm(xMismatchedData, this);

        //    oForm.ShowDialog();

        //    //user has not canceled operation -
        //    //get the reviewed data
        //    string xReviewedData = oForm.ReviewedData;
        //    string[] aReviewedData = xReviewedData.Split(StringArray.mpEndOfRecord);

        //    //cycle through each reviewed variable -
        //    //writing the value to the deactivation doc var
        //    foreach (string xReviewedVar in aReviewedData)
        //    {
        //        //get detail of each reviewed variable
        //        string[] aReviewedVar = xReviewedVar.Split(StringArray.mpEndOfElement);

        //        //get the deactivation document variable that
        //        //contains the data for this reviewed variable
        //        string xSegmentID = aReviewedVar[1];
        //        int iPos = xSegmentID.LastIndexOf('.');
        //        if (iPos > -1)
        //            xSegmentID = xSegmentID.Substring(iPos + 1);
        //        object oName = "Deactivation_" + xSegmentID;
        //        Word.Variable oDocVar = this.ForteDocument.WordDocument.Variables.get_Item(ref oName);

        //        //get the value of the document variable
        //        string xValue = StringArray.mpEndOfRecord +
        //            LMP.String.Decrypt(oDocVar.Value) + StringArray.mpEndOfRecord;

        //        //search for the deactivated name/value pair
        //        iPos = xValue.IndexOf(StringArray.mpEndOfRecord +
        //            aReviewedVar[2] + StringArray.mpEndOfElement);

        //        if (iPos > -1)
        //        {
        //            //pair was found - find the end of the old name/value pair
        //            int iPos2 = xValue.IndexOf(StringArray.mpEndOfRecord, iPos + 1);

        //            //reconstruct the new name/value pair
        //            string xNewValue = StringArray.mpEndOfRecord + aReviewedVar[2] +
        //                StringArray.mpEndOfElement + aReviewedVar[3];

        //            //replace the existing value with the reviewed value
        //            string xNewVarValue = xValue.Substring(0, iPos) +
        //                xNewValue + xValue.Substring(iPos2);

        //            xNewVarValue = xNewVarValue.Trim(StringArray.mpEndOfRecord);

        //            //set document variable to the new value
        //            oDocVar.Value = LMP.String.Encrypt(xNewVarValue,
        //                LMP.Data.Application.EncryptionPassword);

        //            //force variable value to be updated on next request (5/19/11 dm)
        //            Segment oSegment;
        //            if (aReviewedVar[1] != this.FullTagID)
        //                //variable belongs to a child of this segment
        //                oSegment = FindSegment(aReviewedVar[1], this.Segments);
        //            else
        //                oSegment = this;
        //            Variable oVar = oSegment.Variables.ItemFromName(aReviewedVar[2]);
        //            oVar.ForceValueRefresh();
        //        }
        //    }

        //    //return true if reviewed data contains no fixed value markers
        //    return (xReviewedData.IndexOf(ForteConstants.mpFixedValueMarker) == -1);
        //}

        /// <summary>
        /// returns true iff the specified segment
        /// property has a non-empty value
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        protected bool PropertyValueExists(ListDictionary oLD, string xPropertyName)
        {
            return oLD[xPropertyName] != null && oLD[xPropertyName].ToString() != "";
        }
        /// <summary>
        /// returns true if and only if the segment or one of
        /// its direct children are of the specified type
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        public bool Contains(LMP.Data.mpObjectTypes iType)
        {
            if (this.TypeID == iType)
                return true;
            else
            {
                int iCount = this.Segments.Count;
                for (int i = 0; i < iCount; i++)
                {
                    if (this.Segments[i].TypeID == iType)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// looks for qualifier in xRefName;
        /// returns target object and unqualified xRefName
        /// </summary>
        /// <param name="xRefName"></param>
        /// <returns></returns>
        internal XmlSegment GetReferenceTarget(ref string xRefName)
        {
            DateTime t0 = DateTime.Now;
            string xFullReference = xRefName;

            try
            {
                Trace.WriteNameValuePairs("xRefName", xRefName);

                //check for parent or defined sub-object keyword
                int iPos = xRefName.IndexOf("::");
                if (iPos == -1)
                    //no qualifier
                    return this;

                //parse out qualifier
                string xQualifier = xRefName.Substring(0, iPos).ToUpper();
                xRefName = xRefName.Substring(iPos + 2);

                //get top-level parent
                XmlSegment oTopParent = this.Parent;
                if (oTopParent != null)
                {
                    while (oTopParent.Parent != null)
                        oTopParent = oTopParent.Parent;
                }
                int iStartIndex;
                string xTypeID;
                mpObjectTypes iObjectType;

                if (xQualifier == "PARENT")
                    return this.Parent;
                else if (xQualifier == "TOPPARENT")
                    return oTopParent;
                else if (xQualifier.IndexOf("CHILD") > -1)
                {
                    //search this segment's children for a segment of the specified type
                    if (xQualifier.IndexOf("CHILDREN") > -1)
                        iStartIndex = 8;
                    else
                        iStartIndex = 5;
                    xTypeID = xQualifier.Substring(iStartIndex);
                    iObjectType = (mpObjectTypes)System.Int16.Parse(xTypeID);
                    return FindSegment(iObjectType, this.Segments);
                }
                else if (xQualifier.IndexOf("SIBLING") > -1)
                {
                    //search all descendants of this segment's top parent for a segment
                    //of the specified type, excluding this segment and its ancestors
                    if (oTopParent != null)
                    {
                        if (xQualifier.IndexOf("SIBLINGS") > -1)
                            iStartIndex = 8;
                        else
                            iStartIndex = 7;
                        xTypeID = xQualifier.Substring(iStartIndex);
                        iObjectType = (mpObjectTypes)System.Int16.Parse(xTypeID);
                        XmlSegment[] oSegments = FindSegments(iObjectType, oTopParent.Segments);
                        foreach (XmlSegment oSegment in oSegments)
                        {
                            string xTagID = oSegment.FullTagID;
                            if ((this.FullTagID != xTagID) &&
                                ((this.FullTagID.Length < xTagID.Length + 1) ||
                                (this.FullTagID.Substring(0, xTagID.Length + 1) != xTagID + ".")))
                                return oSegment;
                        }
                        return null;
                    }
                    else if (this.ForteDocument.Segments.Count > 1)
                    {
                        //this is a top level segment - a sibling is also a top level segment
                        if (this.ForteDocument.Segments[0] == this)
                        {
                            //return the second segment in the collection -
                            //that's the first sibling
                            return this.ForteDocument.Segments[1];
                        }
                        else
                        {
                            //return the first segment in the collection -
                            //that's the first sibling
                            return this.ForteDocument.Segments[0];
                        }
                    }
                    else
                        return null;
                }
                else if (xQualifier.IndexOf("SEGMENT") > -1)
                {
                    //search entire document for a segment of the specified type
                    if (xQualifier.IndexOf("SEGMENTS") > -1)
                        iStartIndex = 8;
                    else
                        iStartIndex = 7;
                    xTypeID = xQualifier.Substring(iStartIndex);
                    iObjectType = (mpObjectTypes)System.Int16.Parse(xTypeID);
                    return FindSegment(iObjectType, this.ForteDocument);
                }
                else
                {
                    //unsupported keyword - raise error
                    throw new LMP.Exceptions.KeywordException(
                        LMP.Resources.GetLangString("Error_InvalidKeyword") + xQualifier);
                }
            }
            finally
            {
                LMP.Benchmarks.Print(t0, xFullReference);
            }
        }

        /// <summary>
        /// looks for qualifier in xRefName;
        /// returns target objects and unqualified xRefName
        /// </summary>
        /// <param name="xRefName"></param>
        /// <returns></returns>
        public XmlSegment[] GetReferenceTargets(ref string xRefName)
        {
            XmlSegment[] oSegments = null;

            Trace.WriteNameValuePairs("xRefName", xRefName);

            //check for parent or defined sub-object keyword
            int iPos = xRefName.IndexOf("::");
            if (iPos == -1)
                //no qualifier - return this segment
                return new XmlSegment[1] { this };

            //parse out qualifier
            string xQualifier = xRefName.Substring(0, iPos).ToUpper();

            if (xQualifier.IndexOf("CHILDREN") + xQualifier.IndexOf("SIBLINGS") +
                xQualifier.IndexOf("SEGMENTS") == -3)
            {
                //perhaps this is one of the singular keywords
                XmlSegment oTarget = GetReferenceTarget(ref xRefName);
                if (oTarget != null)
                    return new XmlSegment[1] { oTarget };
                else
                    return null;
            }

            //parse out type id
            string xTypeID = xQualifier.Substring(8);
            mpObjectTypes iObjectType = (mpObjectTypes)System.Int16.Parse(xTypeID);

            if (xQualifier.IndexOf("CHILDREN") > -1)
            {
                //search this segment's children for segments of the specified type
                oSegments = FindSegments(iObjectType, this.Segments);
            }
            else if (xQualifier.IndexOf("SIBLINGS") > -1)
            {
                //search all descendants of this segment's top parent for segments
                //of the specified type, excluding this segment's ancestors
                XmlSegment oTopParent = this.Parent;
                if (oTopParent != null)
                {
                    //get top-level parent
                    while (oTopParent.Parent != null)
                        oTopParent = oTopParent.Parent;
                    oSegments = FindSegments(iObjectType, oTopParent.Segments);

                    //add found segments to array list
                    ArrayList oSegArrayList = new ArrayList();
                    foreach (XmlSegment oSegment in oSegments)
                    {
                        //do not include direct ancestors
                        string xTagID = oSegment.FullTagID;
                        //GLOG 4237: Avoid using Substring comparison on ancestors or same Segment
                        if ((this.FullTagID.Length < xTagID.Length + 1) ||
                            (this.FullTagID.Substring(0, xTagID.Length + 1) != xTagID + "."))
                            oSegArrayList.Add(oSegment);
                    }

                    //copy segments to array
                    oSegments = new XmlSegment[oSegArrayList.Count];
                    for (int i = 0; i < oSegArrayList.Count; i++)
                        oSegments[i] = (XmlSegment)oSegArrayList[i];
                }
            }
            else if (xQualifier.IndexOf("SEGMENTS") > -1)
            {
                //search all segments for segments of the specified type
                oSegments = FindSegments(iObjectType, this.ForteDocument.Segments);
            }

            //parse out remainder of code
            xRefName = xRefName.Substring(iPos + 2);

            return oSegments;
        }
        /// <summary>
        /// returns the first variable that has 
        /// an InsertDetailInTable variable action
        /// </summary>
        public XmlVariable FirstInsertDetailVariable
        {
            get
            {
                for (int i = 0; i < this.Variables.Count; i++)
                {
                    XmlVariable oVar = this.Variables.ItemFromIndex(i);

                    if (oVar.HasDetailTableAction())
                        return oVar;
                }

                return null;
            }
        }
        /// <summary>
        /// returns the doc property referred to by xDocPropReference
        /// can be 
        /// </summary>
        /// <param name="oMPObject"></param>
        /// <param name="xVariableRef">either the name of the variable, or a fully qualified name, which
        /// is of the form A::B, where A is the parent or child segment containing the variable,
        /// and B is the name of the variable</param>
        /// <returns></returns>
        public XmlVariable GetVariable(string xVariableRef)
        {
            DateTime t0 = DateTime.Now;

            XmlVariable oVar = null; ;

            try
            {
                Trace.WriteNameValuePairs("xVariableRef", xVariableRef);

                if (xVariableRef != null && xVariableRef != "")
                {
                    //get target object
                    XmlSegment oTarget = this.GetReferenceTarget(ref xVariableRef);

                    //get target property
                    try
                    {
                        oVar = oTarget.Variables.ItemFromName(xVariableRef);
                    }
                    catch { }

                    return oVar;
                }
                else
                    return null;
            }
            finally
            {
                LMP.Benchmarks.Print(t0, oVar != null ? oVar.Name : "null");
            }
        }

        /// <summary>
        /// refreshes this segment
        /// </summary>
        public void Refresh()
        {
            this.Refresh(XmlSegment.RefreshTypes.All);
        }
        /// <summary>
        /// refreshes this segment's node store, variables, blocks, and,
        /// if specified, child segments
        /// </summary>
        /// <param name="oType"></param>
        public void Refresh(XmlSegment.RefreshTypes oType)
        {
            DateTime t0 = DateTime.Now;

            //refresh variables, blocks and, if specified, child segments
            RefreshMembers(oType);

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns a fully created Oxml Segment
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="oForteDocument"></param>
        /// <returns></returns>
        public static XmlSegment CreateOxmlSegment(string xID, XmlSegment oParent, XmlForteDocument oForteDocument, XmlPrefill oPrefill)
        {
            return CreateOxmlSegment(xID, oParent, oForteDocument, oPrefill, null, null, null, OxmlCreationMode.Default, 0, false);
        }
        public static XmlSegment CreateOxmlSegment(string xID, XmlSegment oParent, XmlForteDocument oForteDocument, XmlPrefill oPrefill, string xTemplate,
            CollectionTableStructure oStructure, OxmlCreationMode iMode)
        {
            return CreateOxmlSegment(xID, oParent, oForteDocument, oPrefill, null, null, null, iMode, 0, false);
        }
        /// <summary>
        /// Create XmlSegment object with WordprocessingDocument populated with all variable values
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="oParent"></param>
        /// <param name="oForteDocument"></param>
        /// <param name="oPrefill"></param>
        /// <param name="xBodyText"></param>
        /// <param name="xTemplate"></param>
        /// <param name="oStructure"></param>
        /// <param name="bDesign"></param>
        /// <param name="bFinalize">Remove mSEG Content Controls and encrypt all doc variables</param>
        /// <returns></returns>
        public static XmlSegment CreateOxmlSegment(string xID, XmlSegment oParent, XmlForteDocument oForteDocument, XmlPrefill oPrefill, string xBodyText,
            string xTemplate, CollectionTableStructure oStructure, OxmlCreationMode iMode, short shCompatibilityVersion, bool bSuppressDesignErrors)
        {
            DateTime t0 = DateTime.Now;
            //get the segment def for this segment
            ISegmentDef oDef = null;
            try
            {
                oDef = XmlSegment.GetDefFromID(xID);
            }
            catch { }
            if (oDef == null)
                return null;

            XmlSegment oSegment = null;
            //GLOG 8655
            mpObjectTypes iCreateType = oDef.TypeID;
            bool bCreatingFromSavedContent = (iCreateType == mpObjectTypes.SavedContent);
            if (bCreatingFromSavedContent)
            {
                //GLOG 6920:  If Saved Segment, get Segment Definition from XML
                //this is saved content - segment needs to be
                //switched over to segment of contained xml
                string xContainedID = oDef.ChildSegmentIDs;
                if (!xContainedID.Contains(".") || xContainedID.EndsWith(".0"))
                {
                    //this is an admin segment
                    int iID1;
                    int iID2;
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    LMP.Data.Application.SplitID(xContainedID, out iID1, out iID2);
                    AdminSegmentDef oContentDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);
                    iCreateType = oContentDef.TypeID;
                }
                else
                {
                    //this is a user segment
                    //GLOG 4728: Filter collection by User to include accessible objects
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, LMP.Data.Application.User.ID);
                    UserSegmentDef oContentDef = (UserSegmentDef)oDefs.ItemFromID(xContainedID);
                    iCreateType = oContentDef.TypeID;
                }
            }
            oSegment = CreateSegmentObject(iCreateType, oForteDocument, oParent != null);
            oSegment.Definition = oDef;
            //GLOG 15864
            oSegment.CreationMode = iMode;
            if (oParent == null)
            {
                oSegment.ForteDocument = oForteDocument;
                if (oForteDocument.WPDocument == null)
                    oForteDocument.WPDocument = oSegment.WPDoc;
            }
            else
            {
                string xAssemblyName = "";
                string xClassName = "";
                if (LMP.MacPac.MacPacImplementation.IsServer)
                {
                    //running the server - create Oxml server objects
                    xAssemblyName = LMP.Data.Application.AppDirectory + "\\fArchitectOxml.dll";
                    xClassName = "LMP.Architect.Oxml.Xml.XmlForteDocument";
                }
                else
                {
                    //create Oxml desktop Word object
                    xAssemblyName = LMP.Data.Application.AppDirectory + "\\fArchitectOxmlDesktop.dll";
                    xClassName = "LMP.Architect.Oxml.Word.XmlForteDocument";
                }
                oSegment.ForteDocument = (XmlForteDocument)Assembly.LoadFile(xAssemblyName).CreateInstance(
                    xClassName, true, 0, null, null, null, null);
                oSegment.ForteDocument.WPDocument = oSegment.WPDoc;

            }
            //GLOG 15850
            if (iMode == OxmlCreationMode.Design)
                oSegment.ForteDocument.Mode = XmlForteDocument.Modes.Design;
            else
                oSegment.ForteDocument.Mode = XmlForteDocument.Modes.Edit;
            try
            {
                if (bCreatingFromSavedContent)
                {
                    //GLOG 15756: remove any headers/footers that belonged to deleted sections
                    Query.RemoveOrphanedParts(oSegment.WPDoc);
                    //GLOG 15810: DocVars may be encrypted in Saved Segment
                    Query.SetDocumentVariableEncryption(oSegment.WPDoc, false, "");
                }
                //GLOG 8462
                if (shCompatibilityVersion > 0)
                {
                    Query.SetWordCompatibility(oSegment.WPDoc, shCompatibilityVersion);
                }
                //GLOG 8371: Ensure Forte Document Schema is present for TaskPane display
                Query.AttachForteSchemaIfNecessary(oSegment.WPDoc);
                if (!string.IsNullOrEmpty(xTemplate))
                {
                    //JTS 8/1/16:  With API creation, blank document is created based on Template, then segment XML inserted into that.
                    //This means that all Template styles exist in output document.  To replicate this behavior, we first replace styles
                    //XML in WPDocument with that in Template, then copy over those styles that exist in the segment XML.
                    //=========================================
                    //TODO:  Are there other template properties we should be copying as well, since we're not starting with a 
                    //blank document based on the template?  (Doc Variables, Doc Properties, etc.)
                    if (File.Exists(xTemplate))
                    {
                        string xTempFile = Path.GetTempFileName();
                        //Copy source template to temp file, even if it's currently being read
                        using (var inputFile = new FileStream(
                                xTemplate,
                                FileMode.Open,
                                FileAccess.Read,
                                FileShare.ReadWrite))
                        {
                            using (var outputFile = new FileStream(xTempFile, FileMode.Create))
                            {
                                var buffer = new byte[inputFile.Length];
                                int bytes;

                                while ((bytes = inputFile.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    outputFile.Write(buffer, 0, bytes);
                                }
                            }
                        }

                        WordprocessingDocument oTemplateDoc = WordprocessingDocument.Open(xTempFile, true);
                        //Copy styles from Segment to temp file, overwriting any that already exist
                        //GLOG 8605: Don't overwrite template DocDefaults settings
                        Query.CopyStyles(oSegment.WPDoc, oTemplateDoc, false, true, false);
                        //Now replace original Segment styles from temp file - will now combine styles from Template and Segment
                        Query.ReplaceDocStyles(oTemplateDoc, oSegment.WPDoc);
                        //GLOG8851
                        Query.UpdateSettingsFromTemplate(oTemplateDoc, oSegment.WPDoc);
                        oTemplateDoc.Close();
                        oTemplateDoc = null;
                        File.Delete(xTempFile);
                        //Import styles defined in Segment XML
                        oSegment.ForteDocument.UpdateStylesFromXML(oDef.XML, oSegment.WPDoc);
                        Random oRand = new Random();
                        string xRelId = "rId" + oRand.Next(10000000, 99999999).ToString();
                        AttachedTemplate oTemplate = new AttachedTemplate() { Id = xRelId };
                        DocumentSettingsPart oSettingsPart = oSegment.WPDoc.MainDocumentPart.DocumentSettingsPart;
                        oSettingsPart.AddExternalRelationship("http://schemas.openxmlformats.org/officeDocument/2006/relationships/attachedTemplate",
                            new Uri(xTemplate, UriKind.Absolute), xRelId);
                        oSettingsPart.Settings.Append(oTemplate);
                        oSettingsPart.RootElement.Save();
                    }
                }
                UpdateImageRelationshipIds(oSegment.WPDoc);
                //Ensure unique Doc Var IDs
                Query.RegenerateDocVarIDs(oSegment.WPDoc, oParent != null ? oParent.WPDoc : null);
                //GLOG 8620: Make sure mpo variables don't contain XML tokens
                //This ensures variables are in same state as after AddDocumentVariablesFromXML on API side
                Query.RestoreXMLCharsInDocVars(oSegment.WPDoc);
                //GLOG 8712
                oSegment.RawSegmentParts = Query.GetTopLevelRawSegmentParts(oSegment.WPDoc, oSegment is XmlUserSegment ? xID : null);
                if (iMode != OxmlCreationMode.Design)
                {
                    //Assign GUID TagIDs
                    oSegment.Reindex(oSegment.WPDoc, oParent != null ? oParent.FullTagID : "", true);
                }
                if (oParent != null)
                {
                    //Set ParentTagID for external children
                    foreach (RawSegmentPart oPart in oSegment.RawSegmentParts)
                    {
                        if (oSegment is XmlPaper || oSegment is XmlSidebar || oPart.ContentControl.Ancestors<Document>().Count() == 0)
                        {
                            oPart.SetSegmentPropertyValue("ParentTagID", oParent.TagID, false); //GLOG 8542
                        }
                    }
                }
                string xTagID = oSegment.RawSegmentParts[0].TagID;
                oSegment.Initialize(xTagID, oParent);
                if (iMode != OxmlCreationMode.Design)
                {
                    //GLOG 8726: Delete bookmarks before initializing variables to ensure these don't
                    //interfere with handling of DeleteScopes
                    //Delete mSEG bookmarks - avoids orphaned bookmarks and these will be recreated when Content Controls are removed
                    Query.DeleteSegmentBookmarks(oSegment.WPDoc);
                    Query.DeleteBookmark("mpNewSegment", oSegment.WPDoc);
                    //GLOG 15754: Need to initialize Authors for Saved Segments
                    //GLOG 8652
                    oSegment.InitializeAuthors(oPrefill, iMode == OxmlCreationMode.Finalize && !LMP.MacPac.MacPacImplementation.IsServer);
                    if (!bCreatingFromSavedContent || oPrefill != null) //GLOG 15809
                    {
                        //GLOG 8820: Don't attempt to initialize values if there are no authors
                        //Message will be displayed on the TaskPane side.
                        if (oSegment.Authors.Count > 0 || !oSegment.DefinitionRequiresAuthor)
                        {
                            //GLOG 15786: Force all Prefill values to be used if finishing Static segment
                            oSegment.InitializeValues(oPrefill, oSegment.CreationMode == OxmlCreationMode.StaticFinish);
                            if (oStructure != null)
                                oSegment.InsertCollectionTableStructure(oSegment, oStructure);
                        }
                    }
                    else
                    {
                        oSegment.SetChildSegmentsStatus(Status.Finished);
                    }
                    oSegment.SetBodyText(xBodyText);
                    if (iMode == OxmlCreationMode.Finalize)
                    {
                        oSegment.ForteDocument.UpdateContentXml(new XmlSegment[] { oSegment });
                        //GLOG 15850: Don't remove Segment content controls until writing or returning Flat Xml
                        //Query.DeleteSegmentContentControls(oSegment.WPDoc);
                        Query.SetDocVarAttributesEncryption(oSegment.WPDoc, true, "");
                        //GLOG 15875: This will now be handled by the TaskPane
                        ////GLOG 8824
                        //if (!string.IsNullOrEmpty(oSegment.ProtectedFormPassword))
                        //{
                        //    string xPassword = oSegment.ProtectedFormPassword;
                        //    if (xPassword.ToUpper() == "[EMPTY]")
                        //    {
                        //        xPassword = "";
                        //    }
                        //    XmlForteDocument.ApplyDocumentProtection(oSegment.WPDoc, xPassword, DocumentProtectionValues.Forms);
                        //}
                    }
                }
                else
                {
                    oSegment.RestoreDeletedScopesForDesign(bSuppressDesignErrors);
                    oSegment.InsertPlaceHolderText();
                    //oSegment.ForteDocument.UpdateContentXml(new XmlSegment[] { oSegment });
                    //Always encrypt in design
                    Query.SetDocVarAttributesEncryption(oSegment.WPDoc, true, "");
                }
                oSegment.CreationStatus = Status.Finished;
                //GLOG 15786: Run Distributed Segment actions at end
                if (iMode == OxmlCreationMode.StaticFinish)
                {
                    oSegment.PrepareForFinish();
                }
                LMP.Benchmarks.Print(t0, oSegment.Name + " [" + oSegment.ID + "]");
            }
            catch (System.Exception oE)
            {
                //Show or log error
                if (iMode != OxmlCreationMode.Design || !bSuppressDesignErrors)
                    Error.Show(oE);
                else
                    throw oE;
            }
            return oSegment;
        }
        public void SetBodyText(string xBodyText)
        {
            DateTime t0 = DateTime.Now;
            try
            {
                OpenXmlElement oTarget = null;
                ParagraphProperties oProps = null;
                OpenXmlElement oNewBody = null;
                WordprocessingDocument oBodyDoc = null;
                string xFootnotesRefXML = "";
                string xEndnotesRefXML = "";
                string xCommentsRefXML = "";
                string xEmbeddedXML = "";
                string xBodyXML = "";
                if (!string.IsNullOrEmpty(xBodyText))
                {
                    SdtElement oBody = this.GetBody();
                    if (oBody != null)
                    {
                        if (oBody is SdtCell)
                        {
                            oBody = XmlForteDocument.ConvertSdtCellToSdtBlock((SdtCell)oBody);
                            if (!(oBody is SdtBlock))
                            {
                                //Conversion didn't work for some reason
                                return;
                            }
                        }
                        if (oBody is SdtRun)
                        {
                            oTarget = oBody.Ancestors<Paragraph>().First();
                            oProps = oTarget.Descendants<ParagraphProperties>().FirstOrDefault();
                            oNewBody = new SdtBlock(oBody.OuterXml);
                            oNewBody.Descendants<SdtContentBlock>().First().RemoveAllChildren();
                        }
                        else
                        {
                            oTarget = oBody;
                            oProps = oBody.Descendants<Paragraph>().First().Descendants<ParagraphProperties>().FirstOrDefault();
                            oNewBody = new SdtBlock(oBody.OuterXml);
                            oNewBody.Descendants<SdtContentBlock>().First().RemoveAllChildren();
                        }
                        //Need to add to parent before setting InnerXml, to ensure all required NameSpaces are defined
                        oNewBody = oTarget.Parent.InsertBefore(oNewBody, oTarget);
                        //Remove original Body paragraph
                        oTarget.Remove();
                        if (String.IsWordOpenXML(xBodyText))
                        {
                            //Create WPDocument based on Body XML
                            oBodyDoc = XmlForteDocument.GetWPDocument(xBodyText);
                            //Copy In-Use styles to Segment's WPDocument if they don't already exist
                            Query.CopyStyles(oBodyDoc, this.WPDoc, true, false, false);
                            //string xBodyXml = String.ExtractBodyXML(xBodyText);
                            //GLOG 15921
                            Query.GetEmbeddedPartsXml(xBodyText, ref xBodyXML, ref xEmbeddedXML, ref xFootnotesRefXML, ref xEndnotesRefXML, ref xCommentsRefXML, 
                                this.WPDoc.MainDocumentPart, this.WPDoc, this.ForteDocument);
                            //Limit content to set of paragraphs
                            int iPos = xBodyXML.IndexOf("<w:p>");
                            //GLOG 8622: Xml may include w:rsidP attributes
                            if (iPos == -1)
                                iPos = xBodyXML.IndexOf("<w:p w:rsid");
                            if (iPos > -1)
                            {
                                //Strip off Section properties (headers and footers)
                                int iPos2 = xBodyXML.IndexOf("<w:sectPr");
                                if (iPos2 > -1)
                                {
                                    xBodyXML = xBodyXML.Substring(0, iPos2);
                                }
                                iPos2 = xBodyXML.LastIndexOf("</w:p>");
                                if (iPos2 > -1)
                                {
                                    xBodyText = xBodyXML.Substring(iPos, (iPos2 + 6) - iPos);
                                    oNewBody.Descendants<SdtContentBlock>().First().InnerXml = xBodyText;
                                }
                                else
                                    return;
                            }
                            else
                                return;
                        }
                        else
                        {
                            oNewBody.AppendChild<Paragraph>(new Paragraph(new Run(new Text(xBodyText))));
                        }
                        //If no <w:pStyle> nodes, all text is Normal style
                        if (!oNewBody.Descendants<ParagraphStyleId>().Any())
                        {
                            Paragraph[] oParas = oNewBody.Descendants<Paragraph>().ToArray<Paragraph>();
                            foreach (Paragraph oPara in oParas)
                            {
                                //Remove all style and direct formatting from inserted paragraphs
                                oPara.RemoveAllChildren<ParagraphProperties>();
                                if (oProps != null)
                                {
                                    //Duplicate existing Paragraph Properties (including style) for each inserted paragraph
                                    oPara.PrependChild(oProps.CloneNode(true));
                                }

                            }
                        }
                        //GLOG : 15921 : ceh - RestoreRelatedParts
                        Query.RestoreEmbeddedParts(xEmbeddedXML, xFootnotesRefXML, xEndnotesRefXML, xCommentsRefXML, this.WPDoc.MainDocumentPart, this.WPDoc, this.ForteDocument);
                    }

                }
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// returns ContentControl containing the body of the segment
        /// if it exists - else returns null
        /// </summary>
        /// <returns></returns>
        public SdtElement GetBody()
        {
            XmlBlock oBlock = null;
            for (int i = 0; i < this.Blocks.Count; i++)
            {
                //GLOG 3260: Make sure oBlock is never set to non-body block
                if (this.Blocks[i].IsBody)
                {
                    oBlock = this.Blocks[i];
                    break;
                }
            }

            if (oBlock == null)
            {
                // The segment has no body. Use message variable as body if it exists.
                if (this.Variables.VariableExists("Message"))
                {
                    XmlVariable oVar = this.Variables.ItemFromName("Message");

                    if (oVar.AssociatedContentControls.Count() > 0)
                        return oVar.AssociatedContentControls[0];
                    else
                        return null;
                }
                else
                    return null;
            }
            else
                return oBlock.AssociatedContentControl;
        }
        private void InsertPlaceHolderText()
        {
            DateTime t0 = DateTime.Now;
            //Look for variable content controls in each RawSegmentPart
            foreach (RawSegmentPart oPart in this.RawSegmentParts)
            {
                SdtElement oCC = oPart.ContentControl;
                SdtElement[] oVarList = oCC.Descendants<SdtElement>().Where(o => Query.GetBaseName(o) == "mVar").ToArray();
                foreach (SdtElement oVarCC in oVarList)
                {
                    //Skip mVars belongin to child segments to avoid duplication
                    if (Query.GetParentSegmentContentControl(oVarCC) == oCC)
                    {
                        string xObjectData = Query.GetAttributeValue(this.WPDoc, oVarCC, "ObjectData");
                        string xVarName = Query.GetAttributeValue(this.WPDoc, oVarCC, "Name");
                        if (xObjectData != null)
                        {
                            string xText = Query.GetmVarObjectDataValue(xObjectData, xVarName, Query.VariableProperties.DisplayName);
                            xText = String.RestoreXMLChars(xText);
                            xText = xText.Replace("&", "");
                            //Delete any doc vars associated with mSubVars
                            //JTS: Use SetText to ensure existing formatting is retained
                            m_oForteDocument.SetText(oVarCC, xText);
                            Query.DeleteAssociatedDocVars(this.WPDoc, oVarCC, "mSubVar", "", "");
                            //OpenXmlElement oContent = oVarCC.Descendants().Where(x => x.LocalName == "sdtContent").FirstOrDefault();
                            //if (oContent != null && oContent.InnerText != xText)
                            //{
                            //    Paragraph oPara = oContent.Descendants<Paragraph>().FirstOrDefault();
                            //    if (oPara != null)
                            //    {
                            //        for (int i = oPara.ChildElements.Count - 1; i >= 0; i--)
                            //        {
                            //            OpenXmlElement oChild = oPara.ChildElements[i];
                            //            if (!(oChild is ParagraphProperties))
                            //                oChild.Remove();
                            //        }
                            //        oPara.AppendChild<Run>(new Run(new Text() { Text = xText }));
                            //    }
                            //    else
                            //    {
                            //        oContent.RemoveAllChildren();
                            //        oContent.AppendChild<Run>(new Run(new Text() { Text = xText }));
                            //    }
                            //}
                        }
                    }
                }
            }
            //Also process all child segments
            for (int i = 0; i < this.Segments.Count; i++)
            {
                XmlSegment oChild = this.Segments[i];
                oChild.InsertPlaceHolderText();
            }
            LMP.Benchmarks.Print(t0);
        }
        //jws 8/26/16
        private void Reindex(WordprocessingDocument oWPDoc, string xParentTagID, bool bDoChildren)
        {
            DateTime t0 = DateTime.Now;
            string xNewTagID = Query.ReindexSegment(oWPDoc, this.RawSegmentParts[0].TagID, xParentTagID, true);
            this.FullTagID = (xParentTagID != "" ? xParentTagID + "." : "") + xNewTagID;
            if (bDoChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    XmlSegment oChild = this.Segments[i];
                    oChild.Reindex(oWPDoc, this.FullTagID, bDoChildren);
                }
            }
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns an OpenXml WordprocessingDocument for segment
        /// with the specified ID
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        public static WordprocessingDocument GetSegmentWPDoc(string xSegmentID)
        {
            DateTime t0 = DateTime.Now;

            //get the segment def for this segment
            ISegmentDef oDef = XmlSegment.GetDefFromID(xSegmentID);

            //segment xml is now held as a Base64 string
            byte[] bytes = Convert.FromBase64String(oDef.XML);
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                WordprocessingDocument wpDoc = WordprocessingDocument.Open(ms, false);

                Benchmarks.Print(t0);

                return wpDoc;
            }

        }

        /// <summary>
        /// returns a base64 string that represents the opc archive
        /// of the specified flat segment xml
        /// </summary>
        /// <param name="xFlatSegmentXML"></param>
        /// <returns></returns>
        public static string GetOPCFromFlatXml(string xFlatSegmentXML)
        {
            DateTime t0 = DateTime.Now;
            MemoryStream oStream = null;
            string xBase64Xml = null;
            bool bHasAuthorPrefsTag = false;

            if (xFlatSegmentXML.StartsWith("<mAuthorPrefs>"))
            {
                xFlatSegmentXML = xFlatSegmentXML.Substring(14);
                bHasAuthorPrefsTag = true;
            }

            XmlDocument oDocXML = new XmlDocument();
            oDocXML.LoadXml(xFlatSegmentXML);
            XmlNamespaceManager oNSMan = new XmlNamespaceManager(oDocXML.NameTable);

            if (!String.IsWordOpenXML(xFlatSegmentXML))
                throw new LMP.Exceptions.XMLException("The segment xml is not valid.");

            OpenXmlPowerTools.FlatOpc.FlatToOpc(xFlatSegmentXML, out oStream);

            using (oStream)
            {
                byte[] aOpcBytes = oStream.ToArray();
                xBase64Xml = Convert.ToBase64String(aOpcBytes);
            }

            if (bHasAuthorPrefsTag)
                xBase64Xml = "<mAuthorPrefs>" + xBase64Xml;

            Benchmarks.Print(t0);

            return xBase64Xml;
        }

        public static string GetFlatXmlFromBase64OpcString(string xBase64Opc)
        {
            DateTime t0 = DateTime.Now;
            bool bHasAuthorPrefsTag = false;

            if (xBase64Opc.StartsWith("<mAuthorPrefs>"))
            {
                //trim author prefs tag
                xBase64Opc = xBase64Opc.Substring(14);
                bHasAuthorPrefsTag = true;
            }

            //segment xml is now held as a Base64 string
            byte[] bytes = Convert.FromBase64String(xBase64Opc);

            using (MemoryStream ms = new MemoryStream(bytes))
            {
                string flatXml = OpenXmlPowerTools.FlatOpc.OpcToTextString(ms);

                if (bHasAuthorPrefsTag)
                    //prepend back the author prefs tag
                    flatXml = "<mAuthorPrefs>" + flatXml;

                Benchmarks.Print(t0);

                return flatXml;
            }
        }

        public static string GetFlatXml(string xSegmentID)
        {
            ISegmentDef oDef = XmlSegment.GetDefFromID(xSegmentID);
            string xSourceXML = oDef.XML;

            if (String.IsBase64SegmentString(xSourceXML))
                return GetFlatXmlFromBase64OpcString(xSourceXML);
            else
                return xSourceXML;
        }

        internal void RefreshBlocks()
        {
            m_oBlocks = new XmlBlocks(this);
        }

        /// <summary>
        /// returns a prefill object populated with 
        /// the values of the segment's variables
        /// </summary>
        /// <returns></returns>
        public XmlPrefill CreatePrefill()
        {
            return new XmlPrefill(this);
        }

        ///// <summary>
        ///// calls CI
        ///// </summary>
        //public void GetContacts()
        //{
        //    //GLOG : 7469 : ceh
        //    if (!LMP.Data.Application.AllowCIWithoutOutlookRunning())
        //        return;

        //    Data.Application.StartCIIfNecessary();

        //    //GLOG 3461: Reset these each time, as list of CI-enabled variables might have changed
        //    m_iRetrieveDataFrom = 0;
        //    m_iRetrieveDataTo = 0;
        //    m_iRetrieveDataCC = 0;
        //    m_iRetrieveDataBCC = 0;
        //    m_aCustomEntities = null;
        //    m_iCIAlerts = 0;

        //    GetCIFormat(ref m_iRetrieveDataTo, ref m_iRetrieveDataFrom, ref m_iRetrieveDataCC,
        //        ref m_iRetrieveDataBCC, ref m_aCustomEntities, ref m_iCIAlerts);
        //    bool bCustomEntitiesExist = (m_aCustomEntities != null && m_aCustomEntities.Length > 0);

        //    // No CI-enabled variabled
        //    if (m_iRetrieveDataTo == 0 && m_iRetrieveDataFrom == 0 &&
        //        m_iRetrieveDataCC == 0 && m_iRetrieveDataBCC == 0 &&
        //        !bCustomEntitiesExist)
        //        return;

        //    //GLOG - 3503 - ceh 
        //    //GLOG - 3218 - ceh 
        //    //GLOG - 3631 - ceh 
        //    //Get Max Contacts information
        //    GetCIMaxContacts(ref m_iMax, ref m_iToMax, ref m_iFromMax, ref m_iCCMax, ref m_iBCCMax, ref m_iOtherMax);

        //    //check if maximum contacts has been reached
        //    if ((m_iRetrieveDataTo != 0 || m_iRetrieveDataFrom != 0 ||
        //        m_iRetrieveDataCC != 0 || m_iRetrieveDataBCC != 0 ) &&
        //        (m_iToMax == -1 && m_iFromMax == -1 && m_iCCMax == -1 && m_iBCCMax == -1 && m_iOtherMax == -1))
        //    {
        //        //System.Windows.Forms.MessageBox.Show(LMP.Resources.GetLangString("Msg_MaxNumberOfContactsAllFields"),
        //        //                                LMP.ComponentProperties.ProductName, System.Windows.Forms.MessageBoxButtons.OK,
        //        //                                System.Windows.Forms.MessageBoxIcon.Exclamation); 
        //        return;
        //    }

        //    //Get Display Name information
        //    GetCIDisplayName(ref m_xToLabel, ref m_xFromLabel, ref m_xCCLabel, ref m_xBCCLabel);

        //    try
        //    {
        //        //get position to "park" CI dialog
        //        System.Drawing.Point oCoords = LMP.Data.Application.User.UserSettings.CIDlgLocation;
        //        float x = (float)oCoords.X;
        //        float y = (float)oCoords.Y;

        //        //set CI dialog position
        //        TSG.CI.CSession oCI = Data.Application.CISession;
        //        oCI.FormLeft = x;
        //        oCI.FormTop = y;

        //        string xSep = "|";

        //        //if custom entities exist, show the custom CI UI -
        //        //TODO: we have to, at some point, support custom entities
        //        //when there are standard entities as well, but for now,
        //        //when custom entities exist, no standard entities are displayed -
        //        //clearly, segments can't request both
        //        ICContacts oContacts = null;

        //        if (bCustomEntitiesExist)
        //        {
        //            Array oArray = (Array)m_aCustomEntities;
        //            oContacts = oCI.GetContactsFromArray(ref oArray, xSep);
        //        }
        //        else
        //        {
        //            //#3218 - ceh 
        //            //use variable display name as the label for the right side list box in the CI dialog
        //            oContacts = oCI.GetContactsEx(m_iRetrieveDataTo,
        //                m_iRetrieveDataFrom, m_iRetrieveDataCC, m_iRetrieveDataBCC,
        //                TSG.CI.CI.ciSelectionLists.ciSelectionList_To,0, m_iCIAlerts,
        //                m_xToLabel, m_xFromLabel, m_xCCLabel, m_xBCCLabel,
        //                m_iToMax, m_iFromMax, m_iCCMax, m_iBCCMax);
        //        }

        //        //alert that CI dialog has been released
        //        if (CIDialogReleased != null)
        //            CIDialogReleased(this, new EventArgs());

        //        //save CI dialog coordinates for next use
        //        oCoords.X = (int)oCI.FormLeft;
        //        oCoords.Y = (int)oCI.FormTop;

        //        LMP.Data.Application.User.UserSettings.CIDlgLocation = oCoords;

        //        if ((oContacts != null) && (oContacts.Count() > 0))
        //        {
        //            //add contact detail to ci-enabled variables
        //            SetCIEnabledVariables(oContacts);

        //            //alert that contacts have been added
        //            if (this.ContactsAdded != null)
        //                this.ContactsAdded(this, new EventArgs());
        //        }
        //        if (oContacts != null)
        //            System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContacts);

        //        oContacts = null;
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.CIException(
        //        LMP.Resources.GetLangString("Error_CouldNotRetrieveCIData"), oE);
        //    }
        //}
        ///// <summary>
        ///// calls CI to populate a single variable
        ///// </summary>
        //public void GetContacts(XmlVariable oVar)
        //{
        //    string xToLabel = "";
        //    string xFromLabel = "";
        //    string xCCLabel = "";
        //    string xBCCLabel = "";
        //    int iMax = 0;
        //    int iToMax = -1;
        //    int iFromMax = -1;
        //    int iCCMax = -1;
        //    int iBCCMax = -1;
        //    int iOtherMax = -1;

        //    //GLOG : 7469 : ceh
        //    if (!Data.Application.AllowCIWithoutOutlookRunning())
        //        return;

        //    if (oVar.CIContactType == 0)
        //        return;

        //    Data.Application.StartCIIfNecessary();

        //    TSG.CI.ciRetrieveData iRetrieveDataTo = TSG.CI.ciRetrieveData.ciRetrieveData_None;
        //    TSG.CI.ciRetrieveData iRetrieveDataFrom = TSG.CI.ciRetrieveData.ciRetrieveData_None;
        //    TSG.CI.ciRetrieveData iRetrieveDataCC = TSG.CI.ciRetrieveData.ciRetrieveData_None;
        //    TSG.CI.ciRetrieveData iRetrieveDataBCC = TSG.CI.ciRetrieveData.ciRetrieveData_None;
        //    TSG.CI.ciAlerts iAlerts = TSG.CI.ciAlerts.ciAlert_None;
        //    TSG.CI.CI.ciSelectionLists iDefList = 0;

        //    List<XmlVariable> oVarList = new List<XmlVariable>();
        //    oVarList.Add(oVar);
        //    object[,] aCustomEntity = null;

        //    XmlContacts.GetCIFormat(oVarList, ref iRetrieveDataTo, ref iRetrieveDataFrom, ref iRetrieveDataCC,
        //        ref iRetrieveDataBCC, ref aCustomEntity, ref iAlerts);

        //    // Variable not ci-enabled
        //    if (iRetrieveDataTo == 0 && iRetrieveDataFrom == 0 &&
        //        iRetrieveDataCC == 0 && iRetrieveDataBCC == 0 && 
        //        (aCustomEntity == null || aCustomEntity[0,0] == null))
        //        return;

        //    //GLOG - 3503 - ceh 
        //    //GLOG - 3218 - ceh 
        //    //GLOG - 3631 - ceh 
        //    //GLOG - 3635 - ceh
        //    //Get Max Contacts information
        //    this.GetCIVariableInfo(oVarList, ref iMax, ref iToMax,
        //                           ref iFromMax, ref iCCMax, ref iBCCMax, ref iOtherMax);

        //    //check if maximum contacts has been reached
        //    if ((iRetrieveDataTo != 0 || iRetrieveDataFrom != 0 ||
        //        iRetrieveDataCC != 0 || iRetrieveDataBCC != 0) &&
        //        (iToMax == -1 && iFromMax == -1 && iCCMax == -1 && iBCCMax == -1 && iOtherMax == -1))
        //    {
        //        //System.Windows.Forms.MessageBox.Show(LMP.Resources.GetLangString("Msg_MaxNumberOfContacts"),
        //        //                                LMP.ComponentProperties.ProductName, System.Windows.Forms.MessageBoxButtons.OK,
        //        //                                System.Windows.Forms.MessageBoxIcon.Exclamation);
        //        return;
        //    }

        //    //Get Display Name information
        //    this.GetCIVariableInfo(oVarList, ref xToLabel,
        //                           ref xFromLabel, ref xCCLabel, ref xBCCLabel);

        //    try
        //    {
        //        ICContacts oContacts =  null;

        //        //get position to "park" CI dialog
        //        System.Drawing.Point oCoords = LMP.Data.Application.User.UserSettings.CIDlgLocation;
        //        float x = (float)oCoords.X;
        //        float y = (float)oCoords.Y;

        //        //set CI dialog position
        //        TSG.CI.CSession oCI = Data.Application.CISession;
        //        oCI.FormLeft = x;
        //        oCI.FormTop = y;

        //        if (aCustomEntity != null && aCustomEntity.Length != 0)
        //        {
        //            string xSep = "|";
        //            System.Array oArray = (Array)aCustomEntity;
        //            oContacts = Data.Application.CISession.GetContactsFromArray(ref oArray, xSep);
        //        }
        //        else
        //        {
        //            //#3218 - ceh 
        //            //use variable display name as the label for the right side list box in the CI dialog
        //            oContacts = Data.Application.CISession
        //                .GetContactsEx(iRetrieveDataTo, iRetrieveDataFrom,
        //                iRetrieveDataCC, iRetrieveDataBCC,
        //                iDefList, 0, iAlerts, 
        //                xToLabel, xFromLabel, xCCLabel, xBCCLabel,
        //                iToMax, iFromMax, iCCMax, iBCCMax);
        //        }

        //        //alert that CI dialog has been released
        //        if (CIDialogReleased != null)
        //            CIDialogReleased(this, new EventArgs());

        //        //save CI dialog coordinates for next use
        //        oCoords.X = (int)oCI.FormLeft;
        //        oCoords.Y = (int)oCI.FormTop;

        //        LMP.Data.Application.User.UserSettings.CIDlgLocation = oCoords;

        //        if ((oContacts != null) && (oContacts.Count() > 0))
        //        {
        //            //add contact detail to ci-enabled variables
        //            SetCIEnabledVariables(oVarList, oContacts, true);

        //            //alert that contacts have been added
        //            if (this.ContactsAdded != null)
        //                this.ContactsAdded(this, new EventArgs());

        //            //if(oContacts != null)
        //            //    System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContacts);

        //            oContacts = null;
        //        }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.CIException(
        //        LMP.Resources.GetLangString("Error_CouldNotRetrieveCIData"), oE);
        //    }
        //}
        /// <summary>
        /// returns the child segments of this segment
        /// that are of the specified type
        /// </summary>
        /// <param name="iChildObjectType"></param>
        /// <returns></returns>
        public XmlSegment[] FindChildren(mpObjectTypes iChildObjectType)
        {
            return XmlSegment.FindSegments(iChildObjectType, this.Segments);
        }

        /// <summary>
        /// returns true iff the specified 
        /// segment is contained in this segment
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public bool Contains(string xTagID)
        {
            return FindSegment(xTagID, this.Segments) != null;
        }

        /// <summary>
        /// restores all deleted scopes in the segment
        /// </summary>
        public void RestoreDeletedScopesForDesign(bool bSuppressErrors)
        {
            DateTime t0 = DateTime.Now;
            //GLOG 1407 - set flag to avoid execution indexes from being adjusted
            //when variables are added with delete scopes
            m_oForteDocument.DeleteScopesUpdatingInDesign = true;

            //GLOG 5315 (dm) - ensure that deleted scopes nested in restored blocks
            //are themselves restored
            bool bHasConditionalBlock = false;

            //cycle through variables - execute action, leaving expanded scope, if variable
            //has an IncludeExcludeBlock action or is in its deleted state
            for (int i = 0; i < this.Variables.Count; i++)
            {
                XmlVariable oVar = this.Variables[i];
                for (int j = 0; j < oVar.VariableActions.Count; j++)
                {
                    XmlVariableAction oAction = oVar.VariableActions[j];
                    XmlVariableActions.Types iType = oAction.Type;
                    //GLOG - 3395 - CEH
                    if ((iType == XmlVariableActions.Types.IncludeExcludeBlocks) ||
                        (oVar.TagType == XmlForteDocument.TagTypes.DeletedBlock) ||
                        (oVar.TagType == XmlForteDocument.TagTypes.Mixed))
                    {
                        //restore deleted scope
                        try
                        {
                            oAction.Execute(true);
                        }
                        catch (System.Exception oE)
                        {
                            //Always log error in Server mode
                            if (!bSuppressErrors || LMP.MacPac.MacPacImplementation.IsServer)
                            {
                                Error.Show(oE);
                            }
                            else
                            {
                                //log error and continue
                                string xErrorDetail = LMP.Error.GetErrorDetail(oE);
                                LMP.Trace.WriteError(xErrorDetail);
                            }
                            continue;
                        }

                        //GLOG 5315 (dm) - flag presence of conditional block
                        if (iType == XmlVariableActions.Types.IncludeExcludeBlocks)
                            bHasConditionalBlock = true;

                        //break;
                    }
                }
            }

            //GLOG 5315 (dm) - check for any remaining mDels
            if (bHasConditionalBlock)
            {
                this.Refresh();
                for (int i = 0; i < this.Variables.Count; i++)
                {
                    XmlVariable oVar = this.Variables[i];
                    if ((oVar.TagType == XmlForteDocument.TagTypes.DeletedBlock) ||
                        (oVar.TagType == XmlForteDocument.TagTypes.Mixed))
                        oVar.VariableActions.Execute(true);
                }
            }

            //GLOG 6328 (dm) - we're no longer persisting deleted scopes xml in design -
            //any value that remains is obsolete - clear out to prevent conflicts in the
            //event that a variable of the same name is imported
            //TODO: OpenXML rewrite
            //if (this.Nodes.GetItemDeletedScopes(this.FullTagID).Length > 2)
            //    this.Nodes.ClearItemDeletedScopes(this.FullTagID);

            //child segments
            for (int i = 0; i < this.Segments.Count; i++)
                this.Segments[i].RestoreDeletedScopesForDesign(bSuppressErrors);

            //GLOG 1407 - reset flag
            m_oForteDocument.DeleteScopesUpdatingInDesign = false;
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// inserts the xml of the segment at the specified location
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="oLocation"></param>
        /// <returns>returns the unindexed TagID of the inserted segment</returns>
        //abstract public void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation, 
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted, 
        //    mpSegmentIntendedUses iIntendedUse);
        /// <summary>
        /// Returns List of Unique IDs of child segments
        /// </summary>
        /// <returns></returns>
        internal System.Collections.Generic.List<string> ChildSegmentList(bool bIncludeChildren)
        {
            System.Collections.Generic.List<string> aList = new System.Collections.Generic.List<string>();
            for (int i = 0; i < this.Segments.Count; i++)
            {
                if (!aList.Contains(this.Segments[i].ID))
                    aList.Add(this.Segments[i].ID);

                if (bIncludeChildren)
                {
                    //Append IDs of Children of Children
                    System.Collections.Generic.List<string> aChildList = this.Segments[i].ChildSegmentList(bIncludeChildren);
                    foreach (string xItem in aChildList)
                    {
                        if (!aList.Contains(xItem))
                            aList.Add(xItem);
                    }
                }
            }
            return aList;
        }
        /// <summary>
        /// sets authors of all child segments whose authors are
        /// not linked to this segment's authors
        /// </summary>
        public void SetChildAuthorsFromDocument(bool bExecuteActions)
        {
            for (int i = 0; i < this.Segments.Count; i++)
            {
                XmlSegment oChild = this.Segments[i];
                //do only if not linked to this segment's authors
                if (!oChild.LinkAuthorsToParent)
                {
                    oChild.SetAuthorsFromDocument(bExecuteActions);
                    oChild.SetChildAuthorsFromDocument(bExecuteActions);
                }
            }
        }
        /// <summary>
        /// returns the body of the segment
        /// if it exists - else returns null
        /// </summary>
        /// <returns></returns>
        //public Word.Range GetBody()
        //{
        //    XmlBlock oBlock = null;
        //    for (int i = 0; i < this.Blocks.Count; i++)
        //    {
        //        //GLOG 3260: Make sure oBlock is never set to non-body block
        //        if (this.Blocks[i].IsBody)
        //        {
        //            oBlock = this.Blocks[i];
        //            break;
        //        }
        //    }

        //    if (oBlock == null)
        //    {
        //        // The segment has no body. Use message variable as body if it exists.
        //        if (this.Variables.VariableExists("Message"))
        //        {
        //            XmlVariable oVar = this.Variables.ItemFromName("Message");

        //            if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
        //            {
        //                if (oVar.AssociatedWordTags.Length > 0)
        //                    return oVar.AssociatedWordTags[0].Range;
        //                else
        //                    return null;
        //            }
        //            else
        //            {
        //                if (oVar.AssociatedContentControls.Length > 0)
        //                    return oVar.AssociatedContentControls[0].Range;
        //                else
        //                    return null;
        //            }
        //        }
        //        else
        //            return null;
        //    }
        //    else if (this.ForteDocument.FileFormat == mpFileFormats.Binary)
        //    {
        //        return oBlock.AssociatedWordTag.Range;
        //    }
        //    else
        //        return oBlock.AssociatedContentControl.Range;
        //}
        /// <summary>
        /// builds jurisdictional domain string from segment def L0-L4
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        public string GetJurisdictionText()
        {
            return GetJurisdictionText(true, true, true, true, true);
        }
        /// <summary>
        /// builds jurisdictional domain string from segment def L0-L4
        /// </summary>
        /// <param name="bIncludeL0"></param>
        /// <param name="bIncludeL1"></param>
        /// <param name="bIncludeL2"></param>
        /// <param name="bIncludeL3"></param>
        /// <param name="bIncludeL4"></param>
        /// <returns></returns>
        public string GetJurisdictionText(bool bIncludeL0, bool bIncludeL1, bool bIncludeL2,
            bool bIncludeL3, bool bIncludeL4)
        {
            if (!(this is XmlAdminSegment))
                return "";

            XmlAdminSegment oSegment = (XmlAdminSegment)this;
            //build display string from jurisdiction levels 0 - 4
            StringBuilder oSB = new StringBuilder();
            //GLOG 3248: Handle invalid/non-existent levels, so that Design will load successfully
            try
            {
                Jurisdictions oJurs = null;
                Jurisdiction oJur;
                int iL0 = 0;
                int iL1 = 0;
                int iL2 = 0;
                int iL3 = 0;
                int iL4 = 0;

                if (bIncludeL0)
                {
                    oJurs = new Jurisdictions(Jurisdictions.Levels.Zero);

                    if (oJurs.Count != 0)
                    {
                        try
                        { iL0 = oSegment.L0; }

                        catch { }
                        if (iL0 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL0));
                            }
                            catch (System.Exception oE)
                            {
                                //Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                            oSB.Append("\r\n");
                        }
                    }
                }
                if (bIncludeL1)
                {
                    oJurs = null;
                    oJurs = new Jurisdictions(Jurisdictions.Levels.One);
                    if (oJurs != null)
                    {
                        try
                        { iL1 = oSegment.L1; }

                        catch { }
                        if (iL1 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL1));
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 3248: Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                            oSB.Append("\r\n");
                        }
                    }
                }

                if (bIncludeL2)
                {
                    oJurs = null;
                    oJurs = new Jurisdictions(Jurisdictions.Levels.Two);
                    if (oJurs != null)
                    {
                        try
                        { iL2 = oSegment.L2; }

                        catch { }
                        if (iL2 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL2));
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 3248: Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                            oSB.Append("\r\n");
                        }
                    }
                }

                if (bIncludeL3)
                {
                    oJurs = null;
                    oJurs = new Jurisdictions(Jurisdictions.Levels.Three);
                    if (oJurs != null)
                    {
                        try
                        { iL3 = oSegment.L3; }

                        catch { }
                        if (iL3 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL3));
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 3248: Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                            oSB.Append("\r\n");
                        }
                    }
                }

                if (bIncludeL4)
                {
                    oJurs = null;
                    oJurs = new Jurisdictions(Jurisdictions.Levels.Four);
                    if (oJurs != null)
                    {
                        try
                        { iL4 = oSegment.L4; }
                        catch { }
                        if (iL4 != 0)
                        {
                            try
                            {
                                oJur = ((Jurisdiction)oJurs.ItemFromID(iL4));
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 3248: Invalid level - stop processing here
                                throw oE;
                            }
                            oSB.Append(oJur.Name);
                        }
                    }
                }
            }
            catch { }
            string xTemp = oSB.ToString().TrimEnd('\n', '\r');
            return oSB.ToString();
        }
        /// <summary>
        /// returns true if segment design requires level chooser control
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        public bool LevelChooserRequired()
        {
            if (!(this is XmlAdminSegment))
                return false;

            switch (this.TypeID)
            {
                case mpObjectTypes.Pleading:
                    return true;
                case mpObjectTypes.Service:
                    return true;
                case mpObjectTypes.Notary:
                    return true;
                case mpObjectTypes.Verification:
                    return true;
                case mpObjectTypes.PleadingCaption:
                    return true;
                case mpObjectTypes.PleadingCounsel:
                    return true;
                case mpObjectTypes.PleadingSignature:
                    return true;
                case mpObjectTypes.PleadingPaper:
                    return true;
                case mpObjectTypes.PleadingCaptionBorderSet:
                    return true;
                case mpObjectTypes.PleadingCoverPage:
                    return true;
                case mpObjectTypes.PleadingCaptions:
                    return true;
                case mpObjectTypes.PleadingCounsels:
                    return true;
                case mpObjectTypes.PleadingSignatures:
                    return true;
                case mpObjectTypes.PleadingSignatureNonTable:
                    return true;
                default:
                    return false;
            }
        }
        /// <summary>
        /// Builds delmited string formatted for use with JurisdictionChooser control
        /// </summary>
        /// <returns></returns>
        public string GetJurisdictionLevels()
        {
            if (!(this is XmlAdminSegment))
                return "";

            XmlAdminSegment oSeg = (XmlAdminSegment)this;
            string xSep = StringArray.mpEndOfRecord.ToString();
            string xFormat = "{0}" + xSep + "{1}" + xSep + "{2}" + xSep + "{3}" + xSep + "{4}";
            return string.Format(xFormat, oSeg.L0.ToString(), oSeg.L1.ToString(), oSeg.L2.ToString(), oSeg.L3.ToString(), oSeg.L4.ToString());
        }
        /// <summary>
        /// updates the contact detail of all
        /// variables in the segment
        /// </summary>
        public bool UpdateContactDetail()
        {
            try
            {
                bool bDetailUpdated = false;
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].UpdateContactDetail())
                    {
                        bDetailUpdated = true;
                    }
                }

                for (int i = 0; i < this.Variables.Count; i++)
                {
                    if (this.Variables[i].UpdateContactDetail())
                    {
                        bDetailUpdated = true;
                    }
                }
                return bDetailUpdated;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotUpdateSegmentContactDetail"), oE);
            }
        }
        /// <summary>
        /// Clear AssociatedControls to release any objects
        /// </summary>
        /// <param name="bIncludeChildren"></param>
        internal void UnloadVariableControls(bool bIncludeChildren)
        {
            for (int i = 0; i < m_oVariables.Count; i++)
            {
                //Need to explicitly release ICContacts object
                if (m_oVariables[i].AssociatedControl is LMP.Controls.SalutationCombo)
                    ((LMP.Controls.SalutationCombo)m_oVariables[i].AssociatedControl).ClearContacts();
                m_oVariables[i].AssociatedControl = null;
            }
            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    this.Segments[i].UnloadVariableControls(bIncludeChildren);
                }
            }
        }

        /// <summary>
        /// returns TRUE if the specified language is supported by this segment
        /// </summary>
        /// <param name="iCulture"></param>
        /// <returns></returns>
        public bool LanguageIsSupported(int iCulture)
        {
            string xSupportedCultures = '�' + this.SupportedLanguages + '�';
            string xCulture = '�' + iCulture.ToString() + '�';
            return (xSupportedCultures.IndexOf(xCulture) > -1);
        }

        /// <summary>
        /// sets creation status of all child segments
        /// </summary>
        /// <param name="iStatus"></param>
        internal void SetChildSegmentsStatus(LMP.Architect.Oxml.XmlSegment.Status iStatus)
        {
            for (int i = 0; i < this.Segments.Count; i++)
            {
                XmlSegment oChild = this.Segments[i];
                oChild.CreationStatus = iStatus;
                oChild.SetChildSegmentsStatus(iStatus);
            }
        }
        public void UpdateTableBorders()
        {
            //try
            //{
            //    //GLOG 4282: Adjust length of center special character border
            //    //TODO: BOOKMARKS - RESTORE THIS FUNCTIONALITY - dcf
            //    return;
            //    if (!(this is XmlCollectionTableItem))
            //        //Currently only appropriate for collection table items
            //        return;

            //    //JTS 4/16/10: Content Control support
            //    bool bContentControls = this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML;

            //    Environment oEnv = new Environment(this.ForteDocument.WordDocument);
            //    Word.Range oRng = null;
            //    Object oNodesArray = null;
            //    if (bContentControls)
            //    {
            //        oRng = this.FirstContentControl.Range;
            //        oNodesArray = this.ContentControls;
            //    }
            //    else
            //    {
            //        oRng = this.FirstWordTag.Range;
            //        oNodesArray = this.WordTags;
            //    }

            //    //GLOG 3144: Update length special character border if it exists
            //    if (oRng.Tables.Count > 0 &&
            //        oRng.Tables[1].Columns.Count > 2)
            //    {
            //        oEnv.SaveState();
            //        int iRowIndex = 0;
            //        iRowIndex = oRng.Cells[1].RowIndex;
            //        //Check that there is a narrow border column
            //        if (oRng.Tables[1].Cell(iRowIndex, 2).Width < 72)
            //        {
            //            DateTime t0 = DateTime.Now;
            //            XmlForteDocument.WordXMLEvents iIgnore = LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents;
            //            //Ignore XML events during update
            //            LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;
            //            LMP.Forte.MSWord.WordDoc.UpdateSpecialCaptionBorders(ref oNodesArray, "", "", "");
            //            //Restore original state
            //            LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = iIgnore;
            //            LMP.Benchmarks.Print(t0);
            //        }
            //        oEnv.RestoreState(true, false, false);
            //    }
            //}
            //catch (System.Exception oE)
            //{
            //    throw oE;
            //}
        }

        /// <summary>
        /// returns a new object database id and increments tracker
        /// </summary>
        /// <returns></returns>
        public int GetNewObjectDatabaseID()
        {
            int iID = m_iNextObjectDatabaseID;
            m_iNextObjectDatabaseID++;
            return iID;
        }

        /// <summary>
        /// deletes all variables belonging to this segment and its children
        /// </summary>
        public void DeleteVariables()
        {
            DeleteVariables(false);
        }
        /// <summary>
        /// deletes all variables belonging to this segment and its children
        /// </summary>
        public void DeleteVariables(bool bTaglessOnly)
        {
            ////delete variables of child segments
            //for (int i = 0; i < this.Segments.Count; i++)
            //    this.Segments[i].DeleteVariables(bTaglessOnly);

            ////delete variables of this segment
            //for (int i = this.Variables.Count - 1; i >= 0; i--)
            //    if (!bTaglessOnly || this.Variables[i].IsTagless)
            //        this.Variables.Delete(i);

            ////GLOG 7361 (dm) - remove deleted scope doc vars
            //Word.Bookmark[] aBmks = this.Bookmarks;
            //object oBmks = (object)aBmks;
            //LMP.Forte.MSWord.WordDoc.RemoveDeletedScopeDocVars(oBmks);
        }

        /// <summary>
        /// returns whether the segment has variables without invoking its
        /// variables property and thereby populating the collection -
        /// 8-18-11 (dm) - changed from a property to a method in order to add parameter
        /// </summary>
        /// <param name="bIncludeChildren"></param>
        /// <returns></returns>
        public bool HasVariables(bool bIncludeChildren)
        {
            //GLOG 6708: Access this.Variables instead of m_oVariables 
            //to ensure collection is initialized if necessary
            if ((this.Variables != null) && (this.Variables.Count > 0))
                return true;

            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].HasVariables(true))
                        return true;
                }
            }

            return false;
        }
        /// <summary>
        /// returns true iff the segment has variables that
        /// display in the task pane
        /// </summary>
        /// <param name="bIncludeChildren"></param>
        /// <returns></returns>
        public bool HasDisplayVariables(bool bIncludeChildren)
        {
            //XmlVariable oVar = null;

            int iNumVariables = this.Variables.Count;

            for (int i = 0; i < iNumVariables; i++)
            {
                if ((this.Variables[i].DisplayIn & XmlVariable.ControlHosts.DocumentEditor) ==
                    XmlVariable.ControlHosts.DocumentEditor)
                {
                    return true;
                }
            }

            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].HasDisplayVariables(true))
                        return true;
                }
            }

            return false;
        }
        /// <summary>
        /// returns whether the segment has blocks without invoking its
        /// blocks property and thereby populating the collection -
        /// </summary>
        /// <param name="bIncludeChildren"></param>
        /// <returns></returns>
        public bool HasBlocks(bool bIncludeChildren)
        {
            //GLOG 6708: Access this.Blocks instead of m_oBlocks
            //to ensure collection is initialized if necessary
            if ((this.Blocks != null) && (this.Blocks.Count > 0))
                return true;

            if (bIncludeChildren)
            {
                for (int i = 0; i < this.Segments.Count; i++)
                {
                    if (this.Segments[i].HasBlocks(true))
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// writes the segment to the specified file in the specified directory
        /// </summary>
        /// <returns>returns fully qualified file name</returns>
        public string WriteToFile(string xDir, string xFileName, bool bFinish)
        {
            if (!Directory.Exists(xDir))
                throw new LMP.Exceptions.DirectoryException(
                    "Could not write file.  The 'Deca\\SaveDirectoryUrl' registry value is missing or invalid.");

            string xFullName = xDir + "\\" + xFileName;
            xFullName = xFullName.Replace("\\\\", "\\");

            WordprocessingDocument oCopyDoc = null;
            MemoryStream oCopyStream = new MemoryStream();
            FlushBuffer();
            //GLOG 15850: If creating document for editing, remove Segment Content Controls
            if (bFinish)
            {
                this.m_oStream.WriteTo(oCopyStream);
                oCopyDoc = WordprocessingDocument.Open(oCopyStream, true);
                Query.DeleteSegmentContentControls(oCopyDoc);
                oCopyDoc.Package.Flush();
                oCopyDoc.Close();
            }
            else
            {
                oCopyStream = this.Stream;
                oCopyDoc = this.WPDoc;
            }
            using (FileStream oFS = File.Create(xFullName))
            {
                oCopyStream.Seek(0, SeekOrigin.Begin);
                oCopyStream.CopyTo(oFS);
                oFS.Close();
            }
            return xFullName;
        }

        /// <summary>
        /// flushes the package buffer to ensure that all
        /// changes are reflected in the XmlSegment's underlying stream 
        /// </summary>
        public void FlushBuffer()
        {
            //Close and reopen WordProcessingDocument
            //to ensure all changes to packages are reflected in stream
            this.WPDoc.Package.Flush();
            this.WPDoc.Close();
            MemoryStream oNewStream = new MemoryStream();
            m_oStream.WriteTo(oNewStream);
            m_oStream.Close();
            m_oStream = oNewStream;
            m_oStream.Position = 0L;
            this.WPDoc = WordprocessingDocument.Open(m_oStream, true);
            if (this.ForteDocument != null)
            {
                this.ForteDocument.WPDocument = this.WPDoc;
                if (this.IsTopLevel)
                {
                    //GLOG 15850: Need to requery RawSegmentParts
                    //since original WPDoc no longer exists
                    this.RawSegmentParts = Query.GetTopLevelRawSegmentParts(m_oWPDoc, this.ID); //GLOG 15951
                    this.GetChildSegments();
                }
            }
        }
        /// <summary>
        /// writes the segment to the save directory
        /// </summary>
        /// <returns>returns fully qualified file name</returns>
        public string WriteToSaveDirectory()
        {
            string xDir = LMP.Registry.GetMacPac10Value("SaveDirectoryUrl");
            string xName = System.Guid.NewGuid().ToString() + ".docx";
            return WriteToFile(xDir, xName, this.ForteDocument.Mode != Base.ForteDocument.Modes.Design);
        }

        /// <summary>
        /// writes the segment to the temp directory
        /// </summary>
        /// <returns></returns>
        public string WriteToTempDirectory(string xFileName)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                string xDir = Path.GetTempPath();
                //GLOG 15850
                return WriteToFile(xDir, xFileName, this.ForteDocument.Mode != Base.ForteDocument.Modes.Design);
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
        }
        #endregion
        #region *********************static members*********************
        ///// <summary>
        ///// fills an ordered dictionaryh with the
        ///// child segment structure of the specified segment
        ///// </summary>
        ///// <param name="oSegment"></param>
        ///// <param name="oStructure"></param>
        //public static void GetChildTableCollectionStructure(Segment oSegment, ref ChildStructure oStructure)
        //{
        //    for (int i = 0; i < oSegment.Segments.Count; i++)
        //    {
        //        Segment oChild = oSegment.Segments[i];
        //        if (oChild.Segments.Count > 0)
        //        {
        //            //recurse
        //            GetChildTableCollectionStructure(oChild, ref oStructure);
        //        }

        //        //limit child structure to table collections and
        //        //table collection items, as these are the only children
        //        //whose location we can know on re-insertion
        //        if (oChild is CollectionTableItem)
        //        {
        //            //get child segment prefill
        //            Prefill oPrefill = new Prefill(oChild);

        //            oStructure.Add(oChild.FullTagID + "_" + oChild.ID, oPrefill);
        //        }
        //    }
        //}

        /// <summary>
        /// returns a list of segment IDs of segments that
        /// contain the segment with the specified ID
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        public static List<string> GetParentSegmentIDs(string xSegmentID)
        {
            //GLOG 4449
            return GetParentSegmentIDs(xSegmentID, false);
        }
        public static List<string> GetParentSegmentIDs(string xSegmentID, bool bIncludeUserSegments)
        {
            List<string> oParentList = null;

            // Get list of other segments containing this segment
            ArrayList aRelatedList = LMP.Data.Application.GetContainingSegmentIDs(xSegmentID);

            if (aRelatedList != null)
            {
                oParentList = new List<string>();

                for (int i = 0; i < aRelatedList.Count; i++)
                {
                    string xNewID = (((object[])aRelatedList[i])[0]).ToString();
                    if (!xNewID.Contains("."))
                        xNewID = xNewID + ".0";

                    //GLOG 4449: Don't include UserSegments in List unless specified
                    if (xNewID.Contains(".0") || bIncludeUserSegments)
                    {
                        //Don't add duplicates
                        if (!oParentList.Contains(xNewID))
                            oParentList.Add(xNewID);
                    }
                }
            }

            return oParentList;
        }

        ///// <summary>
        ///// inserts the specified segment at the specified location of the specified document
        ///// </summary>
        ///// <param name="iDefinitionID">the id of the segment definition</param>
        ///// <param name="oLocation">the location to insert the segment</param>
        ///// <param name="iInsertionOptions">options available for segment insertion</param>
        ///// <param name="oForteDoc">MacPac document representing the Word document into which the segment will be inserted.</param>
        ///// <param name="oPrefill">the prefill used to prefill segment variables</param>
        ///// <param name="bInsertXMLOnly"></param>
        ///// <param name="bTargetXMLInsertion"></param>
        ///// <param name="xXML">the XML inserted - if empty, the XML of the segment definition is used</param>
        ///// <returns></returns>
        //internal static XmlSegment Insert(string xID, XmlSegment oParent, XmlSegment.InsertionLocations iInsertionLocation,
        //    XmlSegment.InsertionBehaviors iInsertionBehavior, XmlForteDocument oForteDoc,
        //    XmlPrefill oPrefill, bool bUseTargetedXMLInsertion, string xXML)
        //{
        //    return Insert(xID, oParent, iInsertionLocation, iInsertionBehavior, oForteDoc, oPrefill,
        //        bUseTargetedXMLInsertion, true, xXML, false, false, false, null);
        //}
        ////GLOG 6021
        //internal static XmlSegment Insert(string xID, XmlSegment oParent, XmlSegment.InsertionLocations iInsertionLocation,
        //    XmlSegment.InsertionBehaviors iInsertionBehavior, XmlForteDocument oForteDoc,
        //    XmlPrefill oPrefill, bool bUseTargetedXMLInsertion, bool bInsertingInMainStory,
        //    string xXML, bool bUpdateAllStyles, bool bForcePrefillOverride, bool bAttachToTemplate,
        //    XmlCollectionTableStructure oChildStructure)
        //{
        //    return Insert(xID, oParent, iInsertionLocation, iInsertionBehavior, oForteDoc, oPrefill,
        //        bUseTargetedXMLInsertion, bInsertingInMainStory, xXML, bUpdateAllStyles, bForcePrefillOverride, bAttachToTemplate,
        //        oChildStructure, null);
        //}
        ///// <summary>
        ///// inserts the specified segment at the specified location of the specified document
        ///// </summary>
        ///// <param name="iDefinitionID">the id of the segment definition</param>
        ///// <param name="oLocation">the location to insert the segment</param>
        ///// <param name="iInsertionOptions">options available for segment insertion</param>
        ///// <param name="oForteDoc">MacPac document representing the Word document into which the segment will be inserted.</param>
        ///// <param name="oPrefill">the prefill used to prefill segment variables</param>
        ///// <param name="bInsertXMLOnly"></param>
        ///// <param name="bTargetXMLInsertion"></param>
        ///// <param name="InsertingInHeaderFooter">Set to true by ContentManager when inserting in a Header/Footer</param>
        ///// <param name="xXML">the XML inserted - if empty, the XML of the segment definition is used</param>
        ///// <returns></returns>
        //internal static XmlSegment Insert(string xID, XmlSegment oParent, XmlSegment.InsertionLocations iInsertionLocation,
        //    XmlSegment.InsertionBehaviors iInsertionBehavior, XmlForteDocument oForteDoc,
        //    XmlPrefill oPrefill, bool bUseTargetedXMLInsertion, bool bInsertingInMainStory,
        //    string xXML, bool bUpdateAllStyles, bool bForcePrefillOverride, bool bAttachToTemplate,
        //    XmlCollectionTableStructure oChildStructure, ArrayList aSections) //GLOG 6021:  Additional parameter to specify sections for insertion
        //{
        //    DateTime t0 = DateTime.Now;

        //    bool bTempParaCreated = false;
        //    bool bInsertedAtEnd = false; //GLOG 7827
        //    XmlSegment oSegment = null;
        //    XmlSegment oContentSegment = null; //GLOG 6920
        //    ISegmentDef oContentDef = null; //GLOG 6920
        //    object oXMLArray = null; //GLOG 7876
        //    //GLOG 4418
        //    string[] xTrailerXML = null;
        //    bool bUseQuickTrailerInsertion = false;
        //    int iTrailerIndex = 0;
        //    bool bOneInserted = false;
        //    string xSegmentName = "";
        //    string xTagID = "";

        //    //GLOG #5925 CEH
        //    //turn off track changes if necessary
        //    bool bTrackChanges = oForteDoc.WordDocument.TrackRevisions;
        //    if (bTrackChanges)
        //        oForteDoc.WordDocument.TrackRevisions = false;

        //    //temporary only - seeing if we can get rid of the bDesign parameter
        //    bool bDesign = oForteDoc.Mode == XmlForteDocument.Modes.Design;

        //    //get the segment def for this segment
        //    ISegmentDef oDef = XmlSegment.GetDefFromID(xID);

        //    //oUndo = new LMP.Forte.MSWord.Undo();
        //    //oUndo.MarkStart("Segment Insertion - " + oDef.DisplayName);

        //
        //    Word.Document oWordDoc = oForteDoc.WordDocument;

        //    short shLocation;

        //    //if no location has been specified, use the default location
        //    if (iInsertionLocation == 0)
        //        shLocation = (short)InsertionLocations.Default;
        //    else
        //        shLocation = (short)iInsertionLocation;

        //    //delete previous existing instances
        //    //of single instance segments
        //    oSegment = CreateSegmentObject(oDef.TypeID, oForteDoc);
        //    oSegment.Definition = oDef;
        //    //GLOG 6913: Section below moved up
        //    //GLOG 6799 (dm) - moved this block up for new segment bookmark validation
        //    //GLOG 6920: Moved up again so that appropriate InsertXML override can be called
        //    bool bInsertingSavedContent = oDef.TypeID == mpObjectTypes.SavedContent;
        //    string xValidationID = xID;
        //    if (bInsertingSavedContent)
        //    {
        //        //GLOG 6920:  If Saved Segment, get Segment Definition from XML
        //        //this is saved content - segment needs to be
        //        //switched over to segment of contained xml
        //        string xContainedID = oDef.ChildSegmentIDs;
        //        if (!xContainedID.Contains(".") || xContainedID.EndsWith(".0"))
        //        {
        //            //this is an admin segment
        //            int iID1;
        //            int iID2;
        //            AdminSegmentDefs oDefs = new AdminSegmentDefs();
        //            LMP.Data.Application.SplitID(xContainedID, out iID1, out iID2);
        //            oContentDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);
        //            oContentSegment = CreateSegmentObject(oContentDef.TypeID, oForteDoc);
        //            oContentSegment.Definition = oContentDef;
        //        }
        //        else
        //        {
        //            //this is a user segment
        //            //GLOG 4728: Filter collection by User to include accessible objects
        //            UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, LMP.Data.Application.User.ID);
        //            oContentDef = (UserSegmentDef)oDefs.ItemFromID(xContainedID);
        //            oContentSegment = CreateSegmentObject(oContentDef.TypeID, oForteDoc);
        //            oContentSegment.Definition = oContentDef;
        //        }
        //        xValidationID = xContainedID; //GLOG 6799 (dm)
        //    }
        //    else
        //    {
        //        oContentSegment = oSegment;
        //        oContentDef = oDef;
        //        oContentSegment.Definition = oDef;
        //    }
        //    //GLOG item #4734 - dcf
        //    //get specific insertion behavior if insertion behavior is
        //    //set to default
        //    if (iInsertionBehavior == InsertionBehaviors.Default)
        //        iInsertionBehavior = GetDefaultInsertionBehaviors(oContentDef.TypeID);


        //    //GLOG 6799 (dm, 5/30/13)
        //    if (xValidationID.EndsWith(".0"))
        //        xValidationID = xValidationID.Replace(".0", "");

        //    //GLOG 6021
        //    object[] oSections = new object[0];
        //    if (aSections != null)
        //        oSections = aSections.ToArray();
        //    //GLOG 6920:  Where appropriate below, we should be checking properties of oContentSegment in case oSegment is Saved Content
        //    if (oContentSegment is ISingleInstanceSegment && aSections == null) //GLOG 6021: Don't check this if called from Trailer dialog
        //    {
        //        //GLOG 6820: Don't need to preserve Segment bookmark here, since content is only being deleted
        //        System.Array aInsertionLoc = oDoc.GetInsertionLocations(oWordDoc, shLocation, oSections, false); //GLOG 6021

        //        XmlSegments oExistingSegments = null;
        //        foreach (Word.Range oLocation in aInsertionLoc)
        //        {
        //            Word.Range oInsertRange = oLocation.Duplicate;
        //            oExistingSegments = ((ISingleInstanceSegment)oContentSegment)
        //                .GetExistingSegments(oInsertRange.Sections.First);
        //            if (oExistingSegments != null)
        //            {
        //                for (int i = oExistingSegments.Count - 1; i >= 0; i--)
        //                {
        //                    int iSections = oContentSegment.ForteDocument.WordDocument.Sections.Count;
        //                    oContentSegment.ForteDocument.DeleteSegment(oExistingSegments.ItemFromIndex(i));
        //                    //GLOG 4862: If number of sections has changed, Refresh Tags
        //                    //as XMLNodes in Headers/Footers might have changed
        //                    if (iSections > oContentSegment.ForteDocument.WordDocument.Sections.Count)
        //                        oContentSegment.ForteDocument.Refresh(bDesign ?
        //                            XmlForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes :
        //                            XmlForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes);

        //                }
        //            }
        //        }
        //    }
        //    //deal with insertion location
        //    //GLOG 6820:  Temp paragraph doesn't need to be inserted for trailers
        //    System.Array aInsertionLocations = oDoc.GetInsertionLocations(oWordDoc, shLocation, oSections, oContentDef.TypeID != mpObjectTypes.Trailer, (int)oContentDef.TypeID); //GLOG 6021 //GLOG 7608 //GLOG 7548
        //    foreach (Word.Range oLocation in aInsertionLocations)
        //    {
        //        //GLOG 6913: If oSegment cleared by Trailer insertion, recreate new object
        //        //GLOG 7000: Reinitialize Segment object on each pass, so that Nodes will reflect latest state of document
        //        if (bOneInserted)
        //        {
        //            oSegment = CreateSegmentObject(oContentDef.TypeID, oForteDoc);
        //            oSegment.Definition = oContentDef;
        //        }
        //        Word.Range oInsertRange = oLocation.Duplicate;
        //        int iSection = oLocation.Sections[1].Index; //GLOG 6021
        //        //GLOG 5472: If Trailer or Draft Stamp, skip insertion after first section if all stories used by Content are linked
        //        if (bOneInserted && (oContentDef.TypeID == mpObjectTypes.Trailer || oContentDef.TypeID == mpObjectTypes.DraftStamp))
        //        {
        //            Word.Section oCurSection = oInsertRange.Sections[1];
        //            bool bLinkedFirstFooter = false;
        //            bool bLinkedFirstHeader = false;
        //            bool bLinkedPrimaryFooter = false;
        //            bool bLinkedPrimaryHeader = false;
        //            bool bLinkedEvenFooter = false;
        //            bool bLinkedEvenHeader = false;

        //            bool bSectionContainsLinkedStories = LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(oCurSection,
        //                ref bLinkedPrimaryFooter, ref bLinkedPrimaryHeader, ref bLinkedFirstFooter, ref bLinkedFirstHeader,
        //                ref bLinkedEvenFooter, ref bLinkedEvenHeader, true, false);
        //            if (bSectionContainsLinkedStories)
        //            {
        //                string xSegXML = oContentDef.XML;
        //                string xBodyXML = LMP.String.ExtractBodyXML(xSegXML);

        //                //This can appear 2 ways, depending on which version of Word was used to save
        //                xBodyXML = xBodyXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
        //                    "<w:body><w:p/><w:sectPr>");
        //                xBodyXML = xBodyXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
        //                    "<w:body><wx:sect><w:p/><w:sectPr>");

        //                //GLOG 5403 (dm) - the preconversion process adds a space to the w:br node
        //                if (!LMP.String.IsWordOpenXML(xSegXML))
        //                {
        //                    xBodyXML = xBodyXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
        //                        "<w:body><w:p/><w:sectPr>");
        //                    xBodyXML = xBodyXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
        //                        "<w:body><wx:sect><w:p/><w:sectPr>");
        //                }
        //                //GLOG 5472: Skip if Segment has empty body, and all Headers and Footers used by content are linked in current section
        //                if (!LMP.String.ContentContainsUnlinkedStories(oDef.XML, bLinkedPrimaryFooter, bLinkedPrimaryHeader,
        //                    bLinkedFirstFooter, bLinkedFirstHeader, bLinkedEvenFooter, bLinkedEvenHeader) &&
        //                    (xBodyXML.IndexOf("<w:body><w:p/><w:sectPr>") > -1 || xBodyXML.IndexOf("<w:body><w:p /><w:sectPr>") > -1 ||
        //                    xBodyXML.IndexOf("<w:body><wx:sect><w:p/><w:sectPr>") > -1 || xBodyXML.IndexOf("<w:body><w:p /><w:sectPr>") > -1))
        //                    continue;
        //            }
        //        }
        //        //GLOG 7876: Insert finished Trailer XML directly after first section
        //        if (bUseQuickTrailerInsertion && oXMLArray != null)
        //        {
        //            LMP.Data.FirmApplicationSettings oFirmSettings =
        //                new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);
        //            if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains("CONVERTTRAILERSTO9XFORMAT") ||
        //                oFirmSettings.UseMacPac9xStyleTrailer)
        //            {
        //                oDoc.InsertConverted9xTrailerXMLAtLocation(oLocation, ref oXMLArray);
        //            }
        //            else
        //            {
        //                oDoc.InsertTrailerXMLAtLocation(oLocation, ref oXMLArray);
        //            }
        //            continue;
        //        }
        //        //insert wrapper segment if specified and not already wrapped
        //        XmlSegment oInsertedChild = null;
        //        InsertWrapperIfNecessary(xID, oDef, ref oParent, ref oInsertRange,
        //            ref iInsertionBehavior, oForteDoc, oPrefill, bUseTargetedXMLInsertion,
        //            bInsertingInMainStory, ref oInsertedChild);

        //        if (oInsertedChild != null)
        //            //target segment is included in definition of wrapper segment,
        //            //so was already inserted along with it
        //            return oInsertedChild;

        //        //deal with insertion behavior
        //        bool bInsertInNextPageSection = (iInsertionBehavior &
        //            InsertionBehaviors.InsertInSeparateNextPageSection) ==
        //            InsertionBehaviors.InsertInSeparateNextPageSection;

        //        bool bInsertInContinuousSection = (iInsertionBehavior &
        //            InsertionBehaviors.InsertInSeparateContinuousSection) ==
        //            InsertionBehaviors.InsertInSeparateContinuousSection;

        //        LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType;

        //        if (oContentDef.IntendedUse != mpSegmentIntendedUses.AsDocument && !(oContentSegment is XmlPaper))
        //            //segment is not a document segment and is not paper -
        //            //don't remove header content
        //            if (oContentSegment is IDocumentStamp)
        //            {
        //                //GLOG 4243: Set Header/Footer Insertion option depending on whether
        //                //Stamp is being inserted in single section only, or entire document
        //                if (aInsertionLocations.Length > 1)
        //                    iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Append;
        //                else
        //                    iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_ReplaceSameType;
        //            }
        //            else
        //                iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_None;
        //        else
        //        {
        //            //segment is a document segment or is paper -
        //            //set header/footer insertion behavior based on
        //            //specified insertion behavior - if none specified, set behavior
        //            //according to intended use - 
        //            //If Segment is inserted in a header/footer, only insert main body XML
        //            if ((iInsertionBehavior & InsertionBehaviors.KeepExistingHeadersFooters) > 0 ||
        //                ((iInsertionLocation == InsertionLocations.InsertAtSelection)
        //                || (iInsertionLocation == InsertionLocations.Default)) && !bInsertingInMainStory)
        //                iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_None;
        //            else if (((iInsertionBehavior & InsertionBehaviors.KeepExistingHeadersFooters) == 0) &&
        //                ((iInsertionBehavior & InsertionBehaviors.InsertInSeparateNextPageSection) > 0))
        //                //Replace existing header/footers if not explicitly set
        //                iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Replace;
        //            else
        //            {
        //                iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion
        //                    .mpHeaderFooterInsertion_ReplaceSameOrNoType;
        //            }
        //        }
        //        //GLOG - 3590 - CEH
        //        if (bInsertInNextPageSection || bInsertInContinuousSection)
        //        {
        //            bInsertedAtEnd = false; //GLOG 7827
        //            //ensure that markup is showing
        //            oWordDoc.ActiveWindow.View.ShowXMLMarkup = -1;

        //            //insert section at location - 
        //            //GLOG #3989 - dcf -
        //            //some prep work needs to be done if inserting
        //            //in a section at either start or end of a document
        //            object oMissing = System.Reflection.Missing.Value;
        //            if (oInsertRange.Tables.Count > 0 &&
        //                (iInsertionLocation == InsertionLocations.InsertAtStartOfAllSections ||
        //                iInsertionLocation == InsertionLocations.InsertAtStartOfCurrentSection ||
        //                iInsertionLocation == InsertionLocations.InsertAtStartOfDocument))
        //            {
        //                Word.Table oTable = oInsertRange.Tables[1];
        //                LMP.Forte.MSWord.WordDoc.AddParaBeforeTable(ref oTable);
        //                object oParaRng = oInsertRange.Paragraphs[1].Range;
        //                oSegment.ForteDocument.WordDocument.Bookmarks.Add("zzmpTempSectionPara", ref oParaRng); //GLOG 7827

        //                object oCount = 1;
        //                oInsertRange = oInsertRange.Paragraphs[1].Next(ref oCount).Range;
        //                oInsertRange = (Word.Range)oParaRng;
        //                oInsertRange.StartOf(ref oMissing, ref oMissing);
        //                bTempParaCreated = true;
        //            }
        //            else if (iInsertionLocation == InsertionLocations.InsertAtEndOfAllSections ||
        //                iInsertionLocation == InsertionLocations.InsertAtEndOfCurrentSection ||
        //                iInsertionLocation == InsertionLocations.InsertAtEndOfDocument)
        //            {
        //                object oEnd = oInsertRange.End;
        //                Word.Range oLoc = oInsertRange.Document.Range(ref oEnd, ref oEnd);

        //                //GLOG 6464 (dm) - insert "TEMP" placeholder into an empty paragraph -
        //                //any existing text in the last paragraph was getting deleted with the placeholder
        //                if (oInsertRange.Paragraphs[1].Range.Text != "\r")
        //                    oInsertRange.InsertAfter("\r");

        //                oInsertRange.InsertAfter("TEMP\r");
        //                object oParaRng = oInsertRange.Paragraphs.Last.Range;
        //                oSegment.ForteDocument.WordDocument.Bookmarks.Add("zzmpTempSectionPara", ref oParaRng); //GLOG 7827
        //                bTempParaCreated = true;
        //                bInsertedAtEnd = true; //GLOG 7827
        //                oInsertRange.EndOf(ref oMissing, ref oMissing);
        //            }
        //            if (oForteDoc.FileFormat == LMP.Data.mpFileFormats.OpenXML)
        //            {
        //                //JTS 6/9/10: 10.2
        //                oInsertRange = LMP.Forte.MSWord.WordDoc.InsertSection_CC(oInsertRange,
        //                    (iInsertionBehavior & InsertionBehaviors.RestartPageNumbering) > 0,
        //                    bInsertInContinuousSection);
        //            }
        //            else
        //            {
        //                //GLOG 3165: Make use of 'Restart Page Numbering' option
        //                oInsertRange = LMP.Forte.MSWord.WordDoc.InsertSection(oInsertRange,
        //                    (iInsertionBehavior & InsertionBehaviors.RestartPageNumbering) > 0,
        //                    bInsertInContinuousSection);
        //            }
        //        }

        //        //use definition xml if there is no supplied xml -
        //        //this will exist, e.g., when previewing a document-
        //        //in that case we pass in the xml directly from the designer
        //        if (string.IsNullOrEmpty(xXML))
        //            xXML = oDef.XML;

        //        //preconvert if necessary (dm 2/9/11)
        //        //GLOG 5932 (dm) - don't do if preinjunction and file format is .doc -
        //        //adding the bookmarks isn't necessary in this case and was causing
        //        //problem in Cleary's letterhead in Word 2007 - the Reserved attribute
        //        //and doc vars will be added by RefreshTags if it hasn't been done here
        //        if ((!LMP.String.IsWordOpenXML(xXML)) &&
        //            (LMP.Forte.MSWord.WordApp.IsPostInjunctionWordVersion() ||
        //            (oForteDoc.FileFormat == mpFileFormats.OpenXML)))
        //        {
        //            int iID1;
        //            int iID2;
        //            LMP.Data.Application.SplitID(xID, out iID1, out iID2);
        //            xXML = LMP.Conversion.PreconvertIfNecessary(xXML, false, iID1.ToString());
        //        }

        //        //if in design mode, and segment will be inserted in header/footer,
        //        //set the parent to the top-level segment
        //        //GLOG 5317: If oParent has been passed from Designer (because Child Segment
        //        //is being updated in Related Segments) don't re-evaluate here -
        //        //Segment may be a child of a child
        //        if ((oForteDoc.Mode == XmlForteDocument.Modes.Design) &&
        //            (!XmlSegment.TargetIsBody(xXML)) && oParent == null)
        //            oParent = oForteDoc.Segments[0];

        //        //If body consists of page break only, replace with 
        //        //empty paragraph so body won't be inserted
        //        if ((!bDesign || oParent != null) && bUseTargetedXMLInsertion)
        //        {
        //            //This can appear 2 ways, depending on which version of Word was used to save
        //            xXML = xXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
        //                "<w:body><w:p/><w:sectPr>");
        //            xXML = xXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\"/></w:r></w:p><w:sectPr>",
        //                "<w:body><wx:sect><w:p/><w:sectPr>");

        //            //GLOG 5403 (dm) - the preconversion process adds a space to the w:br node
        //            if (!LMP.String.IsWordOpenXML(xXML))
        //            {
        //                xXML = xXML.Replace("<w:body><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
        //                    "<w:body><w:p/><w:sectPr>");
        //                xXML = xXML.Replace("<w:body><wx:sect><w:p><w:r><w:br w:type=\"page\" /></w:r></w:p><w:sectPr>",
        //                    "<w:body><wx:sect><w:p/><w:sectPr>");
        //            }
        //        }

        //        if ((oParent != null) && !(oContentSegment is XmlCollectionTableItem))
        //        {
        //            //add the parent's tag id to the child's
        //            //tags in the xml if necessary -
        //            //GLOG 3482 - don't do for collection table items because oInsertRange
        //            //may not be inside the table at this point and collection table items
        //            //are never external children
        //            XmlSegment.AddParentIDToXMLIfNecessary(ref xXML, oParent, oInsertRange);
        //        }

        //        if (!bUpdateAllStyles)
        //        {
        //            string xDefaultStyle = "";
        //            if ((oContentDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText) ||
        //                (oContentDef.IntendedUse == mpSegmentIntendedUses.AsSentenceText))
        //            {
        //                //Get style of selection, to be used as default
        //                try
        //                {
        //                    xDefaultStyle = ((Word.Style)oInsertRange.Paragraphs[1].get_Style()).NameLocal;
        //                }
        //                catch { }

        //                if (oContentDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText)
        //                {
        //                    //Remove any tags marking paragraphs as Body Text or Normal, 
        //                    //so these will inherit the default style instead
        //                    xXML = xXML.Replace("<w:pStyle w:val=\"Normal\"/>", "");
        //                    xXML = xXML.Replace("<w:pStyle w:val=\"BodyText\"/>", "");
        //                }
        //                else
        //                {
        //                    //GLOG 2543 - always remove style node from sentence text 
        //                    int iPos = xXML.IndexOf("<w:pStyle ");
        //                    if (iPos != -1)
        //                    {
        //                        int iPos2 = xXML.IndexOf("/>", iPos);
        //                        xXML = xXML.Substring(0, iPos) + xXML.Substring(iPos2 + 2);
        //                    }
        //                }
        //            }
        //            //Update Styles definitions to match Target Doc, plus any styles unique to the source
        //            //so that derived style attributes will match base styles in Target -
        //            //we get the xml for the first paragraph because it's the smallest unit we
        //            //can get without breaking Word's XMLSelectionChange event for the session
        //            //GLOG 5128: Make sure both Target and Source XML are using the same format
        //            if (LMP.String.IsWordOpenXML(xXML))
        //                xXML = LMP.String.MergeStylesWordOpenXML(xXML, oForteDoc.WordDocument.Paragraphs[1].Range.WordOpenXML, xDefaultStyle);
        //            else
        //            {
        //                //GLOG 5489:  In Word 2007, Range.XML can result in error 5460 if Mirror Indent paragraph formatting
        //                //exists anywhere in document.  Ignore any error in get_XML, and just leave
        //                //Segment styles unchanges
        //                string xDocXML = "";
        //                try
        //                {
        //                    xDocXML = oForteDoc.WordDocument.Paragraphs[1].Range.get_XML(false);
        //                }
        //                catch
        //                {
        //                }
        //                xXML = LMP.String.MergeStylesXML(xXML, xDocXML, xDefaultStyle);
        //            }
        //        }
        //        //disable Word event handlers
        //        LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;

        //        try
        //        {
        //            object oNewBookmark = "mpNewSegment";
        //            //Make sure there's no existing mpNewSegment bookmark
        //            //Otherwise GetNewSegmentTagID below will return wrong Tag ID
        //            Word.Bookmarks oBmks = oSegment.ForteDocument.WordDocument.Bookmarks;
        //            if (oBmks.Exists(oNewBookmark.ToString()))
        //            {
        //                oBmks.get_Item(ref oNewBookmark).Delete();
        //            }
        //        }
        //        catch { }

        //        //GLOG 4418: If trailer is being inserted in all sections, speed it up
        //        //by plugging the mSEG contents of the first generated trailer directly
        //        //into the XML used for subsequent trailers.  This avoids the time required
        //        //for setting variables and running variable actions
        //        if (!LMP.String.IsWordOpenXML(xXML) && oDef.TypeID == mpObjectTypes.Trailer && xTrailerXML != null && !bDesign)
        //        {
        //            string xNSPrefix = LMP.String.GetNamespacePrefix(xXML, ForteConstants.MacPacNamespace);
        //            //Increment index used in XML for each subsequent trailer, so each will be seen as belonging
        //            //to a different Segment by RefreshTags
        //            bUseQuickTrailerInsertion = LMP.String.SubstituteTrailerXML(
        //                ref xXML, ++iTrailerIndex, xNSPrefix, oContentDef.Name, xTrailerXML);
        //        }

        //        //10.2 (dm) - generate new doc var ids to avoid conflicts on insertion
        //        //GLOG 5932 (dm) - don't do unless xml is preconverted - we're no longer
        //        //preconverting before insertion in non-reconstitution scenarios
        //        if ((oForteDoc.Mode != XmlForteDocument.Modes.Design) &&
        //            LMP.Conversion.IsPreconvertedXML(xXML))
        //            oDoc.RegenerateDocVarIDs(ref xXML, oForteDoc.WordDocument);

        //        try
        //        {
        //            //doc vars from source xml to target document
        //            //12-10-10 (dm) - moved before InsertXML(), since retagging
        //            //in Word 2010 now occurs immediately after xml insertion and
        //            //doc vars are required for that
        //            oDoc.AddDocumentVariablesFromXML(oLocation.Document, xXML, bInsertingSavedContent == true); //GLOG 7906: Include Encrypted doc vars in Saved Content


        //            //insert segment xml - get segment 
        //            //node of inserted segment
        //            oContentSegment.InsertXML(xXML, oParent, oInsertRange,
        //                iHeaderFooterInsertionType, bUseTargetedXMLInsertion, oContentDef.IntendedUse);

        //            //GLOG 7444 (dm) - don't run the following in design
        //            if ((oContentSegment is XmlCollectionTable) && !bDesign)
        //            {
        //                //bookmark segment - these are not bookmarked
        //                //as they are built in, and don't get opened in the
        //                //designer where bookmarking occurs automatically
        //                object o1 = 1;
        //                if (oForteDoc.FileFormat == mpFileFormats.Binary)
        //                {
        //                    Word.XMLNode oNode = oInsertRange.Paragraphs[1].Range.XMLNodes[1];
        //                    object oNodeRng = oNode.Range.Tables[1].Range;
        //                    string xTag = "";
        //                    for (int a = 1; a <= oNode.Attributes.Count; a++)
        //                    {
        //                        if (oNode.Attributes[a].BaseName == "Reserved")
        //                        {
        //                            xTag = oNode.Attributes[a].NodeValue;
        //                            break;
        //                        }
        //                    }
        //                    Word.Bookmark oBmk = oNode.Range.Bookmarks.Add("_" + xTag, ref oNodeRng);
        //                    oDoc.InsertNewSegmentBookmark(oForteDoc.WordDocument, oNode);
        //                }
        //                else
        //                {
        //                    Word.ContentControl oCC = oInsertRange.Paragraphs[1].Range.ContentControls.get_Item(ref o1);
        //                    object oCCRng = oCC.Range.Tables[1].Range;
        //                    Word.Bookmark oBmk = oCC.Range.Bookmarks.Add("_" + oCC.Tag, ref oCCRng);
        //                    oDoc.InsertNewSegmentBookmark_CC(oForteDoc.WordDocument, oCC);
        //                }

        //                //GLOG 7070 (dm) - if inserting at end of parent, expand parent's bookmark to
        //                //encompass the table - this was an issue on insertion of O'Melveneys'
        //                //agreement segments because the signature was replaced by the author's pref
        //                //beyond the bounds of the parent
        //                if (oInsertRange.Start > 0)
        //                {
        //                    Word.Bookmark oParentBmk = null;
        //                    Word.Range oTargetRng = oInsertRange.Duplicate;
        //                    if (oParent != null)
        //                        oParentBmk = oParent.PrimaryBookmark;
        //                    else
        //                    {
        //                        int iStart = oTargetRng.Start - 1;
        //                        oTargetRng.SetRange(iStart, iStart);
        //                        oParentBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oTargetRng);
        //                    }
        //                    if ((oParentBmk != null) && (oParentBmk.End == oInsertRange.End))
        //                    {
        //                        oTargetRng.SetRange(oParentBmk.Start, oInsertRange.Tables[1].Range.End);
        //                        object oTargetRngObject = oTargetRng;
        //                        oTargetRng.Document.Bookmarks.Add(oParentBmk.Name, ref oTargetRngObject);
        //                    }
        //                }
        //            }


        //            //reenable Word event handlers
        //            LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;

        //            //delete segment bounding objects so that users never see
        //            //segment ccs or tags
        //            //Don't do this for Trailers, as all bounding objects will be removed later
        //            //GLOG 6768: Don't delete Bounding Objects in Design
        //            //GLOG 6795: Don'r delete Bounding Object for Master Data Segment
        //            //if (oContentDef.TypeID != mpObjectTypes.Trailer && oContentDef.TypeID != mpObjectTypes.MasterData && !bDesign)
        //            if (!bDesign) //GLOG 6780: Delete Bounding Objects in Master Data also
        //                oDoc.DeleteSegmentBoundingObjects(oForteDoc.WordDocument, false, true); //GLOG 7306 (dm)

        //            oSegment.CreationStatus = Status.BoilerplateInserted;
        //        }
        //        catch (LMP.Exceptions.SegmentInsertionCancelledException)
        //        {
        //            //Insertion was cancelled by Architect class InsertXML
        //            return null;
        //        }
        //        catch (System.Exception oE)
        //        {
        //            string xErrMsg = "";
        //            //Check for predefined error message returned by COM method
        //            if (oE.InnerException != null)
        //            {
        //                xErrMsg = oE.InnerException.Message;
        //                if (xErrMsg.StartsWith("<"))
        //                {
        //                    //Get error message from Resource string
        //                    xErrMsg = xErrMsg.Replace("<", "");
        //                    xErrMsg = xErrMsg.Replace(">", "");
        //                    xErrMsg = LMP.Resources.GetLangString(xErrMsg);
        //                }
        //            }
        //            if (string.IsNullOrEmpty(xErrMsg))
        //                //pass on original Exception
        //                throw oE;
        //            else
        //                //raise special error message text
        //                throw new LMP.Exceptions.SegmentInsertionException(xErrMsg, oE);
        //        }

        //        LMP.Forte.MSWord.WordDoc.RemoveUnavailableXMLSchema(
        //            oSegment.ForteDocument.WordDocument);

        //        //get full tag id of the inserted segment
        //        if (!bUseQuickTrailerInsertion) //GLOG 4418: skip all this for all but first trailer
        //        {
        //            //refresh document tags - if this is runtime, segment tag ids
        //            //should get GUID indexes
        //            oForteDoc.RefreshTags(!bDesign);

        //            //reenable Word event handlers
        //            LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.None;

        //            oWordDoc.Application.ScreenUpdating = false;

        //            xTagID = "";
        //            try
        //            {
        //                //GLOG 6799 (dm) - supply segment id to ensure that we
        //                //find the right segment
        //                xTagID = GetNewSegmentTagID(oForteDoc, xValidationID);
        //            }
        //            catch { }
        //            //GLOG 6820: Move after above line, since GetNewSegmentTagID may trigger XML events
        //            //reenable Word event handlers
        //            LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.None;

        //            oWordDoc.Application.ScreenUpdating = false;

        //            //Insertion may have done nothing if headers/footers are linked
        //            //and there's no body XML - continue with next location
        //            if (string.IsNullOrEmpty(xTagID))
        //                continue;

        //            //CEH - 3333 - CEH
        //            string xStylesXML = oDef.XML;

        //            if (bInsertingSavedContent)
        //            {
        //                //get the segment from the xml in the document
        //                oSegment = XmlSegment.GetSegment(xTagID, oForteDoc);

        //                //GLOG 6465 (dm) - GetSegment() prematurely sets the creation
        //                //status to Finished - restore it to BoilerplateInserted
        //                oSegment.CreationStatus = Status.BoilerplateInserted;
        //            }
        //            else
        //            {
        //                //initialize segment with tag data, etc.
        //                oSegment.Initialize(xTagID, oParent);
        //            }

        //            if (!bDesign)
        //            {
        //                //JTS 4/1/13:  Set Dynamic Editing Environment once at start, so that it won't need to be set individually in each variable action
        //                oSegment.ForteDocument.DynamicEditingInProgress = true;
        //                try
        //                {
        //                    if (!bInsertingSavedContent || oPrefill != null)
        //                    {
        //                        //initialize authors
        //                        //GLOG 6943: Use Parent Authors for linked children
        //                        //GLOG 7842: If Parent Segment doesn't have authors, get from Prefill
        //                        if (!oSegment.IsTopLevel && oSegment.LinkAuthorsToParent && oSegment.Parent.Authors.Count > 0)
        //                            oSegment.InitializeAuthors(null);
        //                        else
        //                            oSegment.InitializeAuthors(oPrefill);

        //                        if (oSegment.Authors.Count == 0 && oSegment.DefinitionRequiresAuthor)
        //                        {
        //                            //author is required, but no authors are available - alert
        //                            MessageBox.Show(LMP.Resources.GetLangString("Msg_AuthorRequiredToInsertSegment"),
        //                                LMP.ComponentProperties.ProductName,
        //                                MessageBoxButtons.OK,
        //                                MessageBoxIcon.Exclamation);
        //                            return null;
        //                        }

        //                        //execute AfterDefaultAuthorSet actions
        //                        oSegment.Actions.Execute(XmlSegment.Events.AfterDefaultAuthorSet);

        //                        //raise after default author set
        //                        if (oSegment.AfterDefaultAuthorSet != null)
        //                            oSegment.AfterDefaultAuthorSet(oSegment, new EventArgs());

        //                        //set segment status
        //                        oSegment.m_iStatus = Status.DefaultAuthorsSet;
        //                    }

        //                    //add the segment to the appropriate collection - 
        //                    //we need to do this at this point because subsequent
        //                    //code may refer to the segment's parent or.ForteDocument
        //                    if (oSegment.IsTopLevel)
        //                        oForteDoc.Segments.Add(oSegment);
        //                    else
        //                        oSegment.Parent.Segments.Add(oSegment);

        //                    //GLOG 4519: update variable that holds id of collection item, if one exists
        //                    //do for first item in collection only
        //                    if (oSegment is XmlCollectionTableItem && (oSegment.Parent.Segments[0] == oSegment))
        //                    {
        //                        XmlSegment oTopSegment = oSegment.Parent.Parent;
        //                        if (oTopSegment != null)
        //                        {
        //                            //Look for variable in form of LetterSignatureID, PleadingCaptionID, etc.
        //                            XmlVariable oVar = null;
        //                            try
        //                            {
        //                                oVar = oTopSegment.Variables.ItemFromName(
        //                                    oSegment.TypeID.ToString() + "ID");
        //                            }
        //                            catch { }
        //                            try
        //                            {
        //                                if (oVar != null)
        //                                    oVar.SetValue(oSegment.ID1.ToString(), false);
        //                            }
        //                            catch { }
        //                        }
        //                    }

        //                    if (bAttachToTemplate)
        //                    {
        //                        string xTemplate = XmlExpression.Evaluate(oSegment.WordTemplate, oSegment, oForteDoc);
        //                        try
        //                        {
        //                            oForteDoc.AttachToTemplate(xTemplate, false);
        //                        }
        //                        catch
        //                        {
        //                            System.Windows.Forms.MessageBox.Show(LMP.Resources.GetLangString("Msg_UnableToAttachTemplate") + xTemplate,
        //                                LMP.ComponentProperties.ProductName, System.Windows.Forms.MessageBoxButtons.OK,
        //                                System.Windows.Forms.MessageBoxIcon.Exclamation);
        //                        }
        //                    }

        //                    if (bUpdateAllStyles)
        //                    {
        //                        if (oSegment.HasDefinition)
        //                            oForteDoc.UpdateStylesFromXML(xStylesXML);
        //                    }
        //                    else if (!string.IsNullOrEmpty(oSegment.RequiredStyles))
        //                    {
        //                        if (oSegment.HasDefinition)
        //                        {
        //                            string[] aStyleList = oSegment.RequiredStyles.Split(new char[] { ',' });
        //                            oForteDoc.UpdateStylesFromXML(xStylesXML, aStyleList);
        //                        }
        //                    }

        //                    if (!bInsertingSavedContent || oPrefill != null)
        //                    {
        //                        //set up the segment that was inserted
        //                        //GLOG 6943: Authors already initialized above
        //                        oSegment.InitializeValues(oPrefill, "", bForcePrefillOverride, oChildStructure != null, false);
        //                    }
        //                    else
        //                        //GLOG 6315 (dm) - unfinished status of children was
        //                        //preventing post-creation author update - finished status
        //                        //is reached at the end of InitializeValues()
        //                        oSegment.SetChildSegmentsStatus(Status.Finished);
        //                }
        //                catch (System.Exception oE)
        //                {
        //                    throw oE;
        //                }
        //                finally
        //                {
        //                    //Clear Dynamic Editing flag
        //                    oSegment.ForteDocument.DynamicEditingInProgress = false;
        //                }
        //            }
        //            else
        //            {
        //                //add the segment to the appropriate collection - 
        //                //we need to do this at this point because subsequent
        //                //code may refer to the segment's parent or.ForteDocument
        //                if (oSegment.IsTopLevel)
        //                    oForteDoc.Segments.Add(oSegment);
        //                else
        //                    oSegment.Parent.Segments.Add(oSegment);

        //                //Copy any Required Styles in Designer
        //                if (!string.IsNullOrEmpty(oSegment.RequiredStyles))
        //                {
        //                    if (oSegment.HasDefinition)
        //                    {
        //                        string[] aStyleList = oSegment.RequiredStyles.Split(new char[] { ',' });
        //                        oForteDoc.UpdateStylesFromXML(oSegment.Definition.XML, aStyleList);
        //                    }
        //                }
        //                oSegment.RestoreDeletedScopesForDesign();

        //                //hide TagExpandedValue field codes in designer -
        //                //10.2 (dm) - skip this for now when using content controls -
        //                //it's not yet clear whether this will ultimately be necessary
        //                if (oForteDoc.FileFormat == LMP.Data.mpFileFormats.Binary)
        //                    oSegment.RefreshExpandedTagValueCodes(false);
        //            }
        //            //execute AfterSegmentXMLInsert actions
        //            oSegment.Actions.Execute(XmlSegment.Events.AfterSegmentXMLInsert);
        //        }

        //        //insert pleading paper, if appropriate -
        //        //we do this here instead of when the XML
        //        //is inserted because the parent has to be fully
        //        //initialized for the pleading paper insertion
        //        //to execute successfully
        //        if (oSegment is ILitigationAddOnSegment)
        //            XmlPleadingPaper.InsertExistingInCurrentSection(oSegment.ForteDocument, oSegment);

        //        //GLOG : 6967 : ceh - remmed out per comment #28798
        //        //GLOG : 6967 : ceh
        //        //check for Pleading Paper
        //        LMP.Forte.MSWord.PleadingPaper oPaper = new LMP.Forte.MSWord.PleadingPaper();

        //        //reset compatibility option - no need with Forte format
        //        //if (oPaper.HasPleadingPaper() &&
        //        //(LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(oSegment.ForteDocument.WordDocument) < 15))
        //        //{
        //            //oSegment.ForteDocument.WordDocument.Compatibility[Word.WdCompatibility.wdSuppressTopSpacing] = false;
        //        //}

        //        if (oSegment is XmlPleadingPaper)
        //        {
        //            //adjust header to match pleading paper
        //            //if parent is toa
        //            if (oSegment.Parent != null && oSegment.Parent.TypeID == mpObjectTypes.TOA)
        //            {
        //                //GLOG : 6967 : ceh
        //                //LMP.Forte.MSWord.PleadingPaper oPaper = new LMP.Forte.MSWord.PleadingPaper();

        //                //GLOG 6852: Use Bookmark range
        //                Word.HeadersFooters oHFs = oSegment.PrimaryRange.Sections.First.Headers;

        //                //JTS 5/7/10: Make sure XML events are not run due to selection changes in COM function
        //                LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
        //                oPaper.AlignHeaderToLine1(oHFs[Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage]);
        //                oPaper.AlignHeaderToLine1(oHFs[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary]);
        //                LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.None;
        //            }
        //        }

        //        //GLOG 6121 - this is necessary because ccs embedded in delete scopes
        //        //are not routinely retagged on insertion - a conflict arises from a
        //        //recreate sequence in which the same non-default paper may get
        //        //inserted twice, once with prefill and again with the child structure
        //        //GLOG 6381 (dm) - retagging is now done routinely, making the
        //        //following code duplicative
        //        //if ((oForteDoc.FileFormat == mpFileFormats.OpenXML) && (oSegment is Paper))
        //        //{
        //        //    Word.ContentControl[] aCCs = oSegment.Nodes.GetContentControls(
        //        //        oSegment.FullTagID);
        //        //    object oSegCCs = (object)aCCs;
        //        //    LMP.Forte.MSWord.WordDoc.RetagDeletedScopes(oSegCCs);
        //        //}

        //        if (oChildStructure != null)
        //        {
        //            LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;
        //            oChildStructure.Insert(oSegment);
        //            LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.None;
        //        }

        //        //set segment status
        //        oSegment.m_iStatus = Status.Finished;

        //        //GLOG 7876: Previous code to pre-populate WordML XML for Trailers has been replaced by new quick insertion code

        //        if (bTempParaCreated)
        //        {
        //            try
        //            {
        //                //GLOG 7827:  Different handling required for deleted range before or after section break
        //                LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All; //GLOG 6075 (dm)
        //                object xName = "zzmpTempSectionPara";
        //                Word.Bookmark oBmk = oSegment.ForteDocument.WordDocument.Bookmarks.get_Item(ref xName);
        //                Word.Range oRng = oBmk.Range.Paragraphs.Last.Range;
        //                if (bInsertedAtEnd)
        //                {
        //                    //Before section break, simple Delete() will work
        //                    oRng.Delete();
        //                }
        //                else
        //                {
        //                    //GLOG 7937 (dm) - if the target range is two tabs and the paragraph mark,
        //                    //which will be the case when AddParaBeforeTable() is used with a 3-column table,
        //                    //the GLOG 7827 code below will result in the first row of the table getting
        //                    //deleted as well - delete the paragraph text first, then the paragraph mark
        //                    object oUnit = Word.WdUnits.wdCharacter;
        //                    object oCount = -1;
        //                    oRng.MoveEnd(ref oUnit, ref oCount);
        //                    oRng.Delete();

        //                    //set range back to paragraph
        //                    oRng = oBmk.Range.Paragraphs.Last.Range;

        //                    //GLOG 7827:  Last paragraph may not be empty if before table,
        //                    //so explicitly indicate number of characters to be deleted;
        //                    //otherwise paragraph mark won't be deleted
        //                    //object oUnit = Word.WdUnits.wdCharacter;
        //                    //object oCount = oRng.Characters.Count;
        //                    //oRng.Delete(ref oUnit, ref oCount);
        //                    oRng.Delete();
        //                }
        //                oBmk.Delete();
        //            }
        //            catch { }
        //            finally
        //            {
        //                LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.None; //GLOG 6075 (dm)
        //            }
        //        }
        //        //GLOG 5472
        //        bOneInserted = true;
        //        xSegmentName = oSegment.Name;
        //        //JTS 6/30/11:  Remove Content Controls or XML Tags from Trailer content
        //        //GLOG 5661: Don't remove Bounding Objects if ConvertTrailersTo9xFormat metadata value is found.
        //        //Separate custom code will deal with this at a later point.
        //        if (oSegment is XmlTrailer && !bDesign) //GLOG 6021: Code reorganized to convert to 9.x format here instead of in DocumentStampUI
        //        {
        //            object oNodesArray = null;
        //            if (oSegment.ForteDocument.FileFormat == mpFileFormats.Binary)
        //            {
        //                //GLOG 6944: Use Bookmarks if present
        //                if (oSegment.WordTags.GetLength(0) == 0)
        //                {
        //                    oNodesArray = oSegment.Bookmarks;
        //                }
        //                else
        //                {
        //                    //xml tags
        //                    Word.XMLNode[] oNodes = oSegment.WordTags;
        //                    oNodesArray = oNodes;
        //                }
        //            }
        //            else
        //            {
        //                //GLOG 6944: Use Bookmarks if present
        //                if (oSegment.ContentControls.GetLength(0) == 0)
        //                {
        //                    oNodesArray = oSegment.Bookmarks;
        //                }
        //                else
        //                {
        //                    //content controls
        //                    Word.ContentControl[] oNodes = oSegment.ContentControls;
        //                    oNodesArray = oNodes;
        //                }
        //            }

        //            oSegment.ForteDocument.Segments.Delete(oSegment.FullTagID);

        //            //2-27-12 (dm) - pass in oTags, so that deleted tags/cc
        //            //can be removed from the collection
        //            LMP.Forte.MSWord.Tags oTags = oForteDoc.Tags;
        //            //GLOG 7876
        //            if (aInsertionLocations.GetUpperBound(0) > 0)
        //            {
        //                //GLOG 7876: Create array for XML inserted in Body,
        //                //3 Footer and 3 Header stories
        //                oXMLArray = new string[7];
        //                bUseQuickTrailerInsertion = true;
        //            }

        //            //GLOG 6108 (dm) - added 9.x trailer option as firm application setting
        //            LMP.Data.FirmApplicationSettings oFirmSettings =
        //                new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);
        //            if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains("CONVERTTRAILERSTO9XFORMAT") ||
        //                oFirmSettings.UseMacPac9xStyleTrailer)
        //            {
        //                int iLocation9x = 100; //JTS 8/12/09 - Default to Footer
        //                switch (iInsertionLocation)
        //                {
        //                    //Insertion locations translate to End of Document 9.x type
        //                    case XmlSegment.InsertionLocations.InsertAtEndOfAllSections:
        //                    case XmlSegment.InsertionLocations.InsertAtEndOfCurrentSection:
        //                    case XmlSegment.InsertionLocations.InsertAtEndOfDocument:
        //                        iLocation9x = 101;
        //                        break;
        //                }
        //                LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;
        //                DateTime t1 = DateTime.Now;
        //                try
        //                {
        //                    //GLOG 6676 (dm): there won't be any prefill when updating an
        //                    //existing 9.x trailer - avoid object reference error
        //                    string xPrefill = "";
        //                    if (oPrefill != null)
        //                        xPrefill = oPrefill.ToString();

        //                    //GLOG 4404: Extra parameter specifying to also insert converted XML in 
        //                    //additional sections
        //                    LMP.Forte.MSWord.WordDoc.ConvertTrailersTo9xFormat(ref oNodesArray,
        //                        oForteDoc.WordDocument.FullName, iLocation9x, xPrefill,
        //                        iInsertionLocation == InsertionLocations.InsertAtStartOfDocument, ref oTags, ref oXMLArray); //GLOG 7876
        //                }
        //                finally
        //                {
        //                    LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.None;
        //                }
        //                LMP.Benchmarks.Print(t1, "ConvertTrailersTo9xFormat");

        //            }
        //            else
        //            {
        //                LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;
        //                DateTime t1 = DateTime.Now;
        //                try
        //                {
        //                    LMP.Forte.MSWord.WordDoc.DeleteTrailerBoundingObjects(
        //                        ref oNodesArray, ref oTags, ref oXMLArray); //GLOG 7876
        //                }
        //                finally
        //                {
        //                    LMP.Architect.Oxml.XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.None;
        //                }
        //                LMP.Benchmarks.Print(t1, "DeleteTrailerBoundingObjects");
        //            }
        //            oSegment = null;
        //        }
        //    }

        //    //GLOG #5925 CEH
        //    //GLOG 6281 (dm) - was erring in trailer
        //    if (bTrackChanges)
        //        oForteDoc.WordDocument.TrackRevisions = true;

        //    if (oSegment != null)
        //    {
        //        //mark as having been initialized with a prefill
        //        oSegment.m_oPrefill = oPrefill;
        //    }

        //    //oUndo.MarkEnd();
        //    LMP.Benchmarks.Print(t0, xSegmentName);

        //    return oSegment;
        //}
        //GLOG item #4734 - dcf
        /// <summary>
        /// returns the default insertion behavior for a segment type
        /// </summary>
        /// <param name="iSegmentType"></param>
        /// <returns></returns>
        public static InsertionBehaviors GetDefaultInsertionBehaviors(LMP.Data.mpObjectTypes iSegmentType)
        {
            switch (iSegmentType)
            {
                case mpObjectTypes.TOA:
                    return InsertionBehaviors.InsertInSeparateNextPageSection | InsertionBehaviors.RestartPageNumbering;
                default:
                    //unspecified
                    return InsertionBehaviors.Default;
            }
        }
        /// <summary>
        /// Populate Array with contents of all mSEG tags matching TagID
        /// </summary>
        /// <param name="xSectionXML"></param>
        /// <param name="xTagID"></param>
        /// <param name="xNodes"></param>
        private static void PopulateTrailerNodeArray(string xSectionXML, string xTagID, ref string[] xNodes)
        {
            try
            {
                string xNSPrefix = LMP.String.GetNamespacePrefix(xSectionXML, ForteConstants.MacPacNamespace);
                xTagID = Regex.Escape(xTagID);
                Regex oRegex = new Regex("<" + xNSPrefix + ":mSEG TagID=\\\"" + xTagID + ".*?</" + xNSPrefix + ":mSEG>");
                MatchCollection oMatches = oRegex.Matches(xSectionXML);
                if (oMatches.Count > 0)
                {
                    xNodes = new string[oMatches.Count];
                    for (int i = 0; i < oMatches.Count; i++)
                    {
                        Match oMatch = oMatches[i];
                        string xTag = oMatch.Value;
                        int iStart = xTag.IndexOf(">") + 1;
                        int iEnd = xTag.IndexOf("</" + xNSPrefix + ":mSEG>");
                        xNodes[i] = xTag.Substring(iStart, iEnd - iStart);
                    }
                }
            }
            catch
            {
                xNodes = null;
            }
        }

        /// <summary>
        /// replaces one segment with another
        /// </summary>
        /// <param name="xNewSegmentID">ID of segment to be inserted</param>
        /// <param name="xOldSegmentID">ID of segment to be replaced</param>
        /// <param name="oOldSegmentParent">parent of segment to be replaced - limits
        /// search of old segment to parent - can be null</param>
        /// <param name="oForteDoc">MacPac document containing segment to be replaced -
        /// will be used to locate old segment if no parent segment is specified</param>
        /// <returns></returns>
        public static void Replace(string xNewSegmentID, string xOldSegmentID,
            XmlSegment oOldSegmentParent, XmlForteDocument oForteDoc)
        {
            Replace(xNewSegmentID, xOldSegmentID, oOldSegmentParent, oForteDoc, null, null, false);
        }
        public static void Replace(string xNewSegmentID, string xOldSegmentID,
            XmlSegment oOldSegmentParent, XmlForteDocument oForteDoc, XmlPrefill oPrefill,
            string xBodyText, bool bDesign)
        {

            //alert if new segment doesn't exist
            ISegmentDef oDef = null;

            try
            {
                oDef = XmlSegment.GetDefFromID(xNewSegmentID);
            }
            catch { }

            if (oDef == null)
                throw new LMP.Exceptions.SegmentDefinitionException(
                    LMP.Resources.GetLangString("Msg_CantReplaceSegmentWithNonExistentSegment") +
                    xNewSegmentID);

            //get domain of segments that we should search to
            //find the specified existing segment
            XmlSegments aDomain = oOldSegmentParent == null ?
                oForteDoc.Segments : oOldSegmentParent.Segments;

            //find all instances of the existing segment in the domain
            XmlSegment[] aOldSegments = XmlSegment.FindSegments(xOldSegmentID, aDomain);

            //cycle through all found instances of the segment,
            //replacing each with an instance of the new segment
            foreach (XmlSegment oSegment in aOldSegments)
            {
                //GLOG 4439 - moved this block into a separate method that can be
                //used to target a specific segment, as opposed to all like
                //segments in the domain
                ReplaceIndividualSegment(oSegment, xNewSegmentID, oDef, oPrefill, xBodyText);
            }
        }

        /// <summary>
        /// updates oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oPrefill"></param>
        /// <param name="xBodyText"></param>
        public static void Update(XmlSegment oSegment, XmlPrefill oPrefill, string xBodyText)
        {
            ReplaceIndividualSegment(oSegment, oSegment.ID, oSegment.Definition, oPrefill, xBodyText);
        }

        //GLOG 15384
        /// <summary>
        /// Returns list of sections which contain the parts of this segment
        /// </summary>
        /// <returns></returns>
        public List<int> GetSectionNumbers()
        {
            List<int> aSections = new List<int>();
            foreach (RawSegmentPart oPart in this.RawSegmentParts)
            {
                int iCCIndex = 0;
                int iStoryType = 0;
                int iSectionIndex = 0;
                string xShapeName = "";
                Query.GetContentControlLocation(this.WPDoc, Query.GetTag(oPart.ContentControl), ref iCCIndex, ref iStoryType, ref iSectionIndex, ref xShapeName);
                if (iSectionIndex > 0 && !aSections.Contains(iSectionIndex))
                {
                    aSections.Add(iSectionIndex);
                }
                aSections.Sort();
            }
            return aSections;
        }
        /// <summary>
        /// replaces oSegment as specified
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xNewSegmentID"></param>
        /// <param name="oNewSegmentDef"></param>
        /// <param name="oPrefill"></param>
        /// <param name="xBodyText"></param>
        /// <param name="bDesign"></param>
        private static void ReplaceIndividualSegment(XmlSegment oSegment, string xNewSegmentID, ISegmentDef oNewSegmentDef, XmlPrefill oPrefill, string xBodyText)
        {
            XmlForteDocument oForteDoc = oSegment.ForteDocument;

            try
            {
                XmlSegment oParent = oSegment.Parent;
                OpenXmlElement oParentElement = null;
                OpenXmlElement oSiblingElement = null;
                bool bUpdateAllStyles = false;

                try
                {
                    oParentElement = oSegment.RawSegmentParts[0].ContentControl.Parent;
                }
                catch { }
                try
                {
                    oSiblingElement = oSegment.RawSegmentParts[0].ContentControl.NextSibling();
                }
                catch { }

                if ((oSegment.TypeID == mpObjectTypes.PleadingSignature && oNewSegmentDef.TypeID == mpObjectTypes.PleadingSignatureNonTable) ||
                    (oSegment.TypeID == mpObjectTypes.LetterSignature && oNewSegmentDef.TypeID == mpObjectTypes.LetterSignatureNonTable))
                {
                    //GLOG 8659: Handle replacing collectiontableitem with non-table signature
                    try
                    {
                        List<string> xmDels = new List<string>();
                        xmDels = DeleteScope.GetChildmDels(oForteDoc.WPDocument, oParent.RawSegmentParts[0].ContentControl, oParent.TagID);
                        if (oParent is XmlCollectionTable && oParent.Segments.Count == 1)
                        {
                            oParentElement = oParent.RawSegmentParts[0].ContentControl.Parent;
                            if (oParentElement is Table)
                            {
                                oSiblingElement = oParentElement.NextSibling<Paragraph>();
                                oParentElement = oSiblingElement.Parent;
                            }
                            else
                            {
                                oSiblingElement = oParent.RawSegmentParts[0].ContentControl.PreviousSibling();
                            }
                            //Delete entire collection table
                            oForteDoc.DeleteSegment(oParent, true, true, false);
                            oParent = oParent.Parent;
                        }
                        else
                        {
                            oForteDoc.DeleteSegment(oSegment, true, true, false);
                            if (oParent is XmlCollectionTable)
                            {
                                oParent = oParent.Parent;
                            }
                        }

                        XmlSegment oNewSegment = InsertXmlSegmentInXmlSegment(xNewSegmentID, oParent, oForteDoc, oParentElement, oSiblingElement, oPrefill, xBodyText, bUpdateAllStyles);

                        DeleteScope.RestoreChildmDels(oForteDoc.WPDocument, oNewSegment.RawSegmentParts[0].ContentControl, xmDels);

                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.SegmentException(
                            LMP.Resources.GetLangString("Error_CouldNotRemoveSegment") +
                            oSegment.ID, oE);
                    }
                }
                else if (oSegment is XmlCollectionTableItem && oSegment.TypeID == oNewSegmentDef.TypeID)
                {

                    //save any non-native mDels in collection tables -
                    List<string> xmDels = new List<string>();
                    xmDels = DeleteScope.GetChildmDels(oForteDoc.WPDocument, oParent.RawSegmentParts[0].ContentControl, oParent.TagID);
                    for (int iPosition = 1; iPosition <= oParent.Segments.Count; iPosition++)
                    {
                        if (oParent.Segments[iPosition - 1] == oSegment)
                        {
                            oForteDoc.DeleteSegment(oSegment, false, false, false);
                            InsertCollectionTableItem(oParent, xNewSegmentID, oPrefill, CollectionTableStructure.ItemInsertionLocations.EndOfTable);
                        }
                    }
                    //restore non-native mDels
                    //GLOG 8697
                    if (xmDels != null)
                    {
                        DeleteScope.RestoreChildmDels(oForteDoc.WPDocument, oParent.RawSegmentParts[0].ContentControl, xmDels);
                    }

                }
                else
                {
                    //GLOG 15834: Get sections used by existing paper,
                    //New Paper will be inserted in these sections only
                    List<int> aSections = oSegment.GetSectionNumbers();
                    if (!(oSegment is XmlCollectionTableItem))
                    {
                        try
                        {
                            oForteDoc.DeleteSegment(oSegment, true, true, false);
                        }
                        catch (System.Exception oE)
                        {
                            throw new LMP.Exceptions.SegmentException(
                                LMP.Resources.GetLangString("Error_CouldNotRemoveSegment") +
                                oSegment.ID, oE);
                        }
                    }
                    //GLOG 15948: Handle Sidebar same as Paper
                    if (oSegment is XmlPaper || oSegment is XmlSidebar)
                    {
                        InsertPaperInXmlSegment(xNewSegmentID, oParent, oPrefill, aSections);
                    }
                    else
                    {
                        //Replace text or paragraph segment
                        InsertXmlSegmentInXmlSegment(xNewSegmentID, oParent, oForteDoc, oParentElement, oSiblingElement, oPrefill, xBodyText, bUpdateAllStyles);
                    }
                }

            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        internal static void InsertCollectionTableItem(XmlSegment oParent, string xNewSegmentID, XmlPrefill oPrefill, CollectionTableStructure.ItemInsertionLocations iLocation)
        {
            XmlForteDocument oForteDoc = oParent.ForteDocument;
            XmlSegment oNewSegment = CreateOxmlSegment(xNewSegmentID, oParent, oForteDoc, oPrefill);
            if (oNewSegment == null)
                return;

            oForteDoc.CopySegmentStyles(oNewSegment, false, true, true);
            List<RawSegmentPart> oParts = oNewSegment.RawSegmentParts;
            XmlSegment oExisting = null;
            OpenXmlElement oTarget = null;
            int iChildCount = oParent.Segments.Count;
            XmlCollectionTableItem oCTI = (XmlCollectionTableItem)oNewSegment;
            bool bSideBySide = oCTI.AllowSideBySide;
            if (oNewSegment.RawSegmentParts.Count() > 1)
                bSideBySide = false;
            if (bSideBySide && iChildCount > 0)
            {
                //If any existing items don't allow side-by-side layout ignore that property
                for (int i = 0; i < oParent.Segments.Count; i++)
                {
                    if (Query.GetmSEGObjectDataValue(Query.GetAttributeValue(oForteDoc.WPDocument, oParent.Segments[i].RawSegmentParts[0].ContentControl, "ObjectData"), "AllowSideBySide").ToUpper() != "TRUE")
                    {
                        bSideBySide = false;
                        iLocation = iLocation | CollectionTableStructure.ItemInsertionLocations.EndOfTable;
                        break;
                    }
                }
            }
            TableRow oEmptyRow = null;
            TableCell oExistingCell = null;
            TableCell oInsertCell = null;
            if (iChildCount > 0)
            {
                oExisting = oParent.Segments[iChildCount - 1];
                //GLOG 8486: If previous item has more than one Content Control, insert new item after last
                if (oExisting.RawSegmentParts[0].ContentControl is SdtRow)
                    oTarget = oExisting.RawSegmentParts.Last<RawSegmentPart>().ContentControl;
                else
                    oTarget = oExisting.RawSegmentParts.Last<RawSegmentPart>().ContentControl.Ancestors<TableRow>().FirstOrDefault<TableRow>();

            }
            else
            {
                //GLOG 8486: If Collection Table Content Control is SdtRow or SdtCell, it will have a Table Ancestor,
                //otherwise, the Content Control will contain the Table (occurs when Segment design contains multi-row Collection Item
                if (!(oParent.RawSegmentParts[0].ContentControl is SdtBlock))
                    oEmptyRow = oParent.RawSegmentParts[0].ContentControl.Ancestors<Table>().First<Table>().Descendants<TableRow>().First<TableRow>();
                else
                    oEmptyRow = oParent.RawSegmentParts[0].ContentControl.Descendants<TableRow>().First<TableRow>();
                oTarget = oEmptyRow;
            }
            if (bSideBySide)
            {
                if (oExisting != null && (iLocation & CollectionTableStructure.ItemInsertionLocations.EndOfTable) != CollectionTableStructure.ItemInsertionLocations.EndOfTable)
                {
                    oExistingCell = GetCollectionItemCell(oExisting);
                    if (oExistingCell.PreviousSibling<TableCell>() == null)
                    {
                        if ((iLocation & CollectionTableStructure.ItemInsertionLocations.LeftCell) == CollectionTableStructure.ItemInsertionLocations.LeftCell)
                        {
                            //If Target Cell is already in use, append to end of table instead
                            iLocation = iLocation | CollectionTableStructure.ItemInsertionLocations.EndOfTable;
                        }
                        else
                        {
                            TableCell oNextCell = oExistingCell.NextSibling<TableCell>();
                            if (oNextCell.InnerText != "")
                            {
                                //Both cells are already occupied - append to table
                                iLocation = iLocation | CollectionTableStructure.ItemInsertionLocations.EndOfTable;
                            }
                        }
                    }
                    else
                    {
                        if ((iLocation & CollectionTableStructure.ItemInsertionLocations.RightCell) == CollectionTableStructure.ItemInsertionLocations.RightCell)
                        {
                            //If Target Cell is already in use, append to end of table instead
                            iLocation = iLocation | CollectionTableStructure.ItemInsertionLocations.EndOfTable;
                        }
                        else
                        {
                            TableCell oPrevCell = oExistingCell.PreviousSibling<TableCell>();
                            if (oPrevCell.InnerText != "")
                            {
                                //Both cells are already occupied - append to table
                                iLocation = iLocation | CollectionTableStructure.ItemInsertionLocations.EndOfTable;
                            }
                        }
                    }
                }
                oInsertCell = GetCollectionItemCell(oNewSegment);
                OpenXmlElement oTargetRow = null;
                if (oTarget is TableRow)
                    oTargetRow = oTarget;
                else
                    oTargetRow = oTarget.Descendants<TableRow>().First();
                if ((iLocation & CollectionTableStructure.ItemInsertionLocations.EndOfTable) == CollectionTableStructure.ItemInsertionLocations.EndOfTable)
                {
                    //Append new empty row for item
                    OpenXmlElement oNewRow = oTarget.Parent.InsertAfter(oTargetRow.CloneNode(true), oTarget);
                    TableCell[] oCells = oNewRow.Descendants<TableCell>().ToArray<TableCell>();
                    foreach (TableCell oCell in oCells)
                    {
                        oCell.RemoveAllChildren<SdtElement>();
                        oCell.RemoveAllChildren<Paragraph>();
                        oCell.RemoveAllChildren<Run>();
                        oCell.AppendChild<Paragraph>(new Paragraph());
                    }
                    oTargetRow = oNewRow;
                }
                int iNewIndex = 0;
                if ((iLocation & CollectionTableStructure.ItemInsertionLocations.RightCell) == CollectionTableStructure.ItemInsertionLocations.RightCell)
                {
                    iNewIndex = 1;
                }
                TableCell oTargetCell = oTargetRow.Descendants<TableCell>().ToArray<TableCell>()[iNewIndex];
                if (oInsertCell.Descendants<SdtElement>().Contains<SdtElement>(oNewSegment.RawSegmentParts[0].ContentControl))
                {
                    //New Segment Content Control is within Cell - append copy
                    SdtBlock oNewCC = new SdtBlock(oNewSegment.RawSegmentParts[0].ContentControl.OuterXml);
                    Paragraph oLastPara = oTargetCell.Descendants<Paragraph>().Last();
                    if (oLastPara != null)
                    {
                        oTargetCell.InsertBefore(oNewCC, oLastPara);
                        oLastPara.Remove();
                    }
                    else
                    {
                        oTargetCell.AppendChild(oNewCC);
                    }
                }
                else
                {
                    //Original CC is sdtRow or sdtCell - create new SdtBlock with same tag and clone cell contents
                    TableCell oCellClone = (TableCell)oInsertCell.CloneNode(true);
                    oCellClone.RemoveAllChildren<TableCellProperties>();
                    SdtContentBlock oNewBlock = new SdtContentBlock();
                    oNewBlock.InnerXml = oCellClone.InnerXml;
                    SdtBlock oNewCC = new SdtBlock(new SdtProperties(new Tag() { Val = Query.GetTag(oNewSegment.RawSegmentParts[0].ContentControl) }, new ShowingPlaceholder()), oNewBlock);
                    oNewCC.Descendants<SdtContentBlock>().First().RemoveAllChildren<TableCellProperties>();
                    Paragraph oLastPara = oTargetCell.Descendants<Paragraph>().Last();
                    if (oLastPara != null)
                    {
                        oTargetCell.InsertBefore(oNewCC, oLastPara);
                        oLastPara.Remove();
                    }
                    else
                    {
                        oTargetCell.AppendChild(oNewCC);
                    }
                }
            }
            else
            {
                //Insert new rows
                for (int i = 0; i < oNewSegment.RawSegmentParts.Count(); i++)
                {
                    oTarget = oTarget.Parent.InsertAfter(oNewSegment.RawSegmentParts[i].ContentControl.CloneNode(true), oTarget);
                }
                if (oEmptyRow != null)
                {
                    //Remove original empty row
                    oEmptyRow.Remove();
                }
            }

            Query.CopyDocumentVariables(oNewSegment.WPDoc, oForteDoc.WPDocument);
            //Refresh Parent Collection
            oParent.GetChildSegments();
        }
        internal static TableCell GetCollectionItemCell(XmlSegment oSegment)
        {
            SdtElement oCC = oSegment.RawSegmentParts[0].ContentControl;
            TableRow oRow = null;
            if (oCC is SdtRow)
            {
                //Row is child of SdtRow
                oRow = oCC.Descendants<TableRow>().First();
                TableCell[] oCells = oRow.Descendants<TableCell>().ToArray<TableCell>();
                foreach (TableCell oCell in oCells)
                {
                    if (oCell.InnerText != "")
                    {
                        return oCell;
                    }
                }
            }
            else if (oCC is SdtCell)
            {
                return oCC.Descendants<TableCell>().FirstOrDefault();
            }
            else
            {
                return oCC.Ancestors<TableCell>().FirstOrDefault();
            }
            return null;
        }
        internal static XmlSegment InsertXmlSegmentInXmlSegment(string xNewSegmentID, XmlSegment oParent, XmlForteDocument oForteDoc, OpenXmlElement oParentElement,
            OpenXmlElement oSiblingElement, XmlPrefill oPrefill, string xBodyText, bool bUpdateAllStyles)
        {
            XmlSegment oNewSegment = CreateOxmlSegment(xNewSegmentID, oParent, oForteDoc, oPrefill);
            if (oNewSegment == null)
                return null;
            oForteDoc.CopySegmentStyles(oNewSegment, bUpdateAllStyles, true, true);
            RawSegmentPart oPart = oNewSegment.RawSegmentParts[0];
            SdtElement oNewCC = (SdtElement)oPart.ContentControl.Clone();
            if (oSiblingElement != null)
            {
                oParentElement.InsertBefore<SdtElement>(oNewCC, oSiblingElement);
            }
            else
            {
                oParentElement.PrependChild<SdtElement>(oNewCC);
            }
            CopyImagePartsFromSource(oNewSegment.WPDoc.MainDocumentPart, oForteDoc.WPDocument.MainDocumentPart, oForteDoc.WPDocument);
            Query.CopyDocumentVariables(oNewSegment.WPDoc, oForteDoc.WPDocument);
            oParent.GetChildSegments();
            return oNewSegment;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xNewSegmentID"></param>
        /// <param name="oParent"></param>
        /// <param name="oPrefill"></param>
        /// <param name="xBodyText"></param>
        /// <param name="bDesign"></param>
        internal static XmlSegment InsertPaperInXmlSegment(string xNewSegmentID, XmlSegment oParent, XmlPrefill oPrefill, List<int> aSections)
        {
            XmlForteDocument oForteDoc = oParent.ForteDocument;
            XmlSegment oNewSegment = CreateOxmlSegment(xNewSegmentID, oParent, oForteDoc, oPrefill);
            if (oNewSegment == null)
                return null;
            //GLOG 8528: Make sure any styles in use in inserted segment exist in the target
            oForteDoc.CopySegmentStyles(oNewSegment, false, true, true);
            List<RawSegmentPart> oParts = oNewSegment.RawSegmentParts;
            ArrayList aImageIds = new ArrayList();
            ArrayList aCompleteParts = new ArrayList();
            if (aSections == null)
            {
                aSections = new List<int>() { 1 };
            }
            //GLOG 15951
            bool bRefreshParentParts = false;
            foreach (RawSegmentPart oPart in oParts)
            {
                OpenXmlElement omSeg = oPart.ContentControl;
                OpenXmlElement oPict = omSeg.Ancestors<Picture>().FirstOrDefault<Picture>();

                if (oPict != null && omSeg.NextSibling<Paragraph>() == null && omSeg.PreviousSibling<Paragraph>() == null)
                {
                    //If containing Content Control is only content of Textbox, include the Textbox as part of insertion
                    omSeg = oPict;
                }
                Header oHeader = omSeg.Ancestors<Header>().FirstOrDefault<Header>();
                Footer oFooter = omSeg.Ancestors<Footer>().FirstOrDefault<Footer>();

                //If mSEG is SdtRun, need to insert containing paragraph so that XML structure
                //will still be valid when Content Controls are removed
                if (omSeg is SdtRun && omSeg.Ancestors<Paragraph>().FirstOrDefault() != null)
                {
                    omSeg = omSeg.Ancestors<Paragraph>().FirstOrDefault();
                }
                if (oHeader != null)
                {
                    HeaderPart oSourcePart = oHeader.HeaderPart;

                    oPart.SetSegmentPropertyValue("ParentTagID", oParent.TagID, false); //GLOG 15948
                    HeaderFooterValues oHeaderType = Query.GetHeaderFooterType(oHeader.HeaderPart, oNewSegment.WPDoc);
                    List<SectionProperties> oSecProps = oParent.WPDoc.MainDocumentPart.Document.Descendants<SectionProperties>().ToList();
                    //GLOG 15384: Insert in each section of Parent segment
                    foreach (int iSection in aSections)
                    {
                        if (iSection < 1 || iSection > oSecProps.Count())
                        {
                            continue;
                        }
                        SectionProperties oSecPr = oSecProps[iSection - 1];
                        HeaderPart oHeaderPart = Query.GetHeaderPartByType(oHeaderType, iSection, oParent.WPDoc);
                        if (iSection > 1)
                        {
                            List<SdtElement> oCCs = oSourcePart.Header.Descendants<SdtElement>().ToList();
                            oCCs.Reverse();
                            foreach (SdtElement oCC in oCCs)
                            {
                                Query.ReTagContentControl(oCC, oNewSegment.WPDoc, true);
                            }
                        }
                        if (oHeaderPart == null)
                        {
                            oHeaderPart = oParent.WPDoc.MainDocumentPart.AddNewPart<HeaderPart>();
                            oHeaderPart.Header = new Header(oSourcePart.Header.OuterXml);
                            string xPartId = oParent.WPDoc.MainDocumentPart.GetIdOfPart(oHeaderPart);
                            HeaderReference oRef = new HeaderReference() { Id = xPartId, Type = oHeaderType };
                            oSecPr.Append(oRef);
                            aCompleteParts.Add(xPartId);
                        }
                        if (oHeaderPart != null)
                        {
                            if (!aCompleteParts.Contains(oParent.WPDoc.MainDocumentPart.GetIdOfPart(oHeaderPart)))
                            {
                                Header oTargetHeader = oHeaderPart.Header;
                                //If Target Header is completely empty, replace entirely with InnerXML of Source Header
                                if (oTargetHeader.InnerText == "" && oTargetHeader.Descendants<Picture>().Count() == 0 && oTargetHeader.Descendants<SdtElement>().Count() == 0) //GLOG 8838
                                {
                                    oTargetHeader.InnerXml = oSourcePart.Header.InnerXml;
                                    aCompleteParts.Add(oParent.WPDoc.MainDocumentPart.GetIdOfPart(oHeaderPart));
                                }
                                else
                                {
                                    //Append current Content Control to existing header
                                    if (omSeg is Picture)
                                    {
                                        //GLOG 15755: If textbox CC is contained in another RawSegmentPart for same segment, don't need to reinsert
                                        bool bSkipCC = false;
                                        foreach (RawSegmentPart oOtherPart in oParts)
                                        {
                                            if (oOtherPart != oPart)
                                            {
                                                if (oOtherPart.ContentControl.Descendants<Picture>().Contains(omSeg))
                                                {

                                                    bSkipCC = true;
                                                    break;
                                                }
                                                else if (oOtherPart.ContentControl is SdtRun && oOtherPart.ContentControl.Ancestors<Paragraph>().FirstOrDefault() != null)
                                                {
                                                    if (oOtherPart.ContentControl.Ancestors<Paragraph>().FirstOrDefault().Descendants<Picture>().Contains(omSeg))
                                                    {
                                                        bSkipCC = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (bSkipCC)
                                            continue;
                                        //GLOG 8519: Include Anchor paragraph with Textbox
                                        Paragraph oPara = omSeg.Ancestors<Paragraph>().FirstOrDefault();
                                        if (oPara != null)
                                        {
                                            //GLOG 8838: If source anchor paragraph is empty, don't insert extra paragraph
                                            Paragraph oLastPara = null;
                                            string xParaText = "";
                                            if (oTargetHeader.Elements<Paragraph>().Count() == 1)
                                            {
                                                oLastPara = oTargetHeader.Elements<Paragraph>().First();
                                            }
                                            else if (oTargetHeader.Elements<SdtBlock>().Count() == 1)
                                            {
                                                oLastPara = oTargetHeader.Elements<SdtBlock>().First().Descendants<Paragraph>().LastOrDefault();
                                            }
                                            try
                                            {
                                                xParaText = oPara.Elements<Run>().First().Elements<Text>().First().InnerText;
                                            }
                                            catch { }
                                            if (xParaText == "" && oLastPara != null &&  oLastPara.Ancestors<Picture>().Count() == 0 && oLastPara.Descendants<Picture>().Count() == 0)
                                            {
                                                Run oRun = new Run();
                                                oRun.AppendChild(omSeg.CloneNode(true));
                                                oLastPara.AppendChild(oRun);
                                            }
                                            else
                                            {
                                                OpenXmlElement oNewPara = oPara.CloneNode(true);
                                                oNewPara.RemoveAllChildren<Run>();
                                                Run oRun = new Run();
                                                oRun.AppendChild(omSeg.CloneNode(true));
                                                oNewPara.AppendChild(oRun);
                                                oTargetHeader.InnerXml = oTargetHeader.InnerXml + oNewPara.OuterXml;
                                            }
                                        }
                                        else
                                        {
                                            oTargetHeader.InnerXml = oTargetHeader.InnerXml + omSeg.OuterXml;
                                        }
                                    }
                                    else
                                    {
                                        oTargetHeader.InnerXml = oTargetHeader.InnerXml + omSeg.OuterXml;
                                    }
                                }
                                oTargetHeader.Save();
                            }
                            CopyImagePartsFromSource(oSourcePart, oHeaderPart, oParent.WPDoc);
                        }
                        //GLOG 15951
                        bRefreshParentParts = true;
                    }
                }
                else if (oFooter != null)
                {
                    FooterPart oSourcePart = oFooter.FooterPart;

                    oPart.SetSegmentPropertyValue("ParentTagID", oParent.TagID, false); //GLOG 15948
                    HeaderFooterValues oFooterType = Query.GetHeaderFooterType(oFooter.FooterPart, oNewSegment.WPDoc);
                    List<SectionProperties> oSecProps = oParent.WPDoc.MainDocumentPart.Document.Descendants<SectionProperties>().ToList();
                    //GLOG 15384: Insert in each section of Parent segment
                    foreach (int iSection in aSections)
                    {
                        if (iSection < 1 || iSection > oSecProps.Count())
                        {
                            continue;
                        }
                        SectionProperties oSecPr = oSecProps[iSection - 1];
                        FooterPart oFooterPart = Query.GetFooterPartByType(oFooterType, iSection, oParent.WPDoc);
                        if (iSection > 1)
                        {
                            List<SdtElement> oCCs = oSourcePart.Footer.Descendants<SdtElement>().ToList();
                            oCCs.Reverse();
                            foreach (SdtElement oCC in oCCs)
                            {
                                Query.ReTagContentControl(oCC, oNewSegment.WPDoc, true);
                            }
                        }
                        if (oFooterPart == null)
                        {
                            oFooterPart = oParent.WPDoc.MainDocumentPart.AddNewPart<FooterPart>();
                            oFooterPart.Footer = new Footer(oSourcePart.Footer.OuterXml);
                            string xPartId = oParent.WPDoc.MainDocumentPart.GetIdOfPart(oFooterPart);
                            FooterReference oRef = new FooterReference() { Id = xPartId, Type = oFooterType };
                            oSecPr.Append(oRef);
                            aCompleteParts.Add(xPartId);
                        }
                        if (oFooterPart != null)
                        {
                            if (!aCompleteParts.Contains(oParent.WPDoc.MainDocumentPart.GetIdOfPart(oFooterPart)))
                            {
                                Footer oTargetFooter = oFooterPart.Footer;
                                //If Target Footer is completely empty, replace entirely with InnerXML of Source Footer
                                if (oTargetFooter.InnerText == "" && oTargetFooter.Descendants<Picture>().Count() == 0 && oTargetFooter.Descendants<SdtElement>().Count() == 0) //GLOG 8838
                                {
                                    oTargetFooter.InnerXml = oSourcePart.Footer.InnerXml;
                                    aCompleteParts.Add(oParent.WPDoc.MainDocumentPart.GetIdOfPart(oFooterPart));
                                }
                                else
                                {
                                    //Append current Content Control to existing Footer
                                    if (omSeg is Picture)
                                    {
                                        //GLOG 15755: If textbox CC is contained in another RawSegmentPart for same segment, don't need to reinsert
                                        bool bSkipCC = false;
                                        foreach (RawSegmentPart oOtherPart in oParts)
                                        {
                                            if (oOtherPart != oPart)
                                            {
                                                if (oOtherPart.ContentControl.Descendants<Picture>().Contains(omSeg))
                                                {

                                                    bSkipCC = true;
                                                    break;
                                                }
                                                else if (oOtherPart.ContentControl is SdtRun && oOtherPart.ContentControl.Ancestors<Paragraph>().FirstOrDefault() != null)
                                                {
                                                    if (oOtherPart.ContentControl.Ancestors<Paragraph>().FirstOrDefault().Descendants<Picture>().Contains(omSeg))
                                                    {
                                                        bSkipCC = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (bSkipCC)
                                            continue;
                                        //GLOG 8519: Include Anchor paragraph with Textbox
                                        Paragraph oPara = omSeg.Ancestors<Paragraph>().FirstOrDefault();
                                        if (oPara != null)
                                        {
                                            //GLOG 8838: If source anchor paragraph is empty, don't insert extra paragraph
                                            Paragraph oLastPara = null;
                                            string xParaText = "";
                                            if (oTargetFooter.Elements<Paragraph>().Count() == 1)
                                            {
                                                oLastPara = oTargetFooter.Elements<Paragraph>().First();
                                            }
                                            else if (oTargetFooter.Elements<SdtBlock>().Count() == 1)
                                            {
                                                oLastPara = oTargetFooter.Elements<SdtBlock>().First().Descendants<Paragraph>().LastOrDefault();
                                            }
                                            try
                                            {
                                                xParaText = oPara.Elements<Run>().First().Elements<Text>().First().InnerText;
                                            }
                                            catch { }
                                            if (xParaText == "" && oLastPara != null && oLastPara.Ancestors<Picture>().Count() == 0 && oLastPara.Descendants<Picture>().Count() == 0)
                                            {
                                                Run oRun = new Run();
                                                oRun.AppendChild(omSeg.CloneNode(true));
                                                oLastPara.AppendChild(oRun);
                                            }
                                            else
                                            {
                                                OpenXmlElement oNewPara = oPara.CloneNode(true);
                                                oNewPara.RemoveAllChildren<Run>();
                                                Run oRun = new Run();
                                                oRun.AppendChild(omSeg.CloneNode(true));
                                                oNewPara.AppendChild(oRun);
                                                oTargetFooter.InnerXml = oTargetFooter.InnerXml + oNewPara.OuterXml;
                                            }
                                        }
                                        else
                                        {
                                            oTargetFooter.InnerXml = oTargetFooter.InnerXml + omSeg.OuterXml;
                                        }

                                    }
                                    else
                                    {
                                        oTargetFooter.InnerXml = oTargetFooter.InnerXml + omSeg.OuterXml;
                                    }
                                }
                                oTargetFooter.Save();
                            }
                            CopyImagePartsFromSource(oSourcePart, oFooterPart, oParent.WPDoc);
                        }
                        //GLOG 15951
                        bRefreshParentParts = true;
                    }
                }
            }
            Query.CopyDocumentVariables(oNewSegment.WPDoc, oForteDoc.WPDocument);
            if (oNewSegment is XmlPaper)
                XmlSegment.CopyPageSetup(oParent, oNewSegment, true);
            if (bRefreshParentParts)
            {
                //GLOG 15951: Need to requery RawSegmentParts, since setting InnerXml will replace the original Content Controls
                oParent.RawSegmentParts = Query.GetTopLevelRawSegmentParts(oParent.WPDoc, oParent.ID);
            }
            oParent.GetChildSegments();
            //oParent.Segments.Add(oNewSegment);
            return oNewSegment;
        }
        public static void CopyImagePartsFromSource(OpenXmlPart oSourcePart, OpenXmlPart oTargetPart, WordprocessingDocument oWPDoc)
        {
            MainDocumentPart oMain = oSourcePart as MainDocumentPart;
            HeaderPart oHeader = oSourcePart as HeaderPart;
            FooterPart oFooter = oSourcePart as FooterPart;
            ArrayList aImageIds = new ArrayList();
            if (oMain != null)
            {
                MainDocumentPart oMainTarget = (MainDocumentPart)oTargetPart;
                foreach (ImagePart oImagePart in oMain.ImageParts)
                {
                    string xRelId = oMain.GetIdOfPart(oImagePart);

                    ImagePart oIP = oMainTarget.ImageParts.Where(i => oMainTarget.GetIdOfPart(i) == xRelId).FirstOrDefault();
                    if (oIP == null)
                    {
                        //ImagePart already added, just add relationship
                        if (aImageIds.Contains(xRelId))
                        {
                            oMainTarget.AddImagePart(oImagePart.ContentType, xRelId);
                        }
                        else
                        {
                            oMainTarget.AddPart<ImagePart>(oImagePart, xRelId);
                            aImageIds.Add(xRelId);
                        }
                    }
                }

            }
            else if (oHeader != null)
            {
                HeaderPart oHeaderTarget = (HeaderPart)oTargetPart;
                foreach (ImagePart oImagePart in oHeader.ImageParts)
                {
                    string xRelId = oHeader.GetIdOfPart(oImagePart);

                    ImagePart oIP = oHeaderTarget.ImageParts.Where(i => oHeaderTarget.GetIdOfPart(i) == xRelId).FirstOrDefault();
                    if (oIP == null)
                    {
                        //ImagePart already added, just add relationship
                        if (aImageIds.Contains(xRelId))
                        {
                            oHeaderTarget.AddImagePart(oImagePart.ContentType, xRelId);
                        }
                        else
                        {
                            oHeaderTarget.AddPart<ImagePart>(oImagePart, xRelId);
                            aImageIds.Add(xRelId);
                        }
                    }
                }
                oHeader.Header.Save();
            }
            else if (oFooter != null)
            {
                FooterPart oFooterTarget = (FooterPart)oTargetPart;
                foreach (ImagePart oImagePart in oFooter.ImageParts)
                {
                    string xRelId = oFooter.GetIdOfPart(oImagePart);

                    ImagePart oIP = oFooterTarget.ImageParts.Where(i => oFooterTarget.GetIdOfPart(i) == xRelId).FirstOrDefault();
                    if (oIP == null)
                    {
                        //ImagePart already added, just add relationship
                        if (aImageIds.Contains(xRelId))
                        {
                            oFooterTarget.AddImagePart(oImagePart.ContentType, xRelId);
                        }
                        else
                        {
                            oFooterTarget.AddPart<ImagePart>(oImagePart, xRelId);
                            aImageIds.Add(xRelId);
                        }
                    }
                }
                oFooter.Footer.Save();
            }
            else
                return;

        }
        /// <summary>
        /// Generate random Relationship IDs for images to avoid conflicts with existing images
        /// </summary>
        /// <param name="oDoc"></param>
        internal static void UpdateImageRelationshipIds(WordprocessingDocument oDoc)
        {
            DateTime t0 = DateTime.Now;
            Random oRand = new Random();
            ArrayList aNewIDs = new ArrayList();

            ImagePart[] aImages = oDoc.MainDocumentPart.ImageParts.ToArray();
            for (int i = 0; i < aImages.Count(); i++)
            {
                if (!aNewIDs.Contains(oDoc.MainDocumentPart.GetIdOfPart(aImages[i])))
                {
                    string xRelId = "rId" + oRand.Next(10000000, 99999999).ToString();
                    string xOldID = oDoc.MainDocumentPart.GetIdOfPart(aImages[i]);
                    oDoc.MainDocumentPart.ChangeIdOfPart(aImages[i], xRelId);
                    //Replace embed IDs in Main body
                    oDoc.MainDocumentPart.RootElement.InnerXml = oDoc.MainDocumentPart.RootElement.InnerXml.Replace("\"" + xOldID + "\"", "\"" + xRelId + "\"");
                    oDoc.MainDocumentPart.RootElement.Save();
                    aNewIDs.Add(xRelId);
                }
            }
            foreach (HeaderPart oHP in oDoc.MainDocumentPart.HeaderParts)
            {
                aImages = oHP.ImageParts.ToArray();
                for (int i = 0; i < aImages.Count(); i++)
                {
                    if (!aNewIDs.Contains(oHP.GetIdOfPart(aImages[i])))
                    {
                        string xRelId = "rId" + oRand.Next(10000000, 99999999).ToString();
                        string xOldID = oHP.GetIdOfPart(aImages[i]);
                        oHP.ChangeIdOfPart(aImages[i], xRelId);
                        //Replace embed IDs in Header
                        oHP.Header.InnerXml = oHP.Header.InnerXml.Replace("\"" + xOldID + "\"", "\"" + xRelId + "\"");
                        oHP.Header.Save();
                        aNewIDs.Add(xRelId);
                    }
                }
            }
            foreach (FooterPart oFP in oDoc.MainDocumentPart.FooterParts)
            {
                aImages = oFP.ImageParts.ToArray();
                for (int i = 0; i < aImages.Count(); i++)
                {
                    if (!aNewIDs.Contains(oFP.GetIdOfPart(aImages[i])))
                    {
                        string xRelId = "rId" + oRand.Next(10000000, 99999999).ToString();
                        string xOldID = oFP.GetIdOfPart(aImages[i]);
                        oFP.ChangeIdOfPart(aImages[i], xRelId);
                        //Replace embed IDs in Footer
                        oFP.Footer.InnerXml = oFP.Footer.InnerXml.Replace("\"" + xOldID + "\"", "\"" + xRelId + "\"");
                        oFP.Footer.Save();
                        aNewIDs.Add(xRelId);
                    }
                }
            }
            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// Copy PageSetup values from SectionProperties of oParent to oTarget
        /// </summary>
        /// <param name="oTarget"></param>
        /// <param name="oParent"></param>
        /// <param name="bIncludeFirst"></param>
        internal static void CopyPageSetup(XmlSegment oTarget, XmlSegment oSource, bool bIncludeFirst)
        {
            //TODO:  Deal with multiple sections in target doc
            SectionProperties oTargetPr = oTarget.WPDoc.MainDocumentPart.Document.Body.Descendants<SectionProperties>().FirstOrDefault();
            SectionProperties oSourcePr = oSource.WPDoc.MainDocumentPart.Document.Body.Descendants<SectionProperties>().FirstOrDefault();
            if (oTargetPr != null && oSourcePr != null)
            {
                Type[] oTypes = new Type[] { 
                        typeof(PageSize), 
                        typeof(PageMargin), 
                        typeof(TitlePage), //Different first page
                        typeof(PaperSource), 
                        typeof(LineNumberType),
                        typeof(PageBorders), 
                        typeof(PageNumberType), 
                        typeof(VerticalAlignmentValues),
                        typeof(Columns)};

                foreach (Type oType in oTypes)
                {
                    CopySectionPropertiesValues(oTargetPr, oSourcePr, oType);
                }
            }
        }
        /// <summary>
        /// Copy values for SectionProperties child of specified type from Source to Target
        /// </summary>
        /// <param name="oTargetPr"></param>
        /// <param name="oSourcePr"></param>
        /// <param name="oType"></param>
        internal static void CopySectionPropertiesValues(SectionProperties oTargetPr, SectionProperties oSourcePr, System.Type oType)
        {
            OpenXmlElement oChild = oSourcePr.Descendants().Where(c => c.GetType().Name == oType.Name).FirstOrDefault();

            if (oChild != null)
            {
                OpenXmlElement oTargetChild = oTargetPr.Descendants().Where(c => c.GetType().Name == oType.Name).FirstOrDefault();
                if (oTargetChild != null)
                {
                    oTargetPr.InnerXml = oTargetPr.InnerXml.Replace(oTargetChild.OuterXml, oChild.OuterXml);

                }
                else
                {
                    //Append new element
                    OpenXmlElement oNew = (OpenXmlElement)oChild.Clone();
                    oTargetPr.AppendChild(oNew);
                }
            }
            else
            {
                //If Child doesn't exist in Source, also delete from Target
                OpenXmlElement oTargetChild = oTargetPr.Descendants().Where(c => c.GetType().Name == oType.Name).FirstOrDefault();
                if (oTargetChild != null)
                {
                    oTargetChild.Remove();
                }
            }

        }
        /// <summary>
        /// returns the segment with the specified tag id in the specified document
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="oForteDoc"></param>
        /// <param name="oParent"></param>
        /// <returns></returns>
        internal static XmlSegment GetSegment(string xTagID, XmlForteDocument oForteDoc)
        {
            return GetSegment(xTagID, oForteDoc, true, true);
        }
        internal static XmlSegment GetSegment(string xTagID, XmlForteDocument oForteDoc, bool bExecuteActions, bool bPromptForMissingAuthors)
        {
            try
            {
                //get segment id from segment node
                string xTypeID = null;
                mpObjectTypes iTypeID = 0;

                ////get node store containing only those
                ////nodes for the specified tag id
                //NodeStore oNodes = new NodeStore(oForteDoc.Tags, xTagID,
                //    mpNodeStoreMatchTypes.Equals);

                //try
                //{
                //    //get segment type ID from mSEG
                //    xTypeID = oNodes.GetItemObjectDataValue(xTagID, "ObjectTypeID");
                //    iTypeID = (mpObjectTypes)Int32.Parse(xTypeID);
                //}
                //catch (System.Exception oE)
                //{
                //    throw new LMP.Exceptions.SegmentDefinitionException(
                //        LMP.Resources.GetLangString("Error_InvalidObjectTypeID" + xTypeID), oE);
                //}

                //create segment object
                XmlSegment oSegment = XmlSegment.CreateSegmentObject(iTypeID, oForteDoc);

                oSegment.Initialize(xTagID);

                oSegment.PromptForMissingAuthors = bPromptForMissingAuthors;

                if (oSegment.ForteDocument.Mode != XmlForteDocument.Modes.Design &&
                    (oSegment.Parent == null || !oSegment.LinkAuthorsToParent))
                {
                    oSegment.m_bAllowUpdateForAuthor = false;
                    oSegment.SetAuthorsFromDocument(bExecuteActions);
                    oSegment.m_bAllowUpdateForAuthor = true;
                }

                //the segment has already been created in the document
                //- we're just retrieving the code construct
                oSegment.CreationStatus = Status.Finished;
                return oSegment;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotGetSegment") + xTagID, oE);
            }
        }
        /// <summary>
        /// returns the tag ID of the nearest common ancestor of the segments with the
        /// specified tag IDs - if the specified tag ids are the same, returns the same ID -
        /// if no common ancestor is found, returns null
        /// </summary>
        /// <param name="xSegment1TagID"></param>
        /// <param name="xSegment2TagID"></param>
        /// <returns></returns>
        public static string GetNearestCommonAncestorTagID(string xSegment1TagID,
            string xSegment2TagID)
        {
            string xCommon = null;
            string xAncestor1 = null;
            string xAncestor2 = null;

            //if specified IDs are the same, return the same ID
            if (xSegment1TagID == xSegment2TagID)
                return xSegment1TagID;

            //get each segment's top level ancestor 
            int iPos1 = xSegment1TagID.IndexOf('.');
            int iPos2 = xSegment2TagID.IndexOf('.');
            if (iPos1 == -1)
                xAncestor1 = xSegment1TagID;
            else
                xAncestor1 = xSegment1TagID.Substring(0, iPos1);
            if (iPos2 == -1)
                xAncestor2 = xSegment2TagID;
            else
                xAncestor2 = xSegment2TagID.Substring(0, iPos2);

            //cycle through each successive generation
            //until there's no longer a common ancestor
            while (xAncestor1 == xAncestor2)
            {
                xCommon = xAncestor1;
                if (iPos1 != -1)
                    iPos1 = xSegment1TagID.IndexOf('.', iPos1 + 1);
                if (iPos2 != -1)
                    iPos2 = xSegment2TagID.IndexOf('.', iPos2 + 1);
                if ((iPos1 == -1) || (iPos2 == -1))
                    break;
                xAncestor1 = xSegment1TagID.Substring(0, iPos1);
                xAncestor2 = xSegment2TagID.Substring(0, iPos2);
            }

            //return common ancestor
            return xCommon;
        }

        /// <summary>
        /// returns all segments that start in oSection
        /// </summary>
        /// <param name="oForteDoc"></param>
        /// <param name="oSection"></param>
        /// <returns></returns>
        public static XmlSegments GetSegments(XmlForteDocument oForteDoc)
        {
            DateTime t0 = DateTime.Now;

            //create new collection
            XmlSegments oSegments = new XmlSegments(oForteDoc);

            if (oForteDoc.WPDocument != null)
            {
                List<RawSegmentPart> oSegParts = Query.GetTopLevelRawSegmentParts(oForteDoc.WPDocument, null);

                //cycle through child segment nodes, adding each as a child segment
                foreach (RawSegmentPart oSegPart in oSegParts)
                {
                    string xTagID = oSegPart.TagID;

                    bool bAlreadyAdded = false;

                    for (int i = 0; i < oSegments.Count; i++)
                    {
                        XmlSegment oSeg = oSegments[i];

                        if (oSeg.TagID == xTagID)
                        {
                            bAlreadyAdded = true;
                            break;
                        }
                    }

                    //TODO: ensure that no child segment parts are included
                    if (!bAlreadyAdded)
                    {
                        //add
                        string xTypeID = null;
                        mpObjectTypes iTypeID = 0;

                        try
                        {
                            //get segment ID from mSEG
                            xTypeID = oSegPart.GetSegmentPropertyValue("ObjectTypeID");
                            iTypeID = (mpObjectTypes)Int32.Parse(xTypeID);
                        }
                        catch (System.Exception oE)
                        {
                            throw new LMP.Exceptions.SegmentDefinitionException(
                                LMP.Resources.GetLangString("Error_InvalidObjectTypeID" + xTypeID), oE);
                        }

                        //add segment
                        XmlSegment oSegment = GetSegment(oForteDoc, oSegPart);
                        oSegments.Add(oSegment);
                    }
                }
            }

            LMP.Benchmarks.Print(t0);

            return oSegments;
        }

        /// <summary>
        /// returns a new child segment that has been added
        /// to the segment's child segments collection
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        private static XmlSegment GetSegment(XmlForteDocument oForteDoc, RawSegmentPart oRawSegmentPart)
        {
            //create instance based on type of segment
            XmlSegment oSegment = XmlSegment.CreateSegmentObject(
                (mpObjectTypes)Enum.Parse(typeof(mpObjectTypes), oRawSegmentPart.GetSegmentPropertyValue("ObjectTypeID")), oForteDoc);

            oSegment.Initialize(oRawSegmentPart.TagID, null, oRawSegmentPart);
            List<RawSegmentPart> oRawParts = new List<RawSegmentPart>();
            oRawParts.Add(oRawSegmentPart);
            oSegment.RawSegmentParts = oRawParts;
            //GLOG 15772: Make sure WPDoc is set to same object as part
            oSegment.WPDoc = oRawSegmentPart.WPDocument;
            ////subscribe to changes in child segment validation
            //oSegment.ValidationChange += new LMP.Architect.Base.ValidationChangedHandler(oChildSegment_ValidationChange);
            //oSegment.AfterAllVariablesVisited += new AfterAllVariablesVisitedHandler(oChildSegment_AfterAllVariablesVisited);

            //set creation status
            oSegment.CreationStatus = Status.DefaultAuthorsSet;

            return oSegment;
        }

        /// <summary>
        /// returns all of the segments in the document that are set to display
        /// in the toolkit task pane - added for GLOG 7101 (dm)
        /// </summary>
        /// <param name="oForteDoc"></param>
        /// <returns></returns>
        public static void GetToolkitTaskPaneSegments(XmlSegments oSegments,
            ref XmlSegments oToolkitSegments)
        {
            for (int i = 0; i < oSegments.Count; i++)
            {
                XmlSegment oSegment = oSegments[i];
                if (LMP.Data.Application.DisplayInToolkitTaskPane(oSegment.TypeID))
                    oToolkitSegments.Add(oSegment);
                GetToolkitTaskPaneSegments(oSegment.Segments, ref oToolkitSegments);
            }
        }

        /// <summary>
        /// returns the default wrapper id specified in oDef
        /// </summary>
        /// <param name="oDef"></param>
        /// <returns></returns>
        public static string GetWrapperID(ISegmentDef oDef, bool bContentControls)
        {
            //default wrapper is no longer configurable -
            //it is now built into certain segment types
            mpCollectionSegmentIDs iSegmentID;
            switch (oDef.TypeID)
            {
                case mpObjectTypes.PleadingCaption:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingCaptionsCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingCaptions;
                    break;
                case mpObjectTypes.PleadingCounsel:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingCounselsCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingCounsels;
                    break;
                case mpObjectTypes.PleadingSignature:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingSignaturesCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.PleadingSignatures;
                    break;
                case mpObjectTypes.LetterSignature:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.LetterSignaturesCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.LetterSignatures;
                    break;
                case mpObjectTypes.AgreementSignature:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.AgreementSignaturesCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.AgreementSignatures;
                    break;
                case mpObjectTypes.CollectionTableItem:
                    if (bContentControls)
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.CollectionTableCC;
                    else
                        iSegmentID = LMP.Data.mpCollectionSegmentIDs.CollectionTable;
                    break;
                default:
                    return "";
            }

            return ((System.Int32)iSegmentID).ToString();
        }

        /// <summary>
        /// determines whether the specified xml contains a segment with
        /// any parts in the body
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        internal static bool TargetIsBody(string xXML)
        {
            bool bIsInBody = false;
            if (LMP.String.IsWordOpenXML(xXML))
            {
                //GLOG 5267 (dm)
                int iPos = xXML.IndexOf("<w:body>");
                int iPos2 = xXML.IndexOf("</w:body>");
                string xBodyXML = xXML.Substring(iPos, iPos2 - iPos);
                bIsInBody = xBodyXML.Contains("w:tag w:val=\"mps");
            }
            else
            {
                //remove sectPr nodes from xml
                Regex oRegex = new Regex("<w:sectPr( .*|)/?>.*?</?w:sectPr>",
                    RegexOptions.Singleline);
                string xBodyXML = oRegex.Replace(xXML, "");

                //get search string for mSEG start tag
                string xPrefix = LMP.String.GetNamespacePrefix(xXML, ForteConstants.MacPacNamespace);
                string xmSEGStart = "<" + xPrefix + ":mSEG ";

                //see if there's an mSEG in the body
                bIsInBody = (xBodyXML.IndexOf(xmSEGStart) > -1);
            }

            return bIsInBody;
        }

        /// <summary>
        /// returns the value of the specified segment property for the
        /// top-level segment in the specified xml
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        public static string GetPropertyValueFromXML(string xXML, string xPropertyName)
        {
            string xValue = "";

            //create xml document for xpath searchablity
            XmlDocument oXMLDoc = new XmlDocument();
            oXMLDoc.LoadXml(xXML);

            XmlNamespaceManager oNamespaceManager = new XmlNamespaceManager(
                oXMLDoc.NameTable);

            string xObjectData = null;

            if (String.IsWordOpenXML(xXML))
            {
                //get top-level segment content control
                oNamespaceManager.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);
                string xXPath = "//w:sdt/w:sdtPr/w:tag[starts-with(@w:val, 'mps')]";
                XmlNode oSegNode = oXMLDoc.SelectSingleNode(xXPath, oNamespaceManager);

                string xDocVarName = oSegNode.Attributes["w:val"].Value.Substring(0, 11).Replace("mps", "mpo");

                //get doc var
                xXPath = "//w:docVar[@w:name='" + xDocVarName + "']";
                XmlNode oDocVarNode = oXMLDoc.SelectSingleNode(xXPath, oNamespaceManager);
                string xVal = String.Decrypt(oDocVarNode.Attributes["w:val"].Value);

                //get object data string
                xObjectData = xVal.Substring(xVal.IndexOf("��ObjectData="));
            }
            else
            {
                string xNamespacePrefix = String.GetNamespacePrefix(xXML,
                    ForteConstants.MacPacNamespace);
                oNamespaceManager.AddNamespace(xNamespacePrefix, ForteConstants.MacPacNamespace);

                string xXPath = @"//" + xNamespacePrefix + ":mSEG";
                XmlNode oSegNode = oXMLDoc.SelectSingleNode(xXPath, oNamespaceManager);
                XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
                xObjectData = String.Decrypt(oObjectData.Value);
            }

            //find location of specified name
            int iPos1 = xObjectData.IndexOf("|" + xPropertyName + "=");
            if (iPos1 > -1)
            {
                //name was found, get value
                int iPos2 = xObjectData.IndexOf("|", iPos1 + 1);
                int iValueStartPos = iPos1 + xPropertyName.Length + 2;
                xValue = xObjectData.Substring(
                    iValueStartPos, iPos2 - iValueStartPos);
            }

            return xValue;
        }

        #endregion
        #region *********************private members*********************
        private void GetWPDocAndStream()
        {
            if (this.Definition != null)
            {
                string xml = this.Definition.XML;

                if (xml.StartsWith("<mAuthorPrefs>"))
                    xml = xml.Substring(14);

                if (!String.IsBase64SegmentString(xml))
                {
                    //convert to opc
                    OpenXmlPowerTools.FlatOpc.FlatToOpc(xml, out m_oStream);
                }
                else
                {
                    //segment xml is now held as a Base64 string
                    byte[] bytes = Convert.FromBase64String(xml);
                    m_oStream = new MemoryStream();
                    m_oStream.Write(bytes, 0, bytes.Length);
                }

                m_oWPDoc = WordprocessingDocument.Open(m_oStream, true);
                //GLOG 15835: Remove RSID info if it exists in source XML
                OpenXmlPowerTools.MarkupSimplifier.SimplifyMarkup(m_oWPDoc, new SimplifyMarkupSettings() { RemoveRsidInfo = true });
            }
            else
            {
                throw new LMP.Exceptions.NullReferenceException(
                    "No segment definition exists.");
            }
        }
        internal string GetPropertyValue(string xPropName)
        {
            return this.RawSegmentParts[0].GetSegmentPropertyValue(xPropName);
        }
        private void SetPropertyValue(string xPropName, string xValue, bool bEncrypt)
        {
            if (this.RawSegmentParts != null)
            {
                this.RawSegmentParts[0].SetSegmentPropertyValue(xPropName, xValue, bEncrypt);
            }
        }
        private void SetAuthorsFromDocument()
        {
            SetAuthorsFromDocument(true);
        }
        private void SetAuthorsFromDocument(bool bExecuteActions)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                //get authors from document
                //string xAuthors = this.Nodes.GetItemAuthorsData(this.FullTagID);
                string xAuthors = Query.GetAuthors(this);

                //Set this to avoid Author Events from Executing Variable actions
                this.m_bSkipAuthorActions = !bExecuteActions;

                if (xAuthors == "" && this.MaxAuthors > 0) //JTS 3/26/13: Don't set Authors if MaxAuthors=0
                {
                    //If current segment doesn't contain author information use parent
                    if (this.Parent != null)
                        m_oAuthors.SetAuthors(this.Parent.Authors);

                    //Set author to Current user - incorporate for Oxml
                    if ((m_oAuthors == null || m_oAuthors.Count == 0) &&
                        UserIsOnAuthorList() && ((this.ForteDocument == null) || (this.ForteDocument.Mode != XmlForteDocument.Modes.Design))
                        )
                    {
                        //add default author
                        m_oAuthors.Add(LMP.Data.Application.User.PersonObject);
                    }
                    LMP.Benchmarks.Print(t0);
                    return;
                }

                //parse authors XML from string-
                int iPos = xAuthors.IndexOf("|");

                string xAuthorXML;
                if (iPos > -1)
                    xAuthorXML = xAuthors.Substring(0, iPos);
                else
                    xAuthorXML = xAuthors;

                if (xAuthorXML != "")
                    m_oAuthors.XML = xAuthorXML;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(
                    LMP.Resources.GetLangString("Error_CouldNotSetAuthorsFromDocument"), oE);
            }
            finally
            {
                this.m_bSkipAuthorActions = false;
                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// returns the value of the specified attribute
        /// in the specified source xml
        /// </summary>
        /// <param name="xSource"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        private static string xGetValue(string xSource, string xName)
        {
            //search for specified attribute name
            string xSearch = "w:" + xName + "=";
            int iPos = xSource.IndexOf(xSearch);

            if (iPos > -1)
            {
                //attribute was found - get value start
                iPos += xSearch.Length;

                //get value end
                int iPos2 = xSource.IndexOf("\"", iPos + 1);

                //parse and return value
                return xSource.Substring(iPos + 1, iPos2 - iPos - 1);
            }
            else
                //GLOG 5516: Also handle contexts where attribute names don't use w: prefix
                xSearch = " " + xName + "=";
            iPos = xSource.IndexOf(xSearch);

            if (iPos > -1)
            {
                //attribute was found - get value start
                iPos += xSearch.Length;

                //get value end
                int iPos2 = xSource.IndexOf("\"", iPos + 1);

                //parse and return value
                return xSource.Substring(iPos + 1, iPos2 - iPos - 1);
            }
            else
                return null;
        }
        /// <summary>
        /// returns the full tag id of a newly inserted segment
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        //private static string GetNewSegmentTagID(XmlForteDocument oForteDoc)
        //{
        //    return GetNewSegmentTagID(oForteDoc, "");
        //}

        /// <summary>
        /// returns the full tag id of a newly inserted segment
        /// </summary>
        /// <param name="oForteDoc"></param>
        /// <param name="xSegmentID"></param>
        /// <returns></returns>
        //private static string GetNewSegmentTagID(XmlForteDocument oForteDoc,
        //    string xSegmentID)
        //{
        ////get the word tag containing the new segment bookmark
        //if (oForteDoc.Mode == XmlForteDocument.Modes.Design)
        //{
        //    //get the word tag containing the new segment bookmark
        //    if (oForteDoc.FileFormat ==
        //        LMP.Data.mpFileFormats.Binary)
        //    {
        //        //word xml nodes
        //        Word.XMLNode oSegmentTag = oDoc.GetNewSegmentTag(
        //            oForteDoc.WordDocument, true);

        //        if (oSegmentTag == null)
        //            //something's wrong - the word tag is not where it should be
        //            throw new LMP.Exceptions.WordTagException(
        //                LMP.Resources.GetLangString(
        //                "Error_InvalidOrMissingNewSegmentBookmark"));

        //        try
        //        {
        //            //get tag id of inserted segment node -
        //            //the tag id was reindexed by RefreshTags
        //            return oForteDoc.Tags.GetTag(oSegmentTag, false, "", "").FullTagID;
        //        }
        //        catch (System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.WordTagException(
        //                LMP.Resources.GetLangString("Error_InvalidSegmentTagID"), oE);
        //        }
        //    }
        //    else
        //    {
        //        //content controls
        //        Word.ContentControl oSegmentTag = oDoc.GetNewSegmentContentControl(
        //            oForteDoc.WordDocument, true);

        //        if (oSegmentTag == null)
        //            //something's wrong - the word tag is not where it should be
        //            throw new LMP.Exceptions.WordTagException(
        //                LMP.Resources.GetLangString(
        //                "Error_InvalidOrMissingNewSegmentBookmark"));

        //        try
        //        {
        //            //get tag id of inserted segment node -
        //            //the tag id was reindexed by RefreshTags
        //            string xTagID = oForteDoc.Tags.GetTag_CC(oSegmentTag, false, "", "").FullTagID;
        //            return xTagID;
        //        }
        //        catch (System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.WordTagException(
        //                LMP.Resources.GetLangString("Error_InvalidSegmentTagID"), oE);
        //        }
        //    }
        //}
        //else
        //{
        //    //GLOG 6799 (dm) - added xSegmentID parameter for optional validation
        //    Word.Bookmark oSegmentBmk = oDoc.GetNewSegmentBookmark(
        //        oForteDoc.WordDocument, true, xSegmentID);

        //    if (oSegmentBmk == null)
        //    {
        //        //something's wrong
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString(
        //            "Error_InvalidOrMissingNewSegmentBookmark"));
        //    }

        //    try
        //    {
        //        //get tag id of inserted segment node -
        //        //the tag id was reindexed by RefreshTags
        //        return oForteDoc.Tags.GetTag_Bookmark(
        //            oSegmentBmk, false, "", "").FullTagID;
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString("Error_InvalidSegmentTagID"), oE);
        //    }
        //}
        //}

        /// <summary>
        /// returns the segment def of the segment with the specified ID
        /// </summary>
        /// <param name="xID"></param>
        /// <returns></returns>
        public static ISegmentDef GetDefFromID(string xID)
        {
            //GLOG 3474: Access changed to internal to be accessible from Variables.AssignDefaultValues
            //parse ID
            int iID1;
            int iID2;
            String.SplitID(xID, out iID1, out iID2);

            if (iID2 == LMP.Data.ForteConstants.mpFirmRecordID)
            {
                //the segment is an admin segment - 
                //get admin segment definition
                try
                {
                    //get definition
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    return (AdminSegmentDef)oDefs.ItemFromID(iID1);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidSegmentID") + iID1.ToString(), oE);
                }
            }
            else
            {
                //the segment is a user segment - 
                //get user segment definition
                try
                {
                    //get definition
                    //Don't filter this on User, in case segment being inserted is in a Shared folder
                    //Or we're in Admin Mode
                    UserSegmentDefs oDefs = new UserSegmentDefs(
                        mpUserSegmentsFilterFields.Owner, 0);

                    UserSegmentDef oUserDef = (UserSegmentDef)oDefs.ItemFromID(xID);

                    if (oUserDef.TypeID == mpObjectTypes.UserSegment)
                        return oUserDef;
                    else
                    {
                        //convert to admin segment def - this is a segment that was
                        //created by a content designer
                        return oUserDef.ConvertToAdminSegmentDef(0, true);
                    }
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidSegmentID") + xID, oE);
                }
            }
        }
        public static XmlSegment CreateSegmentObject(mpObjectTypes iObjectTypeID, XmlForteDocument oForteDocument)
        {
            return CreateSegmentObject(iObjectTypeID, oForteDocument, false);
        }
        /// <summary>
        /// returns a new segment of the specified object type
        /// </summary>
        /// <param name="iObjectType"></param>
        /// <returns></returns>
        public static XmlSegment CreateSegmentObject(mpObjectTypes iObjectTypeID, XmlForteDocument oForteDocument, bool bSeparateWPDoc)
        {
            DateTime t0 = DateTime.Now;
            Trace.WriteNameValuePairs("iObjectType", iObjectTypeID);

            string xClassName = null;
            string xAssemblyName = null;

            if (LMP.MacPac.MacPacImplementation.IsServer)
            {
                //running the server - create Oxml server objects
                xAssemblyName = LMP.Data.Application.AppDirectory + "\\fArchitectOxml.dll";
                xClassName = "LMP.Architect.Oxml.Xml" + iObjectTypeID.ToString();
            }
            else
            {
                //create Oxml desktop Word object
                xAssemblyName = LMP.Data.Application.AppDirectory + "\\fArchitectOxmlDesktop.dll";
                xClassName = "LMP.Architect.Oxml.Word.Xml" + iObjectTypeID.ToString();
            }

            XmlSegment oSegment = (XmlSegment)Assembly.LoadFile(xAssemblyName).CreateInstance(
                xClassName, true, 0, null, null, null, null);

            if (oSegment == null)
            {
                throw new LMP.Exceptions.ClassInitializationException("Could not create object of type '" + xClassName + "'.");
            }
            if (!bSeparateWPDoc)
                oSegment.ForteDocument = oForteDocument;
            LMP.Benchmarks.Print(t0);
            return oSegment;
        }
        ///// <summary>
        ///// returns a new segment of the specified object type
        ///// </summary>
        ///// <param name="iObjectType"></param>
        ///// <returns></returns>
        //public static XmlSegment CreateSegmentObject(RawSegment oRawSegment, XmlForteDocument oForteDoc)
        //{
        //    int iObjectType = int.Parse(oRawSegment.GetSegmentPropertyValue("ObjectType"));

        //    Trace.WriteNameValuePairs("iObjectType", iObjectType);

        //    //create instance
        //    XmlSegment oSegment = (XmlSegment)Assembly.GetExecutingAssembly()
        //        .CreateInstance("LMP.Architect.Oxml.Xml" + iObjectType.ToString());

        //    //assign MacPac document to Segment
        //    oSegment.ForteDocument = oForteDoc;

        //    return oSegment;
        //}
        /// <summary>
        /// returns a new child segment that has been added
        /// to the segment's child segments collection
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        private XmlSegment AddChildSegment(mpObjectTypes iObjectTypeID, string xTagID, RawSegmentPart oChildRawSegmentPart)
        {
            //create instance based on type of segment
            XmlSegment oSegment = XmlSegment.CreateSegmentObject(iObjectTypeID, this.ForteDocument);
            oSegment.ForteDocument = this.ForteDocument;
            oSegment.WPDoc = this.WPDoc;
            oSegment.Initialize(xTagID, this, oChildRawSegmentPart);
            List<RawSegmentPart> oRawParts = new List<RawSegmentPart>();
            oRawParts.Add(oChildRawSegmentPart);
            oSegment.RawSegmentParts = oRawParts;

            //get authors from document if the parent
            //has already set authors - we also allow
            //child authors to be set when the creation
            //status of the parent is unknown, as this will
            //be the case when a child is inserted into an
            //existing segment (ie, one that is not currently
            //being created.
            if (this.CreationStatus == Status.Unknown ||
                this.CreationStatus >= Status.DefaultAuthorsSet)
            {
                oSegment.SetAuthorsFromDocument();
            }

            //subscribe to changes in child segment validation
            oSegment.ValidationChange += new LMP.Architect.Base.ValidationChangedHandler(oChildSegment_ValidationChange);
            oSegment.AfterAllVariablesVisited += new AfterAllVariablesVisitedHandler(oChildSegment_AfterAllVariablesVisited);

            //add to collection of child segments
            this.m_oChildSegments.Add(oSegment);

            //set creation status
            oSegment.CreationStatus = Status.DefaultAuthorsSet;

            return oSegment;
        }
        /// <summary>
        /// returns the child segments of this segment
        /// </summary>
        private void GetChildSegments()
        {
            //create new collection
            this.m_oChildSegments = new XmlSegments(this.ForteDocument);

            //GLOG 8495: RawSegmentParts may not be populated if we're inserting XmlSegment in finished document
            if (this.RawSegmentParts == null || this.RawSegmentParts.Count == 0)
                return;

            //get child raw segment parts
            List<RawSegmentPart> oChildRawSegParts = Query.GetChildRawSegmentParts(this.RawSegmentParts[0]);

            //cycle through child segment nodes, adding each as a child segment
            foreach (RawSegmentPart oChildPart in oChildRawSegParts)
            {
                string xTagID = oChildPart.TagID;
                //GLOG 15873: Skip Content Controls that don't have corresponding Object Data
                if (string.IsNullOrEmpty(xTagID))
                    continue;
                bool bAlreadyAdded = false;

                for (int i = 0; i < this.Segments.Count; i++)
                {
                    XmlSegment oChild = this.Segments[i];

                    if (oChild.TagID == xTagID)
                    {
                        //Add to RawSegmentParts of existing child
                        oChild.RawSegmentParts.Add(oChildPart);
                        bAlreadyAdded = true;
                        break;
                    }
                }


                if (!bAlreadyAdded && xTagID != this.FullTagID)
                {
                    //this is a child node - add as a child segment -
                    string xTypeID = null;
                    mpObjectTypes iTypeID = 0;

                    try
                    {
                        //get segment ID from mSEG
                        xTypeID = oChildPart.GetSegmentPropertyValue("ObjectTypeID");
                        iTypeID = (mpObjectTypes)Int32.Parse(xTypeID);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.SegmentDefinitionException(
                            LMP.Resources.GetLangString("Error_InvalidObjectTypeID" + xTypeID), oE);
                    }

                    //add child segment
                    AddChildSegment(iTypeID, xTagID, oChildPart);
                }
            }

            //subscribe to events
            this.m_oChildSegments.SegmentAdded += new SegmentAddedHandler(m_oChildSegments_SegmentAdded);
            this.m_oChildSegments.SegmentDeleted += new SegmentDeletedHandler(m_oChildSegments_SegmentDeleted);
        }

        ///// <summary>
        ///// returns the child segments of this segment
        ///// </summary>
        //private void GetChildSegments()
        //{
        //    if (m_oChildSegments != null)
        //    {
        //        //clear existing collection
        //        m_oChildSegments.Clear();
        //        this.m_oChildSegments.SegmentAdded = null;
        //        this.m_oChildSegments.SegmentDeleted = null;
        //    }
        //    else
        //    {
        //        //create new collection
        //        this.m_oChildSegments = new Segments(this.ForteDocument);

        //        //subscribe to events
        //        this.m_oChildSegments.SegmentAdded += new SegmentAddedHandler(m_oChildSegments_SegmentAdded);
        //        this.m_oChildSegments.SegmentDeleted += new SegmentDeletedHandler(m_oChildSegments_SegmentDeleted);
        //    }

        //    //get mSEG nodes contained in segment nodes
        //    ReadOnlyNodeStore oChildSegmentNodes = this.Nodes.GetmSEGNodes();

        //    //cycle through child segment nodes, adding each, 
        //    //except the parent, as a child segment
        //    for (int i = 0; i < oChildSegmentNodes.Count; i++)
        //    {
        //        string xTagID = oChildSegmentNodes.GetItemTagID(i);

        //        if (xTagID != this.TagID)
        //        {
        //            //this is a child node - add as a child segment -
        //            string xTypeID = null;
        //            mpObjectTypes iTypeID = 0;

        //            try
        //            {
        //                //get segment ID from mSEG
        //                xTypeID = oChildSegmentNodes.GetItemObjectDataValue(xTagID, "ObjectTypeID");
        //                iTypeID = (mpObjectTypes)Int32.Parse(xTypeID);
        //            }
        //            catch (System.Exception oE)
        //            {
        //                throw new LMP.Exceptions.SegmentDefinitionException(
        //                    LMP.Resources.GetLangString("Error_InvalidObjectTypeID" + xTypeID), oE);
        //            }

        //            //add child segment
        //            AddChildSegment(iTypeID, xTagID);
        //        }
        //    }
        //}

        /// <summary>
        /// returns the segment with the specified tag id 
        /// that is a member of the specified segment collection
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="oRootSegmentsCollection"></param>
        /// <returns></returns>
        private static XmlSegment FindSegment(string xTagID, XmlSegments oSegments)
        {
            for (int i = 0; i < oSegments.Count; i++)
            {
                if (oSegments[i].FullTagID == xTagID)
                    //we've found the segment
                    return oSegments[i];
                else
                {
                    //attempt to find segment in child segments
                    XmlSegment oSegment = FindSegment(xTagID, oSegments[i].Segments);
                    if (oSegment != null)
                        return oSegment;
                }
            }
            return null;
        }
        /// <summary>
        /// returns the segment with the specified Segment ID 
        /// that is a member or descendant of the specified 
        /// segment collection
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="oRootSegmentsCollection"></param>
        /// <returns></returns>
        internal static XmlSegment[] FindSegments(string xSegmentID, XmlSegments oSegments)
        {
            ArrayList oSegArrayList = new ArrayList();

            //append .0 to segment ID if shorthand ID was supplied
            if (xSegmentID.IndexOf('.') == -1)
                xSegmentID += ".0";

            //cycle through collection, finding all instances
            //having the specified tag id
            for (int i = 0; i < oSegments.Count; i++)
            {
                //GLOG 6452: If recreating a Saved Segment, xSegmentID will be RecreateRedirectID
                if (oSegments[i].ID == xSegmentID || oSegments[i].RecreateRedirectID == xSegmentID)
                    //we've found the segment
                    oSegArrayList.Add(oSegments[i]);
                else
                {
                    //attempt to find segment in child segments
                    XmlSegment[] aChildSegments = FindSegments(xSegmentID, oSegments[i].Segments);
                    foreach (XmlSegment oSegment in aChildSegments)
                        oSegArrayList.Add(oSegment);
                }
            }

            //copy segments to segment array
            XmlSegment[] aSegArray = new XmlSegment[oSegArrayList.Count];

            for (int i = 0; i < oSegArrayList.Count; i++)
                aSegArray[i] = (XmlSegment)oSegArrayList[i];

            return aSegArray;
        }
        /// <summary>
        /// returns the first instance of the segment of the specified
        /// type that is a member of the specified segment collection
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="oSegments"> </param>
        /// <returns></returns>
        public static XmlSegment FindSegment(mpObjectTypes iObjectTypeID, XmlSegments oSegments)
        {
            DateTime t0 = DateTime.Now;

            XmlSegment oFoundSeg = null;
            XmlSegment oSeg = null;

            //cycle recursively through segments 
            //in domain, looking for a match
            for (int i = 0; i < oSegments.Count; i++)
            {
                oSeg = oSegments[i];
                //GLOG 15772: Set WPDoc from ForteDocument
                oSeg.WPDoc = oSegments.ForteDocument.WPDocument;
                if (oSeg.TypeID == iObjectTypeID)
                    //segment is of specified type - return
                    return oSeg;
                else if (oSeg.Segments.Count > 0)
                {
                    //segment has children - check children for match
                    oFoundSeg = FindSegment(iObjectTypeID, oSeg.Segments);
                    if (oFoundSeg != null)
                        return oFoundSeg;
                }
            }

            LMP.Benchmarks.Print(t0, iObjectTypeID.ToString());

            return null;
        }

        public static XmlSegment FindSegment(mpObjectTypes iObjectTypeID, XmlForteDocument oForteDoc)
        {
            DateTime t0 = DateTime.Now;

            if (oForteDoc.WPDocument != null)
            {
                List<RawSegmentPart> oSegParts = Query.GetTopLevelRawSegmentParts(oForteDoc.WPDocument, null);
                return FindSegment(iObjectTypeID, oForteDoc, oSegParts);
            }
            else
                return null;
        }

        private static XmlSegment FindSegment(mpObjectTypes iObjectTypeID, XmlForteDocument oForteDoc, List<RawSegmentPart> oRawSegmentParts)
        {
            XmlSegment oFoundSeg = null;

            //cycle through child segment nodes, checking for specified object type id
            foreach (RawSegmentPart oSegPart in oRawSegmentParts)
            {
                if (oFoundSeg == null)
                {
                    //get segment
                    string xTypeID = oSegPart.GetSegmentPropertyValue("ObjectTypeID");
                    mpObjectTypes iTypeID = (mpObjectTypes)Int32.Parse(xTypeID);

                    if (iTypeID == iObjectTypeID)
                    {
                        //found the segment part - create segment
                        oFoundSeg = GetSegment(oForteDoc, oSegPart);
                        //return oFoundSeg;
                    }
                    else
                    {
                        //segment is not of the specified type -
                        //check children
                        List<RawSegmentPart> oChildParts = Query.GetChildRawSegmentParts(oSegPart);
                        oFoundSeg = FindSegment(iObjectTypeID, oForteDoc, oChildParts);

                        ////return if found
                        //if (oFoundSeg != null)
                        //    return oFoundSeg;
                    }
                }
                else if (oFoundSeg.RawSegmentParts[0].TagID == oSegPart.TagID)
                {
                    //GLOG 15772: Add all matching RawSegmentParts to segment's collection
                    oFoundSeg.RawSegmentParts.Add(oSegPart);
                }

            }

            return oFoundSeg;
        }

        /// <summary>
        /// returns all segments of the specified type that are 
        /// members or descendants of the specified segment collection
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="oSegments"></param>
        /// <returns></returns>
        internal static XmlSegment[] FindSegments(mpObjectTypes iObjectTypeID,
            XmlSegments oSegments)
        {
            ArrayList oSegArrayList = new ArrayList();

            //cycle through collection, finding all segments of the specified type
            for (int i = 0; i < oSegments.Count; i++)
            {
                XmlSegment oSegment = oSegments[i];
                if (oSegment.TypeID == iObjectTypeID)
                    oSegArrayList.Add(oSegment);
                XmlSegment[] oChildren = FindSegments(iObjectTypeID, oSegment.Segments);
                foreach (XmlSegment oChild in oChildren)
                {
                    if (oChild.TypeID == iObjectTypeID)
                        oSegArrayList.Add(oChild);
                }
            }

            //copy segments to segment array
            XmlSegment[] aSegArray = new XmlSegment[oSegArrayList.Count];

            for (int i = 0; i < oSegArrayList.Count; i++)
                aSegArray[i] = (XmlSegment)oSegArrayList[i];

            return aSegArray;
        }

        /// <summary>
        /// returns all collection table item segments that are 
        /// members or descendants of the specified segment collection
        /// </summary>
        /// <param name="iObjectTypeID"></param>
        /// <param name="oSegments"></param>
        /// <returns></returns>
        internal static XmlSegment[] FindCollectionTableItemChildren(XmlSegments oSegments)
        {
            ArrayList oSegArrayList = new ArrayList();

            //cycle through collection, finding all collection table item segments
            for (int i = 0; i < oSegments.Count; i++)
            {
                XmlSegment oSegment = oSegments[i];
                if (oSegment is XmlCollectionTableItem || oSegment is XmlPaper || oSegment is XmlSidebar)
                    oSegArrayList.Add(oSegment);
                XmlSegment[] oChildren = FindCollectionTableItemChildren(oSegment.Segments);
                foreach (XmlSegment oChild in oChildren)
                {
                    if (oChild is XmlCollectionTableItem || oChild is XmlPaper || oChild is XmlSidebar)
                        oSegArrayList.Add(oChild);
                }
            }

            //copy segments to segment array
            XmlSegment[] aSegArray = new XmlSegment[oSegArrayList.Count];

            for (int i = 0; i < oSegArrayList.Count; i++)
                aSegArray[i] = (XmlSegment)oSegArrayList[i];

            return aSegArray;
        }

        /// <summary>
        /// updates authors of child segments to match this segment's authors
        /// </summary>
        public void UpdateChildSegmentAuthors()
        {
            DateTime t0 = DateTime.Now;

            if (!(this.m_iStatus >= Status.VariableDefaultValuesSet ||
                    this.m_iStatus == Status.Unknown))
                return;

            for (int i = 0; i < this.Segments.Count; i++)
            {
                XmlSegment oChild = this.Segments[i];
                try
                {
                    //Set this to avoid Author Events from Executing Variable actions
                    oChild.m_bSkipAuthorActions = this.m_bSkipAuthorActions;
                    oChild.m_bAllowUpdateForAuthor = this.m_bAllowUpdateForAuthor;
                    //do only if link is specified
                    if (oChild.LinkAuthorsToParent)
                    {
                        oChild.Authors.SetAuthors(this.Authors);
                        if (this.PreviousAuthors != null)
                            oChild.PreviousAuthors.SetAuthors(this.PreviousAuthors);
                    }
                }
                finally
                {
                    oChild.m_bAllowUpdateForAuthor = true;
                    oChild.m_bSkipAuthorActions = false;
                }
            }
            LMP.Benchmarks.Print(t0, this.FullTagID);
        }

        /// <summary>
        /// updates values of properties that have linked values related to the specified author -
        /// -2 = no author fields,
        /// 0 = update all author fields, -1 = update lead author only,
        /// 1 = update first author, 2 = update second author, etc.
        /// </summary>
        /// <param name="shAuthorIndex"></param>
        /// <param name="shParentAuthorIndex"></param>
        /// <param name="bExecuteActions"></param>
        private void UpdateForAuthor(short shAuthorIndex, short shParentAuthorIndex, bool bExecuteActions)
        {
            DateTime t0 = DateTime.Now;

            //GLOG 7126 (dm) - use separate method to update finished segment
            //TODO: OpenXML rewrite
            //if (this.IsFinished)
            //{
            //    UpdateSnapshotForAuthor(shAuthorIndex, shParentAuthorIndex, bExecuteActions);
            //    return;
            //}

            //cycle through variables, checking whether default value contains a field code
            //that might evaluate differently for new author
            for (int i = 0; i < this.Variables.Count; i++)
            {
                //get variable
                XmlVariable oVar = this.Variables[i];

                //get default value - exit if empty
                string xExp = oVar.DefaultValue;

                if (XmlExpression.ContainsAuthorCode(xExp, shAuthorIndex, shParentAuthorIndex))
                {
                    //variable's default value contains a related field code,
                    //so update is required - set new value and execute actions if specified - 
                    oVar.SetValue(XmlExpression.Evaluate(xExp, this, this.ForteDocument), false);

                    if (bExecuteActions)
                        oVar.VariableActions.Execute();
                }
                else if (bExecuteActions)
                {
                    //check action parameters for related field code
                    for (int j = 0; j < oVar.VariableActions.Count; j++)
                    {
                        //get parameters
                        string xParam = ((XmlVariableAction)oVar.VariableActions[j]).Parameters;
                        if (XmlExpression.ContainsAuthorCode(xParam, shAuthorIndex,
                            shParentAuthorIndex))
                        {
                            //update is required - execute actions now
                            oVar.VariableActions.Execute();
                            break;
                        }
                        else if (oVar.VariableActions[j].Type == XmlVariableActions.Types.IncludeExcludeText &&
                            XmlExpression.ContainsAuthorCode(oVar.SecondaryDefaultValue, shAuthorIndex, shParentAuthorIndex))
                        {
                            //GLOG 4292: Also update if there is an IncludeExcludeText action associated with an
                            //Author Code in the Secondary Default Value
                            oVar.VariableActions.Execute();
                            break;
                        }
                    }
                }
            }

            //execute segment action
            this.Actions.Execute(Events.AfterAuthorUpdated);

            //raise event
            if (this.AfterAuthorUpdated != null)
                this.AfterAuthorUpdated(this, new EventArgs());

            LMP.Benchmarks.Print(t0, "Segment = " + this.DisplayName);
        }

        /// <summary>
        /// updates snapshot to reflect change of author - added for GLOG 7126 (dm)
        /// </summary>
        /// <param name="shAuthorIndex"></param>
        /// <param name="shParentAuthorIndex"></param>
        /// <param name="bExecuteActions"></param>
        private void UpdateSnapshotForAuthor(short shAuthorIndex, short shParentAuthorIndex, bool bExecuteActions)
        {
            string xAuthors = XmlSnapshot.GetAuthorsSnapshotValue(this.Authors);
            this.Snapshot.SetValue("Authors", xAuthors);

            //load variable config xml
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml(this.Snapshot.Configuration);
            XmlNodeList oNodes = oXML.SelectNodes("/Segment/Variable");

            string[] aVars = this.Snapshot.Values.Split(XmlSnapshot.mpSnapshotFieldDelimiter);
            for (int i = 0; i < aVars.Length; i++)
            {
                if (aVars[i] != "")
                {
                    string[] aVals = aVars[i].Split(XmlSnapshot.mpSnapshotValueDelimiter);
                    XmlNode oNode = oXML.SelectSingleNode("/Segment/Variable[@Name='" +
                        aVals[0] + "']");
                    if (oNode != null)
                    {
                        string xExp = oNode.Attributes["DefaultValue"].Value;
                        if (XmlExpression.ContainsAuthorCode(xExp, shAuthorIndex, shParentAuthorIndex))
                        {
                            //variable's default value contains a related field code,
                            //so update is required - set new value and execute actions if specified - 
                            this.Snapshot.SetValue(aVals[0], XmlExpression.Evaluate(xExp, this, this.ForteDocument));

                            //if (bExecuteActions)
                            //    oVar.VariableActions.Execute();
                        }
                    }
                }
            }

            //save
            this.Snapshot.Save(this.Snapshot.Values);

            ////execute segment action
            //this.Actions.Execute(Events.AfterAuthorUpdated);

            //raise event
            if (this.AfterAuthorUpdated != null)
                this.AfterAuthorUpdated(this, new EventArgs());
        }

        ///// <summary>
        ///// sets up the segment for initial use
        ///// </summary>
        protected virtual void InitializeValues(XmlPrefill oPrefill, bool bForcePrefillOverride)
        {
            InitializeValues(oPrefill, "", bForcePrefillOverride, false);
        }

        //GLOG : 6987 : CEH
        //added new bInitializePrefillAuthors argument specific to ApplyPrefill functionality
        protected virtual void InitializeValues(XmlPrefill oPrefill, bool bForcePrefillOverride,
            bool bInitializePrefillAuthors)
        {
            InitializeValues(oPrefill, "", bForcePrefillOverride, bInitializePrefillAuthors);
        }

        //GLOG : 6987 : CEH
        protected virtual void InitializeValues(XmlPrefill oPrefill, string xRelativePath,
           bool bForcePrefillOverride)
        {
            InitializeValues(oPrefill, xRelativePath, bForcePrefillOverride, false, false);
        }

        protected virtual void InitializeValues(XmlPrefill oPrefill, string xRelativePath,
            bool bForcePrefillOverride, bool bInitializePrefillAuthors)
        {
            InitializeValues(oPrefill, xRelativePath, bForcePrefillOverride, false, bInitializePrefillAuthors);
        }

        /// <summary>
        /// sets up the segment for initial use
        /// </summary>
        protected virtual void InitializeValues(XmlPrefill oPrefill, string xRelativePath,
            bool bForcePrefillOverride, bool bChildStructureAvailable, bool bInitializePrefillAuthors)
        {
            DateTime t0 = DateTime.Now;

            //initialize authors
            if (!string.IsNullOrEmpty(xRelativePath))
            {
                this.InitializeAuthors(null, bInitializePrefillAuthors); //GLOG 8652

                //execute AfterDefaultAuthorSet actions
                this.Actions.Execute(XmlSegment.Events.AfterDefaultAuthorSet);

                //raise after default author set
                if (this.AfterDefaultAuthorSet != null)
                    this.AfterDefaultAuthorSet(this, new EventArgs());

                //set segment status
                this.m_iStatus = Status.DefaultAuthorsSet;
            }
            //GLOG : 6987 : CEH
            //initialize authors only when called from Insert Data
            else if ((oPrefill != null) && bInitializePrefillAuthors)
                this.InitializeAuthors(oPrefill, false); //GLOG 8652
            //raise before segment setup event
            if (this.BeforeSetup != null)
                this.BeforeSetup(this, new EventArgs());

            //set variable default/prefill values
            bool bAssigned = false;
            while (!bAssigned)
            {
                if (oPrefill == null)
                    bAssigned = this.Variables.AssignDefaultValues();
                else
                {
                    if (xRelativePath == "" && this.LevelChooserRequired())
                    {
                        //Set Court Levels from Prefill
                        XmlAdminSegment oASeg = (XmlAdminSegment)this;
                        try
                        {
                            oASeg.L0 = Int32.Parse(oPrefill["L0"]);
                            oASeg.L1 = Int32.Parse(oPrefill["L1"]);
                            oASeg.L2 = Int32.Parse(oPrefill["L2"]);
                            oASeg.L3 = Int32.Parse(oPrefill["L3"]);
                            oASeg.L4 = Int32.Parse(oPrefill["L4"]);
                            //Need to update Segment Object Data also
                            //GLOG 8691
                            this.SetPropertyValue("L0", oASeg.L0.ToString(), false);
                            this.SetPropertyValue("L1", oASeg.L1.ToString(), false);
                            this.SetPropertyValue("L2", oASeg.L2.ToString(), false);
                            this.SetPropertyValue("L3", oASeg.L3.ToString(), false);
                            this.SetPropertyValue("L4", oASeg.L4.ToString(), false);

                        }
                        catch { }
                    }

                    //set culture if it's in the prefill
                    string xCulture = oPrefill["Culture"];
                    if (!string.IsNullOrEmpty(xCulture))
                    {
                        //GLOG 4018: If Language changed by Prefill, run appropriate actions
                        if (this.Culture != int.Parse(xCulture))
                        {
                            this.Culture = int.Parse(xCulture);
                            //GLOG 8691
                            this.SetPropertyValue("Culture", this.Culture.ToString(), false);
                            this.Actions.Execute(Events.AfterLanguageUpdated);
                        }
                    }

                    bAssigned = this.Variables.AssignDefaultValues(oPrefill,
                        xRelativePath, bForcePrefillOverride);
                }
            }

            //set status
            this.m_iStatus = XmlSegment.Status.VariableDefaultValuesSet;

            //execute AfterDefaultValuesSet actions
            this.Actions.Execute(XmlSegment.Events.AfterDefaultValuesSet);

            //raise after default values set
            if (this.AfterDefaultValuesSet != null)
                this.AfterDefaultValuesSet(this, new EventArgs());

            string xSegmentIDs = "|";

            //setup all children- we do this after we set up the
            //the parent because some child segment variables
            //may depend on the value of variables in the parent
            for (int i = 0; i < this.Segments.Count; i++)
            {
                XmlSegment oChild = this.Segments[i];

                //GLOG 6004 (dm) - initialize values for types of children not
                //included in child structures
                if ((!bChildStructureAvailable) || ((!(oChild is XmlCollectionTable)) &&
                    (!(oChild is XmlPaper))))
                {
                    //child structure is not available - initialize child values

                    //determine the number of sibling segments with this Segment ID -
                    //apply an index to the relative path to know which prefill value
                    //to retrieve - e.g. if there are two sibling segments named X,
                    //and the prefill contains prefill values for two (or more)
                    //such segments, we need to apply the first set of prefill values
                    //to X and the second set of prefill values to X.1
                    int iNumSegs = LMP.String.CountChrs("|" + xSegmentIDs, oChild.ID + "|");

                    if (iNumSegs == 0)
                    {
                        //this is the first segment with this ID at this level
                        //GLOG 6943: Pass bInitializePrefillAuthors value to child
                        oChild.InitializeValues(oPrefill, xRelativePath +
                            String.RemoveIllegalNameChars(oChild.DisplayName) + ".", bForcePrefillOverride, bInitializePrefillAuthors);
                    }
                    else
                    {
                        //this is not the first segment with this ID at this level -
                        //get prefill value from .Y index of item - second item has
                        //an index tag of .1
                        oChild.InitializeValues(oPrefill, xRelativePath +
                            String.RemoveIllegalNameChars(oChild.DisplayName) +
                            "." + iNumSegs + ".", bForcePrefillOverride);
                    }
                }

                xSegmentIDs += oChild.ID + "|";

                //GLOG 3605 (2/11/09 dm)
                oChild.CreationStatus = Status.Finished;
            }

            //execute AfterSegmentGenerated actions
            this.Actions.Execute(XmlSegment.Events.AfterSegmentGenerated);

            //raise after segment generated - right now, this occurs right after
            //the AfterDefaultValuesSet event - this might not be the case in the future
            if (this.Generated != null)
                this.Generated(this, new EventArgs());

            //GLOG 3144: Ensure Special caption border is correct length initially
            //TODO: It might make sense to use the Generated event for this, but it's not
            //currently in use, and I'm not sure where would be appropriate to place this (JTS)
            //GLOG 4282: Run for any Collection Table item
            if (this is XmlCollectionTableItem)
            {
                this.UpdateTableBorders();
            }
            this.CreationStatus = Status.Finished;
            //GLOG 7048 (dm) - if doc editor node was already added, it will be displaying
            //as invalid because values hadn't yet been intitialized - let it know that
            //this has changed
            this.RaiseValidationChangeIfNecessary(new LMP.Architect.Base.ValidationChangedEventArgs(true));

            LMP.Benchmarks.Print(t0);
        }

        private void InitializeAuthors()
        {
            InitializeAuthors(null, true);
        }
        private void InitializeAuthors(XmlPrefill oPrefill)
        {
            InitializeAuthors(oPrefill, true);
        }
        private void InitializeAuthors(XmlPrefill oPrefill, bool bPrompt) //GLOG 8652
        {
            DateTime t0 = DateTime.Now;
            bool bAllowManualAuthor = false;

            //GLOG #4327 - dcf
            if (!this.DefinitionRequiresAuthor && !this.LinkAuthorsToParent)
                return;

            else if (!this.IsTopLevel && this.LinkAuthorsToParent && oPrefill == null)
            {
                //this is a child segment whose authors
                //are linked to the parent segment - set
                //authors from parent
                m_oAuthors.SetAuthors(this.Parent.Authors);
                if (this.Parent.PreviousAuthors != null)
                    m_oPrevAuthors.SetAuthors(this.Parent.PreviousAuthors);
            }
            else
            {
                //we need to set authors
                LocalPersons oPersons = null;
                LMP.Controls.mpListTypes iType = LMP.Controls.mpListTypes.AllAuthors;
                string xACP = this.AuthorControlProperties;
                if (!string.IsNullOrEmpty(xACP))
                {
                    string[] aCP = xACP.Split(StringArray.mpEndOfSubValue);
                    //GLOG 3719: Determine if Manual Authors should be allowed
                    if (xACP.Contains("AllowManualInput="))
                    {
                        for (int i = 0; i <= aCP.GetUpperBound(0); i++)
                        {
                            if (aCP[i].StartsWith("AllowManualInput="))
                            {
                                bAllowManualAuthor = aCP[i].Substring(17).ToUpper() == "TRUE";
                                break;
                            }
                        }
                    }

                    //If Author Control Properties include ListType, filter People list based on it
                    if (xACP.Contains("ListType="))
                    {
                        for (int i = 0; i <= aCP.GetUpperBound(0); i++)
                        {
                            if (aCP[i].StartsWith("ListType="))
                            {
                                //Filter Persons collection based on Segment's Author Control setting
                                try
                                {
                                    iType = (LMP.Controls.mpListTypes)(Int32.Parse(aCP[i].Substring(9)));
                                }
                                catch { }
                                //JTS 3/22/10: Include Additional Office records
                                if (iType == LMP.Controls.mpListTypes.Attorneys)
                                    oPersons = new LocalPersons(mpPeopleListTypes.AllPeople, mpTriState.True, UsageStates.OfficeActive);
                                else if (iType == LMP.Controls.mpListTypes.NonAttorneys)
                                    oPersons = new LocalPersons(mpPeopleListTypes.AllPeople, mpTriState.False, UsageStates.OfficeActive);
                                else
                                    //GLOG 4838 //GLOG 4957
                                    oPersons = new LocalPersons(mpTriState.True, mpPeopleListTypes.AllPeople, UsageStates.OfficeActive);
                                break;
                            }
                        }
                    }
                }

                //list type was not specified - get default author list
                if (oPersons == null)
                    //JTS 3/22/10: Make sure additional office records are included in collection
                    oPersons = new LocalPersons(mpTriState.True, mpPeopleListTypes.AllPeople, UsageStates.OfficeActive);

                if (oPrefill != null && oPrefill.Authors.Count > 0)
                {
                    //Get Author XML from Prefill -
                    //GLOG 4417 (dm) - prevent error when MaxAuthors = 0
                    this.Authors.XML = oPrefill.AuthorsXML(Math.Max(this.MaxAuthors, 1));

                    //ensure author is in user db
                    Persons oPeopleInDB = new LocalPersons(mpTriState.True, mpPeopleListTypes.AllActivePeopleInDatabase);
                    Author oLeadAuthor = this.Authors.GetLeadAuthor();
                    Person oLeadAuthorPerson = null;
                    DialogResult iImportAuthors = DialogResult.Ignore;

                    //GLOG : 6482 : ceh
                    string xAuthor = null;
                    int iID1 = 0;
                    int iID2 = 0;

                    //GLOG 3719: Don't display prompt and replace authors 
                    //if initializing a child Segment that allows manual authors
                    if (!oPersons.IsInCollection(oLeadAuthor.ID) && (this.IsTopLevel ||
                        this.LinkAuthorsToParent || !bAllowManualAuthor))
                    {
                        if (!oLeadAuthor.ID.EndsWith(".0"))
                        {
                            //this is a private person that no longer exists -
                            //set author to current user
                            //GLOG 8652: Show Prompt only if specified
                            if (bPrompt && !LMP.MacPac.MacPacImplementation.IsServer)
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Prompt_AuthorDoesNotExistUseUser"),
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }

                            this.Authors.RemoveAll();

                            //get the default office person of the current user
                            int iDefaultOfficeID = LMP.Data.Application.User.PersonObject.DefaultOfficeRecordID;

                            if (iDefaultOfficeID == 0)
                                oLeadAuthorPerson = LMP.Data.Application.User.PersonObject;
                            else
                                oLeadAuthorPerson = oPeopleInDB.ItemFromID(iDefaultOfficeID.ToString() + ".0");

                            this.Authors.Add(oLeadAuthorPerson, oLeadAuthor.BarID); //GLOG 6943: Set Bar ID from Prefilll
                        }
                        else
                        {
                            //GLOG : 6482 : ceh
                            LMP.Data.Application.SplitID(oLeadAuthor.ID, out iID1, out iID2);

                            if (!string.IsNullOrEmpty(oLeadAuthor.FullName))
                                xAuthor = oLeadAuthor.FullName + " (" + iID1.ToString() + ")";
                            else
                                xAuthor = iID1.ToString();

                            //GLOG 4538: Modify message displayed if User is not in filtered Author list
                            string xNewAuthor = "";
                            if (UserIsOnAuthorList())
                                xNewAuthor = "you";
                            else
                                xNewAuthor = "first Author in list";

                            //GLOG 8652: Show Prompt only if specified
                            if (bPrompt && !LMP.MacPac.MacPacImplementation.IsServer)
                                //prompt to either copy author or set user as author
                                iImportAuthors = MessageBox.Show(
                                    string.Format(LMP.Resources.GetLangString("Prompt_Authors_CopyFromNetworkDBOrSetUserAsAuthor"), this.DisplayName, xAuthor, xNewAuthor),
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            else
                                iImportAuthors = DialogResult.No;
                        }

                        System.Windows.Forms.Application.DoEvents();
                    }
                    if (iImportAuthors == DialogResult.Yes)
                    {

                        //GLOG 4538

                        //GLOG : 6482 : ceh
                        //int iID1 = 0;
                        //int iID2 = 0;

                        try
                        {
                            LMP.Data.Application.SplitID(oLeadAuthor.ID, out iID1, out iID2);
                            //GLOG 8276
                            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                                oLeadAuthorPerson = oPersons.CopyPublicUser(iID1, true, true);
                            else
                                oLeadAuthorPerson = oPersons.CopyNetworkUser(iID1, true);
                        }
                        catch
                        {
                            //GLOG 4538: Indicate to User that either User or First Author in filtered list
                            //will be set as author

                            //GLOG : 6482 : ceh
                            //string xAuthor = null;
                            //if (!string.IsNullOrEmpty(oLeadAuthor.FullName))
                            //    xAuthor = oLeadAuthor.FullName + " (" + iID1.ToString() + ")";
                            //else
                            //    xAuthor = iID1.ToString();

                            //GLOG 8562: Show Prompt only if specified
                            if (bPrompt && !LMP.MacPac.MacPacImplementation.IsServer)
                            {
                                string xMsg = string.Format(LMP.Resources.GetLangString(
                                    "Prompt_Authors_CouldNotCopyAuthor"));

                                //couldn't copy author from network db - 
                                //alert user
                                //GLOG 4538: Use iID1 in case oLeadAuthorPerson is null
                                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            //System.Windows.Forms.Application.DoEvents();
                        }
                        //GLOG 4538: If original Prefill Author no longer exists and oLeadAuthorPerson is null
                        //still clear Authors so code below will reset to User or first person in filtered list
                        this.Authors.RemoveAll();
                        //GLOG 6943: Set Bar ID from Prefill
                        if (oLeadAuthorPerson != null)
                            this.Authors.Add(oLeadAuthorPerson, oLeadAuthor.BarID);
                        //GLOG 6943: Add other Authors as well
                        for (int i = 0; i < oPrefill.Authors.Count; i++)
                        {
                            Person oAddPerson = null;
                            string xID = oPrefill.Authors[i][0];
                            if (xID.EndsWith(".0") && xID != oLeadAuthor.ID)
                            {
                                try
                                {
                                    LMP.Data.Application.SplitID(xID, out iID1, out iID2);
                                    //GLOG 8276
                                    if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                                        oAddPerson = oPersons.CopyPublicUser(iID1, true, true);
                                    else
                                        oAddPerson = oPersons.CopyNetworkUser(iID1, true);
                                }
                                catch
                                {
                                }
                            }
                            if (oAddPerson != null)
                                this.Authors.Add(oAddPerson, oPrefill.Authors[i][3]);
                        }
                    }
                    //GLOG 4634: Only clear if No selected to Prompt, or if Manual Authors are not allowed
                    else if (iImportAuthors == DialogResult.No ||
                        (!oPersons.IsInCollection(oLeadAuthor.ID) && !bAllowManualAuthor))
                        //GLOG 4538: Clear authors so current user will be used as indicated in prompt
                        this.Authors.RemoveAll();
                }

                //if no authors, set the author to the first person on the list or the
                //user, unless the user is excluded by the list type
                if ((this.Authors.Count == 0) && ((oPersons.Count > 0) || UserIsOnAuthorList()))
                {
                    //GLOG 4838: Check for Default Office Record as well as Primary ID
                    Person oPerson = LMP.Data.Application.User.PersonObject;
                    if (oPersons.IsInCollection(oPerson.ID) ||
                        oPersons.IsInCollection(oPerson.DefaultOfficeRecordID) ||
                        oPersons.Count == 0)
                    {
                        string xDefaultOfficeRecordID1 = oPerson.DefaultOfficeRecordID.ToString() + ".0";

                        if (xDefaultOfficeRecordID1 != "0.0" && oPerson.ID != xDefaultOfficeRecordID1)
                        {
                            //get the person record specified by the default office record id
                            oPerson = oPersons.ItemFromID(xDefaultOfficeRecordID1);
                        }
                    }
                    else
                        //Set Default to first item in filtered collection
                        oPerson = oPersons.ItemFromIndex(1);

                    if (this is XmlAdminSegment)
                    {
                        XmlAdminSegment oAS = (XmlAdminSegment)this;
                        //Get Default Bar ID
                        string xBarID = oPerson.DefaultBarID(oAS.L0, oAS.L1, oAS.L2, oAS.L3, oAS.L4);
                        this.Authors.Add(oPerson, xBarID);
                    }
                    else
                        //add default author
                        this.Authors.Add(oPerson);
                }
            }
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// raises the ValidationChanged event if the validation
        /// of the segment has changed
        /// </summary>
        /// <param name="e"></param>
        private void RaiseValidationChangeIfNecessary(LMP.Architect.Base.ValidationChangedEventArgs e)
        {
            bool bSegmentCurrentlyValid = this.IsValid;
            bool bValidationChanged = (m_bSegmentPreviouslyValid != bSegmentCurrentlyValid);

            if (bValidationChanged)
            {
                if (bSegmentCurrentlyValid && !m_bSegmentPreviouslyValid)
                {
                    //execute segment actions that should run when the 
                    //segment is valid
                    this.Actions.Execute(XmlSegment.Events.AfterSegmentValid);
                }

                //raise segment ValidationChanged event
                if (this.ValidationChange != null)
                    this.ValidationChange(this,
                        new LMP.Architect.Base.ValidationChangedEventArgs(e.Validated));

                //validation state has changed -
                //mark validation state for next iteration of this method
                m_bSegmentPreviouslyValid = bSegmentCurrentlyValid;
            }
        }
        /// <summary>
        /// Duplicates the Segment content in the specified number sections,
        /// Or removes sections if existing number is greater than iSections
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="iSections"></param>
        internal static void AddOrRemoveSections(XmlSegment oSegment, int iSections)
        {
            WordprocessingDocument oDoc = oSegment.WPDoc;
            foreach (RawSegmentPart oPart in oSegment.RawSegmentParts)
            {
                SdtElement oCC = oPart.ContentControl;
                OpenXmlPart oContainer = Query.GetContainingOpenXmlPart(oCC, oDoc);
                //Can only be used for mSEG in main body
                if (oContainer == oDoc.MainDocumentPart)
                {
                    //GLOG 15834:  additional Header and Footer parts will need to be added for each new section
                    SectionProperties[] aSecProps = Query.GetSectionPropertiesForContentControl(oCC, oDoc);
                    if (aSecProps.Count() == iSections)
                    {
                        //Nothing to do
                        return;
                    }
                    else if (aSecProps.Count() > iSections)
                    {
                        //Need to remove extra sections
                        OpenXmlElement oProp = aSecProps[iSections - 2];
                        Paragraph oPara = oProp.Ancestors<Paragraph>().First();
                        Paragraph oLastPara = oCC.Descendants<Paragraph>().Last();
                        OpenXmlElement[] oNodes = oPara.ElementsAfter().ToArray<OpenXmlElement>();
                        for (int i = oNodes.Count(); i > 0; i--)
                        {
                            if (oNodes[i - 1].IsBefore(oLastPara))
                            {
                                Query.DeleteAssociatedDocVars(oDoc, oNodes[i - 1]);
                                oNodes[i - 1].Remove();
                            }
                        }
                        oLastPara.Remove();
                        oProp.Remove();
                        Query.RemoveOrphanedParts(oSegment.WPDoc);
                    }
                    else
                    {
                        OpenXmlElement oContent = oCC.Descendants().Where(c => c.LocalName == "sdtContent").First();

                        oContent.RemoveAllChildren<BookmarkStart>();
                        oContent.RemoveAllChildren<BookmarkEnd>();
                        OpenXmlElement oFirstChild = oContent.Elements().First();
                        OpenXmlElement oProp = aSecProps[0];
                        string xPropXml = oProp.OuterXml;
                        OpenXmlElement oLastNode = null;
                        if (aSecProps.Count() > 1)
                        {
                            oLastNode = oProp.Ancestors<Paragraph>().First();
                        }
                        else
                        {
                            oLastNode = oContent.Descendants<Paragraph>().Last();
                            //Make sure all Sections except last have SectionProperties attached to final paragraph
                            SectionProperties oSecProps = oLastNode.Descendants<SectionProperties>().FirstOrDefault();
                            if (oSecProps == null)
                            {
                                ParagraphProperties oParaProps = oLastNode.Descendants<ParagraphProperties>().FirstOrDefault();
                                if (oParaProps == null)
                                {
                                    oParaProps = oLastNode.AppendChild<ParagraphProperties>(new ParagraphProperties());
                                }
                                oSecProps = oParaProps.AppendChild<SectionProperties>(new SectionProperties(xPropXml));
                            }
                        }
                        List<OpenXmlElement> oList = new List<OpenXmlElement>();
                        foreach (OpenXmlElement oChild in oContent.Elements())
                        {
                            if (oChild.IsBefore(oProp))
                            {
                                OpenXmlElement oCopy = oChild.CloneNode(true);
                                oList.Add(oCopy);
                            }
                            else
                                break;
                        }
                        for (int i = aSecProps.Count() + 1; i <= iSections; i++)
                        {
                            foreach (OpenXmlElement oNode in oList)
                            {
                                SectionProperties oCurProp = oNode.Descendants<SectionProperties>().FirstOrDefault();
                                if (oCurProp != null)
                                {
                                    if (i < iSections)
                                    {
                                        Query.CopyAndReTagHeadersFooters(oCurProp, aSecProps[0], oDoc);
                                    }
                                    else
                                    {
                                        //Remove SectionProperties from last para of last section -
                                        //this will use the original SectionProperties 
                                        oCurProp.Remove();
                                    }
                                }

                                //GLOG 15826: Make sure top child is retagged if it's a Content Control
                                if (oNode is SdtElement)
                                {
                                    if (Query.GetBaseName((SdtElement)oNode) == "mBlock")
                                    {
                                        Query.ReTagAndRenameBlock((SdtElement)oNode, oSegment, oDoc);
                                    }
                                    else if (Query.GetBaseName((SdtElement)oNode) != "mSEG")
                                    {
                                        Query.ReTagContentControl((SdtElement)oNode, oDoc);
                                    }
                                }
                                //GLOG 15805: Process Descendants in reverse order, to ensure CCs nested in other CCs 
                                //get their Tag replaced in the InnerXML of the parent CC
                                List<SdtElement> oChildCCs = oNode.Descendants<SdtElement>().ToList();
                                oChildCCs.Reverse();
                                foreach (SdtElement oElement in oChildCCs)
                                {
                                    if (Query.GetBaseName(oElement) == "mBlock")
                                    {
                                        Query.ReTagAndRenameBlock(oElement, oSegment, oDoc);
                                    }
                                    else if (Query.GetBaseName(oElement) != "mSEG")
                                    {
                                        Query.ReTagContentControl(oElement, oDoc);
                                    }
                                }
                                string x = oNode.OuterXml;
                                oLastNode = oContent.AppendChild(oNode.CloneNode(true)); // oContent.InsertAfter(oNode.CloneNode(true), oLastNode);
                            }
                        }

                        if (!(aSecProps[0].Parent is ParagraphProperties))
                        {
                            Query.CopyAndReTagHeadersFooters(aSecProps[0], (SectionProperties)aSecProps[0].Clone(), oDoc);
                        }
                    }
                }
                break;
            }
            oSegment.RawSegmentParts = Query.GetTopLevelRawSegmentParts(oSegment.WPDoc, oSegment.ID);
            oSegment.GetChildSegments();
        }
        /// <summary>
        /// refreshes this segment's child segments, variables, and blocks
        /// </summary>
        private void RefreshMembers(XmlSegment.RefreshTypes oType)
        {
            DateTime t0 = DateTime.Now;

            if (oType != RefreshTypes.ChildSegmentsOnly)
            {
                //refresh variables
                m_oVariables = new XmlVariables(this);

                //subscribe to notifications that variables have been validated
                m_oVariables.ValidationChanged += new LMP.Architect.Base.ValidationChangedHandler(OnVariablesValidationChanged);

                //refresh blocks
                m_oBlocks = new XmlBlocks(this);
            }

            if (oType != RefreshTypes.NoChildSegments)
            {
                //refresh child segments
                for (int i = 0; i < this.Segments.Count; i++)
                    this.Segments[i].RefreshMembers(RefreshTypes.All);
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// initializes the segment
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="oForteDoc"></param>
        protected virtual void Initialize(string xTagID)
        {
            Initialize(xTagID, null);
        }
        /// <summary>
        /// initializes the segment
        /// </summary>
        /// <param name="xTagID"></param>
        protected virtual void Initialize(string xTagID, XmlSegment oParent, RawSegmentPart oRawSegmentPart)
        {
            Initialize(xTagID, oParent, null);
        }
        /// <summary>
        /// initializes the segment
        /// </summary>
        /// <param name="xTagID"></param>
        protected virtual void Initialize(string xTagID, XmlSegment oParent)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xTagID", xTagID);

            //set tag id
            if (oParent != null)
            {
                this.FullTagID = oParent.FullTagID + "." + xTagID;
            }
            else
            {
                this.FullTagID = xTagID;
            }

            //set the parent segment
            this.Parent = oParent;

            //GLOG item #4219 - dcf - 
            //set language to language of parent,
            //if this is a child
            if (this.Parent != null)
                this.Culture = this.Parent.Culture;

            //hook up to event handlers
            this.Authors = new XmlAuthors(this);
            this.Authors.AuthorAdded += new AuthorAddedHandler(m_oAuthors_AuthorAdded);
            this.Authors.AuthorsReindexed += new AuthorsReindexedHandler(m_oAuthors_AuthorsReindexed);
            this.Authors.LeadAuthorChanged += new LeadAuthorChangedHandler(m_oAuthors_LeadAuthorChanged);

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// parses the specified xml into body and section xml,
        /// and returns a delimited string of page setup values
        /// </summary>
        /// <param name="xSegmentXML"></param>
        /// <param name="xBodyXML"></param>
        /// <param name="xSectionsXML"></param>
        /// <param name="xPageSetupValues"></param>
        public static void ParseSegmentXML(string xSegmentXML, out string xBodyXML,
            out string xSectionsXML, out string xPageSetupValues, out string xCompatOptions)
        {
            DateTime t0 = DateTime.Now;

            //get segment xml without header and footer nodes
            Regex oRegex = null;
            if (LMP.String.IsWordOpenXML(xSegmentXML))
                //word open xml - headers and footers are in separate parts -
                //just remove the references to them in the <w:sectPr> nodes
                oRegex = new Regex("<w:(head|foot)erReference .*?/>", RegexOptions.Singleline);
            else
                oRegex = new Regex("<w:(hd|ft)r .*?</w:(hd|ft)r>", RegexOptions.Singleline);
            xBodyXML = oRegex.Replace(xSegmentXML, "");

            string xDocLayoutValues = GetDocLayoutValues(xSegmentXML);
            //GLOG 5516
            string xDocBackgroundValues = GetPageBackgroundValues(xSegmentXML);

            //create regular expression that matches w:sectPr nodes
            //OLD REGEX - did not result in appropriate matches
            //oRegex = new Regex("<w:sectPr( .*|)/?>.*?</?w:sectPr>", RegexOptions.Singleline);
            oRegex = new Regex("(<w:sectPr .*?>|<w:sectPr?>).*?</?w:sectPr>", RegexOptions.Singleline);

            //cycle through <w:sectPr> nodes, building sections and page setup strings,
            //the former as xml, the latter as a delimited string
            StringBuilder oSBSections = new StringBuilder("<Sections>");
            StringBuilder oSBPageSetup = new StringBuilder();
            MatchCollection oMatches = oRegex.Matches(xSegmentXML);
            foreach (Match oMatch in oMatches)
            {
                //append xml for section
                string xSectionXML = oMatch.Value;
                oSBSections.Append(xSectionXML);

                //append page setup values - add sections separator if necessary
                if (oSBPageSetup.Length > 0)
                    oSBPageSetup.Append('|');
                oSBPageSetup.Append(GetPageSetupValues(xSectionXML, xDocLayoutValues));
            }

            //add end tag
            oSBSections.Append("</Sections>");


            //convert to strings
            xCompatOptions = GetCompatibilityOptions(xSegmentXML);
            xSectionsXML = oSBSections.ToString();
            xPageSetupValues = oSBPageSetup.ToString();
            LMP.Benchmarks.Print(t0);
        }
        protected static string GetPageBackgroundValues(string xXML)
        {
            //GLOG 5516: Parse Document Background settings from XML
            //TODO: Extend to support Gradients, Textures and Patterns -
            //currently just handles solid background
            StringBuilder oSB = new StringBuilder();
            Match oBackground = Regex.Match(xXML, "<w:background.*?/>");
            if (oBackground.Value != "")
            {
                string xHexColor = "";
                string xFillColor = "";
                if (LMP.String.IsWordOpenXML(xXML))
                {
                    //TODO: Commented code is start of Gradient background support
                    //Match oFill1 = Regex.Match(oBackground.Value, "<v:background.*?/>");
                    //if (oFill1.Value != "")
                    //{
                    //    xHexColor = xGetValue(oFill1.Value, "fillcolor");
                    //}
                    //else
                    xHexColor = xGetValue(oBackground.Value, "color");

                    //Match oFill2 = Regex.Match(oBackground.Value, "<v:fill.*?/>");
                    //if (oFill2.Value != "")
                    //{
                    //    xFillColor = xGetValue(oFill2.Value, "color2");
                    //    xAngle = xGetValue(oFill2.Value, "angle");
                    //    xGradient = xGetValue(oFill2.Value, "type");
                    //}
                }
                else
                {
                    //Different attribute name for Word 2003 XML
                    xHexColor = xGetValue(oBackground.Value, "bgcolor");
                }

                if (xHexColor != "")
                {
                    //In Word 2003 XML, Hex Number is preceded by '#'
                    xHexColor = xHexColor.Trim(new char[] { '#' });
                    xFillColor = xFillColor.Trim(new char[] { '#' });
                    if (xHexColor != "")
                    {
                        //For some reason, Hex Value appears reversed, except for last character, e.g., '05F4EA' becomes 'E4F50A'
                        char[] aHex = xHexColor.ToCharArray();
                        Array.Reverse(aHex, 0, aHex.Length - 1);
                        xHexColor = new string(aHex);
                    }
                    oSB.Append(int.Parse(xHexColor, System.Globalization.NumberStyles.HexNumber) + "�");
                    //TODO: Gradient background support
                    //if (xFillColor != "")
                    //{
                    //    char[] aHex = xFillColor.ToCharArray();
                    //    Array.Reverse(aHex, 0, aHex.Length - 1);
                    //    xFillColor = new string(aHex);
                    //    oSB.Append("?" + int.Parse(xFillColor, System.Globalization.NumberStyles.HexNumber) + "?" + xAngle + "?" + xGradient);
                    //}
                    //else
                    //    oSB.Append("???");
                }
                else
                {
                    oSB.Append("�");
                }
            }
            else
            {
                oSB.Append("�");
            }
            return oSB.ToString();
        }
        protected static string GetDocLayoutValues(string xSegmentXML)
        {
            DateTime t0 = DateTime.Now;

            StringBuilder oSB = new StringBuilder();

            Match oDocSettings = Regex.Match(xSegmentXML, "<w:docPr>.*?</w:docPr>");
            //GLOG 3722: Get additional Page Setup settings saved at Document Level
            if (oDocSettings.Value != "")
            {
                //GLOG 3722: Different Even/Odd Headers
                Match oEvenAndOdd = Regex.Match(oDocSettings.Value, "<w:evenAndOddHeaders/>");
                bool bEvenAndOdd = (oEvenAndOdd.Value != "");
                oSB.AppendFormat("{0}�", bEvenAndOdd ? "1" : "0");

                //GLOG 3722: Mirror Margins
                Match oMirror = Regex.Match(oDocSettings.Value, "<w:mirrorMargins/>");
                bool bMirror = (oMirror.Value != "");
                oSB.AppendFormat("{0}�", bMirror ? "1" : "0");

                //GLOG 3722: Book fold Printing
                Match oBookFold = Regex.Match(oDocSettings.Value, "<w:bookFoldPrinting/>");
                bool bBookFold = (oBookFold.Value != "");
                oSB.AppendFormat("{0}�", bBookFold ? "1" : "0");
                if (bBookFold)
                {
                    Match oBookFoldSheets = Regex.Match(oDocSettings.Value, "<w:bookFoldPrintingSheets.*?/>");
                    if (oBookFoldSheets.Value != "")
                    {
                        //Convert value to integer for Word PageSetup dialog, corresponding to index in list
                        //  0 - Auto
                        //  1 - All
                        //  2 - 4
                        //  3 - 8
                        //  4 - 12
                        //  ..and so on in multiples of 4
                        int iSheets = Int32.Parse(xGetValue(oBookFoldSheets.Value, "val"));
                        if (iSheets == -4)
                            iSheets = 0;
                        else
                            iSheets = (iSheets / 4) + 1;
                        oSB.AppendFormat("{0}�", iSheets.ToString());
                    }
                    else
                    {
                        //Default if missing from XML is "All"
                        oSB.Append("1�");
                    }
                }
                else
                {
                    oSB.Append("0�");
                }

                //GLOG 3722: Two pages per sheet
                Match oTwoPages = Regex.Match(oDocSettings.Value, "<w:printTwoOnOne/>");
                bool bTwoPages = (oTwoPages.Value != "");
                oSB.AppendFormat("{0}�", bTwoPages ? "1" : "0");

                //GLOG 3722: Gutter at Top
                Match oTopGutter = Regex.Match(oDocSettings.Value, "<w:gutterAtTop/>");
                bool bTopGutter = (oTopGutter.Value != "");
                oSB.AppendFormat("{0}�", bTopGutter ? "1" : "0"); //GLOG 5516

            }
            else
            {
                oSB.Append("0�0�0�0�0�0�"); //GLOG 5516
            }
            //GLOG 5516: Page Border settings
            Match oAlignBorders = Regex.Match(oDocSettings.Value, "<w:alignBordersAndEdges/>");
            bool bAlign = (oAlignBorders.Value != "");
            oSB.AppendFormat("{0}�", bAlign ? "1" : "0");


            //These settings are named differently in 2003 and WordOpen XML
            Match oSurroundHeader;
            if (LMP.String.IsWordOpenXML(xSegmentXML))
                oSurroundHeader = Regex.Match(oDocSettings.Value, "<w:bordersDontSurroundHeader/>");
            else
                oSurroundHeader = Regex.Match(oDocSettings.Value, "<w:bordersDoNotSurroundHeader/>");
            bool bSurroundHeader = (oSurroundHeader.Value != "");
            oSB.AppendFormat("{0}�", bSurroundHeader ? "0" : "1");

            Match oSurroundFooter;
            if (LMP.String.IsWordOpenXML(xSegmentXML))
                oSurroundFooter = Regex.Match(oDocSettings.Value, "<w:bordersDontSurroundFooter/>");
            else
                oSurroundFooter = Regex.Match(oDocSettings.Value, "<w:bordersDoNotSurroundFooter/>");
            bool bSurroundFooter = (oSurroundFooter.Value != "");
            oSB.AppendFormat("{0}�", bSurroundFooter ? "0" : "1");
            LMP.Benchmarks.Print(t0);

            return oSB.ToString();
        }
        /// <summary>
        /// returns a delimited string of page setup
        /// values from the specified section xml
        /// </summary>
        /// <param name="xSectionXML"></param>
        /// <returns></returns>
        protected static string GetPageSetupValues(string xSectionXML, string xDocLayoutValues)
        {
            //GLOG 3722: Returns delimited string containing these Page Setup settings.
            //These correspond to parameters of WordBasic.PageSetupMargins command:
            //  1. Orientation
            //  2. Height
            //  3. Width
            //  4. Paper Size
            //  5. Left Margin
            //  6. Right Margin
            //  7. Top Margin
            //  8. Bottom Margin
            //  9. Header Distance
            // 10. Footer Distance
            // 11. Gutter Distance
            // 12. Vertical Alignment
            // 13. Different First Page
            // 14. Page 1 Tray
            // 15. Other Pages Tray
            // 16. Show Line Numbers
            // 17. Starting Number
            // 18. Count By
            // 19. Restart mode
            // 20. Distance from text
            //
            //Settings from DocPr XML:
            // 21. Different Even and Odd
            // 22. Mirror Margins
            // 23. Bookfold Printing
            // 24. Bookfold sheets
            // 25. Two per page
            // 26. Gutter at top

            //Page Border Settings (GLOG 5516)
            //From DocPr:
            // 27. Align Borders and Edges
            // 28. Surround Headers
            // 29. Surround Footers

            //From SectPr:
            // 30. Offset
            // 31. Display first page
            // 32. Display other pages
            // 33. Z-Order
            // 34. Top Style
            // 35. Top width
            // 36. Top distance
            // 37. Top color
            // 38. Top Shadow
            // 39. Left Style
            // 40. Left width
            // 41. Left distance
            // 42. Left color
            // 43. Left Shadow
            // 44. Bottom Style
            // 45. Bottom width
            // 46. Bottom distance
            // 47. Bottom color
            // 48. Bottom Shadow
            // 49. Right Style
            // 50. Right width
            // 51. Right distance
            // 52. Right color
            // 53. Right Shadow

            DateTime t0 = DateTime.Now;

            StringBuilder oSB = new StringBuilder();
            //GLOG 4230: Strip out footer/header XML to ensure only section-specific nodes are being evaluated.
            //For instance, individual table cells might contain v:Align node, which we don't want applied to
            //page setup.
            //GLOG 4537: Need to adjust Regex to ensure all hdr/ftr nodes were included, not just the first.
            //Singleline option is required, because pictures objects in the header or footer will result in
            //embedded \n characters, which will stop continued matching by default
            Match oStrip = Regex.Match(xSectionXML, "<w:(ftr|hdr)(.*</w:(ftr|hdr)>)*", RegexOptions.Singleline);
            if (oStrip != null && !string.IsNullOrEmpty(oStrip.Value))
                xSectionXML = xSectionXML.Remove(oStrip.Index, oStrip.Length);

            string[] aDocLayoutProps = xDocLayoutValues.Split('�');
            //get page size values
            Match oPageSize = Regex.Match(xSectionXML, "<w:pgSz.*?/>");
            if (oPageSize.Value != "")
            {
                //GLOG 3722: Include xml for Orientation
                string xOrient = xGetValue(oPageSize.Value, "orient");
                if (!string.IsNullOrEmpty(xOrient) && xOrient.ToUpper() == "LANDSCAPE")
                    oSB.Append("1�");
                else
                    oSB.Append("0�");

                int iPgHeight = Int32.Parse(xGetValue(oPageSize.Value, "h"));
                int iPgWidth = Int32.Parse(xGetValue(oPageSize.Value, "w"));
                if (aDocLayoutProps[4] == "1")
                {
                    //if 2-on-one is enabled, actual height or width will be double value in XML
                    if (xOrient.ToUpper() == "LANDSCAPE")
                        iPgWidth = iPgWidth * 2;
                    else
                        iPgHeight = iPgHeight * 2;
                }
                else if (aDocLayoutProps[2] == "1")
                {
                    //If bookfold printing enabled, actual width will be double value in XML
                    iPgWidth = iPgWidth * 2;
                }

                oSB.AppendFormat("{0}�", iPgHeight.ToString());
                oSB.AppendFormat("{0}�", iPgWidth.ToString());
                //GLOG 3722: Include xml for Paper Size code
                string xCode = xGetValue(oPageSize.Value, "code");
                if (!string.IsNullOrEmpty(xCode))
                    oSB.AppendFormat("{0}�", xCode);
                else
                    oSB.Append("1�");
            }
            else
                //Use default settings (but I think pgSz XML will always be present)
                oSB.Append("0�12240�15840�1�");

            //get page margin values
            Match oPageMargins = Regex.Match(xSectionXML, "<w:pgMar.*?/>");
            if (oPageMargins.Value != "")
            {
                try
                {
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "left"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "right"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "top"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "bottom"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "header"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "footer"));
                    oSB.AppendFormat("{0}�", xGetValue(oPageMargins.Value, "gutter"));
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_InvalidPageSetupValue"), oE);
                }
            }
            else
            {
                //Use default values
                oSB.Append("1440�1440�1440�1440�720�720�0�");
            }
            //GLOG 3722: Include xml for Page Alignment
            Match oAlign = Regex.Match(xSectionXML, "<w:vAlign.*?/>");
            if (oAlign.Value != "")
            {
                try
                {
                    //Convert XML value to integer for Page Setup dialog
                    string xAlign = xGetValue(oAlign.Value, "val");
                    int iAlign = 0;
                    switch (xAlign.ToUpper())
                    {
                        case "CENTER":
                            iAlign = 1;
                            break;
                        case "BOTH":
                            iAlign = 2;
                            break;
                        case "BOTTOM":
                            iAlign = 3;
                            break;
                    }
                    oSB.AppendFormat("{0}�", iAlign.ToString());
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_InvalidPageSetupValue"), oE);
                }
            }
            else
                oSB.Append("0�");

            //get different first page value
            Match oTitlePage = Regex.Match(xSectionXML, "<w:titlePg.*?/>");
            bool bTitlePage = (oTitlePage.Value != "");
            oSB.AppendFormat("{0}�", bTitlePage ? "1" : "0");

            //get paper tray settings
            Match oPaperSrc = Regex.Match(xSectionXML, "<w:paperSrc.*?/>");
            if (oPaperSrc.Value != "")
            {
                string xFirst = xGetValue(oPaperSrc.Value, "first");
                oSB.AppendFormat("{0}�", xFirst);
                string xOther = xGetValue(oPaperSrc.Value, "other");
                oSB.AppendFormat("{0}�", xOther);
            }
            else
            {
                oSB.Append("0�0�");
            }

            //get paper tray settings
            Match oLineNumbers = Regex.Match(xSectionXML, "<w:lnNumType.*?/>");
            if (oLineNumbers.Value != "")
            {
                oSB.Append("1�");
                //starting number in XML is one less than actual value
                string xStart = xGetValue(oLineNumbers.Value, "start");
                if (!string.IsNullOrEmpty(xStart))
                {
                    oSB.AppendFormat("{0}�", Int32.Parse(xStart) + 1);
                }
                else
                {
                    oSB.Append("1�");
                }

                oSB.AppendFormat("{0}�", xGetValue(oLineNumbers.Value, "count-by"));

                string xNumMode = xGetValue(oLineNumbers.Value, "restart");
                if (!string.IsNullOrEmpty(xNumMode))
                {
                    //Default restart numbering each page
                    int iRestart = 0;
                    if (xNumMode == "new-section")
                        iRestart = 1;
                    else if (xNumMode == "continuous")
                        iRestart = 2;
                    oSB.AppendFormat("{0}�", iRestart);
                }
                else
                {
                    oSB.Append("0�");
                }
                string xDistance = xGetValue(oLineNumbers.Value, "distance");
                if (!string.IsNullOrEmpty(xDistance))
                {
                    oSB.AppendFormat("{0}�", xDistance);
                }
                else
                {
                    oSB.Append("0�");
                }

            }
            else
            {
                oSB.Append("0�1�1�0�0�");
            }
            oSB.Append(xDocLayoutValues);
            //GLOG 5516: Parse Page Border options from XML
            //TODO: Extend to support Border Art options
            Match oPageBorders = Regex.Match(xSectionXML, "<w:pgBorders.*?</w:pgBorders>");
            if (oPageBorders.Value != "")
            {
                string xTest = xGetValue(oPageBorders.Value, "offsetFrom");
                if (xTest == "page")
                    //Offset from page
                    oSB.Append("1�");
                else
                    //Offset from text
                    oSB.Append("0�");

                //Enable First Page and Other Pages Options
                xTest = xGetValue(oPageBorders.Value, "display");
                if (xTest == "firstPage" || xTest == "first-page")
                    oSB.Append("-1�0�");
                else if (xTest == "notFirstPage" || xTest == "not-first-page")
                    oSB.Append("0�-1�");
                else
                    oSB.Append("-1�-1�");

                //Always in Front value
                xTest = xGetValue(oPageBorders.Value, "zorder");
                //Check Word 2003 attribute name also
                if (string.IsNullOrEmpty(xTest))
                    xTest = xGetValue(oPageBorders.Value, "z-order");
                if (xTest == "back")
                    oSB.Append("0�");
                else
                    oSB.Append("1�");

                string[] xBorders = new string[] { "top", "left", "bottom", "right" };
                foreach (string xBorder in xBorders)
                {
                    Match oBorder = Regex.Match(oPageBorders.Value, "<w:" + xBorder + ".*?/>");
                    if (oBorder.Value != "")
                    {
                        xTest = xGetValue(oBorder.Value, "val");
                        int iLineType = 0;
                        //test both WordOpen and 2003 variants
                        switch (xTest)
                        {
                            case "single":
                                iLineType = 1;
                                break;
                            case "dotted":
                                iLineType = 2;
                                break;
                            case "dashSmallGap":
                            case "dash-small-gap":
                                iLineType = 3;
                                break;
                            case "dashed":
                                iLineType = 4;
                                break;
                            case "dotDash":
                            case "dot-dash":
                                iLineType = 5;
                                break;
                            case "dotDotDash":
                            case "dot-dot-dash":
                                iLineType = 6;
                                break;
                            case "double":
                                iLineType = 7;
                                break;
                            case "triple":
                                iLineType = 8;
                                break;
                            case "thickThinSmallGap":
                            case "thick-thin-small-gap":
                                iLineType = 9;
                                break;
                            case "thinThickSmallGap":
                            case "thin-thick-small-gap":
                                iLineType = 10;
                                break;
                            case "thinThickThinSmallGap":
                            case "thin-thick-thin-small-gap":
                                iLineType = 11;
                                break;
                            case "thinThickMediumGap":
                            case "thin-thick-medium-gap":
                                iLineType = 12;
                                break;
                            case "thickThinMediumGap":
                            case "thick-thin-medium-gap":
                                iLineType = 13;
                                break;
                            case "thinThickThinMediumGap":
                            case "thin-thick-thin-medium-gap":
                                iLineType = 14;
                                break;
                            case "thinThickLargeGap":
                            case "thin-thick-large-gap":
                                iLineType = 15;
                                break;
                            case "thickThinLargeGap":
                            case "thick-thin-large-gap":
                                iLineType = 16;
                                break;
                            case "thinThickThinLargeGap":
                            case "thin-thick-thin-large-gap":
                                iLineType = 17;
                                break;
                            case "wave":
                                iLineType = 18;
                                break;
                            case "doubleWave":
                            case "double-wave":
                                iLineType = 19;
                                break;
                            case "dashDotStroked":
                            case "dash-dot-stroked":
                                iLineType = 20;
                                break;
                            case "threeDEmboss":
                            case "three-d-emboss":
                                iLineType = 21;
                                break;
                            case "threeDEngrave":
                            case "three-d-engrave":
                                iLineType = 22;
                                break;
                            case "outset":
                                iLineType = 23;
                                break;
                            case "inset":
                                iLineType = 24;
                                break;
                        }
                        string xSize = xGetValue(oBorder.Value, "sz");
                        string xSpace = xGetValue(oBorder.Value, "space");
                        string xColor = xGetValue(oBorder.Value, "color");
                        //TODO: OpenXML rewrite
                        //int iColor = (int)Word.WdColor.wdColorAutomatic;
                        //if (!string.IsNullOrEmpty(xColor) && xColor != "auto")
                        //{
                        //    //For some reason, Hex Value appears reversed, except for last character, e.g., '05F4EA' becomes 'E4F50A'
                        //    char[] aHex = xColor.ToCharArray();
                        //    Array.Reverse(aHex, 0, aHex.Length - 1);
                        //    xColor = new string(aHex);
                        //    try
                        //    {
                        //        iColor = int.Parse(xColor, System.Globalization.NumberStyles.HexNumber);
                        //    }
                        //    catch { }
                        //}

                        string xShadow = xGetValue(oBorder.Value, "shadow");
                        int iShadow = (xShadow == "1" || xShadow == "on") ? 1 : 0;
                        //TODO: OpenXML rewrite
                        //oSB.AppendFormat("{0}?{1}?{2}?{3}?{4}?", iLineType, xSize, xSpace, iColor, iShadow);
                    }
                    else
                        oSB.Append("�����");
                }
            }
            else
                oSB.Append("������������������������");
            LMP.Benchmarks.Print(t0);
            return oSB.ToString();
        }

        public static string GetCompatibilityOptions(string xSegmentXML)
        {
            //GLOG 4967 (dm) - open xml has multiple w:compat nodes -
            //make sure to start with the right one
            string xXML = null;
            bool bWordOpenXML = false;
            if (LMP.String.IsWordOpenXML(xSegmentXML))
            {
                bWordOpenXML = true;
                int iPos = xSegmentXML.IndexOf("<pkg:part pkg:name=\"/word/settings.xml\"");
                xXML = xSegmentXML.Substring(iPos);
            }
            else
                xXML = xSegmentXML;

            Regex oRegex = new Regex("<w:compat>.*?</w:compat>");
            Match oCompat = oRegex.Match(xXML);
            string xCompatXML = "";
            string xCompatValues = "";
            string xFormat = "{0}�{1}";
            StringBuilder oSB = new StringBuilder();
            if (oCompat != null && oCompat.Value != "")
            {
                int iIndex;
                bool bValue;
                XmlDocument oXML = new XmlDocument();
                xCompatXML = oCompat.Value.Replace("w:", "");
                oXML.LoadXml(xCompatXML);
                foreach (XmlNode oNode in oXML.DocumentElement.ChildNodes)
                {
                    iIndex = 0;
                    //Default is false for all but 5 items - presence of node indicates change from default
                    bValue = true;
                    switch (oNode.Name)
                    {
                        case "suppressTopSpacing":
                            iIndex = 8;
                            break;
                        case "suppressBottomSpacing":
                            iIndex = 29;
                            break;
                        case "wrapTrailSpaces":
                            iIndex = 4;
                            break;
                        case "printColBlack":
                            iIndex = 3;
                            break;
                        case "usePrinterMetrics":
                            iIndex = 26;
                            break;
                        //GLOG 5709: Checked state in UI corresponds to False setting for compatibility option
                        case "doNotExpandShiftReturn":
                            iIndex = 14;
                            bValue = false;
                            break;
                        //GLOG 5112: The following settings don't default to True in WordOpenXML -
                        //only set if corresonding Tag is present
                        case "breakWrappedTables":
                            if (bWordOpenXML)
                                iIndex = 43;
                            break;
                        case "snapToGridInCell":
                            if (bWordOpenXML)
                                iIndex = 44;
                            break;
                        case "wrapTextWithPunct":
                            if (bWordOpenXML)
                                iIndex = 47;
                            break;
                        case "useAsianBreakRules":
                            if (bWordOpenXML)
                                iIndex = 48;
                            break;
                        case "dontGrowAutoFit":
                            if (bWordOpenXML)
                                iIndex = 50;
                            break;
                        default:
                            break;
                    }
                    if (iIndex > 0)
                    {
                        oSB.Append(string.Format(xFormat, iIndex, bValue));
                        oSB.Append("|");
                    }
                }
            }
            //GLOG 5112: These settings are handled differently in old-style XML
            if (!bWordOpenXML)
            {
                //Default for these 5 is true - 
                //if missing from XML, corresponding option needs to be set to true
                if (xCompatXML.IndexOf("breakWrappedTables") == -1)
                {
                    oSB.Append(string.Format(xFormat, 43, true));
                    oSB.Append("|");
                }
                if (xCompatXML.IndexOf("snapToGridInCell") == -1)
                {
                    oSB.Append(string.Format(xFormat, 44, true));
                    oSB.Append("|");
                }
                if (xCompatXML.IndexOf("wrapTextWithPunct") == -1)
                {
                    oSB.Append(string.Format(xFormat, 47, true));
                    oSB.Append("|");
                }
                if (xCompatXML.IndexOf("useAsianBreakRules") == -1)
                {
                    oSB.Append(string.Format(xFormat, 48, true));
                    oSB.Append("|");
                }
                if (xCompatXML.IndexOf("dontGrowAutofit") == -1)
                {
                    oSB.Append(string.Format(xFormat, 50, true));
                    oSB.Append("|");
                }

            }
            xCompatValues = oSB.ToString();
            if (xCompatValues != "")
                xCompatValues = xCompatValues.TrimEnd('|');
            return xCompatValues;
        }
        /// <summary>
        /// Converts delimited list of levels into string array
        /// </summary>
        /// <param name="xValue"></param>
        /// <returns></returns>
        public static int[] GetJurisdictionArray(string xValue)
        {
            //Initialize all levels to 0
            int[] xLevels = new int[] { 0, 0, 0, 0, 0 };
            string[] xL = xValue.Split(StringArray.mpEndOfRecord);

            for (int i = 0; i <= xL.GetUpperBound(0); i++)
            {
                try
                {
                    xLevels[i] = Int32.Parse(xL[i]);
                }
                catch { }
            }
            return xLevels;
        }
        /// <summary>
        /// True if Segment is set up to be shell for other Segments of the same type
        /// </summary>
        /// <param name="oSeg"></param>
        /// <returns></returns>
        public static bool IsShellSegment(XmlSegment oSeg)
        {
            return oSeg.IntendedUse == mpSegmentIntendedUses.AsDocument && oSeg.ShowChooser == true
                && oSeg.Variables.Count == 0 && oSeg.Blocks.Count == 0 &&
                oSeg.MaxAuthors == 0;
        }
        /// <summary>
        /// appends the specified contacts to each CI-enabled Variable
        /// </summary>
        /// <param name="oContacts"></param>
        private void SetCIEnabledVariables(ICContacts oContacts)
        {
            SetCIEnabledVariables(this.Variables.CIEnabledVariables, oContacts, true);
            //GLOG 5940: Child Segment handling moved to overload
        }

        /// <summary>
        /// adds the specified contacts to each CI-enabled Variable
        /// </summary>
        /// <param name="oContacts"></param>
        /// <param name="bAppend">if true, contacts will be appended</param>
        private void SetCIEnabledVariables(List<XmlVariable> oVarList, ICContacts oContacts, bool bAppend)
        {
            //GLOG 5940: Child Segments are now handled by this method, so that children of children will be included
            if (oContacts == null)
                return;

            foreach (XmlVariable oVar in oVarList)
            {
                //refer back to variables collection because oVar will no longer
                //be current if a variable action of the previous variable in the cycle
                //has forced a segment refresh, e.g. following the restoration of
                //a deleted scope that has multiple variables in the scope (dm 5/18/09)
                this.Variables.ItemFromName(oVar.Name).SetValueFromContacts(
                    oContacts, bAppend);
            }
            // Populate CI-Enabled variables of Child Segments also
            for (int i = 0; i < this.Segments.Count; i++)
                this.Segments[i].SetCIEnabledVariables(this.Segments[i].Variables.CIEnabledVariables, oContacts, true);
        }

        /// <summary>
        /// Retrieve Max Contacts information by cycling through all CI-Enabled variables
        /// </summary>
        /// <param name="iMax"></param>
        /// <param name="iToMax"></param>
        /// <param name="iFromMax"></param>
        /// <param name="iCCMax"></param>
        /// <param name="iBCCMax"></param>
        private void GetCIMaxContacts(ref int iMax, ref int iToMax,
                                    ref int iFromMax, ref int iCCMax,
                                    ref int iBCCMax, ref int iOtherMax)
        {
            this.GetCIVariableInfo(this.Variables.CIEnabledVariables, ref iMax, ref iToMax,
                                   ref iFromMax, ref iCCMax, ref iBCCMax, ref iOtherMax);

            // Check child segments for additional ci-enabled variables
            for (int j = 0; j < this.Segments.Count; j++)
                this.Segments[j].GetCIMaxContacts(ref iMax, ref iToMax, ref iFromMax, ref iCCMax, ref iBCCMax, ref iOtherMax);
        }

        /// <summary>
        /// Retrieve Display Name by cycling through all CI-Enabled variables
        /// </summary>
        /// <param name="xToLabel"></param>
        /// <param name="xFromLabel"></param>
        /// <param name="xCCLabel"></param>
        /// <param name="xBCCLabel"></param>
        private void GetCIDisplayName(ref string xToLabel, ref string xFromLabel,
                                    ref string xCCLabel, ref string xBCCLabel)
        {
            this.GetCIVariableInfo(this.Variables.CIEnabledVariables, ref xToLabel,
                                   ref xFromLabel, ref xCCLabel, ref xBCCLabel);

            // Check child segments for additional ci-enabled variables
            for (int j = 0; j < this.Segments.Count; j++)
                this.Segments[j].GetCIDisplayName(ref xToLabel, ref xFromLabel, ref xCCLabel, ref xBCCLabel);
        }

        /// <summary>
        /// Get Display Name by cycling through all variables in List collection
        /// </summary>
        /// <param name="oVarList"></param>
        /// <param name="xToLabel"></param>
        /// <param name="xFromLabel"></param>
        /// <param name="xCCLabel"></param>
        /// <param name="xBCCLabel"></param>
        private void GetCIVariableInfo(List<XmlVariable> oVarList, ref string xToLabel, ref string xFromLabel,
                                       ref string xCCLabel, ref string xBCCLabel)
        {
            //cycle through each ci-enabled variable
            foreach (XmlVariable oVar in oVarList)
            {
                switch (oVar.CIContactType)
                {
                    case ciContactTypes.ciContactType_To:
                        if (oVar.DisplayName != "")
                            xToLabel = LMP.String.HotKeyString(oVar.DisplayName, oVar.Hotkey) + ":";
                        break;
                    case ciContactTypes.ciContactType_From:
                        if (oVar.DisplayName != "")
                            xFromLabel = LMP.String.HotKeyString(oVar.DisplayName, oVar.Hotkey) + ":";
                        break;
                    case ciContactTypes.ciContactType_CC:
                        if (oVar.DisplayName != "")
                            xCCLabel = LMP.String.HotKeyString(oVar.DisplayName, oVar.Hotkey) + ":";
                        break;
                    case ciContactTypes.ciContactType_BCC:
                        if (oVar.DisplayName != "")
                            xBCCLabel = LMP.String.HotKeyString(oVar.DisplayName, oVar.Hotkey) + ":";
                        break;
                }
            }
        }

        /// <summary>
        /// Get Max Contacts by cycling through all variables in List collection
        /// </summary>
        /// <param name="oVarList"></param>
        /// <param name="iMax"></param>
        /// <param name="iToMax"></param>
        /// <param name="iFromMax"></param>
        /// <param name="iCCMax"></param>
        /// <param name="iBCCMax"></param>
        private void GetCIVariableInfo(List<XmlVariable> oVarList, ref int iMax, ref int iToMax,
                                       ref int iFromMax, ref int iCCMax, ref int iBCCMax, ref int iOtherMax)
        {
            string[] aProps = null;

            //cycle through each variable in oVarlist
            foreach (XmlVariable oVar in oVarList)
            {
                //#3503 - ceh 
                //limit max contacts for variable controls of type 'Detail Grid'
                if (oVar.ControlType == mpControlTypes.DetailGrid && oVar.ControlProperties.IndexOf("MaxEntities=") != -1)
                {
                    //split into array
                    aProps = oVar.ControlProperties.Split(StringArray.mpEndOfSubValue);

                    //cycle through property name/value pairs
                    for (int j = 0; j < aProps.Length; j++)
                    {
                        int iPos = aProps[j].IndexOf("MaxEntities=");
                        if (iPos != -1)
                        {
                            //get value
                            string xValue = aProps[j].Substring(12);

                            //evaluate control prop value, which may be a MacPac expression
                            xValue = XmlExpression.Evaluate(xValue, this, this.ForteDocument);

                            //convert to integer
                            iMax = Int32.Parse(xValue);

                            if (iMax == 0)
                                break;

                            //check AssociatedCcontrol count and
                            //update iMax as necessary
                            int iCurrentCount = 0;

                            //find last instance of Index
                            //GLOG 7597:  Create AssociatedControl if necessary
                            int iNewPos = -1;
                            IControl oCIControl = null;
                            if (oVar.AssociatedControl != null)
                            {
                                oCIControl = oVar.AssociatedControl;
                            }
                            else
                            {
                                oCIControl = oVar.CreateAssociatedControl(null);
                            }
                            iNewPos = oCIControl.Value.LastIndexOf("Index=");
                            if (iNewPos != -1)
                            {
                                //get current index value
                                string xNewValue = oCIControl.Value.Substring(iNewPos + 7); //GLOG 7597
                                iNewPos = xNewValue.IndexOf("\"");
                                iCurrentCount = Int32.Parse(xNewValue.Substring(0, iNewPos));
                            }
                            else
                            {
                                iMax = iMax - iCurrentCount;
                                break;
                            }
                            //update iMax
                            iMax = iMax - iCurrentCount;
                            if (iMax <= 0)
                                iMax = -1;
                            break;
                        }
                    }
                }
                else
                {
                    iMax = 0;
                }

                //GLOG - 3503 - ceh
                //don't reset max property if variable is not visible
                if (oVar.DisplayIn == XmlVariable.ControlHosts.None)
                    return;

                switch (oVar.CIContactType)
                {
                    case ciContactTypes.ciContactType_To:
                        iToMax = iMax;
                        break;
                    case ciContactTypes.ciContactType_From:
                        iFromMax = iMax;
                        break;
                    case ciContactTypes.ciContactType_CC:
                        iCCMax = iMax;
                        break;
                    case ciContactTypes.ciContactType_BCC:
                        iBCCMax = iMax;
                        break;
                    case ciContactTypes.ciContactType_Other:
                        iOtherMax = iMax;
                        break;
                }
            }
        }

        /// <summary>
        /// Select CI Data to retrieve by cycling through all CI-Enabled variables
        /// </summary>
        /// <param name="iDataTo"></param>
        /// <param name="iDataFrom"></param>
        /// <param name="iDataCC"></param>
        /// <param name="iDataBCC"></param>
        /// <param name="iAlerts"></param>
        public void GetCIFormat(ref ciRetrieveData iDataTo, ref ciRetrieveData iDataFrom,
            ref ciRetrieveData iDataCC, ref ciRetrieveData iDataBCC, ref object[,] aCustomEntities, ref ciAlerts iAlerts)
        {
            //GLOG 3461: We're no longer saving these values between calls
            //CI-enabled variables may have been added or removed since previous call
            //// If Format has already been retrieved just return current values
            //if (m_bCIFormatRetrieved)
            //{
            //    iDataTo = iDataTo | m_iRetrieveDataTo;
            //    iDataFrom = iDataFrom | m_iRetrieveDataFrom;
            //    iDataCC = iDataCC | m_iRetrieveDataCC;
            //    iDataBCC = iDataBCC | m_iRetrieveDataBCC;
            //    aCustomEntities = m_aCustomEntities;
            //    iAlerts = iAlerts | m_iCIAlerts;
            //    return;
            //}
            XmlContacts.GetCIFormat(this.Variables.CIEnabledVariables, ref iDataTo, ref iDataFrom, ref iDataCC,
                ref iDataBCC, ref aCustomEntities, ref iAlerts);

            // Check child segments for additional ci-enabled variables
            for (int i = 0; i < this.Segments.Count; i++)
                this.Segments[i].GetCIFormat(ref iDataTo, ref iDataFrom, ref iDataCC,
                    ref iDataBCC, ref aCustomEntities, ref iAlerts);
        }

        /// <summary>
        /// save author info to Segment mSEGs
        /// </summary>
        private void SaveAuthorsDetail(XmlSegment oSegment)
        {
            DateTime t0 = DateTime.Now;

            //save author xml
            foreach (RawSegmentPart oPart in oSegment.RawSegmentParts)
            {
                //TODO: address encryption of authors
                Query.SetAttributeValue(oSegment.WPDoc, oPart.ContentControl, "Authors", oSegment.Authors.XML, false, null);
            }
        }

        /// <summary>
        /// returns TRUE if the current user is on the list specified for
        /// this segment's author control
        /// </summary>
        /// <returns></returns>
        private bool UserIsOnAuthorList()
        {
            LMP.Controls.mpListTypes iType = LMP.Controls.mpListTypes.AllAuthors;
            string xACP = this.AuthorControlProperties;
            if (xACP != null && xACP.Contains("ListType="))
            {
                //author list is filtered - get list type
                string[] aCP = xACP.Split(StringArray.mpEndOfSubValue);
                for (int i = 0; i <= aCP.GetUpperBound(0); i++)
                {
                    if (aCP[i].StartsWith("ListType="))
                    {
                        try
                        {
                            iType = (LMP.Controls.mpListTypes)(Int32.Parse(aCP[i].Substring(9)));
                        }
                        catch { }
                        break;
                    }
                }
            }

            //determine whether list type includes user
            Person oUser = LMP.Data.Application.User.PersonObject;
            if (iType == LMP.Controls.mpListTypes.Attorneys)
                return oUser.IsAttorney;
            else if (iType == LMP.Controls.mpListTypes.NonAttorneys)
                return !oUser.IsAttorney;
            else if (iType == LMP.Controls.mpListTypes.AllAuthors)
                return oUser.IsAuthor;
            else
                return true;
        }
        /// <summary>
        /// Get full path and name of Template to use in creating new blank document for segment
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="xSourceXML"></param>
        /// <returns></returns>
        public static string GetNewDocumentTemplate(string xID, string xSourceXML)
        {
            //GLOG 3088
            DateTime t0 = DateTime.Now;
            try
            {
                ISegmentDef oDef = XmlSegment.GetDefFromID(xID);
                if (string.IsNullOrEmpty(xSourceXML))
                {
                    //if XML not specified get XML from the segment def for this segment
                    xSourceXML = oDef.XML;
                }
                //GLOG 8655: If Saved Content, look for ID of contained Segment
                if (oDef.TypeID == mpObjectTypes.SavedContent)
                {
                    xID = oDef.ChildSegmentIDs;
                }
                if (xID.EndsWith(".0"))
                    xID = xID.Substring(0, xID.Length - 2);

                if (String.IsBase64SegmentString(xSourceXML))
                {
                    //convert to flat xml
                    xSourceXML = XmlSegment.GetFlatXmlFromBase64OpcString(xSourceXML);
                }

                //Create XML Document from XML
                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(xSourceXML);
                XmlNamespaceManager oNmSpcMgr = new XmlNamespaceManager(oXML.NameTable);

                //10.2 (dm)
                bool bSeekContentControl = (LMP.String.IsWordOpenXML(xSourceXML));
                string xXPath = "";
                if (bSeekContentControl)
                {
                    oNmSpcMgr.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                    xXPath = "//w:sdtPr/w:tag";
                }
                else
                {
                    string xNSPrefix = String.GetNamespacePrefix(xSourceXML, ForteConstants.MacPacNamespace);
                    oNmSpcMgr.AddNamespace(xNSPrefix, ForteConstants.MacPacNamespace);
                    xXPath = "//" + xNSPrefix + ":mSEG[@ObjectData]";
                }

                //get all mSEG nodes
                XmlNodeList oMSegNodes = oXML.SelectNodes(xXPath, oNmSpcMgr);
                foreach (XmlNode oNode in oMSegNodes)
                {
                    //Check ObjectData for SegmentID match
                    string xObjectData = null;
                    if (bSeekContentControl)
                    {
                        //content control - get object data from associated doc var
                        string xTag = oNode.Attributes["w:val"].Value;
                        if (LMP.String.GetBoundingObjectBaseName(xTag) == "mSEG")
                        {
                            string xDocVarID = LMP.String.GetDocVarIDFromTag(xTag);
                            xXPath = "//w:docVar[@w:name='mpo" + xDocVarID + "']";
                            XmlNode oDVNode = oXML.SelectSingleNode(xXPath, oNmSpcMgr);
                            if (oDVNode != null)
                                xObjectData = oDVNode.Attributes["w:val"].Value;
                        }
                    }
                    else
                        xObjectData = oNode.Attributes["ObjectData"].Value;

                    if (!(string.IsNullOrEmpty(xObjectData)))
                    {
                        //Need to make sure string is decrypted
                        xObjectData = LMP.String.Decrypt(xObjectData);
                        if (xObjectData.Contains("SegmentID=" + xID + "|"))
                        {
                            //Locate WordTemplate property in ObjectData
                            int iStart = xObjectData.IndexOf("|WordTemplate=");
                            if (iStart > -1)
                            {
                                iStart = iStart + @"|WordTemplate=".Length;
                                int iEnd = xObjectData.IndexOf("|", iStart);
                                if (iEnd > iStart)
                                {
                                    string xTemplate = xObjectData.Substring(iStart, iEnd - iStart);
                                    //If Property contains fieldcode, can't evaluate here, since the 
                                    //corresponding Segment and.ForteDocument objects don't exist yet.
                                    //Return default template and Segment.Insert will handle
                                    //evaluating and attaching to template after insertion
                                    //GLOG 3203: Skip if WordTemplate value is blank
                                    if (xTemplate != "" && !(xTemplate.Contains("[") && xTemplate.Contains("]")))
                                    {
                                        //Make sure template extension is appropriate for Word version
                                        if (xTemplate.ToLower().EndsWith(".dot"))
                                            xTemplate = xTemplate + "x";
                                        //If no path specified, used MacPac Templates directory
                                        if (System.IO.Path.GetDirectoryName(xTemplate) == string.Empty)
                                        {
                                            //Use default templates location
                                            xTemplate = Data.Application.TemplatesDirectory + @"\" + @xTemplate;
                                        }
                                        //Check that specified template actually exists
                                        if (System.IO.File.Exists(xTemplate))
                                            return xTemplate;
                                    }
                                }
                            }
                            //SegmentID found, but no word template specified
                            break;
                        }
                    }
                }
                //return default template (ForteNormal.dotx)
                return LMP.Data.Application.BaseTemplate;
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }

        }
        /// <summary>
        /// Get Segment ID from first defined mSEG in XML
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="xSourceXML"></param>
        /// <returns></returns>
        public static string GetSegmentIDFromXML(string xSourceXML)
        {
            //GLOG 3088
            DateTime t0 = DateTime.Now;
            try
            {
                if (string.IsNullOrEmpty(xSourceXML))
                    return "";

                //Create XML Document from XML
                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(xSourceXML);
                XmlNamespaceManager oNmSpcMgr = new XmlNamespaceManager(oXML.NameTable);
                if (LMP.String.IsWordOpenXML(xSourceXML))
                {
                    //get tag of first content control
                    string xNSPrefix = String.GetNamespacePrefix(xSourceXML, ForteConstants.WordOpenXMLNamespace);
                    oNmSpcMgr.AddNamespace(xNSPrefix, ForteConstants.WordOpenXMLNamespace);
                    XmlNode oNode = oXML.SelectSingleNode("//" + xNSPrefix +
                        ":tag[starts-with(@w:val,'mps')]", oNmSpcMgr);
                    if (oNode != null)
                    {
                        //search for associated doc var
                        string xTag = oNode.Attributes["w:val"].Value;
                        string xDocVar = "mpo" + xTag.Substring(3, 8);
                        XmlNode oDocVarNode = oXML.SelectSingleNode(
                            "//w:docVar[@w:name='" + xDocVar + "']", oNmSpcMgr);
                        if (oDocVarNode != null)
                        {
                            string xValue = oDocVarNode.Attributes["w:val"].Value;
                            int iPos = xValue.IndexOf("SegmentID=");
                            if (iPos > -1)
                            {
                                iPos += 10;
                                int iPos2 = xValue.IndexOf('|', iPos);
                                string xID = xValue.Substring(iPos, iPos2 - iPos);
                                return xID;
                            }
                        }
                    }
                }
                else
                {
                    string xNSPrefix = String.GetNamespacePrefix(xSourceXML, ForteConstants.MacPacNamespace);
                    oNmSpcMgr.AddNamespace(xNSPrefix, ForteConstants.MacPacNamespace);

                    //get all mSEG nodes
                    XmlNode oNode = oXML.SelectSingleNode("//" + xNSPrefix + ":mSEG[@ObjectData]", oNmSpcMgr);
                    if (oNode != null)
                    {
                        //Look in ObjecData for SegmentID
                        string xObjectData = oNode.Attributes["ObjectData"].Value;
                        if (!(string.IsNullOrEmpty(xObjectData)))
                        {
                            //Need to make sure string is decrypted
                            xObjectData = LMP.String.Decrypt(xObjectData);
                            //Locate WordTemplate property in ObjectData
                            int iStart;
                            //SegmentID may be at start of ObjectData, or after Tag Prefix
                            if (xObjectData.StartsWith("SegmentID="))
                                iStart = 0 + @"SegmentID=".Length;
                            else
                            {
                                iStart = xObjectData.IndexOf(String.mpTagPrefixIDSeparator + "SegmentID=");
                                if (iStart > -1)
                                    iStart = iStart + (String.mpTagPrefixIDSeparator + @"SegmentID=").Length;
                            }

                            if (iStart > -1)
                            {
                                int iEnd = xObjectData.IndexOf("|", iStart);
                                if (iEnd > -1)
                                {
                                    string xID = xObjectData.Substring(iStart, iEnd - iStart);
                                    return xID;
                                }
                            }
                        }
                    }
                }
                return "";
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }

        }

        /// <summary>
        /// returns TRUE if the specified xml is for a segment that's in a table
        /// </summary>
        /// <param name="xXML"></param>
        /// <returns></returns>
        public static bool IsTableSegment(string xXML)
        {
            //search for table start tag
            int iPos = xXML.IndexOf("<w:tbl>");
            if (iPos == -1)
                //no table
                return false;

            //get table xml
            int iPos2 = xXML.IndexOf("</w:tbl>", iPos);
            string xTable = xXML.Substring(iPos, iPos2 - iPos);

            //return TRUE is mpNewSegment bookmark is inside the table
            if (xTable.IndexOf("w:name=" + '\"' + "mpNewSegment" + '\"') > -1)
                return true;

            //if the table has multiple rows, the bookmark will be before the table -
            //return TRUE if there are no intervening paragraph end tags
            iPos2 = xXML.IndexOf("w:name=" + '\"' + "mpNewSegment" + '\"');
            //GLOG 6799: Don't check if no bookmark found
            if (iPos2 > -1)
            {
                string xPreceding = xXML.Substring(iPos2, iPos - iPos2);
                bool bIsTable = ((xPreceding.IndexOf("</w:p>") == -1) &&
                    (xPreceding.IndexOf("<w:p/>") == -1));
                return bIsTable;
            }
            else
                return false;
        }

        public static bool DefinitionContainsAuthorPreferences(string xSegmentXML)
        {
            //GLOG 4578 (dm) - changed this from a property to a static method
            xSegmentXML = xSegmentXML.ToUpper();
            return (xSegmentXML.Contains("[AUTHORPREFERENCE_") || xSegmentXML.Contains("[LEADAUTHORPREFERENCE_") ||
                xSegmentXML.Contains("[AUTHORPARENTPREFERENCE") || xSegmentXML.Contains("[LEADAUTHORPARENTPREFERENCE_"));
        }

        /// <summary>
        /// gets the opening xml for a new mSEG content control
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="bAddNewSegmentBookmark"></param>
        /// <param name="xDocVarID"></param>
        /// <returns></returns>
        private static string GetNewmSEGStartXML(string xSegmentID, bool bAddNewSegmentBookmark,
            out string xDocVarID)
        {
            const string mpCCNodeStart = "<w:sdt><w:sdtPr><w:tag w:val=\"{0}\"/></w:sdtPr><w:sdtContent>";

            //GLOG 15872: Use static function to ensure unique IDs
            xDocVarID = LMP.Conversion.GenerateDocVarID();

            //construct tag string
            string xTag = "mps" + xDocVarID + xSegmentID + "000000000";
            string xCCNodeStart = string.Format(mpCCNodeStart, xTag);

            //bookmark start of content control range if specified
            if (bAddNewSegmentBookmark)
                xCCNodeStart = string.Concat(xCCNodeStart, NEW_SEGMENT_BMK_OPENXML);

            return xCCNodeStart;
        }

        /// <summary>
        /// adds the specified document variable to the supplied xml
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        private static void AddDocVarToSegmentXML(ref string xXML, string xName, string xValue)
        {
            const string mpDocVarNode = "<w:docVar w:name=\"{0}\" w:val=\"{1}\"/>";

            //look for existing <w:docVars> node - if none, add to end of <w:settings>
            int iPos = xXML.IndexOf("</w:docVars>");
            if (iPos == -1)
            {
                iPos = xXML.IndexOf("</w:settings>");
                xXML = xXML.Insert(iPos, "<w:docVars></w:docVars>");
                iPos = iPos + 11;
            }

            //insert doc var
            xXML = xXML.Insert(iPos, string.Format(mpDocVarNode, xName, xValue));
        }
        protected override void GetChildCollectionTableStructure(ref CollectionTableStructure oStructure)
        {
            int iRowIndex = 0; //GLOG 6361 (dm)
            for (int i = 0; i < this.Segments.Count; i++)
            {
                XmlSegment oChild = this.Segments[i];

                //GLOG 8373: Don't include Child segments that have been deleted from database as part of structure
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oTest = null;
                try
                {
                    oTest = (AdminSegmentDef)oDefs.ItemFromID(oChild.ID1);
                }
                catch
                {
                    continue;
                }
                if (oTest == null || oTest.TypeID != oChild.TypeID)
                    continue;

                if (oChild.Segments.Count > 0)
                {
                    //recurse
                    oChild.GetChildCollectionTableStructure(ref oStructure);
                }

                //limit child structure to table collections and
                //table collection items, as these are the only children
                //whose location we can know on re-insertion
                if (oChild is XmlCollectionTableItem || oChild is XmlPaper)
                {
                    //get child segment prefill
                    XmlPrefill oPrefill = new XmlPrefill(oChild);

                    //GLOG 6361 (dm) - append location of collection table items -
                    //this will allow us to recreate in same position
                    //GLOG 7848: Dictionary key now in this format:
                    //<FullTagID>|<SegmentID>|<ObjectTypeID>|<Location>
                    string xKey = oChild.FullTagID + "|" + oChild.ID + "|" + (int)oChild.TypeID;
                    if (oChild is XmlCollectionTableItem)
                    {
                        CollectionTableStructure.ItemInsertionLocations iLoc =
                            CollectionTableStructure.ItemInsertionLocations.EndOfTable;
                        if (((XmlCollectionTableItem)oChild).AllowSideBySide)
                        {
                            TableCell oCell = oChild.RawSegmentParts[0].ContentControl.Descendants<TableCell>().First();
                            int iCurRowIndex = 0;
                            if (oCell.PreviousSibling<TableCell>() != null)
                            {
                                iLoc = CollectionTableStructure.ItemInsertionLocations.RightCell;
                                TableRow oRow = oCell.Ancestors<TableRow>().First();
                                Table oTbl = oCell.Ancestors<Table>().First();
                                TableRow[] oRows = oTbl.Descendants<TableRow>().ToArray<TableRow>();
                                for (iCurRowIndex = 0; iCurRowIndex < oRows.Count(); i++)
                                {
                                    if (oRows[iCurRowIndex] == oRow)
                                    {
                                        if (iCurRowIndex == iRowIndex)
                                            iLoc = iLoc | CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                iLoc = CollectionTableStructure.ItemInsertionLocations.LeftCell;
                            }
                            iRowIndex = iCurRowIndex;
                        }
                        xKey = xKey + "|" + ((short)iLoc).ToString();
                    }
                    else
                        xKey = xKey + "|0"; //GLOG 7848: Use unspecified value for non Collection item location

                    oStructure.Dictionary.Add(xKey, oPrefill);

                    //GLOG 6002 (dm) - add sidebar here to maintain
                    //relationship with parent pleading paper
                    //GLOG 6519:  Support Letterhead or Pleading Paper as Sidebar parent
                    if (oChild is XmlPaper) // if (oChild is PleadingPaper)
                    {
                        for (int j = 0; j < oChild.Segments.Count; j++)
                        {
                            XmlSegment oGrandchild = oChild.Segments[j];
                            if (oGrandchild is XmlSidebar)
                            {
                                oPrefill = new XmlPrefill(oGrandchild);
                                //GLOG 7848
                                oStructure.Dictionary.Add(oGrandchild.FullTagID + "|" + oGrandchild.ID + "|" + (int)oGrandchild.TypeID + "|0", oPrefill);
                                break;
                            }
                        }
                    }
                }
            }
        }
        internal void InsertCollectionTableStructure(XmlSegment oTarget, CollectionTableStructure oStructure)
        {
            DateTime t0 = DateTime.Now;
            //GLOG 5944 (dm) - set flag that child structure is being inserted
            oTarget.ForteDocument.ChildStructureInsertionInProgess = true;

            //GLOG 5887 (dm) - raise event
            oTarget.ForteDocument.RaiseBeforeCollectionTableStructureInsertedEvent(oTarget);

            //GLOG 5909 (dm) - get any non-native mDels in collection tables -
            //we'll need to restore these later
            ListDictionary omDels = new ListDictionary();
            this.GetNonNativemDelsInCollectionTables(oTarget, ref omDels);

            //delete all table collection items -
            //we'll add back below the ones in the collection table structure
            XmlSegment[] aCollectionTableItems = XmlSegment.FindCollectionTableItemChildren(oTarget.Segments);

            //GLOG 7848: Track Object types that need updating
            Dictionary<mpObjectTypes, bool> oUpdateTypes = new Dictionary<mpObjectTypes, bool>();

            //GLOG 6002 (dm) - hang on to pleading paper as sidebar target -
            //since structure is flat, child sidebar will always follow
            //immediately in collection
            XmlSegment oPPaper = null;

            //Process in reverse order so that Pleading Paper won't be deleted before Sidebar
            for (int i = aCollectionTableItems.Length - 1; i >= 0; i--)
            {
                //GLOG 7848: Determine if item is a type that needs updating.
                //If not, just apply prefill to the existing item

                XmlSegment oItem = oTarget.GetChildSegmentFromFullTagID(aCollectionTableItems[i].FullTagID);
                mpObjectTypes oType;
                //GLOG 8528: Sidebar may have been deleted with Pleading Paper
                if (oItem != null)
                {
                    oType = oItem.TypeID;
                    if (oItem is XmlPaper)
                        oPPaper = oItem;
                }
                else
                {
                    oType = aCollectionTableItems[i].TypeID;
                }


                if (!oUpdateTypes.ContainsKey(oType))
                {
                    List<string> aSiblings = oStructure.GetSegmentsOfSameType(aCollectionTableItems, oType);
                    List<string> aStructureSiblings = oStructure.GetStructureItemsOfSameType(oType);
                    //GLOG 8373: Don't mark item to be replaced if original segment being recreated no longer exists
                    if (!oStructure.ContainsChildOfType(oType) || aSiblings.SequenceEqual(aStructureSiblings))
                    {
                        //Existing collection is identical to that being recreated
                        oUpdateTypes.Add(oType, false);
                    }
                    else
                    {
                        oUpdateTypes.Add(oType, true);
                    }
                }
                //If existing and pre-existing collections aren't identical, 
                //Delete items to be reinserted
                if (oUpdateTypes[oType] == true)
                {
                    //GLOG 5909 (dm) - don't preserve non-native mDels here
                    oTarget.ForteDocument.DeleteSegment(oItem, false, false, true);
                }
                else if (oItem != null)
                {
                    //Item already exists - just apply prefill
                    foreach (string xKey in oStructure.Dictionary.Keys)
                    {
                        string[] xProps = xKey.Split('|');
                        string xChildID = xProps[1];
                        if (xChildID.EndsWith(".0"))
                            xChildID = xChildID.Replace(".0", "");
                        if (xChildID == oItem.ID.Replace(".0", ""))
                        {
                            oItem.ApplyPrefill(new XmlPrefill(oStructure[xKey]));
                            break;
                        }
                    }
                }
            }

            //cycle through supplied child structure,
            //adding each of the collections items
            //in the structure
            foreach (string xKey in oStructure.Dictionary.Keys)
            {
                //GLOG 6361 (dm) - the insertion location for collection table items
                //is now appended to the key
                string[] xProps = xKey.Split('|');
                string xChildID = xProps[1]; //GLOG 7848
                //GLOG 6966: Make sure string can be parsed as a Double even if
                //the Decimal separator is something other than '.' in Regional Settings
                if (xChildID.EndsWith(".0"))
                    xChildID = xChildID.Replace(".0", "");
                //GLOG 7848: Insert only if existing item was deleted above
                int iTypeID = Int32.Parse(xProps[2]);
                //GLOG 8373: Additional test not necessary
                //GLOG 8415
                //GLOG 8790: Insert Sidebar only if existing Sidebar was deleted above or pleading paper being replaced had no sidebar
                if (oUpdateTypes.ContainsKey((mpObjectTypes)iTypeID) && (oUpdateTypes[(mpObjectTypes)iTypeID] == true ||
                    ((mpObjectTypes)iTypeID == mpObjectTypes.Sidebar && oStructure.GetSegmentsOfSameType(aCollectionTableItems, mpObjectTypes.Sidebar).Count == 0)))
                {
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();

                    //GLOG 8373: Avoid error if child segment has been deleted
                    AdminSegmentDef oDef = null;

                    try
                    {
                        oDef = (AdminSegmentDef)oDefs.ItemFromID((int)double.Parse(xChildID));
                    }
                    catch
                    {
                        continue;
                    }


                    if (oDef.IsPaperType)
                    {
                        //GLOG 6002 (dm) - added support for sidebars
                        XmlSegment oSeg = XmlSegment.InsertPaperInXmlSegment(oDef.ID.ToString(), oTarget, new XmlPrefill(oStructure[xKey]), null);
                        //GLOG 6519:  Recognize either Letterhead or Pleading Paper as Sidebar parent
                        if (oSeg is XmlPaper) // if (oSeg is PleadingPaper)
                            oPPaper = oSeg;

                    }
                    else if (oDef.TypeID == mpObjectTypes.Sidebar)
                    {
                        if (oPPaper != null)
                        {
                            //GLOG 6002 (dm) - added support for sidebars
                            XmlSegment.InsertPaperInXmlSegment(oDef.ID.ToString(), oPPaper, new XmlPrefill(oStructure[xKey]), null);
                        }
                    }
                    else
                    {
                        mpObjectTypes iParentType = 0;
                        switch (oDef.TypeID)
                        {
                            case mpObjectTypes.PleadingCaption:
                                iParentType = mpObjectTypes.PleadingCaptions;
                                break;
                            case mpObjectTypes.PleadingCounsel:
                                iParentType = mpObjectTypes.PleadingCounsels;
                                break;
                            case mpObjectTypes.PleadingSignature:
                                iParentType = mpObjectTypes.PleadingSignatures;
                                break;
                            case mpObjectTypes.LetterSignature:
                                iParentType = mpObjectTypes.LetterSignatures;
                                break;
                            case mpObjectTypes.AgreementSignature:
                                iParentType = mpObjectTypes.AgreementSignatures;
                                break;
                            default:
                                iParentType = mpObjectTypes.CollectionTable;
                                break;
                        }

                        XmlSegment oCollection = oTarget.FindChildren((mpObjectTypes)iParentType)[0];
                        InsertCollectionTableItem(oCollection, oDef.ID.ToString(), new XmlPrefill(oStructure[xKey]), (CollectionTableStructure.ItemInsertionLocations)System.Int16.Parse(xProps[3]));

                    }
                }
            }

            //GLOG 5909 (dm) - restore non-native mDels
            foreach (DictionaryEntry oEntry in omDels)
            {
                XmlSegment oItem = oTarget.Segments.ItemFromID(oEntry.Key.ToString());
                DeleteScope.RestoreChildmDels(oTarget.ForteDocument.WPDocument, oItem.RawSegmentParts[0].ContentControl, (List<string>)oEntry.Value);
            }
            //GLOG 5887 (dm) - raise event
            oTarget.ForteDocument.RaiseAfterCollectionTableStructureInsertedEvent(oTarget);

            //GLOG 5944 (dm)
            oTarget.ForteDocument.ChildStructureInsertionInProgess = false;
            LMP.Benchmarks.Print(t0);

        }
        /// <summary>
        /// creates a dictionary entry for each child collection table of oSegment
        /// that contains non-native mDels 
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="omDels"></param>
        private void GetNonNativemDelsInCollectionTables(XmlSegment oSegment,
            ref ListDictionary omDels)
        {
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                XmlSegment oChild = oSegment.Segments[i];
                if (oChild is XmlCollectionTable)
                {
                    List<string> xmDels = DeleteScope.GetChildmDels(oSegment.WPDoc, oChild.RawSegmentParts[0].ContentControl, oChild.RawSegmentParts[0].TagID);
                    if (xmDels != null)
                        omDels.Add(oChild.FullTagID, xmDels);
                }

                GetNonNativemDelsInCollectionTables(oChild, ref omDels);
            }
        }
        #endregion
        #region *********************event handlers*********************
        private void m_oAuthors_AuthorAdded(object AuthorsCollection, AuthorAddedEventArgs oArgs)
        {
            SaveAuthorsDetail(this);

            //update for additional author
            if (m_bAllowUpdateForAuthor &&
                (this.CreationStatus >= XmlSegment.Status.VariableDefaultValuesSet) &&
                (m_oAuthors.Count > 1))
                UpdateForAuthor(oArgs.NewIndex, -2, !m_bSkipAuthorActions);

            //update child segment authors
            UpdateChildSegmentAuthors();
        }
        private void m_oAuthors_AuthorsReindexed(object AuthorsCollection, AuthorsReindexedEventArgs oArgs)
        {
            SaveAuthorsDetail(this);

            //update for authors
            if (m_bAllowUpdateForAuthor &&
                this.CreationStatus >= XmlSegment.Status.VariableDefaultValuesSet)
                UpdateForAuthor(-1, -2, !m_bSkipAuthorActions); //GLOG 6998: Update variables for all authors, not just LeadAuthor

            //update child segment authors
            UpdateChildSegmentAuthors();
        }
        private void m_oAuthors_LeadAuthorChanged(object AuthorsCollection, LeadAuthorChangedEventArgs oArgs)
        {
            SaveAuthorsDetail(this);

            //update for new lead author
            if (m_bAllowUpdateForAuthor &&
                this.CreationStatus >= XmlSegment.Status.VariableDefaultValuesSet)
                UpdateForAuthor(-1, -2, !m_bSkipAuthorActions);

            //update child segment authors
            UpdateChildSegmentAuthors();
        }
        private void m_oVariables_BeforeContactsUpdateEvent(object sender, BeforeContactsUpdateEventArgs oArgs)
        {
            //System.Windows.Forms.MessageBox.Show("About to update contacts.");
        }
        private void OnVariablesValidationChanged(object sender, LMP.Architect.Base.ValidationChangedEventArgs e)
        {
            RaiseValidationChangeIfNecessary(e);
        }
        private void oChildSegment_ValidationChange(object sender, LMP.Architect.Base.ValidationChangedEventArgs e)
        {
            RaiseValidationChangeIfNecessary(e);
        }
        private void oChildSegment_AfterAllVariablesVisited(object sender, EventArgs e)
        {
            //Check if event should also be raised for parent segment
            if (this.AllRequiredValuesVisited)
            {
                if (this.AfterAllVariablesVisited != null)
                    this.AfterAllVariablesVisited(this, new EventArgs());
            }
        }
        private void m_oChildSegments_SegmentAdded(object sender, SegmentEventArgs oArgs)
        {
            //re-raise event
            this.ForteDocument.RaiseSegmentAddedEvent(oArgs.Segment);
        }
        private void m_oChildSegments_SegmentDeleted(object sender, SegmentEventArgs oArgs)
        {
            //re-raise event
            this.ForteDocument.RaiseSegmentDeletedEvent(oArgs.Segment);
        }
        private void OnVariableVisited(object sender, VariableEventArgs oArgs)
        {
            if (this.AllRequiredValuesVisited)
            {
                if (this.AfterAllVariablesVisited != null)
                    this.AfterAllVariablesVisited(this, new EventArgs());
            }
        }
        #endregion
    }

    /// <summary>
    /// defines a collection of Segments -
    /// contains methods to manage the collection-
    /// created by Dan Fisherman - 11/01/05
    /// </summary>
    public class XmlSegments : NameObjectCollectionBase, IEnumerable
    {
        #region *********************events*********************
        public event SegmentDeletedHandler SegmentDeleted;
        public event SegmentAddedHandler SegmentAdded;
        #endregion
        #region *********************fields*********************
        private XmlForteDocument m_oForteDocument;
        #endregion
        #region *********************constructors*********************
        public XmlSegments(XmlForteDocument oForteDoc)
        {
            m_oForteDocument = oForteDoc;
        }
        #endregion
        #region *********************methods*********************
        public XmlSegment ItemFromIndex(int iIndex)
        {
            return (XmlSegment)base.BaseGet(iIndex);
        }

        //jws 8/26/16
        public XmlSegment ItemFromID(string xID)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].FullTagID == xID)
                {
                    //remove from collection
                    return (XmlSegment)base.BaseGet(i);
                }
            }
            return null;
        }


        public XmlSegment this[int iIndex]
        {
            get { return ItemFromIndex(iIndex); }
        }
        public XmlSegment this[string xID]
        {
            get
            {
                return ItemFromID(xID);
            }
        }
        public override int Count
        {
            get { return base.Count; }
        }

        public void Add(XmlSegment oSegment)
        {
            base.BaseAdd(oSegment.FullTagID, oSegment);

            //raise event
            if (this.SegmentAdded != null)
                this.SegmentAdded(this, new SegmentEventArgs(oSegment));
        }

        //jws 8/26/16
        public void Delete(string xID)
        {
            //get segment
            XmlSegment oSegment = null;
            for (int i = 0; i < this.Count; i++)
            {
                oSegment = ItemFromIndex(i);
                if (oSegment.FullTagID == xID)
                {
                    //remove from collection
                    base.BaseRemoveAt(i);
                    break;
                }
            }
            //raise event
            if (this.SegmentDeleted != null)
                this.SegmentDeleted(this, new SegmentEventArgs(oSegment));
        }

        public bool PrepareForFinish()
        {
            foreach (XmlSegment oSegment in this.BaseGetAllValues())
            {
                if (!oSegment.PrepareForFinish())
                    return false;
            }

            return true;
        }
        public bool ExecutePostFinish()
        {
            foreach (XmlSegment oSegment in this.BaseGetAllValues())
            {
                if (!oSegment.ExecutePostFinish())
                    return false;
            }

            return true;
        }
        public void Clear()
        {
            //run Delete() override in order to raise Deleted event for each segment
            //Start with last item, since Count will be reduced after each deletion
            for (int i = this.Count - 1; i >= 0; i--)
                Delete(this[i].FullTagID);
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// returns true iff all segments in the collection are valid
        /// </summary>
        public bool AreValid
        {
            get
            {
                for (int i = 0; i < this.Count; i++)
                    if (!this[i].IsValid)
                        //segment is invalid
                        return false;

                return true;
            }
        }
        /// <summary>
        /// Returns true if all 'Must Visit' variables of each segment have been visited
        /// </summary>
        public bool AllRequiredValuesVisited
        {
            get
            {
                for (int i = 0; i < this.Count; i++)
                    if (!this[i].AllRequiredValuesVisited)
                        //segment is incomplete
                        return false;

                return true;

            }
        }
        /// <summary>
        /// returns the MacPac document containing this collection
        /// </summary>
        public XmlForteDocument ForteDocument
        {
            get { return this.m_oForteDocument; }
        }

        public bool ContentIsMissing
        {
            get
            {
                for (int i = 0; i < this.Count; i++)
                {
                    if (this[i].ContentIsMissing)
                        return true;
                }
                return false;
            }
        }

        #endregion
        #region *********************event handlers*********************
        #endregion
        #region IEnumerable Members

        //        IEnumerator<> IEnumerable.GetEnumerator()
        //        {
        //        }

        //        #endregion

        //        private class SegmentEnumerator:IEnumerator{

        //    #region IEnumerator Members

        //public object  Current
        //{
        //    get { throw new Exception("The method or operation is not implemented."); }
        //}

        //public bool  MoveNext()
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public void Reset()
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        #endregion
    }
    /// <summary>
    /// Class that represents the Raw Data of a Segment
    /// </summary>
    public class RawSegmentPart
    {
        #region *********************constructors*********************
        public RawSegmentPart(WordprocessingDocument oWPDoc, SdtElement oContentControl)
        {
            this.WPDocument = oWPDoc;
            this.ContentControl = oContentControl;
        }
        #endregion
        #region *********************properties*********************
        public WordprocessingDocument WPDocument { get; set; }
        public SdtElement ContentControl { get; set; }
        public string TagID
        {
            get
            {
                return Query.GetAttributeValue(this.WPDocument, this.ContentControl, "TagID");
            }
        }
        #endregion
        #region *********************methods*********************
        public string this[string xPropertyName]
        {
            get { return GetSegmentPropertyValue(xPropertyName); }
            set { this.SetSegmentPropertyValue(xPropertyName, value, false); }
        }
        /// <summary>
        /// returns the value of the specified segment property
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        public string GetSegmentPropertyValue(string xPropertyName)
        {
            string xObjData = Query.GetAttributeValue(this.WPDocument, this.ContentControl, "ObjectData");
            return Query.GetmSEGObjectDataValue(xObjData, xPropertyName);
        }
        /// <summary>
        /// returns the value of the specified segment property
        /// </summary>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        public string GetSegmentObjectDataString()
        {
            return Query.GetAttributeValue(this.WPDocument, this.ContentControl, "ObjectData");
        }
        public void SetSegmentPropertyValue(string xPropertyName, string xPropertyValue, bool bEncrypt)
        {
            string xObjData = "|" + Query.GetAttributeValue(this.WPDocument, this.ContentControl, "ObjectData");

            //find property by name
            int iPos = xObjData.IndexOf("|" + xPropertyName);

            if (iPos > -1)
            {
                //find subsequent "="
                iPos = xObjData.IndexOf("=", iPos + 1);

                //find next sep
                int iPos2 = xObjData.IndexOf("|", iPos);
                if (iPos2 > -1)
                {
                    xObjData = xObjData.Substring(1, iPos) + xPropertyValue + xObjData.Substring(iPos2);
                }
                else
                {
                    //Item is at end of ObjectData
                    xObjData = xObjData.Substring(1, iPos) + xPropertyValue;
                }
            }
            else
            {
                //Property is not included in ObjectData - add to end
                xObjData = xObjData.TrimEnd('|');
                //Always append pipe, since other functions may expect this
                xObjData = xObjData + "|" + xPropertyName + "=" + xPropertyValue + "|";
            }
            //GLOG 8734: Remove extra pipe from start
            xObjData = xObjData.TrimStart('|');
            Query.SetAttributeValue(this.WPDocument, this.ContentControl, "ObjectData", xObjData, bEncrypt, null);
        }
        #endregion
        #region *********************RawSegmentTagIdComparer Helper Class*********************
        public class RawSegmentTagIDComparer : IComparer<RawSegmentPart>
        {
            public int Compare(RawSegmentPart x, RawSegmentPart y)
            {
                int i = string.Compare(x.TagID, y.TagID);

                if (i == 0)
                    return 1;
                else
                    return i;
            }
        }
        #endregion
    }

}
