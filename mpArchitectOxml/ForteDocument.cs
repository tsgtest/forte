using System;
using System.Windows.Forms;
using LMP.Data;
using System.IO;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.CustomProperties;
using DocumentFormat.OpenXml.ExtendedProperties;
using DocumentFormat.OpenXml.VariantTypes;
using System.Security.Cryptography;

namespace LMP.Architect.Oxml
{
    public delegate void CurrentSegmentChangedEventHandler(object sender, LMP.Architect.Base.CurrentSegmentSwitchedEventArgs args);

	/// <summary>
	/// contains the members of a MacPac document -
	/// a Word document created by MacPac.
	/// </summary>
	public abstract class XmlForteDocument : LMP.Architect.Base.ForteDocument
	{
        #region *********************fields*********************
        //private XmlVariables m_oVariables;
        //private XmlBlocks m_oBlocks;
		//private XmlSegments m_oSegments;
		private static WordXMLEvents m_iIgnoreWordXMLEvents = WordXMLEvents.None;
        private WordprocessingDocument m_oWPDoc = null;
        private MemoryStream m_oStream = null;
        //private Modes m_iMode;
        //private XmlSegment m_xFixedTopLevelSegment;
        //private int m_iWindowHandle = 0;
        //private int m_iReplaceSegmentNodeIndex = -1;
        //private bool m_bDeleteScopesUpdatingInDesign = false;
        //private mpFileFormats m_oFileFormat = mpFileFormats.None;
        //private bool m_bFunctionalityIsAvailable = true;
        //private bool m_bChildStructureInsertionInProgress = false; //GLOG 5944 (dm)
        //private bool m_bDynamicEditingSet = false; //JTS 4/1/13
        private List<ImageData> m_oImageList = null;
        private string m_xContentXML = null;
        #endregion
		#region *********************events*********************
        public event VariableDeletedHandler VariableDeleted;
        public event VariableAddedHandler VariableAdded;
        public event VariableDefinitionChangedHandler VariableDefinitionChanged;
        public event BlockDeletedHandler BlockDeleted;
        public event BlockAddedHandler BlockAdded;
        public event BlockDefinitionChangedHandler BlockDefinitionChanged;
        public event SegmentDeletedHandler SegmentDeleted;
        public event SegmentAddedHandler SegmentAdded;
        public event SegmentRefreshedByActionHandler SegmentRefreshedByAction;
        public event SegmentDistributedSectionsHandler SegmentDistributedSectionsChanged;
        public event BeforeSegmentReplacedByActionHandler BeforeSegmentReplacedByAction;
        public event BeforeCollectionTableStructureInsertedHandler BeforeCollectionTableStructureInserted;
        public event AfterCollectionTableStructureInsertedHandler AfterCollectionTableStructureInserted;
        //public event CurrentSegmentChangedEventHandler CurrentSegmentChanged;
        #endregion
		#region *********************constructor*********************
        public XmlForteDocument() { }
        public XmlForteDocument(WordprocessingDocument oWPDoc)
        {
            this.WPDocument = oWPDoc;
        }
        public XmlForteDocument(RawSegmentPart oRawSegment)
        {
            this.WPDocument = oRawSegment.WPDocument;
        }
        #endregion
		#region *********************properties*********************
        //returns true iff the macpac document
        //has finished segments
        //public bool ContainsFinishedSegments
        //{
        //    get
        //    {
        //        return LMP.Forte.MSWord.WordDoc.HasSnapshotDocVars(this.WordDocument);
        //    }
        //}

        //returns true if MacPac functionality is available in this document
        public bool MacPacFunctionalityIsAvailable { get; set; }

        public WordprocessingDocument WPDocument { set; get; }

        public int WindowHandle { get; private set; }

        /// <summary>
        /// gets/sets index of editor node of segment about to be replaced
        /// </summary>
        public int ReplaceSegmentNodeIndex { get; set; }

        /// <summary>
        /// gets/sets flag indicating whether the designer
        /// is in the process of refreshing delete scopes
        /// </summary>
        public bool DeleteScopesUpdatingInDesign { get; set; }

        public bool ChildStructureInsertionInProgess { get; set; }

		/// <summary>
		/// returns the type of document this is - choices 
		/// are listed in the domain ForteDocument.Types
		/// </summary>
		public XmlForteDocument.Types Type
		{
			get
			{
				//TODO: implement this function -
				//for now, return MacPac 10
				return XmlForteDocument.Types.MacPac10;
			}
		}

		/// <summary>
		/// returns the segments currently in the document
		/// </summary>
		public XmlSegments Segments
		{
			get
			{
                DateTime t0 = DateTime.Now;

                try
                {
                    return XmlSegment.GetSegments(this);
                }
                finally
                {
                    LMP.Benchmarks.Print(t0);
                }
			}
		}


        ///// <summary>
        ///// returns the standalone variables of this.ForteDocument
        ///// </summary>
        //public Variables Variables
        //{
        //    get
        //    {
        //        if (m_oVariables == null)
        //            RefreshVariables();

        //        return m_oVariables;
        //    }
        //    private set { this.m_oVariables = value; }
        //}

        ///// <summary>
        ///// returns whether the.ForteDocument has variables without invoking its
        ///// variables property and thereby populating the collection
        ///// </summary>
        //public bool HasVariables
        //{
        //    get { return ((m_oVariables != null) && (m_oVariables.Count > 0)); }
        //}

        ///// <summary>
        ///// returns the standalone blocks of this.ForteDocument
        ///// </summary>
        //public Blocks Blocks
        //{
        //    get
        //    {
        //        if (m_oBlocks == null)
        //            RefreshBlocks();

        //        return m_oBlocks;
        //    }
        //    private set { this.m_oBlocks = value; }
        //}
		/// flag to turn off event handling
		/// </summary>
		public static WordXMLEvents IgnoreWordXMLEvents
		{
			get{return m_iIgnoreWordXMLEvents;}
			set{m_iIgnoreWordXMLEvents = value;}
		}
        /// <summary>
        /// Returns true if document does not match any of the criteria
        /// indicating that automatic trailer update should not occur
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        //public static bool IsDocumentTrailerable(Word.Document oDoc)
        //{
        //    //GLOG - 3906 - CEH
        //    int iFormat = 0;
        //    string xCustomCode = LMP.Data.Application.GetMetadata("CustomCode");

        //    try
        //    {
        //        //Get Save Format
        //        iFormat = oDoc.SaveFormat;
        //    }
        //    catch { }           

            
        //    LMP.Data.FirmApplicationSettings oFirmAppSettings = 
        //        new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);

        //    if (oDoc == null) // || oDoc.ReadOnly) GLOG 5764:  Read-only should not prevent trailer insertion
        //        return false;
        //    //GLOG 4283
        //    else if (oFirmAppSettings.SkipTrailerForTempFiles && IsDocumentInTemporaryFolder(oDoc))
        //        return false;
        //    //Littler customization
        //    else if (xCustomCode.Contains("NoTrailerForTempDoc") &&
        //            (oDoc.Path.ToUpper().Contains(@"\TEMPORARY")))
        //        return false;
        //    else if (LMP.Forte.MSWord.WordDoc.SkipTrailerUpdate(oDoc))
        //        //Document is marked for no trailer based on custom macro criteria
        //        return false;
        //    else if (iFormat > 1 && (iFormat < 12 || iFormat > 16))
        //        //Document is not a Word document
        //        return false;
        //    else if (DocCannotBeUnprotected(oDoc))
        //        //GLOG 7107 (dm) - we need to check this before checking sections
        //        //for property tags, which can insert an extra paragraph in the header
        //        return false;
        //    else if (AllSectionsMarkedForNoTrailer(oDoc))
        //        //Document has been marked for no trailer in property tags
        //        return false;
        //    else
        //    {
        //        //return false for non-Word or special-use documents
        //        string xFileName = oDoc.Name.ToUpper();
        //        if (xFileName.EndsWith(".DOT") || xFileName.EndsWith(".DOTX") ||
        //            xFileName.EndsWith(".DOTM") || xFileName.EndsWith(".DIC") ||
        //            xFileName.EndsWith(".INI") || xFileName.EndsWith(".MBP") ||
        //            xFileName.EndsWith(".STY") || xFileName.EndsWith(".TMP") ||
        //            xFileName.EndsWith(".CMD") || xFileName.EndsWith(".BAT"))
        //            return false;
        //    }
        //    return true;
        //}
        //public static bool IsDocumentInTemporaryFolder(Word.Document oDoc)
        //{
        //    //GLOG 4283: Returns true if document path is in one of the predefined Temp locations
        //    string xFilePath = oDoc.Path.ToUpper();
        //    //Get Temporary Internet Files Special Folder path
        //    string xTempInternet = System.Environment.GetFolderPath((System.Environment.SpecialFolder)System.Environment.SpecialFolder.InternetCache);
        //    return (xFilePath.Contains(xTempInternet.ToUpper()) ||
        //        xFilePath.Contains(System.Environment.GetEnvironmentVariable("TEMP").ToUpper()) ||
        //        xFilePath.Contains(System.Environment.GetEnvironmentVariable("TMP").ToUpper()));
        //}
        ///// <summary>
        ///// Get/Set 'NoTrailer' property in ObjectData of mDocProps tag
        ///// </summary>
        //public bool AllowDocumentTrailer
        //{
        //    get 
        //    {
        //        string xVal = LMP.Forte.MSWord.WordDoc.GetDocPropValue(m_oWordDocument, "NoTrailer");
        //        if (string.IsNullOrEmpty(xVal))
        //            return true;
        //        else 
        //            return xVal.ToUpper() != "TRUE"; 
                
        //    }
        //    set
        //    {
        //        LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents bIgnoreWordXMLEvents = XmlForteDocument.IgnoreWordXMLEvents;
        //        XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
        //        LMP.Forte.MSWord.WordDoc.SetDocPropValue(m_oWordDocument, "NoTrailer", (!value).ToString());
        //        XmlForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
        //    }
        //}
        ///// <summary>
        ///// True if 'NoTrailer' property is true in ObjectData of mSecProps tag
        ///// </summary>
        ///// <param name="iSecIndex"></param>
        ///// <returns></returns>
        //public bool IsSectionMarkedForNoTrailer(int iSecIndex)
        //{
        //    string xVal = LMP.Forte.MSWord.WordDoc.GetSectionPropValue(m_oWordDocument, iSecIndex, "NoTrailer");

        //    if (string.IsNullOrEmpty(xVal))
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return xVal.ToUpper() == "TRUE";
        //    }
        //}
        ///// <summary>
        ///// Set NoTrailer property in ObjectData of mSecProps tag
        ///// </summary>
        ///// <param name="iSecIndex"></param>
        ///// <param name="bAllow"></param>
        //public void MarkSectionForNoTrailer(int iSecIndex, bool bAllow)
        //{
        //    LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents bIgnoreWordXMLEvents = XmlForteDocument.IgnoreWordXMLEvents;
        //    XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
        //    LMP.Forte.MSWord.WordDoc.SetSectionPropValue(m_oWordDocument, iSecIndex, "NoTrailer", (!bAllow).ToString());
        //    XmlForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
        //}
        ///// <summary>
        ///// Returns true of Document is marked for No Trailer in mDocProps tag
        ///// or if all sections are marked for No Trailer in mSecProps tags
        ///// </summary>
        ///// <param name="oDoc"></param>
        ///// <returns></returns>
        //public static bool AllSectionsMarkedForNoTrailer(Word.Document oDoc)
        //{
        //    if (oDoc == null)
        //        return true;
        //    XmlForteDocument oMPDoc = new XmlForteDocument(oDoc);

        //    if (!oMPDoc.AllowDocumentTrailer)
        //    {
        //        //Trailer is disabled at document level
        //        return true;
        //    }
        //    else
        //    {
        //        //Check if any section is not marked for no trailer
        //        for (int i = 1; i <= oMPDoc.WordDocument.Sections.Count; i++)
        //        {
        //            if (!oMPDoc.IsSectionMarkedForNoTrailer(i))
        //                return false;
        //        }
        //    }
        //    return true;
        //}

        ///// <summary>
        ///// returns true if document is document is password protected and
        ///// password is unavailable - added for GLOG 7107 (dm)
        ///// </summary>
        ///// <param name="oDoc"></param>
        ///// <returns></returns>
        //public static bool DocCannotBeUnprotected(Word.Document oDoc)
        //{
        //    bool bValue = false;
        //    //GLOG 7958
        //    bool bSaved = oDoc.Saved;
        //    LMP.Architect.Oxml.Environment oEnv = new LMP.Architect.Oxml.Environment(oDoc);
        //    try
        //    {
        //        oEnv.UnprotectDocument();
        //    }
        //    catch (LMP.Exceptions.PasswordException)
        //    {
        //        bValue = true;
        //    }
        //    finally
        //    {
        //        oEnv.RestoreProtection();
        //        //GLOG 7958: Restore original Saved state
        //        if (bSaved && !oDoc.Saved)
        //            oDoc.Saved = true;
        //    }
        //    return bValue;
        //}

        public Modes Mode { get; set; }

        public XmlSegment FixedTopLevelSegment { get; set; }
        public void UnloadVariableControls()
        {
            XmlSegments oSegs = this.Segments;

            for (int i = 0; i < oSegs.Count; i++)
            {
                oSegs[i].UnloadVariableControls(true);
            }
        }
        public List<ImageData> ImageList
        {
            get
            {
                return m_oImageList;
            }
            set
            {
                m_oImageList = value;
            }
        }
        #endregion
		#region *********************methods*********************

        /// <summary>
        /// returns true iff the document contains a segment
        /// of the specified type
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        public bool ContainsSegmentOfType(mpObjectTypes iType)
        {
            return ContainsSegmentOfType(this.Segments, iType);
        }
        /// returns true iff the specified segments collection 
        /// contains a segment of the specified type
        private bool ContainsSegmentOfType(XmlSegments oSegments, mpObjectTypes iType)
        {
            //cycle through segments collection
            for (int i = 0; i < oSegments.Count; i++)
            {
                XmlSegment oSegment = oSegments[i];

                //check for segment of specified type in child segments
                bool bContainsSegment = ContainsSegmentOfType(oSegment.Segments, iType);

                //return true if either the child or the parent
                //are of the specified type
                if (bContainsSegment || oSegment.TypeID == iType)
                    return true;
            }

            return false;
        }

        ///// <summary>
        ///// deletes the specified segment from the document
        ///// </summary>
        ///// <param name="oSegment"></param>
        public void DeleteSegment(XmlSegment oSegment)
        {
            DeleteSegment(oSegment, true, true, true);
        }
        public void DeleteSegment(XmlSegment oSegment, bool bDeleteEmptyParagraph,
            bool bPreserveNonNativemDels, bool bDeleteEmptySection)
        {
            LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents bIgnoreWordXMLEvents = XmlForteDocument.IgnoreWordXMLEvents;

            DateTime t0 = DateTime.Now;

            try
            {
                LMP.Trace.WriteNameValuePairs("oSegment.ID", oSegment.ID,
                    "oSegment.TagID", oSegment.FullTagID);

                //Need to delete external children separately
                //To ensure all headers/footers are removed
                int iChildCount = oSegment.Segments.Count;
                for (int i = iChildCount - 1; i >= 0; i--)
                {
                    if (oSegment.Segments[i].IsExternalChild)
                        DeleteSegment(oSegment.Segments[i], bDeleteEmptyParagraph,
                            bPreserveNonNativemDels, bDeleteEmptySection);
                }
                for (int p = oSegment.RawSegmentParts.Count() - 1; p >= 0; p--)
                {
                    SdtElement oCC = oSegment.RawSegmentParts[p].ContentControl;
                    TextBoxContent oTBC = oCC.Ancestors<TextBoxContent>().FirstOrDefault<TextBoxContent>();
                    Table oTbl = oCC.Ancestors<Table>().FirstOrDefault<Table>();
                    TableRow oRow = oCC.Descendants<TableRow>().FirstOrDefault<TableRow>();
                    bool bFullRowFormat = oCC is SdtRow;

                    Paragraph oPara = oCC.Ancestors<Paragraph>().FirstOrDefault<Paragraph>();
                    Query.DeleteAssociatedDocVars(this.WPDocument, oCC);
                    if (bFullRowFormat && oSegment is XmlCollectionTableItem)
                    {
                        SdtElement oCollectionCC = oSegment.Parent.RawSegmentParts[0].ContentControl; 
                        List<string> omDels = new List<string>();
                        if (bPreserveNonNativemDels)
                            omDels = DeleteScope.GetChildmDels(this.WPDocument, oCollectionCC, oSegment.Parent.RawSegmentParts[0].TagID);

                        //If deleting Collection Item, leave one empty row
                        if (oTbl.Descendants<TableRow>().Count() == 1)
                        {
                            OpenXmlElement oNewRow = oCC.Parent.InsertBefore(oRow.CloneNode(true), oCC);
                            TableCell[] oCells = oNewRow.Descendants<TableCell>().ToArray<TableCell>();
                            foreach (TableCell oCell in oCells)
                            {
                                oCell.RemoveAllChildren<SdtElement>();
                                oCell.RemoveAllChildren<Paragraph>();
                                oCell.RemoveAllChildren<Run>();
                                oCell.AppendChild<Paragraph>(new Paragraph());
                            }
                        }
                        //GLOG 8697
                        if (omDels != null)
                            DeleteScope.RestoreChildmDels(this.WPDocument, oCollectionCC, omDels);
                    }
                    oCC.Remove();
                    if (oTBC != null)
                    {
                        oPara = oTBC.Ancestors<Paragraph>().FirstOrDefault<Paragraph>();
                        //if mSEG was within a Textbox that is now empty, remove Textbox as well
                        if (oTBC.Descendants<Paragraph>().Count<Paragraph>() < 2 && oTBC.Descendants<Run>().Count<Run>() == 0)
                        {
                            oTBC.Ancestors<Picture>().First<Picture>().Remove();
                        }
                    }
                    //If Paragraph is empty and contains no shapes, delete if specifed
                    if (bDeleteEmptyParagraph &&  oPara != null && oPara.Descendants<Picture>().FirstOrDefault<Picture>() == null)
                    {
                        if (oPara.InnerText == "")
                        {
                            oPara.Remove();
                        }
                    }
                }


                ////GLOG 4418: Added new parameter so that when multiple trailer segments are being deleted
                ////it is not necessary to Refresh after each deletion
                //if (bRefreshTags)
                //{
                //    //refresh tags - use indexes instead of GUIDs
                //    //when in design mode
                //    bool bRefreshWithGUIDs = (this.Mode != Modes.Design);
                //    this.RefreshTags(bRefreshWithGUIDs);
                //}

                ////10-21-11 (dm) - delete snapshot doc vars
                //if (XmlSnapshot.Exists(oSegment))
                //    XmlSnapshot.Delete(oSegment, true);

                //delete the segment object from the collection
                if (oSegment.IsTopLevel)
                {
                    this.Segments.Delete(oSegment.FullTagID);
                }
                else
                {
                    XmlSegment oParent = oSegment.Parent;

                    //GLOG 6362 (dm) - refresh collection table parent after deleting
                    //first item in collection to account for any non-native mDels
                    bool bIsFirstItemInCollection = ((oParent is XmlCollectionTable) &&
                        (!oParent.IsTopLevel) && (oParent.Segments[0].TagID == oSegment.TagID));

                    oParent.Segments.Delete(oSegment.FullTagID);

                    ////refresh parent's node store - it may have been necessary to
                    ////delete and recreate parent's tag in order to delete child
                    //try
                    //{
                    //    oParent.RefreshNodes();

                    //    //GLOG 6362 (dm)
                    //    if (bIsFirstItemInCollection)
                    //        oParent.Parent.RefreshNodes();
                    //}
                    //catch { }
                }
                oSegment.WPDoc.MainDocumentPart.RootElement.Save();
            }
            finally
            {
                XmlForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
                LMP.Benchmarks.Print(t0, oSegment.DisplayName);
            }
        }

        ///// <summary>
        ///// inserts the segment with the specified ID for design purposes
        ///// </summary>
        ///// <param name="xSegmentID"></param>
        ///// <returns></returns>
        //public XmlSegment InsertSegmentForDesign(string xSegmentID)
        //{
        //    return SetUpSegmentForDesign(xSegmentID, true);
        //}

        ///// <summary>
        ///// sets up the segment with the specified ID for design purposes -
        ///// assumes valid xml exists in the document
        ///// </summary>
        ///// <param name="xSegmentID"></param>
        ///// <returns></returns>
        //public XmlSegment SetUpSegmentForDesign(string xSegmentID)
        //{
        //    return SetUpSegmentForDesign(xSegmentID, false);
        //}
        /// <summary>
        /// Deletes existing segment and replaces with latest definition of same segment
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="oParentSegment"></param>
        public void UpdateSegmentDesign(string xSegmentID, XmlSegment oParentSegment)
        {
            XmlSegment.Replace(xSegmentID, xSegmentID, oParentSegment, this, null, null, true);

            //refresh parent
            if (oParentSegment != null)
            {
                XmlSegment oNewParent = null;

                //GLOG 3640 (dm) - it turns out that the issue I addressed in GLOG 4381
                //below is a more general problem when replacing collection table items -
                //if the user has changed the type, the parent collection segment will
                //no longer be in the document - in this case, refresh the top-level parent
                if (oParentSegment is XmlCollectionTable)
                {
                    XmlSegments oSegs = null;
                    if (oParentSegment.IsTopLevel)
                        oSegs = this.Segments;
                    else
                        oSegs = oParentSegment.Parent.Segments;
                    oNewParent = XmlSegment.FindSegment(oParentSegment.TypeID, oSegs);
                    if (oNewParent == null)
                        oNewParent = this.Segments[0];
                }
                else
                    oNewParent = oParentSegment;

                //refresh
                oNewParent.Refresh();
            }
        }
        public void UpdateSegment(string xSegmentID, XmlSegment oParentSegment, XmlPrefill oPrefill, string xBodyText)
        {
            UpdateSegment(xSegmentID, oParentSegment, oPrefill, xBodyText, false);
        }
        public void UpdateSegment(string xSegmentID, XmlSegment oParentSegment, XmlPrefill oPrefill, string xBodyText, bool bDesign)
        {
            XmlSegment.Replace(xSegmentID, xSegmentID, oParentSegment, this, oPrefill, xBodyText, bDesign);
            //GLOG 4433 (dm) - the following code errs when recreating collection table items,
            //because the parent also gets replaced - it's also unnecessary because the
            //entire doc gets refreshed immediately after this method is called
            //if (oParentSegment != null)
            //    oParentSegment.Refresh();
        }
        ///// <summary>
        ///// sets up the segment with the specified ID for design purposes-
        ///// either runs InsertForDesign or SetUpForDesign, based on specified request
        ///// </summary>
        ///// <param name="xDefinitionID"></param>
        ///// <param name="bInsert"></param>
        ///// <returns></returns>
        //private XmlSegment SetUpSegmentForDesign(string xDefinitionID, bool bInsert)
        //{
        //    DateTime t0 = DateTime.Now;
        //    XmlSegment oSegment = null;

        //    //save state
        //    LMP.Architect.Oxml.Environment oEnv = new Environment(this.WordDocument);
        //    oEnv.SaveState();

        //    try
        //    {
        //        if (bInsert)
        //            oSegment = XmlSegment.InsertForDesign(xDefinitionID, this);
        //        else
        //            oSegment = XmlSegment.SetUpForDesign(xDefinitionID, this);
        //    }
        //    finally
        //    {
        //        //restore state
        //        oEnv.RestoreState();

        //        //after restoring state, always force ShowXMLMarkup when entering 
        //        //Design mode pass Undefined to avoid resetting anything else
        //        mpTriState iUndefined = mpTriState.Undefined;

        //        oEnv.SetExecutionState(iUndefined, iUndefined, 
        //            iUndefined, iUndefined, mpTriState.True, false);
        //    }

        //    //mark as not dirty
        //    this.WordDocument.Saved = true;

        //    LMP.Benchmarks.Print(t0);

        //    return oSegment;
        //}

        ///// <summary>
        ///// returns the segment with the specified tag id in the specified MacPac document
        ///// </summary>
        ///// <param name="xTagID"></param>
        ///// <param name="oForteDoc"></param>
        ///// <returns></returns>
        //public XmlSegment GetSegment(string xTagID, XmlForteDocument oForteDoc)
        //{
        //    XmlSegment oSegment = XmlSegment.GetSegment(xTagID, oForteDoc, true, true);
        //    return oSegment;
        //}
        //public XmlSegment GetSegment(string xTagID, XmlForteDocument oForteDoc, bool bExecuteActions)
        //{
        //    XmlSegment oSegment = XmlSegment.GetSegment(xTagID, oForteDoc, bExecuteActions, true);
        //    return oSegment;
        //}
        //public XmlSegment GetSegment(string xTagID, XmlForteDocument oForteDoc, bool bExecuteActions, bool bPromptForMissingAuthors)
        //{
        //    XmlSegment oSegment = XmlSegment.GetSegment(xTagID, oForteDoc, bExecuteActions, bPromptForMissingAuthors);
        //    return oSegment;
        //}
        ///// <summary>
        ///// returns value of designated word tag attribute
        ///// </summary>
        ///// <param name="oXMLNode"></param>
        ///// <param name="xAttributeName"></param>
        ///// <returns></returns>
        //public string GetAttributeValue(Word.XMLNode oXMLNode, string xAttributeName)
        //{
        //    return LMP.String.Decrypt(oXMLNode.SelectSingleNode(
        //        "@" + xAttributeName, null, true).NodeValue);
        //}
        ///// <summary>
        ///// returns value of designated content control attribute
        ///// </summary>
        ///// <param name="oXMLNode"></param>
        ///// <param name="xAttributeName"></param>
        ///// <returns></returns>
        //public string GetAttributeValue(Word.ContentControl oCC, string xAttributeName)
        //{
        //    LMP.Forte.MSWord.WordDoc oDocClass = LMP.Forte.MSWord.WordDoc;
        //    return oDocClass.GetAttributeValue_CC(oCC, xAttributeName);
        //}
        ///// <summary>
        ///// returns value of designated segment bookmark attribute
        ///// </summary>
        ///// <param name="oXMLNode"></param>
        ///// <param name="xAttributeName"></param>
        ///// <returns></returns>
        //public string GetAttributeValue(Word.Bookmark oBmk, string xAttributeName)
        //{
        //    LMP.Forte.MSWord.WordDoc oDocClass = LMP.Forte.MSWord.WordDoc;
        //    return oDocClass.GetAttributeValue_Bmk(oBmk, xAttributeName);
        //}
        ///// <summary>
        ///// inserts at the specified location the segments defined in the specified task set
        ///// </summary>
        ///// <param name="iTaskSetID"></param>
        ///// <param name="oLocation"></param>
        ///// <returns></returns>
        //public XmlSegment[] InsertTaskSet(int iTaskSetID, Word.Range oLocation)
        //{
        //    //TODO: implement this method
        //    return null;
        //}

		/// <summary>
		/// returns the segment with the specified segment tag ID
		/// </summary>
		/// <param name="xSegmentTagID"></param>
		/// <returns></returns>
        public XmlSegment FindSegment(string xSegmentTagID)
        {
            throw new LMP.Exceptions.NotImplementedException("Method not implemented.");
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xSegmentTagID", xSegmentTagID);

            //get the position of the first segment separator
            int iPos = xSegmentTagID.IndexOf(".");

            //if (this.Segments.Count == 0)
            //{
            //    //ensure that we have a populated segments collection
            //    this.RefreshSegments(false, false);
            //}

            XmlSegment oSegment = null;
            string xTagID = "";

            if (iPos == -1)
            {
                try
                {
                    //there is no segment separator - return 
                    //the segment with this tag ID
                    oSegment = this.Segments[xSegmentTagID];
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.NotInCollectionException(
                        LMP.Resources.GetLangString(
                            "Error_ItemWithIDNotInCollection") + xSegmentTagID, oE);
                }
            }
            else
            {
                //cycle through all segments in tag ID,
                //returning each as the child of the previous tag
                while (iPos > -1)
                {
                    //get nested segment
                    xTagID = xSegmentTagID.Substring(0, iPos);

                    try
                    {
                        if (oSegment == null)
                        {
                            //this is the top level segment
                            oSegment = this.Segments[xTagID];
                            if (oSegment == null)
                                return null;
                        }
                        else
                            oSegment = oSegment.Segments[xTagID];
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString(
                                "Error_ItemWithIDNotInCollection") + xSegmentTagID, oE);
                    }

                    //get the next segment separator
                    iPos = xSegmentTagID.IndexOf(".", iPos + 1);
                }

                try
                {
                    //get last segment in tag ID
                    oSegment = oSegment.Segments[xSegmentTagID];
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.NotInCollectionException(
                        LMP.Resources.GetLangString(
                        "Error_ItemWithIDNotInCollection") + xSegmentTagID, oE);
                }
            }

            LMP.Benchmarks.Print(t0);

            return oSegment;
        }
        /// <summary>
        /// refreshes this ForteDocument
        /// </summary>
        /// <param name="oOptions"></param>
        public void Refresh(RefreshOptions oOptions)
        {
            Refresh(oOptions, true);
        }
        public void Refresh(RefreshOptions oOptions, bool bExecuteActions)
        {
            Refresh(oOptions, bExecuteActions, true);
        }
        public void Refresh(RefreshOptions oOptions, bool bExecuteActions, bool bPromptForMissingAuthors)
		{
			DateTime t0 = DateTime.Now;

            throw new LMP.Exceptions.NotImplementedException("Method not implemented.");
            ////get top level segments
            //this.RefreshSegments(bExecuteActions, bPromptForMissingAuthors);

            LMP.Benchmarks.Print(t0);
		}

        /// <summary>
        /// raises the BeforeVariableActionsExecuted event
        /// </summary>
        /// <param name="xTagID"></param>
        internal void RaiseVariableDeletedEvent(XmlVariable oVar)
        {
            if (this.VariableDeleted != null)
                this.VariableDeleted(this, new VariableEventArgs(oVar));
        }

        /// <summary>
        /// raises the VariableAdded event
        /// </summary>
        /// <param name="oVar"></param>
        internal void RaiseVariableAddedEvent(XmlVariable oVar)
        {
            if (this.VariableAdded != null)
                this.VariableAdded(this, new VariableEventArgs(oVar));
        }

        /// <summary>
        /// raises the VariableDefinitionChanged event
        /// </summary>
        /// <param name="oVar"></param>
        internal void RaiseVariableDefinitionChangedEvent(XmlVariable oVar)
        {
            if (this.VariableDefinitionChanged != null)
                this.VariableDefinitionChanged(this, new VariableEventArgs(oVar));
        }

        /// <summary>
        internal void RaiseBlockDeletedEvent(XmlBlock oBlock)
        {
            if (this.BlockDeleted != null)
                this.BlockDeleted(this, new BlockEventArgs(oBlock));
        }

        /// <summary>
        /// raises the BlockAdded event
        /// </summary>
        /// <param name="oBlock"></param>
        internal void RaiseBlockAddedEvent(XmlBlock oBlock)
        {
            if (this.BlockAdded != null)
                this.BlockAdded(this, new BlockEventArgs(oBlock));
        }

        /// <summary>
        /// raises the BlockDefinitionChanged event
        /// </summary>
        /// <param name="oBlock"></param>
        internal void RaiseBlockDefinitionChangedEvent(XmlBlock oBlock)
        {
            if (this.BlockDefinitionChanged != null)
                this.BlockDefinitionChanged(this, new BlockEventArgs(oBlock));
        }

        /// <summary>
        /// raises the SegmentRefreshedByAction event
        /// </summary>
        /// <param name="oVar"></param>
        internal void RaiseSegmentRefreshedByActionEvent(XmlSegment oSegment)
        {
            if (this.SegmentRefreshedByAction != null)
                this.SegmentRefreshedByAction(this, new SegmentEventArgs(oSegment));
        }

        /// <summary>
        /// raises the SegmentDeleted event
        /// </summary>
        /// <param name="oSeg"></param>
        internal void RaiseSegmentDeletedEvent(XmlSegment oSeg)
        {
            if (this.SegmentDeleted != null)
                this.SegmentDeleted(this, new SegmentEventArgs(oSeg));
        }

        /// <summary>
        /// raises the SegmentAdded event
        /// </summary>
        /// <param name="oSeg"></param>
        internal void RaiseSegmentAddedEvent(XmlSegment oSeg)
        {
            if (this.SegmentAdded != null)
                this.SegmentAdded(this, new SegmentEventArgs(oSeg));
        }

        /// <summary>
        /// raises the SegmentDistributedSectionsChanged event
        /// </summary>
        /// <param name="oSeg"></param>
        internal void RaiseSegmentDistributedSectionsChangedEvent(XmlSegment oSeg)
        {
            if (this.SegmentDistributedSectionsChanged != null)
                this.SegmentDistributedSectionsChanged(this, new SegmentEventArgs(oSeg));
        }

        /// <summary>
        /// raises the BeforeSegmentReplacedByAction event
        /// </summary>
        /// <param name="oSegment"></param>
        internal void RaiseBeforeSegmentReplacedByActionEvent(XmlSegment oSegment)
        {
            if (this.BeforeSegmentReplacedByAction != null)
                this.BeforeSegmentReplacedByAction(this, new SegmentEventArgs(oSegment));
        }

        /// <summary>
        /// raises the BeforeCollectionTableStructureInserted event
        /// </summary>
        /// <param name="oSegment"></param>
        internal void RaiseBeforeCollectionTableStructureInsertedEvent(XmlSegment oSegment)
        {
            if (this.BeforeCollectionTableStructureInserted != null)
                this.BeforeCollectionTableStructureInserted(this, new SegmentEventArgs(oSegment));
        }

        /// <summary>
        /// raises the AfterCollectionTableStructureInserted event
        /// </summary>
        /// <param name="oSegment"></param>
        internal void RaiseAfterCollectionTableStructureInsertedEvent(XmlSegment oSegment)
        {
            if (this.AfterCollectionTableStructureInserted != null)
                this.AfterCollectionTableStructureInserted(this, new SegmentEventArgs(oSegment));
        }

        public void RebuildSegment(XmlSegment oSegment)
		{
		}
		public void RebuildSegmentAs(XmlSegment oSegment, string xSegmentID, 
            XmlSegment.InsertionLocations iInsertionLocation, 
            XmlSegment.InsertionBehaviors iInsertionBehavior)
		{
            //XmlPrefill oPrefill = oSegment.CreatePrefill();

            ////get current insertion location
            //Word.Range oRng = oSegment.PrimaryRange;

            ////delete current segment
            //object oMissing = System.Type.Missing;
            //oRng.Delete(ref oMissing, ref oMissing);

            ////insert new segment of specified type at location
            //XmlSegment.Insert(xSegmentID, null, iInsertionLocation,
            //    iInsertionBehavior, this, oPrefill, true, null);

            ////add old body
            ////TODO: add old body to rebuilt segment
		}

     

        ///// <summary>
        ///// Copies all or specified styles defined in source XML
        ///// </summary>
        ///// <param name="xBaseXML"></param>
        ///// <param name="aStyleList"></param>
        //public void UpdateStylesFromXML(string xBaseXML)
        //{
        //    UpdateStylesFromXML(xBaseXML, null, m_oWordDocument);
        //}
        //public void UpdateStylesFromXML(string xBaseXML, string[] aStyleList)
        //{
        //    UpdateStylesFromXML(xBaseXML, aStyleList, m_oWordDocument);
        //}
        ///// <summary>
        ///// Copies all or specified styles defined in source XML
        ///// </summary>
        ///// <param name="xBaseXML"></param>
        ///// <param name="aStyleList"></param>
        //public static void UpdateStylesFromXML(string xBaseXML, Word.Document oTargetDocument)
        //{
        //    UpdateStylesFromXML(xBaseXML, null, oTargetDocument);
        //}
        //public static void UpdateStylesFromXML(string xBaseXML, string[] aStyleList, Word.Document oTargetDocument)
        //{
        //    DateTime t0 = DateTime.Now;

        //    string xSourcePath = "";

        //    try
        //    {
        //        //Extract style nodes from XML
        //        //string xStyleXML = LMP.String.GetMinimalStyleDocumentXML(xBaseXML);
        //        string xStyleXML = xBaseXML;
        //        // Convert XML text to byte array
        //        byte[] bytXML = System.Text.Encoding.UTF8.GetBytes((xStyleXML).ToCharArray());

        //        // Create temporary file
        //        xSourcePath = Path.GetTempFileName();

        //        try
        //        {
        //            FileStream oStream = File.Open(xSourcePath, FileMode.Create);

        //            // Write bytXML to temporary file
        //            oStream.Write(bytXML, 0, bytXML.Length);
        //            oStream.Close();
        //        }
        //        catch (System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.XMLException(oE.Message);
        //        }

        //        //GLOG 7915 (dm) - the ContentControlExit event is sometimes firing here
        //        XmlForteDocument.WordXMLEvents oEvents = XmlForteDocument.IgnoreWordXMLEvents;
        //        XmlForteDocument.IgnoreWordXMLEvents = XmlForteDocument.WordXMLEvents.All;
        //        try
        //        {
        //            DateTime t1 = DateTime.Now;
        //            LMP.Forte.MSWord.WordDoc.OrganizerCopyStyles(oTargetDocument, xSourcePath, aStyleList);
        //            LMP.Benchmarks.Print(t1, "OrganizerCopyStyles");
        //        }
        //        catch (System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.WordDocException(oE.Message);
        //        }
        //        finally
        //        {
        //            XmlForteDocument.IgnoreWordXMLEvents = oEvents;
        //        }
        //    }
        //    finally
        //    {
        //        try
        //        {
        //            // clean up temporary file
        //            if (File.Exists(xSourcePath))
        //                File.Delete(xSourcePath);
        //        }
        //        catch (IOException)
        //        {
        //            OS.AddFileToDeletionList(xSourcePath);
        //        }
        //        LMP.Benchmarks.Print(t0);
        //    }

        //}
        /// <summary>
        /// Returns one-dimensional array containing names of styles in document
        /// </summary>
        /// <param name="bInUseOnly"></param>
        /// <returns></returns>
        public string[] GetStyleArray(bool bInUseOnly)
        {
            System.Collections.Generic.List<string> aStyles = new System.Collections.Generic.List<string>();
            //TODO: OpenXML rewrite
            //foreach (Word.Style oStyle in this.WordDocument.Styles)
            //{
            //    if (!bInUseOnly || oStyle.InUse)
            //    {
            //        aStyles.Add(oStyle.NameLocal);
            //    }
            //}
            return aStyles.ToArray();
        }
        /// <summary>
        /// Insert sample paragraphs in each document style
        /// </summary>
        public void InsertStyleSummary(bool bInUseOnly, string xDescription)
        {
            //const string xTemplate = "{0} {0} {0} {0} {0}\r\n";
            //string xSampleText = LMP.Resources.GetLangString("Prompt_StyleSummarySample");
            ////GLOG 4440: This code has been restored since Word 2003 compatibility is no longer a concern
            //try
            //{
            //    //Convert from XML to avoid Compatibility Mode
            //    this.WordDocument.Convert();
            //}
            //catch { }
            //string[] aStyles = GetStyleArray(bInUseOnly);
            //Word.Range oRng = this.WordDocument.Content;
            //object oEnd = Word.WdCollapseDirection.wdCollapseEnd;
            //object oSpaceAfter = 24;
            //object oNormalStyleID = Word.WdBuiltinStyle.wdStyleNormal;
            //object oDefaultParaID = Word.WdBuiltinStyle.wdStyleDefaultParagraphFont;
            //Word.Style oNormal = this.WordDocument.Styles.get_Item(ref oNormalStyleID);
            //string xNormalName = oNormal.NameLocal;
            //string xSentence = string.Format(xSampleText, xNormalName + " paragraph");
            //string xParagraph = string.Format(xTemplate, xSentence);
            //object oNormalObject = (object)xNormalName;
            //oRng.set_Style(ref oNormalObject);
            //oRng.InsertAfter(xDescription + "\r\n");
            //oRng.Font.Bold = 1;
            //oRng.Paragraphs[1].Borders.OutsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
            //oRng.Paragraphs[1].Range.HighlightColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdYellow;
            //oRng.ParagraphFormat.SpaceAfter = 24;
            //oRng.Collapse(ref oEnd);
            //oRng.InsertAfter(xParagraph);
            //oRng.Font.Reset();
            //oRng.ParagraphFormat.SpaceAfter = 24;
            //oRng.Collapse(ref oEnd);
            //foreach (string xStyle in aStyles)
            //{
            //    object oStyleObject = (object)xStyle;
            //    Word.Style oStyle = this.WordDocument.Styles.get_Item(ref oStyleObject);
            //    object oStyleName = (object)oStyle.NameLocal;
            //    string xType = "";
            //    switch (oStyle.Type)
            //    {
            //        case Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeCharacter:
            //            //Skip Default Paragraph Font style
            //            if (oStyle.NameLocal == this.WordDocument.Styles.get_Item(ref oDefaultParaID).NameLocal)
            //                continue;
            //            xType = " character";
            //            break;
            //        case Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph:
            //            //Skip Normal Style, since it's already included
            //            if (oStyle.NameLocal == this.WordDocument.Styles.get_Item(ref oNormalStyleID).NameLocal)
            //                continue;
            //            xType = " paragraph";
            //            break;
            //        case Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeTable:
            //            xType = " table";
            //            //Don't process this, as it's only valid inside a table
            //            continue;
            //        case Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeList:
            //            xType = " list";
            //            break;
            //    }
            //    xSentence = string.Format(xSampleText, oStyle.NameLocal + xType);
            //    xParagraph = string.Format(xTemplate, xSentence);
            //    //Reset Normal first
            //    oRng.set_Style(ref oNormalObject);
            //    oRng.set_Style(ref oStyleName);
            //    oRng.InsertAfter(xParagraph);
            //    oRng.ParagraphFormat.SpaceAfter = 24;
            //    oRng.Collapse(ref oEnd);
            //}
            //oRng.set_Style(ref oNormalObject);
        }
        /// <summary>
        /// Removes the temp Callout shape object if it exists
        /// </summary>
        public void ClearCallout()
        {
            //Word.Document oDoc = this.WordDocument;
            //LMP.Forte.MSWord.WordDoc.RemoveCalloutIfNecessary(ref oDoc);
        }

        //public XmlSegment GetTopLevelSegmentForRange(Word.Range oRng)
        //{
        //    return GetTopLevelSegmentForRange(oRng, false);
        //}

        //public XmlSegment GetTopLevelSegmentForRange(Word.Range oRng,
        //    bool bIncludeIntendedAsDocument)
        //{
        //    DateTime t0 = DateTime.Now;

        //    //GLOG 6005 (dm) - added bIncludeIntendedAsDocument parameter
        //    XmlSegment oSegment = null;
        //    if (this.FileFormat == mpFileFormats.OpenXML)
        //    {
        //        //GLOG 4215: Allows getting top level segment for a range without affecting current Selection
        //        Word.ContentControl oCC = null;
        //        try
        //        {
        //            oCC = oRng.ParentContentControl;
        //        }
        //        catch { }
        //        if (oCC == null)
        //        {
        //            //If not parent tag, look for first tag within range
        //            if (oRng.Start == oRng.End)
        //            {
        //                //If range is collapsed, extend end 1 character to encompass any starting XML tag
        //                object oChar = Word.WdUnits.wdCharacter;
        //                object oCount = 1;
        //                try
        //                {
        //                    oRng.MoveEnd(ref oChar, ref oCount);
        //                }
        //                catch { }
        //            }
        //            if (oRng.ContentControls.Count > 0)
        //            {
        //                object iIndex = 1;
        //                oCC = oRng.ContentControls.get_Item(ref iIndex);
        //            }
        //        }

        //        //keep checking parent node until an mSEG is found
        //        while ((oCC != null) && (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mSEG"))
        //            oCC = oCC.ParentContentControl;

        //        string xSegFullTagID = "";
        //        if (oCC == null)
        //        {
        //            Word.Bookmark oParentBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oRng);
        //            if (oParentBmk != null)
        //                xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oParentBmk);
        //        }
        //        else
        //        {
        //            //get the segment corresponding to tag
        //
        //            xSegFullTagID = oDoc.GetFullTagID_CC(oCC);
        //        }
        //        if (!System.String.IsNullOrEmpty(xSegFullTagID))
        //            oSegment = this.FindSegment(xSegFullTagID);
        //    }
        //    else if (LMP.Forte.MSWord.WordApp.Version < 15) //XML Tags not supported in Word 2013
        //    {
        //        //GLOG 4215: Allows getting top level segment for a range without affecting current Selection
        //        Word.XMLNode oTag = null;
        //        try
        //        {
        //            oTag = oRng.XMLParentNode;
        //        }
        //        catch { }
        //        if (oTag == null)
        //        {
        //            //If not parent tag, look for first tag within range
        //            if (oRng.Start == oRng.End)
        //            {
        //                //If range is collapsed, extend end 1 character to encompass any starting XML tag
        //                object oChar = Word.WdUnits.wdCharacter;
        //                object oCount = 1;
        //                try
        //                {
        //                    oRng.MoveEnd(ref oChar, ref oCount);
        //                }
        //                catch { }
        //            }
        //            if (oRng.XMLNodes.Count > 0)
        //            {
        //                oTag = oRng.XMLNodes[1];
        //            }
        //        }

        //        string xSegFullTagID = "";

        //        //keep checking parent node until an mSEG is found
        //        while ((oTag != null) && (oTag.BaseName != "mSEG"))
        //            oTag = oTag.ParentNode;

        //        if (oTag == null)
        //        {
        //            Word.Bookmark oParentBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oRng);
        //            if (oParentBmk != null)
        //                xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oParentBmk);
        //        }
        //        else
        //        {
        //            //get the segment corresponding to tag
        //
        //            xSegFullTagID = oDoc.GetFullTagID(oTag);
        //        }
        //        if (!System.String.IsNullOrEmpty(xSegFullTagID))
        //            oSegment = this.FindSegment(xSegFullTagID);
        //    }

        //    //7/10/08 - removed the following block because an mSEG
        //    //may belong to a different client
        //    //if (oSegment == null)
        //    //    //something is wrong - there's an mSEG tag but no
        //    //    //corresponding mSEG
        //    //    throw new LMP.Exceptions.SegmentException(
        //    //        LMP.Resources.GetLangString("Error_mSEGButNoSegment") + xSegFullTagID);

        //    //cycle through parents until we 
        //    //get to the top level segment
        //    //GLOG 6005 (dm) - added exceptions for TOA and intended "as document"
        //    if (oSegment != null)
        //    {
        //        while ((oSegment.Parent != null) && ((!bIncludeIntendedAsDocument) ||
        //            ((!(oSegment is LMP.Architect.Oxml.XmlTOA)) &&
        //            (oSegment.IntendedUse != mpSegmentIntendedUses.AsDocument))))
        //            oSegment = oSegment.Parent;
        //    }

        //    LMP.Benchmarks.Print(t0);

        //    return oSegment;

        //}
        ///// <summary>
        ///// returns the top level segment that
        ///// contains the current selection
        ///// </summary>
        ///// <returns></returns>
        //public XmlSegment GetSegmentFromSelection(LMP.Data.mpObjectTypes iType)
        //{
        //    //JTS 5/31/10 10.2: Added Content Control compatibility
        //    XmlSegment oSegment = null;
        //    if (this.FileFormat == mpFileFormats.OpenXML)
        //    {
        //        Word.Range oRange = LMP.Forte.MSWord.WordApp
        //            .CurrentWordApplication().Selection.Range;
        //        Word.ContentControl oCC = oRange.ParentContentControl;
        //        string xSegFullTagID = "";

        //        //GLOG 4979 (dm) - cursor may be immediately before
        //        //top-level content control
        //        if (oCC == null)
        //        {
        //            object oMissing = System.Type.Missing;
        //            oRange.Move(ref oMissing, ref oMissing);
        //            oCC = oRange.ParentContentControl;
        //        }

        //        if (oCC == null)
        //        {
        //            Word.Bookmark oBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oRange);

        //            if (oBmk == null)
        //            {
        //                return null;
        //            }
        //            else
        //            {
        //                //get the segment corresponding to bookmark
        //                xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oBmk);

        //                if (!System.String.IsNullOrEmpty(xSegFullTagID))
        //                    oSegment = this.FindSegment(xSegFullTagID);
        //            }
        //        }
        //        else
        //        {
        //            //keep checking parent node until an mSEG is found
        //            while ((oCC != null) && (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mSEG"))
        //                oCC = oCC.ParentContentControl;
        //        }

        //        if (oCC != null)
        //        {
        //            //get the segment corresponding to tag
        //            xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oCC);
        //            if (!System.String.IsNullOrEmpty(xSegFullTagID))
        //                oSegment = this.FindSegment(xSegFullTagID);
        //        }
        //    }
        //    else if (LMP.Forte.MSWord.WordApp.Version < 15) //XML Tags not supported in Word 2013
        //    {
        //        string xSegFullTagID = "";

        //        Word.Application oWordApp = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
        //        Word.Range oRange = oWordApp.Selection.Range;

        //        Word.XMLNode oTag = oWordApp.Selection.XMLParentNode;

        //        if (oTag == null)
        //        {
        //            Word.Bookmark oBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oRange);

        //            if (oBmk == null)
        //            {
        //                return null;
        //            }
        //            else
        //            {
        //                //get the segment corresponding to bookmark
        //                xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oBmk);

        //                if (!System.String.IsNullOrEmpty(xSegFullTagID))
        //                    oSegment = this.FindSegment(xSegFullTagID);
        //            }
        //        }
        //        else
        //        {
        //            //keep checking parent node until an mSEG is found
        //            while ((oTag != null) && (oTag.BaseName != "mSEG"))
        //                oTag = oTag.ParentNode;
        //        }

        //        if (oTag != null)
        //        {
        //            //get the segment corresponding to tag
        //
        //            xSegFullTagID = oDoc.GetFullTagID(oTag);
        //            if (!System.String.IsNullOrEmpty(xSegFullTagID))
        //                oSegment = this.FindSegment(xSegFullTagID);
        //        }
        //    }

        //    //cycle through parents until we
        //    //find the segment of the specified type,
        //    //or we get to the top level segment
        //    if (oSegment != null)
        //    {
        //        if (oSegment.TypeID == iType)
        //        {
        //            return oSegment;
        //        }
        //        else
        //        {
        //            while (oSegment.Parent != null)
        //            {
        //                if (oSegment.Parent.TypeID == iType)
        //                    return oSegment;

        //                oSegment = oSegment.Parent;
        //            }
        //        }
        //    }

        //    return null;
        //}

        ///// <summary>
        ///// returns the top level segment that
        ///// contains the current selection
        ///// </summary>
        ///// <returns></returns>
        //public XmlSegment GetTopLevelSegmentFromSelection()
        //{
        //    //JTS 1/17/13: Use GetTopLevelSegmentForRange on Selection
        //    Word.Range oRange = LMP.Forte.MSWord.WordApp
        //        .CurrentWordApplication().Selection.Range;
        //    return GetTopLevelSegmentForRange(oRange);
            
        //    ////JTS 5/31/10 10.2: Added Content Control compatibility
        //    //Segment oSegment = null;
        //    //Word.Bookmark oBmk = null;
        //    //if (this.FileFormat == mpFileFormats.OpenXML)
        //    //{
        //    //    Word.Range oRange = LMP.Forte.MSWord.WordApp
        //    //        .CurrentWordApplication().Selection.Range;
        //    //    Word.ContentControl oCC = oRange.ParentContentControl;
        //    //    //GLOG 4979 (dm) - cursor may be immediately before
        //    //    //top-level content control
        //    //    if (oCC == null)
        //    //    {
        //    //        object oMissing = System.Type.Missing;
        //    //        oRange.Move(ref oMissing, ref oMissing);
        //    //        oCC = oRange.ParentContentControl;
        //    //    }

        //    //    if (oCC == null)
        //    //        oBmk = GetTopLevelSegmentForRange
        //    //        return null;
        //    //    else
        //    //        //keep checking parent node until an mSEG is found
        //    //        while ((oCC != null) && (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mSEG"))
        //    //            oCC = oCC.ParentContentControl;
        //    //    string xSegFullTagID = "";
        //    //    if (oCC != null)
        //    //    {
        //    //        //get the segment corresponding to tag
        //    //
        //    //        xSegFullTagID = oDoc.GetFullTagID_CC(oCC);
        //    //        if (!System.String.IsNullOrEmpty(xSegFullTagID))
        //    //            oSegment = this.FindSegment(xSegFullTagID);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    Word.XMLNode oTag = LMP.Forte.MSWord.WordApp
        //    //        .CurrentWordApplication().Selection.XMLParentNode;

        //    //    if (oTag == null)
        //    //        return null;
        //    //    else
        //    //        //keep checking parent node until an mSEG is found
        //    //        while ((oTag != null) && (oTag.BaseName != "mSEG"))
        //    //            oTag = oTag.ParentNode;
        //    //    string xSegFullTagID = "";
        //    //    if (oTag != null)
        //    //    {
        //    //        //get the segment corresponding to tag
        //    //
        //    //        xSegFullTagID = oDoc.GetFullTagID(oTag);
        //    //        if (!System.String.IsNullOrEmpty(xSegFullTagID))
        //    //            oSegment = this.FindSegment(xSegFullTagID);
        //    //    }
        //    //}

        //    ////7/10/08 - removed the following block because an mSEG
        //    ////may belong to a different client
        //    ////if (oSegment == null)
        //    ////    //something is wrong - there's an mSEG tag but no
        //    ////    //corresponding mSEG
        //    ////    throw new LMP.Exceptions.SegmentException(
        //    ////        LMP.Resources.GetLangString("Error_mSEGButNoSegment") + xSegFullTagID);

        //    ////cycle through parents until we 
        //    ////get to the top level segment
        //    ////if (oSegment != null)
        //    ////{
        //    ////    while (oSegment.Parent != null)
        //    ////        oSegment = oSegment.Parent;
        //    ////}

        //    //return oSegment;
        //}

        /// <summary>
        /// updates the contact detail for the macpac document
        /// </summary>
        public bool UpdateContactDetail()
        {
            try
            {
                bool bDetailUpdated = false;
                for (int i = 0; i < this.Segments.Count; i++)
                    bDetailUpdated = this.Segments[i].UpdateContactDetail();
                return bDetailUpdated;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_CouldNotUpdat.ForteDocumentContactDetail"), oE);
            }
        }

        /// <summary>
        /// returns true if the document contains any segments or standalone
        /// variables or blocks belonging to the current client
        /// </summary>
        /// <returns></returns>
        public bool ContainsClientContent()
        {
            //segments
            for (int i = 0; i < this.Segments.Count; i++)
            {
                if (this.Segments[i].BelongsToCurrentClient)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// creates a snapshot for each segment
        /// in the MacPac Document
        /// </summary>
        public void CreateSegmentSnapshots()
        {
            //cycle through all top level segments in macpac document
            //creating a "snapshot" of variable values
            for (int i = 0; i < this.Segments.Count; i++)
            {
                //GLOG 6060 (dm) - skip trailers
                XmlSegment oSegment = this.Segments[i];
                if (oSegment.TypeID != mpObjectTypes.Trailer && !(oSegment is LMP.Architect.Base.IStaticDistributedSegment))  //GLOG 7874
                    XmlSnapshot.Create(oSegment);
            }
        }
        /// <summary>
        /// returns whether the MacPac Document contains any unfinished segments
        /// </summary>
        /// <returns></returns>
        public bool ContainsUnfinishedContent()
        {
            for (int i = 0; i < this.Segments.Count; i++)
            {
                XmlSegment oSegment = this.Segments[i];
                //10-10-11 (dm) - modified to check for variables and blocks
                //instead of snapshots - segments designed without variables
                //will not have snapshots but should be treated as finished
                if (oSegment.HasVariables(true) || oSegment.HasBlocks(true))
                    return true;
            }
            return false;
        }

        public bool ContainsUnfinishedTopLevelContent()
        {
            for (int i = 0; i < this.Segments.Count; i++)
            {
                XmlSegment oSegment = this.Segments[i];
                //10-10-11 (dm) - modified to check for variables and blocks
                //instead of snapshots - segments designed without variables
                //will not have snapshots but should be treated as finished
                if (oSegment.HasVariables(false) || oSegment.HasBlocks(false))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// sets the text of the specified content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="xText"></param>
        public  void SetText(SdtElement oCC, string xText)
        {
            DateTime t0 = DateTime.Now;

            Type oCCType = oCC.GetType();
            if (oCCType == typeof(SdtRun) && xText.Contains('\r'))
            {
                oCC = ConvertSdtRunToSdtBlock((SdtRun)oCC);
                oCCType = oCC.GetType();
            }
            //If inserting into SdtRun, all delimiters will be replaced with linebreaks.
            //If inserting into SdtBlock '\r' delimiter will separate paragraphs, '\v' and '\r' will be linebreaks within a paragraph
            char[] arSep = oCCType == typeof(SdtRun) ? new char[] {'\v', '\n', '\r'} : new char[] {'\r'};
            //split into separate paragraph xml if necessary
            string[] aText = xText.Split(arSep);

            if (oCCType == typeof(SdtBlock))
            {
                SetText((SdtBlock)oCC, aText);
            }
            else if (oCCType == typeof(SdtRun))
            {
                SetText((SdtRun)oCC, aText);
            }
            else if (oCCType == typeof(SdtCell))
            {
                oCC = ConvertSdtCellToSdtBlock((SdtCell)oCC);
                if (oCC is SdtBlock)
                {
                    SetText((SdtBlock)oCC, aText);
                }
                else
                    //TODO: Implement for SdtCell
                    throw new LMP.Exceptions.NotImplementedException(
                        "BaseMethods.SetText() not implemented for type " + oCCType.ToString() + ".");
            }
            else
            {
                throw new LMP.Exceptions.NotImplementedException(
                    "BaseMethods.SetText() not implemented for type " + oCCType.ToString() + ".");
            }

            LMP.Benchmarks.Print(t0, xText);
        }

        /// <summary>
        /// sets the specified text (paragraphs) as
        /// the text of the block content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="aText"></param>
        public void SetText(SdtBlock oCC, string[] aRuns)
        {
            //JTS 12/16/16: This has been completed reworked
            //We can't depend on the replacing the InnerText, since
            //what appears as a single text string may span multiple nodes in the InnerXml

            SdtContentBlock oContent = oCC.Descendants<SdtContentBlock>().FirstOrDefault();

            //Check for ParagraphProperties
            OpenXmlElement oPPr = oContent.Descendants<ParagraphProperties>().FirstOrDefault();
            //Check for font formatting applied to text
            OpenXmlElement oRPr = oContent.Descendants<RunProperties>().FirstOrDefault();
            //GLOG 8731: If CC text is only underlined required spaces, don't retain underline formatting
            if (oContent.InnerText != "" && oContent.InnerText == new string((char)160, oContent.InnerText.Length))
            {
                oRPr.RemoveAllChildren<Underline>();
            }

            oContent.RemoveAllChildren();

            //Make sure CC is not showing placeholder
            Query.ClearPlaceholderText(oCC);
            bool bHasText = false;
            foreach (string xRunText in aRuns)
            {
                Paragraph oNewPara = new Paragraph();
                if (oPPr != null)
                {
                    //Apply original Paragraph Formatting
                    oNewPara.AppendChild(oPPr.CloneNode(true));
                }
                if (xRunText.StartsWith("<w:") && xRunText.EndsWith(">"))
                {
                    Paragraph oParaXml = new Paragraph();
                    oParaXml.InnerXml = xRunText;
                    foreach (OpenXmlElement oElement in oParaXml)
                    {
                        oNewPara.Append(oElement.CloneNode(true));
                    }
                }
                else
                {
                    Run oNewRun = oNewPara.AppendChild<Run>(new Run());
                    if (oRPr != null)
                    {
                        //Apply original Run Properties
                        oNewRun.AppendChild(oRPr.CloneNode(true));
                    }
                    if (xRunText != "")
                    {
                        bHasText = true;
                        string[] aText = xRunText.Split(new char[] { '\v', '\n' });
                        for (int i = 0; i < aText.GetLength(0); i++)
                        {
                            string xText = aText[i];
                            oNewRun.AppendChild(new Text() { Text = xText, Space = SpaceProcessingModeValues.Preserve });
                            //Separate multiple lines by linebreak
                            if (i < aText.GetUpperBound(0))
                                oNewRun.AppendChild(new Break());
                        }
                    }
                    //GLOG 8660: Replace tab delimiters in text with Xml
                    oNewPara.InnerXml = oNewPara.InnerXml.Replace("\t", "</w:t></w:r><w:r><w:tab/><w:t>");
                }
                oContent.AppendChild<Paragraph>(oNewPara);
            }
            //GLOG 8700: Leave <w:showingPlcHdr/> in xml if there is no text.
            //Otherwise 4 spaces will be displayed when CC gets focus.
            try
            {
                if (!bHasText)
                    oCC.Elements<SdtProperties>().FirstOrDefault().AppendChild<ShowingPlaceholder>(new ShowingPlaceholder());
            }
            catch { }

        }

        /// <summary>
        /// sets the specified text (paragraphs) as
        /// the text of the run content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="aText"></param>
        public void SetText(SdtRun oCC, string[] aText)
        {
            SdtContentRun oOldContent = oCC.Descendants<SdtContentRun>().FirstOrDefault<SdtContentRun>();
            //Make sure CC is not showing placeholder
            Query.ClearPlaceholderText(oCC);
            OpenXmlElement oProps = oCC.Descendants<RunProperties>().FirstOrDefault();
            //GLOG 8731: If CC text consists only of underlined required spaces, don't retain underline formatting
            if (oOldContent.InnerText != "" && oOldContent.InnerText == new string((char)160, oOldContent.InnerText.Length))
            {
                oProps.RemoveAllChildren<Underline>();
            }
            oOldContent.RemoveAllChildren();
            bool bHasText = false;
            for (int i = 0; i <= aText.GetUpperBound(0); i++)
            {
                string xText = aText[i];
                OpenXmlElement[] aElements = new OpenXmlElement[] { };
                //GLOG 8898: If text is XML, handle differently
                if (xText.StartsWith("<w:") && xText.EndsWith(">"))
                {
                    bHasText = true;
                    //Set InnerXML directly
                    oOldContent.InnerXml = xText;
                    if (oProps != null)
                    {
                        //Apply format to each individual Run
                        foreach (Run oTextRun in oOldContent.Descendants<Run>())
                        {
                            oTextRun.PrependChild(oProps.CloneNode(true));
                        }
                    }
                }
                else
                {
                    if (xText != "")
                    {
                        bHasText = true;
                        //Make sure inserted text retains original formatting
                        if (oProps != null)
                        {
                            aElements = new OpenXmlElement[] { oProps.CloneNode(true), new Text() { Text = xText, Space = SpaceProcessingModeValues.Preserve } };
                        }
                        else
                        {
                            aElements = new OpenXmlElement[] { new Text() { Text = xText, Space = SpaceProcessingModeValues.Preserve } };
                        }
                    }
                    else if (oProps != null)
                    {
                        aElements = new OpenXmlElement[] { oProps.CloneNode(true) };
                    }
                    if (aElements.GetLength(0) > 0)
                    {
                        Run oRun = oOldContent.AppendChild<Run>(new Run(aElements));
                        if (i < aText.GetUpperBound(0))
                        {
                            oOldContent.AppendChild<Break>(new Break());
                        }
                    }
                }
            }
            //GLOG 8660: Replace tab delimiters in text with Xml
            oOldContent.InnerXml = oOldContent.InnerXml.Replace("\t", "</w:t></w:r><w:r><w:tab/><w:t>");
            //GLOG 8700: Leave <w:showingPlcHdr/> in xml if there is no text.
            //Otherwise 4 spaces will be displayed when CC gets focus.
            try
            {
                if (!bHasText)
                    oCC.Elements<SdtProperties>().FirstOrDefault().AppendChild<ShowingPlaceholder>(new ShowingPlaceholder());
            }
            catch { }
        }

        /// <summary>
        /// Inserts Detail SubVar XML
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="aElements"></param>
        public  void SetDetail(SdtElement oCC, string[] aElements, string xStandardDelimiter)
        {
            Type oCCType = oCC.GetType();

            if (oCCType == typeof(SdtBlock))
            {
                SetDetail((SdtBlock)oCC, aElements, xStandardDelimiter);
            }
            else if (oCCType == typeof(SdtRun))
            {
                SetDetail((SdtRun)oCC, aElements, xStandardDelimiter);
            }
            else if (oCCType == typeof(SdtCell))
            {
                oCC = ConvertSdtCellToSdtBlock((SdtCell)oCC);
                if (oCC is SdtBlock)
                {
                    SetDetail((SdtBlock)oCC, aElements, xStandardDelimiter);
                }
                else
                    //TODO: Implement for SdtCell
                    throw new LMP.Exceptions.NotImplementedException(
                        "BaseMethods.SetDetail() not implemented for type " + oCCType.ToString() + ".");

            }
            else
            {
                //TODO: Implement for SdtCell
                throw new LMP.Exceptions.NotImplementedException(
                    "BaseMethods.SetDetail() not implemented for type " + oCCType.ToString() + ".");
            }
        }

        /// <summary>
        /// Inserts Detail SubVar XML
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="aElements"></param>
        public void SetDetail(SdtBlock oCC, string[] aElements, string xStandardDelimiter)
        {
            //get paragraph xml "template"
            Paragraph oOldPara = oCC.Descendants<Paragraph>().FirstOrDefault<Paragraph>();
            //Clear out existing child elements other that <w:rPr>
            //TODO: Are there other types of child elements that might be present?
            oOldPara.RemoveAllChildren<Run>();
            oOldPara.RemoveAllChildren<SdtBlock>();
            oOldPara.RemoveAllChildren<SdtRun>();
            if (oOldPara.Elements<Run>().Count<Run>() == 0)
            {
                //JTS 8/19/15: If Paragraph XML consists only of a single <w:p/> tag, make sure there
                //is at least one Text child
                oOldPara.AppendChild<Run>(new Run(new Text("")));
            }
            string xOldParaXml = oOldPara.OuterXml;
            char[] aOldParaXml = xOldParaXml.ToCharArray();
            //string xOldParaText = oOldPara.InnerText;

            //add dummy element - to be removed later
            Run oDummyElement = oOldPara.InsertAfterSelf<Run>(new Run());

            //delete existing paragraphs
            oCC.Elements<SdtContentBlock>()
                .First<SdtContentBlock>().RemoveAllChildren<Paragraph>();

            //Make sure CC is not showing placeholder
            Query.ClearPlaceholderText(oCC);
            //GLOG 8461:  Changes made to support paragraph or new line separators
            Paragraph oNewPara = null;
            //Insert Detail SubVars
            for (int i = 0; i <= aElements.GetUpperBound(0); i++)
            {
                //add paragraph xml - start with template
                string xNewParaXml = new string(aOldParaXml);
                //JTS: Remove empty Text element added above
                xNewParaXml = xNewParaXml.Replace(" />", "/>");
                xNewParaXml = xNewParaXml.Replace("<w:r><w:t/></w:r>", "");

                string xSubVarXML = aElements[i];

                if (i > 0)
                {
                    //TODO: Might need to flesh this out to deal with other cases
                    switch (xStandardDelimiter)
                    {
                        case "\r\t":
                            //Insert tab before start of Special Reline
                            xSubVarXML = "<w:r><w:tab/></w:r>" + xSubVarXML;
                            break;
                        case "\n\t":
                            //Insert tab before start of Special Reline
                            xSubVarXML = "<w:r><w:br/></w:r><w:r><w:tab/></w:r>" + xSubVarXML; //GLOG 8461
                            break;
                        case "\r":
                            //Nothing to do
                            break;
                        case "\v":
                        case "\n":
                            //Insert other configured text
                            xSubVarXML = "<w:r><w:br/></w:r>" + xSubVarXML;  //GLOG 8461
                            break;
                        default:
                            //trim para character from start
                            xStandardDelimiter.TrimStart('\r');
                            //Insert other configured text
                            xSubVarXML = "<w:r><w:t>" + xStandardDelimiter + "</w:t></w:r>" + xSubVarXML;  //GLOG 8461
                            break;
                    }
                }
                if (xStandardDelimiter.IndexOf('\r') == -1)
                {
                    //All Details are in single paragraph
                    if (oNewPara == null)
                    {
                        //If original paragraph text was empty, insert new text before closing tag of paragraph
                        xNewParaXml = xNewParaXml.Insert(xNewParaXml.LastIndexOf("</w:p>"), xSubVarXML);
                        oNewPara = new Paragraph(xNewParaXml);
                    }
                    else
                    {
                        oNewPara.InnerXml = oNewPara.InnerXml + xSubVarXML;
                    }
                    if (i == aElements.GetUpperBound(0))
                    {
                        //insert paragraph
                        oCC.Descendants<Run>().Last<Run>().InsertBeforeSelf<Paragraph>(oNewPara);
                    }
                }
                else
                {
                    //Details in separate paragraphs
                    //If original paragraph text was empty, insert new text before closing tag of paragraph
                    xNewParaXml = xNewParaXml.Insert(xNewParaXml.LastIndexOf("</w:p>"), xSubVarXML);
                    //insert new paragraph before dummy element
                    oNewPara = new Paragraph(xNewParaXml);
                    //insert paragraph
                    oCC.Descendants<Run>().Last<Run>().InsertBeforeSelf<Paragraph>(oNewPara);
                }
            }

            //remove dummy element
            oDummyElement.Remove();
        }

        /// <summary>
        /// Replaces existing SdtCell Element with SdtBlock by 
        /// Swapping TableCell/Sdt parent/child
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        internal static SdtElement ConvertSdtCellToSdtBlock(SdtCell oCC)
        {
            TableCell oTC = oCC.Descendants<TableCell>().FirstOrDefault<TableCell>();
            if (oTC != null)
            {
                //GLOG 15924: If SdtCell contains nested SdtCell(s), move Cell from lowermost child
                SdtContentCell oOldContent = oCC.Descendants<SdtContentCell>().LastOrDefault<SdtContentCell>();
                if (oTC.Elements<Paragraph>().Count<Paragraph>() > 0)
                {
                    //Move all Paragraph elements from w:tc to w:sdtContent
                    foreach (Paragraph oPara in oTC.Elements<Paragraph>())
                    {
                        //Paragraph element will be moved to SdtContent
                        Paragraph oParaCopy = (Paragraph)oPara.Clone();
                        oOldContent.AppendChild<Paragraph>(oParaCopy);
                    }
                    oTC.RemoveAllChildren<Paragraph>();
                }
                int iLevel = 0;
                while (oCC.Parent is SdtContentCell)
                {
                    oCC = (SdtCell)oCC.Parent.Parent;
                    iLevel++;
                }
                //Insert new w:tc element before w:sdt
                TableCell oNewTC = oCC.InsertBeforeSelf<TableCell>((TableCell)oTC.Clone());
                //Remove original w:tc element within w:sdt
                oTC.Remove();
                //Create new SdtBlock with same OuterXML as original SdtCell
                SdtElement oNewCC = new SdtBlock(oCC.OuterXml);
                //Add as child element of w:tc
                oNewTC.AppendChild<SdtElement>(oNewCC);
                //Remove original w:sdt node
                oCC.Remove();
                if (iLevel > 0)
                {
                    oNewCC = oNewCC.Descendants<SdtElement>().ToArray()[iLevel - 1];
                }
                return oNewCC;
            }
            else
                return oCC;

        }
        /// <summary>
        /// Replaces existing SdtRun Element with SdtBlock by 
        /// Swapping Paragraph/Sdt parent/child
        /// </summary>
        /// <param name="oCC"></param>
        /// <returns></returns>
        internal static SdtElement ConvertSdtRunToSdtBlock(SdtRun oCC)
        {
            OpenXmlElement oPara = oCC.Parent; // Ancestors<Paragraph>().FirstOrDefault();
            if (oPara != null && oPara is Paragraph)
            {
                SdtContentRun oOldContent = oCC.Descendants<SdtContentRun>().FirstOrDefault();
                //Paragraph element will be moved to SdtContent
                Paragraph oParaCopy = (Paragraph)oPara.Clone();
                foreach (OpenXmlElement oChild in oOldContent.Elements())
                {
                    oParaCopy.Append(oChild.CloneNode(true));
                }
                oOldContent.RemoveAllChildren();
                oOldContent.AppendChild<Paragraph>(oParaCopy);
                //Create new SdtBlock with same OuterXML as original SdtCell
                SdtBlock oNewCC = new SdtBlock(oCC.OuterXml);
                //Add as child element of w:tc
                oPara.InsertBeforeSelf(oNewCC);
                //Remove original w:sdt node
                oPara.Remove();
                return oNewCC;
            }
            else
                return oCC;
        }

        /// <summary>
        /// Inserts Detail SubVar XML
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="aElements"></param>
        public void SetDetail(SdtRun oCC, string[] aElements, string xStandardDelimiter)
        {
            SdtContentRun oOldContent = oCC.Descendants<SdtContentRun>().FirstOrDefault<SdtContentRun>();

            //Clear out existing child elements other that <w:rPr>
            //TODO: Are there other types of child elements that might be present?
            oOldContent.RemoveAllChildren<Paragraph>();
            oOldContent.RemoveAllChildren<Run>();
            oOldContent.RemoveAllChildren<SdtRun>();
            //JTS 8/19/15: If Paragraph XML consists only of a single <w:p/> tag, make sure there
            //is at least one Text child
            Run oDummyElement = oOldContent.AppendChild<Run>(new Run());
            string xOldContentXml = oOldContent.InnerXml;
            //add dummy element - to be removed later

            //Make sure CC is not showing placeholder
            Query.ClearPlaceholderText(oCC);
            //Insert Detail SubVars
            string xInsertXML = "";
            for (int i = 0; i <= aElements.GetUpperBound(0); i++)
            {
                string xSubVarXML = aElements[i];
                if (i > 0)
                {
                    //Replace Para delimiter with newline
                    xStandardDelimiter = xStandardDelimiter.Replace('\r', '\n');
                    //TODO: Might need to flesh this out to deal with other cases
                    switch (xStandardDelimiter)
                    {
                        case "\n\t":
                            //Insert tab before start of Special Reline
                            xSubVarXML = "<w:r><w:br/></w:r><w:r><w:tab/></w:r>" + xSubVarXML; //GLOG 8461
                            break;
                        case "\n":
                        case "\v": //GLOG 8461
                            xSubVarXML = "<w:r><w:br/></w:r>" + xSubVarXML; //GLOG 8461
                            //Nothing to do
                            break;
                        default:
                            //Insert other configured text
                            xSubVarXML = "<w:r><w:t>" + xStandardDelimiter + "</w:t></w:r>" + xSubVarXML; //GLOG 8461
                            break;
                    }
                }
                //handle new-line separator
                xSubVarXML = xSubVarXML.Replace("\n", "<w:br/>"); //GLOG 8461
                xInsertXML = xInsertXML + xSubVarXML;
            }

            //JTS: Remove empty Text element added above
            string xNewContentXml = xOldContentXml.Insert(xOldContentXml.LastIndexOf("<w:r"), xInsertXML);
            oOldContent.InnerXml = xNewContentXml;
            //remove dummy element
            oOldContent.LastChild.Remove();
            LMP.Trace.WriteNameValuePairs("oCC.OuterXML", oCC.OuterXml);
        }
        private void SetShadingPropertyValue(OpenXmlElement oTargetObject, string xPropertyName, string xPropertyValue)
        {
            Shading oShade = (Shading)oTargetObject;
            int iColor = -1;
            switch (xPropertyName.ToLower())
            {
                case "texture":
                    ShadingPatternValues iPattern = GetShadingPatternValueFromVBATexture(xPropertyValue);
                    oShade.Val = iPattern;
                    break;
                case "backgroundpatterncolor":
                    if (Int32.TryParse(xPropertyValue, out iColor))
                        oShade.Fill = GetColorRGB(iColor);
                    break;
                case "backgroundpatterncolorindex":
                    oShade.Fill = GetColorIndexRGB(xPropertyValue);
                    break;
                case "foregroundpatterncolor":
                    if (Int32.TryParse(xPropertyValue, out iColor))
                        oShade.Color = GetColorRGB(iColor);
                    break;
                case "foregroundpatterncolorindex":
                    oShade.Color = GetColorIndexRGB(xPropertyValue);
                    break;
            }
        }
        /// <summary>
        /// Sets property of a specific border item
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyName"></param>
        /// <param name="xPropertyValue"></param>
        /// <param name="bExecuteOnRow"></param>
        private void SetBorderItemPropertyValue(OpenXmlElement oTargetObject, string xPropertyName, string xPropertyValue, bool bExecuteOnRow)
        {
            OpenXmlAttribute oAttVal;
            OpenXmlAttribute oAttSize;
            OpenXmlAttribute oAttColor;
            string xColorHex = "";
            int iColor = -1;
            switch (xPropertyName.ToLower())
            {
                case "visible":
                    if (xPropertyValue.ToLower() == "false" || xPropertyValue.ToLower() == "0")
                    {
                        try
                        {
                            oAttVal = oTargetObject.GetAttribute("val", oTargetObject.NamespaceUri);
                        }
                        catch { }
                        if (oAttVal.Value != "nil")
                        {
                            oTargetObject.ClearAllAttributes();
                            oAttVal = new OpenXmlAttribute("w", "val", oTargetObject.NamespaceUri, "nil");
                            oTargetObject.SetAttribute(oAttVal);
                        }
                    }
                    else
                    {
                        try
                        {
                            oAttVal = oTargetObject.GetAttribute("val", oTargetObject.NamespaceUri);
                        }
                        catch { }
                        if (oAttVal.Value == "none" || oAttVal.Value == "nil")
                        {
                            oAttVal.Value = "single";
                            oAttSize = new OpenXmlAttribute("w", "sz", oTargetObject.NamespaceUri, "4");
                            oTargetObject.SetAttribute(oAttVal);
                            oTargetObject.SetAttribute(oAttSize);
                        }
                    }
                    break;
                case "linewidth":
                    oAttSize = new OpenXmlAttribute("w", "sz", oTargetObject.NamespaceUri, xPropertyValue);
                    oTargetObject.SetAttribute(oAttSize);
                    break;
                case "linestyle":
                    string xVal = GetBorderValStringFromVBALineStyle(xPropertyValue);
                    if (xVal != "")
                    {
                        oAttVal = new OpenXmlAttribute("w", "val", oTargetObject.NamespaceUri, xVal);
                        oTargetObject.SetAttribute(oAttVal);
                    }
                    break;
                case "color":
                    xColorHex = xPropertyValue;
                    if (Int32.TryParse(xPropertyValue, out iColor))
                    {
                        xColorHex = GetColorRGB(iColor);
                    }
                    if (xColorHex != "")
                    {
                        oAttColor = new OpenXmlAttribute("w", "color", oTargetObject.NamespaceUri, xColorHex);
                        oTargetObject.SetAttribute(oAttColor);
                    }
                    break;
                case "colorindex":
                    iColor = -1;
                    if (Int32.TryParse(xPropertyValue, out iColor))
                    {
                        xColorHex = GetColorIndexRGB(iColor.ToString());
                    }
                    if (xColorHex != "")
                    {
                        oAttColor = new OpenXmlAttribute("w", "color", oTargetObject.NamespaceUri, xColorHex);
                        oTargetObject.SetAttribute(oAttColor);
                    }
                    break;
            }
        }
        private ShadingPatternValues GetShadingPatternValueFromVBATexture(string xTexture)
        {
            switch (xTexture)
            {
                case "25":  //VBA Value not supported in Oxml
                case "50":
                    return ShadingPatternValues.Percent5;
                case "100":
                    return ShadingPatternValues.Percent10;
                case "125":
                    return ShadingPatternValues.Percent12;
                case "150":
                case "175": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent15;
                case "200":
                case "225": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent20;
                case "250":
                case "275": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent25;
                case "300":
                case "325": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent30;
                case "350":
                    return ShadingPatternValues.Percent35;
                case "375":
                    return ShadingPatternValues.Percent37;
                case "400":
                case "425":  //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent40;
                case "450":
                case "475": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent45;
                case "500":
                case "525": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent50;
                case "550":
                case "575": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent55;
                case "600":
                    return ShadingPatternValues.Percent60;
                case "625":
                    return ShadingPatternValues.Percent62;
                case "650":
                case "675": 
                    return ShadingPatternValues.Percent60;
                case "700":
                case "725": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent70;
                case "750":
                case "775": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent75;
                case "800":
                case "825": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent80;
                case "850":
                    return ShadingPatternValues.Percent85;
                case "875":
                    return ShadingPatternValues.Percent87;
                case "900":
                case "925": //VBA Value not supported in Oxml
                    return ShadingPatternValues.Percent90;
                case "950":
                case "975":
                    return ShadingPatternValues.Percent95;
                case "-11":
                    return ShadingPatternValues.ThinHorizontalCross;
                case "-5":
                    return ShadingPatternValues.HorizontalCross;
                case "-6":
                    return ShadingPatternValues.DiagonalCross;
                case "-4":
                    return ShadingPatternValues.DiagonalStripe;
                case "-3":
                    return ShadingPatternValues.ReverseDiagonalStripe;
                case "-1":
                    return ShadingPatternValues.HorizontalStripe;
                case "-2":
                    return ShadingPatternValues.VerticalStripe;
                case "-12":
                    return ShadingPatternValues.ThinDiagonalCross;
                case "-10":
                    return ShadingPatternValues.ThinDiagonalStripe;
                case "-9":
                    return ShadingPatternValues.ThinReverseDiagonalStripe;
                case "-7":
                    return ShadingPatternValues.ThinHorizontalStripe;
                case "1000":
                    return ShadingPatternValues.Solid;
                case "-8":
                    return ShadingPatternValues.ThinVerticalStripe;
                case "0":
                    return ShadingPatternValues.Clear;
                default:
                    return ShadingPatternValues.Nil;
            }

        }
        /// <summary>
        /// Convert wdLineStyle value from VBA to OpenXML BorderValues string
        /// </summary>
        /// <param name="xLineStyle"></param>
        /// <returns></returns>
        private string GetBorderValStringFromVBALineStyle(string xLineStyle)
        {
            switch (xLineStyle)
            {
                case "0":
                    return "none";
                case "1":
                    return "single";
                case "2":
                    return "dotted";
                case "3":
                    return "dashSmallGap";
                case "4":
                    return "dashed";
                case "5":
                    return "dotDash";
                case "6":
                    return "dotDotDash";
                case "7":
                    return "double";
                case "8":
                    return "triple";
                case "9":
                    return "thickThinSmallGap";
                case "10":
                    return "thinThickSmallGap";
                case "11":
                    return "thinThickThinSmallGap";
                case "12":
                    return "thinThickMediumGap";
                case "13":
                    return "thickThinMediumGap";
                case "14":
                    return "thinThickThinMediumGap";
                case "15":
                    return "thinThickLargeGap";
                case "16":
                    return "thickThinLargeGap";
                case "17":
                    return "thinThickThinLargeGap";
                case "18":
                    return "wave";
                case "19":
                    return "doubleWave";
                case "20":
                    return "dashDotStroked";
                case "21":
                    return "threeDEmboss";
                case "22":
                    return "threeDEngrave";
                case "23":
                    return "outset";
                case "24":
                    return "inset";
                default:
                    return "";
            }
        }
        private void SetBordersPropertyValue(OpenXmlElement oTargetObject, string xPropertyName, string xPropertyValue, bool bExecuteOnRow)
        {
            switch (xPropertyName.ToLower())
            {
                case "enable":
                    int iBorderCount = 4;
                    //Include InsideH and InsideV borders for Table
                    if (oTargetObject is TableBorders)
                        iBorderCount = 6;
                    if (xPropertyValue.ToLower() == "false" || xPropertyValue.ToLower() == "0")
                    {

                        for (int i = 1; i <= iBorderCount; i++)
                        {
                            OpenXmlElement oBorder = GetTargetBorder(oTargetObject, i);
                            OpenXmlAttribute oAttVal;
                            try
                            {
                                oAttVal = oBorder.GetAttribute("val", oBorder.NamespaceUri);
                            }
                            catch { }
                            if (oAttVal.Value != "nil")
                            {
                                //Clear current attributes
                                oBorder.ClearAllAttributes();
                                oAttVal.Value = "nil";
                                oBorder.SetAttribute(oAttVal);
                            }
                        }
                        if (oTargetObject is TableBorders)
                        {
                            //Clear all individual Cell Borders
                            Table oTable = oTargetObject.Ancestors<Table>().First();
                            TableCellBorders[] oCellBorders = oTable.Descendants<TableCellBorders>().Where(c => c.Ancestors<Table>().First() == oTable).ToArray();
                            foreach (TableCellBorders oCell in oCellBorders)
                            {
                                oCell.Remove();
                            }
                        }
                        else if (oTargetObject is TableCellBorders)
                        {
                            Table oTable = oTargetObject.Ancestors<Table>().First();
                            TableRow oRow = oTargetObject.Ancestors<TableRow>().First();
                            TableRow oPrevRow = oRow.PreviousSibling<TableRow>();
                            TableRow oNextRow = oRow.NextSibling<TableRow>();
                            TableCell oCell = oTargetObject.Ancestors<TableCell>().First();
                            TableCell oPrevCell = oCell.PreviousSibling<TableCell>();
                            TableCell oNextCell = oCell.NextSibling<TableCell>();
                            //Also need to hide right border of preceding cell
                            if (oPrevCell != null)
                            {
                                TableCellBorders oPrevCellBorders = oPrevCell.TableCellProperties.Descendants<TableCellBorders>().FirstOrDefault();
                                if (oPrevCellBorders == null)
                                {
                                    oPrevCellBorders = oPrevCell.TableCellProperties.AppendChild<TableCellBorders>(new TableCellBorders());
                                }
                                RightBorder oRB = oPrevCellBorders.Elements<RightBorder>().FirstOrDefault();
                                if (oRB == null)
                                {
                                    oRB = oPrevCellBorders.AppendChild(new RightBorder());
                                }
                                SetBorderItemPropertyValue(oRB, "visible", "false", false);
                            }
                            //Also need to hide left border of following cell
                            if (oNextCell != null)
                            {
                                TableCellBorders oNextCellBorders = oNextCell.TableCellProperties.Descendants<TableCellBorders>().FirstOrDefault();
                                if (oNextCellBorders == null)
                                {
                                    oNextCellBorders = oNextCell.TableCellProperties.AppendChild<TableCellBorders>(new TableCellBorders());
                                }
                                LeftBorder oLB = oNextCellBorders.Elements<LeftBorder>().FirstOrDefault();
                                if (oLB == null)
                                {
                                    oLB = oNextCellBorders.AppendChild(new LeftBorder());
                                }
                                SetBorderItemPropertyValue(oLB, "visible", "false", false);
                                
                            }
                            if (oPrevRow != null || oNextRow != null)
                            {
                                if (bExecuteOnRow)
                                {
                                    //Also need to hide bottom border of row above
                                    if (oPrevRow != null)
                                    {
                                        TableCell[] oPrevRowCells = oPrevRow.Elements<TableCell>().ToArray();
                                        foreach (TableCell oRowCell in oPrevRowCells)
                                        {
                                            TableCellBorders oCellBorders = oRowCell.TableCellProperties.Descendants<TableCellBorders>().FirstOrDefault();
                                            if (oCellBorders == null)
                                            {
                                                oCellBorders = oRowCell.TableCellProperties.AppendChild<TableCellBorders>(new TableCellBorders());
                                            }
                                            BottomBorder oBB = oCellBorders.Elements<BottomBorder>().FirstOrDefault();
                                            if (oBB == null)
                                            {
                                                oBB = oCellBorders.AppendChild(new BottomBorder());
                                            }
                                            SetBorderItemPropertyValue(oBB, "visible", "false", false);
                                        }
                                    }
                                    //Also need to hide top border of row below
                                    if (oNextRow != null)
                                    {
                                        TableCell[] oNextRowCells = oNextRow.Elements<TableCell>().ToArray();
                                        foreach (TableCell oRowCell in oNextRowCells)
                                        {
                                            TableCellBorders oCellBorders = oRowCell.TableCellProperties.Descendants<TableCellBorders>().FirstOrDefault();
                                            if (oCellBorders == null)
                                            {
                                                oCellBorders = oRowCell.TableCellProperties.AppendChild<TableCellBorders>(new TableCellBorders());
                                            }
                                            TopBorder oTB = oCellBorders.Elements<TopBorder>().FirstOrDefault();
                                            if (oTB == null)
                                            {
                                                oTB = oCellBorders.AppendChild(new TopBorder());
                                            }
                                            SetBorderItemPropertyValue(oTB, "visible", "false", false);
                                        }
                                    }
                                }
                                else
                                {
                                    GridSpan oSpan = oTargetObject.Descendants<GridSpan>().FirstOrDefault();
                                    int iSpan = 1;
                                    int iSpanStart = 1;
                                    if (oSpan != null)
                                    {
                                        iSpan = oSpan.Val;
                                    }
                                    TableCell[] oRowCells = oRow.Elements<TableCell>().ToArray();
                                    //Get starting grid position
                                    foreach (TableCell oRowCell in oRowCells)
                                    {
                                        if (oRowCell.IsBefore(oCell))
                                        {
                                            oSpan = oRowCell.TableCellProperties.Descendants<GridSpan>().FirstOrDefault();
                                            if (oSpan != null)
                                            {
                                                iSpanStart += oSpan.Val;
                                            }
                                            else
                                                iSpanStart += 1;
                                        }
                                        else
                                            break;
                                    }
                                    //Also hide bottom borders of cells in previous row that are within the span of current cell
                                    if (oPrevRow != null)
                                    {
                                        int iCurStart = 1;
                                        int iCurEnd = 1;
                                        TableCell[] oPrevRowCells = oPrevRow.Elements<TableCell>().ToArray();
                                        foreach (TableCell oRowCell in oPrevRowCells)
                                        {
                                            if (iCurStart >= iSpanStart + iSpan)
                                                break;

                                            GridSpan oCurSpan = oRowCell.TableCellProperties.Descendants<GridSpan>().FirstOrDefault();
                                            if (oCurSpan != null)
                                            {
                                                iCurEnd = iCurStart + oCurSpan.Val;
                                            }
                                            else
                                                iCurEnd = iCurStart + 1;
                                            if (iCurStart >= iSpanStart && iCurEnd <= iSpanStart + iSpan)
                                            {
                                                TableCellBorders oCellBorders = oRowCell.TableCellProperties.Descendants<TableCellBorders>().FirstOrDefault();
                                                if (oCellBorders == null)
                                                {
                                                    oCellBorders = oRowCell.TableCellProperties.AppendChild<TableCellBorders>(new TableCellBorders());
                                                }
                                                BottomBorder oBB = oCellBorders.Elements<BottomBorder>().FirstOrDefault();
                                                if (oBB == null)
                                                {
                                                    oBB = oCellBorders.AppendChild(new BottomBorder());
                                                }
                                                SetBorderItemPropertyValue(oBB, "visible", "false", false);

                                            }
                                            iCurStart = iCurEnd;
                                        }
                                    }
                                    //Also hide top borders of cells in next row that are within the span of current cell
                                    if (oNextRow != null)
                                    {
                                        int iCurStart = 1;
                                        int iCurEnd = 1;
                                        TableCell[] oNextRowCells = oPrevRow.Elements<TableCell>().ToArray();
                                        foreach (TableCell oRowCell in oNextRowCells)
                                        {
                                            if (iCurStart >= iSpanStart + iSpan)
                                                break;

                                            GridSpan oCurSpan = oRowCell.TableCellProperties.Descendants<GridSpan>().FirstOrDefault();
                                            if (oCurSpan != null)
                                            {
                                                iCurEnd = iCurStart + oCurSpan.Val;
                                            }
                                            else
                                                iCurEnd = iCurStart + 1;
                                            if (iCurStart >= iSpanStart && iCurEnd <= iSpanStart + iSpan)
                                            {
                                                TableCellBorders oCellBorders = oRowCell.TableCellProperties.Descendants<TableCellBorders>().FirstOrDefault();
                                                if (oCellBorders == null)
                                                {
                                                    oCellBorders = oRowCell.TableCellProperties.AppendChild<TableCellBorders>(new TableCellBorders());
                                                }
                                                TopBorder oTB = oCellBorders.Elements<TopBorder>().FirstOrDefault();
                                                if (oTB == null)
                                                {
                                                    oTB = oCellBorders.AppendChild(new TopBorder());
                                                }
                                                SetBorderItemPropertyValue(oTB, "visible", "false", false);
                                            }
                                            iCurStart = iCurEnd;
                                        }
                                    }

                                }
                            }

                        }

                    }
                    else
                    {
                        for (int i = 1; i <= iBorderCount; i++)
                        {
                            OpenXmlElement oBorder = GetTargetBorder(oTargetObject, i);
                            OpenXmlAttribute oAttVal;
                            try
                            {
                                oAttVal = oBorder.GetAttribute("val", oBorder.NamespaceUri);
                            }
                            catch { }
                            if (oAttVal.Value == null || oAttVal.Value == "none" || oAttVal.Value == "nil")
                            {
                                oAttVal.Value = "single";
                                OpenXmlAttribute oAttSize = new OpenXmlAttribute("w", "sz", oBorder.NamespaceUri, "4");
                                oBorder.SetAttribute(oAttVal);
                                oBorder.SetAttribute(oAttSize);
                            }
                        }
                    }
                    break;
                case "distancefromtop":
                    if (oTargetObject is ParagraphBorders)
                    {
                        TopBorder oTopB = oTargetObject.Elements<TopBorder>().FirstOrDefault();
                        if (oTopB != null)
                        {
                            oTopB.Space = UInt32.Parse(xPropertyValue);
                        }
                    }
                    break;
                case "distancefrombottom":
                    if (oTargetObject is ParagraphBorders)
                    {
                        BottomBorder oBotB = oTargetObject.Elements<BottomBorder>().FirstOrDefault();
                        if (oBotB != null)
                        {
                            oBotB.Space = UInt32.Parse(xPropertyValue);
                        }
                    }
                    break;
                case "distancefromleft":
                    if (oTargetObject is ParagraphBorders)
                    {
                        LeftBorder oLeftB = oTargetObject.Elements<LeftBorder>().FirstOrDefault();
                        if (oLeftB != null)
                        {
                            oLeftB.Space = UInt32.Parse(xPropertyValue);
                        }
                    }
                    break;
                case "distancefromright":
                    if (oTargetObject is ParagraphBorders)
                    {
                        RightBorder oRightB = oTargetObject.Elements<RightBorder>().FirstOrDefault();
                        if (oRightB != null)
                        {
                            oRightB.Space = UInt32.Parse(xPropertyValue);
                        }
                    }
                    break;

            }
        }
        private void SetParagraphPropertyValue(OpenXmlElement oTargetObject, string xPropertyName, string xPropertyValue)
        {
            OpenXmlElement oProps = null;
            if (oTargetObject is Paragraph)
            {
                oProps = oTargetObject.Elements<ParagraphProperties>().FirstOrDefault();
                if (oProps == null)
                {
                    oProps = new ParagraphProperties();
                    oTargetObject.AppendChild(oProps);
                }
            }
            else if (oTargetObject is ParagraphProperties)
            {
                oProps = oTargetObject;
            }
            else if (oTargetObject is StyleParagraphProperties) //GLOG 8893
            {
                oProps = oTargetObject;
            }
            else
            {
                throw new LMP.Exceptions.ActionException(
                    "Could not set property value." +
                    xPropertyName + "=" + xPropertyValue);
            }
            switch (xPropertyName.ToLower())
            {
                case "style":
                    SetStyle(oProps, xPropertyValue);
                    break;
                case "firstlineindent":
                    SetParaFirstLineIndent(oProps, xPropertyValue);
                    break;
                case "leftindent":
                    SetParaLeftIndent(oProps, xPropertyValue);
                    break;
                case "spacebefore":
                    SetParaSpaceBefore(oProps, xPropertyValue);
                    break;
                case "spaceafter":
                    SetParaSpaceAfter(oProps, xPropertyValue);
                    break;
                case "alignment":
                    SetParaAlignment(oProps, xPropertyValue);
                    break;
                case "keeptogether":
                    SetParaKeepLines(oProps, xPropertyValue);
                    break;
                case "keepwithnext":
                    SetParaKeepNext(oProps, xPropertyValue);
                    break;
                case "widowcontrol":
                    SetParaWidowControl(oProps, xPropertyValue);
                    break;
                case "outlinelevel":
                    SetParaOutlineLevel(oProps, xPropertyValue);
                    break;
                case "pagebreakbefore":
                    SetParaPageBefore(oProps, xPropertyValue);
                    break;
                case "nolinenumber":
                    SetParaSuppressLines(oProps, xPropertyValue);
                    break;
                case "mirrorindents":
                    SetParaMirrorIndents(oProps, xPropertyValue);
                    break;
                case "hyphenation":
                    SetParaAutoHyphens(oProps, xPropertyValue);
                    break;
                case "linespacing":
                    SetParaLineSpacing(oProps, xPropertyValue);
                    break;
                case "linespacingrule":
                    SetParaLineSpacingRule(oProps, xPropertyValue);
                    break;
            }
        }

        /// <summary>
        /// sets the property with the specified name to the specified value
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyName"></param>
        /// <param name="xPropertyValue"></param>
        public void SetPropertyValue(OpenXmlElement oTargetObject, string xPropertyName, string xPropertyValue, bool  bExecuteOnRow)
        {
            //switch on property name - if name is ambiguous, 
            //i.e. if it is a member of multiple target objects,
            //condition on oTargetObject type to distinguish
            if (oTargetObject is Paragraph || oTargetObject is ParagraphProperties || oTargetObject is StyleParagraphProperties)
            {
                SetParagraphPropertyValue(oTargetObject, xPropertyName, xPropertyValue);
                return;
            }
            else if (oTargetObject is TableBorders || oTargetObject is TableCellBorders || oTargetObject is ParagraphBorders || oTargetObject is PageBorders)
            {
                SetBordersPropertyValue(oTargetObject, xPropertyName, xPropertyValue, bExecuteOnRow);
                return;
            }
            else if (oTargetObject is Border || oTargetObject is TopBorder || oTargetObject is BottomBorder || oTargetObject is LeftBorder || oTargetObject is RightBorder ||
                oTargetObject is InsideVerticalBorder || oTargetObject is InsideHorizontalBorder || oTargetObject is TopLeftToBottomRightCellBorder || oTargetObject is TopRightToBottomLeftCellBorder)
            {
                SetBorderItemPropertyValue(oTargetObject, xPropertyName, xPropertyValue, bExecuteOnRow);
                return;
            }
            else if (oTargetObject is Shading)
            {
                SetShadingPropertyValue(oTargetObject, xPropertyName, xPropertyValue);
                return;
            }
            switch (xPropertyName.ToLower())
            {
                case "size":
                    SetFontSize(oTargetObject, xPropertyValue);
                    break;
                case "name":
                    SetFontName(oTargetObject, xPropertyValue);
                    break;
                case "namefareast":
                    SetFarEastFontName(oTargetObject, xPropertyValue);
                    break;
                case "bold":
                    SetFontBold(oTargetObject, xPropertyValue);
                    break;
                case "hidden":
                    SetFontHidden(oTargetObject, xPropertyValue);
                    break;
                case "allcaps":
                    SetFontAllCaps(oTargetObject, xPropertyValue);
                    break;
                case "smallcaps":
                    SetFontSmallCaps(oTargetObject, xPropertyValue);
                    break;
                case "italic":
                    SetFontItalic(oTargetObject, xPropertyValue);
                    break;
                case "languageid":
                    SetLanguageID(oTargetObject, xPropertyValue);
                    break;
                case "topmargin":
                    SetPageSetupTopMargin(oTargetObject, xPropertyValue);
                    break;
                case "bottommargin":
                    SetPageSetupBottomMargin(oTargetObject, xPropertyValue);
                    break;
                case "leftmargin":
                    SetPageSetupLeftMargin(oTargetObject, xPropertyValue);
                    break;
                case "rightmargin":
                    SetPageSetupRightMargin(oTargetObject, xPropertyValue);
                    break;
                case "headerdistance":
                    SetPageSetupHeaderDistance(oTargetObject, xPropertyValue);
                    break;
                case "footerdistance":
                    SetPageSetupFooterDistance(oTargetObject, xPropertyValue);
                    break;
                case "gutter":
                    SetPageSetupGutter(oTargetObject, xPropertyValue);
                    break;
                case "height":
                    if (oTargetObject is TableRow)
                    {
                        SetTableRowHeight(oTargetObject, xPropertyValue);
                    }
                    else
                    {
                        throw new LMP.Exceptions.ActionException(
                            "Could not set property value.  Property name not recognized.  " +
                            xPropertyName + "=" + xPropertyValue);
                    }
                    break;
                case "heightrule":
                    SetTableRowHeightRule(oTargetObject, xPropertyValue);
                    break;
                case "style":
                    SetStyle(oTargetObject, xPropertyValue);
                    break;
                case "superscript":
                    SetFontSuperscript(oTargetObject, xPropertyValue);
                    break;
                case "subscript":
                    SetFontSubscript(oTargetObject, xPropertyValue);
                    break;
                case "textcolor":
                    SetFontTextColor(oTargetObject, xPropertyValue);
                    break;
                case "doublestrikethrough":
                    SetFontDoubleStrikethrough(oTargetObject, xPropertyValue);
                    break;
                case "strikethrough":
                    SetFontStrikethrough(oTargetObject, xPropertyValue);
                    break;
                case "spacing":
                    SetFontSpacing(oTargetObject, xPropertyValue);
                    break;
                case "position":
                    SetFontPosition(oTargetObject, xPropertyValue);
                    break;
                case "verticalalignment":
                    if (oTargetObject is TableCellProperties)
                    {
                        SetTableCellVerticalAlignment(oTargetObject, xPropertyValue);
                    }
                    else if (oTargetObject is SectionProperties)
                    {
                        SetPageVerticalAlignment(oTargetObject, xPropertyValue);
                    }
                    else
                    {
                        throw new LMP.Exceptions.ActionException(
                            "Could not set property value.  Property name not recognized.  " +
                            xPropertyName + "=" + xPropertyValue);
                    }
                    break;
                default:
                    throw new LMP.Exceptions.ActionException(
                        "Could not set property value.  Property name not recognized.  " +
                        xPropertyName + "=" + xPropertyValue);
            }
        }

        /// <summary>
        /// runs the method with the specified name
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xMethodName"></param>
        public void RunVBAMethod(OpenXmlElement oTargetObject, string xMethodName)
        {
            //switch on method name - if name is ambiguous, 
            //i.e. if it is a member of multiple target objects,
            //condition on oTargetObject type to distinguish
            switch (xMethodName.ToLower())
            {
                case "reset":
                    if (oTargetObject.GetType().Name == "ParagraphProperties")
                    {
                        oTargetObject.RemoveAllChildren();
                        oTargetObject.Remove();
                    }
                    break;
                case "update":
                    //can this be done?
                    break;
                case "unlink":
                    string xTargetType = oTargetObject.GetType().Name.ToLower();

                    switch (xTargetType.ToLower())
                    {
                        case "fieldsimple":
                            //Remove element only
                            oTargetObject.Remove();
                            break;
                        case "fieldchar":
                        case "fieldcode":
                            //Remove parent Run
                            oTargetObject.Parent.Remove();
                            break;
                        default:
                            //no fields to unlink
                            break;
                    }
                    break;
                default:
                    throw new LMP.Exceptions.ActionException("Could not execute RunVBAMethod - " + xMethodName);
            }
        }

        /// <summary>
        /// returns the target LanguageID element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        public OpenXmlElement[] GetTargetLanguageID(OpenXmlElement[] oBaseParents)
        {
            List<OpenXmlElement> oTargetLanguageIDs = new List<OpenXmlElement>();

            try
            {
                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    if (oBP.GetType().Name == "Style")
                    {
                        StyleRunProperties runProp = oBP.Elements<StyleRunProperties>().FirstOrDefault();
                        if (runProp == null)
                        {
                            runProp = new StyleRunProperties();
                            oBP.Append(new OpenXmlElement[] { runProp });
                        }
                        oTargetLanguageIDs.Add(runProp);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetLanguageID", oE);
            }

            return oTargetLanguageIDs.ToArray();
        }

        /// <summary>
        /// returns the target ParagraphFormat element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParents"></param>
        public OpenXmlElement[] GetTargetParagraphFormat(OpenXmlElement[] oBaseParents)
        {
            List<OpenXmlElement> oTargetParaFormats = new List<OpenXmlElement>();

            try
            {
                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    switch (oBP.GetType().Name.ToLower())
                    {
                        case "paragraph":
                            ParagraphProperties oParaProp = oBP.Elements<ParagraphProperties>().FirstOrDefault();
                            if (oParaProp == null)
                            {
                                oParaProp = new ParagraphProperties();
                                oBP.PrependChild<ParagraphProperties>(oParaProp);
                            }
                            oTargetParaFormats.Add(oParaProp); //JTS 9/26/15
                            break;
                        case "style":
                            StyleParagraphProperties styParaProp = oBP.Elements<StyleParagraphProperties>().FirstOrDefault();
                            if (styParaProp == null)
                            {
                                styParaProp = new StyleParagraphProperties();
                                oBP.PrependChild<StyleParagraphProperties>(styParaProp);
                            }
                            oTargetParaFormats.Add(styParaProp); //JTS 9/26/15
                            break;
                        case "sdtblock":
                        case "range":
                            Paragraph[] oParas = oBP.Descendants<Paragraph>().ToArray<Paragraph>(); ;
                            foreach (Paragraph oPara in oParas)
                            {
                                ParagraphProperties oPpr = oPara.Descendants<ParagraphProperties>().FirstOrDefault<ParagraphProperties>();
                                if (oPpr == null)
                                {
                                    oPpr = new ParagraphProperties();
                                    oPara.PrependChild<ParagraphProperties>(oPpr);
                                }
                                oTargetParaFormats.Add(oPpr);
                            }
                            break;
                        case "run":
                            Paragraph oParent = oBP.Ancestors<Paragraph>().FirstOrDefault<Paragraph>();
                            if (oParent != null)
                            {
                                ParagraphProperties oPpr = oParent.Descendants<ParagraphProperties>().FirstOrDefault<ParagraphProperties>();
                                if (oPpr == null)
                                {
                                    oPpr = new ParagraphProperties();
                                    oParent.PrependChild<ParagraphProperties>(oPpr);
                                }
                                oTargetParaFormats.Add(oPpr);
                            }
                            break;
                        default:
                            throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                                    "Error_CouldNotExecuteVariableAction") + "GetTargetParagraphFormat");
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetParagraphFormat", oE);
            }

            return oTargetParaFormats.ToArray();
        }

        /// <summary>
        /// returns the target ParagraphFormat element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParents"></param>
        public OpenXmlElement[] GetTargetShading(OpenXmlElement[] oBaseParents)
        {
            List<OpenXmlElement> oTargetShading = new List<OpenXmlElement>();
            try
            {
                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    List<OpenXmlElement> oTargetProps = new List<OpenXmlElement>();
                    switch (oBP.GetType().Name.ToLower())
                    {
                        case "paragraph":
                            ParagraphProperties oPPr = oBP.Elements<ParagraphProperties>().FirstOrDefault();
                            if (oPPr == null)
                            {
                                oPPr = oBP.PrependChild<ParagraphProperties>(new ParagraphProperties());
                            }
                            oTargetProps.Add(oPPr);
                            break;
                        case "table":
                        case "tablecell":
                        case "sdtcell":
                        case "tablerow":

                            List<TableCell> oCells = new List<TableCell>();
                            if (oBP is TableCell)
                            {
                                oCells.Add((TableCell)oBP);
                            }
                            else if (oBP is SdtCell)
                            {
                                //Get cells contained in Content Control
                                oCells = oBP.Descendants<TableCell>().ToList();
                            }
                            else if (oBP is TableRow)
                            {
                                oCells = oBP.Descendants<TableCell>().Where(c => c.Ancestors<TableRow>().First() == oBP).ToList();
                            }
                            else
                            {
                                TableProperties oTPr = oBP.Elements<TableProperties>().FirstOrDefault();
                                if (oTPr == null)
                                {
                                    oTPr = oBP.PrependChild<TableProperties>(new TableProperties());
                                }
                                oTargetProps.Add(oTPr);
                                //Also need to set in individual cell properties
                                oCells.AddRange(oBP.Descendants<TableCell>().Where(c => c.Ancestors<Table>().First() == oBP).ToArray());
                            }
                            foreach (TableCell oCell in oCells)
                            {
                                TableCellProperties oCPr = oCell.Elements<TableCellProperties>().FirstOrDefault();
                                if (oCPr == null)
                                {
                                    oCPr = oBP.AppendChild<TableCellProperties>(new TableCellProperties());
                                }
                                oTargetProps.Add(oCPr);
                            }
                            break;
                        case "sdtblock":
                            Paragraph[] oParas = oBP.Descendants<Paragraph>().ToArray();
                            foreach (Paragraph oPara in oParas)
                            {
                                oPPr = oPara.Elements<ParagraphProperties>().FirstOrDefault();
                                if (oPPr == null)
                                {
                                    oPPr = oPara.PrependChild<ParagraphProperties>(new ParagraphProperties());
                                }
                                oTargetProps.Add(oPPr);
                            }
                            break;
                        case "sdtrun":
                        case "run":
                            Run[] oRuns = null;
                            if (oBP is Run)
                            {
                                oRuns = new Run[] { (Run)oBP };
                            }
                            else
                            {
                                oRuns = oBP.Descendants<Run>().ToArray();
                            }
                            foreach (Run oRun in oRuns)
                            {
                                RunProperties oRPr = oRun.Elements<RunProperties>().FirstOrDefault();
                                if (oRPr == null)
                                {
                                    oRPr = oRun.PrependChild<RunProperties>(new RunProperties());
                                }
                                oTargetProps.Add(oRPr);
                            }
                            break;
                        default:
                            throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                                    "Error_CouldNotExecuteVariableAction") + "GetTargetParagraphFormat");
                    }
                    foreach (OpenXmlElement oProps in oTargetProps)
                    {
                        Shading oShade = oProps.Elements<Shading>().FirstOrDefault();
                        if (oShade == null)
                        {
                            oShade = new Shading() { Val = ShadingPatternValues.Nil, Fill = "auto", Color = "auto" };
                            oProps.AppendChild<Shading>(oShade);
                        }
                        oTargetShading.Add(oShade);
                    }
                }
                return oTargetShading.ToArray();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetParagraphFormat", oE);
            }

            return oTargetShading.ToArray();
        }
        /// <summary>
        /// returns the target Paragraph element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        /// <param name="aObjectProps"></param>
        /// <param name="iIndex"></param>
        public OpenXmlElement[] GetTargetParagraphs(OpenXmlElement[] oBaseParents, string[] aObjectProps, ref int iIndex)
        {
            List<OpenXmlElement> oParagraphs = new List<OpenXmlElement>();
            Paragraph oPara = null;

            string xNextElement = aObjectProps[iIndex + 1].ToLower();

            try
            {
                if (xNextElement.Substring(0, 5) == "item(")
                {
                    //get index
                    string xIndex = xNextElement.Substring(5, xNextElement.IndexOf(")") - 5);

                    foreach (OpenXmlElement oBP in oBaseParents)
                    {
                        //get single paragraph element
                        if (oBP.GetType().Name == "SdtRun")
                        {
                            oPara = (Paragraph)oBP.Parent;
                            oParagraphs.Add(oPara);
                        }
                        else if (oBP.GetType().Name == "SdtBlock")
                        {
                            oPara = oBP.Descendants<Paragraph>().ElementAtOrDefault(Convert.ToInt32(xIndex) - 1);
                            oParagraphs.Add(oPara);
                        }
                        else
                            throw new LMP.Exceptions.ActionException("Could not get target paragraph for the OpenXml element " +
                                    oBP.GetType().Name + ".");
                    }
                    //skip 'item()' element
                    iIndex += 1;
                }
                else
                    throw new LMP.Exceptions.ActionException("Target paragraphs collection not implemented.");
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetParagraphFormat", oE);
            }

            return oParagraphs.ToArray();
        }

        /// <summary>
        /// returns the target Table element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        /// <param name="aObjectProps"></param>
        /// <param name="iIndex"></param>
        public OpenXmlElement[] GetTargetTables(OpenXmlElement[] oBaseParents, string[] aObjectProps, ref int iIndex)
        {
            List<OpenXmlElement> oTargetTables = new List<OpenXmlElement>();
            Table oTable = null;
            string xNextElement = "";

            if (aObjectProps.Length > iIndex + 1)
                xNextElement = aObjectProps[iIndex + 1].ToLower();

            try
            {
                string xIndex = "";

                if ((xNextElement != "") && (xNextElement.Substring(0, 5) == "item("))
                {
                    //get index
                    xIndex = xNextElement.Substring(5, xNextElement.IndexOf(")") - 5);
                }

                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    Table oFirstTable = oBP.Descendants<Table>().FirstOrDefault();
                    //get table at index
                    if ((xIndex == "1" || xIndex == "") && oBP.GetType().Name != "Table")
                    {
                        //If inside a table, .Tables.Item(1) will point to parent table
                        oTable = oBP.Ancestors<Table>().FirstOrDefault();
                        if (oTable != null)
                        {
                            //If range is inside a table and also contains a table,
                            //both oRange.Tables and oRange.Tables.Item(1) will point to
                            //parent table, and Tables collection will have only 1 item
                            oTargetTables.Add(oTable);
                            continue;
                        }
                    }

                    if (oTable == null && oFirstTable != null)
                    {
                        oTable = oFirstTable;
                        //Need to make sure we're getting table at same level as 
                        //first Descendant table, and not a nested table
                        int iCount = 0;
                        do
                        {
                            //If no index specified return entire collection
                            if (xIndex == "" || (++iCount).ToString() == xIndex)
                            {
                                oTargetTables.Add(oTable);
                                if (xIndex != "")
                                {
                                    string x = oTable.OuterXml;
                                    //stop after specific index reached
                                    break;
                                }
                            }

                        } while ((oTable = oTable.NextSibling<Table>()) != null);
                    }

                    //increment past 'item()' element
                    if (xIndex != "")
                        iIndex += 1;
                }

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetTables", oE);
            }

            return oTargetTables.ToArray();
        }

        /// <summary>
        /// returns the target Table element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        /// <param name="aObjectProps"></param>
        /// <param name="iIndex"></param>
        public OpenXmlElement[] GetTargetBorders(OpenXmlElement[] oBaseParents, string[] aObjectProps, ref int iIndex)
        {
            List<OpenXmlElement> oTargetBorders = new List<OpenXmlElement>();
            string xNextElement = "";

            if (aObjectProps.Length > iIndex + 1)
                xNextElement = aObjectProps[iIndex + 1].ToLower();

            try
            {
                string xIndex = "";

                if ((xNextElement != "") && (xNextElement.Substring(0, 5) == "item("))
                {
                    //get index
                    xIndex = xNextElement.Substring(5, xNextElement.IndexOf(")") - 5);
                }

                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    if (oBP is SdtElement)
                    {
                        OpenXmlElement[] oParas = GetTargetParagraphFormat(new OpenXmlElement[] { oBP });
                        if (oParas.GetLength(0) == 0)
                        {
                            OpenXmlElement[] oRuns = GetTargetRunProperties(new OpenXmlElement[] { oBP });
                            int iDummyIndex = iIndex;
                            oTargetBorders.AddRange(GetTargetBorders(oRuns, aObjectProps, ref iDummyIndex));
                            continue;
                        }
                        else
                        {
                            int iDummyIndex = iIndex;
                            oTargetBorders.AddRange(GetTargetBorders(oParas, aObjectProps, ref iDummyIndex));
                            continue;
                        }

                    }
                    switch (oBP.GetType().Name.ToLower())
                    {
                        case "sectionproperties":
                            PageBorders oBorders = oBP.Elements<PageBorders>().FirstOrDefault();
                            if (oBorders == null)
                            {
                                oBorders = oBP.AppendChild<PageBorders>(new PageBorders() { OffsetFrom = PageBorderOffsetValues.Page });
                            }
                            if (xIndex != "")
                            {
                                int iTypeIndex = 0;
                                if (Int32.TryParse(xIndex, out iTypeIndex))
                                {
                                    OpenXmlElement oPageB = GetTargetBorder(oBorders, iTypeIndex);
                                    oTargetBorders.Add(oPageB);
                                }
                            }
                            else
                                oTargetBorders.Add(oBorders);
                            break;
                        case "table":
                            TableProperties oTProps = oBP.Elements<TableProperties>().FirstOrDefault();
                            if (oTProps == null)
                            {
                                oTProps = oBP.AppendChild<TableProperties>(new TableProperties());
                            }
                            TableBorders oTB = oTProps.Elements<TableBorders>().FirstOrDefault();
                            if (oTB == null)
                            {
                                oTB = new TableBorders();
                                oTProps.AppendChild<TableBorders>(oTB);
                            }
                            if (xIndex != "")
                            {
                                int iTypeIndex = 0;
                                if (Int32.TryParse(xIndex, out iTypeIndex))
                                {
                                    OpenXmlElement oTableB = GetTargetBorder(oTB, iTypeIndex);
                                    oTargetBorders.Add(oTableB);
                                }
                            }
                            else
                                oTargetBorders.Add(oTB);
                            break;
                        case "tablerow":
                            int iDummyIndex = iIndex;
                            //Row borders consist of individual cell borders
                            oTargetBorders.AddRange(GetTargetBorders(oBP.Elements<TableCell>().ToArray<OpenXmlElement>(), aObjectProps, ref iDummyIndex));
                            break;
                        case "tablecell":
                            TableCell [] oCells;
                            if (oBP is TableRow)
                            {
                                oCells = oBP.Descendants<TableCell>().Where(c => c.Ancestors<TableRow>().First() == oBP).ToArray();
                            }
                            else
                            {
                                oCells = new TableCell[] { (TableCell)oBP };
                            }
                            foreach (TableCell oCell in oCells)
                            {
                                TableCellProperties oTCProps = oCell.Elements<TableCellProperties>().FirstOrDefault();
                                if (oTCProps == null)
                                {
                                    oTCProps = oCell.PrependChild<TableCellProperties>(new TableCellProperties());
                                }
                                TableCellBorders oTCB = oTCProps.Descendants<TableCellBorders>().FirstOrDefault();
                                if (oTCB == null)
                                {
                                    oTCB = new TableCellBorders();
                                    oTCProps.PrependChild<TableCellBorders>(oTCB);
                                }
                                if (xIndex != "")
                                {
                                    int iTypeIndex = 0;
                                    if (Int32.TryParse(xIndex, out iTypeIndex))
                                    {
                                        OpenXmlElement oCellB = GetTargetBorder(oTCB, iTypeIndex);
                                        oTargetBorders.Add(oCellB);
                                    }
                                }
                                else
                                    oTargetBorders.Add(oTCB);
                            }
                            break;
                        case "paragraph":
                        case "paragraphproperties":

                            ParagraphProperties oParaP = null;
                            if (oBP is ParagraphProperties)
                            {
                                oParaP = (ParagraphProperties)oBP;
                            }
                            else
                            {
                                oParaP = oBP.Elements<ParagraphProperties>().FirstOrDefault();
                                if (oParaP == null)
                                {
                                    oParaP = oBP.PrependChild<ParagraphProperties>(new ParagraphProperties());
                                }
                            }
                            ParagraphBorders oPB = oParaP.Elements<ParagraphBorders>().FirstOrDefault();
                            if (oPB == null)
                            {
                                oPB = new ParagraphBorders();
                                oParaP.AppendChild<ParagraphBorders>(oPB);
                            }
                            if (xIndex != "")
                            {
                                int iTypeIndex = 0;
                                if (Int32.TryParse(xIndex, out iTypeIndex))
                                {
                                    OpenXmlElement oParaB = GetTargetBorder(oPB, iTypeIndex);
                                    oTargetBorders.Add(oParaB);
                                }
                            }
                            else
                                oTargetBorders.Add(oPB);
                            break;
                        case "run":
                        case "runproperties":
                            RunProperties oRunPr = null;
                            if (oBP is RunProperties)
                            {
                                oRunPr = (RunProperties)oBP;
                            }
                            else
                            {
                                oRunPr = oBP.Elements<RunProperties>().FirstOrDefault();
                                if (oRunPr == null)
                                {
                                    oRunPr = oBP.PrependChild<RunProperties>(new RunProperties());
                                }
                            }
                            Border oRB = oRunPr.Elements<Border>().FirstOrDefault();
                            if (oRB == null)
                            {
                                oRB = new Border() { Val = BorderValues.Single, Size = 4, Space = 0, Color = "auto" };
                                oRunPr.AppendChild<Border>(oRB);
                            }
                            oTargetBorders.Add(oRB);
                            break;
                    }
                }

                //increment past 'item()' element
                if (xIndex != "")
                    iIndex += 1;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetTables", oE);
            }

            return oTargetBorders.ToArray();
        }
        private OpenXmlElement GetTargetBorder(OpenXmlElement oParent, int iType)
        {
            OpenXmlElement oBorder = null;
            UInt32 iSpace = 0;
            //Different default space for page borders
            if (oParent is PageBorders)
            {
                iSpace = 24;
            }
            switch (Math.Abs(iType))
            {
                case 1:
                    oBorder = new TopBorder() {Val = BorderValues.Single, Size = 4, Space = iSpace, Color = "auto"};

                    break;
                case 2:
                    oBorder = new LeftBorder() { Val = BorderValues.Single, Size = 4, Space = iSpace, Color = "auto" };
                    break;
                case 3:
                    oBorder = new BottomBorder() { Val = BorderValues.Single, Size = 4, Space = iSpace, Color = "auto" };
                    break;
                case 4:
                    oBorder = new RightBorder() { Val = BorderValues.Single, Size = 4, Space = iSpace, Color = "auto" };
                    break;
                case 5:
                    oBorder = new InsideHorizontalBorder() { Val = BorderValues.Single, Size = 4, Space = iSpace, Color = "auto" };
                    break;
                case 6:
                    oBorder = new InsideVerticalBorder() { Val = BorderValues.Single, Size = 4, Space = iSpace, Color = "auto" };
                    break;
                case 7:
                    oBorder = new TopRightToBottomLeftCellBorder() { Val = BorderValues.Single, Size = 4, Space = iSpace, Color = "auto" };
                    break;
                case 8:
                    oBorder = new TopLeftToBottomRightCellBorder() { Val = BorderValues.Single, Size = 4, Space = iSpace, Color = "auto" };
                    break;
                default:
                    return null;

            }

            OpenXmlElement oCurBorder = oParent.ChildElements.Where<OpenXmlElement>(e => e.LocalName == oBorder.LocalName).FirstOrDefault();
            if (oCurBorder == null)
            {
                oCurBorder = oParent.AppendChild(oBorder.CloneNode(true));
            }
            return oCurBorder;
        }
        /// <summary>
        /// returns the target Row element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        /// <param name="aObjectProps"></param>
        /// <param name="iIndex"></param>
        public OpenXmlElement[] GetTargetRows(OpenXmlElement[] oBaseParents, string[] aObjectProps, ref int iIndex)
        {
            List<OpenXmlElement> oRows = new List<OpenXmlElement>();
            TableRow oRow = null;
            string xNextElement = "";

            if (aObjectProps.Length > iIndex + 1)
                xNextElement = aObjectProps[iIndex + 1].ToLower();

            try
            {
                string xIndex = "";
                if ((xNextElement != "") && (xNextElement.Substring(0, 5) == "item("))
                {
                    //get index
                    xIndex = xNextElement.Substring(5, xNextElement.IndexOf(")") - 5);
                }

                //cycle through base parents array
                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    TableRow oFirstRow = oBP.Descendants<TableRow>().FirstOrDefault<TableRow>();
                    if ((xIndex == "1" || xIndex == "") && oBP.GetType().Name != "Table")
                    {
                        //If inside a row, .Rows.Item(1) could point to parent row
                        oRow = oBP.Ancestors<TableRow>().FirstOrDefault();
                        if (oRow != null)
                        {
                            //If range is inside a table and also contains a table,
                            //both oRange.Rows and oRange.Rows.Item(1) will point to
                            //parent row, and Rows collection will have only 1 item
                            oRows.Add(oRow);
                            continue;
                        }
                    }
                    if (oRow == null && oFirstRow != null)
                    {
                        oRow = oFirstRow;
                        //Need to make sure we're getting row at same level as 
                        //first Descendant row, and not a row from a nested table
                        int iCount = 0;
                        do
                        {
                            //If no index specified return entire collection
                            if (xIndex == "" || (++iCount).ToString() == xIndex)
                            {
                                oRows.Add(oRow);
                                if (xIndex != "")
                                {
                                    //stop after specific index reached
                                    break;
                                }
                            }
                            
                        } while ((oRow = oRow.NextSibling<TableRow>()) != null);
                    }

                }

                //increment past 'item()' element
                if (xIndex != "")
                    iIndex += 1;

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetRows", oE);
            }

            return oRows.ToArray();
        }

        /// <summary>
        /// returns the target range element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        public OpenXmlElement[] GetTargetRange(OpenXmlElement[] oBaseParents, string[] aObjectProps, ref int iIndex)
        {

            List<OpenXmlElement> oTargetRanges = new List<OpenXmlElement>();

            try
            {
                string xNextElement = "";

                if (aObjectProps.Length > iIndex + 1)
                    xNextElement = aObjectProps[iIndex + 1].ToLower();

                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    string xtype = oBP.GetType().Name.ToLower();   
                    switch (oBP.GetType().Name.ToLower())
                    {
                        //get section range
                        case "sectionproperties":
                            //check for last section
                            if (oBP.Parent.GetType().Name == "Body")
                            {
                                //get previous siblings in current section
                                oTargetRanges = oBP.ElementsBefore()
                                                    .TakeWhile
                                                    (e => e.Descendants<SectionProperties>().Count() == 0)
                                                    .ToList();
                            }
                            else
                            {
                                //get parent paragraph
                                OpenXmlElement oPar = oBP.Ancestors<Paragraph>().LastOrDefault();
                                oTargetRanges.Add(oPar);

                                //get previous siblings in current section
                                IEnumerable<OpenXmlElement> oElements = oPar.ElementsBefore()
                                                                        .TakeWhile
                                                                        (e => e.Descendants<SectionProperties>().Count() == 0)
                                                                        .ToList();

                                //add to list
                                foreach (OpenXmlElement oEl in oElements)
                                    oTargetRanges.Add(oEl);
                            }
                            break;
                        case "tablecell":
                            //Return all Runs contained in the table element to be acted upon
                            IEnumerable<OpenXmlElement> oCellRuns = oBP.Descendants<Run>().ToList();
                            foreach (OpenXmlElement oRun in oCellRuns)
                                oTargetRanges.Add(oRun);
                            break;
                        case "tablerow":
                        case "table":
                            if (xNextElement == "cells")
                            {
                                if (aObjectProps.Length > iIndex + 2)
                                {
                                    //if expression like Tables.Item(1).Range.Cells.Item(2), just skip Range object
                                    xNextElement = aObjectProps[iIndex + 2].ToLower();
                                    if (xNextElement.ToLower().StartsWith("item("))
                                    {
                                        oTargetRanges.Add(oBP);
                                        break;
                                    }
                                }
                                IEnumerable<OpenXmlElement> oCellProps = oBP.Descendants<TableCellProperties>().ToList();
                                foreach (OpenXmlElement oCell in oCellProps)
                                {
                                    oTargetRanges.Add(oCell);
                                }
                                //Skip past .Cells element
                                iIndex++;
                            }
                            else
                            {
                                //Return all Runs contained in the table element to be acted upon
                                IEnumerable<OpenXmlElement> oTableRuns = oBP.Descendants<Run>().ToList();
                                foreach (OpenXmlElement oRun in oTableRuns)
                                    oTargetRanges.Add(oRun);
                            }
                            break;
                        case "document":
                            OpenXmlElement oBody = oBP.Descendants<Body>().FirstOrDefault();
                            oTargetRanges.Add(oBody);
                            break;
                        case "paragraph":
                            IEnumerable<OpenXmlElement> oRuns = oBP.Descendants<Run>().ToList();
                            if (oRuns.Count() > 0)
                            {
                                oTargetRanges.AddRange(oRuns);
                            }
                            else
                            {
                                //If not child Runs, add paragraph itself
                                oTargetRanges.Add(oBP);
                            }
                            break;
                        default:
                            throw new LMP.Exceptions.ActionException("Could not get target range for the OpenXml element " +
                                    oBP.GetType().Name + ".");
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetRange", oE);
            }

            return oTargetRanges.ToArray();
        }

        /// <summary>
        /// returns the target font element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        
        public OpenXmlElement[] GetTargetRunProperties(OpenXmlElement[] oBaseParents)
        {
            List<OpenXmlElement> oTargetRunProps = new List<OpenXmlElement>();
 
            try
            {
                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    switch (oBP.GetType().Name.ToLower())
                    {
                        case "style":
                            StyleRunProperties runProp = oBP.Elements<StyleRunProperties>().FirstOrDefault();
                            if (runProp == null)
                            {
                                runProp = new StyleRunProperties();
                                oBP.Append(new OpenXmlElement[] { runProp });
                            }
                            oTargetRunProps.Add(runProp);
                            break;
                        case "runproperties":
                            oTargetRunProps.Add(oBP);
                            break;
                        case "run":
                            RunProperties oProp = oBP.Elements<RunProperties>().FirstOrDefault();
                            if (oProp == null)
                            {
                                oProp = new RunProperties();
                                oBP.PrependChild<RunProperties>(oProp);
                            }
                            oTargetRunProps.Add(oProp);
                            break;
                        //case "sdtblock":
                        //case "sdtrun":
                        //    //If Parent is a CC, target the individual runs within ot
                        //    IEnumerable<OpenXmlElement> oCCRuns = oBP.Descendants<Run>().ToList();
                        //    foreach (OpenXmlElement oCCRun in oCCRuns)
                        //    {
                        //        RunProperties oRunProps = oCCRun.Elements<RunProperties>().FirstOrDefault();
                        //        if (oRunProps == null)
                        //        {
                        //            oRunProps = new RunProperties();
                        //            oCCRun.PrependChild<RunProperties>(oRunProps);
                        //        }
                        //        oTargetFonts.Add(oRunProps);
                        //    }
                        //    break;
                         case "paragraph":
                            //return array of run properties
                            Run[] aChildRuns = oBP.Descendants<Run>().ToArray();
                            if (aChildRuns.GetLength(0) == 0)
                            {
                                //If Paragraph has no Runs, get RunProperties from the Paragraph itself
                                RunProperties oRunProp = oBP.Elements<RunProperties>().FirstOrDefault();
                                if (oRunProp == null)
                                {
                                    oRunProp = new RunProperties();
                                    oBP.PrependChild<RunProperties>(oRunProp);
                                }
                                oTargetRunProps.Add(oRunProp);
                            }
                            else
                            {
                                foreach (Run oRunElement in aChildRuns)
                                {
                                    RunProperties oRunProp = oRunElement.Elements<RunProperties>().FirstOrDefault();
                                    if (oRunProp == null)
                                    {
                                        oRunProp = new RunProperties();
                                        oBP.PrependChild<RunProperties>(oRunProp);
                                    }
                                    oTargetRunProps.Add(oRunProp);
                                }
                            }
			                break;
                        default:
                            //Font properties are found in the RunProperties
                            Run[] aRuns = oBP.Descendants<Run>().ToArray();
                            if (aRuns.GetLength(0) == 0)
                            {
                                //If there are no child runs, run properties can be assigned at paragraph level
                                Paragraph[] oParas = oBP.Descendants<Paragraph>().ToArray();
                                foreach (Paragraph oPara in oParas)
                                {
                                    RunProperties oRunProps = oPara.Elements<RunProperties>().FirstOrDefault();
                                    if (oRunProps == null)
                                    {
                                        oRunProps = new RunProperties();
                                        oPara.PrependChild<RunProperties>(oRunProps);
                                    }
                                    oTargetRunProps.Add(oRunProps);
                                }
                            }
                            else
                            {
                                foreach (Run oRun in aRuns)
                                {
                                    RunProperties oRunProps = oRun.Elements<RunProperties>().FirstOrDefault();
                                    if (oRunProps == null)
                                    {
                                        oRunProps = new RunProperties();
                                        oRun.PrependChild<RunProperties>(oRunProps);
                                    }
                                    oTargetRunProps.Add(oRunProps);
                                }
                            }
                            break;
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetFont", oE);
            }

            return oTargetRunProps.ToArray();
        }

        /// <summary>
        /// returns the target Field element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        /// <param name="aObjectProps"></param>
        /// <param name="iIndex"></param>
        public OpenXmlElement[] GetTargetFields(OpenXmlElement[] oBaseParents, string[] aObjectProps, ref int iIndex)
        {
            List<OpenXmlElement> oFields = new List<OpenXmlElement>();
            string xNextElement = "";

            if (aObjectProps.Length > iIndex + 1)
                xNextElement = aObjectProps[iIndex + 1].ToLower();

            try
            {
                if ((xNextElement != "") && (xNextElement.Substring(0, 5) == "item("))
                {
                    ////get index
                    string xIndex = xNextElement.Substring(5, xNextElement.IndexOf(")") - 5);

                    throw new LMP.Exceptions.ActionException("Target field item not implemented.");

                    ////cycle through base parents array
                    //foreach (OpenXmlElement oBP in oBaseParents)
                    //{

                    //}

                    ////increment past 'item()' element
                    //iIndex += 1;
                }
                else
                {
                    foreach (OpenXmlElement oBP in oBaseParents)
                    {
                        //TODO:simplify
                        //get Simple fields
                        IEnumerable<SimpleField> oSimpleField = oBP.Descendants<SimpleField>();
                        foreach (SimpleField oF in oSimpleField)
                            oFields.Add(oF);
                        //add complex fields
                        IEnumerable<FieldChar> oComplexField = oBP.Descendants<FieldChar>();
                        foreach (FieldChar oF in oComplexField)
                            oFields.Add(oF);
                        //add field codes
                        IEnumerable<FieldCode> oFieldCode = oBP.Descendants<FieldCode>();
                        foreach (FieldCode oF in oFieldCode)
                            oFields.Add(oF);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetFields", oE);
            }

            return oFields.ToArray();
        }

        /// <summary>
        /// returns the target text element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        public OpenXmlElement[] GetTargetText(OpenXmlElement[] oBaseParents)
        {
            List<OpenXmlElement> oText = new List<OpenXmlElement>();

            try
            {
                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    if (oBP.GetType().Name.ToLower() == "sdtcontent")
                    {
                        Run rRun = oBP.Elements<Run>().FirstOrDefault();
                        if (rRun == null)
                        {
                            rRun = new Run();
                            oBP.Append(new OpenXmlElement[] { rRun });
                        }
                        oText.Add(rRun);
                    }
                    else
                        throw new LMP.Exceptions.ActionException("Could not get target text for the OpenXml element " +
                                oBP.GetType().Name + ".");
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetText", oE);
            }

            return oText.ToArray();
        }

        /// <summary>
        /// returns the target Style element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        public OpenXmlElement[] GetTargetStyle(OpenXmlElement[] oBaseParents)
        {
            //coded for block tables only (see Separate Statement segment)
            List<OpenXmlElement> oTargetProps = new List<OpenXmlElement>();

            try
            {
                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    switch (oBP.GetType().Name.ToLower())
                    {
                        case "sdtblock":
                            //get all Table Properties elements in block
                            IEnumerable<TableProperties> oTableProperties = oBP.Descendants<TableProperties>();
                            if (oTableProperties.Count() == 0)
                                //todo: add new??
                                throw new LMP.Exceptions.ActionException(LMP.Resources.GetLangString(
                                        "Error_CouldNotExecuteVariableAction") + "GetTargetStyle");
                            else
                            {
                                foreach (TableProperties oTpr in oTableProperties)
                                    oTargetProps.Add(oTpr);
                            }
                            break;
                        default:
                            throw new LMP.Exceptions.ActionException("Could not get target style for the OpenXml element " +
                                    oBP.GetType().Name + ".");
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetStyle", oE);
            }

            return oTargetProps.ToArray();
        }

        /// <summary>
        /// returns the target Section element from
        /// the specified base parent element
        /// </summary>
        /// <param name="oBaseParent"></param>
        /// <param name="aObjectProps"></param>
        /// <param name="iIndex"></param>
        public OpenXmlElement[] GetTargetSections(OpenXmlElement[] oBaseParents, string[] aObjectProps, ref int iIndex)
        {
            List<OpenXmlElement> oSections = new List<OpenXmlElement>();
            SectionProperties oSecProp = null;
            string xNextElement = "";

            if (aObjectProps.Length > iIndex + 1)
                xNextElement = aObjectProps[iIndex + 1].ToLower();

            try
            {
                string xIndex = "";

                if ((xNextElement != "") && (xNextElement.Substring(0, 5) == "item("))
                {
                    //get index
                    xIndex = xNextElement.Substring(5, xNextElement.IndexOf(")") - 5);
                }
                foreach (OpenXmlElement oBP in oBaseParents)
                {
                    OpenXmlElement oBody = null;
                    if (oBP is Body)
                    {
                        oBody = oBP;
                    }
                    else if (oBP is Document)
                    {
                        oBody = oBP.Descendants<Body>().FirstOrDefault();
                    }
                    else
                    {
                        oBody = oBP.Ancestors<Body>().FirstOrDefault();
                    }
                    if (oBody == null)
                    {
                        throw new LMP.Exceptions.ActionException(
                            LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                            "GetTargetSections");
                    }
                    SectionProperties[] oSecProps = oBody.Descendants<SectionProperties>().ToArray();
                    if (xIndex == "")
                    {

                        for (int s = 0; s < oSecProps.GetLength(0); s++)
                        {
                            SectionProperties oProps = oSecProps[s];
                            if (s == 0 && (oProps.IsAfter(oBP) || oBP.Contains(oProps)))
                            {
                                oSections.Add(oProps);
                            }
                            else if (s > 1 && oBP.Contains(oProps))
                            {
                                oSections.Add(oProps);
                            }
                        }
                    }
                    else
                    {
                        int iFirstIndex = -1;
                        for (int s = 0; s < oSecProps.GetLength(0); s++ )
                        {
                            SectionProperties oProps = oSecProps[s];
                            if (oProps.IsAfter(oBP) || oBP.Contains(oProps))
                            {
                                iFirstIndex = s;
                                break;
                            }
                        }
                        oSections.Add(oSecProps[iIndex + iFirstIndex]);
                        //increment past 'item()' element
                        iIndex += 1;
                    }

                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                    "GetTargetSections", oE);
            }

            return oSections.ToArray();
        }
        public void SetStyle(OpenXmlElement oTargetObject, string xName)
        {
            OpenXmlElement[] aTargetElements = null;
            StyleDefinitionsPart oPart = this.WPDocument.MainDocumentPart.StyleDefinitionsPart;
            if (oPart != null)
            {
                //special case for Table Normal because UI name differs from primary name
                if (xName == "Table Normal")
                    xName = "Normal Table";

                Style oStyle = oPart.Styles.Elements<Style>().Where(s => s.StyleName.Val.Value.ToUpper() == xName.ToUpper()).FirstOrDefault();
                if (oStyle == null)
                {
                    throw new LMP.Exceptions.ActionException(
                        LMP.Resources.GetLangString("Error_StyleDoesNotExist") + xName);

                }
                switch (oStyle.Type.Value)
                {
                    case StyleValues.Character:
                        aTargetElements = oTargetObject.Descendants<Run>().ToArray();
                        SetCharacterStyle(aTargetElements, oStyle.StyleId.Value);
                        break;
                    case StyleValues.Paragraph:
                        Paragraph oParentPara = oTargetObject.Ancestors<Paragraph>().FirstOrDefault();
                        if (oParentPara == null)
                        {
                            aTargetElements = oTargetObject.Descendants<Paragraph>().ToArray();
                        }
                        else
                        {
                            aTargetElements = new OpenXmlElement[] { oParentPara };
                        }
                        SetParagraphStyle(aTargetElements, oStyle.StyleId.Value);
                        break;
                    case StyleValues.Table:
                        if (oTargetObject is Table)
                        {
                            aTargetElements = new OpenXmlElement[] { oTargetObject };
                        }
                        else if (oTargetObject is TableRow || oTargetObject is TableCell)
                        {
                            Table oParentTable = oTargetObject.Ancestors<Table>().FirstOrDefault();
                            if (oParentTable != null)
                            {
                                aTargetElements = new OpenXmlElement[] { oParentTable };
                            }
                        }
                        SetTableStyle(aTargetElements, oStyle.StyleId);
                        break;
                }
            }
        }
        /// <summary>
        /// Apply character style for the specified targets
        /// </summary>
        /// <param name="aTargetObjects"></param>
        /// <param name="xStyleId"></param>
        public void SetCharacterStyle(OpenXmlElement[] aTargetObjects, string xStyleId)
        {
            OpenXmlElement[] aPProps = GetTargetRunProperties(aTargetObjects);
            foreach (OpenXmlElement oProps in aPProps)
            {
                RunStyle oStyle = oProps.Elements<RunStyle>().FirstOrDefault();
                if (oStyle == null)
                {
                    oStyle = new RunStyle();
                    oProps.AppendChild<RunStyle>(oStyle);
                }
                oStyle.Val = xStyleId;
            }
        }
        /// <summary>
        /// Apply Paragraph style for the specified targets
        /// </summary>
        /// <param name="aTargetObjects"></param>
        /// <param name="xStyleId"></param>
        public void SetParagraphStyle(OpenXmlElement[] aTargetObjects, string xStyleId)
        {
            OpenXmlElement[] aPProps = GetTargetParagraphFormat(aTargetObjects);
            foreach (OpenXmlElement oProps in aPProps)
            {
                ParagraphStyleId oStyle = oProps.Elements<ParagraphStyleId>().FirstOrDefault();
                if (oStyle == null)
                {
                    oStyle = new ParagraphStyleId();
                    oProps.AppendChild<ParagraphStyleId>(oStyle);
                }
                oStyle.Val = xStyleId;
            }
        }
        /// <summary>
        /// Apply table style to the specified targets
        /// </summary>
        /// <param name="aTargetObjects"></param>
        /// <param name="xStyleId"></param>
        public void SetTableStyle(OpenXmlElement[] aTargetObjects, string xStyleId)
        {
            OpenXmlElement[] aTProps = GetTargetTableProperties(aTargetObjects);
            foreach (OpenXmlElement oProps in aTProps)
            {
                TableStyle oTStyle = oProps.Elements<TableStyle>().FirstOrDefault();
                if (oTStyle == null)
                {
                    oTStyle = new TableStyle();
                    oProps.AppendChild<TableStyle>(oTStyle);
                }
                oTStyle.Val = xStyleId;
            }
        }
        public OpenXmlElement[] GetTargetTableProperties(OpenXmlElement[] aTargetObjects)
        {
            List<OpenXmlElement> oTableProps = new List<OpenXmlElement>();
            foreach (OpenXmlElement oTarget in aTargetObjects)
            {
                if (oTarget is Table)
                {
                    TableProperties oTpr = oTarget.Elements<TableProperties>().FirstOrDefault();
                    if (oTpr == null)
                    {
                        oTpr = new TableProperties();
                        oTarget.PrependChild<TableProperties>(oTpr);
                    }
                    oTableProps.Add(oTpr);
                }
                else
                {
                    //If target isn't a table, get table properties for parent table.
                    //If not in a table, get properties for tables contained in target, if any
                    Table[] aTables;
                    Table oParentTable = oTarget.Ancestors<Table>().FirstOrDefault();
                    if (oParentTable != null)
                    {
                        aTables = new  Table[] {oParentTable};
                    }
                    else
                    {
                        aTables = oTarget.Descendants<Table>().ToArray();
                    }
                    foreach (Table oTable in aTables)
                    {
                        TableProperties oTpr = oTable.Elements<TableProperties>().FirstOrDefault();
                        if (oTpr == null)
                        {
                            oTpr = new TableProperties();
                            oTable.PrependChild<TableProperties>(oTpr);
                        }
                        oTableProps.Add(oTpr);
                    }
                }
            }
            return oTableProps.ToArray();
        }
        /// <summary>
        /// sets the font size of the specified target element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontSize(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            int iSizeVal = (int)(decimal.Parse(xPropertyValue) * 2);

            FontSize oSize = oTargetObject.Descendants<FontSize>().FirstOrDefault();
            if (oSize == null)
            {
                oSize = new FontSize();
                oTargetObject.Append(oSize);
            }
            oSize.Val = iSizeVal.ToString();
        }

        /// <summary>
        /// sets the font name of the specified target element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontName(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            RunFonts oFont = oTargetObject.Descendants<RunFonts>().FirstOrDefault();
            if (oFont == null)
            {
                oFont = new RunFonts();
                oTargetObject.Append(oFont);
            }
            oFont.HighAnsi = new StringValue(xPropertyValue);
            oFont.Ascii = new StringValue(xPropertyValue);
            oFont.ComplexScript = new StringValue(xPropertyValue);
        }

        /// <summary>
        /// sets the font far east name of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFarEastFontName(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            RunFonts oFont = oTargetObject.Descendants<RunFonts>().FirstOrDefault();
            if (oFont == null)
            {
                oFont = new RunFonts();
                oTargetObject.Append(oFont);
            }
            oFont.EastAsia = new StringValue(xPropertyValue);
        }

        /// <summary>
        /// sets the bold of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontBold(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            Bold oBold = oTargetObject.Descendants<Bold>().FirstOrDefault();
            if (oBold == null)
            {
                oBold = new Bold();
                oTargetObject.Append(oBold);
            }

            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oBold.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }

        /// <summary>
        /// sets the visibility of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontHidden(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            Vanish oVanish = oTargetObject.Descendants<Vanish>().FirstOrDefault();
            if (oVanish == null)
            {
                oVanish = new Vanish();
                oTargetObject.Append(oVanish);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oVanish.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }

        /// <summary>
        /// sets double strikethrough value of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontDoubleStrikethrough(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            DoubleStrike oProp = oTargetObject.Descendants<DoubleStrike>().FirstOrDefault();
            if (oProp == null)
            {
                oProp = new DoubleStrike();
                oTargetObject.Append(oProp);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oProp.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }

        /// <summary>
        /// sets strikethrough value of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontStrikethrough(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            Strike oProp = oTargetObject.Descendants<Strike>().FirstOrDefault();
            if (oProp == null)
            {
                oProp = new Strike();
                oTargetObject.Append(oProp);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oProp.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }
        /// <summary>
        /// sets superscript value of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontSuperscript(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            VerticalTextAlignment oProp = oTargetObject.Descendants<VerticalTextAlignment>().FirstOrDefault();
            if (oProp == null)
            {
                oProp = new VerticalTextAlignment();
                oTargetObject.Append(oProp);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1" || xPropertyValue.ToLower() == "true")
                oProp.Val = VerticalPositionValues.Superscript;
            else
                oProp.Val = VerticalPositionValues.Baseline;
        }

        /// <summary>
        /// sets subscript value of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontSubscript(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            VerticalTextAlignment oProp = oTargetObject.Descendants<VerticalTextAlignment>().FirstOrDefault();
            if (oProp == null)
            {
                oProp = new VerticalTextAlignment();
                oTargetObject.Append(oProp);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1" || xPropertyValue.ToLower() == "true")
                oProp.Val = VerticalPositionValues.Subscript;
            else
                oProp.Val = VerticalPositionValues.Baseline;
        }
        public void SetFontColorIndex(OpenXmlElement oTargetObject, string xIndex)
        {
            //Convert one of 17 VBA ColorIndex constants to RGB value
            string xColorRGB = "";
            if (xIndex == "0")
            {
                //wdAuto
                oTargetObject.RemoveAllChildren<Color>();
                return;
            }
            else
            {
                xColorRGB = GetColorIndexRGB(xIndex);
            }
            if (xColorRGB != "")
                SetFontRGB(oTargetObject, xColorRGB);
        }
        private string GetColorIndexRGB(string xIndex)
        {
            string xColorRGB = "";
            switch (xIndex)
            {
                case "1": // wdBlack
                    xColorRGB = "000000";
                    break;
                case "2": // wdBlue
                    xColorRGB = "0000FF";
                    break;
                case "3": // wdTurquoise (Cyan)
                    xColorRGB = "00FFFF";
                    break;
                case "4": // wdBrightGreen (Green)
                    xColorRGB = "00FF00";
                    break;
                case "5": // wdPink (Magenta)
                    xColorRGB = "FF00FF";
                    break;
                case "6": // wdRed
                    xColorRGB = "FF0000";
                    break;
                case "7": // wdYellow
                    xColorRGB = "FFFF00";
                    break;
                case "8": // wdWhite
                    xColorRGB = "FFFFFF";
                    break;
                case "9": // wdDarkBlue
                    xColorRGB = "000080";
                    break;
                case "10": // wdTeal (Dark Cyan)
                    xColorRGB = "008080";
                    break;
                case "11": // wdGreen (Dark Green)
                    xColorRGB = "008000";
                    break;
                case "12": // wdViolet (Dark Magenta)
                    xColorRGB = "800080";
                    break;
                case "13": // wdDarkRed
                    xColorRGB = "800000";
                    break;
                case "14": // wdDarkYellow
                    xColorRGB = "808000";
                    break;
                case "15": // wdGray50 (Dark Gray)
                    xColorRGB = "808080";
                    break;
                case "16": // wdGray25 (Light Gray)
                    xColorRGB = "C0C0C0";
                    break;
                default:
                    //No other valid values
                    break;
            }
            return xColorRGB;
        }
        public string GetColorRGB(int iColor)
        {
            //Convert VBA integer value to RGB
            int iRed = iColor % 256;
            int iGreen = iColor / 256 % 256;
            int iBlue = iColor / 256 / 256 % 256;
            //Value is string of 3 2-digit hex values for RGB
            return iRed.ToString("X2") + iGreen.ToString("X2") + iBlue.ToString("X2");
        }

        public void SetFontTextColor(OpenXmlElement oTargetObject, string xColor)
        {

            int iColor = -1;
            string xRGB = "";
            if (Int32.TryParse(xColor, out iColor))
            {
                xRGB = GetColorRGB(iColor);
            }
            else
                return;
            
            SetFontRGB(oTargetObject, xRGB);
        }
        public void SetFontRGB(OpenXmlElement oTargetObject, string xRGB)
        {
            Color oColor = oTargetObject.Descendants<Color>().FirstOrDefault();
            if (oColor == null)
            {
                oColor = new Color();
                oTargetObject.AppendChild<Color>(oColor);
            }
            oColor.Val = xRGB;
        }
        public void SetFontPosition(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //Oxml value is number of half-points
            int iPosition = Int32.Parse(xPropertyValue) * 2;

            Position oPos = oTargetObject.Descendants<Position>().FirstOrDefault();
            if (oPos == null)
            {
                oPos = new Position();
                oTargetObject.Append(oPos);
            }
            oPos.Val = iPosition.ToString();
        }
        public void SetFontSpacing(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //Convert Points to twips
            int iSpacing = (int)(Decimal.Parse(xPropertyValue) * 20);

            Spacing oSpacing = oTargetObject.Descendants<Spacing>().FirstOrDefault();
            if (oSpacing == null)
            {
                oSpacing = new Spacing();
                oTargetObject.Append(oSpacing);
            }
            oSpacing.Val = iSpacing;
        }

        /// <summary>
        /// sets the all caps of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontAllCaps(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            Caps oCaps = oTargetObject.Descendants<Caps>().FirstOrDefault();
            if (oCaps == null)
            {
                oCaps = new Caps();
                oTargetObject.Append(oCaps);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oCaps.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }

        /// <summary>
        /// sets the small caps of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontSmallCaps(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            SmallCaps oSmallCaps = oTargetObject.Descendants<SmallCaps>().FirstOrDefault();
            if (oSmallCaps == null)
            {
                oSmallCaps = new SmallCaps();
                oTargetObject.Append(oSmallCaps);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oSmallCaps.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }

        /// <summary>
        /// sets the italics of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetFontItalic(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            Italic oItalic = oTargetObject.Descendants<Italic>().FirstOrDefault();
            if (oItalic == null)
            {
                oItalic = new Italic();
                oTargetObject.Append(oItalic);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oItalic.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }

        /// <summary>
        /// sets the first line indent of the specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaFirstLineIndent(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            xPropertyValue = ((int)(decimal.Parse(xPropertyValue) * 20)).ToString();

            Indentation oIndent = oTargetObject.Descendants<Indentation>().FirstOrDefault();
            if (oIndent == null)
            {
                oIndent = new Indentation();
                oTargetObject.Append(oIndent);
            }
            oIndent.FirstLine = new StringValue(xPropertyValue);
        }
        public void SetTableCellVerticalAlignment(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            TableCellVerticalAlignment oVert = oTargetObject.Elements<TableCellVerticalAlignment>().FirstOrDefault();
            TableVerticalAlignmentValues iVert = TableVerticalAlignmentValues.Top;
            switch (xPropertyValue)
            {
                case "3":
                    iVert = TableVerticalAlignmentValues.Bottom;
                    break;
                case "1":
                    iVert = TableVerticalAlignmentValues.Center;
                    break;
            }
            if (oVert == null)
            {
                oVert = new TableCellVerticalAlignment();
                oTargetObject.Append(oVert);
            }
            oVert.Val = iVert;
        }
        public void SetPageVerticalAlignment(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            VerticalJustificationValues iVert = (VerticalJustificationValues)Enum.Parse(typeof(VerticalJustificationValues), xPropertyValue);
            VerticalTextAlignmentOnPage oVert = oTargetObject.Elements<VerticalTextAlignmentOnPage>().FirstOrDefault();
            if (oVert == null)
            {
                oVert = new VerticalTextAlignmentOnPage();
                oTargetObject.Append(oVert);
            }
            oVert.Val = iVert;
        }
        /// <summary>
        /// sets the left indent of the specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaLeftIndent(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            xPropertyValue = ((int)(decimal.Parse(xPropertyValue) * 20)).ToString();
            Indentation oIndent = oTargetObject.Descendants<Indentation>().FirstOrDefault();
            if (oIndent == null)
            {
                oIndent = new Indentation();
                oTargetObject.Append(oIndent);
            }
            oIndent.Left = new StringValue(xPropertyValue);
        }

        /// <summary>
        /// sets the space before the specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaSpaceBefore(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            xPropertyValue = ((int)(decimal.Parse(xPropertyValue) * 20)).ToString();

            SpacingBetweenLines oSpacing = oTargetObject.Descendants<SpacingBetweenLines>().FirstOrDefault();
            if (oSpacing == null)
            {
                oSpacing = new SpacingBetweenLines();
                oTargetObject.Append(oSpacing);
            }
            oSpacing.Before = new StringValue(xPropertyValue);
        }

        /// <summary>
        /// sets the space after the specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaSpaceAfter(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            xPropertyValue = ((int)(decimal.Parse(xPropertyValue) * 20)).ToString();
            SpacingBetweenLines oSpacing = oTargetObject.Descendants<SpacingBetweenLines>().FirstOrDefault();
            if (oSpacing == null)
            {
                oSpacing = new SpacingBetweenLines();
                oTargetObject.Append(oSpacing);
            }
            oSpacing.After = new StringValue(xPropertyValue);
        }
        /// <summary>
        /// sets the space after the specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaLineSpacing(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            xPropertyValue = ((int)(decimal.Parse(xPropertyValue) * 240)).ToString();
            SpacingBetweenLines  oSpacing = oTargetObject.Descendants<SpacingBetweenLines>().FirstOrDefault();
            if (oSpacing == null)
            {
                oSpacing = new SpacingBetweenLines();
                oSpacing.LineRule.Value = LineSpacingRuleValues.Auto;
                oTargetObject.Append(oSpacing);
            }
            oSpacing.Line.Value = xPropertyValue;
        }
        /// <summary>
        /// sets the space after the specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaLineSpacingRule(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            LineSpacingRuleValues iRule = 0;
            int iLine = -1;
            switch (xPropertyValue)
            {
                case "0":
                    iRule = LineSpacingRuleValues.Auto;
                    iLine = 240;
                    break;
                case "1":
                    iRule = LineSpacingRuleValues.Auto;
                    iLine = 360;
                    break;
                case "2":
                    iRule = LineSpacingRuleValues.Auto;
                    iLine = 480;
                    break;
                case "3":
                    iRule = LineSpacingRuleValues.AtLeast;
                    break;
                case "4":
                    iRule = LineSpacingRuleValues.Exact;
                    break;
                case "5":
                    iRule = LineSpacingRuleValues.Auto;
                    break;
            }
            SpacingBetweenLines oSpacing = oTargetObject.Descendants<SpacingBetweenLines>().FirstOrDefault();
            if (oSpacing == null)
            {
                oSpacing = new SpacingBetweenLines();
                oTargetObject.Append(oSpacing);
            }
            oSpacing.LineRule = iRule;
            if (iLine > -1)
            {
                oSpacing.Line = iLine.ToString();
            }
        }

        /// <summary>
        /// sets the alignment of the specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaAlignment(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            int iAlign = 0;
            //Convert VBA values to OpenXML enum values
            if (Int32.TryParse(xPropertyValue, out iAlign))
            {
                JustificationValues xAlign = JustificationValues.Left;
                switch (iAlign)
                {
                    case 0:
                        xAlign = JustificationValues.Left;
                        break;
                    case 1:
                        xAlign = JustificationValues.Center;
                        break;
                    case 2:
                        xAlign = JustificationValues.Right;
                        break;
                    case 3:
                        xAlign = JustificationValues.Both;
                        break;
                    case 4:
                        xAlign = JustificationValues.Distribute;
                        break;
                    case 5:
                        xAlign = JustificationValues.MediumKashida;
                        break;
                    case 6:
                        xAlign = JustificationValues.HighKashida;
                        break;
                    case 7:
                        xAlign = JustificationValues.LowKashida;
                        break;
                    case 8:
                        xAlign = JustificationValues.ThaiDistribute;
                        break;
                }
                Justification oJust = oTargetObject.Descendants<Justification>().FirstOrDefault();
                if (oJust == null)
                {
                    oJust = new Justification();
                    oTargetObject.Append(oJust);
                }
                oJust.Val = xAlign;
            }
        }
        /// <summary>
        /// sets Keep Lines Together for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaKeepLines(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            KeepLines oKeep = oTargetObject.Descendants<KeepLines>().FirstOrDefault();
            if (oKeep == null)
            {
                oKeep = new KeepLines();
                oTargetObject.Append(oKeep);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oKeep.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }
        /// <summary>
        /// sets Keep With Next for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaKeepNext(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            KeepNext oKeep = oTargetObject.Descendants<KeepNext>().FirstOrDefault();
            if (oKeep == null)
            {
                oKeep = new KeepNext();
                oTargetObject.Append(oKeep);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oKeep.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }
        /// <summary>
        /// sets Page Break Before for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaPageBefore(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            PageBreakBefore oBreak = oTargetObject.Descendants<PageBreakBefore>().FirstOrDefault();
            if (oBreak == null)
            {
                oBreak = new PageBreakBefore();
                oTargetObject.Append(oBreak);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oBreak.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }
        /// <summary>
        /// sets Suppress Line Numbers for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaSuppressLines(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            SuppressLineNumbers oSuppress = oTargetObject.Descendants<SuppressLineNumbers>().FirstOrDefault();
            if (oSuppress == null)
            {
                oSuppress = new SuppressLineNumbers();
                oTargetObject.Append(oSuppress);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oSuppress.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }

        /// <summary>
        /// sets Suppress Auto Hyphens for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaAutoHyphens(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            SuppressAutoHyphens oSuppress = oTargetObject.Descendants<SuppressAutoHyphens>().FirstOrDefault();
            if (oSuppress == null)
            {
                oSuppress = new SuppressAutoHyphens();
                oTargetObject.Append(oSuppress);
            }
            //VBA Property returns 0 or -1
            //ParagraphFormat.Hyphenation=1 corresponds to suppressAutoHyphens=off
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "False";
            else if (xPropertyValue == "0")
                xPropertyValue = "True";
            oSuppress.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }
        /// <summary>
        /// sets "don't add space between paragraphs of the same style" for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaDontAddSpace(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            ContextualSpacing oSpacing = oTargetObject.Descendants<ContextualSpacing>().FirstOrDefault();
            if (oSpacing == null)
            {
                oSpacing = new ContextualSpacing();
                oTargetObject.Append(oSpacing);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oSpacing.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }
        /// <summary>
        /// sets Mirror Indents for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaMirrorIndents(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            MirrorIndents oMirror = oTargetObject.Descendants<MirrorIndents>().FirstOrDefault();
            if (oMirror == null)
            {
                oMirror = new MirrorIndents();
                oTargetObject.Append(oMirror);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oMirror.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }

        /// <summary>
        /// sets Widow/Orphan control for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaWidowControl(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            WidowControl oWidow = oTargetObject.Descendants<WidowControl>().FirstOrDefault();
            if (oWidow == null)
            {
                oWidow = new WidowControl();
                oTargetObject.Append(oWidow);
            }
            //VBA Property returns 0 or -1
            if (xPropertyValue == "-1" || xPropertyValue == "1")
                xPropertyValue = "True";
            else if (xPropertyValue == "0")
                xPropertyValue = "False";
            oWidow.Val = OnOffValue.FromBoolean(Convert.ToBoolean(xPropertyValue));
        }
        /// <summary>
        /// sets Outline Level for specified paragraph element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetParaOutlineLevel(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            OutlineLevel oSuppress = oTargetObject.Descendants<OutlineLevel>().FirstOrDefault();
            if (oSuppress == null)
            {
                oSuppress = new OutlineLevel();
                oTargetObject.Append(oSuppress);
            }
            int iLevel = 9;
            if (Int32.TryParse(xPropertyValue, out iLevel))
            {
                //zero-based
                iLevel = iLevel - 1;
            }
            else
            {
                //Body text
                iLevel = 9;
            }
            oSuppress.Val = iLevel;
        }

        /// <summary>
        /// sets the language id of the specified element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetLanguageID(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            int iCulture = 0;
            if (Int32.TryParse(xPropertyValue, out iCulture))
            {
                System.Globalization.CultureInfo oCulture = System.Globalization.CultureInfo.GetCultureInfo(iCulture);
                if (oCulture != null)
                {
                    Languages oLang = oTargetObject.Descendants<Languages>().FirstOrDefault();
                    if (oLang == null)
                    {
                        oLang = new Languages();
                        oTargetObject.Append(oLang);
                    }
                    oLang.Val = oCulture.Name;
                }
            }
        }

        /// <summary>
        /// sets the top margin of the specified page setup element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetPageSetupTopMargin(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            int iValue = ((int)(decimal.Parse(xPropertyValue) * 20));
            PageMargin oPageMargin = oTargetObject.Descendants<PageMargin>().FirstOrDefault();
            if (oPageMargin != null)
            {
                oPageMargin = new PageMargin();
                oTargetObject.Append(oPageMargin);
            }
            oPageMargin.Top = iValue;
        }

        /// <summary>
        /// sets the bottom margin of the specified page setup element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetPageSetupBottomMargin(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            int iValue = ((int)(decimal.Parse(xPropertyValue) * 20));
            PageMargin oPageMargin = oTargetObject.Descendants<PageMargin>().FirstOrDefault();
            if (oPageMargin != null)
            {
                oPageMargin = new PageMargin();
                oTargetObject.Append(oPageMargin);
            }
            oPageMargin.Bottom = iValue;
        }

        /// <summary>
        /// sets the left margin of the specified page setup element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetPageSetupLeftMargin(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            int iValue = ((int)(decimal.Parse(xPropertyValue) * 20));
            PageMargin oPageMargin = oTargetObject.Descendants<PageMargin>().FirstOrDefault();
            if (oPageMargin != null)
            {
                oPageMargin = new PageMargin();
                oTargetObject.Append(oPageMargin);
            }
            oPageMargin.Left = UInt32Value.FromUInt32((uint)iValue);
        }

        /// <summary>
        /// sets the right margin of the specified page setup element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetPageSetupRightMargin(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            int iValue = ((int)(decimal.Parse(xPropertyValue) * 20));
            PageMargin oPageMargin = oTargetObject.Descendants<PageMargin>().FirstOrDefault();
            if (oPageMargin != null)
            {
                oPageMargin = new PageMargin();
                oTargetObject.Append(oPageMargin);
            }
            oPageMargin.Right = UInt32Value.FromUInt32((uint)iValue);
        }

        /// <summary>
        /// sets the header distance of the specified page setup element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetPageSetupHeaderDistance(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            int iValue = ((int)(decimal.Parse(xPropertyValue) * 20));
            PageMargin oPageMargin = oTargetObject.Descendants<PageMargin>().FirstOrDefault();
            if (oPageMargin != null)
            {
                oPageMargin = new PageMargin();
                oTargetObject.Append(oPageMargin);
            }
            oPageMargin.Header = UInt32Value.FromUInt32((uint)iValue);
        }

        /// <summary>
        /// sets the footer distance of the specified page setup element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetPageSetupFooterDistance(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            int iValue = ((int)(decimal.Parse(xPropertyValue) * 20));
            PageMargin oPageMargin = oTargetObject.Descendants<PageMargin>().FirstOrDefault();
            if (oPageMargin != null)
            {
                oPageMargin = new PageMargin();
                oTargetObject.Append(oPageMargin);
            }
            oPageMargin.Footer = UInt32Value.FromUInt32((uint)iValue);
        }

        /// <summary>
        /// sets the gutter of the specified page setup element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetPageSetupGutter(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            //convert points to twips
            int iValue = ((int)(decimal.Parse(xPropertyValue) * 20));
            PageMargin oPageMargin = oTargetObject.Descendants<PageMargin>().FirstOrDefault();
            if (oPageMargin != null)
            {
                oPageMargin = new PageMargin();
                oTargetObject.Append(oPageMargin);
            }
            oPageMargin.Gutter = UInt32Value.FromUInt32((uint)iValue);
        }

        /// <summary>
        /// sets the height rule of the specified table row element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetTableRowHeightRule(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            HeightRuleValues iRule = 0;
            switch (xPropertyValue)
            {
                case "0":
                    iRule = HeightRuleValues.Auto;
                    break;
                case "1":
                    iRule = HeightRuleValues.AtLeast;
                    break;
                case "2":
                    iRule = HeightRuleValues.Exact;
                    break;
                default:
                    //not a valid value
                    throw new LMP.Exceptions.ActionException(
                        LMP.Resources.GetLangString("Error_CouldNotExecuteVariableAction") +
                        "GetTableRowHeightRule");
            }
            TableRowProperties oTRP = oTargetObject.Elements<TableRowProperties>().FirstOrDefault();
            if (oTRP == null)
            {
                oTRP = new TableRowProperties();
                oTargetObject.PrependChild<TableRowProperties>(oTRP);
            }
            TableRowHeight oRowH = oTRP.Elements<TableRowHeight>().FirstOrDefault();
            if (oRowH == null)
            {
                oRowH = new TableRowHeight();
                oTRP.AppendChild<TableRowHeight>(oRowH);
            }
            //Convert points to twips
            oRowH.HeightType = iRule;
        }
        /// <summary>
        /// sets the height of the specified table row element
        /// </summary>
        /// <param name="oTargetObject"></param>
        /// <param name="xPropertyValue"></param>
        public void SetTableRowHeight(OpenXmlElement oTargetObject, string xPropertyValue)
        {
            uint iHeight = 0;
            //Convert VBA values to OpenXML enum values
            if (uint.TryParse(xPropertyValue, out iHeight))
            {
                TableRowProperties oTRP = oTargetObject.Elements<TableRowProperties>().FirstOrDefault();
                if (oTRP == null)
                {
                    oTRP = new TableRowProperties();
                    oTargetObject.PrependChild<TableRowProperties>(oTRP);
                }
                TableRowHeight oRowH = oTRP.Elements<TableRowHeight>().FirstOrDefault();
                if (oRowH == null)
                {
                    oRowH = new TableRowHeight();
                    oTRP.AppendChild<TableRowHeight>(oRowH);
                }
                //Convert points to twips
                oRowH.Val = iHeight * 20;
            }
        }
        /// <summary>
        /// Retrieves value of Built-in Document Property located in the Core Properties part
        /// </summary>
        /// <param name="oWPDoc"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        public string WDRetrieveCoreProperty(WordprocessingDocument oWPDoc, string xPropertyName)
        {
            // Given a document name and a core property, retrieve the value of the property.
            // Note that because this code uses the SelectSingleNode method, 
            // the search is case sensitive. That is, looking for "Author" is not 
            // the same as looking for "author".
            string xPropVal = "";

            const string CORE_PROPERTIES_SCHEMA = "http://schemas.openxmlformats.org/package/2006/metadata/core-properties";
            const string DC_PROPERTIES_SCHEMA = "http://purl.org/dc/elements/1.1/";
            const string DC_TERMS_PROPERTIES_SCHEMA = "http://purl.org/dc/terms/";

            // Get the core properties part (core.xml).
            CoreFilePropertiesPart oCorePropertiesPart = oWPDoc.CoreFilePropertiesPart;
            if (oCorePropertiesPart != null)
            {

                // Manage namespaces to perform XML XPath queries.
                NameTable oNT = new NameTable();
                XmlNamespaceManager oNSManager = new XmlNamespaceManager(oNT);
                oNSManager.AddNamespace("cp", CORE_PROPERTIES_SCHEMA);
                oNSManager.AddNamespace("dc", DC_PROPERTIES_SCHEMA);
                oNSManager.AddNamespace("dcterms", DC_TERMS_PROPERTIES_SCHEMA);

                // Get the properties from the package.
                XmlDocument oXDoc = new XmlDocument(oNT);

                // Load the XML in the part into an XmlDocument instance.
                oXDoc.Load(oCorePropertiesPart.GetStream());

                string xSearchString = string.Format("//cp:coreProperties/{0}", xPropertyName);

                XmlNode xNode = oXDoc.SelectSingleNode(xSearchString, oNSManager);
                if (!(xNode == null))
                {
                    xPropVal = xNode.InnerText;
                }
                return xPropVal;
            }
            else
                //No built-in properties exist
                return "";
        }
        /// <summary>
        /// Sets value of Built-in Document Property stored in the Core Properties part
        /// </summary>
        /// <param name="oWPDoc"></param>
        /// <param name="xPropertyName"></param>
        /// <param name="xPropertyValue"></param>
        public void WDSetCoreProperty(WordprocessingDocument oWPDoc, string xPropertyName, string xPropertyValue)
        {
            // Given a document name, a property name, and a value, update the document.

            const string CORE_PROPERTIES_SCHEMA = "http://schemas.openxmlformats.org/package/2006/metadata/core-properties";
            const string DC_PROPERTIES_SCHEMA = "http://purl.org/dc/elements/1.1/";

            string xProp = "";
            switch (xPropertyName.ToLower())
            {
                case "title":
                case "subject":
                    xProp = "dc:" + xPropertyName.ToLower();
                    break;
                case "author":
                    xProp = "dc:creator";
                    break;
                case "keywords":
                case "category":
                    xProp = "cp:" + xPropertyName.ToLower();
                    break;
                case "comments":
                    xProp = "cp:description";
                    break;
                case "document version":
                    xProp = "cp:version";
                    break;
                case "last author":
                    xProp = "cp:lastModifiedBy";
                    break;
                default:
                    //read-only property
                    return;
            }

            CoreFilePropertiesPart oCorePropertiesPart = oWPDoc.CoreFilePropertiesPart;
            if (oCorePropertiesPart == null)
            {
                //Create valid cp:coreProperties part
                oCorePropertiesPart = oWPDoc.AddCoreFilePropertiesPart();
                using (XmlTextWriter oWriter = new XmlTextWriter(oCorePropertiesPart.GetStream(System.IO.FileMode.Create), System.Text.Encoding.UTF8))
                {
                    oWriter.WriteRaw("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<cp:coreProperties xmlns:cp=\"http://schemas.openxmlformats.org/package/2006/metadata/core-properties\" " +
                        "xmlns:dc=\"http://purl.org/dc/elements/1.1/\" " +
                        "xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:dcmitype=\"http://purl.org/dc/dcmitype/\" " +
                        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"></cp:coreProperties>");
                    oWriter.Flush();
                }

            }
            if (oCorePropertiesPart != null)
            {

                // Manage namespaces to perform Xml XPath queries.
                NameTable oNT = new NameTable();
                XmlNamespaceManager oNSManager = new XmlNamespaceManager(oNT);
                oNSManager.AddNamespace("cp", CORE_PROPERTIES_SCHEMA);
                oNSManager.AddNamespace("dc", DC_PROPERTIES_SCHEMA);

                // Get the properties from the package.
                XmlDocument oXDoc = new XmlDocument(oNT);

                // Load the XML in the part into an XmlDocument instance:
                oXDoc.Load(oCorePropertiesPart.GetStream());

                string xSearchString = string.Format("//cp:coreProperties/{0}", xProp);
                XmlNode oNode = oXDoc.SelectSingleNode(xSearchString, oNSManager);
                if (oNode != null)
                {
                    // Now update the value.
                    oNode.InnerText = xPropertyValue;

                    // Save the properties XML back to its part.
                    oXDoc.Save(oCorePropertiesPart.GetStream());
                }
                else
                {
                    // Save the properties XML back to its part.
                    oXDoc.DocumentElement.InnerXml = oXDoc.DocumentElement.InnerXml + "<" + xProp + ">" + xPropertyValue + "</" + xProp + ">";
                    oXDoc.Save(oCorePropertiesPart.GetStream());
                }
            }
        }

        /// <summary>
        /// Sets value of Built-in Document Property stored in the Extended File Properties part
        /// </summary>
        /// <param name="oWPDoc"></param>
        /// <param name="xPropertyName"></param>
        /// <param name="xPropertyValue"></param>
        public void WDSetAppProperty(WordprocessingDocument oWPDoc, string xPropertyName, string xPropertyValue)
        {
            // Given a document, a property name, and a value, update the document.
            ExtendedFilePropertiesPart oAppPropertiesPart = oWPDoc.ExtendedFilePropertiesPart;
            if (oAppPropertiesPart == null)
            {
                //Create valid cp:coreProperties part
                oAppPropertiesPart = oWPDoc.AddExtendedFilePropertiesPart();
                oAppPropertiesPart.Properties  = new DocumentFormat.OpenXml.ExtendedProperties.Properties();

            }
            DocumentFormat.OpenXml.ExtendedProperties.Properties oProps = oAppPropertiesPart.Properties;
            if (oProps != null)
            {
                switch (xPropertyName.ToLower())
                {
                    case "manager":
                        Manager oManager = oProps.Manager;
                        if (oManager == null)
                        {
                            oProps.Manager = new Manager();
                        }
                        oProps.Manager.Text = xPropertyValue;
                        break;
                    case "company":
                        Company oCompany = oProps.Company;
                        if (oCompany == null)
                        {
                            oProps.Company = new Company();
                        }
                        oProps.Company.Text = xPropertyValue;
                        break;
                    case "hyperlink base":
                        HyperlinkBase oHL = oProps.HyperlinkBase;
                        if (oHL == null)
                        {
                            oProps.HyperlinkBase = new HyperlinkBase();
                        }
                        oProps.HyperlinkBase.Text = xPropertyValue;
                        break;
                    default:
                        //other properties read-only
                        return;
                }
            }
            oProps.Save();
        }
        /// <summary>
        /// Retrieves value of Built-in Document Property stored in the ExtendedFileProperties part
        /// </summary>
        /// <param name="oWPDoc"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        public string WDRetrieveAppProperty(WordprocessingDocument oWPDoc, string xPropertyName)
        {
            // Given a document name, a property name, and a value, update the document.
            ExtendedFilePropertiesPart oAppPropertiesPart = oWPDoc.ExtendedFilePropertiesPart;
            if (oAppPropertiesPart != null)
            {
                try
                {
                    switch (xPropertyName.ToLower())
                    {
                        case "manager":
                            return oAppPropertiesPart.Properties.Manager.Text;
                        case "company":
                            return oAppPropertiesPart.Properties.Company.Text;
                        case "hyperlink base":
                            return oAppPropertiesPart.Properties.HyperlinkBase.Text;
                        case "application name":
                            return oAppPropertiesPart.Properties.Application.Text;
                        case "application version":
                            return oAppPropertiesPart.Properties.ApplicationVersion.Text;
                        case "number of lines":
                            return oAppPropertiesPart.Properties.Lines.Text;
                        case "number of pages":
                            return oAppPropertiesPart.Properties.Pages.Text;
                        case "number of paragraphs":
                            return oAppPropertiesPart.Properties.Paragraphs.Text;
                        case "total editing time":
                            return oAppPropertiesPart.Properties.TotalTime.Text;
                        case "number of words":
                            return oAppPropertiesPart.Properties.Words.Text;
                        case "template":
                            return oAppPropertiesPart.Properties.Template.Text;
                        case "number of characters":
                            return oAppPropertiesPart.Properties.Characters.Text;
                        case "number of characters (with spaces)":
                            return oAppPropertiesPart.Properties.CharactersWithSpaces.Text;
                        case "security":
                            return oAppPropertiesPart.Properties.DocumentSecurity.Text;
                    }
                }
                catch { }

            }
            //property not found
            return "";
        }
        public static string[,] GetCustomProperties(WordprocessingDocument oDoc)
        {
            //GLOG 15753
            return Query.GetCustomPropertiesArray(oDoc);
        }
        /// <summary>
        /// Sets value of custom document property stored in the Custom File Properties part
        /// </summary>
        /// <param name="oWPDoc"></param>
        /// <param name="xPropertyName"></param>
        /// <param name="xPropertyValue"></param>
        public void WDSetCustomProperty(WordprocessingDocument oWPDoc, string xPropertyName, string xPropertyValue)
        {
            CustomFilePropertiesPart oPropPart = oWPDoc.CustomFilePropertiesPart;
            //Add new part if necessary
            if (oPropPart == null)
            {
                oPropPart = oWPDoc.AddNewPart<CustomFilePropertiesPart>();
                oPropPart.Properties = new  DocumentFormat.OpenXml.CustomProperties.Properties();
            }
            DocumentFormat.OpenXml.CustomProperties.Properties oCustProps = oPropPart.Properties;
            if (oCustProps != null)
            {
                var oCustProp = oCustProps.Where(p => ((CustomDocumentProperty)p).Name.Value.ToUpper() == xPropertyName.ToUpper()).FirstOrDefault();
                if (oCustProp != null)
                {
                    //If Custom Property already exists, remove and recreate to ensure text type it used
                    oCustProp.Remove();
                }
                CustomDocumentProperty oNewProp = new CustomDocumentProperty();
                oNewProp.VTLPWSTR = new VTLPWSTR(xPropertyValue); //Text property
                oNewProp.FormatId = "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}"; //FormatId is always this
                oNewProp.Name = xPropertyName;
                oCustProps.AppendChild<CustomDocumentProperty>(oNewProp);
                int iPid = 2;
                //PropertyId values must be consecutive starting with 2
                foreach (CustomDocumentProperty oCDP in oCustProps)
                {
                    oNewProp.PropertyId = iPid++;
                }
                oCustProps.Save();
            }
        }
        /// <summary>
        /// Retrieves value of Custom Document Property stored in the Custom File Properties part
        /// </summary>
        /// <param name="oWPDoc"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        public string WDRetrieveCustomProperty(WordprocessingDocument oWPDoc, string xPropertyName)
        {
            const string CUSTOM_PROPERTIES_SCHEMA = "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties";
            const string CUSTOM_VTYPES_SCHEMA = "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes";

            string xPropertyValue = "";

            // Get the custom properties part (custom.xml).
            CustomFilePropertiesPart oCustomPropertiesPart = oWPDoc.CustomFilePropertiesPart;
            if (oCustomPropertiesPart != null)
            {
                // There may not be a custom properties part.
                if (oCustomPropertiesPart != null)
                {
                    // Manage namespaces to perform XML XPath queries.
                    NameTable oNT = new NameTable();
                    XmlNamespaceManager oNSManager = new XmlNamespaceManager(oNT);
                    oNSManager.AddNamespace("d", CUSTOM_PROPERTIES_SCHEMA);
                    oNSManager.AddNamespace("vt", CUSTOM_VTYPES_SCHEMA);

                    // Get the properties from the package.
                    XmlDocument oXDoc = new XmlDocument(oNT);

                    // Load the XML in the part into an XmlDocument instance.
                    oXDoc.Load(oCustomPropertiesPart.GetStream());

                    string xSearchString = string.Format("d:Properties/d:property[@name='{0}']", xPropertyName);
                    XmlNode xNode = oXDoc.SelectSingleNode(xSearchString, oNSManager);
                    if ((xNode != null))
                    {
                        xPropertyValue = xNode.InnerText;
                    }
                }

            }
            return xPropertyValue;
        }
        /// <summary>
        /// returns true if property name corresponds to a built-in document property stored in the Core Properties part
        /// </summary>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        public bool WDIsCorePropertyName(string xPropName)
        {
            //Return true if one of Word's built-in properties
            switch (xPropName.ToUpper())
            {
                case "TITLE":
                case "SUBJECT":
                case "AUTHOR":
                case "KEYWORDS":
                case "COMMENTS":
                case "LAST AUTHOR":
                case "REVISION NUMBER":
                case "LAST PRINT DATE":
                case "CREATION DATE":
                case "LAST SAVE TIME":
                case "CATEGORY":
                case "FORMAT":
                case "CONTENT TYPE":
                case "CONTENT STATUS":
                case "LANGUAGE":
                case "DOCUMENT VERSION":
                    return true;
                default:
                    return false;
            }
        }
        /// <summary>
        /// returns true if property name corresponds to a built-in document property stored in the Extended File Properties part
        /// </summary>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        public bool WDIsAppPropertyName(string xPropName)
        {
            //Return true if one of Word's built-in properties
            switch (xPropName.ToUpper())
            {
                case "MANAGER":
                case "COMPANY":
                case "HYPERLINK BASE":
                case "APPLICATION NAME":
                case "TEMPLATE":
                case "SECURITY":
                case "NUMBER OF CHARACTERS (WITH SPACES)":
                case "NUMBER OF BYTES":
                case "NUMBER OF LINES":
                case "NUMBER OF PARAGRAPHS":
                case "NUMBER OF PAGES":
                case "NUMBER OF WORDS":
                case "NUMBER OF CHARACTERS":
                case "TOTAL EDITING TIME":
                    return true;
                default:
                    return false;
            }
        }
        public void WDSetDocVarValue(WordprocessingDocument oWPDoc, string xVarName, string xVarValue)
        {
            //get settings part
            Settings oSettings = oWPDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            if (oSettings != null)
            {
                //Variable Names not case sensitive
                var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                               where dv.Name.Value.ToUpper() == xVarName.ToUpper()
                               select dv;

                if (oDocVars.Count() == 0)
                {
                    //Document Variable doesn' exist - append new variable
                    oSettings.Elements<DocumentVariables>().FirstOrDefault().AppendChild(new DocumentVariable() { Name = xVarName, Val = xVarValue });
                }
                else
                {
                    //Set value of existing doc variable
                    DocumentVariable oDocVar = oDocVars.First();
                    oDocVar.Val.Value = xVarValue;
                }
            }
        }
        public string WDRetrieveDocVarValue(WordprocessingDocument oWPDoc, string xVarName)
        {
            //get settings part
            Settings oSettings = oWPDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;
            if (oSettings != null)
            {
                //Variable Names not case sensitive
                var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                               where dv.Name.Value.ToUpper() == xVarName.ToUpper()
                               select dv;

                if (oDocVars.Count() == 0)
                {
                    //Document Variable doesn' exist
                    return "";
                }
                else
                {
                    //Set value of existing doc variable
                    DocumentVariable oDocVar = oDocVars.First();
                    return oDocVar.Val.Value;
                }
            }
            //No Document Variables in XML
            return "";
        }
        public bool SetupLabelTable(SdtElement oCC, WordprocessingDocument oDoc, int iLabelCount, int iStartPos)
        {
            Table oTbl = null;
            bool bSepRows = false;
            bool bSepCols = false;
            int iRows = 0;
            int iCols = 0;
            int iColsPerRow = 0;
            int iRowsPerPage = 0;
            int iPagesNeeded = 0;
            int iLabelsPerPage = 0;
            int iLastLabel = 0;
            string xCellXml = "";
            string xEmptyCellXml = "";
            oTbl = oCC.Ancestors<Table>().FirstOrDefault<Table>();

            if (oTbl == null)
            {
                throw new LMP.Exceptions.XMLException(LMP.Resources.GetLangString("NoTableInXml"));
            }

            TableGrid oGrid = null;
            try
            {
                oGrid = oTbl.Elements<TableGrid>().FirstOrDefault();
            }
            catch { }
            if (oGrid != null)
            {
                GridColumn[] aGridCols = oGrid.Descendants<GridColumn>().ToArray();
                iColsPerRow = aGridCols.Count();
                int iFirstColWidth = int.Parse(aGridCols[0].Width.Value);
                for (int c = 0; c < aGridCols.GetLength(0); c++)
                {
                    if (iFirstColWidth - (Int32.Parse(aGridCols[c].Width.Value)) > 200)
                    {
                        bSepCols = true;
                    }
                    else
                    {
                        iCols++;
                    }
                }
            }
            else
            {
                //If there's no TableGrid, there can't be separator columns -
                //just count cells in row
                TableCell[] aCells = oTbl.Elements<TableRow>().First().Elements<TableCell>().ToArray();
                iCols = aCells.GetLength(0);
                iColsPerRow = iCols;
            }
            TableRow[] oRows = oTbl.Elements<TableRow>().ToArray();
            int iFirstRowHeight = (int)oRows[0].Descendants<TableRowHeight>().First().Val.Value;
            iRowsPerPage = oRows.GetLength(0);
            foreach (TableRow oRow in oRows)
            {
                if (iFirstRowHeight - (int)oRow.Descendants<TableRowHeight>().First().Val.Value > 10)
                {
                    bSepRows = true;
                }
                else
                {
                    iRows++;
                }
            }
            iLabelsPerPage = iRows * iCols;
            if (iLabelCount == -1)
                iLabelCount = iLabelsPerPage - (iStartPos - 1);
            else if (iLabelCount == 0) //GLOG 15901: Always leave at least 1 content control
                iLabelCount = 1;

            iLastLabel = iStartPos + iLabelCount - 1;
            iPagesNeeded = (int)Math.Ceiling((decimal)((double)iLastLabel / (double)iLabelsPerPage));

            TableCell oFirstCell = oCC.Ancestors<TableCell>().First();

            oFirstCell.RemoveAllChildren<BookmarkStart>();
            oFirstCell.RemoveAllChildren<BookmarkEnd>();
            xCellXml = oFirstCell.InnerXml;
            if (iCols > 1 || iRows > 1)
            {
                xEmptyCellXml = oTbl.Elements<TableRow>().Last().Elements<TableCell>().Last().InnerXml;
            }

            int iStartLabel = 1;
            int iCurLabel = 0;
            int iRowCounter = bSepRows ? 2 : 1;
            int iColCounter = bSepCols ? 2 : 1;
            List<string> oRowsXml = new List<string>();

            if (iPagesNeeded == 1)
            {
                iStartLabel = iStartPos;
            }
            else
            {
                iStartLabel = 1;
            }
            for (int r = 0; r < iRowsPerPage; r += iRowCounter)
            {
                TableRow oCurRow = oRows[r];
                TableCell[] oCells = oCurRow.Elements<TableCell>().ToArray();
                for (int c = 0; c < iColsPerRow; c += iColCounter)
                {
                    TableCell oCurCell = oCells[c];
                    iCurLabel++;
                    if (iCurLabel < iStartLabel || iCurLabel > iLastLabel)
                    {
                        oCurCell.InnerXml = xEmptyCellXml;
                    }
                    else
                    {
                        oCurCell.InnerXml = xCellXml;
                        if (iCurLabel > iStartLabel)
                        {
                            List<SdtElement> oChildCCs = oCurCell.Descendants<SdtElement>().ToList();
                            oChildCCs.Reverse();
                            foreach (SdtElement e in oChildCCs)
                            {
                                Query.ReTagContentControl(e, oDoc);
                            }
                        }
                    }
                }
            }
            if (iPagesNeeded > 1)
            {
                foreach (TableRow oRow in oRows)
                {
                    oRowsXml.Add(oRow.OuterXml);
                }
                for (int p = 2; p <= iPagesNeeded; p++)
                {
                    for (int r = 0; r < oRowsXml.Count; r++)
                    {
                        TableRow oNewRow = new TableRow(oRowsXml[r]);
                        List<SdtElement> oChildCCs = oNewRow.Descendants<SdtElement>().ToList();
                        oChildCCs.Reverse();
                        foreach (SdtElement e in oChildCCs)
                        {
                            Query.ReTagContentControl(e, oDoc);
                        }
                        oTbl.AppendChild<TableRow>(oNewRow);
                    }
                }
                oRows = oTbl.Elements<TableRow>().ToArray();
                int iCurCell = 0;
                if (iStartPos > 1)
                {
                    for (int r = 0; r < oRows.GetLength(0); r += iRowCounter)
                    {
                        TableRow oCurRow = oRows[r];
                        TableCell[] oCells = oCurRow.Elements<TableCell>().ToArray();
                        for (int c = 0; c < oCells.GetLength(0); c += iColCounter)
                        {
                            iCurCell++;
                            if (iCurCell < iStartPos)
                            {
                                oCells[c].InnerXml = xEmptyCellXml;
                            }
                            else
                                break;
                        }
                    }
                }
                iCurCell = iPagesNeeded * iLabelsPerPage;
                for (int r = oRows.GetLength(0) - 1; r >= 0; r -= iRowCounter)
                {
                    TableRow oCurRow = oRows[r];
                    TableCell[] oCells = oCurRow.Elements<TableCell>().ToArray();
                    for (int c = oCells.GetLength(0) - 1; c >= 0;  c -= iColCounter)
                    {
                        if (iCurCell > iLastLabel)
                        {
                            oCells[c].InnerXml = xEmptyCellXml;
                            iCurCell--;
                        }
                        else
                        {
                            break;
                        }
                    }

                }
            }

            return true;
        }
        /// <summary>
        /// Setup table with appropriate number of rows xfor Detail variables
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="oDoc"></param>
        /// <param name="iDetailCount"></param>
        /// <param name="iStartingPos"></param>
        /// <param name="bFullRow"></param>
        /// <param name="iHeaderRows"></param>
        /// <param name="iMinRows"></param>
        /// <param name="iMaxRows"></param>
        /// <param name="iTableThreshold"></param>
        /// <returns></returns>
        public bool SetupDistributedDetailTable(SdtElement oCC, WordprocessingDocument oDoc, int iDetailCount, int iStartingPos, bool bFullRow, int iHeaderRows, int iMinRows, int iMaxRows, int iTableThreshold)
        {
            Table oTbl = null;
            TableRow oRow = null;
            TableRow oNewRow = null;
            int iRows = 0;
            int iCols = 0;
            int iRowsNeeded = 0;
            bool bContentControlsChanged = false;

            oTbl = oCC.Ancestors<Table>().FirstOrDefault<Table>();

            if (oTbl == null)
            {
                if (iTableThreshold < 0)
                {
                    throw new LMP.Exceptions.XMLException(LMP.Resources.GetLangString("NoTableInXml"));
                }
                else if (iTableThreshold > 0 && iDetailCount >= iTableThreshold)
                {
                    //Insert full width 2-column table
                    string xCCXml = oCC.OuterXml;
                    string xParaPropXml = "";
                    ParagraphProperties oPPr = oCC.Descendants<ParagraphProperties>().FirstOrDefault<ParagraphProperties>();
                    if (oPPr != null)
                    {
                        //Use existing Paragraph format for empty cells
                        xParaPropXml = oPPr.OuterXml;
                    }
                    Table oNewTbl = new Table(new TableProperties(new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct },
                        new TableCellMarginDefault(new LeftMargin() { Width = "0", Type = TableWidthUnitValues.Dxa }, new RightMargin() { Width = "0", Type = TableWidthUnitValues.Dxa })),
                        new TableRow(new TableCell(new TableCellProperties(new TableCellWidth() { Width = "2500", Type = TableWidthUnitValues.Pct })),
                        new TableCell(new TableCellProperties(new TableCellWidth() { Width = "2500", Type = TableWidthUnitValues.Pct }), new Paragraph(new ParagraphProperties(xParaPropXml)))));
                    oTbl = oCC.InsertAfterSelf<Table>(oNewTbl);
                    //Remove origional CC
                    oCC.Remove();
                    TableCell oCell = oTbl.Descendants<TableCell>().First<TableCell>();
                    //Recreate oCC within first table cell
                    oCC = (SdtElement)oCell.AppendChild<SdtBlock>(new SdtBlock(xCCXml));
                }
                else
                {
                    //No table needed below threshold
                    return false;
                }
            }
            else if (iTableThreshold > 0 && iDetailCount < iTableThreshold) //GLOG 15847
            {
                //Delete existing table
                string xCCXml = oCC.OuterXml;
                oCC = oTbl.InsertBeforeSelf<SdtBlock>(new SdtBlock(xCCXml));
                TableCell[] oCells = oTbl.Descendants<TableCell>().ToArray<TableCell>();
                for (int i = 1; i < oCells.Count<TableCell>(); i++)
                {
                    //Delete associated document variables for CCs after first
                    Query.DeleteAssociatedDocVars(oDoc, oCells[i]);
                }
                oTbl.Remove();
                return true;
            }
            if (iStartingPos < 1)
                iStartingPos = 1;
            if (iHeaderRows < 0)
                iHeaderRows = 0;
            if (iDetailCount < 1)
                iDetailCount = 1;
            if (iMinRows < iMaxRows)
                iMinRows = iMaxRows;
            if (bFullRow)
            {
                oTbl = oCC.Ancestors<Table>().First<Table>();
                iRows = oTbl.Elements<TableRow>().Count<TableRow>();

                if (iDetailCount > iMaxRows && iMaxRows > 0)
                    iDetailCount = iMaxRows;

                iRowsNeeded = Math.Max(iDetailCount, iMinRows) + iHeaderRows;


                if (iRowsNeeded < iRows)
                {
                    //Delete extra rows
                    TableRow[] oRows = oTbl.Elements<TableRow>().ToArray<TableRow>();
                    for (int i = 1; i <= iRows - iRowsNeeded; i++)
                    {
                        oRow = oTbl.Elements<TableRow>().Last<TableRow>();
                        Query.DeleteAssociatedDocVars(oDoc, oRow);
                        oRow.Remove();
                        bContentControlsChanged = true;
                    }
                }
                else if (iRowsNeeded > iRows)
                {
                    //Save and delete unrelated mDels in Target CC
                    List<string> omDels = DeleteScope.GetChildmDels(oDoc, oCC);
                    for (int i = 1; i <= iRowsNeeded - iRows; i++) //GLOG 8477
                    {
                        oRow = oCC.Ancestors<TableRow>().FirstOrDefault<TableRow>();
                        oNewRow = (TableRow)oRow.Clone();
                        //GLOG 15805: Process Descendants in reverse order, to ensure CCs nested in other CCs 
                        //get their Tag replaced in the InnerXML of the parent CC
                        List<SdtElement> oChildCCs = oNewRow.Descendants<SdtElement>().ToList();
                        oChildCCs.Reverse();
                        foreach (SdtElement c in oChildCCs)
                        {
                            if (Query.GetBaseName(c) == "mVar" || Query.GetBaseName(c) == "mDel")
                            {
                                Query.ReTagContentControl(c, oDoc);
                            }
                        }
                        oTbl.AppendChild<TableRow>(oNewRow);
                        bContentControlsChanged = true;
                    }
                    //Restore saved mDels to Target CC
                    if (omDels != null)
                    {
                        DeleteScope.RestoreChildmDels(oDoc, oCC, omDels);
                    }
                }
            }
            else
            {
                iRows = oTbl.Elements<TableRow>().Count<TableRow>();
                oRow = oTbl.Elements<TableRow>().First<TableRow>();
                iCols = oRow.Elements<TableCell>().Count<TableCell>();

                //One cell per detail
                //GLOG 8702: Need to make sure result of division is not truncated to integer
                int iTotalRows = (int)Math.Ceiling(((Double)(iDetailCount + (iStartingPos - 1)) / iCols)) + iHeaderRows;
                iRowsNeeded = iTotalRows - iRows;
                if (iRowsNeeded < 0)
                {
                    //Delete extra rows
                    TableRow[] oRows = (TableRow[])oTbl.Elements<TableRow>();
                    for (int i = 1; i <= Math.Abs(iRowsNeeded); i++)
                    {
                        oRow = oTbl.Elements<TableRow>().Last<TableRow>();
                        Query.DeleteAssociatedDocVars(oDoc, oRow);
                        oRow.Remove();
                        bContentControlsChanged = true;
                    }
                }
                else
                {
                    oRow = oCC.Ancestors<TableRow>().First<TableRow>();
                    TableCell oCell = oCC.Ancestors<TableCell>().First<TableCell>();
                    //Save and delete unrelated mDels in Target CC
                    //GLOG 15804: Don't remove mDels that are native to this table cell,
                    //since these will need to be copied to additional cells
                    List<string> omDels = DeleteScope.GetChildmDels(oDoc, oCC, 0, true, true, "");
                    string xCellXml = oCell.InnerXml;
                    int iStartCell = (iHeaderRows * iCols) + iStartingPos;
                    int iEndCell = (iStartCell + iDetailCount) - 1;

                    //Append needed rows
                    for (int i = 1; i <= iRowsNeeded; i++)
                    {
                        oNewRow = (TableRow)oRow.Clone();
                        //GLOG 15805: Process Descendants in reverse order, to ensure CCs nested in other CCs 
                        //get their Tag replaced in the InnerXML of the parent CC
                        List<SdtElement> oChildCCs = oNewRow.Descendants<SdtElement>().ToList();
                        oChildCCs.Reverse();
                        foreach (SdtElement oElement in oChildCCs)
                        {
                            if (Query.GetBaseName(oElement) == "mVar" || Query.GetBaseName(oElement) == "mDel")
                            {
                                Query.ReTagContentControl(oElement, oDoc);
                            }
                        }
                        oTbl.AppendChild<TableRow>(oNewRow);
                        bContentControlsChanged = true;
                    }


                    TableCell[] oCells = oTbl.Descendants<TableCell>().ToArray<TableCell>();

                    for (int c = iHeaderRows * iCols; c < oCells.Count<TableCell>(); c++)
                    {
                        if (oCells[c].Descendants<SdtElement>().Count<SdtElement>() == 0 && c >= iStartCell - 1 && c < iEndCell)
                        {
                            //Populate empty Detail cells
                            oCells[c].InnerXml = xCellXml;

                            //GLOG 15805: Process Descendants in reverse order, to ensure CCs nested in other CCs 
                            //get their Tag replaced in the InnerXML of the parent CC
                            List<SdtElement> oChildCCs = oCells[c].Descendants<SdtElement>().ToList();
                            oChildCCs.Reverse();
                            foreach (SdtElement oElement in oChildCCs)
                            {
                                if (Query.GetBaseName(oElement) == "mVar" || Query.GetBaseName(oElement) == "mDel")
                                {
                                    Query.ReTagContentControl(oElement, oDoc);
                                    bContentControlsChanged = true;
                                }
                            }
                        }
                        else if (oCells[c].Descendants<SdtElement>().Count<SdtElement>() > 0 && (c < iStartCell - 1 || c > iEndCell))
                        {
                            //Clear Detail cells that are outside of start and end bounds
                            SdtElement[] oChildCCs = oCells[c].Descendants<SdtElement>().ToArray<SdtElement>();
                            for (int i = oChildCCs.Count() - 1; i >= 0; i--)
                            {
                                SdtElement oChildCC = oChildCCs[i];
                                Query.DeleteAssociatedDocVars(oDoc, oChildCC);
                                oChildCC.Remove();
                                bContentControlsChanged = true;
                            }
                        }
                    }
                    //Restore saved mDels to Target CC
                    if (omDels != null)
                    {
                        DeleteScope.RestoreChildmDels(oDoc, oCC, omDels);
                    }
                    string x = oTbl.InnerXml;
                }
            }
            return bContentControlsChanged;
        }
        /// <summary>
        /// Copies all or specified styles defined in source XML
        /// </summary>
        /// <param name="xBaseXML"></param>
        /// <param name="aStyleList"></param>
        public void UpdateStylesFromXML(string xBaseXML)
        {
            UpdateStylesFromXML(xBaseXML, null, this.WPDocument, true, false);
            UpdateStylesFromXML(xBaseXML, null, this.WPDocument, false, false);
        }
        public void UpdateStylesFromXML(string xBaseXML, string[] aStyleList)
        {
            UpdateStylesFromXML(xBaseXML, aStyleList, this.WPDocument, true, false);
            UpdateStylesFromXML(xBaseXML, aStyleList, this.WPDocument, false, false);
        }
        /// <summary>
        /// Copies all or specified styles defined in source XML
        /// </summary>
        /// <param name="xBaseXML"></param>
        /// <param name="aStyleList"></param>
        public void UpdateStylesFromXML(string xBaseXML, WordprocessingDocument oTargetDocument)
        {
            UpdateStylesFromXML(xBaseXML, null, oTargetDocument, true, false);
            UpdateStylesFromXML(xBaseXML, null, oTargetDocument, false, false);
        }
        public void UpdateStylesFromXML(string xBaseXML, string[] aStyleList, WordprocessingDocument oTargetDocument, bool bEffectsStyles, bool bDocDefaults)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                System.Collections.ArrayList aStylesXML = new System.Collections.ArrayList();
                //Extract style nodes from XML
                //string xStyleXML = LMP.String.GetMinimalStyleDocumentXML(xBaseXML);
                XmlDocument oStyleDoc = new XmlDocument();
                oStyleDoc.LoadXml(xBaseXML);
                NameTable oNT = new NameTable();
                XmlNamespaceManager oMgr = new XmlNamespaceManager(oNT);
                oMgr.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                oMgr.AddNamespace("pkg", "http://schemas.microsoft.com/office/2006/xmlPackage");
                string xXPath = "";
                string xDocDefXPath = "";

                if (bEffectsStyles && oTargetDocument.MainDocumentPart.StylesWithEffectsPart == null)
                    //Nothing to do
                    return;
                else if (!bEffectsStyles && oTargetDocument.MainDocumentPart.StyleDefinitionsPart == null)
                    //Nothing to do
                    return;

                if (oStyleDoc.ChildNodes[0].LocalName == "styles")
                {
                    //xBaseXML is just the <styles> nodes
                    xXPath = @"descendant::w:style";
                    xDocDefXPath = @"descendant::w:docdefaults";
                }
                else
                {
                    //xBaseXML is full document XML
                    if (bEffectsStyles)
                    {
                        xXPath = @"descendant::w:styles/w:style[ancestor::pkg:part[@pkg:name='/word/stylesWithEffects.xml']]";
                        xDocDefXPath = @"descendant::w:styles/w:docDefaults[ancestor::pkg:part[@pkg:name='/word/stylesWithEffects.xml']]";
                    }
                    else
                    {
                        //xBaseXML is full document XML
                        xXPath = @"descendant::w:styles/w:style[ancestor::pkg:part[@pkg:name='/word/styles.xml']]";
                        xDocDefXPath = @"descendant::w:styles/w:docDefaults[ancestor::pkg:part[@pkg:name='/word/styles.xml']]";
                    }
                }
                //Get all style nodes from styles part
                XmlNodeList oSourceStyles = oStyleDoc.DocumentElement.SelectNodes(xXPath, oMgr);
                
                if (oSourceStyles == null)
                    return;

                XmlNode oDocDefaults = oStyleDoc.DocumentElement.SelectSingleNode(xDocDefXPath, oMgr);
                string xDocDefaults = "";
                if (aStyleList == null)
                {
                    //If copying all styles, copy DocDefaults also
                    if (oDocDefaults != null)
                        xDocDefaults = oDocDefaults.InnerXml;
                    //copy all styles in source XML
                    foreach (XmlNode oNode in oSourceStyles)
                    {
                        string xName = oNode.SelectSingleNode("child::w:name", oMgr).Attributes.GetNamedItem("w:val").Value;
                        aStylesXML.Add(new string[] {xName, oNode.OuterXml});
                    }
                }
                else
                {
                    //copy specific named styles
                    for (int i = 0; i <= aStyleList.GetUpperBound(0); i++)
                    {
                        XmlNode oNode = oStyleDoc.DocumentElement.SelectSingleNode(@"descendant::w:styles/w:style[child::w:name[@w:val='" + aStyleList[i] + "']]", oMgr);
                        if (oNode != null)
                        {
                            aStylesXML.Add(new string[] { aStyleList[i], oNode.OuterXml });
                        }
                    }
                }
                if (aStylesXML.Count == 0)
                    return;

                Styles oStyles = null;
                if (bEffectsStyles)
                {
                    oStyles = oTargetDocument.MainDocumentPart.StylesWithEffectsPart.Styles;
                }
                else
                {
                    oStyles = oTargetDocument.MainDocumentPart.StyleDefinitionsPart.Styles;
                }

                try
                {
                    if (bDocDefaults && xDocDefaults != "")
                        oStyles.DocDefaults.InnerXml = xDocDefaults;
                    for (int i = 0; i < aStylesXML.Count; i++)
                    {
                        string[] aCurSty = (string[])aStylesXML[i];
                        Style oNewStyle = new Style(aCurSty[1]);
                        Style oSty = oStyles.Elements<Style>().Where(s => s.StyleName.Val == aCurSty[0]).FirstOrDefault();
                        if (oSty != null)
                        {
                            oSty.InsertAfterSelf<Style>(oNewStyle);
                            //Remove existing style, to be replaced with new definition
                            oSty.Remove();
                        }
                        else
                        {
                            oStyles.AppendChild<Style>(oNewStyle);
                        }
                    }
                    oStyles.Save();
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.WordDocException(oE.Message);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordDocException(oE.Message);
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }

        }
        public void SetCheckbox(SdtElement oCC, string xChar, string xFont)
        {
            //Make sure CC is not showing placeholder
            Query.ClearPlaceholderText(oCC);
            Run oRun = oCC.Descendants<Run>().FirstOrDefault<Run>();
            //Clear any existing Runs and replace with dummy run
            if (oRun != null)
            {
                oRun = oRun.InsertBeforeSelf<Run>(new Run(new Text("TempText")));
                while (oRun.NextSibling<Run>() != null)
                {
                    oRun.NextSibling<Run>().Remove();
                }
            }
            else
            {
                BookmarkEnd oBmk = oCC.Descendants<BookmarkEnd>().LastOrDefault<BookmarkEnd>();
                if (oBmk != null)
                {
                    oRun = oBmk.InsertBeforeSelf<Run>(new Run(new Text("TempText")));
                }
            }
            string xRunOuterXml = "<w:r><w:t>TempText</w:t></w:r>";

            //Set font to be used
            RunProperties oRP = new RunProperties(new RunFonts() { Ascii = xFont, HighAnsi = xFont });
            //Full field consists of 3 runs, for begin, code and end
            Run oNewRun = new Run(new RunProperties(oRP.OuterXml), new Run(new FieldChar() { FieldCharType = FieldCharValues.Begin }, new Run(new RunProperties(oRP.OuterXml), new FieldCode(" MACROBUTTON zzmpToggleFormsCheckbox " + xChar) { Space = SpaceProcessingModeValues.Preserve }),
                new Run(new RunProperties(oRP.OuterXml),  new FieldChar() { FieldCharType = FieldCharValues.End })));

            string xInnerXml = oCC.InnerXml.Replace(xRunOuterXml, oNewRun.InnerXml);
            
            oCC.InnerXml = xInnerXml;

        }
        /// <summary>
        /// Populates list of data for all ImageParts in WPDoc
        /// </summary>
        /// <param name="oForteDoc"></param>
        /// <param name="oDoc"></param>
        public static void PopulateImageList(XmlForteDocument oForteDoc, WordprocessingDocument oDoc)
        {
            if (oDoc != null)
            {
                oForteDoc.ImageList = new List<ImageData>();
                ImagePart[] docImages = oDoc.MainDocumentPart.GetPartsOfType<ImagePart>().ToArray();
                foreach (ImagePart oPart in docImages)
                {
                    bool bNew = true;
                    ImageData oNewData = new ImageData(oPart);
                    for (int i = 0; i < oForteDoc.ImageList.Count; i++)
                    {
                        if (oForteDoc.ImageList[i].Compare(oNewData))
                        {
                            bNew = false;
                            break;
                        }
                    }
                    if (bNew)
                    {
                        oForteDoc.ImageList.Add(oNewData);
                    }
                }
                HeaderPart[] oHeaders = oDoc.MainDocumentPart.GetPartsOfType<HeaderPart>().ToArray();
                foreach (HeaderPart oHeader in oHeaders)
                {
                    ImagePart[] hImages = oHeader.GetPartsOfType<ImagePart>().ToArray();
                    foreach (ImagePart oPart in hImages)
                    {
                        bool bNew = true;
                        ImageData oNewData = new ImageData(oPart);
                        for (int i = 0; i < oForteDoc.ImageList.Count; i++)
                        {
                            if (oForteDoc.ImageList[i].Compare(oNewData))
                            {
                                bNew = false;
                                break;
                            }
                        }
                        if (bNew)
                        {
                            oForteDoc.ImageList.Add(oNewData);
                        }
                    }
                }
                FooterPart[] oFooters = oDoc.MainDocumentPart.GetPartsOfType<FooterPart>().ToArray();
                foreach (FooterPart oFooter in oFooters)
                {
                    ImagePart[] fImages = oFooter.GetPartsOfType<ImagePart>().ToArray();
                    foreach (ImagePart oPart in fImages)
                    {
                        bool bNew = true;
                        ImageData oNewData = new ImageData(oPart);
                        for (int i = 0; i < oForteDoc.ImageList.Count; i++)
                        {
                            if (oForteDoc.ImageList[i].Compare(oNewData))
                            {
                                bNew = false;
                                break;
                            }
                        }
                        if (bNew)
                        {
                            oForteDoc.ImageList.Add(oNewData);
                        }
                    }
                }
            }
        }
        public void CopySegmentStyles(XmlSegment oSegment, bool bCopyAllStyles, bool bCopyRequiredStyles, bool bCopyInUseStyles)
        {
            List<string> aStyles = new List<string>();
            string xDefXml = oSegment.Definition.XML;
            if (bCopyAllStyles)
            {
                this.UpdateStylesFromXML(xDefXml);
                return;
            }
            if (bCopyRequiredStyles && !string.IsNullOrEmpty(oSegment.RequiredStyles))
            {
                string[] aRequired = oSegment.RequiredStyles.Split(',');
                aStyles.AddRange(aRequired);
            }
            if (bCopyInUseStyles)
            {
                Styles oStyles = this.WPDocument.MainDocumentPart.GetPartsOfType<StylesPart>().First().Styles;
                string[] aStylesInUse = Query.GetStylesInUse(oSegment.WPDoc);
                foreach (string xSty in aStylesInUse)
                {
                    //Add styles that don't exist in current document
                    var oSty = oStyles.Descendants<Style>().Where(s => s.StyleName.Val == xSty).Any();
                    if (!oSty)
                        aStyles.Add(xSty);
                }
            }
            if (aStyles.Count > 0)
                this.UpdateStylesFromXML(xDefXml, aStyles.ToArray<string>());

        }
        protected internal static WordprocessingDocument GetWPDocument(string xWordOpenXml)
        {
            WordprocessingDocument oWPDoc = null;
            MemoryStream oStream = null;
            string xDocXml = LMP.Architect.Oxml.XmlSegment
                .GetOPCFromFlatXml(xWordOpenXml);

            //segment xml is now held as a Base64 string
            byte[] bytes = Convert.FromBase64String(xDocXml);
            oStream = new MemoryStream();
            oStream.Write(bytes, 0, bytes.Length);

            oWPDoc = WordprocessingDocument.Open(oStream, true);

            return oWPDoc;
        }
        internal void UpdateContentXml()
        {
            XmlSegment[] aSegments = new XmlSegment[this.Segments.Count];
            for (int i = 0; i < this.Segments.Count; i++)
            {
                aSegments[i] = this.Segments[i];
            }
            UpdateContentXml(aSegments);
        }
        internal void UpdateContentXml(XmlSegment[] oSegments)
        {
            DateTime t0 = DateTime.Now;
            m_xContentXML = "<mpNodes>" + GetContentXml(oSegments, 1, "") + "</mpNodes>";
            LMP.Benchmarks.Print(t0);
        }
        public string ContentXml
        {
            get
            {
                if (m_xContentXML == null)
                {
                    UpdateContentXml();
                }
                return m_xContentXML;
            }
        }
        private string GetContentXml(XmlSegment[] oSegments, int iStartIndex, string xParentIndex)
        {
            StringBuilder sbXML = new StringBuilder();
            try
            {
                int iSegIndex = iStartIndex;
                for (int i = 0; i < oSegments.GetLength(0); i++)
                {
                    StringBuilder sbTagless = new StringBuilder("");
                    StringBuilder sbDeleted = new StringBuilder("");
                    System.Collections.Hashtable oChildTags = new System.Collections.Hashtable();
                    XmlSegment oSegment = oSegments[i];
                    //Attributes that are the same for all parts of Segment
                    string xTagID = oSegment.TagID;
                    string xParentTagID = (oSegment.Parent != null) ? oSegment.Parent.TagID : "";
                    string xObjectData = Query.GetAttributeValue(oSegment.WPDoc, oSegment.RawSegmentParts[0].ContentControl, "ObjectData");
                    xObjectData = String.ReplaceXMLChars(xObjectData, true);
                    string xAuthors = Query.GetAttributeValue(oSegment.WPDoc, oSegment.RawSegmentParts[0].ContentControl, "Authors");
                    xAuthors = String.ReplaceXMLChars(xAuthors, true);

                    for (int v = 0; v < oSegment.Variables.Count; v++)
                    {
                        XmlVariable oVar = oSegment.Variables[v];
                        //JTS 11/9/16: Avoid multiple calls to AssociateContentControls
                        List<SdtElement> oCCs = oVar.AssociatedContentControls;
                        if (oCCs.Count == 0)
                        {
                            //GLOG 8469: Replace quotes in variable name with tokens
                            sbTagless  = sbTagless.Append(String.ReplaceXMLChars(oVar.Name, true) + "|");
                        }
                        else
                        {
                            if (oVar.TagType == TagTypes.Segment)
                            {
                                //GLOG 8469: Replace quotes in variable name with tokens
                                sbDeleted  = sbDeleted.Append(String.ReplaceXMLChars(oVar.Name, true) + "|");
                            }

                            foreach (SdtElement oCC in oCCs)
                            {
                                oChildTags.Add(Query.GetTag(oCC), oVar);
                            }
                        }
                    }
                    for (int b = 0; b < oSegment.Blocks.Count; b++)
                    {
                        XmlBlock oBlock = oSegment.Blocks[b];

                        oChildTags.Add(Query.GetTag(oBlock.AssociatedContentControl), oBlock);
                    }
                    string xPattern = "<mSEG TagID=\"{0}\" OldID=\"{0}\" ColIndex=\"{1}\" Destination=\"\" DefinedVariables=\"{2}\" " +
                            "ParentID=\"{3}\" ObjectData=\"{4}\" Part=\"{5}\" Authors=\"{6}\" AssociatedParentVariable=\"\" TagValue=\"{7}\"/>";
                    for (int s = 0; s < oSegment.RawSegmentParts.Count; s++)
                    {
                        RawSegmentPart oPart = oSegment.RawSegmentParts[s];
                        SdtElement oSegCC = oSegment.RawSegmentParts[s].ContentControl;
                        StringBuilder xDefined = sbTagless;
                        if (s == 0)
                        {
                            xDefined =  xDefined.Append(sbDeleted);
                        }
                        string xTagValue = Query.GetTag(oSegCC);
                        string xPart = Query.GetAttributeValue(oSegment.WPDoc, oSegCC, "PartNumber");
                        string xColIndex = xParentIndex + (iSegIndex++).ToString();
                        sbXML = sbXML.AppendFormat(xPattern, xTagID, xColIndex, xDefined, xParentTagID, xObjectData, xPart, xAuthors, xTagValue);
                        if (s == 0)
                        {
                            int iSegmentCount = 1;
                            for (int c = 0; c < oSegment.Segments.Count; c++)
                            {
                                sbXML = sbXML.Append(GetContentXml(new XmlSegment[] {oSegment.Segments[c]}, iSegmentCount++, xColIndex + "."));
                            }
                        }
                        Tag[] oChildren = oSegCC.Descendants<Tag>().ToArray<Tag>();
                        int iVarIndex = 1;
                        foreach (Tag oTag in oChildren)
                        {
                            //GLOG 8698: Skip children contained in TextBox mSEG anchored to top-level mSEG
                            if (oTag.Ancestors<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mps")).First() != oSegCC)
                            {
                                continue;
                            }
                            string xCCTag = oTag.Val.Value;
                            if (oChildTags.ContainsKey(xCCTag))
                            {
                                SdtElement oVarCC = oTag.Ancestors<SdtElement>().First();
                                XmlVariable oVar = oChildTags[xCCTag] as XmlVariable;
                                XmlBlock oBlock = oChildTags[xCCTag] as XmlBlock;
                                string xBaseName = Query.GetBaseName(oVarCC);
                                string xVarObjectData = Query.GetAttributeValue(oSegment.WPDoc, oVarCC, "ObjectData");
                                xVarObjectData = String.ReplaceXMLChars(xVarObjectData, true);
                                string xVarPattern = "<{0} TagID=\"{1}\" OldID=\"{1}\" ColIndex=\"{2}\" Destination=\"\" Value=\"{3}\" ParentID=\"{4}\" ObjectData=\"{5}\" TagValue=\"{6}\" />";
                                string xValue = "";
                                string xName = "";
                                if (oVar != null)
                                {
                                    //GLOG 8469: Replace quotes in variable name with tokens
                                    xName = String.ReplaceXMLChars(oVar.Name, true);
                                    xValue = String.ReplaceXMLChars(oVar.Value.ToUpper(), true);
                                }
                                else if (oBlock != null)
                                {
                                    xValue = "";
                                    //GLOG 8469: Replace quotes in variable name with tokens
                                    xName = String.ReplaceXMLChars(oBlock.Name, true);
                                }
                                sbXML = sbXML.AppendFormat(xVarPattern, xBaseName, xName, xColIndex + "." + (iVarIndex++).ToString(), xValue, oSegment.FullTagID, xVarObjectData, xCCTag);

                            }
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(oE.Message);
            }
            finally
            {
            }
            return sbXML.ToString().Replace("\v", "\r\n");
        }
        /// <summary>
        /// GLOG 8824: Add documentProtection to WPDoc
        /// </summary>
        /// <param name="oWPDoc"></param>
        /// <param name="xPassword"></param>
        /// <param name="iProtectType"></param>
        public static void ApplyDocumentProtection(WordprocessingDocument oWPDoc, string xPassword, DocumentProtectionValues iProtectType)
        {
            int[] InitialCodeArray = { 0xE1F0, 0x1D0F, 0xCC9C, 0x84C0, 0x110C, 0x0E10, 0xF1CE, 0x313E, 0x1872, 0xE139, 0xD40F, 0x84F9, 0x280C, 0xA96A, 0x4EC3 };
            int[,] EncryptionMatrix = new int[15, 7] {
                /* char 1  */ {0xAEFC, 0x4DD9, 0x9BB2, 0x2745, 0x4E8A, 0x9D14, 0x2A09},
                /* char 2  */ {0x7B61, 0xF6C2, 0xFDA5, 0xEB6B, 0xC6F7, 0x9DCF, 0x2BBF},
                /* char 3  */ {0x4563, 0x8AC6, 0x05AD, 0x0B5A, 0x16B4, 0x2D68, 0x5AD0},
                /* char 4  */ {0x0375, 0x06EA, 0x0DD4, 0x1BA8, 0x3750, 0x6EA0, 0xDD40},
                /* char 5  */ {0xD849, 0xA0B3, 0x5147, 0xA28E, 0x553D, 0xAA7A, 0x44D5},
                /* char 6  */ {0x6F45, 0xDE8A, 0xAD35, 0x4A4B, 0x9496, 0x390D, 0x721A},
                /* char 7  */ {0xEB23, 0xC667, 0x9CEF, 0x29FF, 0x53FE, 0xA7FC, 0x5FD9},
                /* char 8  */ {0x47D3, 0x8FA6, 0x0F6D, 0x1EDA, 0x3DB4, 0x7B68, 0xF6D0},
                /* char 9  */ {0xB861, 0x60E3, 0xC1C6, 0x93AD, 0x377B, 0x6EF6, 0xDDEC},
                /* char 10 */ {0x45A0, 0x8B40, 0x06A1, 0x0D42, 0x1A84, 0x3508, 0x6A10},
                /* char 11 */ {0xAA51, 0x4483, 0x8906, 0x022D, 0x045A, 0x08B4, 0x1168},
                /* char 12 */ {0x76B4, 0xED68, 0xCAF1, 0x85C3, 0x1BA7, 0x374E, 0x6E9C},
                /* char 13 */ {0x3730, 0x6E60, 0xDCC0, 0xA9A1, 0x4363, 0x86C6, 0x1DAD},
                /* char 14 */ {0x3331, 0x6662, 0xCCC4, 0x89A9, 0x0373, 0x06E6, 0x0DCC},
                /* char 15 */ {0x1021, 0x2042, 0x4084, 0x8108, 0x1231, 0x2462, 0x48C4}
                };

            // Iterations specifies the number of times the hashing function shall be iteratively run (using each
            // iteration's result as the input for the next iteration).
            int iterations = 100000;

            // Generate the Salt
            byte[] arrSalt = new byte[16];
            RandomNumberGenerator rand = new RNGCryptoServiceProvider();
            rand.GetNonZeroBytes(arrSalt);

            //Array to hold Key Values
            byte[] generatedKey = new byte[4];

            //Maximum length of the password is 15 chars.
            int intMaxPasswordLength = 15;


            if (!string.IsNullOrEmpty(xPassword))
            {
                // Truncate the password to 15 characters
                xPassword = xPassword.Substring(0, Math.Min(xPassword.Length, intMaxPasswordLength));

                // Construct a new NULL-terminated string consisting of single-byte characters:
                //  -- > Get the single-byte values by iterating through the Unicode characters of the truncated Password.
                //   --> For each character, if the low byte is not equal to 0, take it. Otherwise, take the high byte.

                byte[] arrByteChars = new byte[xPassword.Length];

                for (int intLoop = 0; intLoop < xPassword.Length; intLoop++)
                {
                    int intTemp = Convert.ToInt32(xPassword[intLoop]);
                    arrByteChars[intLoop] = Convert.ToByte(intTemp & 0x00FF);
                    if (arrByteChars[intLoop] == 0)
                        arrByteChars[intLoop] = Convert.ToByte((intTemp & 0xFF00) >> 8);
                }

                // Compute the high-order word of the new key:

                // --> Initialize from the initial code array (see below), depending on the strPassword�s length. 
                int intHighOrderWord = InitialCodeArray[arrByteChars.Length - 1];

                // --> For each character in the strPassword:
                //      --> For every bit in the character, starting with the least significant and progressing to (but excluding) 
                //          the most significant, if the bit is set, XOR the key�s high-order word with the corresponding word from 
                //          the Encryption Matrix

                for (int intLoop = 0; intLoop < arrByteChars.Length; intLoop++)
                {
                    int tmp = intMaxPasswordLength - arrByteChars.Length + intLoop;
                    for (int intBit = 0; intBit < 7; intBit++)
                    {
                        if ((arrByteChars[intLoop] & (0x0001 << intBit)) != 0)
                        {
                            intHighOrderWord ^= EncryptionMatrix[tmp, intBit];
                        }
                    }
                }

                // Compute the low-order word of the new key:

                // Initialize with 0
                int intLowOrderWord = 0;

                // For each character in the strPassword, going backwards
                for (int intLoopChar = arrByteChars.Length - 1; intLoopChar >= 0; intLoopChar--)
                {
                    // low-order word = (((low-order word SHR 14) AND 0x0001) OR (low-order word SHL 1) AND 0x7FFF)) XOR character
                    intLowOrderWord = (((intLowOrderWord >> 14) & 0x0001) | ((intLowOrderWord << 1) & 0x7FFF)) ^ arrByteChars[intLoopChar];
                }

                // Lastly,low-order word = (((low-order word SHR 14) AND 0x0001) OR (low-order word SHL 1) AND 0x7FFF)) XOR strPassword length XOR 0xCE4B.
                intLowOrderWord = (((intLowOrderWord >> 14) & 0x0001) | ((intLowOrderWord << 1) & 0x7FFF)) ^ arrByteChars.Length ^ 0xCE4B;

                // Combine the Low and High Order Word
                int intCombinedkey = (intHighOrderWord << 16) + intLowOrderWord;

                // The byte order of the result shall be reversed [Example: 0x64CEED7E becomes 7EEDCE64. end example],
                // and that value shall be hashed as defined by the attribute values.

                for (int intTemp = 0; intTemp < 4; intTemp++)
                {
                    generatedKey[intTemp] = Convert.ToByte(((uint)(intCombinedkey & (0x000000FF << (intTemp * 8)))) >> (intTemp * 8));
                }
                // Implementation Notes List:
                // --> In this third stage, the reversed byte order legacy hash from the second stage shall be converted to Unicode hex 
                // --> string representation 
                StringBuilder sb = new StringBuilder();
                for (int intTemp = 0; intTemp < 4; intTemp++)
                {
                    sb.Append(Convert.ToString(generatedKey[intTemp], 16));
                }
                generatedKey = Encoding.Unicode.GetBytes(sb.ToString().ToUpper());

                // Implementation Notes List:
                //Word appends the binary form of the salt attribute and not the base64 string representation when hashing
                // Before calculating the initial hash, you are supposed to prepend (not append) the salt to the key
                byte[] tmpArray1 = generatedKey;
                byte[] tmpArray2 = arrSalt;
                byte[] tempKey = new byte[tmpArray1.Length + tmpArray2.Length];
                Buffer.BlockCopy(tmpArray2, 0, tempKey, 0, tmpArray2.Length);
                Buffer.BlockCopy(tmpArray1, 0, tempKey, tmpArray2.Length, tmpArray1.Length);
                generatedKey = tempKey;

                // Implementation Notes List:
                //Word requires that the initial hash of the password with the salt not be considered in the count.
                //    The initial hash of salt + key is not included in the iteration count.
                HashAlgorithm sha512 = new SHA512Managed();
                generatedKey = sha512.ComputeHash(generatedKey);
                byte[] iterator = new byte[4];
                for (int intTmp = 0; intTmp < iterations; intTmp++)
                {

                    //When iterating on the hash, you are supposed to append the current iteration number.
                    iterator[0] = Convert.ToByte((intTmp & 0x000000FF) >> 0);
                    iterator[1] = Convert.ToByte((intTmp & 0x0000FF00) >> 8);
                    iterator[2] = Convert.ToByte((intTmp & 0x00FF0000) >> 16);
                    iterator[3] = Convert.ToByte((intTmp & 0xFF000000) >> 24);

                    generatedKey = concatByteArrays(iterator, generatedKey);
                    generatedKey = sha512.ComputeHash(generatedKey);
                }
            }

            DocumentProtection oExistingProtection = oWPDoc.MainDocumentPart.DocumentSettingsPart.Settings.Elements<DocumentProtection>().FirstOrDefault();

            // Apply the element
            DocumentProtection oNewProtection = new DocumentProtection();
            oNewProtection.Edit = iProtectType;

            OnOffValue docProtection = new OnOffValue(true);
            oNewProtection.Enforcement = docProtection;
            //If no password, only edit and enforcement members needed
            if (!string.IsNullOrEmpty(xPassword))
            {
                oNewProtection.CryptographicAlgorithmClass = CryptAlgorithmClassValues.Hash;
                oNewProtection.CryptographicProviderType = CryptProviderValues.RsaAdvancedEncryptionStandard;
                oNewProtection.CryptographicAlgorithmType = CryptAlgorithmValues.TypeAny;
                oNewProtection.CryptographicAlgorithmSid = 14; // SHA512
                //    The iteration count is unsigned
                UInt32Value uintVal = new UInt32Value();
                uintVal.Value = (uint)iterations;
                oNewProtection.CryptographicSpinCount = uintVal;
                oNewProtection.Hash = Convert.ToBase64String(generatedKey);
                oNewProtection.Salt = Convert.ToBase64String(arrSalt);
            }
            if (oExistingProtection != null)
            {
                //Replace existing child with new object
                oExistingProtection.Remove();
            }
            oWPDoc.MainDocumentPart.DocumentSettingsPart.Settings.AppendChild(oNewProtection);
            oWPDoc.MainDocumentPart.DocumentSettingsPart.Settings.Save();
        }
        private static byte[] concatByteArrays(byte[] array1, byte[] array2)
        {
            byte[] result = new byte[array1.Length + array2.Length];
            Buffer.BlockCopy(array2, 0, result, 0, array2.Length);
            Buffer.BlockCopy(array1, 0, result, array2.Length, array1.Length);
            return result;
        }
        public static void FinishStaticSegment(XmlSegment oSegment)
        {
            foreach (RawSegmentPart oPart in oSegment.RawSegmentParts)
            {
                SdtElement omSEG = oPart.ContentControl;
                Query.DeleteSegmentBookmarks(oSegment.WPDoc);
                List<SdtElement> oChildCCs = omSEG.Descendants<SdtElement>().ToList();
                oChildCCs.Reverse();
                foreach (SdtElement oCC in oChildCCs)
                {
                    if (Query.GetBaseName(oCC) != "")
                    {
                        string xc = oCC.InnerXml;
                        OpenXmlElement oContent = oCC.Descendants().Where(c => c.LocalName == "sdtContent").FirstOrDefault();
                        if (oContent != null)
                        {
                            foreach (OpenXmlElement oElement in oContent)
                            {
                                if (oElement is BookmarkEnd || oElement is BookmarkStart)
                                    continue;
                                OpenXmlElement oNewElement = (OpenXmlElement)oElement.Clone();
                                oCC.Parent.InsertBefore(oNewElement, oCC);
                            }
                        }
                        oCC.Remove();
                    }
                }
            }
            Query.DeleteSegmentContentControls(oSegment.WPDoc);
            Settings oSettings = oSegment.WPDoc.MainDocumentPart.DocumentSettingsPart.Settings;
            DocumentVariables oDocVars = oSettings.Descendants<DocumentVariables>().FirstOrDefault();
            if (oDocVars != null)
            {
                oDocVars.RemoveAllChildren<DocumentVariable>();
                oSettings.Save();
            }
            //Perform any type-specific actions
            oSegment.ExecutePostFinish();
        }
        #endregion
        #region *********************event handlers*********************
        void m_oSegments_SegmentAdded(object sender, SegmentEventArgs oArgs)
        {
            //re-raise event
            XmlSegment oSeg = oArgs.Segment;
            if (this.SegmentAdded != null)
                this.SegmentAdded(this, new SegmentEventArgs(oSeg));
        }
        void m_oSegments_SegmentDeleted(object sender, SegmentEventArgs oArgs)
        {
            //re-raise event
            XmlSegment oSeg = oArgs.Segment;
            if (this.SegmentDeleted != null)
                this.SegmentDeleted(this, new SegmentEventArgs(oSeg));
        }
        #endregion
    }
    // JTS: adapted from class in OpenXMLPowerTools
    // This class is used to prevent duplication of images
    public class ImageData
    {
        internal string ContentType { get; set; }
        private byte[] Image { get; set; }
        internal OpenXmlPart ImagePart { get; set; }
        internal string Target { get; set; }

        public ImageData(ImagePart part)
        {
            ContentType = part.ContentType;
            Target = part.Uri.OriginalString.Replace("/word/", "");
            ImagePart = part;

            using (Stream s = part.GetStream(FileMode.Open, FileAccess.Read))
            {
                Image = new byte[s.Length];
                s.Read(Image, 0, (int)s.Length);
            }
        }

        public bool Compare(string xContentType, string xImageXML)
        {
            if (ContentType != xContentType)
                return false;

            byte[] aBytes = Convert.FromBase64String(xImageXML.Replace("_x000d__x000a_", "\n"));
            if (Image.GetLongLength(0) != aBytes.GetLongLength(0))
                return false;
            // Compare the arrays byte by byte
            long length = Image.GetLongLength(0);
            for (long n = 0; n < length; n++)
                if (Image[n] != aBytes[n])
                    return false;
            return true;
        }
        public bool Compare(ImageData arg)
        {
            if (ContentType != arg.ContentType)
                return false;
            if (Image.GetLongLength(0) != arg.Image.GetLongLength(0))
                return false;
            // Compare the arrays byte by byte
            long length = Image.GetLongLength(0);
            byte[] image1 = Image;
            byte[] image2 = arg.Image;
            for (long n = 0; n < length; n++)
                if (image1[n] != image2[n])
                    return false;
            return true;
        }
    }
}
