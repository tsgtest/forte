using System;
using LMP.Data;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Reflection;
using LMP.Controls;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using System.Linq;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.Architect.Oxml
{
    public class XmlUserVariable: XmlVariable
	{
        internal XmlUserVariable(XmlSegment oParent) : base(oParent)
        {
            //get new GUID as ID -
            //user variable IDs are prefixed with a 'U'
            this.ID = "U" + System.Guid.NewGuid().ToString();
            this.IsDirty = false;
        }
        internal XmlUserVariable(XmlForteDocument oParent) : base(oParent)
        {
            //get new GUID as ID -
            //user variable IDs are prefixed with a 'U'
            this.ID = "U" + System.Guid.NewGuid().ToString();
            this.IsDirty = false;
        }
    }

	public class XmlAdminVariable: XmlVariable
	{
        internal XmlAdminVariable(XmlSegment oParent) : base(oParent)
        {
            //get new GUID as ID
            this.ID = System.Guid.NewGuid().ToString();
            this.IsDirty = false;
        }
        internal XmlAdminVariable(XmlForteDocument oParent) : base(oParent)
        {
            //get new GUID as ID
            this.ID = System.Guid.NewGuid().ToString();
            this.IsDirty = false;
        }
    }

    /// <summary>
    /// contains properties that define the arguments for the VariableDeleted
    /// and VariableAdded events
    /// </summary>
    public class VariableEventArgs
    {
        private XmlVariable m_oVar;

        public VariableEventArgs(XmlVariable oVar)
        {
            m_oVar = oVar;
        }

        public XmlVariable Variable
        {
            get { return m_oVar; }
            set { m_oVar = value; }
        }
    }
    /// <summary>
    /// contains the properties to handle a request to update Contacts
    /// </summary>
    public class BeforeContactsUpdateEventArgs
    {
        public bool Cancel = false;
    }
    public delegate void BeforeContactsUpdateHandler(object sender, BeforeContactsUpdateEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a VariableDeleted event handler
    /// </summary>
    public delegate void VariableDeletedHandler(object sender, VariableEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a VariableAdded event handler
    /// </summary>
    public delegate void VariableAddedHandler(object sender, VariableEventArgs oArgs);

    /// <summary>
    /// declares the delegate for a VariableDefinitionChanged event handler
    /// </summary>
    public delegate void VariableDefinitionChangedHandler(object sender, VariableEventArgs oArgs);

	public delegate void ValueAssignedEventHandler(object sender, EventArgs e);
    public delegate void VariableVisitedHandler(object sender, VariableEventArgs e);

	/// <summary>
	/// contains the methods and properties that define a
	/// MacPac Variable - created by Daniel Fisherman 09/20/05
	/// </summary>
	public abstract class XmlVariable : LMP.Architect.Base.Variable
	{
        #region *********************fields*********************
        private XmlVariableActions m_oActions = null;
        private XmlControlActions m_oControlActions;
        private XmlSegment m_oSegment;
        private XmlForteDocument m_oForteDocument;
        //private XmlForteDocument.TagTypes m_bytTagType = XmlForteDocument.TagTypes.Variable;
        private XmlVariable m_oUISourceVariable = null;
        #endregion
        #region *********************events*********************
        public event ValueAssignedEventHandler ValueAssigned;
        public event VariableVisitedHandler VariableVisited;
        #endregion
        #region *********************constructors*********************
        internal XmlVariable(XmlSegment oParent)
        {
            m_oSegment = oParent;
            m_oForteDocument = m_oSegment.ForteDocument;
            m_oActions = new XmlVariableActions(this);
            m_oControlActions = new XmlControlActions(this);

            //subscribe to events
            m_oActions.ActionsDirtied += new LMP.Architect.Base.ActionsDirtiedEventHandler(
                m_oActions_ActionsDirtied);
            m_oControlActions.ActionsDirtied += new LMP.Architect.Base.ActionsDirtiedEventHandler(
                m_oControlActions_ActionsDirtied);
        }

        internal XmlVariable(XmlForteDocument oParent)
        {
            m_oForteDocument = oParent;
            m_oActions = new XmlVariableActions(this);
            m_oControlActions = new XmlControlActions(this);

            //subscribe to events
            m_oActions.ActionsDirtied += new LMP.Architect.Base.ActionsDirtiedEventHandler(
                m_oActions_ActionsDirtied);
            m_oControlActions.ActionsDirtied += new LMP.Architect.Base.ActionsDirtiedEventHandler(
                m_oControlActions_ActionsDirtied);
        }
        #endregion
		#region *********************properties*********************
        public string Value
		{
			get
			{
                if (m_xValue == null)
				{
                    //TODO: OpenXML rewrite
                    m_xValue = this.GetValueFromSource();
 
					SetValidationField();
				}

				return m_xValue;
			}
		}
		/// <summary>
		/// returns true iff the value satisfies the validation condition
		/// </summary>
		public bool HasValidValue
		{
            get
            {
                //test validation if necessary
                if (!this.m_bValidityTested)
                    SetValidationField();

                return m_bValueIsValid;
            }
		}
        /// <summary>
        /// returns true iff the variable is a standalone variable
        /// </summary>
        public bool IsStandalone
        {
            get { return this.Segment == null; }
        }
		public XmlVariableActions VariableActions
		{
			get{return m_oActions;}
		}		
		public XmlControlActions ControlActions
		{
            get{return m_oControlActions;}
		}
		public XmlSegment Segment
		{
			get{return m_oSegment;}
		}
        public XmlForteDocument ForteDocument
        {
            get
            {
                return this.Segment != null ?
                    m_oSegment.ForteDocument : m_oForteDocument;
            }
        }
        /// <summary>
        /// returns the variables collection to which this variable belongs
        /// </summary>
        public XmlVariables Parent
        {
            get
            {
                ////return the segment variables collection
                ////if this variable is not standalone, else
                ////the document's variables collection
                //if (this.IsStandalone)
                //    return this.ForteDocument.Variables;
                //else
                    return this.Segment.Variables;
            }
        }
        public XmlSegment AssociatedSegment
        {
            get 
            {
                ArrayList oSegArrayList = new ArrayList();

                //cycle through collection, finding all segments of the specified type
                for (int i = 0; i < this.Segment.Segments.Count; i++)
                {
                    XmlSegment oSegment = this.Segment.Segments[i];
                    //TODO: OpenXML rewrite
                    //if (oSegment.ParentVariableID == this.ID)
                    //    return oSegment;
                }
                return null;
            }
        }
        /// <summary>
        /// True if user is required to visit this variable in the UI
        /// </summary>
        public bool MustVisit
        {
            get { return m_bMustVisit; }
            set 
            {
                if (m_bMustVisit != value)
                {
                    m_bMustVisit = value;
                    this.m_bIsDirty = true;

                    //raise event if there are subscribers
                    if (!m_bMustVisit && this.VariableVisited != null)
                        this.VariableVisited(this, new VariableEventArgs(this));
                }
            }
        }
        /// <summary>
        /// gets the variable whose control directly sets/reflects the value of this variable
        /// </summary>
        public XmlVariable UISourceVariable
        {
            get
            {
                if (m_oUISourceVariable == null)
                {
                    //detail variables may be a subfield of a different variable's control -
                    //if so, the name of this variable will be in the Expression parameter
                    //of the InsertDetail variable action
                    if (((this.ValueSourceExpression == "[DetailValue]") ||
                        (this.ValueSourceExpression == "[DetailValue]" + "__" + this.Name)) &&
                        (this.ControlType != mpControlTypes.DetailGrid) &&
                        (this.ControlType != mpControlTypes.DetailList))
                    {
                        //get parameters of InsertDetail action
                        string xParameters = "";
                        for (int i = 0; i < this.VariableActions.Count; i++)
                        {
                            XmlVariableAction oAction = this.VariableActions[i];
                            if (oAction.Type == XmlVariableActions.Types.InsertDetail)
                            {
                                xParameters = oAction.Parameters;
                                break;
                            }
                        }

                        //if parameters found, check Expression for the name of a variable
                        //with a detail control
                        if (xParameters != "")
                        {
                            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);
                            if (aParams.GetUpperBound(0) > 6)
                            {
                                string xExpression = aParams[7];
                                int iPos = xExpression.IndexOf("[Variable_");
                                while (iPos > -1)
                                {
                                    iPos += 10;
                                    int iPos2 = xExpression.IndexOf("]", iPos);
                                    try
                                    {
                                        //JTS 12/19/08: Fieldcode could be [Variable__ or [Variable_,
                                        //so trim any starting '_' character
                                        m_oUISourceVariable = m_oSegment.Variables.ItemFromName(
                                            xExpression.Substring(iPos, iPos2 - iPos).TrimStart(@"_".ToCharArray()));
                                    }
                                    catch { }

                                    if ((m_oUISourceVariable != null) &&
                                        ((m_oUISourceVariable.ControlType == mpControlTypes.DetailGrid) ||
                                        (m_oUISourceVariable.ControlType == mpControlTypes.DetailList)))
                                        break;
                                    else
                                    {
                                        m_oUISourceVariable = null;
                                        iPos = xExpression.IndexOf("[Variable_", iPos2);
                                    }
                                }
                            }
                        }
                    }
                    //GLOG - 3255 - CEH
                    //this might not be the nicest way to implement...
                    else if (this.DefaultValue != null)
                    {
                        //get variable from Default Value
                        string xName = this.DefaultValue;
                        //GLOG 6719:  Don't attempt to parse if there is more than one Variable code referenced.
                        if (String.CountChrs(xName, "Variable_") == 1)
                        {

                            int iPos = 0;
                            iPos = xName.IndexOf("Variable_");

                            if (iPos > 0)
                            {
                                int iPos2 = -1;
                                int iPos3 = -1;
                                //GLOG 6719: Get last opening bracket before 'Variable_' text
                                //get opening bracket position before 'Variable_' string
                                if (xName.IndexOf("[") > -1)
                                {
                                    iPos2 = xName.Substring(0, iPos).LastIndexOf("[");
                                }
                                if (iPos2 > -1)
                                {
                                    //GLOG 6719: Get first closing bracket after 'Variable_' text
                                    //get closing bracket position following 'Variable_' string
                                    iPos3 = xName.IndexOf("]", iPos);
                                    if (iPos3 > -1)
                                    {

                                        //get string between brackets
                                        xName = xName.Substring(iPos2 + 1, (iPos3 - iPos2) - 1);

                                        //remove 'Variable_' strings
                                        xName = xName.Replace("Variable__", "");
                                        xName = xName.Replace("Variable_", "");

                                        //return Variable object
                                        //GLOG 6719: Leave null if error occurs
                                        try
                                        {
                                            m_oUISourceVariable = Segment.GetVariable(xName);
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                    }

                    if (m_oUISourceVariable == null)
                        m_oUISourceVariable = this;
                }

                return m_oUISourceVariable;
            }
        }
        /// <summary>
        /// Returns true if Variable is language-specific via Date format, Preference, Default Value or Variable Action
        /// </summary>
        public bool IsLanguageDependent
        {
            //GLOG 3528
            get
            {
                string xDef = this.ToString(true);
                //check variable actions for InsertDate action
                bool bDefinesInsertDateAction = false;
                for (int j = 0; j < this.VariableActions.Count; j++)
                {
                    if (this.VariableActions[j].Type == XmlVariableActions.Types.InsertDate)
                        bDefinesInsertDateAction = true;
                }

                return (bDefinesInsertDateAction ||
                    (XmlExpression.ContainsPreferenceCode(xDef) ||
                    (xDef.IndexOf("[SegmentLanguage") > -1)));
            }
        }
        /// <summary>
        /// Returns true if Default Value or Action Parameter contains Author fieldcode
        /// </summary>
        public bool IsAuthorRelated
        {
            get
            {
                //get default value - exit if empty
                string xExp = this.DefaultValue;
                if (string.IsNullOrEmpty(xExp))
                {
                    return false;
                }
                else if (XmlExpression.ContainsAuthorCode(xExp, -1, -2))
                {
                    return true;
                }
                else
                {
                    //check action parameters for related field code
                    for (int j = 0; j < this.VariableActions.Count; j++)
                    {
                        //get parameters
                        string xParam = ((XmlVariableAction)this.VariableActions[j]).Parameters;
                        if (XmlExpression.ContainsAuthorCode(xParam, -1, -2))
                        {
                            return true;
                        }
                        else if (this.VariableActions[j].Type == XmlVariableActions.Types.IncludeExcludeText &&
                            XmlExpression.ContainsAuthorCode(this.SecondaryDefaultValue, -1, -2))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// returns true if this variable has more than one tag
        /// </summary>
        public bool HasMultipleTags
        {
            get
            {
               return (this.AssociatedContentControls.Count > 1);
            }
        }

        /// <summary>
        /// returns true iff the variable is tagless
        /// </summary>
        public bool IsTagless
        {
            get
            {
               return this.AssociatedContentControls.Count == 0;
            }
        }

        public List<SdtElement> AssociatedContentControls
        {
            get
            {
                return RelatedContentControls(true);
            }
        }
        public List<SdtElement> ContainingContentControls
        {
            get
            {
                return RelatedContentControls(false);
            }
        }
        private List<SdtElement> RelatedContentControls(bool bAssociated)
        {
            DateTime t0 = DateTime.Now;

            List<SdtElement> oList = new List<SdtElement>();
            if (this.TagType == Base.ForteDocument.TagTypes.Segment)
            {
                //Tagless variable has no Associated Content Controls
                if (!bAssociated)
                {
                    oList.Add(this.m_oSegment.RawSegmentParts[0].ContentControl);
                }
            }
            else if (this.TagType == Base.ForteDocument.TagTypes.DeletedBlock)
            {
                string xVal = this.Name + "��";
                string xTag = "";

                //get settings part
                Settings oSettings = m_oSegment.WPDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;

                //JTS 11/2/16: reworked to avoid linq queries where possible
                //set query
                //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                //               where dv.Val.Value.StartsWith(xVal)
                //               select dv;
                DocumentVariable[] oDocVars = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Val.Value.StartsWith(xVal)).ToArray();

                foreach (DocumentVariable oDocVar in oDocVars)
                {
                    xTag = oDocVar.Name.Value.Substring(3);
                    foreach (RawSegmentPart oPart in m_oSegment.RawSegmentParts)
                    {
                        string xSegTag = oPart.ContentControl.Descendants<Tag>().First<Tag>().Val.Value;
                        //get variables corresponding to mDels
                        Tag[] aTags = oPart.ContentControl.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpd" + xTag)).ToArray();
                        //var omDels = from cc in oPart.ContentControl.Descendants<SdtElement>()
                        //             where (from pr in cc.Elements<SdtProperties>()
                        //                    where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpd" + xTag)).Any()
                        //                    select pr).Any()
                        //             select cc;
                        foreach (Tag oTag in aTags)
                        //if (omDels.Count() > 0 && Query.GetAttributeValue(m_oSegment.WPDoc, omDels.First<SdtElement>(), "ObjectData").Contains(m_oSegment.TagID))
                        {
                            SdtElement oCC = oTag.Ancestors<SdtElement>().First();
                            //GLOG 8484: Avoid error on non-Forte Content Controls
                            SdtElement oSegCC = oCC.Ancestors<SdtElement>().Where(s => Query.GetTag(s).StartsWith("mps")).FirstOrDefault();
                            //Only add to List if Content Control's mSEG parent is m_oSegment and not a child segment
                            if (oSegCC == oPart.ContentControl) // oSegCC != null && Query.GetTag(oSegCC) == xSegTag)
                            {
                                if (!bAssociated)
                                {
                                    //Return parent mSEG as Containing Content Control
                                    oList.Add(oPart.ContentControl);
                                    break;
                                }
                                else
                                {
                                    //Return mDel as Associated Content Control
                                    oList.Add(oCC);
                                    //oList.Add(omDels.First<SdtElement>());
                                }
                            }
                            break;
                        }
                    }
                }
            }
            else
            {
                string xVal = this.Name + "��";
                string xTag = "";

                //get settings part
                Settings oSettings = m_oSegment.WPDoc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First().Settings;

                //set query to selected related document variables
                //var oDocVars = from dv in oSettings.Descendants<DocumentVariable>()
                //               where dv.Val.Value.StartsWith(xVal)
                //               select dv;
                DocumentVariable[] oDocVars = oSettings.Descendants<DocumentVariable>().Where(dv => dv.Val.Value.StartsWith(xVal)).ToArray();
                //GLOG 8702: Document variables will be in a random order and the related tags may skip between locations.  
                //We need to make sure list of elements follows the order in the document, therefore we need to keep a Hashtable 
                //maintaining a sorted list for each RawSegmentPart
                Hashtable oPartsHash = new Hashtable();
                //get variables corresponding to mVars
                foreach (DocumentVariable oDocVar in oDocVars)
                {
                    xTag = oDocVar.Name.Value.Substring(3);
                    //Check which mSEG Content Control contains this mDel
                    foreach (RawSegmentPart oPart in m_oSegment.RawSegmentParts)
                    {
                        List<SdtElement> oPartList = null;
                        if (oPartsHash.ContainsKey(oPart))
                        {
                            oPartList = (List<SdtElement>)oPartsHash[oPart];
                        }
                        else
                        {
                            oPartList = new List<SdtElement>();
                            oPartsHash.Add(oPart, oPartList);
                        }
                        string xSegTag = oPart.ContentControl.Descendants<Tag>().First<Tag>().Val.Value;
                        //get mVar Content Control with matching Tag
                        Tag[] aTags = oPart.ContentControl.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpv" + xTag)).ToArray();

                        //var omVars = from cc in oPart.ContentControl.Descendants<SdtElement>()
                        //             where (from pr in cc.Elements<SdtProperties>()
                        //                    where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpv" + xTag)).Any()
                        //                    select pr).Any()
                        //             select cc;
                        foreach (Tag oTag in aTags)
                        {
                            SdtElement oCC = oTag.Ancestors<SdtElement>().First();
                            SdtElement oSegCC = oCC.Ancestors<SdtElement>().Where(s => Query.GetTag(s).StartsWith("mps")).FirstOrDefault();
                            //Only add to List if Content Control's mSEG parent is m_oSegment and not a child segment
                            if (oSegCC == oPart.ContentControl) // oSegCC != null && Query.GetTag(oSegCC) == xSegTag)
                            //if (omVars.First<SdtElement>().Ancestors<SdtElement>().Where<SdtElement>(s => s.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mps")).Any()).First<SdtElement>().Descendants<Tag>().First<Tag>().Val.Value == xSegTag)
                            {
                                int iIndex = oPartList.Count;
                                //GLOG 15834: This won't work for Headers and Footers.
                                //Instead entire list will be sorted by SortCCList below
                                ////GLOG 8702: Need to add these in order they appear in document
                                //for (int c = 0; c < oPartList.Count; c++)
                                //{
                                //    if (oCC.IsBefore(oPartList[c]))
                                //    {
                                //        iIndex = c;
                                //        break;
                                //    }
                                //}
                                oPartList.Insert(iIndex, oCC);
                            }
                        }
                    }
                }
                foreach (object oItem in oPartsHash.Values)
                {
                    oList.AddRange((List<SdtElement>)oItem);
                }
                oList = Query.SortCCList(oList, m_oSegment.WPDoc);
            }
            LMP.Benchmarks.Print(t0);
            return oList;
        }
        #endregion
		#region *********************methods*********************
		public string[] ToArray()
		{
			return ToArray(false);
		}

		/// <summary>
		/// returns the persistent values of the Variable
		/// as an array - the persistent values are those
		/// that are stored in the Word Tag. SegmentName,
		/// TagID, and Actions are added to the array at run time
		/// </summary>
		/// <param name="bPersistentValuesOnly"></param>
		/// <returns></returns>
		public string[] ToArray(bool bPersistentValuesOnly)
		{
			string[] aDef = null;
			int iOffset = 0;

			if(bPersistentValuesOnly)
			{
				aDef = new string[33];
			}
			else
			{
				aDef = new string[37];

				//populate the non-persistent indices
				aDef[0] = this.IsDirty.ToString();
				aDef[1] = this.SegmentName;
				aDef[2] = this.TagID;
				aDef[3] = this.TagParentPartNumbers;
				iOffset = 4;
			}

			aDef[iOffset] = this.ID;
			aDef[iOffset + 1] = this.Name;
			aDef[iOffset + 2] = this.DisplayName;
			aDef[iOffset + 3] = this.TranslationID.ToString();
			aDef[iOffset + 4] = this.ExecutionIndex.ToString();
			//there may be two values stored in DefaultValue field
			string xDefaultValue = this.DefaultValue;
			if (this.SecondaryDefaultValue != null)
				xDefaultValue = xDefaultValue + "�" + this.SecondaryDefaultValue;
			aDef[iOffset + 5] = xDefaultValue;
			aDef[iOffset + 6] = this.ValueSourceExpression;

            //GLOG 2355 - convert detail grid reserve value from xml to delimited
            //string to reduce size of ObjectData attribute in Word
            aDef[iOffset + 7] = this.DetailReserveValueToDelimitedString(this.ReserveValue);

			aDef[iOffset + 8] = this.Reserved;
			aDef[iOffset + 9] = ((byte) this.ControlType).ToString();
			aDef[iOffset + 10] = this.ControlProperties;
			aDef[iOffset + 11] = this.ValidationCondition;
			aDef[iOffset + 12] = this.ValidationResponse;
			aDef[iOffset + 13] = this.HelpText;
			aDef[iOffset + 14] = this.VariableActionsToString();
            aDef[iOffset + 15] = this.ControlActionsToString();
            aDef[iOffset + 16] = this.RequestCIForEntireSegment.ToString();
            aDef[iOffset + 17] = ((int) this.CIContactType).ToString();
            aDef[iOffset + 18] = this.CIDetailTokenString;
            aDef[iOffset + 19] = ((int) this.CIAlerts).ToString();
            aDef[iOffset + 20] = ((int)this.DisplayIn).ToString();
            aDef[iOffset + 21] = ((int)this.DisplayLevel).ToString();
            aDef[iOffset + 22] = this.DisplayValue;
            aDef[iOffset + 23] = this.Hotkey;
            aDef[iOffset + 24] = this.MustVisit.ToString();
            aDef[iOffset + 25] = ((int)this.AllowPrefillOverride).ToString();
            aDef[iOffset + 26] = this.IsMultiValue.ToString();
            aDef[iOffset + 27] = this.AssociatedPrefillNames;
            aDef[iOffset + 28] = this.TabNumber.ToString();
            aDef[iOffset + 29] = this.CIEntityName;
            //GLOG 2645
            aDef[iOffset + 30] = this.RuntimeControlValues;
            //GLOG 1408
            aDef[iOffset + 31] = this.NavigationTag;
            //10.2
            aDef[iOffset + 32] = this.ObjectDatabaseID.ToString();
            return aDef;
        }
        
		/// <summary>
		/// returns the variable as a delimited string -
		/// includes both persistent and non-persistent values
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
            return this.ToString(false);
		}
		/// <summary>
		/// returns the variable as a delimited string - 
		/// includes persistent values only if specified
		/// </summary>
		/// <param name="bPersistentValuesOnly"></param>
		/// <returns></returns>
		public string ToString(bool bPersistentValuesOnly)
		{
            //Need to replace pipes, since those will separate multiple
            //VariableDefinitions in the ObjectData
            return string.Join("�", this.ToArray(bPersistentValuesOnly)).Replace("|", 
                String.mpPipeReplacementChars);
		}

        /// <summary>
        /// sets the value of the variable to its default value
        /// </summary>
        public void AssignDefaultValue()
        {
            this.AssignDefaultValue(false);
        }
        public void SetValueFromContacts(ICContacts oContacts, bool bAppend)
        {
            if (this.ControlType == mpControlTypes.SalutationCombo)
            {
                LMP.Controls.SalutationCombo oSal;

                //create salutation control if necessary
                if (this.AssociatedControl == null)
                    this.CreateAssociatedControl(null);

                oSal = (LMP.Controls.SalutationCombo)this.AssociatedControl;

                //pass contacts to control
                oSal.SetContacts(oContacts, this.CIContactType, bAppend);

                //set variable value to the value of the control
                this.SetValue(this.AssociatedControl.Value);

                //GLOG 2645
                this.SetRuntimeControlValues(this.AssociatedControl.SupportingValues);
            }
            else
            {
                string xOldVal = this.Value;
                string xNewVal = "";
                int iItemCount = 0;

                int c = 0;

                for (int i = 1; i <= oContacts.Count(); i++)
                {
                    //cycle through contacts, adding to control 
                    //only those contacts of the specified type
                    Object oIndex = i;
                    ICContact oContact = oContacts.Item(oIndex);
                    string xCIFormat = XmlExpression.Evaluate(
                        this.CIDetailTokenString, this.Segment, this.ForteDocument);
                    string[] xFields = null;
                    string xFieldList = "";
                    string xField = null;
                    int iPos = 0;
                    int iPos2 = 0;

                    iPos = xCIFormat.IndexOf("<");

                    // Get list of CI tokens to replace
                    while (iPos > -1)
                    {
                        iPos2 = xCIFormat.IndexOf(">", iPos + 1);
                        if (iPos2 > 0)
                        {
                            xField = xCIFormat.Substring(iPos + 1, iPos2 - (iPos + 1));

                            //add field to list if the field isn't conditioned on a business address type
                            //or it is conditioned as such and the address is a business is a business address
                            if (!xField.ToLower().EndsWith("ifbusiness") ||
                                oContact.AddressTypeName.ToLower() != "home")
                            {
                                //trim "ifbusiness" from field name
                                if(xField.ToLower().EndsWith("ifbusiness"))
                                    xField = xField.Substring(0, xField.Length - 10);

                                if (xFieldList != "")
                                    xFieldList = xFieldList + "|";

                                xFieldList = xFieldList + xField;

                                iPos = xCIFormat.IndexOf("<", iPos + 1);
                            }
                            else
                            {
                                //field needs to be removed, as it shouldn't be included
                                xCIFormat = xCIFormat.Substring(0, iPos) + xCIFormat.Substring(iPos2 + 1);
                            }
                        }
                        else
                        {
                            iPos = xCIFormat.IndexOf("<", iPos + 1);
                        }

                    }

                    xFields = xFieldList.Split('|');

                    //remove "ifbusiness" from CI format string
                    xCIFormat = xCIFormat.Replace("IFBUSINESS", "");

                    // build XML string, but replacing < and > with @[ and ]@
                    for (int iIndex = 0; iIndex < xFields.Length; iIndex++)
                        xCIFormat = xCIFormat.Replace("<" + xFields[iIndex] + ">",
                            "@[" + xFields[iIndex] + " Index=\"\" UNID=\"\"]@<" +
                            xFields[iIndex] + ">@[/" + xFields[iIndex] + "]@");

                    //GLOG item #6745 - dcf - added condition to set iItemCount only
                    //if it has not already been set
                    if (bAppend && xOldVal != "" && iItemCount == 0)
                    {
                        iItemCount = LMP.String.CountChrs(xOldVal.ToUpper(),
                            string.Concat("<", xFields[0].ToUpper()));
                        c = iItemCount;
                    }

                    //GLOG 5359 - 1/5/11: Start with unchanged CI Format from Variable properties for each pass
                    string xAdjustedCIFormat = xCIFormat;
                    //There appears to be a bug where get_ContactType returns 0 instead of Other
                    //if Custom UI was loaded with a Contact prefilled, so in addition to 
                    //checking type, we need to back it up by checking the CustomTypeName
                    if (oContact.ContactType == this.CIContactType ||
                        (this.CIContactType == ciContactTypes.ciContactType_Other && 
                        oContact.CustomTypeName == this.DisplayName))
                    {
                        if (this.CIContactType == ciContactTypes.ciContactType_Other)
                        {
                            if (oContact.CustomTypeName != this.DisplayName)
                            {
                                //Custom type doesn't match current variable
                                continue;
                            }
                            else
                            {
                                //Custom UI only supports one entity per category
                                bAppend = false;
                                iItemCount = 0;
                                c = 0;
                            }
                        }
                        iItemCount++;
                        c++;
                        //GLOG 5359
                        xAdjustedCIFormat = xAdjustedCIFormat.Replace("<MAILINGDETAIL>", "<FULLNAMEWITHPREFIXANDSUFFIX>\r\n<COMPANY>\r\n<COREADDRESS>");

                        //GLOG 4825: Prevent duplication of Company name if FullName and Company are the same
                        if (xAdjustedCIFormat.Contains("<FULLNAME") && oContact.FullName == oContact.Company)
                        {
                            xAdjustedCIFormat = xAdjustedCIFormat.Replace("<COMPANY>", "");
                            xAdjustedCIFormat = xAdjustedCIFormat.Replace("<TITLE>", "");
                        }

                        //GLOG : 8031 : ceh
                        //fill in CoreAddress token
                        string xNewDetail = xAdjustedCIFormat.Replace("<COREADDRESS>", LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));
                        
                        //fill in other details
                        xNewDetail = oContact.GetDetail(xNewDetail, true);

                        //Replace any ampersands in the detail
                        xNewDetail = xNewDetail.Replace("&", "&amp;");
                        //replace temp markers for XML tags
                        xNewDetail = xNewDetail.Replace("@[", "<");
                        xNewDetail = xNewDetail.Replace("]@", ">");
                        // Fill in Index attribute
                        xNewDetail = xNewDetail.Replace("Index=\"\"", "Index=\"" + c.ToString() + "\"");
                        // Fill in UNID attribute
                        xNewDetail = xNewDetail.Replace("UNID=\"\"", "UNID=\"" + oContact.UNID + "\"");

                        //replace '\v' with '\r\n' - '\v' conflicts with XML
                        xNewDetail = xNewDetail.Replace("\v", "\r\n");
                        LMP.Trace.WriteNameValuePairs("xNewDetail", xNewDetail);

                        XmlDocument oXML = new XmlDocument();
                        oXML.LoadXml("<zzmpC>" + xNewDetail + "</zzmpC>");
                        string xAdjustedDetail = "";
                        foreach (XmlNode oNode in oXML.ChildNodes[0].ChildNodes)
                        {
                            if (oNode.InnerText != "")
                                xAdjustedDetail += oNode.OuterXml;
                        }
                        xNewVal += xAdjustedDetail;

                        //if(oContact != null)
                        //    System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContact);

                        oContact = null;
                    }
                    if (this.CIContactType == ciContactTypes.ciContactType_Other && c > 0)
                        //There can only be one matching entry, so stop looking
                        break;
                }

                if (bAppend)
                {
                    if (xNewVal != "")
                    {
                        // append new XML to existing value
                        this.SetValue(xOldVal + xNewVal);
                    }
                }
                else
                    this.SetValue(xNewVal);
            }
        }

        /// <summary>
        /// sets the value of the variable to its default value
        /// </summary>
        /// <param name="bIgnoreDeleteScope">
        /// if true, expanded scopes will not be deleted when value is empty
        /// </param>
        public void AssignDefaultValue(bool bIgnoreDeleteScope)
        {
            DateTime t0 = DateTime.Now;
            //bool bFirstTry = true;
            string xOfficeDefault = null;
            string xFirmDefault = null;
            string xDefaultValue = "";
            string xPref = "";
            XmlSegment oTarget = null;
            int iTry = 1;
            try
            {
                do
                {
                    if (iTry == 1)
                    {
                        //evaluate default value expression
                        xDefaultValue = XmlExpression.Evaluate(
                            this.DefaultValue, this.m_oSegment, this.ForteDocument);
                    }
                    else
                    {
                        //GLOG 3689:  Action failed with Author Preference - use Office or Firm default instead
                        if (xOfficeDefault == null)
                        {
                            string xCode = "";
                            int iPrefStart = -1;
                            iPrefStart = this.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPREFERENCE_");
                            if (iPrefStart == -1)
                                iPrefStart = this.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPARENTPREFERENCE_");
                            if (iPrefStart == -1)
                                iPrefStart = this.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPARENTPREFERENCE_");
                            if (iPrefStart == -1)
                                iPrefStart = this.DefaultValue.ToUpper().IndexOf("[AUTHORPARENTPREFERENCE_");
                            if (iPrefStart > -1)
                            {
                                int iPrefEnd = -1;
                                iPrefEnd = this.DefaultValue.IndexOf("]", iPrefStart);
                                if (iPrefEnd > -1)
                                {
                                    xCode = this.DefaultValue.Substring(iPrefStart, iPrefEnd + 1 - iPrefStart);
                                    xPref = xCode.Substring(xCode.LastIndexOf("_") + 1).TrimEnd(']');
                                    oTarget = this.Segment;
                                    if (xCode.ToUpper().Contains("AUTHORPARENTPREFERENCE"))
                                    {
                                        //Use top level parent as the keyset Scope
                                        while (oTarget.Parent != null)
                                            oTarget = oTarget.Parent;
                                    }
                                    int iOffice = 0;
                                    try
                                    {
                                        iOffice = oTarget.Authors.GetLeadAuthor().PersonObject.OfficeID;
                                    }
                                    catch { }
                                    //Check value in Office Preferences
                                    KeySet oKeys = new KeySet(mpPreferenceSetTypes.Author, oTarget.ID, iOffice.ToString());
                                    bool bDefaultPref = false;
                                    //Replace Preference code with Office Default for evaluation
                                    xOfficeDefault = this.DefaultValue.Replace(xCode, oKeys.GetValue(xPref, out bDefaultPref));
                                    //JTS 9/24/09: Manual Author won't have Office, so always use Firm Default
                                    if (!bDefaultPref && !oTarget.Authors.GetLeadAuthor().IsManualAuthor)
                                    {
                                        //There are separate Office and Firm  keysets -
                                        //Replace Preference code with Firm Default Value
                                        xFirmDefault = this.DefaultValue.Replace(xCode, oKeys.GetDefaultValue(xPref));
                                    }
                                    else
                                    {
                                        //There is no separate Office keyset, only Firm default
                                        xFirmDefault = xOfficeDefault;
                                        iTry++;
                                    }
                                }
                            }
                        }
                        string xTestDefault = null;
                        if (iTry == 2)
                            //Try separate Office default value
                            xTestDefault = XmlExpression.Evaluate(xOfficeDefault, this.m_oSegment, this.ForteDocument);
                        if (iTry == 3 || xTestDefault == xDefaultValue)
                        {
                            //Office Preference non-existent or same as Author Preference - use Firm setting
                            xDefaultValue = XmlExpression.Evaluate(xFirmDefault, this.m_oSegment, this.ForteDocument);
                        }
                        else
                        {
                            xDefaultValue = xTestDefault;
                        }
                    }
                    //if this is a user preference that has not yet been set,
                    //use secondary default value
                    if ((xDefaultValue == "null") && (XmlExpression.ContainsPreferenceCode(
                        this.DefaultValue, XmlExpression.PreferenceCategories.User)) &&
                        (this.SecondaryDefaultValue != ""))
                    {
                        xDefaultValue = XmlExpression.Evaluate(this.SecondaryDefaultValue,
                            this.m_oSegment, this.ForteDocument);
                    }

                    //get current value
                    string xValue = this.Value;


                    //GLOG 8524: Use ValueSourceExpression if specified and there's no Default value
                    if ((xDefaultValue == "null" || xDefaultValue == "")
                        && this.ValueSourceExpression != "" && !this.ValueSourceExpression.ToUpper().Contains("[TAGEXPANDEDVALUE") 
                        && !this.ValueSourceExpression.ToUpper().Contains("[RELINEVALUE") 
                        && !this.ValueSourceExpression.ToUpper().Contains("[DETAILVALUE")  //GLOG 8477
                        && this.DefaultValue == "")
                    {
                        xDefaultValue = this.GetValueFromSource();
                    }
                    
                    if (xDefaultValue == "null" || xDefaultValue == "")
                    {
                        switch (this.ControlType)
                        {
                            case mpControlTypes.Checkbox:
                                xDefaultValue = "false";
                                break;
                            case mpControlTypes.Calendar:
                                xDefaultValue = DateTime.Today.ToString();
                                break;
                            case mpControlTypes.Spinner:
                                xDefaultValue = "0";
                                break;
                            default:
                                xDefaultValue = "";
                                break;
                        }
                    }

                    //assign result as value of variable
                    if (!LMP.String.CompareBooleanCaseInsensitive(xValue, xDefaultValue))
                    {
                        try
                        {
                            this.SetValue(xDefaultValue, true, bIgnoreDeleteScope);
                            if (iTry > 1)
                            {
                                //GLOG 3689: Original Author Preference was invalid.
                                //Action was completed using Office or Firm Preference instead
                                if (!this.Segment.InvalidPreferenceVariables.Contains(this.Name + "|") &&
                                    //JTS 9/24/09: Don't display message for Manual author
                                    !this.Segment.Authors.GetLeadAuthor().IsManualAuthor)
                                {
                                    string xMsg = string.Format(LMP.Resources.GetLangString("Msg_InvalidAuthorPreferenceValue"), xPref,
                                        (iTry < 3 ? "Office" : "Firm"), oTarget.DisplayName);
                                    //GLOG 8727: Don't show message, since there may not be any UI
                                    Error.Show(new LMP.Exceptions.ValueException(xMsg));
                                    //MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //Flag that message has been displayed in Segment - action may run again if 
                                    //a different variable's action refreshes the Variables collection
                                    this.Segment.InvalidPreferenceVariables += this.Name + "|";
                                }
                                iTry = 4;
                            }
                        }
                        catch (LMP.Exceptions.ActionException oLE)
                        {
                            if (iTry == 3)
                                //Firm default also invalid
                                throw oLE;
                            else if (XmlExpression.ContainsPreferenceCode(this.DefaultValue, XmlExpression.PreferenceCategories.Author))
                                iTry++;
                        }
                        catch (System.Exception oE)
                        {
                            throw oE;
                        }
                    }
                    else
                    {
                        //GLOG 3689: End loop if value hasn't changed
                        iTry = 4;
                        //SetValidationField variable, as the SetValidationField function
                        //runs only when the value is set
                        this.SetValidationField();

                        //execute actions for include/exclude type variable if necessary -
                        //text may need updating even when default value matches current value;
                        //TODO: this issue theoretically applies to any variable with an action
                        //that has a field code in one of its parameters, so this code may need
                        //to be generalized at some point if we can find an efficient way to do so
                        if (((this.ValueSourceExpression == "[TagValue] ^!= ") ||
                            (this.ValueSourceExpression == "[TagValue] ^!=") ||
                            (this.ValueSourceExpression == "[TagValue] ^!= [Empty]") ||
                            (this.ValueSourceExpression == "[MyTagValue] ^!= ") ||
                            (this.ValueSourceExpression == "[MyTagValue] ^!=") ||
                            (this.ValueSourceExpression == "[MyTagValue] ^!= [Empty]")) &&
                            (xValue.ToLower() == "true") && (this.SecondaryDefaultValue != null))
                        {
                            //compare tag value to secondary default value
                            xDefaultValue = XmlExpression.Evaluate(
                                this.SecondaryDefaultValue, this.m_oSegment, this.ForteDocument);
                            string xNodeText = this.AssociatedContentControls[0].InnerText; 

                            if (xNodeText != xDefaultValue)
                                this.VariableActions.Execute(bIgnoreDeleteScope);
                        }
                        else if (!bIgnoreDeleteScope)
                        {
                            //ensure that block gets deleted if necessary -
                            //this may not have been done in design because variable def
                            //contains field codes or actions dependent on runtime values -
                            //added 12/16/08 for Jones Day and Cleary fax configuration
                            //where cc table is in a block that gets excluded when there
                            //are no cc's (Doug)
                            for (int i = 0; i < this.VariableActions.Count; i++)
                            {
                                XmlVariableAction oAction = this.VariableActions[i];
                                //GLOG - 3395 - CEH
                                if (oAction.Type == XmlVariableActions.Types.IncludeExcludeBlocks)
                                {
                                    //get parameters
                                    string xInclude;
                                    string xExpression = "";
                                    string[] aParams = oAction.Parameters.Split(
                                        LMP.StringArray.mpEndOfSubField);
                                    string xBlockName = XmlExpression.Evaluate(aParams[0],
                                        m_oSegment, m_oForteDocument);
                                    if (aParams.Length == 2)
                                        xExpression = aParams[1];
                                    if (xExpression != "")
                                        xInclude = XmlExpression.Evaluate(xExpression,
                                            m_oSegment, m_oForteDocument);
                                    else
                                        xInclude = xValue;

                                    //execute only if exclusion is specified and
                                    //block is still in the document
                                    if (xInclude.ToLower() == "false")
                                    {
                                        XmlBlock oBlock = null;
                                        try
                                        {
                                            oBlock = m_oSegment.Blocks.ItemFromName(
                                                xBlockName);
                                        }
                                        catch { }
                                        if (oBlock != null)
                                        {
                                            this.VariableActions.Execute();
                                            break;
                                        }
                                    }
                                }
                                else if (oAction.Type == XmlVariableActions.Types.ApplyStyleSheet)
                                {
                                    //Always run ApplyStyleSheet action, since Style Sheet may have changed
                                    this.VariableActions.Execute();
                                    break;
                                }
                            }
                        }
                    }
                }
                while (iTry > 1 && iTry < 4);
            }
            catch (System.Exception oE)
            {
                if (this.IsStandalone)
                {
                    //not all standalone vars can have their
                    //default value set - e.g. when the default value contains
                    //a field code that requires a containing segment
                    Error.Show(new LMP.Exceptions.ValueException(LMP.Resources.GetLangString("Msg_CouldNotSetValueOfStandaloneVar") + this.DisplayName));
                    //MessageBox.Show(LMP.Resources.GetLangString(
                    //    "Msg_CouldNotSetValueOfStandaloneVar") + this.DisplayName,
                    //    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                    //    MessageBoxIcon.Exclamation);
                }
                else
                {
                    if (!this.m_oSegment.InvalidPreferenceVariables.Contains(this.Name + "|"))
                    {
                        this.m_oSegment.InvalidPreferenceVariables += this.Name + "|";
                    }
                    throw new LMP.Exceptions.ValueException(
                        LMP.Resources.GetLangString(
                        "Error_CouldNotAssignDefaultValueToVariable") + this.Name + " (" + this.ID + ")", oE);
                }
            }

            LMP.Benchmarks.Print(t0, this.Name);
        }
        ///// <summary>
        ///// sets the value of the variable to the specified value -
        ///// executes actions if specified and appropriate
        ///// </summary>
        ///// <param name="bValue"></param>
        ///// <param name="bExecuteActions"></param>
        //public void SetValue(string xValue, bool bExecuteActions,
        //    bool bIgnoreDeleteScope)
        //{
        //    //exit if segment is deactivated (5/10/11, dm)
        //    if (!this.Segment.Activated)
        //        return;

        //    //mark that value has been actively set
        //    m_bHasInitializedValue = true;

        //    //every variable has a starting value
        //    //when the content is inserted - there
        //    //is no such thing as content whose variables
        //    //have no value - hence, if the field (m_xValue)
        //    //hasn't been assigned, do so now - this is 
        //    //different than the default value, which will
        //    //be assigned as the value in another iteration
        //    //through this property set
        //    if (m_xValue == null)
        //        m_xValue = this.GetValueFromSource();

        //    if (xValue == null)
        //    {
        //        m_xValue = this.GetValueFromSource();
        //        return;
        //    }

        //    if (!LMP.String.CompareBooleanCaseInsensitive(xValue, m_xValue))
        //    {
        //        //value has changed - set value
        //        m_xValue = xValue;

        //        //save the value of the sticky field
        //        SavePreferenceIfNecessary();

        //        //set reserve value if necessary
        //        NodeStore oNS = this.Parent.Nodes;

        //        if (((this.TagType == XmlForteDocument.TagTypes.Segment) &&
        //            ((this.ValueSourceExpression == "") ||
        //            (this.ValueSourceExpression.Contains("[DetailValue"))) ||
        //            (this.ValueSourceExpression.Contains("[ReserveValue]"))))
        //        {
        //            this.ReserveValue = xValue;
        //            this.IsDirty = true;
        //            this.Parent.Save(this, true);
        //        }
        //        //set the private validation field to
        //        //mark whether or not the variable
        //        //now has a valid value
        //        SetValidationField();

        //        //test new
        //        if (bExecuteActions && !(this.Segment is IStaticCreationSegment))
        //        {
        //            if (!(this.Segment is IStaticRecipientCreationSegment) ||
        //                this.Name != "Recipients")
        //            {
        //                //execute actions
        //                this.VariableActions.Execute(bIgnoreDeleteScope);
        //            }
        //            else
        //            {
        //            }
        //        }

        //        //raise event if there are subscribers
        //        if (this.ValueAssigned != null)
        //            this.ValueAssigned(this, new EventArgs());
        //    }
        //}

        public void SetValue(string xValue, bool bExecuteActions,
            bool bIgnoreDeleteScope)
        {
            //mark that value has been actively set
            m_bHasInitializedValue = true;

            //every variable has a starting value
            //when the content is inserted - there
            //is no such thing as content whose variables
            //have no value - hence, if the field (m_xValue)
            //hasn't been assigned, do so now - this is 
            //different than the default value, which will
            //be assigned as the value in another iteration
            //through this property set
            //TODO: OpenXML rewrite
            //if (m_xValue == null)
            //    m_xValue = this.GetValueFromSource();

            //if (xValue == null)
            //{
            //    m_xValue = this.GetValueFromSource();
            //    return;
            //}

            if (!LMP.String.CompareBooleanCaseInsensitive(xValue, m_xValue))
            {
                //value has changed - set value
                m_xValue = xValue;

                //save the value of the sticky field
                SavePreferenceIfNecessary();

                if (((this.TagType == XmlForteDocument.TagTypes.Segment) &&
                    ((this.ValueSourceExpression == "") ||
                    (this.ValueSourceExpression.Contains("[DetailValue"))) ||
                    (this.ValueSourceExpression.Contains("[ReserveValue]"))))
                {
                    this.ReserveValue = xValue;
                    this.IsDirty = true;
                    this.Parent.Save(this, true);
                }
                //set the private validation field to
                //mark whether or not the variable
                //now has a valid value
                SetValidationField();

                if (bExecuteActions)
                {
                    //execute actions
                    this.VariableActions.Execute(bIgnoreDeleteScope);
                }

                //raise event if there are subscribers
                if (this.ValueAssigned != null)
                    this.ValueAssigned(this, new EventArgs());
            }
        }
        
        public void SetValue(string xValue)
		{
			this.SetValue(xValue, true);
		}
        public void SetValue(string xValue, bool bExecuteActions)
        {
            this.SetValue(xValue, bExecuteActions, false);
        }

        /// <summary>
        /// returns true iff this variable has a
        /// distributed segment variable action -
        /// either distributed sections or label table
        /// </summary>
        /// <returns></returns>
        public bool HasDistributedSegmentVariableActions()
        {
            //bool bHasDistributedVariableAction = false;

            for (int i = 0; i < this.VariableActions.Count; i++)
            {
                XmlVariableActions.Types iType = this.VariableActions[i].Type;

                if (iType == XmlVariableActions.Types.SetupDistributedSections ||
                    iType == XmlVariableActions.Types.SetupLabelTable)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// returns true iff this variable has a
        /// distributed segment variable action
        /// </summary>
        /// <returns></returns>
        public bool HasDetailTableAction()
        {
            //bool bHasDistributedVariableAction = false;

            for (int i = 0; i < this.VariableActions.Count; i++)
            {
                if (this.VariableActions[i].Type ==
                    XmlVariableActions.Types.InsertDetail)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// returns the value defined by the ValueSourceExpression for this variable
        /// </summary>
        /// <returns></returns>
        public string GetValueFromSource()
        {
            return GetValueFromSource(null);
        }

        /// <summary>
        /// returns the value defined by the ValueSourceExpression for this variable
        /// </summary>
        /// <param name="oCC">determines which of the content controls associated with this
        /// variable to use as the source where appropriate - if null or invalid,
        /// uses the first associated content control in the node store</param>
        /// <returns></returns>
        public string GetValueFromSource(DocumentFormat.OpenXml.Wordprocessing.SdtElement oCCElement)
        {
            //GLOG 7921/7947 (dm) - this code was causing the value to be lost
            //on refresh and seems to no longer be necessary - it also relies
            //on the presence of variable actions that are no longer used
            ////if this variable is part of a distributed segment,
            ////and it has a distributed variable action, don't
            ////update it's value from the document source - these
            ////variables are static (e.g. envelope and labels recipients)
            //if ((this.Segment is IStaticDistributedSegment) &&
            //    this.HasDistributedSegmentVariableActions())
            //{
            //    return m_xValue == null ? "" : m_xValue;
            //}

            try
            {
                DateTime t0 = DateTime.Now;
                //Word.Range oCCRange = null;

                //validate specified tag
                if (oCCElement != null)
                {
                    if (Query.GetBaseName(oCCElement) == "mVar")
                    {
                        string xTagID = Query.GetAttributeValue(m_oSegment.WPDoc, oCCElement, "TagID");
                        //GLOG 968 (dm) - added second condition below
                        if ((xTagID != this.Name) && !this.IsDependentVariable(xTagID))
                            oCCElement = null;
                    }
                    else
                        oCCElement = null;
                }

                //get value source expression
                string xValue = "";
                if (m_oSegment != null)
                {
                    xValue = this.ValueSourceExpression;

                    //we have no need to support this programmatically generated
                    //field code on the content control side
                    if (xValue.StartsWith("[TagExpandedValue"))
                        xValue = "";
                }
                if (xValue == "")
                {
                    //JTS 11/9/16: Avoid multiple calls to AssociatedContentControls
                    List<SdtElement> oCCs = this.AssociatedContentControls;
                    //    //value source expression is empty, which is the default -
                    //    //get the nodestore for this collection of vars
                    //    NodeStore oNS;
                    //    oNS = this.Parent.Nodes;

                    //    //get the element name of this var
                    //    string xElementName = oNS.GetItemElementName(this.TagID);

                    //    if (xElementName == "mSEG")
                    if (oCCs.Count == 0)
                    {
                        //mSEG - return value held in variable definition
                        xValue = this.ReserveValue;
                    }
                    else
                    {
                        //    else if (xElementName == "mVar")
                        //    {
                        int iStartIndex = 0;
                        int iEndIndex = 0;
                        string xTempValue = null;

                        //For Multiple Value type, Value will be concatenation of all associated nodes
                        if (this.IsMultiValue)
                        {
                            iEndIndex = oCCs.Count - 1; //JTS: 4/21/16
                            oCCElement = null;
                        }

                        for (int i = iStartIndex; i <= iEndIndex; i++)
                        {
                            //return text from associated content control
                            if (oCCElement == null)
                                oCCElement = oCCs[i];

                            //            //10.2 (dm) - I think this code is obsolete - are sub vars
                            //            //ever used without a corresponding value source expression?
                            //            //check for sub vars
                            //            object oIndex = 1;
                            //            if (oCC.Range.ContentControls.Count > 0 &&
                            //                LMP.String.GetBoundingObjectBaseName(
                            //                oCC.Range.ContentControls.get_Item(ref oIndex).Tag) == "mSubVar")
                            //            {
                            //                //Convert mSubVar Tags to standard XML tags

                            //                //TODO: check for text not in sub vars - in general, this indicates
                            //                //that the user entered text directly into the document
                            //                //that ended up outside of a sub var - only reline allows
                            //                //non-subvar text in a variable that contains subvars

                            //                xTempValue = LMP.Forte.MSWord.WordDoc.GetSubVarXML_CC(ref oCC);
                            //            }
                            //            else
                            //            {
                            //                oCCRange = oCC.Range;

                            //                //GLOG 7394 (dm) - we need to take revisions into account here,
                            //                //as Range.Text might include deletions
                            //                xTempValue = LMP.Forte.MSWord.WordDoc.GetTextWithRevisions(oCCRange);
                            //                //xTempValue = oCCRange.Text;

                            xTempValue = Query.GetElementInnerText(oCCElement, true);
                            //If value consists only of spaces, strip from result
                            if (LMP.String.ContainsOnlySpaces(xTempValue))
                                xTempValue = "";
                            //Handle checkbox variable
                            if (xTempValue.Contains("MACROBUTTON zzmpToggleFormsCheckbox") && oCCElement.Descendants().Where(d => d.LocalName == "instrText").Any())
                            {
                                if (xTempValue.EndsWith(((char)253).ToString()))
                                    xTempValue = "True";
                                else 
                                    xTempValue = "False";
                            }

                            ////if text is null or empty, contains placeholder text, or is not upper case,
                            ////bypass processing - xValue will be node.Text
                            //if (!string.IsNullOrEmpty(xTempValue) &&
                            //    xTempValue != this.DisplayName.ToUpper() &&
                            //    xTempValue.ToUpper() == xTempValue)
                            //{
                            //    LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents bIgnoreEvents = XmlForteDocument.IgnoreWordXMLEvents;

                            //    //Make sure resetting Node won't trigger change event
                            //    XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;

                            //    ////node text is upper case - 
                            //    ////we need to get the input as it was typed - if
                            //    ////the node text was formatted to all caps, oWordNode.Text
                            //    ////returns the all cap version, even when the text was
                            //    ////not input in all caps
                            //    try
                            //    {
                            //        xTempValue = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCCRange);
                            //    }
                            //    finally
                            //    {
                            //        XmlForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;
                            //    }
                            //}
                            if (i < iEndIndex)
                                //use delimiter between multiple non-xml values
                                xTempValue = xTempValue + StringArray.mpEndOfValue;
                            xValue = xValue + xTempValue;
                            oCCElement = null;
                        }
                    }
                    if (this.IsMultiValue && xValue.Replace(StringArray.mpEndOfValue.ToString(), "") == "")
                        //If all tags are empty, return empty string
                        xValue = "";
                }
                else
                {
                    //get value from expression from code.
                    //JTS 11/9/16: Avoid multiple calls to AssociatedContentControls (IsTagless calls this)
                    List<SdtElement> oCCs = this.AssociatedContentControls;
                    if ((oCCElement == null) && oCCs.Count > 0)
                        oCCElement = oCCs[0];

                    //GLOG 8461
                    if (xValue == "[DetailValue]")
                    {
                        //GLOG 8477
                        //get detail value
                        string xDetailValue = GetDetailValue(this, oCCElement, false);

                        //we support this field code with or w/o the variable name
                        xValue = xValue.Replace("[DetailValue]", xDetailValue);
                        xValue = xValue.Replace("[DetailValue" + "__" + this.Name + "]",
                            xDetailValue);

                    }

                    //GLOG 4476 (dm) - consolidated [TagValue] and [MyTagValue] into a
                    //single block to avoid duplicate code
                    if (xValue.Contains("[TagValue]") || xValue.Contains("[MyTagValue]"))
                    {
                        string xTagValue = oCCElement.InnerText;

                        //GLOG 2151: Replace any reserved Expression characters before evaluating
                        xTagValue = XmlExpression.MarkLiteralReservedCharacters(xTagValue);

                        ////GLOG 4476 (dm) - if field code is displayed, get result
                        //if ((!string.IsNullOrEmpty(xTagValue)) &&
                        //    xTagValue.Contains(((char)21).ToString()) &&
                        //    (oCCRange.Fields.Count > 0))
                        //    xTagValue = oCCRange.Fields[1].Result.Text;

                        // TagValue is now deprecated.
                        if (xValue.Contains("[TagValue]"))
                        {
                            //replace [TagValue] code
                            xValue = xValue.Replace("[TagValue]", xTagValue);
                        }

                        //get value from expression the newly introduced MyTagValue 
                        if (xValue.Contains("[MyTagValue]"))
                        {
                            //replace [MyTagValue] code
                            DateTime oDateTime;
                            //GLOG 6594:  Test date using the underlying language for the range -
                            //Date formats for a particular language may not be valid for the System culture
                            System.Globalization.CultureInfo oCulture = null;
                            //GLOG 6634: Accessing the Range LanguageID in Word 2007 can result in Word freezing
                            //if next variable accessed is in header.  Instead, just use Segment Language
                            int iLCID = this.Segment.Culture;
                            //TODO: Get CC Language from style or direct formatting
                            try
                            {
                                oCulture = new System.Globalization.CultureInfo(iLCID);
                            }
                            catch
                            {
                                //If LanguageID not a valid Culture ID, use System default
                                oCulture = System.Globalization.CultureInfo.CurrentCulture;
                            }
                            if (DateTime.TryParse(xTagValue, oCulture, System.Globalization.DateTimeStyles.None, out oDateTime)) //GLOG 6594
                            {
                                //tag contains a date - check for date field
                                OpenXmlElement oFld = oCCElement.Descendants().Where(d => d.LocalName == "instrText").FirstOrDefault();
                                if (oFld != null)
                                {
                                    string xCode = oFld.InnerText.Trim();
                                    if (xCode.StartsWith("DATE \\@") || xCode.StartsWith("TIME \\@"))
                                    {
                                        //Return date format portionV of date field with quotes removed
                                        string xDateFormat = xCode.Substring(xCode.IndexOf("\"") + 1, xCode.LastIndexOf("\"") - xCode.IndexOf("\"")) + " /F";

                                        if (xDateFormat != "")
                                        {
                                            if (xValue == "[MyTagValue]")
                                            {
                                                //Only return Date Format if not part of a larger expression
                                                LMP.Benchmarks.Print(t0, this.Name);
                                                return xDateFormat;
                                            }
                                            else
                                                xTagValue = xDateFormat;

                                        }
                                    }
                                }
                            }

                            xValue = xValue.Replace("[MyTagValue]", xTagValue);
                        }
                    }

                    //GLOG 968 (4/17/09, dm)
                    //for single value variables, this field code was previously always
                    //getting evaluated using the variable's first associated tag
                    //if (xValue.Contains("[DetailValue"))
                    //{
                    //    //get detail value
                    //    //This will always be empty when insering Oxml segment
                    //    string xDetailValue = ""; //GetDetailValue(this, null, oCC, false);

                    //    //we support this field code with or w/o the variable name
                    //    xValue = xValue.Replace("[DetailValue]", xDetailValue);
                    //    xValue = xValue.Replace("[DetailValue" + "__" + this.Name + "]",
                    //        xDetailValue);
                    //}

                    //If value consists only of spaces, strip from result
                    if (LMP.String.ContainsOnlySpaces(xValue))
                        xValue = "";

                    //replace [ReserveValue] code
                    xValue = xValue.Replace("[ReserveValue]", this.ReserveValue);

                    //append variable name to codes where necessary
                    xValue = xValue.Replace("[RelineValue]", "[RelineValue" +
                        "__" + this.Name + "]");

                    //evaluate
                    string xSavedValue = xValue; //GLOG 6598
                    xValue = XmlExpression.Evaluate(xValue, this.m_oSegment, this.ForteDocument);
                    //GLOG 6598:  There's no way to differentiate between "d" and "dd" token for 2-digit days
                    //Test replacing single 'd' with 'dd' in evaluated format, and use that if it matches
                    //one of the formats configured for the control.
                    if (xSavedValue.ToUpper().Contains("[DATEFORMAT_") && this.ControlType == mpControlTypes.DateCombo)
                    {
                        if (xValue.StartsWith("d M"))
                        {
                            string xTestFormat = "d" + xValue;
                            if (this.ControlProperties.Contains(xTestFormat)) //GLOG 7321 - Make sure 'dd MMMM' format is not assigned unless configured for the control
                                xValue = xTestFormat;
                        }
                        else if (xValue.StartsWith("MMMM d") && !xValue.StartsWith("MMMM dd"))
                        {
                            string xTestFormat = xValue.Replace("MMMM d", "MMMM dd");
                            if (this.ControlProperties.Contains(xTestFormat))
                                xValue = xTestFormat;
                        }
                    }
                }

                LMP.Benchmarks.Print(t0, this.Name);
                return xValue == null ? "" : xValue;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.ValueSourceException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotEvaluateValueSourceExpression") +
                    this.ValueSourceExpression, oE);
            }
        }
        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by xActionParameters
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xActionParameters"></param>
        /// <returns></returns>
        private static string GetVariableDetailValue(XmlVariable oVar, string xActionParameters,
            SdtElement oTargetCC, bool bReorderEntitiesByLocation)
        {
            DateTime t0 = DateTime.Now;

            //get separators from action parameters
            XmlSegment oSegment = oVar.Segment;
            XmlForteDocument oMPDocument = oSegment.ForteDocument;
            string[] aParams = xActionParameters.Split(LMP.StringArray.mpEndOfSubField);
            string xItemSeparator = XmlExpression.Evaluate(aParams[0], oSegment, oMPDocument);
            string xEntitySeparator = XmlExpression.Evaluate(aParams[1], oSegment, oMPDocument);
            //GLOG 4651: Get Alternate Format type to determine how entities inside table cells should be handled
            XmlVariableAction.AlternateFormatTypes iAltFormat = XmlVariableAction.AlternateFormatTypes.None;
            try
            {
                iAltFormat = (XmlVariableAction.AlternateFormatTypes)Int32.Parse(aParams[3]);
            }
            catch { }

            //get fields
            string xFields = "";
            string xTargetFields = "|";
            string xControlProps = "";
            mpControlTypes oControlType = oVar.ControlType;

            if ((oControlType != mpControlTypes.DetailList) &&
                (oControlType != mpControlTypes.DetailGrid))
            {
                //get control variable
                XmlVariable oUISourceVar = oVar.UISourceVariable;
                oControlType = oUISourceVar.ControlType;
                if ((oControlType == mpControlTypes.DetailList) ||
                    (oControlType == mpControlTypes.DetailGrid))
                    xControlProps = oUISourceVar.ControlProperties;

                //get target fields from template
                string xTemplate = XmlExpression.Evaluate(aParams[2], oSegment, oMPDocument);
                int iPos = xTemplate.IndexOf("[SubVar_");
                while (iPos != -1)
                {
                    iPos = iPos + 8;
                    int iPos2 = xTemplate.IndexOf(']', iPos);
                    if (iPos2 == -1)
                    {
                        //this is only ever the case due to a bug
                        //in Expression.EvaluateOperation()
                        break;
                    }
                    //JTS 12/19/08: Fieldcode may use [SubVar__, so trim any starting '_' character
                    xTargetFields = xTargetFields + xTemplate.Substring(
                        iPos, iPos2 - iPos).TrimStart(@"_".ToCharArray()) + "|";
                    iPos = xTemplate.IndexOf("[SubVar_", iPos2);
                }

                if ((xControlProps == "") || (xTargetFields == "|"))
                    return "";
            }
            else
                xControlProps = oVar.ControlProperties;

            if (oControlType == mpControlTypes.DetailList)
            {
                //list
                string xName = "Item";
                string xRows = "1";

                //get ci token if specified
                int iPos = xControlProps.IndexOf("CIToken=<");
                if (iPos != -1)
                {
                    iPos += 9;
                    int iPos2 = xControlProps.IndexOf(">", iPos);
                    xName = xControlProps.Substring(iPos, iPos2 - iPos);
                }

                //get rows per item if specified
                iPos = xControlProps.IndexOf("RowsPerItem=");
                if (iPos != -1)
                {
                    iPos += 12;
                    int iPos2 = xControlProps.IndexOf(StringArray.mpEndOfSubValue, iPos);
                    if (iPos2 == -1)
                        iPos2 = xControlProps.IndexOf(StringArray.mpEndOfValue, iPos);
                    xRows = xControlProps.Substring(iPos, iPos2 - iPos);
                }

                xFields = xName + StringArray.mpEndOfValue + xRows;
            }
            else if (oControlType == mpControlTypes.DetailGrid)
            {
                //grid - parse config string to get field names and # of rows
                int iPos = xControlProps.IndexOf("ConfigString=") + 13;
                int iPos2 = xControlProps.IndexOf(StringArray.mpEndOfSubValue, iPos);
                if (iPos2 == -1)
                    iPos2 = xControlProps.IndexOf(StringArray.mpEndOfValue, iPos);
                string xConfigString = xControlProps.Substring(iPos, iPos2 - iPos);
                string[] aConfig = xConfigString.Split(StringArray.mpEndOfSubField);
                byte bytFieldCount = (byte)(aConfig.Length / 5);
                int iOffset = 0;
                for (short sh = 1; sh <= bytFieldCount; sh++)
                {
                    string xField = aConfig[iOffset + 1];
                    if ((xTargetFields == "|") ||
                        (xTargetFields.ToUpper().IndexOf("|" + xField.ToUpper() + "|") > -1))
                    {
                        xFields = xFields + xField + StringArray.mpEndOfValue;
                        if (LMP.String.IsNumericInt32(aConfig[iOffset + 4]))
                            //get number of rows
                            xFields = xFields + aConfig[iOffset + 4] + StringArray.mpEndOfField;
                        else
                            //TODO: use list name to get number of rows - for now, assume 1
                            xFields = xFields + "1" + StringArray.mpEndOfField;
                    }
                    iOffset = iOffset + 5;
                }
                xFields = xFields.Substring(0, xFields.Length - 1);
            }
            else
                return "";

            //get value from associated tags -
            //for distributed detail, value will be concatenation of all associated nodes
            //content controls
            List<SdtElement> oCCs = null;
            if (oVar.IsMultiValue)
                oCCs = oVar.AssociatedContentControls;
            else
            {
                //GLOG 968 (dm) - use specified tag if provided; otherwise use first one
                if (oTargetCC == null)
                    oTargetCC = oVar.AssociatedContentControls[0];
                oCCs = new List<SdtElement> { oTargetCC };
            }

            //GLOG 4561: Added parameter indicating whether to assume only 1 entity per mVar tag inside table cell
            //GLOG 8407: Only check revisions if Segment is fully created
            string xDetailValue = GetDetailValueXml(ref oCCs, xFields, oSegment.WPDoc);

            LMP.Benchmarks.Print(t0);

            return xDetailValue;
        }
        internal static string GetDetailValueXml(ref List<SdtElement> oCCs, string xItems, WordprocessingDocument oDoc)
        {
            //GLOG 8477:  Builds Detail XML string from mSubVars.
            //This is more basic than API version - we're assuming that
            //all text will be within an mSubVar
            string xDetailXml = "";
            //create array of field names and max lines
            string[] aItems = xItems.Split('|');
            List<string>oFieldList = new List<string>();
            for (int i = 0; i < aItems.GetLength(0); i++)
            {
                //Create array of names and max lines - only names are used here
                string[] aProps = aItems[i].Split('�');
                oFieldList.Add(aProps[0]);
            }
            Hashtable oItemIndices = new Hashtable();
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml("<zzmpD></zzmpD>");
            foreach (SdtElement oCC in oCCs)
            {
                SdtElement[] aSubVars = oCC.Descendants<SdtElement>().Where(cc => Query.GetBaseName(cc)=="mSubVar").ToArray();
                if (aSubVars.GetLength(0) == 0)
                {
                    //If no SubVars (e.g., Letter with "Recipients" directly in mVar CC)
                    //just return InnerText of parent content control
                    return oCC.InnerText;
                }
                foreach (SdtElement oSubVar in aSubVars)
                {
                    string xName = Query.GetAttributeValue(oDoc, oSubVar, "Name");
                    if (oFieldList.Contains(xName))
                    {
                        string xObjectData = Query.GetAttributeValue(oDoc, oSubVar, "ObjectData");
                        string xIndex = Query.GetmSubVarObjectDataValue(xObjectData, Query.SubVariableProperties.Index);
                        if (xIndex != "")
                        {
                            xIndex = xIndex.Substring(6);
                        }
                        if (!oItemIndices.ContainsKey(xName + xIndex))
                        {
                            oItemIndices.Add(xName + xIndex, xName + xIndex);
                            string xUNID = Query.GetmSubVarObjectDataValue(xObjectData, Query.SubVariableProperties.UNID);
                            if (xUNID != "")
                            {
                                xUNID = xUNID.Substring(5);
                            }
                            XmlNode oNode = oXML.CreateNode(XmlNodeType.Element, xName, "");

                            XmlAttribute oAtt = oXML.CreateAttribute("Index");
                            oAtt.Value = xIndex;
                            oNode.Attributes.SetNamedItem(oAtt);

                            oAtt = oXML.CreateAttribute("UNID");
                            oAtt.Value = xUNID;
                            oNode.Attributes.SetNamedItem(oAtt);

                            //GLOG 15830
                            string xText = "";
                            Text[] aText = oSubVar.Descendants<Text>().ToArray();
                            if (aText.Count() < 2)
                            {
                                xText = aText[0].InnerText;
                            }
                            else
                            {
                                //Append runs separated by delimiters, since oSubVar.InnerText will omit linebreaks
                                foreach (Text oSubText in aText)
                                {
                                    if (xText != "")
                                    {
                                        xText = xText + "\r\n";
                                    }
                                    xText = xText + oSubText.InnerText;
                                }
                            }
                            oNode.InnerText = xText;
                            oXML.DocumentElement.AppendChild(oNode);
                        }

                    }
                }
            }
            xDetailXml = oXML.DocumentElement.InnerXml;
            return xDetailXml;
        }
        /// <summary>
        /// returns an xml string representing the value of oVar in the document, where
        /// oVar is a tagless varaible with a detail grid that populates other variables
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private static string GetMasterVariableDetailValue(XmlVariable oVar,  SdtElement oTargetCC, bool bReorderEntitiesByLocation)
        {
            DateTime t0 = DateTime.Now;

            string xValue = "";
            string xSortedValue = "";

            if (oVar.ControlType == mpControlTypes.DetailGrid)
            {
                int iPos = -1;
                string xReserveValue = oVar.ReserveValue;
                XmlSegment oSegment = oVar.Segment;
                XmlForteDocument oMPDocument = oSegment.ForteDocument;

                //get names of "sub-variables" from this variable's RunVariableActions actions
                for (int i = 0; i < oVar.VariableActions.Count; i++)
                {
                    XmlVariableAction oAction = oVar.VariableActions[i];
                    if (oAction.Type == XmlVariableActions.Types.RunVariableActions)
                    {
                        //get sub-variable
                        string[] aParams = oAction.Parameters.Split(
                            LMP.StringArray.mpEndOfSubField);
                        string[] aVarNames = aParams[0].Split(',');
                        for (int v = 0; v < aVarNames.GetLength(0); v++)
                        {
                            //JTS 12/19/08: Trim any extra spaces from ends of string
                            string xVarName = aVarNames[v].Trim();
                            XmlVariable oSubVar = null;
                            //Variable might not be found if it's currently in a hidden block
                            try
                            {
                                oSubVar = oSegment.Variables.ItemFromName(xVarName);
                            }
                            catch { }

                            if (oSubVar != null)
                            {
                                //if the Expression parameter of the InsertDetail action evaluates
                                //empty due to a condition other that the value of the master variable,
                                //the document will not reflect the true value of this sub-variable -
                                //extract the current value from the ReserveValue property
                                string xPreserve = "";
                                string xParameters = "";
                                for (int j = 0; j < oSubVar.VariableActions.Count; j++)
                                {
                                    XmlVariableAction oSubAction = oSubVar.VariableActions[j];
                                    if (oSubAction.Type == XmlVariableActions.Types.InsertDetail)
                                    {
                                        xParameters = oSubAction.Parameters;
                                        break;
                                    }
                                }

                                if (xParameters != "")
                                {
                                    //get Expression parameter
                                    aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);
                                    if (aParams.GetUpperBound(0) > 6)
                                    {

                                        //JTS 12/19/08: Could be either [Variable__ or [Variable_
                                        //replace field code that returns value of master variable
                                        string xExpression = aParams[7].Replace("[Variable__" +
                                            oVar.Name + "]", "x");
                                        xExpression = xExpression.Replace("[Variable_" +
                                            oVar.Name + "]", "x");
                                        //evaluate
                                        xExpression = XmlExpression.Evaluate(xExpression, oSegment,
                                            oMPDocument);
                                        if (xExpression == "")
                                        {
                                            //value won't be inserted in the doc - get impacted fields
                                            //from Template parameter
                                            string xFields = "";
                                            string xTemplate = XmlExpression.Evaluate(aParams[2],
                                                oSegment, oMPDocument);
                                            iPos = xTemplate.IndexOf("[SubVar_");
                                            while (iPos > -1)
                                            {
                                                iPos += 8;
                                                int iPos2 = xTemplate.IndexOf("]", iPos);
                                                if (xFields != "")
                                                    xFields += "|";
                                                //JTS 12/19/08: Trim starting '_' to account for new [SubVar__xxx] format
                                                xFields += xTemplate.Substring(iPos, iPos2 - iPos).TrimStart(@"_".ToCharArray());
                                                iPos = xTemplate.IndexOf("[SubVar_", iPos2);
                                            }

                                            //extract values of these fields from current ReserveValue
                                            if (xFields != "")
                                            {
                                                string[] aFields = xFields.Split('|');
                                                for (int j = 0; j < aFields.Length; j++)
                                                {
                                                    xPreserve += ExtractDetailFieldValue(xReserveValue, aFields[j]);
                                                }
                                            }
                                        }
                                    }
                                }
                                //append
                                if (xPreserve != "")
                                    //use ReserveValue
                                    xValue += xPreserve;
                                else
                                {
                                    //get value from doc
                                    SdtElement oCC = null;
                                    //content controls
                                    if (oTargetCC != null)
                                    {
                                        //use tag only if it belongs to this subvariable
                                        string xTagID = Query.GetAttributeValue(oSegment.WPDoc, oTargetCC, "TagID");
                                        if (xTagID == oSubVar.Name)
                                            oCC = oTargetCC;
                                    }
                                    xValue += GetVariableDetailValue(oSubVar, xParameters, oCC, bReorderEntitiesByLocation);
                                }
                            }
                        }
                    }
                }
                //If no SubVariables were found, but there's a reserve value, use that
                if (xValue == "" && xReserveValue != "")
                {
                    xValue = xReserveValue;
                    //If ReserveValue has been refreshed from Tag, reserved XML characters
                    //might already be restored - ensure & and ' are always replaced
                    xValue = String.RestoreXMLChars(xValue);
                    xValue = xValue.Replace("&", "&amp;");
                    xValue = xValue.Replace("'", "&apos;");
                }
                //reorder by index
                int iIndex = 1;
                bool bFound = true;
                while (bFound)
                {
                    iPos = xValue.IndexOf("Index=" + "\"" + iIndex.ToString() + "\"");
                    bFound = (iPos > -1);
                    while (iPos > -1)
                    {
                        iPos = xValue.LastIndexOf('<', iPos);
                        int iPos2 = xValue.IndexOf("</", iPos);
                        iPos2 = xValue.IndexOf(">", iPos2);
                        xSortedValue += xValue.Substring(iPos, (iPos2 - iPos) + 1);
                        iPos = xValue.IndexOf("Index=" + "\"" + iIndex.ToString() + "\"", iPos2);
                    }
                    iIndex++;
                }
            }

            LMP.Benchmarks.Print(t0);

            return xSortedValue;
        }

        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by the InsertDetail action(s) of it or, if tagless, of the other
        /// variables it populates
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        public static string GetDetailValue(XmlVariable oVar)
        {
            return GetDetailValue(oVar, null, false);
        }
        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by the InsertDetail action(s) of it or, if tagless, of the other
        /// variables it populates
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        public static string GetDetailValue(XmlVariable oVar, SdtElement oTargetCC, bool bReorderEntitiesByLocation)
        {
            DateTime t0 = DateTime.Now;

            //get InsertDetail parameters
            string xParameters = "";
            for (int i = 0; i < oVar.VariableActions.Count; i++)
            {
                XmlVariableAction oAction = oVar.VariableActions[i];
                if (oAction.Type == XmlVariableActions.Types.InsertDetail)
                {
                    xParameters = oAction.Parameters;
                    break;
                }
            }

            //if variable does not have InsertDetail action, it may
            //be a tagless "master" variable
            string xValue = "";
            if (xParameters != "")
                xValue = GetVariableDetailValue(oVar, xParameters, oTargetCC, bReorderEntitiesByLocation);
            else if (oVar.Segment is LMP.Architect.Base.IStaticDistributedSegment)
                //GLOG 7921 - for labels and envelopes, always use reserve value
                xValue = oVar.ReserveValue;
            else
                xValue = GetMasterVariableDetailValue(oVar, oTargetCC, bReorderEntitiesByLocation);

            //GLOG 5344: Make sure reserved characters in value aren't handled as fieldcodes
            xValue = XmlExpression.MarkLiteralReservedCharacters(xValue);

            LMP.Benchmarks.Print(t0, oVar.Name + " value: " + xValue);

            return xValue;
        }
        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by the InsertReline action
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        public static string GetRelineValue(XmlVariable oVar)
        {
            //get InsertReline parameters
            string xParameters = "";
            for (int i = 0; i < oVar.VariableActions.Count; i++)
            {
                XmlVariableAction oAction = oVar.VariableActions[i];
                if (oAction.Type == XmlVariableActions.Types.InsertReline)
                {
                    xParameters = oAction.Parameters;
                    break;
                }
            }

            //exit if there's no InsertReline action or if the control isn't a RelineGrid
            if ((xParameters == "") || (oVar.ControlType != mpControlTypes.RelineGrid))
                return "";

            //get parameters for retrieving value
            XmlSegment oSegment = oVar.Segment;
            XmlForteDocument oMPDocument = oSegment.ForteDocument;
            WordprocessingDocument oDoc = oSegment.WPDoc;
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);
            string xControlProps = oVar.ControlProperties;
            int iPos = xControlProps.IndexOf("Style=") + 6;
            LMP.Controls.mpRelineStyles iStyle = (LMP.Controls.mpRelineStyles)Enum.Parse(typeof(LMP.Controls.mpRelineStyles), xControlProps.Substring(iPos, 1));
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml("<zzmpReline></zzmpReline>");
            SdtElement oCC = oVar.AssociatedContentControls[0];
            SdtElement oSubStandard = null;
            SdtElement oSubSpecial = null;
            if (iStyle != Controls.mpRelineStyles.ShowSpecialOnly)
            {
                oSubStandard = oCC.Descendants<SdtElement>().Where(c => Query.GetAttributeValue(oDoc, c, "Name") == "Standard").FirstOrDefault();
                if (oSubStandard != null)
                {
                    string x = oSubStandard.InnerXml;
                    int iFormat = 0;
                    string xDefault = "";
                    XmlAttribute oAtt = null;
                    //if there's an existing Standard subvar, get specified format and default
                    string xObjectData = Query.GetAttributeValue(oDoc, oSubStandard, "ObjectData");
                    if (xObjectData != "")
                    {
                        iFormat = Int32.Parse(Query.GetmSubVarObjectDataValue(xObjectData, Query.SubVariableProperties.Format).Substring(7));
                        xDefault = Query.GetmSubVarObjectDataValue(xObjectData, Query.SubVariableProperties.Default).Substring(8);
                    }
                    XmlNode oNode = oXML.DocumentElement.AppendChild(oXML.CreateNode(XmlNodeType.Element, "Standard", ""));
                    oNode.InnerText = oSubStandard.InnerText;
                    oAtt = oXML.CreateAttribute("Format");
                    oAtt.Value = iFormat.ToString();
                    oNode.Attributes.SetNamedItem(oAtt);
                    oAtt = oXML.CreateAttribute("Default");
                    oAtt.Value = xDefault;
                    oNode.Attributes.SetNamedItem(oAtt);
                }
            }
            if (iStyle != mpRelineStyles.ShowRelineOnly)
            {
                oSubSpecial = oCC.Descendants<SdtElement>().Where(c => Query.GetAttributeValue(oDoc, c, "Name") == "Special").FirstOrDefault();
                if (oSubSpecial != null)
                {
                    string x = oSubSpecial.InnerXml;
                    int iFormat = 0;
                    string xDefault = "";
                    XmlAttribute oAtt = null;
                    //if there's an existing Standard subvar, get specified format and default
                    string xObjectData = Query.GetAttributeValue(oDoc, oSubStandard, "ObjectData");
                    if (xObjectData != "")
                    {
                        iFormat = Int32.Parse(Query.GetmSubVarObjectDataValue(xObjectData, Query.SubVariableProperties.Format).Substring(7));
                        xDefault = Query.GetmSubVarObjectDataValue(xObjectData, Query.SubVariableProperties.Default).Substring(8);
                    }
                    XmlNode oNode = oXML.DocumentElement.AppendChild(oXML.CreateNode(XmlNodeType.Element, "Special", ""));
                    oAtt = oXML.CreateAttribute("Format");
                    oAtt.Value = iFormat.ToString();
                    oNode.Attributes.SetNamedItem(oAtt);
                    oAtt = oXML.CreateAttribute("Default");
                    oAtt.Value = xDefault;
                    oNode.Attributes.SetNamedItem(oAtt);
                    SdtElement[] aItems = oSubSpecial.Descendants<SdtElement>().Where(s => Query.GetBaseName(s) == "mSubVar").ToArray();
                    foreach (SdtElement oItem in aItems)
                    {
                        string xItemObjectData = Query.GetAttributeValue(oDoc, oItem, "ObjectData");
                        string xName = Query.GetAttributeValue(oDoc, oItem, "Name");
                        string xIndex = Query.GetmSubVarObjectDataValue(xItemObjectData, Query.SubVariableProperties.Index).Substring(6);
                        XmlNode oItemNode = oNode.AppendChild(oXML.CreateNode(XmlNodeType.Element, xName, ""));
                        oItemNode.InnerText = oItem.InnerText;
                        oAtt = oXML.CreateAttribute("Index");
                        oAtt.Value = xIndex;
                        oItemNode.Attributes.SetNamedItem(oAtt);
                    }
                }
            }
            return oXML.DocumentElement.InnerXml;
        }


        /// <summary>
        /// GLOG 2645: Set RuntimeControlValues and update ObjectData
        /// </summary>
        /// <param name="xValues"></param>
        internal void SetRuntimeControlValues(string xValues, bool bSaveDefinition)
        {
            SetStringPropertyValue(ref m_xRuntimeControlValues, xValues, 0);
            if (this.IsDirty && bSaveDefinition)
                this.Parent.Save(this);
        }
        public void SetRuntimeControlValues(string xValues)
        {
            SetRuntimeControlValues(xValues, true);
        }
        /// <summary>
        /// inserts mVar word tag at current selection
        /// </summary>
        /// <param name="range"></param>
        //public Word.XMLNode InsertAssociatedTag()
        //{
        //    try
        //    {
        //        //get tag for new mVar tag
        //        bool bIsNew = (this.TagID == "");
        //        string xTagID = this.Name;
        //        string xFullTagID = this.m_oSegment.FullTagID + "." + xTagID;
        //        this.TagID = xFullTagID;
        //        this.TagType = XmlForteDocument.TagTypes.Variable;

        //        //get object data for new mVar tag
        //        string xObjectData;
        //        if (bIsNew)
        //        {
        //            //get definition
        //            xObjectData = "VariableDefinition=" + this.ToString(true) + "|";
        //        }
        //        else
        //        {
        //            //get from node store, so as not to include optional properties
        //            //that aren't in existing tags
        //            try
        //            {
        //                xObjectData = this.Parent.Nodes.GetItemObjectData(xFullTagID);
        //            }
        //            catch
        //            {
        //                //Definition wasn't in Node Store - might be reinserting
        //                //as part of an InsertSegment action
        //                xObjectData = "VariableDefinition=" + this.ToString(true) + "|";
        //            }
        //        }

        //        //replace xml chars and trim leading pipe
        //        xObjectData = LMP.String.ReplaceXMLChars(xObjectData).TrimStart('|');

        //        //insert mVar tag with placeholder text

        //        //ignore word event handler that execute after an mVar
        //        //has been inserted - this should only be a problem in
        //        //versions of Word > 11, where we subscribe to XMLAfterInsert -
        //        //however, we don't conditionalize, just to be safe
        //        LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents bIgnoreWordEvents = XmlForteDocument.IgnoreWordXMLEvents;
        //        XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
        //        Word.XMLNode oNewNode = oDoc.InsertmVarAtSelection(xTagID,
        //            xObjectData, this.DisplayName);
        //        XmlForteDocument.IgnoreWordXMLEvents = bIgnoreWordEvents;

        //        //add tag to collection
        //        m_oForteDocument.Tags.Insert(oNewNode, !bIsNew);

        //        //refresh node store
        //        m_oSegment.RefreshNodes();

        //        //update tag parent part numbers
        //        this.TagParentPartNumbers = this.Parent.Nodes.GetItemPartNumbers(this.TagID);

        //        return oNewNode;
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString(
        //            "Error_CouldNotInsertWordTag"), oE);
        //    }
        //}
        ///// <summary>
        ///// inserts mVar content control at current selection
        ///// </summary>
        ///// <param name="range"></param>
        //public Word.ContentControl InsertAssociatedContentControl()
        //{
        //    try
        //    {
        //        //get tag for new mVar tag
        //        bool bIsNew = (this.TagID == "");
        //        string xTagID = this.Name;
        //        string xFullTagID = this.m_oSegment.FullTagID + "." + xTagID;
        //        this.TagID = xFullTagID;
        //        this.TagType = XmlForteDocument.TagTypes.Variable;

        //        //get object data for new mVar tag
        //        string xObjectData;
        //        if (bIsNew)
        //        {
        //            //get definition
        //            xObjectData = "VariableDefinition=" + this.ToString(true) + "|";
        //        }
        //        else
        //        {
        //            //get from node store, so as not to include optional properties
        //            //that aren't in existing tags
        //            try
        //            {
        //                xObjectData = this.Parent.Nodes.GetItemObjectData(xFullTagID);
        //            }
        //            catch
        //            {
        //                //Definition wasn't in Node Store - might be reinserting
        //                //as part of an InsertSegment action
        //                xObjectData = "VariableDefinition=" + this.ToString(true) + "|";
        //            }
        //        }

        //        //replace xml chars and trim leading pipe
        //        xObjectData = LMP.String.ReplaceXMLChars(xObjectData).TrimStart('|');

        //        //insert mVar tag with placeholder text

        //        //ignore word event handler that execute after an mVar
        //        //has been inserted - this should only be a problem in
        //        //versions of Word > 11, where we subscribe to XMLAfterInsert -
        //        //however, we don't conditionalize, just to be safe
        //        LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents bIgnoreWordEvents = XmlForteDocument.IgnoreWordXMLEvents;
        //        XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
        //        Word.ContentControl oCC = oDoc.InsertmVarAtSelection_CC(xTagID,
        //            xObjectData, this.DisplayName);
        //        XmlForteDocument.IgnoreWordXMLEvents = bIgnoreWordEvents;

        //        //add tag to collection
        //        m_oForteDocument.Tags.Insert_CC(oCC, !bIsNew);

        //        //refresh node store
        //        m_oSegment.RefreshNodes();

        //        //update tag parent part numbers
        //        this.TagParentPartNumbers = this.Parent.Nodes.GetItemPartNumbers(this.TagID);

        //        return oCC;
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString(
        //            "Error_CouldNotInsertWordTag"), oE);
        //    }
        //}
        /// <summary>
        /// creates the associated control for the Variable -
        /// returns the control (for convenience)
        /// </summary>
        /// <param name="oParentControl">the control that hosts this control</param>
        /// <returns></returns>
        public IControl CreateAssociatedControl(System.Windows.Forms.Control oParentControl)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                System.Windows.Forms.Control oCtl = null;
                //create new control
                if (this.IsMultiValue)
                {
                    switch (this.ControlType)
                    {
                        case mpControlTypes.Checkbox:
                        case mpControlTypes.Combo:
                        case mpControlTypes.DropdownList:
                        case mpControlTypes.MultilineCombo:
                        case mpControlTypes.MultilineTextbox:
                        case mpControlTypes.Textbox:
                            //Use MultiValueControl, configured for the selected control type
                            oCtl = new LMP.Controls.MultiValueControl(this.ControlType);
                            break;
                        default:
                            oCtl = LMP.Architect.Base.Application.CreateControl(this.ControlType, oParentControl);
                            break;
                    }
                }
                else
                {
                    oCtl = LMP.Architect.Base.Application.CreateControl(this.ControlType, oParentControl);
                }
                
                try
                {
                    IControl oIControl = (IControl)oCtl;

                    //set the control as the associated control
                    this.AssociatedControl = oIControl;

                    //set up control by setting control properties
                    SetAssociatedControlProperties();

                    //do any control-specific setup of control
                    oIControl.ExecuteFinalSetup();

                    return oIControl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                    LMP.Benchmarks.Print(t0);
                }

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    this.ControlType.ToString(), oE);
            }
        }
        /// <summary>
        /// sets the properties of the associated control
        /// </summary>
        public void SetAssociatedControlProperties()
        {
            DateTime t0 = DateTime.Now;
            
            System.Windows.Forms.Control oCtl = (System.Windows.Forms.Control)this.AssociatedControl;
            System.Type oCtlType = oCtl.GetType();

            if (oCtl is Chooser)
            {
                Chooser oChooser = (Chooser)oCtl;

                //set target object type id and target object id
                oChooser.TargetObjectID = this.Segment.ID1;
                oChooser.TargetObjectType = this.Segment.TypeID;
            }
            else if (oCtl is MultiValueControl)
            {
                //TODO: OpenXML rewrite
                //if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                //{
                //    //Configure control to display single or multiple values
                //    ((MultiValueControl)oCtl).NumberOfValues = this.AssociatedWordTags.GetLength(0);
                //}
                //else
                //{
                //    //Configure control to display single or multiple values
                //    ((MultiValueControl)oCtl).NumberOfValues = this.AssociatedContentControls.GetLength(0);
                //}
            }

            //set up control properties-
            //get control properties from variable
            string[] aProps = null;

            if (this.ControlProperties != "")
                aProps = this.ControlProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);

                //evaluate control prop value, which may be a MacPac expression
                xValue = XmlExpression.Evaluate(xValue, this.Segment, this.ForteDocument);

                PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType,
                        LMP.Culture.USEnglishCulture);

                DateTime t1 = DateTime.Now;
                oPropInfo.SetValue(oCtl, oValue, null);
                LMP.Benchmarks.Print(t1, "SetPropertyValue", xName);
            }
            IControl oICtl = (IControl)oCtl;

            LMP.Benchmarks.Print(t0);
        }
        //public bool SetTagState(bool bSetTagged, bool bDeleteVariableText, Word.Range oRng)
        //{
        //    return SetTagState(bSetTagged, bDeleteVariableText, oRng, false);
        //}
        ///// <summary>
        ///// Moves variable definition from mVar tag to mSeg
        ///// </summary>
        ///// <param name="bDeleteVariableText"></param>
        //public bool SetTagState(bool bSetTagged, bool bDeleteVariableText, Word.Range oRng, bool bForceValid)
        //{
        //    DateTime t0 = DateTime.Now;

        //    Trace.WriteNameValuePairs("bSetTagged", bSetTagged, "bDeleteVariableText",
        //        bDeleteVariableText);
        //    if (!bSetTagged && this.TagType == XmlForteDocument.TagTypes.None)
        //    {
        //        // Variable is already tagless, do nothing
        //        return false;
        //    }
        //    else if (bSetTagged && this.TagType == XmlForteDocument.TagTypes.Variable)
        //    {
        //        // Variable already has tag, no dothing
        //        return false;
        //    }
        //    else if (bSetTagged)
        //    {
        //        LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents bIgnoreEvents = XmlForteDocument.IgnoreWordXMLEvents;
        //        XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
        //        try
        //        {
        //            LMP.Forte.MSWord.TagInsertionValidityStates iValid = 0;

        //            if (this.Segment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
        //            {
        //                iValid = oDoc.ValidateTagInsertion(
        //                    oRng, this.Segment.PrimaryWordTag, LMP.Forte.MSWord.TagTypes.Variable);
        //            }
        //            else
        //            {
        //                iValid = oDoc.ValidateContentControlInsertion(
        //                    oRng, this.Segment.PrimaryContentControl, LMP.Forte.MSWord.TagTypes.Variable);
        //            }

        //            if (iValid != LMP.Forte.MSWord.TagInsertionValidityStates.Valid && !bForceValid)
        //            {
        //                throw new LMP.Exceptions.XMLNodeException(LMP.Resources.GetLangString("Msg_InvalidTagInsertionLocation"));
        //            }
        //            this.TagType = XmlForteDocument.TagTypes.Variable;
        //            //remove variable def from mSeg object data
        //            string xObjectData = this.Segment.Nodes.GetItemObjectData(this.Segment.FullTagID);
        //            int iPos1 = xObjectData.IndexOf("VariableDefinition=" + this.ID);
        //            if (iPos1 > -1)
        //            {
        //                int iPos2 = xObjectData.IndexOf("|", iPos1 + 1) + 1;
        //                string xNew = xObjectData.Substring(0, iPos1) + xObjectData.Substring(iPos2);
        //                this.Segment.Nodes.SetItemObjectData(this.Segment.FullTagID, xNew);
        //            }
                    
        //            //Insert Tag only if Range has been specified
        //            if (oRng != null)
        //            {
        //                oRng.Select();
        //                if (this.Segment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
        //                    this.InsertAssociatedTag();
        //                else
        //                    this.InsertAssociatedContentControl();
        //            }
        //            return true;
        //        }
        //        catch
        //        {
        //            return false;
        //        }
        //        finally
        //        {
        //            XmlForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;
        //        }
        //    }
        //    else if (!bSetTagged)
        //    {
        //        //delete mVar and add variable def to mSeg

        //        LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents bIgnoreEvents = XmlForteDocument.IgnoreWordXMLEvents;
        //        XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
        //        try
        //        {
        //            if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
        //            {
        //                if (bDeleteVariableText && this.AssociatedWordTags[0].BaseName == "mVar")
        //                {
        //                    object oMissing = System.Reflection.Missing.Value;
        //                    this.AssociatedWordTags[0].Range.Delete(ref oMissing, ref oMissing);
        //                }
        //            }
        //            else
        //            {
        //                if (bDeleteVariableText && String.GetBoundingObjectBaseName(
        //                    this.AssociatedContentControls[0].Tag) == "mVar")
        //                {
        //                    object oMissing = System.Reflection.Missing.Value;
        //                    this.AssociatedContentControls[0].Range.Delete(ref oMissing, ref oMissing);
        //                }
        //            }

        //            string xVarObjectData = "VariableDefinition=" + this.ToString(true) + "|";
        //            string xSegObjectData = null;

        //            if (this.m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
        //            {
        //                //JTS 6/13/13: Use PrimaryBookmark for Segment if available
        //                if (this.Segment.PrimaryBookmark != null)
        //                {
        //                    xSegObjectData = this.m_oForteDocument.GetAttributeValue(
        //                         this.Segment.PrimaryBookmark, "ObjectData");
        //                }
        //                else
        //                {
        //                    xSegObjectData = this.m_oForteDocument.GetAttributeValue(
        //                         this.Segment.PrimaryWordTag, "ObjectData");
        //                }

        //                //Remove mVar tag
        //                this.AssociatedWordTags[0].Delete();

        //                //append Variable objectdata to existing PrimaryWordTag object data
        //                if (this.Segment.PrimaryBookmark != null)
        //                {
        //                    oDoc.SetAttributeValue_Bookmark(this.Segment.PrimaryBookmark,
        //                        "ObjectData", xSegObjectData + xVarObjectData,
        //                        LMP.Data.Application.EncryptionPassword);
        //                }
        //                else
        //                {
        //                    oDoc.SetAttributeValue(this.Segment.PrimaryWordTag,
        //                        "ObjectData", xSegObjectData + xVarObjectData,
        //                        LMP.Data.Application.EncryptionPassword);
        //                }
        //            }
        //            else
        //            {
        //                if (this.Segment.PrimaryBookmark != null)
        //                {
        //                    xSegObjectData = this.m_oForteDocument.GetAttributeValue(
        //                         this.Segment.PrimaryBookmark, "ObjectData");
        //                }
        //                else
        //                {
        //                    xSegObjectData = this.m_oForteDocument.GetAttributeValue(
        //                         this.Segment.PrimaryContentControl, "ObjectData");
        //                }
        //                //Remove mVar cc
        //                this.AssociatedContentControls[0].Delete(false);

        //                //append Variable objectdata to existing PrimaryWordTag object data
        //                if (this.Segment.PrimaryBookmark != null)
        //                {
        //                    oDoc.SetAttributeValue_Bookmark(this.Segment.PrimaryBookmark,
        //                        "ObjectData", xSegObjectData + xVarObjectData,
        //                        LMP.Data.Application.EncryptionPassword);
        //                }
        //                else
        //                {
        //                    oDoc.SetAttributeValue_CC(this.Segment.PrimaryContentControl,
        //                        "ObjectData", xSegObjectData + xVarObjectData,
        //                        LMP.Data.Application.EncryptionPassword);
        //                }
        //            }

        //            this.TagType = XmlForteDocument.TagTypes.None;

        //            //update segment tag and refresh node store
        //            this.Segment.ForteDocument.RefreshTags();
        //            this.Segment.RefreshNodes();
        //            return true;
        //        }
        //        catch
        //        {
        //            return false;
        //        }
        //        finally
        //        {
        //            XmlForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;
        //            LMP.Benchmarks.Print(t0);
        //        }

        //    }
        //    else
        //    {
        //        return false;
        //    }

        //}
        /// <summary>
        /// sets the private validation field to true iff
        /// the validation condition is satisfied
        /// </summary>
        public void SetValidationField()
        {
            DateTime t0 = DateTime.Now;

            if (this.ValidationCondition == "")
                //there is no condition - value is always valid
                m_bValueIsValid = true;
            else
            {
                string xValue = m_xValue;
                //TODO: OpenXML rewrite
                //if (xValue == null)
                //    xValue = GetValueFromSource();

                //GLOG 2151: Mark reserved expression characters to be handled as literals in expression
                xValue = XmlExpression.MarkLiteralReservedCharacters(xValue);
                //evaluate [MyValue] field code
                //string xCondition = this.ValidationCondition.Replace("[MyValue]", xValue);
                string xCondition = XmlFieldCode.EvaluateMyValue(this.ValidationCondition, xValue);

                LMP.Trace.WriteNameValuePairs("xCondition", xCondition, "xValue", xValue);

                bool bIsValid = false;

                if (xValue != "zzmpTempValue" && xValue != this.DisplayName)
                {
                    //finish evaluating condition
                    string xIsValid = XmlExpression.Evaluate(xCondition, this.Segment, this.ForteDocument);

                    try
                    {
                        //test to see if condition evaluates to a boolean
                        bIsValid = Boolean.Parse(xIsValid);
                    }
                    catch
                    {
                        //condition doesn't evaluate to a boolean - alert
                        throw new LMP.Exceptions.ExpressionException(
                            LMP.Resources.GetLangString("Error_InvalidValidationCondition") +
                            this.ValidationCondition);
                    }
                }

                m_bValueIsValid = bIsValid;
            }

            this.m_bValidityTested = true;
            LMP.Benchmarks.Print(t0, this.DisplayName);
        }
        /// <summary>
        /// set the variable value to the updated contact detail
        /// </summary>
        /// <param name="xContactDetailXML"></param>
        /// <returns></returns>
        public bool UpdateContactDetail()
        {
            bool bDetailUpdated = false;

            if (this.CIContactType == 0)
                //variable is not configured to 
                //have contact detail
                return false;

            //cycle through each item of contact detail-
            //if it has a UNID replace it with updated detail-
            //else leave as is
            string xUpdatedDetail = null;
            Match oMatch = null;
            int j = 1;
            ArrayList oUNIDs = new ArrayList();
            string xMatch = null;
            string xContactDetailXML = this.Value;

            try
            {
                do
                {
                    //get entity UNID
                    string xPattern = "Index=\"" + j.ToString() + "\" UNID=\".*?\"";
                    oMatch = Regex.Match(xContactDetailXML, xPattern);

                    if (oMatch == null || oMatch.Value == "")
                        //there are no more matches
                        break;

                    //parse UNID from match
                    xMatch = oMatch.Value;
                    int iPos = xMatch.IndexOf("UNID=");
                    xMatch = xMatch.Substring(iPos + 6, xMatch.Length - (iPos + 7));

                    if (xMatch != "0")
                    {
                        //at least one match
                        bDetailUpdated = true;

                        //entity has a UNID - get the contact
                        //associated with the UNID
                        string[] aUNIDS = { xMatch };

                        ICContacts oContacts = null;
                        try
                        {
                            oContacts = XmlContacts.GetContactsFromUNIDs(this, aUNIDS);
                            System.Windows.Forms.Application.DoEvents();
                        }
                        catch { }

                        //GLOG 15812
                        if (oContacts != null && oContacts.Count() == 1)
                        {
                            object oIndex = 1;
                            ICContact oContact = oContacts.Item(oIndex);

                            //contact was found - get matching nodes
                            xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                                "\" UNID=\"[^<>]*\">[^<>]*</[^<>]*>";
                            MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);
                            string xItemXML = "";
                            foreach (Match oElement in oMatches)
                                xItemXML += oElement.Value;
                            XmlDocument oXML = new XmlDocument();
                            oXML.LoadXml("<zzmpD>" + xItemXML + "</zzmpD>");
                            XmlNodeList oNodeList = oXML.ChildNodes[0].ChildNodes;
                            //Value may contain CI and non-CI fields -
                            //Leave non-CI values unchanged
                            foreach (XmlNode oNode in oNodeList)
                            {
                                string xCIFormat = "<" + oNode.Name + ">";
                                string xVal = "";
                                xCIFormat = xCIFormat.Replace("<MAILINGDETAIL>", "<FULLNAMEWITHPREFIXANDSUFFIX>\r\n<COMPANY>\r\n<COREADDRESS>");

                                //GLOG : 8031 : ceh
                                //fill in CoreAddress token
                                xVal = xCIFormat.Replace("<COREADDRESS>", LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));
                                //fill in other details
                                xVal = oContact.GetDetail(xVal, true);
                                //Field is a CI Detail item, replace existing value
                                if (xVal != xCIFormat)
                                {
                                    oNode.InnerText = xVal;
                                }
                                xUpdatedDetail += oNode.OuterXml;
                            }
                        }
                        else
                        {
                            //GLOG : 15890 : ceh
                            //contact was not found - use existing detail
                            xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                                "\" UNID=\"[^<>]*\">[^<>]*</[^<>]*>";

                            ////contact was not found - use existing detail
                            //xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                            //    "\" UNID=\"0\">[^<>]*</[^<>]*>";

                            MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);

                            foreach (Match oElement in oMatches)
                                xUpdatedDetail += oElement.Value;
                        }
                    }
                    else
                    {
                        //the entity does not have a UNID -
                        //use existing detail
                        xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                            "\" UNID=\"0\">[^<>]*</[^<>]*>";
                        MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);

                        foreach (Match oElement in oMatches)
                            xUpdatedDetail += oElement.Value; ;
                    }

                    j++;
                } while (oMatch != null && oMatch.Value != "");

                //GLOG 3197: Don't attempt to update variable whose value doesn't contain Contact XML
                if (xUpdatedDetail != null && !LMP.String.CompareBooleanCaseInsensitive(this.Value, xUpdatedDetail))
                    //updated detail is different - set variable value
                    this.SetValue(xUpdatedDetail, true);
            }
            catch
            {
                //some general problem occured - alert 
                //user that contacts will not be updated
                //MessageBox.Show(LMP.Resources.GetLangString("Msg_CouldNotUpdateVariableContactDetail") + this.DisplayName,
                //    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                //    MessageBoxIcon.Exclamation);
            }

            //variable contains at least one updatable contact detail
            return bDetailUpdated;

        }

        /// <summary>
        /// returns TRUE if this variable is the UI source variable for
        /// the variable with the specified name
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public bool IsDependentVariable(string xName)
        {
            XmlVariable oVar = null;
            try
            {
                oVar = this.Segment.Variables.ItemFromName(xName);
            }
            catch { }

            if (oVar == null)
                return false;
            else
                return (oVar.UISourceVariable == this);
        }

        /// <summary>
        /// converts detail grid reserve value to xml if necessary - we now write it to the
        /// node store as a delimited string to reduce the size of the object data attribute
        /// in Word - see GLOG 2355
        /// </summary>
        /// <param name="xValue"></param>
        /// <returns></returns>
        internal string DetailReserveValueToXML(string xValue)
        {
            //exit if it's a different control type or value is already xml
            if ((this.ControlType != mpControlTypes.DetailGrid) || xValue.Contains("</") ||
                System.String.IsNullOrEmpty(xValue))
                return xValue;

            //get field names
            string[] aFields = this.GetDetailGridFieldNames();

            //get entities
            string[] aEntities = xValue.Split(LMP.StringArray.mpEndOfSubField);

            //create xml document
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml("<zzmpD></zzmpD>");

            //cycle through entities
            for (int i = 0; i < aEntities.Length; i++)
            {
                string[] aItems = aEntities[i].Split(
                    LMP.StringArray.mpEndOfSubValue);

                for (int j = 2; j < aItems.Length; j++)
                {
                    //create node only if field has a value
                    if (aItems[j] != "")
                    {
                        //create node
                        XmlNode oFieldNode = oXML.CreateNode(XmlNodeType.Element,
                            aFields[j - 2], "");
                        oFieldNode.InnerText = aItems[j];

                        //create index attribute
                        XmlAttribute oA = oXML.CreateAttribute("Index");
                        oA.InnerText = aItems[0];
                        oFieldNode.Attributes.Append(oA);

                        //create unid attribute
                        oA = oXML.CreateAttribute("UNID");
                        oA.InnerText = aItems[1];
                        oFieldNode.Attributes.Append(oA);

                        //add node
                        oXML.DocumentElement.AppendChild(oFieldNode);
                    }
                }
            }

            string xItems = oXML.OuterXml;
            xItems = xItems.Replace("<zzmpD>", "");
            xItems = xItems.Replace("</zzmpD>", "");
            xItems = xItems.TrimEnd('\r', '\n');
            return xItems;
        }

        /// <summary>
        /// converts detail grid reserve value to a delimited string if necessary - this is
        /// done to reduce the size of the object data attribute in Word - see GLOG 2355
        /// </summary>
        /// <param name="xValue"></param>
        /// <returns></returns>
        internal string DetailReserveValueToDelimitedString(string xValue)
        {
            //exit if it's a different control type or value is not xml
            if ((this.ControlType != mpControlTypes.DetailGrid) || !xValue.Contains("</"))
                return xValue;

            //get field names
            string[] aFields = this.GetDetailGridFieldNames();

            StringBuilder oSB = new StringBuilder();
            XmlDocument oXML = new System.Xml.XmlDocument();
            oXML.LoadXml(string.Concat("<zzmpD>", xValue, "</zzmpD>"));
            XmlNodeList oNodeList;
            int iIndex = 0;
            do
            {
                iIndex++;
                string xIndex = iIndex.ToString();
                oNodeList = oXML.DocumentElement.SelectNodes(
                    "(/zzmpD/*|/zzmpD/*)[@Index='" + xIndex + "']");
                if (oNodeList.Count > 0)
                {
                    //append entity separator
                    if (oSB.Length > 0)
                        oSB.Append(LMP.StringArray.mpEndOfSubField);

                    //get UNID, if one exists
                    string xUNID = "";
                    try
                    {
                        xUNID = oNodeList[0].Attributes.GetNamedItem("UNID").Value;
                    }
                    catch { }
                    if (xUNID == "")
                        xUNID = "0";

                    //append index and UNID
                    oSB.AppendFormat("{0}�{1}�", xIndex, xUNID);

                    //append the field values
                    for (int i = 0; i < aFields.Length; i++)
                    {
                        string xFieldValue = "";
                        for (int j = 0; j < oNodeList.Count; j++)
                        {
                            if (oNodeList[j].Name.ToUpper() == aFields[i].ToUpper())
                            {
                                xFieldValue = oNodeList[j].InnerText;
                                break;
                            }
                        }
                        oSB.AppendFormat("{0}�", xFieldValue);
                    }

                    oSB.Remove(oSB.Length - 1, 1);
                }
            } while (oNodeList.Count > 0);

            return oSB.ToString();
        }
        #endregion
		#region *********************private members*********************
		/// <summary>
		/// returns the collection of variable actions 
		/// assigned to this variable as a delimited string
		/// </summary>
		/// <returns></returns>
		private string VariableActionsToString()
		{
			//get actions string
			string xActions = "";

			//cycle through actions, converting object to string
			//and adding it to the actions string
			for(int i=0; i<this.VariableActions.Count; i++)
			{
				XmlVariableAction oAction = this.VariableActions[i];

				//convert object to string
				StringBuilder oSB = new StringBuilder();
				oSB.AppendFormat("{0}�{1}�{2}�{3}�{4}", oAction.ExecutionIndex,
					oAction.LookupListID, oAction.ExecutionCondition, 
					(int) oAction.Type, oAction.Parameters);

				//add to actions string
				xActions += oSB.ToString();

				//add record separator if not last action
				if(i < this.VariableActions.Count - 1)
					xActions += StringArray.mpEndOfRecord;
			}

			return xActions;
		}
		/// <summary>
		/// returns the collection of control actions
		/// assigned to this variable as a delimited string
		/// </summary>
		/// <returns></returns>
		private string ControlActionsToString()
		{
			//get actions string
			string xActions = "";

			//cycle through actions, converting object to string
			//and adding it to the actions string
			for(int i=0; i<this.ControlActions.Count; i++)
			{
				XmlControlAction oAction = this.ControlActions[i];

				//convert object to string
				StringBuilder oSB = new StringBuilder();
				oSB.AppendFormat("{0}�{1}�{2}�{3}�{4}", oAction.ExecutionIndex,
					oAction.ExecutionCondition, (int) oAction.Event,
					(int) oAction.Type, oAction.Parameters);

				//add to actions string
				xActions += oSB.ToString();

				//add record separator if not last action
				if(i < this.ControlActions.Count - 1)
					xActions += StringArray.mpEndOfRecord;
			}

			return xActions;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="xVar"></param>
		/// <param name="xVal"></param>
		/// <param name="iMaxLength">maximum length of string - specify 0 for an unlimited length</param>
        //private void SetStringPropertyValue(ref string xVar, string xVal, int iMaxLength)
        //{
        //    if(xVar != xVal)
        //    {
        //        if(iMaxLength > 0 && xVal.Length > iMaxLength)
        //        {
        //            throw new LMP.Exceptions.DataException(
        //                Resources.GetLangString("Error_PropertyCharacterOverflow"));
        //        }

        //        xVar = xVal;
        //        this.IsDirty = true;
        //    }
        //}

        ///// <summary>
        ///// sets the value of the specified string variable to the specified value, with a maximum length of 255 characters.
        ///// </summary>
        ///// <param name="xVar"></param>
        ///// <param name="xVal"></param>
        //private void SetStringPropertyValue(ref string xVar, string xVal)
        //{
        //    SetStringPropertyValue(ref xVar, xVal, 255);
        //}

        ///// <summary>
        ///// sets the value of the specified integer variable to the specified value
        ///// </summary>
        ///// <param name="xVar"></param>
        ///// <param name="xVal"></param>
        //private void SetIntPropertyValue(ref int iVar, int iVal)
        //{
        //    if(iVar != iVal)
        //    {
        //        iVar = iVal;
        //        this.IsDirty = true;
        //    }
        //}

        ///// <summary>
        ///// sets the value of the specified byte variable to the specified value
        ///// </summary>
        ///// <param name="xVar"></param>
        ///// <param name="xVal"></param>
        //private void SetBytePropertyValue(ref byte byVar, byte byVal)
        //{
        //    if(byVar != byVal)
        //    {
        //        byVar = byVal;
        //        this.IsDirty = true;
        //    }
        //}

        ///// <summary>
        ///// sets the value of the specified short variable to the specified value
        ///// </summary>
        ///// <param name="xVar"></param>
        ///// <param name="xVal"></param>
        //private void SetShortPropertyValue(ref short shVar, short shVal)
        //{
        //    if(shVar != shVal)
        //    {
        //        shVar = shVal;
        //        this.IsDirty = true;
        //    }
        //}

        ///// <summary>
        ///// sets the value of the specified boolean variable to the specified value
        ///// </summary>
        ///// <param name="xVar"></param>
        ///// <param name="xVal"></param>
        //private void SetBoolPropertyValue(ref bool bVar, bool bVal)
        //{
        //    if(bVar != bVal)
        //    {
        //        bVar = bVal;
        //        this.IsDirty = true;
        //    }
        //}

        internal void SavePreferenceIfNecessary()
        {
            //GLOG 3627: Don't save Temp value as a preference
            if (this.Value == "zzmpTempValue")
                return;

            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("this.DefaultValue", this.DefaultValue);

            if (System.String.IsNullOrEmpty(this.DefaultValue))
                return;
            else if (!XmlExpression.ContainsPreferenceCode(this.DefaultValue,
                XmlExpression.PreferenceCategories.User))
                return;

            //check default value for user preference field code
            int iPos = this.DefaultValue.ToUpper().IndexOf("PREFERENCE_");

            //get field code name
            string xFieldCode = this.DefaultValue.Substring(1, iPos + 9).ToUpper();

            //get target segment - field code may reference a parent, child, or sibling
            XmlSegment oTarget = this.Segment.GetReferenceTarget(ref xFieldCode);

            //get key
            string xKey = this.DefaultValue.Substring(iPos + 11).TrimStart('_');
            xKey = xKey.TrimEnd(']');

            switch (xFieldCode)
            {
                case "OBJECTPREFERENCE":
                    KeySet.SetKeyValue(xKey, this.Value, mpKeySetTypes.UserObjectPref,
                       oTarget.ID1.ToString(), LMP.Data.Application.User.ID, 0,
                       oTarget.Culture);
                    break;
                case "TYPEPREFERENCE":
                    //GLOG 5239 (dm) - removed reference to oTarget.Definition
                    KeySet.SetKeyValue(xKey, this.Value, mpKeySetTypes.UserTypePref,
                        ((int)oTarget.TypeID).ToString(),
                        LMP.Data.Application.User.ID, 0, oTarget.Culture);
                    break;
                case "APPLICATIONPREFERENCE":
                    KeySet.SetKeyValue(xKey, this.Value, mpKeySetTypes.UserAppPref,
                        "0", LMP.Data.Application.User.ID, 0, oTarget.Culture);
                    break;
            }

            LMP.Benchmarks.Print(t0);
        }


        /// <summary>
        /// returns an xml string representing the value of oVar in the document as
        /// defined by xActionParameters
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xActionParameters"></param>
        /// <returns></returns>
        //private static string GetVariableDetailValue(XmlVariable oVar, string xActionParameters,
        //    Word.XMLNode oTargetTag, Word.ContentControl oTargetCC, bool bReorderEntitiesByLocation)
        //{
        //    DateTime t0 = DateTime.Now;

        //    //get separators from action parameters
        //    XmlSegment oSegment = oVar.Segment;
        //    XmlForteDocument oForteDoc = oSegment.ForteDocument;
        //    string[] aParams = xActionParameters.Split(LMP.StringArray.mpEndOfSubField);
        //    string xItemSeparator = XmlExpression.Evaluate(aParams[0], oSegment, oForteDoc);
        //    string xEntitySeparator = XmlExpression.Evaluate(aParams[1], oSegment, oForteDoc);
        //    //GLOG 4651: Get Alternate Format type to determine how entities inside table cells should be handled
        //    XmlVariableAction.AlternateFormatTypes iAltFormat = XmlVariableAction.AlternateFormatTypes.None;
        //    try
        //    {
        //        iAltFormat = (XmlVariableAction.AlternateFormatTypes)Int32.Parse(aParams[3]);
        //    }
        //    catch { }

        //    //get fields
        //    string xFields = "";
        //    string xTargetFields = "|";
        //    string xControlProps = "";
        //    mpControlTypes oControlType = oVar.ControlType;

        //    if ((oControlType != mpControlTypes.DetailList) &&
        //        (oControlType != mpControlTypes.DetailGrid))
        //    {
        //        //get control variable
        //        XmlVariable oUISourceVar = oVar.UISourceVariable;
        //        oControlType = oUISourceVar.ControlType;
        //        if ((oControlType == mpControlTypes.DetailList) ||
        //            (oControlType == mpControlTypes.DetailGrid))
        //            xControlProps = oUISourceVar.ControlProperties;

        //        //get target fields from template
        //        string xTemplate = XmlExpression.Evaluate(aParams[2], oSegment, oForteDoc);
        //        int iPos = xTemplate.IndexOf("[SubVar_");
        //        while (iPos != -1)
        //        {
        //            iPos = iPos + 8;
        //            int iPos2 = xTemplate.IndexOf(']', iPos);
        //            if (iPos2 == -1)
        //            {
        //                //this is only ever the case due to a bug
        //                //in Expression.EvaluateOperation()
        //                break;
        //            }
        //            //JTS 12/19/08: Fieldcode may use [SubVar__, so trim any starting '_' character
        //            xTargetFields = xTargetFields + xTemplate.Substring(
        //                iPos, iPos2 - iPos).TrimStart(@"_".ToCharArray()) + "|";
        //            iPos = xTemplate.IndexOf("[SubVar_", iPos2);
        //        }

        //        if ((xControlProps == "") || (xTargetFields == "|"))
        //            return "";
        //    }
        //    else
        //        xControlProps = oVar.ControlProperties;

        //    if (oControlType == mpControlTypes.DetailList)
        //    {
        //        //list
        //        string xName = "Item";
        //        string xRows = "1";

        //        //get ci token if specified
        //        int iPos = xControlProps.IndexOf("CIToken=<");
        //        if (iPos != -1)
        //        {
        //            iPos += 9;
        //            int iPos2 = xControlProps.IndexOf(">", iPos);
        //            xName = xControlProps.Substring(iPos, iPos2 - iPos);
        //        }

        //        //get rows per item if specified
        //        iPos = xControlProps.IndexOf("RowsPerItem=");
        //        if (iPos != -1)
        //        {
        //            iPos += 12;
        //            int iPos2 = xControlProps.IndexOf(StringArray.mpEndOfSubValue, iPos);
        //            if (iPos2 == -1)
        //                iPos2 = xControlProps.IndexOf(StringArray.mpEndOfValue, iPos);
        //            xRows = xControlProps.Substring(iPos, iPos2 - iPos);
        //        }

        //        xFields = xName + StringArray.mpEndOfValue + xRows;
        //    }
        //    else if (oControlType == mpControlTypes.DetailGrid)
        //    {
        //        //grid - parse config string to get field names and # of rows
        //        int iPos = xControlProps.IndexOf("ConfigString=") + 13;
        //        int iPos2 = xControlProps.IndexOf(StringArray.mpEndOfSubValue, iPos);
        //        if (iPos2 == -1)
        //            iPos2 = xControlProps.IndexOf(StringArray.mpEndOfValue, iPos);
        //        string xConfigString = xControlProps.Substring(iPos, iPos2 - iPos);
        //        string[] aConfig = xConfigString.Split(StringArray.mpEndOfSubField);
        //        byte bytFieldCount = (byte)(aConfig.Length / 5);
        //        int iOffset = 0;
        //        for (short sh = 1; sh <= bytFieldCount; sh++)
        //        {
        //            string xField = aConfig[iOffset + 1];
        //            if ((xTargetFields == "|") ||
        //                (xTargetFields.ToUpper().IndexOf("|" + xField.ToUpper() + "|") > -1))
        //            {
        //                xFields = xFields + xField + StringArray.mpEndOfValue;
        //                if (LMP.String.IsNumericInt32(aConfig[iOffset + 4]))
        //                    //get number of rows
        //                    xFields = xFields + aConfig[iOffset + 4] + StringArray.mpEndOfField;
        //                else
        //                    //TODO: use list name to get number of rows - for now, assume 1
        //                    xFields = xFields + "1" + StringArray.mpEndOfField;
        //            }
        //            iOffset = iOffset + 5;
        //        }
        //        xFields = xFields.Substring(0, xFields.Length - 1);
        //    }
        //    else
        //        return "";

        //    //get value from associated tags -
        //    //for distributed detail, value will be concatenation of all associated nodes
        //    object oVarNodes = null;
        //    if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
        //    {
        //        //xml tags
        //        Word.XMLNode[] oWordNodes = null;
        //        if (oVar.IsMultiValue)
        //            oWordNodes = oVar.AssociatedWordTags;
        //        else
        //        {
        //            //GLOG 968 (dm) - use specified tag if provided; otherwise use first one
        //            if (oTargetTag == null)
        //                oTargetTag = oVar.AssociatedWordTags[0];
        //            oWordNodes = new Word.XMLNode[1] { oTargetTag };
        //        }
        //        oVarNodes = oWordNodes;
        //    }
        //    else
        //    {
        //        //content controls
        //        Word.ContentControl[] oCCs = null;
        //        if (oVar.IsMultiValue)
        //            oCCs = oVar.AssociatedContentControls;
        //        else
        //        {
        //            //GLOG 968 (dm) - use specified tag if provided; otherwise use first one
        //            if (oTargetCC == null)
        //                oTargetCC = oVar.AssociatedContentControls[0];
        //            oCCs = new Word.ContentControl[1] { oTargetCC };
        //        }
        //        oVarNodes = oCCs;
        //    }
        //    string xAmbiguousEntities = "";

        //    //GLOG 4561: Added parameter indicating whether to assume only 1 entity per mVar tag inside table cell
        //    string xDetailValue = LMP.Forte.MSWord.WordDoc.GetDetailValue(ref oVarNodes, xFields,
        //        xEntitySeparator, xItemSeparator, ref xAmbiguousEntities,
        //        bReorderEntitiesByLocation, oVar.IsMultiValue || iAltFormat != XmlVariableAction.AlternateFormatTypes.None,
        //        oVar.ForteDocument.WordDocument);

        //    LMP.Benchmarks.Print(t0);

        //    return xDetailValue;
        //}

        ///// <summary>
        ///// returns an xml string representing the value of oVar in the document, where
        ///// oVar is a tagless varaible with a detail grid that populates other variables
        ///// </summary>
        ///// <param name="oVar"></param>
        ///// <returns></returns>
        //private static string GetMasterVariableDetailValue(XmlVariable oVar,
        //    Word.XMLNode oTargetTag, Word.ContentControl oTargetCC,
        //    bool bReorderEntitiesByLocation)
        //{
        //    DateTime t0 = DateTime.Now;

        //    string xValue = "";
        //    string xSortedValue = "";

        //    if (oVar.ControlType == mpControlTypes.DetailGrid)
        //    {
        //        int iPos = -1;
        //        string xReserveValue = oVar.ReserveValue;
        //        XmlSegment oSegment = oVar.Segment;
        //        XmlForteDocument oForteDoc = oSegment.ForteDocument;

        //        //get names of "sub-variables" from this variable's RunVariableActions actions
        //        for (int i = 0; i < oVar.VariableActions.Count; i++)
        //        {
        //            XmlVariableAction oAction = oVar.VariableActions[i];
        //            if (oAction.Type == XmlVariableActions.Types.RunVariableActions)
        //            {
        //                //get sub-variable
        //                string[] aParams = oAction.Parameters.Split(
        //                    LMP.StringArray.mpEndOfSubField);
        //                string[] aVarNames = aParams[0].Split(',');
        //                for (int v = 0; v < aVarNames.GetLength(0); v++)
        //                {
        //                    //JTS 12/19/08: Trim any extra spaces from ends of string
        //                    string xVarName = aVarNames[v].Trim();
        //                    XmlVariable oSubVar = null;
        //                    //Variable might not be found if it's currently in a hidden block
        //                    try
        //                    {
        //                        oSubVar = oSegment.Variables.ItemFromName(xVarName);
        //                    }
        //                    catch { }

        //                    if (oSubVar != null)
        //                    {
        //                        //if the Expression parameter of the InsertDetail action evaluates
        //                        //empty due to a condition other that the value of the master variable,
        //                        //the document will not reflect the true value of this sub-variable -
        //                        //extract the current value from the ReserveValue property
        //                        string xPreserve = "";
        //                        string xParameters = "";
        //                        for (int j = 0; j < oSubVar.VariableActions.Count; j++)
        //                        {
        //                            XmlVariableAction oSubAction = oSubVar.VariableActions[j];
        //                            if (oSubAction.Type == XmlVariableActions.Types.InsertDetail)
        //                            {
        //                                xParameters = oSubAction.Parameters;
        //                                break;
        //                            }
        //                        }

        //                        if (xParameters != "")
        //                        {
        //                            //get Expression parameter
        //                            aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);
        //                            if (aParams.GetUpperBound(0) > 6)
        //                            {

        //                                //JTS 12/19/08: Could be either [Variable__ or [Variable_
        //                                //replace field code that returns value of master variable
        //                                string xExpression = aParams[7].Replace("[Variable__" +
        //                                    oVar.Name + "]", "x");
        //                                xExpression = xExpression.Replace("[Variable_" +
        //                                    oVar.Name + "]", "x");
        //                                //evaluate
        //                                xExpression = XmlExpression.Evaluate(xExpression, oSegment,
        //                                    oForteDoc);
        //                                if (xExpression == "")
        //                                {
        //                                    //value won't be inserted in the doc - get impacted fields
        //                                    //from Template parameter
        //                                    string xFields = "";
        //                                    string xTemplate = XmlExpression.Evaluate(aParams[2],
        //                                        oSegment, oForteDoc);
        //                                    iPos = xTemplate.IndexOf("[SubVar_");
        //                                    while (iPos > -1)
        //                                    {
        //                                        iPos += 8;
        //                                        int iPos2 = xTemplate.IndexOf("]", iPos);
        //                                        if (xFields != "")
        //                                            xFields += "|";
        //                                        //JTS 12/19/08: Trim starting '_' to account for new [SubVar__xxx] format
        //                                        xFields += xTemplate.Substring(iPos, iPos2 - iPos).TrimStart(@"_".ToCharArray());
        //                                        iPos = xTemplate.IndexOf("[SubVar_", iPos2);
        //                                    }

        //                                    //extract values of these fields from current ReserveValue
        //                                    if (xFields != "")
        //                                    {
        //                                        string[] aFields = xFields.Split('|');
        //                                        for (int j = 0; j < aFields.Length; j++)
        //                                        {
        //                                            xPreserve += ExtractDetailFieldValue(xReserveValue,
        //                                                aFields[j]);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        //append
        //                        if (xPreserve != "")
        //                            //use ReserveValue
        //                            xValue += xPreserve;
        //                        else
        //                        {
        //                            //get value from doc
        //                            Word.XMLNode oTag = null;
        //                            Word.ContentControl oCC = null;
        //                            if (oForteDoc.FileFormat ==
        //                                LMP.Data.mpFileFormats.Binary)
        //                            {
        //                                //xml tags
        //                                if (oTargetTag != null)
        //                                {
        //                                    //use tag only if it belongs to this subvariable
        //                                    string xTagID = oTargetTag.SelectSingleNode(
        //                                        "@TagID", "", true).NodeValue;
        //                                    if (xTagID == oSubVar.Name)
        //                                        oTag = oTargetTag;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                //content controls
        //                                if (oTargetCC != null)
        //                                {
        //                                    //use tag only if it belongs to this subvariable
        //                                    string xTagID = oForteDoc.GetAttributeValue(oTargetCC, "TagID");
        //                                    if (xTagID == oSubVar.Name)
        //                                        oCC = oTargetCC;
        //                                }
        //                            }

        //                            xValue += GetVariableDetailValue(oSubVar, xParameters,
        //                                oTag, oCC, bReorderEntitiesByLocation);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        //If no SubVariables were found, but there's a reserve value, use that
        //        if (xValue == "" && xReserveValue != "")
        //        {
        //            xValue = xReserveValue;
        //            //If ReserveValue has been refreshed from Tag, reserved XML characters
        //            //might already be restored - ensure & and ' are always replaced
        //            xValue = String.RestoreXMLChars(xValue);
        //            xValue = xValue.Replace("&", "&amp;");
        //            xValue = xValue.Replace("'", "&apos;");
        //        }
        //        //reorder by index
        //        int iIndex = 1;
        //        bool bFound = true;
        //        while (bFound)
        //        {
        //            iPos = xValue.IndexOf("Index=" + "\"" + iIndex.ToString() + "\"");
        //            bFound = (iPos > -1);
        //            while (iPos > -1)
        //            {
        //                iPos = xValue.LastIndexOf('<', iPos);
        //                int iPos2 = xValue.IndexOf("</", iPos);
        //                iPos2 = xValue.IndexOf(">", iPos2);
        //                xSortedValue += xValue.Substring(iPos, (iPos2 - iPos) + 1);
        //                iPos = xValue.IndexOf("Index=" + "\"" + iIndex.ToString() + "\"", iPos2);
        //            }
        //            iIndex++;
        //        }
        //    }

        //    LMP.Benchmarks.Print(t0);

        //    return xSortedValue;
        //}

        ///// <summary>
        ///// returns an xml string representing the value of oVar in the document as
        ///// defined by the InsertDetail action(s) of it or, if tagless, of the other
        ///// variables it populates
        ///// </summary>
        ///// <param name="oVar"></param>
        ///// <returns></returns>
        //public static string GetDetailValue(XmlVariable oVar)
        //{
        //    return GetDetailValue(oVar, null, null, false);
        //}

        ///// <summary>
        ///// returns an xml string representing the value of oVar in the document as
        ///// defined by the InsertDetail action(s) of it or, if tagless, of the other
        ///// variables it populates
        ///// </summary>
        ///// <param name="oVar"></param>
        ///// <returns></returns>
        //public static string GetDetailValue(XmlVariable oVar, Word.XMLNode oTargetTag,
        //    Word.ContentControl oTargetCC, bool bReorderEntitiesByLocation)
        //{
        //    DateTime t0 = DateTime.Now;

        //    //get InsertDetail parameters
        //    string xParameters = "";
        //    for (int i = 0; i < oVar.VariableActions.Count; i++)
        //    {
        //        XmlVariableAction oAction = oVar.VariableActions[i];
        //        if (oAction.Type == XmlVariableActions.Types.InsertDetail)
        //        {
        //            xParameters = oAction.Parameters;
        //            break;
        //        }
        //    }

        //    //if variable does not have InsertDetail action, it may
        //    //be a tagless "master" variable
        //    string xValue = "";
        //    if (xParameters != "")
        //        xValue = GetVariableDetailValue(oVar, xParameters, oTargetTag,
        //            oTargetCC, bReorderEntitiesByLocation);
        //    else if (oVar.Segment is IStaticDistributedSegment)
        //        //GLOG 7921 - for labels and envelopes, always use reserve value
        //        xValue = oVar.ReserveValue;
        //    else
        //        xValue = GetMasterVariableDetailValue(oVar, oTargetTag, oTargetCC,
        //            bReorderEntitiesByLocation);

        //    //GLOG 5344: Make sure reserved characters in value aren't handled as fieldcodes
        //    xValue = XmlExpression.MarkLiteralReservedCharacters(xValue);

        //    LMP.Benchmarks.Print(t0, oVar.Name + " value: " + xValue);

        //    return xValue;
        //}

        /// <summary>
        /// returns the xField elements of xValue -
        /// faster than loading xml and using xpath
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="xField"></param>
        /// <returns></returns>
        private static string ExtractDetailFieldValue(string xValue, string xField)
        {
            string xDetail = "";
            string xValueUpper = xValue.ToUpper();
            string xFieldUpper = xField.ToUpper();

            int iPos = xValueUpper.IndexOf("<" + xFieldUpper + " ");
            while (iPos > -1)
            {
                int iPos2 = xValue.IndexOf("</", iPos);
                iPos2 = xValue.IndexOf(">", iPos2);
                xDetail += xValue.Substring(iPos, (iPos2 - iPos) + 1);
                iPos = xValueUpper.IndexOf("<" + xFieldUpper + " ", iPos2);
            }

            return xDetail;
        }
        #endregion
		#region *********************event handlers*********************
		private void m_oActions_ActionsDirtied(object sender, EventArgs e)
		{
			//variable actions have been edited - set variable dirty
			this.IsDirty = true;
		}

		private void m_oControlActions_ActionsDirtied(object sender, EventArgs e)
		{
			//control actions have been edited - set variable dirty
			this.IsDirty = true;
		}

        #endregion
	}

	/// <summary>
	/// contains the methods and properties that manage
	/// a collection of MacPac variables - created by Daniel Fisherman 09/20/05
	/// </summary>
	public class XmlVariables : LMP.Architect.Base.Variables
	{
		#region *********************constants*********************
		#endregion
		#region *********************fields*********************
        private Hashtable m_oDefsByID;
        private Hashtable m_oDefsByName;
        private Hashtable m_oDefsByTagID;
        private ArrayList m_oDefsByIndex;
        //private ArrayList m_oCIEnabledDefs;
        private List <XmlVariable> m_oCIEnabledDefs;
		private XmlSegment m_oSegment;
        private XmlForteDocument m_oForteDocument;
		private bool m_bVarsPreviouslySetValidationField;

		public event LMP.Architect.Base.ValidationChangedHandler ValidationChanged;
        public event VariableVisitedHandler VariableVisited;
		#endregion
        #region *********************events*********************
        public event BeforeContactsUpdateHandler BeforeContactsUpdateEvent;
        #endregion
		#region *********************constructors*********************
        /// <summary>
        /// creates a variables collection belonging
        /// to the specified segment
        /// </summary>
        /// <param name="oSegmentNodes"></param>
        /// <param name="oParent"></param>
        internal XmlVariables(XmlSegment oParent)
		{
            DateTime t0 = DateTime.Now;
			m_oSegment = oParent;
            this.WPDoc = oParent.WPDoc;
			//populate internal storage
            PopulateStorage();

            LMP.Benchmarks.Print(t0);
		}

		#endregion
		#region *********************properties*********************
        public WordprocessingDocument WPDoc { private set; get; }

		public XmlVariable this[int iIndex]
		{
			get{return ItemFromIndex(iIndex);}
		}
		public XmlVariable this[string xID]
		{
			get{return this.ItemFromID(xID);}
		}

		public int Count
		{
			get
			{
				return m_oDefsByID.Count;
			}
		}
		public XmlSegment Segment
		{
			get{return m_oSegment;}
		}
        public XmlForteDocument ForteDocument
        {
            get
            {
                return m_oSegment != null ?
                    m_oSegment.ForteDocument : m_oForteDocument;
            }
        }
        /// <summary>
        /// returns true iff the values of all
        /// variables in the collectio are valid
        /// </summary>
		public bool ValuesAreValid
		{
			get
			{
				//return true if all vars are valid
				for(int i=0; i<this.Count; i++)
					if(!this[i].HasValidValue)
						return false;

				return true;
			}
		}
        /// <summary>
        /// Returns true of all "Must Visit" variables have been set
        /// </summary>
        public bool AllRequiredValuesVisited
        {
            get
            {
                //return true if all vars are valid
                for (int i = 0; i < this.Count; i++)
                    if (this[i].MustVisit)
                        return false;

                return true;
            }
        }

        /// <summary>
        /// returns the collection variables 
        /// that are CI-enabled as an array
        /// </summary>
        public List<XmlVariable> CIEnabledVariables
        {
            get { return m_oCIEnabledDefs; }
        }

        /// <summary>
        /// TRUE if the parent segment has a new variables collection
        /// </summary>
        public bool CollectionIsObsolete
        {
            get { return this != this.Segment.Variables; }
        }
        #endregion
		#region *********************methods*********************
		/// <summary>
		/// returns a new admin variable
		/// </summary>
		/// <returns></returns>
		public XmlVariable CreateAdminVariable()
		{
			XmlVariable oVar = new XmlAdminVariable(this.Segment);
			oVar.SegmentName = this.Segment.FullTagID;

			//subscribe to notifications 
			oVar.ValueAssigned += new ValueAssignedEventHandler(OnVariableValueAssigned);
            oVar.VariableVisited += new VariableVisitedHandler(OnVariableVisited);
            return oVar;
		}

		/// <summary>
		/// returns a new user variable
		/// </summary>
		/// <returns></returns>
		public XmlVariable CreateUserVariable()
		{
			XmlVariable oVar = new XmlUserVariable(this.Segment);
			oVar.SegmentName = this.Segment.FullTagID;

			//subscribe to notifications 
			oVar.ValueAssigned +=new ValueAssignedEventHandler(OnVariableValueAssigned);
            oVar.VariableVisited += new VariableVisitedHandler(OnVariableVisited);
            
            return oVar;
		}

		/// <summary>
		/// returns the variable at the specified index
		/// </summary>
		/// <param name="iIndex"></param>
		/// <returns></returns>
		public XmlVariable ItemFromIndex(int iIndex)
		{
			//get the variable at the specified index
			if(iIndex > m_oDefsByIndex.Count - 1 || iIndex < 0)
			{
				//index is not in range - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemNotInCollection"));
			}

			//get variable ID at specified index
			XmlVariable oVar = (XmlVariable) m_oDefsByIndex[iIndex];

			return oVar;
		}

		/// <summary>
		/// returns the variable definition with the specified ID
		/// </summary>
		/// <param name="xID"></param>
		/// <returns></returns>
		public XmlVariable ItemFromID(string xID)
		{
			XmlVariable oVar = null;

			Trace.WriteNameValuePairs("xID", xID);

            object o = m_oDefsByID[xID];

            if (o != null)
            {
                oVar = (XmlVariable)m_oDefsByID[xID];
            }

			return oVar;
		}

		/// <summary>
		/// returns the variable definition with the specified ID
		/// </summary>
		/// <param name="xID"></param>
		/// <returns></returns>
		public XmlVariable ItemFromName(string xName)
		{
			XmlVariable oVar = null;

			Trace.WriteNameValuePairs("xName", xName);

            object o = m_oDefsByName[xName];

            if (o != null)
            {
                oVar = (XmlVariable)o;
            }

			return oVar;
		}

        /// <summary>
		/// returns the variable whose selection tag is the specified Word tag
		/// </summary>
		/// <param name="oWordTag"></param>
		/// <returns></returns>
        //public XmlVariable ItemFromAssociatedTag(Word.XMLNode oWordTag)
        //{
        //    XmlVariable oVar = null;

        //    if((oWordTag.BaseName != "mVar") && (oWordTag.BaseName != "mDel"))
        //    {
        //        //mVar tags are required for this function
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString("Error_VariableTagRequired"));
        //    }

        //    //get tag id of specified Word tag
        //
        //    string xTagID = oDoc.GetFullTagID(oWordTag);

        //    try
        //    {
        //        //get variable def from hashtable that maps 
        //        //selection tag names to variables
        //        oVar = (XmlVariable) this.m_oDefsByTagID[xTagID];
        //    }
        //    catch{}

        //    if(oVar == null)
        //        return null;
        //    else
        //        return oVar;
        //}

        ///// <summary>
        ///// returns the variable whose selection tag is the specified content control
        ///// </summary>
        ///// <param name="oCC"></param>
        ///// <returns></returns>
        //public XmlVariable ItemFromAssociatedContentControl(Word.ContentControl oCC)
        //{
        //    XmlVariable oVar = null;

        //    string xTag = oCC.Tag;
        //    string xElement = LMP.String.GetBoundingObjectBaseName(xTag);
        //    if ((xElement != "mVar") && (xElement != "mDel"))
        //    {
        //        //mVar tags are required for this function
        //        throw new LMP.Exceptions.WordTagException(
        //            LMP.Resources.GetLangString("Error_VariableTagRequired"));
        //    }

        //    //get tag id of specified Word tag
        //
        //    string xTagID = oDoc.GetFullTagID_CC(oCC);

        //    try
        //    {
        //        //get variable def from hashtable that maps 
        //        //selection tag names to variables
        //        oVar = (XmlVariable)this.m_oDefsByTagID[xTagID];
        //    }
        //    catch { }

        //    if (oVar == null)
        //        return null;
        //    else
        //        return oVar;
        //}

        /// <summary>
        /// saves the specified variable to the Word XML Tag
        /// </summary>
        /// <param name="oVar"></param>
        public void Save(XmlVariable oVar)
        {
            this.Save(oVar, false);
        }

		/// <summary>
		/// saves the specified variable to the Word XML Tag
		/// </summary>
		/// <param name="oVar"></param>
        /// <param name="bOnlyReserveValueChanged"></param>
		public void Save(XmlVariable oVar, bool bOnlyReserveValueChanged)
		{
            //JTS 8/10/15:  Save Tagless variables to XMLSegment ObjectData when value changes at insertion
            DateTime t0 = DateTime.Now;

            try
            {
                Trace.WriteNameValuePairs("oVar.ID", oVar.ID);

                //save only if variable is dirty
                if (!oVar.IsDirty)
                    return;

                if (!oVar.HasValidDefinition)
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidVariableDefinition") + string.Join("|", oVar.ToArray()));

                if (m_oDefsByID[oVar.ID] != null)
                {
                    //variable is already in collection
                    string xTagID = oVar.TagID;
                    //if (!this.Nodes.NodeExists(xTagID))
                    //{
                    //    //can't save to specified tag - alert
                    //    throw new LMP.Exceptions.WordXmlNodeException(
                    //        LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + xTagID);
                    //}

                    //get new definition 
                    string xVarForTag = oVar.ToString(true);

                    ////update the variable definitions in the node store and document
                    //if ((oVar.TagType == XmlForteDocument.TagTypes.DeletedBlock) ||
                    //    (oVar.TagType == XmlForteDocument.TagTypes.Mixed))
                    //{
                    //    //variable def is in deleted scopes
                    //    this.Nodes.UpdateVariableDefInDeletedScopes(oVar);
                    //}

                    //Save ObjectData for Tagless variable to Segment ObjectData
                    if (oVar.TagType == Base.ForteDocument.TagTypes.Segment)
                    {
                        //search for existing variable def in tag's object data
                        string xObjectData = this.m_oSegment.RawSegmentParts[0].GetSegmentObjectDataString();
                        string xNewObjectData = "";
                        const byte mpLenVarDef = 19;

                        int iPos1 = xObjectData.IndexOf("VariableDefinition=" + oVar.ID);
                        if (iPos1 > -1)
                        {
                            //variable definition already exists - replace with updated definition
                            int iPos2 = xObjectData.IndexOf("|", iPos1 + 1);
                            if (iPos2 == -1)
                                iPos2 = xObjectData.Length;
                            xNewObjectData = xObjectData.Substring(0, iPos1 + mpLenVarDef) +
                                xVarForTag + xObjectData.Substring(iPos2);
                        }
                        else
                        {
                            //raise error
                            throw new LMP.Exceptions.WordXmlNodeException(
                                LMP.Resources.GetLangString("Error_ObjectDataValueNotFound") +
                                "VariableDefinition=" + oVar.ID);
                        }

                        //set object data for specified Node
                        //GLOG 15941: Update ObjectData for all parts
                        foreach (RawSegmentPart oPart in this.m_oSegment.RawSegmentParts)
                        {
                            Query.SetAttributeValue(this.WPDoc, oPart.ContentControl, "ObjectData", xNewObjectData, false, null);
                        }
                    }

                    //raise event if the definition has 
                    //changed - definition has not changed if
                    //reserve value is the only thing that's changed
                    if (!bOnlyReserveValueChanged)
                        this.ForteDocument.RaiseVariableDefinitionChangedEvent(oVar);
                }
                //else
                //{
                //    //variable is not yet in the collection - this method assumes that
                //    //that the definition for the new variable is already in the
                //    //appropriate Word XMLNode(s) and that the ForteDocument.Tags collection
                //    //has been updated to reflect any newly added XMLNodes (if TagType = Variable)
                //    //or definitions (if TagType = Segment) - it also assumes that all of the
                //    //properties of the new variable have been set - all of this is handled by
                //    //the front-end

                //    //refresh node store
                //    if (!oVar.IsStandalone)
                //        m_oSegment.RefreshNodes();
                //    else
                //        m_oForteDocument.RefreshOrphanNodes();

                //    //add variable to internal arrays
                //    AddToInternalArrays(oVar);
                //    CompressIndexArray();

                //    //raise event
                //    this.ForteDocument.RaiseVariableAddedEvent(oVar);
                //}

                //remove dirt
                oVar.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.VariableException(
                    LMP.Resources.GetLangString("Error_CantSaveVariable") + oVar.ID, oE);
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
		}

        /// <summary>
        /// returns true if the variable with the specified name is in the collection
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public bool VariableExists(string xName)
        {
			XmlVariable oVar = null;

			Trace.WriteNameValuePairs("xName", xName);

			try
			{
				oVar = (XmlVariable) m_oDefsByName[xName];
			}
			catch{}

            return (oVar != null);
        }

	    /// <summary>
	    /// deletes the variable at the specified collection index and
        /// its associated tags if specified
	    /// </summary>
	    /// <param name="iIndex"></param>
        /// <param name="bDeleteAssociatedTags"></param>
        public void Delete(int iIndex, bool bDeleteAssociatedTags)
	    {
		    XmlVariable oVar = null;

		    try
		    {
			    oVar = this.ItemFromIndex(iIndex);
		    }
		    catch(System.Exception oE)
		    {
			    throw new LMP.Exceptions.NotInCollectionException(
				    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex, oE);
		    }

		    Delete(oVar, bDeleteAssociatedTags, false);
	    }

		/// <summary>
		/// deletes the variable with the specified name and
        /// its associated tags if specified
		/// </summary>
		/// <param name="xName"></param>
        /// <param name="bDeleteAssociatedTags"></param>
        public void Delete(string xName, bool bDeleteAssociatedTags)
		{
			XmlVariable oVar = null;

			try
			{
				oVar = (XmlVariable) m_oDefsByName[xName];
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemWithIDNotInCollection") + xName, oE);
			}

			Delete(oVar, bDeleteAssociatedTags, false);
		}

		/// <summary>
        /// deletes the specified variable and its associated tags if specified
		/// </summary>
		/// <param name="oVar"></param>
        /// <param name="bDeleteAssociatedTags"></param>
        public void Delete(XmlVariable oVar, bool bDeleteAssociatedTags, bool bDeleteVariableText)
        {
            Delete(oVar, bDeleteAssociatedTags, bDeleteVariableText, true);
        }

		/// <summary>
        /// deletes the specified variable and its associated tags if specified
		/// </summary>
		/// <param name="oVar"></param>
        /// <param name="bDeleteAssociatedTags"></param>
        public void Delete(XmlVariable oVar, bool bDeleteAssociatedTags, bool bDeleteVariableText,
            bool bRemovedDeletedScopeXML)
        {
            //DateTime t0 = DateTime.Now;

            //Trace.WriteNameValuePairs("oVar.ID", oVar.ID, "bDeleteAssociatedTags",
            //    bDeleteAssociatedTags);

            ////JTS 4/30/10: If variable Tag was removed as part of the DeleteScope of a different
            ////variable or block, it won't exist in Nodestore, but we still need to delete
            ////the object from the Variables collection
            //if (!this.Nodes.NodeExists(oVar.TagID) && (bDeleteAssociatedTags || bDeleteVariableText))
            //{
            //    //can't find specified tag - alert
            //    throw new LMP.Exceptions.WordXmlNodeException(
            //        LMP.Resources.GetLangString("Error_WordXmlNodeDoesNotExist") + oVar.TagID);
            //}

            ////remove definition from mSEG attributes
            //if (oVar.TagType != XmlForteDocument.TagTypes.Variable)
            //{
            //    if (oVar.TagType == XmlForteDocument.TagTypes.Segment)
            //    {
            //        //variable def in in object data
            //        string xObjectData = this.Nodes.GetItemObjectData(oVar.TagID);
            //        int iPos1 = xObjectData.IndexOf("VariableDefinition=" + oVar.ID);
            //        if (iPos1 > -1)
            //        {
            //            int iPos2 = xObjectData.IndexOf("|", iPos1 + 1) + 1;
            //            string xNew = xObjectData.Substring(0, iPos1) + xObjectData.Substring(iPos2);
            //            this.Nodes.SetItemObjectData(oVar.TagID, xNew);
            //        }
            //    }
            //    else if (bRemovedDeletedScopeXML)
            //    {
            //        //variable def in in deleted scopes
            //        this.Nodes.RemoveVariableDefFromDeletedScopes(oVar);
            //    }
            //}
            ////delete associated tags
            //if (bDeleteAssociatedTags)
            //{
            //    XmlForteDocument.WordXMLEvents iIgnore = XmlForteDocument.IgnoreWordXMLEvents;
            //    XmlForteDocument.IgnoreWordXMLEvents = LMP.Architect.Oxml.XmlForteDocument.WordXMLEvents.All;
            //    //JTS 5/10/10: 10.2
            //    if (this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
            //    {
            //        Word.ContentControl[] oAssociatedCCs = oVar.AssociatedContentControls;
            //        if (oAssociatedCCs != null)
            //        {
            //            for (int i = oAssociatedCCs.Length - 1; i >= 0; i--)
            //            {
            //                Word.ContentControl oCC = oAssociatedCCs[i];

            //                //remove tag from Tags collection
            //                this.ForteDocument.Tags.Delete_CC(oCC);

            //                //GLOG 6326 (dm) - delete associated doc var and bookmark
            //                LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oCC.Tag,
            //                    this.ForteDocument.WordDocument, false);

            //                //delete actual Word XMLNode, plus text if specified
            //                oCC.Delete(bDeleteVariableText);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        Word.XMLNode[] oAssociatedTags = oVar.AssociatedWordTags;
            //        if (oAssociatedTags != null)
            //        {
            //            for (int i = oAssociatedTags.Length - 1; i >= 0; i--)
            //            {
            //                Word.XMLNode oTag = oAssociatedTags[i];

            //                //delete tag range content if specified.
            //                if (bDeleteVariableText)
            //                {
            //                    object oMissing = System.Reflection.Missing.Value;
            //                    oTag.Range.Delete(ref oMissing, ref oMissing);
            //                }
            //                //remove tag from Tags collection
            //                this.ForteDocument.Tags.Delete(oTag);

            //                //GLOG 6326 (dm) - delete associated doc var and bookmark
            //                Word.XMLNode oReserved = oTag.SelectSingleNode("@Reserved", "", true);
            //                if (oReserved != null)
            //                {
            //                    LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oReserved.NodeValue,
            //                        this.ForteDocument.WordDocument, false);
            //                }

            //                //delete actual Word XMLNode
            //                oTag.Delete();
            //            }
            //        }
            //    }
            //    XmlForteDocument.IgnoreWordXMLEvents = iIgnore;
            //}
  
            ////delete deactivation value (5/19/11 dm)
            //if (!m_oSegment.Activated)
            //{
            //    //get the deactivation document variable that
            //    //contains the data for this reviewed variable
            //    object oName = "Deactivation_" + m_oSegment.TagID;
            //    Word.Variable oDocVar = this.ForteDocument.WordDocument.Variables.get_Item(ref oName);

            //    //get the value of the document variable
            //    string xValue = StringArray.mpEndOfRecord +
            //        LMP.String.Decrypt(oDocVar.Value) + StringArray.mpEndOfRecord;

            //    //search for the deactivated name/value pair
            //    int iPos = xValue.IndexOf(StringArray.mpEndOfRecord +
            //        oVar.Name + StringArray.mpEndOfElement);

            //    if (iPos > -1)
            //    {
            //        //pair was found - find the end of the name/value pair
            //        int iPos2 = xValue.IndexOf(StringArray.mpEndOfRecord, iPos + 1);

            //        //remove the name value pair
            //        xValue = xValue.Substring(0, iPos) + xValue.Substring(iPos2);
            //        xValue = xValue.Trim(StringArray.mpEndOfRecord);

            //        //set document variable to the new value
            //        oDocVar.Value = LMP.String.Encrypt(xValue,
            //            LMP.Data.Application.EncryptionPassword);
            //    }
            //}

            ////delete the var from the internal arrays
            //DeleteFromInternalArrays(oVar);

            ////refresh node store
            //if (m_oSegment != null)
            //    m_oSegment.RefreshNodes();
            //else
            //    m_oForteDocument.RefreshOrphanNodes();

            ////raise event
            //this.ForteDocument.RaiseVariableDeletedEvent(oVar);

            //LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// deletes the specified variable and its associated tags
        /// </summary>
        /// <param name="oVar"></param>
        public void Delete(XmlVariable oVar)
        {
            Delete(oVar, true, false);
        }

        /// <summary>
        /// deletes the specified variable, its associated tags
        /// and if specified, the text range contained within the 
        /// deleted tag
        /// </summary>
        /// <param name="oVar"></param>
        public void Delete(XmlVariable oVar, bool bDeleteRange)
        {
            Delete(oVar, true, bDeleteRange);
        }
        /// <summary>
        /// deletes the variable at the specified collection index and its associated tags
        /// </summary>
        /// <param name="iIndex"></param>
        public void Delete(int iIndex)
        {
            Delete(iIndex, true);
        }

        /// <summary>
        /// deletes the variable with the specified name and its associated tags
        /// </summary>
        /// <param name="xName"></param>
        public void Delete(string xName)
        {
            Delete(xName, true);
        }

        /// <summary>
        /// sets the values of the variables in the 
        /// collection to their default values, or
        /// the relevant prefill value, if one is specified.
        /// If Variable is in a child Segment, Prefill Name
        /// will consist of relative path plus variable name
        /// </summary>
        /// <returns>TRUE if all specified values were assigned</returns>
        public bool AssignDefaultValues(XmlPrefill oPrefill, string xRelativePath, bool bForcePrefillOverride)
        {
            DateTime t0 = DateTime.Now;
            int iPos1 = 0;
            int iVarCount = this.Count;
            for (int i = 0; i < this.Count; i++)
            {
                //GLOG 3475: Additional Prefill Override options added
                bool bPrefillOverride = false;
                XmlSegment oSegment = null;

                //exit if assignment of a value has forced a segment refresh,
                //e.g. when variables are added or deleted by an action
                //with an expanded deletion scope which contains other tags
                if (this.CollectionIsObsolete || iVarCount != this.Count)
                    return false;

                //If assignment has failed previously, don't try again
                if (this.m_oSegment.InvalidPreferenceVariables.Contains(this[i].Name + "|"))
                    continue;

                switch (this[i].AllowPrefillOverride)
                {
                    case XmlVariable.PrefillOverrideTypes.Never:
                        bPrefillOverride = false;
                        break;
                    case XmlVariable.PrefillOverrideTypes.Always:
                        bPrefillOverride = true;
                        break;
                    case XmlVariable.PrefillOverrideTypes.SameSegmentID:
                        //GLOG 3475: Check if current Segment or any Parent Segments match ID
                        oSegment = this[i].Segment;
                        do
                        {
                            if (oPrefill.SegmentID == oSegment.ID)
                            {
                                bPrefillOverride = true;
                                break;
                            }
                            else
                                oSegment = oSegment.Parent;
                        } while (oSegment != null);
                        break;
                    case XmlVariable.PrefillOverrideTypes.ForSameType:
                        //GLOG 3475: Check if current Segment or any Parent Segments match type
                        oSegment = this[i].Segment;
                        do
                        {
                            if (oPrefill.SegmentType == oSegment.TypeID)
                            {
                                bPrefillOverride = true;
                                break;
                            }
                            else
                                oSegment = oSegment.Parent;
                        } while (oSegment != null);
                        break;
                }


                //get variable
                XmlVariable oVar = this[i];

                if (oVar != null)
                {
                    string xPrefill = null;
                    List<string> aNameList = new List<string>();
                    aNameList.Add(oVar.Name);
                    if (oVar.AssociatedPrefillNames != "")
                    {
                        //Check for matches with alternate names as well
                        string[] aVarNames = oVar.AssociatedPrefillNames.Split(',');
                        aNameList.AddRange(aVarNames);
                    }
                    foreach (string xName in aNameList)
                    {
                        xPrefill = oPrefill[xRelativePath + xName];

                        if (xPrefill == null && xRelativePath != "")
                        {
                            //exact match not found -
                            //relative path was specified, but no exact match was found -
                            //check for just name without relative path - this is useful
                            //when working with data packets, as prefill name/value pairs
                            //don't use relative paths (they are divorced from segment
                            //structure)
                            xPrefill = oPrefill[xName];
                        }

                        if (xPrefill == null)
                        {
                            //still no match - look for Child segment variables 
                            //with the same name - e.g. when we use a letter prefill
                            //to generate a memo, memo cc has no relative path, but the
                            //letter prefill cc does, as letter cc is found in the signature
                            for (int c = 0; c < oPrefill.Count; c++)
                            {
                                if (oPrefill.ItemName(c).EndsWith("." + xName))
                                {
                                    xPrefill = oPrefill[c];
                                    break;
                                }
                            }
                        }

                        if (xPrefill != null)
                            break;
                    }

                    //GLOG 3058: added condition to use prefill value when
                    //we're forcing prefill overrides and there is a prefill value
                    //GLOG 3124: Don't force override for Preference codes
                    //if (xPrefill == null || !oVar.AllowPrefillOverride ||
                    //    (bForcePrefillOverride && ((Expression.ContainsAuthorCode(oVar.DefaultValue, -1, -2) &&
                    //    !Expression.ContainsPreferenceCode(oVar.DefaultValue)))) ||
                    //    !bForcePrefillOverride)

                    //conditions revamped per GLOG items 3081 and 3181
                    bool bContainsAuthorDetailCode = XmlExpression.ContainsAuthorCode(oVar.DefaultValue, -1, -2) &&
                        !XmlExpression.ContainsPreferenceCode(oVar.DefaultValue);
                    if (bContainsAuthorDetailCode)
                    {
                        //GLOG 6123 (dm) - the intent of this exception was to ensure accurate
                        //accurate author detail, not to ignore the prefill for boolean variables
                        //that default based on author critera
                        string xAuthorValue = XmlExpression.Evaluate(oVar.DefaultValue,
                            m_oSegment, m_oSegment.ForteDocument).ToUpper();
                        bContainsAuthorDetailCode = ((xAuthorValue != "TRUE") &&
                            (xAuthorValue != "FALSE"));
                    }
                    //bool bContainsAuthorCode = Expression.ContainsAuthorCode(oVar.DefaultValue, -1, -2) ||
                    //    Expression.ContainsPreferenceCode(oVar.DefaultValue);

                    //GLOG 3475
                    if (xPrefill == null ||
                        //GLOG 4519: Don't always evaluate Default Value with Author Field
                        //go by Saved Data Override value instead
                        //(this.Segment.LinkAuthorsToParent && this.Segment.Parent != null && bContainsAuthorCode) ||
                        (!bForcePrefillOverride && (!bPrefillOverride || bContainsAuthorDetailCode)))
                    {
                        //either 1) there is no prefill for this variable,
                        //2) the segment is linked to it's parent's authors and
                        //the default value of the variable contains an
                        //expression related to the author (GLOG item #2510),
                        //3) the variable can't accept a prefill value, or
                        //4)we're forcing prefill values on all variables -
                        //assign the default value if the value
                        //hasn't been set previously by another variable -
                        //e.g. through a variable action
                        //GLOG 6914 (dm) - skip detail sub variables - master variable
                        //is responsible for initializing its subs
                        if ((!oVar.HasInitializedValue) && (!oVar.IsDetailSubComponent))
                            this[i].AssignDefaultValue();
                    }
                    else
                    {
                        //set value to prefill - determine if prefill value is xml
                        XmlDocument oXML = new XmlDocument();
                        try
                        {
                            //GLOG 6149
                            //GLOG 7725: Make sure any &amp; tokens already in the string are converted properly
                            oXML.LoadXml("<Prefill>" + String.RestoreXMLChars(xPrefill) + "</Prefill>");
                        }
                        catch { }

                        if (oXML.DocumentElement != null && oXML.DocumentElement.FirstChild != null &&
                            oXML.DocumentElement.FirstChild.NodeType == XmlNodeType.Element)
                        {
                            //prefill is xml - check if variable actions require xml -
                            //if not, change value to text - this is useful for reline,
                            //in particular, as some relines use xml and some use text
                            bool bActionRequiresXML = false;
                            //GLOG 5999: If configured control is one of the types requiring XML, set to true
                            if (oVar.ControlType == mpControlTypes.DetailGrid ||
                                oVar.ControlType == mpControlTypes.DetailList || oVar.ControlType == mpControlTypes.RelineGrid)
                            {
                                bActionRequiresXML = true;
                            }
                            else
                            {
                                for (int j = 0; j < oVar.VariableActions.Count; j++)
                                {
                                    XmlVariableAction oA = oVar.VariableActions[j];
                                    if (oA.Type == XmlVariableActions.Types.InsertDetail ||
                                        oA.Type == XmlVariableActions.Types.InsertDetailIntoTable ||
                                        oA.Type == XmlVariableActions.Types.InsertReline ||
                                        oA.Type == XmlVariableActions.Types.SetupDetailTable ||
                                        oA.Type == XmlVariableActions.Types.SetupLabelTable ||
                                        oA.Type == XmlVariableActions.Types.SetVariableValue ||
                                        oA.Type == XmlVariableActions.Types.SetupDistributedSections)
                                    {
                                        //variable action requires xml
                                        bActionRequiresXML = true;
                                        break;
                                    }
                                }
                            }
                            if (!bActionRequiresXML)
                            {
                                //convert to text - extract first element
                                xPrefill = oXML.DocumentElement.FirstChild.InnerText;
                            }
                            else
                            {
                                //GLOG 6149: Restore double quotes in XML value
                                //GLOG 7725: Make sure &amp; tokens in the string are handled properly
                                xPrefill = String.RestoreXMLChars(xPrefill);
                            }
                        }
                        //unrem this code if we're updating contact detail
                        //when assigning variable values
                        if ((oVar.CIContactType != 0 || oVar.CIEntityName != "") && xPrefill.Contains("UNID="))
                        {
                            if (PrefillIsCIDetailDeficient(xPrefill, oVar))
                            {
                                //this variable is linked to CI and does not contain
                                //the required detail - automatically update contacts
                                xPrefill = GetUpdatedContactDetail(xPrefill, oVar, true);
                            }
                            //GLOG 5894: Discard CI Detail Token STring portion of Prefill Value if present
                            int iTokenStart = xPrefill.IndexOf("<CIDetailTokenString>");
                            int iTokenEnd = xPrefill.IndexOf("</CIDetailTokenString>");
                            if (iTokenStart > -1 && iTokenEnd > -1 && iTokenEnd > iTokenStart)
                            {
                                xPrefill = xPrefill.Substring(0, iTokenStart);
                            }
                        }

                        bool bUsePrefillValue = true;
                        int iCount = oVar.VariableActions.Count;

                        //GLOG 3474: Make sure any Variables with a ReplaceSegment action linked to the Variable value
                        //are only set to Prefill value if it corresponds to a valid assigned Segment ID
                        for (int k = 0; k < iCount; k++)
                        {
                            XmlVariableAction oA = oVar.VariableActions[k];
                            if (oA.Type == XmlVariableActions.Types.ReplaceSegment
                                && oA.Parameters.ToUpper().Contains("MYVALUE"))
                            {
                                //GLOG 15747: If New Segment ID parameter contains [MyValue] as part
                                //of a larger expression, substitute Prefill value to test for valid Segment ID
                                string[] aParams = oA.Parameters.Split(LMP.StringArray.mpEndOfSubField);
                                string xNewID = aParams[1];
                                if (xNewID.ToUpper().Contains("MYVALUE"))
                                {
                                    try
                                    {
                                        int iMVPos = xNewID.IndexOf("[myvalue]", StringComparison.CurrentCultureIgnoreCase);
                                        if (iMVPos > -1)
                                        {
                                            xNewID = xNewID.Substring(0, iMVPos) + xPrefill + xNewID.Substring(iMVPos + 9);
                                        }
                                        else
                                        {
                                            iMVPos = xNewID.IndexOf("myvalue", StringComparison.CurrentCultureIgnoreCase);
                                            if (iMVPos > -1)
                                            {
                                                xNewID = xNewID.Substring(0, iMVPos) + xPrefill + xNewID.Substring(iMVPos + 7);
                                            }
                                        }
                                        xNewID = XmlExpression.Evaluate(xNewID, this.Segment, this.ForteDocument);
                                        ISegmentDef oSeg = XmlSegment.GetDefFromID(xNewID);
                                        //Only check Admin Segment IDs
                                        if (oSeg is AdminSegmentDef)
                                        {
                                            //See if there are specific Assignments of this type for the parent Segment
                                            Assignments oAssignments = new Assignments(oVar.Segment.TypeID, oVar.Segment.ID1,
                                                oSeg.TypeID);
                                            if (oAssignments.Count > 0)
                                            {
                                                bUsePrefillValue = false;
                                                for (int l = 0; l < oAssignments.Count; l++)
                                                {
                                                    if (oAssignments.ItemFromIndex(l).AssignedObjectID == ((AdminSegmentDef)oSeg).ID)
                                                    {
                                                        //This is an assigned ID
                                                        bUsePrefillValue = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        //Value is not a valid Segment ID
                                        bUsePrefillValue = false;
                                    }
                                    //Don't check any more actions
                                    break;
                                }
                            }
                        }
                        //GLOG 3474: New ID for ReplaceSegment was not valid assignment for parent Segment
                        if (!bUsePrefillValue)
                        {
                            oVar.AssignDefaultValue();
                        }
                        else if (!LMP.String.CompareBooleanCaseInsensitive(oVar.Value, xPrefill))
                        {
                            //GLOG - 3783 - ceh
                            string xCIToken = "";
                            string xControlProps = oVar.ControlProperties;

                            //get ci token if specified
                            iPos1 = xControlProps.IndexOf("CIToken=<");
                            int iIndexCount = 1;

                            if (iPos1 != -1)
                            {
                                iPos1 += 9;
                                int iPos2 = xControlProps.IndexOf(">", iPos1);
                                xCIToken = xControlProps.Substring(iPos1, iPos2 - iPos1);


                                //find last instance of Index & get value
                                int iNewPos = xPrefill.LastIndexOf("Index=");
                                if (iNewPos != -1)
                                {
                                    //get last index value
                                    string xNewValue = xPrefill.Substring(iNewPos + 7);
                                    iNewPos = xNewValue.IndexOf("\"");
                                    try
                                    {
                                        iIndexCount = Int32.Parse(xNewValue.Substring(0, iNewPos));
                                    }
                                    catch
                                    {
                                        //GLOG item #4200 - dcf
                                        //xml string does not include Index="x"
                                        //search for Index='x', which is the format
                                        //returned by the document analyzer
                                        iNewPos = xNewValue.IndexOf("'");
                                        iIndexCount = Int32.Parse(xNewValue.Substring(0, iNewPos));
                                    }
                                }

                                //cycle through indexes and replace CI Token element if necessary
                                for (int k = 1; k <= iIndexCount; k++)
                                {
                                    if (!xPrefill.Contains("<" + xCIToken + " Index=" + "\"" + k.ToString() + "\""))
                                    {
                                        xPrefill = xPrefill.Replace("FULLNAMEWITHPREFIXANDSUFFIX", xCIToken);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                //get sub controls if associated control is DetailGrid
                                if (oVar.ControlType == mpControlTypes.DetailGrid)
                                {
                                    string xConfigString = xControlProps.Split('�')[0];
                                    int iPos = xConfigString.IndexOf("=");
                                    xConfigString = xConfigString.Substring(iPos + 1);
                                    string[] aConfigParams = xConfigString.Split('�');
                                    string xFields = "";

                                    //get every 2nd array item - that's the field name
                                    for (int j = 0; j < aConfigParams.Length; j++)
                                    {
                                        if (j % 5 == 1)
                                        {
                                            xFields += aConfigParams[j] + ",";
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(xFields))
                                    {
                                        xFields = "," + xFields;
                                        //GLOG 7588: If Prefill uses a different FULLNAME token, conform to current variable
                                        if (xFields.ToUpper().Contains(",FULLNAMEWITHPREFIXANDSUFFIX,"))
                                        {
                                            xPrefill = xPrefill.Replace("<FULLNAME ", "<FULLNAMEWITHPREFIXANDSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAME>", "</FULLNAMEWITHPREFIXANDSUFFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHSUFFIX ", "<FULLNAMEWITHPREFIXANDSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHSUFFIX>", "</FULLNAMEWITHPREFIXANDSUFFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIX ", "<FULLNAMEWITHPREFIXANDSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIX>", "</FULLNAMEWITHPREFIXANDSUFFIX>");
                                        }
                                        else if (xFields.ToUpper().Contains(",FULLNAME,"))
                                        {
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIXANDSUFFIX ", "<FULLNAME ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIXANDSUFFIX>", "</FULLNAME>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHSUFFIX ", "<FULLNAME ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHSUFFIX>", "</FULLNAME>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIX ", "<FULLNAME ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIX>", "</FULLNAME>");
                                        }
                                        else if (xFields.ToUpper().Contains(",FULLNAMEWITHPREFIX,"))
                                        {
                                            xPrefill = xPrefill.Replace("<FULLNAME ", "<FULLNAMEWITHPREFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAME>", "</FULLNAMEWITHPREFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHSUFFIX ", "<FULLNAMEWITHPREFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHSUFFIX>", "</FULLNAMEWITHPREFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIXANDSUFFIX ", "<FULLNAMEWITHPREFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIXANDSUFFIX>", "</FULLNAMEWITHPREFIX>");
                                        }
                                        else if (xFields.ToUpper().Contains(",FULLNAMEWITHSUFFIX,"))
                                        {
                                            xPrefill = xPrefill.Replace("<FULLNAME ", "<FULLNAMEWITHSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAME>", "</FULLNAMEWITHSUFFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIX ", "<FULLNAMEWITHSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIX>", "</FULLNAMEWITHSUFFIX>");
                                            xPrefill = xPrefill.Replace("<FULLNAMEWITHPREFIXANDSUFFIX ", "<FULLNAMEWITHSUFFIX ");
                                            xPrefill = xPrefill.Replace("</FULLNAMEWITHPREFIXANDSUFFIX>", "</FULLNAMEWITHSUFFIX>");
                                        }
                                        //check each row of the prefill value - if the value of
                                        //each of these fields is empty, remove the row from the 
                                        //prefill for this variable
                                        //string xPrefillXml = "<Prefill>" + xPrefill + "</Prefill>";
                                        string xPrefillXml = "<Prefill>" + xPrefill.Replace("&quot;", "\"").Replace("&", "&amp;") + "</Prefill>";
                                        XmlDocument oPrefillXML = new XmlDocument();
                                        oPrefillXML.LoadXml(xPrefillXml);

                                        int k = 0;

                                        XmlNodeList oNodes = null;
                                        do
                                        {
                                            k++;
                                            bool bEntityContainsValues = false;
                                            oNodes = oPrefillXML.SelectNodes("//*[@Index='" + k.ToString() + "']");

                                            foreach (XmlNode oNode in oNodes)
                                            {
                                                if (xFields.ToUpper().Contains("," + oNode.Name.ToUpper() + ","))
                                                {
                                                    //node is one of the specified fields -
                                                    //check for value
                                                    if (!string.IsNullOrEmpty(oNode.InnerText))
                                                    {
                                                        bEntityContainsValues = true;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (!bEntityContainsValues)
                                            {
                                                foreach (XmlNode oNode in oNodes)
                                                {
                                                    xPrefill = xPrefill.Replace(oNode.OuterXml, "");
                                                }

                                            }
                                        } while (oNodes.Count > 0);
                                    }
                                }

                                //GLOG 7588: This is now being handled above
                                ////GLOG - 3783 - ceh
                                ////necessary to clean up manually inserted recipients
                                //iPos1 = xControlProps.IndexOf("FULLNAMEWITHPREFIXANDSUFFIX");
                                //if (iPos1 != -1)
                                //{
                                //    xPrefill = xPrefill.Replace("<FULLNAME ", "<FULLNAMEWITHPREFIXANDSUFFIX ");
                                //    xPrefill = xPrefill.Replace("</FULLNAME>", "</FULLNAMEWITHPREFIXANDSUFFIX>");
                                //}
                            }

                            try
                            {
                                //prefill value is different than existing value
                                oVar.SetValue(xPrefill);
                            }
                            catch (System.Exception oE)
                            {
                                //GLOG 8727: We should not be showing any MessageBoxes during creation, since there may not be a UI
                                //could not assign prefill value - alert and assign default value
                                Error.Show(oE, string.Format(LMP.Resources.GetLangString("Error_InvalidPrefillValue"), oVar.Name, "").TrimEnd(new char[] { '\r', '\n' }));
                                //MessageBox.Show(string.Format(LMP.Resources.GetLangString("Error_InvalidPrefillValue"),
                                //    "", LMP.Error.GetErrorDetail(oE)),
                                //    LMP.ComponentProperties.ProductName,
                                //    MessageBoxButtons.OK,
                                //    MessageBoxIcon.Exclamation);
                                this.m_oSegment.InvalidPreferenceVariables += oVar.Name + "|";
                                try
                                {
                                    oVar.AssignDefaultValue();
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            //SetValidationField variable, as the SetValidationField function
                            //runs only when the value is set
                            oVar.SetValidationField();

                            //execute actions for include/exclude type variable if necessary -
                            //text may need updating even when default value matches current value;
                            //TODO: this issue theoretically applies to any variable with an action
                            //that has a field code in one of its parameters, so this code may need
                            //to be generalized at some point if we can find an efficient way to do so
                            //GLOG 6838: Test for additional variants of ValueSourceExpression
                            if (((oVar.ValueSourceExpression == "[TagValue] ^!= ") ||
                                (oVar.ValueSourceExpression == "[TagValue] ^!=") ||
                                (oVar.ValueSourceExpression == "[TagValue] ^!= [Empty]") ||
                                (oVar.ValueSourceExpression == "[MyTagValue] ^!= ") ||
                                (oVar.ValueSourceExpression == "[MyTagValue] ^!=") ||
                                (oVar.ValueSourceExpression == "[MyTagValue] ^!= [Empty]")) &&
                                (xPrefill.ToLower() == "true") && (oVar.SecondaryDefaultValue != null))
                            {

                                xPrefill = XmlExpression.Evaluate(
                                    oVar.SecondaryDefaultValue, this.m_oSegment, this.ForteDocument);
                                string xNodeText = oVar.AssociatedContentControls[0].InnerText;
                                if (xNodeText != xPrefill)
                                    oVar.VariableActions.Execute();
                                //GLOG 2957: Also update preference
                                oVar.SavePreferenceIfNecessary();
                            }
                            else if (oVar.Segment is LMP.Architect.Base.IDocumentStamp)
                            {
                                //GLOG 3508: Make sure checkbox options selected in Trailer dialog
                                //are saved as preference if the Variable is Tagless
                                oVar.SavePreferenceIfNecessary();
                            }
                        }
                    }
                }
            }
            LMP.Benchmarks.Print(t0, this.Segment.FullTagID);

            return true;
        }

        /// <summary>
        /// sets the values of the variables in the 
        /// collection to their default values
        /// </summary>
        /// <returns>TRUE if all specified values were assigned</returns>
        public bool AssignDefaultValues()
        {
            DateTime t0 = DateTime.Now;
            for (int i = 0; i < this.Count; i++)
            {
                //exit if assignment of a value has forced a segment refresh,
                //e.g. when variables are added or deleted by an action
                //with an expanded deletion scope which contains other tags
                if (this.CollectionIsObsolete)
                    return false;
                //if variable assignment has failed on a prior pass, don't try again
                if (!m_oSegment.InvalidPreferenceVariables.Contains(this[i].Name + "|"))
                {
                    //GLOG 6914 (dm) - skip detail sub variables - master variable
                    //is responsible for initializing its subs
                    if ((this[i] != null) && !this[i].IsDetailSubComponent)
                    {
                        this[i].AssignDefaultValue();
                    }
                }
            }
            LMP.Benchmarks.Print(t0);

            return true;
        }

        /// <summary>
        /// returns a variable populated with the supplied definition values
        /// </summary>
        /// <param name="xVariableDef"></param>
        /// <returns></returns>
        public XmlVariable GetVariableFromDefinition(string xVariableDef)
        {
            xVariableDef = xVariableDef.Trim('|');

            //convert definition to array
            string[] aVariableDef = xVariableDef.Split('�');

            //get Variable from array
            return GetVariableFromArray(aVariableDef);
        }

        /// <summary>
        /// returns the updated contact detail
        /// of the specified contact detail
        /// </summary>
        /// <param name="xContactDetailXML"></param>
        /// <returns></returns>
        internal string GetUpdatedContactDetail(string xContactDetailXML, XmlVariable oVar)
        {
            return GetUpdatedContactDetail(xContactDetailXML, oVar, false);
        }
        internal string GetUpdatedContactDetail(string xContactDetailXML, XmlVariable oVar, bool bMissingOnly)
        {
            //cycle through each item of contact detail-
            //if it has a UNID replace it with updated detail-
            //else leave as is
            string xUpdatedDetail = "";
            //GLOG 5894: Get Source variable Detail Token String from Prefill
            ArrayList aPrefillCIFields = new ArrayList();
            if (xContactDetailXML.Contains("<CIDetailTokenString>") && xContactDetailXML.Contains("</CIDetailTokenString>"))
            {
                int iStart = xContactDetailXML.IndexOf("<CIDetailTokenString>") + @"<CIDetailTokenString>".Length;
                int iEnd = xContactDetailXML.IndexOf("</CIDetailTokenString>");
                if (iEnd > iStart)
                {
                    string xPrefillCIFields = xContactDetailXML.Substring(iStart, iEnd - iStart);
                    //Fieldcode separator could be either 1 or 2 underscores - make consistent
                    xPrefillCIFields = xPrefillCIFields.Replace("[CIDetail__", "[CIDetail_");
                    string xTokenPattern = @"\[CIDetail_\w*\]";
                    MatchCollection oTokens = Regex.Matches(xPrefillCIFields, xTokenPattern);
                    foreach (Match oToken in oTokens)
                    {
                        string xToken = oToken.Value.Replace("[CIDetail_", "").Replace("]", "");
                        aPrefillCIFields.Add(xToken.ToUpper());
                    }
                }
            }
            Match oMatch = null;
            int j = 1;
            ArrayList oUNIDs = new ArrayList();
            string xMatch = null;

            try
            {
                do
                {
                    string xCurDetail = "";
                    string xNewDetail = "";
                    //get entity UNID
                    string xPattern = "Index=\"" + j.ToString() + "\" UNID=\".*?\"";
                    oMatch = Regex.Match(xContactDetailXML, xPattern);

                    if (oMatch == null || oMatch.Value == "")
                        //there are no more matches
                        break;

                    //parse UNID from match
                    xMatch = oMatch.Value;
                    int iPos = xMatch.IndexOf("UNID=");
                    xMatch = xMatch.Substring(iPos + 6, xMatch.Length - (iPos + 7));

                    if (xMatch != "0")
                    {
                        //entity has a UNID - get the contact
                        //associated with the UNID
                        string[] aUNIDS = { xMatch };

                        ICContacts oContacts = null;
                        try
                        {
                            System.Windows.Forms.Application.DoEvents();
                            oContacts = XmlContacts.GetContactsFromUNIDs(oVar, aUNIDS);
                            System.Windows.Forms.Application.DoEvents();
                        }
                        catch { }

                        //this will err if no contacts returned-
                        //user will be alerted below
                        if (oContacts != null && oContacts.Count() == 1)
                        {
                            //contact was found - get updated detail
                            xNewDetail = XmlContacts.GetDetail(oContacts, oVar, j);
                            //GLOG 5894: UNID of returned contact may not include Address Type values
                            //Replace current UNID with original string to ensure match between new and old 
                            Object oIndex = 1;
                            xNewDetail = xNewDetail.Replace("UNID=\"" + oContacts.Item(oIndex).UNID.ToString() + "\"", "UNID=\"" + aUNIDS[0] + "\"");
                        }

                        //GLOG 5894:  If specified, use value in Prefill if it was part of the original CI Detail Token String,
                        //otherwise, value from newly-retrieved Contact will be used.
                        if (bMissingOnly || oContacts == null || oContacts.Count() != 1)
                        {
                            //contact was not found - use existing detail
                            //JTS 9/24/09: This needs to use specific UNID, instead of '0'
                            //'|' is reserved RegEx character, so replace all instances with '\|'
                            //GLOG 4857: '^' is also reserved, and may occur in Groupwise UNID
                            //GLOG 7683: ( and ) added, as these may occur in Interaction UNID
                            xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                                "\" UNID=\"" + xMatch.Replace("|", "\\|").Replace("^", "\\^").Replace("(", "\\(").Replace(")", "\\)") + "\">[^<>]*</[^<>]*>";
                            MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);
                            //GLOG 5894: Replace new Contact value with Prefill value if field was part of original CI Detail Token String.
                            if (xNewDetail != "")
                            {
                                MatchCollection oCurMatches = Regex.Matches(xNewDetail, xPattern);
                                foreach (Match oCurMatch in oCurMatches)
                                {
                                    string xField = oCurMatch.Value.Substring(1, oCurMatch.Value.IndexOf(" ") - 1);
                                    if (aPrefillCIFields.Contains(xField))
                                    {
                                        foreach (Match oElement in oMatches)
                                        {
                                            if (oElement.Value.StartsWith("<" + xField + " "))
                                            {
                                                xCurDetail += oElement.Value;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                        xCurDetail += oCurMatch.Value;
                                }
                            }
                            else
                            {
                                foreach (Match oElement in oMatches)
                                {
                                    xCurDetail += oElement.Value;
                                }
                            }

                        }
                        else
                            xCurDetail = xNewDetail;

                    }
                    else
                    {
                        //the entity does not have a UNID -
                        //use existing detail
                        xPattern = @"<[^<>]*Index=\""" + j.ToString() +
                            "\" UNID=\"0\">[^<>]*</[^<>]*>";
                        MatchCollection oMatches = Regex.Matches(xContactDetailXML, xPattern);

                        foreach (Match oElement in oMatches)
                            xCurDetail += oElement.Value; ;
                    }
                    xUpdatedDetail += xCurDetail;
                    j++;
                } while (oMatch != null && oMatch.Value != "");

                return xUpdatedDetail;
            }
            catch
            {
                //some general problem occured - alert 
                //user that contact will not be updated -
                //get contact display name from xml
                string xName = null;
                int iPos = xContactDetailXML.IndexOf(">");
                if (iPos > -1)
                {
                    int iPos2 = xContactDetailXML.IndexOf("<", iPos);
                    xName = xContactDetailXML.Substring(iPos + 1, iPos2 - iPos - 1);
                }
                //MessageBox.Show(LMP.Resources.GetLangString("Msg_CouldNotUpdateContacts") + 
                //    (xName != null ? xName : xContactDetailXML),
                //    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                //    MessageBoxIcon.Exclamation);

                return xContactDetailXML;
            }
        }
        #endregion
		#region *********************private members*********************
        private void PopulateStorage()
        {
            DateTime t0 = DateTime.Now;

            m_oDefsByID = new Hashtable();
            m_oDefsByName = new Hashtable();
            m_oDefsByIndex = new ArrayList();
            m_oDefsByTagID = new Hashtable();
            m_oCIEnabledDefs = new List<XmlVariable>();

            XmlVariable oVar = null;
            string[] aVariableDef;
            string xVariableDef = "";
            string xName = "";

            //get tagless variables
            string xTagID = m_oSegment.FullTagID;
            SdtElement omSEG = m_oSegment.RawSegmentParts[0].ContentControl;
            string xObjectData = m_oSegment.RawSegmentParts[0].GetSegmentObjectDataString();
            int iPos1 = xObjectData.IndexOf("VariableDefinition=");
            while (iPos1 > -1)
            {
                iPos1 += 19;
                int iPos2 = xObjectData.IndexOf("|", iPos1 + 1);
                if (iPos2 == -1)
                    iPos2 = xObjectData.Length;
                xVariableDef = xObjectData.Substring(iPos1, iPos2 - iPos1);
                aVariableDef = xVariableDef.Split('�');
                xName = aVariableDef[1];

                //get variable
                if (m_oDefsByName[xName] == null)
                {
                    oVar = this.Add(aVariableDef, omSEG);
                    oVar.TagID = xTagID;
                    oVar.TagType = Base.ForteDocument.TagTypes.Segment;
                }
                else
                    oVar = (XmlVariable)m_oDefsByName[xName];

                //add content control to list
                //oVar.ContainingContentControls.Add(omSEG);

                //add parent part number
                string xParts = ";" + oVar.TagParentPartNumbers + ";";
                string xPart = Query.GetAttributeValue(this.WPDoc, omSEG, "PartNumber");
                if (xParts == ";;")
                    oVar.TagParentPartNumbers = xPart;
                else if (xParts.IndexOf(";" + xPart + ";") == -1)
                    oVar.TagParentPartNumbers += ";" + xPart;

                iPos1 = xObjectData.IndexOf("VariableDefinition=", iPos1);
            }

            //get variables corresponding to mVars
            foreach (RawSegmentPart oPart in m_oSegment.RawSegmentParts)
            {
                omSEG = oPart.ContentControl;
                //JTS 11/9/16: reworked for speed
                //GLOG 8484: Avoid error on non-Forte Content Controls
                SdtElement[] omVars = omSEG.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mpv")).ToArray();
                //var omVars = from cc in omSEG.Descendants<SdtElement>()
                //             where (from pr in cc.Elements<SdtProperties>()
                //                 where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpv")).Any()
                //                 select pr).Any()
                //             select cc;
               foreach (SdtElement omVar in omVars)
               {
                   //exclude mVars contained in child mSEGs
                   if (Query.GetParentSegmentContentControl(omVar) == omSEG)
                   {
                       xObjectData = Query.GetAttributeValue(this.WPDoc, omVar, "ObjectData");
                       //JTS 10/08/15
                       if (xObjectData == "")
                           continue;
                       xName = Query.GetAttributeValue(this.WPDoc, omVar, "TagID");

                       //check whether we've already found this variable
                       if (m_oDefsByName[xName] == null)
                       {
                           //add new variable
                           xVariableDef = xObjectData.Substring(xObjectData.IndexOf("VariableDefinition=") + 19);
                           xVariableDef = xVariableDef.Trim('|');
                           aVariableDef = xVariableDef.Split('�');
                           oVar = this.Add(aVariableDef, omVar);
                           oVar.TagID = xTagID + "." + xName;
                           oVar.TagType = Base.ForteDocument.TagTypes.Variable;
                       }
                       else
                       {
                           //TODO: rename if values are different as we do in RefreshTags()?
                           oVar = (XmlVariable)m_oDefsByName[xName];
                       }

                       //add content control to lists
                       // oVar.ContainingContentControls.Add(omVar);
                       //oVar.AssociatedContentControls.Add(omVar);

                       //add parent part number
                       string xParts = ";" + oVar.TagParentPartNumbers + ";";
                       string xPart = Query.GetAttributeValue(this.WPDoc, omSEG, "PartNumber");
                       if (xParts == ";;")
                           oVar.TagParentPartNumbers = xPart;
                       else if (xParts.IndexOf(";" + xPart + ";") == -1)
                           oVar.TagParentPartNumbers += ";" + xPart;

                       //adjust mixed type
                       if (oVar.TagType != Base.ForteDocument.TagTypes.Variable)
                           oVar.TagType = Base.ForteDocument.TagTypes.Mixed;
                   }
               }

                //get variables corresponding to mDels
               //GLOG 8484: Avoid error on non-Forte Content Controls
               SdtElement[] omDels = omSEG.Descendants<SdtElement>().Where(cc => Query.GetTag(cc).StartsWith("mpd")).ToArray();
               //var omDels = from cc in omSEG.Descendants<SdtElement>()
               //              where (from pr in cc.Elements<SdtProperties>()
               //                  where pr.Descendants<Tag>().Where(o => o.Val.Value.StartsWith("mpd")).Any()
               //                  select pr).Any()
               //              select cc;
                foreach (SdtElement omDel in omDels)
                {
                   xObjectData = Query.GetAttributeValue(this.WPDoc, omDel, "ObjectData");
                   string[] aProps = xObjectData.Split('|');
                   if (aProps[0] == m_oSegment.TagID)
                   {
                        //get variable
                        xName = Query.GetAttributeValue(this.WPDoc, omDel, "TagID");

                        if (m_oDefsByName[xName] == null)
                        {
                            xVariableDef = this.GetVariableDefFromDeletedScopes(this.WPDoc, omSEG, xName);
                            if (xVariableDef != null)
                            {
                                aVariableDef = xVariableDef.Split('�');
                                oVar = this.Add(aVariableDef, omSEG);
                                oVar.TagID = xTagID + "." + xName;
                                oVar.TagType = Base.ForteDocument.TagTypes.DeletedBlock;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                            oVar = (XmlVariable)m_oDefsByName[xName];

                        //add content controls to lists
                        //oVar.ContainingContentControls.Add(omSEG);
                        //oVar.AssociatedContentControls.Add(omDel);

                        //add parent part number
                        string xParts = ";" + oVar.TagParentPartNumbers + ";";
                        string xPart = Query.GetAttributeValue(this.WPDoc, omSEG, "PartNumber");
                        if (xParts == ";;")
                            oVar.TagParentPartNumbers = xPart;
                        else if (xParts.IndexOf(";" + xPart + ";") == -1)
                            oVar.TagParentPartNumbers += ";" + xPart;

                        //adjust mixed type
                        if (oVar.TagType != Base.ForteDocument.TagTypes.DeletedBlock)
                            oVar.TagType = Base.ForteDocument.TagTypes.Mixed;
                    }
                }
            }

            CompressIndexArray();

            LMP.Benchmarks.Print(t0);
        }

        private XmlVariable Add(string[] aVariableDef, SdtElement oSourceCC)
        {
            XmlVariable oVar = this.GetVariableFromArray(aVariableDef);
            
            //initialize content controls lists
            //oVar.ContainingContentControls = new List<SdtElement>();
            //oVar.AssociatedContentControls = new List<SdtElement>();

            //set runtime properties
            oVar.SegmentName = m_oSegment.Name;

            ////set tag prefix id
            string xObjectData = Query.GetAttributeValue(this.WPDoc, oSourceCC, "ObjectData");
            if (xObjectData.StartsWith("|"))
                xObjectData = xObjectData.Substring(1);
            oVar.TagPrefixID = LMP.String.GetTagPrefixID(xObjectData);

            //10.2 (dm) - set next object database id for segment
            m_oSegment.NextObjectDatabaseID = oVar.ObjectDatabaseID + 1;

            oVar.IsDirty = false;

            //add to internal arrays
            AddToInternalArrays(oVar);

            //add variable to CI variables collection
            //if variable is CI-enabled
            if (oVar.CIDetailTokenString != "" &&
                (oVar.CIContactType != 0 || oVar.CIEntityName != ""))
                m_oCIEnabledDefs.Add(oVar);

            return oVar;
        }

        /// <summary>
        /// gets the definition of the specified variable from the deleted
        /// scopes attribute of the specified mSEG
        /// </summary>
        /// <param name="oVar"></param>
        private string GetVariableDefFromDeletedScopes(WordprocessingDocument oDoc,
            SdtElement omSEG, string xVarName)
        {
            string xDeletedScopes = Query.GetAttributeValue(m_oSegment.WPDoc, omSEG, "DeletedScopes");
            int iPos1 = xDeletedScopes.IndexOf("DeletedTag=" + xVarName + "�");
            if (iPos1 != -1)
            {
                int iPos2 = xDeletedScopes.IndexOf("�/w:body>|", iPos1 + 1) + 10;
                string xScope = xDeletedScopes.Substring(iPos1, iPos2 - iPos1);
                iPos1 = xScope.IndexOf("�w:tag w:val=");
                while (iPos1 != -1)
                {
                    iPos1 += 14;
                    iPos2 = xScope.IndexOf('\"', iPos1);
                    string xTag = xScope.Substring(iPos1, iPos2 - iPos1);
                    if (xTag.StartsWith("mpv")) //limit to mVars
                    {
                        string xDef = Query.GetAttributeValue(oDoc, xTag, "ObjectData");
                        xDef = xDef.Substring(xDef.IndexOf("VariableDefinition=") + 19);
                        xDef = xDef.Trim('|');
                        string[] aDef = xDef.Split('�');
                        if ((aDef.Length > 1) && (aDef[1] == xVarName))
                            return xDef;
                        else
                            iPos1 = xScope.IndexOf("�w:tag w:val=", iPos2);
                    }
                    else
                        iPos1 = xScope.IndexOf("�w:tag w:val=", iPos2);
                }
            }

            return null;
        }
        /// <summary>
        /// Make the ExecutionIndex equal the array index position + 1. This correction is necessary
        /// when two or more variables stored in the m_oDefsByIndex arrayList have the same ExectionIndex. 
        /// This can also happen when a variable is deleted.
        /// </summary>
        private void AssociateExecutionIndexToIndexArrayPosition()
        {
            DateTime t0 = DateTime.Now;

            //GLOG 1407 - exit if called while delete scopes are getting updated -
            //this will avoid a potential object data mismatch between associated mVars
            //in different delete states
            if (this.ForteDocument.DeleteScopesUpdatingInDesign)
                return;

            for (int i = 0; i < m_oDefsByIndex.Count; i++)
            {
                if (m_oDefsByIndex[i] != null)
                {
                    XmlVariable oVar = ((XmlVariable)(m_oDefsByIndex[i]));

                    // Correct the execution index to correspond to i + 1.
                    if (oVar.ExecutionIndex != i + 1)
                    {
                        oVar.ExecutionIndex = i + 1;

                        // Save the change to the variable's execution index so that when a refresh is
                        // performed the new i + 1 execution index value is retrieved.
                        this.Save(oVar);
                    }
                }
            }

            LMP.Benchmarks.Print(t0);
        }

		/// <summary>
		/// removes null elements in internal index array
		/// </summary>
		private void CompressIndexArray()
		{
			//cycle through array from last to 
			//first index removing null elements
			for(int i=m_oDefsByIndex.Count - 1; i>=0; i--)
				if(m_oDefsByIndex[i] == null)
					m_oDefsByIndex.RemoveAt(i);
            if (this.ForteDocument.Mode == XmlForteDocument.Modes.Design)
                AssociateExecutionIndexToIndexArrayPosition();
		}
		/// <summary>
		/// returns a variable populated with the supplied definition values
		/// </summary>
		/// <param name="aDef"></param>
		/// <returns></returns>
		private XmlVariable GetVariableFromArray(object[] aDef)
		{
			DateTime t0 = DateTime.Now;

			XmlVariable oVar = null;
			string xActions = "";

			try
			{
				string xID = aDef[0].ToString();

                if (xID.StartsWith("U"))
                {
                    //definition is for a user variable -
                    if (this.Segment == null)
                        oVar = new XmlUserVariable(this.ForteDocument);
                    else
                        oVar = new XmlUserVariable(this.Segment);
                }
                else
                {
                    //definition is for an admin variable
                    if (this.Segment == null)
                        oVar = new XmlAdminVariable(this.ForteDocument);
                    else
                        oVar = new XmlAdminVariable(this.Segment);
                }
                oVar.ID = xID;
				oVar.Name = aDef[1].ToString();
				oVar.DisplayName = aDef[2].ToString();
				oVar.TranslationID = Convert.ToInt32(aDef[3]);
				oVar.ExecutionIndex = Convert.ToInt32(aDef[4]);

				//there may be two values stored in DefaultValue field
				string[] aDefaultValue = aDef[5].ToString().Split('�');
				oVar.DefaultValue = aDefaultValue[0];
				if (aDefaultValue.Length > 1)
					oVar.SecondaryDefaultValue = aDefaultValue[1];

				oVar.ValueSourceExpression = aDef[6].ToString();

                //GLOG 2355 - convert detail grid reserve value to xml if necessary -
                //this requires loading control type and properties first
                oVar.ControlType = (LMP.Data.mpControlTypes)System.Convert.ToByte(aDef[9]);
                oVar.ControlProperties = aDef[10].ToString();
                //GLOG 6149:  Replace any &quot; tokens left in Reserve Value with double qoutes
                oVar.ReserveValue = oVar.DetailReserveValueToXML(aDef[7].ToString().Replace("&quot;", "\""));

				//index 8 is not currently used
				//oVar.Reserved = aDef[8].ToString();

				oVar.ValidationCondition = aDef[11].ToString();
				oVar.ValidationResponse = aDef[12].ToString();
				oVar.HelpText = aDef[13].ToString();

				//populate variable actions collection
				xActions = aDef[14].ToString();
				oVar.VariableActions.SetFromString(xActions);

				//populate control actions collection
				xActions = aDef[15].ToString();
				oVar.ControlActions.SetFromString(xActions);

                oVar.RequestCIForEntireSegment = bool.Parse(aDef[16].ToString());
                oVar.CIContactType = (ciContactTypes) Convert.ToInt32(aDef[17]);
                oVar.CIDetailTokenString = aDef[18].ToString();
                oVar.CIAlerts = (ciAlerts)Convert.ToInt32(aDef[19]);
                oVar.DisplayIn = (XmlVariable.ControlHosts)Convert.ToInt32(aDef[20]);
                oVar.DisplayLevel = (XmlVariable.Levels)Convert.ToInt32(aDef[21]);
                oVar.DisplayValue = aDef[22].ToString();
                oVar.Hotkey = aDef[23].ToString();

                //these properties were added after segment production began,
                //and hence, may not be in the variable definition string
                if (aDef.GetUpperBound(0) > 23)
                    oVar.MustVisit = Convert.ToBoolean(aDef[24]);
                else
                    oVar.MustVisit = false;
                if (aDef.GetUpperBound(0) > 24)
                {
                    //GLOG 3475: Convert old True/False value to new Enum
                    if (!String.IsNumericInt32(aDef[25].ToString()))
                    {
                        if (aDef[25].ToString().ToUpper() == "FALSE")
                            oVar.AllowPrefillOverride = XmlVariable.PrefillOverrideTypes.Never;
                        else 
                            //if AllowPrefillOverride=True and the variable is an author pref,
                            //reset AllowPrefillOverride to SameSegmentID.
                            if(oVar.DefaultValue.ToUpper().Contains("AUTHORPREFERENCE_"))
                            oVar.AllowPrefillOverride = XmlVariable.PrefillOverrideTypes.SameSegmentID;
                        else
                            oVar.AllowPrefillOverride = XmlVariable.PrefillOverrideTypes.Always;
                    }
                    else
                        oVar.AllowPrefillOverride = (XmlVariable.PrefillOverrideTypes)Convert.ToInt32(aDef[25]);
                }
                else
                    //default is to allow prefill override
                    oVar.AllowPrefillOverride = XmlVariable.PrefillOverrideTypes.Always;
                
                if (aDef.GetUpperBound(0) > 25)
                    oVar.IsMultiValue = Convert.ToBoolean(aDef[26]);
                else
                    oVar.IsMultiValue = false;

                if (aDef.GetUpperBound(0) > 26)
                    oVar.AssociatedPrefillNames = aDef[27].ToString();
                else
                    oVar.AssociatedPrefillNames = "";

                if (aDef.GetUpperBound(0) > 27)
                    oVar.TabNumber = Int32.Parse(aDef[28].ToString());
                else
                    oVar.TabNumber = 1;

                if (aDef.GetUpperBound(0) > 28)
                    oVar.CIEntityName = aDef[29].ToString();
                else
                    oVar.CIEntityName = "";

                //GLOG 2645
                if (aDef.GetUpperBound(0) > 29)
                    oVar.SetRuntimeControlValues(aDef[30].ToString(), false);
                else
                    oVar.SetRuntimeControlValues("", false);

                //GLOG 1408
                if (aDef.GetUpperBound(0) > 30)
                    oVar.NavigationTag = aDef[31].ToString();
                else
                    oVar.NavigationTag = "";

                //10.2
                if (aDef.GetUpperBound(0) > 31)
                    oVar.ObjectDatabaseID = Int32.Parse(aDef[32].ToString());
                else
                    oVar.ObjectDatabaseID = 0;

                //subscribe to events
				oVar.ValueAssigned += new ValueAssignedEventHandler(OnVariableValueAssigned);
                oVar.VariableVisited += new VariableVisitedHandler(OnVariableVisited);
            }
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.DataException(
					LMP.Resources.GetLangString(
						"Error_InvalidVariableDefinition") + oVar.ID, oE);
			}

			LMP.Benchmarks.Print(t0, oVar.DisplayName);
			return oVar;
		}

		/// <summary>
		/// true IFF definition for the variable with xID and xName
		/// is in the delete scope xml of another variable
		/// </summary>
		/// <param name="xObjectData"></param>
		/// <param name="xVariableName"></param>
		/// <returns></returns>
		private bool VariableIsDeleted(string xDeletedScopes, string xID, string xName)
		{
			//search for delete scope records in xDeletedScopes
			int iPos = xDeletedScopes.IndexOf("DeletedTag=");
			while (iPos != -1)
			{
				//delete scope found - get name of variable
				iPos = iPos + 11;
				int iPos2 = xDeletedScopes.IndexOf('�', iPos);
                if (xDeletedScopes.Substring(iPos, iPos2 - iPos) != xName)
                {
                    //delete scope is for a different variable - check whether
                    //it contains definition for this variable
                    iPos2 = xDeletedScopes.IndexOf("�/w:body>|", iPos);
                    string xScope = xDeletedScopes.Substring(iPos, iPos2 - iPos);
                    if (xScope.IndexOf("VariableDefinition=" + xID) != -1)
                        return true;
                }
                else
                {
                    //delete scope is for this variable, so variable is still active -
                    //verify that there's a corresponding mDel
                    //GLOG 6328 (dm) - include element parameter - this will avoid
                    //loading an obsolete definition in place of the one contained in
                    //a subsequently added variable with the same name
                    //TODO: OpenXML rewrite
                    //if (this.Nodes.NodeExists(this.Segment.FullTagID + '.' + xName, "mDel"))
                    //    return false;
                    //else
                    //    //no mDel - don't add variable, but leave delete scope in tag -
                    //    //we may at some point want to prompt users for the insertion
                    //    //location when the mDel is missing
                    //    return true;
                }
				iPos = xDeletedScopes.IndexOf("DeletedTag=", iPos);
			}
			return false;
		}

		/// <summary>
		/// adds the specified variable to the internal arrays -
		/// assumes that variable has not already been added
		/// </summary>
		/// <param name="oVar"></param>
		private void AddToInternalArrays(XmlVariable oVar)
		{
			//add to hashtables
			m_oDefsByID.Add(oVar.ID, oVar);
			m_oDefsByName.Add(oVar.Name, oVar);

			//add to tag id hashtable
            XmlNode oNode = null;
            string xFullTagID = null;
            if (!oVar.IsStandalone)
                xFullTagID = System.String.Concat(oVar.SegmentName, '.', oVar.Name);
            else
                xFullTagID = oVar.Name;
            try
            {
                //TODO: OpenXML rewrite
                //oNode = this.Nodes.GetItem(xFullTagID);
            }
            catch{}
            if (oNode !=null)
				m_oDefsByTagID.Add(xFullTagID, oVar);

			//add to array list at proper index
            //GLOG 7610 (dm) - position relative to the defined execution
            //indexes of the existing variables, not their current positions
            int iCount = m_oDefsByIndex.Count;
            if (iCount == 0)
                m_oDefsByIndex.Add(oVar);
            else
            {
                for (int i = 0; i < iCount; i++)
                {
                    string[] aProps = m_oDefsByIndex[i].ToString().Split(
                        LMP.StringArray.mpEndOfValue);
                    if (int.Parse(aProps[8]) > oVar.ExecutionIndex)
                    {
                        m_oDefsByIndex.Insert(i, oVar);
                        break;
                    }
                    else if (i == iCount - 1)
                        m_oDefsByIndex.Add(oVar);
                }
            }
		}
		/// <summary>
		/// deletes the specified variable from the internal arrays
		/// </summary>
		/// <param name="oVar"></param>
		private void DeleteFromInternalArrays(XmlVariable oVar)
		{			
			//delete from internal arrays
			m_oDefsByIndex.Remove(oVar);
			m_oDefsByID.Remove(oVar.ID);
			m_oDefsByName.Remove(oVar.Name);
			m_oDefsByTagID.Remove(oVar.TagID);
            
            //GLOG 3461: Remove from list of CI-enabled variables
            m_oCIEnabledDefs.Remove(oVar);
            if (this.ForteDocument.Mode == XmlForteDocument.Modes.Design)
                AssociateExecutionIndexToIndexArrayPosition();
		}

        /// <summary>
        /// returns true iff the supplied prefill does not
        /// contain data for the required CI field
        /// </summary>
        /// <param name="xPrefill"></param>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private bool PrefillIsCIDetailDeficient(string xPrefill, XmlVariable oVar)
        {
            DateTime t0 = DateTime.Now;

            //get required CI fields for variable
            string xRequiredCIFields = oVar.CIDetailTokenString;
            //GLOG 5894: Get Source variable Detail Token String from Prefill
            string xPrefillCIFields = "";
            if (xPrefill.Contains("<CIDetailTokenString>") && xPrefill.Contains("</CIDetailTokenString>"))
            {
                int iStart = xPrefill.IndexOf("<CIDetailTokenString>") + @"<CIDetailTokenString>".Length;
                int iEnd = xPrefill.IndexOf("</CIDetailTokenString>");
                if (iEnd > iStart)
                {
                    xPrefillCIFields = xPrefill.Substring(iStart, iEnd - iStart);
                    //Fieldcode separator could be either 1 or 2 underscores - make consistent
                    xPrefillCIFields = xPrefillCIFields.Replace("[CIDetail__", "[CIDetail_");
                }

            }
            //JTS 9/24/09: Fieldcode separator could be either 1 or 2 underscores - make consistent
            xRequiredCIFields = xRequiredCIFields.Replace("[CIDetail__", "[CIDetail_");
            //GLOG 5894: If Source and Target have same CI Detail Token String, no need to check further
            if (xPrefillCIFields.ToUpper() == xRequiredCIFields.ToUpper())
                return false;
            //cycle through each CI field,
            //checking if the prefill contains data for that field -
            //e.g. a letter prefill will typically not contain the phone
            //and fax numbers required by the fax recipients variable
            MatchCollection oMatches = Regex.Matches(xRequiredCIFields, @"\[CIDetail_\w*\]");
            foreach (Match oMatch in oMatches)
            {
                //parse out the field name
                string xField = oMatch.ToString();
                xField = xField.Substring(10, xField.Length - 11);
                //GLOG 5894:  Compare fields against Detail Token STring in Prefill, if available
                //Otherwise, use Variable XML Value
                if (xPrefillCIFields != "")
                {
                    xField = "[CIDetail_" + xField + "]";
                    //return true if the field name is
                    //not included in the Source Detail Token String
                    if (!xPrefillCIFields.ToUpper().Contains(xField.ToUpper()))
                        return true;
                }
                else
                {
                    //return true if the field name is
                    //not an element name in the xml
                    if (!xPrefill.ToUpper().Contains("<" + xField.ToUpper() + " "))
                        return true;
                }
            }

            LMP.Benchmarks.Print(t0);
            return false;
        }

        /// <summary>
        /// replaces each deleted scope in the specified string with a pipe-delimited
        /// string containing the definitions of the variables in that scope
        /// </summary>
        /// <param name="xDeletedScopes"></param>
        /// <returns></returns>
        private string GetDeletedScopeVariableDefs(string xDeletedScopes)
        {
            //cycle through deleted scopes
            string xDeletedScopesMod = xDeletedScopes;
			int iPos = xDeletedScopesMod.IndexOf("DeletedTag=");
            while (iPos != -1)
            {
                //get deleted scope
                string xVariableDefs = "";
                iPos = xDeletedScopesMod.IndexOf('�', iPos) + 1;
                int iPos2 = xDeletedScopesMod.IndexOf("�/w:body>|", iPos);
                string xScope = xDeletedScopesMod.Substring(iPos, iPos2 - iPos);

                //cycle through w:tag nodes, looking for mVars
                int iPos3 = xScope.IndexOf("�w:tag w:val=");
                if (iPos3 != -1)
                {
                    while (iPos3 != -1)
                    {
                        iPos3 += 14;
                        int iPos4 = xScope.IndexOf("\"", iPos3);
                        string xTag = xScope.Substring(iPos3, iPos4 - iPos3);
                        if (LMP.String.GetBoundingObjectBaseName(xTag) == "mVar")
                        {
                            //get object data of mVar
                            //TODO: OpenXML rewrite
                            //xVariableDefs += LMP.Forte.MSWord.WordDoc.GetAttributeValueFromTag(
                            //    xTag, "ObjectData");
                            if (xVariableDefs.Substring(xVariableDefs.Length - 1, 1) != "|")
                                xVariableDefs += '|';
                        }
                        iPos3 = xScope.IndexOf("�w:tag w:val=", iPos4);
                    }
                }
                else
                {
                    //binary branch (1/17/11 dm) - in 10.2, the DeletedScopes attribute
                    //may contain a mix of open xml and WordML scopes
                    iPos3 = xScope.IndexOf("VariableDefinition=");
                    while (iPos3 != -1)
                    {
                        int iPos4 = xScope.IndexOf('|', iPos3) + 1;
                        xVariableDefs += xScope.Substring(iPos3, iPos4 - iPos3);
                        iPos3 = xScope.IndexOf("VariableDefinition=", iPos4);
                    }
                }

                //replace scope with variable defs - need to leave wrapped in w:body tags
                //for compatibility with existing code
                xDeletedScopesMod = xDeletedScopesMod.Substring(0, iPos) + "�w:body>" +
                    xVariableDefs + xDeletedScopesMod.Substring(iPos2);

                iPos = xDeletedScopesMod.IndexOf("DeletedTag=", iPos);
            }

            return xDeletedScopesMod;
        }
		#endregion
		#region *********************event handlers*********************
		/// <summary>
		/// handles notification that a variable has been assigned -
		/// used to keep track of valid variables - when all variables
		/// are valid, this method will raise an event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVariableValueAssigned(object sender, EventArgs e)
		{
			XmlVariable oVar = sender as XmlVariable;
			bool bVarIsValid = oVar.HasValidValue;

			//validation may have changed if the var is valid and previously
			//the variables collection was not valid, or visa versa - this is
			//for performance only
            bool bValidationMayHaveChanged = (bVarIsValid != m_bVarsPreviouslySetValidationField);
            
			if(bValidationMayHaveChanged)
			{
				bool bVarsAreValid = this.ValuesAreValid;
				if(m_bVarsPreviouslySetValidationField != bVarsAreValid)
				{
					//validation has changed - notify subscribers
					if(this.ValidationChanged != null)
						//raise VariablesChanged event -
						//pass in whether or not values are SetValidationField
                        this.ValidationChanged(this,
                            new LMP.Architect.Base.ValidationChangedEventArgs(bVarsAreValid));

					//mark for next iteration
					m_bVarsPreviouslySetValidationField = bVarsAreValid;
                }
            }
        }
        void OnVariableVisited(object sender, VariableEventArgs oArgs)
        {
            if (this.VariableVisited != null)
            {
                this.VariableVisited(this, oArgs);
            }
        }

        #endregion
	}
}
