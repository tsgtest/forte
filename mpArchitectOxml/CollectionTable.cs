﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Text.RegularExpressions;
using LMP.Data;
using CollectionTableStructure = LMP.Architect.Base.CollectionTableStructure;

namespace LMP.Architect.Oxml
{
    public class XmlCollectionTable : XmlAdminSegment
    {

        private bool m_bReplacementInProgress = false;
        private CollectionTableStructure.ItemInsertionLocations m_iPendingInsertionLocation =
            CollectionTableStructure.ItemInsertionLocations.Unspecified;

        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        XmlSegment.InsertXML(xXML, oLocation,
        //            iHeaderFooterInsertionType,
        //            LMP.Forte.MSWord.mpSectionOnePageSetupOptions.mpSectionOnePageSetupOption_False, false,
        //            iIntendedUse, this.ItemType); //GLOG 6983
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}

        /// <summary>
        /// returns the definition for this pleading table - this is used when
        /// there's no standalone record in the Segments table
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        public AdminSegmentDef GetSegmentDefFromSegment(string xName)
        {
            AdminSegmentDefs oDefs = new AdminSegmentDefs();
            AdminSegmentDef oDef = (AdminSegmentDef)oDefs.Create(this.TypeID);
            oDef.DefaultDoubleClickBehavior = (short)this.DefaultDoubleClickBehavior;
            oDef.DefaultDoubleClickLocation = (short)this.DefaultDoubleClickLocation;
            oDef.DefaultDragBehavior = (short)this.DefaultDragBehavior;
            oDef.DefaultDragLocation = (short)this.DefaultDragLocation;
            oDef.DefaultMenuInsertionBehavior = (short)this.DefaultMenuInsertionBehavior;
            oDef.DisplayName = this.DisplayName;
            oDef.HelpText = this.HelpText;
            oDef.IntendedUse = this.IntendedUse;
            oDef.L0 = this.L0;
            oDef.L1 = this.L1;
            oDef.L2 = this.L2;
            oDef.L3 = this.L3;
            oDef.L4 = this.L4;
            oDef.MenuInsertionOptions = (short)this.MenuInsertionOptions;
            oDef.Name = xName;
            oDef.TranslationID = this.TranslationID;

            //get parent's xml
            //TODO: remove the rest of the pleading text from the xml - for now, we're
            //assuming this is unnecessary, since only the style nodes are currently used
            //(by the variable actions for performance reasons)
            XmlSegment oParent = this.Parent;
            if (oParent != null)
                oDef.XML = oParent.Definition.XML;

            return oDef;
        }

        public bool ReplacementInProgress
        {
            get { return m_bReplacementInProgress; }
            set { m_bReplacementInProgress = value; }
        }

        public CollectionTableStructure.ItemInsertionLocations PendingInsertionLocation
        {
            get { return m_iPendingInsertionLocation; }
            set { m_iPendingInsertionLocation = value; }
        }

        /// <summary>
        /// gets the appropriate item object type for a member of
        /// this type of collection table
        /// </summary>
        virtual public mpObjectTypes ItemType
        {
            get {return mpObjectTypes.CollectionTableItem;}
        }

        /// <summary>
        /// returns a two column segment array that
        /// represents the layout of the child segments
        /// in this collection table segment
        /// </summary>
        /// <param name="oSeg"></param>
        /// <returns></returns>
        //public XmlSegment[,] GetLayout()
        //{
        //    //get collection table
        //    Word.Table oTable = this.ContentControls[0].Range.Tables[1];

        //    //create array that defines the current layout
        //    XmlSegment[,] oSigTable = new XmlSegment[oTable.Rows.Count, 2];

        //    int iCurCell = 1;

        //    //cycle through each signature, marking each in the layout array
        //    for (int i = 0; i < this.Segments.Count; i++)
        //    {
        //        //skip cells that are not part of a signature
        //        while (oTable.Range.Cells[iCurCell].Range.ParentContentControl == this.ContentControls[0])
        //            iCurCell++;

        //        XmlSegment oSig = this.Segments[i];

        //        if (oSig.ContentControls[0].Range.Cells.Count == 2)
        //        {
        //            //signature is two cells
        //            if (iCurCell % 2 == 1)
        //            {
        //                //next available cell is start of row
        //                oSigTable[(int)(iCurCell / 2), 0] = oSig;
        //                oSigTable[(int)(iCurCell / 2), 1] = oSig;
        //            }
        //            else
        //            {
        //                //next available cell is middle of row
        //                oSigTable[(int)(iCurCell / 2), 1] = oSig;
        //                oSigTable[(int)(iCurCell / 2) + 1, 0] = oSig;
        //            }

        //            //increment current cell
        //            iCurCell = iCurCell + 2;
        //        }
        //        else
        //        {
        //            //signature is one cell
        //            if (iCurCell % 2 == 1)
        //            {
        //                //next available cell is start of row
        //                oSigTable[(int)(iCurCell / 2), 0] = oSig;
        //            }
        //            else
        //            {
        //                //next available cell is middle of row
        //                oSigTable[(int)(iCurCell / 2), 1] = oSig;
        //            }

        //            //increment current cell
        //            iCurCell++;
        //        }
        //    }

        //    return oSigTable;
        //}
    }

    public class XmlCollectionTableItem : XmlAdminSegment
    {
        bool m_bAllowSideBySide = false;

        //public override void InsertXML(string xXML, XmlSegment oParent, Word.Range oLocation,
        //    LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType, bool bTargeted,
        //    LMP.Data.mpSegmentIntendedUses iIntendedUse)
        //{
        //    try
        //    {
        //        Word.XMLNode oCollectionTag = null;
        //        Word.ContentControl oCollectionCC = null;
        //        Word.Bookmark oCollectionBmk = null;
        //        bool bIsReplacement = false;
        //        bool bContentControls = false;
        //        bool bDesign = false;
        //        bContentControls = this.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML;
        //        bDesign = this.ForteDocument.Mode == XmlForteDocument.Modes.Design;
        //        if (oParent != null)
        //        {
        //            //get parent's tag
        //            if (bContentControls)
        //                oCollectionCC = oParent.PrimaryContentControl;
        //            else
        //                oCollectionTag = oParent.PrimaryWordTag;

        //            if (oCollectionCC == null && oCollectionTag == null)
        //                oCollectionBmk = oParent.PrimaryBookmark;

        //            //determine whether this is a segment replacement (e.g. via chooser),
        //            //in which case we'll want to insert at current location in table
        //            XmlCollectionTable oPT = (XmlCollectionTable)oParent;
        //            bIsReplacement = oPT.ReplacementInProgress;
        //            oPT.ReplacementInProgress = false;
        //        }

        //        //determine insertion location
        //        bool bAtCurrentRow = bIsReplacement;
        //        LMP.Forte.MSWord.Pleading.mpCollectionTableItemScopes iScope =
        //            LMP.Forte.MSWord.Pleading.mpCollectionTableItemScopes.FullRow;
        //        if (oParent != null)
        //        {
        //            XmlCollectionTable.ItemInsertionLocations iInsertionLocation =
        //                ((XmlCollectionTable)oParent).PendingInsertionLocation;
        //            bAtCurrentRow = (bAtCurrentRow || ((iInsertionLocation &
        //                XmlCollectionTable.ItemInsertionLocations.CurrentRow) > 0));
        //            if (!(this is XmlPleadingCaption))
        //            {
        //                if ((iInsertionLocation &
        //                    XmlCollectionTable.ItemInsertionLocations.LeftCell) > 0)
        //                    iScope = LMP.Forte.MSWord.Pleading.mpCollectionTableItemScopes.LeftCell;
        //                else if ((iInsertionLocation &
        //                    XmlCollectionTable.ItemInsertionLocations.RightCell) > 0)
        //                    iScope = LMP.Forte.MSWord.Pleading.mpCollectionTableItemScopes.RightCell;
        //            }

        //            //restore pending location to unspecified
        //            ((XmlCollectionTable)oParent).PendingInsertionLocation =
        //                XmlCollectionTable.ItemInsertionLocations.Unspecified;
        //        }

        //        //insert xml
        //        LMP.Forte.MSWord.Pleading oCOM = new LMP.Forte.MSWord.Pleading();
        //        LMP.Forte.MSWord.Tags oTags = this.ForteDocument.Tags;

        //        if (bContentControls && (bDesign || oCollectionCC != null))
        //        {
        //            oCOM.InsertCollectionTableItem_CC(xXML, oLocation, bAtCurrentRow,
        //                iScope, (short)this.ItemType, (short)this.CollectionType,
        //                ref oCollectionCC, ref oTags);
        //        }
        //        else if (bDesign || oCollectionTag != null)
        //        {
        //            oCOM.InsertCollectionTableItem(xXML, oLocation, bAtCurrentRow,
        //                iScope, (short)this.ItemType, (short)this.CollectionType,
        //                ref oCollectionTag, ref oTags);
        //        }
        //        else
        //        {
        //            oCOM.InsertCollectionTableItem_Bmk(xXML, oLocation, bAtCurrentRow,
        //                iScope, (short)this.ItemType, (short)this.CollectionType,
        //                ref oCollectionBmk, ref oTags);
        //        }

        //

        //        //add Required Document Variables
        //        oDoc.AddDocumentVariablesFromXML(this.ForteDocument.WordDocument, xXML);

        //        if (bContentControls && oCollectionCC != null)
        //        {
        //            if (oParent == null)
        //            {
        //                //get collection tag id
        //                string xTagID = oDoc.GetFullTagID_CC(oCollectionCC);

        //                //set parent
        //                oParent = this.ForteDocument.FindSegment(xTagID);
        //            }

        //            //refresh node stores of parent and siblings - this is necessary
        //            //because they may have been retagged -
        //            //GLOG 4948 (dm) - unremmed the remainder of this if block
        //            oParent.RefreshNodes();
        //            //if (bInEmptyExistingCell)
        //            //{
        //                for (int i = 0; i < oParent.Segments.Count; i++)
        //                    oParent.Segments[i].RefreshNodes();
        //            //}
        //        }
        //        else if (!bContentControls && oCollectionTag != null)
        //        {
        //            if (oParent == null)
        //            {
        //                //get collection tag id
        //                string xTagID = oDoc.GetFullTagID(oCollectionTag);

        //                //set parent
        //                oParent = this.ForteDocument.FindSegment(xTagID);
        //            }

        //            //refresh node stores of parent and siblings - this is necessary
        //            //because they may have been retagged
        //            oParent.RefreshNodes();

        //            //if (bInEmptyExistingCell)
        //            //{
        //                for (int i = 0; i < oParent.Segments.Count; i++)
        //                    oParent.Segments[i].RefreshNodes();
        //            //}
        //        }
        //        else if (oCollectionBmk != null)
        //        {
        //            if (oParent == null)
        //            {
        //                //get collection tag id
        //                string xTagID = oDoc.GetFullTagID_Bookmark(oCollectionBmk);

        //                //set parent
        //                oParent = this.ForteDocument.FindSegment(xTagID);
        //            }

        //            //refresh node stores of parent and siblings - this is necessary
        //            //because they may have been retagged -
        //            //GLOG 4948 (dm) - unremmed the remainder of this if block
        //            oParent.RefreshNodes();
        //            //if (bInEmptyExistingCell)
        //            //{
        //            for (int i = 0; i < oParent.Segments.Count; i++)
        //                oParent.Segments[i].RefreshNodes();
        //            //}
        //        }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.XMLException(
        //            LMP.Resources.GetLangString("Error_CouldNotInsertXml"), oE);
        //    }
        //}

        /// <summary>
        /// gets/sets whether this collection table item can share
        /// a row with another item
        /// </summary>
        public bool AllowSideBySide
        {
            get { return m_bAllowSideBySide; }
            set { m_bAllowSideBySide = value; }
        }

        /// <summary>
        /// gets the object type of this collection table item
        /// </summary>
        virtual public mpObjectTypes ItemType
        {
            get { return mpObjectTypes.CollectionTableItem; }
        }

        /// <summary>
        /// gets the appropriate collection table object type for
        /// this type of collection table item
        /// </summary>
        virtual public mpObjectTypes CollectionType
        {
            get { return mpObjectTypes.CollectionTable; }
        }
    }
}
