using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace LMP.Architect.Oxml
{
    public class XmlSnapshot
    {
        #region *********************fields*********************
        NameValueCollection m_oValues = new NameValueCollection();
        //List<string[]> m_aAuthors = null;
        public const char mpSnapshotFieldDelimiter = LMP.StringArray.mpEndOfElement;
        public const char mpSnapshotValueDelimiter = LMP.StringArray.mpEndOfRecord;
        public const char mpSnapshotAuthorsSeparator = '|';
        public const char mpSnapshotAuthorsValueSeparator = LMP.StringArray.mpEndOfValue;
        //private string m_xConfig = null;
        //private LMP.Data.mpObjectTypes m_iSegmentType = 0;
        //private Segment m_oSegment = null;
        private string m_xValueString = null;
        private string m_xConfigString = null;
        private XmlSegment m_oSegment = null;
        #endregion
        #region *********************constructors*********************
        /// <summary>
        /// creates a Snapshot populated with values 
        /// from the variables of the specified segment
        /// </summary>
        /// <param name="oVariables"></param>
        public XmlSnapshot(XmlSegment oSegment)
        {
            this.m_oSegment = oSegment;

            this.LoadValueStringFromDoc(oSegment);
            this.LoadConfigurationStringFromDoc(oSegment);

            ////if segment is deactivated, update deactivation data, prompting
            ////user to resolve any discrepancies with tag values (5/19/11 dm)
            //if (!oSegment.Activated)
            //    oSegment.ReviewData();
        }
        #endregion
        #region *********************properties*********************
        public string this[int iIndex]
        {
            get {return m_oValues[iIndex];}
        }
        /// <summary>
        /// returns the number of elements in the Snapshot
        /// </summary>
        public int ValuesCount
        {
            get {return m_oValues.Count;}
        }
        ///// <summary>
        ///// Returns List of Author string arrays
        ///// containing ID, LeadAuthor, FullName and BarID
        ///// </summary>
        ///// <returns></returns>
        //public List<string[]> Authors
        //{
        //    get
        //    {
        //        if (m_aAuthors == null)
        //        {
        //            m_aAuthors = new List<string[]>();
        //            string xAuthors = m_oValues["Authors"];
        //            if (!string.IsNullOrEmpty(xAuthors))
        //            {
        //                string[] aAuthors = xAuthors.Split(mpSnapshotAuthorsSeparator);
        //                for (int i = 0; i <= aAuthors.GetUpperBound(0); i++)
        //                {
        //                    if (aAuthors[i] != "")
        //                    {
        //                        string[] aValues;
        //                        //Snapshot might use old format with only Author ID
        //                        if (aAuthors[i].IndexOf(mpSnapshotAuthorsValueSeparator) > -1)
        //                        {
        //                            aValues = aAuthors[i].Split(mpSnapshotAuthorsValueSeparator);
        //                        }
        //                        else
        //                        {
        //                            aValues = new string[] { aAuthors[i] };
        //                        }
        //                        m_aAuthors.Add(aValues);
        //                    }
        //                }
        //            }
        //        }
        //        return m_aAuthors;
        //    }
        //}
        //public bool ContainsUpdatableContacts
        //{
        //    get
        //    {
        //        string xContent = this.ToString();
        //        return xContent.Contains("UNID=");
        //    }
        //}
        ///// <summary>
        ///// Returns Author information in AuthorSelector XML format
        ///// </summary>
        ///// <param name="iMaxAuthors"></param>
        ///// <returns></returns>
        //public string AuthorsXML(int iMaxAuthors)
        //{
        //    string xAuthorXML = "";
        //    for (int i = 0; i < this.Authors.Count && i < iMaxAuthors; i++)
        //    {
        //        string xID = "";
        //        string xIsLead = "";
        //        string xFullName = "";
        //        string xBarID = "";
        //        string[] aAuthors = this.Authors[i];
        //        xID = aAuthors[0];
        //        try
        //        {
        //            xIsLead = aAuthors[1];
        //            xFullName = aAuthors[2];
        //            xBarID = aAuthors[3];
        //        }
        //        catch { }
        //        //GLOG 3719: Needed to replace xID with xIsLead for correct evaluation of LeadAuthor
        //        //GLOG 5034: Replace any reserved XML characters in Author Name or Bar ID
        //        xAuthorXML += string.Concat("<Author Lead=",
        //            xIsLead == "-1" ? "\"-1\"" : "\"0\"", " ID=\"",
        //            xID, "\" FullName=\"", LMP.String.ReplaceXMLChars(xFullName), "\" BarID=\"", LMP.String.ReplaceXMLChars(xBarID), "\"></Author>");
        //    }
        //    return xAuthorXML;
        //}
        public string Configuration
        {
            get { return m_xConfigString; }
        }
        /// <summary>
        /// returns the Snapshot as a delimited string of name/value pairs
        /// </summary>
        /// <returns></returns>
        public string Values
        {
            get
            {
                StringBuilder oSB = new StringBuilder();

                //cycle through all prefill elements,
                //adding each to the string
                for (int i = 0; i < m_oValues.Count; i++)
                    oSB.AppendFormat("{0}" + mpSnapshotValueDelimiter.ToString() + 
                        "{1}" + mpSnapshotFieldDelimiter.ToString(),
                        m_oValues.Keys[i], m_oValues[i]);

                if (oSB.Length > 0)
                {
                    // Remove final delimiter
                    oSB.Remove(oSB.Length - 1, 1);
                }

                return oSB.ToString();
            }
        }
        #endregion
        #region *********************methods********************
        /// <summary>
        /// returns the snapshot value with the specified name
        /// </summary>
        /// <param name="xVarName"></param>
        /// <returns></returns>
        public string GetValue(string xVarName)
        {
            string xSnapshot = m_xValueString;
            int iPos = xSnapshot.IndexOf('�' + xVarName + "�");
            if (iPos > -1)
            {
                iPos = xSnapshot.IndexOf('�', iPos);
                //GLOG 6364 (dm) - if var value was empty, return value
                //included the next name/value pair
                //int iPos2 = xSnapshot.IndexOf('�', iPos + 2);
                int iPos2 = xSnapshot.IndexOf('�', iPos + 1);
                return xSnapshot = xSnapshot.Substring(iPos + 1, iPos2 - iPos - 1);
            }
            else
            {
                return "";
            }
        }
        /// <summary>
        /// sets the value of the specified element to the specified value
        /// </summary>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        public void SetValue(string xName, string xValue)
        {
            m_oValues.Set(xName, xValue);
        }
        /// <summary>
        /// deletes the element with the specified name
        /// </summary>
        /// <param name="xName"></param>
        public void DeleteValue(string xName)
        {
            m_oValues.Remove(xName);
        }
        public XmlPrefill ToPrefill()
        {
            string xContentString = GetSegmentPrefillString(m_oSegment);
            xContentString = xContentString.TrimEnd('�');
            return new XmlPrefill(xContentString, m_oSegment.Name, m_oSegment.ID);
        }
        private string GetSegmentPrefillString(XmlSegment oSegment)
        {
            string xPrefillString = null;

            //cycle recursively through children,
            //building the prefill string
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                XmlSegment oChild = oSegment.Segments[i];

                string xPrefillStringAppendage = GetSegmentPrefillString(oChild);
                xPrefillString += xPrefillStringAppendage;
            }

            //TODO: OpenXML rewrite
            //if (XmlSnapshot.Exists(oSegment))
            //{
            //    //get values, and add to prefill string
            //    string xSnapshot = oSegment.Snapshot.Values;
            //    xPrefillString += oSegment.FullTagID + "." + xSnapshot.Replace("�", "�" + oSegment.FullTagID + ".");
            //    xPrefillString = xPrefillString.TrimEnd('|');
            //    xPrefillString += "�";
            //    xPrefillString = System.Text.RegularExpressions.Regex.Replace(xPrefillString, "_{.*?}", "");

            //    XmlSegment oTopLevelSegment = oSegment;
            //    XmlSegment oParent = oSegment.Parent;
            //    while (oParent != null)
            //    {
            //        oTopLevelSegment = oParent;
            //        oParent = oParent.Parent;
            //    }

            //    xPrefillString = xPrefillString.Replace(oTopLevelSegment.Name + ".", "");

            //    return xPrefillString;
            //}
            //else
                return "";
        }
        #endregion
        #region *********************static members*********************
        /// <summary>
        /// creates a snapshot of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public static void Create(XmlSegment oSegment)
        {
            ////GLOG 5814 (dm) - don't create snapshot for text segments
            //if ((!XmlSnapshot.Exists(oSegment)) &&
            //    (oSegment.IntendedUse != LMP.Data.mpSegmentIntendedUses.AsParagraphText) &&
            //    (oSegment.IntendedUse != LMP.Data.mpSegmentIntendedUses.AsSentenceText))
            //{
            //    XmlSnapshot oSnapshot = new XmlSnapshot(oSegment);

            //    oSnapshot.CreateConfigurationXML(oSegment);
            //    oSnapshot.CreateValuesCollection(oSegment);

            //    //encrypt snapshot config string
            //    string xSnapshotConfig = LMP.String.Encrypt(
            //        oSnapshot.m_xConfigString,
            //        LMP.Data.Application.EncryptionPassword);

            //    object oName = "FinishedConfig_" + oSegment.TagID;

            //    //add snapshot configuration doc var
            //    oSegment.ForteDocument.WordDocument.Variables
            //        .get_Item(ref oName).Value = xSnapshotConfig;

            //    SaveSnapshotValuesDocVar(oSegment.ForteDocument, oSegment.TagID, oSnapshot.Values);
            //}

            ////cycle through child segments, adding a snapshot of each
            //for (int j = 0; j < oSegment.Segments.Count; j++)
            //{
            //    Create(oSegment.Segments[j]);
            //}
        }

        /// <summary>
        /// refreshes the snapshot from the doc var
        /// </summary>
        public void Refresh()
        {
            this.LoadValueStringFromDoc(m_oSegment);
            this.LoadConfigurationStringFromDoc(m_oSegment);
        }

        /// <summary>
        /// returns true iff the segment has a snapshot
        /// </summary>
        //public static bool Exists(XmlSegment oSegment)
        //{
            //string xSnapshot = null;
            //object oName = "FinishedConfig_" + oSegment.TagID;
            //try
            //{
            //    xSnapshot = oSegment.ForteDocument.WordDocument.Variables.get_Item(ref oName).Value;
            //}
            //catch { }
            //return !string.IsNullOrEmpty(xSnapshot);
        //}
        public void Save(string xValues)
        {
            CreateValuesCollection(xValues);
            XmlSnapshot.SaveSnapshotValuesDocVar(m_oSegment.ForteDocument, m_oSegment.TagID, xValues); 
        }
        private static void SaveSnapshotValuesDocVar(XmlForteDocument oMPDoc, string xSegmentTagID, string xValues)
        {
            //object oName = null;

            ////encrypt snapshot values
            //string xSnapshotValues = LMP.String.Encrypt(xValues,
            //    LMP.Data.Application.EncryptionPassword);

            ////store snapshot values in doc vars -
            ////split in to 65K chunks to avoid
            ////overflowing the capacity of a doc var
            //string xRemainingChars = xSnapshotValues;
            //int i = 1;
            //while (xRemainingChars.Length > 65000)
            //{
            //    string xBuffer = xRemainingChars.Substring(0, 65000);
            //    xRemainingChars = xRemainingChars.Substring(65001);

            //    oName = "Finished_" + i.ToString() + "_" + xSegmentTagID;
            //    oMPDoc.WordDocument.Variables.get_Item(ref oName).Value = xBuffer;
            //    i++;
            //}

            //oName = "Finished_" + i.ToString() + "_" + xSegmentTagID;
            //oMPDoc.WordDocument.Variables.get_Item(ref oName).Value = xRemainingChars;
        }

        /// <summary>
        /// deletes the snapshot doc vars for oSegment and children if specified
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="bDoChildren"></param>
        public static void Delete(XmlSegment oSegment, bool bDoChildren)
        {
            //if (bDoChildren)
            //{
            //    for (int i = 0; i < oSegment.Segments.Count; i++)
            //        XmlSnapshot.Delete(oSegment.Segments[i], true);
            //}

            //Word.Document oDoc = oSegment.ForteDocument.WordDocument;
            //object oName = null;
            //try
            //{
            //    oName = "FinishedConfig_" + oSegment.TagID;
            //    oDoc.Variables.get_Item(ref oName).Delete();
            //}
            //catch { }

            //Word.Variable oVar = null;
            //try
            //{
            //    int i = 1;
            //    oName = "Finished_" + i.ToString() + "_" + oSegment.TagID;
            //    oVar = oDoc.Variables.get_Item(ref oName);
            //    while (oVar != null)
            //    {
            //        oVar.Delete();
            //        oVar = null;
            //        i++;
            //        oName = "Finished_" + i.ToString() + "_" + oSegment.TagID;
            //        oVar = oDoc.Variables.get_Item(ref oName);
            //    }
            //}
            //catch { }
        }

        /// <summary>
        /// returns authors in snapshot format - added for GLOG 7126 (dm)
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <returns></returns>
        public static string GetAuthorsSnapshotValue(XmlAuthors oAuthors)
        {
            string xAuthorIDs = "";
            for (short i = 1; i <= oAuthors.Count; i++)
            {
                LMP.Data.Author oAuthor = oAuthors.ItemFromIndex(i);
                string xLeadAuthor = oAuthors.GetLeadAuthor().ID;
                //Save ID, Is LeadAuthor, FullName and BarID
                xAuthorIDs = xAuthorIDs + oAuthor.ID +
                    mpSnapshotAuthorsValueSeparator + (oAuthor.ID == xLeadAuthor ? "-1" : "0") +
                    mpSnapshotAuthorsValueSeparator + oAuthor.FullName +
                    mpSnapshotAuthorsValueSeparator + oAuthor.BarID + mpSnapshotAuthorsSeparator.ToString();
            }
            return xAuthorIDs;
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// returns the snapshot value string
        /// </summary>
        private void LoadValueStringFromDoc(XmlSegment oSegment)
        {
            string xSnapshot = null;
            string xPartialSnapshot = null;
            int i = 1;
            object oName = "Finished_" + i.ToString() + "_" + oSegment.TagID;
            try
            {
                //TODO: OpenXML rewrite
                //xPartialSnapshot = oSegment.ForteDocument
                //    .WordDocument.Variables.get_Item(ref oName).Value;
            }
            catch { }

            while (xPartialSnapshot != null)
            {
                xSnapshot += xPartialSnapshot;
                i++;
                oName = "Finished_" + i.ToString() + "_" + oSegment.TagID;
                xPartialSnapshot = null;

                try
                {
                    //TODO: OpenXML rewrite
                    //xPartialSnapshot = oSegment.ForteDocument.WordDocument
                    //    .Variables.get_Item(ref oName).Value;
                }
                catch { }
            }

            string xPWD = LMP.Data.Application.EncryptionPassword;
            m_xValueString = LMP.String.Decrypt(xSnapshot, ref xPWD);
            this.CreateValuesCollection(xSnapshot);
        }
        /// <summary>
        /// returns the snapshot task pane configuration string
        /// </summary>
        /// <param name="oSegment"></param>
        private void LoadConfigurationStringFromDoc(XmlSegment oSegment)
        {
            try
            {
                string xSnapshot = null;
                object oName = "FinishedConfig_" + oSegment.TagID;
                try
                {
                    //TODO: OpenXML rewrite
                    //xSnapshot = oSegment.ForteDocument.WordDocument.Variables.get_Item(ref oName).Value;
                }
                catch { }

                m_xConfigString = LMP.String.Decrypt(xSnapshot);
                //GLOG 6429: Avoid errors when Display Name contains quotes
                int iStartName = m_xConfigString.IndexOf("Segment Name=\"");
                if (iStartName > -1)
                {
                    iStartName = iStartName + "Segment Name=\"".Length;
                    int iEndName = m_xConfigString.IndexOf("\" IntendedUse=", iStartName);
                    if (iEndName == -1)
                        iEndName = m_xConfigString.IndexOf("\" TagID=");
                    if (iEndName > -1)
                    {
                        string xName = m_xConfigString.Substring(iStartName, iEndName - iStartName).Replace("\"", "&amp;&quot;");
                        m_xConfigString = m_xConfigString.Substring(0, iStartName) + xName + m_xConfigString.Substring(iEndName);
                    }

                }
            }
            catch
            {
                m_xConfigString = null;
            }
        }
        /// <summary>
        /// loads the 
        /// </summary>
        /// <param name="oSegment"></param>
        private void CreateConfigurationXML(XmlSegment oSegment)
        {
            //Snapshot has the following form:
            // Parent Segment Display Name, Intended Use
            // Child Segment 1
            // Variable 1 Display Name, Variable Name, Control, Control Configuration, Display Value Expression, Display, Hot Key, CI Config, CIAlerts, Display Level
            // Variable 2 Display Name, Variable Name, Control, Control Configuration, Display Value Expression, Display, Hot Key, CI Config, CIAlerts, Display Level
            // Child Segment 2
            // Variable 1 Display Name, Variable Name, Control, Control Configuration, Display Value Expression, Display, Hot Key, CI Config, CIAlerts, Display Level
            // Variable 2 Display Name, Variable Name, Control, Control Configuration, Display Value Expression, Display, Hot Key, CI Config, CIAlerts, Display Level
            // Variable 1 Display Name, Variable Name, Control, Control Configuration, Display Value Expression, Display, Hot Key, CI Config, CIAlerts, Display Level
            // Variable 2 Display Name, Variable Name, Control, Control Configuration, Display Value Expression, Display, Hot Key, CI Config, CIAlerts, Display Level

            StringBuilder oSB = new StringBuilder();
            //GLOG 6429: replace reserved XML characters in Display Name
            oSB.AppendFormat("<Segment Name=\"{0}\" IntendedUse=\"{1}\" TagID=\"{2}\">",
                LMP.String.ReplaceXMLChars(oSegment.DisplayName.Replace("\"", "&quot;")), ((int)oSegment.IntendedUse).ToString(), oSegment.FullTagID);

            for (int i = 0; i < oSegment.Variables.Count; i++)
            {
                XmlVariable oVar = oSegment.Variables[i];

                //cycle through variable actions, getting the definitions
                //of set variable value and run variable actions actions
                string xVariableActionDefs = "";
                string xControlActionDefs = "";

                for (int j = 0; j < oVar.VariableActions.Count; j++)
                {
                    XmlVariableAction oAction = oVar.VariableActions[j];

                    if (oAction.Type == XmlVariableActions.Types.SetVariableValue ||
                        oAction.Type == XmlVariableActions.Types.RunVariableActions)
                    {
                        xVariableActionDefs += oAction.ToString() + "|";
                    }
                }

                for (int j = 0; j < oVar.ControlActions.Count; j++)
                {
                    XmlControlAction oAction = oVar.ControlActions[j];

                    xControlActionDefs += oAction.ToString() + "|";
                }

                xControlActionDefs = xControlActionDefs.TrimEnd('|');

                //GLOG 5799 (dm) - added translation id and IsMultiValue
                //GLOG 7126 (dm) - added DefaultValue
                oSB.AppendFormat("<Variable DisplayName=\"{0}\" Name=\"{1}\" Control=\"{2}\" ControlProperties=\"{3}\" DisplayValue=\"{4}\" Display=\"{5}\" HotKey=\"{6}\" CI=\"{7}\" CIAlerts=\"{8}\" DisplayLevel=\"{9}\" TranslationID=\"{10}\" IsMultiValue=\"{11}\" ActionDefs=\"{12}\" CtlActionDefs=\"{13}\" DefaultValue=\"{14}\"/>",
                    String.ReplaceXMLChars(oVar.DisplayName.Replace("\"", "&quot;")), 
                    String.ReplaceXMLChars(oVar.Name.Replace("\"", "&quot;")), 
                    ((int)oVar.ControlType).ToString(), String.ReplaceXMLChars(oVar.ControlProperties.Replace("\"", "&quot;")),
                    oVar.DisplayValue, ((oVar.DisplayIn & XmlVariable.ControlHosts.DocumentEditor) == 
                    XmlVariable.ControlHosts.DocumentEditor) ? "true" : "false", 
                    oVar.Hotkey, oVar.CIDetailTokenString, oVar.CIAlerts, ((int)oVar.DisplayLevel).ToString(),
                    oVar.TranslationID.ToString(), oVar.IsMultiValue.ToString(),
                    xVariableActionDefs, xControlActionDefs, oVar.DefaultValue);
            }

            oSB.Append("</Segment>");
            m_xConfigString = oSB.ToString();
        }
        /// <summary>
        /// Populate name/value pairs by parsing input string
        /// </summary>
        /// <param name="xContentString"></param>
        private void CreateValuesCollection(string xValuesString)
        {
            if (!string.IsNullOrEmpty(xValuesString))
            {
                string xPWD = LMP.Data.Application.EncryptionPassword;
                xValuesString = LMP.String.Decrypt(xValuesString, ref xPWD);
                string[] aVars = xValuesString.Split(mpSnapshotFieldDelimiter);
                for (int i = 0; i < aVars.Length; i++)
                {
                    if (aVars[i] != "")
                    {
                        string[] aVals = aVars[i].Split(mpSnapshotValueDelimiter);
                        m_oValues.Set(aVals[0], aVals[1]);
                    }
                }
            }
        }
        /// <summary>
        /// adds all the variable values 
        /// in the specified segment to the Snapshot
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xBasePath"></param>
        private void CreateValuesCollection(XmlSegment oSegment)
        {
            //cycle through all variables in the segment,
            //adding the variable values from each
            for (int i = 0; i < oSegment.Variables.Count; i++)
            {
                XmlVariable oVar = oSegment.Variables[i];
                string xValue = oVar.Value;

                //GLOG 7965 (dm) - replace shift-return placeholders used by TOC -
                //these may be in the value because the last refresh occurred
                //during TOC pleading paper insertion
                xValue = xValue.Replace("ZZMPSHIFTRETURN", "\v");
                xValue = xValue.Replace("zzmpShiftReturn", "\v");

                //GLOG 5894: If Variable takes value from CI, store CI Detail Token String in Prefill
                if ((oVar.CIContactType != 0 || oVar.CIEntityName != "") && xValue.Contains("UNID=") && oVar.CIDetailTokenString != "")
                {
                    xValue = xValue + "<CIDetailTokenString>" + oVar.CIDetailTokenString + "</CIDetailTokenString>";
                }
                //GLOG 3493: If Value Source Expression uses [DateFormat] fieldcode
                //save the format, even if current Value is a manually-entered date
                if (oVar.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT_"))
                {
                    DateTime oDT = DateTime.MinValue;
                    if (DateTime.TryParse(xValue, out oDT))
                        //Value is Date Text, convert to format string
                        xValue = LMP.Architect.Base.Application.GetDateFormatPattern(xValue);
                }
                if (m_oValues[oVar.Name] != null)
                {
                    //key already exists - cycle through
                    //counter finding first available index
                    for (int j = 1; true; j++)
                    {
                        if (m_oValues[j.ToString() + oVar.Name] == null)
                        {
                            //index is available
                            m_oValues.Set(j.ToString() + "." + oVar.Name, xValue);
                            break;
                        }
                    }
                }
                else
                    m_oValues.Set(oVar.Name, xValue);
            }

            //GLOG item #34384 - save segment language
            m_oValues.Set("Culture", oSegment.Culture.ToString());

            //Also save segment authors for top-level only
            if (oSegment.Authors.Count > 0)
            {
                //GLOG 7126 (dm) - added separate method to get value
                string xAuthorIDs = GetAuthorsSnapshotValue(oSegment.Authors);
                m_oValues.Set("Authors", xAuthorIDs);
            }

            //Save top-level segment court levels
            if (oSegment.LevelChooserRequired())
            {
                XmlAdminSegment oASeg = (XmlAdminSegment)oSegment;
                m_oValues.Set("L0", oASeg.L0.ToString());
                m_oValues.Set("L1", oASeg.L1.ToString());
                m_oValues.Set("L2", oASeg.L2.ToString());
                m_oValues.Set("L3", oASeg.L3.ToString());
                m_oValues.Set("L4", oASeg.L4.ToString());
            }
        }
        #endregion
    }
}
