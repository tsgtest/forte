using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;
using System.Text;

namespace LMP.Architect.Oxml
{
	public enum mpNodeStoreMatchTypes
	{
		Any = 1,
		Equals = 2,
		StartsWith = 3,
		Contains = 4
	}

	public class ReadOnlyNodeStore:NameObjectCollectionBase
	{
		protected XmlDocument m_oXML;
		protected XmlNodeList m_oChildNodes;
        protected fCOM.cTags m_oTags;
        protected LMP.Data.mpFileFormats m_oBoundingObjectType =
            LMP.Data.mpFileFormats.None;

		#region *********************constructors*********************
		/// <summary>
		/// constructor
		/// </summary>
        protected ReadOnlyNodeStore(fCOM.cTags oTags)
		{
            m_oTags = oTags;
			m_oXML = new XmlDocument();
			m_oXML.LoadXml("<Nodes></Nodes>");

			//get child nodes - for performance, we get this only once
			m_oChildNodes = m_oXML.DocumentElement.ChildNodes;
		}
		#endregion
		#region *********************properties*********************
        public string XML
        {
            get { return m_oXML.OuterXml; }
        }

        public LMP.Data.mpFileFormats Type
        {
            get { return m_oBoundingObjectType; }
            set { m_oBoundingObjectType = value; }
        }
		#endregion
		#region *********************methods*********************
		public override int Count
		{
			get
			{
				return m_oChildNodes.Count;
			}
		}

		/// <summary>
		/// returns the tags with the specified ID
		/// </summary>
		/// <param name="xTagID"></param>
		/// <returns></returns>
		public Word.XMLNode[] GetWordTags(string xTagID)
		{
            try
            {
                DateTime t0 = DateTime.Now;

                Trace.WriteNameValuePairs("xTagID", xTagID);
                Word.XMLNode[] aTags = null;

                //get xml node with specified tag id
                XmlNode oNode = m_oXML.SelectSingleNode(
                    "/Nodes/Node[@TagID='" + xTagID + "']");

                //get the hash IDs of selected node
                string xIDs = oNode.Attributes["HashID"].Value;

                //parse hash id string to retrieve hash id of specified part
                string[] aHashIDs = xIDs.Split(';');

                //create Word tag array
                Word.XMLNode[] aTagsTmp = new Word.XMLNode[aHashIDs.Length];

                //cycle through hash IDs adding the corresponding
                //Word tag to the array
                int iCountNonNull = 0;
                bool bNullsExist = false;

                //cycle through hash IDs adding the corresponding
                //Word tag to the array
                for(int i=0; i < aHashIDs.Length; i++)
                {
                    string xHashID = aHashIDs[i];

                    Word.XMLNode oWordNode = this.BaseGet(xHashID) as Word.XMLNode;
                    if (oWordNode != null)
                    {
                        //get the Word tag with the specified hash ID
                        aTagsTmp[i] = (Word.XMLNode)this.BaseGet(xHashID);

                        iCountNonNull++;
                    }
                    else
                    {
                        bNullsExist = true;
                    }
                }

                if (bNullsExist)
                {
                    aTags = new Word.XMLNode[iCountNonNull];
                    int j = 0;
                    foreach (Word.XMLNode oWordNode in aTagsTmp)
                    {
                        if (oWordNode != null)
                        {
                            aTags[j] = oWordNode;
                            j++;
                        }
                    }
                }
                else
                {
                    aTags = aTagsTmp;
                }

                LMP.Benchmarks.Print(t0, xTagID);
                return aTags;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_WordXmlNodeDoesNotExist") + xTagID, oE);
            }
        }

		/// <summary>
		/// returns the Word tag with the specified ID and part
		/// </summary>
		/// <param name="xTagID"></param>
		/// <param name="xPart"></param>
		/// <returns></returns>
		public Word.XMLNode GetWordTag(string xTagID, int iPart)
		{
            try
            {
                Trace.WriteNameValuePairs("xTagID", xTagID, "iPart", iPart);

                //get xml node with specified tag id
                XmlNode oNode = m_oXML.SelectSingleNode(
                    "/Nodes/Node[@TagID='" + xTagID + "']");

                //get the associated parts
                string xParts = oNode.Attributes["Parts"].Value;
                string[] aParts = xParts.Split(';');

                //get the index of the specified part
                int iIndex = 0;
                for (int i = 0; i < aParts.Length; i++)
                {
                    if (aParts[i] == iPart.ToString())
                    {
                        iIndex = i;
                        break;
                    }
                }

                //get the hash ID of selected node
                string xIDs = oNode.Attributes["HashID"].Value;

                //parse hash id string to retrieve hash id of specified part
                string[] aHashIDs = xIDs.Split(';');
                string xID = aHashIDs[iIndex];

                //get the Word Tag with the specified hash ID
                Word.XMLNode oXMLNode = null;

                try
                {
                    //ignore type mismatch error, as the hashtable value
                    //may be of type Word.Bookmark
                    oXMLNode = (Word.XMLNode)this.BaseGet(xID);
                }
                catch { }

                return oXMLNode;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_WordXmlNodeDoesNotExist") + xTagID, oE);
            }
		}

		/// <summary>
		/// returns the Word tag at the specified index
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		public Word.XMLNode GetWordTag(int iIndex)
		{
			return (Word.XMLNode) this.BaseGet(iIndex);
		}

		/// <summary>
		/// returns the xml node with the specified ID
		/// </summary>
		/// <param name="xTagID"></param>
		/// <returns></returns>
		public XmlNode GetItem(string xTagID)
		{
    		Trace.WriteNameValuePairs("xTagID", xTagID);

			XmlNode oNode = m_oXML.SelectSingleNode(
				"/Nodes/Node[@TagID='" + xTagID + "']");

			if(oNode == null)
				//node doesn't exist - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_NodeDoesNotExist") + 
					xTagID);

			return oNode;
		}

		/// <summary>
		/// returns the node at the specified index
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		public XmlNode GetItem(int iIndex)
		{
			XmlNode oChildNode = null;

			try
			{
				oChildNode = this.m_oChildNodes[iIndex];
			}
			catch(System.Exception oE)
			{
				if(iIndex >= m_oXML.DocumentElement.ChildNodes.Count || iIndex < 0)
					throw new LMP.Exceptions.NotInCollectionException(
						LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);
				else
					throw oE;
			}

			return oChildNode;
		}

		/// <summary>
		/// integer indexer for class
		/// </summary>
		public XmlNode this[int iIndex]
		{
			get{return this.GetItem(iIndex);}
		}

		/// <summary>
		/// string indexer for class
		/// </summary>
		public XmlNode this[string xTagID]
		{
			get{return this.GetItem(xTagID);}
		}

		/// <summary>
		/// retrieves the nodes contained in the specified segment
		/// </summary>
		/// <param name="xSegmentTagID"></param>
		/// <returns></returns>
		public ReadOnlyNodeStore GetSegmentNodes(string xSegmentTagID, string xDocID)
		{
			Trace.WriteNameValuePairs("xSegmentTagID", xSegmentTagID, "xDocID", xDocID);

			//build XPath query to get all nodes beginning with the specified segment and doc ids
			string xXPath = @"//Node[starts-with(@TagID, '" + xDocID + "." + xSegmentTagID + ".')]";
			return GetNodes(xXPath);
		}

		/// <summary>
		/// returns the nodes that match the specified XPath query
		/// </summary>
		/// <param name="xXPath"></param>
		/// <returns></returns>
		public ReadOnlyNodeStore GetNodes(string xXPath)
		{
			DateTime t0 = DateTime.Now;
			
			System.Text.StringBuilder oSB = new StringBuilder();

			Trace.WriteNameValuePairs("xXPath", xXPath);

			//get all nodes that match the specified XPath query
			XmlNodeList oNodes = m_oXML.SelectNodes(xXPath);

			//create empty node store collection
			ReadOnlyNodeStore oNS = new ReadOnlyNodeStore(m_oTags);

			foreach(XmlNode oNode in oNodes)
			{
				//get xml node id
				string xID = oNode.Attributes["HashID"].Value;

                //get bounding object from hash table
                object oBoundingObject = this.BaseGet(xID);

                if (oBoundingObject is Word.XMLNode)
                {
                    //add Word xml node to new node store
                    oNS.BaseAdd(xID, (Word.XMLNode)oBoundingObject);
                }
                else if (oBoundingObject is Word.ContentControl)
                {
                    //add content control to new node store
                    oNS.BaseAdd(xID, (Word.ContentControl)oBoundingObject);
                }
                else
                {
                    //add bookmark to new node store
                    oNS.BaseAdd(xID, (Word.Bookmark)oBoundingObject);
                }

                XmlAttributeCollection oAttrs = oNode.Attributes;

                //TODO: figure out why child segments in letter have
                //oAttrs["AssociatedParentVariable"] == null - this is not
                //null for any other attribute.  Also, the attribute is not
                //null when the segment is not used as a child segment
                XmlAttribute oAssocParentVarAttr = oAttrs["AssociatedParentVariable"];
                oSB.AppendFormat("<Node TagID='{0}' HashID='{1}' ObjectData='{2}' ElementName='{3}' Parts='{4}' Authors='{5}' AssociatedParentVariable='{6}'/>",
                    oAttrs["TagID"].Value, xID, String.ReplaceXMLChars(oAttrs["ObjectData"].Value),
                    oAttrs["ElementName"].Value, oAttrs["Parts"].Value,
                    String.ReplaceXMLChars(oAttrs["Authors"].Value), oAssocParentVarAttr == null ?
                    "" : oAssocParentVarAttr.Value);
            }

			oSB.Append("</Nodes>");

			//append new string to existing XML string
            string xReplace = oSB.ToString();
			oNS.m_oXML.InnerXml = oNS.m_oXML.InnerXml.Replace("</Nodes>", xReplace);
			oNS.m_oChildNodes = oNS.m_oXML.DocumentElement.ChildNodes;
			LMP.Benchmarks.Print(t0);
			return oNS;
		}

		/// <summary>
		/// returns the names of the top level segments
		/// </summary>
		public string[] GetTopLevelSegmentNames()
		{
			DateTime t0 = DateTime.Now;

			//get all top level segment nodes - those that contain exactly one "."
			const string mpXPath = @"/Nodes/Node[not(contains(substring-after(@TagID,'.'), '.'))]";
			XmlNodeList oNodes = m_oXML.SelectNodes(mpXPath);
			
			StringBuilder oSB = new StringBuilder("|");

			//cycle through nodes adding name if not already added
			for(int i = 0; i < oNodes.Count; i++)
			{
				//get full tag
				string xTagID = oNodes[i].Attributes["TagID"].Value;

				//parse out DocID
				int iPos = xTagID.IndexOf(".");
				string xName = xTagID.Substring(iPos + 1);
				if(oSB.ToString().IndexOf(xName) == -1)
					//name not added - add name to delimited string
					oSB.Append(xName + "|");
			}

			//trim string to remove starting and trailing pipes
			string xNames = oSB.ToString().Substring(1, oSB.Length - 2);

			Trace.WriteNameValuePairs("xNames", xNames);

			LMP.Benchmarks.Print(t0);

			//convert string to array
			return xNames.Split('|');
		}

		/// <summary>
		/// returns a node store containing those nodes 
		/// whose TagID contains the specified search string
		/// </summary>
		/// <param name="xSearch"></param>
		public ReadOnlyNodeStore GetMatchingNodes(string xTagIDMatchString, mpNodeStoreMatchTypes oMatchType)
		{
			Trace.WriteNameValuePairs("xTagIDMatchString", xTagIDMatchString);

			//build XPath string depending on match type
			string xXPath = null;
			if(oMatchType == mpNodeStoreMatchTypes.Contains)
				xXPath= @"//Node[contains(@TagID,'" + xTagIDMatchString + "')]";
			else if(oMatchType == mpNodeStoreMatchTypes.Equals)
				xXPath= @"//Node[@TagID='" + xTagIDMatchString + "']";
			else if(oMatchType == mpNodeStoreMatchTypes.StartsWith)
				xXPath= @"//Node[starts-with(@TagID,'" + xTagIDMatchString + "')]";
			else if(oMatchType == mpNodeStoreMatchTypes.Any)
				xXPath= @"//Node";

			return GetNodes(xXPath);
		}

		/// <summary>
		/// returns the object data for the node with the specified ID
		/// </summary>
		/// <param name="xNodeID"></param>
		/// <returns></returns>
		public string GetItemObjectData(string xTagID)
		{
			string xObjectData = this.GetNodeAttributeValue(xTagID, "ObjectData");

			//ensure that object data string starts and ends with a pipe
			if(!xObjectData.StartsWith("|"))
				xObjectData = "|" + xObjectData;
			if(!xObjectData.EndsWith("|"))
				xObjectData = xObjectData + "|";

            ////delimited TagPrefixId from name/value pair
            //xObjectData = xObjectData.Replace("¯°¯", "¯°¯|");

            //restore XML start tag
            xObjectData = xObjectData.Replace("¬", "<");

			return LMP.String.RestoreXMLChars(xObjectData);
		}

		/// <summary>
		/// returns the TagID of the item at the specified index
		/// </summary>
		/// <param name="iIndex"></param>
		/// <returns></returns>
		public string GetItemTagID(int iIndex)
		{
			if(iIndex < 0 || iIndex > this.Count)
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);

			return this[iIndex].Attributes["TagID"].Value;
		}
		/// <summary>
		/// returns the Element Name of the item at the specified index
		/// </summary>
		/// <param name="iIndex"></param>
		/// <returns></returns>
		public string GetItemElementName(int iIndex)
		{
			if(iIndex < 0 || iIndex > this.Count)
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);

			return this[iIndex].Attributes["ElementName"].Value;
		}
		/// <summary>
		/// returns a semi-colon delimited list of the part numbers
        /// of the item at the specified index
		/// </summary>
		/// <param name="iIndex"></param>
		/// <returns></returns>
		public string GetItemPartNumbers(int iIndex)
		{
			if(iIndex < 0 || iIndex > this.Count)
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);

            //get node
            XmlNode oNode = this[iIndex];

            //get part numbers
            return GetItemPartNumbers(oNode);
        }

        /// <summary>
        /// returns a semi-colon delimited list the part numbers
        /// of the item with the specified tag ID
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public string GetItemPartNumbers(string xTagID)
        {
            //get node
            XmlNode oNode = this[xTagID];

            //get part numbers
            return GetItemPartNumbers(oNode);
        }

        /// <summary>
        /// returns the number of parts of the item at the specified index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public int GetItemPartsCount(int iIndex)
        {
            int iCount = 0;
            string xParts = GetItemPartNumbers(iIndex);
            if (xParts != null)
            {
                string[] aParts = xParts.Split(';');
                iCount = aParts.Length;
            }
            return iCount;
        }

        /// <summary>
        /// returns the number of parts of the item with the specified tag id
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public int GetItemPartsCount(string xTagID)
        {
            int iCount = 0;
            string xParts = GetItemPartNumbers(xTagID);
            if (xParts != null)
            {
                string[] aParts = xParts.Split(';');
                iCount = aParts.Length;
            }
            return iCount;
        }

        /// <summary>
        /// returns the object data of the item at the specified index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public string GetItemObjectData(int iIndex)
		{
			if(iIndex < 0 || iIndex > this.Count)
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);

			string xValue = this[iIndex].Attributes["ObjectData"].Value;
            //restore xml start tags
            xValue = xValue.Replace("¬", "<");
            return LMP.String.RestoreXMLChars(xValue);
		}

        /// <summary>
        /// returns the deleted scopes for the item at the specified index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public string GetItemDeletedScopes(int iIndex)
        {
            if (iIndex < 0 || iIndex > this.Count)
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);

            string xValue = this[iIndex].Attributes["DeletedScopes"].Value;
            return LMP.String.RestoreXMLChars(xValue);
        }

        /// <summary>
        /// returns the deleted scopes for the item with the specified ID
        /// </summary>
        /// <param name="xNodeID"></param>
        /// <returns></returns>
        public string GetItemDeletedScopes(string xTagID)
        {
            return this.GetNodeAttributeValue(xTagID, "DeletedScopes");
        }

        /// <summary>
        /// returns the AssociatedParentVariable attribute for the item at the specified index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public string GetItemAssociatedParentVariable(int iIndex)
        {
            if (iIndex < 0 || iIndex > this.Count)
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);

            return this[iIndex].Attributes["AssociatedParentVariable"].Value;
        }

        /// <summary>
        /// returns the AssociatedParentVariable attribute for the item with the specified ID
        /// </summary>
        /// <param name="xNodeID"></param>
        /// <returns></returns>
        public string GetItemAssociatedParentVariable(string xTagID)
        {
            return this.GetNodeAttributeValue(xTagID, "AssociatedParentVariable");
        }
        /// <summary>
        /// returns the authors attribute value
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public string GetItemAuthorsData(string xTagID)
        {
            string xAuthors = this.GetNodeAttributeValue(xTagID, "Authors");

            //restore XML start tag
            xAuthors = xAuthors.Replace("¬", "<");

            //GLOG 5034: XML Characters are already restored at this point
            //Don't run RestoreXMLChars again, because any XML tokens within
            //the Author data should remain
            //xAuthors = LMP.String.RestoreXMLChars(xAuthors);

            return xAuthors;
        }

        /// <summary>
		/// returns the element name for the node with the specified ID
		/// </summary>
		/// <param name="xNodeID"></param>
		/// <returns></returns>
		public string GetItemElementName(string xTagID)
		{
			return this.GetNodeAttributeValue(xTagID, "ElementName");
		}
		public ReadOnlyNodeStore GetmSEGNodes()
		{
			return GetNodes(@"/Nodes/Node[@ElementName='mSEG']");
		}

        /// <summary>
        /// sets the value of the AssociatedParentVariable attribute of the node with the specified ID
        /// </summary>
        /// <param name="xNodeID"></param>
        /// <param name="xObjectData"></param>
        public void SetItemAssociatedParentVariable(string xTagID, string xParentVariableID)
        {
            Trace.WriteNameValuePairs("xTagID", xTagID, "xParentVariableID", xParentVariableID);

            //get nodes with specified tag id
            XmlNode oNode = m_oXML.SelectSingleNode(
                "/Nodes/Node[@TagID='" + xTagID + "']");

            if (oNode == null)
                //node doesn't exist - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_NodeDoesNotExist") +
                    xTagID);

            //set associated parent variable in XML
            oNode.Attributes["AssociatedParentVariable"].Value = xParentVariableID;

            //set associated parent variable in document
            fCOM.cWordDocument oDoc = new fCOM.cWordDocument();
            
            bool bGetNodesFromBookmarks = false;

            if (this.Type == LMP.Data.mpFileFormats.Binary)
            {
                //set associated parent variable of each Word tag with specified tag id
                Word.XMLNode[] oWordTags = this.GetWordTags(xTagID);

                if (oWordTags.Length > 0)
                {
                    foreach (Word.XMLNode oWordTag in oWordTags)
                    {
                        //set attribute in document
                        oDoc.SetAttributeValue(oWordTag, "AssociatedParentVariable", xParentVariableID,
                            LMP.Data.Application.EncryptionPassword);

                        //update corresponding cTag
                        m_oTags.Update(oWordTag, false, "", "");
                    }
                }
                else
                {
                    bGetNodesFromBookmarks = true;
                }
            }
            else
            {
                //set associated parent variable of each content control with specified tag id
                Word.ContentControl[] oCCs = this.GetContentControls(xTagID);

                if (oCCs.Length > 0)
                {
                    foreach (Word.ContentControl oCC in oCCs)
                    {
                        //set attribute in document
                        oDoc.SetAttributeValue_CC(oCC, "AssociatedParentVariable", xParentVariableID,
                            LMP.Data.Application.EncryptionPassword);

                        //update corresponding cTag
                        m_oTags.Update_CC(oCC, false, "", "");
                    }
                }
                else
                {
                    bGetNodesFromBookmarks = true;
                }
            }

            if (bGetNodesFromBookmarks)
            {
                Word.Bookmark[] oBmks = this.GetBookmarks(xTagID);

                foreach (Word.Bookmark oBmk in oBmks)
                {
                    oDoc.SetAttributeValue_Bookmark(oBmk, "AssociatedParentVariable", xParentVariableID,
                        LMP.Data.Application.EncryptionPassword);

                    //update corresponding cTag
                    m_oTags.Update_Bmk(oBmk, false, "", "");
                }
            }
        }
        /// <summary>
        /// sets the value of the ObjectData attribute of the node with the specified ID
        /// </summary>
        /// <param name="xNodeID"></param>
        /// <param name="xObjectData"></param>
        public void SetItemObjectData(string xTagID, string xObjectData)
        {
            Trace.WriteNameValuePairs("xTagID", xTagID, "xObjectData", xObjectData);

            //xObjectData = xObjectData.Replace("<", "¬");

            //replace reserved characters with escape sequences
            xObjectData = LMP.String.ReplaceXMLChars(xObjectData);

            //ensure that any prepended pipes are 
            //removed before writing back to doc
            if (xObjectData.StartsWith("|"))
                xObjectData = xObjectData.Substring(1);

            //get nodes with specified tag id
            XmlNode oNode = m_oXML.SelectSingleNode(
                "/Nodes/Node[@TagID='" + xTagID + "']");

            if (oNode == null)
                //node doesn't exist - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_NodeDoesNotExist") +
                    xTagID);

            //set object data in XML
            oNode.Attributes["ObjectData"].Value = xObjectData;

            fCOM.cWordDocument oDoc = new fCOM.cWordDocument();
            bool bIsFinished = true;

            //set object data in document
            if (this.Type == LMP.Data.mpFileFormats.Binary)
            {
                //set object data of each Word tag with specified tag id
                Word.XMLNode[] oWordTags = this.GetWordTags(xTagID);

                foreach (Word.XMLNode oWordTag in oWordTags)
                {
                    if (oWordTag == null)
                        continue;
                    else
                        bIsFinished = false;

                    //skip mDels - the object data of an mDel should not
                    //be set on the front-end
                    if (oWordTag.BaseName != "mDel")
                    {
                        //set attribute in document
                        oDoc.SetAttributeValue(oWordTag, "ObjectData", xObjectData,
                            LMP.Data.Application.EncryptionPassword);

                        //update corresponding cTag
                        try
                        {
                            m_oTags.Update(oWordTag, false, "", "");
                        }
                        catch (System.Exception oE)
                        {
                            int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                            if (iErr == -2146823760)
                            {
                                //item not in collection - this will occur when the parent tag id
                                //has been modified in the Word tag above thereby changing the
                                //full tag id and making it impossible to find the cTag in the
                                //collection - this time, provide the full tag id
                                m_oTags.Update(oWordTag, false, "", xTagID);
                            }
                            else
                            {
                                //rethrow
                                throw;
                            }
                        }
                    }
                }
            }
            else
            {
                //set object data of each content control with specified tag id
                Word.ContentControl[] oCCs = this.GetContentControls(xTagID);

                foreach (Word.ContentControl oCC in oCCs)
                {
                    if (oCC == null)
                        continue;
                    else
                        bIsFinished = false;

                    //skip mDels - the object data of an mDel should not
                    //be set on the front-end
                    if (oDoc.GetContentControlBaseName(oCC) != "mDel")
                    {
                        //set attribute in document
                        oDoc.SetAttributeValue_CC(oCC, "ObjectData", xObjectData,
                            LMP.Data.Application.EncryptionPassword);

                        //update corresponding cTag
                        try
                        {
                            m_oTags.Update_CC(oCC, false, "", "");
                        }
                        catch (System.Exception oE)
                        {
                            int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                            if (iErr == -2146823760)
                            {
                                //item not in collection - this will occur when the parent tag id
                                //has been modified in the Word tag above thereby changing the
                                //full tag id and making it impossible to find the cTag in the
                                //collection - this time, provide the full tag id
                                m_oTags.Update_CC(oCC, false, "", xTagID);
                            }
                            else
                            {
                                //rethrow
                                throw;
                            }
                        }
                    }
                }
            }

            if (bIsFinished)
            {
                //set object data of each content control with specified tag id
                Word.Bookmark[] oBookmarks = this.GetBookmarks(xTagID);

                if (oBookmarks.Length > 0 && oBookmarks[0] != null)
                {
                    foreach (Word.Bookmark oBmk in oBookmarks)
                    {
                        //set attribute in document
                        oDoc.SetAttributeValue_Bookmark(oBmk, "ObjectData", xObjectData,
                            LMP.Data.Application.EncryptionPassword);

                        //update corresponding cTag
                        try
                        {
                            m_oTags.Update_Bmk(oBmk, false, "", "");
                        }
                        catch (System.Exception oE)
                        {
                            int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                            if (iErr == -2146823760)
                            {
                                //item not in collection - this will occur when the parent tag id
                                //has been modified in the Word tag above thereby changing the
                                //full tag id and making it impossible to find the cTag in the
                                //collection - this time, provide the full tag id
                                m_oTags.Update_Bmk(oBmk, false, "", xTagID);
                            }
                            else
                            {
                                //rethrow
                                throw;
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// sets the value of the ObjectData attribute of the node with the specified ID
        /// </summary>
        /// <param name="xNodeID"></param>
        /// <param name="xAuthorData"></param>
        public void SetItemAuthorsData(string xTagID, string xAuthorData)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xTagID", xTagID, 
                "xAuthorData", xAuthorData);

            //xAuthorData = xAuthorData.Replace("<", "¬");

            //replace reserved characters with escape sequences
            //GLOG 5034: Authors.XML may contain replaced XML characters in the data -
            //Replace '&' with '&amp;' to ensure the tokens remain after ReplaceXMLChars
            xAuthorData = xAuthorData.Replace("&", "&amp;");
            xAuthorData = LMP.String.ReplaceXMLChars(xAuthorData);

            //get node store node with specified tag id
            XmlNode oNode = m_oXML.SelectSingleNode(
                "/Nodes/Node[@TagID='" + xTagID + "']");

            if (oNode == null)
                //node doesn't exist - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_NodeDoesNotExist") +
                    xTagID);

            //set author data in XML
            oNode.Attributes["Authors"].Value = xAuthorData;

            fCOM.cWordDocument oDoc = new fCOM.cWordDocument();
            bool bIsFinished = true;

            //set author data in document
            if (this.Type == LMP.Data.mpFileFormats.Binary)
            {
                //set author data of each Word tag with specified tag id
                Word.XMLNode[] oWordTags = this.GetWordTags(xTagID);

                foreach (Word.XMLNode oWordTag in oWordTags)
                {
                    if (oWordTag == null)
                        continue;
                    else
                        bIsFinished = false;

                    //set attribute in document
                    oDoc.SetAttributeValue(oWordTag, "Authors", xAuthorData,
                        LMP.Data.Application.EncryptionPassword);

                    //update corresponding cTag
                    //8-3-11 (dm) - see note in content control branch below
                    try
                    {
                        m_oTags.Update(oWordTag, false, "", "");
                    }
                    catch (System.Exception oE)
                    {
                        int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                        if (iErr == -2146823760)
                        {
                            //item not in collection - this will occur when the parent tag id
                            //has been modified in the Word tag above thereby changing the
                            //full tag id and making it impossible to find the cTag in the
                            //collection - this time, provide the full tag id
                            m_oTags.Update(oWordTag, false, "", xTagID);
                        }
                        else
                        {
                            //rethrow
                            throw;
                        }
                    }
                }
            }
            else
            {
                //set author data of each content control width specified tag id
                Word.ContentControl[] oCCs = this.GetContentControls(xTagID);

                foreach (Word.ContentControl oCC in oCCs)
                {
                    if (oCC == null)
                        continue;
                    else
                        bIsFinished = false;

                    //set attribute in document
                    oDoc.SetAttributeValue_CC(oCC, "Authors", xAuthorData,
                        LMP.Data.Application.EncryptionPassword);

                    //update corresponding cTag
                    //8-3-11 (dm) - added same try/catch block that's in SetItemObjectData() -
                    //this was to address errors in Morgan Lewis pleadings with TOCs in
                    //which users manually added section breaks - Update_CC() couldn't
                    //determine that the pleading paper belonged to the pleading
                    try
                    {
                        m_oTags.Update_CC(oCC, false, "", "");
                    }
                    catch (System.Exception oE)
                    {
                        int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                        if (iErr == -2146823760)
                        {
                            //item not in collection - this will occur when the parent tag id
                            //has been modified in the Word tag above thereby changing the
                            //full tag id and making it impossible to find the cTag in the
                            //collection - this time, provide the full tag id
                            m_oTags.Update_CC(oCC, false, "", xTagID);
                        }
                        else
                        {
                            //rethrow
                            throw;
                        }
                    }
                }
            }

            if (bIsFinished)
            {
                //set object data of each content control with specified tag id
                Word.Bookmark[] oBookmarks = this.GetBookmarks(xTagID);

                if (oBookmarks.Length > 0 && oBookmarks[0] != null)
                {
                    foreach (Word.Bookmark oBmk in oBookmarks)
                    {
                        //set attribute in document
                        oDoc.SetAttributeValue_Bookmark(oBmk, "Authors", xAuthorData,
                            LMP.Data.Application.EncryptionPassword);

                        //update corresponding cTag
                        try
                        {
                            m_oTags.Update_Bmk(oBmk, false, "", "");
                        }
                        catch (System.Exception oE)
                        {
                            int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                            if (iErr == -2146823760)
                            {
                                //item not in collection - this will occur when the parent tag id
                                //has been modified in the Word tag above thereby changing the
                                //full tag id and making it impossible to find the cTag in the
                                //collection - this time, provide the full tag id
                                m_oTags.Update_Bmk(oBmk, false, "", xTagID);
                            }
                            else
                            {
                                //rethrow
                                throw;
                            }
                        }
                    }
                }
            }

            LMP.Benchmarks.Print(t0);
        }
        #region SetItemObjectDataValueOLD
        ///// <summary>
        ///// sets the value of an item in the 
        ///// matching node's object data string
        ///// to the specified value
        ///// </summary>
        ///// <param name="xTagID"></param>
        ///// <param name="xName"></param>
        ///// <param name="xValue"></param>
        //public void SetItemObjectDataValueOLD(string xTagID, string xName, string xValue)
        //{
        //    Trace.WriteNameValuePairs("xTagID", xTagID, "xName", xName, "xValue", xValue);

        //    //get ObjectData attribute for specified node
        //    string xObjectData = this.GetItemObjectData(xTagID);

        //    //find location of specified key
        //    string xObjectDataStartString = "|" + xName + "=";
        //    int iValueStartPos = xObjectData.IndexOf(xObjectDataStartString);

        //    if (iValueStartPos > -1)
        //    {
        //        iValueStartPos += xObjectDataStartString.Length;

        //        //key was found, get end of value
        //        int iValueEndPos = xObjectData.IndexOf("|", iValueStartPos + 1);

        //        //replace old value with new value in object data string
        //        xObjectData = xObjectData.Substring(0, iValueStartPos) + xValue +
        //            xObjectData.Substring(iValueEndPos);

        //        //save object data string
        //        SetItemObjectData(xTagID, xObjectData);
        //    }
        //    else
        //        throw new LMP.Exceptions.DataException(
        //            LMP.Resources.GetLangString("Error_InvalidObjectDataItemName") + xName);
        //}
        #endregion
        /// <summary>
        /// sets the value of an item in the 
        /// matching node's object data string
        /// to the specified value
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        public void SetItemObjectDataValue(string xTagID, string xName, string xValue)
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteNameValuePairs("xTagID", xTagID, "xName", xName, "xValue", xValue);

            //replace xml start tags
            //xValue = xValue.Replace("<", "¬");

            //replace reserved xml characters with escape sequences
            xValue = LMP.String.ReplaceXMLChars(xValue);

            //get ObjectData attribute for specified node
            string xObjectData = this.GetItemObjectData(xTagID);

            //find location of specified key
            string xObjectDataStartString = "|" + xName + "=";
            int iValueStartPos = xObjectData.IndexOf(xObjectDataStartString);

            if (iValueStartPos > -1)
            {
                //key was found, get end of value
                iValueStartPos += xObjectDataStartString.Length;
                int iValueEndPos = xObjectData.IndexOf("|", iValueStartPos);

                //replace old value with new value in object data string
                xObjectData = xObjectData.Substring(0, iValueStartPos) + xValue +
                    xObjectData.Substring(iValueEndPos);
            }
            else
            {
                //key was not found - add to end of object data string
                xObjectData = xObjectData.TrimEnd('|');
                xObjectData += xObjectDataStartString + xValue + "|";
            }

            //save object data string
            SetItemObjectData(xTagID, xObjectData);

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// removes the definition of oVar from the DeletedScopes attributes
        /// of the NodeStore and the Word document
        /// </summary>
        /// <param name="oVar"></param>
        public void RemoveVariableDefFromDeletedScopes(XmlVariable oVar)
        {
            string xTagID = oVar.Segment.FullTagID;

            Trace.WriteNameValuePairs("oVar.TagID", xTagID);

            //get nodes with specified tag id
            XmlNode oNode = m_oXML.SelectSingleNode(
                "/Nodes/Node[@TagID='" + xTagID + "']");

            if (oNode == null)
                //node doesn't exist - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_NodeDoesNotExist") + xTagID);

            //modify attribute in xml - the value being modified here includes the
            //deleted scopes for all parts of the segments
            string xDeletedScopes = oNode.Attributes["DeletedScopes"].Value;
            int iPos1 = xDeletedScopes.IndexOf("DeletedTag=" + oVar.Name + "¦");
            if (iPos1 != -1)
            {
                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>|", iPos1 + 1) + 10;
                xDeletedScopes = xDeletedScopes.Substring(0, iPos1) +
                   xDeletedScopes.Substring(iPos2);
                oNode.Attributes["DeletedScopes"].Value = xDeletedScopes;
            }

            fCOM.cWordDocument oDoc = new fCOM.cWordDocument();

            //modify attribute in document - the values being modified here include only
            //the deleted scopes applicable to each individual part
            string[] aParts = oVar.TagParentPartNumbers.Split(';');
            for (int i = 0; i < aParts.Length; i++)
            {
                if (this.Type == LMP.Data.mpFileFormats.Binary)
                {
                    //word xml node
                    Word.XMLNode oWordTag = null;

                    try
                    {
                        oWordTag = GetWordTag(xTagID, int.Parse(aParts[i]));
                    }
                    catch { }

                    if (oWordTag != null)
                    {
                        Word.XMLNode oDeletedScopes = oWordTag.SelectSingleNode("@DeletedScopes", "", false);
                        if (oDeletedScopes != null)
                        {
                            //initialize password to default - existing tags might already use a different password
                            string xPassword = LMP.Data.Application.EncryptionPassword;
                            xDeletedScopes = LMP.String.Decrypt(oDeletedScopes.NodeValue, ref xPassword);
                            iPos1 = xDeletedScopes.IndexOf("DeletedTag=" + oVar.Name + "¦");
                            if (iPos1 != -1)
                            {
                                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>|", iPos1 + 1) + 10;
                                xDeletedScopes = xDeletedScopes.Substring(0, iPos1) +
                                   xDeletedScopes.Substring(iPos2);
                                oDeletedScopes = oWordTag.SelectSingleNode("@DeletedScopes", "", false);
                                oDeletedScopes.NodeValue = LMP.String.Encrypt(xDeletedScopes, xPassword);

                                //update corresponding cTag
                                m_oTags.Update(oWordTag, false, "", "");
                            }
                        }
                    }
                    else
                    {
                        //parent must be bookmark bound
                        Word.Bookmark oBmk = GetBookmark(xTagID, int.Parse(aParts[i]));

                        xDeletedScopes = oDoc.GetAttributeValue_Bmk(oBmk, "DeletedScopes");
                        if (!string.IsNullOrEmpty(xDeletedScopes))
                        {
                            iPos1 = xDeletedScopes.IndexOf("DeletedTag=" + oVar.Name + "¦");
                            if (iPos1 != -1)
                            {
                                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>|", iPos1 + 1) + 10;
                                xDeletedScopes = xDeletedScopes.Substring(0, iPos1) +
                                   xDeletedScopes.Substring(iPos2);
                                oDoc.SetAttributeValue_Bookmark(oBmk, "DeletedScopes", xDeletedScopes, "");

                                //update corresponding cTag
                                m_oTags.Update_Bmk(oBmk, false, "", "");
                            }
                        }
                    }

                }
                else
                {
                    //content control
                    Word.ContentControl oCC = null;

                    try
                    {
                        oCC = GetContentControl(xTagID, int.Parse(aParts[i]));
                    }
                    catch { }

                    if (oCC != null)
                    {
                        xDeletedScopes = oDoc.GetAttributeValue_CC(oCC, "DeletedScopes");
                        if (xDeletedScopes != "")
                        {
                            iPos1 = xDeletedScopes.IndexOf("DeletedTag=" + oVar.Name + "¦");
                            if (iPos1 != -1)
                            {
                                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>|", iPos1 + 1) + 10;
                                xDeletedScopes = xDeletedScopes.Substring(0, iPos1) +
                                   xDeletedScopes.Substring(iPos2);
                                oDoc.SetAttributeValue_CC(oCC, "DeletedScopes", xDeletedScopes, "");

                                //update corresponding cTag
                                m_oTags.Update_CC(oCC, false, "", "");
                            }
                        }
                    }
                    else
                    {
                        //parent must be bookmark bound
                        Word.Bookmark oBmk = GetBookmark(xTagID, int.Parse(aParts[i]));

                        xDeletedScopes = oDoc.GetAttributeValue_Bmk(oBmk, "DeletedScopes");
                        if (!string.IsNullOrEmpty(xDeletedScopes))
                        {
                            iPos1 = xDeletedScopes.IndexOf("DeletedTag=" + oVar.Name + "¦");
                            if (iPos1 != -1)
                            {
                                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>|", iPos1 + 1) + 10;
                                xDeletedScopes = xDeletedScopes.Substring(0, iPos1) +
                                   xDeletedScopes.Substring(iPos2);
                                oDoc.SetAttributeValue_Bookmark(oBmk, "DeletedScopes", xDeletedScopes, "");

                                //update corresponding cTag
                                m_oTags.Update_Bmk(oBmk, false, "", "");
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// updates the definition of oVar in the DeletedScopes attributes
        /// of the NodeStore and the Word document
        /// </summary>
        /// <param name="oVar"></param>
        public void UpdateVariableDefInDeletedScopes(XmlVariable oVar)
        {
            string xVarForTag = oVar.ToString(true);
            string xTagID = oVar.Segment.FullTagID;

            Trace.WriteNameValuePairs("oVar", xVarForTag);

            //get nodes with specified tag id
            XmlNode oNode = m_oXML.SelectSingleNode(
                "/Nodes/Node[@TagID='" + xTagID + "']");

            if (oNode == null)
                //node doesn't exist - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_NodeDoesNotExist") + xTagID);

            fCOM.cWordDocument oDoc = new fCOM.cWordDocument(); //GLOG 7032 (dm)

            //modify attribute in xml - the value being modified here includes the
            //deleted scopes for all parts of the segments
            string xDeletedScopes = oNode.Attributes["DeletedScopes"].Value;
            int iPos1 = xDeletedScopes.IndexOf("VariableDefinition=" + oVar.ID);
            if (iPos1 != -1)
            {
                int iPos2 = xDeletedScopes.IndexOf("|", iPos1 + 1);
                xDeletedScopes = xDeletedScopes.Substring(0, iPos1 + 19) +
                    xVarForTag + xDeletedScopes.Substring(iPos2);
                oNode.Attributes["DeletedScopes"].Value = xDeletedScopes;
            }

            //modify attribute in document - the values being modified here include only
            //the deleted scopes applicable to each individual part
            string[] aParts = oVar.TagParentPartNumbers.Split(';');
            for (int i = 0; i < aParts.Length; i++)
            {
                //GLOG 7032 (dm) - added support for bookmark mSEGs
                if (this.Type == LMP.Data.mpFileFormats.Binary)
                {
                    Word.XMLNode oWordTag = null;
                    try
                    {
                        oWordTag = GetWordTag(xTagID, int.Parse(aParts[i]));
                    }
                    catch { }

                    if (oWordTag != null)
                    {
                        Word.XMLNode oDeletedScopes = oWordTag.SelectSingleNode("@DeletedScopes", "", false);
                        if (oDeletedScopes != null)
                        {
                            //initialize password to default - existing tags might already use a different password
                            string xPassword = LMP.Data.Application.EncryptionPassword;
                            xDeletedScopes = LMP.String.Decrypt(oDeletedScopes.NodeValue, ref xPassword);
                            iPos1 = xDeletedScopes.IndexOf("VariableDefinition=" + oVar.ID);
                            if (iPos1 != -1)
                            {
                                int iPos2 = xDeletedScopes.IndexOf("|", iPos1 + 1);
                                xDeletedScopes = xDeletedScopes.Substring(0, iPos1 + 19) +
                                    xVarForTag + xDeletedScopes.Substring(iPos2);
                                oDeletedScopes = oWordTag.SelectSingleNode("@DeletedScopes", "", false);
                                oDeletedScopes.NodeValue = LMP.String.Encrypt(xDeletedScopes, xPassword);

                                //update corresponding cTag
                                m_oTags.Update(oWordTag, false, "", "");
                            }
                        }
                    }
                    else
                    {
                        //parent must be bookmark bound
                        Word.Bookmark oBmk = GetBookmark(xTagID, int.Parse(aParts[i]));

                        xDeletedScopes = oDoc.GetAttributeValue_Bmk(oBmk, "DeletedScopes");
                        if (!string.IsNullOrEmpty(xDeletedScopes))
                        {
                            iPos1 = xDeletedScopes.IndexOf("VariableDefinition=" + oVar.ID);
                            if (iPos1 != -1)
                            {
                                int iPos2 = xDeletedScopes.IndexOf("|", iPos1 + 1);
                                xDeletedScopes = xDeletedScopes.Substring(0, iPos1 + 19) +
                                    xVarForTag + xDeletedScopes.Substring(iPos2);
                                oDoc.SetAttributeValue_Bookmark(oBmk, "DeletedScopes", xDeletedScopes, "");

                                //update corresponding cTag
                                m_oTags.Update_Bmk(oBmk, false, "", "");
                            }
                        }
                    }
                }
                else
                {
                    //content control
                    Trace.WriteInfo("1D");
                    Word.ContentControl oCC = null;

                    try
                    {
                        oCC = GetContentControl(xTagID, int.Parse(aParts[i]));
                    }
                    catch { }

                    if (oCC != null)
                    {
                        xDeletedScopes = oDoc.GetAttributeValue_CC(oCC, "DeletedScopes");
                        if (xDeletedScopes != "")
                        {
                            iPos1 = xDeletedScopes.IndexOf("VariableDefinition=" + oVar.ID);
                            if (iPos1 != -1)
                            {
                                int iPos2 = xDeletedScopes.IndexOf("|", iPos1 + 1);
                                xDeletedScopes = xDeletedScopes.Substring(0, iPos1 + 19) +
                                    xVarForTag + xDeletedScopes.Substring(iPos2);
                                oDoc.SetAttributeValue_CC(oCC, "DeletedScopes", xDeletedScopes, "");

                                //update corresponding cTag
                                m_oTags.Update_CC(oCC, false, "", "");
                            }
                        }
                    }
                    else
                    {
                        //parent must be bookmark bound
                        Word.Bookmark oBmk = GetBookmark(xTagID, int.Parse(aParts[i]));

                        xDeletedScopes = oDoc.GetAttributeValue_Bmk(oBmk, "DeletedScopes");
                        if (!string.IsNullOrEmpty(xDeletedScopes))
                        {
                            iPos1 = xDeletedScopes.IndexOf("VariableDefinition=" + oVar.ID);
                            if (iPos1 != -1)
                            {
                                int iPos2 = xDeletedScopes.IndexOf("|", iPos1 + 1);
                                xDeletedScopes = xDeletedScopes.Substring(0, iPos1 + 19) +
                                    xVarForTag + xDeletedScopes.Substring(iPos2);
                                oDoc.SetAttributeValue_Bookmark(oBmk, "DeletedScopes", xDeletedScopes, "");

                                //update corresponding cTag
                                m_oTags.Update_Bmk(oBmk, false, "", "");
                            }
                        }
                    }
               }
            }
        }

        /// <summary>
        /// gets the definition of the specified variable from the
        /// DeletedScopes attributes of the NodeStore
        /// </summary>
        /// <param name="oVar"></param>
        public string GetVariableDefFromDeletedScopes(XmlSegment oSeg, string xVarName)
        {
            string xTagID = oSeg.FullTagID;

            Trace.WriteNameValuePairs("xTagID", xTagID, "xVarName", xVarName);

            XmlNode oNode = m_oXML.SelectSingleNode(
                "/Nodes/Node[@TagID='" + xTagID + "']");
            if (oNode == null)
            {
                //node doesn't exist - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_NodeDoesNotExist") + xTagID);
            }

            string xDeletedScopes = oNode.Attributes["DeletedScopes"].Value;
            int iPos1 = xDeletedScopes.IndexOf("DeletedTag=" + xVarName + "¦");
            if (iPos1 != -1)
            {
                int iPos2 = xDeletedScopes.IndexOf("¬/w:body>|", iPos1 + 1) + 10;
                string xScope = xDeletedScopes.Substring(iPos1, iPos2 - iPos1);
                if (this.Type == LMP.Data.mpFileFormats.Binary)
                {
                    //xml tags - get def from xml
                    iPos1 = xScope.IndexOf("VariableDefinition=");
                    while (iPos1 != -1)
                    {
                        iPos2 = xScope.IndexOf("|", iPos1 + 1);
                        string xDef = xScope.Substring(iPos1, iPos2 - iPos1);
                        string[] aDef = xDef.Split('¦');
                        if (aDef[1] == xVarName)
                            return xDef;
                        else
                            iPos1 = xScope.IndexOf("VariableDefinition=", iPos2);
                    }
                }
                else
                {
                    //content controls - get def from associated doc var
                    //9-2-11 (dm) - this was erring for nested delete scopes
                    //because we were previously searching xDeletedScopes,
                    //rather than xScope, so that the first tag was always returned
                    iPos1 = xScope.IndexOf("¬w:tag w:val=");
                    while (iPos1 != -1)
                    {
                        iPos1 += 14;
                        iPos2 = xScope.IndexOf('\"', iPos1);
                        string xTag = xScope.Substring(iPos1, iPos2 - iPos1);
                        string xDef = LMP.fCOMObjects.cWordDoc.GetAttributeValueFromTag(
                            xTag, "ObjectData");
                        string[] aDef = xDef.Split('¦');
                        if ((aDef.Length > 1) && (aDef[1] == xVarName))
                            return xDef;
                        else
                            iPos1 = xScope.IndexOf("¬w:tag w:val=", iPos2);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// clears the DeletedScopes attribute of the node with the specified ID
        /// </summary>
        /// <param name="xNodeID"></param>
        /// <param name="xObjectData"></param>
        public void ClearItemDeletedScopes(string xTagID)
        {
            Trace.WriteNameValuePairs("xTagID", xTagID);

            //get node with specified tag id
            XmlNode oNode = m_oXML.SelectSingleNode(
                "/Nodes/Node[@TagID='" + xTagID + "']");

            if (oNode == null)
                //node doesn't exist - alert
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_NodeDoesNotExist") +
                    xTagID);

            //set object data in XML
            oNode.Attributes["DeletedScopes"].Value = "";

            fCOM.cWordDocument oDoc = new fCOM.cWordDocument();

            //set object data in document
            if (this.Type == LMP.Data.mpFileFormats.Binary)
            {
                //set object data of each Word tag with specified tag id
                Word.XMLNode[] oWordTags = this.GetWordTags(xTagID);

                foreach (Word.XMLNode oWordTag in oWordTags)
                {
                    //set attribute in document
                    oDoc.SetAttributeValue(oWordTag, "DeletedScopes", "",
                        LMP.Data.Application.EncryptionPassword);

                    //update corresponding cTag
                    try
                    {
                        m_oTags.Update(oWordTag, false, "", "");
                    }
                    catch (System.Exception oE)
                    {
                        int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                        if (iErr == -2146823760)
                        {
                            //item not in collection - this will occur when the parent tag id
                            //has been modified in the Word tag above thereby changing the
                            //full tag id and making it impossible to find the cTag in the
                            //collection - this time, provide the full tag id
                            m_oTags.Update(oWordTag, false, "", xTagID);
                        }
                        else
                        {
                            //rethrow
                            throw;
                        }
                    }
                }
            }
            else
            {
                //set object data of each content control with specified tag id
                Word.ContentControl[] oCCs = this.GetContentControls(xTagID);

                foreach (Word.ContentControl oCC in oCCs)
                {
                    //set attribute in document
                    oDoc.SetAttributeValue_CC(oCC, "DeletedScopes", "",
                        LMP.Data.Application.EncryptionPassword);

                    //update corresponding cTag
                    try
                    {
                        m_oTags.Update_CC(oCC, false, "", "");
                    }
                    catch (System.Exception oE)
                    {
                        int iErr = System.Runtime.InteropServices.Marshal.GetHRForException(oE);
                        if (iErr == -2146823760)
                        {
                            //item not in collection - this will occur when the parent tag id
                            //has been modified in the Word tag above thereby changing the
                            //full tag id and making it impossible to find the cTag in the
                            //collection - this time, provide the full tag id
                            m_oTags.Update_CC(oCC, false, "", xTagID);
                        }
                        else
                        {
                            //rethrow
                            throw;
                        }
                    }
                }
            }
        }

        /// <summary>
		/// returns the object data for all collection nodes as an array
		/// </summary>
		/// <returns></returns>
		public string[] GetObjectDataArray()
		{
			//get nodes in collection
			XmlNodeList oNodes = m_oXML.DocumentElement.ChildNodes;

			//create storage space
			string[] aObjectData = new string[oNodes.Count];

			//cycle through collection, adding ObjectData string to array
			for(int i = 0; i < oNodes.Count; i++)
			{
                string xValue;

				try
				{
                    xValue = oNodes[i].Attributes["ObjectData"].Value;
                    xValue = LMP.String.RestoreXMLChars(xValue);
				}
				catch
				{
					//insert empty ObjectData string
                    xValue = "";
				}

                aObjectData[i] = xValue;
			}

			return aObjectData;
		}

		/// <summary>
		/// returns the object data of the node as an array
		/// </summary>
		/// <param name="xTagID"></param>
		/// <returns></returns>
		public string[] GetItemObjectDataArray(string xTagID)
		{
			Trace.WriteNameValuePairs("xTagID", xTagID);

			//get ObjectData attribute for specified node
			string xObjectData = this.GetItemObjectData(xTagID);

			if(xObjectData.StartsWith("|"))
				//trim preceding pipe
				xObjectData = xObjectData.Substring(1);

			if(xObjectData.EndsWith("|"))
				//trim trailing pipe
				xObjectData = xObjectData.Substring(0, xObjectData.Length - 1);

            //trim tag prefix id
            int iPos = xObjectData.IndexOf(LMP.String.mpTagPrefixIDSeparator);
            if (iPos != -1)
            {
                xObjectData = xObjectData.Substring(iPos +
                    LMP.String.mpTagPrefixIDSeparator.Length);
            }

			string[] aObjectData = xObjectData.Split('|');

			return aObjectData;
		}

		/// <summary>
		/// returns the object data of the node with 
		/// the specified tag id as a list dictionary
		/// </summary>
		/// <param name="xTagID"></param>
		/// <returns></returns>
		public ListDictionary GetItemObjectDataListDictionary(string xTagID)
		{
			//create new list dictionary
			ListDictionary oLD = new ListDictionary();

			//get array of object data values
			string[] aValues = this.GetItemObjectDataArray(xTagID);

			//cycle through array elements, adding 
			//each name/value pair to the list dictionary
			for(int i=0; i<aValues.Length; i++)
			{
				//parse name and value from array element
				int iPos = aValues[i].IndexOf("=");

                if (iPos == -1)
                    continue;

				string xName = aValues[i].Substring(0, iPos);
				string xValue = aValues[i].Substring(iPos + 1);

                bool bAdded = false;

                //cycle until added - we may need
                //several iterations of the loop, as 
                //the name may have been added previously -
                //this occurs particularly with VariableDefinition
                int j = 1;
                while (!bAdded)
                {
                    try
                    {
                        //add pair to list dictionary
                        oLD.Add(xName, xValue);
                        bAdded = true;
                    }
                    catch(System.Exception oE)
                    {
                        if (j < 100)
                        {
                            //item was not added - try changing name
                            j++;
                            xName += j.ToString();
                        }
                        else
                        {
                            //something's wrong - we tried 100 times
                            //to rename and add
                            throw new LMP.Exceptions.DataException(
                                LMP.Resources.GetLangString(
                                    "Error_CouldNotAddObjectDataItemToListDictionary") + xName, oE);
                        }
                    }
                }
			}

			return oLD;
		}

		/// <summary>
		/// returns the value of the specified Name/Value 
		/// object data pair for the specified node
		/// </summary>
		/// <param name="xTagID"></param>
		/// <param name="xName"></param>
		/// <returns></returns>
		public string GetItemObjectDataValue(string xTagID, string xName)
		{
			Trace.WriteNameValuePairs("xTagID", xTagID, "xName", xName);

			string xValue = "";

			//get ObjectData attribute for specified node
			string xObjectData = this.GetItemObjectData(xTagID);

			//find location of specified key
			int iPos1 = xObjectData.IndexOf("|" + xName + "=");

			if(iPos1 > -1)
			{
				//key was found, get value
				int iPos2 = xObjectData.IndexOf("|", iPos1 + 1);
				int iValueStartPos = iPos1 + xName.Length + 2;

				xValue = xObjectData.Substring(
					iValueStartPos, iPos2 - iValueStartPos);
			}

			return xValue;
		}

		/// <summary>
		/// returns true if specified node exists
		/// </summary>
		/// <param name="xTagID"></param>
		/// <returns></returns>
		public bool NodeExists(string xTagID)
		{
			XmlNode oNode = m_oXML.SelectSingleNode(@"//Node[@TagID='" + xTagID + "']");
			return !(oNode == null);
		}

        /// <summary>
        /// returns true if specified node exists
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public bool NodeExists(string xTagID, string xElement)
        {
            XmlNodeList oNodes = m_oXML.SelectNodes(@"//Node[@TagID='" + xTagID + "']");
            foreach (XmlNode oNode in oNodes)
            {
                if (GetItemElementName(xTagID) == xElement)
                    return true;
            }
            return false;
        }

        public override string ToString()
        {
            return m_oXML.OuterXml;
        }

        public string GetItemTagPrefixID(string xTagID)
        {
            string xObjectData = this.GetItemObjectData(xTagID);
            if (xObjectData.StartsWith("|"))
                xObjectData = xObjectData.Substring(1);
            return LMP.String.GetTagPrefixID(xObjectData);
        }

        /// <summary>
        /// returns the content controls with the specified ID
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public Word.ContentControl[] GetContentControls(string xTagID)
        {
            try
            {
                DateTime t0 = DateTime.Now;
                Word.ContentControl[] aTags = null;

                Trace.WriteNameValuePairs("xTagID", xTagID);

                //get xml node with specified tag id
                XmlNode oNode = m_oXML.SelectSingleNode(
                    "/Nodes/Node[@TagID='" + xTagID + "']");

                //get the hash IDs of selected node
                string xIDs = oNode.Attributes["HashID"].Value;

                //parse hash id string to retrieve hash id of specified part
                string[] aHashIDs = xIDs.Split(';');

                //create Word tag array
                Word.ContentControl[] aTagsTmp = new Word.ContentControl[aHashIDs.Length];

                //cycle through hash IDs adding the corresponding
                //Word tag to the array
                int iCountNonNull = 0;
                bool bNullsExist = false;

                for (int i = 0; i < aHashIDs.Length; i++)
                {
                    string xHashID = aHashIDs[i];

                    Word.ContentControl oCC = this.BaseGet(xHashID) as Word.ContentControl;
                    if (oCC != null)
                    {
                        //get the Word tag with the specified hash ID
                        aTagsTmp[i] = (Word.ContentControl)this.BaseGet(xHashID);

                        iCountNonNull++;
                    }
                    else
                    {
                        bNullsExist = true;
                    }
                }

                if (bNullsExist)
                {
                    aTags = new Word.ContentControl[iCountNonNull];
                    int j = 0;
                    foreach (Word.ContentControl oCC in aTagsTmp)
                    {
                        if (oCC != null)
                        {
                            aTags[j] = oCC;
                            j++;
                        }
                    }
                }
                else
                {
                    aTags = aTagsTmp;
                }

                LMP.Benchmarks.Print(t0, xTagID);
                return aTags;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_ContentControlDoesNotExist") + xTagID, oE);
            }
        }

        /// <summary>
        /// returns the content controls with the specified ID
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public Word.Bookmark[] GetBookmarks(string xTagID)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                Trace.WriteNameValuePairs("xTagID", xTagID);

                //get xml node with specified tag id
                XmlNode oNode = m_oXML.SelectSingleNode(
                    "/Nodes/Node[@TagID='" + xTagID + "']");

                //get the hash IDs of selected node
                string xIDs = oNode.Attributes["HashID"].Value;

                //parse hash id string to retrieve hash id of specified part
                string[] aHashIDs = xIDs.Split(';');

                //create Word tag array
                Word.Bookmark[] aBookmarks = new Word.Bookmark[aHashIDs.Length];

                //cycle through hash IDs adding the corresponding
                //Word tag to the array
                for (int i = 0; i < aHashIDs.Length; i++)
                {
                    string xHashID = aHashIDs[i];

                    //get the Word tag with the specified hash ID
                    aBookmarks[i] = (Word.Bookmark)this.BaseGet(xHashID);
                }

                LMP.Benchmarks.Print(t0, xTagID);
                return aBookmarks;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_ContentControlDoesNotExist") + xTagID, oE);
            }
        }

        /// <summary>
        /// returns the content control with the specified ID and part
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="xPart"></param>
        /// <returns></returns>
        public Word.ContentControl GetContentControl(string xTagID, int iPart)
        {
            try
            {
                Trace.WriteNameValuePairs("xTagID", xTagID, "iPart", iPart);

                //get xml node with specified tag id
                XmlNode oNode = m_oXML.SelectSingleNode(
                    "/Nodes/Node[@TagID='" + xTagID + "']");

                //get the associated parts
                string xParts = oNode.Attributes["Parts"].Value;
                string[] aParts = xParts.Split(';');

                //get the index of the specified part
                int iIndex = 0;
                for (int i = 0; i < aParts.Length; i++)
                {
                    if (aParts[i] == iPart.ToString())
                    {
                        iIndex = i;
                        break;
                    }
                }

                //get the hash ID of selected node
                string xIDs = oNode.Attributes["HashID"].Value;

                //parse hash id string to retrieve hash id of specified part
                string[] aHashIDs = xIDs.Split(';');
                string xID = aHashIDs[iIndex];

                //get the Word cc with the specified hash ID
                Word.ContentControl oControl = null;

                try
                {
                    //ignore type mismatch error, as the hashtable value
                    //may be of type Word.Bookmark
                    oControl = (Word.ContentControl)this.BaseGet(xID);
                }
                catch { }

                return oControl;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_ContentControlDoesNotExist") + xTagID, oE);
            }
        }

        /// <summary>
        /// returns the Bookmark with the specified ID and part
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="xPart"></param>
        /// <returns></returns>
        public Word.Bookmark GetBookmark(string xTagID, int iPart)
        {
            try
            {
                Trace.WriteNameValuePairs("xTagID", xTagID, "iPart", iPart);

                //get xml node with specified tag id
                XmlNode oNode = m_oXML.SelectSingleNode(
                    "/Nodes/Node[@TagID='" + xTagID + "']");

                //get the associated parts
                string xParts = oNode.Attributes["Parts"].Value;
                string[] aParts = xParts.Split(';');

                //get the index of the specified part
                int iIndex = 0;
                for (int i = 0; i < aParts.Length; i++)
                {
                    if (aParts[i] == iPart.ToString())
                    {
                        iIndex = i;
                        break;
                    }
                }

                //get the hash ID of selected node
                string xIDs = oNode.Attributes["HashID"].Value;

                //parse hash id string to retrieve hash id of specified part
                string[] aHashIDs = xIDs.Split(';');
                string xID = aHashIDs[iIndex];

                //get the Word tag with the specified hash ID
                Word.Bookmark oBmk = (Word.Bookmark)this.BaseGet(xID);

                return oBmk;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordTagException(
                    LMP.Resources.GetLangString(
                    "Error_BookmarkDoesNotExist") + xTagID, oE);
            }
        }

        /// <summary>
        /// returns the content control at the specified index
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public Word.ContentControl GetContentControl(int iIndex)
        {
            return (Word.ContentControl)this.BaseGet(iIndex);
        }

        /// <summary>
        /// returns the tags of the item at the specified index
        /// </summary>
        /// <param name="iIndex"></param>
        /// <returns></returns>
        public string GetItemTags(int iIndex)
        {
            if (iIndex < 0 || iIndex > this.Count)
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_ItemAtIndexNotInCollection") + iIndex);

            return this[iIndex].Attributes["Tags"].Value;
        }

        /// <summary>
        /// returns the tags of the item at with the specified tag id
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        public string GetItemTags(string xTagID)
        {
            return this.GetNodeAttributeValue(xTagID, "Tags");
        }
            
        /// <summary>
        /// cycles through all mSeg nodes, refreshing
        /// the associated bounding object - particularly
        /// useful on Finish, when bounding objects need
        /// to be switched to bookmarks
        /// </summary>
        public void RefreshMSegBoundingObjects()
        {
            for(int i=0; i < this.BaseGetAllKeys().Length; i++)
            {
                object oBoundingObject = this.BaseGet(i);
                
                if(oBoundingObject is Word.ContentControl)
                {
                    Word.ContentControl oCC = (Word.ContentControl)oBoundingObject;
                    if (oCC.Tag.StartsWith("mps"))
                    {
                        object oBmkName = "_" + oCC.Tag;
                        Word.Bookmark oBmk = oCC.Range.Document.Bookmarks.get_Item(ref oBmkName);
                        this.BaseSet(i, oBmk);
                    }
                }
            }
        }
        #endregion
		#region *********************private members*********************
		/// <summary>
		/// returns the value of the specified attribute 
		/// belonging to the node with the specified ID
		/// </summary>
		/// <param name="xTagID"></param>
		/// <param name="xAttributeName"></param>
		/// <returns></returns>
		private string GetNodeAttributeValue(string xTagID, string xAttributeName)
		{
			Trace.WriteNameValuePairs("xTagID", xTagID, 
				"xAttributeName", xAttributeName);

			//get specified node
			XmlNode oNode = m_oXML.SelectSingleNode("Nodes/Node[@TagID='" + xTagID + "']");

			if(oNode == null)
				//node doesn't exist - alert
				throw new LMP.Exceptions.NotInCollectionException(
					LMP.Resources.GetLangString("Error_NodeDoesNotExist") + 
					xTagID);

            XmlAttribute oAttr = oNode.Attributes[xAttributeName];

            return oAttr != null ? oAttr.Value : "";
		}

        /// <summary>
        /// returns the part numbers for the specified node
        /// of the item with the specified tag ID
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private string GetItemPartNumbers(XmlNode oNode)
        {
            string xUniqueParts = "";
            string xParts = oNode.Attributes["Parts"].Value;
            if (xParts != "")
            {
                string[] aParts = xParts.Split(';');
                for (int i = 0; i < aParts.Length; i++)
                {
                    //only add if not already on the list
                    string xPart = aParts[i] + ';';
                    if ((xUniqueParts == "") || (!xUniqueParts.Contains(xPart)))
                        xUniqueParts += xPart;
                }
                xUniqueParts = xUniqueParts.TrimEnd(new char[] { ';' });
            }
            return xUniqueParts;
        }
        #endregion
		#region *********************protected members*********************
        #endregion
    }

	/// <summary>
	///LMP.MacPac.NodeStore class -
	///defines a class that stores Word.XMLNodes -
	///offers XPath searching and subset retrieval
	///created by Daniel Fisherman - 08/18/05
	/// </summary>
	public class NodeStore:ReadOnlyNodeStore
	{
		private int m_iNextID = 0;

		#region *********************constructors*********************
		public NodeStore(fCOM.cTags oTags): base(oTags)
		{
			this.Add();
		}

        public NodeStore(fCOM.cTags oTags, bool bOrphans): base(oTags)
        {
            if (bOrphans)
                this.Add(null, mpNodeStoreMatchTypes.Equals);
            else
                this.Add();
        }

		public NodeStore(fCOM.cTags oTags, string xMatchString,
            mpNodeStoreMatchTypes oMatchType): base(oTags)
		{
			this.Add(xMatchString, oMatchType);
		}
		#endregion
		#region *********************methods*********************
		/// <summary>
		/// adds the fCOM.cTags collection to the store
		/// </summary>
		/// <param name="oNodes"></param>
		private void Add()
		{
			Add("" , mpNodeStoreMatchTypes.Any);
		}

		/// <summary>
		/// adds the fCOM.cTags collection to the store-
		/// filters according to MatchString and MatchType
		/// </summary>
		/// <param name="oNodes"></param>
		private void Add(string xMatchString, mpNodeStoreMatchTypes oMatchType)
		{
			DateTime t0 = DateTime.Now;

			StringBuilder oSB = new StringBuilder();

            //determine the type of object that this node store will contain
            if ((this.Type == LMP.Data.mpFileFormats.None) &&
                (m_oTags.Count > 0))
            {
                if (m_oTags.BoundingObjectType == fCOM.mpBoundingObjectTypes.ContentControls)
                    this.Type = LMP.Data.mpFileFormats.OpenXML;
                else
                    this.Type = LMP.Data.mpFileFormats.Binary;
            }

            if (xMatchString == null)
            {
                //match string is null - get standalone var and block nodes
                for (int i = 1; i <= m_oTags.Count; i++)
                {
                    fCOM.cTag oTag = m_oTags.get_Item(ref i);

                    if (oTag.ElementName == "mVar" || oTag.ElementName == "mBlock")
                    {
                        //tag is var or block - add to collection
                        //assign then increment ID counter
                        int iHashID = m_iNextID++;

                        //add new xml element to end of string
                        string xFormat = "<Node TagID='{0}' HashID='{1}' ObjectData='{2}' " +
                            "ElementName='{3}' Parts='{4}' DeletedScopes='{5}' Authors='{6}'/>";

                        string xObjectData = oTag.ObjectData;
                        if (xObjectData != null)
                            xObjectData = String.ReplaceXMLChars(xObjectData);
                        oSB.AppendFormat(xFormat, oTag.FullTagID, iHashID,
                            xObjectData, oTag.ElementName, "", "", "");

                        //add to base hashtable
                        this.BaseAdd(iHashID.ToString(), oTag.WordXMLNode);
                    }
                }
            }
            else
            {
                //cycle through cTags, adding those that satisfy the match condition
                for (int i = 1; i <= m_oTags.Count; i++)
                {
                    fCOM.cTag oTag = m_oTags.get_Item(ref i);

                    //the top level takes only mSEGs
                    if (oTag.ElementName != "mSEG")
                        continue;

                    //find tags in collection whose tags we want to add to the NodeStore
                    ArrayList oMatchingTags = new ArrayList();
                    GetMatchingTags(oTag, xMatchString, oMatchType, oMatchingTags);
                    for (int j = 0; j < oMatchingTags.Count; j++)
                    {
                        fCOM.cTag oMatchedTag = (fCOM.cTag) oMatchingTags[j];

                        //assign then increment ID counter
                        int iHashID = m_iNextID++;

                        //get tag properties
                        string xPart = oMatchedTag.Part;
                        string xDeletedScopes = oMatchedTag.DeletedScopes;

                        //replace xml chars
                        if (xDeletedScopes != null)
                            xDeletedScopes = LMP.String.ReplaceXMLChars(xDeletedScopes);

                        //append part number and separator
                        if ((xDeletedScopes != "") && (xDeletedScopes != null))
                            xDeletedScopes += "Part=" + xPart + "|¤¤";

                        //check if a node with the same tag ID already exists -
                        //the segment may have multiple parts
                        string xXML = oSB.ToString();
                        int iExistingNodePos = xXML.IndexOf("<Node TagID='" + oMatchedTag.FullTagID + "'");
                        if (iExistingNodePos > -1)
                        {
                            //add multipart node hash ID and child nodes to existing node
                            string xXMLMod = this.AddMultipartNode(xXML, iExistingNodePos,
                                xPart, iHashID, "mSEG", xDeletedScopes);
                            oSB.Remove(0, oSB.Length);
                            oSB.Append(xXMLMod);
                        }
                        else
                        {
                            //prepend deleted scopes separator
                            xDeletedScopes = "¤¤" + xDeletedScopes;
                            string xObjectData = oMatchedTag.ObjectData;
                            if (xObjectData != null)
                                xObjectData = String.ReplaceXMLChars(xObjectData);
                            string xAuthors = oMatchedTag.Authors;
                            if (xAuthors != null)
                                xAuthors = String.ReplaceXMLChars(xAuthors);

                            //add new xml element to end of string
                            //10.2 (dm) - added tag attribute
                            string xFormat = "<Node TagID='{0}' HashID='{1}' ObjectData='{2}' " +
                                "ElementName='mSEG' Parts='{3}' DeletedScopes='{4}' Authors='{5}' " + 
                                "AssociatedParentVariable='{6}' Tags='{7}'/>";

                            oSB.AppendFormat(xFormat, oMatchedTag.FullTagID, iHashID,
                                xObjectData, xPart, xDeletedScopes, xAuthors, 
                                oMatchedTag.ParentVariable, oMatchedTag.Tag);
                        }

                        //add child node xml
                        AddChildNodesXML(oMatchedTag, xPart, oSB);
                        
                        //add to base hashtable
                        if (oMatchedTag.WordXMLNode == null && oMatchedTag.ContentControl == null)
                            this.BaseAdd(iHashID.ToString(), oMatchedTag.Bookmark);
                        else if (this.Type == LMP.Data.mpFileFormats.Binary)
                            this.BaseAdd(iHashID.ToString(), oMatchedTag.WordXMLNode);
                        else
                            this.BaseAdd(iHashID.ToString(), oMatchedTag.ContentControl);
                    }
                }
            }

			oSB.Append("</Nodes>");

			//append new string to existing XML string
			m_oXML.InnerXml = m_oXML.InnerXml.Replace("</Nodes>", oSB.ToString());
			
			//get child nodes - for performance, we get this only once
			base.m_oChildNodes = m_oXML.DocumentElement.ChildNodes;

			LMP.Benchmarks.Print(t0, xMatchString);
		}

        /// <summary>
        /// adds a multipart node to the nodestore
        /// </summary>
        /// <param name="xNodesXML"></param>
        /// <param name="iExistingNodePos"></param>
        /// <param name="xPart"></param>
        /// <param name="iHashID"></param>
        /// <param name="xElementName"></param>
        /// <param name="xDeletedScopes"></param>
        /// <returns></returns>
		private string AddMultipartNode(string xNodesXML, int iExistingNodePos, 
			string xPart, int iHashID, string xElementName, string xDeletedScopes)
		{
            return this.AddMultipartNode(xNodesXML, iExistingNodePos, xPart, iHashID,
                xElementName, xDeletedScopes, "", "");
		}

        /// <summary>
        /// adds a multipart node to the nodestore
        /// </summary>
        /// <param name="xNodesXML"></param>
        /// <param name="iExistingNodePos"></param>
        /// <param name="xPart"></param>
        /// <param name="iHashID"></param>
        /// <param name="xElementName"></param>
        /// <param name="xDeletedScopes"></param>
        /// <param name="xObjectData"></param>
        /// <returns></returns>
        private string AddMultipartNode(string xNodesXML, int iExistingNodePos,
            string xPart, int iHashID, string xElementName, string xDeletedScopes,
            string xObjectData, string xTag)
        {
            //node already exists - add this tag's Hash ID to existing node
            int iStartPos = xNodesXML.IndexOf("HashID='", iExistingNodePos) + 9;
            int iEndPos = xNodesXML.IndexOf("'", iStartPos);
            xNodesXML = xNodesXML.Substring(0, iEndPos) + ";" + iHashID.ToString() +
                xNodesXML.Substring(iEndPos);

            //add this tag's part (for mSEGs) or parent part (for mVars)
            //number to the Parts attribute
            iStartPos = xNodesXML.IndexOf("Parts='", iExistingNodePos) + 8;
            iEndPos = xNodesXML.IndexOf("'", iStartPos);
            xNodesXML = xNodesXML.Substring(0, iEndPos) + ";" + xPart +
                xNodesXML.Substring(iEndPos);

            //add new part's deleted scopes
            if ((xElementName == "mSEG") && (xDeletedScopes != "") && (xDeletedScopes != null))
            {
                iStartPos = xNodesXML.IndexOf("DeletedScopes='", iExistingNodePos) + 15;
                iEndPos = xNodesXML.IndexOf("¤¤'", iStartPos) + 2;
                xNodesXML = xNodesXML.Substring(0, iEndPos) + xDeletedScopes +
                    xNodesXML.Substring(iEndPos);
            }

            //adjust for mixed tag type
            iStartPos = xNodesXML.IndexOf("ElementName='", iExistingNodePos) + 13;
            iEndPos = xNodesXML.IndexOf("'", iStartPos);
            string xOldElementName = xNodesXML.Substring(iStartPos, iEndPos - iStartPos);
            if (((xOldElementName == "mVar") && (xElementName == "mDel")) ||
                ((xOldElementName == "mDel") && (xElementName == "mVar")))
            {
                //adjust element name
                xNodesXML = xNodesXML.Substring(0, iStartPos) + "mixed" +
                    xNodesXML.Substring(iEndPos);

                //adjust object data - we're interested in mVar, not mDel
                if (xElementName == "mVar")
                {
                    iStartPos = xNodesXML.IndexOf("ObjectData='", iExistingNodePos) + 12;
                    iEndPos = xNodesXML.IndexOf("'", iStartPos);
                    xNodesXML = xNodesXML.Substring(0, iStartPos) + xObjectData +
                        xNodesXML.Substring(iEndPos);
                }
            }

            //10.2 (dm) - add tag if supplied and not already in the attribute -
            //each mDel has a unique tag
            if (xTag != "")
            {
                iStartPos = xNodesXML.IndexOf("Tags='", iExistingNodePos) + 6;
                iEndPos = xNodesXML.IndexOf("'", iStartPos);
                string xTags = xNodesXML.Substring(iStartPos, iEndPos - iStartPos);
                xTags = ';' + xTags + ';';
                if (xTags.IndexOf(';' + xTag + ';') == -1)
                {
                    xNodesXML = xNodesXML.Substring(0, iEndPos) + ";" + xTag +
                        xNodesXML.Substring(iEndPos);
                }
            }

            //return reconstructed nodes xml
            return xNodesXML;
        }

        /// <summary>
		/// adds the XML for the child nodes of the supplied tag
		/// </summary>
		/// <param name="oTag"></param>
		/// <param name="xParentPart"></param>
        /// <param name="oSB"></param>
        /// <returns></returns>
		private void AddChildNodesXML(fCOM.cTag oTag, string xParentPart,
            StringBuilder oSB)
		{
			fCOM.cTags oChildTags = oTag.Tags;

			//cycle through child nodes of node adding XML
			for(int i=1; i<=oChildTags.Count; i++)
			{
				fCOM.cTag oChildTag = oChildTags.get_Item(ref i);

                //get properties of child tag
                string xElementName = oChildTag.ElementName;
                string xObjectData = "";
                if (xElementName != "mDel")
                {
                    //mDel object data is used only by mpCOM - for mDels,
                    //the object data the front-end is concerned with is the
                    //variable def, which is in the mSEG's DeletedScopes attribute
                    xObjectData = oChildTag.ObjectData;
                    if (xObjectData != null)
                        xObjectData = String.ReplaceXMLChars(xObjectData);
                }
                string xAuthorDetail = oChildTag.Authors;
                if (xAuthorDetail != null)
                    xAuthorDetail = String.ReplaceXMLChars(xAuthorDetail);

                //get part and deleted scope
                string xPart;
                string xDeletedScopes = "";
                if (xElementName == "mSEG")
                {
                    xPart = oChildTag.Part;
                    xDeletedScopes = oChildTag.DeletedScopes;
                    if (xDeletedScopes != null)
                        xDeletedScopes = LMP.String.ReplaceXMLChars(xDeletedScopes);

                    //append part number and separator to deleted scopes
                    if ((xDeletedScopes != "") && (xDeletedScopes != null))
                        xDeletedScopes += "Part=" + xPart + "|¤¤";
                }
                else
                    xPart = xParentPart;

				//assign next ID as the hash id of this node
				int iHashID = m_iNextID++;

				//get the fully qualified tag id - since segment parts
				//2 and higher are nested in the part 1 in the cTags collection,
				//some children will be parts of the same segment - the tag id
				//should remain the name of the segment
				string xTagID = "";

				if(oChildTag.FullTagID == oTag.FullTagID)
					xTagID = oTag.FullTagID;
				else
					xTagID = oTag.FullTagID + "." + oChildTag.TagID;

                //check if a node with the same tag ID already exists
				string xXML = oSB.ToString();

				int iExistingNodePos = xXML.IndexOf("<Node TagID='" + xTagID + "'");

				if(iExistingNodePos > -1)
				{
					//add multipart node hash ID and child nodes to existing node
					string xXMLMod = this.AddMultipartNode(xXML, iExistingNodePos,
                        xPart, iHashID, xElementName, xDeletedScopes, xObjectData,
                        oChildTag.Tag);
					oSB.Remove(0, oSB.Length);
					oSB.Append(xXMLMod);
				}
				else
				{
                    //prepend deleted scopes separator
                    if (xElementName == "mSEG")
                        xDeletedScopes = "¤¤" + xDeletedScopes;

                    //add new xml element to end of string
                    //10.2 (dm) - added tag attribute
                    string xFormat = "<Node TagID='{0}' HashID='{1}' ObjectData='{2}' " +
                        "ElementName='{3}' Parts='{4}' DeletedScopes='{5}' Authors='{6}' " +
                        "Tags='{7}'/>";
                    oSB.AppendFormat(xFormat, xTagID, iHashID, xObjectData,
                        xElementName, xPart, xDeletedScopes, xAuthorDetail, oChildTag.Tag);
				}

				//add to base hashtable
                if(oChildTag.WordXMLNode == null && oChildTag.ContentControl == null)
                    this.BaseAdd(iHashID.ToString(), oChildTag.Bookmark);
                else if (this.Type == LMP.Data.mpFileFormats.Binary)
                    this.BaseAdd(iHashID.ToString(), oChildTag.WordXMLNode);
                else
                    this.BaseAdd(iHashID.ToString(), oChildTag.ContentControl);
			}
		}

		/// <summary>
		/// removes the specified XMLNode
		/// </summary>
		/// <param name="xTagID"></param>
		public void Remove(string xTagID)
		{
			//TODO: implement this method
			//delete from internal xml
		}
		#endregion
		#region *********************private methods*********************
		/// <summary>
		/// returns the string ID of the specified Word.XMLNode
		/// </summary>
		/// <param name="?"></param>
		/// <returns></returns>
		private string GetXMLNodeID(Word.XMLNode oNode)
		{
			string xID = "";
			string xXML = oNode.get_XML(true);
			
			int iPos = xXML.IndexOf("TagID");

			if(iPos > -1)
			{
				int iPos1 = xXML.IndexOf("\"", iPos + 1);
				int iPos2 = xXML.IndexOf("\"", iPos1 + 1);

				xID = xXML.Substring(iPos1 + 1, iPos2 - iPos1 - 1);
			}

			return xID;
		}

        /// <summary>
        /// adds all of the tags in oRootTag that match the specified criteria,
        /// including itself if applicable, to oMatchingTags
        /// </summary>
        /// <param name="oRootTag"></param>
        /// <param name="xMatchString"></param>
        /// <param name="oMatchType"></param>
        /// <param name="oMatchingTags"></param>
        /// <returns></returns>
        private void GetMatchingTags(fCOM.cTag oRootTag, string xMatchString,
            mpNodeStoreMatchTypes oMatchType, ArrayList oMatchingTags)
        {
            //check whether root tag is a match
            if (TagIsMatching(oRootTag, xMatchString, oMatchType))
                oMatchingTags.Add(oRootTag);

            //cycle through child tags
            for (int i = 1; i <= oRootTag.Tags.Count; i++)
            {
                fCOM.cTag oTag = oRootTag.Tags.get_Item(ref i);
                GetMatchingTags(oTag, xMatchString, oMatchType, oMatchingTags);
            }
        }

        /// <summary>
        /// returns TRUE if oTag matches the specified criteria
        /// </summary>
        /// <param name="oTag"></param>
        /// <param name="xMatchString"></param>
        /// <param name="oMatchType"></param>
        /// <returns></returns>
        private bool TagIsMatching(fCOM.cTag oTag, string xMatchString,
            mpNodeStoreMatchTypes oMatchType)
        {
            return (oMatchType == mpNodeStoreMatchTypes.Equals && oTag.FullTagID == xMatchString) ||
                   (oMatchType == mpNodeStoreMatchTypes.Any && oTag.FullTagID != "") ||
                   (oMatchType == mpNodeStoreMatchTypes.StartsWith && oTag.FullTagID.StartsWith(xMatchString)) ||
                   (oMatchType == mpNodeStoreMatchTypes.Contains && oTag.FullTagID.IndexOf(xMatchString) > -1);
        }

		#endregion
	}
}
