using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.MacPac;

namespace LMP.Controls
{
    internal partial class ControlForm : Form
    {
        public ControlForm()
        {
            InitializeComponent();
        }

        private void ControlForm_Deactivate(object sender, EventArgs e)
        {
        }

        private void ControlForm_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// processes the tab and shift-tab key combinations -
        /// selects next or previous node -
        /// unfortunately, there is an IBF bug that prevents
        /// this method from getting executed when appropriate -
        /// in cases when this method does not execute, the
        /// tab pressed event handler will execute
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            //if ((keyData & (Keys.Tab | Keys.Shift)) == (Keys.Tab | Keys.Shift))
            //{
            //    //Shift-Tab was pressed - select previous node
            //    this.Visible = false;
            //    return true;
            //}
            //else if ((keyData & Keys.Tab) == Keys.Tab)
            //{
            //    //tab was pressed - select next node
            //    this.Visible = false;
            //    return true;
            //}
            //else
                return false;
        }
    }
}