﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Infragistics.Win.UltraWinTree;
using LMP.Data;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class TrailerDisplayOptionsManager : LMP.Controls.ManagerBase
    {
        private bool m_bInitialized = false;
        public TrailerDisplayOptionsManager()
        {
            InitializeComponent();
        }
        ArrayList m_al = new ArrayList();

        private void TrailerDisplayOptionsManager_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;
                try
                {
                    LocalizeTrailerDisplayOptionsManager();

                    int iOption = (int)oSettings.TrailerInsertionOption;

                    if (iOption == 0)
                    {
                        FirmApplicationSettings oFirmSettings = Session.CurrentUser.FirmSettings;
                        if (oFirmSettings.DefaultTrailerBehavior == 0)
                            this.trailerDisplayOptionsComboBox1.SelectedIndex = 2;
                        else
                            this.trailerDisplayOptionsComboBox1.SelectedIndex = (int)oFirmSettings.DefaultTrailerBehavior - 1;
                    }
                    else
                    {
                        this.trailerDisplayOptionsComboBox1.SelectedIndex = iOption - 1;
                    }

                    m_bInitialized = true;
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
        }

        private void LocalizeTrailerDisplayOptionsManager()
        {
            this.lblTrailerDisplayOptions.Text = LMP.Resources.GetLangString("lblTrailerDisplayOptions");
        }

        private void trailerDisplayOptionsComboBox1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    TrailerInsertionOptions iOption = 0;
                    switch (this.trailerDisplayOptionsComboBox1.SelectedIndex)
                    {
                        case 0:
                            iOption = TrailerInsertionOptions.Manual;
                            break;
                        case 1:
                            iOption = TrailerInsertionOptions.Silent;
                            break;
                        case 2:
                            iOption = TrailerInsertionOptions.Prompt;
                            break;
                    }

                    Session.CurrentUser.UserSettings.TrailerInsertionOption = iOption;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
    }
}
