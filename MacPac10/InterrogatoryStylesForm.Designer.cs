﻿namespace LMP.MacPac
{
    partial class InterrogatoryStylesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.grpCharacterFormats = new System.Windows.Forms.GroupBox();
            this.chkUnderlined = new System.Windows.Forms.CheckBox();
            this.chkBold = new System.Windows.Forms.CheckBox();
            this.chkSmallCaps = new System.Windows.Forms.CheckBox();
            this.chkAllCaps = new System.Windows.Forms.CheckBox();
            this.grpParagraphFormats = new System.Windows.Forms.GroupBox();
            this.cmbLineSpacing = new LMP.Controls.ComboBox();
            this.spnSpaceAfter = new System.Windows.Forms.NumericUpDown();
            this.spnSpaceBefore = new System.Windows.Forms.NumericUpDown();
            this.spnFirstLineIndent = new System.Windows.Forms.NumericUpDown();
            this.lblSpaceAfter = new System.Windows.Forms.Label();
            this.lblSpaceBefore = new System.Windows.Forms.Label();
            this.lblSpacing = new System.Windows.Forms.Label();
            this.lblFirstLineIndent = new System.Windows.Forms.Label();
            this.lblStyles = new System.Windows.Forms.Label();
            this.lstStyles = new LMP.Controls.ListBox();
            this.grpCharacterFormats.SuspendLayout();
            this.grpParagraphFormats.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spnSpaceAfter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSpaceBefore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnFirstLineIndent)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(313, 301);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 25);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Location = new System.Drawing.Point(12, 164);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(162, 25);
            this.btnReset.TabIndex = 11;
            this.btnReset.TabStop = false;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.AutoSize = true;
            this.btnSave.Location = new System.Drawing.Point(220, 301);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(77, 25);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // grpCharacterFormats
            // 
            this.grpCharacterFormats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCharacterFormats.AutoSize = true;
            this.grpCharacterFormats.Controls.Add(this.chkUnderlined);
            this.grpCharacterFormats.Controls.Add(this.chkBold);
            this.grpCharacterFormats.Controls.Add(this.chkSmallCaps);
            this.grpCharacterFormats.Controls.Add(this.chkAllCaps);
            this.grpCharacterFormats.Location = new System.Drawing.Point(190, 18);
            this.grpCharacterFormats.Name = "grpCharacterFormats";
            this.grpCharacterFormats.Size = new System.Drawing.Size(218, 93);
            this.grpCharacterFormats.TabIndex = 9;
            this.grpCharacterFormats.TabStop = false;
            this.grpCharacterFormats.Text = "Character Formats";
            // 
            // chkUnderlined
            // 
            this.chkUnderlined.AutoSize = true;
            this.chkUnderlined.Location = new System.Drawing.Point(112, 23);
            this.chkUnderlined.Name = "chkUnderlined";
            this.chkUnderlined.Size = new System.Drawing.Size(77, 17);
            this.chkUnderlined.TabIndex = 3;
            this.chkUnderlined.Text = "&Underlined";
            this.chkUnderlined.UseVisualStyleBackColor = true;
            this.chkUnderlined.CheckedChanged += new System.EventHandler(this.chkUnderlined_CheckedChanged);
            // 
            // chkBold
            // 
            this.chkBold.AutoSize = true;
            this.chkBold.Location = new System.Drawing.Point(20, 23);
            this.chkBold.Name = "chkBold";
            this.chkBold.Size = new System.Drawing.Size(47, 17);
            this.chkBold.TabIndex = 2;
            this.chkBold.Text = "&Bold";
            this.chkBold.UseVisualStyleBackColor = true;
            this.chkBold.Click += new System.EventHandler(this.chkBoldText_CheckedChanged);
            // 
            // chkSmallCaps
            // 
            this.chkSmallCaps.AutoSize = true;
            this.chkSmallCaps.Location = new System.Drawing.Point(112, 57);
            this.chkSmallCaps.Name = "chkSmallCaps";
            this.chkSmallCaps.Size = new System.Drawing.Size(78, 17);
            this.chkSmallCaps.TabIndex = 5;
            this.chkSmallCaps.Text = "S&mall Caps";
            this.chkSmallCaps.UseVisualStyleBackColor = true;
            this.chkSmallCaps.CheckedChanged += new System.EventHandler(this.chkSmallCaps_CheckedChanged);
            // 
            // chkAllCaps
            // 
            this.chkAllCaps.AutoSize = true;
            this.chkAllCaps.Checked = true;
            this.chkAllCaps.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllCaps.Location = new System.Drawing.Point(20, 57);
            this.chkAllCaps.Name = "chkAllCaps";
            this.chkAllCaps.Size = new System.Drawing.Size(64, 17);
            this.chkAllCaps.TabIndex = 4;
            this.chkAllCaps.Text = "&All Caps";
            this.chkAllCaps.UseVisualStyleBackColor = true;
            this.chkAllCaps.CheckedChanged += new System.EventHandler(this.chkAllCaps_CheckedChanged);
            // 
            // grpParagraphFormats
            // 
            this.grpParagraphFormats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpParagraphFormats.AutoSize = true;
            this.grpParagraphFormats.Controls.Add(this.cmbLineSpacing);
            this.grpParagraphFormats.Controls.Add(this.spnSpaceAfter);
            this.grpParagraphFormats.Controls.Add(this.spnSpaceBefore);
            this.grpParagraphFormats.Controls.Add(this.spnFirstLineIndent);
            this.grpParagraphFormats.Controls.Add(this.lblSpaceAfter);
            this.grpParagraphFormats.Controls.Add(this.lblSpaceBefore);
            this.grpParagraphFormats.Controls.Add(this.lblSpacing);
            this.grpParagraphFormats.Controls.Add(this.lblFirstLineIndent);
            this.grpParagraphFormats.Location = new System.Drawing.Point(190, 125);
            this.grpParagraphFormats.Name = "grpParagraphFormats";
            this.grpParagraphFormats.Size = new System.Drawing.Size(218, 167);
            this.grpParagraphFormats.TabIndex = 8;
            this.grpParagraphFormats.TabStop = false;
            this.grpParagraphFormats.Text = "Paragraph Formats";
            // 
            // cmbLineSpacing
            // 
            this.cmbLineSpacing.AllowEmptyValue = false;
            this.cmbLineSpacing.AutoSize = true;
            this.cmbLineSpacing.Borderless = false;
            this.cmbLineSpacing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLineSpacing.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLineSpacing.IsDirty = false;
            this.cmbLineSpacing.LimitToList = true;
            this.cmbLineSpacing.ListName = "";
            this.cmbLineSpacing.Location = new System.Drawing.Point(105, 58);
            this.cmbLineSpacing.MaxDropDownItems = 8;
            this.cmbLineSpacing.Name = "cmbLineSpacing";
            this.cmbLineSpacing.SelectedIndex = -1;
            this.cmbLineSpacing.SelectedValue = null;
            this.cmbLineSpacing.SelectionLength = 0;
            this.cmbLineSpacing.SelectionStart = 0;
            this.cmbLineSpacing.Size = new System.Drawing.Size(85, 26);
            this.cmbLineSpacing.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLineSpacing.SupportingValues = "";
            this.cmbLineSpacing.TabIndex = 9;
            this.cmbLineSpacing.Tag2 = null;
            this.cmbLineSpacing.Value = "";
            this.cmbLineSpacing.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLineSpacing_ValueChanged);
            // 
            // spnSpaceAfter
            // 
            this.spnSpaceAfter.Increment = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.spnSpaceAfter.Location = new System.Drawing.Point(105, 123);
            this.spnSpaceAfter.Name = "spnSpaceAfter";
            this.spnSpaceAfter.Size = new System.Drawing.Size(85, 20);
            this.spnSpaceAfter.TabIndex = 13;
            this.spnSpaceAfter.ValueChanged += new System.EventHandler(this.spnSpaceAfter_ValueChanged);
            // 
            // spnSpaceBefore
            // 
            this.spnSpaceBefore.Increment = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.spnSpaceBefore.Location = new System.Drawing.Point(105, 92);
            this.spnSpaceBefore.Name = "spnSpaceBefore";
            this.spnSpaceBefore.Size = new System.Drawing.Size(85, 20);
            this.spnSpaceBefore.TabIndex = 11;
            this.spnSpaceBefore.ValueChanged += new System.EventHandler(this.spnSpaceBefore_ValueChanged);
            // 
            // spnFirstLineIndent
            // 
            this.spnFirstLineIndent.DecimalPlaces = 1;
            this.spnFirstLineIndent.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spnFirstLineIndent.Location = new System.Drawing.Point(105, 29);
            this.spnFirstLineIndent.Name = "spnFirstLineIndent";
            this.spnFirstLineIndent.Size = new System.Drawing.Size(85, 20);
            this.spnFirstLineIndent.TabIndex = 7;
            this.spnFirstLineIndent.ValueChanged += new System.EventHandler(this.spnFirstLineIndent_ValueChanged);
            // 
            // lblSpaceAfter
            // 
            this.lblSpaceAfter.AutoSize = true;
            this.lblSpaceAfter.Location = new System.Drawing.Point(17, 125);
            this.lblSpaceAfter.Name = "lblSpaceAfter";
            this.lblSpaceAfter.Size = new System.Drawing.Size(66, 13);
            this.lblSpaceAfter.TabIndex = 12;
            this.lblSpaceAfter.Text = "Spa&ce After:";
            // 
            // lblSpaceBefore
            // 
            this.lblSpaceBefore.AutoSize = true;
            this.lblSpaceBefore.Location = new System.Drawing.Point(17, 95);
            this.lblSpaceBefore.Name = "lblSpaceBefore";
            this.lblSpaceBefore.Size = new System.Drawing.Size(75, 13);
            this.lblSpaceBefore.TabIndex = 10;
            this.lblSpaceBefore.Text = "Space B&efore:";
            // 
            // lblSpacing
            // 
            this.lblSpacing.AutoSize = true;
            this.lblSpacing.Location = new System.Drawing.Point(17, 63);
            this.lblSpacing.Name = "lblSpacing";
            this.lblSpacing.Size = new System.Drawing.Size(49, 13);
            this.lblSpacing.TabIndex = 8;
            this.lblSpacing.Text = "S&pacing:";
            // 
            // lblFirstLineIndent
            // 
            this.lblFirstLineIndent.AutoSize = true;
            this.lblFirstLineIndent.Location = new System.Drawing.Point(17, 31);
            this.lblFirstLineIndent.Name = "lblFirstLineIndent";
            this.lblFirstLineIndent.Size = new System.Drawing.Size(85, 13);
            this.lblFirstLineIndent.TabIndex = 6;
            this.lblFirstLineIndent.Text = "First &Line Indent:";
            // 
            // lblStyles
            // 
            this.lblStyles.AutoSize = true;
            this.lblStyles.Location = new System.Drawing.Point(11, 8);
            this.lblStyles.Name = "lblStyles";
            this.lblStyles.Size = new System.Drawing.Size(38, 13);
            this.lblStyles.TabIndex = 0;
            this.lblStyles.Text = "S&tyles:";
            // 
            // lstStyles
            // 
            this.lstStyles.AllowEmptyValue = false;
            this.lstStyles.FormattingEnabled = true;
            this.lstStyles.IsDirty = false;
            this.lstStyles.ListName = "";
            this.lstStyles.Location = new System.Drawing.Point(12, 24);
            this.lstStyles.Name = "lstStyles";
            this.lstStyles.Size = new System.Drawing.Size(162, 134);
            this.lstStyles.SupportingValues = "";
            this.lstStyles.TabIndex = 1;
            this.lstStyles.Tag2 = null;
            this.lstStyles.Value = "";
            this.lstStyles.SelectedIndexChanged += new System.EventHandler(this.lstStyles_SelectedIndexChanged);
            // 
            // InterrogatoryStylesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(420, 338);
            this.Controls.Add(this.lstStyles);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.grpCharacterFormats);
            this.Controls.Add(this.grpParagraphFormats);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblStyles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InterrogatoryStylesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Discovery Styles";
            this.Load += new System.EventHandler(this.InterrogatoryStylesForm_Load);
            this.grpCharacterFormats.ResumeLayout(false);
            this.grpCharacterFormats.PerformLayout();
            this.grpParagraphFormats.ResumeLayout(false);
            this.grpParagraphFormats.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spnSpaceAfter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSpaceBefore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnFirstLineIndent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox grpCharacterFormats;
        private System.Windows.Forms.CheckBox chkUnderlined;
        private System.Windows.Forms.CheckBox chkBold;
        private System.Windows.Forms.CheckBox chkSmallCaps;
        private System.Windows.Forms.CheckBox chkAllCaps;
        private System.Windows.Forms.GroupBox grpParagraphFormats;
        private System.Windows.Forms.NumericUpDown spnSpaceAfter;
        private System.Windows.Forms.NumericUpDown spnSpaceBefore;
        private System.Windows.Forms.NumericUpDown spnFirstLineIndent;
        private System.Windows.Forms.Label lblSpaceAfter;
        private System.Windows.Forms.Label lblSpaceBefore;
        private System.Windows.Forms.Label lblSpacing;
        private System.Windows.Forms.Label lblFirstLineIndent;
        private System.Windows.Forms.Label lblStyles;
        private LMP.Controls.ListBox lstStyles;
        private LMP.Controls.ComboBox cmbLineSpacing;
    }
}