namespace LMP.MacPac
{
    partial class PrefillChooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblPrefill = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.grpOptions = new System.Windows.Forms.GroupBox();
            this.chkRestartNumbering = new System.Windows.Forms.CheckBox();
            this.chkKeepExisting = new System.Windows.Forms.CheckBox();
            this.chkSeparateSection = new System.Windows.Forms.CheckBox();
            this.cmbLocation = new LMP.Controls.ComboBox();
            this.ftvPrefill = new LMP.Controls.SearchableFolderTreeView();
            this.lblFormSizeMarker1 = new System.Windows.Forms.Label();
            this.grpOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(346, 303);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(427, 303);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblPrefill
            // 
            this.lblPrefill.AutoSize = true;
            this.lblPrefill.BackColor = System.Drawing.Color.Transparent;
            this.lblPrefill.Location = new System.Drawing.Point(12, 12);
            this.lblPrefill.Name = "lblPrefill";
            this.lblPrefill.Size = new System.Drawing.Size(36, 15);
            this.lblPrefill.TabIndex = 0;
            this.lblPrefill.Text = "&Prefill:";
            // 
            // lblLocation
            // 
            this.lblLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(287, 14);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(96, 15);
            this.lblLocation.TabIndex = 2;
            this.lblLocation.Text = "Insertion &Location:";
            // 
            // grpOptions
            // 
            this.grpOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpOptions.AutoSize = true;
            this.grpOptions.Controls.Add(this.chkRestartNumbering);
            this.grpOptions.Controls.Add(this.chkKeepExisting);
            this.grpOptions.Controls.Add(this.chkSeparateSection);
            this.grpOptions.Location = new System.Drawing.Point(287, 76);
            this.grpOptions.Name = "grpOptions";
            this.grpOptions.Size = new System.Drawing.Size(215, 108);
            this.grpOptions.TabIndex = 4;
            this.grpOptions.TabStop = false;
            this.grpOptions.Text = "Insertion Options";
            // 
            // chkRestartNumbering
            // 
            this.chkRestartNumbering.AutoSize = true;
            this.chkRestartNumbering.Location = new System.Drawing.Point(16, 68);
            this.chkRestartNumbering.Name = "chkRestartNumbering";
            this.chkRestartNumbering.Size = new System.Drawing.Size(145, 19);
            this.chkRestartNumbering.TabIndex = 13;
            this.chkRestartNumbering.Text = "&Restart Page Numbering";
            this.chkRestartNumbering.UseVisualStyleBackColor = true;
            // 
            // chkKeepExisting
            // 
            this.chkKeepExisting.AutoSize = true;
            this.chkKeepExisting.Location = new System.Drawing.Point(16, 44);
            this.chkKeepExisting.Name = "chkKeepExisting";
            this.chkKeepExisting.Size = new System.Drawing.Size(178, 19);
            this.chkKeepExisting.TabIndex = 12;
            this.chkKeepExisting.Text = "Keep &Existing Headers/Footers";
            this.chkKeepExisting.UseVisualStyleBackColor = true;
            // 
            // chkSeparateSection
            // 
            this.chkSeparateSection.AutoSize = true;
            this.chkSeparateSection.Location = new System.Drawing.Point(16, 22);
            this.chkSeparateSection.Name = "chkSeparateSection";
            this.chkSeparateSection.Size = new System.Drawing.Size(151, 19);
            this.chkSeparateSection.TabIndex = 11;
            this.chkSeparateSection.Text = "Insert in &Separate Section";
            this.chkSeparateSection.UseVisualStyleBackColor = true;
            // 
            // cmbLocation
            // 
            this.cmbLocation.AllowEmptyValue = false;
            this.cmbLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLocation.AutoSize = true;
            this.cmbLocation.Borderless = false;
            this.cmbLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLocation.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLocation.IsDirty = false;
            this.cmbLocation.LimitToList = true;
            this.cmbLocation.ListName = "";
            this.cmbLocation.Location = new System.Drawing.Point(287, 32);
            this.cmbLocation.MaxDropDownItems = 8;
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.SelectedIndex = -1;
            this.cmbLocation.SelectedValue = null;
            this.cmbLocation.SelectionLength = 0;
            this.cmbLocation.SelectionStart = 0;
            this.cmbLocation.Size = new System.Drawing.Size(215, 26);
            this.cmbLocation.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLocation.SupportingValues = "";
            this.cmbLocation.TabIndex = 3;
            this.cmbLocation.Tag2 = null;
            this.cmbLocation.Value = "";
            this.cmbLocation.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLocation_ValueChanged);
            this.cmbLocation.Load += new System.EventHandler(this.cmbLocation_Load);
            // 
            // ftvPrefill
            // 
            this.ftvPrefill.AllowFavoritesMenu = false;
            this.ftvPrefill.AllowSearching = true;
            this.ftvPrefill.AutoSize = true;
            this.ftvPrefill.DisableExcludedTypes = false;
            this.ftvPrefill.DisplayOptions = ((LMP.Controls.FolderTreeView.mpFolderTreeViewOptions)((((((LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserSegments) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.StyleSheets) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SegmentPackets)));
            this.ftvPrefill.ExcludeNonMacPacTypes = false;
            this.ftvPrefill.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvPrefill.Location = new System.Drawing.Point(12, 25);
            this.ftvPrefill.Name = "ftvPrefill";
            this.ftvPrefill.OwnerID = 0;
            this.ftvPrefill.ShowCheckboxes = false;
            this.ftvPrefill.Size = new System.Drawing.Size(257, 273);
            this.ftvPrefill.TabIndex = 1;
            this.ftvPrefill.DoubleClick += new System.EventHandler(this.ftvPrefill_DoubleClick);
            // 
            // lblFormSizeMarker1
            // 
            this.lblFormSizeMarker1.AutoSize = true;
            this.lblFormSizeMarker1.Location = new System.Drawing.Point(246, 256);
            this.lblFormSizeMarker1.Name = "lblFormSizeMarker1";
            this.lblFormSizeMarker1.Size = new System.Drawing.Size(23, 15);
            this.lblFormSizeMarker1.TabIndex = 12;
            this.lblFormSizeMarker1.Text = "----";
            this.lblFormSizeMarker1.Visible = false;
            // 
            // PrefillChooser
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(515, 340);
            this.Controls.Add(this.lblFormSizeMarker1);
            this.Controls.Add(this.ftvPrefill);
            this.Controls.Add(this.cmbLocation);
            this.Controls.Add(this.grpOptions);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.lblPrefill);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrefillChooser";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert using Prefill";
            this.grpOptions.ResumeLayout(false);
            this.grpOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblPrefill;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.GroupBox grpOptions;
        private System.Windows.Forms.CheckBox chkRestartNumbering;
        private System.Windows.Forms.CheckBox chkKeepExisting;
        private System.Windows.Forms.CheckBox chkSeparateSection;
        private LMP.Controls.ComboBox cmbLocation;
        private LMP.Controls.SearchableFolderTreeView ftvPrefill;
        private System.Windows.Forms.Label lblFormSizeMarker1;
    }
}