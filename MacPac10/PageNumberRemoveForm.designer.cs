namespace LMP.MacPac
{
    partial class PageNumberRemoveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbRemoveOptions = new System.Windows.Forms.ComboBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.lblRemoveInfo = new System.Windows.Forms.Label();
            this.gbApplyTo = new System.Windows.Forms.GroupBox();
            this.lblRemoveOptions = new System.Windows.Forms.Label();
            this.txtMultiple = new System.Windows.Forms.TextBox();
            this.rbMultiple = new System.Windows.Forms.RadioButton();
            this.rbSection = new System.Windows.Forms.RadioButton();
            this.rbDocument = new System.Windows.Forms.RadioButton();
            this.gbApplyTo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(175, 244);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 25);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmbRemoveOptions
            // 
            this.cmbRemoveOptions.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbRemoveOptions.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbRemoveOptions.FormattingEnabled = true;
            this.cmbRemoveOptions.Location = new System.Drawing.Point(16, 141);
            this.cmbRemoveOptions.Name = "cmbRemoveOptions";
            this.cmbRemoveOptions.Size = new System.Drawing.Size(207, 23);
            this.cmbRemoveOptions.TabIndex = 2;
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.AutoSize = true;
            this.btnRemove.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnRemove.Location = new System.Drawing.Point(86, 244);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(81, 25);
            this.btnRemove.TabIndex = 3;
            this.btnRemove.Text = "&Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            this.btnRemove.Enter += new System.EventHandler(this.btnRemove_Enter);
            // 
            // lblRemoveInfo
            // 
            this.lblRemoveInfo.AutoSize = true;
            this.lblRemoveInfo.Location = new System.Drawing.Point(19, 200);
            this.lblRemoveInfo.MaximumSize = new System.Drawing.Size(230, 35);
            this.lblRemoveInfo.Name = "lblRemoveInfo";
            this.lblRemoveInfo.Size = new System.Drawing.Size(50, 15);
            this.lblRemoveInfo.TabIndex = 3;
            this.lblRemoveInfo.Text = "Info Text";
            this.lblRemoveInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // gbApplyTo
            // 
            this.gbApplyTo.AutoSize = true;
            this.gbApplyTo.Controls.Add(this.cmbRemoveOptions);
            this.gbApplyTo.Controls.Add(this.lblRemoveOptions);
            this.gbApplyTo.Controls.Add(this.txtMultiple);
            this.gbApplyTo.Controls.Add(this.rbMultiple);
            this.gbApplyTo.Controls.Add(this.rbSection);
            this.gbApplyTo.Controls.Add(this.rbDocument);
            this.gbApplyTo.Location = new System.Drawing.Point(11, 10);
            this.gbApplyTo.Name = "gbApplyTo";
            this.gbApplyTo.Size = new System.Drawing.Size(248, 185);
            this.gbApplyTo.TabIndex = 1;
            this.gbApplyTo.TabStop = false;
            this.gbApplyTo.Text = "Remove From";
            // 
            // lblRemoveOptions
            // 
            this.lblRemoveOptions.AutoSize = true;
            this.lblRemoveOptions.Location = new System.Drawing.Point(13, 123);
            this.lblRemoveOptions.Name = "lblRemoveOptions";
            this.lblRemoveOptions.Size = new System.Drawing.Size(41, 15);
            this.lblRemoveOptions.TabIndex = 1;
            this.lblRemoveOptions.Text = "&Pages:";
            // 
            // txtMultiple
            // 
            this.txtMultiple.Enabled = false;
            this.txtMultiple.Location = new System.Drawing.Point(35, 94);
            this.txtMultiple.Name = "txtMultiple";
            this.txtMultiple.Size = new System.Drawing.Size(188, 22);
            this.txtMultiple.TabIndex = 4;
            this.txtMultiple.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMultiple_KeyPress);
            // 
            // rbMultiple
            // 
            this.rbMultiple.AutoSize = true;
            this.rbMultiple.Location = new System.Drawing.Point(16, 74);
            this.rbMultiple.Name = "rbMultiple";
            this.rbMultiple.Size = new System.Drawing.Size(181, 19);
            this.rbMultiple.TabIndex = 3;
            this.rbMultiple.Text = "M&ultiple Sections (e.g., 1, 3-5, 7)";
            this.rbMultiple.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbMultiple.UseVisualStyleBackColor = true;
            this.rbMultiple.CheckedChanged += new System.EventHandler(this.rbMultiple_CheckedChanged);
            // 
            // rbSection
            // 
            this.rbSection.AutoSize = true;
            this.rbSection.Location = new System.Drawing.Point(16, 50);
            this.rbSection.Name = "rbSection";
            this.rbSection.Size = new System.Drawing.Size(101, 19);
            this.rbSection.TabIndex = 2;
            this.rbSection.Text = "Current S&ection";
            this.rbSection.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbSection.UseVisualStyleBackColor = true;
            // 
            // rbDocument
            // 
            this.rbDocument.AutoSize = true;
            this.rbDocument.Checked = true;
            this.rbDocument.Location = new System.Drawing.Point(16, 25);
            this.rbDocument.Name = "rbDocument";
            this.rbDocument.Size = new System.Drawing.Size(105, 19);
            this.rbDocument.TabIndex = 1;
            this.rbDocument.TabStop = true;
            this.rbDocument.Text = "Entire &Document";
            this.rbDocument.UseVisualStyleBackColor = true;
            // 
            // PageNumberRemoveForm
            // 
            this.AcceptButton = this.btnRemove;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(272, 281);
            this.Controls.Add(this.gbApplyTo);
            this.Controls.Add(this.lblRemoveInfo);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnCancel);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PageNumberRemoveForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Page Number";
            this.gbApplyTo.ResumeLayout(false);
            this.gbApplyTo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cmbRemoveOptions;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Label lblRemoveInfo;
        private System.Windows.Forms.GroupBox gbApplyTo;
        private System.Windows.Forms.TextBox txtMultiple;
        private System.Windows.Forms.RadioButton rbMultiple;
        private System.Windows.Forms.RadioButton rbSection;
        private System.Windows.Forms.RadioButton rbDocument;
        private System.Windows.Forms.Label lblRemoveOptions;
    }
}