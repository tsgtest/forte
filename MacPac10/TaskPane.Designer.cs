using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class TaskPane
    {
        #region *********************fields*********************
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl pageContent;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl pageEdit;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl pageImportVariables;
        private System.Windows.Forms.PictureBox picMenu;
        private LMP.MacPac.ContentManager ucContentManager;
        private LMP.MacPac.DocumentDesigner ucDocDesigner;
        private LMP.MacPac.DocumentEditor ucDocEditor;
        private LMP.MacPac.DataReviewer ucDataReviewer;
        private System.ComponentModel.IContainer components = null;
        #endregion
        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskPane));
            this.pageContent = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.pageEdit = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.pageImportVariables = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.pageReview = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tabMain = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.picMenu = new System.Windows.Forms.PictureBox();
            this.mnuMacPac_ConvertToLanguageSupport = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuMacPac_FavoriteSegments = new System.Windows.Forms.ToolStripMenuItem();
            this.FavoriteSegmentPlaceHolder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_PasteSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_UpdateCI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_UpdateDocumentContent = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMacPac_ToggleDocTrailer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_ToggleSecTrailer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMacPac_ManagePeople = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_AppSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_ChangeUser = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_EditAuthorPrefs = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMacPac_RecreateLegacyContent = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_SaveLegacyData = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_Sep7 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMacPac_RefreshTree = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_PauseResume = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_ShowKeyboardShortcuts = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_Sep5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMacPac_ImportSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_ShowAdministrator = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_Sep6 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMacPac_ShowAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMacPac_Sep4 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).BeginInit();
            this.tabMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMenu)).BeginInit();
            this.mnuMacPac.SuspendLayout();
            this.SuspendLayout();
            // 
            // pageContent
            // 
            this.pageContent.Location = new System.Drawing.Point(-10000, -10000);
            this.pageContent.Name = "pageContent";
            this.pageContent.Size = new System.Drawing.Size(312, 698);
            // 
            // pageEdit
            // 
            this.pageEdit.Location = new System.Drawing.Point(-10000, -10000);
            this.pageEdit.Name = "pageEdit";
            this.pageEdit.Size = new System.Drawing.Size(312, 698);
            // 
            // pageImportVariables
            // 
            this.pageImportVariables.Location = new System.Drawing.Point(-10000, -10000);
            this.pageImportVariables.Name = "pageImportVariables";
            this.pageImportVariables.Size = new System.Drawing.Size(312, 698);
            // 
            // pageReview
            // 
            this.pageReview.Location = new System.Drawing.Point(-10000, -10000);
            this.pageReview.Name = "pageReview";
            this.pageReview.Size = new System.Drawing.Size(312, 698);
            // 
            // tabMain
            // 
            appearance1.BorderColor = System.Drawing.Color.DarkGray;
            appearance1.FontData.BoldAsString = "True";
            appearance1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabMain.ActiveTabAppearance = appearance1;
            this.tabMain.AllowDrop = true;
            appearance2.BackColor = System.Drawing.Color.PaleTurquoise;
            appearance2.BackColor2 = System.Drawing.Color.White;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.BackwardDiagonal;
            appearance2.BorderColor3DBase = System.Drawing.Color.White;
            appearance2.FontData.Name = "Microsoft Sans Serif";
            appearance2.ForeColor = System.Drawing.Color.DimGray;
            this.tabMain.Appearance = appearance2;
            this.tabMain.BackColorInternal = System.Drawing.Color.Transparent;
            this.tabMain.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabMain.Controls.Add(this.pageContent);
            this.tabMain.Controls.Add(this.pageEdit);
            this.tabMain.Controls.Add(this.pageImportVariables);
            this.tabMain.Controls.Add(this.pageReview);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.MinTabWidth = 0;
            this.tabMain.Name = "tabMain";
            this.tabMain.ScrollArrowStyle = Infragistics.Win.UltraWinTabs.ScrollArrowStyle.VisualStudio;
            this.tabMain.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabMain.ShowButtonSeparators = true;
            this.tabMain.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
            this.tabMain.Size = new System.Drawing.Size(312, 720);
            this.tabMain.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.StateButtons;
            this.tabMain.TabButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            this.tabMain.TabIndex = 2;
            appearance3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            appearance3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.tabMain.TabListButtonAppearance = appearance3;
            this.tabMain.TabPadding = new System.Drawing.Size(2, 2);
            ultraTab1.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
            ultraTab1.Key = "ContentManager";
            ultraTab1.TabPage = this.pageContent;
            ultraTab1.Text = "&Create/Insert";
            ultraTab2.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
            ultraTab2.Key = "DocumentEditor";
            ultraTab2.TabPage = this.pageEdit;
            ultraTab2.Text = "&Edit";
            ultraTab3.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
            ultraTab3.Key = "ImportLibrary";
            ultraTab3.TabPage = this.pageImportVariables;
            ultraTab3.Text = "I&mport";
            ultraTab3.Visible = false;
            ultraTab4.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
            ultraTab4.Key = "DataReviewer";
            ultraTab4.TabPage = this.pageReview;
            ultraTab4.Text = "&Finished Data";
            ultraTab4.Visible = false;
            this.tabMain.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.tabMain.TabStop = false;
            this.tabMain.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabMain_MouseClick);
            this.tabMain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabMain_KeyDown);
            this.tabMain.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.tabMain_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(0, 22);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(312, 698);
            // 
            // picMenu
            // 
            this.picMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picMenu.Image = ((System.Drawing.Image)(resources.GetObject("picMenu.Image")));
            this.picMenu.Location = new System.Drawing.Point(295, 6);
            this.picMenu.Name = "picMenu";
            this.picMenu.Size = new System.Drawing.Size(11, 11);
            this.picMenu.TabIndex = 3;
            this.picMenu.TabStop = false;
            this.picMenu.Click += new System.EventHandler(this.picMenu_Click);
            // 
            // mnuMacPac_ConvertToLanguageSupport
            // 
            this.mnuMacPac_ConvertToLanguageSupport.Name = "mnuMacPac_ConvertToLanguageSupport";
            this.mnuMacPac_ConvertToLanguageSupport.Size = new System.Drawing.Size(32, 19);
            // 
            // mnuMacPac
            // 
            this.mnuMacPac.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuMacPac_FavoriteSegments,
            this.mnuMacPac_PasteSegment,
            this.mnuMacPac_UpdateCI,
            this.mnuMacPac_UpdateDocumentContent,
            this.mnuMacPac_Sep1,
            this.mnuMacPac_ToggleDocTrailer,
            this.mnuMacPac_ToggleSecTrailer,
            this.mnuMacPac_Sep2,
            this.mnuMacPac_ManagePeople,
            this.mnuMacPac_AppSettings,
            this.mnuMacPac_ChangeUser,
            this.mnuMacPac_EditAuthorPrefs,
            this.mnuMacPac_Sep3,
            this.mnuMacPac_RecreateLegacyContent,
            this.mnuMacPac_SaveLegacyData,
            this.mnuMacPac_Sep7,
            this.mnuMacPac_RefreshTree,
            this.mnuMacPac_PauseResume,
            this.mnuMacPac_ShowKeyboardShortcuts,
            this.mnuMacPac_Sep5,
            this.mnuMacPac_ImportSegment,
            this.mnuMacPac_ShowAdministrator,
            this.mnuMacPac_Sep6,
            this.mnuMacPac_ShowAbout});
            this.mnuMacPac.Name = "mnuMacPac";
            this.mnuMacPac.Size = new System.Drawing.Size(242, 436);
            this.mnuMacPac.TimerInterval = 500;
            this.mnuMacPac.UseTimer = true;
            this.mnuMacPac.Opening += new System.ComponentModel.CancelEventHandler(this.mnuMacPac_Opening);
            // 
            // mnuMacPac_FavoriteSegments
            // 
            this.mnuMacPac_FavoriteSegments.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FavoriteSegmentPlaceHolder});
            this.mnuMacPac_FavoriteSegments.Name = "mnuMacPac_FavoriteSegments";
            this.mnuMacPac_FavoriteSegments.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_FavoriteSegments.Text = "#Favorite Segments#";
            this.mnuMacPac_FavoriteSegments.DropDownOpening += new System.EventHandler(this.mnuMacPac_FavoriteSegments_DropDownOpening);
            // 
            // FavoriteSegmentPlaceHolder
            // 
            this.FavoriteSegmentPlaceHolder.Name = "FavoriteSegmentPlaceHolder";
            this.FavoriteSegmentPlaceHolder.Size = new System.Drawing.Size(236, 22);
            this.FavoriteSegmentPlaceHolder.Text = "Favorite Segment Place Holder";
            // 
            // mnuMacPac_PasteSegment
            // 
            this.mnuMacPac_PasteSegment.Name = "mnuMacPac_PasteSegment";
            this.mnuMacPac_PasteSegment.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_PasteSegment.Text = "#Paste Segment#";
            this.mnuMacPac_PasteSegment.Click += new System.EventHandler(this.mnuMacPac_PasteSegment_Click);
            // 
            // mnuMacPac_UpdateCI
            // 
            this.mnuMacPac_UpdateCI.Name = "mnuMacPac_UpdateCI";
            this.mnuMacPac_UpdateCI.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_UpdateCI.Text = "#Update CI#";
            this.mnuMacPac_UpdateCI.Click += new System.EventHandler(this.mnuMacPac_UpdateCI_Click);
            // 
            // mnuMacPac_UpdateDocumentContent
            // 
            this.mnuMacPac_UpdateDocumentContent.Name = "mnuMacPac_UpdateDocumentContent";
            this.mnuMacPac_UpdateDocumentContent.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_UpdateDocumentContent.Text = "#Update Document Content#";
            this.mnuMacPac_UpdateDocumentContent.Click += new System.EventHandler(this.mnuMacPac_UpdateDocumentContent_Click);
            // 
            // mnuMacPac_Sep1
            // 
            this.mnuMacPac_Sep1.Name = "mnuMacPac_Sep1";
            this.mnuMacPac_Sep1.Size = new System.Drawing.Size(238, 6);
            // 
            // mnuMacPac_ToggleDocTrailer
            // 
            this.mnuMacPac_ToggleDocTrailer.Name = "mnuMacPac_ToggleDocTrailer";
            this.mnuMacPac_ToggleDocTrailer.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_ToggleDocTrailer.Text = "#Document Trailer#";
            this.mnuMacPac_ToggleDocTrailer.Click += new System.EventHandler(this.mnuMacPac_ToggleDocTrailer_Click);
            // 
            // mnuMacPac_ToggleSecTrailer
            // 
            this.mnuMacPac_ToggleSecTrailer.Name = "mnuMacPac_ToggleSecTrailer";
            this.mnuMacPac_ToggleSecTrailer.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_ToggleSecTrailer.Text = "#Section Trailer#";
            this.mnuMacPac_ToggleSecTrailer.Click += new System.EventHandler(this.mnuMacPac_ToggleSecTrailer_Click);
            // 
            // mnuMacPac_Sep2
            // 
            this.mnuMacPac_Sep2.Name = "mnuMacPac_Sep2";
            this.mnuMacPac_Sep2.Size = new System.Drawing.Size(238, 6);
            // 
            // mnuMacPac_ManagePeople
            // 
            this.mnuMacPac_ManagePeople.Name = "mnuMacPac_ManagePeople";
            this.mnuMacPac_ManagePeople.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_ManagePeople.Text = "#Manage People#";
            this.mnuMacPac_ManagePeople.Click += new System.EventHandler(this.mnuMacPac_ManagePeople_Click);
            // 
            // mnuMacPac_AppSettings
            // 
            this.mnuMacPac_AppSettings.Name = "mnuMacPac_AppSettings";
            this.mnuMacPac_AppSettings.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_AppSettings.Text = "#Application Settings...#";
            this.mnuMacPac_AppSettings.Click += new System.EventHandler(this.mnuMacPac_AppSettings_Click);
            // 
            // mnuMacPac_ChangeUser
            // 
            this.mnuMacPac_ChangeUser.Name = "mnuMacPac_ChangeUser";
            this.mnuMacPac_ChangeUser.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_ChangeUser.Text = "#Change User#";
            this.mnuMacPac_ChangeUser.DropDownOpening += new System.EventHandler(this.mnuMacPac_ChangeUser_DropDownOpening);
            // 
            // mnuMacPac_EditAuthorPrefs
            // 
            this.mnuMacPac_EditAuthorPrefs.Name = "mnuMacPac_EditAuthorPrefs";
            this.mnuMacPac_EditAuthorPrefs.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_EditAuthorPrefs.Text = "#EditAuthorPreferences#";
            this.mnuMacPac_EditAuthorPrefs.Click += new System.EventHandler(this.mnuMacPac_EditAuthorPrefs_Click);
            // 
            // mnuMacPac_Sep3
            // 
            this.mnuMacPac_Sep3.Name = "mnuMacPac_Sep3";
            this.mnuMacPac_Sep3.Size = new System.Drawing.Size(238, 6);
            // 
            // mnuMacPac_RecreateLegacyContent
            // 
            this.mnuMacPac_RecreateLegacyContent.Name = "mnuMacPac_RecreateLegacyContent";
            this.mnuMacPac_RecreateLegacyContent.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_RecreateLegacyContent.Text = "#Recreate Legacy Document...#";
            this.mnuMacPac_RecreateLegacyContent.Click += new System.EventHandler(this.mnuMacPac_RecreateLegacyContent_Click);
            // 
            // mnuMacPac_SaveLegacyData
            // 
            this.mnuMacPac_SaveLegacyData.Name = "mnuMacPac_SaveLegacyData";
            this.mnuMacPac_SaveLegacyData.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_SaveLegacyData.Text = "#Save Legacy Data...#";
            this.mnuMacPac_SaveLegacyData.Click += new System.EventHandler(this.mnuMacPac_SaveLegacyData_Click);
            // 
            // mnuMacPac_Sep7
            // 
            this.mnuMacPac_Sep7.Name = "mnuMacPac_Sep7";
            this.mnuMacPac_Sep7.Size = new System.Drawing.Size(238, 6);
            // 
            // mnuMacPac_RefreshTree
            // 
            this.mnuMacPac_RefreshTree.Name = "mnuMacPac_RefreshTree";
            this.mnuMacPac_RefreshTree.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_RefreshTree.Text = "#Refresh Tree#";
            this.mnuMacPac_RefreshTree.Click += new System.EventHandler(this.mnuMacPac_RefreshTree_Click);
            // 
            // mnuMacPac_PauseResume
            // 
            this.mnuMacPac_PauseResume.Name = "mnuMacPac_PauseResume";
            this.mnuMacPac_PauseResume.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_PauseResume.Text = "#Pause Resume#";
            this.mnuMacPac_PauseResume.Click += new System.EventHandler(this.mnuMacPac_PauseResume_Click);
            // 
            // mnuMacPac_ShowKeyboardShortcuts
            // 
            this.mnuMacPac_ShowKeyboardShortcuts.Name = "mnuMacPac_ShowKeyboardShortcuts";
            this.mnuMacPac_ShowKeyboardShortcuts.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_ShowKeyboardShortcuts.Text = "#Show Keyboard Shortcuts#";
            this.mnuMacPac_ShowKeyboardShortcuts.Click += new System.EventHandler(this.mnuMacPac_ShowKeyboardShortcuts_Click);
            // 
            // mnuMacPac_Sep5
            // 
            this.mnuMacPac_Sep5.Name = "mnuMacPac_Sep5";
            this.mnuMacPac_Sep5.Size = new System.Drawing.Size(238, 6);
            // 
            // mnuMacPac_ImportSegment
            // 
            this.mnuMacPac_ImportSegment.Name = "mnuMacPac_ImportSegment";
            this.mnuMacPac_ImportSegment.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_ImportSegment.Text = "#Pause Resume#";
            this.mnuMacPac_ImportSegment.Click += new System.EventHandler(this.mnuMacPac_ImportSegment_Click);
            // 
            // mnuMacPac_ShowAdministrator
            // 
            this.mnuMacPac_ShowAdministrator.Name = "mnuMacPac_ShowAdministrator";
            this.mnuMacPac_ShowAdministrator.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_ShowAdministrator.Text = "#Show Administrator#";
            this.mnuMacPac_ShowAdministrator.Click += new System.EventHandler(this.mnuMacPac_ShowAdministrator_Click);
            // 
            // mnuMacPac_Sep6
            // 
            this.mnuMacPac_Sep6.Name = "mnuMacPac_Sep6";
            this.mnuMacPac_Sep6.Size = new System.Drawing.Size(238, 6);
            // 
            // mnuMacPac_ShowAbout
            // 
            this.mnuMacPac_ShowAbout.Name = "mnuMacPac_ShowAbout";
            this.mnuMacPac_ShowAbout.Size = new System.Drawing.Size(241, 22);
            this.mnuMacPac_ShowAbout.Text = "#About#";
            this.mnuMacPac_ShowAbout.Click += new System.EventHandler(this.mnuMacPac_ShowAbout_Click);
            // 
            // mnuMacPac_Sep4
            // 
            this.mnuMacPac_Sep4.Name = "mnuMacPac_Sep4";
            this.mnuMacPac_Sep4.Size = new System.Drawing.Size(242, 6);
            // 
            // TaskPane
            // 
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.Controls.Add(this.picMenu);
            this.Controls.Add(this.tabMain);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "TaskPane";
            this.Size = new System.Drawing.Size(312, 720);
            this.Load += new System.EventHandler(this.TaskPane_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TaskPane_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).EndInit();
            this.tabMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMenu)).EndInit();
            this.mnuMacPac.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        void mnuMacPac_ImportSegment_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        #endregion

        private LMP.Controls.TimedContextMenuStrip mnuMacPac;
        private System.Windows.Forms.ToolStripMenuItem mnuMacPac_ManagePeople;
        private System.Windows.Forms.ToolStripMenuItem mnuMacPac_EditAuthorPrefs;
        private System.Windows.Forms.ToolStripMenuItem mnuMacPac_AppSettings;
        private System.Windows.Forms.ToolStripMenuItem mnuMacPac_RefreshTree;
        private System.Windows.Forms.ToolStripMenuItem mnuMacPac_ChangeUser;
        private ToolStripMenuItem mnuMacPac_PasteSegment;
        private ToolStripMenuItem mnuMacPac_ShowKeyboardShortcuts;
        private ToolStripMenuItem mnuMacPac_UpdateCI;
        private DesignerLibraryManager ucVariableImportManager;
        private ToolStripMenuItem mnuMacPac_FavoriteSegments;
        private ToolStripMenuItem FavoriteSegmentPlaceHolder;
        private ToolStripSeparator mnuMacPac_Sep1;
        private ToolStripSeparator mnuMacPac_Sep3;
        private ToolStripMenuItem mnuMacPac_RecreateLegacyContent;
        private ToolStripMenuItem mnuMacPac_SaveLegacyData;
        private ToolStripSeparator mnuMacPac_Sep4;
        private ToolStripSeparator mnuMacPac_Sep5;
        private ToolStripMenuItem mnuMacPac_ShowAdministrator;
        private ToolStripSeparator mnuMacPac_Sep6;
        private ToolStripMenuItem mnuMacPac_ShowAbout;
        private ToolStripSeparator mnuMacPac_Sep2;
        private ToolStripMenuItem mnuMacPac_ToggleDocTrailer;
        private ToolStripMenuItem mnuMacPac_ToggleSecTrailer;
        private ToolStripMenuItem mnuMacPac_UpdateDocumentContent;
        private ToolStripMenuItem mnuMacPac_ConvertToLanguageSupport;
        private ToolStripSeparator mnuMacPac_Sep7;
        private ToolStripMenuItem mnuMacPac_PauseResume;
        private ToolStripMenuItem mnuMacPac_ImportSegment;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl pageReview;

    }
}
