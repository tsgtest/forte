namespace LMP.MacPac
{
    partial class StylesManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblAvailableStyles = new System.Windows.Forms.Label();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnDeselectAll = new System.Windows.Forms.Button();
            this.lblCopyTo = new System.Windows.Forms.Label();
            this.lbStyles = new System.Windows.Forms.CheckedListBox();
            this.lblProgress = new System.Windows.Forms.Label();
            this.ftvSegments = new LMP.Controls.FolderTreeView(this.components);
            this.cmbCopyTo = new LMP.Controls.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ftvSegments)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.Location = new System.Drawing.Point(293, 337);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(83, 25);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(382, 337);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 25);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblAvailableStyles
            // 
            this.lblAvailableStyles.AutoSize = true;
            this.lblAvailableStyles.BackColor = System.Drawing.Color.Transparent;
            this.lblAvailableStyles.Location = new System.Drawing.Point(14, 12);
            this.lblAvailableStyles.Name = "lblAvailableStyles";
            this.lblAvailableStyles.Size = new System.Drawing.Size(86, 15);
            this.lblAvailableStyles.TabIndex = 0;
            this.lblAvailableStyles.Text = "Available &Styles:";
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectAll.AutoSize = true;
            this.btnSelectAll.Location = new System.Drawing.Point(14, 337);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(83, 25);
            this.btnSelectAll.TabIndex = 7;
            this.btnSelectAll.TabStop = false;
            this.btnSelectAll.Text = "Select &All";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnDeselectAll
            // 
            this.btnDeselectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeselectAll.AutoSize = true;
            this.btnDeselectAll.Location = new System.Drawing.Point(105, 337);
            this.btnDeselectAll.Name = "btnDeselectAll";
            this.btnDeselectAll.Size = new System.Drawing.Size(83, 25);
            this.btnDeselectAll.TabIndex = 8;
            this.btnDeselectAll.TabStop = false;
            this.btnDeselectAll.Text = "D&eselect All";
            this.btnDeselectAll.UseVisualStyleBackColor = true;
            this.btnDeselectAll.Click += new System.EventHandler(this.btnDeselectAll_Click);
            // 
            // lblCopyTo
            // 
            this.lblCopyTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCopyTo.AutoSize = true;
            this.lblCopyTo.BackColor = System.Drawing.Color.Transparent;
            this.lblCopyTo.Location = new System.Drawing.Point(234, 13);
            this.lblCopyTo.Name = "lblCopyTo";
            this.lblCopyTo.Size = new System.Drawing.Size(52, 15);
            this.lblCopyTo.TabIndex = 2;
            this.lblCopyTo.Text = "&Copy To:";
            // 
            // lbStyles
            // 
            this.lbStyles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbStyles.CheckOnClick = true;
            this.lbStyles.FormattingEnabled = true;
            this.lbStyles.Location = new System.Drawing.Point(14, 30);
            this.lbStyles.Name = "lbStyles";
            this.lbStyles.Size = new System.Drawing.Size(197, 293);
            this.lbStyles.Sorted = true;
            this.lbStyles.TabIndex = 1;
            // 
            // lblProgress
            // 
            this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblProgress.Location = new System.Drawing.Point(131, 140);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(196, 53);
            this.lblProgress.TabIndex = 9;
            this.lblProgress.Text = "Copying Styles";
            this.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ftvSegments
            // 
            this.ftvSegments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.White;
            this.ftvSegments.Appearance = appearance1;
            this.ftvSegments.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.ftvSegments.DisableExcludedTypes = false;
            this.ftvSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders;
            this.ftvSegments.ExcludeNonMacPacTypes = false;
            this.ftvSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvSegments.FilterText = "";
            this.ftvSegments.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ftvSegments.HideSelection = false;
            this.ftvSegments.IsDirty = false;
            this.ftvSegments.Location = new System.Drawing.Point(237, 62);
            this.ftvSegments.Mode = LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FolderTree;
            this.ftvSegments.Name = "ftvSegments";
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.ftvSegments.Override = _override1;
            this.ftvSegments.OwnerID = 0;
            this.ftvSegments.ShowCheckboxes = false;
            this.ftvSegments.ShowFindPaths = true;
            this.ftvSegments.ShowTopUserFolderNode = false;
            this.ftvSegments.Size = new System.Drawing.Size(228, 264);
            this.ftvSegments.TabIndex = 4;
            this.ftvSegments.TransparentBackground = false;
            // 
            // cmbCopyTo
            // 
            this.cmbCopyTo.AllowEmptyValue = false;
            this.cmbCopyTo.AutoSize = true;
            this.cmbCopyTo.Borderless = false;
            this.cmbCopyTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbCopyTo.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCopyTo.IsDirty = false;
            this.cmbCopyTo.LimitToList = true;
            this.cmbCopyTo.ListName = "";
            this.cmbCopyTo.Location = new System.Drawing.Point(237, 30);
            this.cmbCopyTo.MaxDropDownItems = 8;
            this.cmbCopyTo.Name = "cmbCopyTo";
            this.cmbCopyTo.SelectedIndex = -1;
            this.cmbCopyTo.SelectedValue = null;
            this.cmbCopyTo.SelectionLength = 0;
            this.cmbCopyTo.SelectionStart = 0;
            this.cmbCopyTo.Size = new System.Drawing.Size(228, 26);
            this.cmbCopyTo.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbCopyTo.SupportingValues = "";
            this.cmbCopyTo.TabIndex = 3;
            this.cmbCopyTo.Tag2 = null;
            this.cmbCopyTo.Value = "";
            this.cmbCopyTo.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbCopyTo_ValueChanged);
            // 
            // StylesManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(479, 373);
            this.Controls.Add(this.cmbCopyTo);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.ftvSegments);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnDeselectAll);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lbStyles);
            this.Controls.Add(this.lblAvailableStyles);
            this.Controls.Add(this.lblCopyTo);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StylesManager";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Copy Styles";
            this.Shown += new System.EventHandler(this.StylesManager_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ftvSegments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblAvailableStyles;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnDeselectAll;
        private LMP.Controls.FolderTreeView ftvSegments;
        private System.Windows.Forms.Label lblCopyTo;
        private System.Windows.Forms.CheckedListBox lbStyles;
        private System.Windows.Forms.Label lblProgress;
        private LMP.Controls.ComboBox cmbCopyTo;
    }
}