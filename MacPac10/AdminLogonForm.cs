using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    internal partial class AdminLogonForm : Form
    {
        public enum LoginChoices
        {
            User = 1,
            Administrator = 2
        }

        public AdminLogonForm()
        {
            InitializeComponent();
        }

        public LoginChoices LoginChoice
        {
            get
            {
                if (optAdministrator.Checked)
                    return LoginChoices.Administrator;
                else
                    return LoginChoices.User;
            }
            set
            {
                optAdministrator.Checked = (value == LoginChoices.Administrator);
                optUser.Checked = (value != LoginChoices.Administrator);
            }
        }

        private void LoginAsForm_Load(object sender, EventArgs e)
        {
        }

        private void pnlBorder_Paint(object sender, PaintEventArgs e)
        {
            Pen oP = new Pen(Brushes.LightGray,2);
            Rectangle oR = this.pnlBorder.ClientRectangle;
            oR.Inflate(-1, -1);
            e.Graphics.DrawRectangle(oP, oR);
        }
    }
}