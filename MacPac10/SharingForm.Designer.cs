namespace LMP.MacPac
{
    partial class SharingForm    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            this.folderTreeView1 = new LMP.Controls.FolderTreeView(this.components);
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lstPeople = new LMP.Controls.PeopleListBox(this.components);
            this.chkSharedFolder = new System.Windows.Forms.CheckBox();
            this.chkOwnerFolder = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.folderTreeView1)).BeginInit();
            this.SuspendLayout();
            // 
            // folderTreeView1
            // 
            this.folderTreeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            appearance1.BackColor = System.Drawing.Color.White;
            this.folderTreeView1.Appearance = appearance1;
            this.folderTreeView1.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.folderTreeView1.DisableExcludedTypes = false;
            this.folderTreeView1.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders;
            this.folderTreeView1.ExcludeNonMacPacTypes = false;
            this.folderTreeView1.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.folderTreeView1.FilterText = "";
            this.folderTreeView1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.folderTreeView1.HideSelection = false;
            this.folderTreeView1.IsDirty = false;
            this.folderTreeView1.Location = new System.Drawing.Point(12, 36);
            this.folderTreeView1.Mode = LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FolderTree;
            this.folderTreeView1.Name = "folderTreeView1";
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.folderTreeView1.Override = _override1;
            this.folderTreeView1.OwnerID = 0;
            this.folderTreeView1.ShowCheckboxes = false;
            this.folderTreeView1.ShowFindPaths = true;
            this.folderTreeView1.ShowTopUserFolderNode = false;
            this.folderTreeView1.Size = new System.Drawing.Size(221, 264);
            this.folderTreeView1.TabIndex = 1;
            this.folderTreeView1.TransparentBackground = false;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.Location = new System.Drawing.Point(304, 306);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(83, 25);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(393, 306);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 25);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lstPeople
            // 
            this.lstPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstPeople.FilterOption = LMP.Controls.PeopleListBox.mpPeopleFilterOptions.Users;
            this.lstPeople.ListSource = LMP.Controls.PeopleListBox.mpPeopleListSources.Local;
            this.lstPeople.Location = new System.Drawing.Point(248, 36);
            this.lstPeople.MultipleSelection = false;
            this.lstPeople.Name = "lstPeople";
            this.lstPeople.Size = new System.Drawing.Size(229, 264);
            this.lstPeople.TabIndex = 3;
            this.lstPeople.Value = "";
            this.lstPeople.WhereClause = "";
            // 
            // chkSharedFolder
            // 
            this.chkSharedFolder.AutoSize = true;
            this.chkSharedFolder.Location = new System.Drawing.Point(13, 14);
            this.chkSharedFolder.Name = "chkSharedFolder";
            this.chkSharedFolder.Size = new System.Drawing.Size(95, 19);
            this.chkSharedFolder.TabIndex = 0;
            this.chkSharedFolder.Text = "&Shared Folder";
            this.chkSharedFolder.UseVisualStyleBackColor = true;
            this.chkSharedFolder.CheckedChanged += new System.EventHandler(this.chkSharedFolder_CheckedChanged);
            // 
            // chkOwnerFolder
            // 
            this.chkOwnerFolder.AutoSize = true;
            this.chkOwnerFolder.Location = new System.Drawing.Point(248, 14);
            this.chkOwnerFolder.Name = "chkOwnerFolder";
            this.chkOwnerFolder.Size = new System.Drawing.Size(93, 19);
            this.chkOwnerFolder.TabIndex = 2;
            this.chkOwnerFolder.Text = "&Owner Folder";
            this.chkOwnerFolder.UseVisualStyleBackColor = true;
            this.chkOwnerFolder.CheckedChanged += new System.EventHandler(this.chkOwnerFolder_CheckedChanged);
            // 
            // SharingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(490, 344);
            this.Controls.Add(this.chkOwnerFolder);
            this.Controls.Add(this.chkSharedFolder);
            this.Controls.Add(this.lstPeople);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.folderTreeView1);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SharingForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Share Content";
            this.Load += new System.EventHandler(this.SharingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.folderTreeView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LMP.Controls.FolderTreeView folderTreeView1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private LMP.Controls.PeopleListBox lstPeople;
        private System.Windows.Forms.CheckBox chkSharedFolder;
        private System.Windows.Forms.CheckBox chkOwnerFolder;
    }
}