using System;
using LMP.Controls;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;

namespace LMP.MacPac
{
    /// <summary>
    /// Summary description for ContentTab.
    /// </summary>
    public partial class ContentManager : System.Windows.Forms.UserControl
    {
        private System.Windows.Forms.Panel pnlContentDescription;
        private System.Windows.Forms.Splitter splitContent;
        private System.Windows.Forms.Panel pnlContentTree;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFindContent;
        private System.Windows.Forms.Label lblFindContent;
        private Infragistics.Win.UltraWinTree.UltraTree treeContent;
        private IContainer components;
        private ImageList ilImages;
        private TimedContextMenuStrip mnuSegment;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContentManager));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override2 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            this.pnlContentDescription = new System.Windows.Forms.Panel();
            this.wbDescription = new System.Windows.Forms.WebBrowser();
            this.mnuQuickHelp = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuQuickHelp_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuQuickHelp_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuQuickHelp_PrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQuickHelp_Print = new System.Windows.Forms.ToolStripMenuItem();
            this.lblStatus = new System.Windows.Forms.Label();
            this.splitContent = new System.Windows.Forms.Splitter();
            this.pnlContentTree = new System.Windows.Forms.Panel();
            this.btnFindContentAdvanced = new System.Windows.Forms.Button();
            this.picMenuDropdown = new System.Windows.Forms.PictureBox();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.chkFindContent = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.treeContent = new Infragistics.Win.UltraWinTree.UltraTree();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.txtFindContent = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.mnuSearch = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuSearch_Clear = new System.Windows.Forms.ToolStripMenuItem();
            this.lblFindContent = new System.Windows.Forms.Label();
            this.treeResults = new Infragistics.Win.UltraWinTree.UltraTree();
            this.tipMain = new System.Windows.Forms.ToolTip(this.components);
            this.mnuSegment = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuSegment_InsertUsingSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertUsingTransientPrefill = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertUsingSavedPrefill = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertUsingClientMatterLookup = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertMultipleusingSavedData = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_AttachTo = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSegment_CreateNewDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_CreateExternalContent = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Insert = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertAtSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertAtSelectionAsPlainText = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertStartOfDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertEndOfDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertStartOfThisSection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertEndOfThisSection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertStartOfAllSections = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_InsertEndOfAllSections = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_ApplyStyles = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_CreateMasterData = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSegment_SelectionAsBody = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_SeparateSection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_RestartPageNumbering = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_KeepHeadersFooters = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSegment_ManageAuthorPreferences = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_CopyStyles = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_StyleSummary = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Sep4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSegment_EditAnswerFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Clone = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_AddToFavorites = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_RemoveFromFavorites = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_ConvertToAdminContent = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_UpdateAdminContent = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Share = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Sep5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSegment_EditSavedData = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Design = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Export = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_SaveAsUserSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_ShowAssignments = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_Properties = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegment_HelpSep = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSegment_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuFolder_CreateNewSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewSegmentOptions = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuNewSegment_Blank = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewSegment_Selection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewSegment_Document = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewSegment_StyleSheet = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewSegment_AnswerFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewSegment_SegmentPacket = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewSegment_MasterDataForm = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_CreateNewDesigner = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_AddFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_ImportSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFolder_CreateNewFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Properties = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_SetPermissions = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFolder_ApplyStyles = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_SaveStyles = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFolder_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Sep4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFolder_Default = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_SelectDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_RemoveDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Sep5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFolder_MoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_MoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Sep6 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFolder_CollapseAllSubfolders = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolder_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFolderHelpSep = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFolder_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlContentDescription.SuspendLayout();
            this.mnuQuickHelp.SuspendLayout();
            this.pnlContentTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMenuDropdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFindContent)).BeginInit();
            this.mnuSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeResults)).BeginInit();
            this.mnuSegment.SuspendLayout();
            this.mnuFolder.SuspendLayout();
            this.mnuNewSegmentOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlContentDescription
            // 
            this.pnlContentDescription.BackColor = System.Drawing.Color.Transparent;
            this.pnlContentDescription.Controls.Add(this.wbDescription);
            this.pnlContentDescription.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlContentDescription.Location = new System.Drawing.Point(0, 453);
            this.pnlContentDescription.Name = "pnlContentDescription";
            this.pnlContentDescription.Size = new System.Drawing.Size(328, 75);
            this.pnlContentDescription.TabIndex = 29;
            // 
            // wbDescription
            // 
            this.wbDescription.AllowWebBrowserDrop = false;
            this.wbDescription.CausesValidation = false;
            this.wbDescription.ContextMenuStrip = this.mnuQuickHelp;
            this.wbDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbDescription.IsWebBrowserContextMenuEnabled = false;
            this.wbDescription.Location = new System.Drawing.Point(0, 0);
            this.wbDescription.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbDescription.Name = "wbDescription";
            this.wbDescription.ScriptErrorsSuppressed = true;
            this.wbDescription.Size = new System.Drawing.Size(328, 75);
            this.wbDescription.TabIndex = 7;
            this.wbDescription.TabStop = false;
            this.tipMain.SetToolTip(this.wbDescription, "dan fisherman");
            this.wbDescription.WebBrowserShortcutsEnabled = false;
            this.wbDescription.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.wbDescription_PreviewKeyDown);
            // 
            // mnuQuickHelp
            // 
            this.mnuQuickHelp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuQuickHelp_Copy,
            this.toolStripMenuItem2,
            this.mnuQuickHelp_SaveAs,
            this.toolStripMenuItem1,
            this.mnuQuickHelp_PrintPreview,
            this.mnuQuickHelp_Print});
            this.mnuQuickHelp.Name = "mnuQuickHelp";
            this.mnuQuickHelp.Size = new System.Drawing.Size(180, 104);
            this.mnuQuickHelp.TimerInterval = 500;
            this.mnuQuickHelp.UseTimer = true;
            // 
            // mnuQuickHelp_Copy
            // 
            this.mnuQuickHelp_Copy.Name = "mnuQuickHelp_Copy";
            this.mnuQuickHelp_Copy.ShortcutKeyDisplayString = "";
            this.mnuQuickHelp_Copy.ShowShortcutKeys = false;
            this.mnuQuickHelp_Copy.Size = new System.Drawing.Size(179, 22);
            this.mnuQuickHelp_Copy.Text = "&Copy";
            this.mnuQuickHelp_Copy.Click += new System.EventHandler(this.mnuQuickHelp_Copy_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(176, 6);
            // 
            // mnuQuickHelp_SaveAs
            // 
            this.mnuQuickHelp_SaveAs.Name = "mnuQuickHelp_SaveAs";
            this.mnuQuickHelp_SaveAs.Size = new System.Drawing.Size(179, 22);
            this.mnuQuickHelp_SaveAs.Text = "Save As &Web Page...";
            this.mnuQuickHelp_SaveAs.Click += new System.EventHandler(this.mnuQuickHelp_SaveAs_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(176, 6);
            // 
            // mnuQuickHelp_PrintPreview
            // 
            this.mnuQuickHelp_PrintPreview.Name = "mnuQuickHelp_PrintPreview";
            this.mnuQuickHelp_PrintPreview.Size = new System.Drawing.Size(179, 22);
            this.mnuQuickHelp_PrintPreview.Text = "Print Pre&view...";
            this.mnuQuickHelp_PrintPreview.Click += new System.EventHandler(this.mnuQuickHelp_PrintPreview_Click);
            // 
            // mnuQuickHelp_Print
            // 
            this.mnuQuickHelp_Print.Name = "mnuQuickHelp_Print";
            this.mnuQuickHelp_Print.Size = new System.Drawing.Size(179, 22);
            this.mnuQuickHelp_Print.Text = "&Print...";
            this.mnuQuickHelp_Print.Click += new System.EventHandler(this.mnuQuickHelp_Print_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(5, 432);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(301, 14);
            this.lblStatus.TabIndex = 45;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // splitContent
            // 
            this.splitContent.BackColor = System.Drawing.Color.SlateGray;
            this.splitContent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContent.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitContent.Location = new System.Drawing.Point(0, 448);
            this.splitContent.Name = "splitContent";
            this.splitContent.Size = new System.Drawing.Size(328, 5);
            this.splitContent.TabIndex = 30;
            this.splitContent.TabStop = false;
            this.splitContent.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContent_SplitterMoved);
            // 
            // pnlContentTree
            // 
            this.pnlContentTree.BackColor = System.Drawing.Color.Transparent;
            this.pnlContentTree.Controls.Add(this.btnFindContentAdvanced);
            this.pnlContentTree.Controls.Add(this.picMenuDropdown);
            this.pnlContentTree.Controls.Add(this.picHelp);
            this.pnlContentTree.Controls.Add(this.lblStatus);
            this.pnlContentTree.Controls.Add(this.chkFindContent);
            this.pnlContentTree.Controls.Add(this.treeContent);
            this.pnlContentTree.Controls.Add(this.txtFindContent);
            this.pnlContentTree.Controls.Add(this.lblFindContent);
            this.pnlContentTree.Controls.Add(this.treeResults);
            this.pnlContentTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContentTree.Location = new System.Drawing.Point(0, 0);
            this.pnlContentTree.Name = "pnlContentTree";
            this.pnlContentTree.Size = new System.Drawing.Size(328, 448);
            this.pnlContentTree.TabIndex = 31;
            this.pnlContentTree.Resize += new System.EventHandler(this.pnlContentTree_Resize);
            // 
            // btnFindContentAdvanced
            // 
            this.btnFindContentAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindContentAdvanced.FlatAppearance.BorderSize = 0;
            this.btnFindContentAdvanced.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFindContentAdvanced.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindContentAdvanced.Image = ((System.Drawing.Image)(resources.GetObject("btnFindContentAdvanced.Image")));
            this.btnFindContentAdvanced.Location = new System.Drawing.Point(304, 5);
            this.btnFindContentAdvanced.Margin = new System.Windows.Forms.Padding(0);
            this.btnFindContentAdvanced.Name = "btnFindContentAdvanced";
            this.btnFindContentAdvanced.Size = new System.Drawing.Size(21, 23);
            this.btnFindContentAdvanced.TabIndex = 4;
            this.btnFindContentAdvanced.Text = "�";
            this.tipMain.SetToolTip(this.btnFindContentAdvanced, "Advanced find options..");
            this.btnFindContentAdvanced.UseVisualStyleBackColor = true;
            this.btnFindContentAdvanced.Click += new System.EventHandler(this.btnFindContentAdvanced_Click);
            // 
            // picMenuDropdown
            // 
            this.picMenuDropdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picMenuDropdown.Image = ((System.Drawing.Image)(resources.GetObject("picMenuDropdown.Image")));
            this.picMenuDropdown.InitialImage = ((System.Drawing.Image)(resources.GetObject("picMenuDropdown.InitialImage")));
            this.picMenuDropdown.Location = new System.Drawing.Point(307, 43);
            this.picMenuDropdown.Name = "picMenuDropdown";
            this.picMenuDropdown.Size = new System.Drawing.Size(11, 11);
            this.picMenuDropdown.TabIndex = 50;
            this.picMenuDropdown.TabStop = false;
            this.picMenuDropdown.Click += new System.EventHandler(this.picMenu_Click);
            // 
            // picHelp
            // 
            this.picHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.picHelp.Image = ((System.Drawing.Image)(resources.GetObject("picHelp.Image")));
            this.picHelp.Location = new System.Drawing.Point(310, 431);
            this.picHelp.Name = "picHelp";
            this.picHelp.Size = new System.Drawing.Size(15, 15);
            this.picHelp.TabIndex = 48;
            this.picHelp.TabStop = false;
            this.picHelp.Click += new System.EventHandler(this.picHelp_Click);
            // 
            // chkFindContent
            // 
            this.chkFindContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.chkFindContent.Appearance = appearance1;
            this.chkFindContent.BackColor = System.Drawing.Color.Transparent;
            this.chkFindContent.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.chkFindContent.ImageTransparentColor = System.Drawing.Color.Black;
            this.chkFindContent.Location = new System.Drawing.Point(285, 7);
            this.chkFindContent.Name = "chkFindContent";
            this.chkFindContent.Size = new System.Drawing.Size(17, 22);
            this.chkFindContent.Style = Infragistics.Win.EditCheckStyle.Custom;
            this.chkFindContent.TabIndex = 3;
            this.tipMain.SetToolTip(this.chkFindContent, "Find matching content");
            this.chkFindContent.CheckedChanged += new System.EventHandler(this.chkFindContent_CheckedChanged);
            // 
            // treeContent
            // 
            this.treeContent.AllowDrop = true;
            this.treeContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.BackColor2 = System.Drawing.Color.White;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.BackwardDiagonal;
            this.treeContent.Appearance = appearance2;
            this.treeContent.AutoDragExpandDelay = 1000;
            this.treeContent.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.treeContent.HideSelection = false;
            this.treeContent.ImageList = this.ilImages;
            this.treeContent.Location = new System.Drawing.Point(1, 30);
            this.treeContent.Name = "treeContent";
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BorderColor = System.Drawing.Color.White;
            _override1.LabelEditAppearance = appearance3;
            _override1.SelectionType = Infragistics.Win.UltraWinTree.SelectType.SingleAutoDrag;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.treeContent.Override = _override1;
            this.treeContent.ScrollBounds = Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill;
            this.treeContent.Size = new System.Drawing.Size(326, 398);
            this.treeContent.TabIndex = 0;
            this.treeContent.ViewStyle = Infragistics.Win.UltraWinTree.ViewStyle.Standard;
            this.treeContent.AfterActivate += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeContent_AfterActivate);
            this.treeContent.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeContent_AfterSelect);
            this.treeContent.BeforeCollapse += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeContent_BeforeCollapse);
            this.treeContent.BeforeLabelEdit += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeContent_BeforeLabelEdit);
            this.treeContent.BeforeExpand += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeContent_BeforeExpand);
            this.treeContent.Scroll += new Infragistics.Win.UltraWinTree.TreeScrollEventHandler(this.treeContent_Scroll);
            this.treeContent.ValidateLabelEdit += new Infragistics.Win.UltraWinTree.ValidateLabelEditEventHandler(this.treeContent_ValidateLabelEdit);
            this.treeContent.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeContent_DragDrop);
            this.treeContent.DragOver += new System.Windows.Forms.DragEventHandler(this.treeContent_DragOver);
            this.treeContent.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.treeContent_GiveFeedback);
            this.treeContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeContent_KeyDown);
            this.treeContent.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.treeContent_MouseDoubleClick);
            this.treeContent.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeContent_MouseDown);
            this.treeContent.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeContent_MouseMove);
            this.treeContent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeContent_MouseUp);
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Fuchsia;
            this.ilImages.Images.SetKeyName(0, "ClosedFolder");
            this.ilImages.Images.SetKeyName(1, "OpenFolder");
            this.ilImages.Images.SetKeyName(2, "Document");
            this.ilImages.Images.SetKeyName(3, "Search");
            this.ilImages.Images.SetKeyName(4, "UserPrefill");
            this.ilImages.Images.SetKeyName(5, "ParagraphText");
            this.ilImages.Images.SetKeyName(6, "DocumentComponent");
            this.ilImages.Images.SetKeyName(7, "Stylesheet");
            this.ilImages.Images.SetKeyName(8, "Person");
            this.ilImages.Images.SetKeyName(9, "Bullet");
            this.ilImages.Images.SetKeyName(10, "SegmentPacket");
            this.ilImages.Images.SetKeyName(11, "DataInterview");
            this.ilImages.Images.SetKeyName(12, "WordDocument");
            this.ilImages.Images.SetKeyName(13, "ExternalContent");
            this.ilImages.Images.SetKeyName(14, "XLDocument");
            this.ilImages.Images.SetKeyName(15, "User Document");
            this.ilImages.Images.SetKeyName(16, "User Paragraph Text");
            this.ilImages.Images.SetKeyName(17, "UserStylesheet");
            this.ilImages.Images.SetKeyName(18, "SentenceText");
            this.ilImages.Images.SetKeyName(19, "User SentenceText");
            this.ilImages.Images.SetKeyName(20, "User Component Segment.bmp");
            this.ilImages.Images.SetKeyName(21, "UserSegmentPacket");
            this.ilImages.Images.SetKeyName(22, "MasterData");
            // 
            // txtFindContent
            // 
            this.txtFindContent.AcceptsReturn = true;
            this.txtFindContent.AlwaysInEditMode = true;
            this.txtFindContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.Image = 1;
            appearance4.TextVAlignAsString = "Middle";
            this.txtFindContent.Appearance = appearance4;
            this.txtFindContent.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtFindContent.ContextMenuStrip = this.mnuSearch;
            this.txtFindContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFindContent.Location = new System.Drawing.Point(35, 7);
            this.txtFindContent.Multiline = true;
            this.txtFindContent.Name = "txtFindContent";
            this.txtFindContent.Size = new System.Drawing.Size(244, 20);
            this.txtFindContent.TabIndex = 2;
            this.tipMain.SetToolTip(this.txtFindContent, "Search for Content - Ctrl+F1");
            this.txtFindContent.TextChanged += new System.EventHandler(this.txtFindContent_TextChanged);
            this.txtFindContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFindContent_KeyDown);
            // 
            // mnuSearch
            // 
            this.mnuSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSearch_Clear});
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Size = new System.Drawing.Size(102, 26);
            this.mnuSearch.TimerInterval = 500;
            this.mnuSearch.UseTimer = true;
            // 
            // mnuSearch_Clear
            // 
            this.mnuSearch_Clear.Name = "mnuSearch_Clear";
            this.mnuSearch_Clear.Size = new System.Drawing.Size(101, 22);
            this.mnuSearch_Clear.Text = "&Clear";
            this.mnuSearch_Clear.Click += new System.EventHandler(this.mnuSearch_Clear_Click);
            // 
            // lblFindContent
            // 
            this.lblFindContent.BackColor = System.Drawing.Color.Transparent;
            this.lblFindContent.Location = new System.Drawing.Point(5, 10);
            this.lblFindContent.Name = "lblFindContent";
            this.lblFindContent.Size = new System.Drawing.Size(29, 15);
            this.lblFindContent.TabIndex = 1;
            this.lblFindContent.Text = "Find:";
            // 
            // treeResults
            // 
            this.treeResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.Color.Transparent;
            this.treeResults.Appearance = appearance5;
            this.treeResults.AutoDragExpandDelay = 1000;
            this.treeResults.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.treeResults.HideSelection = false;
            this.treeResults.ImageList = this.ilImages;
            this.treeResults.LeftImagesSize = new System.Drawing.Size(32, 16);
            this.treeResults.Location = new System.Drawing.Point(1, 32);
            this.treeResults.Name = "treeResults";
            _override2.ExpandedNodeAppearance = appearance4;
            _override2.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override2.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override2.Multiline = Infragistics.Win.DefaultableBoolean.False;
            appearance6.Image = 0;
            _override2.NodeAppearance = appearance6;
            _override2.NodeDoubleClickAction = Infragistics.Win.UltraWinTree.NodeDoubleClickAction.None;
            _override2.NodeSpacingAfter = 2;
            _override2.SelectionType = Infragistics.Win.UltraWinTree.SelectType.SingleAutoDrag;
            this.treeResults.Override = _override2;
            this.treeResults.ShowLines = false;
            this.treeResults.ShowRootLines = false;
            this.treeResults.Size = new System.Drawing.Size(324, 394);
            this.treeResults.TabIndex = 3;
            this.treeResults.ViewStyle = Infragistics.Win.UltraWinTree.ViewStyle.Standard;
            this.treeResults.Visible = false;
            this.treeResults.AfterActivate += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeResults_AfterActivate);
            this.treeResults.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeResults_AfterSelect);
            this.treeResults.BeforeActivate += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeResults_BeforeActivate);
            this.treeResults.BeforeSelect += new Infragistics.Win.UltraWinTree.BeforeNodeSelectEventHandler(this.treeResults_BeforeSelect);
            this.treeResults.Scroll += new Infragistics.Win.UltraWinTree.TreeScrollEventHandler(this.treeResults_Scroll);
            this.treeResults.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeResults_DragDrop);
            this.treeResults.DragOver += new System.Windows.Forms.DragEventHandler(this.treeResults_DragOver);
            this.treeResults.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.treeResults_QueryContinueDrag);
            this.treeResults.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeResults_KeyDown);
            this.treeResults.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.treeResults_MouseDoubleClick);
            this.treeResults.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeResults_MouseDown);
            this.treeResults.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeResults_MouseMove);
            this.treeResults.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeResults_MouseUp);
            // 
            // tipMain
            // 
            this.tipMain.IsBalloon = true;
            // 
            // mnuSegment
            // 
            this.mnuSegment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSegment_InsertUsingSelection,
            this.mnuSegment_InsertUsingTransientPrefill,
            this.mnuSegment_InsertUsingSavedPrefill,
            this.mnuSegment_InsertUsingClientMatterLookup,
            this.mnuSegment_InsertMultipleusingSavedData,
            this.mnuSegment_AttachTo,
            this.mnuSegment_Sep2,
            this.mnuSegment_CreateNewDocument,
            this.mnuSegment_CreateExternalContent,
            this.mnuSegment_Insert,
            this.mnuSegment_InsertAtSelection,
            this.mnuSegment_InsertAtSelectionAsPlainText,
            this.mnuSegment_InsertStartOfDocument,
            this.mnuSegment_InsertEndOfDocument,
            this.mnuSegment_InsertStartOfThisSection,
            this.mnuSegment_InsertEndOfThisSection,
            this.mnuSegment_InsertStartOfAllSections,
            this.mnuSegment_InsertEndOfAllSections,
            this.mnuSegment_ApplyStyles,
            this.mnuSegment_CreateMasterData,
            this.mnuSegment_Sep1,
            this.mnuSegment_SelectionAsBody,
            this.mnuSegment_SeparateSection,
            this.mnuSegment_RestartPageNumbering,
            this.mnuSegment_KeepHeadersFooters,
            this.mnuSegment_Sep3,
            this.mnuSegment_ManageAuthorPreferences,
            this.mnuSegment_CopyStyles,
            this.mnuSegment_StyleSummary,
            this.mnuSegment_Sep4,
            this.mnuSegment_EditAnswerFile,
            this.mnuSegment_Clone,
            this.mnuSegment_AddToFavorites,
            this.mnuSegment_RemoveFromFavorites,
            this.mnuSegment_ConvertToAdminContent,
            this.mnuSegment_UpdateAdminContent,
            this.mnuSegment_Copy,
            this.mnuSegment_Delete,
            this.mnuSegment_Share,
            this.mnuSegment_Sep5,
            this.mnuSegment_EditSavedData,
            this.mnuSegment_Design,
            this.mnuSegment_Export,
            this.mnuSegment_SaveAsUserSegment,
            this.mnuSegment_ShowAssignments,
            this.mnuSegment_Properties,
            this.mnuSegment_HelpSep,
            this.mnuSegment_Help});
            this.mnuSegment.Name = "mnuContent";
            this.mnuSegment.ShowCheckMargin = true;
            this.mnuSegment.ShowImageMargin = false;
            this.mnuSegment.Size = new System.Drawing.Size(265, 964);
            this.mnuSegment.TimerInterval = 500;
            this.mnuSegment.UseTimer = true;
            this.mnuSegment.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.mnuSegment_Closed);
            this.mnuSegment.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.mnuSegment_Closing);
            this.mnuSegment.Opening += new System.ComponentModel.CancelEventHandler(this.mnuSegment_Opening);
            this.mnuSegment.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuSegment_ItemClicked);
            // 
            // mnuSegment_InsertUsingSelection
            // 
            this.mnuSegment_InsertUsingSelection.Name = "mnuSegment_InsertUsingSelection";
            this.mnuSegment_InsertUsingSelection.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertUsingSelection.Text = "#Insert using Selection#";
            // 
            // mnuSegment_InsertUsingTransientPrefill
            // 
            this.mnuSegment_InsertUsingTransientPrefill.Name = "mnuSegment_InsertUsingTransientPrefill";
            this.mnuSegment_InsertUsingTransientPrefill.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertUsingTransientPrefill.Text = "#Insert using Transient Prefill#";
            // 
            // mnuSegment_InsertUsingSavedPrefill
            // 
            this.mnuSegment_InsertUsingSavedPrefill.Name = "mnuSegment_InsertUsingSavedPrefill";
            this.mnuSegment_InsertUsingSavedPrefill.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertUsingSavedPrefill.Text = "#Insert using Saved Prefill#";
            // 
            // mnuSegment_InsertUsingClientMatterLookup
            // 
            this.mnuSegment_InsertUsingClientMatterLookup.Name = "mnuSegment_InsertUsingClientMatterLookup";
            this.mnuSegment_InsertUsingClientMatterLookup.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertUsingClientMatterLookup.Text = "#Insert using Client Matter Lookup#";
            // 
            // mnuSegment_InsertMultipleusingSavedData
            // 
            this.mnuSegment_InsertMultipleusingSavedData.Name = "mnuSegment_InsertMultipleusingSavedData";
            this.mnuSegment_InsertMultipleusingSavedData.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertMultipleusingSavedData.Text = "#Insert Multiple using Saved Prefill#";
            // 
            // mnuSegment_AttachTo
            // 
            this.mnuSegment_AttachTo.Name = "mnuSegment_AttachTo";
            this.mnuSegment_AttachTo.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_AttachTo.Text = "#AttachTo#";
            // 
            // mnuSegment_Sep2
            // 
            this.mnuSegment_Sep2.Name = "mnuSegment_Sep2";
            this.mnuSegment_Sep2.Size = new System.Drawing.Size(261, 6);
            // 
            // mnuSegment_CreateNewDocument
            // 
            this.mnuSegment_CreateNewDocument.Name = "mnuSegment_CreateNewDocument";
            this.mnuSegment_CreateNewDocument.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_CreateNewDocument.Text = "#Create New Document#";
            // 
            // mnuSegment_CreateExternalContent
            // 
            this.mnuSegment_CreateExternalContent.Name = "mnuSegment_CreateExternalContent";
            this.mnuSegment_CreateExternalContent.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_CreateExternalContent.Text = "#Create#";
            // 
            // mnuSegment_Insert
            // 
            this.mnuSegment_Insert.Name = "mnuSegment_Insert";
            this.mnuSegment_Insert.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Insert.Text = "#Insert#";
            // 
            // mnuSegment_InsertAtSelection
            // 
            this.mnuSegment_InsertAtSelection.Name = "mnuSegment_InsertAtSelection";
            this.mnuSegment_InsertAtSelection.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertAtSelection.Text = "#Insert at Selection#";
            // 
            // mnuSegment_InsertAtSelectionAsPlainText
            // 
            this.mnuSegment_InsertAtSelectionAsPlainText.Name = "mnuSegment_InsertAtSelectionAsPlainText";
            this.mnuSegment_InsertAtSelectionAsPlainText.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertAtSelectionAsPlainText.Text = "#Insert at Selection With Plain Text#";
            // 
            // mnuSegment_InsertStartOfDocument
            // 
            this.mnuSegment_InsertStartOfDocument.Name = "mnuSegment_InsertStartOfDocument";
            this.mnuSegment_InsertStartOfDocument.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertStartOfDocument.Text = "#Insert at Start of Document#";
            // 
            // mnuSegment_InsertEndOfDocument
            // 
            this.mnuSegment_InsertEndOfDocument.Name = "mnuSegment_InsertEndOfDocument";
            this.mnuSegment_InsertEndOfDocument.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertEndOfDocument.Text = "#Insert at End of Document#";
            // 
            // mnuSegment_InsertStartOfThisSection
            // 
            this.mnuSegment_InsertStartOfThisSection.Name = "mnuSegment_InsertStartOfThisSection";
            this.mnuSegment_InsertStartOfThisSection.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertStartOfThisSection.Text = "#Insert at Start of Current Section#";
            // 
            // mnuSegment_InsertEndOfThisSection
            // 
            this.mnuSegment_InsertEndOfThisSection.Name = "mnuSegment_InsertEndOfThisSection";
            this.mnuSegment_InsertEndOfThisSection.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertEndOfThisSection.Text = "#Insert at End of Current Section#";
            // 
            // mnuSegment_InsertStartOfAllSections
            // 
            this.mnuSegment_InsertStartOfAllSections.Name = "mnuSegment_InsertStartOfAllSections";
            this.mnuSegment_InsertStartOfAllSections.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertStartOfAllSections.Text = "#Insert at Start of All Sections#";
            // 
            // mnuSegment_InsertEndOfAllSections
            // 
            this.mnuSegment_InsertEndOfAllSections.Name = "mnuSegment_InsertEndOfAllSections";
            this.mnuSegment_InsertEndOfAllSections.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_InsertEndOfAllSections.Text = "#Insert at End of All Sections#";
            // 
            // mnuSegment_ApplyStyles
            // 
            this.mnuSegment_ApplyStyles.Name = "mnuSegment_ApplyStyles";
            this.mnuSegment_ApplyStyles.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_ApplyStyles.Text = "#Apply Styles#";
            // 
            // mnuSegment_CreateMasterData
            // 
            this.mnuSegment_CreateMasterData.Name = "mnuSegment_CreateMasterData";
            this.mnuSegment_CreateMasterData.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_CreateMasterData.Text = "#Create Master Data#";
            // 
            // mnuSegment_Sep1
            // 
            this.mnuSegment_Sep1.Name = "mnuSegment_Sep1";
            this.mnuSegment_Sep1.Size = new System.Drawing.Size(261, 6);
            // 
            // mnuSegment_SelectionAsBody
            // 
            this.mnuSegment_SelectionAsBody.CheckOnClick = true;
            this.mnuSegment_SelectionAsBody.Name = "mnuSegment_SelectionAsBody";
            this.mnuSegment_SelectionAsBody.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_SelectionAsBody.Text = "#Insert Selected Text into Body#";
            // 
            // mnuSegment_SeparateSection
            // 
            this.mnuSegment_SeparateSection.CheckOnClick = true;
            this.mnuSegment_SeparateSection.Name = "mnuSegment_SeparateSection";
            this.mnuSegment_SeparateSection.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_SeparateSection.Text = "#In Separate Section#";
            // 
            // mnuSegment_RestartPageNumbering
            // 
            this.mnuSegment_RestartPageNumbering.CheckOnClick = true;
            this.mnuSegment_RestartPageNumbering.Name = "mnuSegment_RestartPageNumbering";
            this.mnuSegment_RestartPageNumbering.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_RestartPageNumbering.Text = "#Restart Page Numbering#";
            // 
            // mnuSegment_KeepHeadersFooters
            // 
            this.mnuSegment_KeepHeadersFooters.CheckOnClick = true;
            this.mnuSegment_KeepHeadersFooters.Name = "mnuSegment_KeepHeadersFooters";
            this.mnuSegment_KeepHeadersFooters.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_KeepHeadersFooters.Text = "#Keep Existing Headers/Footers#";
            // 
            // mnuSegment_Sep3
            // 
            this.mnuSegment_Sep3.Name = "mnuSegment_Sep3";
            this.mnuSegment_Sep3.Size = new System.Drawing.Size(261, 6);
            // 
            // mnuSegment_ManageAuthorPreferences
            // 
            this.mnuSegment_ManageAuthorPreferences.Name = "mnuSegment_ManageAuthorPreferences";
            this.mnuSegment_ManageAuthorPreferences.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_ManageAuthorPreferences.Text = "#Manage Author Preferences#";
            // 
            // mnuSegment_CopyStyles
            // 
            this.mnuSegment_CopyStyles.Name = "mnuSegment_CopyStyles";
            this.mnuSegment_CopyStyles.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_CopyStyles.Text = "#Copy Styles#";
            // 
            // mnuSegment_StyleSummary
            // 
            this.mnuSegment_StyleSummary.Name = "mnuSegment_StyleSummary";
            this.mnuSegment_StyleSummary.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_StyleSummary.Text = "#Style Summary#";
            // 
            // mnuSegment_Sep4
            // 
            this.mnuSegment_Sep4.Name = "mnuSegment_Sep4";
            this.mnuSegment_Sep4.Size = new System.Drawing.Size(261, 6);
            // 
            // mnuSegment_EditAnswerFile
            // 
            this.mnuSegment_EditAnswerFile.Name = "mnuSegment_EditAnswerFile";
            this.mnuSegment_EditAnswerFile.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_EditAnswerFile.Text = "#Edit Answer File#";
            // 
            // mnuSegment_Clone
            // 
            this.mnuSegment_Clone.Name = "mnuSegment_Clone";
            this.mnuSegment_Clone.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Clone.Text = "#Clone#";
            // 
            // mnuSegment_AddToFavorites
            // 
            this.mnuSegment_AddToFavorites.Name = "mnuSegment_AddToFavorites";
            this.mnuSegment_AddToFavorites.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_AddToFavorites.Text = "#Add To Favorites#";
            // 
            // mnuSegment_RemoveFromFavorites
            // 
            this.mnuSegment_RemoveFromFavorites.Name = "mnuSegment_RemoveFromFavorites";
            this.mnuSegment_RemoveFromFavorites.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_RemoveFromFavorites.Text = "#Remove From Favorites#";
            // 
            // mnuSegment_ConvertToAdminContent
            // 
            this.mnuSegment_ConvertToAdminContent.Name = "mnuSegment_ConvertToAdminContent";
            this.mnuSegment_ConvertToAdminContent.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_ConvertToAdminContent.Text = "#Convert To Admin Content#";
            // 
            // mnuSegment_UpdateAdminContent
            // 
            this.mnuSegment_UpdateAdminContent.Name = "mnuSegment_UpdateAdminContent";
            this.mnuSegment_UpdateAdminContent.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_UpdateAdminContent.Text = "#Update Admin Content#";
            // 
            // mnuSegment_Copy
            // 
            this.mnuSegment_Copy.Name = "mnuSegment_Copy";
            this.mnuSegment_Copy.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Copy.Text = "#Copy#";
            // 
            // mnuSegment_Delete
            // 
            this.mnuSegment_Delete.Name = "mnuSegment_Delete";
            this.mnuSegment_Delete.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Delete.Text = "#Delete#";
            // 
            // mnuSegment_Share
            // 
            this.mnuSegment_Share.Name = "mnuSegment_Share";
            this.mnuSegment_Share.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Share.Text = "#Share#";
            // 
            // mnuSegment_Sep5
            // 
            this.mnuSegment_Sep5.Name = "mnuSegment_Sep5";
            this.mnuSegment_Sep5.Size = new System.Drawing.Size(261, 6);
            // 
            // mnuSegment_EditSavedData
            // 
            this.mnuSegment_EditSavedData.Name = "mnuSegment_EditSavedData";
            this.mnuSegment_EditSavedData.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_EditSavedData.Text = "#Edit Saved Data#";
            // 
            // mnuSegment_Design
            // 
            this.mnuSegment_Design.Name = "mnuSegment_Design";
            this.mnuSegment_Design.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Design.Text = "#Design#";
            // 
            // mnuSegment_Export
            // 
            this.mnuSegment_Export.Name = "mnuSegment_Export";
            this.mnuSegment_Export.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Export.Text = "#Export#";
            // 
            // mnuSegment_SaveAsUserSegment
            // 
            this.mnuSegment_SaveAsUserSegment.Name = "mnuSegment_SaveAsUserSegment";
            this.mnuSegment_SaveAsUserSegment.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_SaveAsUserSegment.Text = "#Save As User Segment#";
            // 
            // mnuSegment_ShowAssignments
            // 
            this.mnuSegment_ShowAssignments.Name = "mnuSegment_ShowAssignments";
            this.mnuSegment_ShowAssignments.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_ShowAssignments.Text = "#ShowAssignments#";
            // 
            // mnuSegment_Properties
            // 
            this.mnuSegment_Properties.Name = "mnuSegment_Properties";
            this.mnuSegment_Properties.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Properties.Text = "#Properties#";
            // 
            // mnuSegment_HelpSep
            // 
            this.mnuSegment_HelpSep.Name = "mnuSegment_HelpSep";
            this.mnuSegment_HelpSep.Size = new System.Drawing.Size(261, 6);
            // 
            // mnuSegment_Help
            // 
            this.mnuSegment_Help.Name = "mnuSegment_Help";
            this.mnuSegment_Help.Size = new System.Drawing.Size(264, 22);
            this.mnuSegment_Help.Text = "#Help#";
            // 
            // mnuFolder
            // 
            this.mnuFolder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFolder_CreateNewSegment,
            this.mnuFolder_CreateNewDesigner,
            this.mnuFolder_AddFile,
            this.mnuFolder_ImportSegment,
            this.mnuFolder_Sep1,
            this.mnuFolder_CreateNewFolder,
            this.mnuFolder_Properties,
            this.mnuFolder_SetPermissions,
            this.mnuFolder_Delete,
            this.mnuFolder_Sep2,
            this.mnuFolder_ApplyStyles,
            this.mnuFolder_SaveStyles,
            this.mnuFolder_Sep3,
            this.mnuFolder_Paste,
            this.mnuFolder_Sep4,
            this.mnuFolder_Default,
            this.mnuFolder_SelectDefault,
            this.mnuFolder_RemoveDefault,
            this.mnuFolder_Sep5,
            this.mnuFolder_MoveUp,
            this.mnuFolder_MoveDown,
            this.mnuFolder_Sep6,
            this.mnuFolder_CollapseAllSubfolders,
            this.mnuFolder_Refresh,
            this.mnuFolderHelpSep,
            this.mnuFolder_Help});
            this.mnuFolder.Name = "mnuFolder";
            this.mnuFolder.Size = new System.Drawing.Size(210, 464);
            this.mnuFolder.TimerInterval = 500;
            this.mnuFolder.UseTimer = true;
            this.mnuFolder.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.mnuFolder_Closed);
            this.mnuFolder.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.mnuFolder_Closing);
            this.mnuFolder.Opening += new System.ComponentModel.CancelEventHandler(this.mnuFolder_Opening);
            this.mnuFolder.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuFolder_ItemClicked);
            // 
            // mnuFolder_CreateNewSegment
            // 
            this.mnuFolder_CreateNewSegment.DropDown = this.mnuNewSegmentOptions;
            this.mnuFolder_CreateNewSegment.Name = "mnuFolder_CreateNewSegment";
            this.mnuFolder_CreateNewSegment.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_CreateNewSegment.Text = "#Create New Segment#";
            this.mnuFolder_CreateNewSegment.DropDownOpening += new System.EventHandler(this.mnuFolder_CreateNewSegment_DropDownOpening);
            // 
            // mnuNewSegmentOptions
            // 
            this.mnuNewSegmentOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewSegment_Blank,
            this.mnuNewSegment_Selection,
            this.mnuNewSegment_Document,
            this.mnuNewSegment_StyleSheet,
            this.mnuNewSegment_AnswerFile,
            this.mnuNewSegment_SegmentPacket,
            this.mnuNewSegment_MasterDataForm});
            this.mnuNewSegmentOptions.Name = "mnuNewSegmentOptions";
            this.mnuNewSegmentOptions.OwnerItem = this.mnuFolder_CreateNewSegment;
            this.mnuNewSegmentOptions.Size = new System.Drawing.Size(211, 158);
            this.mnuNewSegmentOptions.TimerInterval = 500;
            this.mnuNewSegmentOptions.UseTimer = true;
            this.mnuNewSegmentOptions.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.mnuNewSegmentOptions_Closed);
            this.mnuNewSegmentOptions.Opening += new System.ComponentModel.CancelEventHandler(this.mnuNewSegmentOptions_Opening);
            this.mnuNewSegmentOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuNewSegmentOptions_ItemClicked);
            // 
            // mnuNewSegment_Blank
            // 
            this.mnuNewSegment_Blank.Name = "mnuNewSegment_Blank";
            this.mnuNewSegment_Blank.Size = new System.Drawing.Size(210, 22);
            this.mnuNewSegment_Blank.Text = "#Blank Segment#";
            // 
            // mnuNewSegment_Selection
            // 
            this.mnuNewSegment_Selection.Name = "mnuNewSegment_Selection";
            this.mnuNewSegment_Selection.Size = new System.Drawing.Size(210, 22);
            this.mnuNewSegment_Selection.Text = "#From Current Selection#";
            // 
            // mnuNewSegment_Document
            // 
            this.mnuNewSegment_Document.Name = "mnuNewSegment_Document";
            this.mnuNewSegment_Document.Size = new System.Drawing.Size(210, 22);
            this.mnuNewSegment_Document.Text = "#From Entire Document#";
            // 
            // mnuNewSegment_StyleSheet
            // 
            this.mnuNewSegment_StyleSheet.Name = "mnuNewSegment_StyleSheet";
            this.mnuNewSegment_StyleSheet.Size = new System.Drawing.Size(210, 22);
            this.mnuNewSegment_StyleSheet.Text = "#Style Sheet#";
            // 
            // mnuNewSegment_AnswerFile
            // 
            this.mnuNewSegment_AnswerFile.Name = "mnuNewSegment_AnswerFile";
            this.mnuNewSegment_AnswerFile.Size = new System.Drawing.Size(210, 22);
            this.mnuNewSegment_AnswerFile.Text = "#Answer File#";
            this.mnuNewSegment_AnswerFile.Visible = false;
            // 
            // mnuNewSegment_SegmentPacket
            // 
            this.mnuNewSegment_SegmentPacket.Name = "mnuNewSegment_SegmentPacket";
            this.mnuNewSegment_SegmentPacket.Size = new System.Drawing.Size(210, 22);
            this.mnuNewSegment_SegmentPacket.Text = "#Segment Packet#";
            // 
            // mnuNewSegment_MasterDataForm
            // 
            this.mnuNewSegment_MasterDataForm.Name = "mnuNewSegment_MasterDataForm";
            this.mnuNewSegment_MasterDataForm.Size = new System.Drawing.Size(210, 22);
            this.mnuNewSegment_MasterDataForm.Text = "#Master Data Form#";
            // 
            // mnuFolder_CreateNewDesigner
            // 
            this.mnuFolder_CreateNewDesigner.DropDown = this.mnuNewSegmentOptions;
            this.mnuFolder_CreateNewDesigner.Name = "mnuFolder_CreateNewDesigner";
            this.mnuFolder_CreateNewDesigner.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_CreateNewDesigner.Text = "#Create New Designer#";
            this.mnuFolder_CreateNewDesigner.DropDownOpening += new System.EventHandler(this.mnuFolder_CreateNewDesigner_DropDownOpening);
            // 
            // mnuFolder_AddFile
            // 
            this.mnuFolder_AddFile.Name = "mnuFolder_AddFile";
            this.mnuFolder_AddFile.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_AddFile.Text = "#Add File#";
            this.mnuFolder_AddFile.Click += new System.EventHandler(this.mnuFolder_AddFile_Click);
            // 
            // mnuFolder_ImportSegment
            // 
            this.mnuFolder_ImportSegment.Name = "mnuFolder_ImportSegment";
            this.mnuFolder_ImportSegment.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_ImportSegment.Text = "#ImportSegment#";
            // 
            // mnuFolder_Sep1
            // 
            this.mnuFolder_Sep1.Name = "mnuFolder_Sep1";
            this.mnuFolder_Sep1.Size = new System.Drawing.Size(206, 6);
            // 
            // mnuFolder_CreateNewFolder
            // 
            this.mnuFolder_CreateNewFolder.Name = "mnuFolder_CreateNewFolder";
            this.mnuFolder_CreateNewFolder.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_CreateNewFolder.Text = "#Create New Folder#";
            // 
            // mnuFolder_Properties
            // 
            this.mnuFolder_Properties.Name = "mnuFolder_Properties";
            this.mnuFolder_Properties.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_Properties.Text = "#Folder Properties#";
            // 
            // mnuFolder_SetPermissions
            // 
            this.mnuFolder_SetPermissions.Name = "mnuFolder_SetPermissions";
            this.mnuFolder_SetPermissions.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_SetPermissions.Text = "#Permissions#";
            // 
            // mnuFolder_Delete
            // 
            this.mnuFolder_Delete.Name = "mnuFolder_Delete";
            this.mnuFolder_Delete.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_Delete.Text = "#Delete#";
            // 
            // mnuFolder_Sep2
            // 
            this.mnuFolder_Sep2.Name = "mnuFolder_Sep2";
            this.mnuFolder_Sep2.Size = new System.Drawing.Size(206, 6);
            // 
            // mnuFolder_ApplyStyles
            // 
            this.mnuFolder_ApplyStyles.Name = "mnuFolder_ApplyStyles";
            this.mnuFolder_ApplyStyles.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_ApplyStyles.Text = "#Apply Styles#";
            // 
            // mnuFolder_SaveStyles
            // 
            this.mnuFolder_SaveStyles.Name = "mnuFolder_SaveStyles";
            this.mnuFolder_SaveStyles.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_SaveStyles.Text = "#Save Styles#";
            // 
            // mnuFolder_Sep3
            // 
            this.mnuFolder_Sep3.Name = "mnuFolder_Sep3";
            this.mnuFolder_Sep3.Size = new System.Drawing.Size(206, 6);
            // 
            // mnuFolder_Paste
            // 
            this.mnuFolder_Paste.Name = "mnuFolder_Paste";
            this.mnuFolder_Paste.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_Paste.Text = "#Paste#";
            // 
            // mnuFolder_Sep4
            // 
            this.mnuFolder_Sep4.Name = "mnuFolder_Sep4";
            this.mnuFolder_Sep4.Size = new System.Drawing.Size(206, 6);
            // 
            // mnuFolder_Default
            // 
            this.mnuFolder_Default.Name = "mnuFolder_Default";
            this.mnuFolder_Default.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_Default.Text = "#Default#";
            // 
            // mnuFolder_SelectDefault
            // 
            this.mnuFolder_SelectDefault.Name = "mnuFolder_SelectDefault";
            this.mnuFolder_SelectDefault.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_SelectDefault.Text = "#Select Default#";
            // 
            // mnuFolder_RemoveDefault
            // 
            this.mnuFolder_RemoveDefault.Name = "mnuFolder_RemoveDefault";
            this.mnuFolder_RemoveDefault.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_RemoveDefault.Text = "#Remove Default#";
            // 
            // mnuFolder_Sep5
            // 
            this.mnuFolder_Sep5.Name = "mnuFolder_Sep5";
            this.mnuFolder_Sep5.Size = new System.Drawing.Size(206, 6);
            // 
            // mnuFolder_MoveUp
            // 
            this.mnuFolder_MoveUp.Name = "mnuFolder_MoveUp";
            this.mnuFolder_MoveUp.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_MoveUp.Text = "#Move Up#";
            // 
            // mnuFolder_MoveDown
            // 
            this.mnuFolder_MoveDown.Name = "mnuFolder_MoveDown";
            this.mnuFolder_MoveDown.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_MoveDown.Text = "#Move Down#";
            // 
            // mnuFolder_Sep6
            // 
            this.mnuFolder_Sep6.Name = "mnuFolder_Sep6";
            this.mnuFolder_Sep6.Size = new System.Drawing.Size(206, 6);
            // 
            // mnuFolder_CollapseAllSubfolders
            // 
            this.mnuFolder_CollapseAllSubfolders.Name = "mnuFolder_CollapseAllSubfolders";
            this.mnuFolder_CollapseAllSubfolders.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_CollapseAllSubfolders.Text = "#Collapse All Subfolders#";
            // 
            // mnuFolder_Refresh
            // 
            this.mnuFolder_Refresh.Name = "mnuFolder_Refresh";
            this.mnuFolder_Refresh.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_Refresh.Text = "#Refresh#";
            // 
            // mnuFolderHelpSep
            // 
            this.mnuFolderHelpSep.Name = "mnuFolderHelpSep";
            this.mnuFolderHelpSep.Size = new System.Drawing.Size(206, 6);
            // 
            // mnuFolder_Help
            // 
            this.mnuFolder_Help.Name = "mnuFolder_Help";
            this.mnuFolder_Help.Size = new System.Drawing.Size(209, 22);
            this.mnuFolder_Help.Text = "#Help#";
            this.mnuFolder_Help.Click += new System.EventHandler(this.mnuFolder_Help_Click);
            // 
            // ContentManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnlContentTree);
            this.Controls.Add(this.splitContent);
            this.Controls.Add(this.pnlContentDescription);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ContentManager";
            this.Size = new System.Drawing.Size(328, 528);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ContentManager_KeyDown);
            this.pnlContentDescription.ResumeLayout(false);
            this.mnuQuickHelp.ResumeLayout(false);
            this.pnlContentTree.ResumeLayout(false);
            this.pnlContentTree.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMenuDropdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFindContent)).EndInit();
            this.mnuSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeResults)).EndInit();
            this.mnuSegment.ResumeLayout(false);
            this.mnuFolder.ResumeLayout(false);
            this.mnuNewSegmentOptions.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private ToolStripMenuItem mnuSegment_Properties;
        private ToolStripMenuItem mnuSegment_Delete;
        private ToolStripMenuItem mnuSegment_InsertAtSelection;
        private ToolStripMenuItem mnuSegment_InsertAtSelectionAsPlainText;     //GLOG : 8248
        private ToolStripMenuItem mnuSegment_InsertEndOfDocument;
        private ToolStripSeparator mnuSegment_Sep1;
        private ToolStripMenuItem mnuSegment_CreateNewDocument;
        private ToolStripMenuItem mnuSegment_InsertStartOfDocument;
        private ToolStripMenuItem mnuSegment_InsertStartOfThisSection;
        private ToolStripMenuItem mnuSegment_InsertStartOfAllSections;
        private ToolStripMenuItem mnuSegment_KeepHeadersFooters;
        private ToolStripSeparator mnuSegment_Sep3;
        private ToolStripMenuItem mnuSegment_Copy;
        private ToolStripMenuItem mnuSegment_RestartPageNumbering;
        private ToolStripMenuItem mnuSegment_SeparateSection;
        private ToolStripMenuItem mnuSegment_Insert;
        private TimedContextMenuStrip mnuFolder;
        private ToolStripMenuItem mnuFolder_CreateNewFolder;
        private ToolStripSeparator mnuFolder_Sep1;
        private ToolStripMenuItem mnuFolder_Properties;
        private ToolStripMenuItem mnuFolder_Delete;
        private ToolStripSeparator mnuFolder_Sep2;
        private ToolStripMenuItem mnuFolder_CollapseAllSubfolders;
        private ToolStripMenuItem mnuFolder_Refresh;
        private ToolStripMenuItem mnuFolder_Default;
        private ToolStripSeparator mnuFolder_Sep4;
        private ToolStripMenuItem mnuFolder_SelectDefault;
        private ToolStripMenuItem mnuFolder_RemoveDefault;
        private ToolStripMenuItem mnuFolder_Paste;
        private ToolStripSeparator mnuSegment_Sep4;
        private ToolStripMenuItem mnuSegment_Design;
        private ToolStripMenuItem mnuSegment_InsertEndOfThisSection;
        private ToolStripMenuItem mnuSegment_InsertEndOfAllSections;
        private ToolStripMenuItem mnuFolder_CreateNewSegment;
        private ToolStripSeparator mnuFolder_Sep3;
        private TimedContextMenuStrip mnuNewSegmentOptions;
        private ToolStripMenuItem mnuSegment_ApplyStyles;
        private ToolStripSeparator mnuSegment_Sep5;
        private ToolStripMenuItem mnuFolder_ApplyStyles;
        private ToolStripMenuItem mnuFolder_SaveStyles;
        private ToolStripSeparator mnuFolder_Sep5;
        private ToolStripMenuItem mnuSegment_Clone;
        private UltraTree treeResults;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkFindContent;
        private WebBrowser wbDescription;
        private ToolStripMenuItem mnuSegment_InsertUsingSavedPrefill;
        private ToolStripSeparator mnuSegment_Sep2;
        private ToolStripMenuItem mnuSegment_SelectionAsBody;
        private ToolStripMenuItem mnuSegment_Share;
        private ToolStripMenuItem mnuFolder_MoveUp;
        private ToolStripMenuItem mnuFolder_MoveDown;
        private ToolStripSeparator mnuFolder_Sep6;
        private ToolStripMenuItem mnuFolder_SetPermissions;
        private ToolStripMenuItem mnuFolder_ImportSegment;
        private Label lblStatus;
        private ToolStripMenuItem mnuSegment_CopyStyles;
        private ToolStripMenuItem mnuSegment_StyleSummary;
        private PictureBox picHelp;
        private ToolStripMenuItem mnuSegment_InsertUsingTransientPrefill;
        private ToolStripMenuItem mnuSegment_AttachTo;
        private ToolStripMenuItem mnuSegment_ManageAuthorPreferences;
        private TimedContextMenuStrip mnuSearch;
        private ToolStripMenuItem mnuSearch_Clear;
        private TimedContextMenuStrip mnuQuickHelp;
        private ToolStripMenuItem mnuQuickHelp_Copy;
        private ToolStripMenuItem mnuQuickHelp_SaveAs;
        private ToolStripMenuItem mnuQuickHelp_Print;
        private ToolStripSeparator toolStripMenuItem2;
        private ToolStripSeparator toolStripMenuItem1;
        private ToolStripMenuItem mnuQuickHelp_PrintPreview;
        private ToolStripMenuItem mnuFolder_AddFile;
        private ToolStripMenuItem mnuSegment_CreateExternalContent;
        private ToolStripMenuItem mnuSegment_ConvertToAdminContent;
        private ToolStripMenuItem mnuSegment_UpdateAdminContent;
        private ToolTip tipMain;
        private ToolStripMenuItem mnuSegment_Help;
        private ToolStripSeparator mnuFolderHelpSep;
        private ToolStripMenuItem mnuFolder_Help;
        private ToolStripSeparator mnuSegment_HelpSep;
        private ToolStripMenuItem mnuSegment_InsertUsingSelection;
        private PictureBox picMenuDropdown;
        private ToolStripMenuItem mnuSegment_EditAnswerFile;
        private ToolStripMenuItem mnuSegment_SaveAsUserSegment;
        private ToolStripMenuItem mnuSegment_Export;
        private ToolStripMenuItem mnuSegment_ShowAssignments;
        private ToolStripMenuItem mnuSegment_InsertMultipleusingSavedData;
        private ToolStripMenuItem mnuSegment_AddToFavorites;
        private ToolStripMenuItem mnuSegment_EditSavedData;
        private ToolStripMenuItem mnuSegment_InsertUsingClientMatterLookup;
        private ToolStripMenuItem mnuSegment_RemoveFromFavorites;
        private ToolStripMenuItem mnuSegment_CreateMasterData;
        private Button btnFindContentAdvanced;
        private ToolStripMenuItem mnuNewSegment_Blank;
        private ToolStripMenuItem mnuNewSegment_Selection;
        private ToolStripMenuItem mnuNewSegment_Document;
        private ToolStripMenuItem mnuNewSegment_StyleSheet;
        private ToolStripMenuItem mnuNewSegment_AnswerFile;
        private ToolStripMenuItem mnuNewSegment_SegmentPacket;
        private ToolStripMenuItem mnuNewSegment_MasterDataForm;
        private ToolStripMenuItem mnuFolder_CreateNewDesigner;
    }
}