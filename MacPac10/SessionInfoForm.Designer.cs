﻿namespace LMP.MacPac
{
    partial class SessionInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.printSessionDocument = new System.Drawing.Printing.PrintDocument();
            this.TabControl1 = new LMP.MacPac.TabControlEx();
            this.tpMetadata = new System.Windows.Forms.TabPage();
            this.grdSessionInfo = new System.Windows.Forms.DataGridView();
            this.tpPeople = new System.Windows.Forms.TabPage();
            this.lblPeople = new System.Windows.Forms.Label();
            this.grdPeople = new System.Windows.Forms.DataGridView();
            this.TabControl1.SuspendLayout();
            this.tpMetadata.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSessionInfo)).BeginInit();
            this.tpPeople.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.Location = new System.Drawing.Point(508, 354);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.AutoSize = true;
            this.btnPrint.Location = new System.Drawing.Point(426, 354);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printSessionDocument
            // 
            this.printSessionDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.printSessionDocument_BeginPrint);
            this.printSessionDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printSessionDocument_PrintPage);
            // 
            // TabControl1
            // 
            this.TabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl1.Controls.Add(this.tpMetadata);
            this.TabControl1.Controls.Add(this.tpPeople);
            this.TabControl1.Location = new System.Drawing.Point(1, 1);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(605, 344);
            this.TabControl1.TabIndex = 0;
            // 
            // tpMetadata
            // 
            this.tpMetadata.Controls.Add(this.grdSessionInfo);
            this.tpMetadata.Location = new System.Drawing.Point(4, 22);
            this.tpMetadata.Name = "tpMetadata";
            this.tpMetadata.Padding = new System.Windows.Forms.Padding(3);
            this.tpMetadata.Size = new System.Drawing.Size(597, 318);
            this.tpMetadata.TabIndex = 0;
            this.tpMetadata.Text = "Metadata";
            this.tpMetadata.UseVisualStyleBackColor = true;
            // 
            // grdSessionInfo
            // 
            this.grdSessionInfo.AllowUserToAddRows = false;
            this.grdSessionInfo.AllowUserToDeleteRows = false;
            this.grdSessionInfo.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grdSessionInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdSessionInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSessionInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdSessionInfo.Location = new System.Drawing.Point(3, 3);
            this.grdSessionInfo.Name = "grdSessionInfo";
            this.grdSessionInfo.ReadOnly = true;
            this.grdSessionInfo.Size = new System.Drawing.Size(591, 312);
            this.grdSessionInfo.TabIndex = 2;
            // 
            // tpPeople
            // 
            this.tpPeople.BackColor = System.Drawing.SystemColors.Control;
            this.tpPeople.Controls.Add(this.lblPeople);
            this.tpPeople.Controls.Add(this.grdPeople);
            this.tpPeople.Location = new System.Drawing.Point(4, 22);
            this.tpPeople.Name = "tpPeople";
            this.tpPeople.Padding = new System.Windows.Forms.Padding(3);
            this.tpPeople.Size = new System.Drawing.Size(597, 318);
            this.tpPeople.TabIndex = 1;
            this.tpPeople.Text = "People";
            // 
            // lblPeople
            // 
            this.lblPeople.AutoSize = true;
            this.lblPeople.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeople.Location = new System.Drawing.Point(6, 5);
            this.lblPeople.Name = "lblPeople";
            this.lblPeople.Size = new System.Drawing.Size(183, 15);
            this.lblPeople.TabIndex = 4;
            this.lblPeople.Text = "Active Firm People in Forte.mdb:";
            // 
            // grdPeople
            // 
            this.grdPeople.AllowUserToAddRows = false;
            this.grdPeople.AllowUserToDeleteRows = false;
            this.grdPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdPeople.BackgroundColor = System.Drawing.SystemColors.Control;
            this.grdPeople.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPeople.Location = new System.Drawing.Point(3, 30);
            this.grdPeople.Name = "grdPeople";
            this.grdPeople.ReadOnly = true;
            this.grdPeople.Size = new System.Drawing.Size(591, 285);
            this.grdPeople.TabIndex = 3;
            // 
            // SessionInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(604, 389);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.TabControl1);
            this.Controls.Add(this.btnOK);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SessionInfoForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Session Information";
            this.Load += new System.EventHandler(this.SessionInfoForm_Load);
            this.TabControl1.ResumeLayout(false);
            this.tpMetadata.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdSessionInfo)).EndInit();
            this.tpPeople.ResumeLayout(false);
            this.tpPeople.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private TabControlEx TabControl1;  // System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage tpMetadata;
        private System.Windows.Forms.TabPage tpPeople;
        private System.Windows.Forms.DataGridView grdSessionInfo;
        private System.Windows.Forms.DataGridView grdPeople;
        private System.Windows.Forms.Label lblPeople;
        private System.Windows.Forms.Button btnPrint;
        private System.Drawing.Printing.PrintDocument printSessionDocument;
    }
}