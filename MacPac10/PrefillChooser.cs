using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class PrefillChooser : Form
    {
        public enum PrefillSources
        {
            SelectedSegment = 1,
            SavedPrefill = 2,
            PastedContent = 3,
            Selection = 4
        }

        private string m_xPrefillID = "";
        private ISegmentDef m_oSegDef = null;
        private short m_shInsertionLocation = 0;
        private short m_shInsertionBehavior = 0;
        private PrefillSources m_iPrefillSource = PrefillSources.SavedPrefill;

        public PrefillChooser():this(LMP.Resources.GetLangString("Dialog_InsertUsingPrefill"))
        {
        }

        public PrefillChooser(string xDlgCaption)
        {
            InitializeComponent();
            this.PrefillSource = PrefillSources.SavedPrefill;
            this.grpOptions.Visible = false;
            this.lblLocation.Visible = false;
            this.cmbLocation.Visible = false;

            this.Text = xDlgCaption;
            this.lblPrefill.Text = LMP.Resources.GetLangString("Item_SavedPrefill") + ":";

            //setup prefill treeview control
            this.ftvPrefill.OwnerID = Session.CurrentUser.ID;
            this.ftvPrefill.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders |
                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Prefills;
            ftvPrefill.ExecuteFinalSetup();

            this.Size = new Size(this.grpOptions.Left + 12, this.ftvPrefill.Top + this.ftvPrefill.Height + this.btnOK.Height + 70);
            this.ftvPrefill.Height = this.btnOK.Top - this.ftvPrefill.Top - 10;
        }
        /// <summary>
        /// Contructor - requires AdminSegmentDef object
        /// </summary>
        /// <param name="oDef"></param>
        public PrefillChooser(ISegmentDef oDef, PrefillSources oSource)
        {
            try
            {
                InitializeComponent();
                m_oSegDef = oDef;
                this.PrefillSource = oSource;

                LoadSegmentOptions();

                this.lblLocation.Text = LMP.Resources.GetLangString("Item_InsertionLocation") + ":";
                this.grpOptions.Text = LMP.Resources.GetLangString("Item_InsertionOptions");
                this.chkKeepExisting.Text = LMP.Resources.GetLangString("Item_KeepExistingHeadersFooters");
                this.chkRestartNumbering.Text = LMP.Resources.GetLangString("Item_RestartPageNumbering");
                this.chkSeparateSection.Text = LMP.Resources.GetLangString("Item_InsertInSeparateSection");

                if (oSource == PrefillSources.SelectedSegment)
                {
                    //hide prefill treeview control
                    this.ftvPrefill.Visible = false;
                    this.lblPrefill.Visible = false;

                    this.Text = LMP.Resources.GetLangString("Dialog_InsertUsingContent");

                    //GLOG #8492 - dcf
                    //this.grpOptions.Top = 62;
                    this.grpOptions.Top = this.cmbLocation.Top + this.cmbLocation.Height + 4;

                    //resize dlg - visible controls will move
                    //because anchor has been set to top + right
                    this.Size = new Size(this.lblFormSizeMarker1.Left, this.lblFormSizeMarker1.Top);
                    //GLOG item #7847 - dcf
                    Application.AdjustDialogWidthForWord15(this);
                }
                else if (oSource == PrefillSources.PastedContent)
                {
                    //hide prefill treeview control
                    this.ftvPrefill.Visible = false;
                    this.lblPrefill.Visible = false;

                    this.Text = LMP.Resources.GetLangString("Dialog_PasteSegmentContent");
                    this.grpOptions.Top = 62;

                    //resize dlg - visible controls will move
                    //because anchor has been set to top + right
                    this.Size = new Size(246, 240);

                    //GLOG item #7847 - dcf
                    Application.AdjustDialogWidthForWord15(this);
                }
                else if (oSource == PrefillSources.Selection)
                {
                    //hide prefill treeview control
                    this.ftvPrefill.Visible = false;
                    this.lblPrefill.Visible = false;

                    this.Text = LMP.Resources.GetLangString("Dialog_InsertUsingSelection");
                    //GLOG : 8912 : ceh
                    //this.grpOptions.Top = 62;
                    this.grpOptions.Top = this.cmbLocation.Top + this.cmbLocation.Height + 4;

                    //resize dlg - visible controls will move
                    //because anchor has been set to top + right
                    //this.Size = new Size(246, 240);
                    this.Size = new Size(this.lblFormSizeMarker1.Left, this.lblFormSizeMarker1.Top);

                    //GLOG item #7847 - dcf
                    Application.AdjustDialogWidthForWord15(this);
                }
                else
                {
                    this.Text = LMP.Resources.GetLangString("Dialog_InsertUsingPrefill");
                    this.lblPrefill.Text = LMP.Resources.GetLangString("Item_SavedPrefill") + ":";

                    //setup prefill treeview control
                    this.ftvPrefill.OwnerID = Session.CurrentUser.ID;
                    this.ftvPrefill.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders |
                        LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Prefills;
                    ftvPrefill.ExecuteFinalSetup();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Returns Insertion location selected in Combo
        /// </summary>
        public short InsertionLocation
        {
            get { return m_shInsertionLocation; }
        }
        /// <summary>
        /// Returns Insertion Behavior based on checkbox values
        /// </summary>
        public short InsertionBehavior
        {
            get { return m_shInsertionBehavior; }
        }
        /// <summary>
        /// Returns ID of Variable Set selected in Treeview
        /// </summary>
        public string VariableSetID
        {
            get { return m_xPrefillID; }
        }
        public PrefillSources PrefillSource
        {
            set { m_iPrefillSource = value; }
            get { return m_iPrefillSource; }
        }

        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;

                if (ftvPrefill.MemberList.Count > 0)
                {
                    m_xPrefillID = ((ArrayList)ftvPrefill.MemberList[0])[0].ToString();
                }
                int iBehaviors = 0;
                if (chkSeparateSection.Checked == true)
                    iBehaviors += (int)Segment.InsertionBehaviors.InsertInSeparateNextPageSection;
                if (chkKeepExisting.Checked == true)
                    iBehaviors += (int)Segment.InsertionBehaviors.KeepExistingHeadersFooters;
                if (chkRestartNumbering.Checked == true)
                    iBehaviors += (int)Segment.InsertionBehaviors.RestartPageNumbering;
                m_shInsertionBehavior = (short)iBehaviors;
                m_shInsertionLocation = short.Parse(cmbLocation.SelectedValue.ToString());
            }
            catch { }

            if (this.PrefillSource == PrefillSources.SavedPrefill && m_xPrefillID == "")
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_NoPrefillSelected"), LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.DialogResult = DialogResult.None;
            }
        }
        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_xPrefillID = "";
        }
        /// <summary>
        /// Setup Display options for current Segment
        /// </summary>
        private void LoadSegmentOptions()
        {
            ISegmentDef oDef = null;
            if (m_oSegDef.TypeID == mpObjectTypes.SavedContent)
            {
                //segment is saved content - get options of contained segment
                string xSegmentID = m_oSegDef.ChildSegmentIDs;
                if (!xSegmentID.Contains(".") || xSegmentID.EndsWith(".0"))
                {
                    //GLOG 6966: Make sure string can be parsed as a Double even if
                    //the Decimal separator is something other than '.' in Regional Settings
                    if (xSegmentID.EndsWith(".0"))
                    {
                        xSegmentID = xSegmentID.Replace(".0", "");
                    }
                    //contained segment is admin segment - get def
                    double dSegmentID = double.Parse(xSegmentID);
                    int iSegmentID = (int)dSegmentID;

                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    oDef = (AdminSegmentDef)oDefs.ItemFromID(iSegmentID); ;
                }
                else
                {
                    //contained segment is user segment - get def
                    UserSegmentDefs oDefs = new UserSegmentDefs();
                    oDef = (UserSegmentDef)oDefs.ItemFromID(xSegmentID);
                }
            }
            else
            {
                //segment is not saved data
                oDef = m_oSegDef;
            }

            if (oDef != null && oDef.TypeID != mpObjectTypes.UserSegment)
            {
                AdminSegmentDef oAdminDef = (AdminSegmentDef)oDef;
                ArrayList oListItems = new ArrayList();
                //get array of insertion locations
                object[,] aDefaultLocations = new object[,]{
                    {"Start of Current Section",1}, 
                    {"End of Current Section",2}, 
                    {"Start of All Sections",4}, 
                    {"End of All Sections",8}, 
                    {"Insertion Point",16}, 
                    {"Start of Document",32}, 
                    {"End of Document",64},
                    {"New Document",128},
                    {"Default Location", 256}};

                object[,] aBehaviors = new object[,]{
                {"Insert In Separate Next Page Section",1}, 
                {"Insert In Separate Continuous Section",8}, 
                {"Keep Existing Headers Footers",2}, 
                {"Restart Page Numbering",4}};

                for (int i = aDefaultLocations.GetUpperBound(0); i >= 0; i--)
                {
                    if ((oAdminDef.MenuInsertionOptions &
                        (short)(int)aDefaultLocations[i, 1]) == (short)(int)aDefaultLocations[i, 1])
                    {
                        oListItems.Add(new object[] { aDefaultLocations[i, 1], aDefaultLocations[i, 0] });
                    }
                }
                if (oListItems.Count == 0)
                {
                    cmbLocation.SetList(aDefaultLocations);
                }
                else
                {
                    cmbLocation.SetList(oListItems);
                }
                // Select default double-click item in list
                cmbLocation.SelectedValue = oAdminDef.DefaultDoubleClickLocation;
                if (cmbLocation.SelectedIndex == -1)
                    cmbLocation.SelectedIndex = 0;

                // Set checkbox options based on Segment definition
                if ((oAdminDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection) !=
                    (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection)
                {
                    this.chkSeparateSection.Enabled = false;
                }
                else
                {
                    this.chkSeparateSection.Checked = ((oAdminDef.DefaultDoubleClickBehavior &
                        (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection) ==
                        (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection);

                }
                if ((oAdminDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters) !=
                    (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters)
                {
                    this.chkKeepExisting.Enabled = false;
                }
                else
                {
                    this.chkKeepExisting.Checked = ((oAdminDef.DefaultDoubleClickBehavior &
                        (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters) ==
                        (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters);

                }
                if ((oAdminDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.RestartPageNumbering) !=
                    (short)Segment.InsertionBehaviors.RestartPageNumbering)
                {
                    this.chkRestartNumbering.Enabled = false;
                }
                else
                {
                    this.chkRestartNumbering.Checked = ((oAdminDef.DefaultDoubleClickBehavior &
                        (short)Segment.InsertionBehaviors.RestartPageNumbering) ==
                        (short)Segment.InsertionBehaviors.RestartPageNumbering);
                }
            }
            else if (oDef != null || this.PrefillSource == PrefillSources.PastedContent)
            {
                ArrayList oListItems = new ArrayList();
                oListItems.Add(new object[] { 16, "Insertion Point" });
                oListItems.Add(new object[] { 128, "New Document" });
                cmbLocation.SetList(oListItems);
                this.grpOptions.Enabled = false;
            }
            else
            {
                this.lblLocation.Visible = false;
                this.cmbLocation.Visible = false;
                this.grpOptions.Visible = false;
            }

        }

        private void cmbLocation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.cmbLocation.Text == "New Document")
                {
                    this.chkSeparateSection.Checked = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void cmbLocation_Load(object sender, EventArgs e)
        {
            try
            {
                if (this.cmbLocation.Text == "New Document")
                {
                    this.chkSeparateSection.Checked = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // GLOG : 4764 : CEH
        private void ftvPrefill_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (ftvPrefill.MemberList.Count > 0)
                {
                    //Insert Prefill
                    btnOK_Click(sender, null);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }

}