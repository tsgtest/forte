using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac
{
    public class Undo
    {
        private Word.Document m_oDoc;
        private object m_oID;

        public Undo(Word.Document oDoc)
        {
            this.m_oDoc = oDoc;
            this.m_oID = (new Guid()).ToString();
        }

        /// <summary>
        /// marks the state of the document
        /// to which a call to restore will
        /// return the document
        /// </summary>
        public void Mark()
        {
            object oVal = "1";
            this.m_oDoc.Variables.Add(this.m_oID.ToString(), ref oVal);
        }

        /// <summary>
        /// restores the document to the marked state
        /// </summary>
        public void Restore()
        {
            Word.Variable oDocVar = null;
            object oTimes = 1;
            try
            {
                oDocVar = null;
                oDocVar = m_oDoc.Variables.get_Item(ref this.m_oID);
                string xVal = oDocVar.Value;
            }
            catch { }

            while (oDocVar != null)
            {
                m_oDoc.Undo(ref oTimes);
                try
                {
                    oDocVar = null;
                    oDocVar = m_oDoc.Variables.get_Item(ref this.m_oID);
                    string xVal = oDocVar.Value;
                }
                catch { }
            }
        }
    }
}
