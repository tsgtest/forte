namespace LMP.MacPac
{
    partial class RecreateOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpLocation = new System.Windows.Forms.GroupBox();
            this.optReplaceExisting = new System.Windows.Forms.RadioButton();
            this.optNewDocument = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.optNoText = new System.Windows.Forms.RadioButton();
            this.optEntireBody = new System.Windows.Forms.RadioButton();
            this.optSelectedText = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.grpLocation.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpLocation
            // 
            this.grpLocation.AutoSize = true;
            this.grpLocation.Controls.Add(this.optReplaceExisting);
            this.grpLocation.Controls.Add(this.optNewDocument);
            this.grpLocation.Location = new System.Drawing.Point(12, 14);
            this.grpLocation.Name = "grpLocation";
            this.grpLocation.Size = new System.Drawing.Size(154, 120);
            this.grpLocation.TabIndex = 0;
            this.grpLocation.TabStop = false;
            this.grpLocation.Text = "Location Options";
            // 
            // optReplaceExisting
            // 
            this.optReplaceExisting.AutoSize = true;
            this.optReplaceExisting.Location = new System.Drawing.Point(17, 53);
            this.optReplaceExisting.Name = "optReplaceExisting";
            this.optReplaceExisting.Size = new System.Drawing.Size(106, 19);
            this.optReplaceExisting.TabIndex = 1;
            this.optReplaceExisting.Text = "Replace &Existing";
            this.optReplaceExisting.UseVisualStyleBackColor = true;
            // 
            // optNewDocument
            // 
            this.optNewDocument.AutoSize = true;
            this.optNewDocument.Checked = true;
            this.optNewDocument.Location = new System.Drawing.Point(17, 27);
            this.optNewDocument.Name = "optNewDocument";
            this.optNewDocument.Size = new System.Drawing.Size(99, 19);
            this.optNewDocument.TabIndex = 0;
            this.optNewDocument.TabStop = true;
            this.optNewDocument.Text = "&New Document";
            this.optNewDocument.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.optNoText);
            this.groupBox1.Controls.Add(this.optEntireBody);
            this.groupBox1.Controls.Add(this.optSelectedText);
            this.groupBox1.Location = new System.Drawing.Point(172, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 120);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Include";
            // 
            // optNoText
            // 
            this.optNoText.AutoSize = true;
            this.optNoText.Location = new System.Drawing.Point(17, 80);
            this.optNoText.Name = "optNoText";
            this.optNoText.Size = new System.Drawing.Size(92, 19);
            this.optNoText.TabIndex = 2;
            this.optNoText.Text = "N&o Body Text";
            this.optNoText.UseVisualStyleBackColor = true;
            // 
            // optEntireBody
            // 
            this.optEntireBody.AutoSize = true;
            this.optEntireBody.Location = new System.Drawing.Point(17, 53);
            this.optEntireBody.Name = "optEntireBody";
            this.optEntireBody.Size = new System.Drawing.Size(81, 19);
            this.optEntireBody.TabIndex = 1;
            this.optEntireBody.Text = "Entire &Body";
            this.optEntireBody.UseVisualStyleBackColor = true;
            // 
            // optSelectedText
            // 
            this.optSelectedText.AutoSize = true;
            this.optSelectedText.Checked = true;
            this.optSelectedText.Location = new System.Drawing.Point(17, 27);
            this.optSelectedText.Name = "optSelectedText";
            this.optSelectedText.Size = new System.Drawing.Size(92, 19);
            this.optSelectedText.TabIndex = 0;
            this.optSelectedText.TabStop = true;
            this.optSelectedText.Text = "&Selected Text";
            this.optSelectedText.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(241, 149);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 25);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(154, 149);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(81, 25);
            this.btnOK.TabIndex = 11;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // RecreateOptionsForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(337, 189);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpLocation);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RecreateOptionsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Recreate";
            this.Load += new System.EventHandler(this.RecreateOptionsForm_Load);
            this.grpLocation.ResumeLayout(false);
            this.grpLocation.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpLocation;
        private System.Windows.Forms.RadioButton optReplaceExisting;
        private System.Windows.Forms.RadioButton optNewDocument;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton optEntireBody;
        private System.Windows.Forms.RadioButton optSelectedText;
        private System.Windows.Forms.RadioButton optNoText;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }
}