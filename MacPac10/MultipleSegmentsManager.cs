﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Architect.Api;
using LMP.Controls;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace LMP.MacPac
{
    public delegate void OKClickedHandler(object sender, EventArgs e);
    public delegate void CancelClickedHandler(object sender, EventArgs e);

    public partial class MultipleSegmentsManager : UserControl
    {
        #region *********************enum**************************
        #endregion
        #region *********************fields**************************
        //TODO: settle on an ITEM_SEP - these are set to work with ComboBox.SetList(string)
        public const char ITEM_SEP = '~';
        public const char NAME_VALUE_SEP = ';';
        private string m_xSegmentID = "";
        private TaskPane m_oTaskPane = null;
        private Prefill m_oPrefill = null;
        private int iCurSelRow = -1;
        private bool m_bInsertableOnly = false; //GLOG 8163
        private bool m_bIsDoubleClick = false; //GLOG 15743
        #endregion
        #region *********************events**************************
        public event OKClickedHandler OKClicked;
        public event CancelClickedHandler CancelClicked;
        #endregion
        #region *********************constructors**************************
        public MultipleSegmentsManager()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************methods**************************

        /// <summary>
        /// adds the segment corresponding to the active node
        /// to the list of selected segments
        /// </summary>
        private void AddSegment()
        {
            int iMenuInsertionOptions = 0;

            //GLOG item #6162 - dcf
            if (this.ftvSegments.MemberList.Count == 0)
                return;
            //GLOG : 15743 : jsw
            if (m_bIsDoubleClick == true && !bCursorIsOverNode(this.ftvSegments.NodePoint))
                return;

            //GLOG : 6255 : ceh - deal with Segment Packets
            bool bIsSegmentPacket = false;
            string xSegmentInsertionInfo = "";
            DataTable oDT = (DataTable)this.grdSelSegs.DataSource;

            if (this.ftvSegments.Value.EndsWith(".0") || !this.ftvSegments.Value.Contains("."))
            {
                //GLOG 6966: Make sure string can be parsed as a Double even if
                //the Decimal separator is something other than '.' in Regional Settings
                string xSegmentID = ftvSegments.Value;
                if (xSegmentID.EndsWith(".0"))
                {
                    xSegmentID = xSegmentID.Replace(".0", "");
                }

                //adding an admin segment
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID((int)double.Parse(xSegmentID)); //GLOG 6966

                //GLOG : 6255 : ceh
                bIsSegmentPacket = (oDef.TypeID == mpObjectTypes.SegmentPacket);

                if (!bIsSegmentPacket)
                {
                    Segment.InsertionLocations iLoc = (Segment.InsertionLocations)Enum.Parse(
                    typeof(Segment.InsertionLocations), oDef.DefaultDoubleClickLocation.ToString());
                    Segment.InsertionBehaviors iBehaviors = (Segment.InsertionBehaviors)(int)oDef.DefaultMenuInsertionBehavior;

                    //DataTable oDT = (DataTable)this.grdSelSegs.DataSource;
                    oDT.Rows.Add(oDef.ID.ToString(), oDef.DisplayName, GetInsertionOptionText(iLoc),
                        (iBehaviors & Segment.InsertionBehaviors.InsertInSeparateNextPageSection) == Segment.InsertionBehaviors.InsertInSeparateNextPageSection,
                        (iBehaviors & Segment.InsertionBehaviors.RestartPageNumbering) == Segment.InsertionBehaviors.RestartPageNumbering,
                        (iBehaviors & Segment.InsertionBehaviors.KeepExistingHeadersFooters) == Segment.InsertionBehaviors.KeepExistingHeadersFooters);

                    iMenuInsertionOptions = oDef.MenuInsertionOptions;
                }
                else
                //GLOG : 6255 : ceh - deal with Segment Packets
                {
                    xSegmentInsertionInfo = oDef.XML;

                    //parse out save/deautomate flags from xml if they exist -
                    //this was added for Pierce InsertUsingClientMatter for segment packets
                    if (!string.IsNullOrEmpty(xSegmentInsertionInfo) &&
                        xSegmentInsertionInfo.Substring(1, 1) == "|" &&
                        xSegmentInsertionInfo.Substring(3, 1) == "|")
                    {
                        xSegmentInsertionInfo = xSegmentInsertionInfo.Substring(4);
                    }

                    string[] aDetails = xSegmentInsertionInfo.Split('~');

                    foreach (string xDetail in aDetails)
                    {
                        string[] aIDs = xDetail.Split(';');
                        Segment.InsertionLocations iLoc = (Segment.InsertionLocations)int.Parse(aIDs[2]);

                        int iBehaviorValue = (aIDs[3] == "True" ? 1 : 0) +
                            (aIDs[4] == "True" ? 4 : 0) + (aIDs[5] == "True" ? 2 : 0);

                        Segment.InsertionBehaviors iBehaviors = (Segment.InsertionBehaviors)iBehaviorValue;

                        oDT.Rows.Add(aIDs[1], aIDs[0], GetInsertionOptionText(iLoc),
                            (iBehaviors & Segment.InsertionBehaviors.InsertInSeparateNextPageSection) == Segment.InsertionBehaviors.InsertInSeparateNextPageSection,
                            (iBehaviors & Segment.InsertionBehaviors.RestartPageNumbering) == Segment.InsertionBehaviors.RestartPageNumbering,
                            (iBehaviors & Segment.InsertionBehaviors.KeepExistingHeadersFooters) == Segment.InsertionBehaviors.KeepExistingHeadersFooters);
                    }

                }                
            }
            else
            {
                //adding a user segment
                UserSegmentDefs oDefs = new UserSegmentDefs();
                UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(this.ftvSegments.Value);

                //GLOG : 6255 : ceh
                bIsSegmentPacket = (oDef.TypeID == mpObjectTypes.SegmentPacket);

                if (!bIsSegmentPacket)
                {
                    Segment.InsertionLocations iLoc = (Segment.InsertionLocations)Enum.Parse(
                        typeof(Segment.InsertionLocations), oDef.DefaultDoubleClickLocation.ToString());

                    //GLOG : 8079 : ceh - not sure why this was here in the first place
                    //if (this.grdSelSegs.Rows.Count > 1 && iLoc == Segment.InsertionLocations.InsertAtSelection)
                    //{
                    //    iLoc = Segment.InsertionLocations.InsertInNewDocument;
                    //}

                    Segment.InsertionBehaviors iBehaviors = (Segment.InsertionBehaviors)(int)oDef.DefaultMenuInsertionBehavior;

                    //DataTable oDT = (DataTable)this.grdSelSegs.DataSource;
                    oDT.Rows.Add(oDef.ID, oDef.DisplayName, GetInsertionOptionText(iLoc),
                        (iBehaviors & Segment.InsertionBehaviors.InsertInSeparateNextPageSection) == Segment.InsertionBehaviors.InsertInSeparateNextPageSection,
                        (iBehaviors & Segment.InsertionBehaviors.RestartPageNumbering) == Segment.InsertionBehaviors.RestartPageNumbering,
                        (iBehaviors & Segment.InsertionBehaviors.KeepExistingHeadersFooters) == Segment.InsertionBehaviors.KeepExistingHeadersFooters);

                    iMenuInsertionOptions = oDef.MenuInsertionOptions;
                }
                else
                //GLOG : 6255 : ceh - deal with Segment Packets
                {
                    xSegmentInsertionInfo = oDef.XML;

                    //parse out save/deautomate flags from xml if they exist -
                    //this was added for Pierce InsertUsingClientMatter for segment packets
                    if (!string.IsNullOrEmpty(xSegmentInsertionInfo) &&
                        xSegmentInsertionInfo.Substring(1, 1) == "|" &&
                        xSegmentInsertionInfo.Substring(3, 1) == "|")
                    {
                        xSegmentInsertionInfo = xSegmentInsertionInfo.Substring(4);
                    }

                    string[] aDetails = xSegmentInsertionInfo.Split('~');

                    foreach (string xDetail in aDetails)
                    {
                        string[] aIDs = xDetail.Split(';');
                        Segment.InsertionLocations iLoc = (Segment.InsertionLocations)int.Parse(aIDs[2]);

                        int iBehaviorValue = (aIDs[3] == "True" ? 1 : 0) +
                            (aIDs[4] == "True" ? 4 : 0) + (aIDs[5] == "True" ? 2 : 0);
                        Segment.InsertionBehaviors iBehaviors = (Segment.InsertionBehaviors)iBehaviorValue;

                        oDT.Rows.Add(aIDs[1], aIDs[0], GetInsertionOptionText(iLoc),
                            (iBehaviors & Segment.InsertionBehaviors.InsertInSeparateNextPageSection) == Segment.InsertionBehaviors.InsertInSeparateNextPageSection,
                            (iBehaviors & Segment.InsertionBehaviors.RestartPageNumbering) == Segment.InsertionBehaviors.RestartPageNumbering,
                            (iBehaviors & Segment.InsertionBehaviors.KeepExistingHeadersFooters) == Segment.InsertionBehaviors.KeepExistingHeadersFooters);
                    }
                }
            }
            
            //GLOG : 8513 : CEH
            //GLOG : 8372 : JSW
            if (oDT.Rows.Count > 0)
            {
                //change title to show numbering of segments
                this.lblSelectedSegments.Text = "Selected Segments (" + oDT.Rows.Count.ToString() + "):";
            }

            this.btnSave.Enabled = true;
            this.btnOK.Enabled = true;
        }
        //private void AddSegment()
        //{
        //    int iMenuInsertionOptions = 0;

        //    //GLOG item #6162 - dcf
        //    if (this.ftvSegments.MemberList.Count == 0)
        //        return;

        //    if (this.ftvSegments.Value.EndsWith(".0") || !this.ftvSegments.Value.Contains("."))
        //    {
        //        //GLOG 6966: Make sure string can be parsed as a Double even if
        //        //the Decimal separator is something other than '.' in Regional Settings
        //        string xSegmentID = ftvSegments.Value;
        //        if (xSegmentID.EndsWith(".0"))
        //        {
        //            xSegmentID = xSegmentID.Replace(".0", "");
        //        }

        //        //adding an admin segment
        //        AdminSegmentDefs oDefs = new AdminSegmentDefs();
        //        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID((int)double.Parse(xSegmentID)); //GLOG 6966
        //        Segment.InsertionLocations iLoc = (Segment.InsertionLocations)Enum.Parse(
        //            typeof(Segment.InsertionLocations), oDef.DefaultDoubleClickLocation.ToString());
        //        Segment.InsertionBehaviors iBehaviors = (Segment.InsertionBehaviors)(int)oDef.DefaultMenuInsertionBehavior;

        //        DataTable oDT = (DataTable)this.grdSelSegments.DataSource;
        //        oDT.Rows.Add(oDef.ID.ToString(), oDef.DisplayName, GetInsertionOptionText(iLoc),
        //            (iBehaviors & Segment.InsertionBehaviors.InsertInSeparateNextPageSection) == Segment.InsertionBehaviors.InsertInSeparateNextPageSection,
        //            (iBehaviors & Segment.InsertionBehaviors.RestartPageNumbering) == Segment.InsertionBehaviors.RestartPageNumbering,
        //            (iBehaviors & Segment.InsertionBehaviors.KeepExistingHeadersFooters) == Segment.InsertionBehaviors.KeepExistingHeadersFooters);

        //        iMenuInsertionOptions = oDef.MenuInsertionOptions;
        //    }
        //    else
        //    {
        //        //adding a user segment
        //        UserSegmentDefs oDefs = new UserSegmentDefs();
        //        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(this.ftvSegments.Value);
        //        Segment.InsertionLocations iLoc = (Segment.InsertionLocations)Enum.Parse(
        //            typeof(Segment.InsertionLocations), oDef.DefaultDoubleClickLocation.ToString());

        //        if (this.grdSelSegments.Rows.Count > 1 && iLoc == Segment.InsertionLocations.InsertAtSelection)
        //        {
        //            iLoc = Segment.InsertionLocations.InsertInNewDocument;
        //        }

        //        Segment.InsertionBehaviors iBehaviors = (Segment.InsertionBehaviors)(int)oDef.DefaultMenuInsertionBehavior;

        //        DataTable oDT = (DataTable)this.grdSelSegments.DataSource;
        //        oDT.Rows.Add(oDef.ID, oDef.DisplayName, GetInsertionOptionText(iLoc),
        //            (iBehaviors & Segment.InsertionBehaviors.InsertInSeparateNextPageSection) == Segment.InsertionBehaviors.InsertInSeparateNextPageSection,
        //            (iBehaviors & Segment.InsertionBehaviors.RestartPageNumbering) == Segment.InsertionBehaviors.RestartPageNumbering,
        //            (iBehaviors & Segment.InsertionBehaviors.KeepExistingHeadersFooters) == Segment.InsertionBehaviors.KeepExistingHeadersFooters);

        //        iMenuInsertionOptions = oDef.MenuInsertionOptions;
        //    }

        //    //GLOG item #6878 - dcf -
        //    //selection must move to newly added row
        //    this.grdSelSegments.ClearSelection();
        //    this.grdSelSegments.Rows[this.grdSelSegments.Rows.Count - 2].Selected = true;

        //    this.btnSave.Enabled = true;
        //    this.btnOK.Enabled = true;
        //}

        public void SetupDialog(string xOKButtonCaption, string xSegmentInsertionData, bool bShowSavePacketButton, TaskPane oParentTaskPane)
        {
            InitializeControls(xOKButtonCaption, xSegmentInsertionData, bShowSavePacketButton, false, false); //GLOG 8069
            m_oTaskPane = oParentTaskPane;
        }
        //GLOG 8069: Added Prefill parameters
        public void SetupDialog(string xOKButtonCaption, string xSegmentInsertionData, bool bShowSavePacketButton, TaskPane oParentTaskPane, bool bShowSaveDeautomateOptions, bool bShowPrefillButton, Prefill oPrefill)
        {
            m_oTaskPane = oParentTaskPane;
            m_oPrefill = oPrefill;
            InitializeControls(xOKButtonCaption, xSegmentInsertionData, bShowSavePacketButton, bShowSaveDeautomateOptions, bShowPrefillButton);
        }
        public void SetupDialog(string xOKButtonCaption, string xSegmentInsertionData, bool bShowSavePacketButton, bool bShowSaveDeautomateOptions)
        {
            InitializeControls(xOKButtonCaption, xSegmentInsertionData, bShowSavePacketButton, bShowSaveDeautomateOptions, false); //GLOG 8069
        }
        /// <summary>
        /// returns a string array of the specified available locations
        /// </summary>
        /// <param name="iOptions"></param>
        /// <returns></returns>
        private string[] GetInsertionOptionsFromBitwiseInt(int iOptions)
        {
            string xOptions = "";

            if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfCurrentSection) == (int)Segment.InsertionLocations.InsertAtStartOfCurrentSection)
                xOptions = "Start of Current Section|";
            if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfCurrentSection) == (int)Segment.InsertionLocations.InsertAtEndOfCurrentSection)
                xOptions = xOptions + "End of Current Section|";
            if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfAllSections) == (int)Segment.InsertionLocations.InsertAtStartOfAllSections)
                xOptions = xOptions + "Start of All Sections|";
            if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfAllSections) == (int)Segment.InsertionLocations.InsertAtEndOfAllSections)
                xOptions = xOptions + "End of All Sections|";
            if ((iOptions & (int)Segment.InsertionLocations.InsertAtSelection) == (int)Segment.InsertionLocations.InsertAtSelection)
                xOptions = xOptions + "Insertion Point|";
            if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfDocument) == (int)Segment.InsertionLocations.InsertAtStartOfDocument)
                xOptions = xOptions + "Start of Document|";
            if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfDocument) == (int)Segment.InsertionLocations.InsertAtEndOfDocument)
                xOptions = xOptions + "End of Document|";
            if ((iOptions & (int)Segment.InsertionLocations.InsertInNewDocument) == (int)Segment.InsertionLocations.InsertInNewDocument)
                xOptions = xOptions + "New Document|";
            if ((iOptions & (int)Segment.InsertionLocations.Default) == (int)Segment.InsertionLocations.Default)
                xOptions = xOptions + "Default|";

            return xOptions.TrimEnd('|').Split('|');
        }

        /// <summary>
        /// returns the text of the specified insertion location
        /// </summary>
        /// <param name="iLocation"></param>
        /// <returns></returns>
        private string GetInsertionOptionText(Segment.InsertionLocations iLocation)
        {
            switch (iLocation)
            {
                case Segment.InsertionLocations.InsertAtStartOfCurrentSection:
                    return "Start of Current Section";
                case Segment.InsertionLocations.InsertAtEndOfCurrentSection:
                    return "End of Current Section";
                case Segment.InsertionLocations.InsertAtStartOfAllSections:
                    return "Start of All Sections";
                case Segment.InsertionLocations.InsertAtEndOfAllSections:
                    return "End of All Sections";
                case Segment.InsertionLocations.InsertAtSelection:
                    return "Insertion Point";
                case Segment.InsertionLocations.InsertAtStartOfDocument:
                    return "Start of Document";
                case Segment.InsertionLocations.InsertAtEndOfDocument:
                    return "End of Document";
                case Segment.InsertionLocations.InsertInNewDocument:
                    return "New Document";
                default:
                    return "Default";
            }
        }

        /// <summary>
        /// returns the text of the specified insertion location
        /// </summary>
        /// <param name="iLocation"></param>
        /// <returns></returns>
        private Segment.InsertionLocations GetInsertionOptionFromText(string xOptionText)
        {
            switch (xOptionText)
            {
                case "Start of Current Section":
                    return Segment.InsertionLocations.InsertAtStartOfCurrentSection;
                case "End of Current Section":
                    return Segment.InsertionLocations.InsertAtEndOfCurrentSection;
                case "Start of All Sections":
                    return Segment.InsertionLocations.InsertAtStartOfAllSections;
                case "End of All Sections":
                    return Segment.InsertionLocations.InsertAtEndOfAllSections;
                case "Insertion Point":
                    return Segment.InsertionLocations.InsertAtSelection;
                case "Start of Document":
                    return Segment.InsertionLocations.InsertAtStartOfDocument;
                case "End of Document":
                    return Segment.InsertionLocations.InsertAtEndOfDocument;
                case "New Document":
                    return Segment.InsertionLocations.InsertInNewDocument;
                default:
                    return Segment.InsertionLocations.Default;
            }
        }

        /// <summary>
        /// refreshes the dropdown items listed in the insertion locations dropdown
        /// </summary>
        /// <param name="iLocations"></param>
        private void RefreshInsertionLocationDropdown(int iLocations)
        {
            string[] oItems = GetInsertionOptionsFromBitwiseInt(iLocations);
            ValueList oList = this.grdSelSegs.DisplayLayout.ValueLists[0];
            oList.ValueListItems.Clear();

            foreach (string oItem in oItems)
            {
                oList.ValueListItems.Add(oItem);
            }

            //DataGridViewComboBoxColumn oCol3 = (DataGridViewComboBoxColumn)this.grdSelSegments.Columns[2];
            //oCol3.Items.Clear();
            //oCol3.Items.AddRange(GetInsertionOptionsFromBitwiseInt(iLocations));
        }

        //GLOG 8069: Added Prefill parameters
        private void InitializeControls(string xOKButtonCaption, string xSegmentInsertionData, bool bShowSavePacketButton, bool bShowSaveDeautomateOptions, bool bShowPrefillButton)
        {
            if (LMP.MacPac.Session.IsInitialized)
            {
                try
                {
                    this.btnSave.Visible = bShowSavePacketButton;
                    this.btnSave.Enabled = false;

                    //GLOG 8069
                    this.btnSavedData.Visible = bShowPrefillButton && m_oPrefill == null;
                    if (m_oPrefill == null)
                        this.lblSavedData.Text = "";
                    else
                        this.lblSavedData.Text = "[Saved Data selected:  " +  m_oPrefill.Name + "]";

                    bool bShowOptions = LMP.Registry.GetMacPac10Value("ShowAdminSegmentPacketSaveDeautomateOptions") == "1";
                    this.chkSaveSegments.Visible = bShowOptions && bShowSaveDeautomateOptions;
                    this.chkDeautomateSegments.Visible = bShowOptions && bShowSaveDeautomateOptions;

                    bool bDeautomate = false;
                    bool bSave = false;

                    if (!string.IsNullOrEmpty(xSegmentInsertionData) && 
                        xSegmentInsertionData.Substring(1, 1) == "|" && xSegmentInsertionData.Substring(3, 1) == "|")
                    {
                        bDeautomate = xSegmentInsertionData.Substring(0, 1) == "1";
                        bSave = xSegmentInsertionData.Substring(2, 1) == "1";
                        xSegmentInsertionData = xSegmentInsertionData.Substring(4);
                    }

                    this.chkDeautomateSegments.Checked = bDeautomate;
                    this.chkSaveSegments.Checked = bSave;

                    this.btnOK.Text = xOKButtonCaption;

                    this.ftvSegments.OwnerID = LMP.Data.Application.User.ID;
					//GLOG : 6255 : ceh - display segment packets
                    this.ftvSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                        LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments |
                        LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders |
                        LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserSegments |
                        LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SegmentPackets;
                    if (!m_bInsertableOnly)
                        this.ftvSegments.DisplayOptions = this.ftvSegments.DisplayOptions | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.MasterData;
                    this.ftvSegments.Expansion = FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
                    this.ftvSegments.ExecuteFinalSetup();

                    this.grdSelSegs.DisplayLayout.Scrollbars = Scrollbars.Vertical;

                    DataTable oDT = new DataTable();
                    oDT.Columns.Add("ID");
                    oDT.Columns.Add("DisplayName");
                    oDT.Columns.Add("InsertionLocation");
                    oDT.Columns.Add("SeparateSection");
                    oDT.Columns.Add("RestartPageNumbering");
                    oDT.Columns.Add("KeepHeadersFooters");

                    //populate with existing data if such data exists
                    if (!string.IsNullOrEmpty(xSegmentInsertionData))
                    {
                        string[] aData = xSegmentInsertionData.Split(ITEM_SEP);

                        foreach (string xData in aData)
                        {
                            string[] aDataItem = xData.Split(NAME_VALUE_SEP);
                            string xOption = GetInsertionOptionText((Segment.InsertionLocations)int.Parse(aDataItem[2]));

                            oDT.Rows.Add(aDataItem[1], aDataItem[0], xOption, aDataItem[3], aDataItem[4], aDataItem[5]);
                        }
                    }

                    this.grdSelSegs.DataSource = oDT;
                    this.grdSelSegs.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True;

                    //GLOG #8492 - dcf
                    int iGridWidth = (int)(this.grdSelSegs.Width * Session.ScreenScalingFactor);

                    UltraGridBand oBand = this.grdSelSegs.DisplayLayout.Bands[0];
                    UltraGridColumn oCol1 = oBand.Columns[0];
                    oCol1.Hidden = true;

                    UltraGridColumn oCol2 = oBand.Columns[1];
                    oCol2.Header.Caption = "Name";
                    oCol2.Width = (int)((double)140 / (double)535 * iGridWidth);
                    oCol2.CellClickAction = CellClickAction.RowSelect;

                    UltraGridColumn oCol3 = oBand.Columns[2];
                    oCol3.Header.Caption = "Insertion Location";
                    oCol3.Width = (int)((double)140 / (double)535 * iGridWidth);
                    oCol3.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                    oCol3.ValueList = this.grdSelSegs.DisplayLayout.ValueLists[0];

                    UltraGridColumn oCol4 = oBand.Columns[3];
                    oCol4.Header.Caption = "In Separate Section";
                    oCol4.Width = (int)((double)70 / (double)535 * iGridWidth);
                    oCol4.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

                    UltraGridColumn oCol5 = oBand.Columns[4];
                    oCol5.Header.Caption = "Restart Page Numbering";
                    oCol5.Width = (int)((double)80 / (double)535 * iGridWidth);
                    oCol5.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

                    UltraGridColumn oCol6 = oBand.Columns[5];
                    oCol6.Header.Caption = "Keep Headers/ Footers";
                    oCol6.Width = (int)((double)90 / (double)535 * iGridWidth);
                    oCol6.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

                    //GLOG : 8532 : ceh
                    this.grdSelSegs.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

                    //DataGridViewColumn oCol1 = new DataGridViewTextBoxColumn();
                    //oCol1.DataPropertyName = "ID";

                    //DataGridViewColumn oCol2 = new DataGridViewTextBoxColumn();
                    //oCol2.DataPropertyName = "DisplayName";
                    //oCol2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    //oCol2.ReadOnly = true;

                    //DataGridViewColumn oCol3 = new DataGridViewComboBoxColumn();
                    //oCol3.DataPropertyName = "InsertionLocation";
                    //oCol3.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    //oCol3.Width = 140;

                    //DataGridViewComboBoxColumn oComboCol3 = (DataGridViewComboBoxColumn)oCol3;
                    //oComboCol3.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton;
                    //oComboCol3.FlatStyle = FlatStyle.Flat;

                    //DataGridViewCheckBoxColumn oCol4 = new DataGridViewCheckBoxColumn(false);
                    //oCol4.DataPropertyName = "SeparateSection";
                    //oCol4.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    //oCol4.Width = 70;
                    //oCol4.ThreeState = false;

                    //DataGridViewCheckBoxColumn oCol5 = new DataGridViewCheckBoxColumn(false);
                    //oCol5.DataPropertyName = "RestartPageNumbering";
                    //oCol5.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    //oCol5.Width = 70;
                    //oCol5.ThreeState = false;

                    //DataGridViewCheckBoxColumn oCol6 = new DataGridViewCheckBoxColumn(false);
                    //oCol6.DataPropertyName = "KeepHeadersFooters";
                    //oCol6.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    //oCol6.Width = 70;
                    //oCol6.ThreeState = false;

                    //this.grdSelSegments.Columns.AddRange(oCol1, oCol2, oCol3, oCol4, oCol5, oCol6);
                    //this.grdSelSegments.Columns[0].Visible = false;
                    //this.grdSelSegments.Columns[1].HeaderText = "Name";
                    //this.grdSelSegments.Columns[2].HeaderText = "Insertion Location";
                    //this.grdSelSegments.Columns[3].HeaderText = "In Separate Section";
                    //this.grdSelSegments.Columns[4].HeaderText = "Restart Page Numbering";
                    //this.grdSelSegments.Columns[5].HeaderText = "Keep Headers/ Footers";
                    //this.grdSelSegments.RowHeadersVisible = false;

                    this.Text = LMP.Resources.GetLangString("Dialog_SegmentTree_Title_Multi");
                    this.lblSelectedSegments.Text = LMP.Resources.GetLangString("Dialog_SegmentTree_SelectedCaption");
                    this.ftvSegments.TreeDoubleClick += new LMP.Controls.TreeDoubleClickEventHandler(ftvSegments_TreeDoubleClick);
                    this.ftvMain.KeyPress += new KeyPressEventHandler(ftvSegments_KeyPress);
                    //GLOG : 6257 : CEH
                    this.ftvSegments.TreeKeyDown += new TreeKeyDownEventHandler(ftvSegments_TreeKeyDown);

                    this.btnOK.Enabled = !string.IsNullOrEmpty(xSegmentInsertionData);
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
        }
        void ftvSegments_TreeDoubleClick(object sender, EventArgs e)
        {
            try
            {
                AddSegment();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Return listbox contents as delimited list of Segment IDs and Display Names
        /// </summary>
        /// <returns></returns>
        private string BuildSegmentList()
        {
            string xTemp = "";
            DataTable oDT = (DataTable)this.grdSelSegs.DataSource;
            for (int i = 0; i < oDT.Rows.Count; i++)
            {
                //get insertion location
                Segment.InsertionLocations iLoc = GetInsertionOptionFromText(oDT.Rows[i]["InsertionLocation"].ToString());
                xTemp = xTemp + oDT.Rows[i]["DisplayName"] + NAME_VALUE_SEP.ToString() + oDT.Rows[i]["ID"] + 
                    NAME_VALUE_SEP.ToString() + ((int)iLoc).ToString() + NAME_VALUE_SEP.ToString() + (oDT.Rows[i]["SeparateSection"].ToString() == "True") +
                    NAME_VALUE_SEP.ToString() + (oDT.Rows[i]["RestartPageNumbering"].ToString() == "True") + NAME_VALUE_SEP.ToString() + (oDT.Rows[i]["KeepHeadersFooters"].ToString() == "True");

                if (i < oDT.Rows.Count - 1)
                    xTemp = xTemp + ITEM_SEP.ToString();
            }
            return xTemp;
        }

	//GLOG : 8164 : ceh - todo: return array?
        /// <summary>
        /// Returns Segment Display Names as comma separated list
        /// </summary>
        /// <returns></returns>
        public string SegmentList()
        {
            string xTemp = "";
            DataTable oDT = (DataTable)this.grdSelSegs.DataSource;
            for (int i = 0; i < oDT.Rows.Count; i++)
            {
                xTemp = xTemp + oDT.Rows[i]["DisplayName"];

                if (i < oDT.Rows.Count - 1)
                    xTemp = xTemp + ", ";
            }
            return xTemp;
        }

        /// <summary>
        /// Hide optional controls and resize
        /// </summary>
        private void SetupControl()
        {
        }

        private void Save()
        {
            //TaskPane oTP = null;
            //Segment oSegment = null;
            string xName = null;
            string xDisplayName = null;
	    //GLOG : 8164 : ceh
            string xDescription = LMP.Resources.GetLangString("Msg_SegmentPacketDescription") + this.SegmentList();

            ContentPropertyForm oSegForm = new ContentPropertyForm();
	    //GLOG : 8164 : ceh
            DialogResult iRes = oSegForm.ShowForm(null, null, new Point(),
                ContentPropertyForm.DialogMode.PromptForSegmentPacketAndFolderInfoOnly, null, null, true, xDescription); //GLOG : 15816 : JSW
            
            if (iRes == DialogResult.OK)
            {
                xName = oSegForm.ContentName;
                xDisplayName = oSegForm.ContentDisplayName;
                xDescription = oSegForm.ContentDescription;

                UserSegmentDefs oDefs = new UserSegmentDefs();
                UserSegmentDef oDef = (UserSegmentDef)oDefs.Create();

                oDef.TypeID = mpObjectTypes.SegmentPacket;
                oDef.XML = this.Value;
                oDef.Name = xName;
                oDef.DisplayName = xDisplayName;
                oDef.HelpText = xDescription;
                oDef.OwnerID = LMP.MacPac.Session.CurrentUser.ID;
                oDef.IntendedUse = mpSegmentIntendedUses.AsAnswerFile;
                oDef.MenuInsertionOptions = 0;
                oDef.DefaultMenuInsertionBehavior = 0;
                oDef.DefaultDoubleClickBehavior = 0;
                oDef.DefaultDoubleClickLocation = (int)Segment.InsertionLocations.Default;
                oDef.DefaultDragBehavior = 0;
                oDef.DefaultDragLocation = 0;
                oDefs.Save(oDef);

                //create user folder member
                string xID = oSegForm.FolderID;
                int iID1, iID2;

                LMP.Data.Application.SplitID(xID, out iID1, out iID2);

                FolderMembers oMembers = new FolderMembers(iID1, iID2);
                FolderMember oMember = (FolderMember)oMembers.Create();

                xID = oDef.ID;
                LMP.Data.Application.SplitID(xID, out iID1, out iID2);
                oMember.ObjectID1 = iID1;
                oMember.ObjectID2 = iID2;
                oMember.ObjectTypeID = mpFolderMemberTypes.UserSegmentPacket;
                oMembers.Save(oMember);

                if (m_oTaskPane != null)
                {
                    m_oTaskPane.Refresh(true, false, false);
                }
            }
        }


        #endregion
        #region *********************properties**************************
        /// <summary>
        /// Returns ID of Segment selected in Treeview, or delimited list of IDs and Names for Multiple selection
        /// </summary>
        public string Value
        {
            get
            {
                string xDeautomate = this.chkDeautomateSegments.Checked ? "1" : "0";
                string xSave = this.chkSaveSegments.Checked ? "1" : "0";
                return xDeautomate + "|" + xSave + "|" + this.BuildSegmentList();
            }
        }
        //GLOG 8069
        public Prefill Prefill
        {
            get
            {
                return m_oPrefill;
            }
        }
        public bool InsertableOnly
        {
            get
            {
                return m_bInsertableOnly;
            }
            set
            {
                m_bInsertableOnly = value;
            }
        }
        #endregion
        #region *********************event handlers**************************
        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (ftvSegments.MemberList.Count > 0)
                    m_xSegmentID = ((ArrayList)ftvSegments.MemberList[0])[0].ToString();
            }
            catch { }

            if (OKClicked != null)
                OKClicked(this, new EventArgs());
        }
        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_xSegmentID = "";

            if (CancelClicked != null)
                CancelClicked(this, new EventArgs());
        }
        /// <summary>
        /// Set options first time form is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MultipleSegmentsManager_Load(object sender, EventArgs e) //GLOG 8163
        {
            SetupControl();

            //temporarily add all options to the dropdown
            RefreshInsertionLocationDropdown(511);
        }
        /// <summary>
        /// Delete selected segment from list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveSelectedSegment()
        {
            try
            {
                if (this.grdSelSegs.ActiveRow == null)
                    return;

                this.grdSelSegs.ActiveRow.Delete(false);

                //GLOG : 8513 : CEH
                //GLOG : 8372 : JSW
                if (this.grdSelSegs.Rows.Count < 1)
                {
                    //change title to show numbering of segments
                    this.lblSelectedSegments.Text = LMP.Resources.GetLangString("Label_SelectedSegments"); ;
                }
                else
                    this.lblSelectedSegments.Text = "Selected Segments (" + grdSelSegs.Rows.Count.ToString() + "):";

                //GLOG 15744
                this.btnOK.Enabled = this.grdSelSegs.Rows.Count > 0;
                this.btnSave.Enabled = this.grdSelSegs.Rows.Count > 0;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnAddSegment_Click(object sender, EventArgs e)
        {
            try
            {
                AddSegment();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6257 : CEH
        void ftvSegments_TreeKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    AddSegment();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdSelSegments_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnDeleteSegment_Click(object sender, EventArgs e)
        {
            try
            {
                RemoveSelectedSegment();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnMoveSegmentUp_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.grdSelSegs.ActiveRow != null)
                {
                    int iRow = this.grdSelSegs.ActiveRow.Index;

                    if (iRow == 0)
                        return;

                    //move up
                    DataTable oDT = (DataTable)this.grdSelSegs.DataSource;
                    DataRow oDR = oDT.Rows[iRow];
                    object[] oItems = oDR.ItemArray;

                    oDT.Rows.RemoveAt(iRow);
                    oDR = oDT.NewRow();
                    oDR.ItemArray = oItems;
                    oDT.Rows.InsertAt(oDR, iRow - 1);
                    this.grdSelSegs.Update();
                    this.grdSelSegs.ActiveRow = this.grdSelSegs.Rows[iRow - 1];
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnMoveSegmentDown_Click(object sender, EventArgs e)
        {
            if (this.grdSelSegs.ActiveRow != null)
            {
                int iRow = this.grdSelSegs.ActiveRow.Index;

                if (iRow == this.grdSelSegs.Rows.Count - 1)
                    return;

                //move up
                DataTable oDT = (DataTable)this.grdSelSegs.DataSource;
                DataRow oDR = oDT.Rows[iRow];
                object[] oItems = oDR.ItemArray;

                oDT.Rows.RemoveAt(iRow);
                oDR = oDT.NewRow();
                oDR.ItemArray = oItems;
                oDT.Rows.InsertAt(oDR, iRow + 1);
                this.grdSelSegs.Update();
                this.grdSelSegs.ActiveRow = this.grdSelSegs.Rows[iRow + 1];
            }
        }

        private void grdSelSegments_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            LMP.Error.Show(e.Exception);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void ftvSegments_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void ftvSegments_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        {
            //GLOG item #6375 - dcf
            try
            {
                this.btnAddSegment.Enabled = this.ftvSegments.MemberList.Count > 0;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void ftvSegments_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                //GLOG : 15743 : jsw
                m_bIsDoubleClick = true;
                AddSegment();
                m_bIsDoubleClick = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdSelSegs_BeforeEnterEditMode(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.grdSelSegs.ActiveRow == null)
                    return;
                else if (this.grdSelSegs.ActiveCell.Column.Index != 2)
                {
                    //not in the Insertion Locations column
                    return;
                }

                int iRow = this.grdSelSegs.ActiveRow.Index;

                if (iRow == iCurSelRow)
                {
                    //row has not changed - return
                    return;
                }
                else
                {
                    iCurSelRow = iRow;
                }

                try
                {
                    //update locations dropdown with choices appropriate for selected segment
                    string xID = this.grdSelSegs.ActiveRow.Cells[0].Value.ToString();

                    if (xID.EndsWith(".0") || !xID.Contains("."))
                    {
                        //GLOG 6966: Make sure string can be parsed as a Double even if
                        //the Decimal separator is something other than '.' in Regional Settings
                        if (xID.EndsWith(".0"))
                        {
                            xID = xID.Replace(".0", "");
                        }

                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID((int)double.Parse(xID));
                        RefreshInsertionLocationDropdown(oDef.MenuInsertionOptions);
                    }
                    else
                    {
                        UserSegmentDefs oDefs = new UserSegmentDefs();
                        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xID);

                        //GLOG item #6165 - dcf
                        int iOptions = oDef.MenuInsertionOptions;
                        if (iOptions == 0)
                            iOptions = 144;
                        RefreshInsertionLocationDropdown(iOptions);
                    }
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnSavedData_Click(object sender, EventArgs e)
        {
            //GLOG 8069
            try
            {
                //PrefillChooser oForm = new PrefillChooser();
                SegmentForm oForm = new SegmentForm(false, null, "Select Saved Data");
                oForm.DisplayOptions = FolderTreeView.mpFolderTreeViewOptions.Prefills |
                    FolderTreeView.mpFolderTreeViewOptions.UserFolders |
                    FolderTreeView.mpFolderTreeViewOptions.SharedFolders;

                DialogResult iRes = oForm.ShowDialog();

                if (iRes == DialogResult.OK)
                {
                    //get prefill from selection
                    //string xVSetID = oForm.VariableSetID;
                    string xVSetID = oForm.Value;

                    VariableSetDefs oSetDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                    VariableSetDef oSetDef = (VariableSetDef)oSetDefs.ItemFromID(xVSetID);

                    m_oPrefill = new Prefill(oSetDef.ContentString, oSetDef.Name, oSetDef.SegmentID);
                    this.lblSavedData.Text = "[Saved Data selected:  " + m_oPrefill.Name + "]";
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
 
        /// <summary>
        /// Returns false if cursor is not over the node,
        /// </summary>
        /// <param name="oMenu"></param>
        /// <returns></returns>
        //GLOG : 15743 : jsw
        static bool bCursorIsOverNode(Point oNPoint)
        {
            bool bCursorIsOverNode = false;

            Point oPt = Cursor.Position;
            
            if (Math.Abs(oNPoint.X - oPt.X) < 41 && Math.Abs(oNPoint.Y - oPt.Y) < 41)
            {    
                bCursorIsOverNode = true;
            }
            else
            {
                bCursorIsOverNode = false;
            }
            return bCursorIsOverNode;
        }
    
    }
}
