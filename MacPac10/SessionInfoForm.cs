﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class SessionInfoForm : Form
    {
        StringFormat oFormat;
        ArrayList aColumnLefts = new ArrayList();
        ArrayList aColumnWidths = new ArrayList();
        int iCellHeight = 0;
        int iTotalWidth = 0;
        int iRow = 0;
        bool bFirstPage = false; //Used to check whether we are printing first page
        bool bNewPage = false;// Used to check whether we are printing a new page
        int iHeaderHeight = 0;

        bool m_bShowPeople = false;
        public SessionInfoForm()
        {
            InitializeComponent();
        }
        public SessionInfoForm(bool bShowPeople)
        {
            m_bShowPeople = bShowPeople;
            InitializeComponent();
        }
        private void SessionInfoForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (!m_bShowPeople)
                {
                    //No Visible property for TagPage, so remove at run-time
                    this.TabControl1.TabPages.Remove(this.tpPeople);
                }
                LMP.Data.User oUser = LMP.MacPac.Session.CurrentUser;

                if (oUser == null)
                {
                    try
                    {
                        LMP.Data.Application.Login(System.Environment.UserName, false);
                    }
                    catch (System.Exception oE)
                    {
                        if (oE.Message.StartsWith("Could not establish the following database connection:"))
                        {
                            LMP.Error.Show(oE);
                            return;
                        }
                    }
                    oUser = LMP.Data.Application.User;
                }

                string xLastSync = "";

                if (oUser != null)
                {
                    xLastSync = LMP.Data.Application.GetMetadata("LastSync" +
                        LMP.MacPac.Session.CurrentUser.ID);
                }

                string[] aSyncDetails = null;

                if (xLastSync != "")
                {
                    aSyncDetails = xLastSync.Split('|');
                }

                DataTable oDT = new DataTable();
                oDT.Columns.Add("Name");
                oDT.Columns.Add("Value");

                if (oUser != null)
                {
                    oDT.Rows.Add("User ID", oUser != null ? oUser.SystemUserID : "");
                    oDT.Rows.Add("Mode", oUser == null || LMP.Data.Application.AdminMode ? "Administrator" : "User");
                    oDT.Rows.Add("Root Directory", Registry.GetMacPac10Value("RootDirectory"));
                    oDT.Rows.Add("Data Directory", LMP.Data.Application.PublicDataDirectory); //JTS 3/28/13
                    oDT.Rows.Add("Writable Database Directory", LMP.Data.Application.WritableDBDirectory);  //JTS 3/28/13
                    oDT.Rows.Add("Database Creation Time", LMP.Data.Application.GetMetadata("DateCreated"));

                    oDT.Rows.Add("DB Server", oUser.FirmSettings.NetworkDatabaseServer);
                    oDT.Rows.Add("Database", oUser.FirmSettings.NetworkDatabaseName);
                    oDT.Rows.Add("IIS Server", oUser.FirmSettings.IISServerIPAddress);

                    oDT.Rows.Add("Last Synch Time", xLastSync == "" ? "N/A" : aSyncDetails[0]);
                    oDT.Rows.Add("Last Synch Server", xLastSync == "" ? "N/A" : aSyncDetails[1]);
                    oDT.Rows.Add("Last Synch Database", xLastSync == "" ? "N/A" : aSyncDetails[2]);

                    string xBench = Registry.GetMacPac10Value("BenchmarkMode");
                    oDT.Rows.Add("Benchmarking", string.IsNullOrEmpty(xBench) || xBench == "0" ? "No" : "Yes");
                }
                else
                {
                    //no user in db - just display what we can from the registry and metadata table
                    System.Collections.ArrayList oArray = LMP.Data.SimpleDataCollection.GetArray(LMP.Data.LocalConnection.ConnectionObject,
                        "SELECT * FROM Metadata");

                    oDT.Rows.Add("User ID", "NONE");
                    oDT.Rows.Add("Root Directory", Registry.GetMacPac10Value("RootDirectory"));
                    oDT.Rows.Add("Writable Database Directory", LMP.Data.Application.WritableDBDirectory);  //JTS 3/28/13
                    oDT.Rows.Add("Data Directory", LMP.Data.Application.PublicDataDirectory); //JTS 3/28/13
                    string xBench = Registry.GetMacPac10Value("BenchmarkMode");
                    oDT.Rows.Add("Benchmarking",  string.IsNullOrEmpty(xBench) || xBench == "0" ? "No" : "Yes");

                    string xExcludes = ";CustomCode;EncryptionPassword;TagPrefixID;";

                    foreach (object[] oItem in oArray)
                    {
                        if (!xExcludes.Contains(";" + oItem[0].ToString() + ";"))
                            oDT.Rows.Add(oItem[0], oItem[1]);
                    }
                }
                this.grdSessionInfo.DataSource = oDT;
                this.grdSessionInfo.RowHeadersVisible = false;
                this.grdSessionInfo.ColumnHeadersVisible = false;

                this.grdSessionInfo.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.grdSessionInfo.Columns["Value"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //Show tab with People if configured
                if (m_bShowPeople)
                {
                    DataSet oPeopleDS = LMP.Data.SimpleDataCollection.GetDataSet(LMP.Data.LocalConnection.ConnectionObject, 
                        "SELECT p.DisplayName AS [Name], p.UserID AS [Login ID], o.DisplayName AS [Office], p.ID1 AS [MacPac ID] " +
                        "FROM Offices AS o RIGHT JOIN People AS p ON o.ID = p.OfficeID WHERE (((p.UserID)<>'') AND ((p.ID2)=0) AND ((p.UsageState)=1)) ORDER BY p.DisplayName;");
                    this.grdPeople.DataSource = oPeopleDS.Tables[0];
                    this.grdPeople.RowHeadersVisible = false;
                    this.grdPeople.ColumnHeadersVisible = true;
                    grdPeople.Columns[0].Width = this.tpPeople.Width / 3;
                    grdPeople.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    grdPeople.Columns[1].Width = this.tpPeople.Width / 6;
                    grdPeople.Columns[2].Width = this.tpPeople.Width / 6;
                    grdPeople.Columns[3].Width = this.tpPeople.Width / 6;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6756 : jsw
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //GLOG : 6756 : jsw
        private void btnPrint_Click(object sender, EventArgs e)
        {
            //Open the print dialog
            PrintDialog printDialog = new PrintDialog();            
            printDialog.Document = printSessionDocument;
            printDialog.UseEXDialog = true;    
            //Get the document
            if (DialogResult.OK == printDialog.ShowDialog())
            {
                printSessionDocument.DocumentName = "Forte Session Information";
                printSessionDocument.Print();
            }
        }

        //GLOG : 6756 : jsw
        private void printSessionDocument_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                oFormat = new StringFormat();
                oFormat.Alignment = StringAlignment.Near;
                oFormat.LineAlignment = StringAlignment.Center;
                oFormat.Trimming = StringTrimming.EllipsisCharacter;

                aColumnLefts.Clear();
                aColumnWidths.Clear();
                iCellHeight = 0;
                iRow = 0;
                bFirstPage = true;
                bNewPage = true;
 
                // Calculating Total Widths
                iTotalWidth = 0;
                foreach (DataGridViewColumn dgvGridCol in grdSessionInfo.Columns)
                {
                    iTotalWidth += dgvGridCol.Width;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6756 : jsw
        private void printSessionDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                //Set the left margin
                int iLeftMargin = e.MarginBounds.Left;
                //Set the top margin
                int iTopMargin = e.MarginBounds.Top;
                //Whether more pages have to print or not
                bool bMorePagesToPrint = false;
                int iTmpWidth = 0;

                //For the first page to print set the cell width and header height
                if (bFirstPage)
                {
                    foreach (DataGridViewColumn GridCol in grdSessionInfo.Columns)
                    {
                        iTmpWidth = (int)(Math.Floor((double)((double)GridCol.Width /
                                       (double)iTotalWidth * (double)iTotalWidth *
                                       ((double)e.MarginBounds.Width / (double)iTotalWidth))));

                        iHeaderHeight = (int)(e.Graphics.MeasureString(GridCol.HeaderText,
                                    GridCol.InheritedStyle.Font, iTmpWidth).Height) + 11;

                        // Save width and height of headres
                        aColumnLefts.Add(iLeftMargin);
                        aColumnWidths.Add(iTmpWidth);
                        iLeftMargin += iTmpWidth;
                    }
                }

                foreach (DataGridViewColumn GridCol in grdSessionInfo.Columns)
                {
                    iTmpWidth = (int)(Math.Floor((double)((double)GridCol.Width /
                                    (double)iTotalWidth * (double)iTotalWidth *
                                    ((double)e.MarginBounds.Width / (double)iTotalWidth))));

                    iHeaderHeight = (int)(e.Graphics.MeasureString(GridCol.HeaderText,
                                GridCol.InheritedStyle.Font, iTmpWidth).Height) + 11;

                    // Save width and height of headres
                    aColumnLefts.Add(iLeftMargin);
                    aColumnWidths.Add(iTmpWidth);
                    iLeftMargin += iTmpWidth;
                }
                //Loop till all the grid rows not get printed
                while (iRow <= grdSessionInfo.Rows.Count - 1)
                {
                    DataGridViewRow GridRow = grdSessionInfo.Rows[iRow];
                    //Set the cell height
                    iCellHeight = GridRow.Height + 5;
                    int iCount = 0;
                    //Check whether the current page settings allo more rows to print
                    if (iTopMargin + iCellHeight >= e.MarginBounds.Height + e.MarginBounds.Top)
                    {
                        bNewPage = true;
                        bFirstPage = false;
                        bMorePagesToPrint = true;
                        break;
                    }
                    else
                    {
                        if (bNewPage)
                        {
                            //Draw Header
                            e.Graphics.DrawString(LMP.ComponentProperties.ProductName + "Session Information", new Font(grdSessionInfo.Font, FontStyle.Bold),
                                    Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top -
                                    e.Graphics.MeasureString(LMP.ComponentProperties.ProductName + "Session Information", new Font(grdSessionInfo.Font,
                                    FontStyle.Bold), e.MarginBounds.Width).Height - 13);

                            string xDate = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString();
                            //Draw Date
                            e.Graphics.DrawString(xDate, new Font(grdSessionInfo.Font, FontStyle.Bold),
                                    Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width -
                                    e.Graphics.MeasureString(xDate, new Font(grdSessionInfo.Font,
                                    FontStyle.Bold), e.MarginBounds.Width).Width), e.MarginBounds.Top -
                                    e.Graphics.MeasureString(LMP.ComponentProperties.ProductName + "Session Information", new Font(new Font(grdSessionInfo.Font,
                                    FontStyle.Bold), FontStyle.Bold), e.MarginBounds.Width).Height - 13);

                            //Draw Columns                 
                            iTopMargin = e.MarginBounds.Top;
                            foreach (DataGridViewColumn GridCol in grdSessionInfo.Columns)
                            {
                                e.Graphics.FillRectangle(new SolidBrush(Color.LightGray),
                                    new Rectangle((int)aColumnLefts[iCount], iTopMargin,
                                    (int)aColumnWidths[iCount], iHeaderHeight));

                                e.Graphics.DrawRectangle(Pens.Black,
                                    new Rectangle((int)aColumnLefts[iCount], iTopMargin,
                                    (int)aColumnWidths[iCount], iHeaderHeight));

                                e.Graphics.DrawString(GridCol.HeaderText, GridCol.InheritedStyle.Font,
                                    new SolidBrush(GridCol.InheritedStyle.ForeColor),
                                    new RectangleF((int)aColumnLefts[iCount], iTopMargin,
                                    (int)aColumnWidths[iCount], iHeaderHeight), oFormat);
                                iCount++;
                            }
                            bNewPage = false;
                            iTopMargin += iHeaderHeight;

                        }
                        iCount = 0;
                        //Draw Columns Contents                
                        foreach (DataGridViewCell Cel in GridRow.Cells)
                        {
                            if (Cel.Value != null)
                            {
                                e.Graphics.DrawString(Cel.Value.ToString(), Cel.InheritedStyle.Font,
                                            new SolidBrush(Cel.InheritedStyle.ForeColor),
                                            new RectangleF((int)aColumnLefts[iCount], (float)iTopMargin,
                                            (int)aColumnWidths[iCount], (float)iCellHeight), oFormat);
                            }
                            //Drawing Cells Borders 
                            e.Graphics.DrawRectangle(Pens.Black, new Rectangle((int)aColumnLefts[iCount],
                                    iTopMargin, (int)aColumnWidths[iCount], iCellHeight));

                            iCount++;
                        }
                        iRow++;
                        iTopMargin += iCellHeight;
                    }

                    //If more lines exist, print another page.
                    if (bMorePagesToPrint)
                        e.HasMorePages = true;
                    else
                        e.HasMorePages = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

    }
    internal class TabControlEx : TabControl
    {
        //Allows hiding Tabs in Tab Control
        protected override void WndProc(ref Message m)
        {
            // Hide tabs by trapping the TCM_ADJUSTRECT message
            if (m.Msg == 0x1328 && !DesignMode)
                if (this.TabPages.Count < 2)
                    m.Result = (IntPtr)1;
                else
                    base.WndProc(ref m);
            else
                base.WndProc(ref m);
        }

    }
}
