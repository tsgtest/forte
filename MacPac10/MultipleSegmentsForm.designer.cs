using LMP.Controls;

namespace LMP.MacPac
{
    partial class MultipleSegmentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucMultipleSegmentManager = new LMP.MacPac.MultipleSegmentsManager();
            this.SuspendLayout();
            // 
            // ucMultipleSegmentManager
            // 
            this.ucMultipleSegmentManager.AutoSize = true;
            this.ucMultipleSegmentManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucMultipleSegmentManager.InsertableOnly = true;
            this.ucMultipleSegmentManager.Location = new System.Drawing.Point(0, 0);
            this.ucMultipleSegmentManager.Name = "ucMultipleSegmentManager";
            this.ucMultipleSegmentManager.Size = new System.Drawing.Size(595, 475);
            this.ucMultipleSegmentManager.TabIndex = 0;
            this.ucMultipleSegmentManager.Load += new System.EventHandler(this.ucMultipleSegmentManager_Load);
            // 
            // MultipleSegmentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(595, 475);
            this.Controls.Add(this.ucMultipleSegmentManager);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MultipleSegmentsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Multiple Segments";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MultipleSegmentsManager ucMultipleSegmentManager;

    }
}