﻿namespace LMP.MacPac
{
    partial class ImportDataFileMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.pnlMessage = new System.Windows.Forms.Panel();
            this.pnlLabel = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnReplace = new System.Windows.Forms.Button();
            this.btnRename = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlIcon = new System.Windows.Forms.Panel();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.pnlMessage.SuspendLayout();
            this.pnlLabel.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitMain.IsSplitterFixed = true;
            this.splitMain.Location = new System.Drawing.Point(0, 0);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.BackColor = System.Drawing.SystemColors.Window;
            this.splitMain.Panel1.Controls.Add(this.pnlMessage);
            this.splitMain.Panel1.ForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitMain.Panel2.Controls.Add(this.btnReplace);
            this.splitMain.Panel2.Controls.Add(this.btnRename);
            this.splitMain.Panel2.Controls.Add(this.btnCancel);
            this.splitMain.Size = new System.Drawing.Size(461, 150);
            this.splitMain.SplitterDistance = 109;
            this.splitMain.SplitterWidth = 1;
            this.splitMain.TabIndex = 4;
            this.splitMain.Paint += new System.Windows.Forms.PaintEventHandler(this.splitMain_Paint);
            // 
            // pnlMessage
            // 
            this.pnlMessage.AutoSize = true;
            this.pnlMessage.BackColor = System.Drawing.Color.Transparent;
            this.pnlMessage.Controls.Add(this.pnlIcon);
            this.pnlMessage.Controls.Add(this.pnlLabel);
            this.pnlMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMessage.Location = new System.Drawing.Point(0, 0);
            this.pnlMessage.Name = "pnlMessage";
            this.pnlMessage.Size = new System.Drawing.Size(461, 109);
            this.pnlMessage.TabIndex = 1;
            this.pnlMessage.Paint += new System.Windows.Forms.PaintEventHandler(this.splitMain_Paint);
            // 
            // pnlLabel
            // 
            this.pnlLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlLabel.BackColor = System.Drawing.Color.Transparent;
            this.pnlLabel.Controls.Add(this.lblMessage);
            this.pnlLabel.Location = new System.Drawing.Point(69, 12);
            this.pnlLabel.Name = "pnlLabel";
            this.pnlLabel.Size = new System.Drawing.Size(385, 88);
            this.pnlLabel.TabIndex = 2;
            // 
            // lblMessage
            // 
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMessage.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblMessage.Location = new System.Drawing.Point(0, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(385, 88);
            this.lblMessage.TabIndex = 2;
            this.lblMessage.Text = "label1";
            // 
            // btnReplace
            // 
            this.btnReplace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReplace.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnReplace.Location = new System.Drawing.Point(156, 8);
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Size = new System.Drawing.Size(89, 23);
            this.btnReplace.TabIndex = 6;
            this.btnReplace.Text = "&Replace";
            this.btnReplace.UseVisualStyleBackColor = true;
            // 
            // btnRename
            // 
            this.btnRename.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRename.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnRename.Location = new System.Drawing.Point(259, 8);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(89, 23);
            this.btnRename.TabIndex = 5;
            this.btnRename.Text = "Re&name";
            this.btnRename.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(361, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(89, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlIcon
            // 
            this.pnlIcon.BackColor = System.Drawing.Color.Transparent;
            this.pnlIcon.Location = new System.Drawing.Point(12, 23);
            this.pnlIcon.Name = "pnlIcon";
            this.pnlIcon.Size = new System.Drawing.Size(40, 40);
            this.pnlIcon.TabIndex = 3;
            // 
            // ImportDataFileMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 150);
            this.Controls.Add(this.splitMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportDataFileMessageBox";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ImportDataFileMessageBox";
            this.Load += new System.EventHandler(this.ImportDataFileMessageBox_Load);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel1.PerformLayout();
            this.splitMain.Panel2.ResumeLayout(false);
            this.splitMain.ResumeLayout(false);
            this.pnlMessage.ResumeLayout(false);
            this.pnlLabel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.Panel pnlMessage;
        private System.Windows.Forms.Panel pnlLabel;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnReplace;
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlIcon;

    }
}