using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    internal partial class ParentAssignmentsForm : Form
    {
        #region *********************constants*********************
        const int ORIG_STATE_COL = 0;
        const int ITEM_ID_COL = 1;
        #endregion
        #region *********************fields*********************
        private mpObjectTypes m_iAssignedObjectType;
        private object[,] m_aStateArray;

        // GLOG : 3047 : JAB
        // Declare an array to store the items that exists for a given filter.
        private object[] m_aFilteredStateArray;

        // GLOG : 3047 : JAB
        // Introduce a field for the table containing the current assignments.

        DataTable m_oDT = null;
        #endregion
        #region *********************constructors*********************
        public ParentAssignmentsForm(int iTargetObjectID,
           mpObjectTypes iAssignedObjectType)
        {
            try
            {
                InitializeComponent();

                //set field values
                this.m_iAssignedObjectType = iAssignedObjectType;

                //get available assignments of the specified type
                m_oDT = Assignments.GetParentAssignments(
                     iTargetObjectID, iAssignedObjectType);

                //create state array to store ID and original state
                m_aStateArray = new object[m_oDT.Rows.Count, 2];

                // GLOG : 3047 : JAB
                // Create an array to store the items that exists for a given filter.
                m_aFilteredStateArray = new object[m_oDT.Rows.Count];

                //cycle through table rows, adding each to
                //the list and state array
                int i = 0;
                foreach (DataRow oDR in m_oDT.Rows)
                {
                    //add to list
                    this.lstAssignments.Items.Add(oDR["DisplayName"].ToString());

                    //add original state and ID to storage array
                    m_aStateArray[i, ITEM_ID_COL] = oDR["ID"];

                    //this.clstAssignments.SetItemChecked(i, true);

                    // GLOG : 3047 : JAB
                    // Store the id of the items in this initial view.
                    this.m_aFilteredStateArray[i] = oDR["ID"];

                    i++;
                }

                // GLOG : 3047 : JAB
                // Bring the first checked item into view.
                //ShowFirstCheckedItem();

                // Localize the form
                LocalizeForm();

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UIException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotInitializeParentAssignmentsForm"), oE);
            }
        }
        #endregion
        #region *********************event handlers*********************
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************event handlers*********************
        #endregion
        

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Localize the form.
        /// </summary>
        private void LocalizeForm()
        {
            this.btnSearch.Text = LMP.Resources.GetLangString("AssignmentsFormSearch");
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Handle the enter key as a trigger to perform the search.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchForAssignment();
            }
            e.Handled = (e.KeyCode == Keys.Enter);
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Search for the assignment specified in the search text box.
        /// </summary>
        private void SearchForAssignment()
        {
            string xSearchText = this.txtSearch.Text;

            DataRow[] oRows = m_oDT.Select("DisplayName LIKE '%" + xSearchText + "%'");

            this.lstAssignments.Items.Clear();
            // Set the checked boxs based on the state stored in the assignments table.
            int i = 0;
            foreach (DataRow oDR in oRows)
            {
                this.lstAssignments.Items.Add(oDR["DisplayName"].ToString());

                // Store the id of each item so that it can be used to update the
                // corresponding row in the assignments table.
                this.m_aFilteredStateArray[i] = oDR["ID"];

                i++;
            }
            // Bring the first checked item into view.
            ShowFirstItem();
            return;
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Display the first items that is checked.
        /// </summary>
        private void ShowFirstItem()
        {
            if (this.lstAssignments.Items.Count > 0)
            {
                {
                    this.lstAssignments.SetSelected(0, true);
                }
            }
        }
        
        /// <summary>
        /// GLOG : 3047 : JAB
        /// Perform a search when the search button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.SearchForAssignment();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}