﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class ProxiesForm : Form
    {
        public ProxiesForm()
        {
            InitializeComponent();
        }

        private void ProxiesForm_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.proxiesManager1.Enabled = true;
            }
        }
    }
}
