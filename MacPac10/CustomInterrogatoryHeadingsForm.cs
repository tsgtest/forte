﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class CustomInterrogatoryHeadingsForm : Form
    {
        public CustomInterrogatoryHeadingsForm()
        {
            InitializeComponent();
        }

        public string CustomQuestion
        {
            get { return this.txtCustomQuestion.Text; }
            set { this.txtCustomQuestion.Text = value; }
        }

        public string CustomResponse
        {
            get { return this.txtCustomResponse.Text; }
            set { this.txtCustomResponse.Text = value; }
        }

        private void txtCustomQuestion_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //this.btnOK.Enabled = !(string.IsNullOrEmpty(this.txtCustomQuestion.Text) ||
                //        string.IsNullOrEmpty(this.txtCustomResponse.Text));
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void txtCustomResponse_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //this.btnOK.Enabled = !(string.IsNullOrEmpty(this.txtCustomQuestion.Text) ||
                //    string.IsNullOrEmpty(this.txtCustomResponse.Text));
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void CustomInterrogatoryHeadingsForm_Activated(object sender, EventArgs e)
        {
            //this.btnOK.Enabled = !(string.IsNullOrEmpty(this.txtCustomQuestion.Text) ||
            //    string.IsNullOrEmpty(this.txtCustomResponse.Text));
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtCustomQuestion.Text = "";
            this.txtCustomResponse.Text = "";
        }
    }
}
