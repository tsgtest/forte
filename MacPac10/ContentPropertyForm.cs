using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using LMP.Architect.Api;
using LMP.Data;
using LMP.Controls;
using System.Xml;


namespace LMP.MacPac
{
    internal partial class ContentPropertyForm : Form
    {
        //TODO: remove content creation functionality from form.  Also, consider
        //converting all the different dialog modes into different actual forms.
        //We would create a base form that contained common functionality, and
        //derived the specific forms from that.
        #region *********************fields*********************
        private ISegmentDef m_oSegmentDef = null;
        private DialogMode m_iMode;
        private ObjectTypes m_iObjectType;
        private UltraTreeNode m_oRelatedNode = null;
        private UserFolder m_oNewUserFolder = null;
        private Folder m_oNewAdminFolder = null;
        private FolderMember m_oSourceMember = null;
        private Prefill m_oPrefill = null;
        private string m_xSegmentID = null;
        private bool m_bCancel = true;
        private Segment m_oSegment = null;
        private bool m_bExecuteOnFinish = true;
        //GLOG : 8152 : JSW
        private bool m_bAllowFolderMenu = false;
        //GLOG 8376
        private bool m_bSegmentDesigner = false;
        #endregion
        #region *****************enumerations*******************
        public enum DialogMode
        {
            NewSegment = 1,
            NewParagraphTextSegment = 2,
            ClonedSegment = 3,
            NewFolder = 4,
            EditProperties = 5,
            CreatePrefill = 6,
            NewStyleSheet = 7,
            ClonedStyleSheet = 8,
            CreateLegacyPrefill = 9,
            NewAnswerFile = 10,
            ClonedAnswerFile = 11,
            SavedContent = 12,
            NewSentenceTextSegment = 13,
            RenameImportedSegment = 14, //GLOG 5826
            PromptForSegmentInfoOnly = 15,
            PromptForSegmentAndFolderInfoOnly = 16,
            NewMasterDataForm = 17,
            NewSegmentPacket = 18,
            PromptForSegmentPacketAndFolderInfoOnly = 19  //GLOG : 8164 : ceh
        }
        internal enum ObjectTypes
        {
            UserSegment = 1,
            AdminSegment = 2,
            UserFolder = 3,
            AdminFolder = 4,
            Prefill = 5
        }
        #endregion
        #region ****************constructors********************
        public ContentPropertyForm()
        {
            InitializeComponent();
            //GLOG 8329
            this.btnAddHelpText.Visible = false;
        }

        #endregion
        #region *****************properties*********************
        public ISegmentDef NewSegmentDef
        {
            get { return m_oSegmentDef; }
            set { m_oSegmentDef = value; }
        }
        public UserFolder NewUserFolder
        {
            get { return m_oNewUserFolder; }
            set { m_oNewUserFolder = value; }
        }
        public Folder NewAdminFolder
        {
            get { return m_oNewAdminFolder; }
            set { m_oNewAdminFolder = value; }
        }
        internal DialogMode Mode
        {
            get { return m_iMode;}
            set { m_iMode = value; }
        }
        internal ObjectTypes ObjectType
        {
            get { return m_iObjectType; }
            set { m_iObjectType = value; }
        }
        private UltraTreeNode RelatedNode
        {
            get { return m_oRelatedNode; }
            set { m_oRelatedNode = value; }
        }
        private Segment Segment
        {
            get { return m_oSegment; }
            set { m_oSegment = value; }
        }
        public bool Canceled
        {
            get { return m_bCancel; }
        }
        public string ContentDisplayName
        {
            get { return this.txtDisplayName.Text; }
        }
        public string ContentName
        {
            get { return this.txtName.Text; }
        }
        public string ContentDescription
        {
            get { return this.mltDescription.Text; }
        }
        public string FolderID
        {
            get
            {
                if (ftvLocation.FolderList.Count > 0)
                    return ((ArrayList)ftvLocation.FolderList[0])[0].ToString();
                else
                    return null;
            }
        }

        /// <summary>
        /// returns the selected folder member if one is selected -
        /// otherwise returns null
        /// </summary>
        public FolderMember SelectedFolderMember
        {
            get
            {
                FolderMember oMem = ftvLocation.SelectedNodes[0].Tag as FolderMember;
                if (oMem != null)
                    return oMem;
                else
                    return null;
            }
        }

        //GLOG : 8152 : JSW
        public bool AllowFolderMenu
        {
            get { return m_bAllowFolderMenu; }
            set
            {
                m_bAllowFolderMenu = value;
            }
        }
        #endregion
        #region **********************methods*******************
        /// <summary>
        /// Setup and Display form and set whether in Segment Creation or Edit Properties mode
        /// </summary>
        /// <param name="oParent"></param>
        /// <param name="oNode"></param>
        /// <param name="oPt"></param>
        /// <param name="bEditMode"></param>
        public DialogResult ShowForm(Form oParent, UltraTreeNode oNode, Point oPt, 
            DialogMode iMode)
        {
            return ShowForm(oParent, oNode, oPt, iMode, null, null);
        }
        public DialogResult ShowForm(Form oParent, UltraTreeNode oNode, Point oPt,
            DialogMode iMode, Prefill oPrefill, Segment oSegment)
        {
            return ShowForm(oParent, oNode, oPt, iMode, oPrefill, oSegment, true);
        }
        public DialogResult ShowForm(Form oParent, UltraTreeNode oNode, Point oPt, 
            DialogMode iMode, Prefill oPrefill, Segment oSegment, bool bExecuteOnFinish)
        {
            return ShowForm(oParent, oNode, oPt, iMode, oPrefill, oSegment, bExecuteOnFinish, "");
        }
        //GLOG : 8305 : ceh
        public DialogResult ShowForm(Form oParent, UltraTreeNode oNode, Point oPt,
            DialogMode iMode, Prefill oPrefill, Segment oSegment, bool bExecuteOnFinish, string xDescription)
        {
            return ShowForm(oParent, oNode, oPt, iMode, oPrefill, oSegment, bExecuteOnFinish, xDescription, false);
        }
        public DialogResult ShowForm(Form oParent, UltraTreeNode oNode, Point oPt,
            DialogMode iMode, Prefill oPrefill, Segment oSegment, bool bExecuteOnFinish, string xDescription, bool bIsSegmentFromSelection)
        {
            return ShowForm(oParent, oNode, oPt, iMode, oPrefill, oSegment, bExecuteOnFinish, xDescription, false, false);
        }
        //GLOG : 8164 : ceh
        //GLOG : 8305 : ceh
        //GLOG 8376: Handler Segment Designer content different from user content
        public DialogResult ShowForm(Form oParent, UltraTreeNode oNode, Point oPt,
            DialogMode iMode, Prefill oPrefill, Segment oSegment, bool bExecuteOnFinish, string xDescription, bool bIsSegmentFromSelection, bool bAsDesigner)
        {
            try
            {
                int i = 0;
                string xDisplayName = "";
                string xTypeName = "";
                string xNewDisplayName = "";

                this.lblDisplayName.Text = LMP.Resources.GetLangString("Dialog_NewSegment_Name");
                this.lblDescription.Text = LMP.Resources.GetLangString("Dialog_NewSegment_Description");
                this.btnCancel.Text = LMP.Resources.GetLangString("Dialog_NewSegment_Cancel");
                this.btnSave.Text = LMP.Resources.GetLangString("Dialog_NewSegment_Save");
                this.RelatedNode = oNode;
                this.Mode = iMode;
                this.Segment = oSegment;
                m_bSegmentDesigner = bAsDesigner; //GLOG 8376

                this.m_bExecuteOnFinish = bExecuteOnFinish;

                switch (this.Mode)
                {
                    case DialogMode.EditProperties:
                        this.txtName.Visible = false;
                        this.lblName.Visible = false;
                        if (oNode.Tag is FolderMember)
                        {
                            FolderMember oMember = (FolderMember)oNode.Tag;
                            //GLOG : 6095 : CEH
                            //this.txtDisplayName.Text = oMember.Name;
                            if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
                            {
                                this.mltDescription.Visible = true;
                                this.lblDescription.Visible = true;
                                VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                                VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(oMember.ObjectID1.ToString() +
                                        "." + oMember.ObjectID2.ToString());
                                //GlOG : 6095 : CEH
                                this.txtDisplayName.Text = oDef.Name;
                                //GLOG 5184 - dcf
                                string xHelpText = oDef.Description.Replace("\n", "\r\n");
                                xHelpText = xHelpText.Replace("_x000a_", "\r\n");
                                this.mltDescription.Text = xHelpText.Replace("<br>", "\r\n");
                                this.Height = this.ftvLocation.Top + 82;
                                this.Height = this.lblFormSizeMarker3.Top;

                                this.ObjectType = ObjectTypes.Prefill;
                                // GLOG : 2994 : JAB
                                // Provide the appropriate title bar text for the dialog box.
                                this.Text = LMP.Resources.GetLangString("Dialog_MySavedData_Properties_TitleBar");
                            }
                            else
                            {
                                // GLOG : 2928 : JAB
                                // Dislay the description multiline text box for Segments.
                                this.mltDescription.Visible = true;
                                this.lblDescription.Visible = true;
                                // GLOG : 2928 : JAB
                                // Show the help text multiline box for segments.
                                this.Height = this.lblDescription.Top + this.mltDescription.Bottom + 30;
                                this.Height = this.lblFormSizeMarker2.Top;
                                
                                if (oMember.FolderType == mpFolderTypes.Admin)
                                {
                                    this.ObjectType = ObjectTypes.AdminSegment;
                                    AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.AdminFolderMembers);
                                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(oMember.ID));
                                    this.txtDisplayName.Text = oDef.DisplayName;
                                    //GLOG 5184 - dcf
                                    string xHelpText = oDef.HelpText.Replace("\n", "\r\n");
                                    xHelpText = xHelpText.Replace("_x000a_", "\r\n");
                                    this.mltDescription.Text = xHelpText.Replace("<br>", "\r\n");

                                    // GLOG : 2994 : JAB
                                    // Provide the appropriate title bar text for the dialog box.
                                    this.Text = LMP.Resources.GetLangString("Dialog_FolderMember_Properties_TitleBar");
                                }
                                else
                                {
                                    this.ObjectType = ObjectTypes.UserSegment;
                                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                                    UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(oMember.ObjectID1.ToString() +
                                                            "." + oMember.ObjectID2.ToString());
                                    this.txtDisplayName.Text = oDef.DisplayName;
                                    //GLOG 5184 - dcf
                                    string xHelpText = oDef.HelpText.Replace("\n", "\r\n");
                                    xHelpText = xHelpText.Replace("_x000a_", "\r\n");
                                    this.mltDescription.Text = xHelpText.Replace("<br>", "\r\n");
                                    
                                    // GLOG : 2994 : JAB
                                    // Provide the appropriate title bar text for the dialog box.
                                    if(oDef.IntendedUse == mpSegmentIntendedUses.AsStyleSheet)
                                    {
                                        this.Text = LMP.Resources.GetLangString("Dialog_MyStyleSheet_Properties_TitleBar");
                                    }
                                    else
                                    {
                                        this.Text = LMP.Resources.GetLangString("Dialog_MyContent_Properties_TitleBar");
                                    }
                                }
                            }
                            this.lblLocation.Visible = false;
                            this.ftvLocation.Visible = false;
                            this.lblFolderDescription.Visible = false;
                        }
                        else
                        {
                            this.Text = LMP.Resources.GetLangString("Dialog_Folder_Properties_TitleBar");
                            this.lblLocation.Visible = false;
                            this.ftvLocation.Visible = false;
                            this.lblFolderDescription.Visible = false;

                            //GLOG #8492 - dcf
                            //this.Height = this.lblLocation.Top + 82; // this.mltDescription.Height - 10;
                            this.Height = this.lblFormSizeMarker2.Top + 40;
                            this.mltDescription.Height = this.btnSave.Top - this.mltDescription.Top - 20;

                            // GLOG : 3017 : JAB
                            // Handle displaying properties for user segments in Shared folders.
                            if (oNode.Tag is UserSegmentDef)
                            {
                                this.ObjectType = ObjectTypes.UserSegment;
                                UserSegmentDef oDef = (UserSegmentDef)oNode.Tag;
                                this.txtDisplayName.Text = oDef.DisplayName;
                                //GLOG 5184 - dcf
                                string xHelpText = oDef.HelpText.Replace("\n", "\r\n");
                                xHelpText = xHelpText.Replace("_x000a_", "\r\n");
                                this.mltDescription.Text = xHelpText.Replace("<br>", "\r\n");
                            }
                            else if (this.RelatedNode.Tag is VariableSetDef)
                            {
                                VariableSetDef oDef = (VariableSetDef)this.RelatedNode.Tag;
                                this.ObjectType = ObjectTypes.Prefill;
                                this.txtDisplayName.Text = oDef.Name;
                                //GLOG 5184 - dcf
                                string xHelpText = oDef.Description.Replace("\n", "\r\n");
                                xHelpText = xHelpText.Replace("_x000a_", "\r\n");
                                this.mltDescription.Text = xHelpText.Replace("<br>", "\r\n");
                            }
                            else if (this.RelatedNode.Tag is Folder)
                            {
                                Folder oFolder = (Folder)oNode.Tag;
                                this.txtDisplayName.Text = oFolder.Name;
                                this.mltDescription.Text = oFolder.Description;
                                this.ObjectType = ObjectTypes.AdminFolder;
                            }
                            else
                            {
                                UserFolder oFolder = (UserFolder)oNode.Tag;
                                this.txtDisplayName.Text = oFolder.Name;
                                this.mltDescription.Text = oFolder.Description;
                                this.ObjectType = ObjectTypes.UserFolder;
                            }
                        }
                        break;
                    case DialogMode.SavedContent:
                        // GLOG : 2810 : CEH
                        // GLOG : 3334 : CEH
                        // Prefill Display Name as we do in the Save Data dialog.

                        if (oSegment != null)
                        {
                            xDisplayName = oSegment.DisplayName;
                            xTypeName = LMP.Data.Application.GetObjectTypeDisplayName(oSegment.TypeID);
                        }

                        string xBaseName = LMP.Resources.GetLangString("Dialog_CreatePrefillContent_DefaultName")
                        .Replace("{SegmentName}", xDisplayName);

                        do
                        {
                            i++;
                            xNewDisplayName = xBaseName + " " + i.ToString();
                        }
                        while (LMP.Data.Application.NamedContentExists(xNewDisplayName, Session.CurrentUser.ID));
                        this.txtDisplayName.Text = xNewDisplayName;

                        this.mltDescription.Visible = true;
                        // GLOG : 2808 : JAB
                        // Display "Created By" and "Created On" as we do in the Save Data dialog.
                        xDisplayName = oSegment.DisplayName;
                        xTypeName = LMP.Data.Application.GetObjectTypeDisplayName(oSegment.TypeID);
                        this.mltDescription.Text =
                            LMP.Resources.GetLangString("Msg_SavedDataCreatedBy") + Session.CurrentUser.Name + "\r\n" +
                            LMP.Resources.GetLangString("Msg_SavedDataCreatedOn") + System.DateTime.Now.ToString() + "\r\n" +
                            LMP.Resources.GetLangString("Msg_SavedDataType") + xTypeName + "\r\n" +
                            LMP.Resources.GetLangString("Msg_SavedContentDescription") + xDisplayName;

                        this.lblDescription.Visible = true;
                        this.lblName.Visible = false;
                        this.txtName.Visible = false;
                        this.lblLocation.Visible = true;
                        this.ftvLocation.Visible = true;
                        //GLOG : 6206 : CEH
                        this.lblFolderDescription.Visible = true;
                        //GLOG : 8152 : JSW
                        this.AllowFolderMenu = true;
                        this.Height = 550;
                        this.Height = this.lblFormSizeMarker3.Top + 40;
                        this.mltDescription.Height = this.lblLocation.Top - this.mltDescription.Top - 10;

                        this.Text = LMP.Resources.GetLangString("Dialog_SaveSegment_TitleBar");
                        this.ObjectType = ObjectTypes.UserSegment;
                        this.ftvLocation.OwnerID = Session.CurrentUser.ID;
                        // GLOG : 3071 : JAB
                        // Show only user segments when saving Content with data.
                        this.ftvLocation.DisplayOptions = FolderTreeView.mpFolderTreeViewOptions.UserFolders | 
                            FolderTreeView.mpFolderTreeViewOptions.UserSegments;
                        //GLOG : 8152 : jsw
                        this.ftvLocation.ShowTopUserFolderNode = true;
                        this.ftvLocation.ExecuteFinalSetup();
                        try
                        {
                            //attempt to select first folder in My Folders-
                            //ignore if unsuccessful
                            SelectDefaultPrefillFolder();
                        }
                        catch { }

                        break;
                    case DialogMode.ClonedSegment:
                    case DialogMode.ClonedStyleSheet:
                    case DialogMode.NewParagraphTextSegment:
                    case DialogMode.NewSentenceTextSegment:
                    case DialogMode.NewSegment:
                    case DialogMode.NewStyleSheet:
                    case DialogMode.NewAnswerFile:
                    case DialogMode.ClonedAnswerFile:
                    case DialogMode.RenameImportedSegment: //GLOG 5826
                    case DialogMode.NewMasterDataForm:
                        // GLOG : 2928 : JAB
                        // Allow the user content to enter a description.
                        bool bIsUserContent = IsUserContent(oNode);
                        this.txtName.Visible = !bIsUserContent;
                        this.lblName.Visible = !bIsUserContent;
                        this.mltDescription.Visible = bIsUserContent;
                        this.lblDescription.Visible = bIsUserContent;
                        this.lblLocation.Visible = false;
                        this.ftvLocation.Visible = false;
                        this.lblFolderDescription.Visible = false;

                        //GLOG #8492 - dcf
                        //// GLOG : 2928 : JAB
                        //// User segments will display a help field box. Adjust the height accordingly.
                        //this.Height = bIsUserContent ? this.lblDescription.Top + 216 : this.lblDescription.Top + 128;
                        this.Height = (bIsUserContent ? this.lblFormSizeMarker2.Top : this.lblFormSizeMarker1.Top) + 40;

                        if (oNode.Tag is UserFolder)
                        {
                            //GLOG : 8305 : ceh
                            //Only show checkbox if SegmentFromSelection menu item was chosen
                            if (bIsSegmentFromSelection)
                            {
                                this.btnAddHelpText.Visible = true;
                            }

                            // Glog 2709 : Hide the Name field for user content. 
                            this.txtName.Visible = false;
                            this.lblName.Visible = false;

                            //GLOG : 8305 : ceh - this doesn't apply to user content
                            // Glog 2709 : Adjust the height of this dialog to compensate for the hidden Name field.
                            //int iNameFieldHeight = (this.txtName.Location.Y + this.txtName.Height) // Bottom of Name textbox.
                            //                        - (this.txtDisplayName.Location.Y + this.txtDisplayName.Height); // Bottom of DisplayName textbox.
                            //this.Height -= iNameFieldHeight;

                            ////GLOG #8492 - dcf
                            //this.Height = this.lblFormSizeMarker2.Top + 40;
                            this.mltDescription.Height = this.btnSave.Top - this.mltDescription.Top - 20;

                            //Creating new UserSegment
                            if (this.Mode == DialogMode.NewStyleSheet)
                                this.Text = LMP.Resources.GetLangString("Dialog_NewStyleSheet_TitleBar");
                            else if (this.Mode == DialogMode.RenameImportedSegment) //GLOG 5826
                            {
                                //Titlebar set externally
                            }
                            else
                                this.Text = LMP.Resources.GetLangString("Dialog_NewSegment_TitleBar");
                            this.ObjectType = ObjectTypes.UserSegment;

                            // GLOG : 8171 : ceh
                            // Provide the appropriate description text for the dialog box.
                            switch (this.Mode)
                            {
                                case DialogMode.NewParagraphTextSegment:
                                    xDisplayName = "Paragraph Text";
                                    break;
                                case DialogMode.NewSentenceTextSegment:
                                    xDisplayName = "Sentence Text";
                                    break;
                                case DialogMode.NewAnswerFile:
                                case DialogMode.ClonedAnswerFile:
                                    xDisplayName = "Answer File";
                                    break;
                                case DialogMode.RenameImportedSegment:
                                case DialogMode.NewSegment:
                                case DialogMode.ClonedSegment:
                                    xDisplayName = "Document";
                                    break;
                                case DialogMode.ClonedStyleSheet:
                                case DialogMode.NewStyleSheet:
                                    xDisplayName = "Style Sheet";
                                    break;
                                case DialogMode.NewMasterDataForm:
                                    xDisplayName = "Master Data Form";
                                    break;
                                default:
                                    xDisplayName = "";
                                    break;
                            }
                            //GLOG 8376
                            if (!bAsDesigner)
                            {
                                this.mltDescription.Text = this.mltDescription.Text +
                                                            LMP.Resources.GetLangString("Msg_SavedDataCreatedBy") + Session.CurrentUser.Name + "\r\n" +
                                                            LMP.Resources.GetLangString("Msg_SavedDataCreatedOn") + System.DateTime.Now.ToString() + "\r\n" +
                                                            LMP.Resources.GetLangString("Msg_SavedDataType") + "User Segment\r\n" +
                                                            LMP.Resources.GetLangString("Msg_SegmentIntendedUseDescription") + xDisplayName;
                            }
                        }
                        else if (oNode.Tag is Folder)
                        {
                            //Creating new AdminSegment
                            switch (this.Mode)
                            {
                                case DialogMode.NewStyleSheet:
                                    this.Text = LMP.Resources.GetLangString("Dialog_NewStyleSheet_TitleBar");
                                    break;
                                case DialogMode.NewAnswerFile:
                                    this.Text = LMP.Resources.GetLangString("Dialog_NewAnswerFile_TitleBar");
                                    break;
                                case DialogMode.NewMasterDataForm:
                                    this.Text = LMP.Resources.GetLangString("Dialog_NewMasterDataForm_TitleBar");
                                    break;
                                case DialogMode.RenameImportedSegment: //GLOG 5826
                                    //Titlebar set externally
                                    break;
                                default:
                                    this.Text = LMP.Resources.GetLangString("Dialog_NewSegment_TitleBar");
                                    break;
                            }

                            this.ObjectType = ObjectTypes.AdminSegment;
                        }
                        else if (this.Mode == DialogMode.ClonedSegment)
                        {
                            //Cloning existing segment
                            this.Text = LMP.Resources.GetLangString("Dialog_NewSegment_TitleBar");
                            m_oSourceMember = (FolderMember)oNode.Tag;
                            this.txtDisplayName.Text = LMP.Resources.GetLangString("Prompt_CopyOf")
                                + oNode.Text;
                            if (m_oSourceMember.FolderType == mpFolderTypes.Admin)
                                this.ObjectType = ObjectTypes.AdminSegment;
                            else
                                this.ObjectType = ObjectTypes.UserSegment;
                        }
                        else if (this.Mode == DialogMode.ClonedStyleSheet)
                        {
                            //Cloning existing style sheet
                            this.Text = LMP.Resources.GetLangString("Dialog_NewStyleSheet_TitleBar");
                            m_oSourceMember = (FolderMember)oNode.Tag;
                            this.txtDisplayName.Text = LMP.Resources.GetLangString("Prompt_CopyOf")
                                + oNode.Text;
                            if (m_oSourceMember.FolderType == mpFolderTypes.Admin)
                                this.ObjectType = ObjectTypes.AdminSegment;
                            else
                                this.ObjectType = ObjectTypes.UserSegment;
                        }
                        else if (this.Mode == DialogMode.ClonedAnswerFile)
                        {
                            //Cloning existing style sheet
                            this.Text = LMP.Resources.GetLangString("Dialog_NewAnswerFile_TitleBar");
                            m_oSourceMember = (FolderMember)oNode.Tag;
                            this.txtDisplayName.Text = LMP.Resources.GetLangString("Prompt_CopyOf")
                                + oNode.Text;
                            if (m_oSourceMember.FolderType == mpFolderTypes.Admin)
                                this.ObjectType = ObjectTypes.AdminSegment;
                            else
                                this.ObjectType = ObjectTypes.UserSegment;
                        }
                        break;
                    case DialogMode.PromptForSegmentInfoOnly:
                    case DialogMode.PromptForSegmentAndFolderInfoOnly:
                    case DialogMode.PromptForSegmentPacketAndFolderInfoOnly:	//GLOG : 8164 : ceh
                        {
                            bool bShowFolders = false;
			                //GLOG : 8164 : ceh
                            string xTitleBar = null;
                            bIsUserContent = true;
                            switch (this.Mode)
                            {
                                case DialogMode.PromptForSegmentAndFolderInfoOnly:
                                    bShowFolders = true;
                                    xDisplayName = "User Segment";
                                    xTitleBar = LMP.Resources.GetLangString("Dialog_NewSegment_TitleBar");
                                    break;
                                case DialogMode.PromptForSegmentPacketAndFolderInfoOnly:
                                    bShowFolders = true;
                                    xDisplayName = "Segment Packet";
                                    xTitleBar = LMP.Resources.GetLangString("Dialog_NewSegmentPacket_TitleBar");
                                    break;
                                case DialogMode.PromptForSegmentInfoOnly:
                                    xDisplayName = "User Segment";
                                    xTitleBar = LMP.Resources.GetLangString("Dialog_NewSegment_TitleBar");
                                    break;
                                default:
                                    xDisplayName = "User Segment";
                                    xTitleBar = LMP.Resources.GetLangString("Dialog_NewSegment_TitleBar");
                                    break;
                            }

                            xDescription = (xDescription == "" ? LMP.Resources.GetLangString("lblGroupDescription") : xDescription);

                            this.txtName.Visible = false;
                            this.lblName.Visible = false;
                            this.mltDescription.Visible = true;
                            this.lblDescription.Visible = true;
                            this.lblLocation.Visible = bShowFolders;
                            this.ftvLocation.Visible = bShowFolders;
                            this.lblFolderDescription.Visible = bShowFolders;

                            this.ftvLocation.OwnerID = Session.CurrentUser.ID;

                            //GLOG : 8972 : jsw
                            if (this.Mode == DialogMode.PromptForSegmentPacketAndFolderInfoOnly)
                                this.ftvLocation.DisplayOptions = FolderTreeView.mpFolderTreeViewOptions.UserFolders | FolderTreeView.mpFolderTreeViewOptions.SegmentPackets;
                            else
                                this.ftvLocation.DisplayOptions = FolderTreeView.mpFolderTreeViewOptions.UserFolders;

                            //GLOG 15807 (dm) - account for possibility of no existing user folders
                            if (this.Mode != DialogMode.PromptForSegmentInfoOnly)
                            {
                                this.ftvLocation.ShowTopUserFolderNode = true;
                                this.AllowFolderMenu = true;
                                this.ftvLocation.Expansion = FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
                            }
                            else
                                this.ftvLocation.Expansion = FolderTreeView.mpFolderTreeViewExpansion.ExpandNone;
                            this.ftvLocation.ExecuteFinalSetup();
                            try
                            {
                                //attempt to select first folder in My Folders-
                                //ignore if unsuccessful
                                SelectDefaultPrefillFolder();
                            }
                            catch { }

                            if (!bShowFolders)
                            {
                                // User segments will display a help field box. Adjust the height accordingly.
                                this.Height = this.mltDescription.Top + this.mltDescription.Height + 80;
                            }

                            this.Text = xTitleBar;
                            this.ObjectType = ObjectTypes.UserSegment;

                            this.mltDescription.Text = LMP.Resources.GetLangString("Msg_SavedDataCreatedBy") + Session.CurrentUser.Name + "\r\n" +
                                                        LMP.Resources.GetLangString("Msg_SavedDataCreatedOn") + System.DateTime.Now.ToString() + "\r\n" +
                                                        LMP.Resources.GetLangString("Msg_SavedDataType") + xDisplayName + "\r\n" +
                                                        xDescription;
                            break;
                        }
                    case DialogMode.NewSegmentPacket:
                        {
                            bool bShowFolders = this.Mode == DialogMode.PromptForSegmentAndFolderInfoOnly;
			                //GLOG : 8164 : ceh
                            bIsUserContent = IsUserContent(oNode);
                            this.txtName.Visible = !bIsUserContent;
                            this.lblName.Visible = !bIsUserContent;
                            //GLOG : 8847 : jsw
                            this.mltDescription.Visible = true;
                            this.lblDescription.Visible = true;

                            this.lblLocation.Visible = bShowFolders;
                            this.ftvLocation.Visible = bShowFolders;
                            this.lblFolderDescription.Visible = bShowFolders;

                            this.ftvLocation.OwnerID = Session.CurrentUser.ID;
                            this.ftvLocation.DisplayOptions = FolderTreeView.mpFolderTreeViewOptions.UserFolders;
                            this.ftvLocation.Expansion = FolderTreeView.mpFolderTreeViewExpansion.ExpandNone;
                            this.ftvLocation.ExecuteFinalSetup();
                            try
                            {
                                //attempt to select first folder in My Folders-
                                //ignore if unsuccessful
                                SelectDefaultPrefillFolder();
                            }
                            catch { }

                            if (!bShowFolders)
                            {
                                // User segments will display a help field box. Adjust the height accordingly.
                                //GLOG : 8510 : jsw 
                                //GLOG 8543
                                this.Height = (bIsUserContent ?  this.lblFormSizeMarker2.Top : this.lblFormSizeMarker1.Top) + 40;
                                if (bIsUserContent)
                                {
                                    this.mltDescription.Height = this.btnSave.Top - this.mltDescription.Top - 20;
                                }
                                //GLOG : 8847 : jsw
                                //adjust to display name, description and help controls
                                if (!bIsUserContent)
                                {
                                    this.Height = this.Height + 100;
                                    this.lblDescription.Top = this.lblName.Top + 46;
                                    this.mltDescription.Top = this.txtName.Top + 46;
                                    this.mltDescription.Height = this.btnSave.Top - this.mltDescription.Top - 20;

                                }
                            }

                            this.Text = LMP.Resources.GetLangString("Dialog_NewSegmentPacket_TitleBar");

                            if (bIsUserContent)
                            {
                                this.ObjectType = ObjectTypes.UserSegment;

                                this.mltDescription.Text = LMP.Resources.GetLangString("Msg_SavedDataCreatedBy") + Session.CurrentUser.Name + "\r\n" +
                                                            LMP.Resources.GetLangString("Msg_SavedDataCreatedOn") + System.DateTime.Now.ToString() + "\r\n" +
                                                            LMP.Resources.GetLangString("Msg_SavedDataType") + "Segment Packet\r\n" +
                                                            xDescription;
                            }
                            else
                                this.ObjectType = ObjectTypes.AdminSegment;

                            break;
                        }
                    case DialogMode.NewFolder:
                        this.txtName.Visible = false;
                        this.lblName.Visible = false;
                        this.lblLocation.Visible = false;
                        this.ftvLocation.Visible = false;
                        this.lblFolderDescription.Visible = false;

                        //GLOG #8492 - dcf
                        this.mltDescription.Height = this.lblLocation.Top - this.mltDescription.Top - 20;
                        this.Height = this.lblFormSizeMarker2.Top;

                        // Tag will be string "0.0" for MyFolders node
                        if (oNode.Tag is UserFolder || oNode.Tag is string)
                        {
                            //Creating new UserSegment
                            this.Text = LMP.Resources.GetLangString("Dialog_NewFolder_TitleBar");
                            this.ObjectType = ObjectTypes.UserFolder;
                        }
                        else
                        {
                            //Creating new AdminSegment
                            this.Text = LMP.Resources.GetLangString("Dialog_NewFolder_TitleBar");
                            this.ObjectType = ObjectTypes.AdminFolder;
                        }
                        break;
                    case DialogMode.CreatePrefill:
                    case DialogMode.CreateLegacyPrefill:
                        //Creating new Saved Data
                        this.txtName.Visible = false;
                        this.lblName.Visible = false;
                        //GLOG : 8152 : JSW
                        this.AllowFolderMenu = true;
                        m_oPrefill = oPrefill;
                        if (this.Mode == DialogMode.CreatePrefill)
                        {
                            if (oSegment == null && this.RelatedNode != null)
                            {
                                oSegment = (Segment)this.RelatedNode.Tag;
                            }
                            m_xSegmentID = oSegment.ID;
                        }

                        this.Text = LMP.Resources.GetLangString("Dialog_CreatePrefill_TitleBar");
                        this.ObjectType = ObjectTypes.Prefill;

                        if (oSegment != null)
                        {
                            xDisplayName = oSegment.DisplayName;
                            xTypeName = LMP.Data.Application.GetObjectTypeDisplayName(oSegment.TypeID);
                        }

                            xBaseName = LMP.Resources.GetLangString("Dialog_CreatePrefill_DefaultName")
                            .Replace("{SegmentName}", xDisplayName);
                        do
                        {
                            i++;
                            xNewDisplayName = xBaseName + " " + i.ToString();
                        }
                        while (LMP.Data.Application.NamedPrefillExists(xNewDisplayName, Session.CurrentUser.ID));
                        this.txtDisplayName.Text = xNewDisplayName;

                        this.mltDescription.Text = 
                            LMP.Resources.GetLangString("Msg_SavedDataCreatedBy") + Session.CurrentUser.Name + "\r\n" +
                            LMP.Resources.GetLangString("Msg_SavedDataCreatedOn") + System.DateTime.Now.ToString() + "\r\n" +
                            LMP.Resources.GetLangString("Msg_SavedDataType") + xTypeName + "\r\n" +
                            LMP.Resources.GetLangString("Msg_SavedDataDescription") + xDisplayName;

                        //GLOG #8492 - dcf
                        this.mltDescription.Height = this.lblLocation.Top - this.mltDescription.Top - 10;

                        this.ftvLocation.OwnerID = Session.CurrentUser.ID;
                        this.ftvLocation.DisplayOptions = FolderTreeView.mpFolderTreeViewOptions.UserFolders | FolderTreeView.mpFolderTreeViewOptions.Prefills;
                        //this.ftvLocation.Expansion = FolderTreeView.mpFolderTreeViewExpansion.ExpandNone;
                        //GLOG : 8152 : JSW
                        this.ftvLocation.ShowTopUserFolderNode = true;
                        this.ftvLocation.Expansion = FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
                        this.ftvLocation.ExecuteFinalSetup();
                        try
                        {
                            //attempt to select first folder in My Folders-
                            //ignore if unsuccessful
                            SelectDefaultPrefillFolder();
                        }
                        catch { }

                        break;
                }

                //TODO Set startup position based on current mouse coordinates?
                //this.SetBounds(oPt.X, oPt.Y, this.Width, this.Height);
                return this.ShowDialog();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UIException(
                    LMP.Resources.GetLangString("Error_Content_Property_Initialize"), oE);
            }

        }

        /// <summary>
        /// GLOG : 2928 : JAB
        /// Determine if the content destined to be stored within a node is user content.
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private bool IsUserContent(UltraTreeNode oNode)
        {
            // GLOG : 2929 : JAB
            // Include style sheets as user content.
	    // GLOG : 8164 : ceh
	    // Include Segment Packets as user content.
            return (this.Mode == DialogMode.NewSegment || 
                this.Mode == DialogMode.NewStyleSheet || 
                this.Mode == DialogMode.NewParagraphTextSegment ||
                this.Mode == DialogMode.NewSentenceTextSegment ||
                this.Mode == DialogMode.NewSegmentPacket ||
                this.Mode == DialogMode.RenameImportedSegment ) && //GLOG 5826
                oNode.Key.StartsWith("0.0");
        }


        /// <summary>
        /// Set the previously selected prefill folder as the selected prefill folder.
        /// </summary>
        private void SelectDefaultPrefillFolder()
        {
            UltraTreeNode oDefaultNode = null;

            string xDefaultFolder = Session.CurrentUser.UserSettings.DefaultPrefillFolder;

            if (xDefaultFolder.Length != 0)
            {
                try
                {
                    // The key is formatted such that each folder parent's key is separated by a '.'
                    //  such as: 9400.24680.13579
                    // Split the key into its component parent folder keys (eg {"9400", "25680", "13579"}
                    // so that we can navigate down to the folder node.
                    string[] xPathNodeKeys = xDefaultFolder.Split('/');

                    // Start at the root node and navigate down from there.
                    oDefaultNode = this.ftvLocation.Nodes[0];

                    for (int i = 1; i < xPathNodeKeys.Length; i++)
                    {
                        string xPathNodeKey = (string)xPathNodeKeys[i];
                        oDefaultNode = oDefaultNode.Nodes[xPathNodeKey];
                    }
                }
                catch
                {
                    //skip setting the default folder - just select the first folder (below)
                }
            }

            if(oDefaultNode == null)
            {
                oDefaultNode = this.ftvLocation.Nodes[0];
            }

            oDefaultNode.Selected = true;
        }

        /// <summary>
        /// Save the selected prefill folder so that the next time a prefill is created, this folder is selected.
        /// </summary>
        private void SaveSelectedPrefillFolder()
        {
            // Save the selected prefill folder as the default prefill folder.

            UltraTreeNode oPrefillNode = this.ftvLocation.SelectedNodes[0];

            string xRootPrefillNodeKey = "0.0";
            string xSelectedPrefillNodeKey = xRootPrefillNodeKey;
            ArrayList alxPrefillFolderNodeKeys = new ArrayList();

            do
            {
                xSelectedPrefillNodeKey = oPrefillNode.Key;
                alxPrefillFolderNodeKeys.Add(xSelectedPrefillNodeKey);
                oPrefillNode = oPrefillNode.Parent;
            } while (xSelectedPrefillNodeKey != xRootPrefillNodeKey && oPrefillNode != null);

            StringBuilder sbSelectedPrefillFolderNode = new StringBuilder();

            for (int i = alxPrefillFolderNodeKeys.Count - 1; i >= 0; i--)
            {
                if (i < alxPrefillFolderNodeKeys.Count - 1)
                {
                    sbSelectedPrefillFolderNode.Append("/");
                }
                sbSelectedPrefillFolderNode.Append((string)alxPrefillFolderNodeKeys[i]);
            }

            Session.CurrentUser.UserSettings.DefaultPrefillFolder = sbSelectedPrefillFolderNode.ToString();
        }

        /// <summary>
        /// Check that all required fields have been entered
        /// </summary>
        /// <returns></returns>
        private bool ValidateControls()
        {
            if (this.txtDisplayName.Text == "")
            {
                string xMsg;
                if (this.ObjectType == ObjectTypes.AdminFolder || 
                        this.ObjectType == ObjectTypes.UserFolder)
                    xMsg = LMP.Resources.GetLangString("Msg_Folder_Name_Required");
                else if (this.ObjectType == ObjectTypes.Prefill)
                    xMsg = LMP.Resources.GetLangString("Dialog_Prefill_Name_Required");
                else
                    xMsg = LMP.Resources.GetLangString("Msg_Segment_Name_Required");
                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName, 
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.txtDisplayName.Focus();
                return false;
            }
            else if (this.txtName.Visible == true && this.txtName.Text == "")
            {
                //JTS 4/8/13: Don't allow empty Name field
                string xMsg = LMP.Resources.GetLangString("Error_NameFieldCannotBeNull");
                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.txtName.Focus();
                return false;
            }
            else if (this.ObjectType == ObjectTypes.AdminFolder && this.mltDescription.Text == "")
            {
                string xMsg;
                if (this.ObjectType == ObjectTypes.Prefill)
                    xMsg = LMP.Resources.GetLangString("Dialog_Prefill_Description_Required");
                else
                    xMsg = LMP.Resources.GetLangString("Msg_Folder_Description_Required");
                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.mltDescription.Focus();
                return false;
            }
            //GLOG : 8152 : jsw
            //User folder messages for user segments as well as saved data
            //GLOG : 8304 : jsw
            //this if clause should only run if Mode is CreatePrefill, CreateLegacyPrefill or SavedContent
            //if this is the case, AllowFolderMenu will be true.
            else if ((this.ObjectType == ObjectTypes.Prefill || (this.ObjectType == ObjectTypes.UserSegment && this.AllowFolderMenu)) && this.Mode != DialogMode.EditProperties &&
                this.ftvLocation.FolderList.Count == 0)
            {
                // Allow selecting a node containing a FolderMember. This indicates that an existing prefill 
                // was selected to be overwritten. 
                SelectedNodesCollection oNodes = this.ftvLocation.SelectedNodes;
                //GLOG : 8152 : jsw
                //message if no user folder exists
                if ((oNodes[0].Selected && oNodes[0].Tag == "0.0"))
                {
                    if (oNodes[0].Nodes.Count == 0)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_PrefillRequiresUserFolder"),
                           LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        this.ftvLocation.Focus();
                        return false;
                    }
                    else
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_PrefillSaveToUserFolder"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        this.ftvLocation.Focus();
                        return false;
                    }
                }
                else
                {
                    if (oNodes.Count == 0 || !(oNodes[0].Tag is FolderMember))
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Dialog_Prefill_Destination_Required"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        this.ftvLocation.Focus();
                        return false;
                    }
                }
            }

            return true;
        }

        private void ftvLocation_AfterSelect(object sender, SelectEventArgs e)
        {
            UltraTreeNode oFolderNode = null;
            string xFolderDesc = "";
            if (this.ftvLocation.SelectedNodes[0].Tag is FolderMember)
            {
                // The user has selected an existing prefill. 
                // Set the name in the name text box to the name of this existing prefill.
                this.txtDisplayName.Text = this.ftvLocation.SelectedNodes[0].Text;
                oFolderNode = this.ftvLocation.SelectedNodes[0].Parent;
            }
            else
                 oFolderNode = this.ftvLocation.SelectedNodes[0];
            if (oFolderNode != null)
            {
                if (oFolderNode.Tag is UserFolder)
                {
                    UserFolder oUserFolder = (UserFolder)oFolderNode.Tag;
                    xFolderDesc = oUserFolder.Description;
                }
                else if (oFolderNode.Tag is Folder)
                {
                    Folder oFolder = (Folder)oFolderNode.Tag;
                    xFolderDesc = oFolder.Description;
                }
            }
            //Show folder description
            lblFolderDescription.Text = xFolderDesc.Replace("\r\n", " ");
        }
        private UltraTreeNode GetExistingPrefillNode(string xPrefillName)
        {
            // Iterate through the folders to find a prefill with this name.
            UltraTreeNode oPrefillFolderNode;
            
            if (this.ftvLocation.SelectedNodes[0].Tag is FolderMember)
            {
                // The user may have selected the name by selecting an existing prefill. Use
                // the selected prefill's folder as the folder to search in.
                oPrefillFolderNode = this.ftvLocation.SelectedNodes[0].Parent;
            }
            else
            {
                oPrefillFolderNode = this.ftvLocation.SelectedNodes[0];
            }

            //GLOG : 4569 : CEH
            //make sure folder member nodes are initialized
            if (oPrefillFolderNode.Nodes.Count == 0)
                oPrefillFolderNode.ExpandAll(ExpandAllType.Always);
          

            foreach (UltraTreeNode oPrefillFolderMemberNode in oPrefillFolderNode.Nodes)
            {
                if (oPrefillFolderMemberNode.Tag is FolderMember)
                {
                    if (oPrefillFolderMemberNode.Text == xPrefillName)
                    {
                        return oPrefillFolderMemberNode;
                    }
                }
            }

            return null;
        }

        private void ftvLocation_DoubleClick(object sender, EventArgs e)
        {
            if (this.ftvLocation.SelectedNodes[0].Tag is FolderMember)
            {
                // The user has selected an existing prefill. 
                // Set the name in the name text box to the name of this existing prefill.
                this.txtDisplayName.Text = this.ftvLocation.SelectedNodes[0].Text;

                // Save the prefill.
                btnSave_Click(sender, null);
            }
        }

        private bool CreatePrefill()
        {
            int iTries = 0;
            try
            {
                string xDisplayName = this.txtDisplayName.Text;
                if (this.ftvLocation.SelectedNodes[0].Tag is FolderMember)
                {
                    this.ftvLocation.SelectedNodes[0].Parent.Selected = true;
                }
                
                // Get ID of currently selected folder
                string xFolderID = ((ArrayList)ftvLocation.FolderList[0])[0].ToString();
                int iFolderID1 = 0;
                int iFolderID2 = 0;
                LMP.Data.Application.SplitID(xFolderID, out iFolderID1, out iFolderID2);
                FolderMembers oMembers = new FolderMembers(iFolderID1, iFolderID2);

                // Get the node containing the prefill with this def's name in one exists.
                UltraTreeNode oExistingPrefillNode = this.GetExistingPrefillNode(xDisplayName);

                //GLOG : 4569 : CEH
                bool bIsDuplicatedName = false;

                if (oExistingPrefillNode != null)
                {
                    DialogResult iRes = MessageBox.Show(xDisplayName +
                        LMP.Resources.GetLangString("Prompt_OverwriteExistingPrefill"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2);

                    if (iRes != DialogResult.Yes)
                    {
                        // The user chose not to overwrite the existing prefill -
                        // return true so not to indicate that 
                        //there was a problem creating the prefill
                        return true;
                    }
                }
                else 
                {
                    //Duplicate names are not prevented by table structure
                    //So check FolderMembers directly for duplicates of other types
                    for (int i = 1; i <= oMembers.Count; i++)
                    {
                        string xMemberName = ((FolderMember)oMembers[i]).Name;
                        if (xMemberName.ToUpper() == xDisplayName.ToUpper())
                        {
                            bIsDuplicatedName = true;
                            break;
                        }
                    }
                }

                if (bIsDuplicatedName == true)
                {
                    DialogResult iRes = MessageBox.Show(xDisplayName +
                    LMP.Resources.GetLangString("Prompt_OverwriteExistingPrefill"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button2);

                    if (iRes != DialogResult.Yes)
                    {
                        // The user chose not to overwrite the existing prefill -
                        // return true so not to indicate that 
                        //there was a problem creating the prefill
                        return true;
                    }
                }
                
                Prefill oPrefill = null;
                string xSegmentID = "";
                if (m_oPrefill == null)
                {
                    Segment oSegment = this.Segment;
                    oPrefill = new Prefill(oSegment);

                    if (oSegment.RecreateRedirectID != "")
                        xSegmentID = oSegment.RecreateRedirectID;
                    else
                    {
                        xSegmentID = oSegment.ID;
                    }
                }
                else
                {
                    oPrefill = m_oPrefill;
                    xSegmentID = m_xSegmentID;
                }

                VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);

                VariableSetDef oDef;

                if (oExistingPrefillNode != null)
                {
                    // The user has chosen to overwrite the existing prefill. Delete the existing prefill.
                    FolderMember oFolderMember = (FolderMember)oExistingPrefillNode.Tag;
                    string xFolderMemberID = oFolderMember.ObjectID1.ToString() + "." + oFolderMember.ObjectID2.ToString();
                    
                    oDef = (VariableSetDef)oDefs.ItemFromID(xFolderMemberID);
                }
                else
                {
                    // Create a new variableSetDef.
                    oDef = (VariableSetDef)oDefs.Create();
                    StringIDSimpleDataItem oItem = null;
                    try
                    {
                        //Check for existing item with new ID
                        oItem = oDefs.ItemFromID(oDef.ID);
                    }
                    catch { }
                    //Retry if duplicate ID was generated
                    while (iTries < 10 && oItem != null)
                    {
                        oItem = null;
                        oDef = (VariableSetDef)oDefs.Create();
                        try
                        {
                            oItem = oDefs.ItemFromID(oDef.ID);
                        }
                        catch { }
                        iTries++;
                    }
                }

                oDef.Name = xDisplayName;
                oDef.Description = this.mltDescription.Text.Replace("\r\n", "<br>");
                oDef.SegmentID = xSegmentID;
                oDef.OwnerID = Session.CurrentUser.ID;
                oDef.ContentString = oPrefill.ToString();

                oDefs.Save((StringIDSimpleDataItem)oDef);
                oPrefill.VariableSetID = oDef.ID;
                this.Segment.Prefill = oPrefill;

                // Only assign the prefill to a folder if its a new prefill.
                if (oExistingPrefillNode == null)
                {
                    // Assign new prefill to selected user folder
                    FolderMember oMember = (FolderMember)oMembers.Create();
                    iTries = 0;
                    StringIDSimpleDataItem oItem = null;
                    try
                    {
                        //Check for existing item with new ID
                        oItem = oMembers.ItemFromID(oMember.ID);
                    }
                    catch { }
                    //Retry if duplicate ID was generated
                    while (iTries < 10 && oItem != null)
                    {
                        oItem = null;
                        oMember = (FolderMember)oMembers.Create();
                        try
                        {
                            oItem = oMembers.ItemFromID(oMember.ID);
                        }
                        catch { }
                        iTries++;
                    }
                    int iID1 = 0;
                    int iID2 = 0;
                    LMP.Data.Application.SplitID(oDef.ID, out iID1, out iID2);
                    oMember.ObjectID1 = iID1;
                    oMember.ObjectID2 = iID2;
                    oMember.ObjectTypeID = mpFolderMemberTypes.VariableSet;
                    oMembers.Save((StringIDSimpleDataItem)oMember);
                }

                return true;
            }
            catch (LMP.Exceptions.AlreadyInCollectionException)
            {
                MessageBox.Show(
                    LMP.Resources.GetLangString("Msg_NewSegment_DefinitionNotUnique"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtDisplayName.Focus();
                return false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(LMP.Resources.GetLangString("Error_Prefill_Create"), oE);
            }

        }
        /// <summary>
        /// Create new user or admin segment with the properties assigned in dialog
        /// </summary>
        /// <returns></returns>
        private bool CreateNewSegment()
        {
            int iTries = 0;
            try
            {
                //get intended use from dialog mode
                mpSegmentIntendedUses iIntendedUse = mpSegmentIntendedUses.AsDocument;
                Segment.InsertionLocations iLocation = Segment.InsertionLocations.InsertInNewDocument;
                switch (this.Mode)
                {
                    case DialogMode.NewParagraphTextSegment:
                        {
                            iIntendedUse = mpSegmentIntendedUses.AsParagraphText;

                            //GLOG 2543 (dm) - default insertion location for text blocks should be "at selecton"
                            iLocation = Segment.InsertionLocations.InsertAtSelection;
                            break;
                        }
                    case DialogMode.NewSentenceTextSegment:
                        {
                            //GLOG 2543 (dm) - added sentence text mode
                            iIntendedUse = mpSegmentIntendedUses.AsSentenceText;
                            iLocation = Segment.InsertionLocations.InsertAtSelection;
                            break;
                        }
                    case DialogMode.NewStyleSheet:
                    case DialogMode.ClonedStyleSheet:
                        {
                            iIntendedUse = mpSegmentIntendedUses.AsStyleSheet;
                            break;
                        }
                    case DialogMode.NewAnswerFile:
                    case DialogMode.ClonedAnswerFile:
                        {
                            iIntendedUse = mpSegmentIntendedUses.AsAnswerFile;
                            break;
                        }
                    case DialogMode.NewMasterDataForm:
                        {
                            iIntendedUse = mpSegmentIntendedUses.AsMasterDataForm;
                            break;
                        }
                }

                if (this.ObjectType == ObjectTypes.AdminSegment)
                {
                    // Create new Admin Segment
                    AdminSegmentDefs oAdminSegs = new AdminSegmentDefs(mpObjectTypes.Architect);
                    AdminSegmentDef oAdminSeg = (AdminSegmentDef)oAdminSegs.Create();
                    AdminSegmentDef oSourceDef = null;
                    oAdminSeg.Name = this.txtName.Text;
                    oAdminSeg.DisplayName = this.txtDisplayName.Text;
                    oAdminSeg.TranslationID = 0;
                    if (m_oSourceMember != null)
                    {
                        // Copy properties of existing segment
                        oSourceDef = (AdminSegmentDef)oAdminSegs.ItemFromID(m_oSourceMember.ObjectID1);
                        oAdminSeg.L0 = oSourceDef.L0;
                        oAdminSeg.L1 = oSourceDef.L1;
                        oAdminSeg.L2 = oSourceDef.L2;
                        oAdminSeg.L3 = oSourceDef.L3;
                        oAdminSeg.L4 = oSourceDef.L4;
                        oAdminSeg.HelpText = oSourceDef.HelpText;
                        oAdminSeg.MenuInsertionOptions = oSourceDef.MenuInsertionOptions;
                        oAdminSeg.TypeID = oSourceDef.TypeID;
                        oAdminSeg.DefaultDoubleClickBehavior = oSourceDef.DefaultDoubleClickBehavior;
                        oAdminSeg.DefaultDoubleClickLocation = oSourceDef.DefaultDoubleClickLocation;
                        oAdminSeg.DefaultDragBehavior = oSourceDef.DefaultDragBehavior;
                        oAdminSeg.DefaultDragLocation = oSourceDef.DefaultDragLocation;
                        oAdminSeg.DefaultMenuInsertionBehavior = oSourceDef.DefaultMenuInsertionBehavior;
                        oAdminSeg.ChildSegmentIDs = oSourceDef.ChildSegmentIDs;
                        oAdminSeg.IntendedUse = oSourceDef.IntendedUse;
                        oAdminSegs.Save((LongIDSimpleDataItem)oAdminSeg);
                        LMP.Data.Application.CopyFirmKeySetsForObjectID(oSourceDef.ID, oAdminSeg.ID);
                        LMP.Data.Application.CopyAssignmentsForObject(oSourceDef.TypeID, oSourceDef.ID, oAdminSeg.ID);
                        if (oAdminSeg.TypeID != mpObjectTypes.SegmentPacket)
                        {
                            // Update XML after ID is generated
                            oAdminSeg.XML = String.ReplaceSegmentXML(oSourceDef.XML, oSourceDef.ID.ToString(),
                                oAdminSeg.ID.ToString(), oSourceDef.DisplayName, oAdminSeg.DisplayName,
                                oSourceDef.Name, oAdminSeg.Name, oSourceDef.TranslationID.ToString());
                        }
                    }
                    else
                    {
                        //set intended use  - default to document for admin segments
                        oAdminSeg.IntendedUse = iIntendedUse;

                        //GLOG 2543 - this property wasn't getting updated in the definition
                        //to match the object data template used for the xml
                        oAdminSeg.DefaultDoubleClickLocation = (short)iLocation;
                    }

                    oAdminSegs.Save((LongIDSimpleDataItem)oAdminSeg);
                    this.NewSegmentDef = (ISegmentDef)oAdminSeg;
                    return true;
                }
                else
                {
                    // Create new UserSegment
                    string xDisplayName = this.txtDisplayName.Text;
                    UltraTreeNode oExistingPrefillNode = null;
                    if (this.Mode == DialogMode.SavedContent)
                    {
                        SelectedNodesCollection oSelNodes = this.ftvLocation.SelectedNodes;

                        if ((oSelNodes.Count > 0) && (oSelNodes[0].Tag is FolderMember))
                        {
                            oSelNodes[0].Parent.Selected = true;
                        }

                        // Get ID of currently selected folder
                        string xFolderID = ((ArrayList)ftvLocation.FolderList[0])[0].ToString();
                        int iFolderID1 = 0;
                        int iFolderID2 = 0;
                        LMP.Data.Application.SplitID(xFolderID, out iFolderID1, out iFolderID2);
                        FolderMembers oMembers = new FolderMembers(iFolderID1, iFolderID2);

                        // Get the node containing the prefill with this def's name in one exists.
                        oExistingPrefillNode = this.GetExistingPrefillNode(xDisplayName);

                        if (oExistingPrefillNode != null)
                        {
                            DialogResult iRes = MessageBox.Show(xDisplayName +
                                LMP.Resources.GetLangString("Prompt_OverwriteExistingPrefill"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                                MessageBoxDefaultButton.Button2);

                            if (iRes != DialogResult.Yes)
                            {
                                // The user chose not to overwrite the existing prefill -
                                // cancel
                                this.txtDisplayName.Focus();
                                return false;
                            }
                        }
                        else
                        {
                            //Duplicate names are not prevented by table structure
                            //So check FolderMembers directly for duplicates of other types
                            for (int i = 1; i <= oMembers.Count; i++)
                            {
                                string xMemberName = ((FolderMember)oMembers[i]).Name;
                                if (xMemberName.ToUpper() == xDisplayName.ToUpper())
                                    throw new LMP.Exceptions.AlreadyInCollectionException("");
                            }
                        }
                    }
                    else
                    {
                        if (this.RelatedNode != null && this.RelatedNode.Tag is UserFolder)
                        {
                            //Duplicate names are not prevented by table structure
                            //So check FolderMembers directly for duplicates
                            UserFolder oUserFolder = (UserFolder)this.RelatedNode.Tag;
                            FolderMembers oUserMembers = oUserFolder.GetMembers();
                        for (int i = 1; i <= oUserMembers.Count; i++)
                        {
                            string xMemberName = ((FolderMember)oUserMembers[i]).Name;
                            if (xMemberName.ToUpper() == xDisplayName.ToUpper())
                            {
                                DialogResult iChoice = MessageBox.Show(
                                    LMP.Resources.GetLangString("ContentPropertyForm_OverwriteExistingSegment"), 
                                    LMP.ComponentProperties.ProductName, 
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (iChoice == DialogResult.No)
                                    {
                                        return false;
                                    }

                                    // Delete the existing node so that it may be replaced.
                                    oUserMembers.Delete(((FolderMember)oUserMembers[i]).ID);
                                }
                            }
                        }
                    }

                    UserSegmentDefs oUserSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                    //UserSegmentDef oUserSeg = (UserSegmentDef)oUserSegs.Create();

                    UserSegmentDef oUserSeg;

                    if (oExistingPrefillNode != null)
                    {
                        // The user has chosen to overwrite the existing segment
                        FolderMember oFolderMember = (FolderMember)oExistingPrefillNode.Tag;
                        string xFolderMemberID = oFolderMember.ObjectID1.ToString() + "." + oFolderMember.ObjectID2.ToString();

                        oUserSeg = (UserSegmentDef)oUserSegs.ItemFromID(xFolderMemberID);

                    }
                    else
                    {
                        oUserSeg = (UserSegmentDef)oUserSegs.Create();


                        StringIDSimpleDataItem oItem = null;
                        try
                        {
                            //Check for existing item with new ID
                        oItem = oUserSegs.ItemFromID(oUserSeg.ID);
                    }
                    catch { }
                    //Retry if duplicate ID was generated
                    while (iTries < 10 && oItem != null)
                    {
                        oItem = null;
                        oUserSeg = (UserSegmentDef)oUserSegs.Create();
                            try
                            {
                                oItem = oUserSegs.ItemFromID(oUserSeg.ID);
                            }
                            catch { }
                            iTries++;
                        }
                    }
                    UserSegmentDef oSourceDef = null;
                    oUserSeg.OwnerID = Session.CurrentUser.ID;
                    oUserSeg.DisplayName = xDisplayName;
                    //name is display name without the spaces and apostrophes
                    string xName = this.txtName.Text;
                    oUserSeg.Name = xName;

                    if (m_oSourceMember != null)
                    {
                        // Copy properties of existing segment
                        oSourceDef = (UserSegmentDef)oUserSegs.ItemFromID(m_oSourceMember.ObjectID1 + "." + m_oSourceMember.ObjectID2);
                        oUserSeg.HelpText = oSourceDef.HelpText;
                        oUserSeg.IntendedUse = oSourceDef.IntendedUse;
                        oUserSeg.ChildSegmentIDs = oSourceDef.ChildSegmentIDs;
                        oUserSeg.DefaultDoubleClickBehavior = oSourceDef.DefaultDoubleClickBehavior;
                        oUserSeg.DefaultDoubleClickLocation = oSourceDef.DefaultDoubleClickLocation;
                        oUserSeg.DefaultDragBehavior = oSourceDef.DefaultDragBehavior;
                        oUserSeg.DefaultDragLocation = oSourceDef.DefaultDragLocation;
                        oUserSeg.DefaultMenuInsertionBehavior = oSourceDef.DefaultMenuInsertionBehavior;
                        oUserSeg.L0 = oSourceDef.L0;
                        oUserSeg.L1 = oSourceDef.L1;
                        oUserSeg.L2 = oSourceDef.L2;
                        oUserSeg.L3 = oSourceDef.L3;
                        oUserSeg.L4 = oSourceDef.L4;
                        oUserSeg.MenuInsertionOptions = oSourceDef.MenuInsertionOptions;
                        oUserSeg.TypeID = oSourceDef.TypeID;
                        oUserSegs.Save((StringIDSimpleDataItem)oUserSeg);

                        // Update XML after ID is generated
                        oUserSeg.XML = String.ReplaceSegmentXML(oSourceDef.XML, oSourceDef.ID,
                            oUserSeg.ID, oSourceDef.DisplayName, oUserSeg.DisplayName,
                            oSourceDef.Name, oUserSeg.Name, "0");
                    }
                    else
                    {
                        //set intended use to text block
                        oUserSeg.IntendedUse = iIntendedUse;
                        //GLOG 8376
                        if (m_bSegmentDesigner)
                        {
                            oUserSeg.TypeID = mpObjectTypes.Architect;
                            oUserSeg.MenuInsertionOptions = (short)iLocation;
                            oUserSeg.DefaultDoubleClickLocation = (short)iLocation;
                            oUserSeg.DefaultDragLocation = (short)iLocation;
                        }
                        else
                        {
                            oUserSeg.TypeID = mpObjectTypes.UserSegment;
                            oUserSeg.MenuInsertionOptions = (int)Segment.InsertionLocations.InsertAtSelection;
                            oUserSeg.DefaultDoubleClickLocation = (int)Segment.InsertionLocations.InsertAtSelection;
                            oUserSeg.DefaultDragLocation = (int)Segment.InsertionLocations.InsertAtSelection;
                        }
                        //GLOG : 8050 : jsw
                        //replace double quotes in help text
                        string xHelpText = this.ContentDescription;
                        xHelpText = xHelpText.Replace("\"", "&quot;");
                        oUserSeg.HelpText = xHelpText;
                    }

                    oUserSegs.Save((StringIDSimpleDataItem)oUserSeg);
                    this.NewSegmentDef = (ISegmentDef)oUserSeg;
                    return true;
                }
            }
            catch (LMP.Exceptions.StoredProcedureException oE)
            {
                MessageBox.Show(
                    LMP.Resources.GetLangString("Msg_NewSegment_DefinitionNotUnique"), 
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtDisplayName.Focus();
                return false;
            }
            catch (LMP.Exceptions.AlreadyInCollectionException oE)
            {
                MessageBox.Show(
                    LMP.Resources.GetLangString("Msg_NewSegment_DefinitionNotUnique"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtDisplayName.Focus();
                return false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(LMP.Resources.GetLangString("Error_Segment_Create"), oE);
            }
        }
        /// <summary>
        /// Create new user or admin folder with the properties assigned in dialog
        /// </summary>
        /// <returns></returns>
        private bool CreateNewFolder()
        {
            int iTries = 0;
            try
            {
                if (this.ObjectType == ObjectTypes.UserFolder)
                {
                    string xID1, xID2;
                    if (this.RelatedNode.Tag is string)
                    {
                        // Parent Folder is MyFolders node
                        xID1 = "0";
                        xID2 = "0";
                    }
                    else
                    {
                        UserFolder oFolder = (UserFolder)this.RelatedNode.Tag;
                        LMP.String.SplitString(oFolder.ID, out xID1, out xID2, ".");
                    }
                    int iHighIndex = 0;
                    UserFolders oFolders = new UserFolders(Session.CurrentUser.ID, Int32.Parse(xID1),
                        Int32.Parse(xID2));
                    iHighIndex = oFolders.GetHighIndex();
                    UserFolder oNewFolder = (UserFolder)oFolders.Create(Session.CurrentUser.ID,
                        Int32.Parse(xID1), Int32.Parse(xID2));
                    StringIDSimpleDataItem oItem = null;
                    try
                    {
                        // Check for a user folder by the same name
                        ArrayList oDisplay = oFolders.ToDisplayArray();

                        for (int i = 0; i < oDisplay.Count; i++)
                        {
                            object [] oFolderDetails = (object [])oDisplay[i];

                            if (this.txtDisplayName.Text == (string)oFolderDetails[0])
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Error_FolderAlreadyExists") + this.txtDisplayName.Text,
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                                this.txtDisplayName.Focus();
                                this.txtDisplayName.SelectionLength = txtDisplayName.Text.Length;
                                return false;
                            }
                        }

                        //Check for existing item with new ID
                        oItem = oFolders.ItemFromID(oNewFolder.ID);
                    }
                    catch { }
                    //Retry if duplicate ID was generated
                    while (iTries < 10 && oItem != null)
                    {
                        oItem = null;
                        oNewFolder = (UserFolder)oFolders.Create(Session.CurrentUser.ID,
                            Int32.Parse(xID1), Int32.Parse(xID2));
                        try
                        {
                            oItem = oFolders.ItemFromID(oNewFolder.ID);
                        }
                        catch { }
                        iTries++;
                    }

                    oNewFolder.Name = this.txtDisplayName.Text;
                    oNewFolder.Description = this.mltDescription.Text;
                    oNewFolder.Index = iHighIndex + 1;
                    oFolders.Save((StringIDSimpleDataItem)oNewFolder);
                    this.NewUserFolder = oNewFolder;
                    return true;
                }
                else 
                {
                    int iParentID = 0;
                    int iHighIndex = 0;
                    if (this.RelatedNode.Tag is Folder)
                    {
                        iParentID =  ((Folder)this.RelatedNode.Tag).ID;
                    }
                    else if (this.RelatedNode.Tag is int)
                    {
                        iParentID = (int)this.RelatedNode.Tag;
                    }
                    Folders oFolders = new Folders(0, iParentID);

                    // GLOG : 2926 : JAB
                    // Disallow admin folders of the same name.
                    try
                    {
                        // Check for an admin folder by the same name
                        ArrayList oDisplay = oFolders.ToDisplayArray();

                        for (int i = 0; i < oDisplay.Count; i++)
                        {
                            object[] oFolderDetails = (object[])oDisplay[i];

                            if (this.txtDisplayName.Text == (string)oFolderDetails[0])
                            {
                                MessageBox.Show(LMP.Resources.GetLangString("Error_FolderAlreadyExists") + this.txtDisplayName.Text,
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                                this.txtDisplayName.Focus();
                                this.txtDisplayName.SelectionLength = txtDisplayName.Text.Length;
                                return false;
                            }
                        }
                    }
                    catch { }

                    iHighIndex = oFolders.GetHighIndex();
                    Folder oNewFolder = (Folder)oFolders.Create(iParentID);
                    oNewFolder.Name = this.txtDisplayName.Text;
                    oNewFolder.Description = this.mltDescription.Text;
                    oNewFolder.Index = iHighIndex + 1;
                    oFolders.Save((LongIDSimpleDataItem)oNewFolder);

                    //// Make new folder visible to all by default
                    //oNewFolder.GrantPermission(-999999999);

                    //GLOG item #5984 - dcf -
                    //inherit permissions of parent folder
                    if (iParentID > 0)
                    {
                        Folder oParentFolder = (Folder)oFolders.ItemFromID(iParentID);
                        ArrayList oPermissions = oParentFolder.Permissions();

                        foreach (object[] oPermission in oPermissions)
                        {
                            int iPermission = (int)oPermission[0];
                            oNewFolder.GrantPermission(iPermission);
                        }
                    }
                    else
                    {
                        //grant permission to everyone
                        oNewFolder.GrantPermission(-999999999);
                    }
                    this.NewAdminFolder = oNewFolder;
                    return true;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(LMP.Resources.GetLangString("Error_Folder_Create"), oE);
            }
        }
        /// <summary>
        /// In Edit mode, save entered values back to object and associated tree node
        /// </summary>
        /// <returns></returns>
        private bool SaveProperties()
        {
            try
            {
                if (this.RelatedNode.Tag is FolderMember)
                {
                    FolderMember oMember = (FolderMember)this.RelatedNode.Tag;
                    if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
                    {
                        //Save Variable Set properties
                        VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                        VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(oMember.ObjectID1.ToString()
                            + "." + oMember.ObjectID2.ToString());
                        oDef.Name = this.txtDisplayName.Text;
                        oDef.Description = this.mltDescription.Text;
                        oDefs.Save((StringIDSimpleDataItem)oDef);
                        this.RelatedNode.Text = oDef.Name;
                    }
                    else if (oMember.ObjectTypeID == mpFolderMemberTypes.UserSegment)
                    {
                        UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(oMember.ObjectID1.ToString()
                            + "." + oMember.ObjectID2.ToString());

                        UpdatePropertiesInXML(oDef, this.txtDisplayName.Text, this.mltDescription.Text);

                        oDef.DisplayName = this.txtDisplayName.Text;
                        oDef.HelpText = this.mltDescription.Text;

                        oDefs.Save((StringIDSimpleDataItem)oDef);
                        this.RelatedNode.Text = oDef.DisplayName;
                    }
                    //GLOG item #6382 - dcf
                    else if (oMember.ObjectTypeID == mpFolderMemberTypes.UserSegmentPacket)
                    {
                        UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(oMember.ObjectID1.ToString()
                            + "." + oMember.ObjectID2.ToString());

                        oDef.DisplayName = this.txtDisplayName.Text;
                        oDef.HelpText = this.mltDescription.Text;

                        oDefs.Save((StringIDSimpleDataItem)oDef);
                        this.RelatedNode.Text = oDef.DisplayName;
                    }

                    return true;
                }
                else if (this.RelatedNode.Tag is Folder)
                {
                    Folder oFolder = (Folder)this.RelatedNode.Tag;
                    oFolder.Name = this.txtDisplayName.Text;
                    oFolder.Description = this.mltDescription.Text;
                    Folders oFolders = new Folders();
                    oFolders.Save((LongIDSimpleDataItem)oFolder);
                    this.RelatedNode.Text = oFolder.Name;
                }
                // GLOG : 3017 : JAB
                // Handle saving properties for user segments in Shared folders.
                else if (this.RelatedNode.Tag is UserSegmentDef)
                {
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                    UserSegmentDef oDef = (UserSegmentDef)RelatedNode.Tag;

                    UpdatePropertiesInXML(oDef, this.txtDisplayName.Text, this.mltDescription.Text);

                    oDef.DisplayName = this.txtDisplayName.Text;
                    oDef.HelpText = this.mltDescription.Text;

                    oDefs.Save((StringIDSimpleDataItem)oDef);
                    this.RelatedNode.Text = oDef.DisplayName;
                }
                else
                {
                    UserFolder oFolder = (UserFolder)this.RelatedNode.Tag;
                    oFolder.Name = this.txtDisplayName.Text;
                    oFolder.Description = this.mltDescription.Text;
                    UserFolders oFolders = new UserFolders(oFolder.OwnerID);
                    oFolders.Save((StringIDSimpleDataItem)oFolder);
                    this.RelatedNode.Text = oFolder.Name;
                }
                return true;
            }
            catch(System.Exception oE)
            {
                return false;
            }
        }

        /// <summary>
        /// Save the display/help text to the xml of the segment.
        /// </summary>
        /// <param name="xSegmentName"></param>
        /// <param name="oXML"></param>
        /// <param name="xHelpText"></param>
        public void UpdatePropertiesInXML(UserSegmentDef oDef, string xNewSegmentName, string xHelpText)
        {
            // GLOG : 6095 : CEH
            // edited for Open XML compatibility
            XmlNode oSegmentNode = null;
            XmlNode oDocVarNode = null;
            XmlAttribute oSegmentObjectDataAttribute = null;
            string xXPath = "";
            string xDocVarName = "";
            string xObjectData = "";
            string[] axObjectDataParts = null;
            string xParameterName = "";
            int iParamIndex;

            string xSegmentName = oDef.Name;
            string xXML = oDef.XML;
            int iHelpTextIndex = -1;
            int iSegmentDisplayNameIndex = -1;

            // Create the DOM and load it with the segment xml.
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml(xXML);
            
            // deal with Open XML
            if (LMP.String.IsWordOpenXML(xXML))
            {
                XmlNamespaceManager oNamespaceManager = new XmlNamespaceManager(oXML.NameTable);

                // get top-level segment content control
                oNamespaceManager.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);
                xXPath = "//w:sdt/w:sdtPr/w:tag[starts-with(@w:val, 'mps')]";
                oSegmentNode = oXML.SelectSingleNode(xXPath, oNamespaceManager);

                xDocVarName = oSegmentNode.Attributes["w:val"].Value.Substring(0, 11).Replace("mps", "mpo");

                // get doc var
                xXPath = "//w:docVar[@w:name='" + xDocVarName + "']";
                oDocVarNode = oXML.SelectSingleNode(xXPath, oNamespaceManager);

                // get object Data attribute & value
                oSegmentObjectDataAttribute = oDocVarNode.Attributes["w:val"];
                xObjectData = oSegmentObjectDataAttribute.Value;
                
                // parse the object data
                axObjectDataParts = xObjectData.Split('|');

                // Find the Display Name parameter and the HelpTextParameter
                for (iParamIndex = 0; iParamIndex < axObjectDataParts.Length; iParamIndex++)
                {
                    // Check for the HelpText parameter.
                    if (axObjectDataParts[iParamIndex].StartsWith("HelpText="))
                    {
                        iHelpTextIndex = iParamIndex;

                        // If we've also found the Display Name parameter, we're done.
                        if (iSegmentDisplayNameIndex != -1)
                        {
                            break;
                        }
                    }

                    // Check for the display name parameter.
                    if (axObjectDataParts[iParamIndex].StartsWith("DisplayName="))
                    {
                        iSegmentDisplayNameIndex = iParamIndex;

                        // If we've also found the HelpText parameter, we're done.
                        if (iHelpTextIndex != -1)
                        {
                            break;
                        }
                    }
                }

                if (iHelpTextIndex != -1)
                {
                    // Replace the display name.
                    if (iSegmentDisplayNameIndex != -1)
                    {
                        axObjectDataParts[iSegmentDisplayNameIndex] = "DisplayName=" + xNewSegmentName;
                    }

                    // Replace the HelpText with the appropriate value.
                    axObjectDataParts[iHelpTextIndex] = "HelpText=" + xHelpText;

                    // Reconstruct the object data value string.
                    StringBuilder oSB = new StringBuilder();
                    bool bIsDelimiterRequired = false;

                    for (int i = 0; i < axObjectDataParts.Length; i++)
                    {
                        // Append the '|' delimiter after the first object data part has been added.
                        if (bIsDelimiterRequired)
                        {
                            oSB.Append("|");
                        }

                        oSB.Append(axObjectDataParts[i]);
                        bIsDelimiterRequired = true;
                    }

                    // Set the value for the object data attribute.
                    oSegmentObjectDataAttribute.Value = oSB.ToString();

                    // Update the xml in the def.
                    oDef.XML = oXML.OuterXml;
                }
            }
            else
            {
                // deal with Binary
                XmlNamespaceManager oNmSpcMgr = new XmlNamespaceManager(oXML.NameTable);

                oNmSpcMgr.AddNamespace("ns0", "urn-legalmacpac-data/10");

                string xNSPrefix = String.GetNamespacePrefix(xXML, ForteConstants.MacPacNamespace);

                //get all nodes that match the specified XPath query
                XmlNodeList oMSegNodes = oXML.SelectNodes("//" + xNSPrefix + ":mSEG[contains(@ObjectData, 'Name=" + xSegmentName + "')]", oNmSpcMgr);
                
                foreach (XmlNode oMSegNode in oMSegNodes)
                {
                    // Get the object data attribute of the node.
                    oSegmentObjectDataAttribute = oMSegNode.Attributes["ObjectData"];

                    // Parse the object data so that the name can be compared to the segment name.
                    xObjectData = oSegmentObjectDataAttribute.Value;

                    axObjectDataParts = xObjectData.Split('|');

                    xParameterName = "Name=" + xSegmentName;

                    iHelpTextIndex = -1;
                    iSegmentDisplayNameIndex = -1;

                    // Find the Name parameter and the HelpTextParameter
                    for (iParamIndex = 0; iParamIndex < axObjectDataParts.Length; iParamIndex++)
                    {
                        // Check for the HelpText parameter.
                        if (axObjectDataParts[iParamIndex].StartsWith("HelpText="))
                        {
                            iHelpTextIndex = iParamIndex;

                            // If we've found the Name and the Display Name parameter, we're done.
                            if (oSegmentNode != null && iSegmentDisplayNameIndex != -1)
                            {
                                break;
                            }
                        }

                        // Check for the display name parameter.
                        if (axObjectDataParts[iParamIndex].StartsWith("DisplayName="))
                        {
                            iSegmentDisplayNameIndex = iParamIndex;

                            // If we've found the Name and the HelpText parameter, we're done.
                            if (oSegmentNode != null && iHelpTextIndex != -1)
                            {
                                break;
                            }
                        }

                        // Check for the Name parameter.
                        if (axObjectDataParts[iParamIndex].StartsWith("Name="))
                        {
                            if (axObjectDataParts[iParamIndex] == xParameterName)
                            {
                                // We've found the segment we're are searching for.
                                oSegmentNode = oMSegNode;

                                // If we've found the index for the help text parameter we're done.
                                // Otherwise keep iterating through the params until we find it.
                                if (iHelpTextIndex != -1 && iSegmentDisplayNameIndex != -1)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                // This is not the segment we're looking for. Break so that
                                // we can iterate through other nodes.
                                break;
                            }
                        }
                    }

                    if (oSegmentNode != null)
                    {
                        // We've found the segment node. Stop interating through the segment nodes.
                        break;
                    }
                }

                if (oSegmentNode != null)
                {
                    if(iHelpTextIndex != -1)
                    {
                        // Replace the display name.
                        if (iSegmentDisplayNameIndex != -1)
                        {
                            axObjectDataParts[iSegmentDisplayNameIndex] = "DisplayName=" + xNewSegmentName;
                        }

                        // Replace the HelpText with the appropriate value.
                        axObjectDataParts[iHelpTextIndex] = "HelpText=" + xHelpText;

                        // Reconstruct the object data value string.
                        StringBuilder oSB = new StringBuilder();
                        bool bIsDelimiterRequired = false;

                        for(int i = 0; i < axObjectDataParts.Length; i++)
                        { 
                            // Append the '|' delimiter after the first object data part has been added.
                            if(bIsDelimiterRequired)
                            {
                                oSB.Append("|");
                            }

                            oSB.Append(axObjectDataParts[i]);
                            bIsDelimiterRequired = true;
                        }

                        // Set the value for the object data attribute.
                        oSegmentObjectDataAttribute.Value = oSB.ToString();
                        
                        // Update the xml in the def.
                        oDef.XML = oXML.OuterXml;
                    }
                }
            }
        }

        #endregion
        #region ***********************events***********************
        /// <summary>
        /// Save button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
	    //GLOG : 8164 : ceh
	    //validate controls as first step
            try
            {
                if (!ValidateControls())
                {
                    this.DialogResult = DialogResult.None;
                    return;
                }

                if (!m_bExecuteOnFinish)
                    return;

                m_bCancel = false;

                this.DialogResult = DialogResult.OK;


                System.Windows.Forms.Application.DoEvents();

                switch (this.Mode)
                {
                    case DialogMode.RenameImportedSegment: //GLOG 5826
                    case DialogMode.NewSegmentPacket: //GLOG : 15816 : JSW
                    case DialogMode.PromptForSegmentPacketAndFolderInfoOnly:
                        //Do nothing except validate name
                        if (this.ObjectType == ObjectTypes.AdminSegment)
                        {
                            if (AdminSegmentDefs.Exists(this.ContentName))
                            {
                                MessageBox.Show(
                                    LMP.Resources.GetLangString("Msg_NewSegment_DefinitionNotUnique"),
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                this.txtDisplayName.Focus();
                                this.DialogResult = DialogResult.None;
                                return;
                            }
                        }
                        else
                        {
                            UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, Session.CurrentUser.ID);
                            for (int u = 1; u <= oDefs.Count; u++)
                            {
                                //GLOG 5826: Duplicate names not disallowed by table structure,
                                //so force unique name within User's collection
                                if (((UserSegmentDef)oDefs.ItemFromIndex(u)).DisplayName.ToUpper() == this.ContentDisplayName.ToUpper())
                                {
                                    MessageBox.Show(
                                        LMP.Resources.GetLangString("Msg_NewSegment_DefinitionNotUnique"),
                                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    this.txtDisplayName.Focus();
                                    this.DialogResult = DialogResult.None;
                                    return;
                                }
                            }
                        }
                        break;
                    case DialogMode.EditProperties:
                        if (!SaveProperties())
                            return;
                        break;
                    case DialogMode.SavedContent:
                    case DialogMode.ClonedSegment:
                    case DialogMode.ClonedStyleSheet:
                    case DialogMode.NewSegment:
                    case DialogMode.NewParagraphTextSegment:
                    case DialogMode.NewSentenceTextSegment:
                    case DialogMode.NewStyleSheet:
                    case DialogMode.NewAnswerFile:
                    case DialogMode.ClonedAnswerFile:
                    case DialogMode.NewMasterDataForm:
                        if (!CreateNewSegment())
                        {
                            this.DialogResult = DialogResult.None;
                            return;
                        }
                        
                        break;
                    case DialogMode.NewFolder:
                        if (!CreateNewFolder())
                            return;
                        break;
                    case DialogMode.CreatePrefill:
                        if (!CreatePrefill())
                            return;

                        SaveSelectedPrefillFolder();
                        break;
                    case DialogMode.CreateLegacyPrefill:
                        if (this.SelectedFolderMember != null)
                        {
                            //user chose to overwrite existing prefill
                            DialogResult iRes = MessageBox.Show(this.SelectedFolderMember.Name +
                                LMP.Resources.GetLangString("Prompt_OverwriteExistingPrefill"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                                MessageBoxDefaultButton.Button2);

                            if (iRes == DialogResult.No)
                            {
                                this.btnSave.DialogResult = DialogResult.None;
                                return;
                            }
                        }

                        break;
                    case DialogMode.PromptForSegmentInfoOnly:
                    case DialogMode.PromptForSegmentAndFolderInfoOnly:
                        {
                            break;
                        }

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            this.Close();
        }
        /// <summary>
        /// Cancel button has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                m_bCancel = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.Close();
            }
        }
        /// <summary>
        /// Simulate clicking Save if enter key is pressed in Name textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtName_EnterKeyPressed(object sender, EnterKeyPressedEventArgs e)
        {
            btnSave_Click(sender, e);
        }
        private void txtDisplayName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtName.Text = String.RemoveIllegalNameChars(this.txtDisplayName.Text);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Disallow characters illegal for Name property
            if (e.KeyChar == ' ' ||
               e.KeyChar == '.' ||
               e.KeyChar == '\'' ||
               e.KeyChar == '>' ||
               e.KeyChar == '<' ||
               e.KeyChar == '&' ||
               e.KeyChar == '[' ||
               e.KeyChar == ']' ||
               e.KeyChar == '^')
                e.Handled = true;
        }

        private void txtDisplayName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //disallow MacPac reserved characters
            if (e.KeyChar == '[' ||
                e.KeyChar == ']' ||
                e.KeyChar == '^')
                e.Handled = true;

        }
        #endregion
        //GLOG : 8152 : JSW
        private void ftvLocation_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {

                    UltraTreeNode oNode = this.ftvLocation.GetNodeFromPoint(e.X, e.Y);
                    string xMyFoldersIDTag = "0.0";

                    if (oNode != null)
                    {
                        oNode.Selected = true;
                        this.ftvLocation.ActiveNode = oNode;
                    }
                    if (this.AllowFolderMenu && (oNode.Tag is UserFolder || oNode.Key == xMyFoldersIDTag))
                    {
                        this.mnuFolder_CreateNewFolder.Visible = true;
                        this.mnu_Folder.Show(oNode.Control, e.X, e.Y);
                    }
                }
            }
            catch { }

        }
        //GLOG : 8152 : JSW
        private void mnuFolder_CreateNewFolder_Click(object sender, EventArgs e)
        {

            try
            {
                ContentPropertyForm oForm = new ContentPropertyForm();
                oForm.ShowForm(this.ParentForm, this.ftvLocation.ActiveNode, Form.MousePosition, ContentPropertyForm.DialogMode.NewFolder);
                if (!oForm.Canceled)
                {
                    this.ftvLocation.ActiveNode.Toggle();
                    this.ftvLocation.ActiveNode.Toggle();
                    this.ftvLocation.ActiveNode.ExpandAll();
                    this.ftvLocation.ActiveNode.Nodes[this.ftvLocation.ActiveNode.Nodes.Count - 1].Selected = true;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_Folder_Create"), oE);
            }
        }

        private void btnAddHelpText_Click(object sender, EventArgs e)
        {
            //GLOG 8329
            try
            {
                //GLOG 8360: Copy selected text to clipboard, so that Outline numbering will be included as part of text
                string xSelectedText = "";
                if (Session.CurrentWordApp.Selection != null && Session.CurrentWordApp.Selection.Type > Microsoft.Office.Interop.Word.WdSelectionType.wdSelectionIP &&
                    Session.CurrentWordApp.Selection.Type < Microsoft.Office.Interop.Word.WdSelectionType.wdSelectionInlineShape)
                {
                    Session.CurrentWordApp.Selection.Range.Copy();
                    //DoEvents required here to allow access to clipboard functions
                    System.Windows.Forms.Application.DoEvents();
                    if (Clipboard.ContainsText(TextDataFormat.UnicodeText))
                    {
                        xSelectedText = Clipboard.GetText(TextDataFormat.UnicodeText);
                    }
                }

                xSelectedText = LMP.String.CleanUpForDisplay(xSelectedText);
                xSelectedText = String.ReplaceMPReservedChars(xSelectedText);
                //GLOG 8340: Replace reserved XML characters with tokens
                xSelectedText = String.ReplaceXMLChars(xSelectedText);
                //GLOG 8340: strip out non-printing characters
                for (int i = 0; i < 32; i++)
                {
                    switch (i)
                    {
                        //GLOG 8360: Don't remove Tab, LF or CR characters
                        case 9:
                            //Replace tabs with spaces
                            xSelectedText = xSelectedText.Replace(((char)i).ToString(), "   ");
                            break;
                        case 10:
                        case 11:
                        case 13:
                            break;
                        default:
                            xSelectedText = xSelectedText.Replace(((Char)i).ToString(), "");
                            break;
                    }
                }

                string xSep = new string('-', 48);
                int iTotalLen = string.Concat(xSelectedText, this.mltDescription.Text != "" ? "\r\n" + xSep + "\r\n" + this.mltDescription.Text : "").Length; //JTS 4/4/16
                if (iTotalLen > 32500)
                {
                    xSelectedText = xSelectedText.Substring(0, Math.Max(32500 - this.mltDescription.Text.Length + 4, 0)); //JTS 4/4/16
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_HelpTextTruncated"), LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.mltDescription.Text = string.Concat(xSelectedText, this.mltDescription.Text != "" ? "\r\n" + xSep + "\r\n" + this.mltDescription.Text : ""); //JTS 4/4/16
                this.btnAddHelpText.Enabled = false; //JTS 4/4/16
                this.btnSave.Focus();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG 8329
        private void mltDescription_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.mltDescription.Text == "" && this.btnAddHelpText.Visible)
                {
                    this.btnAddHelpText.Enabled = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}