using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Architect.Api;

namespace LMP.Architect.Api
{
    public partial class AlignPleadingPaperTextForm : Form
    {
        #region *********************fields*********************
        PleadingPaper.mpPleadingPaperAlignmentScopes m_iScope;
        bool m_bCancelled;
        #endregion *********************fields*********************
        #region *********************properties*********************
        public PleadingPaper.mpPleadingPaperAlignmentScopes Scope
        {
            get { return m_iScope; }
            set
            {
                m_iScope = value;
            }
        }
        public bool Cancelled
        {
            get { return m_bCancelled; }
            set
            {
                m_bCancelled = value;
            }
        }

        /// <summary>
        /// Enable/disable the "Use selected text" option depending on if text is selected.
        /// </summary>
        public bool UseSelectedTextOption
        {
            get
            {
                return this.rdSelection.Enabled;
            }

            set
            {
                this.rdSelection.Enabled = value;
            }
        }
        #endregion
        #region *********************constructors*********************
        public AlignPleadingPaperTextForm(bool bEnableSelectionOptions, bool bEnableBodyOptions)
        {
            InitializeComponent();
            this.rdParagraph.Enabled = bEnableSelectionOptions;
            this.rdSelection.Enabled = bEnableSelectionOptions;
            this.rdToEndOfBody.Enabled = bEnableSelectionOptions && bEnableBodyOptions;
            this.rdPleadingBody.Enabled = bEnableBodyOptions;

            //GLOG : 7100 : jsw
            //hide unused controls for toolbar only, reposition controls
            if (LMP.MacPac.MacPacImplementation.IsToolkit)
            {
                if (bEnableBodyOptions == false)
                {
                    this.rdPleadingBody.Visible = false;
                    this.rdToEndOfBody.Visible = false;
                    int iDiff = (this.rdToEndOfBody.Location.Y - this.rdPleadingBody.Location.Y) * 2;
                    this.rdParagraph.Location = new System.Drawing.Point (this.rdParagraph.Location.X, (this.rdParagraph.Location.Y - iDiff));
                    this.rdSelection.Location = new System.Drawing.Point (this.rdParagraph.Location.X,  (this.rdSelection.Location.Y - iDiff));
                    this.grpScope.Height = (this.grpScope.Height - iDiff);
                    this.Height = (this.Height - iDiff);
                }   
            }

        }
        #endregion *********************constructors*********************

        #region *********************private functions*********************
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdParagraph.Checked)
                    this.Scope = PleadingPaper.mpPleadingPaperAlignmentScopes.Paragraph;
                if (rdSelection.Checked)
                    this.Scope = PleadingPaper.mpPleadingPaperAlignmentScopes.Selection;
                if (rdToEndOfBody.Checked)
                    this.Scope = PleadingPaper.mpPleadingPaperAlignmentScopes.ToSignature;
                if (rdPleadingBody.Checked)
                    this.Scope = PleadingPaper.mpPleadingPaperAlignmentScopes.PleadingBody;

                this.Cancelled = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cancelled = true;
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void AlignText_Load(object sender, EventArgs e)
        {
            if (this.rdSelection.Enabled)
                //GLOG item #5939 - jsw
                this.rdSelection.Select();
            else if (this.rdPleadingBody.Enabled)
                this.rdPleadingBody.Select();
            else if (this.rdParagraph.Enabled)
                //GLOG item #5939 - dcf
                this.rdParagraph.Select();
            else
                this.rdSelection.Select();
        }
        #endregion

    }
}