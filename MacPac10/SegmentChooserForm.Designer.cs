namespace LMP.MacPac
{
    partial class SegmentChooserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSegments = new System.Windows.Forms.Label();
            this.chooser1 = new LMP.Controls.Chooser();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(139, 74);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(221, 74);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblSegments
            // 
            this.lblSegments.AutoSize = true;
            this.lblSegments.BackColor = System.Drawing.Color.Transparent;
            this.lblSegments.Location = new System.Drawing.Point(10, 13);
            this.lblSegments.Name = "lblSegments";
            this.lblSegments.Size = new System.Drawing.Size(41, 15);
            this.lblSegments.TabIndex = 3;
            this.lblSegments.Text = "&Types:";
            // 
            // chooser1
            // 
            this.chooser1.AllowEmptyValue = false;
            this.chooser1.AssignedObjectType = LMP.Data.mpObjectTypes.Architect;
            this.chooser1.AutoSize = true;
            this.chooser1.Borderless = false;
            this.chooser1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.chooser1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooser1.IncludeNoneOption = false;
            this.chooser1.IsDirty = false;
            this.chooser1.LimitToList = true;
            this.chooser1.ListName = "";
            this.chooser1.Location = new System.Drawing.Point(8, 30);
            this.chooser1.MaxDropDownItems = 8;
            this.chooser1.Name = "chooser1";
            this.chooser1.SegmentsList = "";
            this.chooser1.SelectedIndex = -1;
            this.chooser1.SelectedValue = null;
            this.chooser1.SelectionLength = 0;
            this.chooser1.SelectionStart = 0;
            this.chooser1.Size = new System.Drawing.Size(288, 26);
            this.chooser1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.chooser1.SupportingValues = "";
            this.chooser1.TabIndex = 0;
            this.chooser1.Tag2 = null;
            this.chooser1.TargetObjectID = 1;
            this.chooser1.TargetObjectType = LMP.Data.mpObjectTypes.Architect;
            this.chooser1.Value = "";
            this.chooser1.ValueChanged += new LMP.Controls.ValueChangedHandler(this.chooser1_ValueChanged);
            // 
            // SegmentChooserForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(305, 112);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.chooser1);
            this.Controls.Add(this.lblSegments);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SegmentChooserForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Choose Segment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LMP.Controls.Chooser chooser1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblSegments;
    }
}