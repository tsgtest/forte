using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Infragistics.Win.UltraWinTree;
using LMP.Data;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class FavoriteSegmentsManager : LMP.Controls.ManagerBase
    {
        private DataSet dsSelectedSegments;
        private DataTable dtDetails;
        private DataColumn dataColumn1;
        bool m_bIgnoreLocationComboValueChange = false;
    
        public FavoriteSegmentsManager()
        {
            InitializeComponent();

            //GLOG #8492 - dcf
            this.lstSelectedSegments.Height = this.lblDefaultInsertionLocation.Top - this.lstSelectedSegments.Top - 10;
        }

        ArrayList m_al = new ArrayList();

        private void FavoriteSegmentsManager_Load(object sender, EventArgs e)
        {
            try
            {
                // In Word 2003, the menu and task pane exist in 2 different processes 
                // resulting in the static FavoriteSegment class being updated 
                // individually. Refresh the Favorites from the db to synchronize 
                // both instances.
                if (LMP.Architect.Api.Application.CurrentWordVersion == 11)
                {
                    FavoriteSegments.RefreshFavorites();
                }
                DataTable oDT = FavoriteSegments.ToDataTable();

                // GLOG : 2475 : JAB
                // Handle the change in number of rows where the count of items 
                // increased from 0 to 1 by subscribing to the DataRowChangeEventHandler
                // event. See the event delegate for more info.
                oDT.RowChanged += new DataRowChangeEventHandler(oDT_RowChanged);
                this.lstSelectedSegments.DataSource = oDT;
                this.lstSelectedSegments.DisplayMember = oDT.Columns[3].ColumnName;

                LocalizeFavoriteSegmentsManager();
                LoadSegments();
				//GLOG : 6222 : CEH
                this.defaultLocationComboBox1.SelectedIndex = -1;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // GLOG : 2475 : JAB
        // When the count of items in the lstSelectedSegments data source
        // increased from 0 to 1 and the newly added item is selected, no
        // SelectedIndexChanged event occurs for some reason. Deselect and
        // select the lstSelectedSegments.SelectedIndex to trigger the 
        // SelectedIndexChanged event.
        void oDT_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Add && e.Row.Table.Rows.Count == 1)
            {
                this.lstSelectedSegments.SelectedIndex = -1;
                this.lstSelectedSegments.SelectedIndex = 0;
            }
        }

        private void LocalizeFavoriteSegmentsManager()
        {
            this.lblSegments.Text = LMP.Resources.GetLangString("Label_Segments");
            this.lblSelectedSegments.Text = LMP.Resources.GetLangString("Label_SelectedSegments");
            this.lblDefaultInsertionLocation.Text = LMP.Resources.GetLangString("lblDefaultInsertionLocation");
        }

        private void LoadSegments()
        {
            this.ftvSegments.OwnerID = Session.CurrentUser.ID;

            this.ftvSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments |
                                                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserSegments |
                                                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders |
                                                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                                                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Prefills |
                                                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SegmentPackets |
                                                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.StyleSheets |
                                                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.MasterData; //GLOG 8163
            this.ftvSegments.AllowSearching = true; //GLOG 6028
            this.ftvSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;

            ftvSegments.ExecuteFinalSetup();

            // Set the first node as the selected node.
            if (this.ftvSegments.Tree.Nodes.Count > 0) //GLOG 6028
            {
                this.ftvSegments.Tree.Nodes[0].Selected = true; //GLOG 6028
            }
        }

        private void UpdateFavoriteSegmentsManagerView()
        {
            this.btnAddSegment.Enabled =
                (this.ftvSegments.Tree.SelectedNodes.Count > 0) && //GLOG 6028
                (this.lstSelectedSegments.Items.Count < FavoriteSegments.FAVORITE_SEGMENT_MAX_FAVORITES); //GLOG 6221
            this.btnDeleteSegment.Enabled = 
                (this.lstSelectedSegments.SelectedIndex != -1);
            this.btnMoveSegmentUp.Enabled = 
                (this.lstSelectedSegments.SelectedIndex > 0);
            this.btnMoveSegmentDown.Enabled = 
                (this.lstSelectedSegments.SelectedIndex != -1) && 
                (this.lstSelectedSegments.SelectedIndex < 
                this.lstSelectedSegments.Items.Count - 1);

            SetSelectedSegmentComboBox();
        }

        // GLOG : 2475 : JAB
        // Populate the combo box with the appropriate location values
        // based on the selected segment.
        private void SetSelectedSegmentComboBox()
        {
            m_bIgnoreLocationComboValueChange = true;
            bool bIsExternalContent = false;
            bool bStyleSheetOnly = false;
            bool bAnswerFileOnly = false;
            short sMenuInsertionOptions = (short)Segment.InsertionLocations.Default;

            DataTable oDT = (DataTable)this.lstSelectedSegments.DataSource;

            if (oDT.Rows.Count > 0 && this.lstSelectedSegments.SelectedIndex >= 0)
            {
                DataRow oRow = oDT.Rows[this.lstSelectedSegments.SelectedIndex];

                if (oRow.RowState == DataRowState.Detached)
                    return;

                short shSegmentType = (short)oRow["SegmentType"];

                if (shSegmentType == (short)LMP.Data.mpFolderMemberTypes.AdminSegment)
                {
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    AdminSegmentDef oDef = null;

                    try
                    {
                        //GLOG 5448: Don't attempt to cast full ID as Number,
                        //as this may not work if international decimal separator is set in Regional settings
                        string xID = (string)oDT.Rows[this.lstSelectedSegments.SelectedIndex]["SegmentID"];
                        if (xID.EndsWith(".0"))
                            xID = xID.Substring(0, xID.IndexOf(".0"));
                        oDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(xID));
                    }
                    catch (LMP.Exceptions.NotInCollectionException)
                    {
                        DialogResult iRes = MessageBox.Show(
                            LMP.Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);

                        oRow.Delete();
                        lstSelectedSegments.DataSource = oDT;
                        return;
                    }

                    bIsExternalContent = (oDef.TypeID == mpObjectTypes.NonMacPacContent);
                    bStyleSheetOnly = (oDef.IntendedUse == mpSegmentIntendedUses.AsStyleSheet);
                    bAnswerFileOnly = (oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile);
                    sMenuInsertionOptions = oDef.MenuInsertionOptions;
                }
                else if (shSegmentType == (short)LMP.Data.mpFolderMemberTypes.UserSegment)
                {
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);
                    UserSegmentDef oDef = null;

                    try
                    {
                        oDef = (UserSegmentDef)oDefs.ItemFromID((string)oDT.Rows[this.lstSelectedSegments.SelectedIndex]["SegmentID"]);
                    }
                    catch (LMP.Exceptions.NotInCollectionException)
                    {
                        DialogResult iRes = MessageBox.Show(
                            LMP.Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);

                        oRow.Delete();
                        lstSelectedSegments.DataSource = oDT;
                        return;
                    }

                    bIsExternalContent = (oDef.TypeID == mpObjectTypes.NonMacPacContent);
                    bStyleSheetOnly = (oDef.IntendedUse == mpSegmentIntendedUses.AsStyleSheet);
                    bAnswerFileOnly = (oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile);
                    sMenuInsertionOptions = oDef.MenuInsertionOptions;
                }
                else if (shSegmentType == (short)LMP.Data.mpFolderMemberTypes.VariableSet)
                {
                    // GLOG : 2475 : JAB
                    // Prefills need to display the insertion options to their 
                    // corresponding segment.

                    VariableSetDefs oVSDefs = new VariableSetDefs(mpVariableSetsFilterFields.User, Session.CurrentUser.ID);
                    VariableSetDef oVSDef = null;

                    try
                    {
                        oVSDef = (VariableSetDef)oVSDefs.ItemFromID((string)oDT.Rows[
                        this.lstSelectedSegments.SelectedIndex]["SegmentID"]);
                    }
                    catch (LMP.Exceptions.NotInCollectionException)
                    {
                        DialogResult iRes = MessageBox.Show(
                            LMP.Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);

                        oRow.Delete();
                        lstSelectedSegments.DataSource = oDT;
                        return;
                    }

                    string xPrefillSegmentID = oVSDef.SegmentID;
                    int iAdminSegmentID = 0;

                    if (!xPrefillSegmentID.Contains("."))
                    {
                        iAdminSegmentID = Int32.Parse(xPrefillSegmentID);
                    }
                    else if (xPrefillSegmentID.EndsWith(".0"))
                    {
                        iAdminSegmentID = Int32.Parse(xPrefillSegmentID.Substring(0, xPrefillSegmentID.IndexOf(".")));
                    }

                    if (iAdminSegmentID != 0)
                    {
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iAdminSegmentID);

                        bIsExternalContent = (oDef.TypeID == mpObjectTypes.NonMacPacContent);
                        bStyleSheetOnly = (oDef.IntendedUse == mpSegmentIntendedUses.AsStyleSheet);
                        bAnswerFileOnly = (oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile);
                        sMenuInsertionOptions = oDef.MenuInsertionOptions;
                    }
                }
            }

            this.defaultLocationComboBox1.SetInsertionValues(
                sMenuInsertionOptions, bStyleSheetOnly, bAnswerFileOnly, bIsExternalContent);

            // Select the location value for the selected favorite segment.
            if (oDT.Rows.Count > 0 && this.lstSelectedSegments.SelectedIndex >= 0)
            {
                LMP.Data.mpFolderMemberTypes oType = FavoriteSegments
                    .SegmentFolderMemberType(this.lstSelectedSegments.SelectedIndex);
                this.defaultLocationComboBox1.Enabled = 
                    (oType == mpFolderMemberTypes.AdminSegment || oType == mpFolderMemberTypes.UserSegment || oType == mpFolderMemberTypes.VariableSet);

                // Glog 2475 : Variable sets do not have alternative options for inserts. Set the combo box to the default
                // location value (ie, 0).

                if (oType == mpFolderMemberTypes.VariableSet)
                {
                    InsertionLocationComboValue = 0;
                }
                else
                {
                    InsertionLocationComboValue = (int)oDT.Rows[this.lstSelectedSegments.SelectedIndex]["InsertionLocation"];
                }
            }
            else
            {
                // GLOG : 2475 : JAB
                // Set the combo box value to the Default location.
                InsertionLocationComboValue = 0;
                this.defaultLocationComboBox1.Enabled = false;
            }

            m_bIgnoreLocationComboValueChange = false;
        }



        private void UpdateFavoriteSegmentInsertionLocation()
        {
            if (this.ftvSegments.Tree.Nodes.Count > 0 && this.lstSelectedSegments.SelectedIndex >= 0) //GLOG 6028
            {
                DataTable oDT = (DataTable)this.lstSelectedSegments.DataSource;
                oDT.Rows[this.lstSelectedSegments.SelectedIndex]["InsertionLocation"] = InsertionLocationComboValue;
            }
        }

        private int InsertionLocationComboValue
        {
            set
            {
                object[,] aListArray = this.defaultLocationComboBox1.ListArray;
                int iValue = value;
                int iTestValue = 0;

                for (int i = 0; i <= aListArray.GetUpperBound(0); i++)
                {
                    iTestValue = Int32.Parse((string)aListArray[i, 1]);
                    
                    if (iValue == iTestValue)
                    {
                        // GLOG : 2475 : JAB
                        // Only set the combo box's selected index if it is
                        // different than the selected index. This is for 
                        // efficiency since a value changed event is fired 
                        // even if the selected index is set to the current
                        // selected index.
                        if (this.defaultLocationComboBox1.SelectedIndex != i)
                        {
                            this.defaultLocationComboBox1.SelectedIndex = i;
                        }
                        return;
                    }
                }

                // Glog 2475 : If no value corresponds to the combo box values
                // select the combo boxes default location value.
                this.InsertionLocationComboValue = 0; 
            }

            get
            {
                object[,] aListArray = this.defaultLocationComboBox1.ListArray;
                int iValue = Int32.Parse((string)aListArray[this.defaultLocationComboBox1.SelectedIndex, 1]);
                return iValue;
            }
        }


        private void btnAddSegment_Click(object sender, EventArgs e)
        {
            try
            {
                // If a node is selected, move the node over to the list of selected nodes.
                if (this.ftvSegments.Value.Length > 0 && this.ftvSegments.MemberList.Count > 0)
                {
                    // move the node to the list of selected nodes.
                    ArrayList alMemberList0 = (ArrayList)this.ftvSegments.MemberList[0];
                    string xSegmentId = alMemberList0[0].ToString(); //GLOG 8163
                    //GLOG 6966: Make sure string can be parsed as a Double even if
                    //the Decimal separator is something other than '.' in Regional Settings
                    if (xSegmentId.EndsWith(".0"))
                    {
                        xSegmentId = xSegmentId.Replace(".0", "");
                    }
                    string xSegmentName = (string)alMemberList0[1];
                    LMP.Data.mpFolderMemberTypes tSegmentType = (LMP.Data.mpFolderMemberTypes)alMemberList0[2];
                    string xSegmentDisplayName = this.ftvSegments.Tree.ActiveNode.Text; //GLOG 6028
					//GLOG : 6303 : jsw
					//add intended use icon to favorites
                    //revamped for GLOG item #6413 - dcf
                    mpSegmentIntendedUses tSegmentIntendedUse = 0;

                    if (tSegmentType == mpFolderMemberTypes.AdminSegment)
                    {
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID((int)double.Parse(xSegmentId));
                        tSegmentIntendedUse = oDef.IntendedUse;
                    }
                    else if (tSegmentType == mpFolderMemberTypes.UserSegment)
                    {
                        UserSegmentDefs oDefs = new UserSegmentDefs();
                        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xSegmentId);
                        tSegmentIntendedUse = oDef.IntendedUse;
                    }
                    else
                    {
                        VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.User, LMP.MacPac.Session.CurrentUser.ID);
                        VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(xSegmentId);
                        tSegmentIntendedUse = mpSegmentIntendedUses.AsDocument;
                    }
                        
                    //LMP.Data.mpSegmentIntendedUses tSegmentIntendedUse = (LMP.Data.mpSegmentIntendedUses)alMemberList0[3];
                    
                    // Check to see if this selection is already present in the selected segments list.
                    int selectedItemIndex = this.lstSelectedSegments.FindStringExact(xSegmentDisplayName);

                    //GLOG 5448: These variables are unused, and attempt to parse ID causes
                    //error with non-period decimal separator
                    //double dSegmentId = double.Parse(xSegmentId);
                    //int iSegmentId = (int)dSegmentId;

                    if (selectedItemIndex >= 0)
                    {
                        // The selection is already present in the selected segments list.
                        // Make this the selected item in the selected segment list.
                        this.lstSelectedSegments.SelectedIndex = selectedItemIndex;
                    }
                    else
                    {
                        // We have not previously selected this selection. Add it to the selected 
                        // segments list.

                        // Add the segment name to the model.
                        FavoriteSegments.Add(xSegmentName, xSegmentId, tSegmentType, xSegmentDisplayName, tSegmentIntendedUse);
                        
                        this.lstSelectedSegments.SelectedIndex = this.lstSelectedSegments.Items.Count - 1;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnDeleteSegment_Click(object sender, EventArgs e)
        {
            try
            {
                // If a item is selected, move the item over to the list of segments.
                if (this.lstSelectedSegments.SelectedItem != null)
                {
                    // Get the current selected index to set the next item as the selected item.
                    // Selecting the next item allows easy deletion of multiple items.
                    int iSelectedIndex = this.lstSelectedSegments.SelectedIndex;
                    int iSelectedSegmentsCount = this.lstSelectedSegments.Items.Count;

                    // remove the items from the list of selected segments.
                    FavoriteSegments.Remove(iSelectedIndex);

                    // If this is the last item in the list...
                    if (iSelectedIndex == iSelectedSegmentsCount - 1)
                    {
                        // This was the last item in the list. Set the previous item as the selected item.

                        iSelectedIndex -= 1;
                    }
                    
                    // If the list originally contained only 1 item, it was removed and the selected index 
                    // was decremented to -1 above. No selected item needs to be set.
                    if (iSelectedIndex >= 0)
                    {
                        this.lstSelectedSegments.SelectedIndex = iSelectedIndex;
                    }                    
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnMoveSegmentUp_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstSelectedSegments.SelectedIndex > 0)
                {
                    // Get the index of the item selected to be moved up.
                    int iIndexOfObjectedToBeMovedUp = this.lstSelectedSegments.SelectedIndex;

                    // Move the item up 1 level.
                    FavoriteSegments.Move(iIndexOfObjectedToBeMovedUp, -1);

                    // set the moved item as the selected item to allow multiple moves.
                    this.lstSelectedSegments.SelectedIndex = iIndexOfObjectedToBeMovedUp - 1;

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnMoveSegmentDown_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstSelectedSegments.SelectedIndex < this.lstSelectedSegments.Items.Count - 1)
                {
                    // Get the index of the item selected to be moved down.
                    int iIndexOfObjectedToBeMovedDown = this.lstSelectedSegments.SelectedIndex;

                    // Move the item down 1 level.
                    FavoriteSegments.Move(iIndexOfObjectedToBeMovedDown, 1); 

                    // set the moved item as the selected item to allow multiple moves.
                    this.lstSelectedSegments.SelectedIndex = iIndexOfObjectedToBeMovedDown + 1;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void ftvSegments_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                // GLOG : 2475 : JAB
                // Either a node in the tree of segments or a node the list of 
                // selected favorites can be selected but not both simultaneously. 
                // The list of segments has been selected therefore deselect all 
                // the nodes in the favorites list.

                if (this.lstSelectedSegments.SelectedIndex >= 0 && ftvSegments.Tree.SelectedNodes.Count > 0) //GLOG 6028
                {
                    this.lstSelectedSegments.SelectedIndex = -1;
                }

                // GLOG : 2475 : JAB
                // Disable the default location combo box if the item selected 
                // in the Segments tree is not a segment (eg, it's a folder).
                // Folders for example will result in a ftvSegments.Value
                // Enable the Add/DeleteSegment buttons appropriately.
                if (this.ftvSegments.Tree.SelectedNodes.Count > 0) //GLOG 6028
                {
                    this.defaultLocationComboBox1.Enabled = false;
                    this.btnAddSegment.Enabled = (this.ftvSegments.Value.Length > 0 &&
                        this.ftvSegments.MemberList.Count > 0 &&
                        this.lstSelectedSegments.Items.Count < FavoriteSegments.FAVORITE_SEGMENT_MAX_FAVORITES); //GLOG 6221
                    this.btnDeleteSegment.Enabled = false;
                    this.defaultLocationComboBox1.SetInsertionValues((short)0, false, false, false);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstSelectedSegments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // GLOG : 2475 : JAB
                // Either a node in the tree of segments or a node the list of 
                // selected favorites can be selected but not both simultaneously. 
                // The list of favorite segments has been selected therefore deselect
                // all the nodes in the segments list.
                while (this.ftvSegments.Tree.SelectedNodes.Count > 0 && lstSelectedSegments.SelectedIndex >= 0) //GLOG 6028
                {
                    this.ftvSegments.Tree.SelectedNodes[0].Selected = false; //GLOG 6028
                }

                UpdateFavoriteSegmentsManagerView();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void ftvSegments_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                UltraTreeNode oNode = null;
                // GLOG 6133
                // Only add an item if the list has not reached its max of 35 items.
                if (this.lstSelectedSegments.Items.Count < FavoriteSegments.FAVORITE_SEGMENT_MAX_FAVORITES)
                {
                    // Clicking anywhere in the ftvSegments will fire this event (even when double clicking the scroll bar).
                    // Check that a node was double clicked and proceed accordingly.
                    oNode = this.ftvSegments.Tree.GetNodeFromPoint(((MouseEventArgs)e).Location); //GLOG 6028

                    //GLOG item #4457 - dcf
                    //GLOG : 6290 : CEH
                    if (oNode != null)
                    {
                        if (oNode.Tag is LMP.Data.FolderMember || oNode.Tag is object[]) //GLOG 6028
                        {
                            this.btnAddSegment_Click(sender, e);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_FavoritesLimitReached"),
                        FavoriteSegments.FAVORITE_SEGMENT_MAX_FAVORITES),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstSelectedSegments_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.btnDeleteSegment_Click(sender, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void defaultLocationComboBox1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // GLOG : 2475 : JAB
                // Save the new location for the favorite segment.
                if (this.lstSelectedSegments.SelectedIndex >= 0 && !m_bIgnoreLocationComboValueChange)
                {
                    DataTable oDT = (DataTable)this.lstSelectedSegments.DataSource;
                    object[,] aListArray = this.defaultLocationComboBox1.ListArray;
                    oDT.Rows[this.lstSelectedSegments.SelectedIndex]["InsertionLocation"] = InsertionLocationComboValue;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void defaultLocationComboBox1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                this.UpdateFavoriteSegmentInsertionLocation();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
