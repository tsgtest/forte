using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Architect.Api;
using LMP.Controls;

namespace LMP.MacPac
{
    public partial class MultipleSegmentsForm : Form
    {
        #region *********************enum**************************
        #endregion
        #region *********************fields**************************
        //TODO: settle on an ITEM_SEP - these are set to work with ComboBox.SetList(string)
        public const char ITEM_SEP = '~';
        public const char NAME_VALUE_SEP = ';';
        private string m_xSegmentID = "";
        private TaskPane m_oTaskPane = null;
        private Prefill m_oPrefill = null;
        #endregion
        #region *********************constructors**************************
        /// <summary>
        /// Contructor
        /// </summary>
        public MultipleSegmentsForm(string xDialogCaption, string xOKButtonCaption, string xSegmentInsertionData, bool bShowSavePacketButton)
        {
            InitializeComponent();
            this.Text = xDialogCaption;
            this.ucMultipleSegmentManager.SetupDialog(xOKButtonCaption, xSegmentInsertionData, bShowSavePacketButton, false);

            this.ucMultipleSegmentManager.OKClicked += new OKClickedHandler(ucMultipleSegmentManager_OKClicked);
            this.ucMultipleSegmentManager.CancelClicked += new CancelClickedHandler(ucMultipleSegmentManager_CancelClicked);
        }
        public MultipleSegmentsForm(string xDialogCaption, string xOKButtonCaption, string xSegmentInsertionData, bool bShowSavePacketButton, TaskPane oParentTaskPane, bool bShowSaveDeautomateOptions)
        {
            InitializeComponent();
            this.Text = xDialogCaption;
            this.ucMultipleSegmentManager.SetupDialog(xOKButtonCaption, xSegmentInsertionData, bShowSavePacketButton, bShowSaveDeautomateOptions);

            m_oTaskPane = oParentTaskPane;

            this.ucMultipleSegmentManager.OKClicked += new OKClickedHandler(ucMultipleSegmentManager_OKClicked);
            this.ucMultipleSegmentManager.CancelClicked += new CancelClickedHandler(ucMultipleSegmentManager_CancelClicked);
        }
        //GLOG 8069: Allow specifying Prefill
        public MultipleSegmentsForm(string xDialogCaption, string xOKButtonCaption, string xSegmentInsertionData, bool bShowSavePacketButton, TaskPane oParentTaskPane, bool bShowSaveDeautomateOptions, bool bShowPrefillButton, Prefill oPrefill)
        {
            InitializeComponent();
            this.Text = xDialogCaption;
            this.ucMultipleSegmentManager.SetupDialog(xOKButtonCaption, xSegmentInsertionData, bShowSavePacketButton, oParentTaskPane, bShowSaveDeautomateOptions, bShowPrefillButton, oPrefill);

            m_oTaskPane = oParentTaskPane;
            
            this.ucMultipleSegmentManager.OKClicked += new OKClickedHandler(ucMultipleSegmentManager_OKClicked);
            this.ucMultipleSegmentManager.CancelClicked += new CancelClickedHandler(ucMultipleSegmentManager_CancelClicked);
        }
        #endregion
        #region *********************methods**************************
        #endregion
        #region *********************properties**************************
        /// <summary>
        /// Returns ID of Segment selected in Treeview, or delimited list of IDs and Names for Multiple selection
        /// </summary>
        public string Value
        {
            get
            {
                return this.ucMultipleSegmentManager.Value;
            }
        }
        //GLOG 8069
        public Prefill Prefill
        {
            get
            {
                return this.ucMultipleSegmentManager.Prefill;
            }
        }
	// GLOG : 8164 : ceh
        public string SegmentList
        {
            get
            {
                return this.ucMultipleSegmentManager.SegmentList();
            }
        }
        #endregion
        #region *********************event handlers**************************
        void ucMultipleSegmentManager_OKClicked(object sender, EventArgs e)
        {
            this.Close();
        }

        void ucMultipleSegmentManager_CancelClicked(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void ucMultipleSegmentManager_Load(object sender, EventArgs e)
        {

        }
    }
}