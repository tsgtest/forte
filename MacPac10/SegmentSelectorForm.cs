﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class SegmentSelectorForm : Form
    {
        Segments m_oSegments;

        public SegmentSelectorForm(Segments oSegments)
        {
            InitializeComponent();
            m_oSegments = oSegments;
        }

        private void SegmentSelectorForm_Load(object sender, EventArgs e)
        {
            int iCount = m_oSegments.Count;
            int iPaperItems = 0;

            if (iCount > 1)
            {
                //count the number of paper items
                for (int i = 0; i < iCount; i++)
                {
                    if (m_oSegments[i] is Paper)
                    {
                        iPaperItems++;
                    }
                }
            }

            for (int i = 0; i < iCount; i++)
            {
                Segment oSegment = m_oSegments[i];

                //add segment if all segments are paper or
                //the segment is not paper - that is, exclude
                //paper segments when there exist some non-paper segments
                if (iPaperItems == iCount || !(oSegment is Paper))
                {
                    this.lstSegments.Items.Add(oSegment.DisplayName);
                }
            }

            if (this.lstSegments.Items.Count > 0)
            {
                this.lstSegments.SelectedIndex = 0;
            }


        }

        public Segment SelectedSegment
        {
            get { return m_oSegments[this.lstSegments.SelectedIndex]; }
        }
    }
}
