using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using LMP.Controls;
using LMP.Architect.Api;
using LMP.Data;
using System.Xml;
using System.Text.RegularExpressions;
using System.Collections;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac
{
    public partial class DocumentStampUI : Form
    {
        #region *****************enumerations*******************
        public enum Mode
        {
            Trailer = 1,
            DocumentStamp = 2
        }
        private enum PendingAction
        {
            None = 0,
            Insert = 1,
            Remove = 2
        }
        #endregion
        #region *********************fields*********************
        internal const string mpNoTrailerVariable = "zzmp10NoTrailerPromptID";
        internal const string mpConverted9xTrailerVariable = "zzmp10Converted9xTrailerID"; //GLOG 4553
        internal const string mpLastTrailerVariable = "zzmp10LastTrailerInserted";  //JTS 6/24/11
        internal const char mpPrefillVariableSeparator = '�';	// ASCII 135 //JTS 6/24/11
        internal const int mpUpdateExistingTrailersSegmentID = -999996;
        internal const int mpNoTrailerPromptSegmentID = -999998;
        internal const int mpRemoveAllStampsSegmentID = -999997;
        internal const int mpSkipTrailerSegmentID = -999999;
        Segment m_oSegment = null;
        Segment m_oExistingStamp = null;
        Prefill m_oPrefill = null; //JTS 6/24/11
        ForteDocument m_oMPDocument = null;
        TaskPane m_oTP = null;
        DocumentStampUI.Mode m_iMode = Mode.Trailer;
        int m_iCurSegmentID = 0;
        mpObjectTypes m_iAssignedObjectType = mpObjectTypes.Trailer;
        ArrayList m_oVariables = null;
        PendingAction m_iPendingAction = PendingAction.None;
        private Hashtable m_oControlsHash = new Hashtable();
        private int m_iCurrentSection = 0;
        private int m_iScope = -1; //GLOG 4444
        private bool m_bLinkedFirstFooter = false;
        private bool m_bLinkedFirstHeader = false;
        private bool m_bLinkedPrimaryFooter = false;
        private bool m_bLinkedPrimaryHeader = false;
        private bool m_bLinkedEvenFooter = false;
        private bool m_bLinkedEvenHeader = false;
        private bool m_bSectionHasLinkedStories = false;
        private object[] m_aExistingTrailers;
        private string m_xSectionLabel;
        private bool m_bSingleTrailerAtEndOfDoc = false; //2-29-12 (dm)
        private bool m_bIncludeLinkedSections = false; //GLOG 6152 (dm)
        private bool m_bContentContainsLinkedStories = false; //GLOG 6189 (dm)
        private bool m_bDisplayDialog = false; //GLOG 6192 (dm)
        #endregion
        #region ****************constructors********************
        public DocumentStampUI(DocumentStampUI.Mode iMode)
        {
            m_iMode = iMode;
            InitializeComponent();

            //GLOG #8492
            this.lstTypes.Height = this.gbApplyTo.Top - this.lstTypes.Top - 5;
        }
        #endregion
        #region *********************events*********************
        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            //GLOG 6370 (dm) - Enter event isn't reached if user finishes
            //by pressing Enter - validate here as well
            if (this.rbMultiple.Checked)
            {
                if (!ValidateMultipleSectionsControl(true))
                {
                    this.DialogResult = DialogResult.None;
                    return;
                }
            }

            m_iPendingAction = PendingAction.Insert;
        }
        /// <summary>
        /// Remove button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            m_iPendingAction = PendingAction.Remove;
        }
        #endregion
        #region *********************methods*********************
        public void Finish()
        {
            bool bTrackChanges = false; //GLOG 7649 (dm)

            try
            {
                if (m_oTP != null)
                    m_oTP.ScreenUpdating = false;
                else
                    SetWordEcho(false);

                //GLOG 7649 (dm)
                //turn off track changes if necessary
                bTrackChanges = m_oMPDocument.WordDocument.TrackRevisions;
                if (bTrackChanges)
                    m_oMPDocument.WordDocument.TrackRevisions = false;

                ArrayList aSections = new ArrayList();

                //GLOG 6240 (dm) - use UpdateExisting for silent trailer
                if (((m_iCurSegmentID == mpUpdateExistingTrailersSegmentID) ||
                    !m_bDisplayDialog) && (m_aExistingTrailers != null))
                {
                    Hashtable hTrailers = new Hashtable();
                    object oZero = 0;
                    for (int i = 0; i < m_aExistingTrailers.GetLength(0); i++)
                    {
                        if (m_aExistingTrailers[i] != null && m_aExistingTrailers[i] != oZero &&
                            m_aExistingTrailers[i].ToString() != "Linked" && m_aExistingTrailers[i].ToString() != "") //GLOG 7894
                        {
                            int iSegmentID = Int32.Parse(m_aExistingTrailers[i].ToString());
                            if (hTrailers.ContainsKey(iSegmentID))
                            {
                                ((ArrayList)hTrailers[iSegmentID]).Add(i + 1);
                            }
                            else
                                hTrailers.Add(iSegmentID, new ArrayList() {i+1});
                        }
                    }
                    foreach (object oKey in hTrailers.Keys)
                    {
                        int iSegmentID = (int)oKey;
                        aSections = (ArrayList)hTrailers[oKey];
                        Prefill oPrefill = LastTrailerPrefill(iSegmentID);
                        RemoveStamps(aSections);
                        InsertStamps(aSections, iSegmentID, oPrefill);
                    }
                    return;
                }
                if (rbSection.Checked)
                {
                    int iSecIndex = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Sections[1].Index;
                    aSections.Add(iSecIndex);
                }
                else if (rbMultiple.Checked && m_iAssignedObjectType == mpObjectTypes.Trailer)
                {
                    //GLOG 6107 (dm) - strip spaces
                    string xMultiple = this.txtMultiple.Text.Replace(" ", "");
                    string[] xSectionList = xMultiple.Split(',');
                    foreach (string xItem in xSectionList)
                    {
                        if (xItem.Contains("-"))
                        {
                            int iPos = xItem.IndexOf("-");
                            string xStart = xItem.Substring(0, iPos);
                            if (iPos < xItem.Length)
                            {
                                string xEnd = xItem.Substring(iPos + 1);
                                if (String.IsNumericInt32(xStart) && String.IsNumericInt32(xEnd))
                                {
                                    for (int i = Int32.Parse(xStart); i <= Int32.Parse(xEnd); i++)
                                    {
                                        //GLOG : 6406 : CEH
                                        //do not add duplicates
                                        if (!aSections.Contains(i))
                                            aSections.Add(i);
                                    }
                                }
                            }

                        }
                        else
                        {
                            //GLOG : 6406 : CEH
                            //do not add duplicates
                            if (String.IsNumericInt32(xItem) && !aSections.Contains(Int32.Parse(xItem)))
                                aSections.Add(Int32.Parse(xItem));
                        }
                    }
                }
                if (m_iCurSegmentID == mpNoTrailerPromptSegmentID)
                {
                    if (rbDocument.Checked == true) //2-27-12 (DM)
                        RemoveStamps(null);
                    else
                        RemoveStamps(aSections);
                    //Only set No Trailer variable if removed from entire document
                    //GLOG 6156 (dm) - conditional changed to account for new
                    //"multiple sections" option
                    //if (!rbSection.Checked)
                    if (rbDocument.Checked == true)
                        SetNoTrailerVariable();
                }
                else if (m_iCurSegmentID == mpRemoveAllStampsSegmentID)
                {
                    RemoveStamps(aSections);
                }
                else if (m_iPendingAction == PendingAction.Insert)
                {
                    //GLOG 4418: Remove Stamps first all at once
                    //This increases speed since RefreshTags can be
                    //called just once at the end
                    //2-27-12 (dm) - conditional changed to account for new
                    //"multiple sections" option
                    //if (!rbSection.Checked == true)
                    //4-26-12 (dm) - no targeted removal for draft stamps
					//GLOG: 6501 : CEH
					//do not remove all draft stamps by default
                    if (rbDocument.Checked == true) //|| (m_iAssignedObjectType == mpObjectTypes.DraftStamp))
                        RemoveStamps(null);
                    else
                    {
                        //GLOG 6192 (dm) - we don't allow a mix of pre and post enhancement
                        //trailers - if there are pre-enhancement trailers in non-targeted
                        //sections, prompt the user that these will be removed
                        object[] oSections = aSections.ToArray();
                        //GLOG: 6501 : CEH - only prompt for trailers
                        //GLOG 7820: Don't test these conditions if type is Draft Stamp
                        if (m_iAssignedObjectType == mpObjectTypes.Trailer && (m_aExistingTrailers == null) &&
                                LMP.Forte.MSWord.WordDoc.ContainsTrailerInNonTargetedSection(
                                m_oMPDocument.WordDocument, oSections) && 
                                m_iAssignedObjectType == mpObjectTypes.Trailer)
                        {
                            DialogResult iRes = DialogResult.OK;
                            LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                            if (m_bDisplayDialog && oFirmSettings.PromptBeforeRemovingLegacyTrailer)
                            {
                                iRes = MessageBox.Show(LMP.Resources.GetLangString(
                                    "Prompt_LegacyTrailersInNonTargetedSections"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            }
                            if (iRes == DialogResult.Yes)
                                RemoveStamps(null);
                            else
                            {
                                //GLOG 3087: prevent display of empty header paragraph
                                LMP.Forte.MSWord.WordDoc.ResetEmptyHeader();
                                return;
                            }
                        }
                        else if (m_bSectionHasLinkedStories && m_bContentContainsLinkedStories)
                        {
                            //GLOG 6152 (dm) - ensure that trailers in body of
                            //linked sections are removed with footer trailer -
                            //we don't want two trailers in the same section
                            ArrayList aSectionsPlus =
                                AddLinkedSectionsWithUnlinkedTrailers(aSections);
                            RemoveStamps(aSectionsPlus);
                        }
                        else
                            RemoveStamps(aSections);

                        //GLOG 6189 (dm) - due to the way our segment insertion code
                        //works for multiple sections, the xml will not be inserted
                        //into footers linked to previous - retarget accordingly
                        if (m_bContentContainsLinkedStories && (aSections.Count > 1))
                            AdjustLinkedSectionTargets(aSections);
                    }
                    InsertStamps(aSections);
                }
                else if (m_iPendingAction == PendingAction.Remove)
                    RemoveStamps(aSections);
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                //GLOG 7649 (dm)
                if (bTrackChanges)
                    m_oMPDocument.WordDocument.TrackRevisions = true;
                
                if (m_oTP != null)
                    m_oTP.ScreenUpdating = true;
                else
                    SetWordEcho(true);
            }
        }
        private void InsertStamps(ArrayList aSections)
        {
            string xPrefill = this.GetPrefillValues();
            Prefill oPrefill = new Prefill(xPrefill, "", m_iCurSegmentID.ToString());
            InsertStamps(aSections, m_iCurSegmentID, oPrefill);
        }
        private void InsertStamps(ArrayList aSections, int iSegmentID, Prefill oPrefill)
        {
            try
            {
                DateTime t0 = DateTime.Now;
                DateTime t1;
                //JTS 8/12/09: Vedder custom trailer handling
                //GLOG 6108 (dm) - added as firm application setting
                LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                Boolean bConvertTrailers = (m_iAssignedObjectType == mpObjectTypes.Trailer) &&
                    (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains("CONVERTTRAILERSTO9XFORMAT") ||
                    oFirmSettings.UseMacPac9xStyleTrailer);
                AdminSegmentDefs oDefs = new AdminSegmentDefs(m_iAssignedObjectType);
                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iSegmentID);

                Segment.InsertionLocations iLocation = (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation;
                Segment.InsertionBehaviors iBehavior = (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior;
                if (rbDocument.Checked && (m_iCurSegmentID != mpUpdateExistingTrailersSegmentID))
                {
                    switch (iLocation)
                    {
                        case Segment.InsertionLocations.InsertAtStartOfCurrentSection:
                        case Segment.InsertionLocations.InsertAtStartOfAllSections:
                        case Segment.InsertionLocations.Default:
                            //GLOG 4404: If Custom 9.x conversion code is in use, Forte Trailer will
                            //be inserted only in the first section
                            if (bConvertTrailers)
                                iLocation = Segment.InsertionLocations.InsertAtStartOfDocument;
                            else
                                iLocation = Segment.InsertionLocations.InsertAtStartOfAllSections;
                            break;
                        case Segment.InsertionLocations.InsertAtEndOfCurrentSection:
                            iLocation = Segment.InsertionLocations.InsertAtEndOfAllSections;
                            break;
                    }
                }
                else
                {
                    //Only apply to current section
                    switch (iLocation)
                    {
                        case 0:
                            iLocation = Segment.InsertionLocations.InsertAtStartOfCurrentSection;
                            break;
                        case Segment.InsertionLocations.InsertAtStartOfAllSections:
                            if (aSections.Count < 2)
                                iLocation = Segment.InsertionLocations.InsertAtStartOfCurrentSection;
                            break;
                        case Segment.InsertionLocations.InsertAtEndOfAllSections:
                            if (aSections.Count < 2)
                                iLocation = Segment.InsertionLocations.InsertAtEndOfCurrentSection;
                            break;
                        case Segment.InsertionLocations.InsertAtEndOfDocument:
                            if (aSections.Count < 2)
                                iLocation = Segment.InsertionLocations.InsertAtEndOfCurrentSection;
                            else
                                iLocation = Segment.InsertionLocations.InsertAtEndOfAllSections;
                            break;
                        case Segment.InsertionLocations.InsertAtStartOfDocument:
                            if (aSections.Count < 2)
                                iLocation = Segment.InsertionLocations.InsertAtStartOfCurrentSection;
                            else
                                iLocation = Segment.InsertionLocations.InsertAtStartOfAllSections;
                            break;
                        case Segment.InsertionLocations.Default:
                            if (aSections.Count < 2)
                                iLocation = Segment.InsertionLocations.InsertAtStartOfCurrentSection;
                            else
                                iLocation = Segment.InsertionLocations.InsertAtStartOfAllSections;
                            break;
                    }
                }
                if (m_oTP != null)
                    m_oTP.ScreenUpdating = false;
                else
                    SetWordEcho(false);
                
                //JTS 11/03/09: It's not necessary to Remove legacy stamps here, because RemoveStamps
                //has already been called separately to handle this
                //if (m_iAssignedObjectType == mpObjectTypes.Trailer)
                //{
                //    //Remove old 8.x/9.x trailers
                //    t1 = DateTime.Now;
                //    LMP.Forte.MSWord.WordDoc.Remove8xTrailers(Session.CurrentWordApp.ActiveDocument, false);
                //    LMP.Benchmarks.Print(t1, "Remove8xTrailers");
                //}
                //else if (m_iAssignedObjectType == mpObjectTypes.DraftStamp)
                //{
                //    //GLOG 4386: Remove old 9.x Draft Stamps
                //    t1 = DateTime.Now;
                //    LMP.Forte.MSWord.WordDoc.Remove9xDocumentStamps(Session.CurrentWordApp.ActiveDocument);
                //    LMP.Benchmarks.Print(t1, "Remove9xDocumentStamps");
                //}

                if (m_iAssignedObjectType == mpObjectTypes.Trailer &&
                    ForteDocument.AllSectionsMarkedForNoTrailer(m_oMPDocument.WordDocument))
                {
                    //Document has been marked for No Trailer at Document or Section level
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_AllSectionsMarkedForNoTrailer"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                {
                    //Word.Section oCurSection = m_oMPDocument.WordDocument.Sections[m_iCurrentSection];
                    //if (LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(oCurSection))
                    //{
                    //    //examine segment xml to see which header/footers contain
                    //    //MacPac content - if all are linked, insert in previous section -
                    //    //if some are linked, alert user that segment can't be inserted -
                    //    //if none are linked, insert in current section
                    //    string xXML = oDef.XML;
                    //    int iPos = 0;
                    //    int iPos2 = 0;

                    //    //check headers
                    //    //iPos = xXML.IndexOf("<w:hdr w:type=\"odd\"
                    //}

                    //GLOG 5336: Archer Issue - For some documents, inserting EOD trailer results in Word crash.
                    //Turning on XML Tags before insertion avoids this.
                    if (m_oMPDocument.FileFormat == mpFileFormats.Binary && ((iLocation == Segment.InsertionLocations.InsertAtEndOfDocument 
                        || iLocation == Segment.InsertionLocations.InsertAtEndOfCurrentSection) 
                        && m_oMPDocument.WordDocument.Content.XMLNodes.Count == 0))
                    {
                        LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(LMP.Forte.MSWord.WordApp.ActiveDocument());
                        oEnv.SaveState();
                        oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.True, mpTriState.Undefined, mpTriState.Undefined,
                            mpTriState.True, false);
                    }
                    
                    t1 = DateTime.Now;
                    //GLOG 2957: Always want to use Prefill values in this case
                    Segment oStamp = m_oMPDocument.InsertSegment(iSegmentID.ToString(), null, iLocation,
                        iBehavior, true, oPrefill, "", false, false, "", false, true, null, aSections);
                    LMP.Benchmarks.Print(t1, "m_oMPDocument.InsertSegment");

                    //JTS 2/20/12: This is no longer necessary, since Trailer Bounding Objects will have been removed upon insertion
                    ////3-3-11 (dm) - ensure that trailer gets bookmarked - this is necessary for
                    ////Worksite because a trailer may be inserted before the document is actually
                    ////saved, but after our save prep code has already added bookmarks
                    //if (oStamp is Trailer)
                    //{
                    //    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.ConversionClass();
                    //    object oNodes = null;
                    //    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    //        oNodes = (object)oStamp.WordTags;
                    //    else
                    //        oNodes = (object)oStamp.ContentControls;
                    //    oConvert.BookmarkBoundingObjects(m_oMPDocument.WordDocument, oNodes);
                    //}

                    //GLOG 4418: If trailers inserted in multiple sections, Tags are Refreshed once here,
                    //rather than after every insertion
                    if (!bConvertTrailers && rbDocument.Checked && m_oMPDocument.WordDocument.Sections.Count > 1)
                    {
                        //JTS 2/20/12: Not necessary for Trailers, since Bounding Objects will have been removed
                        if (m_iAssignedObjectType == mpObjectTypes.DraftStamp)
                        {
                            m_oMPDocument.RefreshTags();
                            //Update Editor tree also
                            if (m_oTP != null)
                                m_oTP.DocEditor.RefreshTree(true);
                        }
                    }
                    if (m_iAssignedObjectType == mpObjectTypes.Trailer && bConvertTrailers)
                    {
                        //GLOG 4553: Mark converted 9.x trailer to avoid future prompts unless saved as new document
                        SetConverted9xTrailerVariable();
                    }
                }
                //GLOG 5589: Update TaskPane tags to reflect latest values
                if (m_oTP != null && m_iAssignedObjectType == mpObjectTypes.DraftStamp)
                    m_oTP.DocEditor.RefreshTree(true);
                
                //GLOG 6156 (dm) - don't do doc var maintenance on update existing,
                //since nothing has changed - this will also avoid error in documents
                //from early enhancement period when None/Remove was removing the last
                //prefill doc vars for trailers in non-targeted sections
                if (m_iAssignedObjectType == mpObjectTypes.Trailer)
                {
                    if (m_iCurSegmentID != mpUpdateExistingTrailersSegmentID)
                    {
                        string xSections = "";
                        if (rbMultiple.Checked)
                            //GLOG 6107 (dm) - strip spaces
                            xSections = this.txtMultiple.Text.Replace(" ", "");
                        else if (rbSection.Checked)
                            xSections = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Sections[1].Index.ToString();
                        ClearNoTrailerVariable();
                        //JTS 6/24/11:  Save current settings in Doc Variable for re-use
                        SetLastTrailerVariable(oPrefill.SegmentID + mpPrefillVariableSeparator.ToString() + oPrefill.Name +
                            mpPrefillVariableSeparator.ToString() + oPrefill.ToString(), xSections, iSegmentID);
                        //GLOG 4553: Clear Variable marking converted 9.x trailer, if Custom code is not enabled
                        if (!bConvertTrailers)
                            ClearConverted9xTrailerVariable();
                    }
                    else
                    {
                        //GLOG 6271: Update just the DocID part of variable on Update Existing
                        //Doc Name might have changed on Save As
                        string xVar = GetLastTrailerVariable(iSegmentID);
                        string []aVars = xVar.Split(mpPrefillVariableSeparator);
                        string xPrefill = "";
                        if (aVars.Length > 2)
                        {
                            xPrefill = aVars[0] + mpPrefillVariableSeparator.ToString() + aVars[1] + mpPrefillVariableSeparator.ToString() +
                                aVars[2];
                        }
                        else
                        {
                            //GLOG 6431 (dm) - oPrefill will be null if the
                            //zzmp10LastTrailerInserted doc vars have been deleted
                            if (oPrefill != null)
                            {
                                xPrefill = oPrefill.SegmentID + mpPrefillVariableSeparator.ToString() + oPrefill.Name + mpPrefillVariableSeparator.ToString() +
                                    oPrefill.ToString();
                            }
                            else
                            {
                                //GLOG 6431 (dm) - create just prefill root so that the
                                //doc id is positioned correctly in the new doc var
                                xPrefill = iSegmentID.ToString() + ".0" + mpPrefillVariableSeparator.ToString() +
                                    mpPrefillVariableSeparator.ToString();
                            }
                        }
                        string xSections = "";
                        if (aVars.Length > 4)
                            xSections = aVars[4];
                        SetLastTrailerVariable(xPrefill, xSections, iSegmentID);
                    }
                }
                MacPac.Application.SetXMLTagStateForEditing();

                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                if (m_oTP != null)
                    m_oTP.ScreenUpdating = true;
                else
                    SetWordEcho(true);
            }
        }
        private void RemoveStamps(ArrayList aSections)
        {
            //GLOG : 6499 : CEH
            //save state
            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(Session.CurrentWordApp.ActiveDocument);
            oEnv.SaveState();

            try
            {
                if (m_oTP != null)
                    m_oTP.ScreenUpdating = false;
                else
                    SetWordEcho(false);

                //Delete segments that match the appropriate Type ID
                //4-26-12 (dm) - moved this block to before we remove non-segment
                //trailers because, as of GLOG 5929, RemoveOrphanedTrailers now removes
                //segment trailers as well - this was causing an error when specifying
                //sections because the primary tag/content control was no longer there
                for (int i = m_oMPDocument.Segments.Count - 1; i >= 0; i--)
                {
                    Segment oSegment = m_oMPDocument.Segments[i];
                    if (oSegment.TypeID == m_iAssignedObjectType)
                    {
						//GLOG: 6501 : CEH
						//run for all stamp types
                        //if (m_iAssignedObjectType == mpObjectTypes.Trailer && aSections != null && aSections.Count > 0)
                        if (aSections != null && aSections.Count > 0)
                            {
                            if (!aSections.Contains(oSegment.PrimaryRange.Sections[1].Index)) //GLOG 6808
                                continue;
                        }
                        //GLOG 4418
                        m_oMPDocument.DeleteSegment(oSegment, false);
                    }
                }

                if (m_iAssignedObjectType == mpObjectTypes.Trailer)
                {
                    object[] oSections = new object[0];
                    if (aSections != null)
                    {
                        //3-28-12 (dm) - if linked section is specified, trailer won't be
                        //deleted by RemoveOrphanedTrailers unless we specify the section
                        //that actually contains the anchor
                        //4-25-12 (dm) - don't modify aSections itself
                        ArrayList aSectionsMod = (ArrayList)aSections.Clone();
                        if (m_aExistingTrailers != null)
                        {
                            for (int i = 0; i < aSectionsMod.Count; i++)
                            {
                                int iIndex = ((System.Int32)aSectionsMod[i]) - 1;
                                object oZero = 0;
                                if ((m_aExistingTrailers[iIndex] != null) &&
                                    (m_aExistingTrailers[iIndex] != oZero))
                                {
                                    try
                                    {
                                        while (m_aExistingTrailers[iIndex].ToString() == "Linked")
                                            iIndex--;
                                        aSectionsMod[i] = iIndex + 1;
                                    }
                                    catch { }
                                }
                            }
                        }

                        oSections = aSectionsMod.ToArray();
                    }

                    //object[] oSections = new object[aSections.Count];
                    //for (int i = 0; i < aSections.Count; i++)
                    //{
                    //    oSections[i] = aSections[i];
                    //}
                    //Remove old 8.x/9.x trailers
                    //GLOG 6108 (dm) - added ability to target specific sections
                    LMP.Forte.MSWord.WordDoc.Remove8xTrailers(
                        Session.CurrentWordApp.ActiveDocument, true, oSections);
                    //GLOG 5829 (12/29/11): Always check for orphaned trailers -
                    //previous code may have missed trailers with XML Tags stripped from saving
                    //in Word versions that don't support tags.
                    ////12/30/10 JTS:  Don't do this check if there's a known good trailer
                    //if (m_oExistingStamp == null)
                    //{
                        //12/27/10 JTS: Check for MacPac Trailer textboxes that have had invalid tags removed
                        try
                        {
                            LMP.Forte.MSWord.WordDoc.RemoveOrphanedTrailers(
                                Session.CurrentWordApp.ActiveDocument,
                                "MacPac Trailer", oSections);
                        }
                        catch 
                        { 
                        	//just continue if there's an error 
                        }
                    //}
                }
                else if (m_iAssignedObjectType == mpObjectTypes.DraftStamp)
                {
                    //GLOG 4386: Remove old 9.x Draft Stamps
                    LMP.Forte.MSWord.WordDoc.Remove9xDocumentStamps(Session.CurrentWordApp.ActiveDocument);
                }

                //JTS 2/20/12:  Only call RefreshTags if there was a Tagged Trailer or Draft Stamp to begin with
                //Otherwise, NodeStore won't have been affected
                if (m_iAssignedObjectType == mpObjectTypes.DraftStamp || m_oExistingStamp != null)
                {
                    //GLOG 4418
                    m_oMPDocument.RefreshTags(m_oMPDocument.Mode != ForteDocument.Modes.Design);
                }
                if (m_iAssignedObjectType == mpObjectTypes.Trailer)
                {
                    ClearNoTrailerVariable();
                }
            }
            catch (System.Exception oE)
            {
                if (m_oTP != null)
                {
                    m_oTP.ScreenUpdating = true;
                }
                else
                    SetWordEcho(true);
                LMP.Error.Show(oE);
            }
            finally
            {
                //restore state
                oEnv.RestoreState(true, false, false);
            }
        }
        /// <summary>
        /// Setup controls based when selected stamp type changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lstTypes.SelectedValue != null)
                {
                    m_iCurSegmentID = Int32.Parse(lstTypes.SelectedValue.ToString());
                    this.DisplayControls();
                }
                else
                    m_iCurSegmentID = 0;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public DialogResult ShowDialog(Word.Document oDoc, bool bDisplay, bool bAuto)
        {
            try
            {
                //GLOG 6192 (dm)
                m_bDisplayDialog = bDisplay;

                //GLOG 3246: Get Section for current selection
                Word.Section oSection = oDoc.Application.Selection.Range.Sections[1];

                m_iCurrentSection = oSection.Index;
                //GLOG 4243: determine if any stories are linked
                m_bSectionHasLinkedStories = LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(oSection, 
                    ref m_bLinkedPrimaryFooter, ref m_bLinkedPrimaryHeader, ref m_bLinkedFirstFooter, ref m_bLinkedFirstHeader, 
                    ref m_bLinkedEvenFooter, ref m_bLinkedEvenHeader, false, true);
                
                ////modify radio button if necessary
                //if (LMP.Forte.MSWord.WordDoc.HeaderFooterIsLinkedToSourceOrTarget(
                //    oSection, m_iMode == Mode.DocumentStamp,
                //    Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary))
                //{
                //    rbSection.Text = rbSection.Text + " (Section " + m_iCurrentSection.ToString() + ") and linked sections";
                //}
                //else
                //{
                //    rbSection.Text = rbSection.Text + " (Section " + m_iCurrentSection.ToString() + ")";
                //}
                //GLOG 4243
                m_xSectionLabel = rbSection.Text + " (Section " + m_iCurrentSection.ToString() + ")";
                rbSection.Text = m_xSectionLabel;

                //get current task pane
                try
                {
                    m_oTP = TaskPanes.Item(oDoc);
                }
                catch { }

                //get associated MacPac document
                if (m_oTP != null)
                    m_oMPDocument = m_oTP.ForteDocument;
                else
                {
                    m_oMPDocument = new ForteDocument(oDoc);
                }

                if (m_oMPDocument.Segments.Count == 0)
                {
                    SetWordEcho(false);
                    //Need to refresh to be able to access Segment collection
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    if (m_oMPDocument.Mode == ForteDocument.Modes.Design)
                        m_oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes, false);
                    else
                        m_oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes, false);
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    LMP.MacPac.Application.SetXMLTagStateForEditing();
                    SetWordEcho(true);
                }
                //GLOG 4536: No Trailer Integration default was not being honored if 
                //all Segments had this set
                if (m_iMode == Mode.Trailer)
                {
                    //Check if default is to insert no trailer automatically
                    int iDefTrailerID = 0;

                    //GLOG item #4041 - dcf
                    //get first trailerable segment in document
                    for (int iSeg = 0; iSeg < m_oMPDocument.Segments.Count; iSeg++)
                    {
                        //GLOG 7721: Get current Default Trailer from Segments record,
                        //rather than Segment in document
                        Segment oSegment = m_oMPDocument.Segments[iSeg];
                        if (oSegment is AdminSegment)
                        {
                            AdminSegmentDef oDBSegment = null;
                            try
                            {
                                oDBSegment = (AdminSegmentDef)Segment.GetDefFromID(oSegment.ID);
                            }
                            catch
                            {
                            }
                            //Use document Segment default if original record is no longer in database
                            if (oDBSegment == null || oDBSegment.Name != oSegment.Name)
                                iDefTrailerID = oSegment.DefaultTrailerID;
                            else
                                iDefTrailerID = GetDefaultTrailerIDForSegmentDef(oDBSegment);
                        }
                        else
                            iDefTrailerID = oSegment.DefaultTrailerID;

                        if (iDefTrailerID == mpSkipTrailerSegmentID)
                            continue;
                        else
                        {
                            m_oSegment = oSegment;
                            break;
                        }
                    }
                    if ((m_oSegment != null) && (m_oSegment is AdminSegment) 
                        && (m_oSegment.TypeID != mpObjectTypes.Trailer))
                    {
                        for (int i = 0; i < m_oSegment.Segments.Count; i++)
                        {
                            //Check for any Paper-type children with assignments first
                            if (m_oSegment.Segments[i] is Paper)
                            {
                                //GLOG 7721: Get current Default Trailer from Segments record,
                                //rather than Segment in document
                                Segment oSegment = m_oSegment.Segments[i];
                                AdminSegmentDef oDBSegment = null;
                                try
                                {
                                    oDBSegment = (AdminSegmentDef)Segment.GetDefFromID(oSegment.ID);
                                }
                                catch
                                {
                                }
                                //Use document Segment default if original record is no longer in database
                                if (oDBSegment == null || oDBSegment.Name != oSegment.Name)
                                    iDefTrailerID = oSegment.DefaultTrailerID;
                                else
                                    iDefTrailerID = GetDefaultTrailerIDForSegmentDef(oDBSegment);
                                break;
                            }
                        }
                    }
                    if (iDefTrailerID == 0)
                    {
                        //GLOG 8033: check for template-specific assignment
                        iDefTrailerID = GetDefaultTrailerIDForTemplate(oDoc);
                    }
                    //No Segment not marked for no trailer was found
                    if (bAuto && iDefTrailerID == mpSkipTrailerSegmentID)
                        return DialogResult.Cancel;
                }
                //set dialog title
                if (m_iMode == Mode.DocumentStamp)
                {
                    m_iAssignedObjectType = mpObjectTypes.DraftStamp;
                    this.Text = LMP.Resources.GetLangString("Dialog_DocumentStamp_Title");
                }
                else
                {
                    m_iAssignedObjectType = mpObjectTypes.Trailer;
                    this.Text = LMP.Resources.GetLangString("Dialog_Trailer_Title");
                    this.lblTypes.Text = "&Locations:"; //3-28-12 (dm)
                }

                //4-26-12 (dm) - multiple sections option was never meant
                //to display for draft stamp
                if (m_iAssignedObjectType == mpObjectTypes.DraftStamp)
                {
                    this.rbMultiple.Visible = false;
                    this.txtMultiple.Visible = false;
                }

                for (int i = 0; i < m_oMPDocument.Segments.Count; i++)
                {
                    //Look for document stamp of selected type
                    if (m_oMPDocument.Segments[i].TypeID == m_iAssignedObjectType)
                    {
                        m_oExistingStamp = m_oMPDocument.Segments[i];

                        //GLOG 3246: If there is a trailer for current section, stop looking
                        //If current section has no trailer, settings of first existing trailer will be used
                        Word.Range o = m_oExistingStamp.PrimaryRange; //GLOG 6808
                        if (o.Sections[1].Index == m_iCurrentSection)
                            break;
                    }
                }
				//JTS 6/24/11: If no Trailer tags exist, check for doc var containing Prefill information
                if (m_oExistingStamp == null && m_iAssignedObjectType == mpObjectTypes.Trailer)
                {
                    int iTrailerID = 0;
                    //4-3-12 (dm) - extended enhancements to 9.x-style trailer option
                    object oTrailers = null;
                    LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                    if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains(
                        "CONVERTTRAILERSTO9XFORMAT") || oFirmSettings.UseMacPac9xStyleTrailer)
                        oTrailers = LMP.Forte.MSWord.WordDoc.GetConverted9xTrailers(oDoc, "zzmpTrailerItem");
                    else
                        oTrailers = LMP.Forte.MSWord.WordDoc.GetExistingTrailers(oDoc, "MacPac Trailer");
                    if (oTrailers != null)
                    {
                        m_aExistingTrailers = (object[])oTrailers;
                        //GLOG 7237:  If any existing Trailer segments no longer exists in database, 
                        //don't display Update Existing option
                        for (int i = 0; i < m_aExistingTrailers.GetLength(0); i++)
                        {
                            if (m_aExistingTrailers[i] != null)     //GLOG : 8290 : ceh
                            {
                                int iExistingID = 0;
                                if (Int32.TryParse(m_aExistingTrailers[i].ToString(), out iExistingID))
                                {
                                    ISegmentDef oTrailerDef = null;
                                    try
                                    {
                                        oTrailerDef = Segment.GetDefFromID(iExistingID.ToString());
                                    }
                                    catch { }
                                    //GLOG 8386: Check that specified ID still corresponds to a Trailer segment
                                    if (oTrailerDef == null || oTrailerDef.TypeID != mpObjectTypes.Trailer)
                                    {
                                        m_aExistingTrailers = null;
                                        break;
                                    }
                                }
                            }
                       }
                    }
                    if (m_aExistingTrailers != null)
                    {
                        //2-29-12 (dm) - treat "end of document" as document-wide
                        if (SingleTrailerAtEndOfDocument())
                        {
                            m_bSingleTrailerAtEndOfDoc = true;
                            oSection = m_oMPDocument.WordDocument.Sections.Last;
                        }
                        //3-20-12 (dm) - if section has linked trailer, cycle back
                        //to find the appropriate id
                        int iIndex = oSection.Index - 1;
                        object oZero = 0;
                        if ((m_aExistingTrailers[iIndex] != null) &&
                            (m_aExistingTrailers[iIndex] != oZero))
                        {
                            try
                            {
                                while (m_aExistingTrailers[iIndex].ToString() == "Linked")
                                    iIndex--;
                                iTrailerID = Int32.Parse(m_aExistingTrailers[iIndex].ToString());
                            }
                            catch { }
                        }
                        //3-20-12 (dm) - set prefill only if this section is trailered -
                        //otherwise, it should use defaults for the content in that section
                        if (iTrailerID > 0)
                        {
                            m_oPrefill = LastTrailerPrefill(iTrailerID);
                        }
                    }
                    else
                        m_oPrefill = LastTrailerPrefill(iTrailerID);
                }
                SetupForm(bAuto);

                //Show Dialog only if specified
                if (bDisplay)
                {
                    //GLOG : CEH : 7174
                    Session.CurrentWordApp.ScreenUpdating = true;
                    Session.CurrentWordApp.ScreenRefresh();

                    return base.ShowDialog(LMP.OS.GetWordWindow());
                }
                else
                {
                    m_iPendingAction = PendingAction.Insert;
                    return DialogResult.OK;
                }
            }
            catch (System.Exception oE)
            {
                if (m_oTP == null)
                    SetWordEcho(true);
                throw oE;
            }
        }
        //public DialogResult ShowDialog(Word.Document oDoc, bool bDisplay, bool bAuto)
        //{
        //    try
        //    {
        //        //GLOG 3246: Get Section for current selection
        //        m_iCurrentSection = oDoc.Application.Selection.Range.Sections[1].Index;
        //        rbSection.Text = rbSection.Text + " (Section " + m_iCurrentSection.ToString() + ")";
        //        //get current task pane
        //        try
        //        {
        //            m_oTP = TaskPanes.Item(oDoc);
        //        }
        //        catch { }

        //        //get associated MacPac document
        //        if (m_oTP != null)
        //            m_oMPDocument = m_oTP.ForteDocument;
        //        else
        //        {
        //            m_oMPDocument = new ForteDocument(oDoc);
        //        }

        //        if (m_oMPDocument.Segments.Count == 0)
        //        {
        //            SetWordEcho(false);
        //            //Need to refresh to be able to access Segment collection
        //            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
        //            if (m_oMPDocument.Mode == ForteDocument.Modes.Design)
        //                m_oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes, false);
        //            else
        //                m_oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes, false);
        //            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
        //            LMP.MacPac.Application.SetXMLTagStateForEditing();
        //            SetWordEcho(true);
        //        }

        //        //get first top segment in selection
        //        try
        //        {
        //            m_oSegment = m_oMPDocument.GetTopLevelSegmentFromSelection();
        //        }
        //        catch { }

        //        if (bAuto && m_iAssignedObjectType == mpObjectTypes.Trailer && (m_oSegment != null) &&
        //            (m_oSegment is AdminSegment) && (m_oSegment.TypeID != m_iAssignedObjectType))
        //        {

        //            //Check if default is to insert no trailer automatically
        //            int iDefTrailerID = 0;
        //            for (int i = 0; i < m_oSegment.Segments.Count; i++)
        //            {
        //                //Check for any Paper-type children with assignments first
        //                if (m_oSegment.Segments[i] is Paper)
        //                {
        //                    iDefTrailerID = m_oSegment.Segments[i].DefaultTrailerID;
        //                    break;
        //                }
        //            }

        //            if (iDefTrailerID == 0)
        //            {
        //                iDefTrailerID = m_oSegment.DefaultTrailerID;
        //            }
        //            if (iDefTrailerID == mpSkipTrailerSegmentID)
        //            {
        //                return DialogResult.Cancel;
        //            }
        //        }

        //        //set dialog title
        //        if (m_iMode == Mode.DocumentStamp)
        //        {
        //            m_iAssignedObjectType = mpObjectTypes.DraftStamp;
        //            this.Text = LMP.Resources.GetLangString("Dialog_DocumentStamp_Title");
        //        }
        //        else
        //        {
        //            m_iAssignedObjectType = mpObjectTypes.Trailer;
        //            this.Text = LMP.Resources.GetLangString("Dialog_Trailer_Title");
        //        }

        //        for (int i = 0; i < m_oMPDocument.Segments.Count; i++)
        //        {
        //            //Look for document stamp of selected type
        //            if (m_oMPDocument.Segments[i].TypeID == m_iAssignedObjectType)
        //            {
        //                m_oExistingStamp = m_oMPDocument.Segments[i];
        //                //GLOG 3246: If there is a trailer for current section, stop looking
        //                //If current section has no trailer, settings of first existing trailer will be used
        //                Word.Range o = m_oExistingStamp.WordTags[0].Range;
        //                if (m_oExistingStamp.WordTags[0].Range.Sections[1].Index == m_iCurrentSection)
        //                    break;
        //            }
        //        }

        //        SetupForm();

        //        //Show Dialog only if specified
        //        if (bDisplay)
        //            return base.ShowDialog();
        //        else
        //        {
        //            m_iPendingAction = PendingAction.Insert;
        //            return DialogResult.OK;
        //        }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        if (m_oTP == null)
        //            SetWordEcho(true);
        //        throw oE;
        //    }
        //}
        //GLOG : 2476 : CEH
        //JTS 6/24/11:  Modified to take Prefill argument instead of Segment
        //GLOG : 6262 : CEH - modified to take 'object oTrailers' argument
        public DialogResult ShowDialog(object oStamps, Prefill oExistingStamp, int iScope, bool bDisplay, bool bAuto)
        {
            try
            {
                //GLOG 6192 (dm)
                m_bDisplayDialog = bDisplay;

                m_iScope = iScope;
                //GLOG 4444: Scope=1 for entire document, 0 for current section
                if (iScope == 1)
                    m_iCurrentSection = 1;
                else
                    m_iCurrentSection = Session.CurrentWordApp.Selection.Sections[1].Index;
                
                //set dialog title
                if (m_iMode == Mode.DocumentStamp)
                {
                    m_iAssignedObjectType = mpObjectTypes.DraftStamp;
                    this.Text = LMP.Resources.GetLangString("Dialog_DocumentStamp_Title");
                }
                else
                {
                    m_iAssignedObjectType = mpObjectTypes.Trailer;
                    this.Text = LMP.Resources.GetLangString("Dialog_Trailer_Title");
                    this.lblTypes.Text = "&Locations:"; //3-28-12 (dm)
                }

                //4-26-12 (dm) - multiple sections option was never meant
                //to display for draft stamp
                if (m_iAssignedObjectType == mpObjectTypes.DraftStamp)
                {
                    this.rbMultiple.Visible = false;
                    this.txtMultiple.Visible = false;
                }

                //JTS 6/24/11
                m_oMPDocument = new ForteDocument(Session.CurrentWordApp.ActiveDocument);
                m_oPrefill = oExistingStamp;

                //GLOG 6240 (dm) - get post-enhancement trailers
                if (m_iAssignedObjectType == mpObjectTypes.Trailer)
                {
                    if (oStamps == null)
                    {
                        Word.Document oDoc = m_oMPDocument.WordDocument;
                        LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                        if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains(
                            "CONVERTTRAILERSTO9XFORMAT") || oFirmSettings.UseMacPac9xStyleTrailer)
                            oStamps = LMP.Forte.MSWord.WordDoc.GetConverted9xTrailers(oDoc, "zzmpTrailerItem");
                        else
                            oStamps = LMP.Forte.MSWord.WordDoc.GetExistingTrailers(oDoc, "MacPac Trailer");
                    }

                    if (oStamps != null)
                        m_aExistingTrailers = (object[])oStamps;
                }
                else    //GLOG : 8178 : ceh - get existing Draft Stamp
                {
                    m_oExistingStamp = (Segment)oStamps;
                }

                SetupForm(bAuto);

                //Show Dialog only if specified
                if (bDisplay)
                    return base.ShowDialog();
                else
                {
                    m_iPendingAction = PendingAction.Insert;
                    return DialogResult.OK;
                }
            }
            catch (System.Exception oE)
            {
                if (m_oTP == null)
                    SetWordEcho(true);
                throw oE;
            }
        }
        /// <summary>
        /// Loads appropriate list of segments
        /// </summary>
        private void SetupForm(bool bAuto)
        {
            //2-29-12 (dm) - added bAuto parameter
            ArrayList oArray = null;
            FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
            string xDefTrailerIDs = "|";
            int iDefTrailerID = 0;

            if ((m_oSegment != null) && (m_oSegment is AdminSegment) && 
                (m_oSegment.TypeID != m_iAssignedObjectType))
            {
                Assignments oAssignments = null;
                mpObjectTypes iTargetObjectType = m_oSegment.TypeID;
                int iTargetObjectID = m_oSegment.ID1;
                Trace.WriteNameValuePairs("iTargetObjectType", iTargetObjectType,
                     "iTargetObjectID", iTargetObjectID, "iAssignedObjectType", m_iAssignedObjectType);
                
                for (int i = 0; i < m_oSegment.Segments.Count; i++)
                {
                    //Check for any Paper-type children with assignments first
                    if (m_oSegment.Segments[i] is Paper)
                    {
                        //get assigned stamps for the child segment
                        oAssignments = new Assignments(m_oSegment.Segments[i].TypeID,
                            m_oSegment.Segments[i].ID1, m_iAssignedObjectType);
                        //GLOG 7721: Get current Default Trailer from Segments record,
                        //rather than Segment in document
                        Segment oSegment = m_oSegment.Segments[i];
                        AdminSegmentDef oDBSegment = null;
                        try
                        {
                            oDBSegment = (AdminSegmentDef)Segment.GetDefFromID(oSegment.ID);
                        }
                        catch
                        {
                        }
                        //Use document Segment default if original record is no longer in database
                        if (oDBSegment == null || oDBSegment.Name != oSegment.Name)
                            iDefTrailerID = oSegment.DefaultTrailerID;
                        else
                            iDefTrailerID = GetDefaultTrailerIDForSegmentDef(oDBSegment);
                        break;
                    }
                }

                if (oAssignments == null || oAssignments.Count == 0)
                {
                    //get assigned stamps for the segment
                    oAssignments = new Assignments(iTargetObjectType,
                        iTargetObjectID, m_iAssignedObjectType);
                }
                if (iDefTrailerID == 0)
                {
                    if (m_iAssignedObjectType == mpObjectTypes.Trailer)
                    {
                        //GLOG 7721: Get current Default Trailer from Segments record,
                        //rather than Segment in document
                        Segment oSegment = m_oSegment;
                        AdminSegmentDef oDBSegment = null;
                        try
                        {
                            oDBSegment = (AdminSegmentDef)Segment.GetDefFromID(oSegment.ID);
                        }
                        catch
                        {
                        }
                        //Use document Segment default if original record is no longer in database
                        if (oDBSegment == null || oDBSegment.Name != oSegment.Name)
                            iDefTrailerID = oSegment.DefaultTrailerID;
                        else
                            iDefTrailerID = GetDefaultTrailerIDForSegmentDef(oDBSegment);
                    }
                }

                if (oAssignments.Count > 0)
                    oArray = oAssignments.ToArrayList();
            }

            if (oArray == null)
            {
                //If no assignments, load default trailers
                AdminSegmentDefs oDefs = new AdminSegmentDefs(m_iAssignedObjectType);
                //Load ID and DisplayName in array
                oArray = oDefs.ToArrayList(0, 1);

                if (m_iAssignedObjectType == mpObjectTypes.DraftStamp)
                {
                    for (int i = 0; i < oSettings.DefaultDraftStampIDs.Length; i++)
                    {
                        xDefTrailerIDs += oSettings.DefaultDraftStampIDs[i].ToString() + "|";
                    }
                }
                else
                {
                    //GLOG 8033: check for template-specific assignments
                    int[] arrIDs = new int[] { };
                    arrIDs = GetTrailerAssignmentsForTemplate(Session.CurrentWordApp.ActiveDocument);
                    if (arrIDs.GetLength(0) > 0)
                    {
                        iDefTrailerID = GetDefaultTrailerIDForTemplate(Session.CurrentWordApp.ActiveDocument);
                    }
                    else
                    {
                        //Use general default
                        arrIDs = oSettings.DefaultTrailerIDs;
                        for (int i = 0; i < arrIDs.Length; i++)
                        {
                            xDefTrailerIDs += arrIDs[i].ToString() + "|";
                        }
                    }

                }
                xDefTrailerIDs = xDefTrailerIDs.TrimEnd('|'); //GLOG 8603
                for (int i = oArray.Count - 1; i >= 0; i--)
                {
                    string xTrailerID = ((object[])oArray[i])[0].ToString();
                    if (xDefTrailerIDs != "" && !xDefTrailerIDs.Contains(xTrailerID))
                        oArray.RemoveAt(i);
                }
            }

            if (m_iAssignedObjectType == mpObjectTypes.Trailer)
            {
                //Add No Trailer item
                object[] oNoTrailer = new object[] { mpNoTrailerPromptSegmentID, LMP.Resources.GetLangString("Prompt_NoTrailerDoNotPrompt") };
                oArray.Add(oNoTrailer);
                if (m_aExistingTrailers != null)
                {
                    //Add Update Existing item
                    object[] oUpdateExisting = new object[] { mpUpdateExistingTrailersSegmentID, LMP.Resources.GetLangString("Prompt_UpdateExistingTrailers") };
                    oArray.Add(oUpdateExisting);
                }
            }
            else
            {
                ////add Remove All Stamps if document contains mp document schema
                //if (LMP.Forte.MSWord.WordDoc.IsForteDoc(LMP.Forte.MSWord.WordApp.ActiveDocument()))
                //{
                //GLOG 4386: Always show Remove All item, so 9.x stamps can also be removed
                object[] oRemoveAll = new object[] { mpRemoveAllStampsSegmentID, LMP.Resources.GetLangString("Prompt_RemoveAllDraftStamps") };
                //GLOG : 8170 : ceh
                if (oSettings.SetRemoveAllStampsAsFirstInList)
                    oArray.Insert(0, oRemoveAll);
                else
                    oArray.Add(oRemoveAll);
            }

            lstTypes.SetList(oArray);

            try
            {
                //GLOG - 3619 - ceh
                //Default to existing type
                //GLOG  6156 (dm) - if there's a post-enhancement trailer, the
                //new default rules should take precedence
                if (m_aExistingTrailers != null)
                {
                    //2-29-12 (dm) - this block replaces remmed out (m_oPrefill != null) block below
                    if (bAuto)
                        //on autotrailer, default to "Update Existing" whenever
                        //there's any trailer in the document
                        lstTypes.SelectedValue = mpUpdateExistingTrailersSegmentID;
                    else
                    {
                        //manual trailer
                        if (m_iScope == 1)
                            rbDocument.Checked = true;
                        else if (m_iScope == 0)
                            rbSection.Checked = true;
                        else
                        {
                            //set type default
                            if (m_oPrefill != null)
                            {
                                //section has a trailer - use the existing type
                                int iID1, iID2;
                                LMP.Data.Application.SplitID(m_oPrefill.SegmentID,
                                    out iID1, out iID2);
                                lstTypes.SelectedValue = iID1;
                            }
                            else
                            {
                                //untrailered section - use default type for content
                                int iID = 0;
                                Word.Section oSection = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Range.Sections[1];
                                Segment oSegment = m_oMPDocument.GetTopLevelSegmentForRange(
                                    oSection.Range, true);
                                if (oSegment != null)
                                {
                                    //section has content - first check for paper default
                                    for (int i = 0; i < oSegment.Segments.Count; i++)
                                    {
                                        if (oSegment.Segments[i] is Paper)
                                        {
                                            //GLOG 7721: Get current Default Trailer from Segments record,
                                            //rather than Segment in document
                                            AdminSegmentDef oDBSegment = null;
                                            try
                                            {
                                                oDBSegment = (AdminSegmentDef)Segment.GetDefFromID(oSegment.Segments[i].ID);
                                            }
                                            catch
                                            {
                                            }
                                            //Use document Segment default if original record is no longer in database
                                            if (oDBSegment == null || oDBSegment.Name != oSegment.Segments[i].Name)
                                                iID = oSegment.Segments[i].DefaultTrailerID;
                                            else
                                                iID = GetDefaultTrailerIDForSegmentDef(oDBSegment);
                                            break;
                                        }
                                    }

                                    //no paper default - check if target segment has one
                                    if (iID == 0)
                                    {
                                        //GLOG 7721: Get current Default Trailer from Segments record,
                                        //rather than Segment in document
                                        AdminSegmentDef oDBSegment = null;
                                        try
                                        {
                                            oDBSegment = (AdminSegmentDef)Segment.GetDefFromID(oSegment.ID);
                                        }
                                        catch
                                        {
                                        }
                                        //Use document Segment default if original record is no longer in database
                                        if (oDBSegment == null || oDBSegment.Name != oSegment.Name)
                                            iID = oSegment.DefaultTrailerID;
                                        else
                                            iID = GetDefaultTrailerIDForSegmentDef(oDBSegment);
                                    }
                                }

                                if (iID != 0)
                                {
                                    //there's a default trailer for content in section
                                    try
                                    {
                                        lstTypes.SelectedValue = iID;
                                    }
                                    catch
                                    {
                                        //default for section isn't on the list -
                                        //fall back to more general defaults
                                        if (iDefTrailerID != 0)
                                            lstTypes.SelectedValue = iDefTrailerID;
                                        else
                                            lstTypes.SelectedValue = oSettings.DefaultTrailerID.ToString();
                                    }
                                }
                                else
                                    if (iDefTrailerID != 0)
                                        lstTypes.SelectedValue = iDefTrailerID;
                                    else
                                        //use firm-wide default
                                        lstTypes.SelectedValue = oSettings.DefaultTrailerID.ToString();
                            }
                            
                            //set scope - default to "current section" only when
                            //there are discrepancies between the sections -
                            //exception is single trailer at end of document
                            bool bCurrentSection = false;
                            if (!m_bSingleTrailerAtEndOfDoc)
                            {
                                object oTest = m_aExistingTrailers[0];
                                if (oTest == null)
                                    oTest = "";
                                for (int o = 1; o < m_aExistingTrailers.GetLength(0); o++)
                                {
                                    string xOtherExistingID = "";
                                    if (m_aExistingTrailers[o] != null)
                                        xOtherExistingID = m_aExistingTrailers[o].ToString();
                                    if ((oTest.ToString() != xOtherExistingID) &&
                                        (xOtherExistingID != "Linked"))
                                    {
                                        bCurrentSection = true;
                                        break;
                                    }
                                }
                            }
                            if (bCurrentSection)
                                rbSection.Checked = true;
                            else
                                rbDocument.Checked = true;
                        }
                    }
                }
                else if (GetNoTrailerVariable() != "" && GetConverted9xTrailerVariable() == "" &&
                    m_iAssignedObjectType == mpObjectTypes.Trailer)
                    //GLOG 4553: Don't default to None/Remove if there is a converted 9.x trailer
                    lstTypes.SelectedValue = mpNoTrailerPromptSegmentID;
                else if (m_oExistingStamp != null)
                {
                    //GLOG 4444: Select appropriate scope for recreated Trailer
                    lstTypes.SelectedValue = m_oExistingStamp.ID1;
                    if (m_iScope == 1)
                        rbDocument.Checked = true;
                    else if (m_iScope == 0) //GLOG 4478: Only set if value has been passed from RecreateTrailer
                        rbSection.Checked = true;
                }
                //else if (m_oPrefill != null) //JTS 6/24/11
                //{
                //    int iID1, iID2;
                //    LMP.Data.Application.SplitID(m_oPrefill.SegmentID, out iID1, out iID2);
                //    lstTypes.SelectedValue = iID1;
                //    if (m_iScope == -1)
                //    {
                //        bool bUseUpdateDefault = false;
                //        if (m_aExistingTrailers != null)
                //        {
                //            object oTest = m_aExistingTrailers[0];
                //            if (oTest == null)
                //                oTest = "";
                //            for (int o = 1; o < m_aExistingTrailers.GetLength(0); o++)
                //            {
                //                //2-28-12 (dm) - this was erring when there was an
                //                //untrailered section
                //                string xOtherExistingID = "";
                //                if (m_aExistingTrailers[o] != null)
                //                    xOtherExistingID = m_aExistingTrailers[o].ToString();
                //                if (oTest.ToString() != xOtherExistingID)
                //                {
                //                    bUseUpdateDefault = true;
                //                    break;
                //                }
                //            }
                //        }
                //        if (bUseUpdateDefault)
                //        {
                //            lstTypes.SelectedValue = mpUpdateExistingTrailersSegmentID;
                //        }
                //        else
                //        {
                //            string xLastTrailer = DocumentStampUI.GetLastTrailerVariable(iID1);
                //            if (xLastTrailer != "")
                //            {
                //                try
                //                {
                //                    string[] aVals = xLastTrailer.Split(DocumentStampUI.mpPrefillVariableSeparator);
                //                    //2-28-12 (dm): code was previously checking aVals[3] - it may be in the
                //                    //fifth position only due to the inadvertent inclusion of oPrefill.Name,
                //                    //which always appears to be empty, but I'm reluctant to remove this info
                //                    //without knowing the intent
                //                    if (aVals.Length >= 5 && aVals[4] != "")
                //                    {
                //                        if (aVals[4] == LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Sections[1].Index.ToString())
                //                            rbSection.Checked = true;
                //                        else
                //                        {
                //                            rbDocument.Checked = true;
                //                            //rbMultiple.Checked = true;
                //                            //txtMultiple.Text = aVals[4];
                //                        }
                //                    }
                //                    else
                //                        rbDocument.Checked = true;
                //                }
                //                catch
                //                {
                //                    rbDocument.Checked = true;
                //                }
                //            }
                //            else
                //            {
                //                rbDocument.Checked = true;
                //            }
                //        }
                //    }
                //    else if (m_iScope == 1)
                //        rbDocument.Checked = true;
                //    else if (m_iScope == 0)
                //        rbSection.Checked = true;
                //}
                else if (iDefTrailerID != 0)
                    lstTypes.SelectedValue = iDefTrailerID;
                else if (oSettings.DefaultTrailerID == mpSkipTrailerSegmentID && m_iAssignedObjectType == mpObjectTypes.Trailer)
                    lstTypes.SelectedValue = mpNoTrailerPromptSegmentID;
                else if (m_iAssignedObjectType == mpObjectTypes.Trailer)
                    lstTypes.SelectedValue = oSettings.DefaultTrailerID.ToString();                    
            }
            catch { }
            finally
            {
                if (lstTypes.SelectedIndex == -1)
                    lstTypes.SelectedIndex = 0;
            }


        }
        /// <summary>
        /// displays editor controls belonging to the current segment
        /// </summary>
        /// <param name="oNode"></param>
        private void DisplayControls()
        {
            SaveCurrentControlValues();
            if (m_iCurSegmentID < 0)
            {
                //Clear controls for "No Trailer" item
                this.pnlControls.Controls.Clear();

                if (m_iCurSegmentID == mpUpdateExistingTrailersSegmentID)
                {
                    this.gbApplyTo.Enabled = false;
                }
                else
                {
                    this.gbApplyTo.Enabled = true;

                    //GLOG 6155 (dm) - none/remove should display same current
                    //section text as footer
                    if (m_bSectionHasLinkedStories)
                    {
                        //GLOG #8492
                        rbSection.Text = m_xSectionLabel.TrimEnd(')') + " and linked sections)";
                        //rbSection.Top = rbDocument.Top + 17;
                    }
                    else
                    {
                        rbSection.Text = m_xSectionLabel;
                        //rbSection.Top = rbDocument.Top + 25;
                    }
                }

                // GLOG : 2370 : JAB
                // Hide the options window since the controls have been removed.
                //this.HideOptionsWindow();
                this.btnOptions.Enabled = false;                
            }
            else
            {
                AdminSegmentDefs oSegs = new AdminSegmentDefs();
                AdminSegmentDef oSegDef = (AdminSegmentDef)oSegs.ItemFromID(m_iCurSegmentID);

                //GLOG 3095: Disable apply to options for Insert at Selection types
                Segment.InsertionLocations iLocation = (Segment.InsertionLocations)oSegDef.DefaultDoubleClickLocation;
                if (iLocation == Segment.InsertionLocations.InsertAtSelection || iLocation == Segment.InsertionLocations.Default)
                {
                    //GLOG : 6501 : CEH
                    //ensure section is selected for these locations
                    this.rbSection.Checked = true;
                    this.gbApplyTo.Enabled = false;
                }
                else
                {
                    this.gbApplyTo.Enabled = true;
                    switch (iLocation)
                    {
                        //Types that apply to entire document by default
                        case Segment.InsertionLocations.InsertAtEndOfAllSections:
                        case Segment.InsertionLocations.InsertAtEndOfDocument:
                        case Segment.InsertionLocations.InsertAtStartOfAllSections:
                        case Segment.InsertionLocations.InsertAtStartOfDocument:
                            this.rbDocument.Checked = true;
                            break;
                        default:
                            //Default to apply to section for anything else
                            this.rbSection.Checked = true;
                            break;
                    }
                    //GLOG 4243: Determine if selected type includes stories that are linked in current document
                    //GLOG 6152 and 6189 (dm) - added module-level variable
                    m_bContentContainsLinkedStories = LMP.String.ContentContainsLinkedStories(
                        oSegDef.XML, m_bLinkedPrimaryFooter, m_bLinkedPrimaryHeader,
                        m_bLinkedFirstFooter, m_bLinkedFirstHeader, m_bLinkedEvenFooter,
                        m_bLinkedEvenHeader);
                    
                    //GLOG #8492 - dcf
                    if (m_bSectionHasLinkedStories && m_bContentContainsLinkedStories)
                    {
                        rbSection.Text = m_xSectionLabel.TrimEnd(')') +" and linked sections)";
                    }
                    else
                    {
                        rbSection.Text = m_xSectionLabel;
                    }
                }
                //extract variable definitions from xml and put in array
                if (LMP.String.IsWordOpenXML(oSegDef.XML))
                    //GLOG 4876: get variables from Content Control tags and document variables
                    m_oVariables = this.LoadControlDefsCC(oSegDef);
                else
                    m_oVariables = this.LoadControlDefs(oSegDef);

                //sort preferences by execution index
                m_oVariables.Sort(new VariableSorter());

                if (m_oVariables.Count > 0)
                {
                    this.SetupControls();
                    this.SetControlValues();
                    if (this.m_iAssignedObjectType == mpObjectTypes.Trailer)
                        SetDateAsFieldEnabledState();
                }
                else
                    //Clear existing controls
                    this.pnlControls.Controls.Clear();

                // If the window is not in the small view and we have no
                // options to show disable the options button.
                if (this.m_bIsSmallWindow)
                {
                    this.btnOptions.Enabled = (m_oVariables.Count > 0);
                }
                else
                {
                    this.btnOptions.Enabled = true;
                }

                //// GLOG : 2370 : JAB
                //// Automatically hide the options window or show the associated options 
                //// for a selected draft stamp item.
                //if (m_oVariables.Count > 0)
                //{
                //    this.ShowOptions();
                //}
                //else
                //{
                //    this.HideOptionsWindow();
                //}
            }
        }
        /// <summary>
        /// Updates Hashtable with current values of option controls
        /// </summary>
        private void SaveCurrentControlValues()
        {
            foreach (Control oCtl in pnlControls.Controls)
            {
                if (oCtl is IControl)
                {
                    if (!m_oControlsHash.ContainsKey(oCtl.Name))
                        m_oControlsHash.Add(oCtl.Name, null);
                    else
                        m_oControlsHash[oCtl.Name] = ((IControl)oCtl).Value;
                }
            }
        }
        /// <summary>
        /// Sets default values for controls
        /// </summary>
        private void SetControlValues()
        {
            for (int i = 0; i < m_oVariables.Count; i++)
            {
                string[] aDefs = (string[])m_oVariables[i];
                string xCtl = "ctl" + aDefs[0];
                IControl oCtl = this.pnlControls.Controls[xCtl] as IControl;
                string xValue = null;
                //GLOG 2957:  Use last value set in Dialog if available
                if (m_oControlsHash.ContainsKey(xCtl) && m_oControlsHash[xCtl] != null)
                {
                    xValue = m_oControlsHash[xCtl].ToString();
                }
                else
                {
                    if (m_oExistingStamp != null)
                    {
                        try
                        {
                            //Use value from existing document stamp if like-named variable exists
                            Variable oVar = m_oExistingStamp.GetVariable(aDefs[0]);
                            if (oVar != null)
                                xValue = oVar.Value;
                        }
                        catch { }
                    }
                    else if (m_oPrefill != null) //JTS 6/24/11
                    {
                        try
                        {
                            //Use value from existing document stamp if like-named variable exists
                            for (int p = 0; p < m_oPrefill.Count; p++)
                            {
                                if (m_oPrefill.ItemName(p).ToUpper() == aDefs[0].ToUpper())
                                {
                                    xValue = m_oPrefill[p];
                                    break;
                                }
                            }
                        }
                        catch { }
                    }
                    if (xValue == null)
                    {
                        string xDefaultValue = aDefs[5];
                        int iPos = xDefaultValue.IndexOf("__") + 2;
                        if (iPos == 1)
                            //Old-format fieldcode
                            iPos = xDefaultValue.IndexOf('_') + 1;
                        int iPos2 = xDefaultValue.IndexOf(']');
                        if (iPos > 0 && iPos2 > -1)
                        {
                            //Get appropriate keyset if default uses a preference
                            mpPreferenceSetTypes iType = 0;
                            string xScope = "";
                            if (xDefaultValue.ToUpper().Contains("AUTHORPREFERENCE") ||
                                xDefaultValue.ToUpper().Contains("AUTHORPARENTPREFERENCE"))
                            {
                                iType = mpPreferenceSetTypes.Author;
                                xScope = m_iCurSegmentID.ToString();
                            }
                            else if (xDefaultValue.ToUpper().Contains("TYPEPREFERENCE"))
                            {
                                iType = mpPreferenceSetTypes.UserType;
                                xScope = ((int)m_iAssignedObjectType).ToString();
                            }
                            else if (xDefaultValue.ToUpper().Contains("OBJECTPREFERENCE"))
                            {
                                iType = mpPreferenceSetTypes.UserObject;
                                xScope = m_iCurSegmentID.ToString();
                            }
                            else if (xDefaultValue.ToUpper().Contains("APPLICATIONPREFERENCE"))
                            {
                                iType = mpPreferenceSetTypes.UserApp;
                                xScope = "";
                            }
                            if (iType > 0)
                            {
                                string xPrefName = xDefaultValue.Substring(iPos, iPos2 - iPos);
                                try
                                {
                                    xValue = this.GetKeySetValue(iType, xScope, xPrefName);
                                }
                                catch
                                {
                                    xValue = "";
                                }
                            }
                        }
                        if (xValue == null && iPos > 0)
                        {
                            //uses a non-preference fieldcode
                            try
                            {
                                xValue = FieldCode.GetValue(aDefs[5], null, this.m_oMPDocument);
                            }
                            catch
                            {
                                xValue = "";
                            }

                        }
                        if (xValue == null)
                            xValue = aDefs[5];
                    }
                }
                try
                {
                    oCtl.Value = xValue;
                }
                catch { }

                //if value is first item in dropdown list or date combo,
                //text will be selected - deselect now
                if (oCtl is LMP.Controls.ComboBox)
                    ((LMP.Controls.ComboBox)oCtl).SelectionLength = 0;
                else if (oCtl is LMP.Controls.DateCombo)
                    ((LMP.Controls.DateCombo)oCtl).SelectionLength = 0;
                //GLOG 2957: Update HashTable with current control values
                if (!m_oControlsHash.ContainsKey(xCtl))
                    m_oControlsHash.Add(xCtl, oCtl.Value);
            }
        }
        /// <summary>
        /// returns specified keyset value
        /// </summary>
        /// <param name="oObjectTypeDef"></param>
        /// <param name="xPropertyName"></param>
        /// <returns></returns>
        private string GetKeySetValue(mpPreferenceSetTypes iPrefType, string xScope, 
            string xPropertyName)
        {
            string xEntityID = LocalPersons.GetFormattedID(LMP.Data.Application.User.ID);
            //retrieve preferences - return empty string if no keyset
            KeySet oPrefs = null;
            try
            {
                oPrefs = KeySet.GetPreferenceSet(iPrefType, xScope, xEntityID);
            }
            catch { }

            if (oPrefs == null)
                return "";

            string xValue = null;
            try { xValue = oPrefs.GetValue(xPropertyName); }
            catch { }

            if (xValue != null)
                return xValue;
            else
                return "";
        }
        /// <summary>
        /// extracts variable definitions for the current segment from
        /// the specified xml and loads them into an array list
        /// </summary>
        /// <param name="xXML"></param>
        private ArrayList LoadControlDefs(AdminSegmentDef oSegment)
        {
            ArrayList oVariables = new ArrayList();
            //create xml document for xpath searchablity
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml(oSegment.XML);
            XmlNamespaceManager oNSM = new XmlNamespaceManager(oXML.NameTable);
            string xNSPrefix = String.GetNamespacePrefix(oSegment.XML, ForteConstants.MacPacNamespace); 
            oNSM.AddNamespace(xNSPrefix,ForteConstants.MacPacNamespace);

            //get object data of target segment
            string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                oSegment.ID.ToString() + "|')]";

            string xXPath = @"//" + xNSPrefix + ":mSEG" + xSegCondition;
            XmlNode oSegNode = oXML.SelectSingleNode(xXPath, oNSM);
            XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
            string xObjectData = oObjectData.Value;

            //get all mSEGS so that we can retrieve
            //deleted author preference variables
            XmlNodeList oNodes = oXML.SelectNodes(xXPath, oNSM);

            foreach (XmlNode oSegmentNode in oNodes)
            {
                XmlAttribute oDeletionScopes = oSegmentNode.Attributes["DeletedScopes"];
                if (oDeletionScopes != null)
                    xObjectData += oDeletionScopes.Value;
            }

            //append object data of mVars belonging to target segment
            xXPath = @"//" + xNSPrefix + ":mVar[ancestor::" + xNSPrefix + ":mSEG[1]" + xSegCondition + ']';
            XmlNodeList oVarNodes = oXML.SelectNodes(xXPath, oNSM);
            foreach (XmlNode oVarNode in oVarNodes)
            {
                oObjectData = oVarNode.Attributes["ObjectData"];
                xObjectData += oObjectData.Value;
            }

            //get variable definitions containing author pref codes
            string xPattern = @"VariableDefinition=.*?\|";
            Regex oRegex = new Regex(xPattern, RegexOptions.Singleline);
            MatchCollection oMatches = oRegex.Matches(xObjectData);
            if (oMatches.Count > 0)
            {
                string xDefs = "";
                foreach (Match oMatch in oMatches)
                {
                    string xDef = oMatch.Value;
                    if (xDefs.IndexOf(xDef) == -1)
                    {
                        //parse definition to get only the props we need
                        string[] aDef = xDef.Split('�');

                        //ensure that configured to display in document editor
                        Variable.ControlHosts iDisplayIn =
                            (Variable.ControlHosts)Int32.Parse(aDef[20]);
                        if ((iDisplayIn & Variable.ControlHosts.DocumentEditor) !=
                            Variable.ControlHosts.DocumentEditor)
                            continue;

                        string[] aProps = new string[7];

                        //get variable name
                        aProps[0] = aDef[1];

                        //get display name
                        aProps[1] = aDef[2];

                        //get control type
                        aProps[2] = aDef[9];

                        //get control properties
                        aProps[3] = aDef[10];

                        //get execution index
                        aProps[4] = aDef[4];

                        //get default value
                        aProps[5] = aDef[5];

                        //Control actions string
                        aProps[6] = aDef[15];
                        
                        //add to array list
                        oVariables.Add(aProps);

                        //avoid duplicates
                        xDefs += xDef;
                    }
                }
            }
            return oVariables;
        }

        /// <summary>
        /// extracts variable definitions for the current segment from
        /// the specified xml and loads them into an array list
        /// </summary>
        /// <param name="xXML"></param>
        private ArrayList LoadControlDefsCC(AdminSegmentDef oSegment)
        {
            //Format Segment ID as it appears in Content Control tag
            string xCCSegmentID = oSegment.ID.ToString();
            if (xCCSegmentID.EndsWith(".0"))
                xCCSegmentID = xCCSegmentID.Substring(0, xCCSegmentID.Length - 2);
            xCCSegmentID = xCCSegmentID.Replace('.', 'd');
            xCCSegmentID = xCCSegmentID.Replace('-', 'm');
            xCCSegmentID = xCCSegmentID.PadLeft(19, '0');

            ArrayList oVariables = new ArrayList();
            //create xml document for xpath searchablity
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml(oSegment.XML);
            XmlNamespaceManager oNSM = new XmlNamespaceManager(oXML.NameTable);
            oNSM.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);

            //xPath to match Content Controls that contain specified Segment ID as part of Tag
            string xSegCondition = "[w:sdtPr/w:tag[contains(@w:val, '" + xCCSegmentID + "')]]";
            string xXPath = @"descendant::w:sdt" + xSegCondition;
            XmlNode oSegNode = oXML.SelectSingleNode(xXPath, oNSM);
            if (oSegNode == null)
                return oVariables;
            //Parse Document Variable ID from Tag
            XmlAttribute oTagID = oSegNode.SelectSingleNode("w:sdtPr/w:tag", oNSM).Attributes["w:val"];
            string xSegVarID = oTagID.Value.Substring(3, 8);
            XmlNode oDocVarNode = oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", oNSM);
            XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
            //Document Variable contains ObjectData for mSEG
            string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
            //TagID at start of Doc Variable value
            string xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));

            //append object data of mVars belonging to target segment
            xXPath = @"//w:sdt[ancestor::w:sdt[1]" + xSegCondition + " and w:sdtPr/w:tag[contains(@w:val,'mpv')]]";
            XmlNodeList oVarNodes = oXML.SelectNodes(xXPath, oNSM);
            foreach (XmlNode oVarNode in oVarNodes)
            {
                //Check tags of child content controls
                oTagID = oVarNode.SelectSingleNode("w:sdtPr/w:tag", oNSM).Attributes["w:val"];
                if (oTagID.Value.StartsWith("mpv"))
                {
                    string xVarID = oTagID.Value.Substring(3, 8);
                    oDocVarNode = oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xVarID + "')]", oNSM);
                    oVal = oDocVarNode.Attributes["w:val"];
                    //Document Variable contains ObjectData for mVar
                    string xVarData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                    xObjectData += xVarData;
                }
            }

            int iDeleted = 1;
            string xDeletedTags = "";
            //Append all mpd Variables for this Segment to get DeletedTags
            do
            {
                string xDeleted = iDeleted++.ToString().PadLeft(2, '0');
                xXPath = @"//w:docVar[contains(@w:name,'mpd" + xSegVarID + xDeleted + "')]";
                oDocVarNode = oXML.SelectSingleNode(xXPath, oNSM);
                if (oDocVarNode != null)
                {
                    xDeletedTags += oDocVarNode.Attributes["w:val"].Value;
                }
            } while (oDocVarNode != null);
            if (xDeletedTags != "")
            {
                int iPos = xDeletedTags.IndexOf("w:tag w:val=\"mpv");
                while (iPos > -1)
                {
                    //ID of associated Doc Variable is in DeletedTag XML
                    string xVarID = xDeletedTags.Substring(iPos + "w:tag w:val=\"mpv".Length, 8);
                    oDocVarNode = oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xVarID + "')]", oNSM);
                    oVal = oDocVarNode.Attributes["w:val"];
                    //Document Variable contains ObjectData for mVar
                    string xVarData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                    xObjectData += xVarData;
                    iPos = xDeletedTags.IndexOf("w:tag w:val=\"mpv", iPos + 1);
                }
            }

            //get variable definitions containing author pref codes
            string xPattern = @"VariableDefinition=.*?\|";
            Regex oRegex = new Regex(xPattern, RegexOptions.Singleline);
            MatchCollection oMatches = oRegex.Matches(xObjectData);
            if (oMatches.Count > 0)
            {
                string xDefs = "";
                foreach (Match oMatch in oMatches)
                {
                    string xDef = oMatch.Value;
                    if (xDefs.IndexOf(xDef) == -1)
                    {
                        //parse definition to get only the props we need
                        string[] aDef = xDef.Split('�');

                        //ensure that configured to display in document editor
                        Variable.ControlHosts iDisplayIn =
                            (Variable.ControlHosts)Int32.Parse(aDef[20]);
                        if ((iDisplayIn & Variable.ControlHosts.DocumentEditor) !=
                            Variable.ControlHosts.DocumentEditor)
                            continue;

                        string[] aProps = new string[7];

                        //get variable name
                        aProps[0] = aDef[1];

                        //get display name
                        aProps[1] = aDef[2];

                        //get control type
                        aProps[2] = aDef[9];

                        //get control properties
                        aProps[3] = aDef[10];

                        //get execution index
                        aProps[4] = aDef[4];

                        //get default value
                        aProps[5] = aDef[5];

                        //Control actions string
                        aProps[6] = aDef[15];

                        //add to array list
                        oVariables.Add(aProps);

                        //avoid duplicates
                        xDefs += xDef;
                    }
                }
            }
            return oVariables;
        }
        private static int GetDefaultTrailerIDForTemplate(Word.Document oDoc)
        {
            //GLOG 8033: check for template-specific assignments
            Word.Template oTemplate = (Word.Template)oDoc.get_AttachedTemplate();
            if (oTemplate != null)
            {
                string xTemplate = oTemplate.Name;
                FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                List<FirmApplicationSettings.TemplateTrailerOptions> oOptions = oSettings.TemplateTrailerDefaults;
                for (int i = 0; i < oOptions.Count; i++)
                {
                    string xPattern = "^" + Regex.Escape(oOptions[i].TemplateName).Replace("\\*", ".*").Replace("\\?", ".") + "$";
                    if (Regex.IsMatch(xTemplate, xPattern, RegexOptions.IgnoreCase))
                    {
                        return oOptions[i].DefaultTrailerID;
                    }
                }
            }
            return 0;
        }
        private static int[] GetTrailerAssignmentsForTemplate(Word.Document oDoc)
        {
            //GLOG 8033: check for template-specific assignments
            Word.Template oTemplate = (Word.Template)oDoc.get_AttachedTemplate();
            if (oTemplate != null)
            {
                string xTemplate = oTemplate.Name;
                FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                List<FirmApplicationSettings.TemplateTrailerOptions> oOptions = oSettings.TemplateTrailerDefaults;
                for (int i = 0; i < oOptions.Count; i++)
                {
                    string xPattern = "^" + Regex.Escape(oOptions[i].TemplateName).Replace("\\*", ".*").Replace("\\?", ".") + "$";
                    if (Regex.IsMatch(xTemplate, xPattern, RegexOptions.IgnoreCase))
                    {
                        string[] aStrIDs = oOptions[i].TrailerIDs.Split(',');
                        List<int> oIntIDs = new List<int>();
                        for (int s = 0; i < aStrIDs.GetLength(0); s++)
                        {
                            int iTest = 0;
                            if (Int32.TryParse(aStrIDs[s], out iTest))
                                oIntIDs.Add(iTest);    
                        }
                        return oIntIDs.ToArray();
                    }
                }
            }
            return new int[] {};
        }
        private static int GetDefaultTrailerIDForSegmentDef(AdminSegmentDef oSegment)
        {
            if (LMP.String.IsWordOpenXML(oSegment.XML))
                return GetDefaultTrailerIDForSegmentDef_CC(oSegment);
            else
                return GetDefaultTrailerIDForSegmentDef_XML(oSegment);
        }
        private static int GetDefaultTrailerIDForSegmentDef_CC(AdminSegmentDef oSegment)
        {
            //Format Segment ID as it appears in Content Control tag
            int iTrailerType = 0;
            try
            {
                string xCCSegmentID = oSegment.ID.ToString();
                if (xCCSegmentID.EndsWith(".0"))
                    xCCSegmentID = xCCSegmentID.Substring(0, xCCSegmentID.Length - 2);
                xCCSegmentID = xCCSegmentID.Replace('.', 'd');
                xCCSegmentID = xCCSegmentID.Replace('-', 'm');
                xCCSegmentID = xCCSegmentID.PadLeft(19, '0');

                //create xml document for xpath searchablity
                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(oSegment.XML);
                XmlNamespaceManager oNSM = new XmlNamespaceManager(oXML.NameTable);
                oNSM.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);

                //xPath to match Content Controls that contain specified Segment ID as part of Tag
                string xSegCondition = "[w:sdtPr/w:tag[contains(@w:val, '" + xCCSegmentID + "')]]";
                string xXPath = @"descendant::w:sdt" + xSegCondition;
                XmlNode oSegNode = oXML.SelectSingleNode(xXPath, oNSM);
                if (oSegNode == null)
                    return 0;
                //Parse Document Variable ID from Tag
                XmlAttribute oTagID = oSegNode.SelectSingleNode("w:sdtPr/w:tag", oNSM).Attributes["w:val"];
                string xSegVarID = oTagID.Value.Substring(3, 8);
                XmlNode oDocVarNode = oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", oNSM);
                XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
                //Document Variable contains ObjectData for mSEG
                string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                if (xObjectData.Contains("|DefaultTrailerID="))
                {
                    string xTrailerID = xObjectData.Substring(xObjectData.IndexOf("|DefaultTrailerID=") + @"|DefaultTrailerID=".Length);
                    try
                    {
                        xTrailerID = xTrailerID.Substring(0, xTrailerID.IndexOf("|"));
                    }
                    catch { }
                    if (!Int32.TryParse(xTrailerID, out iTrailerType))
                        return 0;
                }
            }
            catch { }
            return iTrailerType;
        }
        private static int GetDefaultTrailerIDForSegmentDef_XML(AdminSegmentDef oSegment)
        {
            int iTrailerType = 0;
            try
            {
                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(oSegment.XML);
                XmlNamespaceManager oNSM = new XmlNamespaceManager(oXML.NameTable);
                string xNSPrefix = String.GetNamespacePrefix(oSegment.XML, ForteConstants.MacPacNamespace);
                oNSM.AddNamespace(xNSPrefix, ForteConstants.MacPacNamespace);

                //get object data of target segment
                string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                    oSegment.ID.ToString() + "|')]";

                string xXPath = @"//" + xNSPrefix + ":mSEG" + xSegCondition;
                XmlNode oSegNode = oXML.SelectSingleNode(xXPath, oNSM);
                XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
                string xObjectData = oObjectData.Value;
                if (xObjectData.Contains("|DefaultTrailerID="))
                {
                    string xTrailerID = xObjectData.Substring(xObjectData.IndexOf("|DefaultTrailerID=") + @"|DefaultTrailerID=".Length);
                    try
                    {
                        xTrailerID = xTrailerID.Substring(0, xTrailerID.IndexOf("|"));
                    }
                    catch { }
                    if (!Int32.TryParse(xTrailerID, out iTrailerType))
                        return 0;
                }
            }
            catch { }
            return iTrailerType;
        }
        /// <summary>
        /// Builds Prefill string based on current control values
        /// </summary>
        /// <returns></returns>
        private string GetPrefillValues()
        {
            string xTemp = "";
            for (int i = 0; i < m_oVariables.Count; i++)
            {
                string[] aDef = (string[])m_oVariables[i];
                string xCtl = "ctl" + aDef[0];
                IControl oCtl = this.pnlControls.Controls[xCtl] as IControl;
                if (xTemp != "")
                    xTemp = xTemp + Prefill.mpPrefillFieldDelimiter;
                xTemp = xTemp + aDef[0] + Prefill.mpPrefillValueDelimiter + oCtl.Value;
            }
            return xTemp;
        }
        /// <summary>
        /// adds controls for the current document stamp segment
        /// </summary>
        private void SetupControls()
        {
            this.pnlControls.SuspendLayout();
            this.pnlControls.Visible = false;
            //Clear existing controls
            this.pnlControls.Controls.Clear();

            //GLOG 5395:  Panel AutoScroll has been turned so avoid limit on number of controls
            int iTop = 10;
            int iNextTabIndex = 4;
            for (int i = 0; i < m_oVariables.Count; i++)
            {
                int iControlLeft = 8;
                //add label
                string[] aDef = (string[])m_oVariables[i];
                //add value control
                Control oControl = this.SetupControl(aDef);
                oControl.Name = "ctl" + aDef[0];

                //add hotkey designator -
                //correlate to variable index to make unique
                string xLabel = aDef[1].Insert(i, "&");

                if (oControl is LMP.Controls.CheckBox)
                {
                    iControlLeft = 12;
                    oControl.Width = this.pnlControls.Width - 10;
                    oControl.BackColor = Color.Transparent;
                    oControl.Text = xLabel;
                }
                else
                {
                    Label oLabel = new Label();
                    oLabel.Name = "lbl" + aDef[0];

                    oLabel.Text = xLabel + ':';
                    oLabel.TabIndex = iNextTabIndex++;
                    oLabel.AutoSize = true;
                    //oLabel.Width = this.pnlControls.Width - 155;
                    oLabel.TextAlign = ContentAlignment.TopLeft;
                    oLabel.BackColor = Color.Transparent;
                    oLabel.Height = 26;
                    this.pnlControls.Controls.Add(oLabel);
                    oLabel.Left = 4;
                    oLabel.Top = iTop + 1;

                    //GLOG #8492 - dcf
                    iControlLeft = oLabel.Left + oLabel.Width + 5;
                    oControl.Width = (this.pnlControls.Left + this.pnlControls.Width) - iControlLeft - 5;
                }

                this.pnlControls.Controls.Add(oControl);
                oControl.Left = iControlLeft;
                oControl.TabIndex = iNextTabIndex++;
                oControl.Top = iTop - 2;
                oControl.Anchor = ((AnchorStyles)((AnchorStyles.Left |
                    AnchorStyles.Top | AnchorStyles.Right)));
                //if value is first item in dropdown list or date combo,
                //text will be selected - deselect now
                if (oControl is LMP.Controls.ComboBox)
                    ((LMP.Controls.ComboBox)oControl).SelectionLength = 0;
                else if (oControl is LMP.Controls.DateCombo)
                    ((LMP.Controls.DateCombo)oControl).SelectionLength = 0;
                //set top for next control
                iTop += (oControl.Height + 6);
            }
            this.pnlControls.Visible = true;
            this.pnlControls.ResumeLayout();
        }
        /// <summary>
        /// returns a new control as specified
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xProperties"></param>
        /// <returns></returns>
        private Control SetupControl(string[] aDef)
        {
            try
            {
                Control oCtl = null;

                //create new control
                LMP.Data.mpControlTypes iType =
                    (mpControlTypes)System.Convert.ToByte(aDef[2]);
                switch (iType)
                {
                    case mpControlTypes.Textbox:
                        oCtl = new LMP.Controls.TextBox();
                        break;
                    case mpControlTypes.MultilineTextbox:
                        oCtl = new LMP.Controls.MultilineTextBox();
                        break;
                    case mpControlTypes.Combo:
                        oCtl = new LMP.Controls.ComboBox();
                        ((LMP.Controls.ComboBox)oCtl).LimitToList = false;
                        break;
                    case mpControlTypes.DropdownList:
                        oCtl = new LMP.Controls.ComboBox();
                        ((LMP.Controls.ComboBox)oCtl).LimitToList = true;
                        break;
                    case mpControlTypes.LocationsDropdown:
                        oCtl = new LMP.Controls.LocationsDropdown();
                        break;
                    case mpControlTypes.Checkbox:
                        oCtl = new LMP.Controls.CheckBox();
                        break;
                    case mpControlTypes.AuthorSelector:
                        oCtl = new LMP.Controls.AuthorSelector();
                        break;
                    case mpControlTypes.List:
                        oCtl = new LMP.Controls.ListBox();
                        break;
                    case mpControlTypes.MultilineCombo:
                        oCtl = new LMP.Controls.MultilineCombo(this);
                        break;
                    case mpControlTypes.DetailGrid:
                        oCtl = new LMP.Controls.Detail();
                        break;
                    case mpControlTypes.RelineGrid:
                        oCtl = new LMP.Controls.Reline();
                        break;
                    case mpControlTypes.SalutationCombo:
                        oCtl = new LMP.Controls.SalutationCombo();
                        break;
                    case mpControlTypes.DetailList:
                        oCtl = new LMP.Controls.DetailList();
                        break;
                    case mpControlTypes.ClientMatterSelector:
                        //oCtl = new LMP.Controls.ClientMatterSelector();
                        //break;
                    case mpControlTypes.SegmentChooser:
                        oCtl = new LMP.Controls.Chooser();

                        //set target object type id and target object id
                        ((Chooser)oCtl).TargetObjectID = this.m_iCurSegmentID;
                        ((Chooser)oCtl).TargetObjectType = this.m_iAssignedObjectType;

                        break;
                    case mpControlTypes.DateCombo:
                        oCtl = new LMP.Controls.DateCombo();
                        break;
                    case mpControlTypes.Spinner:
                        oCtl = new LMP.Controls.Spinner();
                        break;
                    case mpControlTypes.FontList:
                        oCtl = new LMP.Controls.FontList();
                        break;
                    default:
                        break;
                }

                try
                {
                    //set up control properties
                    string xProperties = aDef[3];
                    string[] aProps = null;
                    if (xProperties != "")
                        aProps = xProperties.Split(StringArray.mpEndOfSubValue);
                    else
                        aProps = new string[0];

                    //cycle through property name/value pairs, executing each
                    for (int i = 0; i < aProps.Length; i++)
                    {
                        int iPos = aProps[i].IndexOf("=");
                        string xName = aProps[i].Substring(0, iPos);
                        string xValue = aProps[i].Substring(iPos + 1);

                        System.Type oCtlType = oCtl.GetType();

                        PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                        if (oPropInfo != null)
                        {
                            System.Type oPropType = oPropInfo.PropertyType;

                            object oValue = null;

                            if (oPropType.IsEnum)
                                oValue = Enum.Parse(oPropType, xValue);
                            else
                                oValue = Convert.ChangeType(xValue, oPropType);

                            oPropInfo.SetValue(oCtl, oValue, null);
                        }
                    }
                    //do any control-specific setup of control
                    IControl oIControl = (IControl)oCtl;
                    oIControl.ValueChanged += new ValueChangedHandler(Control_ValueChanged);
                    oCtl.LostFocus += new EventHandler(Control_LostFocus);
                    oCtl.GotFocus += new EventHandler(Control_GotFocus);
                    oIControl.ExecuteFinalSetup();

                    //initialize controls where necessary
                    if (iType == mpControlTypes.DropdownList)
                        ((LMP.Controls.ComboBox)oCtl).SelectedIndex = 0;
                    else if (iType == mpControlTypes.List)
                        ((LMP.Controls.ListBox)oCtl).SelectedIndex = 0;
                    oIControl.IsDirty = false;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Data.mpControlTypes iType =
                    (mpControlTypes)System.Convert.ToByte(aDef[2]);
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    iType.ToString(), oE);
            }
        }

        private void SetWordEcho(bool bOn)
        {
            Word.Application oWord = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
            if (bOn)
            {
                LMP.WinAPI.LockWindowUpdate(0);
                oWord.ScreenUpdating = true;
            }
            else
            {
                //freeze Word
                LMP.WinAPI.LockWindowUpdate(LMP.WinAPI.FindWindow("OpusApp", 0));
                oWord.ScreenUpdating = false;
            }
        }
        void Control_GotFocus(object sender, EventArgs e)
        {
            try
            {
                //TODO: Support Control Actions for GotFocus event
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            try
            {
                //TODO: Support Control Actions for LostFocus event
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Handle ValueChanged event of iControl
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Control_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //TODO:  This is a temporary kludge to duplicate 9.x behavior
                //We will need some way to handle defined ControlActions (and VariableActions?)
                //outside of an existing Segment
                IControl oICtl = (IControl)sender;
                Control oCtl = (Control)oICtl;
                if (oCtl.Name == "ctlIncludeDate" || oCtl.Name == "ctlIncludeTime")
                {
                    SetDateAsFieldEnabledState();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        void SetDateAsFieldEnabledState()
        {
            Control oCtl = null;
            try
            {
                oCtl = this.pnlControls.Controls["ctlInsertDateAsField"];
            }
            catch { }
            if (oCtl != null)
            {
                bool bEnable = false;
                IControl oDateCtl = null;
                IControl oTimeCtl = null;
                try
                {
                    oDateCtl = this.pnlControls.Controls["ctlIncludeDate"] as IControl;
                }
                catch {}
                try
                {
                    oTimeCtl = this.pnlControls.Controls["ctlIncludeTime"] as IControl;
                }
                catch {}
                if (oDateCtl != null)
                {
                    switch (oDateCtl.Value.ToUpper())
                    {
                        case "1":
                        case "TRUE":
                            bEnable = true;
                            break;
                        case "0":
                        case "FALSE":
                            bEnable = bEnable || false;
                            break;
                    }
                }
                if (oTimeCtl != null)
                {
                    switch (oTimeCtl.Value.ToUpper())
                    {
                        case "1":
                        case "TRUE":
                            bEnable = true;
                            break;
                        case "0":
                        case "FALSE":
                            bEnable = bEnable || false;
                            break;
                    }
                }
                oCtl.Enabled = bEnable;
            }
        }
        /// <summary>
        /// return value of NoTrailerPrompt variable
        /// </summary>
        /// <returns></returns>
        internal static string GetNoTrailerVariable()
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            object oVarID = mpNoTrailerVariable;
            string xDocID = "";
            try
            {
                xDocID = oDoc.Variables.get_Item(ref oVarID).Value;
            }
            catch { }
            return xDocID;
        }
        /// <summary>
        /// Get Basic Doc ID of Current document
        /// </summary>
        /// <returns></returns>
        internal static string GetCurrentDocID()
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            string xDocID = "";
            if (fDMS.DMS.IsProfiled)
                //JTS 2/20/12: Include Library in Doc ID
                xDocID = fDMS.DMS.GetProfileValue("Library") + "." + 
                    fDMS.DMS.GetProfileValue("DocNumber") + "." + 
                    fDMS.DMS.GetProfileValue("Version");
            else
                xDocID = oDoc.FullName;
            return xDocID;
        }
        /// <summary>
        /// Create Doc Variable to mark for no trailer prompt
        /// </summary>
        internal static void SetNoTrailerVariable()
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            object oVarID = mpNoTrailerVariable;
            oDoc.Variables.get_Item(ref oVarID).Value = GetCurrentDocID();
            //GLOG 4553: Also clear converted 9.x trailer variable if necessary
            ClearConverted9xTrailerVariable();
            ClearLastTrailerVariable(); //JTS 6/24/11
        }
        /// <summary>
        /// Create Document Variable containing Prefill settings for last-inserted Trailer
        /// using current doc id
        /// </summary>
        /// <param name="xPrefill"></param>
        /// <param name="xSections"></param>
        /// <param name="iTrailerID"></param>
        internal static void SetLastTrailerVariable(string xPrefill, string xSections, int iTrailerID)  //JTS 6/24/11
        {
            string xDocID = GetCurrentDocID();
            SetLastTrailerVariable(xPrefill, xDocID, xSections, iTrailerID);
        }

        /// <summary>
        /// Create Document Variable containing Prefill settings for last-inserted Trailer (for re-use)
        /// </summary>
        /// <param name="iSegmentID"></param>
        internal static void SetLastTrailerVariable(string xPrefill, string xDocID,
            string xSections, int iTrailerID)  //JTS 6/24/11
        {
            //GLOG 6060 (dm): added overload to allow old doc id to be specified
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            //JTS 6/28/11:  Append Doc ID to value before encrypting
            xPrefill = xPrefill + mpPrefillVariableSeparator + xDocID;
            xPrefill = xPrefill + mpPrefillVariableSeparator + xSections;
            xPrefill = LMP.Forte.MSWord.Application.Encrypt(xPrefill, LMP.Data.Application.EncryptionPassword);

            //2-29-12 (dm) - always set generic last trailer variable, even when
            //also setting type-specific one - this will continue to be used
            //InsertTrailerIfNecessary()  for doc id comparison - checking
            //each individual trailer would be too performance-intensive
            object oVarID = mpLastTrailerVariable;
            oDoc.Variables.get_Item(ref oVarID).Value = xPrefill;
            if (iTrailerID > 0)
            {
                oVarID = mpLastTrailerVariable + "_" + iTrailerID.ToString();
                oDoc.Variables.get_Item(ref oVarID).Value = xPrefill;
            }

            //GLOG 4553: Also clear converted 9.x trailer variable if necessary
            ClearConverted9xTrailerVariable();
        }
        /// <summary>
        /// return value of Variable containing Prefill used for last Trailer inserted
        /// </summary>
        /// <returns></returns>
        internal static string GetLastTrailerVariable()
        {
            return GetLastTrailerVariable(0);
        }
        internal static string GetLastTrailerVariable(int iTrailerID)  //JTS 6/24/11
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            object oVarID = mpLastTrailerVariable;
            if (iTrailerID > 0)
                oVarID = mpLastTrailerVariable + "_" + iTrailerID.ToString();
            string xValue = "";
            try
            {
                xValue = oDoc.Variables.get_Item(ref oVarID).Value;
                //Decrypt string
                xValue = LMP.String.Decrypt(xValue);
                //If string is still decrypted (belonging to different client) return empty string
                if (LMP.String.IsEncrypted(xValue))
                    xValue = "";
            }
            catch
            {
                //Try without specific Trailer ID
                if (iTrailerID > 0)
                    return GetLastTrailerVariable(0);
            }
            return xValue;
        }
        /// <summary>
        /// Delete variable containing Prefill used for last Trailer inserted
        /// </summary>
        internal static void ClearLastTrailerVariable()
        {
            ClearLastTrailerVariable(0);
        }
        internal static void ClearLastTrailerVariable(int iTrailerID) //JTS 6/24/11
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            object oVarID = mpLastTrailerVariable;
            if (iTrailerID > 0)
                oVarID = mpLastTrailerVariable + "_" + iTrailerID.ToString();
            try
            {
                oDoc.Variables.get_Item(ref oVarID).Delete();
            }
            catch { }
            if (iTrailerID == 0)
            {
                for (int i = oDoc.Variables.Count; i > 0; i--)
                {
                    object oIndex = i;
                    if (oDoc.Variables.get_Item(ref oIndex).Name.StartsWith(mpLastTrailerVariable))
                    {
                        oDoc.Variables.get_Item(ref oIndex).Delete();
                    }
                }
            }
        }
        /// <summary>
        /// Return Prefill object based on doc var created by last trailer insertion
        /// </summary>
        /// <returns></returns>
        internal static Prefill LastTrailerPrefill()
        {
            return LastTrailerPrefill(0);
        }
        internal static Prefill LastTrailerPrefill(int iTrailerID) //JTS 6/24/11
        {
            string xPrefillVar = DocumentStampUI.GetLastTrailerVariable(iTrailerID);
            if (xPrefillVar != "")
            {
                try
                {
                    string[] aPrefill = xPrefillVar.Split(mpPrefillVariableSeparator);
                    string xID = aPrefill[0];
                    string xName = aPrefill[1];
                    string xValues = aPrefill[2];
                    return new Prefill(xValues, xName, xID);
                }
                catch
                {
                    return null;
                }
            }
            else
                return null;
        }
        /// <summary>
        /// Create Doc Variable to mark as containing converted 9.x trailer
        /// </summary>
        internal static void SetConverted9xTrailerVariable() //GLOG 4553
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            object oVarID = mpConverted9xTrailerVariable;
            oDoc.Variables.get_Item(ref oVarID).Value = GetCurrentDocID();
        }
        /// <summary>
        /// return value of converted 9.x trailer variable
        /// </summary>
        /// <returns></returns>
        internal static string GetConverted9xTrailerVariable() //GLOG 4553
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            object oVarID = mpConverted9xTrailerVariable;
            string xDocID = "";
            try
            {
                xDocID = oDoc.Variables.get_Item(ref oVarID).Value;
            }
            catch { }
            return xDocID;
        }
        /// <summary>
        /// Delete variable marking as containing converted 9.x trailer
        /// </summary>
        internal static void ClearConverted9xTrailerVariable() //GLOG 4553
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            object oVarID = mpConverted9xTrailerVariable;
            try
            {
                oDoc.Variables.get_Item(ref oVarID).Delete();
            }
            catch { }
        }
        /// <summary>
        /// Delete variable marking for no trailer prompt
        /// </summary>
        internal static void ClearNoTrailerVariable()
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            object oVarID = mpNoTrailerVariable;
            try
            {
                oDoc.Variables.get_Item(ref oVarID).Delete();
            }
            catch { }
        }

        /// <summary>
        /// returns TRUE if the last section contains an "end of document"
        /// trailer and this is the only trailer in the document
        /// </summary>
        /// <returns></returns>
        private bool SingleTrailerAtEndOfDocument()
        {
            //false if no trailers
            if (m_aExistingTrailers == null)
                return false;

            //false if no trailer in last section
            object oZero = 0;
            int iCount = m_aExistingTrailers.GetLength(0);
            object oTrailer = m_aExistingTrailers[iCount - 1];
            if ((oTrailer == null) || (oTrailer == oZero))
                return false;

            //false if there's a trailer in a previous section
            for (int i = 0; i < iCount - 1; i++)
            {
                if (m_aExistingTrailers[i] != null)
                    return false;
            }

            //check last section type
            AdminSegmentDef oDef = null;
            int iSegmentID = Int32.Parse(oTrailer.ToString());
            AdminSegmentDefs oDefs = new AdminSegmentDefs(m_iAssignedObjectType);
            try
            {
                oDef = (AdminSegmentDef)oDefs.ItemFromID(iSegmentID);
            }
            catch { }
            if (oDef == null)
                return false;
            else
            {
                return ((Segment.InsertionLocations)oDef.DefaultDoubleClickLocation ==
                    Segment.InsertionLocations.InsertAtEndOfDocument);
            }
        }

        private ArrayList AddLinkedSectionsWithUnlinkedTrailers(ArrayList aSections)
        {
            bool bFirstFooter = false;
            bool bFirstHeader = false;
            bool bPrimaryFooter = false;
            bool bPrimaryHeader = false;
            bool bEvenFooter = false;
            bool bEvenHeader = false;

            //for performance reasons, this is limited to post-enhancement trailers -
            //we might change this if we decide against unconditionally removing all
            //legacy trailers with the first post-enhancement insertion
            if (m_aExistingTrailers == null)
                return aSections;

            string xSections = "";
            object oZero = 0;
            for (int i = 0; i < aSections.Count; i++)
            {
                //add existing section to new array
                int iIndex = (System.Int32)aSections[i];
                if (xSections.IndexOf('|' + iIndex.ToString() + '|') == -1)
                    xSections += '|' + iIndex.ToString() + '|';

                //cycle through previous linked sections
                if (iIndex > 1)
                {
                    LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(
                        m_oMPDocument.WordDocument.Sections[iIndex], ref bPrimaryFooter,
                        ref bPrimaryHeader, ref bFirstFooter, ref bFirstHeader,
                        ref bEvenFooter, ref bEvenHeader, true, true);
                    while ((bPrimaryFooter || bFirstFooter || bEvenFooter) && (iIndex > 1))
                    {
                        iIndex--;
                        if ((xSections.IndexOf('|' + iIndex.ToString() + '|') == -1) &&
                            (m_aExistingTrailers[iIndex - 1] != null) &&
                            (m_aExistingTrailers[iIndex - 1] != oZero) &&
                            (m_aExistingTrailers[iIndex - 1].ToString() != "Linked"))
                        {
                            xSections += '|' + iIndex.ToString() + '|';
                            LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(
                                m_oMPDocument.WordDocument.Sections[iIndex], ref bPrimaryFooter,
                                ref bPrimaryHeader, ref bFirstFooter, ref bFirstHeader,
                                ref bEvenFooter, ref bEvenHeader, true, true);
                        }
                    }
                }

                //cycle through subsequent linked sections
                iIndex = (System.Int32)aSections[i];
                while (iIndex < m_aExistingTrailers.Length)
                {
                    iIndex++;
                    if ((xSections.IndexOf('|' + iIndex.ToString() + '|') == -1) &&
                        (m_aExistingTrailers[iIndex - 1] != null) &&
                        (m_aExistingTrailers[iIndex - 1] != oZero) &&
                        (m_aExistingTrailers[iIndex - 1].ToString() != "Linked"))
                    {
                        LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(
                            m_oMPDocument.WordDocument.Sections[iIndex], ref bPrimaryFooter,
                            ref bPrimaryHeader, ref bFirstFooter, ref bFirstHeader,
                            ref bEvenFooter, ref bEvenHeader, true, true);
                        if (bPrimaryFooter || bFirstFooter || bEvenFooter)
                            xSections += '|' + iIndex.ToString() + '|';
                        else
                            break;
                    }
                }
            }

            //convert to array list
            ArrayList aSectionsNew = new ArrayList();
            xSections = xSections.TrimStart('|');
            xSections = xSections.TrimEnd('|');
            xSections = xSections.Replace("||", "|");
            string[] xSectionList = xSections.Split('|');
            foreach (string xItem in xSectionList)
                aSectionsNew.Add(System.Int32.Parse(xItem));
            return aSectionsNew;
        }

        /// <summary>
        /// repoints the target sections array to the linkage source sections
        /// </summary>
        /// <param name="aSections"></param>
        private void AdjustLinkedSectionTargets(ArrayList aSections)
        {
            bool bFirstFooter = false;
            bool bFirstHeader = false;
            bool bPrimaryFooter = false;
            bool bPrimaryHeader = false;
            bool bEvenFooter = false;
            bool bEvenHeader = false;
            string xSections = "";

            for (int i = aSections.Count - 1; i >= 0; i--)
            {
                int iIndex = (System.Int32)aSections[i];
                if (xSections.IndexOf('|' + iIndex.ToString() + '|') == -1)
                {
                    if (iIndex == 1)
                    {
                        //add to tracking variable
                        xSections += "|1|";
                    }
                    else
                    {
                        //cycle back to first section not linked to previous
                        //GLOG 6230 - ignore first footer if different first page
                        LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(
                            m_oMPDocument.WordDocument.Sections[iIndex], ref bPrimaryFooter,
                            ref bPrimaryHeader, ref bFirstFooter, ref bFirstHeader,
                            ref bEvenFooter, ref bEvenHeader, true, true);
                        while ((bPrimaryFooter || bFirstFooter || bEvenFooter) && (iIndex > 1) &&
                            (xSections.IndexOf('|' + iIndex.ToString() + '|') == -1))
                        {
                            iIndex--;
                            if ((iIndex > 1) && (xSections.IndexOf('|' + iIndex.ToString() + '|') == -1))
                            {
                                LMP.Forte.MSWord.WordDoc.SectionHasLinkedHeadersFooters(
                                    m_oMPDocument.WordDocument.Sections[iIndex], ref bPrimaryFooter,
                                    ref bPrimaryHeader, ref bFirstFooter, ref bFirstHeader,
                                    ref bEvenFooter, ref bEvenHeader, true, true);
                            }
                        }

                        if (xSections.IndexOf('|' + iIndex.ToString() + '|') == -1)
                        {
                            //adjust section and add tracking variable
                            aSections[i] = iIndex;
                            xSections += '|' + iIndex.ToString() + '|';
                        }
                        else
                        {
                            //already targeted - remove from array
                            aSections.RemoveAt(i);
                        }
                    }
                }
                else
                {
                    //already targeted - remove from array
                    aSections.RemoveAt(i);
                }
            }
        }
        #endregion

        private class VariableSorter : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                string[] aPref1 = (string[])x;
                string[] aPref2 = (string[])y;

                int iIndex1 = int.Parse(aPref1[4]);
                int iIndex2 = int.Parse(aPref2[4]);

                if (iIndex1 == iIndex2)
                    return 0;
                else if (iIndex1 > iIndex2)
                    return 1;
                else
                    return -1;
            }

            #endregion
        }

        Point m_obtnOKLocation;
        Point m_obtnCancelLocation;

        int m_iGroupBoxWidth;
        int m_iFormWidth;

        bool m_bIsSmallWindow = false;
        int m_iMargin;

        Point m_obtnOKLocationSmall;
        Point m_obtnCancelLocationSmall;

        int m_iGroupBoxWidthSmall;
        int m_iFormWidthSmall;
        int m_iButtonSpacing;

        private void DocumentStampUI_Load(object sender, EventArgs e)
        {
            // Store the locations and widths of this form's components
            // so that they may be used to restore the form's view after having
            // been minimized.
            this.m_obtnOKLocation = this.btnOK.Location;
            this.m_obtnCancelLocation = this.btnCancel.Location;

            this.m_iGroupBoxWidth = this.gbApplyTo.Width;
            this.m_iFormWidth = this.Width;

            this.m_iMargin = this.gbApplyTo.Location.X;
            this.m_iButtonSpacing = this.btnCancel.Location.X - (this.btnOK.Location.X + this.btnOK.Width);

            // These are the small window component locations and widths.
            //GLOG : 7632 : ceh
            this.m_iFormWidthSmall = m_iMargin +
                                        this.btnOptions.Width +
                                        this.m_iButtonSpacing +
                                        this.btnOK.Width +
                                        this.m_iButtonSpacing +
                                        this.btnCancel.Width +
                                        this.m_iButtonSpacing +
                                        m_iMargin + 150;

            //GLOG : 7632 : ceh
            if (LMP.Forte.MSWord.WordApp.Version > 14)
                this.m_iGroupBoxWidthSmall = this.m_iFormWidthSmall - (2 * m_iMargin) - 17;
            else
                this.m_iGroupBoxWidthSmall = this.m_iFormWidthSmall - (2 * m_iMargin) - 6;

            this.m_obtnOKLocationSmall = new Point(this.btnOptions.Location.X + this.btnOptions.Width + this.m_iButtonSpacing, this.btnOK.Location.Y);
            this.m_obtnCancelLocationSmall = new Point(this.m_obtnOKLocationSmall.X + btnOK.Width + this.m_iButtonSpacing, this.btnCancel.Location.Y);

            this.btnOptions.Visible = (this.m_iMode == Mode.DocumentStamp);

            // If this form is for document stamp, start out with the small view.
            this.m_bIsSmallWindow = (this.m_iMode == Mode.DocumentStamp);

            UpdateView();
        }

        private void UpdateView()
        {
            // There is no need to update the view if we have not initialized the control 
            // dimensions. We'll use m_iGroupBoxWidthSmall as an indicator of the control 
            // dimensions having been initialized. If m_iGroupBoxWidthSmall has a non-zero 
            // value, the control dimensions have been initialized.
            if (m_iGroupBoxWidthSmall != 0)
            {
                this.Width = m_bIsSmallWindow ? m_iFormWidthSmall : m_iFormWidth;
                this.gbApplyTo.Width = m_bIsSmallWindow ? m_iGroupBoxWidthSmall : m_iGroupBoxWidth;
                //this.gbOptions.Width = this.gbApplyTo.Width;
                this.lstTypes.Width = this.gbApplyTo.Width;

                this.gbOptions.Visible = !m_bIsSmallWindow;

                this.btnOptions.Text = m_bIsSmallWindow ? 
                    LMP.Resources.GetLangString("btnOptionsSmall") : LMP.Resources.GetLangString("btnOptions");

                // Disable the options button if there are no controls associated with 
                // the selected type and the window is not expanded.
                this.btnOptions.Enabled = (this.pnlControls.Controls.Count > 0);
            }
        }

        private void btnOptions_Click(object sender, EventArgs e)
        {
            // Toggle the window size;
            this.m_bIsSmallWindow = !this.m_bIsSmallWindow;
            this.UpdateView();
        }
        
        /// <summary>
        /// GLOG : 2370 : JAB
        /// Show the associated options for a selected draft stamp item.
        /// </summary>
        private void ShowOptions()
        {
            this.m_bIsSmallWindow = false;
            this.UpdateView();
        }
        
        /// <summary>
        /// GLOG : 2370 : JAB
        /// Hide the associated options window for a selected draft stamp item.
        /// </summary>
        private void HideOptionsWindow()
        {
            this.m_bIsSmallWindow = true;
            this.UpdateView();
        }

        /// <summary>
        /// GLOG : 6154 : CEH
        /// Validates txtMultiple control contents.
        /// </summary>
        /// <param name="bCheckForEmpty"></param>
        /// <returns></returns>
        private bool ValidateMultipleSectionsControl(bool bCheckForEmpty)
        {
            //GLOG 6370 (dm) - changed return value from void to bool
            int iSections = Session.CurrentWordApp.ActiveDocument.Sections.Count;

            //GLOG 6107 (dm) - strip spaces
            string xMultiple = this.txtMultiple.Text.Replace(" ", "");

            //GLOG 6154 (dm) - prompt if no section specified
            if (xMultiple == "" && bCheckForEmpty == true)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_NoSectionSpecified"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                this.txtMultiple.SelectAll();
                this.txtMultiple.Focus();
                return false;
            }

            string[] xSectionList = xMultiple.Split(',');
            foreach (string xItem in xSectionList)
            {
                if (xItem.Contains("-"))
                {
                    int iPos = xItem.IndexOf("-");
                    string xStart = xItem.Substring(0, iPos);
                    if (iPos < xItem.Length)
                    {
                        string xEnd = xItem.Substring(iPos + 1);
                        if (String.IsNumericInt32(xStart) && String.IsNumericInt32(xEnd))
                        {
                            for (int i = Int32.Parse(xStart); i <= Int32.Parse(xEnd); i++)
                            {
                                //GLOG 6078 (dm) - don't accept "0"
                                if ((i > iSections) || (xStart == "0"))
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString(
                                        "Prompt_InvalidSectionSpecified") + i.ToString(),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    this.txtMultiple.SelectAll();
                                    this.txtMultiple.Focus();
                                    return false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (String.IsNumericInt32(xItem))
                    {
                        //GLOG 6078 (dm) - don't accept "0"
                        if ((Int32.Parse(xItem) > iSections) || (xItem == "0"))
                        {
                            MessageBox.Show(LMP.Resources.GetLangString(
                                "Prompt_InvalidSectionSpecified") + xItem,
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            this.txtMultiple.SelectAll();
                            this.txtMultiple.Focus();
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private void rbMultiple_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtMultiple.Enabled = this.rbMultiple.Checked;
                if (this.rbMultiple.Checked == true)
                {
                    this.txtMultiple.Focus();
                }
            }
            catch { }
        }

        private void txtMultiple_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Ignore all input except numeric, '-' or ','
            //GLOG 6107 (dm) - allow spaces
            if (!((e.KeyChar >= 48 && e.KeyChar <= 57) || (char)e.KeyChar == '-' ||
                (char)e.KeyChar == ',' || (char)e.KeyChar == (char)Keys.Back ||
                e.KeyChar == 32))
            {
                e.Handled = true;
            }
        }
       //GLOG : 6154 : CEH
        private void btnOK_Enter(object sender, EventArgs e)
        {
            if (this.rbMultiple.Checked)
                ValidateMultipleSectionsControl(true);
        }

        //GLOG 6370 (dm) - remmed out - don't validate until the user clicks OK
        //private void txtMultiple_Leave(object sender, EventArgs e)
        //{
        //    if (btnOK.Focused == false && 
        //    txtMultiple.Focused== false && 
        //    btnCancel.Focused == false)
        //        ValidateMultipleSectionsControl(false);
        //}
   }
}