﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMP.Data;
using System.Xml;
using System.Text.RegularExpressions;

namespace LMP.MacPac
{
    public partial class OrphanedDocVarsForm : Form
    {
        //GLOG : 8554 : ceh
        #region *****************fields*************************
        //GLOG 8213
        bool m_bSearchCancelled = false;
        #endregion
        #region *****************constructor*******************
        public OrphanedDocVarsForm()
        {
            InitializeComponent();
        }
        #endregion
        #region *****************events*********************
        private void OrphanedDocVarsForm_Load(object sender, EventArgs e)
        {
            this.btnCopy.Enabled = false;
            this.btnClean.Enabled = false;
            this.btnCancelSearch.Visible = false;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (!btnSearch.Visible)
                return;

            try
            {
                m_bSearchCancelled = false;
                btnSearch.Visible = false;
                btnCancelSearch.Visible = true;
                btnClean.Enabled = false;
                btnCopy.Enabled = false;
                btnClose.Enabled = false;
                grdResults.Rows.Clear();
                System.Windows.Forms.Application.DoEvents();
                tsLabel.Text = "";
                int iSegmentCount = 0;
                int iOrphansTotal = 0;
                int iSegVars = 0;
                int iVarVars = 0;
                int iDelVars = 0;
                int iSubVars = 0;
                int iBlockVars = 0;
                int iEncryptedVars = 0;
                int iPropsVars = 0;
                int iOtherVars = 0;
                int iMaxOrphans = 0;
                string xMaxSegment = "";
                //Load all Segments
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                for (int i = 1; i <= oDefs.Count; i++)
                {
                    if (m_bSearchCancelled)
                    {
                        break;
                    }
                    bool bHasOrphans = false;
                    int iOrphanCount = 0;
                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromIndex(i);
                    tsLabel.Text = "Examining " + oDef.DisplayName + "...";
                    string xXML = oDef.XML;
                    XmlDocument oXDoc = new XmlDocument();
                    try
                    {
                        oXDoc.LoadXml(xXML);
                    }
                    catch
                    {
                        continue;
                    }
                    if (oXDoc == null || oXDoc.ChildNodes.Count == 0)
                    {
                        continue;
                    }
                    XmlNamespaceManager oNSMan = new XmlNamespaceManager(oXDoc.NameTable);
                    oNSMan.AddNamespace("w", LMP.Data.ForteConstants.WordOpenXMLNamespace);

                    XmlNode oDocVarsNode = oXDoc.SelectSingleNode("//w:docVars", oNSMan);
                    if (oDocVarsNode == null)
                    {
                        continue;
                    }
                    string xDeletedTags = "";
                    //Get DeletedScopes XML from mpd variables
                    XmlNodeList oDeletedNodes = oDocVarsNode.SelectNodes("w:docVar[starts-with(@w:name, 'mpd')][string-length(@w:name)=13][substring(@w:name, 12)='01']", oNSMan);
                    foreach (XmlNode oDelNode in oDeletedNodes)
                    {
                        //Append all variable values to single string - text being searched may be split between two doc variables
                        xDeletedTags += oDelNode.Attributes["w:val"].Value;
                        string xDelTag = oDelNode.Attributes["w:name"].Value.Substring(0, 11);
                        int iNextIndex = 2;
                        XmlNode oNextDelNode = null;
                        do
                        {
                            oNextDelNode = oDocVarsNode.SelectSingleNode("w:docVar[contains(@w:name, '" + xDelTag + iNextIndex++.ToString().PadLeft(2, '0') + "')]", oNSMan);
                            if (oNextDelNode != null)
                            {
                                xDeletedTags += oNextDelNode.Attributes["w:val"].Value;
                            }
                        } while (oNextDelNode != null);
                    }

                    foreach (XmlNode oXNode in oDocVarsNode.ChildNodes)
                    {
                        string xName = "";
                        string xValue = "";
                        try
                        {
                            xValue = oXNode.Attributes["w:val"].Value;
                        }
                        catch { }
                        try
                        {
                            xName = oXNode.Attributes["w:name"].Value;
                        }
                        catch { }
                        if (xName.StartsWith("mpo"))
                        {
                            string xTag = xName.Substring(3);
                            if (Regex.IsMatch(xDeletedTags, "mp[svbudpc]" + xTag + "000"))
                                //If Tag is part of deleted scopes, this variable is not an orphan
                                continue;

                            string xXPath = "";
                            if (String.IsWordOpenXML(xXML))
                            {
                                //WordOpenXML - look for matches in Tag attribute of Content Controls
                                xXPath = @"descendant::w:sdt[w:sdtPr/w:tag[substring(@w:val, 4, 8)='" + xTag + "']]";
                            }
                            else
                            {
                                //WordML - look for matches in Reserved attribute of XML Tags
                                xXPath = @"descendant[substring(@Reserved, 4, 8)='" + xTag + "']";
                            }
                            XmlNode oMatch = oXDoc.SelectSingleNode(xXPath, oNSMan);
                            if (oMatch == null)
                            {
                                bHasOrphans = true;
                                iOrphanCount++;
                                if (xValue.Contains("ÌÍObjectData=SegmentID="))
                                    iSegVars++;
                                else if (xValue.Contains("ÌÍObjectData=VariableDefinition="))
                                    iVarVars++;
                                else if (xValue.Contains("ÌÍObjectData=Index="))
                                    iSubVars++;
                                else if (xValue.Contains("ÌÍReserved=mpb"))
                                    iBlockVars++;
                                else if (xValue.Contains("ÌÍObjectData="))
                                    iDelVars++;
                                else if (xValue.StartsWith("ÌÍ"))
                                    iPropsVars++;
                                else if (xValue.Contains("ÌÍ^`~#mp!@") | xValue.Contains("ÌÍ+¦"))
                                    iEncryptedVars++;
                                else
                                    iOtherVars++;

                                //break;
                            }
                        }
                    }
                    if (bHasOrphans)
                    {
                        iSegmentCount++;
                        iOrphansTotal = iOrphansTotal + iOrphanCount;
                        if (iOrphanCount > iMaxOrphans)
                        {
                            iMaxOrphans = iOrphanCount;
                            xMaxSegment = oDef.ID + " - " + oDef.DisplayName + " (" + oDef.TypeID + ")";
                        }
                        grdResults.Rows.Add(oDef.ID, oDef.DisplayName, oDef.TypeID, iOrphanCount);
                    }
                    System.Windows.Forms.Application.DoEvents();
                }
                grdResults.ClearSelection();
                tsLabel.Text = iSegmentCount + " Segments; " + iOrphansTotal + " Variables";
                sstStatus.ShowItemToolTips = true;
                tsLabel.ToolTipText = "(" + iSegVars + " mSEG; " + iVarVars + " mVar; " + iSubVars + " mSubVar; "
                        + iDelVars + " mDel; " + iBlockVars + " mBlock; " + iPropsVars + " mSec/DocProps; " + iEncryptedVars + " Encrypted; " + iOtherVars + " Other)";
                try
                {
                    Clipboard.SetText(tsLabel.Text + "\r" + tsLabel.ToolTipText + "\r" + xMaxSegment + ": " + iMaxOrphans + " orphans");
                }
                catch { }
                btnCopy.Enabled = iSegmentCount > 0;
                btnClean.Enabled = iSegmentCount > 0;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                btnClean.Enabled = grdResults.Rows.Count > 0;
                btnCopy.Enabled = grdResults.Rows.Count > 0;
                btnClose.Enabled = true;
                btnCancelSearch.Visible = false;
                btnSearch.Visible = true;
                m_bSearchCancelled = false;
            }

        }


        private void btnCopy_Click(object sender, EventArgs e)
        {
            grdResults.SelectAll();
            Clipboard.SetDataObject(grdResults.GetClipboardContent());
            grdResults.ClearSelection();
        }

        private void btnCancelSearch_Click(object sender, EventArgs e)
        {
            if (btnCancelSearch.Visible)
                m_bSearchCancelled = true;
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            try
            {
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                DataGridViewSelectedRowCollection oRows = grdResults.SelectedRows;
                int iCount = 0;
                int iError = 0;
                if (oRows.Count == 0)
                {
                    grdResults.SelectAll();
                    oRows = grdResults.SelectedRows;
                }
                grdResults.ClearSelection();
                foreach (DataGridViewRow oRow in oRows)
                {
                    string xID = oRow.Cells[0].Value.ToString();
                    try
                    {
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(xID));
                        tsLabel.Text = "Cleaning " + oDef.DisplayName + "...";
                        string xXml = oDef.XML;
                        xXml = String.DeleteOrphanedDocVarsInXml(xXml);
                        oDef.XML = xXml;
                        oDefs.Save(oDef);
                        grdResults.Rows.Remove(oRow);
                        iCount++;
                    }
                    catch
                    {
                        oRow.DefaultCellStyle.BackColor = Color.Red;
                        iError++;
                    }
                    System.Windows.Forms.Application.DoEvents();
                }
                tsLabel.Text = "Cleaned " + iCount.ToString() + " Segment" + (iCount > 1 ? "s." : ".");
                if (iError > 0)
                {
                    tsLabel.Text += "  Failed to clean " + iError.ToString() + " Segments due to errors.";
                }
            }
            catch (System.Exception oE)
            { }
        }

        private void grdResults_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (grdResults.SelectedRows.Count > 0)
                {
                    btnClean.Text = "C&lean Selected";
                }
                else
                {
                    btnClean.Text = "C&lean All";
                }
            }
            catch { }
        }
        #endregion
    }
}
