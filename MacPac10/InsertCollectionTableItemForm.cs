using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac
{
    internal partial class InsertCollectionTableItemForm : Form
    {
        #region *********************fields*********************
        private bool m_bCancel = true;
        private object[,] m_aLocations;
        #endregion
        #region ****************constructors********************
        public InsertCollectionTableItemForm(int iTargetObjectID, mpObjectTypes oTargetObjectType,
            mpObjectTypes oAssignedObjectType, int iDefaultAssignedObjectID, string xCaption,
            Word.XMLNode oCollectionTag)
        {
            InitializeComponent();
            Setup(iTargetObjectID, oTargetObjectType, oAssignedObjectType,
                iDefaultAssignedObjectID, xCaption, oCollectionTag.Range.Tables[1]);
        }
        public InsertCollectionTableItemForm(int iTargetObjectID, mpObjectTypes oTargetObjectType,
            mpObjectTypes oAssignedObjectType, int iDefaultAssignedObjectID, string xCaption,
            Word.ContentControl oCollectionContentControl)
        {
            InitializeComponent();
            Setup(iTargetObjectID, oTargetObjectType, oAssignedObjectType,
                iDefaultAssignedObjectID, xCaption, oCollectionContentControl.Range.Tables[1]);
        }
        #endregion
        #region *********************properties*********************
        public int SegmentID
        {
            get { return Int32.Parse(this.chooser1.Value); }
        }
        public int ItemLocation
        {
            get { return Int32.Parse(this.cmbLocation.Value); }
        }
        public bool Cancelled
        {
            get { return m_bCancel; }
        }
        #endregion
        #region *********************methods*********************
        private void Setup(int iTargetObjectID, mpObjectTypes oTargetObjectType,
            mpObjectTypes oAssignedObjectType, int iDefaultAssignedObjectID,
            string xCaption, Word.Table oCollectionTable)
        {
            //load available locations list
            LMP.Forte.MSWord.Pleading oCOM = new LMP.Forte.MSWord.Pleading();
            string xProps = oCOM.GetOpenPositionsInTable(oCollectionTable,
                (System.Int32)oAssignedObjectType);
            string[] oProps = xProps.Split('|');
            if (oProps[1] != "")
            {
                string[] oPositions = oProps[1].Split('�');
                int iUpperBound = oPositions.Length;
                m_aLocations = new object[iUpperBound + 1, 2];
                for (int i = 0; i < iUpperBound; i++)
                {
                    int iPosition = System.Int32.Parse(oPositions[i]);
                    if (iPosition % 2 == 1)
                    {
                        //left
                        int iRow = (iPosition + 1)/2;
                        m_aLocations[i, 0] = "Row " + iRow.ToString() + ", left side";
                    }
                    else
                    {
                        //right
                        int iRow = iPosition / 2;
                        m_aLocations[i, 0] = "Row " + iRow.ToString() + ", right side";
                    }
                    m_aLocations[i, 1] = oPositions[i];
                }
                m_aLocations[iUpperBound, 0] = "New row at end of table";
                m_aLocations[iUpperBound, 1] = "0";
            }
            else
                m_aLocations = new object[,]{{"New row at end of table",0}};

            cmbLocation.SetList(m_aLocations);

            //hide locations dropdown if no choices
            bool bVisible = (m_aLocations.GetUpperBound(0) > 0);
            if (!bVisible)
            {
                cmbLocation.Visible = false;
                lblLocation.Visible = false;
                btnOK.Top = btnOK.Top - 49;
                btnCancel.Top = btnCancel.Top - 49;
                this.Height = this.Height - 49;
            }

            //set chooser properties
            this.chooser1.TargetObjectID = iTargetObjectID;
            this.chooser1.TargetObjectType = oTargetObjectType;
            this.chooser1.AssignedObjectType = oAssignedObjectType;
            this.chooser1.ExecuteFinalSetup();
            if (iDefaultAssignedObjectID > 0)
                this.chooser1.Value = iDefaultAssignedObjectID.ToString();

            //set form caption
            this.Text = xCaption;
        }
        #endregion
        #region *********************events*********************
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                m_bCancel = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.Close();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                m_bCancel = false;
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chooser1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //enable location dropdown only if selected segment allows
                //side-by-side insertion
                if (m_aLocations.GetUpperBound(0) > 0)
                {
                    string xSegmentID = this.chooser1.Value;
                    if (xSegmentID != "")
                    {
                        int iSegmentID = System.Int32.Parse(xSegmentID);
                        AdminSegmentDefs oSegs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = (AdminSegmentDef)oSegs.ItemFromID(iSegmentID);
                        bool bAllowSideBySide = (LMP.Architect.Api.Segment.GetPropertyValueFromXML(
                            oDef.XML, "AllowSideBySide").ToUpper() == "TRUE");
                        this.cmbLocation.Enabled = bAllowSideBySide;
                        this.lblLocation.Enabled = bAllowSideBySide;
                        if (!bAllowSideBySide)
                            this.cmbLocation.Value = "0";
                        else if (this.cmbLocation.Value == "0")
                            this.cmbLocation.SelectedIndex = 0;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
    }
}