﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace LMP.MacPac
{
    partial class AboutForm : Form
    {
        private System.Windows.Forms.ContextMenu mnuContext;
        private System.Windows.Forms.MenuItem mnuContext_Copy;

        public AboutForm()
        {
            InitializeComponent();
            this.lblVersion.Text = System.String.Format("Version {0}", AssemblyVersion);
            this.lblCopyright.Text = AssemblyCopyright;
        }

        #region Assembly Attribute Accessors

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblVersion_MouseClick(object sender, MouseEventArgs e)
        {
            //show copy menu if right click
            if (e.Button == MouseButtons.Right)
            {
                Point oPoint = new Point(e.X, e.Y);
                this.mnuContext.Show(lblVersion, oPoint);
                return;
            }
        }

        /// <summary>
        /// Copy version number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        // GLOG : 5055 : CEH
        private void pctCopy_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.lblVersion.Text, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void pctCopy_MouseDown(object sender, MouseEventArgs e)
        {
            //show borders
            pctCopy.BorderStyle = BorderStyle.FixedSingle;
        }

        private void pctCopy_MouseUp(object sender, MouseEventArgs e)
        {
            //show borders
            pctCopy.BorderStyle = BorderStyle.None;
        }


        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Pen oP = new Pen(Brushes.LightGray, 2);
            Rectangle oR = this.pnlBorder.ClientRectangle;
            oR.Inflate(-1, -1);
            e.Graphics.DrawRectangle(oP, oR);
        }

        private void btnSessionInfo_Click(object sender, EventArgs e)
        {
            SessionInfoForm oForm = new SessionInfoForm();
            oForm.ShowDialog();

            //string xDetail = "User ID: " + LMP.MacPac.Session.CurrentUser.SystemUserID + 
            //    "\r\nMode: " + (LMP.Data.Application.AdminMode ? "Administrator" : "User") + 
            //    "\r\nDatabase Directory:\r\n" + LMP.Data.Application.DBDirectory;
            //MessageBox.Show(xDetail, LMP.ComponentProperties.ProductName, 
            //    MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            this.lblProductName.Text = LMP.ComponentProperties.ProductName;
        }
    }
}
