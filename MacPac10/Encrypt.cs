using System;
using System.Collections.Generic;
using System.Text;

namespace LMP.MacPac
{
    internal class Encrypt
    {
        public DateTime UnEncryptDate(string xCode)
        {
            
            string xCodeMod = "";
            string xDate = "";
            string x = "";

            try
            {

                //GLOG 8895: Require 2nd character to be numeric
                if (!String.IsNumericByte(xCode.Substring(1, 1)))
                    return DateTime.MinValue;

                //remove garbage 
                xCode = xCode.Substring(2);

                //collect chars at even indexes
                for (int i = 2; i <= xCode.Length; i += 2)
                    xCodeMod = xCodeMod + xCode.Substring(i - 1, 1);

                while (xCodeMod.Length > 0)
                {
                    x = xCodeMod.Substring(0, 1);
                    if (!LMP.String.IsNumericInt32(x))
                    {
                        if (IsReserved(x))
                        {
                            Byte[] bBytes = System.Text.Encoding.ASCII.GetBytes(x);
                            int iAsciiCode = (int)bBytes[0];
                            int iNum = iAsciiCode - 100;
                            x = iNum.ToString();
                        }
                        else
                        {
                            //non-reserved letter, so get mapping for lower case
                            x = MapLetter(x.ToLower());
                        }
                    }

                    xDate = xDate + x;

                    //trim left char
                    xCodeMod = xCodeMod.Substring(1);

                }

                int iYear = Int32.Parse(xDate.Substring(2, 4));
                int iMonth = Int32.Parse(xDate.Substring(6, 2));
                int iDay = Int32.Parse(xDate.Substring(0, 2));

                DateTime d = new DateTime(iYear, iMonth, iDay);
                //GLOG 8895: Disallow dates past 2099
                if (d.Year > 2099)
                    return DateTime.MinValue;
                else
                    return d;
            }
            
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.LicenseException(
                    LMP.Resources.GetLangString("Error_CouldNotReadLicenseKey"), oE);
            }
        }
        private Boolean IsReserved(string xLetter)
        {
            switch (xLetter)
            {
                case "d":
                case "e":
                case "f":
                case "g":
                case "h":
                case "i":
                case "j":
                case "k":
                case "l":
                case "m":
                    return true;
                    break;
                default:
                    return false;
                    break;
            }
        }
        private string MapLetter(string xLetter)
        {
            string xDigit;

            switch (xLetter)
            {
                case "q":
                    xDigit = "9";
                    break;
                case "r":
                    xDigit = "8";
                    break;
                case "s":
                    xDigit = "7";
                    break;
                case "t":
                    xDigit = "6";
                    break;
                case "u":
                    xDigit = "5";
                    break;
                case "v":
                    xDigit = "4";
                    break;
                case "w":
                    xDigit = "3";
                    break;
                case "x":
                    xDigit = "2";
                    break;
                case "y":
                    xDigit = "1";
                    break;
                case "z":
                    xDigit = "0";
                    break;
                default:
                    xDigit = xLetter;
                    break;
            }
            return xDigit;
        }
    }
}
