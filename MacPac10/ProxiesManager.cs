using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    public partial class ProxiesManager : LMP.Controls.ManagerBase
    {
        DataTable m_oNetworkUsers;

        public ProxiesManager()
        {
            InitializeComponent();

        }
        /// <summary>
        /// Assign selected User as a proxy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddProxy_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstPeople.SelectedIndex > -1)
                {
                    int iPersonID = Int32.Parse(this.lstPeople.SelectedValue.ToString());
                    if (iPersonID > 0)
                    {
                        //GLOG 8190:  Don't allow adding self as Proxy
                        if (iPersonID == Session.CurrentUser.PrimaryID)
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Prompt_CannotAddSelfAsProxy"),
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        LocalPersons oPersons = Session.CurrentUser.GetAuthors();
                        // If selected ID is not in local People table, copy record from Network
                        if (!oPersons.IsInCollection(iPersonID))
                        {
                            try
                            {
                                oPersons.CopyNetworkUser(iPersonID);
                            }
                            catch (System.Exception oE)
                            {
                                LMP.Error.Show(oE);
                            }
                        }
                        try
                        {
                            //Assign proxy rights
                            Session.CurrentUser.GiveProxyRights(iPersonID);
                            RefreshProxyList();
                        }
                        catch (LMP.Exceptions.StoredProcedureException)
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_ProxyAlreadyAssigned") + iPersonID,
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Populate listbox with current Proxies
        /// </summary>
        private void RefreshProxyList()
        {
            LocalPersons oProxies = Session.CurrentUser.GetProxies();
            DataTable oProxiesDT = oProxies.ToDataSet(false).Tables[0];
            lstCurrentProxies.DataSource = oProxiesDT;
            lstCurrentProxies.DisplayMember = "DisplayName";
            lstCurrentProxies.ValueMember = "ID1";
            lstCurrentProxies.Refresh();
        }
        /// <summary>
        /// Remove proxy assignment for selected user(s)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteProxy_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstCurrentProxies.SelectedItems.Count == 0)
                    return;

                ListBox.SelectedObjectCollection oCol = this.lstCurrentProxies.SelectedItems;
                for (int i = 0; i < oCol.Count; i++)
                {
                    DataRowView oRow = (DataRowView)oCol[i];

                    int iProxyID = Int32.Parse(oRow["ID1"].ToString());
                    if (iProxyID > 0)
                    {
                        try
                        {
                            Session.CurrentUser.RemoveProxyRights(iProxyID);
                        }
                        catch (LMP.Exceptions.StoredProcedureException)
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Error_RemoveProxyRights" + iProxyID),
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        }

                    }
                }
                RefreshProxyList();
                this.lstCurrentProxies.ClearSelected();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void ProxiesManager_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && (this.Site == null || !this.Site.DesignMode))
                {
                    this.lblPeople.Text = LMP.Resources.GetLangString("Lbl_People");
                    this.lblProxies.Text = LMP.Resources.GetLangString("Lbl_CurrentProxies");

                    try
                    {
                        //populate people from network db, if necessary
                        if (m_oNetworkUsers == null)
                            m_oNetworkUsers = LMP.Data.User.GetNetworkUserList();
                    }
                    // GLOG : 3375 : JAB
                    // Notify the user that the network is not available instead of showing
                    // this problem as an error.
                    catch (LMP.Exceptions.NetworkException oNE)
                    {
                        this.lstPeople.Items.Add(
                            LMP.Resources.GetLangString("Msg_NoNetworkConnectionAvailable"));
                        this.Enabled = false;
                        MessageBox.Show(LMP.Resources.GetLangString("Prompt_CouldNotRetrieveNetworkUserList"),
                            LMP.ComponentProperties.ProductName);
                    }
                    catch (System.Exception oE)
                    {
                        this.lstPeople.Items.Add(
                            LMP.Resources.GetLangString("Msg_NoNetworkConnectionAvailable"));
                        this.Enabled = false;

                        //GLOG 2942-
                        //GLOG item #4319
                        MessageBox.Show(LMP.Resources.GetLangString("Prompt_CouldNotRetrieveNetworkUserList"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    }
                    finally
                    {
                        if (m_oNetworkUsers != null)
                        {
                            this.lstPeople.DataSource = m_oNetworkUsers;
                            this.lstPeople.DisplayMember = "DisplayName";
                            this.lstPeople.ValueMember = "ID1";
                        }
                        RefreshProxyList();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void ProxiesManager_Resize(object sender, EventArgs e)
        {
            try
            {
                lstPeople.Height = this.Height - (int)(44 * LMP.OS.GetScalingFactor());
                lstCurrentProxies.Height = this.Height - (int)(44 * LMP.OS.GetScalingFactor());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

    }
}
