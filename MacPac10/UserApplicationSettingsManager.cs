using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    public partial class UserApplicationSettingsManager : LMP.Controls.ManagerBase
    {
        private bool m_bInitialized = false;
        private Color m_oTaskPaneBackgroundColor;
        private string m_xTaskPaneBackgroundColorTextEntry;

        public UserApplicationSettingsManager()
        {
            InitializeComponent();

            this.chkDisplayTaskPaneForNonMacPacDoc.Text =
                LMP.Resources.GetLangString("Dialog_DisplayTaskPaneWhenNonMacPacDocOpened");
            this.chkDisplayTaskPaneForMacPacDoc.Text =
                LMP.Resources.GetLangString("Dialog_DisplayTaskPaneWhenMacPacDocOpened");
            this.chkFitToPageWidth.Text =
                LMP.Resources.GetLangString("Dialog_FitToPageWidth");
            this.chkUpdateZoom.Text =
                LMP.Resources.GetLangString("Dialog_UpdateZoom");

            
            // GLOG : 1944 : JAB 
            // For Word 2003, do not display the "Show task pane when opening a document 
            // that contains Forte content"" check box.
            if (LMP.Architect.Api.Application.CurrentWordVersion == 11)
            {
                this.chkDisplayTaskPaneForMacPacDoc.Visible = false;
                this.chkDisplayTaskPaneForNonMacPacDoc.Visible = false;
                this.chkActivateMacPacRibbonTab.Visible = false;
                this.chkActivateMacPacRibbonTabOnOpen.Visible = false;

                int iOffset = this.chkDisplayTaskPaneForMacPacDoc.Height +
                    this.chkDisplayTaskPaneForNonMacPacDoc.Height + 10;

                // Adjust for this check box being omitted by moving the other check boxes up.
                this.chkDisplayTaskPaneForNonMacPacDoc.Top -= iOffset;
                this.chkFitToPageWidth.Top -= iOffset;
                this.chkTaskPaneUsesGradient.Top -= iOffset;
                this.lblTaskPaneBackgroundColor.Top -= iOffset;
                this.pnlTaskPaneColorChip.Top -= iOffset;
                this.btnPickColor.Top -= iOffset;
                this.grpTaskPane.Height -= iOffset;

                int iOffset2 = this.chkDisplayTaskPaneForMacPacDoc.Height + 10;

                this.chkShowXMLTags.Top -= iOffset2;
                this.chkShowLocationTooltips.Top -= iOffset2;
                this.chkShowHelp.Top -= iOffset2;
            }

            this.chkShowXMLTags.Text =
                LMP.Resources.GetLangString("Dialog_ShowXMLTags");
            this.chkShowHelp.Text =
                LMP.Resources.GetLangString("Dialog_ShowQuickHelp");
            this.chkShowLocationTooltips.Text = LMP.Resources.GetLangString("Dialog_ShowLocationTooltips");
            this.chkTaskPaneUsesGradient.Text = LMP.Resources.GetLangString("Dialog_TaskPaneUsesGradient");
            this.chkWarnBeforeDeactivation.Text = LMP.Resources.GetLangString("Dialog_WarnBeforeDeactivation");
            this.chkCloseTaskPaneOnFinish.Text = LMP.Resources.GetLangString("Dialog_CloseTaskPaneOnFinish");
            this.chkWarnOnFinish.Text = LMP.Resources.GetLangString("Dialog_WarnOnFinish");
            //GLOG : 8303 : JSW
            this.chkExpandFindResults.Text = LMP.Resources.GetLangString("Dialog_ExpandFindResults");
            this.lblTaskPaneBackgroundColor.Text = LMP.Resources.GetLangString("Dialog_TaskPaneBackgroundColor");
            this.lblDefaultZoomPercentage.Text = LMP.Resources.GetLangString("Dialog_DefaultZoomPercentage");

            //// Initialize the FavoriteSegments since the closing of the UserAppSettingMgr saves them. 
            //// If this were not done then the an empty set of FavoriteSegments would be saved..
            //FavoriteSegments.Set(Session.CurrentUser.UserSettings.FavoriteSegments);
        }

        private void UserApplicationSettingsManager_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;
                try
                {
                    this.chkDisplayTaskPaneForNonMacPacDoc.Checked =
                        oSettings.ShowTaskPaneWhenOpeningNonMPDoc;

                    this.chkDisplayTaskPaneForMacPacDoc.Checked =
                        oSettings.ShowTaskPaneWhenOpeningMPDoc;

                    this.chkActivateMacPacRibbonTab.Checked = oSettings.ActivateMacPacRibbonTab;
                    this.chkActivateMacPacRibbonTabOnOpen.Checked = oSettings.ActivateMacPacRibbonTabOnOpen;
                    this.chkShowTaskPaneOnBlankNew.Checked = oSettings.ShowTaskPaneWhenCreatingBlankMPDocument;
                    this.chkShowTaskpaneWhenNoVariables.Checked = oSettings.ShowTaskPaneWhenNoVariables; //GLOG 15933
                    this.chkShowXMLTags.Checked = oSettings.ShowXMLTags;
                    this.chkShowHelp.Checked = oSettings.ShowHelp;
                    this.chkShowLocationTooltips.Checked = oSettings.ShowLocationTooltips;
                    this.chkCloseTaskPaneOnFinish.Checked = oSettings.CloseTaskPaneOnFinish;
                    this.chkWarnOnFinish.Checked = oSettings.WarnOnFinish;
                    //GLOG : 8308 : JSW
                    this.chkExpandFindResults.Checked = oSettings.ExpandFindResults;
                    this.chkTaskPaneUsesGradient.Checked = oSettings.UseGradientInTaskPane;
                    this.chkWarnBeforeDeactivation.Checked = oSettings.WarnBeforeDeactivation;
                    this.chkFitToPageWidth.Checked = oSettings.FitToPageWidth;
                    //GLOG : 6597 : JSW
                    this.chkUpdateZoom.Checked = oSettings.UpdateZoom;
                    this.spnDefaultZoomPercentage.Value = oSettings.DefaultPageZoomPercentage;
                    if (this.chkUpdateZoom.Checked == true)
                        this.spnDefaultZoomPercentage.Enabled = true;
                    else
                        this.spnDefaultZoomPercentage.Enabled = false;

                    string xColorCreationFailureReason;

                    m_xTaskPaneBackgroundColorTextEntry = Session.CurrentUser.UserSettings.TaskPaneBackgroundEntry;
                    m_oTaskPaneBackgroundColor = LMP.ColorUtil.ColorFromString(m_xTaskPaneBackgroundColorTextEntry, out xColorCreationFailureReason);

                    if (xColorCreationFailureReason != null)
                    {
                        // We failed to create the color for the reason specified in xColorCreationFailureReason.
                        LMP.Trace.WriteInfo("CreateColorFromColorTextEntry failed: " + xColorCreationFailureReason);

                        // Default to white.
                        m_xTaskPaneBackgroundColorTextEntry = "White";
                        m_oTaskPaneBackgroundColor = Color.White;
                    }

                    this.UpdateTaskPaneBackColorChoiceView();

                    m_bInitialized = true;
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
        }

        private void chkDisplayTaskPaneForNonMacPacDoc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningNonMPDoc = 
                        this.chkDisplayTaskPaneForNonMacPacDoc.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkDisplayTaskPaneForMacPacDoc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningMPDoc =
                        this.chkDisplayTaskPaneForMacPacDoc.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkActiveMacPacRibbonTab_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                    Session.CurrentUser.UserSettings
                        .ActivateMacPacRibbonTab = this.chkActivateMacPacRibbonTab.Checked;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkShowXMLTags_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                    Session.CurrentUser.UserSettings
                        .ShowXMLTags = this.chkShowXMLTags.Checked;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkShowHelp_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                    Session.CurrentUser.UserSettings.ShowHelp = this.chkShowHelp.Checked;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkShowLocationTooltips_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                    Session.CurrentUser.UserSettings.ShowLocationTooltips = this.chkShowLocationTooltips.Checked;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkTaskPaneUsesGradient_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.UseGradientInTaskPane = this.chkTaskPaneUsesGradient.Checked;
                }
            }
            catch(System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkFitToPageWidth_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.FitToPageWidth = this.chkFitToPageWidth.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkWarnBeforeDeletion_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.WarnBeforeDeactivation =
                        this.chkWarnBeforeDeactivation.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void SelectTaskpaneBackcolorViaColorDialog()
        {
            colorDlgTaskPaneBackgroundColor.AllowFullOpen = true;
            colorDlgTaskPaneBackgroundColor.FullOpen = false;
            colorDlgTaskPaneBackgroundColor.ShowHelp = true;
            colorDlgTaskPaneBackgroundColor.SolidColorOnly = true;
            colorDlgTaskPaneBackgroundColor.AnyColor = true;
            
            // Initialize the color dialog to the current color of the task panel
            // in order to keep the original color if the user cancels.
            colorDlgTaskPaneBackgroundColor.Color = this.m_oTaskPaneBackgroundColor;
            colorDlgTaskPaneBackgroundColor.ShowDialog();
            m_oTaskPaneBackgroundColor = colorDlgTaskPaneBackgroundColor.Color;

            if (m_oTaskPaneBackgroundColor.IsKnownColor)
            {
                this.m_xTaskPaneBackgroundColorTextEntry = m_oTaskPaneBackgroundColor.Name;
            }
            else
            {
                this.m_xTaskPaneBackgroundColorTextEntry = m_oTaskPaneBackgroundColor.R.ToString() + ", " +
                                                            m_oTaskPaneBackgroundColor.G.ToString() + ", " +
                                                            m_oTaskPaneBackgroundColor.B.ToString();
            }

            // Save the selected color.
            Session.CurrentUser.UserSettings.TaskPaneBackgroundEntry = this.m_xTaskPaneBackgroundColorTextEntry;
        }

        private void cmbTaskPaneBackgroundColor_DropDown(object sender, EventArgs e)
        {
            SelectTaskpaneBackcolorViaColorDialog();
            UpdateTaskPaneBackColorChoiceView();
        }

        private void UpdateTaskPaneBackColorChoiceView()
        {
            this.pnlTaskPaneColorChip.BackColor = m_oTaskPaneBackgroundColor;
        }

        private string TaskPaneColorName
        {
            get
            {
                if (m_oTaskPaneBackgroundColor.IsNamedColor)
                {
                    return m_oTaskPaneBackgroundColor.Name;
                }

                return      m_oTaskPaneBackgroundColor.R.ToString() + ", " + 
                            m_oTaskPaneBackgroundColor.G.ToString() + ", " +
                            m_oTaskPaneBackgroundColor.B.ToString();
            }
        }

        private void btnPickColor_Click(object sender, EventArgs e)
        {
            try
            {
                SelectTaskpaneBackcolorViaColorDialog();
                UpdateTaskPaneBackColorChoiceView();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkActivateMacPacRibbonTabOnOpen_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                    Session.CurrentUser.UserSettings
                        .ActivateMacPacRibbonTabOnOpen = this.chkActivateMacPacRibbonTabOnOpen.Checked;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void chkShowTaskPaneOnBlankNew_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.ShowTaskPaneWhenCreatingBlankMPDocument =
                        this.chkShowTaskPaneOnBlankNew.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void chkShowTaskPaneWhenNoVariables_CheckedChanged(object sender, EventArgs e)
        {
            //GLOG 15933
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.ShowTaskPaneWhenNoVariables =
                        this.chkShowTaskpaneWhenNoVariables.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG: 6597 : JSW
        private void chkUpdateZoom_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.UpdateZoom = this.chkUpdateZoom.Checked;
                    //enable/disable zoom percentage spinner accordingly
                    if (this.chkUpdateZoom.Checked == true)
                        this.spnDefaultZoomPercentage.Enabled = true;
                    else
                        this.spnDefaultZoomPercentage.Enabled = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 5759 : CEH
        private void spnDefaultZoomPercentage_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    decimal dValue = Convert.ToDecimal(this.spnDefaultZoomPercentage.Value.ToString());
                    int iValue = decimal.ToInt32(dValue + 0.5m);
                    Session.CurrentUser.UserSettings.DefaultPageZoomPercentage = iValue;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6049 : JSW
        private void chkCloseTaskPaneOnFinish_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.CloseTaskPaneOnFinish =
                        this.chkCloseTaskPaneOnFinish.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        //GLOG : 6641 : JSW
        private void chkWarnOnFinish_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.WarnOnFinish =
                        this.chkWarnOnFinish.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        //GLOG : 8308 : JSW
        private void chkExpandFindResults_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bInitialized)
                {
                    Session.CurrentUser.UserSettings.ExpandFindResults =
                        this.chkExpandFindResults.Checked;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        // JAB TODO: BEGIN REMOVE THIS BLOCK
        //private void UpdateFoldersNameView()
        //{
        //    // Get the language appropriate label for each folder.
        //    this.lblPublicFoldersLabel.Text = LMP.Resources.GetLangString("LblPublicFoldersLabel");
        //    this.lblMyFoldersLabel.Text = LMP.Resources.GetLangString("LblMyFoldersLabel");
        //    this.lblSharedFoldersLabel.Text = LMP.Resources.GetLangString("LblSharedFoldersLabel");

        //    // Get the name of each folder. If no value is set use the value used prior to this feature.
        //    this.txtPublicFoldersLabel.Text = Session.CurrentUser.ApplicationSettings.PublicFoldersLabel;

        //    if (this.txtPublicFoldersLabel.Text.Length == 0)
        //    {
        //        this.txtPublicFoldersLabel.Text = LMP.Resources.GetLangString("Prompt_PublicFolders");
        //    }

        //    this.txtMyFoldersLabel.Text = Session.CurrentUser.ApplicationSettings.MyFoldersLabel;

        //    if (this.txtMyFoldersLabel.Text.Length == 0)
        //    {
        //        this.txtMyFoldersLabel.Text = LMP.Resources.GetLangString("Prompt_MyFolders");
        //    }

        //    this.txtSharedFoldersLabel.Text = Session.CurrentUser.ApplicationSettings.SharedFoldersLabel;

        //    if (this.txtSharedFoldersLabel.Text.Length == 0)
        //    {
        //        this.txtSharedFoldersLabel.Text = LMP.Resources.GetLangString("Prompt_SharedFolders");
        //    }
        //}

        //public void SaveFolderNames()
        //{
        //    Session.CurrentUser.ApplicationSettings.PublicFoldersLabel = this.txtPublicFoldersLabel.Text;
        //    Session.CurrentUser.ApplicationSettings.MyFoldersLabel = this.txtMyFoldersLabel.Text;
        //    Session.CurrentUser.ApplicationSettings.SharedFoldersLabel = this.txtSharedFoldersLabel.Text;
        //}

        // JAB TODO: END   REMOVE THIS BLOCK


    }
}
