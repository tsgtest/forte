using System;
using System.Collections.Generic;
using System.Text;
using LMP.Controls;

namespace LMP.MacPac
{
    internal partial class DocumentDesigner : System.Windows.Forms.UserControl
    {
        #region *********************fields*********************
        private System.Windows.Forms.Panel pnlDocContents;
        private System.Windows.Forms.Label lblDocumentContents;
        private System.Windows.Forms.Panel pnlDescription;
        private System.Windows.Forms.ImageList ilImages;
        private LMP.Controls.TreeView treeDocContents;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.PictureBox picPreview;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_Add;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_DeleteAll;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_MoveUp;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_MoveDown;
        private System.Windows.Forms.ToolStripSeparator mnuBlocks_Separator2;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_DeleteAll;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_Copy;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_Paste;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Copy2;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Paste2;
        private System.Windows.Forms.ToolStripSeparator mnuSegments_Separator2;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddVariable;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertText;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ExecuteOnDocument;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ExecuteOnSegment;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ExecuteOnApplication;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_IncludeExcludeText;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertCheckbox;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_SetVariableValue;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_RunMacro;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_RunMethod;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_SetDocPropValue; //GLOG 7495
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_SetDocVarValue;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_SetAsDefaultValue;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ExecuteOnStyle;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ExecuteOnPageSetup;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertTextAsTable;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_UpdateMemoTypePeople;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertTOA;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_EndExecution;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ReplaceSegment;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ExecuteOnBookmark;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ExecuteOnTag;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ExecuteOnBlock;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_RunVariableActions;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_IncludeExcludeBlocks;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_SetPaperSource;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_UnderlineToLongest;
        private TimedContextMenuStrip mnuVariables_AddAction;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAssociatedWordTag;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Assignments;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertDetail;
        #endregion
        #region *********************Component Designer generated code*********************
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentDesigner));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.pnlDocContents = new System.Windows.Forms.Panel();
            this.picPrefs = new System.Windows.Forms.PictureBox();
            this.picSave = new System.Windows.Forms.PictureBox();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.picMenuDropdown = new System.Windows.Forms.PictureBox();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.picPreview = new System.Windows.Forms.PictureBox();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.lblDocumentContents = new System.Windows.Forms.Label();
            this.pnlDescription = new System.Windows.Forms.Panel();
            this.wbHelpText = new System.Windows.Forms.WebBrowser();
            this.mnuSegments_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.ttStatus = new System.Windows.Forms.ToolTip(this.components);
            this.splitContents = new System.Windows.Forms.Splitter();
            this.treeDocContents = new LMP.Controls.TreeView();
            this.mnuVariables = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuVariables_Add = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuVariables_AddAction_EndExecution = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ApplyStyleSheet = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ExecuteOnApplication = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ExecuteOnBookmark = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ExecuteOnDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ExecuteOnPageSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ExecuteOnSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ExecuteOnStyle = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ExecuteOnTag = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ExecuteOnBlock = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_IncludeExcludeBlocks = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_IncludeExcludeText = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertCheckbox = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertDetail = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertDetailIntoTable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertDate = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertReline = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertText = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertTextAsTable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_InsertTOA = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_ReplaceSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_RunMacro = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_RunMethod = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_RunVariableActions = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_SetAsDefaultValue = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_SetDocPropValue = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_SetDocVarValue = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_SetVariableValue = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_SetupDistributedDetailTable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_SetupLabelTable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_SetupDistributedSegmentSections = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_SetPaperSource = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_UnderlineToLongest = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAction_UpdateMemoTypePeople = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_Add = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddVariable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddTaglessVariable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddAssociatedWordTag = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_DeleteAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_MoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_MoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuVariables_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_Separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuVariables_Rename = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuSegments_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_MoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_MoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSegments_Copy2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_Paste2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_Separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSegments_Rename = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_CreateNewChildSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_Assignments = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_ParentAssignments = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_TrailerAssignments = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_DraftStampAssignments = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSegments_DeleteSegment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuBlocks_Add = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks_DeleteAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks_MoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks_MoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuBlocks_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks_Separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuBlocks_Rename = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlocks_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuVariables_AddControlAction_ChangeLabel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_DisplayMessage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_Enable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_EndExecution = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_RefreshControl = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_RunMethod = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_SetDefaultValue = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_SetFocus = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_SetVariableValue = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVariables_AddControlAction_Visible = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAnswerFileSegments = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuAnswerFileSegments_Add = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAnswerFileSegments_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAnswerFileSegments_MoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAnswerFileSegments_MoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlDocContents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPrefs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMenuDropdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPreview)).BeginInit();
            this.pnlDescription.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeDocContents)).BeginInit();
            this.mnuVariables.SuspendLayout();
            this.mnuVariables_AddAction.SuspendLayout();
            this.mnuSegments.SuspendLayout();
            this.mnuBlocks.SuspendLayout();
            this.mnuVariables_AddControlAction.SuspendLayout();
            this.mnuAnswerFileSegments.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDocContents
            // 
            this.pnlDocContents.BackColor = System.Drawing.Color.Transparent;
            this.pnlDocContents.Controls.Add(this.picPrefs);
            this.pnlDocContents.Controls.Add(this.picSave);
            this.pnlDocContents.Controls.Add(this.picClose);
            this.pnlDocContents.Controls.Add(this.picMenuDropdown);
            this.pnlDocContents.Controls.Add(this.picHelp);
            this.pnlDocContents.Controls.Add(this.lblStatus);
            this.pnlDocContents.Controls.Add(this.picPreview);
            this.pnlDocContents.Controls.Add(this.treeDocContents);
            this.pnlDocContents.Controls.Add(this.lblDocumentContents);
            this.pnlDocContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDocContents.Location = new System.Drawing.Point(0, 0);
            this.pnlDocContents.Name = "pnlDocContents";
            this.pnlDocContents.Size = new System.Drawing.Size(328, 453);
            this.pnlDocContents.TabIndex = 19;
            this.pnlDocContents.SizeChanged += new System.EventHandler(this.pnlDocContents_SizeChanged);
            this.pnlDocContents.Resize += new System.EventHandler(this.pnlDocContents_Resize);
            // 
            // picPrefs
            // 
            this.picPrefs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picPrefs.Image = ((System.Drawing.Image)(resources.GetObject("picPrefs.Image")));
            this.picPrefs.Location = new System.Drawing.Point(246, 3);
            this.picPrefs.Name = "picPrefs";
            this.picPrefs.Size = new System.Drawing.Size(17, 15);
            this.picPrefs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPrefs.TabIndex = 54;
            this.picPrefs.TabStop = false;
            this.ttStatus.SetToolTip(this.picPrefs, "Set Author Preferences");
            this.picPrefs.Click += new System.EventHandler(this.picPrefs_Click);
            // 
            // picSave
            // 
            this.picSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picSave.Image = ((System.Drawing.Image)(resources.GetObject("picSave.Image")));
            this.picSave.Location = new System.Drawing.Point(268, 3);
            this.picSave.Name = "picSave";
            this.picSave.Size = new System.Drawing.Size(17, 15);
            this.picSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSave.TabIndex = 53;
            this.picSave.TabStop = false;
            this.ttStatus.SetToolTip(this.picSave, "Save");
            this.picSave.Click += new System.EventHandler(this.picSave_Click);
            // 
            // picClose
            // 
            this.picClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picClose.Image = ((System.Drawing.Image)(resources.GetObject("picClose.Image")));
            this.picClose.Location = new System.Drawing.Point(289, 3);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(17, 15);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picClose.TabIndex = 52;
            this.picClose.TabStop = false;
            this.ttStatus.SetToolTip(this.picClose, "Close");
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // picMenuDropdown
            // 
            this.picMenuDropdown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picMenuDropdown.Image = ((System.Drawing.Image)(resources.GetObject("picMenuDropdown.Image")));
            this.picMenuDropdown.Location = new System.Drawing.Point(295, 28);
            this.picMenuDropdown.Name = "picMenuDropdown";
            this.picMenuDropdown.Size = new System.Drawing.Size(11, 11);
            this.picMenuDropdown.TabIndex = 51;
            this.picMenuDropdown.TabStop = false;
            this.picMenuDropdown.Visible = false;
            this.picMenuDropdown.Click += new System.EventHandler(this.picMenuDropdown_Click);
            // 
            // picHelp
            // 
            this.picHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.picHelp.Image = ((System.Drawing.Image)(resources.GetObject("picHelp.Image")));
            this.picHelp.Location = new System.Drawing.Point(310, 431);
            this.picHelp.Name = "picHelp";
            this.picHelp.Size = new System.Drawing.Size(15, 15);
            this.picHelp.TabIndex = 48;
            this.picHelp.TabStop = false;
            this.ttStatus.SetToolTip(this.picHelp, "Help");
            this.picHelp.Click += new System.EventHandler(this.picHelp_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(5, 432);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 15);
            this.lblStatus.TabIndex = 46;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // picPreview
            // 
            this.picPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picPreview.Image = ((System.Drawing.Image)(resources.GetObject("picPreview.Image")));
            this.picPreview.Location = new System.Drawing.Point(310, 3);
            this.picPreview.Name = "picPreview";
            this.picPreview.Size = new System.Drawing.Size(17, 15);
            this.picPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPreview.TabIndex = 43;
            this.picPreview.TabStop = false;
            this.ttStatus.SetToolTip(this.picPreview, "Preview");
            this.picPreview.Click += new System.EventHandler(this.picPreview_Click);
            this.picPreview.MouseEnter += new System.EventHandler(this.picPreview_MouseEnter);
            this.picPreview.MouseLeave += new System.EventHandler(this.picPreview_MouseLeave);
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Transparent;
            this.ilImages.Images.SetKeyName(0, "DocumentSegment");
            this.ilImages.Images.SetKeyName(1, "ComponentSegment");
            this.ilImages.Images.SetKeyName(2, "ParagraphTextSegment");
            this.ilImages.Images.SetKeyName(3, "StyleSheet");
            this.ilImages.Images.SetKeyName(4, "ValidVariable");
            this.ilImages.Images.SetKeyName(5, "BlueDotTaggedVar.bmpTransparent.bmp");
            this.ilImages.Images.SetKeyName(6, "SentenceTextSegment");
            // 
            // lblDocumentContents
            // 
            this.lblDocumentContents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDocumentContents.AutoSize = true;
            this.lblDocumentContents.BackColor = System.Drawing.Color.Transparent;
            this.lblDocumentContents.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentContents.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblDocumentContents.Location = new System.Drawing.Point(2, 3);
            this.lblDocumentContents.Name = "lblDocumentContents";
            this.lblDocumentContents.Size = new System.Drawing.Size(95, 15);
            this.lblDocumentContents.TabIndex = 0;
            this.lblDocumentContents.Text = "&Content Definition:";
            // 
            // pnlDescription
            // 
            this.pnlDescription.BackColor = System.Drawing.Color.Transparent;
            this.pnlDescription.Controls.Add(this.wbHelpText);
            this.pnlDescription.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDescription.Location = new System.Drawing.Point(0, 453);
            this.pnlDescription.Name = "pnlDescription";
            this.pnlDescription.Size = new System.Drawing.Size(328, 75);
            this.pnlDescription.TabIndex = 39;
            // 
            // wbHelpText
            // 
            this.wbHelpText.AllowWebBrowserDrop = false;
            this.wbHelpText.CausesValidation = false;
            this.wbHelpText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbHelpText.IsWebBrowserContextMenuEnabled = false;
            this.wbHelpText.Location = new System.Drawing.Point(0, 0);
            this.wbHelpText.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbHelpText.Name = "wbHelpText";
            this.wbHelpText.ScriptErrorsSuppressed = true;
            this.wbHelpText.Size = new System.Drawing.Size(328, 75);
            this.wbHelpText.TabIndex = 42;
            this.wbHelpText.TabStop = false;
            this.wbHelpText.WebBrowserShortcutsEnabled = false;
            this.wbHelpText.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.wbHelpText_PreviewKeyDown);
            // 
            // mnuSegments_Copy
            // 
            this.mnuSegments_Copy.Name = "mnuSegments_Copy";
            this.mnuSegments_Copy.Size = new System.Drawing.Size(32, 19);
            // 
            // mnuSegments_Paste
            // 
            this.mnuSegments_Paste.Name = "mnuSegments_Paste";
            this.mnuSegments_Paste.Size = new System.Drawing.Size(32, 19);
            // 
            // splitContents
            // 
            this.splitContents.BackColor = System.Drawing.Color.SlateGray;
            this.splitContents.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContents.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitContents.Location = new System.Drawing.Point(0, 448);
            this.splitContents.Name = "splitContents";
            this.splitContents.Size = new System.Drawing.Size(328, 5);
            this.splitContents.TabIndex = 41;
            this.splitContents.TabStop = false;
            // 
            // treeDocContents
            // 
            this.treeDocContents.AllowDrop = true;
            this.treeDocContents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.White;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.BackwardDiagonal;
            this.treeDocContents.Appearance = appearance1;
            this.treeDocContents.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.treeDocContents.ImageList = this.ilImages;
            this.treeDocContents.ImagePadding = 0;
            this.treeDocContents.IsDirty = false;
            this.treeDocContents.Location = new System.Drawing.Point(1, 19);
            this.treeDocContents.Margin = new System.Windows.Forms.Padding(0);
            this.treeDocContents.Name = "treeDocContents";
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.ForeColor = System.Drawing.Color.Blue;
            _override1.ActiveNodeAppearance = appearance2;
            _override1.ItemHeight = 16;
            _override1.Multiline = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.ForeColor = System.Drawing.Color.Black;
            _override1.NodeAppearance = appearance3;
            _override1.NodeDoubleClickAction = Infragistics.Win.UltraWinTree.NodeDoubleClickAction.ToggleExpansion;
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.ForeColor = System.Drawing.Color.Black;
            _override1.SelectedNodeAppearance = appearance4;
            this.treeDocContents.Override = _override1;
            this.treeDocContents.ScrollBounds = Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill;
            this.treeDocContents.Size = new System.Drawing.Size(326, 409);
            this.treeDocContents.SupportingValues = "";
            this.treeDocContents.TabIndex = 1;
            this.treeDocContents.Tag2 = null;
            this.treeDocContents.Value = null;
            this.treeDocContents.AfterCollapse += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeDocContents_AfterCollapse);
            this.treeDocContents.AfterExpand += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeDocContents_AfterExpand);
            this.treeDocContents.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeDocContents_AfterSelect);
            this.treeDocContents.BeforeActivate += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeActivate);
            this.treeDocContents.BeforeCollapse += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeCollapse);
            this.treeDocContents.BeforeExpand += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeExpand);
            this.treeDocContents.Scroll += new Infragistics.Win.UltraWinTree.TreeScrollEventHandler(this.treeDocContents_Scroll);
            this.treeDocContents.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeDocContents_DragDrop);
            this.treeDocContents.DragOver += new System.Windows.Forms.DragEventHandler(this.treeDocContents_DragOver);
            this.treeDocContents.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.treeDocContents_GiveFeedback);
            this.treeDocContents.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.treeDocContents_QueryContinueDrag);
            this.treeDocContents.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeDocContents_KeyDown);
            this.treeDocContents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeDocContents_MouseDown);
            this.treeDocContents.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeDocContents_MouseMove);
            this.treeDocContents.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeDocContents_MouseUp);
            // 
            // mnuVariables
            // 
            this.mnuVariables.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuVariables_Add,
            this.mnuVariables_AddVariable,
            this.mnuVariables_AddTaglessVariable,
            this.mnuVariables_AddAssociatedWordTag,
            this.mnuVariables_Delete,
            this.mnuVariables_DeleteAll,
            this.mnuVariables_MoveUp,
            this.mnuVariables_MoveDown,
            this.mnuVariables_Separator1,
            this.mnuVariables_Copy,
            this.mnuVariables_Paste,
            this.mnuVariables_Separator2,
            this.mnuVariables_Rename,
            this.mnuVariables_Refresh});
            this.mnuVariables.Name = "mnuVariableActions";
            this.mnuVariables.Size = new System.Drawing.Size(225, 280);
            this.mnuVariables.TimerInterval = 500;
            this.mnuVariables.UseTimer = true;
            this.mnuVariables.Opening += new System.ComponentModel.CancelEventHandler(this.mnuVariables_Opening);
            this.mnuVariables.MouseLeave += new System.EventHandler(this.mnuVariables_MouseLeave);
            // 
            // mnuVariables_Add
            // 
            this.mnuVariables_Add.DropDown = this.mnuVariables_AddAction;
            this.mnuVariables_Add.Name = "mnuVariables_Add";
            this.mnuVariables_Add.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_Add.Text = "#&Add Action...#";
            this.mnuVariables_Add.DropDownOpened += new System.EventHandler(this.mnuVariables_Add_DropDownOpened);
            // 
            // mnuVariables_AddAction
            // 
            this.mnuVariables_AddAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuVariables_AddAction_EndExecution,
            this.mnuVariables_AddAction_ApplyStyleSheet,
            this.mnuVariables_AddAction_ExecuteOnApplication,
            this.mnuVariables_AddAction_ExecuteOnBookmark,
            this.mnuVariables_AddAction_ExecuteOnDocument,
            this.mnuVariables_AddAction_ExecuteOnPageSetup,
            this.mnuVariables_AddAction_ExecuteOnSegment,
            this.mnuVariables_AddAction_ExecuteOnStyle,
            this.mnuVariables_AddAction_ExecuteOnTag,
            this.mnuVariables_AddAction_ExecuteOnBlock,
            this.mnuVariables_AddAction_IncludeExcludeBlocks,
            this.mnuVariables_AddAction_IncludeExcludeText,
            this.mnuVariables_AddAction_InsertCheckbox,
            this.mnuVariables_AddAction_InsertDetail,
            this.mnuVariables_AddAction_InsertDetailIntoTable,
            this.mnuVariables_AddAction_InsertDate,
            this.mnuVariables_AddAction_InsertReline,
            this.mnuVariables_AddAction_InsertSegment,
            this.mnuVariables_AddAction_InsertText,
            this.mnuVariables_AddAction_InsertTextAsTable,
            this.mnuVariables_AddAction_InsertTOA,
            this.mnuVariables_AddAction_ReplaceSegment,
            this.mnuVariables_AddAction_RunMacro,
            this.mnuVariables_AddAction_RunMethod,
            this.mnuVariables_AddAction_RunVariableActions,
            this.mnuVariables_AddAction_SetAsDefaultValue,
            this.mnuVariables_AddAction_SetDocPropValue,
            this.mnuVariables_AddAction_SetDocVarValue,
            this.mnuVariables_AddAction_SetVariableValue,
            this.mnuVariables_AddAction_SetupDistributedDetailTable,
            this.mnuVariables_AddAction_SetupLabelTable,
            this.mnuVariables_AddAction_SetupDistributedSegmentSections,
            this.mnuVariables_AddAction_SetPaperSource,
            this.mnuVariables_AddAction_UnderlineToLongest,
            this.mnuVariables_AddAction_UpdateMemoTypePeople});
            this.mnuVariables_AddAction.Name = "mnuVariables_AddActions";
            this.mnuVariables_AddAction.OwnerItem = this.mnuVariables_Add;
            this.mnuVariables_AddAction.Size = new System.Drawing.Size(221, 774);
            this.mnuVariables_AddAction.TimerInterval = 500;
            this.mnuVariables_AddAction.UseTimer = true;
            this.mnuVariables_AddAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuVariables_AddAction_Opening);
            this.mnuVariables_AddAction.MouseLeave += new System.EventHandler(this.mnuVariables_AddAction_MouseLeave);
            // 
            // mnuVariables_AddAction_EndExecution
            // 
            this.mnuVariables_AddAction_EndExecution.Name = "mnuVariables_AddAction_EndExecution";
            this.mnuVariables_AddAction_EndExecution.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_EndExecution.Tag = "EndExecution";
            this.mnuVariables_AddAction_EndExecution.Text = "EndExecution";
            this.mnuVariables_AddAction_EndExecution.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ApplyStyleSheet
            // 
            this.mnuVariables_AddAction_ApplyStyleSheet.Name = "mnuVariables_AddAction_ApplyStyleSheet";
            this.mnuVariables_AddAction_ApplyStyleSheet.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ApplyStyleSheet.Tag = "ApplyStyleSheet";
            this.mnuVariables_AddAction_ApplyStyleSheet.Text = "ApplyStyleSheet";
            this.mnuVariables_AddAction_ApplyStyleSheet.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ExecuteOnApplication
            // 
            this.mnuVariables_AddAction_ExecuteOnApplication.Name = "mnuVariables_AddAction_ExecuteOnApplication";
            this.mnuVariables_AddAction_ExecuteOnApplication.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ExecuteOnApplication.Tag = "ExecuteOnApplication";
            this.mnuVariables_AddAction_ExecuteOnApplication.Text = "&ExecuteOnApplication";
            this.mnuVariables_AddAction_ExecuteOnApplication.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ExecuteOnBookmark
            // 
            this.mnuVariables_AddAction_ExecuteOnBookmark.Name = "mnuVariables_AddAction_ExecuteOnBookmark";
            this.mnuVariables_AddAction_ExecuteOnBookmark.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ExecuteOnBookmark.Tag = "ExecuteOnBookmark";
            this.mnuVariables_AddAction_ExecuteOnBookmark.Text = "ExecuteOnBookmark";
            this.mnuVariables_AddAction_ExecuteOnBookmark.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ExecuteOnDocument
            // 
            this.mnuVariables_AddAction_ExecuteOnDocument.Name = "mnuVariables_AddAction_ExecuteOnDocument";
            this.mnuVariables_AddAction_ExecuteOnDocument.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ExecuteOnDocument.Tag = "ExecuteOnDocument";
            this.mnuVariables_AddAction_ExecuteOnDocument.Text = "ExecuteOnDocument";
            this.mnuVariables_AddAction_ExecuteOnDocument.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ExecuteOnPageSetup
            // 
            this.mnuVariables_AddAction_ExecuteOnPageSetup.Name = "mnuVariables_AddAction_ExecuteOnPageSetup";
            this.mnuVariables_AddAction_ExecuteOnPageSetup.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ExecuteOnPageSetup.Tag = "ExecuteOnPageSetup";
            this.mnuVariables_AddAction_ExecuteOnPageSetup.Text = "ExecuteOnPageSetup";
            this.mnuVariables_AddAction_ExecuteOnPageSetup.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ExecuteOnSegment
            // 
            this.mnuVariables_AddAction_ExecuteOnSegment.Name = "mnuVariables_AddAction_ExecuteOnSegment";
            this.mnuVariables_AddAction_ExecuteOnSegment.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ExecuteOnSegment.Tag = "ExecuteOnSegment";
            this.mnuVariables_AddAction_ExecuteOnSegment.Text = "ExecuteOnSegment";
            this.mnuVariables_AddAction_ExecuteOnSegment.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ExecuteOnStyle
            // 
            this.mnuVariables_AddAction_ExecuteOnStyle.Name = "mnuVariables_AddAction_ExecuteOnStyle";
            this.mnuVariables_AddAction_ExecuteOnStyle.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ExecuteOnStyle.Tag = "ExecuteOnStyle";
            this.mnuVariables_AddAction_ExecuteOnStyle.Text = "ExecuteOnStyle";
            this.mnuVariables_AddAction_ExecuteOnStyle.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ExecuteOnTag
            // 
            this.mnuVariables_AddAction_ExecuteOnTag.Name = "mnuVariables_AddAction_ExecuteOnTag";
            this.mnuVariables_AddAction_ExecuteOnTag.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ExecuteOnTag.Tag = "ExecuteOnTag";
            this.mnuVariables_AddAction_ExecuteOnTag.Text = "ExecuteOnTag";
            this.mnuVariables_AddAction_ExecuteOnTag.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ExecuteOnBlock
            // 
            this.mnuVariables_AddAction_ExecuteOnBlock.Name = "mnuVariables_AddAction_ExecuteOnBlock";
            this.mnuVariables_AddAction_ExecuteOnBlock.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ExecuteOnBlock.Tag = "ExecuteOnBlock";
            this.mnuVariables_AddAction_ExecuteOnBlock.Text = "ExecuteOnBlock";
            this.mnuVariables_AddAction_ExecuteOnBlock.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_IncludeExcludeBlocks
            // 
            this.mnuVariables_AddAction_IncludeExcludeBlocks.Name = "mnuVariables_AddAction_IncludeExcludeBlocks";
            this.mnuVariables_AddAction_IncludeExcludeBlocks.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_IncludeExcludeBlocks.Tag = "IncludeExcludeBlocks";
            this.mnuVariables_AddAction_IncludeExcludeBlocks.Text = "IncludeExcludeBlocks";
            this.mnuVariables_AddAction_IncludeExcludeBlocks.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_IncludeExcludeText
            // 
            this.mnuVariables_AddAction_IncludeExcludeText.Name = "mnuVariables_AddAction_IncludeExcludeText";
            this.mnuVariables_AddAction_IncludeExcludeText.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_IncludeExcludeText.Tag = "IncludeExcludeText";
            this.mnuVariables_AddAction_IncludeExcludeText.Text = "IncludeExcludeText";
            this.mnuVariables_AddAction_IncludeExcludeText.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertCheckbox
            // 
            this.mnuVariables_AddAction_InsertCheckbox.Name = "mnuVariables_AddAction_InsertCheckbox";
            this.mnuVariables_AddAction_InsertCheckbox.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertCheckbox.Tag = "InsertCheckbox";
            this.mnuVariables_AddAction_InsertCheckbox.Text = "InsertCheckbox";
            this.mnuVariables_AddAction_InsertCheckbox.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertDetail
            // 
            this.mnuVariables_AddAction_InsertDetail.Name = "mnuVariables_AddAction_InsertDetail";
            this.mnuVariables_AddAction_InsertDetail.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertDetail.Tag = "InsertDetail";
            this.mnuVariables_AddAction_InsertDetail.Text = "InsertDetail";
            this.mnuVariables_AddAction_InsertDetail.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertDetailIntoTable
            // 
            this.mnuVariables_AddAction_InsertDetailIntoTable.Name = "mnuVariables_AddAction_InsertDetailIntoTable";
            this.mnuVariables_AddAction_InsertDetailIntoTable.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertDetailIntoTable.Tag = "InsertDetailIntoTable";
            this.mnuVariables_AddAction_InsertDetailIntoTable.Text = "InsertDetailIntoTable";
            this.mnuVariables_AddAction_InsertDetailIntoTable.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertDate
            // 
            this.mnuVariables_AddAction_InsertDate.Name = "mnuVariables_AddAction_InsertDate";
            this.mnuVariables_AddAction_InsertDate.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertDate.Tag = "InsertDate";
            this.mnuVariables_AddAction_InsertDate.Text = "InsertDate";
            this.mnuVariables_AddAction_InsertDate.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertReline
            // 
            this.mnuVariables_AddAction_InsertReline.Name = "mnuVariables_AddAction_InsertReline";
            this.mnuVariables_AddAction_InsertReline.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertReline.Tag = "InsertReline";
            this.mnuVariables_AddAction_InsertReline.Text = "InsertReline";
            this.mnuVariables_AddAction_InsertReline.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertSegment
            // 
            this.mnuVariables_AddAction_InsertSegment.Name = "mnuVariables_AddAction_InsertSegment";
            this.mnuVariables_AddAction_InsertSegment.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertSegment.Tag = "InsertSegment";
            this.mnuVariables_AddAction_InsertSegment.Text = "InsertSegment";
            this.mnuVariables_AddAction_InsertSegment.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertText
            // 
            this.mnuVariables_AddAction_InsertText.Name = "mnuVariables_AddAction_InsertText";
            this.mnuVariables_AddAction_InsertText.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertText.Tag = "InsertText";
            this.mnuVariables_AddAction_InsertText.Text = "InsertText";
            this.mnuVariables_AddAction_InsertText.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertTextAsTable
            // 
            this.mnuVariables_AddAction_InsertTextAsTable.Name = "mnuVariables_AddAction_InsertTextAsTable";
            this.mnuVariables_AddAction_InsertTextAsTable.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertTextAsTable.Tag = "InsertTextAsTable";
            this.mnuVariables_AddAction_InsertTextAsTable.Text = "InsertTextAsTable";
            this.mnuVariables_AddAction_InsertTextAsTable.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_InsertTOA
            // 
            this.mnuVariables_AddAction_InsertTOA.Name = "mnuVariables_AddAction_InsertTOA";
            this.mnuVariables_AddAction_InsertTOA.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_InsertTOA.Tag = "InsertTOA";
            this.mnuVariables_AddAction_InsertTOA.Text = "InsertTOA";
            this.mnuVariables_AddAction_InsertTOA.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_ReplaceSegment
            // 
            this.mnuVariables_AddAction_ReplaceSegment.Name = "mnuVariables_AddAction_ReplaceSegment";
            this.mnuVariables_AddAction_ReplaceSegment.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_ReplaceSegment.Tag = "ReplaceSegment";
            this.mnuVariables_AddAction_ReplaceSegment.Text = "ReplaceSegment";
            this.mnuVariables_AddAction_ReplaceSegment.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_RunMacro
            // 
            this.mnuVariables_AddAction_RunMacro.Name = "mnuVariables_AddAction_RunMacro";
            this.mnuVariables_AddAction_RunMacro.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_RunMacro.Tag = "RunMacro";
            this.mnuVariables_AddAction_RunMacro.Text = "RunMacro";
            this.mnuVariables_AddAction_RunMacro.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_RunMethod
            // 
            this.mnuVariables_AddAction_RunMethod.Name = "mnuVariables_AddAction_RunMethod";
            this.mnuVariables_AddAction_RunMethod.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_RunMethod.Tag = "RunMethod";
            this.mnuVariables_AddAction_RunMethod.Text = "RunMethod";
            this.mnuVariables_AddAction_RunMethod.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_RunVariableActions
            // 
            this.mnuVariables_AddAction_RunVariableActions.Name = "mnuVariables_AddAction_RunVariableActions";
            this.mnuVariables_AddAction_RunVariableActions.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_RunVariableActions.Tag = "RunVariableActions";
            this.mnuVariables_AddAction_RunVariableActions.Text = "RunVariableActions";
            this.mnuVariables_AddAction_RunVariableActions.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_SetAsDefaultValue
            // 
            this.mnuVariables_AddAction_SetAsDefaultValue.Name = "mnuVariables_AddAction_SetAsDefaultValue";
            this.mnuVariables_AddAction_SetAsDefaultValue.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_SetAsDefaultValue.Tag = "SetAsDefaultValue";
            this.mnuVariables_AddAction_SetAsDefaultValue.Text = "SetAsDefaultValue";
            this.mnuVariables_AddAction_SetAsDefaultValue.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_SetDocPropValue
            // 
            this.mnuVariables_AddAction_SetDocPropValue.Name = "mnuVariables_AddAction_SetDocPropValue";
            this.mnuVariables_AddAction_SetDocPropValue.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_SetDocPropValue.Tag = "SetDocPropValue";
            this.mnuVariables_AddAction_SetDocPropValue.Text = "SetDocPropValue";
            this.mnuVariables_AddAction_SetDocPropValue.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_SetDocVarValue
            // 
            this.mnuVariables_AddAction_SetDocVarValue.Name = "mnuVariables_AddAction_SetDocVarValue";
            this.mnuVariables_AddAction_SetDocVarValue.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_SetDocVarValue.Tag = "SetDocVarValue";
            this.mnuVariables_AddAction_SetDocVarValue.Text = "SetDocVarValue";
            this.mnuVariables_AddAction_SetDocVarValue.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_SetVariableValue
            // 
            this.mnuVariables_AddAction_SetVariableValue.Name = "mnuVariables_AddAction_SetVariableValue";
            this.mnuVariables_AddAction_SetVariableValue.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_SetVariableValue.Tag = "SetVariableValue";
            this.mnuVariables_AddAction_SetVariableValue.Text = "SetVariableValue";
            this.mnuVariables_AddAction_SetVariableValue.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_SetupDistributedDetailTable
            // 
            this.mnuVariables_AddAction_SetupDistributedDetailTable.Name = "mnuVariables_AddAction_SetupDistributedDetailTable";
            this.mnuVariables_AddAction_SetupDistributedDetailTable.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_SetupDistributedDetailTable.Tag = "SetupDetailTable";
            this.mnuVariables_AddAction_SetupDistributedDetailTable.Text = "SetupDistributedDetailTable";
            this.mnuVariables_AddAction_SetupDistributedDetailTable.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_SetupLabelTable
            // 
            this.mnuVariables_AddAction_SetupLabelTable.Name = "mnuVariables_AddAction_SetupLabelTable";
            this.mnuVariables_AddAction_SetupLabelTable.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_SetupLabelTable.Tag = "SetupLabelTable";
            this.mnuVariables_AddAction_SetupLabelTable.Text = "SetupLabelTable";
            this.mnuVariables_AddAction_SetupLabelTable.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_SetupDistributedSegmentSections
            // 
            this.mnuVariables_AddAction_SetupDistributedSegmentSections.Name = "mnuVariables_AddAction_SetupDistributedSegmentSections";
            this.mnuVariables_AddAction_SetupDistributedSegmentSections.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_SetupDistributedSegmentSections.Tag = "SetupDistributedSections";
            this.mnuVariables_AddAction_SetupDistributedSegmentSections.Text = "SetupDistributedSections";
            this.mnuVariables_AddAction_SetupDistributedSegmentSections.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_SetPaperSource
            // 
            this.mnuVariables_AddAction_SetPaperSource.Name = "mnuVariables_AddAction_SetPaperSource";
            this.mnuVariables_AddAction_SetPaperSource.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_SetPaperSource.Tag = "SetPaperSource";
            this.mnuVariables_AddAction_SetPaperSource.Text = "SetPaperSource";
            this.mnuVariables_AddAction_SetPaperSource.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_UnderlineToLongest
            // 
            this.mnuVariables_AddAction_UnderlineToLongest.Name = "mnuVariables_AddAction_UnderlineToLongest";
            this.mnuVariables_AddAction_UnderlineToLongest.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_UnderlineToLongest.Tag = "UnderlineToLongest";
            this.mnuVariables_AddAction_UnderlineToLongest.Text = "UnderlineToLongest";
            this.mnuVariables_AddAction_UnderlineToLongest.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddAction_UpdateMemoTypePeople
            // 
            this.mnuVariables_AddAction_UpdateMemoTypePeople.Name = "mnuVariables_AddAction_UpdateMemoTypePeople";
            this.mnuVariables_AddAction_UpdateMemoTypePeople.Size = new System.Drawing.Size(220, 22);
            this.mnuVariables_AddAction_UpdateMemoTypePeople.Tag = "UpdateMemoTypePeople";
            this.mnuVariables_AddAction_UpdateMemoTypePeople.Text = "UpdateMemoTypePeople";
            this.mnuVariables_AddAction_UpdateMemoTypePeople.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuSegments_Add
            // 
            this.mnuSegments_Add.DropDown = this.mnuVariables_AddAction;
            this.mnuSegments_Add.Name = "mnuSegments_Add";
            this.mnuSegments_Add.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_Add.Text = "#&Add Action...#";
            this.mnuSegments_Add.DropDownClosed += new System.EventHandler(this.mnuSegments_Add_DropDownClosed);
            this.mnuSegments_Add.DropDownOpened += new System.EventHandler(this.mnuSegments_Add_DropDownOpened);
            // 
            // mnuVariables_AddVariable
            // 
            this.mnuVariables_AddVariable.Name = "mnuVariables_AddVariable";
            this.mnuVariables_AddVariable.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_AddVariable.Text = "#Add Variable#";
            this.mnuVariables_AddVariable.Click += new System.EventHandler(this.mnuVariables_AddVariable_Click);
            // 
            // mnuVariables_AddTaglessVariable
            // 
            this.mnuVariables_AddTaglessVariable.Name = "mnuVariables_AddTaglessVariable";
            this.mnuVariables_AddTaglessVariable.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_AddTaglessVariable.Text = "#Add Tagless Variable#";
            this.mnuVariables_AddTaglessVariable.Click += new System.EventHandler(this.mnuVariables_AddTaglessVariable_Click);
            // 
            // mnuVariables_AddAssociatedWordTag
            // 
            this.mnuVariables_AddAssociatedWordTag.Name = "mnuVariables_AddAssociatedWordTag";
            this.mnuVariables_AddAssociatedWordTag.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_AddAssociatedWordTag.Text = "#Add Associated Word Tag#";
            this.mnuVariables_AddAssociatedWordTag.Click += new System.EventHandler(this.mnuVariables_AddAssociatedWordTag_Click);
            // 
            // mnuVariables_Delete
            // 
            this.mnuVariables_Delete.Name = "mnuVariables_Delete";
            this.mnuVariables_Delete.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_Delete.Text = "#Delete#";
            this.mnuVariables_Delete.Click += new System.EventHandler(this.mnuVariables_Delete_Click);
            // 
            // mnuVariables_DeleteAll
            // 
            this.mnuVariables_DeleteAll.Name = "mnuVariables_DeleteAll";
            this.mnuVariables_DeleteAll.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_DeleteAll.Text = "#D&elete All...#";
            // 
            // mnuVariables_MoveUp
            // 
            this.mnuVariables_MoveUp.Name = "mnuVariables_MoveUp";
            this.mnuVariables_MoveUp.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_MoveUp.Text = "#&Move Up...#";
            this.mnuVariables_MoveUp.Click += new System.EventHandler(this.mnuVariables_MoveUp_Click);
            // 
            // mnuVariables_MoveDown
            // 
            this.mnuVariables_MoveDown.Name = "mnuVariables_MoveDown";
            this.mnuVariables_MoveDown.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_MoveDown.Text = "#Move &Down...#";
            this.mnuVariables_MoveDown.Click += new System.EventHandler(this.mnuVariables_MoveDown_Click);
            // 
            // mnuVariables_Separator1
            // 
            this.mnuVariables_Separator1.Name = "mnuVariables_Separator1";
            this.mnuVariables_Separator1.Size = new System.Drawing.Size(221, 6);
            // 
            // mnuVariables_Copy
            // 
            this.mnuVariables_Copy.Name = "mnuVariables_Copy";
            this.mnuVariables_Copy.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_Copy.Text = "#Copy#";
            // 
            // mnuVariables_Paste
            // 
            this.mnuVariables_Paste.Name = "mnuVariables_Paste";
            this.mnuVariables_Paste.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_Paste.Text = "#Paste#";
            // 
            // mnuVariables_Separator2
            // 
            this.mnuVariables_Separator2.Name = "mnuVariables_Separator2";
            this.mnuVariables_Separator2.Size = new System.Drawing.Size(221, 6);
            // 
            // mnuVariables_Rename
            // 
            this.mnuVariables_Rename.Name = "mnuVariables_Rename";
            this.mnuVariables_Rename.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_Rename.Text = "#Rename#";
            // 
            // mnuVariables_Refresh
            // 
            this.mnuVariables_Refresh.Name = "mnuVariables_Refresh";
            this.mnuVariables_Refresh.Size = new System.Drawing.Size(224, 22);
            this.mnuVariables_Refresh.Text = "#Refresh#";
            // 
            // mnuSegments
            // 
            this.mnuSegments.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSegments_Add,
            this.mnuSegments_Delete,
            this.mnuSegments_MoveUp,
            this.mnuSegments_MoveDown,
            this.mnuSegments_Separator1,
            this.mnuSegments_Copy2,
            this.mnuSegments_Paste2,
            this.mnuSegments_Separator2,
            this.mnuSegments_Rename,
            this.mnuSegments_Refresh,
            this.mnuSegments_CreateNewChildSegment,
            this.mnuSegments_Assignments,
            this.mnuSegments_ParentAssignments,
            this.mnuSegments_TrailerAssignments,
            this.mnuSegments_DraftStampAssignments,
            this.mnuSegments_DeleteSegment});
            this.mnuSegments.Name = "mnuVariableActions";
            this.mnuSegments.Size = new System.Drawing.Size(231, 324);
            this.mnuSegments.TimerInterval = 500;
            this.mnuSegments.UseTimer = true;
            this.mnuSegments.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.mnuSegments_Closing);
            this.mnuSegments.Opening += new System.ComponentModel.CancelEventHandler(this.mnuSegments_Opening);
            this.mnuSegments.MouseLeave += new System.EventHandler(this.mnuSegments_MouseLeave);
            // 
            // mnuSegments_Delete
            // 
            this.mnuSegments_Delete.Name = "mnuSegments_Delete";
            this.mnuSegments_Delete.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_Delete.Text = "#Delete#";
            this.mnuSegments_Delete.Click += new System.EventHandler(this.mnuSegments_Delete_Click);
            // 
            // mnuSegments_MoveUp
            // 
            this.mnuSegments_MoveUp.Name = "mnuSegments_MoveUp";
            this.mnuSegments_MoveUp.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_MoveUp.Text = "#&Move Up...#";
            this.mnuSegments_MoveUp.Click += new System.EventHandler(this.mnuSegments_MoveUp_Click);
            // 
            // mnuSegments_MoveDown
            // 
            this.mnuSegments_MoveDown.Name = "mnuSegments_MoveDown";
            this.mnuSegments_MoveDown.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_MoveDown.Text = "#Move &Down...#";
            this.mnuSegments_MoveDown.Click += new System.EventHandler(this.mnuSegments_MoveDown_Click);
            // 
            // mnuSegments_Separator1
            // 
            this.mnuSegments_Separator1.Name = "mnuSegments_Separator1";
            this.mnuSegments_Separator1.Size = new System.Drawing.Size(227, 6);
            // 
            // mnuSegments_Copy2
            // 
            this.mnuSegments_Copy2.Name = "mnuSegments_Copy2";
            this.mnuSegments_Copy2.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_Copy2.Text = "#Copy#";
            // 
            // mnuSegments_Paste2
            // 
            this.mnuSegments_Paste2.Name = "mnuSegments_Paste2";
            this.mnuSegments_Paste2.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_Paste2.Text = "#Paste#";
            // 
            // mnuSegments_Separator2
            // 
            this.mnuSegments_Separator2.Name = "mnuSegments_Separator2";
            this.mnuSegments_Separator2.Size = new System.Drawing.Size(227, 6);
            // 
            // mnuSegments_Rename
            // 
            this.mnuSegments_Rename.Name = "mnuSegments_Rename";
            this.mnuSegments_Rename.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_Rename.Text = "#Rename#";
            // 
            // mnuSegments_Refresh
            // 
            this.mnuSegments_Refresh.Name = "mnuSegments_Refresh";
            this.mnuSegments_Refresh.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_Refresh.Text = "#Refresh Segment#";
            // 
            // mnuSegments_CreateNewChildSegment
            // 
            this.mnuSegments_CreateNewChildSegment.Name = "mnuSegments_CreateNewChildSegment";
            this.mnuSegments_CreateNewChildSegment.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_CreateNewChildSegment.Text = "#Create New Child Segment#";
            this.mnuSegments_CreateNewChildSegment.Click += new System.EventHandler(this.mnuSegments_CreateNewChildSegment_Click);
            // 
            // mnuSegments_Assignments
            // 
            this.mnuSegments_Assignments.Name = "mnuSegments_Assignments";
            this.mnuSegments_Assignments.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_Assignments.Text = "#Assignments#";
            this.mnuSegments_Assignments.Click += new System.EventHandler(this.mnuSegments_Assignments_Click);
            // 
            // mnuSegments_ParentAssignments
            // 
            this.mnuSegments_ParentAssignments.Name = "mnuSegments_ParentAssignments";
            this.mnuSegments_ParentAssignments.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_ParentAssignments.Text = "#Parent Assignments#";
            this.mnuSegments_ParentAssignments.Click += new System.EventHandler(this.mnuSegments_ParentAssignments_Click);
            // 
            // mnuSegments_TrailerAssignments
            // 
            this.mnuSegments_TrailerAssignments.Name = "mnuSegments_TrailerAssignments";
            this.mnuSegments_TrailerAssignments.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_TrailerAssignments.Text = "#Trailer Assignments#";
            this.mnuSegments_TrailerAssignments.Click += new System.EventHandler(this.mnuSegments_TrailerAssignments_Click);
            // 
            // mnuSegments_DraftStampAssignments
            // 
            this.mnuSegments_DraftStampAssignments.Name = "mnuSegments_DraftStampAssignments";
            this.mnuSegments_DraftStampAssignments.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_DraftStampAssignments.Text = "#Draft Stamp Assignments#";
            this.mnuSegments_DraftStampAssignments.Click += new System.EventHandler(this.mnuSegments_DraftStampAssignments_Click);
            // 
            // mnuSegments_DeleteSegment
            // 
            this.mnuSegments_DeleteSegment.Name = "mnuSegments_DeleteSegment";
            this.mnuSegments_DeleteSegment.Size = new System.Drawing.Size(230, 22);
            this.mnuSegments_DeleteSegment.Text = "#Delete Segment#";
            this.mnuSegments_DeleteSegment.Click += new System.EventHandler(this.mnuSegments_DeleteSegment_Click);
            // 
            // mnuBlocks
            // 
            this.mnuBlocks.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuBlocks_Add,
            this.mnuBlocks_Delete,
            this.mnuBlocks_DeleteAll,
            this.mnuBlocks_MoveUp,
            this.mnuBlocks_MoveDown,
            this.mnuBlocks_Separator1,
            this.mnuBlocks_Copy,
            this.mnuBlocks_Paste,
            this.mnuBlocks_Separator2,
            this.mnuBlocks_Rename,
            this.mnuBlocks_Refresh});
            this.mnuBlocks.Name = "mnuBlocks";
            this.mnuBlocks.Size = new System.Drawing.Size(150, 214);
            this.mnuBlocks.TimerInterval = 500;
            this.mnuBlocks.UseTimer = true;
            this.mnuBlocks.Opening += new System.ComponentModel.CancelEventHandler(this.mnuBlocks_Opening);
            this.mnuBlocks.MouseLeave += new System.EventHandler(this.mnuBlocks_MouseLeave);
            // 
            // mnuBlocks_Add
            // 
            this.mnuBlocks_Add.Name = "mnuBlocks_Add";
            this.mnuBlocks_Add.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_Add.Text = "#Add#";
            this.mnuBlocks_Add.Click += new System.EventHandler(this.mnuBlocks_Add_Click);
            // 
            // mnuBlocks_Delete
            // 
            this.mnuBlocks_Delete.Name = "mnuBlocks_Delete";
            this.mnuBlocks_Delete.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_Delete.Text = "#Delete#";
            this.mnuBlocks_Delete.Click += new System.EventHandler(this.mnuBlocks_Delete_Click);
            // 
            // mnuBlocks_DeleteAll
            // 
            this.mnuBlocks_DeleteAll.Name = "mnuBlocks_DeleteAll";
            this.mnuBlocks_DeleteAll.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_DeleteAll.Text = "#DeleteAll#";
            // 
            // mnuBlocks_MoveUp
            // 
            this.mnuBlocks_MoveUp.Name = "mnuBlocks_MoveUp";
            this.mnuBlocks_MoveUp.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_MoveUp.Text = "#MoveUp#";
            // 
            // mnuBlocks_MoveDown
            // 
            this.mnuBlocks_MoveDown.Name = "mnuBlocks_MoveDown";
            this.mnuBlocks_MoveDown.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_MoveDown.Text = "#MoveDown#";
            // 
            // mnuBlocks_Separator1
            // 
            this.mnuBlocks_Separator1.Name = "mnuBlocks_Separator1";
            this.mnuBlocks_Separator1.Size = new System.Drawing.Size(146, 6);
            // 
            // mnuBlocks_Copy
            // 
            this.mnuBlocks_Copy.Name = "mnuBlocks_Copy";
            this.mnuBlocks_Copy.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_Copy.Text = "#Copy#";
            // 
            // mnuBlocks_Paste
            // 
            this.mnuBlocks_Paste.Name = "mnuBlocks_Paste";
            this.mnuBlocks_Paste.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_Paste.Text = "#Paste#";
            // 
            // mnuBlocks_Separator2
            // 
            this.mnuBlocks_Separator2.Name = "mnuBlocks_Separator2";
            this.mnuBlocks_Separator2.Size = new System.Drawing.Size(146, 6);
            // 
            // mnuBlocks_Rename
            // 
            this.mnuBlocks_Rename.Name = "mnuBlocks_Rename";
            this.mnuBlocks_Rename.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_Rename.Text = "#Rename#";
            // 
            // mnuBlocks_Refresh
            // 
            this.mnuBlocks_Refresh.Name = "mnuBlocks_Refresh";
            this.mnuBlocks_Refresh.Size = new System.Drawing.Size(149, 22);
            this.mnuBlocks_Refresh.Text = "#Refresh#";
            // 
            // mnuVariables_AddControlAction
            // 
            this.mnuVariables_AddControlAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuVariables_AddControlAction_ChangeLabel,
            this.mnuVariables_AddControlAction_DisplayMessage,
            this.mnuVariables_AddControlAction_Enable,
            this.mnuVariables_AddControlAction_EndExecution,
            this.mnuVariables_AddControlAction_RefreshControl,
            this.mnuVariables_AddControlAction_RunMethod,
            this.mnuVariables_AddControlAction_SetDefaultValue,
            this.mnuVariables_AddControlAction_SetFocus,
            this.mnuVariables_AddControlAction_SetVariableValue,
            this.mnuVariables_AddControlAction_Visible});
            this.mnuVariables_AddControlAction.Name = "mnuVariables_AddActions";
            this.mnuVariables_AddControlAction.Size = new System.Drawing.Size(160, 224);
            this.mnuVariables_AddControlAction.TimerInterval = 500;
            this.mnuVariables_AddControlAction.UseTimer = true;
            this.mnuVariables_AddControlAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuVariables_AddAction_Opening);
            this.mnuVariables_AddControlAction.MouseLeave += new System.EventHandler(this.mnuVariables_AddAction_MouseLeave);
            // 
            // mnuVariables_AddControlAction_ChangeLabel
            // 
            this.mnuVariables_AddControlAction_ChangeLabel.Name = "mnuVariables_AddControlAction_ChangeLabel";
            this.mnuVariables_AddControlAction_ChangeLabel.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_ChangeLabel.Tag = "ChangeLabel";
            this.mnuVariables_AddControlAction_ChangeLabel.Text = "ChangeLabel";
            this.mnuVariables_AddControlAction_ChangeLabel.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_DisplayMessage
            // 
            this.mnuVariables_AddControlAction_DisplayMessage.Name = "mnuVariables_AddControlAction_DisplayMessage";
            this.mnuVariables_AddControlAction_DisplayMessage.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_DisplayMessage.Tag = "DisplayMessage";
            this.mnuVariables_AddControlAction_DisplayMessage.Text = "DisplayMessage";
            this.mnuVariables_AddControlAction_DisplayMessage.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_Enable
            // 
            this.mnuVariables_AddControlAction_Enable.Name = "mnuVariables_AddControlAction_Enable";
            this.mnuVariables_AddControlAction_Enable.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_Enable.Tag = "Enable";
            this.mnuVariables_AddControlAction_Enable.Text = "Enable";
            this.mnuVariables_AddControlAction_Enable.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_EndExecution
            // 
            this.mnuVariables_AddControlAction_EndExecution.Name = "mnuVariables_AddControlAction_EndExecution";
            this.mnuVariables_AddControlAction_EndExecution.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_EndExecution.Tag = "EndExecution";
            this.mnuVariables_AddControlAction_EndExecution.Text = "EndExecution";
            this.mnuVariables_AddControlAction_EndExecution.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_RefreshControl
            // 
            this.mnuVariables_AddControlAction_RefreshControl.Name = "mnuVariables_AddControlAction_RefreshControl";
            this.mnuVariables_AddControlAction_RefreshControl.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_RefreshControl.Tag = "RefreshControl";
            this.mnuVariables_AddControlAction_RefreshControl.Text = "RefreshControl";
            this.mnuVariables_AddControlAction_RefreshControl.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_RunMethod
            // 
            this.mnuVariables_AddControlAction_RunMethod.Name = "mnuVariables_AddControlAction_RunMethod";
            this.mnuVariables_AddControlAction_RunMethod.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_RunMethod.Tag = "RunMethod";
            this.mnuVariables_AddControlAction_RunMethod.Text = "RunMethod";
            this.mnuVariables_AddControlAction_RunMethod.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_SetDefaultValue
            // 
            this.mnuVariables_AddControlAction_SetDefaultValue.Name = "mnuVariables_AddControlAction_SetDefaultValue";
            this.mnuVariables_AddControlAction_SetDefaultValue.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_SetDefaultValue.Tag = "SetDefaultValue";
            this.mnuVariables_AddControlAction_SetDefaultValue.Text = "SetDefaultValue";
            this.mnuVariables_AddControlAction_SetDefaultValue.Visible = false;
            this.mnuVariables_AddControlAction_SetDefaultValue.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_SetFocus
            // 
            this.mnuVariables_AddControlAction_SetFocus.Name = "mnuVariables_AddControlAction_SetFocus";
            this.mnuVariables_AddControlAction_SetFocus.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_SetFocus.Tag = "SetFocus";
            this.mnuVariables_AddControlAction_SetFocus.Text = "SetFocus";
            this.mnuVariables_AddControlAction_SetFocus.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_SetVariableValue
            // 
            this.mnuVariables_AddControlAction_SetVariableValue.Name = "mnuVariables_AddControlAction_SetVariableValue";
            this.mnuVariables_AddControlAction_SetVariableValue.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_SetVariableValue.Tag = "SetVariableValue";
            this.mnuVariables_AddControlAction_SetVariableValue.Text = "SetVariableValue";
            this.mnuVariables_AddControlAction_SetVariableValue.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuVariables_AddControlAction_Visible
            // 
            this.mnuVariables_AddControlAction_Visible.Name = "mnuVariables_AddControlAction_Visible";
            this.mnuVariables_AddControlAction_Visible.Size = new System.Drawing.Size(159, 22);
            this.mnuVariables_AddControlAction_Visible.Tag = "Visible";
            this.mnuVariables_AddControlAction_Visible.Text = "Visible";
            this.mnuVariables_AddControlAction_Visible.Click += new System.EventHandler(this.mnuVariableActions_OnVAClick);
            // 
            // mnuAnswerFileSegments
            // 
            this.mnuAnswerFileSegments.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAnswerFileSegments_Add,
            this.mnuAnswerFileSegments_Delete,
            this.mnuAnswerFileSegments_MoveUp,
            this.mnuAnswerFileSegments_MoveDown});
            this.mnuAnswerFileSegments.Name = "mnuVariableActions";
            this.mnuAnswerFileSegments.Size = new System.Drawing.Size(162, 92);
            this.mnuAnswerFileSegments.TimerInterval = 500;
            this.mnuAnswerFileSegments.UseTimer = true;
            this.mnuAnswerFileSegments.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAnswerFileSegments_Opening);
            // 
            // mnuAnswerFileSegments_Add
            // 
            this.mnuAnswerFileSegments_Add.Name = "mnuAnswerFileSegments_Add";
            this.mnuAnswerFileSegments_Add.Size = new System.Drawing.Size(161, 22);
            this.mnuAnswerFileSegments_Add.Text = "#Add Segment#";
            this.mnuAnswerFileSegments_Add.Click += new System.EventHandler(this.mnuAnswerFileSegments_Add_Click);
            // 
            // mnuAnswerFileSegments_Delete
            // 
            this.mnuAnswerFileSegments_Delete.Name = "mnuAnswerFileSegments_Delete";
            this.mnuAnswerFileSegments_Delete.Size = new System.Drawing.Size(161, 22);
            this.mnuAnswerFileSegments_Delete.Text = "#Delete#";
            this.mnuAnswerFileSegments_Delete.Click += new System.EventHandler(this.mnuAnswerFileSegments_Delete_Click);
            // 
            // mnuAnswerFileSegments_MoveUp
            // 
            this.mnuAnswerFileSegments_MoveUp.Name = "mnuAnswerFileSegments_MoveUp";
            this.mnuAnswerFileSegments_MoveUp.Size = new System.Drawing.Size(161, 22);
            this.mnuAnswerFileSegments_MoveUp.Text = "#&Move Up...#";
            this.mnuAnswerFileSegments_MoveUp.Click += new System.EventHandler(this.mnuAnswerFileSegments_MoveUp_Click);
            // 
            // mnuAnswerFileSegments_MoveDown
            // 
            this.mnuAnswerFileSegments_MoveDown.Name = "mnuAnswerFileSegments_MoveDown";
            this.mnuAnswerFileSegments_MoveDown.Size = new System.Drawing.Size(161, 22);
            this.mnuAnswerFileSegments_MoveDown.Text = "#Move &Down...#";
            this.mnuAnswerFileSegments_MoveDown.Click += new System.EventHandler(this.mnuAnswerFileSegments_MoveDown_Click);
            // 
            // DocumentDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.splitContents);
            this.Controls.Add(this.pnlDocContents);
            this.Controls.Add(this.pnlDescription);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DocumentDesigner";
            this.Size = new System.Drawing.Size(328, 528);
            this.Resize += new System.EventHandler(this.DocumentDesigner_Resize);
            this.pnlDocContents.ResumeLayout(false);
            this.pnlDocContents.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPrefs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMenuDropdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPreview)).EndInit();
            this.pnlDescription.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeDocContents)).EndInit();
            this.mnuVariables.ResumeLayout(false);
            this.mnuVariables_AddAction.ResumeLayout(false);
            this.mnuSegments.ResumeLayout(false);
            this.mnuBlocks.ResumeLayout(false);
            this.mnuVariables_AddControlAction.ResumeLayout(false);
            this.mnuAnswerFileSegments.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private TimedContextMenuStrip mnuVariables;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_Add;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_MoveUp;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_MoveDown;
        private TimedContextMenuStrip mnuSegments;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Add;
        //private System.Windows.Forms.ToolStripMenuItem mnuSegments_Delete1;
        //private System.Windows.Forms.ToolStripMenuItem mnuSegments_DeleteAll;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_MoveUp;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_MoveDown;
        private System.Windows.Forms.ToolStripSeparator mnuVariables_Separator1;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_Refresh;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Refresh;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Copy;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Paste;
        private System.Windows.Forms.ToolStripSeparator mnuSegments_Separator1;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Rename;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_Delete;
        private System.Windows.Forms.ToolStripSeparator mnuVariables_Separator2;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_Rename;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_Delete;
        private TimedContextMenuStrip mnuBlocks;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_Copy;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_Paste;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_Delete;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_Rename;
        private System.Windows.Forms.ToolStripSeparator mnuBlocks_Separator1;
        private System.Windows.Forms.ToolStripMenuItem mnuBlocks_Refresh;
        #endregion
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddTaglessVariable;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertReline;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_CreateNewChildSegment;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_DeleteSegment;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertSegment;
        private TimedContextMenuStrip mnuVariables_AddControlAction;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_Enable;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_SetVariableValue;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_EndExecution;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_Visible;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_ChangeLabel;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_RunMethod;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_SetFocus;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_SetDefaultValue;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertDetailIntoTable;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_SetupLabelTable;
        //private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertTOA;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_SetupDistributedDetailTable;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_SetupDistributedSegmentSections;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_DisplayMessage;
        private System.Windows.Forms.ToolTip ttStatus;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Splitter splitContents;
        private System.Windows.Forms.PictureBox picHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_InsertDate;
        private TimedContextMenuStrip mnuAnswerFileSegments;
        private System.Windows.Forms.ToolStripMenuItem mnuAnswerFileSegments_Add;
        private System.Windows.Forms.ToolStripMenuItem mnuAnswerFileSegments_Delete;
        private System.Windows.Forms.ToolStripMenuItem mnuAnswerFileSegments_MoveUp;
        private System.Windows.Forms.ToolStripMenuItem mnuAnswerFileSegments_MoveDown;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_TrailerAssignments;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_DraftStampAssignments;
        private System.Windows.Forms.PictureBox picMenuDropdown;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddControlAction_RefreshControl;
        private System.Windows.Forms.ToolStripMenuItem mnuVariables_AddAction_ApplyStyleSheet;
        private System.Windows.Forms.PictureBox picSave;
        private System.Windows.Forms.PictureBox picClose;
        private System.Windows.Forms.ToolStripMenuItem mnuSegments_ParentAssignments;
        private System.Windows.Forms.PictureBox picPrefs;
    }
}