﻿namespace LMP.MacPac
{
    partial class TrailerDisplayOptionsManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;


        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTrailerDisplayOptions = new System.Windows.Forms.Label();
            this.trailerDisplayOptionsComboBox1 = new LMP.Controls.TrailerDisplayOptionsComboBox();
            this.SuspendLayout();
            // 
            // lblTrailerDisplayOptions
            // 
            this.lblTrailerDisplayOptions.AutoSize = true;
            this.lblTrailerDisplayOptions.Location = new System.Drawing.Point(13, 16);
            this.lblTrailerDisplayOptions.Name = "lblTrailerDisplayOptions";
            this.lblTrailerDisplayOptions.Size = new System.Drawing.Size(133, 15);
            this.lblTrailerDisplayOptions.TabIndex = 0;
            this.lblTrailerDisplayOptions.Text = "#Trailer Display Options:#";
            // 
            // trailerDisplayOptionsComboBox1
            // 
            this.trailerDisplayOptionsComboBox1.AllowEmptyValue = false;
            this.trailerDisplayOptionsComboBox1.Borderless = false;
            this.trailerDisplayOptionsComboBox1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trailerDisplayOptionsComboBox1.IsDirty = true;
            this.trailerDisplayOptionsComboBox1.LimitToList = true;
            this.trailerDisplayOptionsComboBox1.ListName = "";
            this.trailerDisplayOptionsComboBox1.Location = new System.Drawing.Point(16, 34);
            this.trailerDisplayOptionsComboBox1.MaxDropDownItems = 8;
            this.trailerDisplayOptionsComboBox1.Name = "trailerDisplayOptionsComboBox1";
            this.trailerDisplayOptionsComboBox1.SelectedIndex = 0;
            this.trailerDisplayOptionsComboBox1.SelectedValue = "0";
            this.trailerDisplayOptionsComboBox1.SelectionLength = 0;
            this.trailerDisplayOptionsComboBox1.SelectionStart = 0;
            this.trailerDisplayOptionsComboBox1.Size = new System.Drawing.Size(288, 23);
            this.trailerDisplayOptionsComboBox1.SupportingValues = "";
            this.trailerDisplayOptionsComboBox1.TabIndex = 1;
            this.trailerDisplayOptionsComboBox1.Tag2 = null;
            this.trailerDisplayOptionsComboBox1.Value = "0";
            this.trailerDisplayOptionsComboBox1.ValueChanged += new LMP.Controls.ValueChangedHandler(this.trailerDisplayOptionsComboBox1_ValueChanged);
            // 
            // TrailerDisplayOptionsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.trailerDisplayOptionsComboBox1);
            this.Controls.Add(this.lblTrailerDisplayOptions);
            this.Name = "TrailerDisplayOptionsManager";
            this.Size = new System.Drawing.Size(379, 436);
            this.Load += new System.EventHandler(this.TrailerDisplayOptionsManager_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTrailerDisplayOptions;
        private LMP.Controls.TrailerDisplayOptionsComboBox trailerDisplayOptionsComboBox1;
    }
}
