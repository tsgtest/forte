using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace LMP.MacPac
{
    public partial class AttachToTemplateSelector : Form
    {
        string m_xCurrentDirectory = "";
        string m_xTemplatePath = "";

        public AttachToTemplateSelector()
        {
            InitializeComponent();
        }
        public string TemplatePath
        {
            get
            {
                return m_xTemplatePath;
            }
        }
        /// <summary>
        /// Form is loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachToTemplateSelector_Load(object sender, EventArgs e)
        {
            try
            {
                m_xCurrentDirectory = LMP.Data.Application.TemplatesDirectory;
                lblPath.Text = "Templates in " + m_xCurrentDirectory + ":";
                string[] xFiles = Directory.GetFiles(m_xCurrentDirectory, "*.dot?", SearchOption.AllDirectories);
                Array.Sort(xFiles);
                foreach (string xPath in xFiles)
                {
                    string xName = Path.GetFileName(xPath);
                    //Ignore temp files in directory
                    if (!xName.StartsWith("~"))
                    {
                        ListViewItem oNextItem = this.lvTemplates.Items.Add(xName, "WordTemplate");
                        oNextItem.Tag = xPath;
                    }
                }
                this.lvTemplates.View = View.LargeIcon;
                this.lvTemplates.ArrangeIcons();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (lvTemplates.SelectedItems.Count == 0)
                {
                    MessageBox.Show("No Template is selected", LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                else
                {
                    m_xTemplatePath = lvTemplates.SelectedItems[0].Tag.ToString();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Activated item by double-clicking - simulate OK button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvTemplates_ItemActivate(object sender, EventArgs e)
        {
            btnOK_Click(sender, e);
        }
    }
}