using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Infragistics.Win.UltraWinTree;
using LMP.Data;



namespace LMP.MacPac
{
    public partial class ExternalContentForm : Form
    {
        #region *****************fields*************************
        private DialogMode m_iMode;
        string m_xDefaultDisplayName = "";
        #endregion

        #region *****************enumerations*******************
        public enum DialogMode
        {
            NewExternalContent = 1,
            EditProperties = 2
        }
        #endregion

        #region *****************properties*********************
        internal DialogMode Mode
        {
            get { return m_iMode;}
            set { m_iMode = value; }
        }
        #endregion

        public ExternalContentForm()
        {
            InitializeComponent();

            //GLOG #8492 - dcf
            this.txtHelp.Height = this.btnCancel.Top + this.btnCancel.Height - this.txtHelp.Top;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                BrowseForFile();
            }
            catch(Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void BrowseForFile()
        {
            OpenFileDialog oDlg = new OpenFileDialog();

            oDlg.Filter = WordVersionExtension;
            
            if (oDlg.ShowDialog() == DialogResult.OK)
            {
                FullFileName = oDlg.FileName;

                if(this.IsusingDefaultDisplayName)
                {
                    DisplayName = this.GetDefaultDisplayName(FullFileName);
                }

                this.txtDisplayName.SelectAll();
                this.txtDisplayName.Focus();

                m_xDefaultDisplayName = DisplayName;
            }
        }

        private bool IsusingDefaultDisplayName
        {
            // If the DisplayName is not set or if the user modifies the 
            // DisplayName automatically provided based on the fully qualified
            // filename, DO NOT provide a default display name, ie return false.
            get
            {
                return (m_xDefaultDisplayName == DisplayName);
            }
        }
                
        private string GetDefaultDisplayName(string xFullFileName)
        {
            // The default display name is the fully qualified filename minus the path.
            return xFullFileName.Substring(xFullFileName.LastIndexOf(Path.DirectorySeparatorChar) + 1);
        }

        public string FullFileName
        {
            get
            {
                return this.txtFullFileName.Text;
            }

            set
            {
                this.txtFullFileName.Text = value;
            }
        }

        public string DisplayName
        {
            get
            {
                return this.txtDisplayName.Text;
            }

            set
            {
                this.txtDisplayName.Text = value;
            }
        }

        public string Help
        {
            get
            {
                return this.txtHelp.Text;
            }

            set
            {
                this.txtHelp.Text = value;
            }
        }
        
        private string WordVersionExtension
        {
            get
            {
                return LMP.Resources.GetLangString("AddFileForm_PromptTemplates") + " (.dot, .dotx, .dotm)|*.dot*|" + 
                    LMP.Resources.GetLangString("AddFileForm_PromptAllFiles") + " (*.*)|*.*";                
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (File.Exists(FullFileName))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("The file " + FullFileName + " does not exist.", "Cannot Add File");
            }
        }

        private void txtFullFileName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // Only enable the OK button if an entry is provided for the filename and the display name.
                this.btnOK.Enabled = (this.txtFullFileName.Text.Length > 0 && this.txtDisplayName.Text.Length > 0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void txtDisplayName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // Only enable the OK button if an entry is provided for the filename and the display name.
                this.btnOK.Enabled = (this.txtFullFileName.Text.Length > 0 && this.txtDisplayName.Text.Length > 0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        #region **********************methods*******************
        /// <summary>
        /// Setup and Display form and set whether in Segment Creation or Edit Properties mode
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="iMode"></param>
        /// <param name="oSegment"></param>
        public DialogResult ShowForm(DialogMode iMode)
        {
            return ShowForm(iMode, null);
        }
        public DialogResult ShowForm(DialogMode iMode, UltraTreeNode oNode)
        {
            try
            {
                // Only enable the OK button if an entry is 
                //provided for the filename and the display name.
                this.btnOK.Enabled = (this.txtFullFileName.Text.Length > 0 &&
                    this.txtDisplayName.Text.Length > 0);

                this.lblFullQualifiedFileName.Text = LMP.Resources.GetLangString("AddFileForm_lblFullyQualifiedFileName");
                this.lblDisplayName.Text = LMP.Resources.GetLangString("AddFileForm_lblDisplayName");
                this.lblHelp.Text = LMP.Resources.GetLangString("AddFileForm_lblHelp");
                this.Mode = iMode;

                switch (this.Mode)
                {
                    case DialogMode.EditProperties:
                        this.Text = LMP.Resources.GetLangString("EditFileForm_Title");

                        FolderMember oMember = (FolderMember)oNode.Tag;

                        AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.NonMacPacContent);
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oMember.ObjectID1);
                        this.txtDisplayName.Text = oDef.DisplayName;
                        string xHelpText = oDef.HelpText.Replace("\n", "\r\n");
                        xHelpText = xHelpText.Replace("_x000a_", "\r\n");
                        this.txtHelp.Text = xHelpText.Replace("<br>", "\r\n");
                        this.txtFullFileName.Text = oDef.XML;
                        break;
                    case DialogMode.NewExternalContent:
                        this.txtHelp.Clear();
                        this.Text = LMP.Resources.GetLangString("AddFileForm_Title");
                        break;
                }
                return this.ShowDialog();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UIException(
                    LMP.Resources.GetLangString("Error_External_Content_Initialize"), oE);
            }
#endregion
        }
    }
}