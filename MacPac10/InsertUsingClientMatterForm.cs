﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using LMP.Data;
using LMP.Grail;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class InsertUsingClientMatterForm : Form
    {
        #region***********************************enumerations***********************************
        public enum mpClientMatterLookupFormat
        {
            ClientMatter,
            CaseNumber
        }
        #endregion
        #region***********************************fields***********************************
        private short m_shLastLocation;
        private bool m_bFirstSegmentShown;
        private bool m_bDefinitionIsSegmentPacket;
        private Segment.InsertionLocations iDefLocation;
        private Segment.InsertionBehaviors iDefBehavior;
        private int iLocations;
        private mpObjectTypes iTypeID;
        private mpSegmentIntendedUses iIntendedUse;
        #endregion
        #region***********************************constructors***********************************
        public InsertUsingClientMatterForm(string xFilter, AdminSegmentDef oDef)
        {
            InitializeComponent();

            this.cmMain.MatterDoubleClicked += 
                new MatterDoubleClickedHandler(cmMain_MatterDoubleClicked);

            this.cmMain.ClientMatterChanged += 
                new ClientMatterChangedHandler(cmMain_ClientMatterChanged);

            if (!string.IsNullOrEmpty(xFilter))
            {
                this.cmMain.Filter = xFilter;
            }

            m_bDefinitionIsSegmentPacket = oDef.TypeID == mpObjectTypes.SegmentPacket;

            if (!m_bDefinitionIsSegmentPacket)
            {
                iDefLocation = (Segment.InsertionLocations)Enum.Parse(typeof(
                    Segment.InsertionLocations), oDef.DefaultDoubleClickLocation.ToString());
                iDefBehavior = (Segment.InsertionBehaviors)Enum.Parse(typeof(
                    Segment.InsertionBehaviors), oDef.DefaultDoubleClickBehavior.ToString());
                iLocations = oDef.MenuInsertionOptions;
                iTypeID = oDef.TypeID;
                iIntendedUse = oDef.IntendedUse;
            }
        }
        public InsertUsingClientMatterForm(string xFilter, UserSegmentDef oDef)
        {
            InitializeComponent();

            this.cmMain.MatterDoubleClicked += 
                new MatterDoubleClickedHandler(cmMain_MatterDoubleClicked);

            this.cmMain.ClientMatterChanged +=
                new ClientMatterChangedHandler(cmMain_ClientMatterChanged);

            m_bDefinitionIsSegmentPacket = oDef.TypeID == mpObjectTypes.SegmentPacket;

            if (!string.IsNullOrEmpty(xFilter))
            {
                this.cmMain.Filter = xFilter;
            }
        }

        void cmMain_ClientMatterChanged(object sender, ClientMatterChangedEventArgs e)
        {
            try
            {
                this.btnOK.Enabled = e.Matter != null;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public InsertUsingClientMatterForm(string xFilter)
        {
            InitializeComponent();

            this.cmMain.MatterDoubleClicked +=
                new MatterDoubleClickedHandler(cmMain_MatterDoubleClicked);

            this.cmMain.ClientMatterChanged +=
                new ClientMatterChangedHandler(cmMain_ClientMatterChanged);

            m_bDefinitionIsSegmentPacket = true;

            if (!string.IsNullOrEmpty(xFilter))
            {
                this.cmMain.Filter = xFilter;
            }
        }
        #endregion
        #region ***********************************properties***********************************
        public Segment.InsertionLocations InsertionLocation
        {
            get
            {
                if (string.IsNullOrEmpty(this.cmbLocation.Value))
                {
                    return Segment.InsertionLocations.Default;
                }
                else
                {
                    return (Segment.InsertionLocations)Enum.Parse(
                    typeof(Segment.InsertionLocations), this.cmbLocation.Value);
                }
            }
            set { this.cmbLocation.Value = ((int)value).ToString(); }
        }
        public Segment.InsertionBehaviors InsertBehaviors
        {
            get
            {
                int iBeh = 0;
                if (this.chkKeepExisting.Checked)
                    iBeh = 1;

                if (this.chkRestartNumbering.Checked)
                    iBeh += 2;

                if (this.chkSeparateSection.Checked)
                    iBeh += 4;

                return (Segment.InsertionBehaviors)iBeh;
            }
            set
            {
                int iValue = (int)value;
                this.chkKeepExisting.Checked = (iValue & 1) == 1;
                this.chkRestartNumbering.Checked = (iValue & 2) == 2;
                this.chkSeparateSection.Checked = (iValue & 4) == 4;
            }
        }
        public Client Client
        {
            get
            {
                return this.cmMain.Client;
            }
        }
        public Matter Matter
        {
            get
            {
                return this.cmMain.Matter; ;
            }
        }
        #endregion
        #region************************************methods***********************************
        /// <summary>
        /// refreshes the dropdown items listed in the insertion locations dropdown
        /// </summary>
        /// <param name="iLocations"></param>
        private void RefreshInsertionLocationDropdown(int iLocations)
        {
            string[] oItems = GetInsertionOptionsFromBitwiseInt(iLocations);
            this.cmbLocation.Items.Clear();

            foreach (string oItem in oItems)
            {
                this.cmbLocation.Items.Add(oItem);
            }
        }
        /// <summary>
        /// returns a string array of the specified available locations
        /// </summary>
        /// <param name="iOptions"></param>
        /// <returns></returns>
        private string[] GetInsertionOptionsFromBitwiseInt(int iOptions)
        {
            string xOptions = "";

            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfCurrentSection) == (int)Segment.InsertionLocations.InsertAtStartOfCurrentSection)
            //    xOptions = "Start of Current Section|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfCurrentSection) == (int)Segment.InsertionLocations.InsertAtEndOfCurrentSection)
            //    xOptions = xOptions + "End of Current Section|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfAllSections) == (int)Segment.InsertionLocations.InsertAtStartOfAllSections)
            //    xOptions = xOptions + "Start of All Sections|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfAllSections) == (int)Segment.InsertionLocations.InsertAtEndOfAllSections)
            //    xOptions = xOptions + "End of All Sections|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtSelection) == (int)Segment.InsertionLocations.InsertAtSelection)
            //    xOptions = xOptions + "Insertion Point|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtStartOfDocument) == (int)Segment.InsertionLocations.InsertAtStartOfDocument)
            //    xOptions = xOptions + "Start of Document|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertAtEndOfDocument) == (int)Segment.InsertionLocations.InsertAtEndOfDocument)
            //    xOptions = xOptions + "End of Document|";
            //if ((iOptions & (int)Segment.InsertionLocations.InsertInNewDocument) == (int)Segment.InsertionLocations.InsertInNewDocument)
            //    xOptions = xOptions + "New Document|";
            //if ((iOptions & (int)Segment.InsertionLocations.Default) == (int)Segment.InsertionLocations.Default)
            //    xOptions = xOptions + "Default|";

            return xOptions.TrimEnd('|').Split('|');
        }
        /// <summary>
        /// Setup Display options for current Segment
        /// </summary>
        private void LoadSegmentOptions()
        {
            //GLOG 3983: Save currently selected location so it can be preserved when Segment changes
            short shCurLocation = 0;
            if (this.cmbLocation.SelectedValue != null)
                shCurLocation = short.Parse(this.cmbLocation.SelectedValue.ToString());
            ArrayList oListItems = new ArrayList();

            //get array of insertion locations
            object[,] aDefaultLocations = new object[,]{
            {"Start of Current Section",1}, 
            {"End of Current Section",2}, 
            {"Start of All Sections",4}, 
            {"End of All Sections",8}, 
            {"Insertion Point",16}, 
            {"Start of Document",32}, 
            {"End of Document",64},
            {"New Document",128},
            {"Default Location", 256}};

            object[,] aBehaviors = new object[,]{
            {"Insert In Separate Section",1}, 
            {"Insert In Separate Continuous Section",8}, 
            {"Keep Existing Headers Footers",2}, 
            {"Restart Page Numbering",4}};

            for (int i = aDefaultLocations.GetUpperBound(0); i >= 0; i--)
            {
                if ((iLocations &
                    (short)(int)aDefaultLocations[i, 1]) == (short)(int)aDefaultLocations[i, 1])
                {
                    oListItems.Add(new object[] { aDefaultLocations[i, 1], aDefaultLocations[i, 0] });
                }
            }

            if (oListItems.Count == 0)
            {
                cmbLocation.SetList(aDefaultLocations);
            }
            else
            {
                //GLOG 5723: Add options for Below TOC and Above TOA if appropriate
                if (iTypeID == mpObjectTypes.TOA || iTypeID == mpObjectTypes.PleadingExhibit)
                {
                    if (iTypeID != mpObjectTypes.TOA)
                    {
                        if (LMP.Forte.MSWord.WordDoc.TOAExists(LMP.Forte.MSWord.WordApp.ActiveDocument()))
                            oListItems.Add(new object[] { "1024", "Above TOA Section" });
                    }
                    if (LMP.Forte.MSWord.WordDoc.TOCExists(LMP.Forte.MSWord.WordApp.ActiveDocument()))
                    {
                        oListItems.Add(new object[] { "512", "Below TOC Section" });
                        if (iTypeID == mpObjectTypes.TOA)
                            //Use as default location for TOA
                            iDefLocation = Segment.InsertionLocations.InsertBelowTOC;
                    }

                }
                cmbLocation.SetList(oListItems);
            }

            //GLOG 3983: Retain previously selected location after initial display
            if (!m_bFirstSegmentShown)
            {
                // Select default double-click item in list
                cmbLocation.SelectedValue = iDefLocation;
                m_bFirstSegmentShown = true;
            }
            else
            {
                //Reloading List changed cmbLocation value, so restore saved Last location
                m_shLastLocation = shCurLocation;
                cmbLocation.SelectedValue = shCurLocation;
            }
            if (cmbLocation.SelectedIndex == -1)
                cmbLocation.SelectedIndex = 0;
        }
        private void EnableBehaviorOptions()
        {
            if (this.cmbLocation.SelectedValue == null)
                return;

            if (short.Parse(this.cmbLocation.SelectedValue.ToString()) != 128)
            {
                this.chkSeparateSection.Checked = (iIntendedUse == mpSegmentIntendedUses.AsDocument);
                this.chkKeepExisting.Checked = (iIntendedUse != mpSegmentIntendedUses.AsDocument);
                this.chkRestartNumbering.Checked = (iIntendedUse == mpSegmentIntendedUses.AsDocument);
            }
            else
            {
                //GLOG 3983: These items disabled for 'New Document' location
                this.chkSeparateSection.Enabled = false;
                this.chkRestartNumbering.Enabled = false;
                this.chkKeepExisting.Enabled = false;
            }

            if (iTypeID == mpObjectTypes.AgreementTitlePage)
            {
                this.chkSeparateSection.Enabled = false;
                this.chkKeepExisting.Enabled = false;
                this.chkRestartNumbering.Enabled = true;
            }
            if (iTypeID == mpObjectTypes.PleadingExhibit ||
                iTypeID == mpObjectTypes.AgreementExhibit)
            {
                //GLOG item #6720 - dcf - 04/22/13
                this.chkSeparateSection.Checked = true; //GLOG 6355
                this.chkKeepExisting.Checked = false;
                this.chkRestartNumbering.Checked = true;
            }
            else if (iTypeID == mpObjectTypes.TOA)
            {
                this.chkKeepExisting.Enabled = false;
                this.chkSeparateSection.Checked = true; //GLOG 6355
                this.chkRestartNumbering.Enabled = true;
            }
            else
            {
                //GLOG 3983: Enable options appropriate for current definition
                this.chkSeparateSection.Enabled = (((iDefBehavior &
                    Segment.InsertionBehaviors.InsertInSeparateNextPageSection) ==
                    Segment.InsertionBehaviors.InsertInSeparateNextPageSection) ||
                    ((iDefBehavior &
                    Segment.InsertionBehaviors.InsertInSeparateContinuousSection) ==
                    Segment.InsertionBehaviors.InsertInSeparateContinuousSection));

                //GLOG 3983: Set Separate Section checkbox only if changing from New Document location
                //Otherwise, maintain previously selected value
                if (this.chkSeparateSection.Enabled && m_shLastLocation == 128)
                {
                    this.chkSeparateSection.Checked = true;
                }

                this.chkRestartNumbering.Enabled = ((iDefBehavior &
                    Segment.InsertionBehaviors.RestartPageNumbering) ==
                    Segment.InsertionBehaviors.RestartPageNumbering);

                this.chkKeepExisting.Enabled = ((iDefBehavior &
                    Segment.InsertionBehaviors.KeepExistingHeadersFooters) ==
                    Segment.InsertionBehaviors.KeepExistingHeadersFooters);
            }
        }
        #endregion
        #region ***********************************event handlers***********************************
        private void InsertUsingClientMatterForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.cmbLocation.Visible = !this.m_bDefinitionIsSegmentPacket;
                this.lblLocation.Visible = !this.m_bDefinitionIsSegmentPacket;
                this.grpOptions.Visible = !this.m_bDefinitionIsSegmentPacket;

                if (this.m_bDefinitionIsSegmentPacket)
                {
                    this.Width = 337;
                }
                else
                {
                    LoadSegmentOptions();
                    EnableBehaviorOptions();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbLocation_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                EnableBehaviorOptions();
                //GLOG 3983: Save current location
                if (cmbLocation.SelectedValue != null)
                    m_shLastLocation = short.Parse(cmbLocation.SelectedValue.ToString());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        void cmMain_MatterDoubleClicked(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
        #endregion
    }
}