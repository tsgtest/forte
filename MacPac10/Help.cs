using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class Help : Form
    {
        public Help(string xContent)
        {
            try
            {
                InitializeComponent();
                TaskPane.SetHelpText(this.wbHelp, xContent);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public WebBrowser Browser
        {
            get { return wbHelp; }
        }
    }
}