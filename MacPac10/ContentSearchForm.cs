﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMP.Data;

//GLOG 8037
namespace LMP.MacPac
{
    public partial class ContentSearchForm : Form
    {
        private mpSearchContentType m_iType = mpSearchContentType.AllContent;
        private mpSegmentFindFields m_iField = mpSegmentFindFields.Name;
        private mpSearchLocations m_iLocation = mpSearchLocations.AllFolders;
        private int m_iOwnerID = 0;

        public enum mpSearchContentType
        {
            AllContent = 0,
            Segments = 1,
            SavedData = 2,
            StyleSheets = 3,
            MasterData = 4,
            SegmentPackets = 5
        }
        public enum mpSearchLocations
        {
            AllFolders = 0,
            SelectedFolder = 1,
            SelectedFolderAndSubfolders = 2
        }
        public ContentSearchForm()
        {
            InitializeComponent();

            try
            {
                //GLOG 8037
                this.cmbSearchField.SetList(new string[,] {{"Name Field", ((int)mpSegmentFindFields.Name).ToString()},
                                                            {"Help Text Field", ((int)mpSegmentFindFields.Description).ToString()},
                                                            {"Name and Help Text Fields", ((int)mpSegmentFindFields.Name | (int)mpSegmentFindFields.Description).ToString()},
                                                            {"Folder Name", ((int)mpSegmentFindFields.FolderName).ToString()}}); 
                this.cmbSearchLocation.SetList(new string[,] {{"All Folders", ((int)mpSearchLocations.AllFolders).ToString()}, 
                                                        {"Selected Folder", ((int)mpSearchLocations.SelectedFolder).ToString()},
                                                        {"Selected Folder and Subfolders", ((int)mpSearchLocations.SelectedFolderAndSubfolders).ToString()}});
                this.cmbContentType.SetList(new string[,] {{"All Types", ((int)mpSearchContentType.AllContent).ToString()},
                                                            {"Master Data Forms", ((int)mpSearchContentType.MasterData).ToString()},
                                                            {"Saved Data", ((int)mpSearchContentType.SavedData).ToString()},
                                                            {"Segment Packets", ((int)mpSearchContentType.SegmentPackets).ToString()},
                                                            {"Segments", ((int)mpSearchContentType.Segments).ToString()},
                                                            {"Style Sheets", ((int)mpSearchContentType.StyleSheets).ToString()}});
                
                LocalPersons oPersons = new LocalPersons(mpPeopleListTypes.AllActivePeopleInDatabase, "ID2=0 AND LinkedPersonID=0", UsageStates.Active);

                //retrieve array with DisplayName, ID fields
                Array aPersons = oPersons.ToArray(5, 0);
                //convert to string array for loading into combo
                string[,] oList = new string[aPersons.GetUpperBound(0)+2, 2];

                oList[0, 0] = "Any Owner";
                oList[0, 1] = "0";

                for (int i = 0; i < aPersons.GetLength(0); i++)
                {
                    oList[i+1, 0] = ((Array)aPersons.GetValue(i)).GetValue(0).ToString();
                    oList[i+1, 1] = ((Array)aPersons.GetValue(i)).GetValue(1).ToString();
                }

                //GLOG 8162: Load initial options from User Settings
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;
                this.cmbOwnedBy.SetList(oList);
                try
                {
                    this.cmbContentType.SelectedIndex = -1;
                    this.cmbContentType.Value = oSettings.SearchOptionsType;
                }
                finally
                {
                    if (this.cmbContentType.SelectedIndex == -1)
                        this.cmbContentType.Value = ((int)m_iType).ToString();
                }

                try
                {
                    this.cmbSearchField.SelectedIndex = -1;
                    this.cmbSearchField.Value = oSettings.SearchOptionsField;
                }
                finally
                {
                    if (this.cmbSearchField.SelectedIndex == -1)
                        this.cmbSearchField.Value = ((int)m_iField).ToString();
                }
                try
                {
                    this.cmbSearchLocation.SelectedIndex = -1;
                    this.cmbSearchLocation.Value = oSettings.SearchOptionsLocation;
                }
                finally
                {
                    if (this.cmbSearchLocation.SelectedIndex == -1)
                        this.cmbSearchLocation.Value = ((int)m_iLocation).ToString();
                }
                try
                {
                    this.cmbOwnedBy.SelectedIndex = -1;
                    this.cmbOwnedBy.Value = oSettings.SearchOptionsOwner;
                }
                finally
                {
                    if (this.cmbOwnedBy.SelectedIndex == -1)
                        this.cmbOwnedBy.Value = m_iOwnerID.ToString();
                }
                if (this.SearchText == "")
                {
                    this.SearchText = oSettings.SearchOptionsText;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        public string SearchText
        {
            get { return this.txtSearchText.Text; }
            set { this.txtSearchText.Text = value; }
        }
        public int OwnerID
        {
            get 
            { 
                int iOwner = 0;
                if (Int32.TryParse(cmbOwnedBy.Value, out iOwner))
                {
                    return iOwner;
                }
                else
                {
                    return 0;
                }
            }
        }
        public mpSearchContentType ContentType
        {
            get { return (mpSearchContentType)Int32.Parse( this.cmbContentType.Value); }
        }
        public mpSegmentFindFields SearchField
        {
            get { return (mpSegmentFindFields)Int32.Parse(this.cmbSearchField.Value); }
        }
        public mpSearchLocations SearchFolder
        {
            get { return (mpSearchLocations)Int32.Parse(this.cmbSearchLocation.Value); }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                //GLOG 8076: Keep displaying if no filter criteria selected
                if (this.SearchText == "" && this.SearchFolder == ContentSearchForm.mpSearchLocations.AllFolders &&
                    this.OwnerID == 0 && this.ContentType == ContentSearchForm.mpSearchContentType.AllContent)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_NoSearchCriteriaSelected"),
                        LMP.ComponentProperties.ProductName,
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = System.Windows.Forms.DialogResult.None;
                    this.txtSearchText.Focus();
                    return;
                }
                //Get User Settings
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

                oSettings.SearchOptionsText = this.SearchText;
                oSettings.SearchOptionsOwner = this.OwnerID.ToString();
                oSettings.SearchOptionsLocation = ((int)this.SearchFolder).ToString();
                oSettings.SearchOptionsField = ((int)this.SearchField).ToString();
                oSettings.SearchOptionsType = ((int)this.ContentType).ToString();
            }
            catch { }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtSearchText.Text = "";
                this.cmbContentType.Value = ((int)mpSearchContentType.AllContent).ToString();
                this.cmbSearchLocation.Value = ((int)mpSearchLocations.AllFolders).ToString();
                this.cmbSearchField.Value = ((int)mpSegmentFindFields.Name).ToString();
                this.cmbOwnedBy.Value = "0";
                this.txtSearchText.Focus();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void cmbSearchField_TabPressed(object sender, Controls.TabPressedEventArgs e)
        {

        }

        private void cmbSearchField_KeyPressed(object sender, KeyEventArgs e)
        {

        }

        private void ContentSearchForm_Load(object sender, EventArgs e)
        {

        }

        private void cmbSearchField_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void cmbSearchField_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }

        private void ContentSearchForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, false, true);
                e.Handled = true;
            }

        }

        private void ContentSearchForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\t')
            {
                this.SelectNextControl(this.ActiveControl, true, true, false, true);
                e.Handled = true;
            }
        }
    }
}
