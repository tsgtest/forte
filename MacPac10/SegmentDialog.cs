using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using LMP.Architect.Api;
using LMP.Data;
using LMP.Controls;
using Infragistics.Win;
using Infragistics.Win.UltraWinTabControl;

namespace LMP.MacPac
{
    public partial class SegmentDialog : Form
    {

        #region *********************fields*********************
        private AdminSegment m_oSegment = null;
        private ForteDocument m_oMPDocument = null;
        private VariableSetDef m_oVarSet = null;
        private Prefill m_oPrefill = null;
        private ValueAssignedEventHandler m_oValueAssignedHandler;
        #endregion
        #region *********************constructors*********************
        public SegmentDialog(AdminSegment oSegment, VariableSetDef oVariableSet)
        {
            InitializeComponent();
            m_oSegment = oSegment;
            m_oVarSet = oVariableSet;
            m_oMPDocument = oSegment.ForteDocument;

            if (m_oVarSet != null)
                //GLOG 3475
                m_oPrefill = new Prefill(m_oVarSet.ContentString, m_oVarSet.Name, m_oSegment.ID);

            this.btnCancel.Text = LMP.Resources.GetLangString("Lbl_Cancel");
            this.btnFinish.Text = LMP.Resources.GetLangString("Lbl_Finish");
            this.btnFinish.Visible = m_oVarSet == null;
            this.btnSave.Text = LMP.Resources.GetLangString("Lbl_Save...");

            if (!string.IsNullOrEmpty(m_oSegment.DialogCaption))
                this.Text = m_oSegment.DialogCaption;
            else
                this.Text = m_oSegment.DisplayName;

            m_oValueAssignedHandler = new ValueAssignedEventHandler(OnVariableValueAssigned);
        }
        #endregion
        #region *********************methods*********************
        public new DialogResult ShowDialog()
        {
            return this.ShowDialog(null);
        }
        public new DialogResult ShowDialog(IWin32Window oOwner)
        {
            //TODO: for now, only used for Answer Files
            if (m_oSegment == null || (m_oSegment.IntendedUse != LMP.Data.mpSegmentIntendedUses.AsAnswerFile &&
                !((AdminSegment)m_oSegment).ShowSegmentDialog) || m_oSegment.Variables.Count == 0)
                // Not appropriate for this segment
                return DialogResult.Abort;

            ConfigureTabs();
            PopulateTabs();
            this.tabPages.TabIndex = 0;
            return base.ShowDialog(oOwner);
        }
        /// <summary>
        /// Creates the required number of tabs to be displayed
        /// </summary>
        private void ConfigureTabs()
        {
            this.tabPages.BeginUpdate();
            string[] aCaptions = new string[] { "Main" };
            int iTabCount = 1;
            if (!string.IsNullOrEmpty(m_oSegment.DialogTabCaptions))
            {
                aCaptions = m_oSegment.DialogTabCaptions.Split(';');
                iTabCount = aCaptions.GetLength(0);
            }
            for (int i = 0; i < iTabCount; i++)
            {
                UltraTab oNewTab = null;
                if (i > 0)
                    oNewTab = this.tabPages.Tabs.Add();
                else
                    oNewTab = this.tabPages.Tabs[0];
                oNewTab.Text = aCaptions[i];
            }
            //Remove any extra tabs
            for (int i = this.tabPages.Tabs.Count; i > iTabCount; i--)
            {
                this.tabPages.Tabs.RemoveAt(i - 1);
            }
            for (int i = 0; i < this.tabPages.Tabs.Count; i++)
                this.tabPages.Tabs[i].TabPage.Controls.Clear();
            this.tabPages.EndUpdate();
        }
        /// <summary>
        /// Creates a control for each Segment Variable,
        /// placed on the appropriate tab
        /// </summary>
        private void PopulateTabs()
        {
            this.tabPages.BeginUpdate();
            const int iLeftPosLabel = 5;
            const int iLeftPosControl = 5;
            const int iLabelHeight = 13;
            const int iSpacingDist = 20;
            const int iInitialPos = 15;
            int iLabelWidth;
            int iCurTab = 0;
            int iTabCount = this.tabPages.Tabs.Count;
            bool bControlSkipped = false;
            //Create array to track current position on each tab
            int [] iVPos = new int[iTabCount];
            int[] iTabIndex = new int[iTabCount];
            for (int i = 0; i < iVPos.GetLength(0); i++)
            {
                iVPos[i] = iInitialPos;
                iTabIndex[i] = 0;

            }
            int iMaxHeight = Screen.PrimaryScreen.Bounds.Height - 50;
            int iCtlWidth = this.tabPages.Width - 24;
            iLabelWidth = iCtlWidth;
            if (m_oSegment.MaxAuthors > 0)
            {
                //Configure and display AuthorSelector
                IControl oICtl = null;
                Control oLbl = null;
                Control oCtl = null;
                int iPos = iVPos[iCurTab];
                AuthorSelector oAuthorSelector = new AuthorSelector();
                oICtl = (IControl)oAuthorSelector;
                oCtl = (Control)oAuthorSelector;

                oAuthorSelector.ShowUserPeopleManagerRequested +=
                    new AuthorSelector.UpdatePeopleListEventHandler(oAuthorSelector_ShowUserPeopleManagerRequested);
                oAuthorSelector.MaxAuthors = (short)m_oSegment.MaxAuthors;
                SetAuthorControlProperties(oAuthorSelector, m_oSegment.Authors);

                oAuthorSelector.ExecuteFinalSetup();

                //Create display label
                oLbl = new Label();
                ((Label)oLbl).AutoSize = false;
                ((Label)oLbl).TextAlign = ContentAlignment.TopLeft;
                oLbl.SetBounds(iLeftPosLabel + 6, iPos, iLabelWidth, iLabelHeight);
                oLbl.Text = LMP.String.HotKeyString(m_oSegment.AuthorsNodeUILabel + ((m_oSegment.MaxAuthors > 1) ? "s:" : ":"), "U");
                oLbl.BackColor = Color.Transparent;
                oLbl.TabIndex = iTabIndex[iCurTab]++;
                oLbl.Height = 15;

                //size control to panel
                oCtl.SetBounds(iLeftPosControl + 6, iPos + iLabelHeight + 1, iCtlWidth, oCtl.Height);
                oCtl.TabIndex = iTabIndex[iCurTab]++;
                oCtl.LostFocus += new EventHandler(oICtl_LostFocus);
                oCtl.GotFocus += new EventHandler(oICtl_GotFocus);
                oICtl.KeyReleased += new KeyEventHandler(OnKeyReleased);
                //GLOG 2966: Load Authors from saved data
                if (m_oPrefill != null && m_oPrefill.Authors.Count > 0)
                    m_oSegment.Authors.XML = m_oPrefill.AuthorsXML(m_oSegment.MaxAuthors);
                oICtl.Value = m_oSegment.Authors.XML;
                oCtl.Tag = m_oSegment.Authors;
                this.tabPages.Tabs[iCurTab].TabPage.Controls.Add(oLbl);
                this.tabPages.Tabs[iCurTab].TabPage.Controls.Add(oCtl);
                iVPos[iCurTab] = oCtl.Top + oCtl.Height + iSpacingDist;
            }
            for (int i = 0; i < m_oSegment.Variables.Count; i++)
            {
                Variable oVar = m_oSegment.Variables.ItemFromIndex(i);
                if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) == Variable.ControlHosts.DocumentEditor)
                {
                    oVar.ValueAssigned += new ValueAssignedEventHandler(m_oValueAssignedHandler);

                    IControl oICtl = null;
                    Control oLbl = null;
                    iCurTab = Math.Max(oVar.TabNumber, 1) - 1;
                    if (iCurTab < iTabCount)
                    {
                        int iPos = iVPos[iCurTab];
                        oICtl = oVar.CreateAssociatedControl(this.tabPages.Tabs[iCurTab].TabPage);
                        Control oCtl = (Control)oICtl;
                        oCtl.Name = oVar.Name + "Control";

                        if ((iPos + iLabelHeight + 1 + oCtl.Height) > iMaxHeight)
                        {
                            //Dialog is already at maximum height
                            bControlSkipped = true;
                            continue;
                        }
                        //Create display label
                        oLbl = new Label();
                        ((Label)oLbl).AutoSize = false;
                        ((Label)oLbl).TextAlign = ContentAlignment.TopLeft;
                        oLbl.SetBounds(iLeftPosLabel + 6, iPos, iLabelWidth, iLabelHeight);
                        oLbl.Text = LMP.String.HotKeyString(oVar.DisplayName + ":", oVar.Hotkey);
                        oLbl.BackColor = Color.Transparent;
                        oLbl.TabIndex = iTabIndex[iCurTab]++;
                        oLbl.Height = 15;

                        //size control to panel
                        oCtl.SetBounds(iLeftPosControl + 6, iPos + iLabelHeight + 5, iCtlWidth, oCtl.Height);
                        oCtl.TabIndex = iTabIndex[iCurTab]++;
                        oCtl.LostFocus += new EventHandler(oICtl_LostFocus);
                        oCtl.GotFocus += new EventHandler(oICtl_GotFocus);
                        oICtl.KeyReleased += new KeyEventHandler(OnKeyReleased);
                        //GLOG 2966: Load Prefill value if it exists
                        if (m_oPrefill != null && m_oPrefill[oVar.Name] != null)
                            oVar.SetValue(m_oPrefill[oVar.Name], false);
                        oICtl.Value = oVar.Value;

                        oCtl.Tag = oVar;
                        if (oICtl is ICIable)
                        {
                            ((ICIable)oICtl).CIRequested += new CIRequestedHandler(GetContacts);
                            ((Control)oICtl).DoubleClick += new EventHandler(OnControlDoubleClick);
                        }
                        this.tabPages.Tabs[iCurTab].TabPage.Controls.Add(oLbl);
                        this.tabPages.Tabs[iCurTab].TabPage.Controls.Add(oCtl);
                        iVPos[iCurTab] = oCtl.Top + oCtl.Height + iSpacingDist;
                    }
                }
            }
            int iTabHeight = 200;
            foreach (int i in iVPos)
            {
                //Check for maximum required height
                if (i + 15 > iTabHeight)
                    iTabHeight = i + 15;
            }
            //Get difference between required and actual heights
            int iHeightDiff = this.tabPages.Height - iTabHeight;
            //adjust form height
            this.Height = this.Height - iHeightDiff;
            this.tabPages.EndUpdate();
            if (bControlSkipped)
                MessageBox.Show(LMP.Resources.GetLangString("Dialog_SegmentDialog_TooManyControls"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        void OnControlDoubleClick(object sender, EventArgs e)
        {
            try
            {
                GetContacts(sender, new EventArgs());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// called when the author control requests the User People Manager -
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oAuthorSelector_ShowUserPeopleManagerRequested(Control oControl, ref bool bUseCallBack)
        {
            try
            {
                //setting the handler parameter will prevent the author selector from refreshing
                //the calling instance's people list twice
                bUseCallBack = true;

                //show the User People Manager
                PeopleManager oForm = new PeopleManager();
                oForm.ShowDialog();

                //use the static method to refresh people list in all instances of author selector
                Application.RefreshCurrentAuthorControls();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Set Properties for author control, stored in AuthorControlProperties property of Segment
        /// </summary>
        /// <param name="oControl"></param>
        /// <param name="xProperties"></param>
        private void SetAuthorControlProperties(AuthorSelector oAuthorControl, Authors oAuthors)
        {
            //set up control properties-
            //get control properties from variable
            string[] aProps = null;
            string xProperties = oAuthors.Segment.AuthorControlProperties;
            Control oControl = (Control)oAuthorControl;

            if (!System.String.IsNullOrEmpty(xProperties))
                aProps = xProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);

                //evaluate control prop value, which may be a MacPac expression
                xValue = Expression.Evaluate(xValue, oAuthors.Segment, oAuthors.Segment.ForteDocument);

                System.Type oCtlType = oControl.GetType();
                System.Reflection.PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType);

                oPropInfo.SetValue(oControl, oValue, null);
            }
        }
        /// <summary>
        /// Retrieve contacts for the Segment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetContacts(object sender, EventArgs e)
        {
            try
            {
                Control oCurCtl = sender as Control;
                if (oCurCtl != null  && (IControl)oCurCtl is ICIable)
                {
                    // Update current variable's value from control
                    if (((IControl)oCurCtl).IsDirty)
                    {
                        Variable oCurVar = oCurCtl.Tag as Variable;
                        oCurVar.SetValue(((IControl)oCurCtl).Value);
                    }
                }

                m_oSegment.GetContacts();
                for (int i = 0; i < m_oSegment.Variables.Count; i++)
                {
                    Variable oVar = m_oSegment.Variables[i];
                    // Update control value if it's already been created
                    if (oVar.AssociatedControl != null && oVar.AssociatedControl is ICIable)
                          oVar.AssociatedControl.Value = oVar.Value;
                }

            }
            finally
            {
                Session.CurrentWordApp.ScreenUpdating = true;
                Session.CurrentWordApp.ScreenRefresh();
                ////Need to release this, otherwise CI dialog is unresponsive on next access
                //LMP.Data.Application.CISession  = null;
            }
        }
        /// <summary>
        /// Returns Prefill object populated from dialog
        /// </summary>
        /// <returns></returns>
        public Prefill NewPrefill()
        {
            return m_oPrefill;
        }
        private bool ValidateSegment()
        {
            if (!m_oSegment.IsValid)
            {
                //TODO:  Before dialog can be used for non Answer File segments,
                //this will need to be modified to deal with child segments as well
                string xBadVars = "";
                for (int i = 0; i < m_oSegment.Variables.Count; i++)
                {
                    if (!m_oSegment.Variables[i].HasValidValue)
                    {
                        xBadVars = xBadVars + "\r\r� " + m_oSegment.Variables[i].Name;
                        if (m_oSegment.Variables[i].ValidationResponse != "")
                            xBadVars = xBadVars + ": " + m_oSegment.Variables[i].ValidationResponse;
                    }
                }
                MessageBox.Show(LMP.Resources.GetLangString("Dialog_SegmentDialog_InvalidValues") + xBadVars,
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// sets the color of the dialog
        /// </summary>
        private void SetBackgroundColor()
        {
            // Set the backcolor and gradient of the tree according to the user's prefs
            this.tabPages.Appearance.BackColor =
                Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;

            GradientStyle oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            //set the gradient according to the user's preference.
            this.tabPages.Appearance.BackGradientStyle = oGradient;
        }
        private void ExecuteControlActions(Variable oVariable, ControlActions.Events oEvent)
        {
            ExecuteControlActions(oVariable.ControlActions, oEvent);
        }
        private void ExecuteControlActions(ControlActions oActions, ControlActions.Events oEvent)
        {
            //cycle through actions in collection and execute
            for (int i = 0; i < oActions.Count; i++)
            {
                ControlAction oAction = oActions[i];

                //execute only if the action has beeen specified
                //to run on the specified event, or if
                //no event has been specified
                if (oEvent == 0 || oAction.Event == oEvent)
                {
                    //end execution if specified
                    if ((oAction.Type == ControlActions.Types.EndExecution) &&
                        (oAction.ExecutionIsSpecified()))
                        return;

                    try
                    {
                        ExecuteControlAction(oAction);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.ActionException(
                            LMP.Resources.GetLangString("Error_CouldNotExecuteAction") +
                            oAction.ID.ToString(), oE);
                    }
                }
            }
        }
        /// <summary>
        /// executes the specified ControlAction
        /// </summary>
        /// <param name="oAction"></param>
        private void ExecuteControlAction(ControlAction oAction)
        {
            DateTime t0 = DateTime.Now;

            string xParameters = oAction.Parameters;

            Trace.WriteNameValuePairs("xParameters", xParameters);

            //exit if condition for insertion is not satisfied
            if (!oAction.ExecutionIsSpecified())
                return;

            Variable oVar = oAction.ParentVariable;

            if (xParameters != "" && oVar != null)
            {
                string xValue = oVar.Value;
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                //They will treated as the literal characters by the actions
                xValue = Expression.MarkLiteralReservedCharacters(xValue);

                //evaluate [MyValue] first
                xParameters = FieldCode.EvaluateMyValue(xParameters, xValue);
            }

            try
            {
                switch (oAction.Type)
                {
                    case ControlActions.Types.ChangeLabel:
                        ExecuteControlActionChangeLabel(oAction, xParameters);
                        break;
                    case ControlActions.Types.Enable:
                        ExecuteControlActionEnable(oAction, xParameters);
                        break;
                    case ControlActions.Types.ExecuteControlActionGroup:
                        break;
                    case ControlActions.Types.RunMethod:
                        oAction.Execute();
                        break;
                    case ControlActions.Types.SetControlProperty:
                        break;
                    case ControlActions.Types.SetFocus:
                        ExecuteControlActionSetFocus(oAction, xParameters);
                        break;
                    case ControlActions.Types.SetVariableValue:
                        ExecuteControlActionSetVariableValue(oAction, xParameters);
                        break;
                    case ControlActions.Types.Visible:
                        ExecuteControlActionVisible(oAction, xParameters);
                        break;
                    case ControlActions.Types.DisplayMessage:
                        ExecuteControlActionDisplayMessage(oAction, xParameters);
                        break;
                    case ControlActions.Types.RefreshControl:
                        ExecuteControlActionRefreshControl(oAction, xParameters);
                        break;
                    default:
                        break;
                }
            }
            catch (System.Exception oE)
            {
                //action failed
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_ControlActionExecutionFailed") + //GLOG 7299: Incorrect resource string name specified
                    oVar.Name, oE);
            }

            if (oAction.ParentVariable != null)
                LMP.Benchmarks.Print(t0, Convert.ToString(oAction.Type),
                    "Value=" + oVar.Value, "Parameters=" + xParameters);
            else
                LMP.Benchmarks.Print(t0, Convert.ToString(oAction.Type),
                    "Parameters=" + xParameters);
        }
        private void ExecuteControlActionEnable(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";
            mpTriState bEnable = mpTriState.Undefined;

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    string xEnable = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);

                    switch (xEnable.ToUpper())
                    {
                        case "1":
                        case "TRUE":
                            bEnable = mpTriState.True;
                            break;
                        case "0":
                        case "FALSE":
                            bEnable = mpTriState.False;
                            break;
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //set enabled
                ((Control)m_oSegment.Variables.ItemFromName(xTargetVarPath)
                    .AssociatedControl).Enabled = (bEnable == mpTriState.True);
            }
        }
        private void ExecuteControlActionSetFocus(ControlAction oAction, string xParameters)
        {
            string xVar = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVar = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //select control
                ((Control)m_oSegment.Variables.ItemFromName(xVar)
                        .AssociatedControl).Select();

            }
        }
        private void ExecuteControlActionVisible(ControlAction oAction, string xParameters)
        {
            //GLOG - 3395 - CEH
            mpTriState bVisible = mpTriState.Undefined;
            string xName = "";
            string[] aNames = null;

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //GLOG - 3395 - CEH
                    //Allow for more multiple variable names
                    //get parameters
                    xName = aParams[0];
                    //Can be list of Variable names
                    aNames = xName.Split(',');

                    string xVisible = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);

                    switch (xVisible.ToUpper())
                    {
                        case "1":
                        case "TRUE":
                            bVisible = mpTriState.True;
                            break;
                        case "0":
                        case "FALSE":
                            bVisible = mpTriState.False;
                            break;
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }

                Trace.WriteNameValuePairs("xName", xName);

                //Cycle through each variable
                foreach (string xVar in aNames)
                {
                    //set control visibility
                    ((Control)m_oSegment.Variables.ItemFromName(xVar)
                        .AssociatedControl).Visible = (bVisible == mpTriState.True);
                }
            }
        }
        /// <summary>
        /// GLOG 2645: Update control properties that contain MacPac expressions
        /// </summary>
        /// <param name="oAction"></param>
        /// <param name="xParameters"></param>
        private void ExecuteControlActionRefreshControl(ControlAction oAction, string xParameters)
        {
            string xVar = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVar = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //get target node from variable
                Variable oVar = m_oSegment.Variables.ItemFromName(xVar);

                if (oVar != null)
                {
                    string xCtlValue = oVar.AssociatedControl.Value;

                    IControl oICtl = oVar.AssociatedControl;
                    Control oCtl = (Control)oICtl;
                    System.Type oCtlType = oCtl.GetType();

                    //get control properties from variable
                    string[] aProps = null;

                    if (oVar.ControlProperties != "")
                        aProps = oVar.ControlProperties.Split(StringArray.mpEndOfSubValue);
                    else
                        aProps = new string[0];

                    bool bChanged = false;
                    //cycle through property name/value pairs, setting those that 
                    //have a MacPac Expression as the value
                    for (int i = 0; i < aProps.Length; i++)
                    {
                        int iPos = aProps[i].IndexOf("=");
                        string xName = aProps[i].Substring(0, iPos);
                        string xValue = aProps[i].Substring(iPos + 1);

                        //evaluate control prop value, which may be a MacPac expression
                        string xEval = Expression.Evaluate(xValue, oVar.Segment, oVar.ForteDocument);
                        //Only set those properties using expressions
                        if (xValue != xEval)
                        {
                            PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                            System.Type oPropType = oPropInfo.PropertyType;

                            object oValue = null;

                            if (oPropType.IsEnum)
                                oValue = Enum.Parse(oPropType, xEval);
                            else
                                oValue = Convert.ChangeType(xEval, oPropType,
                                    LMP.Culture.USEnglishCulture);

                            oPropInfo.SetValue(oCtl, oValue, null);
                            bChanged = true;
                        }
                    }
                    //Update Variable Word Tag with latest Control Values if necessary
                    if (bChanged)
                    {
                        if (oICtl.Value != xCtlValue)
                            oVar.SetValue(oICtl.Value);
                        //Also update RuntimeControlValues - these can change without changing
                        //Control Value
                        oVar.SetRuntimeControlValues(oICtl.SupportingValues);
                    }
                }
            }
        }
        private void ExecuteControlActionChangeLabel(ControlAction oAction, string xParameters)
        {
            string xVar = "";
            string xNewLabel = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVar = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    xNewLabel = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);
                    if (xNewLabel == "Null")
                        xNewLabel = "";

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //set control label
                ((Control)m_oSegment.Variables.ItemFromName(xVar)
                    .AssociatedControl).Text = xNewLabel;
            }
        }
        /// <summary>
        /// Displays message box
        /// </summary>
        /// <param name="xParameters"></param>
        protected void ExecuteControlActionDisplayMessage(ControlAction oAction, string xParameters)
        {
            string xMsg = "";
            string xCaption = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xMsg = Expression.Evaluate(aParams[0], oSegment, m_oMPDocument);
                    xCaption = Expression.Evaluate(aParams[1], oSegment, m_oMPDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xMsg", xMsg, "xCaption", xCaption);

            if (xMsg != "")
            {
                if (xCaption == "")
                    xCaption = LMP.ComponentProperties.ProductName;
                MessageBox.Show(xMsg, xCaption, MessageBoxButtons.OK);
            }
        }
        private void ExecuteControlActionSetVariableValue(ControlAction oAction, string xParameters)
        {
            if (oAction.ParentSegment.CreationStatus != Segment.Status.Finished)
                return;

            string xVar = "";
            string xNewValue = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xVar = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    xNewValue = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
                Variable oVar = null;
                oVar = m_oSegment.Variables.ItemFromName(xVar);
                if (oVar != null)
                    oVar.SetValue(xNewValue);
            }
        }
        /// <summary>
        /// GLOG 2999: Updated to support Reference Target format
        /// Given a path in the form x.y.z.var, returns 
        /// Variable object from specified Child segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xVariablePath"></param>
        /// <returns></returns>
        private Variable GetVariableFromPath(Segment oSegment, string xVariablePath)
        {
            Segment oTargetSegment = null;
            Variable oVar = null;
            string xTargetVar = "";

            if (xVariablePath.IndexOf(".") > 0)
            {
                // Variable belongs to a child segment - form is x.y.z.var
                // This is the old method - code left in place to support existing content
                int iLastSep = xVariablePath.LastIndexOf(".");
                xTargetVar = xVariablePath.Substring(iLastSep + 1);
                string xChildSegmentPath = xVariablePath.Substring(0, iLastSep);
                oTargetSegment = oSegment.GetChildSegmentFromPath(xChildSegmentPath);
                try
                {
                    if (oTargetSegment != null)
                    {
                        oVar = oTargetSegment.Variables.ItemFromName(xTargetVar);
                    }
                }
                catch
                {
                    // No match found
                    return null;
                }
            }
            else
            {
                //GLOG 2999: Reference target method
                xTargetVar = xVariablePath;
                Segment[] oTargets = oSegment.GetReferenceTargets(ref xTargetVar);
                if (oTargets != null)
                {
                    foreach (Segment oTarget in oTargets)
                    {
                        //get target variable
                        oVar = oTarget.GetVariable(xTargetVar);

                        //stop after finding match
                        if (oVar != null)
                        {
                            break;
                        }
                    }
                }
            }
            return oVar;
        }

        private void OnVariableValueAssigned(object sender, EventArgs e)
        {
            try
            {
                //execute ControlActions
                try
                {
                    ExecuteControlActions((Variable)sender, ControlActions.Events.ValueChanged);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.ControlActionException(
                        LMP.Resources.GetLangString("Error_CouldNotExecuteControlAction"), oE);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// Clicked on Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_oPrefill = null;
            DialogResult = DialogResult.Cancel;
        }
        /// <summary>
        /// Clicked on Finish
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFinish_Click(object sender, EventArgs e)
        {
            //Update focus to ensure value of current control is updated if 
            //button was selected using hotkey
            btnFinish.Focus();
            if (ValidateSegment())
            {
                m_oPrefill = new Prefill((Segment)m_oSegment);
                DialogResult = DialogResult.OK;
            }
        }

        void oICtl_GotFocus(object sender, EventArgs e)
        {
            Variable oVar = ((Control)sender).Tag as Variable;

            if (oVar != null)
            {
                //Mark variable as visited
                oVar.MustVisit = false;
            }
        }
        /// <summary>
        /// Displays help for the active control
        /// </summary>
        private void ShowHelp()
        {
            try
            {
                string xHelpText = "";

                object oTag = null;

                try
                {
                    oTag = this.scMain.ActiveControl.Tag;
                }
                catch { }

                Authors oAuthors = oTag as Authors;
                Variable oVar = oTag as Variable;

                Help oDlg = null;
                if (oAuthors != null)
                {
                    xHelpText = m_oSegment.AuthorsHelpText;
                    oDlg = new Help(xHelpText);
                    oDlg.Show();
                }
                else if (oVar != null)
                {
                    xHelpText = oVar.HelpText;
                    oDlg = new Help(xHelpText);
                    oDlg.Show();
                }

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tabPages_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                //paint line under tabs
                e.Graphics.DrawLine(new Pen(Brushes.DarkGray, 1), 0, 26, this.tabPages.Width, 26);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Control has lost focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oICtl_LostFocus(object sender, EventArgs e)
        {
            try
            {
                LMP.Trace.WriteNameValuePairs("Control", ((IControl)sender).GetType().ToString());
                IControl oCtl = (IControl)sender;
                if (oCtl.IsDirty)
                {
                    if (oCtl is AuthorSelector)
                    {
                        m_oSegment.Authors.XML = oCtl.Value;
                    }
                    else
                    {
                        Variable oVar = ((Control)oCtl).Tag as Variable;
                        oVar.SetValue(oCtl.Value, false);
                        //GLOG 2645
                        oVar.SetRuntimeControlValues(oCtl.SupportingValues);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void splitContainer1_Paint(object sender, PaintEventArgs e)
        {
            Pen oPen = new Pen(Brushes.Turquoise);
            Rectangle oR = this.scMain.ClientRectangle;
            oR.Inflate(-1, -1);
            e.Graphics.DrawRectangle(oPen, oR);
            oPen = new Pen(Brushes.Turquoise, 4);
            e.Graphics.DrawLine(oPen, oR.Left, this.scMain.Panel2.Top,
                oR.Left + oR.Width, this.scMain.Panel2.Top);
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            //Update focus to ensure value of current control is updated if 
            //button was selected using hotkey
            btnSave.Focus();
            if (ValidateSegment())
            {
                if (m_oVarSet != null)
                {
                    //GLOG 2966: Replace existing VariableSet ContentString with current values
                    DialogResult iResult = MessageBox.Show(LMP.Resources.GetLangString("Prompt_OverwritePrefillDialogCaption"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (iResult == DialogResult.Yes)
                    {
                        VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, Session.CurrentUser.ID);
                        m_oPrefill = new Prefill((Segment)m_oSegment);
                        m_oVarSet.ContentString = m_oPrefill.ToString();
                        oDefs.Save(m_oVarSet);
                        this.DialogResult = DialogResult.Ignore;
                    }
                    return;
                }
                else
                {
                    m_oPrefill = new Prefill((Segment)m_oSegment);
                    Data.UserFolders oFolders = new Data.UserFolders(Session.CurrentUser.ID);
                    // Can't create Prefill if no User Folders exist
                    //GLOG : 8152 : JSW
                    //let ContentPropertyForm handle folder creation messages
                    //if (oFolders.Count == 0)
                    //{
                    //    MessageBox.Show(LMP.Resources.GetLangString("Msg_PrefillRequiresUserFolder"),
                    //        LMP.ComponentProperties.ProductName,
                    //        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    return;
                    //}
                    ContentPropertyForm oForm = new ContentPropertyForm();
                    DialogResult iResult = oForm.ShowForm(this.ParentForm, null, Form.MousePosition,
                        ContentPropertyForm.DialogMode.CreatePrefill, m_oPrefill, (Segment)m_oSegment);

                    if (iResult == DialogResult.OK)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        m_oPrefill = null;
                        LMP.MacPac.TaskPanes.RefreshAll();
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_DataSaved"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        private void tabPages_SelectedTabChanged(object sender, SelectedTabChangedEventArgs e)
        {
            try
            {
                e.Tab.TabPage.Controls[1].Focus();
            }
            catch { }
        }
        private void HandleFormButton_Enter(object sender, EventArgs e)
        {
            try
            {
                this.wbHelpText.DocumentText = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void SegmentDialog_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            try
            {
                e.Cancel = true;
                ShowHelp();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Handle F2 to Retrieve Contacts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyReleased(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F2)
                {
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                    GetContacts(sender, new EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void SegmentDialog_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                this.OnKeyReleased(sender, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
    }
}