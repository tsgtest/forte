﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class ValidationResponseForm : Form
    {
        public ValidationResponseForm()
        {
            InitializeComponent();
        }

        public string ResponseText { get; set; }
    }
}
