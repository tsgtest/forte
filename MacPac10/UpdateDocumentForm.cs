﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    public partial class UpdateDocumentForm : Form
    {
        bool m_bHasContacts = false;
        bool m_bHasAuthor = false;
        bool m_bHasLetterhead = false;

        public UpdateDocumentForm()
        {
            InitializeComponent();
        }

        private void UpdateDocumentForm_Load(object sender, EventArgs e)
        {
            this.Text = LMP.Resources.GetLangString("Dialog_UpdateDocumentForm");
            this.grpUpdateItems.Text = LMP.Resources.GetLangString("Dialog_UpdateDocumentForm_GrpUpdateItems");
            this.chkAuthor.Text = LMP.Resources.GetLangString("Dialog_UpdateDocumentForm_chkAuthor");
            this.chkLetterhead.Text = LMP.Resources.GetLangString("Dialog_UpdateDocumentForm_chkLetterhead");
            this.chkContacts.Text = LMP.Resources.GetLangString("Dialog_UpdateDocumentForm_chkContacts");
            this.btnOK.Text = LMP.Resources.GetLangString("Lbl_OK");
            this.btnCancel.Text = LMP.Resources.GetLangString("Dialog_Cancel");

            TaskPane oTP = TaskPanes.Item(LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveDocument);

            if (oTP != null)
            {
                for (int i = 0; i < oTP.ForteDocument.Segments.Count && 
                    (!m_bHasContacts || !m_bHasAuthor || !m_bHasLetterhead); i++)
                {
                    LMP.Architect.Api.Segment oSegment = oTP.ForteDocument.Segments[i];

                    if (!m_bHasContacts)
                    {
                        m_bHasContacts = HasContacts(oSegment);
                    }

                    if (!m_bHasAuthor)
                    {
                        m_bHasAuthor = HasAuthor(oSegment);
                    }

                    if (!m_bHasLetterhead)
                    {
                        m_bHasLetterhead = HasLetterhead(oSegment);
                    }
                }                
            }

            UpdateView();
        }

        private bool HasContacts(LMP.Architect.Api.Segment oSeg)
        {
            bool bHasContacts = false;

            if (oSeg.IsFinished)
                return false;

            // A segment has contacts if its variables have a non-zero 
            // contact type. Check the variables until the first variable
            // with a non-zero contact type is found.

            if (oSeg.Variables != null)
            {
                for (int i = 0; i < oSeg.Variables.Count && !bHasContacts; i++)
                {
                    if (oSeg.Variables[i].CIContactType != 0)
                    {
                        // Check for a non-zero UNID for the contacts.
                        // Contacts that are not entered manually have a non-zero UNID.
                        // We will only consider variables with non-manually entered 
                        // contact information as being a variable that has contacts.
                        System.Text.RegularExpressions.Match oMatch = null;
                        int j = 1;
                        string xMatch = null;
                        string xContactDetailXML = oSeg.Variables[i].Value;

                        try
                        {
                            do
                            {
                                // Get the UNID.
                                string xPattern = "Index=\"" + j.ToString() + "\" UNID=\".*?\"";
                                oMatch = System.Text.RegularExpressions.Regex.Match(xContactDetailXML, xPattern);

                                if (oMatch != null && oMatch.Value != "")
                                {
                                    // Extract the UNID from match
                                    xMatch = oMatch.Value;
                                    int iPos = xMatch.IndexOf("UNID=");
                                    xMatch = xMatch.Substring(iPos + 6, xMatch.Length - (iPos + 7));

                                    bHasContacts = (xMatch != "0");
                                }
                                
                                j++;
                            } while (oMatch != null && oMatch.Value != "" && !bHasContacts);
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        }
                    }
                }
            }

            // If the variables for this segment don't have contacts, check
            // if the child segments if this segment do.
            for (int i = 0; i < oSeg.Segments.Count && !bHasContacts; i++)
            {
                bHasContacts = HasContacts(oSeg.Segments[i]);
            }

            return bHasContacts;
        }

        private bool HasAuthor(LMP.Architect.Api.Segment oSeg)
        {
            if (oSeg.IsFinished)
                return false;

            // Check if this segment has authors.
            bool bHasAuthor = ( oSeg != null &&
                                oSeg.Authors != null &&
                                oSeg.Authors.Count > 0);

            // Check if the child segments have authors if this
            // segment does not have authors.
            for (int i = 0; i < oSeg.Segments.Count && !bHasAuthor; i++)
            {
                bHasAuthor = HasAuthor(oSeg.Segments[i]);
            }

            return bHasAuthor;
        }

        private bool HasLetterhead(LMP.Architect.Api.Segment oSeg)
        {
            // Check if this segment is a LetterHead type.
            bool bHasLetterhead = (oSeg is LMP.Architect.Api.Letterhead);
            if (bHasLetterhead)
            {
                //GLOG 8727: If Segment is Letterhead but no longer exists
                //in database return false
                AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.Letterhead);
                try
                {
                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oSeg.ID1);
                 }
                catch
                {
                    bHasLetterhead = false;
                }
            }
            // If this segment isn't a letterhead, check if any of 
            // the child segments are a letterhead.
            for(int i = 0; i < oSeg.Segments.Count && !bHasLetterhead; i++)
            {
                bHasLetterhead = HasLetterhead(oSeg.Segments[i]);
            }

            return bHasLetterhead;
        }

        /// <summary>
        /// Get/set whether the Author is to be updated.
        /// </summary>
        public bool UpdateAuthor
        {
            get
            {
                return this.chkAuthor.Checked;
            }

            set
            {
                this.chkAuthor.Checked = value;
            }
        }

        /// <summary>
        /// Get/set whether the Contacts are to be updated.
        /// </summary>
        public bool UpdateContacts
        {
            get
            {
                return this.chkContacts.Checked;
            }

            set
            {
                this.chkContacts.Checked = value;
            }
        }

        /// <summary>
        /// Get/set whether the Letterhead is to be updated.
        /// </summary>
        public bool UpdateLetterhead
        {
            get
            {
                return this.chkLetterhead.Checked;
            }

            set
            {
                this.chkLetterhead.Checked = value;
            }
        }

        private void UpdateView()
        {

            this.chkContacts.Enabled = m_bHasContacts;
            this.chkAuthor.Enabled = m_bHasAuthor;
            this.chkLetterhead.Enabled = m_bHasLetterhead;

            // Enable the OK button only if an item is selected for update.
            this.btnOK.Enabled =    this.chkAuthor.Checked ||
                                    this.chkContacts.Checked ||
                                    this.chkLetterhead.Checked;
        }

        private void chkContacts_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                UpdateView();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkAuthor_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                UpdateView();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkLetterhead_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                UpdateView();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
