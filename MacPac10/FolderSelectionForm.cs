﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class FolderSelectionForm : Form
    {
        public FolderSelectionForm(string xLabelText) : this(xLabelText, true, false) { }
        public FolderSelectionForm(string xLabelText, bool bShowCancelButton, bool bUserFoldersOnly)
        {
            InitializeComponent();
            this.lblFolder.Text = xLabelText;
            if (!bShowCancelButton)
            {
                this.btnOK.Left = this.btnCancel.Left;
                this.btnCancel.Visible = false;
                
            }
            if (bUserFoldersOnly)
            {
                this.ftvPrefill.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders;
                this.ftvPrefill.OwnerID = Session.CurrentUser.ID;
            }

            this.ftvPrefill.ExecuteFinalSetup();

            if (bUserFoldersOnly)
            {
                this.ftvPrefill.Nodes[0].Selected = true;
            }
            else
            {
                string xID = LMP.MacPac.Session.CurrentUser.UserSettings.DefaultDesignerFolderID;
                if (!string.IsNullOrEmpty(xID))
                    this.ftvPrefill.Nodes[xID].Selected = true;
                else
                    this.ftvPrefill.Nodes[0].Selected = true;
            }

            this.ftvPrefill.ActiveNode = this.ftvPrefill.SelectedNodes[0];
        }

        public string SelectedFolderID
        {
            get
            {
                string xFolderID = this.ftvPrefill.Value;
                LMP.MacPac.Session.CurrentUser.UserSettings.DefaultDesignerFolderID = xFolderID;
                return xFolderID;
            }
        }

        public string SelectedFolderNodeKey
        {
            get
            {
                if ((this.ftvPrefill.DisplayOptions & LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) ==
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders))
                {
                    //only user folders are showing - append user folder root key
                    return "0.0\\\\" + this.ftvPrefill.SelectedNodes[0].Key;
                }
                else if ((this.ftvPrefill.DisplayOptions & LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) ==
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders))
                {
                    //only admin folders are showing - append admin folder root key
                    return "0\\\\" + this.ftvPrefill.SelectedNodes[0].Key;
                }
                else if ((this.ftvPrefill.DisplayOptions & LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders) ==
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders))
                {
                    //only shared folders are showing - append shared folder root key
                    return "-2\\\\" + this.ftvPrefill.SelectedNodes[0].Key;
                }
                else
                    //root keys are showing - don't append anything
                    return this.ftvPrefill.SelectedNodes[0].Key;
            }
        }
    }
}
