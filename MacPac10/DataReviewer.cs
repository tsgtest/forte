using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;
using LMP.Architect.Api;
using Infragistics.Win;
using Infragistics.Win.UltraWinTree;
using LMP.Controls;
using System.Xml;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace LMP.MacPac
{
	/// <summary>
	/// Summary description for DataReviewer.
	/// ********************************************************************************
	/// NOTES: 
	/// 1) in the tree, node keys have the following forms:
	/// for segment nodes, key is the tag id.  for variable nodes, key is the 
	/// parent segment tag id + "." + variable id
	/// for author nodes, key is parent segment tag id + ".Authors".
	/// 2) tree node tags contain the following object references:
	/// segment node tags contain a ref to the segment.
	/// variable node tags contain a ref to the variable.
	/// author node tags contain a ref to the segment authors.
	/// value node tags contain a ref to associated control, if there is one.
	/// ********************************************************************************
	/// </summary>
    internal partial class DataReviewer : UserControl
    {
        #region *********************fields*********************
        private Control m_oCurCtl;
        private UltraTreeNode m_oCurNode;
        private TaskPane m_oTaskPane;
        private ForteDocument m_oMPDocument;
        private Modes m_iReviewMode;
        private string m_xEmptyDisplayValue;
        private Hashtable m_oHotkeys = new Hashtable();
        private bool m_bInHotkeyMode;
        private bool m_bHotKeyPressed;
        private string m_xTransparentNodes = "|";
        private Hashtable m_oFinishedValues;
        private System.Windows.Forms.Timer m_oTabNavigationTimer = null;
        private bool m_bTabTimerIsRunning = false;

        //GLOG : 6188 : CEH
        private TimedContextMenuStrip m_oMenuSegment;

        private ToolStripMenuItem m_oMenuSegment_SaveData;
        private ToolStripMenuItem m_oMenuSegment_SaveContent;
        private ToolStripMenuItem m_oMenuSegment_RecreateSegment;

        private bool m_bHandlingTabPress = false;
        private bool m_bDisableHotKeyMode = false;
        private bool m_bIsInControlShiftEditMode = false;
        private CalloutForm m_oCallout = null;
        // Set to false to use Word Callout shape instead of Windows form
        private const bool m_bUseWindowsCallout = true;
        private UltraTreeNode m_oCurTabNode = null;
        private Hashtable m_oControlsHash = null;
        private bool m_bCIIsActive = false;
        private Boolean m_bRepositioningControl = false;
        private bool m_bIsResizing = false;
        private const string mpTrailersNodeKey = "MacPacTrailersNode";
        private Hashtable m_oUnresolvedEmptyTags = new Hashtable();
        private bool m_bIsRefreshingSegmentNode = false;
        private string m_xSkipVarUpdateFromControl = "";
        private bool m_bSuppressTreeActivationHandlers = false;
        private bool m_bShowValueNodes = true;
        private bool m_bSelectingNode = false;
        #endregion
        #region  *********************constants*********************
        private const int KEY_SHIFT = 16;
        private const int KEY_CONTROL = 17;
        private const int KEY_HOME = 36;
        private const int KEY_END = 35;
        private const int KEY_TAB = 9;
        private const int KEY_LEFT = 37;
        private const int KEY_RIGHT = 39;
        private const int KEY_UP = 38;
        private const int KEY_DOWN = 40;
        #endregion
        #region *********************enumerations*********************
        private enum Images
        {
            NoImage = -1,
            ValidDocumentSegment = 0,
            InvalidDocumentSegment = 1,
            ValidComponentSegment = 2,
            InvalidComponentSegment = 3,
            ValidTextBlockSegment = 4,
            InvalidTextBlockSegment =5,
            ValidVariableValue = 6,
            InvalidVariableValue = 7,
            IncompleteVariable = 41,
            IncompleteDocumentSegment = 42,
            IncompleteComponentSegment = 43,
            IncompleteTextBlockSegment = 44,
            HotkeyA = 8,
            HotkeyB = 9,
            HotkeyC = 10,
            HotkeyD = 11,
            HotkeyE = 12,
            HotkeyF = 13,
            HotkeyG = 14,
            HotkeyH = 15,
            HotkeyI = 16,
            HotkeyJ = 17,
            HotkeyK = 18,
            HotkeyL = 19,
            HotkeyM = 20,
            HotkeyN = 21,
            HotkeyO = 22,
            HotkeyP = 23,
            HotkeyQ = 24,
            HotkeyR = 25,
            HotkeyS = 26,
            HotkeyT = 27,
            HotkeyU = 28,
            HotkeyV = 29,
            HotkeyW = 30,
            HotkeyX = 31,
            Hotkey1 = 32,
            Hotkey2 = 33,
            Hotkey3 = 34,
            Hotkey4 = 35,
            Hotkey5 = 36,
            Hotkey6 = 37,
            Hotkey7 = 38,
            Hotkey8 = 39,
            Hotkey9 = 40
        }
        private enum ControlHost
        {
            EditorTree = 1,
            FloatingForm = 2
        }
        private enum NodeTypes
        {
            Segment = 1,
            Variable = 2,
            Value = 3,
            Advanced = 4
        }
        internal enum Modes
        {
            Edit = 1,
            Recreate = 2,
            Transform = 3,
            InsertUsingDocumentData = 4,
            SaveData = 5
        }
        #endregion
        #region *********************constructors*********************
        public DataReviewer(ForteDocument oMPDocument)
        {
            Trace.WriteInfo("In LMP.MacPac.DataReviewer.Ctor");

            InitializeComponent();

            this.m_oMPDocument = oMPDocument;

            //hook up to tree's TabPressed event
            this.treeDocContents.TabPressed += 
                new TabPressedHandler(oIControl_TabPressed);
            this.treeDocContents.CIRequested += new CIRequestedHandler(GetContacts);

            m_oTabNavigationTimer = new System.Windows.Forms.Timer();
            m_oTabNavigationTimer.Stop();
            m_oTabNavigationTimer.Tick += new EventHandler(m_oTabNavigationTimer_Tick);

            m_oControlsHash = new Hashtable();
            m_oFinishedValues = new Hashtable();
        }
        #endregion
        #region *********************properties*********************
        internal Segment CurrentSegment
        {
            get
            {
                string xSegmentTagID = null;
                UltraTreeNode oNode = this.treeDocContents.ActiveNode;
                if (NodeType(oNode) == NodeTypes.Segment)
                {
                    xSegmentTagID = oNode.Key;
                }
                else if (NodeType(oNode.Parent) == NodeTypes.Segment)
                {
                    xSegmentTagID = oNode.Parent.Key;
                }
                else if (NodeType(oNode.Parent) == NodeTypes.Advanced)
                {
                    xSegmentTagID = oNode.Parent.Parent.Key;
                }

                Segment oSegment = this.ForteDocument.FindSegment(xSegmentTagID);

                return oSegment;
            }
        }
        /// <summary>
        /// gets/sets the associated task pane
        /// </summary>
        internal TaskPane TaskPane
        {
            get { return m_oTaskPane; }
            set
            {
                m_oTaskPane = value;
                m_oMPDocument = m_oTaskPane.ForteDocument;
            }
        }
        /// <summary>
        /// returns true if the scroll bars are visible, else false
        /// </summary>
        public bool ScrollBarsVisible
        {
            get
            {
                //get the top level nodes of the tree
                TreeNodesCollection oNodes = this.treeDocContents.Nodes;

                if (oNodes.Count == 0 || !oNodes[0].IsInView)
                    //first node is not in view -
                    //scroll bars must be visible
                    return true;

                //cycle through nodes, checking if last node is visible -
                //if not, scroll bars must be visible
                while (oNodes != null && oNodes.Count > 0)
                {
                    //get last node in collection
                    UltraTreeNode oLastNode = oNodes[oNodes.Count - 1];

                    if (oLastNode.Bounds.Top + oLastNode.Bounds.Height + 10 
                        >= this.treeDocContents.Height)
                        //last node is not visible -
                        //scroll bars are visible
                        return true;
                    else if (oLastNode.Expanded)
                        //get child nodes of last node
                        oNodes = oLastNode.Nodes;
                    else
                        return false;
                }

                //if we got here, all nodes are visible
                return false;
            }
        }
        /// <summary>
        /// Turn off the Ctrl-key hotkey toggle to allow Ctrl in combination with other keys
        /// </summary>
        internal bool DisableHotKeyMode
        {
            get { return this.m_bDisableHotKeyMode; }
            set { this.m_bDisableHotKeyMode = value; }
        }
        /// <summary>
        /// Gets/sets mode indicating that we are using 
        /// control (shift) right/left arrow to highlight
        /// text and distinguish this from using control
        /// to enter hotkey mode.
        /// </summary>
        private bool IsInControlShiftEditMode
        {
            get
            {
                return m_bIsInControlShiftEditMode;
            }

            set
            {
                m_bIsInControlShiftEditMode = value;
            }
        }
        /// <summary>
        /// returns true if the document editor tree is refreshing
        /// </summary>
        private bool IsRefreshingSegmentNode
        {
            get { return m_bIsRefreshingSegmentNode; }
        }
        /// <summary>
        /// an mVar is added to this collection when manually cleared and acted
        /// upon when we're satisfied that the user is done editing it
        /// </summary>
        internal Hashtable UnresolvedEmptyTags
        {
            get { return m_oUnresolvedEmptyTags; }
            set { m_oUnresolvedEmptyTags = value; }
        }
        /// <summary>
        /// returns the macpac document associated with this editor
        /// </summary>
        internal ForteDocument ForteDocument
        {
            get { return m_oMPDocument; }
        }
        /// <summary>
        /// gets/sets visibility of Finish Button
        /// </summary>
        internal bool FinishButtonVisible
        {
            get { return this.tbtnFinish.Visible; }
            set { this.tbtnFinish.Visible = value; }
        }
        /// <summary>
        /// gets/sets the mode of this form -
        /// based on caller
        /// </summary>
        internal Modes ReviewMode
        {
            get { return m_iReviewMode; }
            set
            {
                m_iReviewMode = value;

                if (m_iReviewMode != Modes.Edit)
                {
                    this.TaskPane.ContentManagerVisible = false;
                    this.TaskPane.DocumentEditorVisible = false;
                }
                else
                {
                    this.TaskPane.ContentManagerVisible = true;
                    this.TaskPane.DocumentEditorVisible = true;
                }

                //this.tbtnFinish.Text = "Finish";
            }
        }
        #endregion
        #region *********************event handlers*********************
        private void treeDocContents_Scroll(object sender, Infragistics.Win.UltraWinTree.TreeScrollEventArgs e)
        {

            try
            {
                if (this.m_oCurNode != null && !m_bRepositioningControl)
                {
                    if (this.picMenu.Visible)
                    {
                        ShowContextMenuButton(this.m_oCurNode);
                    }

                    //move control with associated node
                    this.SizeAndPositionControl();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_AfterExpand(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
        {
            try
            {
                if (this.IsRefreshingSegmentNode)
                    return;

                this.SuspendLayout();

                if (e.TreeNode.Tag is Segment)
                {
                    SelectNode(e.TreeNode,true);

                    if (e.TreeNode.Nodes.Count > 0)
                    {
                        //scroll tree to attempt to show all child nodes
                        //if the last child node can't be seen
                        if (!e.TreeNode.Nodes[e.TreeNode.Nodes.Count - 1].IsInView)
                            e.TreeNode.BringIntoView(true);
                    }
                }

                if (this.m_oCurNode != null)
                {
                    if (this.picMenu.Visible)
                    {
                        ShowContextMenuButton(this.m_oCurNode);
                    }

                    //move control with associated node
                    this.SizeAndPositionControl();
                }
            }
            catch (System.InvalidOperationException)
            {
                //ignore potential "cross-thread" exception
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
        }
        private void treeDocContents_BeforeCollapse(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
        {
            try
            {
                if (e.TreeNode == this.m_oCurNode)
                    return;

                if (e.TreeNode.IsAncestorOf(this.m_oCurNode))
                {
                    //exit edit mode
                    this.ExitEditMode();

                    //remove the node selection
                    this.m_oCurNode.Selected = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_BeforeExpand(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
        {
            try
            {
                if (e.TreeNode.Tag == null)
                    return;

                string xSegmentXML = e.TreeNode.Tag.ToString();

                //////GLOG item #5910 - dcf
                //XmlDocument oXML = new XmlDocument();
                //oXML.LoadXml("<Doc>" + xSegmentXML + "</Doc>");

                //XmlNode oNode = null;

                //try
                //{
                //    oNode = oXML.SelectSingleNode("//Segment");
                //}
                //catch { }

                //if (oNode != null && !e.TreeNode.HasNodes)
                if (e.TreeNode.Tag.ToString().StartsWith("<Segment ") && !e.TreeNode.HasNodes)
                {
                    RefreshSegmentNode(e.TreeNode.Key);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_AfterCollapse(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
        {
            try
            {
                if (this.m_oCurNode != null)
                {                    
                    //move control with associated node
                    if (this.picMenu.Visible)
                    {
                        ShowContextMenuButton(this.m_oCurNode);
                    }

                    this.SizeAndPositionControl();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_BeforeActivate(object sender, CancelableNodeEventArgs e)
        {
            if (m_bSuppressTreeActivationHandlers)
                return;

            try
            {
                //GLOG 6829 (dm)
                //GLOG 7126 (dm) - added bIsValueNode condition to avoid error
                //due to TreeNode.Tag being null if you click on the author value
                //node before clicking on the author node itself (the tag is set in
                //EnterAuthorsEditMode) - this block isn't intended for value nodes
                bool bIsValueNode = (e.TreeNode.Key.EndsWith("_Value"));
                if ((!bIsValueNode) && e.TreeNode.Tag.ToString().StartsWith("<Segment") &&
                    this.ForteDocument.Segments.ContentIsMissing)
                {
                    this.TaskPane.DocEditor.RefreshTree();
                    this.RefreshTree();
                }

                if (m_bHandlingTabPress)
                {
                    //If Tabbing through tree, don't select node until
                    //tabbing stops - each Tab press will restart timer
                    m_oCurTabNode = e.TreeNode;
                    m_oTabNavigationTimer.Start();
                    m_bTabTimerIsRunning = true;
                }
                else
                {
                    m_oTabNavigationTimer.Stop();
                    m_bTabTimerIsRunning = false;
                    m_oCurTabNode = null;

                    if (bIsValueNode)
                    {
                        e.Cancel = true;
                        //node is a variable value node - select parent
                        this.treeDocContents.ActiveNode = e.TreeNode.Parent;
                    }
                    else
                    {
                        ExitEditMode();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void treeDocContents_Leave(object sender, EventArgs e)
        {
            try
            {
                //update value of current variable
                if ((m_oCurCtl != null) && (m_oCurNode != null) && (m_oCurNode.Tag is Variable))
                {
                    //Display of listbox for Multiline combo will trigger this event
                    if (m_oCurCtl is MultilineCombo && ((MultilineCombo)m_oCurCtl).Focused)
                        return;
                    else
                    {
                        //GLOG 3792 and 3816 (dm) - make sure Word doc still exists
                        //before proceeding - in Word 2003, this event gets called when
                        //obsolete task panes are disposed in TaskPanes.RemoveUnused()
                        string xName = null;
                        try
                        {
                            xName = m_oMPDocument.WordDocument.Name;
                        }
                        catch { }
                        if (xName != null)
                            UpdateVariableValueFromCurrentCtl();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Timer is started upon tabbing into node
        /// When tabbing through tree, new node will not be selected until
        /// Node has remained active for 1 tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oTabNavigationTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                m_oTabNavigationTimer.Stop();
                m_bTabTimerIsRunning = false;
                //exit if the new selection is the current selection
                if (m_oCurTabNode == null)
                    return;

                this.treeDocContents.ActiveNode = m_oCurTabNode;

                //ensure that screen refreshes
                this.TaskPane.ScreenUpdating = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //private void OnVariableValueAssigned(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Variable oVar = sender as Variable;
        //        //GLOG 3627: If Variable currently has temp value assigned, don't do anything
        //        if (oVar.Value == "zzmpTempValue")
        //            return;

        //        //execute ValueChanged control actions
        //        try
        //        {
        //        }
        //        catch (System.Exception oE)
        //        {
        //            throw new LMP.Exceptions.ControlActionException(
        //                LMP.Resources.GetLangString("Error_CouldNotExecuteControlAction"), oE);
        //        }


        //        //GLOG 3144: Adjust special caption border length if necessary
        //        //GLOG 4282: Run for any Collection Table item
        //        if (oVar.Segment is CollectionTableItem && !oVar.IsTagless)
        //            oVar.Segment.UpdateTableBorders();

        //        //get associated tree node
        //        string xKey = "";
        //        if (oVar.Segment != null)
        //            xKey = oVar.Segment.FullTagID + "." + oVar.ID;
        //        else
        //            xKey = oVar.ID;

        //        //GLOG 2610 - added error trap
        //        UltraTreeNode oNode = null;
        //        try
        //        {
        //            oNode = this.GetNodeFromTagID(xKey);
        //        }
        //        catch { }

        //        if (oNode != null)
        //        {

        //            //set value of associated value node
        //            oNode.Nodes[0].Text = this.GetVariableUIValue(oVar);

        //            //clear previous validation image
        //            oNode.LeftImages.Clear();

        //            //add variable validation image
        //            if (!oVar.HasValidValue)
        //                oNode.LeftImages.Add(Images.InvalidVariableValue);
        //            else if (oVar.MustVisit)
        //                oNode.LeftImages.Add(Images.IncompleteVariable);
        //            else
        //                oNode.LeftImages.Add(Images.ValidVariableValue);
        //        }
        //    }
        //    catch (System.Exception oE)
        //    {
        //        LMP.Error.Show(oE);
        //    }
        //}
        private void DataReviewer_Load(object sender, EventArgs e)
        {
            try
            {
                //Set Tooltip for floatint CI button
                System.Windows.Forms.ToolTip oTip = new System.Windows.Forms.ToolTip();

                this.SetTreeAppearance();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles case when tab or shift-tab have been pressed-
        /// executes only when the ProcessDialogKey doesn't execute,
        /// as the key will get processed by the control, which raises
        /// the event that causes this handler to execute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void oIControl_TabPressed(object sender, TabPressedEventArgs e)
        {
            HandleTabPressed(e.ShiftPressed);
        }
        private void HandleTabPressed(bool bShift)
        {
            //Stop timer
            m_oTabNavigationTimer.Stop();

            //If tab is pressed multiple times, gradually increate timer interval to avoid
            //unnecessary creation of controls for each visited node
            if (m_bTabTimerIsRunning)
                m_oTabNavigationTimer.Interval = Math.Min(m_oTabNavigationTimer.Interval + 50, 200);
            else
                m_oTabNavigationTimer.Interval = 25;
            LMP.Trace.WriteNameValuePairs("Interval", m_oTabNavigationTimer.Interval);

            ExitHotkeyMode();

            m_bHandlingTabPress = true;
            try
            {
                if (bShift)
                    //shift-tab was pressed - select previous node
                    this.SelectPreviousNode();
                else
                    //tab was pressed - select next node
                    this.SelectNextNode();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_bHandlingTabPress = false;
            }
        }

        void oSeg_AfterDefaultAuthorSet(object sender, EventArgs e)
        {
            try
            {
                //execute value changed control actions for authors
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void oSeg_AfterJurisdictionSet(object sender, EventArgs e)
        {
            try
            {
                //execute value changed control actions for authors
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// AfterAuthorUpdated handler - added for GLOG 7126 (dm)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void oSeg_UpdatedForAuthor(object sender, EventArgs e)
        {
            try
            {
                Segment oSeg = sender as Segment;

                //update hashtable with new snapshot values
                m_oFinishedValues[oSeg.FullTagID] = oSeg.Snapshot.Values;

                //exit if transparent
                if (this.NodeIsTransparent(oSeg.FullTagID))
                    return;

                //get segment node
                UltraTreeNode oSegmentNode = null;
                try
                {
                    oSegmentNode = GetNodeFromTagID(oSeg.FullTagID, false);
                }
                catch { }

                //exit if segment node does not yet exist or has not yet been populated
                if (oSegmentNode == null)
                    return;
                else if (oSegmentNode.Nodes.Count == 0)
                    return;

                //create new prefill
                Prefill oPrefill = new Prefill(m_oFinishedValues[oSeg.FullTagID].ToString(), "Values", "1");

                //update variable value nodes
                for (int i = oSegmentNode.Nodes.Count - 1; i >= 0; i--)
                {
                    UltraTreeNode oNode = oSegmentNode.Nodes[i];
                    NodeTypes iCurNodeType = NodeType(oNode);
                    if (iCurNodeType== NodeTypes.Variable)
                    {
                        //ensure that the node is visible
                        this.EnsureVisibleNode(oNode);

                        //GLOG 3416 - added oNode.Visible conditional
                        if (oNode.Visible)
                        {
                            //get raw value
                            int iPos = oNode.Key.LastIndexOf(".");
                            string xPrefillPath = oNode.Key.Substring(iPos + 1);
                            string xRawValue = oPrefill[xPrefillPath];

                            //GLOG 8125: Don't attempt to update Display text if no corresponding Prefill value
                            if (xRawValue != null)
                            {
                                //GLOG 5894: Discard CI Detail Token STring portion of Prefill Value if present
                                int iTokenStart = xRawValue.IndexOf("<CIDetailTokenString>");
                                int iTokenEnd = xRawValue.IndexOf("</CIDetailTokenString>");
                                if (iTokenStart > -1 && iTokenEnd > -1 && iTokenEnd > iTokenStart)
                                {
                                    xRawValue = xRawValue.Substring(0, iTokenStart);
                                }
                                //set value of associated value node
                                oNode.NextVisibleNode.Text = this.GetVariableUIValue(oNode, xRawValue);
                            }
                        }
                    }
                }

                //more node
                UltraTreeNode oMoreNode = null;
                try
                {
                    oMoreNode = GetNodeFromTagID(oSeg.FullTagID + ".Advanced");
                }
                catch { }
                if (oMoreNode != null)
                {
                    // GLOG : 3518 : JAB
                    // The more node expands within the following for loop
                    // when the EnsureVisibleNode(oNode) method is called.
                    // Retain and restore the Expanded state of the More node.
                    bool bMoreNodeExpanded = oMoreNode.Expanded;

                    for (int i = oMoreNode.Nodes.Count - 1; i >= 0; i--)
                    {
                        UltraTreeNode oNode = oMoreNode.Nodes[i];
                        NodeTypes iCurNodeType = NodeType(oNode);
                        if (iCurNodeType == NodeTypes.Variable)
                        {
                            //ensure that the node is visible
                            this.EnsureVisibleNode(oNode);

                            //get raw value
                            int iPos = oNode.Key.LastIndexOf(".");
                            string xPrefillPath = oNode.Key.Substring(iPos + 1);
                            string xRawValue = oPrefill[xPrefillPath];

                            //GLOG 5894: Discard CI Detail Token STring portion of Prefill Value if present
                            int iTokenStart = xRawValue.IndexOf("<CIDetailTokenString>");
                            int iTokenEnd = xRawValue.IndexOf("</CIDetailTokenString>");
                            if (iTokenStart > -1 && iTokenEnd > -1 && iTokenEnd > iTokenStart)
                            {
                                xRawValue = xRawValue.Substring(0, iTokenStart);
                            }

                            //set value of associated value node
                            oNode.NextVisibleNode.Text = this.GetVariableUIValue(oNode, xRawValue);
                        }
                    }

                    // GLOG : 3518 : JAB
                    // Restore the Expanded state of the More node.
                    oMoreNode.Expanded = bMoreNodeExpanded;
                }

                //execute select variable actions
                for (int i = oSegmentNode.Nodes.Count - 1; i >= 0; i--)
                {
                    UltraTreeNode oNode = oSegmentNode.Nodes[i];
                    NodeTypes iCurNodeType = NodeType(oNode);
                    if (iCurNodeType == NodeTypes.Variable)
                        ExecuteFinishedDataVariableActions(oNode, oSeg);
                }

                //more node
                if (oMoreNode != null)
                {
                    for (int i = oMoreNode.Nodes.Count - 1; i >= 0; i--)
                    {
                        UltraTreeNode oNode = oMoreNode.Nodes[i];
                        NodeTypes iCurNodeType = NodeType(oNode);
                        if (iCurNodeType == NodeTypes.Variable)
                            ExecuteFinishedDataVariableActions(oNode, oSeg);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        
        private void DataReviewer_Leave(object sender, EventArgs e)
        {
            try
            {
                //GLOG 3389: In Word 2003, this event gets called at point TaskPane object is disposed
                if (this.TaskPane.Disposing)
                    return;
                this.ExitEditMode();
                //GLOG 4611: Clear m_oCurNode so click on same node will redisplay control
                //when returning to Editor
                this.m_oCurNode = null;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void OnControlClick(object sender, EventArgs e)
        {
            //TODO: remove this handler if we're not going 
            //to reinstate the Click control action event
            //Variable oVar = this.m_oCurNode.Tag as Variable;

            //if (oVar != null)
            //    //execute actions for click event
            //    ExecuteControlActions(ControlActions.Events.Click);
        }

        private void treeDocContents_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Don't pass keystrokes to tree if menu is visible
                if (m_oMenuSegment != null && m_oMenuSegment.Visible)
                {
                    if (e.KeyCode == Keys.Escape)
                        m_oMenuSegment.Close(ToolStripDropDownCloseReason.Keyboard);
                    else
                        m_oMenuSegment.HandleKeyDown(e);
                    e.SuppressKeyPress = true;
                    return;
                }
                else if ((e.KeyCode == Keys.Down) && e.Control && !e.Alt && !e.Shift)
                {
                    e.SuppressKeyPress = true;
                    //Ctrl+Down Arrow displays context menu
                    if (m_oCurNode != null && m_oCurNode.Tag is Segment)
                    {
                        this.CreateSegmentMenu();
                        m_oMenuSegment.Show(this.treeDocContents, m_oCurNode.UIElement.Rect.Left,
                            m_oCurNode.UIElement.Rect.Top);
                    }
                    return;
                }
                // GLOG : 3302 : JAB
                // Disallow the up and down arrow keys from navigating the tree nodes.
                else if ((e.KeyCode == Keys.Down || e.KeyCode == Keys.Up) 
                    && !e.Control && !e.Alt && !e.Shift)
                {
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                }
                else
                {
                    this.DisableHotKeyMode = false;
                    this.OnKeyPressed(this, e);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void treeDocContents_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (m_oMenuSegment != null && m_oMenuSegment.Visible)
                    return;
                this.OnKeyReleased(this, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        
        private void treeDocContents_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if ((e.Button & MouseButtons.Right) == MouseButtons.Right)
                {
                    ShowContextMenu(e.X, e.Y);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void DataReviewer_Resize(object sender, EventArgs e)
        {
            try
            {
                //Don't do anything if Resize code is already running
                if (m_bIsResizing)
                    return;

                if (m_oCurCtl != null)
                {
                    this.SuspendLayout();
                    m_bIsResizing = true;
                    treeDocContents.Refresh();

                    //resize control width - setting the anchor property does not work
                    //set control width based on scroll bar visibility
                    int iControlWidth = 0;
                    if (this.ScrollBarsVisible)
                        iControlWidth = this.treeDocContents.Width -
                            m_oCurCtl.Left - 20;
                    else
                        iControlWidth = this.treeDocContents.Width -
                            m_oCurCtl.Left - 5;

                    //When TaskPane in Initializing in Word 2003, width may not yet be finalized
                    //when Initial control is selected.  Set to arbitrary width to avoid problems
                    if (iControlWidth <= 0)
                        iControlWidth = 192;

                    if (this.btnContacts.Visible)
                        iControlWidth = iControlWidth - (this.btnContacts.Width + 1);

                    m_oCurCtl.Width = iControlWidth;

                    //Reposition floating CI button
                    if (this.btnContacts.Visible)
                        this.btnContacts.Left = m_oCurCtl.Left + m_oCurCtl.Width + 1;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
                m_bIsResizing = false;
            }
        }

        //GLOG : 6188 : CEH
        void m_oMenuSegment_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Assignments oAssignments = null;
                System.Windows.Forms.Application.DoEvents();

                //Segment oSeg = (Segment)m_oCurNode.Tag;
                //THESE LINES ARE DISABLED BECAUSE THEY CAN CAUSE SPORADIC DOCUMENT LOCK-UP
                //m_oMenuSegment.Hide();
                //this.Refresh();
                switch (e.ClickedItem.Name)
                {
                    case "m_oMenuSegment_RecreateSegment":
                        try
                        {
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;

                            this.ExitEditMode();

                            Segment oSeg = this.TaskPane.GetTargetSegment();

                            if (oSeg != null)
                            {
                                //recreate segment
                                this.TaskPane.DocEditor.RecreateSegment(oSeg);
                            }
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        } 
                        break;
                    case "m_oMenuSegment_SaveData":
                        try
                        {
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;

                            ExitEditMode();

                            Segment oSeg = this.TaskPane.GetTargetSegment();

                            if (oSeg != null)
                            {
                                //create Saved Data from segment
                                this.TaskPane.DocEditor.SaveData(oSeg);
                            }
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        } 
                        break;
                    case "m_oMenuSegment_SaveContent":
                        try
                        {
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;

                            ExitEditMode();

                            Segment oSeg = this.TaskPane.GetTargetSegment();

                            if (oSeg != null)
                            {
                                LMP.MacPac.Application.SaveSegmentWithData(oSeg);
                            }
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        } 
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// After selecting help the focus needs to 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oHelp_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.m_oCurCtl != null)
            {
                this.m_oCurCtl.Focus();
            }
        }

        /// <summary>
        /// Floating CI button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnContacts_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_oCurCtl == null)
                    return;

                GetContacts(m_oCurCtl, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void DataReviewer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F3)
            {
                // Toggle to the next task pane component.
                this.TaskPane.Toggle();
            }
        }

        private void picMenu_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                // GLOG : 2985 : JAB
                // Set the m_oCurNode if it is null. This situation is visible when 
                // a saved document is opened and the picMenu is clicked.

                if (m_oCurNode != null)
                {
                    ShowContextMenu(m_oCurNode.Bounds.X + 40, m_oCurNode.Bounds.Y + 10);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handles event raised by SetupDistributedSegmentSections variable action
        /// by refreshing the segment node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_SegmentDistributedSectionsChanged(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                Segment oSegment = oArgs.Segment;
                //GLOG 4025: If Segment is Transparent, Refresh Parent Node
                while (oSegment.Parent != null && oSegment.IsTransparent)
                    oSegment = oSegment.Parent;
                RefreshSegmentNode(oSegment.FullTagID);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnRecreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                {
                    this.TaskPane.DocEditor.RefreshTree();
                    this.RefreshTree();
                }

                this.ExitEditMode();

                Segment oSeg = this.TaskPane.GetTargetSegment();

                if (oSeg != null)
                {
                    //recreate segment
                    this.TaskPane.DocEditor.RecreateSegment(oSeg);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnSaveData_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                {
                    this.TaskPane.DocEditor.RefreshTree();
                    this.RefreshTree();
                }

                ExitEditMode();

                Segment oSeg = this.TaskPane.GetTargetSegment();

                if (oSeg != null)
                {
                    //create Saved Data from segment
                    this.TaskPane.DocEditor.SaveData(oSeg);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************methods*********************
        public void ResizeTreeControl()
        {
        }

        private void Setup()
        {
            //cycle through top level segments, presenting
            //the snapshots of each in the tree
            for (int i = 0; i < this.ForteDocument.Segments.Count; i++)
            {
                Segment oSegment = this.ForteDocument.Segments[i];
                AddSegmentNodeIfNecessary(oSegment, null);
            }

            ExpandTopLevelNodes();
        }

        /// <summary>
        /// Selects the first variable node in the tree
        /// </summary>
        internal void SelectFirstTreeNode()
        {
            try
            {
                //Set focus to first variable node
                if (this.treeDocContents.Nodes.Count > 0)
                {
                    if (this.treeDocContents.Nodes[0].Nodes.Count > 0)
                        SelectNode(this.treeDocContents.Nodes[0].Nodes[0], true);
                    else
                    {
                        //If only node is Segment with chooser, make sure chooser is displayed
                        m_oCurNode = null;
                        SelectNode(this.treeDocContents.Nodes[0], true);
                    }
                }

                if (m_oCurCtl != null)
                {
                    m_oCurCtl.Focus();
                    m_oCurCtl.Select();
                }
                //Make sure scrollbar is at top - additional space for control may have 
                //changed scrollbar state
                this.treeDocContents.TopNode = this.treeDocContents.Nodes[0];
            }
            catch { }
        }

        /// <summary>
        /// populates the supplied list with a list of expanded node keys
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="oNodes"></param>
        private void GetExpandedNodes(List<string> oList, TreeNodesCollection oNodes)
        {
            foreach (UltraTreeNode oNode in oNodes)
            {
                if (oNode.Expanded)
                {
                    //add node key to list
                    oList.Add(oNode.Key);

                    //add expanded child nodes
                    GetExpandedNodes(oList, oNode.Nodes);
                }
            }
        }
        /// <summary>
        /// refreshes the document editor based on the current document
        /// </summary>
        public void RefreshTree()
        {
            RefreshTree(false);
        }
        public void RefreshTree(bool bExpandTopLevel)
        {
            Color oTreeBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            GradientStyle oTreeGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            RefreshTree(bExpandTopLevel, oTreeBackcolor, oTreeGradient, false);
        }

        public void RefreshTree(bool bExpandTopLevel, bool bRefreshUIOnly)
        {
            Color oTreeBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            GradientStyle oTreeGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            RefreshTree(bExpandTopLevel, oTreeBackcolor, oTreeGradient, bRefreshUIOnly);
        }

        public void RefreshTree(Color oBackcolor, GradientStyle oGradient)
        {
            RefreshTree(false, oBackcolor, oGradient, false);
        }

        public void RefreshTree(bool bExpandTopLevel, Color oBackcolor,
            GradientStyle oGradient, bool bRefreshUIOnly)
        {
            bool bScreenUpdating = this.TaskPane.ScreenUpdating;
            this.TaskPane.ScreenUpdating = false;

            ExitEditMode();
            
            try
            {
                //get collection of expanded nodes
                List<string> oExpandedNodes = new List<string>();
                GetExpandedNodes(oExpandedNodes, this.treeDocContents.Nodes);

                this.RefreshTopLevelNodes(bExpandTopLevel);
                
                //restore nodes to expansion state
                foreach (string xNodeKey in oExpandedNodes)
                {
                    UltraTreeNode oNode = this.treeDocContents.GetNodeByKey(xNodeKey);
                    if (oNode != null && !(oNode.Tag is Variable) &&
                        !(oNode.Tag is Authors) && !(oNode.Tag is Block) &&
                        !(oNode.Tag is Jurisdictions) && !oNode.Key.EndsWith(".Language"))
                        oNode.Expanded = true;
                }

                LMP.MacPac.Application.SetXMLTagStateForEditing();
            }
            finally
            {
                if (bScreenUpdating)
                    this.TaskPane.ScreenUpdating = bScreenUpdating;

            }
        }
        public void RemoveTag()
        {
            //JTS 5/10/10: 10.2
            bool bContentControl = m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML;
            string xMsg;
            if (bContentControl)
                xMsg = LMP.Resources.GetLangString("Prompt_RemoveContentControl");
            else
                xMsg = LMP.Resources.GetLangString("Prompt_RemoveTag");
            DialogResult iRes = MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (iRes == DialogResult.Yes)
            {
                ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                try
                {
                    if (m_oCurNode.Tag is Variable)
                    {
                        Variable oVar = (Variable)m_oCurNode.Tag;

                        if (oVar.IsTagless)
                        {
                            if (bContentControl)
                            {
                                foreach (Word.ContentControl oCC in oVar.Segment.ContentControls)
                                {
                                    if (String.GetBoundingObjectBaseName(oCC.Tag) == "mSEG")
                                    {
                                        //clear out all variable definitions
                                        //GLOG 4441: Make sure ObjectData is decrypted if necessary
                                        string xValue =  LMP.Forte.MSWord.WordDoc.GetAttributeValue_CC(oCC, "ObjectData");
                                        xValue = Regex.Replace(xValue, "VariableDefinition=" + oVar.ID + @".*?\|", "");

                                        //write to document
                                        LMP.Forte.MSWord.WordDoc.SetAttributeValue_CC(
                                            oCC, "ObjectData", xValue, LMP.Data.Application.EncryptionPassword);
                                    }
                                }
                            }
                            else
                            {
                                foreach (Word.XMLNode oTag in oVar.Segment.WordTags)
                                {
                                    if (oTag.BaseName == "mSEG")
                                    {
                                        //clear out all variable definitions
                                        //GLOG 4441: Make sure ObjectData is decrypted if necessary
                                        string xValue = String.Decrypt(oTag.SelectSingleNode("@ObjectData", "", false).Text);
                                        xValue = Regex.Replace(xValue, "VariableDefinition=" + oVar.ID + @".*?\|", "");

                                        //write to document
                                        LMP.Forte.MSWord.WordDoc.SetAttributeValue(
                                            oTag, "ObjectData", xValue, LMP.Data.Application.EncryptionPassword);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (bContentControl)
                            {
                                foreach (Word.ContentControl oCC in oVar.AssociatedContentControls)
                                    LMP.Forte.MSWord.WordDoc.RemoveContentControls(oCC,
                                        LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, true);
                            }
                            else
                            {
                                foreach (Word.XMLNode oTag in oVar.AssociatedWordTags)
                                    LMP.Forte.MSWord.WordDoc.RemoveTags(oTag,
                                        LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, true);
                            }
                        }
                    }
                    if (m_oCurNode.Tag is Block)
                    {
                        Block oBlock = (Block)m_oCurNode.Tag;
                        if (bContentControl)
                            LMP.Forte.MSWord.WordDoc.RemoveContentControls(oBlock.AssociatedContentControl, 
                                LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, true);
                        else
                            LMP.Forte.MSWord.WordDoc.RemoveTags(oBlock.AssociatedWordTag,
                                LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, true);
                    }

                    //GLOG item #4294/4354 -
                    //prevent code from seeing variable as having
                    //been edited - otherwise, variable actions
                    //may be executed when refreshing below, 
                    //causing an error - dcf
                    IControl oICtl = (IControl)this.m_oCurCtl;
                    if(oICtl != null)
                        oICtl.IsDirty = false;

                    TaskPane.Refresh(false, true, false);
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = oEvents;
                }
            }
        }
        
        ///// <summary>
        ///// removes the non-segment tags in the specified segment
        ///// </summary>
        ///// <param name="oSegment"></param>
        ///// <param name="bSkipSegmentTags"></param>
        //public void RemoveTags(Segment oSegment)
        //{
        //    DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString("Prompt_RemoveVarAndBlockTags"),
        //        LMP.ComponentProperties.ProductName,
        //        MessageBoxButtons.YesNo,
        //        MessageBoxIcon.Question);

        //    if (iRes == DialogResult.Yes)
        //    {
        //        LMP.MacPac.Application.RemoveBoundingObjects(oSegment, mpseg);

        //        //8-18-11 (dm) - delete tagless variables
        //        string xTagID = oSegment.FullTagID;
        //        oSegment = this.ForteDocument.FindSegment(xTagID);
        //        oSegment.DeleteVariables();
        //        this.RefreshTree(false, true);
        //    }
        //}
        /// <summary>
        /// Refresh the backcolor and gradient of the tree and the help view.
        /// </summary>
        /// <param name="oBackcolor"></param>
        /// <param name="oGradient"></param>
        public void RefreshAppearance(Color oBackcolor, GradientStyle oGradient)
        {
            // Set the tree backcolor and gradient appearance per user's prefs.
            if (oBackcolor == null)
            {
                oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            }

            if (oGradient == null)
            {
                oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                    GradientStyle.BackwardDiagonal : GradientStyle.None;
            }

            if (this.treeDocContents.Appearance.BackColor != oBackcolor)
            {
                // Set the backcolor
                this.treeDocContents.Appearance.BackColor = oBackcolor;
            }

            if (this.treeDocContents.Appearance.BackGradientStyle != oGradient)
            {
                //set the gradient according to the user's preference.
                this.treeDocContents.Appearance.BackGradientStyle = oGradient;
            }
        }
        /// <summary>
        /// this method cycles through all the m_oControlHash, and 
        /// refreshes the author lists of all controls of type LMP.Controls.AuthorSelector
        /// </summary>
        public void RefreshAuthorControls()
        {
            foreach (IControl oCtl in m_oControlsHash.Keys)
                if (oCtl is AuthorSelector)
                    ((AuthorSelector)oCtl).RefreshPeopleList(); 
        }
        public void SetTreeAppearance()
        {            
            // Set the backcolor and gradient of the tree according to the user's prefs
            this.treeDocContents.Appearance.BackColor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;

            GradientStyle oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            //set the gradient according to the user's preference.
            this.treeDocContents.Appearance.BackGradientStyle = oGradient;
        }

        //GLOG item #6258 - dcf
        /// <summary>
        /// sets up the tree for display 
        /// if not already set up
        /// </summary>
        public void SetupTreeIfNecessary()
        {
            if (this.treeDocContents.Nodes.Count == 0)
                RefreshTree(true);
        }
        /// <summary>
        /// expands all top level nodes in the tree
        /// </summary>
        public void ExpandTopLevelNodes()
        {
            //cycle through top level nodes, expanding each
            foreach (UltraTreeNode oNode in this.treeDocContents.Nodes)
                oNode.Expanded = true;
        }
        /// <summary>
        /// collapses tree to initial state
        /// </summary>
        public void CollapseAll(UltraTreeNode oNode)
        {
            if (oNode == null)
            {
                //Collapse all top level nodes
                foreach (UltraTreeNode oTopNode in this.treeDocContents.Nodes)
                    CollapseAll(oTopNode);
                this.treeDocContents.Nodes[0].Expanded = true;
            }
            else
            {
                //Collapse node branch recursively
                for (int i = 0; i < oNode.Nodes.Count; i++)
                {
                    if (oNode.Nodes[i].HasExpansionIndicator)
                        CollapseAll(oNode.Nodes[i]);
                }
                oNode.Expanded = false;
            }
        }
        public void CollapseAll()
        {
            ExitEditMode();
            this.treeDocContents.SuspendLayout();
            foreach (UltraTreeNode oNode in this.treeDocContents.Nodes)
            {
                oNode.Expanded = false;
            }
            this.treeDocContents.ActiveNode = this.treeDocContents.Nodes[0];
            this.treeDocContents.ResumeLayout();
            this.treeDocContents.Refresh();
        }
        /// <summary>
        /// Expand all nodes
        /// </summary>
        public void ExpandAll()
        {
            UltraTreeNode oNode = m_oCurNode;
            ExitEditMode();
            this.treeDocContents.SuspendLayout();
            this.treeDocContents.ExpandAll();
            this.treeDocContents.ActiveNode = oNode;
            this.treeDocContents.ResumeLayout();
            this.treeDocContents.Refresh();
        }
        /// <summary>
        /// Selects first variable node under Selected Segment node
        /// </summary>
        /// <param name="oSegment"></param>
        

        /// <summary>
        /// Determine if the first variable node is selected.
        /// </summary>
        internal bool IsFirstVariableNodeSelected
        {
            get
            {
                TreeNodesCollection oNodes = this.treeDocContents.Nodes;
                return oNodes.Count > 0 && oNodes[0].Nodes.Count > 0 &&
                    this.m_oCurNode == oNodes[0].Nodes[0] && m_oCurCtl != null && m_oCurCtl.Focused;
            }
        }

        /// <summary>
        /// Selects the first variable node in the tree
        /// </summary>
        

        /// <summary>
        /// Selects the last variable node in the tree
        /// </summary>
        
        /// <summary>
        /// refreshes top level nodes in tree
        /// </summary>
        public void RefreshTopLevelNodes(bool bExpandTop)
        {
            DateTime t0 = DateTime.Now;
            bool bScreenUpdating = false;
            
            try
            {
                //get screenupdating state for later use
                bScreenUpdating = this.TaskPane.ScreenUpdating;

                this.TaskPane.ScreenUpdating = false;

                //display top level nodes - first clear tree
                this.treeDocContents.Nodes.Clear();
                this.m_oFinishedValues.Clear();

                //clear out current node references
                this.m_oCurNode = null;
                this.m_oCurCtl = null;
                this.m_oCurTabNode = null;

                //empty controls Hash Table
                m_oControlsHash.Clear();
                //GLOG 3589: Empty Hotkeys Hashtable
                m_oHotkeys.Clear();

                //clear transparent node list
                //GLOG 3482 - added leading pipe
                m_xTransparentNodes = "|";

                //get top level nodes for all segments
                Segments oSegments = this.m_oMPDocument.Segments;
                
                //cycle through segments, adding each as a node to tree
                //cycle through top level segments, presenting
                //the snapshots of each in the tree
                for (int i = 0; i < this.ForteDocument.Segments.Count; i++)
                {
                    Segment oSegment = this.ForteDocument.Segments[i];
                    //GLOG 8108: Make sure m_oFinishedValues contains all child segments,
                    //even if not initially displayed in the tree
                    AddFinishedValuesIfNecessary(oSegment);
                    UltraTreeNode oTreeNode = AddSegmentNodeIfNecessary(oSegment, null);

                    if (oTreeNode != null)
                    {
                        if (bExpandTop)
                        {
                            oTreeNode.Expanded = true;
                        }
                    }
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = bScreenUpdating;
                LMP.Benchmarks.Print(t0);
            }
        }
        //GLOG 8108
        private void AddFinishedValuesIfNecessary(Segment oParentSegment)
        {
            if (Snapshot.Exists(oParentSegment))
            {
                if (!m_oFinishedValues.ContainsKey(oParentSegment.FullTagID))
                    m_oFinishedValues.Add(oParentSegment.FullTagID, oParentSegment.Snapshot.Values);
            }
            for (int i = 0; i < oParentSegment.Segments.Count; i++)
            {
                AddFinishedValuesIfNecessary(oParentSegment.Segments[i]);
            }

        }
        /// <summary>
        /// sets the text of the value node of a variable
        /// </summary>
        /// <param name="xVariableTreeNodeKey"></param>
        /// <param name="oVariable"></param>
        public void SetVariablesNodeValue(string xVariableTreeNodeKey, Variable oVariable)
        {
            //get node with the specified tag id
            UltraTreeNode oNode = this.GetNodeFromTagID(xVariableTreeNodeKey);

            bool bNodeIsInView = oNode.IsInView;

            //ensure that the node is visible
            this.EnsureVisibleNode(oNode);

            //if(oNode.NextVisibleNode != null)
            //    //set value of associated value node
            //    oNode.NextVisibleNode.Text = this.GetVariableUIValue(oVariable);

            //GLOG #3820 - dcf
            if (!bNodeIsInView)
                this.EnsureHiddenNode(oNode);
        }
        /// <summary>
        /// refreshes the portion of the tree
        /// starting with the specified segment tree node
        /// </summary>
        /// <param name="xTagID"></param>
        public void RefreshSegmentNode(string xTagID)
        {
            UltraTreeNode oNode = null;

            //get segment node - if it's not found, simply skip refresh -
            //this method may be called while the segment is still initializing
            //and before it's been added to the tree
            try
            {
                oNode = this.treeDocContents.GetNodeByKey(xTagID);
            }
            catch { }

            //refresh segment node
            if (oNode != null)
            {
                this.pnlDocContents.SuspendLayout();
                this.treeDocContents.SuspendLayout();

                try
                {
                    RefreshSegmentNode(oNode, true);
                }
                finally
                {
                    this.treeDocContents.ResumeLayout();
                    this.pnlDocContents.ResumeLayout();
                }
            }
        }
        /// <summary>
        /// deletes the nodes with the specified tag id
        /// </summary>
        /// <param name="xTagID"></param>
        public void DeleteNode(string xTagID)
        {
            UltraTreeNode oNode = null;

            try
            {
                //if this is the tag id of a transparent segment, just remove from
                //list of transparent node
                if (this.NodeIsTransparent(xTagID))
                {
                    m_xTransparentNodes = m_xTransparentNodes.Replace(xTagID + '|', "");
                    return;
                }

                //get node
                try
                {
                    oNode = this.GetNodeFromTagID(xTagID, false);
                }
                catch { }

                if (oNode != null)
                {
                    UltraTreeNode oParentNode = oNode.Parent;
                    //GLOG 2923: Segment nodes handled separately
                    if ((this.treeDocContents.Nodes.Count == 1) &&
                        (oParentNode == null) && !(oNode.Tag is Segment))
                    {
                        //this is the only node on the tree - to avoid error,
                        //use Clear() instead of Remove()
                        this.treeDocContents.Nodes.Clear();
                    }
                    else
                    {
                        //switch target to More node if this is the only node on it
                        if (oParentNode != null)
                        {
                            if ((oParentNode.Nodes.Count == 1) &&
                                (oParentNode.Key.IndexOf(".Advanced") != -1))
                                oNode = oParentNode;
                        }

                        if (oNode.Tag is Variable)
                        {
                            //remove associated control from hashtable
                            UltraTreeNode oValueNode = oNode.Nodes[0];
                            if ((oValueNode != null) && (oValueNode.Tag != null))
                            {
                                //GLOG 2645: Need to unsubscribe events, otherwise they'll be duplicated
                                //next time ValueNode tag is set in EnterVariableEditMode
                                IControl oICtl = oValueNode.Tag as IControl;
                                if (oICtl != null)
                                {
                                    oICtl.TabPressed -= oIControl_TabPressed;
                                    if (oICtl is ICIable)
                                    {
                                        ((ICIable)oICtl).CIRequested -= GetContacts;
                                        //Enable double-clicking to Get Contacts
                                        ((Control)oICtl).DoubleClick -= OnControlDoubleClick;
                                    }
                                }
                                m_oControlsHash.Remove(oValueNode.Tag);

                                //hide control - added 1/26/09 (dm) because active control
                                //was sometimes still displayed after the deletion of its node,
                                //leading to errors if the user then entered a value in it
                                ((Control)oValueNode.Tag).Visible = false;
                            }
                        }
                        else if (oNode.Tag is Segment)
                        {
                            //GLOG 2923: Remove Chooser if currently displayed
                            if ((m_oCurNode == oNode))
                            {
                                if (m_oCurCtl is Chooser)
                                {
                                    this.treeDocContents.Controls.Remove(m_oCurCtl);
                                    m_oControlsHash.Remove(m_oCurCtl);
                                    //Hide Context Menu button
                                    m_oCurCtl = null;
                                }
                                //Hide Context Menu button
                                this.picMenu.Visible = false;
                            }
                            if (this.treeDocContents.Nodes.Count == 1 && oParentNode == null)
                            {
                                //this is the only node on the tree - to avoid error,
                                //use Clear() instead of Remove()
                                this.treeDocContents.Nodes.Clear();
                                return;
                            }
                        }

                        //GLOG 3367 - delete hotkey
                        if ((oNode.Tag != null) && !(oNode.Tag is Segment))
                            this.DeleteHotkey(oNode);

                        try
                        {
                            //remove node
                            oNode.Remove();
                        }
                        catch (System.ArgumentOutOfRangeException)
                        {
                            //due to a bug in UltraTreeNode, this exception may be raised (for no
                            //apparent reason) in the course of removing multiple nodes -
                            //it works to simply call the Remove() method again
                            oNode.Remove();
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotSelectDocContentsTreeNode") + xTagID, oE);
            }
        }
        /// <summary>
        /// inserts node for specified variable on node for specified segment
        /// at the location indicated by its execution index
        /// </summary>
        /// <param name="oVar"></param>
        public UltraTreeNode InsertVariableNode(string xVarDisplayName, string xVarName, mpControlTypes iVarControl, 
            string xControlProperties, string xParentSegmentTagID)
        {
            UltraTreeNode oVarNode = null;

            return oVarNode;
        }

        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oParent"></param>
        public UltraTreeNode InsertSegmentNode(Segment oSegment)
        {
            return InsertSegmentNode(oSegment, null);
        }

        public UltraTreeNode InsertSegmentNode(Segment oSegment, UltraTreeNode oParentNode)
        {
            DateTime t0 = DateTime.Now;

            //add node
            UltraTreeNode oNode = null;

            //use segment type name if component -
            //otherwise, use segment display name
            //GLOG : 15813 : ceh - get display name for Document components that are of type Architect
            string xNodeText = (oSegment.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent && oSegment.TypeID != mpObjectTypes.Architect) ?
                LMP.Data.Application.GetObjectTypeDisplayName(oSegment.TypeID) : oSegment.DisplayName;

            if (oSegment.HasVariables(true) || oSegment.HasBlocks(true))
            {
                xNodeText += LMP.Resources.GetLangString("Text_SegmentNotFinished");
            }

            if (oParentNode == null)
            {
                oNode = treeDocContents.Nodes.Add(oSegment.FullTagID, xNodeText);
            }
            else
            {
                oNode = oParentNode.Nodes.Add(oSegment.FullTagID, xNodeText);
            }

            //GLOG item #5959 - dcf
            //GLOG 6669: Make sure Segment has a Snapshot, even if unfinished state includes no Variables or Blocks
            //JTS 4/15/13: Original condition here would never evaluate true, since IsFinished would always be false if no Snapshot exists
            if (!Snapshot.Exists(oSegment)) // (oSegment.IsFinished && !Snapshot.Exists(oSegment))
            {
                Snapshot.Create(oSegment);
                oSegment.Snapshot.Refresh();
            }

            oNode.Tag = oSegment.Snapshot.Configuration;

            //add image
            Images iImage = 0;
            switch (oSegment.IntendedUse)
            {
                case mpSegmentIntendedUses.AsDocument:
                    iImage = Images.ValidDocumentSegment;
                    break;
                case mpSegmentIntendedUses.AsDocumentComponent:
                    iImage = Images.ValidComponentSegment;
                    break;
                case mpSegmentIntendedUses.AsParagraphText:
                default:
                    iImage = Images.ValidTextBlockSegment;
                    break;
            }

            oNode.LeftImages.Add(iImage);

            //set properties
            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Always;
            oOverride.NodeSpacingAfter = 10;

            //GLOG 7126 (dm) - subscribe to AfterAuthorUpdated event
            oSegment.AfterAuthorUpdated += new AfterAuthorUpdatedHandler(oSeg_UpdatedForAuthor);

            oNode.Enabled = oSegment.IsFinished || (!oSegment.HasBlocks(true) && !oSegment.HasVariables(true));

            LMP.Benchmarks.Print(t0);

            return oNode;
        }

        /// <summary>
        /// Returns named variable to be displayed in tree
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        Variable GetDisplayedVariable(Segment oSegment, string xName)
        {
            Variable oVar = null;
            try
            {
                oVar = oSegment.Variables.ItemFromName(xName);
            }
            catch { }
            if (oVar == null)
            {
                //Also search recursively through transparent children
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    if (oSegment.Segments[i].IsTransparent)
                    {
                        oVar = GetDisplayedVariable(oSegment.Segments[i], xName);
                    }
                    if (oVar != null)
                        break;
                }
            }
            return oVar;
        }
        /// <summary>
        /// Returns named block to be displayed in tree
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        Block GetDisplayedBlock(Segment oSegment, string xName)
        {
            Block oBlock = null;
            try
            {
                oBlock = oSegment.Blocks.ItemFromName(xName);
            }
            catch { }
            if (oBlock == null)
            {
                //Also search recursively through transparent children
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    if (oSegment.Segments[i].IsTransparent)
                    {
                        oBlock = GetDisplayedBlock(oSegment.Segments[i], xName);
                    }
                    if (oBlock != null)
                        break;
                }
            }
            return oBlock;
        }
        /// <summary>
        /// executes code that must be run
        /// when the user selects a Word tag
        /// </summary>
        /// <param name="Sel"></param>
        /// <param name="OldXMLNode"></param>
        /// <param name="NewXMLNode"></param>
        /// <param name="Reason"></param>
        
        /// <summary>
        /// shows the specified control, under the specified tree node
        /// </summary>
        
        internal void ShowTreeControl(Control oControl, UltraTreeNode oTreeNode, bool bShowCIButton)
        {
            DateTime t0 = DateTime.Now;

            int iControlWidth = 0;

            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (m_bShowValueNodes)
                oValueNode = oTreeNode.Nodes[0];
            else
                oValueNode = oTreeNode;

            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                oControl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            this.treeDocContents.Refresh();

            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
            System.Windows.Forms.Application.DoEvents();

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - 20;
            else
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - 5;

            //When TaskPane in Initializing in Word 2003, width may not yet be finalized
            //when Initial control is selected.  Set to arbitrary width to avoid problems
            if (iControlWidth <= 0)
                iControlWidth = 192;
            
            if (bShowCIButton)
                iControlWidth = iControlWidth - (this.btnContacts.Width + 1);


            //scroll control into view, if necessary
            if (oTreeNode.Bounds.Top + oTreeNode.Bounds.Height + oControl.Height > 
                this.treeDocContents.ClientRectangle.Bottom)
            {
                //bottom of control won't be visible -
                //scroll to top of next node, if it exists
                UltraTreeNode oNode = oTreeNode.NextVisibleNode;

                //skip value nodes - value node tags are IControls
                while (oNode.Tag is IControl)
                {
                    if (oNode.NextVisibleNode != null)
                        oNode = oNode.NextVisibleNode;
                    else
                        break;
                }

                oNode.BringIntoView();
            }

            //the user selected a task pane node
            //position control to display over variable value
            oControl.SetBounds(oValueNode.Bounds.Left,
                Math.Max(oTreeNode.Bounds.Top, 0) + oTreeNode.Bounds.Height,
                iControlWidth, oControl.Height);

            //Position floating CI button to right of textbox
            if (bShowCIButton)
            {
                this.btnContacts.Left = oControl.Left + oControl.Width + 1;
                this.btnContacts.Top = oControl.Top + 6;
                this.btnContacts.Visible = true;
            }

            //add control to container
            this.treeDocContents.Controls.Add(oControl);

            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();

            //ensure that control is visible - controls that
            //have been displayed in the wizard are not visible
            //at this time
            if (!oControl.Visible)
                oControl.Visible = true;

            if (m_bShowValueNodes)
            {
                //clear value node text for display
                oValueNode.Text = "";
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// deactivates all of the variables that
        /// are children of the node with the specified id
        /// </summary>
        /// <param name="xTagID"></param>
        public void SetNodeActivation(string xTagID, bool bActivate)
        {
            UltraTreeNode oParentNode = this.GetNodeFromTagID(xTagID);
            SetNodeActivation(oParentNode, bActivate);
        }
        private void SetNodeActivation(UltraTreeNode oNode, bool bActivate)
        {
            for(int i=0; i<oNode.Nodes.Count; i++)
            {
                SetNodeActivation(oNode.Nodes[i], bActivate);
            }

            ////if activating, update variable value node
            //if (bActivate && (oNode.Tag is Variable))
            //    oNode.Nodes[0].Text = this.GetVariableUIValue((Variable)oNode.Tag);

            if (oNode.Tag is Variable || oNode.Tag is Authors)
                oNode.Enabled = bActivate;
        }
        #endregion
        #region *********************private functions*********************
        private void ShowContextMenuButton(UltraTreeNode oNode)
        {
            // GLOG : 1908 : JAB
            // Limit the location left coordinate so that it does not
            // overlay the scrollbar.
            int iMargin = IsTreeContentVerticalScrollbarVisible ? 17 : 2;
            int iMaxLeft = this.treeDocContents.Right - iMargin - this.picMenu.Width;
            this.picMenu.Left = oNode.Bounds.X + oNode.Bounds.Width + 4;

            if (picMenu.Left > iMaxLeft)
            {
                picMenu.Left = iMaxLeft;
            }

            this.picMenu.Top = oNode.Bounds.Y + 11;
            this.picMenu.Visible = true;
        }
        private string GetEmptyValueDisplay(){
            if(m_xEmptyDisplayValue == null)
                m_xEmptyDisplayValue = LMP.Resources.GetLangString("Msg_EmptyValueDisplayValue");

            return m_xEmptyDisplayValue;
        }
        private void GetContacts(object sender, EventArgs e)
        {
            try
            {
                //GLOG item #5933 - dcf
                if (sender is LMP.Controls.Detail && ((LMP.Controls.Detail)sender).MaxEntitiesReached)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_MaxContactsReached"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);

                    return;
                }

                m_bCIIsActive = true;
                string xKey = this.m_oCurNode.Key;
                string xConfig = this.m_oCurNode.Tag.ToString();
                int iPos1 = xConfig.IndexOf("CI=\"");
                int iPos2 = xConfig.IndexOf("\"", iPos1 + 4);
                string xCIConfig = xConfig.Substring(iPos1 + 4, iPos2 - iPos1 - 4);

                //get max entities from the config string
                int iMaxContacts = 0;
                iPos1 = xConfig.IndexOf("MaxEntities=");

                if (iPos1 > -1)
                {
                    //property specification found -
                    //value could be succeeded by either a separator
                    //or a double quote
                    iPos2 = xConfig.IndexOf("�", iPos1);

                    if (iPos2 == -1)
                        iPos2 = xConfig.IndexOf("\"", iPos1);

                    string xMaxContacts = xConfig.Substring(iPos1 + 12, iPos2 - iPos1 - 12);
                    iMaxContacts = int.Parse(xMaxContacts);
                }

                //iPos2 = 
                int iContactCount = 0;

                if (sender is ICIable)
                    iContactCount = ((ICIable)sender).ContactCount;

                //get contact detail
                string xDetail = Contacts.GetDetail(xCIConfig, this.CurrentSegment, iContactCount + 1, iMaxContacts);

                //If CI was called from a control, update that control's value
                if (sender is IControl  && !(sender is LMP.Controls.TreeView))
                {
                    IControl oICtl = (IControl)sender;
                    if (!string.IsNullOrEmpty(xDetail))
                    {
                        oICtl.Value += xDetail;
                        oICtl.IsDirty = true;
                    }

                    //GLOG item #4584 - dcf
                    //fix for GLOG item #4557 caused this item
                    UltraTreeNode oSelNode = this.treeDocContents.GetNodeByKey(xKey);
                    //this.SelectNode(oSelNode);
                    //GLOG 5211: Make sure focus remains in original control
                    if (m_oCurCtl != null)
                        m_oCurCtl.Focus();
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
                this.TaskPane.ScreenRefresh();
                m_bCIIsActive = false;
            }
        }

        //GLOG : 6188 : CEH
        /// <summary>
        /// shows the appropriate context menu for the specified point
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void ShowContextMenu(int x, int y)
        {
            //Make sure any current control changes are saved
            //this.ExitEditMode();
            UltraTreeNode oNode = this.treeDocContents.GetNodeFromPoint(x, y);
            if (oNode == null)
                return;

            m_oCurNode = oNode;
            this.treeDocContents.ActiveNode = oNode;

            //GLOG 6829 (dm) - oNode will no longer exist if SelectNode
            //discovered that primary bookmark was missing
            if (this.treeDocContents.ActiveNode == null)
                return;

            this.CreateSegmentMenu();
            m_oMenuSegment.Show(this.treeDocContents, x, y);

        }
        private void OnCIDialogRelease(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();
                this.TaskPane.ScreenUpdating = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// refreshes the portion of the tree
        /// starting with the specified segment tree node
        /// if necessary
        /// </summary>
        /// <param name="oNode"></param>
        public void RefreshSegmentNode(UltraTreeNode oNode)
        {
            this.RefreshSegmentNode(oNode, false);
        }
        /// <summary>
        /// refreshes the portion of the tree
        /// starting with the specified segment tree node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh">forces the node to refresh, even if the node already has children</param>
        private void RefreshSegmentNode(UltraTreeNode oSegmentNode, bool bForceRefresh)
        {
            m_bIsRefreshingSegmentNode = true;

            try
            {
                DateTime t0 = DateTime.Now;

                //clear controls hash table
                m_oControlsHash.Clear();

                //exit if node is the same
                if (oSegmentNode == this.m_oCurNode && !bForceRefresh)
                    return;

                //clear any existing child nodes
                oSegmentNode.Nodes.Clear();

                //load variable config xml from the segment node tag
                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(oSegmentNode.Tag.ToString());
                XmlNodeList oNodes = oXML.SelectNodes("/Segment/Variable");

                //GLOG 7126 (dm) - add authors node if configuration data
                //exists to support it
                Segment oSegment = this.ForteDocument.FindSegment(oSegmentNode.Key);
                if ((oXML.SelectSingleNode("//Variable/@DefaultValue") != null) &&
                    (oSegment.IsTopLevel || (!oSegment.LinkAuthorsToParent))
                    && (oSegment.MaxAuthors > 0))
                {
                    //get authors node key
                    string xKey = oSegment.FullTagID + ".Authors";

                    //get appropriate display name for node
                    string xDisplayName = "";
                    //GLOG 6653
                    if (oSegment.AuthorsNodeUILabel == "Author" || oSegment.AuthorsNodeUILabel == "")
                    {
                        if (oSegment.MaxAuthors == 1)
                            xDisplayName = LMP.Resources.GetLangString("Msg_Author");
                        else
                            xDisplayName = LMP.Resources.GetLangString("Msg_Authors");
                    }
                    else if (oSegment.AuthorsNodeUILabel == "Attorney")

                        if (oSegment.MaxAuthors == 1)
                            xDisplayName = LMP.Resources.GetLangString("Msg_Attorney");
                        else
                            xDisplayName = LMP.Resources.GetLangString("Msg_Attorneys");
                    else
                    {
                        xDisplayName = oSegment.AuthorsNodeUILabel;
                    }

                    //add authors node
                    UltraTreeNode oAuthorsNode = oSegmentNode.Nodes.Add(xKey, xDisplayName);

                    //initialize new node
                    InitializeNode(oSegment.Authors, oAuthorsNode);
                }
                
                //cycle through variable config xml nodes,
                //adding variable nodes to the tree
                UltraTreeNode oMoreNode = null;
                foreach (XmlNode oXMLNode in oNodes)
                {
                    if (oXMLNode.Attributes["Display"].Value == "true")
                    {
                        //create new node
                        UltraTreeNode oNode = new UltraTreeNode(oSegmentNode.Key + "." +
                            String.RestoreXMLChars(oXMLNode.Attributes["Name"].Value, true),
                            String.RestoreXMLChars(oXMLNode.Attributes["DisplayName"].Value, true)); //GLOG 6429

                        oNode.Tag = oXMLNode.OuterXml;

                        if (oXMLNode.Attributes["DisplayLevel"].Value == "2")
                        {
                            //display in "More" node - add
                            //"More" node if necessary
                            try
                            {
                                oMoreNode = oSegmentNode.Nodes[oSegmentNode.Key + ".Advanced"];
                            }
                            catch { }

                            if (oMoreNode == null)
                            {
                                oMoreNode = CreateMoreNode(oSegmentNode);
                            }

                            oMoreNode.Nodes.Add(oNode);
                        }
                        else
                        {
                            if (oMoreNode != null)
                            {
                                oSegmentNode.Nodes.Insert(oMoreNode.Index, oNode);
                            }
                            else
                            {
                                oSegmentNode.Nodes.Add(oNode);
                            }
                        }

                        InitializeVariableNode(oNode, oXMLNode.Attributes["HotKey"].Value);
                    }
                }

                //cycle through all nodes, executing the
                //ValueChanged actions - this sets up the nodes
                //based on the initial variable values
                //Segment oSegment = this.ForteDocument.FindSegment(oSegmentNode.Key); //GLOG 7126 (dm)
                for (int i = 0; i < oSegmentNode.Nodes.Count; i++)
                    ExecuteControlActions(oSegmentNode.Nodes[i],
                        ControlActions.Events.ValueChanged, oSegment);

                //add child segments to tree
                AddChildSegmentNodes(oSegmentNode);

                LMP.Benchmarks.Print(t0);
            }
            finally
            {
                m_bIsRefreshingSegmentNode = false;
            }
        }

        public UltraTreeNode CreateMoreNode(UltraTreeNode oSegmentNode)
        {
            UltraTreeNode oMoreNode = oSegmentNode.Nodes.Insert(oSegmentNode.Nodes.Count,
                oSegmentNode.Key + ".Advanced",
                LMP.Resources.GetLangString("Prompt_AdvancedVariables"));
            oMoreNode.Tag = oMoreNode.Parent.Tag;
            oMoreNode.LeftImages.Add(Images.ValidVariableValue);
            oMoreNode.Expanded = false;

            Override oOverride = oMoreNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Always;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.ToggleExpansion;

            return oMoreNode;
        }
        /// <summary>
        /// adds oSegment's child segments to oNode
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        private void AddChildSegmentNodes(UltraTreeNode oParentSegmentNode)
        {
            Segment oSegment = this.ForteDocument.FindSegment(oParentSegmentNode.Key);

            //cycle through child segments, adding each to tree
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                //add node
                AddSegmentNodeIfNecessary(oSegment.Segments[i], oParentSegmentNode);
            }
        }
        /// <summary>
        /// adds the segment node of the specified segment
        /// to the tree if the segment has a snapshot
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oParentNode"></param>
        /// <returns></returns>
        private UltraTreeNode AddSegmentNodeIfNecessary(Segment oSegment, UltraTreeNode oParentNode)
        {
            Segment oNonTransparentSegment = null;
            UltraTreeNode oNode = null;

            //if segment that is defined as transparent has multiple
            //children of the same type, change to non-transparent
            if (oSegment.IsTransparent)
            {
                for (int j = 0; j < oSegment.Segments.Count; j++)
                {
                    if (!oSegment.Segments[j].IsOnlyInstanceOfType)
                    {
                        oSegment.IsTransparent = false;
                        oNonTransparentSegment = oSegment;
                        break;
                    }
                }
            }

            if (oSegment.IsTransparent)
            {
                //GLOG 7126 (dm) - add to list of transparent nodes if necessary
                if (!this.NodeIsTransparent(oSegment.FullTagID))
                    m_xTransparentNodes += oSegment.FullTagID + '|';

                //cycle through children, adding to tree
                for (int j = 0; j < oSegment.Segments.Count; j++)
                {
                    Segment oChild = oSegment.Segments[j];

                    //GLOG item #6283 - dcf
                    if (!m_oFinishedValues.ContainsKey(oChild.FullTagID))
                    {
                        //get values for future node initialization
                        m_oFinishedValues.Add(oChild.FullTagID, oChild.Snapshot.Values);
                    }
                    else
                    {   //update to most recent snapshot values
                        m_oFinishedValues[oChild.FullTagID] = oChild.Snapshot.Values;
                    }

                    if (oChild.IsFinished && oChild.IsTransparent)
                    {
                        //child is transparent too - add to parent
                        RefreshTransparentNode(oChild, oParentNode);
                    }
                    else
                    {
                        //insert the child segment node
                        oNode = InsertSegmentNode(oChild, oParentNode);
                    }
                }

                return oNode;
            }
            else
            {
                //add node
                oNode = InsertSegmentNode(oSegment, oParentNode);

                if (Snapshot.Exists(oSegment))
                {
                    //add values for 
                    //GLOG 8108
                    if (!m_oFinishedValues.ContainsKey(oSegment.FullTagID))
                        m_oFinishedValues.Add(oSegment.FullTagID, oSegment.Snapshot.Values);
                }

                if (oSegment is CollectionTable)
                {
                    //cycle through children, adding to tree
                    for (int j = 0; j < oSegment.Segments.Count; j++)
                    {
                        Segment oChild = oSegment.Segments[j];

                        //GLOG 8108
                        if (!m_oFinishedValues.ContainsKey(oChild.FullTagID))
                            //get values for future node initialization
                            m_oFinishedValues.Add(oChild.FullTagID, oChild.Snapshot.Values);

                        //insert the child segment node
                        InsertSegmentNode(oChild, oNode);
                    }
                }

                return oNode;
            }
        }
        /// <summary>
        /// execute all actions defined for the specified event,
        /// or, if no event has been specified, all actions
        /// </summary>
        /// <param name="oEvent"></param>
        private void ExecuteControlActions(UltraTreeNode oNode, ControlActions.Events oEvent, Segment oSegment)
        {
            List<ControlAction> oCtlActions = new List<ControlAction>();

            if (NodeType(oNode) == NodeTypes.Variable)
            {
                //get control actions
                object oNodeTag = oNode.Tag;

                //get actions defs of the specified node
                //GLOG 6321:  Don't try to get SubString if Tag doesn't contain CtlActionDefs
                string xActionDefs = "";
                int iPos = oNodeTag.ToString().IndexOf("CtlActionDefs=");
                if (iPos > -1)
                {
                    int iPos2 = oNodeTag.ToString().IndexOf("\"", iPos + 15);
                    xActionDefs = oNodeTag.ToString().Substring(iPos + 15, iPos2 - (iPos + 15));
                }

                if (!string.IsNullOrEmpty(xActionDefs))
                {
                    string xCtlValue = null;
                    IControl oICtl = oNode.Nodes[0].Tag as IControl;

                    if (oICtl != null)
                        xCtlValue = oICtl.Value;
                    else
                        xCtlValue = oNode.Nodes[0].Tag.ToString();

                    string[] aActionDefs = xActionDefs.Split('|');

                    //cycle through action defs, acting on those
                    //actions of type SetVariableValue (id=7)
                    foreach (string xActionDef in aActionDefs)
                    {
                        string[] aActionDef = xActionDef.Split('�');
                        ControlAction oAction = new ControlAction(oSegment);
                        oAction.Type = (ControlActions.Types)Enum.Parse(typeof(ControlActions.Types), aActionDef[3]);
                        oAction.Parameters = aActionDef[4];
                        oAction.ExecutionCondition = aActionDef[1];
                        oAction.Event = (ControlActions.Events)Enum.Parse(typeof(ControlActions.Events), aActionDef[2]);

                        oCtlActions.Add(oAction);
                    }
                }
            }

            //cycle through actions in collection and execute
            foreach(ControlAction oAction in oCtlActions)
            {
                //execute only if the action has beeen specified
                //to run on the specified event, or if
                //no event has been specified
                if (oEvent == 0 || oAction.Event == oEvent)
                {
                    //end execution if specified
                    if ((oAction.Type == ControlActions.Types.EndExecution) &&
                        (oAction.ExecutionIsSpecified()))
                        return;

                    try
                    {
                        string xValue = null;

                        if (oNode.Nodes[0].Tag is IControl)
                            xValue = ((IControl)oNode.Nodes[0].Tag).Value;
                        else
                            xValue = oNode.Nodes[0].Tag.ToString();

                        ExecuteControlAction(oAction,xValue);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.ActionException(
                            LMP.Resources.GetLangString("Error_CouldNotExecuteAction") +
                            oAction.ID.ToString(), oE);
                    }
                }
            }
        }

        /// <summary>
        /// executes author control actions - added for GLOG 7126 (dm)
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <param name="oEvent"></param>
        private void ExecuteControlActions(Authors oAuthors, ControlActions.Events oEvent)
        {
            //cycle through actions in collection and execute
            ControlActions oActions = oAuthors.ControlActions;
            for (int i = 0; i < oActions.Count; i++)
            {
                ControlAction oAction = oActions[i];

                //execute only if the action has beeen specified
                //to run on the specified event, or if
                //no event has been specified
                if (oEvent == 0 || oAction.Event == oEvent)
                {
                    if (oAction.Type == ControlActions.Types.EndExecution)
                        //GLOG 7069: Only return if EndExecution type is encountered
                        return;
                    else if (!oAction.ExecutionIsSpecified())
                    {
                        //GLOG 7069: If condition not met, continue with next action
                        continue;
                    }

                    try
                    {
                        ExecuteAuthorControlAction(oAction);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.ActionException(
                            LMP.Resources.GetLangString("Error_CouldNotExecuteAction") +
                            oAction.ID.ToString(), oE);
                    }
                }
            }
        }

        /// <summary>
        /// executes the specified author control action - added for GLOG 7126 (dm)
        /// </summary>
        /// <param name="oAction"></param>
        private void ExecuteAuthorControlAction(ControlAction oAction)
        {
            DateTime t0 = DateTime.Now;

            string xParameters = oAction.Parameters;

            Trace.WriteNameValuePairs("xParameters", xParameters);

            //exit if condition for insertion is not satisfied
            if (!oAction.ExecutionIsSpecified())
                return;

            try
            {
                switch (oAction.Type)
                {
                    case ControlActions.Types.ChangeLabel:
                        ExecuteControlActionChangeLabel(oAction, xParameters);
                        break;
                    case ControlActions.Types.Enable:
                        ExecuteControlActionEnable(oAction, xParameters);
                        break;
                    case ControlActions.Types.ExecuteControlActionGroup:
                        break;
                    case ControlActions.Types.RunMethod:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            oAction.Execute();
                        break;
                    case ControlActions.Types.SetControlProperty:
                        break;
                    case ControlActions.Types.SetFocus:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionSetFocus(oAction, xParameters);
                        break;
                    case ControlActions.Types.SetVariableValue:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionSetVariableValue(oAction, xParameters);
                        break;
                    case ControlActions.Types.Visible:
                        ExecuteControlActionVisible(oAction, xParameters);
                        break;
                    case ControlActions.Types.DisplayMessage:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionDisplayMessage(oAction, xParameters);
                        break;
                    case ControlActions.Types.RefreshControl:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionRefreshControl(oAction, xParameters);
                        break;
                    default:
                        break;
                }
            }
            catch (System.Exception oE)
            {
                //action failed
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_ControlActionFailed"), oE);
            }

            LMP.Benchmarks.Print(t0, Convert.ToString(oAction.Type),
                "Parameters=" + xParameters);
        }

        private void ExecuteControlAction(ControlAction oAction, string xVarValue)
        {
            DateTime t0 = DateTime.Now;

            string xParameters = oAction.Parameters;

            Trace.WriteNameValuePairs("xParameters", xParameters);

            //exit if condition for insertion is not satisfied
            if (!oAction.ExecutionIsSpecified(xVarValue))
                return;

            if (xParameters != "")
            {
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                //They will treated as the literal characters by the actions
                xVarValue = Expression.MarkLiteralReservedCharacters(xVarValue);

                xParameters = FieldCode.EvaluateMyValue(xParameters, xVarValue);
            }

            try
            {
                switch (oAction.Type)
                {
                    case ControlActions.Types.ChangeLabel:
                        ExecuteControlActionChangeLabel(oAction, xParameters);
                        break;
                    case ControlActions.Types.Enable:
                        ExecuteControlActionEnable(oAction, xParameters);
                        break;
                    case ControlActions.Types.SetFocus:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionSetFocus(oAction, xParameters);
                        break;
                    case ControlActions.Types.SetVariableValue:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionSetVariableValue(oAction, xParameters);
                        break;
                    case ControlActions.Types.Visible:
                        ExecuteControlActionVisible(oAction, xParameters);
                        break;
                    case ControlActions.Types.DisplayMessage:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionDisplayMessage(oAction, xParameters);
                        break;
                    case ControlActions.Types.RefreshControl:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionRefreshControl(oAction, xParameters);
                        break;
                    default:
                        break;
                }
            }
            catch (System.Exception oE)
            {
                //action failed
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_ControlActionFailed"), oE);
            }

            LMP.Benchmarks.Print(t0, Convert.ToString(oAction.Type),
                "Parameters=" + xParameters);
        }

        private void ExecuteControlActionEnable(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";
            mpTriState bEnable = mpTriState.Undefined;

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = oSegment.FullTagID + "." + Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    string xEnable = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);

                    switch (xEnable.ToUpper())
                    {
                        case "1":
                        case "TRUE":
                            bEnable = mpTriState.True;
                            break;
                        case "0":
                        case "FALSE":
                            bEnable = mpTriState.False;
                            break;
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //get target node from variable
                if (bEnable != mpTriState.Undefined)
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        // Check for node matching Segment and Variable
                        oNode = this.GetNodeFromTagID(xTargetVarPath);
                    }
                    catch { }

                    if (oNode != null)
                        //node exists - set node enabled state
                        oNode.Enabled = (bEnable == mpTriState.True);
                    //else
                    //    //node does not exist - add to the list of
                    //    //pending control actions - the action will
                    //    //be executed when the node is first 
                    //    //created and initialized
                    //    AddPendingControlAction(oVar.TagID, oAction);
                }
            }
        }
        private void ExecuteControlActionSetFocus(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = oSegment.FullTagID + "." + Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                UltraTreeNode oNode = null;

                try
                {
                    // Check for node matching Segment and Variable
                    oNode = this.GetNodeFromTagID(xTargetVarPath);
                }
                catch { }

                if (oNode != null)
                {
                    //node exists - set Focus
                    SelectNode(oNode, false);
                }
            }
        }
        private void ExecuteControlActionSetVariableValue(ControlAction oAction, string xParameters)
        {
            if (oAction.ParentSegment.CreationStatus != Segment.Status.Finished)
                return;

            string xTargetVarPath = "";
            string xNewValue = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = oSegment.FullTagID + "." + Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    xNewValue = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //GLOG 6677: Avoid error if specified Variable doesn't exist
                UltraTreeNode oNode = null;
                try
                {
                    // Check for node matching Segment and Variable
                    oNode = this.GetNodeFromTagID(xTargetVarPath);
                }
                catch { }

                if (oNode == null)
                    return;

                //if control exists, set control value, else store new value in value node tag
                IControl oICtl = oNode.Nodes[0].Tag as IControl;
                if ((oICtl != null) && (oICtl.Value != xNewValue))
                {
                    oICtl.Value = xNewValue;
                }
                else
                {
                    oNode.Nodes[0].Tag = xNewValue;
                }

                //update snapshot with edited value
                UpdateSnapshot(oNode);

                //set node text to variable value
                oNode.Nodes[0].Text = this.GetVariableUIValue(oNode.Nodes[0], xNewValue);

                //run finished data variable actions
                ExecuteFinishedDataVariableActions(oNode, oSegment);
            }
        }
        protected void ExecuteControlActionDisplayMessage(ControlAction oAction, string xParameters)
        {
            string xMsg = "";
            string xCaption = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xMsg = Expression.Evaluate(aParams[0], oSegment, m_oMPDocument);
                    xCaption = Expression.Evaluate(aParams[1], oSegment, m_oMPDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xMsg", xMsg, "xCaption", xCaption);

            if (xMsg != "")
            {
                if (xCaption == "")
                    xCaption = LMP.ComponentProperties.ProductName;
                MessageBox.Show(xMsg, xCaption, MessageBoxButtons.OK);
            }
        }
        private void ExecuteControlActionVisible(ControlAction oAction, string xParameters)
        {
            //GLOG - 3395 - CEH
            string xTargetVarPath = "";
            mpTriState bVisible = mpTriState.Undefined;
            string xName = "";
            string[] aNames = null;

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //GLOG - 3395 - CEH
                    //Allow for more multiple variable names
                    //get parameters
                    xName = aParams[0];
                    //Can be list of Variable names
                    aNames = xName.Split(',');

                    string xVisible = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);

                    switch (xVisible.ToUpper())
                    {
                        case "1":
                        case "TRUE":
                            bVisible = mpTriState.True;
                            break;
                        case "0":
                        case "FALSE":
                            bVisible = mpTriState.False;
                            break;
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }

                Trace.WriteNameValuePairs("xName", xName);

                //Cycle through each variable
                foreach (string xVar in aNames)
                {
                    xTargetVarPath = oSegment.FullTagID + "." + Expression.Evaluate(xVar,
                        oSegment, m_oMPDocument);

                    if (bVisible != mpTriState.Undefined)
                    {
                        UltraTreeNode oNode = null;
                        try
                        {
                            // Check for node matching Segment and Variable
                            oNode = this.GetNodeFromTagID(xTargetVarPath);
                        }
                        catch { }

                        if (oNode != null)
                            //node exists - set node visible state
                            oNode.Visible = (bVisible == mpTriState.True);
                        //else
                        //    //node does not exist - add to the list of
                        //    //pending control actions - the action will
                        //    //be executed when the node is first 
                        //    //created and initialized
                        //    AddPendingControlAction(oVar.TagID, oAction);
                    }
                }
            }
        }
        /// <summary>
        /// GLOG 2645: Update control properties that contain MacPac expressions
        /// </summary>
        /// <param name="oAction"></param>
        /// <param name="xParameters"></param>
        private void ExecuteControlActionRefreshControl(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = oSegment.FullTagID + "." + Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
                //GLOG 6677: Avoid error if specified Variable doesn't exist
                UltraTreeNode oNode = null;
                try
                {
                    // Check for node matching Segment and Variable
                    oNode = this.GetNodeFromTagID(xTargetVarPath);
                }
                catch { }
                if (oNode == null)
                    return;

                IControl oICtl = oNode.Nodes[0].Tag as IControl;

                if (oICtl == null)
                {
                    return;
                }

                string xCtlValue = oICtl.Value;

                Control oCtl = (Control)oICtl;
                System.Type oCtlType = oCtl.GetType();

                //get control properties from variable
                int iPos = oNode.Tag.ToString().IndexOf("ControlProperties=");
                int iPos2 = oNode.Tag.ToString().IndexOf("\"", iPos + 19);
                string xProps = oNode.Tag.ToString().Substring(iPos + 19, iPos2 - iPos - 19);
                string[] aProps = null;
                aProps = xProps.Split(StringArray.mpEndOfSubValue);

                //cycle through property name/value pairs, setting those that 
                //have a MacPac Expression as the value
                for (int i = 0; i < aProps.Length; i++)
                {
                    iPos = aProps[i].IndexOf("=");
                    string xName = aProps[i].Substring(0, iPos);
                    string xValue = aProps[i].Substring(iPos + 1);

                    //evaluate control prop value, which may be a MacPac expression
                    string xEval = Expression.Evaluate(xValue, oSegment, m_oMPDocument);
                    //Only set those properties using expressions
                    if (xValue != xEval)
                    {
                        PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                        System.Type oPropType = oPropInfo.PropertyType;

                        object oValue = null;

                        if (oPropType.IsEnum)
                            oValue = Enum.Parse(oPropType, xEval);
                        else
                            oValue = Convert.ChangeType(xEval, oPropType,
                                LMP.Culture.USEnglishCulture);

                        oPropInfo.SetValue(oCtl, oValue, null);
                    }
                }
            }
        }
        private void ExecuteControlActionChangeLabel(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";
            string xNewLabel = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = oSegment.FullTagID + "." + Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    xNewLabel = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);
                    if (xNewLabel == "Null")
                        xNewLabel = "";

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                UltraTreeNode oNode = null;
                try
                {
                    // Check for node matching Segment and Variable
                    oNode = this.GetNodeFromTagID(xTargetVarPath);
                }
                catch { }

                if (oNode != null)
                    //node exists - set node enabled state
                    oNode.Text = xNewLabel;
                //else
                //    //node does not exist - add to the list of
                //    //pending control actions - the action will
                //    //be executed when the node is first 
                //    //created and initialized
                //    AddPendingControlAction(oVar.TagID, oAction);
            }
        }


        /// <summary>
        /// adds members of oSegment to oNode
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oHostNode"></param>
        private void RefreshTransparentNode(Segment oSegment, UltraTreeNode oHostNode)
        {
            //get segment associated with host segment tag id
            Segment oHost = this.TaskPane.ForteDocument.FindSegment(oHostNode.Key);

            if (oHost == null)
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_NodeIsNotSegmentNode"));

            ////get More node
            UltraTreeNode oMoreNode = null;

            //GLOG 4025: Place Transparent Segment nodes before any Nodes for other Child Segments
            UltraTreeNode oChildSegNode = null;
            foreach (UltraTreeNode oNode in oHostNode.Nodes)
            {
                if (oNode.Tag is Segment)
                {
                    oChildSegNode = oNode;
                    break;
                }
            }
            //load variable config xml from the segment node tag
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml(oSegment.Snapshot.Configuration);
            XmlNodeList oNodes = oXML.SelectNodes("/Segment/Variable");

            //cycle through variable config xml nodes,
            //adding variable nodes to the tree
            foreach (XmlNode oXMLNode in oNodes)
            {
                if (oXMLNode.Attributes["Display"].Value == "true")
                {
                    UltraTreeNode oNode = new UltraTreeNode(oSegment.FullTagID + "." +
                        oXMLNode.Attributes["Name"].Value,
                        oXMLNode.Attributes["DisplayName"].Value);

                    oNode.Tag = oXMLNode.OuterXml;

                    if (oXMLNode.Attributes["DisplayLevel"].Value == "2")
                    {
                        //display in "More" node - add
                        //"More" node if necessary
                        try
                        {
                            oMoreNode = oHostNode.Nodes[oHostNode.Key + ".Advanced"];
                        }
                        catch { }

                        if (oMoreNode == null)
                        {
                            oMoreNode = CreateMoreNode(oHostNode);
                        }

                        oMoreNode.Nodes.Add(oNode);
                    }
                    else
                    {
                        if (oMoreNode != null)
                        {
                            oHostNode.Nodes.Insert(oMoreNode.Index, oNode);
                        }
                        else
                        {
                            oHostNode.Nodes.Add(oNode);
                        }
                    }

                    InitializeVariableNode(oNode, oXMLNode.Attributes["HotKey"].Value);
                }
            }

            ////add child segments
            //AddChildSegmentNodes(oSegment, oHostNode);
        }

        /// <summary>
        /// returns true if the specified tag id represents a transparent segment node
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        private bool NodeIsTransparent(string xTagID)
        {
            //GLOG 3482 - added leading pipe
            return (m_xTransparentNodes.IndexOf('|' + xTagID + '|') > -1);
        }
        /// <summary>
        /// returns the node with the specified tag ID, populating parent nodes if necessary
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        private UltraTreeNode GetNodeFromTagID(string xTagID)
        {
            return GetNodeFromTagID(xTagID, true);
        }
        /// <summary>
        /// returns the node with the specified tag ID -
        /// if bRefreshParentsIfNecessary=TRUE, then first populates parent nodes if necessary -
        /// if bRefreshParentsIfNecessary=FALSE, returns null if parent nodes have not yet been populated
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="bRefreshParentsIfNecessary"></param>
        /// <returns></returns>
        private UltraTreeNode GetNodeFromTagID(string xTagID, bool bRefreshParentsIfNecessary)
        {
            string xPartialTag = "";
            UltraTreeNode oNode = null;
            int iPos = xTagID.IndexOf(".");

            //cycle through tag id separators,
            //referring the node specified by the
            //partial tag
            while (iPos > -1)
            {
                xPartialTag = xTagID.Substring(0, iPos);

                if (!this.NodeIsTransparent(xPartialTag))
                {
                    try
                    {
                        //get the node referred to by the partial tag
                        if (oNode == null)
                        {
                            try
                            {
                                oNode = this.treeDocContents.Nodes[xPartialTag];
                            }
                            catch { }
                            //GLOG 3246: If Key not found, also check Trailers umbrella node
                            if (oNode == null)
                            {
                                oNode = this.treeDocContents.Nodes[mpTrailersNodeKey].Nodes[xPartialTag];
                            }
                        }
                        else
                            oNode = oNode.Nodes[xPartialTag];
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString(
                            "Error_InvalidDocContentsTreeNode") + xPartialTag, oE);
                    }

                    if (oNode.Nodes.Count == 0)
                    {
                        if (bRefreshParentsIfNecessary)
                        {
                            //get any child nodes -
                            //8-18-11 (dm) - added condition that segment actually
                            //has any variable or block nodes to add - this was
                            //necessary to avoid an infinite loop
                            Segment oSegment = oNode.Tag as Segment;
                            if ((oSegment != null) && (oSegment.HasVariables(true) ||
                                oSegment.HasBlocks(true)))
                                this.RefreshSegmentNode(oNode);
                        }
                        else
                            //parent is not yet populated
                            return null;
                    }
                }

                //get next tag id separator
                iPos = xTagID.IndexOf(".", iPos + 1);
            }

            iPos = xTagID.LastIndexOf(".");

            if (iPos == -1)
            {
                try
                {
                    oNode = this.treeDocContents.Nodes[xTagID];
                }
                catch { }
                //GLOG 3246: If Key not found, also check Trailers umbrella node
                if (oNode == null)
                {
                    oNode = this.treeDocContents.Nodes[mpTrailersNodeKey].Nodes[xTagID];
                }
            }
            else
            {
                try
                {
                    //there are separators - get the last tag element
                    oNode = oNode.Nodes[xTagID];
                }
                catch
                {
                    //no var node with the specified key
                    //is located in the "basic" variables section -
                    //look in the advanced section
                    string xAdvancedVarsNode = oNode.Key + ".Advanced";

                    //get advanced vars node
                    oNode = oNode.Nodes[xAdvancedVarsNode];

                    //get the var in the advanced vars node
                    oNode = oNode.Nodes[xTagID];
                }
            }

            //return the node referred to last
            return oNode;
        }
        /// <summary>
        /// returns the top level segment containing the specified node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private Segment GetTopLevelSegmentFromNode(UltraTreeNode oNode)
        {
            //treat toa as top level segment even when contained in a child
            if (oNode.Tag is LMP.Architect.Api.TOA)
                return (Segment)oNode.Tag;
            else if (oNode.Tag is LMP.Architect.Api.Variable && ((Variable)oNode.Tag).Segment is LMP.Architect.Api.TOA)
                return ((Variable)oNode.Tag).Segment;
            else if (oNode.Tag is LMP.Architect.Api.Block && ((Block)oNode.Tag).Segment is LMP.Architect.Api.TOA)
                return ((Block)oNode.Tag).Segment;

            //get segment tag id of current and new nodes
            int iPos = oNode.Key.IndexOf(".");
            string xTagID = null;
            if (iPos == -1)
            {
                xTagID = oNode.Key;
            }
            else
            {
                xTagID = oNode.Key.Substring(0, iPos);
            }

            return this.m_oMPDocument.FindSegment(xTagID);
        }

        /// <summary>
        /// ensures that the specified node is visible in the tree
        /// </summary>
        /// <param name="oTreeNode"></param>
        private void EnsureVisibleNode(UltraTreeNode oTreeNode)
        {
            UltraTreeNode oParent = oTreeNode;

            while (!oTreeNode.IsInView)
            {
                //get parent
                oParent = oParent.Parent;

                //exit if no more parents
                if (oParent == null)
                    break;

                //expand parent
                oParent.Expanded = true;
            }
        }
        /// <summary>
        /// ensures that the specified node is not visible in the tree
        /// </summary>
        /// <param name="oTreeNode"></param>
        //GLOG item #3820 - df
        private void EnsureHiddenNode(UltraTreeNode oTreeNode)
        {
            UltraTreeNode oParent = oTreeNode;

            while (oTreeNode.IsInView)
            {
                //get parent
                oParent = oParent.Parent;

                //exit if no more parents
                if (oParent == null)
                    break;

                //expand parent
                oParent.Expanded = false;
            }
        }
        /// <summary>
        /// sets up edit mode for the specified  
        /// tree node if necessary and appropriate
        /// </summary>
        /// <param name="oTreeNode"></param>
        private void EnterEditMode(UltraTreeNode oTreeNode)
        {
            if (this.m_oCurNode == oTreeNode)
                //node is the same as current node - exit
                return;

            DateTime t0 = DateTime.Now;

            //ensure that node is visible
            //before entering edit mode
            EnsureVisibleNode(oTreeNode);

            //Clear this so any scroll events generated here won't reposition control
            m_oCurNode = null;
            if (NodeType(oTreeNode) == NodeTypes.Variable)
            {
                EnterVariableEditMode(oTreeNode);
            }
            else if (oTreeNode.Tag is Authors) //GLOG 7126 (dm)
            {
                this.HideCallout();
                EnterAuthorsEditMode(oTreeNode);
            }

            //set node as the current node for the edit tab
            this.m_oCurNode = oTreeNode;

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// sets up the tree for editing the variable
        /// with the specified tree node
        /// </summary>
        private void EnterVariableEditMode(UltraTreeNode oTreeNode)
        {
            IControl oICtl = null;
            bool bShowCIButton = false;

            string xConfig = oTreeNode.Tag.ToString();
            XmlDocument oXML = new XmlDocument();
            oXML.LoadXml(xConfig);

            string xControl = oXML.SelectSingleNode("//Variable/@Control").Value;
            string xControlProperties = oXML.SelectSingleNode("//Variable/@ControlProperties").Value;
            bool bCIDisabled = xConfig.ToLower().Contains("ci=\"\"");
            mpControlTypes iControl = (mpControlTypes)int.Parse(xControl);

            //get value node
            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            //GLOG 5799 (dm) - added IsMultiValue property to finished config doc var
            XmlNode oIMVNode = oXML.SelectSingleNode("//Variable/@IsMultiValue");
            bool bIsMultiValue = ((oIMVNode != null) &&
                (oIMVNode.Value.ToUpper() == "TRUE"));

            if (oValueNode.Tag is string)
            {
                //GLOG 5799 (dm) - pass value to CreateAssociatedControl for proper
                //configuration of multivalue control
                string xValue = oValueNode.Tag.ToString();
                oICtl = CreateAssociatedControl(iControl, bIsMultiValue,
                    xControlProperties, xValue);
                oICtl.Value = xValue;
                oICtl.KeyPressed += new KeyEventHandler(OnKeyPressed);
                oICtl.KeyReleased += new KeyEventHandler(OnKeyReleased);
            }
            else
            {
                oICtl = (IControl)oValueNode.Tag;
            }

            if (oValueNode.Tag is string)
            {
                //associated control is not yet a part of the editor -
                //add associated control to tag of value node
                oValueNode.Tag = oICtl;
                m_oControlsHash.Add(oICtl, oValueNode);

                //add subscribe control to dialog events, if necessary
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                //GLOG item #5587 - dcf - limit to one CIRequested subscription per control
                //GLOG item #7266 - dcf - conditioned on control actually being CI Enabled
                if (oICtl is ICIable && !bCIDisabled && !((ICIable)oICtl).IsSubscribedToCI)
                {
                    ((ICIable)oICtl).CIRequested += new CIRequestedHandler(GetContacts);

                    //Enable double-clicking to Get Contacts
                    ((Control)oICtl).DoubleClick += new EventHandler(OnControlDoubleClick);
                }

                ExecuteControlActions(oTreeNode, ControlActions.Events.FirstDisplay, this.CurrentSegment);
            }

            this.m_oCurCtl = (Control)oICtl;

            ShowTreeControl(this.m_oCurCtl, oTreeNode, bShowCIButton);

            ExecuteControlActions(oTreeNode, ControlActions.Events.GotFocus, this.CurrentSegment);

            if (m_oCurCtl != null)
                m_oCurCtl.Focus();
        }

        /// <summary>
        /// creates the associated control for the Variable -
        /// returns the control (for convenience)
        /// </summary>
        /// <param name="oParentControl">the control that hosts this control</param>
        /// <returns></returns>
        public IControl CreateAssociatedControl(mpControlTypes iControlType,
            bool bIsMultiValue, string xControlProperties, string xValue)
        {
            //GLOG 5799 (dm) - added xValue parameter for multivalue control
            DateTime t0 = DateTime.Now;

            try
            {
                Control oCtl = null;
                //create new control
                if (bIsMultiValue)
                {
                    switch (iControlType)
                    {
                        case mpControlTypes.Checkbox:
                        case mpControlTypes.Combo:
                        case mpControlTypes.DropdownList:
                        case mpControlTypes.MultilineCombo:
                        case mpControlTypes.MultilineTextbox:
                        case mpControlTypes.Textbox:
                            //Use MultiValueControl, configured for the selected control type
                            oCtl = new LMP.Controls.MultiValueControl(iControlType);
                            break;
                        default:
                            oCtl = LMP.Architect.Base.Application.CreateControl(iControlType, this);
                            break;
                    }
                }
                else
                {
                    oCtl = LMP.Architect.Base.Application.CreateControl(iControlType, this);
                }

                try
                {
                    IControl oIControl = (IControl)oCtl;

                    //GLOG 5799 (dm) - configure multivalue control
                    if (oCtl is MultiValueControl)
                    {
                        ((MultiValueControl)oCtl).NumberOfValues =
                            LMP.String.CountChrs(xValue, LMP.StringArray.mpEndOfValue) + 1;
                    }

                    //set up control by setting control properties
                    SetControlProperties(oCtl, xControlProperties);

                    //do any control-specific setup of control
                    oIControl.ExecuteFinalSetup();

                    return oIControl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                    LMP.Benchmarks.Print(t0);
                }

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    iControlType.ToString(), oE);
            }
        }
        /// <summary>
        /// sets the properties of the specified control
        /// </summary>
        private void SetControlProperties(Control oCtl, string xPropertyString)
        {
            DateTime t0 = DateTime.Now;

            System.Type oCtlType = oCtl.GetType();

            //set up control properties-
            //get control properties from variable
            string[] aProps = null;

            if (!string.IsNullOrEmpty(xPropertyString))
                aProps = xPropertyString.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);
                
                //evaluate control prop value, which may be a MacPac expression
                xValue = Expression.Evaluate(xValue, this.CurrentSegment, this.ForteDocument);

                PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType,
                        LMP.Culture.USEnglishCulture);

                DateTime t1 = DateTime.Now;
                oPropInfo.SetValue(oCtl, oValue, null);
                LMP.Benchmarks.Print(t1, "SetPropertyValue", xName);
            }
            IControl oICtl = (IControl)oCtl;

            LMP.Benchmarks.Print(t0);
        }

        void OnControlDoubleClick(object sender, EventArgs e)
        {
            try
            {
                GetContacts(sender, new EventArgs());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// sets the browser to the url
        /// </summary>
        private void OnKeyReleased(object sender, KeyEventArgs e)
        {
            try
            {
                // Prevent entering hotkey mode if we are releasing the 
                // control key after using it while selecting text 
                // (eg conrol-shift-right arrow).
                if (IsInControlShiftEditMode && e.KeyValue == 17)
                {
                    IsInControlShiftEditMode = false;
                    this.DisableHotKeyMode = false;
                    return;
                }

                if (this.DisableHotKeyMode)
                    return;

                //Ignore Ctrl in combination with other keys
                if (e.KeyValue == 17 && !m_bHotKeyPressed)
                {
                    //control key press has ended
                    //without a hotkey pressed - 
                    //toggle hotkey mode
                    if (this.m_bInHotkeyMode)
                        ExitHotkeyMode();
                    else
                        EnterHotkeyMode();
                }
                else if (m_bHotKeyPressed)
                {
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    e.SuppressKeyPress = true;
                    e.Handled = true;

                    //GLOG item #5933 - dcf
                    if (sender is LMP.Controls.Detail && ((LMP.Controls.Detail)sender).MaxEntitiesReached)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_MaxContactsReached"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);

                        return;
                    }

                    GetContacts(m_oCurCtl, new EventArgs());

                    // GLOG : 3411 : JAB
                    // Return focus to the current control.
                    if (this.m_oCurCtl != null)
                    {
                        this.m_oCurCtl.Focus();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// executes actions that should occur
        /// when a key is pressed, specifically hotkeys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyPressed(object sender, KeyEventArgs e)
        {
            try
            {
                m_bHotKeyPressed = false;

                if ((m_bInHotkeyMode || (e.Modifiers == Keys.Control)) && (e.KeyCode != Keys.ControlKey))
                {
                    if (e.KeyCode == Keys.F1 && e.Control)
                    {
                        // Control F1 navigates focus to the Find textbox of the content manager.
                        this.TaskPane.ActivateFind();
                    }
                    else if (e.KeyCode == Keys.F3 && e.Control)
                    {
                        this.TaskPane.Toggle();
                    }
                    else
                    {
                        //either we're in hotkey mode, or the control key 
                        //was pressed in conjunction with another key - 
                        //execute hotkey
                        string xKey = ((char)e.KeyValue).ToString().ToUpper();
                        //Process any system-defined keystrokes normally
                        if (@"ABDEFGHIJKLMNOPQRSTUW123456789".Contains(xKey))
                        {
                            //mark that hotkey was pressed
                            m_bHotKeyPressed = true;
                            e.SuppressKeyPress = true;
                            this.ExecuteHotkey(((char)e.KeyValue).ToString());
                        }
                        else if (xKey != "")
                        {
                            //Mark as hotkey, so labels won't toggle when Ctrl is released
                            m_bHotKeyPressed = true;
                        }
                    }
                }
                else if (e.KeyCode == Keys.F5 && e.Modifiers == Keys.None)
                {
                    //GLOG 2768, 8/1/08
                    TaskPanes.RefreshAll();
                    this.RefreshTree();
                    this.Focus();
                }
                else if (e.Control && e.Shift)
                {
                    //Ctrl+Shift shortcuts defined for the control
                    //Home - move to first variable
                    //End - move to last variable
                    //Left - Collapse tree to initial state
                    //Right - Expand all nodes in tree
                    //get state of Home key
                    int iHomeKeyState = LMP.WinAPI.GetKeyState(KEY_HOME);
                    bool bHomeKeyPressed = !(iHomeKeyState == 1 || iHomeKeyState == 0);
                    //get state of End key
                    int iEndKeyState = LMP.WinAPI.GetKeyState(KEY_END);
                    bool bEndKeyPressed = !(iEndKeyState == 1 || iEndKeyState == 0);
                    //get state of Left key
                    int iLeftKeyState = LMP.WinAPI.GetKeyState(KEY_LEFT);
                    bool bLeftKeyPressed = !(iLeftKeyState == 1 || iLeftKeyState == 0);
                    //get state of Right key
                    int iRightKeyState = LMP.WinAPI.GetKeyState(KEY_RIGHT);
                    bool bRightKeyPressed = !(iRightKeyState == 1 || iRightKeyState == 0);
                    //get state of B key (select body)
                    int iBKeyState = LMP.WinAPI.GetKeyState(66);
                    bool bBKeyPressed = !(iBKeyState == 1 || iBKeyState == 0);
                    if (bHomeKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        //GLOG - 3338 - CEH
                        //if (TaskPane.NewSegment.ShowChooser)
                        //    set focus to chooser
                        //    this.SelectChooserNode();
                        //else
                        //    SelectFirstVariableNode();
                    }
                    else if (bEndKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        //SelectLastVariableNode();
                    }
                    else
                    {
                        // The control key is being pressed while editting 
                        // (eg ctrl-shift-right arrow to select text) to 
                        // select text. Enter the IsInControlShiftEditMode 
                        // to prevent the release of the control key activating
                        // the hot key mode.
                        this.DisableHotKeyMode = true;
                        IsInControlShiftEditMode = true;
                    }
                }
                else if (e.Control && e.Alt)
                {
                    if (e.KeyCode == Keys.Up)
                    {
                        this.DisableHotKeyMode = true;
                        this.CollapseAll();
                    }
                    else if (e.KeyCode == Keys.Down)
                    {
                        this.DisableHotKeyMode = true;
                        this.ExpandAll();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void ExecuteHotkey(string xHotkey)
        {
            try
            {
                //get currently selected node for later comparison
                UltraTreeNode oStartingNode = this.m_oCurNode;

                if (this.m_oHotkeys.ContainsKey(xHotkey))
                {
                    //get node associated with the pressed key - 
                    //the hash key can be assigned either a node or
                    //an ArrayList of nodes
                    UltraTreeNode oNode = null;
                    if (this.m_oHotkeys[xHotkey] is UltraTreeNode)
                        oNode = (UltraTreeNode)this.m_oHotkeys[xHotkey];
                    else
                    {
                        //there are multiple nodes assigned to this hotkey - 
                        //get the next one, ie the one after the current node
                        ArrayList oHotkeyNodesArray = (ArrayList) this.m_oHotkeys[xHotkey];
                        //int iCurNodeIndex = this.m_oCurNode.Index;
                        UltraTreeNode oNextNearestNode = null;
                        for (int i = 0; i < oHotkeyNodesArray.Count; i++)
                        {
                            UltraTreeNode oHotkeyNode = (UltraTreeNode)oHotkeyNodesArray[i];
                            //GLOG 3589: Node may not exist anymore if Segment has been refreshed.
                            //Ignore any obsolete Nodes.
                            if (oHotkeyNode.Control != null)
                                //set node as nearest node if it comes after the current
                                //node and either there is no next nearest node or this
                                //node is nearer than the current next nearest node
                                if (NodeIsBelowNode(oHotkeyNode, m_oCurNode) && (oNextNearestNode == null ||
                                    NodeIsBelowNode(oNextNearestNode, oHotkeyNode)))
                                    oNextNearestNode = oHotkeyNode;
                        }

                        if (oNextNearestNode != null)
                            oNode = oNextNearestNode;
                        else
                        {
                            //the next nearest node is before the current node
                            for (int i = 0; i < oHotkeyNodesArray.Count; i++)
                            {
                                UltraTreeNode oHotkeyNode = (UltraTreeNode)oHotkeyNodesArray[i];

                                // GLOG : 553 : JAB
                                // The selected node must be part of the tree control in order
                                // for it to be selectable. A node that is part of the tree has
                                // Control 
                                if (oHotkeyNode.Control != null)
                                {
                                    //set node to first match in the tree
                                    if (oNextNearestNode == null ||
                                        NodeIsBelowNode(oNextNearestNode, oHotkeyNode))
                                        oNextNearestNode = oHotkeyNode;
                                }
                            }

                            oNode = oNextNearestNode;
                        }
                    }

                    if (oNode != null)
                    {
                        //select node
                        //SelectNode(oNode, false);
                        this.treeDocContents.ActiveNode = oNode;
                        oNode.Selected = true;
                        if (m_oCurCtl != null)
                            m_oCurCtl.Focus();
                    }
                }

                if (this.m_oCurNode != oStartingNode)
                    //a new node was selected -
                    //exit hotkey mode
                    ExitHotkeyMode();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UIException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteHotkey") + 
                    xHotkey, oE);
            }
        }
        /// <summary>
        /// refreshes the selected tree node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh"></param>
        private void SelectNode(UltraTreeNode oNode, bool bForceRefresh)
        {
            //get screenupdating state for later use
            if (oNode == null)
            {
                ExitEditMode();
                return;
            }

            bool bScreenUpdating = this.TaskPane.ScreenUpdating;

            if (oNode.Key.EndsWith("_Value"))
            {
                //node is a variable value node - select parent
                this.treeDocContents.ActiveNode = oNode.Parent;
                return;
            }

            try
            {
                //exit if we're clicking the same node or no node is specified
                if (oNode == null || oNode == this.m_oCurNode)
                    return;

                try
                {
                    //exit edit mode - if not already exited
                    UltraTreeNode oParentNode = oNode.Parent;

                    if (m_oCurCtl != null)
                        this.ExitEditMode();

                    ////Clean up any orphaned controls that might occur
                    ////when quickly tabbing through tree
                    //foreach (Control oCtl in this.treeDocContents.Controls)
                    //{
                    //    if (oCtl is IControl && oCtl.Visible)
                    //    {
                    //        UltraTreeNode oValueNode = null;
                    //        try
                    //        {
                    //            //Get corresponding node from Hash table
                    //            oValueNode = (UltraTreeNode)m_oControlsHash[oCtl];
                    //        }
                    //        catch { }
                    //        this.treeDocContents.Controls.Remove(oCtl);
                    //        if (oValueNode != null)
                    //        {
                    //            //Reset Node text and spacing
                    //            oValueNode.Override.NodeSpacingAfter = 10;
                    //            if (oValueNode.Parent.Tag is Variable)
                    //            {
                    //                oValueNode.Text = GetVariableUIValue(oValueNode.Parent, oValueNode.Tag.ToString());
                    //            }
                    //            else if (oValueNode.Parent.Tag is Authors)
                    //            {
                    //                oValueNode.Text = GetAuthorsUIValue((Authors)oValueNode.Parent.Tag);
                    //            }
                    //            else if (oValueNode.Tag is Jurisdictions)
                    //            {
                    //                oValueNode.Text = ((Segment)oValueNode.Parent.Parent.Tag).GetJurisdictionText();
                    //            }
                    //            else if (oValueNode.Tag is Segment)
                    //            {
                    //                //Remove from hash table - the Chooser is recreated each time
                    //                m_oControlsHash.Remove(oCtl);
                    //            }
                    //            else if (oValueNode.Key.EndsWith(".Language_Value"))
                    //            {
                    //                string xCulture = ((Segment)oValueNode.Parent.Parent.Tag).Culture.ToString();
                    //                oValueNode.Text = LMP.Data.Application.GetLanguagesDisplayValue(xCulture);
                    //            }
                    //        }
                    //        this.treeDocContents.Refresh();
                    //    }
                    //}
                }
                catch { }

                //GLOG 3452: The following was moved out of finally block to aid debugging
                while (!oNode.Enabled)
                    oNode = oNode.NextVisibleNode;

                //hide menu button
                this.picMenu.Visible = false;

                if (NodeType(oNode) == NodeTypes.Segment)
                {
                    //expand node based on user preference
                    if (oNode.Level != 0 && !oNode.Expanded)
                        oNode.Expanded = Session.CurrentUser.FirmSettings.ExpandSelectedChildSegment;

                    //scroll into view
                    if (!oNode.IsInView && oNode.Parent != null && oNode.Parent.Expanded)
                    {
                        //Can only set TopNode if parent is expanded
                        this.treeDocContents.TopNode.Expanded = true;
                        this.treeDocContents.TopNode = this.m_oCurNode;
                    }

                    //set up node for editing
                    EnterEditMode(oNode);

                    //set node as the current node for the edit tab
                    this.m_oCurNode = oNode;

                    //GLOG #2873 - dcf - there seems to be a bug with the infragistics controls
                    //such that the parent control is null when closing a document
                    //that is not active - e.g. from the Windows Task Pane -
                    //hence, the condition below
                    //GLOG #3289 - dcf - similar to #2873
                    if (this.treeDocContents.ActiveNode != null &&
                        this.treeDocContents.ActiveNode.Control != null)
                    {
                        //select node
                        this.treeDocContents.ActiveNode = this.m_oCurNode;
                    }
                }
                else if (NodeType(oNode) == NodeTypes.Variable)
                {
                    //node is a variable -
                    //set up edit mode
                    EnterEditMode(oNode);


                    ////set node as the current node for the edit tab
                    //this.m_oCurNode = GetNodeFromTagID(oNode.Key);

                    //GLOG #2873 - dcf - there seems to be a bug with the infragistics controls
                    //such that the parent control is null when closing a document
                    //that is not active - e.g. from the Windows Task Pane -
                    //hence, the condition below -
                    //GLOG 3969 (dm) - added first condition
                    if ((this.treeDocContents.ActiveNode != null) &&
                        (this.treeDocContents.ActiveNode.Control != null))
                    {
                        //select node
                        this.treeDocContents.ActiveNode = this.m_oCurNode;
                    }

                    if (m_oCurCtl != null)
                        m_oCurCtl.Focus();
                }
                else if (oNode.Key.EndsWith("_Value"))
                {
                    //node is a variable value node - select parent
                    this.treeDocContents.ActiveNode = oNode.Parent;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }
            finally
            {
                m_bSelectingNode = false;
                m_oCurTabNode = null;
                if (bScreenUpdating)
                    this.TaskPane.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// Returns true of oNode1 appears below oNode2 in tree
        /// </summary>
        /// <param name="oNode1"></param>
        /// <param name="oNode2"></param>
        /// <returns></returns>
        private bool NodeIsBelowNode(UltraTreeNode oNode1, UltraTreeNode oNode2)
        {
            if (oNode2.IsAncestorOf(oNode1))
                //Parent node has to be above
                return true;
            else if (oNode1.IsAncestorOf(oNode2))
                //Child node has to be below
                return false;
            else if (oNode1.Parent == oNode2.Parent)
            {
                //If both nodes are in same collection, compare Index of each node
                return (oNode1.Index > oNode2.Index);
            }
            else if (oNode1.Level == oNode2.Level)
            {
                //If nodes are at the same level, compare Position of each parent
                return NodeIsBelowNode(oNode1.Parent, oNode2.Parent);
            }
            else if (oNode2.Level > oNode1.Level)
            {
                //If oNode2 is at a lower level, compare Position of oNode1
                //and oNode2.Parent
                return NodeIsBelowNode(oNode1, oNode2.Parent);
            }
            else if (oNode2.Level < oNode1.Level)
            {
                //If oNode1 is at a lower level, compare Position of oNode2
                //and oNode1.Parent
                return NodeIsBelowNode(oNode1.Parent, oNode2);
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// puts the editor into hotkey mode
        /// </summary>
        private void EnterHotkeyMode()
        {
            DateTime t0 = DateTime.Now;

            //cycle through all nodes that have hotkeys
            IEnumerator oEnumerator = this.m_oHotkeys.Values.GetEnumerator();

            bool bNextExists = oEnumerator.MoveNext();
            while (bNextExists)
            {
                if (oEnumerator.Current is UltraTreeNode)
                {
                    //show hotkey for node - ie add hotkey image
                    UltraTreeNode oNode = (UltraTreeNode)oEnumerator.Current;

                    //get the validation image for the node
                    Images oValidationImage = (Images)oNode.LeftImages[0];

                    Images oHotkeyImage = GetHotkeyImage(oNode);

                    if(oHotkeyImage != null)
                        //get add the hotkey image to the node
                        oNode.LeftImages.Add(oHotkeyImage);
                }
                else
                {
                    //value is array list of nodes - cycle through
                    //list adding images to each
                    ArrayList oArrayList = (ArrayList) oEnumerator.Current;

                    for (int i = 0; i < oArrayList.Count; i++)
                    {
                        UltraTreeNode oNode = (UltraTreeNode)oArrayList[i];

                        //get the validation image for the node
                        Images oImage = (Images)oNode.LeftImages[0];

                        Images oHotkeyImage = GetHotkeyImage(oNode);

                        if (oHotkeyImage != null && oHotkeyImage != Images.NoImage)
                            //get add the hotkey image to the node
                            oNode.LeftImages.Add(GetHotkeyImage(oNode));
                    }
                }

                bNextExists = oEnumerator.MoveNext();
            }

            //update value of currently selected variable and control if necessary -
            //removed with GLOG item 2601 - it's not clear why this is necessary
            //when focus is already in the task pane
            //this.UpdateCurrentVariableValueFromDocument();

            LMP.Benchmarks.Print(t0);

            m_bInHotkeyMode = true;
        }
        /// <summary>
        /// takes the editor out of hotkey mode
        /// </summary>
        private void ExitHotkeyMode()
        {
            //cycle through all nodes that have hotkeys
            IEnumerator oEnumerator = this.m_oHotkeys.Values.GetEnumerator();

            bool bNextExists = oEnumerator.MoveNext();
            while (bNextExists)
            {
                if (oEnumerator.Current is UltraTreeNode)
                {
                    //remove hotkey image
                    UltraTreeNode oNode = (UltraTreeNode)oEnumerator.Current;
                    oNode.LeftImages.Remove(GetHotkeyImage(oNode));
                }
                else
                {
                    //value is array list of nodes - cycle through
                    //list adding images to each
                    ArrayList oArrayList = (ArrayList)oEnumerator.Current;

                    for (int i = 0; i < oArrayList.Count; i++)
                    {
                        UltraTreeNode oNode = (UltraTreeNode)oArrayList[i];
                        oNode.LeftImages.Remove(GetHotkeyImage(oNode));
                    }
                }
                bNextExists = oEnumerator.MoveNext();
            }

            m_bInHotkeyMode = false;
        }
        /// <summary>
        /// returns the hotkey image associated with the specified tree node
        /// </summary>
        /// <param name="oNode"></param>
        private Images GetHotkeyImage(UltraTreeNode oNode)
        {
            string xHotkey = "";
            //if (oNode.Tag is LMP.Architect.Api.Authors)
            //    xHotkey = "U";
            //else if (oNode.Tag is Jurisdictions)
            //    xHotkey = "J";
            //else if (oNode.Key.EndsWith(".Language"))
            //    xHotkey = "L";
            //else
            //{
            //    if (oNode.Tag is Variable)
            //        xHotkey = ((Variable)oNode.Tag).Hotkey;
            //    else
            //        xHotkey = ((Block)oNode.Tag).Hotkey;
            //}

            if (NodeType(oNode) == NodeTypes.Variable)
            {
                XmlDocument oXML = new XmlDocument();
                oXML.LoadXml(oNode.Tag.ToString());
                xHotkey = oXML.SelectSingleNode("//Variable/@HotKey").Value;
            }

            if (!System.String.IsNullOrEmpty(xHotkey))
                return (Images)Enum.Parse(typeof(Images), "Hotkey" + xHotkey);
            else
                return Images.NoImage;
        }
        /// <summary>
        /// sets up the tree for editing authors
        /// </summary>
        private void EnterAuthorsEditMode(UltraTreeNode oTreeNode)
        {
            //get authors - assumes that we've already tested for null
            Authors oAuthors = (Authors)oTreeNode.Tag;

            //get value node
            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (oValueNode.Tag == null)
            {
                //there is no control stored in the value node tag -
                //create new control and store in value node tag
                oValueNode.Tag = SetupAuthorControl(oAuthors);
                //Add to hash table
                m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            }
            else
            {
                AuthorSelector oAuthorSelector = oValueNode.Tag as AuthorSelector;
                //set jurisdiction if the bar id column is displayed
                if (oAuthorSelector != null && (oAuthorSelector.ShowColumns & mpAuthorColumns.BarID) > 0)
                {
                    Segment oLevelSegment = oAuthors.Segment;
                    //GLOG 3316: If Pleading Table item, get Court Levels from parent pleading
                    if (oLevelSegment is CollectionTableItem)
                    {
                        while (oLevelSegment.Parent != null)
                            oLevelSegment = oLevelSegment.Parent;
                    }
                    oAuthorSelector.Jurisdiction = new int[] { oLevelSegment.L0, oLevelSegment.L1, oLevelSegment.L2, oLevelSegment.L3, oLevelSegment.L4 };
                }
            }

            //get the control to show
            this.m_oCurCtl = (Control)oValueNode.Tag;

            //add space to display control
            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                this.m_oCurCtl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            //force tree to redraw to get a 
            //valid vertical reading for scroll bars
            this.treeDocContents.Refresh();
            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();

            int iControlWidth = 0;

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - 20;
            else
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - 5;

            //position control to display over variable value
            this.m_oCurCtl.SetBounds(oValueNode.Bounds.Left,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, this.m_oCurCtl.Height);

            //clear value node text for display
            oValueNode.Text = "";
            //oValueNode.Enabled = true;

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;

                oCtl.Value = oAuthors.XML;
                oCtl.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                //add control to container
                this.treeDocContents.Controls.Add(this.m_oCurCtl);

                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }

                //select beginning of segment
                //JTS 6/13/13: Access Segment Range through Bookmark
                Word.Range oRng = oAuthors.Segment.PrimaryRange;
                object oMissing = Missing.Value;
                oRng.StartOf(ref oMissing, ref oMissing);
                oRng.Select();

            }
        }
        /// <summary>
        /// sets up the tree for editing authors
        /// </summary>
        private void EnterJurisdictionEditMode(UltraTreeNode oTreeNode)
        {
            Segment oSegment = (Segment)oTreeNode.Parent.Tag;

            //get value node
            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (oValueNode.Tag == null)
            {
                //there is no control stored in the value node tag -
                //create new control and store in value node tag
                oValueNode.Tag = SetupJurisdictionControl(oSegment);

                //Add to hash table
                m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            }

            //get the control to show
            this.m_oCurCtl = (Control)oValueNode.Tag;

            //add space to display control
            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                this.m_oCurCtl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            //force tree to redraw to get a 
            //valid vertical reading for scroll bars
            this.treeDocContents.Refresh();
            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();

            int iControlWidth = 0;

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - 20;
            else
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - 5;

            //When TaskPane in Initializing in Word 2003, width may not yet be finalized
            //when Initial control is selected.  Set to arbitrary width to avoid problems
            if (iControlWidth <= 0)
                iControlWidth = 192;

            this.treeDocContents.Refresh();
            //position control to display over variable value
            this.m_oCurCtl.SetBounds(oValueNode.Bounds.Left,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, this.m_oCurCtl.Height);

            //clear value node text for display
            oValueNode.Text = "";
            //oValueNode.Enabled = true;

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;
                oCtl.Value = oSegment.GetJurisdictionLevels();
                oCtl.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                //add control to container
                this.treeDocContents.Controls.Add(this.m_oCurCtl);

                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }
            }
        }
        /// <summary>
        /// sets up the tree for editing the segment
        /// </summary>
        /// <param name="oTreeNode"></param>
        private void EnterSegmentEditMode(UltraTreeNode oTreeNode)
        {
            //get segment - assumes that we've already tested for null
            Segment oSeg = (Segment)oTreeNode.Tag;

            //get the control to show
            Chooser oChooser = (Chooser)new Chooser();
            //Add to hash table
            m_oControlsHash.Add(oChooser, oTreeNode);
            oChooser.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oChooser.KeyReleased += new KeyEventHandler(OnKeyReleased);

            // Subscribe to the chooser controls control-left arrow event
            // so that we can collapse the node.
            oChooser.OnCtrlLeftArrowPressed +=
                new Chooser.CtrlLeftArrowPressedHandler(OnChooserCtlLeftArrowPressed);

            // Subscribe to the chooser controls control-right arrow event
            // so that we can expand the node.
            oChooser.OnCtrlRightArrowPressed +=
                new Chooser.CtrlRightArrowPressedHandler(OnChooserCtrlRightArrowPressed);

            Segment oTarget = oSeg.Parent;

            if (oTarget != null)
            {
                //filter assignments based on the containing parent segment
                if ((oTarget is CollectionTable) && (oTarget.Parent != null))
                    oTarget = oTarget.Parent;
                oChooser.TargetObjectType = oTarget.TypeID;
                oChooser.TargetObjectID = oTarget.ID1;
            }
            else if (oSeg is LMP.Architect.Base.IDocumentStamp)
            {
                //Trailers/Document Stamp assignments work differently
                if (Session.CurrentWordApp.Selection.StoryType != Microsoft.Office.Interop.Word.WdStoryType.wdMainTextStory)
                {
                    //GLOG 3251:  Look in main story for top level segment, as selection might
                    //currently be in the trailer itself
                    //GLOG 4215: Don't change current selection - we don't want to close footer if open
                    //GLOG 6930: Get section from Segment Bookmark
                    int iSection = oSeg.PrimaryRange.Sections[1].Index;

                    Word.Range oSecRange = oSeg.ForteDocument.WordDocument.Sections[iSection].Range;
                    object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                    oSecRange.Collapse(ref oDirection);
                    oTarget = oSeg.ForteDocument.GetTopLevelSegmentForRange(oSecRange);
                }
                else
                    oTarget = oSeg.ForteDocument.GetTopLevelSegmentFromSelection();
                Assignments oAssignments = null;
                if (oTarget == oSeg)
                {
                    oTarget = null;

                }
                if (oTarget != null)
                {
                    for (int i = 0; i < oTarget.Segments.Count; i++)
                    {
                        //Check for any Paper-type children with assignments first
                        if (oTarget.Segments[i] is Paper)
                        {
                            //get assigned stamps for the child segment
                            oAssignments = new Assignments(oTarget.Segments[i].TypeID,
                                oTarget.Segments[i].ID1, oSeg.TypeID);
                            break;
                        }
                    }

                    if (oAssignments == null || oAssignments.Count == 0)
                    {
                        //get assigned stamps for the segment if no assignments based on Paper
                        oAssignments = new Assignments(oTarget.TypeID,
                            oTarget.ID1, oSeg.TypeID);
                    }
                }
                const char ITEM_SEP = '~';
                const char NAME_VALUE_SEP = ';';
                string xSegmentList = "";
                AdminSegmentDefs oDefs = new AdminSegmentDefs(oSeg.TypeID);
                //Build segment list from Assignments or Firm application setting default list
                if (oAssignments == null || oAssignments.Count == 0)
                {
                    //if no segment-specific assignments, show types selected
                    //in Firm Application Settings
                    FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                    if (oSeg.TypeID == mpObjectTypes.DraftStamp)
                    {
                        for (int i = 0; i < oSettings.DefaultDraftStampIDs.GetLength(0); i++)
                        {
                            int iID = oSettings.DefaultDraftStampIDs[i];
                            AdminSegmentDef oDef = null;
                            try
                            {
                                oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                            }
                            catch { }
                            if (oDef != null)
                            {
                                xSegmentList += oDef.DisplayName + NAME_VALUE_SEP + oDef.ID.ToString() + ITEM_SEP;
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < oSettings.DefaultTrailerIDs.GetLength(0); i++)
                        {
                            int iID = oSettings.DefaultTrailerIDs[i];
                            AdminSegmentDef oDef = null;
                            try
                            {
                                oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                            }
                            catch { }
                            if (oDef != null)
                            {
                                xSegmentList += oDef.DisplayName + NAME_VALUE_SEP + oDef.ID.ToString() + ITEM_SEP;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < oAssignments.Count; i++)
                    {
                        int iID = ((Assignment)oAssignments.ItemFromIndex(i)).AssignedObjectID;
                        AdminSegmentDef oDef = null;
                        try
                        {
                            oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                        }
                        catch { }
                        if (oDef != null)
                        {
                            xSegmentList += oDef.DisplayName + NAME_VALUE_SEP + oDef.ID.ToString() + ITEM_SEP;
                        }
                    }
                }
                //If this is blank, then chooser will display all available items
                if (xSegmentList != "")
                {
                    xSegmentList = xSegmentList.TrimEnd(ITEM_SEP);
                    oChooser.SegmentsList = xSegmentList;
                }
            }
            else
            {
                //segment is top level and not a document stamp-
                //get all assignments for itself.
                oChooser.TargetObjectType = oSeg.TypeID;
                oChooser.TargetObjectID = oSeg.ID1;
            }

            //set assigned object type
            oChooser.AssignedObjectType = oSeg.TypeID;
            //load list of segments
            oChooser.ExecuteFinalSetup();

            //select chooser value
            oChooser.Value = oSeg.ID1.ToString();

            //set as current control
            this.m_oCurCtl = (Control)oChooser;

            m_bRepositioningControl = true;
            //add space to display control
            oTreeNode.Override.NodeSpacingAfter =
                Math.Max(oTreeNode.NodeSpacingAfterResolved,
                    this.m_oCurCtl.Height);

            //force tree to redraw so that scroll bar
            //visibility will be reflected in UI
            this.treeDocContents.Refresh();
            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();
            int iControlWidth = 0;

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oTreeNode.Bounds.Left - this.treeDocContents.Indent - 20;
            else
                iControlWidth = this.treeDocContents.Width -
                    oTreeNode.Bounds.Left - this.treeDocContents.Indent - 5;

            //scroll control into view, if necessary
            if (oTreeNode.Bounds.Top + oTreeNode.Bounds.Height + this.m_oCurCtl.Height >
                this.treeDocContents.ClientRectangle.Bottom)
            {
                //bottom of control won't be visible -
                //scroll to top of next node, if it exists
                UltraTreeNode oNode = oTreeNode.NextVisibleNode;

                if (oNode != null)
                {
                    //skip value nodes - value node tags are IControls
                    while (oNode.Tag is IControl)
                        oNode = oNode.NextVisibleNode;

                    oNode.BringIntoView();
                }
                else
                {
                    oTreeNode.BringIntoView();
                }
            }

            //position control to display over variable value
            this.m_oCurCtl.SetBounds(oTreeNode.Bounds.Left + this.treeDocContents.Indent,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, this.m_oCurCtl.Height);

            //add control to container
            this.treeDocContents.Controls.Add(this.m_oCurCtl);

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;

                //TODO:set control value

                //mark as not edited
                oCtl.IsDirty = false;

                //hook up to event that handles chooser selection event-
                //if, indeed, the chooser is associated with the only node in the tree
                TreeNodesCollection oNodes = this.treeDocContents.Nodes;
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }
                m_bRepositioningControl = false;
            }
        }

        /// <summary>
        /// sets up the tree for editing language
        /// </summary>
        private void EnterLanguageEditMode(UltraTreeNode oTreeNode)
        {
            Segment oSegment = (Segment)oTreeNode.Parent.Tag;

            //get value node
            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (oValueNode.Tag == null)
            {
                //there is no control stored in the value node tag -
                //create new control and store in value node tag
                oValueNode.Tag = SetupLanguageControl(oSegment);

                //Add to hash table
                m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            }

            //get the control to show
            this.m_oCurCtl = (Control)oValueNode.Tag;

            //add space to display control
            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                this.m_oCurCtl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            //force tree to redraw to get a 
            //valid vertical reading for scroll bars
            this.treeDocContents.Refresh();
            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();

            int iControlWidth = 0;

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - 20;
            else
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - 5;

            //When TaskPane in Initializing in Word 2003, width may not yet be finalized
            //when Initial control is selected.  Set to arbitrary width to avoid problems
            if (iControlWidth <= 0)
                iControlWidth = 192;

            this.treeDocContents.Refresh();
            //position control to display over variable value
            this.m_oCurCtl.SetBounds(oValueNode.Bounds.Left,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, this.m_oCurCtl.Height);

            //clear value node text for display
            oValueNode.Text = "";
            //oValueNode.Enabled = true;

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;
                oCtl.Value = oSegment.Culture.ToString();
                oCtl.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                //add control to container
                this.treeDocContents.Controls.Add(this.m_oCurCtl);

                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }
            }
        }

        void OnChooserCtrlRightArrowPressed(object sender)
        {
            // Expand the nodes in response to a control-right arrow key stroke.
            this.m_oCurNode.Expanded = true;
        }

        void OnChooserCtlLeftArrowPressed(object sender)
        {
            // Collapse the nodes in response to a control-left arrow key stroke.
            this.m_oCurNode.Expanded = false;
        }

        /// <summary>
        /// exits edit mode -
        /// resets the current node in the tree -
        /// i.e. if the current node is a variable,
        /// sets variable value if necessary,
        /// removes associated control, and respaces node
        /// </summary>
        /// <returns>new segment node if changed by a chooser control</returns>
        private void ExitEditMode()
        {
            DateTime t0 = DateTime.Now;

            //exit if there is no current tree node, or if currently tabbing through nodes
            //GLOG 5531 (dm) - this method shouldn't be running before oFocusForm is initialized
            if (this.m_oCurNode == null || this.m_oCurTabNode != null)
            {
                return;
            }

            //JTS 7/26/10: Don't continue if Word Document has already been closed
            try
            {
                string xTestPath = this.ForteDocument.WordDocument.Path;
            }
            catch
            {
                return;
            }

            try
            {
                if (!m_bHotKeyPressed)
                    ExitHotkeyMode();

                //ensure that node is visible
                //before exiting edit mode
                EnsureVisibleNode(this.m_oCurNode);

                Segment oSegment = this.CurrentSegment;

                object oCurNodeTag = this.m_oCurNode.Tag;
                NodeTypes iCurNodeType = NodeType(this.m_oCurNode);

                //get referred variable
                if (iCurNodeType == NodeTypes.Segment || iCurNodeType == NodeTypes.Variable)
                {
                    UltraTreeNode oCurValueNode = null;

                    if ((iCurNodeType != NodeTypes.Segment) || (oCurNodeTag is Authors)) //GLOG 7126 (dm)
                    {
                        //get current value node
                        oCurValueNode = this.m_oCurNode.Nodes[0];

                        if (oCurValueNode == null)
                        {
                            //something is wrong - all variable and author
                            //nodes need to have a child value node
                            throw new LMP.Exceptions.UINodeException(
                                LMP.Resources.GetLangString("Error_MissingValueNode") +
                                this.m_oCurNode.FullPath);
                        }

                        //restore to original space after value
                        if (m_bShowValueNodes)
                            oCurValueNode.Override.NodeSpacingAfter = 10;
                        else
                            this.m_oCurNode.Override.NodeSpacingAfter = 10;
                    }

                    //save Word environment parameters for later restoration
                    LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                        m_oMPDocument.WordDocument);

                    oEnv.SaveState();

                    //deal with current control
                    if (this.m_oCurCtl != null)
                    {
                        this.btnContacts.Visible = false;
                        IControl oCtl = (IControl)this.m_oCurCtl;

                        try
                        {
                            //refresh value node text -
                            if (iCurNodeType == NodeTypes.Variable)
                            {
                                //GLOG 6829 (dm) - oSegment may no longer exist if this method is
                                //called while refreshing after discovering content missing
                                if (oSegment != null)
                                {
                                    //set node text to variable value
                                    oCurValueNode.Text = this.GetVariableUIValue(oCurValueNode, oCtl.Value);

                                    if (oCtl.IsDirty)
                                    {
                                        //update snapshot with edited value
                                        UpdateSnapshot(this.m_oCurNode);

                                        this.CurrentSegment.Snapshot.Refresh();

                                        ExecuteFinishedDataVariableActions(m_oCurNode, oSegment);
                                        ExecuteControlActions(m_oCurNode, ControlActions.Events.ValueChanged, oSegment);
                                    }

                                    ExecuteControlActions(m_oCurNode, ControlActions.Events.LostFocus, oSegment);
                                }
                            }
                            else if (oCurNodeTag is Segment)
                            {
                                if (oCtl.IsDirty)
                                {
                                    oCtl.IsDirty = false;
                                }

                                //restore to original space after value
                                this.m_oCurNode.Override.NodeSpacingAfter = 10;
                            }
                            else if (oCurNodeTag is Authors) //GLOG 7126 (dm)
                            {
                                Authors oAuthors = (Authors)oCurNodeTag;
                                if (oCtl.IsDirty)
                                {
                                    oCtl.IsDirty = false;

                                    //Save copy of current list for FieldCode evaluation
                                    oAuthors.Segment.PreviousAuthors.SetAuthors(oAuthors);

                                    try
                                    {
                                        //control has been edited - update authors value
                                        oAuthors.XML = oCtl.Value;
                                    }
                                    catch{}

                                    //execute actions associated with ValueChanged event
                                    ExecuteControlActions(oAuthors, ControlActions.Events.ValueChanged);
                                }

                                //set node text to authors UI value
                                oCurValueNode.Text = this.GetAuthorsUIValue(oAuthors);

                                //execute actions associated with LostFocus event
                                ExecuteControlActions(oAuthors, ControlActions.Events.LostFocus);
                            }
                        }
                        finally
                        {
                            //GLOG 3507
                            if (m_oCurCtl != null)
                            {
                                //there is a current control - remove from form
                                this.treeDocContents.Controls.Remove(this.m_oCurCtl);

                                //Can't use DoEvents while OnKeyPressed is still processing
                                if (!m_bHotKeyPressed)
                                    System.Windows.Forms.Application.DoEvents();

                                //clear out current control
                                this.m_oCurCtl = null;
                            }
                        }
                    }
                    else
                        //force a redraw
                        this.treeDocContents.Refresh();
                }
            }
            finally
            {
                m_bSuppressTreeActivationHandlers = true;
                this.treeDocContents.ActiveNode = null;
                this.treeDocContents.SelectedNodes.Clear();
                m_bSuppressTreeActivationHandlers = false;
            }

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// runs the SetVariableValue and RunVariableActions 
        /// actions for the specified node
        /// </summary>
        /// <param name="oNode"></param>
        private void ExecuteFinishedDataVariableActions(UltraTreeNode oNode, Segment oSegment)
        {
            object oNodeTag = oNode.Tag;

            //get actions defs of the specified node
            //GLOG 6321:  Don't try to get SubString if Tag doesn't contain ActionDefs
            string xActionDefs = "";
            int iPos = oNodeTag.ToString().IndexOf("ActionDefs=");
            if (iPos > -1)
            {
                int iPos2 = oNodeTag.ToString().IndexOf("\"", iPos + 12);
                xActionDefs = oNodeTag.ToString().Substring(iPos + 12, iPos2 - (iPos + 12));
            }

            if (!string.IsNullOrEmpty(xActionDefs))
            {
                string xCtlValue = ((LMP.Controls.IControl)m_oCurCtl).Value;
                string[] aActionDefs = xActionDefs.TrimEnd('|').Split('|');

                //cycle through action defs, acting on those
                //actions of type SetVariableValue (id=7)
                foreach (string xActionDef in aActionDefs)
                {
                    if (xActionDef == "")
                        continue;

                    string[] aActionDef = xActionDef.Split('�');

                    //exit if condition for insertion is not satisfied
                    string xExecutionCondition = EvaluateExpression(aActionDef[2], xCtlValue, oSegment);

                    if (xExecutionCondition.ToUpper() == "FALSE")
                        continue;

                    if (aActionDef[3] == "19")
                    {
                        //action is of the type End Execution
                        break;
                    }

                    switch(aActionDef[3])
                    {
                        case "7": //action is of type Set Variable
                            {
                                ExecuteActionSetVariableValue(aActionDef, oSegment, oNode);
                                break;
                            }
                        case "26": //action is of type Run Variable Actions
                            {
                                ExecuteActionRunVariableActions(aActionDef, oSegment);
                                break;
                            }
                    }
                }
            }
        }

        private void ExecuteActionRunVariableActions(string[] aActionDef, Segment oSegment)
        {
            string xCtlValue = ((LMP.Controls.IControl)m_oCurCtl).Value;

            string xVariables = EvaluateExpression(aActionDef[4], xCtlValue, oSegment);
            string[] aVariables = xVariables.Split(',');
            foreach (string xVariable in aVariables)
            {
                UltraTreeNode oNode = null;

                try
                {
                    oNode = GetNodeFromTagID(oSegment.TagID + "." + xVariable.Trim());
                }
                catch { }

                if (oNode != null)
                {
                    ExecuteFinishedDataVariableActions(oNode, oSegment);
                }
            }
        }

        /// <summary>
        /// executes the SetVariableValue action
        /// </summary>
        /// <param name="aActionDef"></param>
        private void ExecuteActionSetVariableValue(string[] aActionDef, Segment oSegment, UltraTreeNode oNode)
        {
            //string xCtlValue = ((LMP.Controls.IControl)m_oCurCtl).Value;
            string xCtlValue = null;

            if (oNode.Nodes[0].Tag is LMP.Controls.IControl)
            {
                xCtlValue = ((LMP.Controls.IControl)oNode.Nodes[0].Tag).Value;
            }
            else
            {
                xCtlValue = oNode.Nodes[0].Tag.ToString();
            }

            string xParameters = aActionDef[4];
            string[] aParams = xParameters.Split('�');
            string xVariable = EvaluateExpression(aParams[0], xCtlValue, oSegment);
            string xValue = EvaluateExpression(aParams[1], xCtlValue, oSegment);

            //get node whose value needs to be set
            Segment[] oTargets = oSegment.GetReferenceTargets(ref xVariable);

            //GLOG item #5990 - dcf
            if (oTargets == null)
            {
                //return - there is no segment of the referenced type
                return;
            }
            //GLOG 8108: May be more than one target segment found
            for (int i = 0; i <= oTargets.GetUpperBound(0); i++)
            {
                //update snapshot with edited value
                //GLOG 5954 (dm) - moved this line up and made ui-independent to
                //deal with target variables that aren't displayed in the tree
                UpdateSnapshot(oTargets[i].FullTagID, xVariable, xValue);

                //GLOG 6677: Avoid error if specified Variable doesn't exist
                UltraTreeNode oDependentNode = null;
                try
                {
                    //get corresponding tree node
                    oDependentNode = this.treeDocContents.GetNodeByKey(
                        oTargets[i].FullTagID + "." + xVariable);
                }
                catch { }

                //GLOG item #5954 - dcf
                //exit if node was not found - there will be such situations when
                //a variable node does not display a control - the variable exists
                //before the segment is finished, but the data reviewer doesn't
                //present such a node
                if (oDependentNode == null)
                    continue; //GLOG 8108

                //if control exists, set control value, else store new value in value node tag
                IControl oICtl = oDependentNode.Nodes[0].Tag as IControl;
                if ((oICtl != null) && (oICtl.Value != xValue))
                {
                    oICtl.Value = xValue;
                }
                else
                {
                    oDependentNode.Nodes[0].Tag = xValue;
                }

                //set node text to variable value
                oDependentNode.Nodes[0].Text = this.GetVariableUIValue(oDependentNode.Nodes[0], xValue);
            }
        }

        /// <summary>
        /// returns true iff execution condition is met or if there is no execution condition
        /// </summary>
        /// <returns></returns>
        private string EvaluateExpression(string xExpression, string xValue, Segment oSegment)
        {
            Trace.WriteNameValuePairs("xExpression", xExpression);

            //return true if no execution condition
            if (xExpression == "")
                return "";

            //determine if there is a [MyValue] code in the execution condition string
            bool bMyValueExists = xExpression.IndexOf("MyValue") > -1;

            //evaluate condition
            if (bMyValueExists)
            {
                //GLOG 2151: Mark any reserved expression characters that are part of value
                //so they will be evaluated as the literal characters
                string xMyValue = xValue;
                xMyValue = Expression.MarkLiteralReservedCharacters(xMyValue);
                //replace all [MyValue] codes with control value
                xExpression = FieldCode.EvaluateMyValue(xExpression, xMyValue);
            }

            //evaluate resulting execution condition
            string xRet = Expression.Evaluate(xExpression,
                oSegment, m_oMPDocument);

            return xRet;
        }

        /// <summary>
        /// updates the snapshot with the value of the specified node
        /// </summary>
        /// <param name="oVarNode"></param>
        private void UpdateSnapshot(UltraTreeNode oVarNode)
        {
            //get segment tag id and variable name
            int iPos = oVarNode.Key.LastIndexOf(".");
            string xVarName = oVarNode.Key.Substring(iPos + 1);
            string xSegmentTagID = oVarNode.Key.Substring(0,iPos);

            IControl oCtl = oVarNode.Nodes[0].Tag as IControl;
            string xNewValue = null;

            if (oCtl != null)
            {
                xNewValue = oCtl.Value;
            }
            else
            {
                xNewValue = oVarNode.Nodes[0].Tag.ToString();
            }

            //GLOG 5954 (dm) - use new ui independent overload to update snapshot
            UpdateSnapshot(xSegmentTagID, xVarName, xNewValue);
        }

        /// <summary>
        /// updates the snapshot as specified
        /// </summary>
        /// <param name="oVarNode"></param>
        private void UpdateSnapshot(string xSegmentTagID, string xVarName,
            string xNewValue)
        {
            //get current snapshot values
            string xValues = "�" + m_oFinishedValues[xSegmentTagID].ToString() + "�";

            //find the name/value pair in the current values
            int iPos1 = xValues.IndexOf("�" + xVarName + "�");

            //GLOG 6488 (dm) - exit if target variable doesn't exist
            if (iPos1 == -1)
                return;

            //GLOG 5774 (dm) - changed first parameter from a string to a char
            int iPos2 = xValues.IndexOf('�', iPos1 + 1);

            //GLOG 7126 (dm) - authors may be at the end of the string
            string xClosing = "�";
            if (iPos2 > -1)
                xClosing = xValues.Substring(iPos2);

            //update the snapshot value
            string xNewValues = xValues.Substring(0, iPos1) +
                "�" + xVarName + "�" + xNewValue + xClosing; //GLOG 7126 (dm)
            m_oFinishedValues[xSegmentTagID] = xNewValues;

            Segment oSegment = m_oMPDocument.FindSegment(xSegmentTagID);

            //save the snapshot
            oSegment.Snapshot.Save(xNewValues);
        }

        /// <summary>
        /// returns the node type of the specified segment
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private NodeTypes NodeType(UltraTreeNode oNode)
        {
            if (oNode.Tag.ToString().StartsWith("<Variable "))
                return NodeTypes.Variable;
            else if (oNode.Key.EndsWith("_Value"))
                return NodeTypes.Value;
            else if (oNode.Key.EndsWith(".Advanced"))
                return NodeTypes.Advanced;
            else
                return NodeTypes.Segment;
        }

        ///// <summary>
        /////gets tree node from segment inserted via designated variable's
        /////InsertSegment variable action, if any
        ///// <param name="oVar"></param>
        ///// <returns></returns>
        //private UltraTreeNode GetNodeFromInsertedSegment(Variable oVar)
        //{
        //    Word.XMLNode oNode = null;
        //    Word.Range oRng = null;
        //    VariableAction oVA = null;
        //    Object oBmk = null;
        //    string xTagID = null;
        //    UltraTreeNode oTreeNode = null;

        //    for (int i = 0; i < oVar.VariableActions.Count; i++)
        //    {
        //        if (oVar.VariableActions.ItemFromIndex(i).Type == VariableActions.Types.InsertSegment)
        //        {
        //            oVA = oVar.VariableActions.ItemFromIndex(i);
        //            oBmk = (object)oVA.Parameters.Split(StringArray.mpEndOfSubField)[0];

        //            //attempt to get range from bookmark param
        //            //this may be empty if bookmark does not exist in document
        //            //or bookmark param is empty
        //            try
        //            {
        //                oRng = m_oMPDocument.WordDocument.Bookmarks.get_Item(ref oBmk).Range;
        //            }
        //            catch {}
                    
        //            //get the tag ID of the bookmarked range top level XML node
        //            //this will be an mSEG if the action has run correctly
        //            if (oRng != null && oRng.XMLNodes.Count > 0)
        //            {
        //                oNode = oRng.XMLNodes[1];
        //                if (oNode.BaseName == "mSEG")
        //                {
        //                    xTagID = m_oMPDocument.GetAttributeValue(oNode, "TagID");
        //                    oTreeNode = GetNodeFromTagID(oVar.Segment.TagID + "." + xTagID);
        //                    return oTreeNode;
        //                }
        //            }
        //        }
        //    }
        //    //no segment has been replaced, return empty node
        //    return oTreeNode;
        //}

        /// <summary>
        /// returns the value to be displayed 
        /// in the tree for the specified variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string GetVariableUIValue(UltraTreeNode oVarNode, string xRawValue)
        {
            DateTime t0 = DateTime.Now;

            string m_xEmptyDisplayValue = GetEmptyValueDisplay();
            string xValue = xRawValue;
            XmlDocument oXML = new XmlDocument();
            XmlNode oXMLNode = null;
            string xConfig = null;

            if (NodeType(oVarNode) == NodeTypes.Value)
            {
                //get variable config xml from parent node
                xConfig = oVarNode.Parent.Tag.ToString();
            }
            else if (NodeType(oVarNode) == NodeTypes.Variable)
            {
                //get variable config xml from node
                xConfig = oVarNode.Tag.ToString();
            }
            else
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_MustBeVariableOrValueNode"));
            }

            oXML.LoadXml(xConfig);
            oXMLNode = oXML.SelectSingleNode("/");

            //check if variable.DisplayValue contains a field code, date format
            //or boolean value, then process accordingly
            string xDisplayExpression = oXMLNode.SelectSingleNode("/Variable/@DisplayValue").Value;

            if (System.String.IsNullOrEmpty(xDisplayExpression))
            {
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                xValue = Expression.MarkLiteralReservedCharacters(xValue);
                xValue = Expression.Evaluate(xValue, null, m_oMPDocument);

                mpControlTypes iControl = (mpControlTypes)int.Parse(oXMLNode.SelectSingleNode("/Variable/@Control").Value);

                if (iControl == mpControlTypes.Checkbox)
                {
                    //get control properties from config xml
                    string xControlProps = oXMLNode.SelectSingleNode("/Variable/@ControlProperties").Value;
                    string[] aProps = xControlProps.Split('�');

                    //cycle through properties looking for the appropriate property
                    foreach (string xProp in aProps)
                    {
                        if (xProp.ToUpper().StartsWith(xRawValue.ToUpper() + "STRING"))
                        {
                            xValue = xProp.Substring((xRawValue.ToUpper() + "String=").Length);
                        }
                    }
                }
                else if (iControl == mpControlTypes.PaperTraySelector)
                {
                    //GLOG 4752: Access Printer bin information via Base wrapper functions
                    if (!string.IsNullOrEmpty(xRawValue))
                        xValue = LMP.Forte.MSWord.Application.GetPrinterBinName(short.Parse(xRawValue));
                }

                //GLOG 5799 (dm) - replace delimiter between multiple values
                XmlNode oNode = oXMLNode.SelectSingleNode("/Variable/@IsMultiValue");
                if ((oNode != null) && (oNode.Value.ToUpper() == "TRUE"))
                    xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
            }
            else
            {
                //evaluate the display value expression using variable value
                try
                {
                    xValue = EvaluateDisplayValue(xDisplayExpression, xRawValue);

                    //if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    //{
                    //    //GLOG 3318: reflect use of field code in display value
                    //    if (oVar.ValueSourceExpression.ToUpper().StartsWith("6[DATEFORMAT") &&
                    //        oVar.AssociatedWordTags[0].Range.Fields.Count > 0)
                    //        xValue = xValue + " (field code)";
                    //}
                    //else
                    //{
                    //    //GLOG 3318: reflect use of field code in display value
                    //    if (oVar.ValueSourceExpression.ToUpper().StartsWith("[DATEFORMAT") &&
                    //        oVar.AssociatedContentControls[0].Range.Fields.Count > 0)
                    //        xValue = xValue + " (field code)";
                    //}
                }
                catch { }
            }

            if (xValue == null || xValue == "" )
                xValue = m_xEmptyDisplayValue;
            else
            {
                //replace non-printing chars with ellipses
                xValue = xValue.Replace("\v", "...");
                xValue = xValue.Replace("\t", "...");
                xValue = xValue.Replace("\r\n", "...");
                xValue = xValue.Replace("\r", "...");
                xValue = xValue.Replace("\n", "...");
                xValue = (xRawValue == "") ? m_xEmptyDisplayValue : xValue;
            }

            //get available value node text width
            //TODO: we may want to enhance this method
            //to determine how much text to enter into
            //the node, given how much width is available
            if (xValue.Length > 35)
                //trim string and append ellipse if necessary
                xValue = xValue.Substring(0, 35) +
                    (xValue.EndsWith("...") ? "" : "...");

            LMP.Benchmarks.Print(t0, xDisplayExpression);

            return xValue;
        }
        /// <summary>
        /// evaluates DisplayValue field code using variable value
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string EvaluateDisplayValue(string xDisplayExpression, string xRawValue)
        {
            if (string.IsNullOrEmpty(xDisplayExpression))
                return "";

            string xVarValue = xRawValue;

            //GLOG 2151: Mark reserved expression characters in Value so they'll
            //be treated as the literal characters in Expression.Evaluate
            xVarValue = Expression.MarkLiteralReservedCharacters(xVarValue);

            //pre-evaluate the DisplayValue field code(s)
            string xFieldCode = FieldCode.EvaluateMyValue(xDisplayExpression, xVarValue);
            xFieldCode = xFieldCode.Replace("[MyTagValue]", xVarValue);

            //GLOG 5775 (dm)- convert date format to display date
            //GLOG 5926 (dm) - remove backslashes before checking for date format
            //and don't check if empty
            if ((xFieldCode != "") &&
                (LMP.Architect.Base.Application.IsValidDateFormat(xFieldCode.Replace("\\", ""))))
                return LMP.String.EvaluateDateTimeValue(xFieldCode, DateTime.Now);

            //handle listlookup field code
            if (xDisplayExpression.StartsWith("[ListLookup"))
                return Expression.Evaluate(xFieldCode, null, m_oMPDocument);  

            //check for ^AND operator in compound DisplayValue strings 
            //or "][" indicating multiple field codes to be evaluated -
            //reline control value, which contains nodes for Standard and Special
            //is an example where DisplayValue may contain two field codes

            //***REPLACING THESE CHARACTERS DOESN'T SEEM TO BE NECESSARY, AND DOES NOT WORK CORRECTLY WITH
            //***MORE COMPLEX EXPRESSIONS - LET EXPRESSION.EVALUATE DEAL WITH THIS IN THE NORMAL FASHION
            //xFieldCode = xFieldCode.Replace("^AND", LMP.StringArray.mpEndOfElement.ToString() + "[");
            //xFieldCode = xFieldCode.Replace("][", "]" + LMP.StringArray.mpEndOfElement.ToString() + "[");

            //create array of field codes for further pre-evaluation
            string[] xFieldCodes = xFieldCode.Split(LMP.StringArray.mpEndOfElement);
            StringBuilder oSB = new StringBuilder();

            //build an array list of values returned by Expression.Evaluate
            ArrayList oArrayList = new ArrayList();
            string xTemp = null;
            string[] aTemp = null;

            //for each configured field code, evaluate and return its string value
            //in the case of CI field codes, the string will be delimited by "; "
            //one member for each recipient
            for (int i = 0; i <= xFieldCodes.GetUpperBound(0); i++)
            {
                xTemp = xFieldCodes[i];
                xTemp = xTemp.Replace("; ", ";mp10Tempmp10 ");
                if (xTemp == "[TagValue]" || xTemp == "[MyTagValue]")
                {
                    xTemp = xVarValue;
                }
                else
                {
                    xTemp = Expression.Evaluate(xTemp, null, m_oMPDocument);
                }

                if (xTemp == null)
                    xTemp = "";

                //split value and add to arraylist
                xTemp = xTemp.Replace(";mp10Tempmp10 ", ";");
                xTemp = xTemp.Replace("mp10Tempmp10 ", "");
                xTemp = xTemp.Replace("; ", "|");
                aTemp = xTemp.Split('|');
                oArrayList.Add(aTemp);
            }

            //go through the array list of field codes and match field code return values
            //In most cases aTemp will have only a single member.  For CI type field codes,
            //aTemp will have as many members as returned contacts.
            for (int i = 0; i < oArrayList.Count; i++)
            {
                string[] aItems = (string[])oArrayList[i];

                for (int j = 0; j < aItems.Length; j++)
                {
                    //delimit each record w/ semicolon, add
                    //commas between record fields, i.e, name, address; name2, address;
                    string xFormat = (j == aItems.Length - 1) ? "{0}; " : "{0}, ";

                    oSB.AppendFormat(xFormat, aItems[j]);
                }
            }

            string xValue = oSB.ToString();
            //Replace value separator for display
            xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");

            //eliminate double delimiters caused by empty values
            xValue = xValue.Replace(";;", ";");
            xValue = xValue.Replace("; ;", ";");
            xValue = xValue.Replace(",,", ",");
            xValue = xValue.Replace(", ,", ",");

            xValue = xValue.TrimEnd();
            xValue = xValue.TrimEnd(';');
            xValue = xValue.TrimStart(';');
            xValue = xValue.TrimStart(',');
            xValue = xValue.TrimStart();

            return xValue;
        }

        /// <summary>
        /// returns the UI value for the specified authors collection
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <returns></returns>
        private string GetAuthorsUIValue(Authors oAuthors)
        {
            //get display text for value node
            Author oLeadAuthor = oAuthors.GetLeadAuthor();

            string xAuthors = "";

            if (oLeadAuthor != null)
            {
                if (oLeadAuthor.IsManualAuthor)
                    xAuthors = oLeadAuthor.FullName;
                else
                {
                    try
                    {
                        //try setting to Display Name first
                        xAuthors = oLeadAuthor.PersonObject.DisplayName;
                    }
                    catch { }
                    if (xAuthors == "")
                        xAuthors = oLeadAuthor.FullName;
                }

                if (oAuthors.Count > 1)
                    xAuthors += ", et al.";
            }
            return xAuthors;
        }
        /// <summary>
        /// creates, sets up, and returns the author selection control
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <returns></returns>
        private Control SetupAuthorControl(Authors oAuthors)
        {
            DateTime t0 = DateTime.Now;

            AuthorSelector oAuthorSelector = new AuthorSelector();

            IControl oIControl = (IControl)oAuthorSelector;
            oIControl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
            oIControl.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oIControl.KeyReleased += new KeyEventHandler(OnKeyReleased);

            oAuthorSelector.MaxAuthors = (short)oAuthors.Segment.MaxAuthors;
            SetAuthorControlProperties(oAuthorSelector, oAuthors);

            // GLOG : 3135 : JAB
            // Subscribe to the UpdateAuthorsRequested event.
            oAuthorSelector.UpdateAuthorsRequested +=
                new AuthorSelector.UpdateAuthorsEventHandler(oAuthorSelector_UpdateAuthorsRequested);

            // GLOG : 2547 : JAB
            // Subscribe to the EditAuthorPreferencesRequested event.
            oAuthorSelector.EditAuthorPreferencesRequested += new AuthorSelector.EditAutorPreferencesEventHandler(oAuthorSelector_EditAuthorPreferencesRequested);

            //set jurisdiction if the bar id column is displayed
            if ((oAuthorSelector.ShowColumns & mpAuthorColumns.BarID) > 0)
            {
                DateTime t1 = DateTime.Now;
                Segment oLevelSegment = oAuthors.Segment;
                //GLOG 3316: If Pleading Table item, get Court Levels from parent pleading
                if (oLevelSegment is CollectionTableItem)
                {
                    while (oLevelSegment.Parent != null)
                        oLevelSegment = oLevelSegment.Parent;
                }
                oAuthorSelector.Jurisdiction = new int[] { oLevelSegment.L0, oLevelSegment.L1, oLevelSegment.L2, oLevelSegment.L3, oLevelSegment.L4 };
                LMP.Benchmarks.Print(t0, "Set Jurisdiction");
            }
            oAuthorSelector.ExecuteFinalSetup();

            LMP.Benchmarks.Print(t0);
            return oAuthorSelector;
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Handle the context menu selection of the Update Authors menu item.
        /// </summary>
        /// <param name="sender"></param>
        void oAuthorSelector_UpdateAuthorsRequested(object sender)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();
                LMP.MacPac.Application.UpdateAuthor();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 2547 : JAB
        /// Handle selection of the Edit Author Preferences menu item.
        /// </summary>
        /// <param name="sender"></param>
        void oAuthorSelector_EditAuthorPreferencesRequested(object sender)
        {
            try
            {
                LMP.MacPac.Application.EditAuthorPreferences(false);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// creates, sets up, and returns the jurisdiction chooser control
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <returns></returns>
        private Control SetupJurisdictionControl(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;

            JurisdictionChooser oChooser = new JurisdictionChooser();

            IControl oIControl = (IControl)oChooser;
            oIControl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
            oIControl.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oIControl.KeyReleased += new KeyEventHandler(OnKeyReleased);

            SetJurisdictionControlProperties(oChooser, oSegment);

            oChooser.ExecuteFinalSetup();

            LMP.Benchmarks.Print(t0);
            return oChooser;
        }

        /// <summary>
        /// creates, sets up, and returns the languages combo box
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private Control SetupLanguageControl(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;

            LanguagesComboBox oLCB = new LanguagesComboBox(oSegment.SupportedLanguages,
                LMP.StringArray.mpEndOfValue);

            IControl oIControl = (IControl)oLCB;
            oIControl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
            oIControl.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oIControl.KeyReleased += new KeyEventHandler(OnKeyReleased);

            oLCB.Value = oSegment.Culture.ToString();
            oLCB.ExecuteFinalSetup();

            LMP.Benchmarks.Print(t0);
            return oLCB;
        }

        /// <summary>
        /// Set Properties for author control, stored in AuthorControlProperties property of Segment
        /// </summary>
        /// <param name="oControl"></param>
        /// <param name="xProperties"></param>
        private void SetAuthorControlProperties(AuthorSelector oAuthorControl, Authors oAuthors)
        {
            //set up control properties-
            //get control properties from variable
            string[] aProps = null;
            string xProperties = oAuthors.Segment.AuthorControlProperties;
            Control oControl = (Control)oAuthorControl;

            if (!System.String.IsNullOrEmpty(xProperties))
                aProps = xProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);

                //evaluate control prop value, which may be a MacPac expression
                xValue = Expression.Evaluate(xValue, oAuthors.Segment, this.TaskPane.ForteDocument);

                System.Type oCtlType = oControl.GetType();
                PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType);

                oPropInfo.SetValue(oControl, oValue, null);
            }
        }
        /// <summary>
        /// Set Properties for Jurisdiction control, stored in 
        /// CourtChooserControlProperties property of segment
        /// </summary>
        /// <param name="oControl"></param>
        /// <param name="xProperties"></param>
        private void SetJurisdictionControlProperties(JurisdictionChooser oChooser, Segment oSegment)
        {
            //set up control properties-
            //get control properties from Segment
            string[] aProps = null;
            string xProperties = oSegment.CourtChooserControlProperties;
            Control oControl = (Control)oChooser;

            if (!System.String.IsNullOrEmpty(xProperties))
                aProps = xProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);

                //evaluate control prop value, which may be a MacPac expression
                xValue = Expression.Evaluate(xValue, oSegment, this.TaskPane.ForteDocument);

                System.Type oCtlType = oControl.GetType();
                PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType);

                oPropInfo.SetValue(oControl, oValue, null);
            }
        }
        /// <summary>
        /// sizes and positions control at the specified node
        /// </summary>
        /// <param name="oUnderlyingNode"></param>
        private void SizeAndPositionControl()
        {
            if (this.m_oCurCtl == null || this.m_oCurNode == null)
                return;

            try
            {
                this.treeDocContents.SuspendLayout();
                this.m_oCurCtl.SuspendLayout();
                m_bRepositioningControl = true;
                int iControlWidth = 0;

                //set control width based on scroll bar visibility
                if (this.ScrollBarsVisible)
                    iControlWidth = this.treeDocContents.Width -
                        this.m_oCurNode.Bounds.Left - this.treeDocContents.Indent - 20;
                else
                    iControlWidth = this.treeDocContents.Width -
                        this.m_oCurNode.Bounds.Left - this.treeDocContents.Indent - 5;


                //When TaskPane in Initializing in Word 2003, width may not yet be finalized
                //when Initial control is selected.  Set to arbitrary width to avoid problems
                if (iControlWidth <= 0)
                    iControlWidth = 192;

                if (this.btnContacts.Visible)
                    iControlWidth = iControlWidth - (this.btnContacts.Width + 1);

                //position control to display over variable value
                this.m_oCurCtl.SetBounds(this.m_oCurNode.Bounds.Left + this.treeDocContents.Indent,
                    this.m_oCurNode.Bounds.Top + this.m_oCurNode.Bounds.Height,
                    iControlWidth, this.m_oCurCtl.Height);
                
                //Reposition floating CI button
                if (this.btnContacts.Visible)
                {
                    this.btnContacts.Left = m_oCurCtl.Left + m_oCurCtl.Width + 1;
                    this.btnContacts.Top = m_oCurCtl.Top + 5;
                }
            }
            finally
            {
                this.treeDocContents.ResumeLayout();
                this.m_oCurCtl.ResumeLayout();
                m_bRepositioningControl = false;
            }
        }
        /// <summary>
        /// sets value of variable associated with the current node equal to
        /// the value of the node's control
        /// </summary>
        private void UpdateVariableValueFromCurrentCtl()
        {
            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(m_oMPDocument.WordDocument);
            oEnv.SaveState();

            try
            {
                bool bScreenUpdating = this.TaskPane.ScreenUpdating;
                this.TaskPane.ScreenUpdating = false;

                Variable oVar = m_oCurNode.Tag as Variable;

                //GLOG 4132 (dm) - added conditional to avoid overwriting manual edits
                if (m_xSkipVarUpdateFromControl != oVar.Segment.FullTagID + "." + oVar.ID)
                {
                    //GLOG 5307: m_oCurCtl may no longer exist after SetValue if exiting
                    //the control and a Variable Action causes the Tree to be refreshed.
                    //Get SupportingValues beforehand
                    string xSuppVals = ((IControl)m_oCurCtl).SupportingValues;
                    oVar.SetValue(((IControl)m_oCurCtl).Value);
                    //GLOG 2645
                    oVar.SetRuntimeControlValues(xSuppVals);
                }

                if (bScreenUpdating)
                    this.TaskPane.ScreenUpdating = true;
            }
            finally
            {
                //8-1-11 (dm) - disable event handling - RestoreState() can trigger
                //content control enter/exit events
                ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                oEnv.RestoreState();
                ForteDocument.IgnoreWordXMLEvents = oEvents;
            }
        }
        /// <summary>
        /// Initializes Jurisdiction node for segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        private void InitializeJurisdictionNode(Segment oSegment, UltraTreeNode oNode)
        {
            //add hotkey to collection of hotkeys for this editor
            this.AddHotkey("J", oNode);

            //set node properties
            oNode.Expanded = true;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //assign empty Jurisdiction object to Tag
            oNode.Tag = new Jurisdictions(0);

            //add authors validation image -
            //TODO: deal with Jurisdiction validation
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //create new control and store in value node tag
            Control oJurCtl = SetupJurisdictionControl(oSegment);

            //oValueNode.Enabled = true;
            string xValue = GetJurisdictionTextForControl((JurisdictionChooser)oJurCtl, (Segment)oSegment);

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value", xValue);
            oValueNode.Tag = oJurCtl;
            //Add to hash table
            m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            oOverride = oValueNode.Override;
            oOverride.Multiline = DefaultableBoolean.True;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
        }
        /// <summary>
        /// Set display text for Jurisdiction Chooser,
        /// based on Min and Max Display levels
        /// </summary>
        /// <param name="oCtl"></param>
        /// <param name="oSeg"></param>
        /// <returns></returns>
        private string GetJurisdictionTextForControl(JurisdictionChooser oCtl, Segment oSeg)
        {
            int iMin = (int)oCtl.FirstLevel;
            int iMax = (int)oCtl.LastLevel;

            string xDisplayValue = (oSeg.GetJurisdictionText(iMin <= 0,
                iMin <= 1 && iMax >= 1, iMin <= 2 && iMax >= 2, iMin <= 3 && iMax >= 3, iMin <= 4 && iMax >= 4));

            if (xDisplayValue == "")
                return LMP.Resources.GetLangString("Msg_NotSpecified");
            else
            {
                return xDisplayValue;
            }
        }
        private void InitializeNode(Authors oAuthors, UltraTreeNode oNode)
        {
            //add hotkey to collection of hotkeys for this editor
            this.AddHotkey("U", oNode);

            //set node properties
            oNode.Expanded = m_bShowValueNodes;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //add authors to node tag
            oNode.Tag = oAuthors;

            //add authors validation image -
            //TODO: deal with Authors validation
            oNode.LeftImages.Add(Images.ValidVariableValue);

            string xAuthors = GetAuthorsUIValue(oAuthors);

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value", xAuthors);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
            //oValueNode.Enabled = true;
        }

        private void InitializeLanguageNode(Segment oSegment, UltraTreeNode oNode)
        {
            //add hotkey to collection of hotkeys for this editor
            this.AddHotkey("L", oNode);

            //set node properties
            oNode.Expanded = true;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //add validation image -
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //create new control
            Control oLCB = SetupLanguageControl(oSegment);

            //add node for value -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value",
                LMP.Data.Application.GetLanguagesDisplayValue(oSegment.Culture.ToString()));
            oValueNode.Tag = oLCB;

            //add to hash table
            m_oControlsHash.Add(oValueNode.Tag, oValueNode);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
        }

        /// <summary>
        /// initializes node corresponding to oVar
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsToplevel"></param>
        private void InitializeVariableNode(UltraTreeNode oNode, string xHotkey)
        {
            InitializeVariableNode(oNode, xHotkey, null);
        }

        /// <summary>
        /// initializes node corresponding to oVar
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsToplevel"></param>
        private void InitializeVariableNode(UltraTreeNode oNode, string xHotkey, string xValueKey)
        {

          DateTime t0 = DateTime.Now;

          if (xHotkey != null)
                AddHotkey(xHotkey, oNode);

            //add image
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //set node properties
            oNode.Expanded = m_bShowValueNodes;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            int iPos = oNode.Key.LastIndexOf(".");
            //trim first level from path, as top-level segments aren't
            //referenced in prefills
            string xPrefillPath = oNode.Key.Substring(iPos + 1);

            //if (string.IsNullOrEmpty(xValueKey))
            //{
            //    if (oNode.Parent.Key.EndsWith(".Advanced"))
            //    {
            //        xValueKey = oNode.Parent.Parent.Key;
            //    }
            //    else
            //    {
            //        xValueKey = oNode.Parent.Key;
            //    }
            //}

            xValueKey = oNode.Key.Substring(0, iPos);
            //iPos = xValueKey.LastIndexOf(".");
            //xValueKey = xValueKey.Substring(iPos + 1);
            string xValues = m_oFinishedValues[xValueKey].ToString();

            Prefill oPrefill = new Prefill(xValues, "Values", "1");

            string xRawValue = oPrefill[xPrefillPath];

            //GLOG 5894: Discard CI Detail Token STring portion of Prefill Value if present
            int iTokenStart = xRawValue.IndexOf("<CIDetailTokenString>");
            int iTokenEnd = xRawValue.IndexOf("</CIDetailTokenString>");
            if (iTokenStart > -1 && iTokenEnd > -1 && iTokenEnd > iTokenStart)
            {
                xRawValue = xRawValue.Substring(0, iTokenStart);
            }

            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value",
                this.GetVariableUIValue(oNode, xRawValue));
            oValueNode.Tag = xRawValue;

            //GLOG : 6377 : CEH
            //gets rid of the lag before the nodes appear in the Reviewer
            //System.Windows.Forms.Application.DoEvents();

            Segment oSegment = this.CurrentSegment;

            //test this
            ExecuteControlActions(oNode, ControlActions.Events.ValueChanged, oSegment);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

             LMP.Benchmarks.Print(t0);
        }

        private void AddHotkey(string xHotkey, UltraTreeNode oNode)
        {
            if (m_oHotkeys.ContainsKey(xHotkey))
            {
                //hotkey already exists -
                //add as part of array -
                //first get current hotkey node
                ArrayList oArrayList = null;
                if (m_oHotkeys[xHotkey] is UltraTreeNode)
                {
                    //get the node that is currently assigned to the hotkey
                    UltraTreeNode oCurNode = (UltraTreeNode)m_oHotkeys[xHotkey];

                    //create array
                    oArrayList = new ArrayList();

                    // Only add the node if the node belongs to a tree control.
                    // This existing node's tree control may have been cleared 
                    // during a refresh of segments.
                    if (oCurNode.Control != null)
                    {
                        //add currently assigned node
                        oArrayList.Add(oCurNode);
                    }
                }
                else
                    //there are already multiple nodes assigned
                    //to this hotkey - get them
                    oArrayList = (ArrayList)m_oHotkeys[xHotkey];

                //add this node
                oArrayList.Add(oNode);

                //add array list to hash table
                m_oHotkeys[xHotkey] = oArrayList;
            }
            else
                //assign node to hotkey
                m_oHotkeys.Add(xHotkey, oNode);
        }
        /// <summary>
        /// initializes node corresponding to oBlock
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsTopLevel"></param>
        private void InitializeNode(Block oBlock, UltraTreeNode oNode, bool bAsTopLevel)
        {
            //get hotkey, and add to hotkey hashtable
            string xHotkey = oBlock.Hotkey;

            if(xHotkey != null)
                AddHotkey(xHotkey, oNode);

            //add image
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //add reference to block
            oNode.Tag = oBlock;

            //set node properties
            oNode.Expanded = m_bShowValueNodes;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //get beginning of block text for display
            Word.Range oRange = null;
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                oRange = oBlock.AssociatedWordTag.Range;
            else
                oRange = oBlock.AssociatedContentControl.Range;

            string xBlockStartText = LMP.Forte.MSWord.WordDoc.GetRangeStartText(oRange, 20);

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value", xBlockStartText);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
            //oValueNode.Enabled = true;
        }
        /// <summary>
        /// selects the previous node on the tree
        /// </summary>
        private void SelectPreviousNode()
        {
            ExitEditMode();

            UltraTreeNode oNode = null;
            try
            {
                if (m_oCurTabNode != null)
                    oNode = this.m_oCurTabNode.PrevVisibleNode;
                else
                    oNode = this.m_oCurNode.PrevVisibleNode;
            }
            catch { }

            //skip over all value nodes - value nodes can't be selected
            while (oNode != null && (oNode.Key.EndsWith("_Value") || !oNode.Enabled))
                oNode = oNode.PrevVisibleNode;

            if (oNode != null)
                this.treeDocContents.ActiveNode = oNode;
            else
            {
                //If there's only one node, make sure it's selected
                if (this.treeDocContents.Nodes.Count == 1 && this.treeDocContents.Nodes[0].Nodes.Count == 0)
                {
                    m_oCurNode = null;
                    //this.SelectNode(treeDocContents.Nodes[0]);
                }
            }

        }
        /// <summary>
        /// selects the next node on the tree
        /// </summary>
        private void SelectNextNode()
        {
            //GLOG item #4588 - dcf -
            //fix for GLOG item #3802 was buggy
            //GLOG : 3802 : CEH
            //get current node key for later use -
            //ExitEditMode may force a reconstitution
            //of nodes, rendering the current node
            //different than the original
            string xKey;
            UltraTreeNode oCurNode = null;
            UltraTreeNode oNode = null;
            //GLOG 4670: Don't call ExitEditMode if currently tabbing without displaying controls
            if (m_oCurTabNode == null && m_oCurNode == null)
                oCurNode = this.treeDocContents.ActiveNode;
            else if (m_oCurTabNode != null)
                oCurNode = m_oCurTabNode;
            else
            {
                xKey = this.m_oCurNode.Key;
                ExitEditMode();
                oCurNode = this.treeDocContents.GetNodeByKey(xKey);
            }

            try
            {
                oNode = oCurNode.NextVisibleNode;
            }
            catch { }

            //skip over all value nodes - value nodes can't be selected
            while (oNode != null && (oNode.Key.EndsWith("_Value") || !oNode.Enabled))
                oNode = oNode.NextVisibleNode;

            if (oNode != null)
            {
                //there is a next node - select it
                this.treeDocContents.ActiveNode = oNode;

                if (m_oCurCtl != null)
                    m_oCurCtl.Focus();
            }
            else
            {
                //there is no next node - select first node
                this.treeDocContents.ActiveNode = this.treeDocContents.Nodes[0];
                //If there's only one control, make sure it's selected
                if (this.treeDocContents.Nodes.Count == 1 && this.treeDocContents.ActiveNode.Nodes.Count == 0)
                {
                    m_oCurNode = null;
                }
            }
        }
        
        /// <summary>
        /// GLOG 2999: Updated to support Reference Target format
        /// Given a path in the form x.y.z.var, returns 
        /// Variable object from specified Child segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xVariablePath"></param>
        /// <returns></returns>
        private Variable GetVariableFromPath(Segment oSegment, string xVariablePath)
        {
            Segment oTargetSegment = null;
            Variable oVar = null;
            string xTargetVar = "";

            if (xVariablePath.IndexOf(".") > 0)
            {
                // Variable belongs to a child segment - form is x.y.z.var
                // This is the old method - code left in place to support existing content
                int iLastSep = xVariablePath.LastIndexOf(".");
                xTargetVar = xVariablePath.Substring(iLastSep + 1);
                string xChildSegmentPath = xVariablePath.Substring(0, iLastSep);
                oTargetSegment = oSegment.GetChildSegmentFromPath(xChildSegmentPath);
                try
                {
                    if (oTargetSegment != null)
                    {
                        oVar = oTargetSegment.Variables.ItemFromName(xTargetVar);
                    }
                }
                catch
                {
                    // No match found
                    return null;
                }
            }
            else
            {
                //GLOG 2999: Reference target method
                xTargetVar = xVariablePath;
                Segment[] oTargets = oSegment.GetReferenceTargets(ref xTargetVar);
                if (oTargets != null)
                {
                    foreach (Segment oTarget in oTargets)
                    {
                        //get target variable
                        oVar = oTarget.GetVariable(xTargetVar);

                        //stop after finding match
                        if (oVar != null)
                        {
                            break;
                        }
                    }
                }
            }
            return oVar;
        }

        //GLOG : 6188 : CEH
        /// <summary>
        /// creates the associated menu for the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private void CreateSegmentMenu()
        {
            if (m_oMenuSegment == null)
            {
                //create new segment menu
                m_oMenuSegment = new TimedContextMenuStrip(this.components);
                m_oMenuSegment.TimerInterval = 1000;
                m_oMenuSegment.ItemClicked += new ToolStripItemClickedEventHandler(m_oMenuSegment_ItemClicked);
                m_oMenuSegment.ShowItemToolTips = false;
            }
            else
                //clear existing menu
                m_oMenuSegment.Items.Clear();

            if (m_oMenuSegment_SaveData == null)
            {
                m_oMenuSegment_SaveData = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_SaveData.Name = "m_oMenuSegment_SaveData";
                m_oMenuSegment_SaveData.Text = LMP.Resources.GetLangString("Menu_DataReviewer_CreatePrefill");
                m_oMenuSegment_SaveData.ToolTipText = LMP.Resources.GetLangString("Menu_DataReviewer_CreatePrefillTooltip");
            }

            if (m_oMenuSegment_SaveContent == null)
            {
                m_oMenuSegment_SaveContent = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_SaveContent.Name = "m_oMenuSegment_SaveContent";
                m_oMenuSegment_SaveContent.Text = LMP.Resources.GetLangString("Menu_DataReviewer_SaveContent");
                m_oMenuSegment_SaveContent.ToolTipText = LMP.Resources.GetLangString("Menu_DataReviewer_SaveContentTooltip");
            }

            if (m_oMenuSegment_RecreateSegment == null)
            {
                m_oMenuSegment_RecreateSegment = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_RecreateSegment.Name = "m_oMenuSegment_RecreateSegment";
                m_oMenuSegment_RecreateSegment.Text = LMP.Resources.GetLangString("Menu_DataReviewer_RecreateSegment");
                m_oMenuSegment_RecreateSegment.ToolTipText = LMP.Resources.GetLangString("Menu_DataReviewer_RecreateSegmentTooltip");
            }

            m_oMenuSegment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                m_oMenuSegment_SaveData,
                m_oMenuSegment_SaveContent,
                new ToolStripSeparator(),
                m_oMenuSegment_RecreateSegment});

            //if (m_oMenuSegment_Help == null)
            //{
            //    m_oMenuSegment_Help = new System.Windows.Forms.ToolStripMenuItem();
            //    m_oMenuSegment_Help.Name = "m_oMenuSegment_Help";
            //    m_oMenuSegment_Help.Text = LMP.Resources.GetLangString("mnuSegment_Help");
            //}

            //ToolStripSeparator oSep3 = new ToolStripSeparator();
            //oSep3.Visible = LMP.MacPac.Session.CurrentUser.FirmSettings.AllowSegmentMemberTagRemoval;

            //// Add Quick Help Menu item in its own section.
            //m_oMenuSegment.Items.Add(new ToolStripSeparator());
            //m_oMenuSegment.Items.Add(m_oMenuSegment_Help);
        }

        private void CreateVariableMenu(Variable oVar)
        {
        }

        void m_oMenuVariable_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Replace GUID TagIDs in XML with integer IDs
        /// to ensure no conflicts when pasting into document
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="oSeg"></param>
        /// <param name="bincludeChildren"></param>
        /// <returns></returns>
        private static string UpdateSegmentTagIDInXML(string xXML, Segment oSeg, bool bincludeChildren)
        {
            return UpdateSegmentTagIDInXML(xXML, oSeg, bincludeChildren, 1);
        }
        private static string UpdateSegmentTagIDInXML(string xXML, Segment oSeg, bool bIncludeChildren, int iIndex)
        {
            //Get Just last part of TagID
            string xTagID = oSeg.FullTagID;
            int iSep = xTagID.LastIndexOf(".");
            iSep = iSep + 1;
            xTagID = xTagID.Substring(iSep);
            int iGUIDStart = xTagID.LastIndexOf("{");
            if (iGUIDStart == -1)
                return xXML;
            //Replace GUID with Integer
            string xNewTagID = xTagID.Substring(0, iGUIDStart) + iIndex.ToString();
            xXML = xXML.Replace(xTagID, xNewTagID);
            if (bIncludeChildren)
            {
                for (int i = 0; i < oSeg.Segments.Count; i++)
                {
                    xXML = UpdateSegmentTagIDInXML(xXML, oSeg.Segments[0], true, i+1);
                }
            }
            return xXML;
        }

        /// <summary>
        /// returns true if the node for a same-type sibling
        /// of the specified segment is transparent
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool SiblingIsTransparent(Segment oSegment)
        {
            if (!oSegment.IsTopLevel)
            {
                Segments oSiblings = oSegment.Parent.Segments;
                for (int i = 0; i < oSiblings.Count; i++)
                {
                    Segment oSibling = oSiblings[i];
                    if ((oSibling.TypeID == oSegment.TypeID) &&
                        (oSibling.FullTagID != oSegment.FullTagID) &&
                        this.NodeIsTransparent(oSibling.FullTagID))
                        return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Display callout above and to left of selected range
        /// </summary>
        /// <param name="xText"></param>
        /// <param name="oRng"></param>
        private void ShowCallout(string xText, Word.Range oRng)
        {
            DateTime t0 = DateTime.Now;

            //show location tooltip popup if specified
            if (!Session.CurrentUser.UserSettings.ShowLocationTooltips)
                return;

            try
            {
                if (m_bUseWindowsCallout)
                {
                    int iPixelsLeft = 0;
                    int iPixelsTop = 0;
                    int iPixelsWidth = 0;
                    int iPixelsHeight = 0;
                    if (m_oCallout == null)
                    {
                        m_oCallout = new CalloutForm();
                    }
                    m_oCallout.Visible = false;
                    m_oCallout.Text = xText;

                    //Get screen coordinates of Word Range
                    oRng.Document.ActiveWindow.GetPoint(out iPixelsLeft, out iPixelsTop,
                        out iPixelsWidth, out iPixelsHeight, oRng);

                    //get call out position offset based on space before of range and font size
                    object oTrue = true;
                    int iPixelsTopOffset = System.Convert.ToInt32(
                        oRng.Application.PointsToPixels(oRng.Paragraphs.First.SpaceBefore, ref oTrue) + 2);

                    m_oCallout.Left = iPixelsLeft - m_oCallout.Width;
                    m_oCallout.Top = iPixelsTop - m_oCallout.Height;
                    m_oCallout.Top = iPixelsTop - m_oCallout.Height + iPixelsTopOffset;
                    m_oCallout.Show(this);
                    m_oCallout.Refresh();
                }
                else
                {
                    LMP.Forte.MSWord.WordDoc.DisplayCallout(oRng, xText);
                }
            }
            catch{ }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// Hide Callout if displayed
        /// </summary>
        internal void HideCallout()
        {
            if (m_bUseWindowsCallout)
            {
                if (m_oCallout != null)
                    m_oCallout.Hide();
            }
            else
                this.m_oMPDocument.ClearCallout();
        }
        /// <summary>
        /// Hides callout form if mouse has moved off of TaskPane or Word application is no longer active
        /// </summary>
        internal void ShowHideCalloutIfNecessary()
        {
            if (m_oCallout == null)
                return;

            //Leave callout on screen while CI Dialog is displayed
            if (m_bCIIsActive)
                return;

            Point oPt = Cursor.Position;
            if (!this.ClientRectangle.Contains(this.PointToClient(oPt)) && m_oCallout != null && m_oCallout.Visible)
                HideCallout();
            else if (this.ClientRectangle.Contains(this.PointToClient(oPt)) && m_oCallout != null && !m_oCallout.Visible)
            {
                //Redisplay callout if mouse moved off and back over tree
                if (m_oCurNode != null && m_oCurNode.Tag is Variable)
                {
                    Variable oVar = m_oCurNode.Tag as Variable;
                    if (oVar != null)
                    {
                        Word.Range oRng = null;
                        try
                        {
                            if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            {
                                oRng = oVar.AssociatedWordTags[0].Range;
                            }
                            else
                            {
                                oRng = oVar.AssociatedContentControls[0].Range;
                            }
                        }
                        catch { }
                        if (oRng != null)
                            this.ShowCallout(oVar.DisplayName,
                                oRng);
                    }
                }
            }
        }

        /// <summary>
        /// deletes the hotkey for oNode
        /// </summary>
        /// <param name="oNode"></param>
        private void DeleteHotkey(UltraTreeNode oNode)
        {
            //get hotkey for node
            string xHotkey = "";
            if (oNode.Tag is LMP.Architect.Api.Authors)
                xHotkey = "U";
            else if (oNode.Tag is Jurisdictions)
                xHotkey = "J";
            else if (oNode.Key.EndsWith(".Language"))
                xHotkey = "L";
            else
            {
                if (oNode.Tag is Variable)
                    xHotkey = ((Variable)oNode.Tag).Hotkey;
                else
                    xHotkey = ((Block)oNode.Tag).Hotkey;
            }

            //delete hotkey
            if (xHotkey != "")
            {
                if (this.m_oHotkeys[xHotkey] is UltraTreeNode)
                    //hash key is assigned a node - delete key
                    m_oHotkeys.Remove(xHotkey);
                else
                {
                    //hash key is assigned an array list
                    ArrayList oHotkeyNodesArray = (ArrayList)this.m_oHotkeys[xHotkey];
                    if (oHotkeyNodesArray.Count == 1)
                        //only one item on list - delete key
                        m_oHotkeys.Remove(xHotkey);
                    else
                    {
                        //find item to delete
                        for (int i = 0; i < oHotkeyNodesArray.Count; i++)
                        {
                            UltraTreeNode oHotkeyNode = (UltraTreeNode)oHotkeyNodesArray[i];
                            if (oHotkeyNode == oNode)
                                oHotkeyNodesArray.Remove(oHotkeyNode);
                        }
                    }
                }
            }
        }
        
        #endregion
        #region *********************protected methods*********************
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// processes the tab and shift-tab key combinations -
        /// selects next or previous node -
        /// unfortunately, there is an IBF bug that prevents
        /// this method from getting executed when appropriate -
        /// in cases when this method does not execute, the
        /// tab pressed event handler will execute
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            try
            {
                //get state of tab key - calling the API KeyState function
                //is the only reliable way to get this info
                int iTabKeyState = LMP.WinAPI.GetKeyState(KEY_TAB);
                bool bTabPressed = !(iTabKeyState == 1 || iTabKeyState == 0);
                //get state of ctrl key
                int iCtrlKeyState = LMP.WinAPI.GetKeyState(KEY_CONTROL);
                bool bCtrlPressed = !(iCtrlKeyState == 1 || iCtrlKeyState == 0);
                //get state of shift key
                int iShiftKeyState = LMP.WinAPI.GetKeyState(KEY_SHIFT);
                bool bShiftPressed = !(iShiftKeyState == 1 || iShiftKeyState == 0);

                if (bTabPressed)
                {
                    keyData = Keys.None;
                    HandleTabPressed(bShiftPressed);
                    return true;
                }
                else if (bCtrlPressed && bShiftPressed)
                {
                    //Ctrl+Shift shortcuts defined for the control
                    //Home - move to first variable
                    //End - move to last variable
                    //Left - Collapse tree to initial state
                    //Right - Expand all nodes in tree
                    //get state of Home key
                    int iHomeKeyState = LMP.WinAPI.GetKeyState(KEY_HOME);
                    bool bHomeKeyPressed = !(iHomeKeyState == 1 || iHomeKeyState == 0);
                    //get state of End key
                    int iEndKeyState = LMP.WinAPI.GetKeyState(KEY_END);
                    bool bEndKeyPressed = !(iEndKeyState == 1 || iEndKeyState == 0);
                    //get state of Left key
                    int iLeftKeyState = LMP.WinAPI.GetKeyState(KEY_LEFT);
                    bool bLeftKeyPressed = !(iLeftKeyState == 1 || iLeftKeyState == 0);
                    //get state of Right key
                    int iRightKeyState = LMP.WinAPI.GetKeyState(KEY_RIGHT);
                    bool bRightKeyPressed = !(iRightKeyState == 1 || iRightKeyState == 0);
                    //get state of B key (select body)
                    int iBKeyState = LMP.WinAPI.GetKeyState(66);
                    bool bBKeyPressed = !(iBKeyState == 1 || iBKeyState == 0);
                    if (bHomeKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        //GLOG - 3338 - CEH
                        //SelectFirstVariableNode();
                        return true;
                    }
                    else if (bEndKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        //SelectLastVariableNode();
                        return true;
                    }
                    else if (bLeftKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        this.CollapseAll();
                        return true;
                    }
                    else if (bRightKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        this.ExpandAll();
                        return true;
                    }
                }
                if (keyData == Keys.F5)
                {
                    //this.TaskPane.Refresh() - relaced with lines below (GLOG 2768, 8/1/08);
                    TaskPanes.RefreshAll();
                    this.RefreshTree();
                    this.Focus();
                    return true;
                }
                else
                {
                    this.DisableHotKeyMode = false;
                    return false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            return false;
        }
        #endregion
        #region *********************static methods*********************
        /// <summary>
        /// creates an instance of each control -
        /// called on a separate thread to increase
        /// performance of initial control display by
        /// forcing code to cache
        /// </summary>
        private static void InitializeControls()
        {
            LMP.Controls.TextBox oTB = new LMP.Controls.TextBox();
            LMP.Controls.MultilineTextBox oMLT = new MultilineTextBox();
            LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
            //LMP.Controls.MultilineCombo oMLC = new MultilineCombo(this.TaskPane);
            LMP.Controls.Reline oReline = new Reline();
            LMP.Controls.Detail oRecipDetail = new Detail();
            LMP.Controls.DetailList oRecipList = new DetailList();
            LMP.Controls.AuthorSelector oAuthorSelector = new AuthorSelector();
            LMP.Controls.Chooser oChooser = new Chooser();
            LMP.Controls.JurisdictionChooser oJurisChooser = new JurisdictionChooser();
            LMP.Controls.NameValuePairGrid oNameValue = new NameValuePairGrid();
            LMP.Controls.LanguagesComboBox oLanguage = new LanguagesComboBox();
            LMP.Controls.PaperTraySelector oPaperTraySelector = new PaperTraySelector();
            //LMP.Controls.ClientMatterSelector oCMSelector = new ClientMatterSelector();

            oTB.Dispose();
            oMLT.Dispose();
            oCombo.Dispose();
            //oMLC.Dispose();
            oReline.Dispose();
            oRecipDetail.Dispose();
            oRecipList.Dispose();
            oAuthorSelector.Dispose();
            oChooser.Dispose();
            oJurisChooser.Dispose();
            oNameValue.Dispose();
            oLanguage.Dispose();
            oPaperTraySelector.Dispose();
            //oCMSelector.Dispose();
        }
        #endregion

        /// <summary>
        /// GLOG : 1908 : JAB
        /// Move the picMenu control to the appropriate location
        /// after resizing the tree control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_Resize(object sender, EventArgs e)
        {
            try
            {
                DisplayContextMenuButton();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 1908 : JAB
        /// Conditionally show the context menu button.
        /// </summary>
        private void DisplayContextMenuButton()
        {
            if (this.m_oCurNode != null)
            {
                //move control with associated node
                if (this.picMenu.Visible)
                {
                    ShowContextMenuButton(this.m_oCurNode);
                }

                this.SizeAndPositionControl();
            }
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Determine if the treeContent control's vertical scrollbar is visible.
        /// This is used in determining the location of the content mgr menu 
        /// picture control. 
        /// </summary>
        private bool IsTreeContentVerticalScrollbarVisible
        {
            get
            {
                UltraTreeNode oLastExposedNode = GetLastExposedNode(this.treeDocContents);
                return !(this.treeDocContents.Nodes[0].IsInView && oLastExposedNode.IsInView);
            }
        }

        ///// <summary>
        ///// True if a context menu is displayed
        ///// </summary>
        ///// <returns></returns>
        //internal bool MenuIsVisible()
        //{
        //    //GLOG 3721
        //    return ((mnuQuickHelp != null && mnuQuickHelp.Visible) || 
        //        (m_oMenuAuthors != null && m_oMenuAuthors.Visible) ||
        //        (m_oMenuSegment != null &&  m_oMenuSegment.Visible) || 
        //        (m_oMenuVariable != null && m_oMenuVariable.Visible) || 
        //        (m_oMenuBlock != null && m_oMenuBlock.Visible));
        //}
        /// <summary>
        /// GLOG : 2918 : JAB
        /// Get the last node that is exposed in the treeContent control. This
        /// is needed to determine if the treeContent's vertical scrollbar is 
        /// visible.
        /// </summary>
        /// <param name="treeContent"></param>
        /// <returns></returns>
        private UltraTreeNode GetLastExposedNode(UltraTree treeContent)
        {
            UltraTreeNode oLastExposedNode = null;

            if (treeContent.Nodes.Count > 0)
            {
                oLastExposedNode = treeContent.Nodes[treeContent.Nodes.Count - 1];

                while (oLastExposedNode.HasNodes && oLastExposedNode.Expanded)
                {
                    oLastExposedNode = oLastExposedNode.Nodes[oLastExposedNode.Nodes.Count - 1];
                }
            }

            return oLastExposedNode;
        }

        /// <summary>
        /// GLOG : 2985 : JAB
        /// Position the document editor's menu picture control appropriately if the treeContent
        /// container panel has been resized.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlDocContents_SizeChanged(object sender, EventArgs e)
        {
            if (this.TaskPane == null || !this.TaskPane.IsSetUp)
                return;

            if (this.treeDocContents.SelectedNodes.Count > 0)
            {
                this.ShowContextMenuButton(this.treeDocContents.SelectedNodes[0]);
            }
            else
            {
                if (this.treeDocContents.ActiveNode != null)
                {
                    this.ShowContextMenuButton(this.treeDocContents.ActiveNode);
                }
            }
        }

        /// <summary>
        /// GLOG : 3303 : JAB
        /// Return the focus to the document when a Block node is activated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_AfterActivate(object sender, NodeEventArgs e)
        {
            if (m_bSuppressTreeActivationHandlers)
                return;

            try
            {
                EnterEditMode(e.TreeNode);
                m_oCurTabNode = null;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 3303 : JAB
        /// Move focus to the document when a block node is selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.RefreshTree();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
        }

        private void tbtnFinish_Click(object sender, EventArgs e)
        {
            try
            {
                ExitEditMode();

                this.TaskPane.SelectTab(TaskPane.Tabs.Edit);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
            }
        }

        private void tbtnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ExitEditMode();

                this.TaskPane.SelectTab(TaskPane.Tabs.Edit);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnSaveSegment_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                {
                    this.TaskPane.DocEditor.RefreshTree();
                    this.RefreshTree();
                }

                ExitEditMode();

                Segment oSeg = this.TaskPane.GetTargetSegment();

                if (oSeg != null)
                {
                    LMP.MacPac.Application.SaveSegmentWithData(oSeg);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
