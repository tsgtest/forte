namespace LMP.Architect.Api
{
    partial class AlignPleadingPaperTextForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.rdToEndOfBody = new System.Windows.Forms.RadioButton();
            this.rdSelection = new System.Windows.Forms.RadioButton();
            this.rdParagraph = new System.Windows.Forms.RadioButton();
            this.rdPleadingBody = new System.Windows.Forms.RadioButton();
            this.grpScope = new System.Windows.Forms.GroupBox();
            this.grpScope.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(228, 181);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(71, 27);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(148, 181);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(71, 27);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // rdToEndOfBody
            // 
            this.rdToEndOfBody.AutoSize = true;
            this.rdToEndOfBody.Location = new System.Drawing.Point(16, 55);
            this.rdToEndOfBody.Name = "rdToEndOfBody";
            this.rdToEndOfBody.Size = new System.Drawing.Size(246, 19);
            this.rdToEndOfBody.TabIndex = 8;
            this.rdToEndOfBody.TabStop = true;
            this.rdToEndOfBody.Text = "&From Cursor Position to End of Pleading Body";
            this.rdToEndOfBody.UseVisualStyleBackColor = true;
            // 
            // rdSelection
            // 
            this.rdSelection.AutoSize = true;
            this.rdSelection.Location = new System.Drawing.Point(16, 113);
            this.rdSelection.Name = "rdSelection";
            this.rdSelection.Size = new System.Drawing.Size(92, 19);
            this.rdSelection.TabIndex = 7;
            this.rdSelection.TabStop = true;
            this.rdSelection.Text = "&Selected Text";
            this.rdSelection.UseVisualStyleBackColor = true;
            // 
            // rdParagraph
            // 
            this.rdParagraph.AutoSize = true;
            this.rdParagraph.Location = new System.Drawing.Point(16, 84);
            this.rdParagraph.Name = "rdParagraph";
            this.rdParagraph.Size = new System.Drawing.Size(116, 19);
            this.rdParagraph.TabIndex = 6;
            this.rdParagraph.TabStop = true;
            this.rdParagraph.Text = "&Current Paragraph";
            this.rdParagraph.UseVisualStyleBackColor = true;
            // 
            // rdPleadingBody
            // 
            this.rdPleadingBody.AutoSize = true;
            this.rdPleadingBody.Location = new System.Drawing.Point(16, 27);
            this.rdPleadingBody.Name = "rdPleadingBody";
            this.rdPleadingBody.Size = new System.Drawing.Size(137, 19);
            this.rdPleadingBody.TabIndex = 1;
            this.rdPleadingBody.TabStop = true;
            this.rdPleadingBody.Text = "Entire &Body of Pleading";
            this.rdPleadingBody.UseVisualStyleBackColor = true;
            // 
            // grpScope
            // 
            this.grpScope.AutoSize = true;
            this.grpScope.Controls.Add(this.rdSelection);
            this.grpScope.Controls.Add(this.rdPleadingBody);
            this.grpScope.Controls.Add(this.rdParagraph);
            this.grpScope.Controls.Add(this.rdToEndOfBody);
            this.grpScope.Location = new System.Drawing.Point(12, 14);
            this.grpScope.Name = "grpScope";
            this.grpScope.Size = new System.Drawing.Size(287, 155);
            this.grpScope.TabIndex = 11;
            this.grpScope.TabStop = false;
            this.grpScope.Text = "Align:";
            // 
            // AlignPleadingPaperTextForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(311, 220);
            this.Controls.Add(this.grpScope);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AlignPleadingPaperTextForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Align Text to Pleading Paper";
            this.Load += new System.EventHandler(this.AlignText_Load);
            this.grpScope.ResumeLayout(false);
            this.grpScope.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.RadioButton rdToEndOfBody;
        private System.Windows.Forms.RadioButton rdSelection;
        private System.Windows.Forms.RadioButton rdParagraph;
        private System.Windows.Forms.RadioButton rdPleadingBody;
        private System.Windows.Forms.GroupBox grpScope;
    }
}