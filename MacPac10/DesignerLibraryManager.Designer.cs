namespace LMP.MacPac
{
    partial class DesignerLibraryManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignerLibraryManager));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override2 = new Infragistics.Win.UltraWinTree.Override();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.btnImport = new System.Windows.Forms.Button();
            this.splitContents = new System.Windows.Forms.Splitter();
            this.lblSegments = new System.Windows.Forms.Label();
            this.pnlVariables = new System.Windows.Forms.Panel();
            this.cmbCategory = new LMP.Controls.ComboBox();
            this.treeImport = new LMP.Controls.TreeView();
            this.pnlSegments = new System.Windows.Forms.Panel();
            this.treeSegments = new LMP.Controls.FolderTreeView(this.components);
            this.pnlVariables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeImport)).BeginInit();
            this.pnlSegments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeSegments)).BeginInit();
            this.SuspendLayout();
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Fuchsia;
            this.ilImages.Images.SetKeyName(0, "DocumentSegment");
            this.ilImages.Images.SetKeyName(1, "ComponentSegment");
            this.ilImages.Images.SetKeyName(2, "TaggedVariable");
            this.ilImages.Images.SetKeyName(3, "TaglessVariable");
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImport.AutoSize = true;
            this.btnImport.Location = new System.Drawing.Point(227, 218);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(87, 25);
            this.btnImport.TabIndex = 8;
            this.btnImport.Text = "I&mport";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            this.btnImport.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnImport_KeyDown);
            // 
            // splitContents
            // 
            this.splitContents.BackColor = System.Drawing.Color.SlateGray;
            this.splitContents.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContents.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitContents.Location = new System.Drawing.Point(0, 268);
            this.splitContents.MinExtra = 200;
            this.splitContents.MinSize = 200;
            this.splitContents.Name = "splitContents";
            this.splitContents.Size = new System.Drawing.Size(328, 5);
            this.splitContents.TabIndex = 48;
            this.splitContents.TabStop = false;
            // 
            // lblSegments
            // 
            this.lblSegments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSegments.AutoSize = true;
            this.lblSegments.BackColor = System.Drawing.Color.Transparent;
            this.lblSegments.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSegments.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSegments.Location = new System.Drawing.Point(2, 3);
            this.lblSegments.Name = "lblSegments";
            this.lblSegments.Size = new System.Drawing.Size(57, 13);
            this.lblSegments.TabIndex = 6;
            this.lblSegments.Text = "&Segments:";
            // 
            // pnlVariables
            // 
            this.pnlVariables.BackColor = System.Drawing.Color.Transparent;
            this.pnlVariables.Controls.Add(this.cmbCategory);
            this.pnlVariables.Controls.Add(this.treeImport);
            this.pnlVariables.Controls.Add(this.btnImport);
            this.pnlVariables.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlVariables.Location = new System.Drawing.Point(0, 273);
            this.pnlVariables.Name = "pnlVariables";
            this.pnlVariables.Size = new System.Drawing.Size(328, 255);
            this.pnlVariables.TabIndex = 47;
            // 
            // cmbCategory
            // 
            this.cmbCategory.AllowEmptyValue = false;
            this.cmbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCategory.AutoSize = true;
            this.cmbCategory.Borderless = false;
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbCategory.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCategory.IsDirty = false;
            this.cmbCategory.LimitToList = true;
            this.cmbCategory.ListName = "";
            this.cmbCategory.Location = new System.Drawing.Point(1, 0);
            this.cmbCategory.MaxDropDownItems = 8;
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.SelectedIndex = -1;
            this.cmbCategory.SelectedValue = null;
            this.cmbCategory.SelectionLength = 0;
            this.cmbCategory.SelectionStart = 0;
            this.cmbCategory.Size = new System.Drawing.Size(325, 26);
            this.cmbCategory.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbCategory.SupportingValues = "";
            this.cmbCategory.TabIndex = 10;
            this.cmbCategory.Tag2 = null;
            this.cmbCategory.Value = "";
            this.cmbCategory.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbCategory_ValueChanged);
            this.cmbCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCategory_KeyDown);
            // 
            // treeImport
            // 
            this.treeImport.AllowKeyboardSearch = false;
            this.treeImport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.White;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.BackwardDiagonal;
            this.treeImport.Appearance = appearance1;
            this.treeImport.BorderStyle = Infragistics.Win.UIElementBorderStyle.Inset;
            this.treeImport.ImageList = this.ilImages;
            this.treeImport.IsDirty = false;
            this.treeImport.Location = new System.Drawing.Point(1, 24);
            this.treeImport.Name = "treeImport";
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.ForeColor = System.Drawing.Color.Blue;
            _override1.ActiveNodeAppearance = appearance2;
            _override1.AllowCopy = Infragistics.Win.DefaultableBoolean.False;
            _override1.AllowCut = Infragistics.Win.DefaultableBoolean.False;
            _override1.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            _override1.AllowPaste = Infragistics.Win.DefaultableBoolean.False;
            _override1.BorderStyleNode = Infragistics.Win.UIElementBorderStyle.None;
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.ForeColor = System.Drawing.Color.Black;
            _override1.ExpandedNodeAppearance = appearance3;
            _override1.Multiline = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.ForeColor = System.Drawing.Color.Black;
            _override1.NodeAppearance = appearance4;
            _override1.NodeDoubleClickAction = Infragistics.Win.UltraWinTree.NodeDoubleClickAction.None;
            _override1.NodeSpacingAfter = 2;
            _override1.NodeSpacingBefore = 2;
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.ForeColor = System.Drawing.Color.Black;
            _override1.SelectedNodeAppearance = appearance5;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.Never;
            this.treeImport.Override = _override1;
            this.treeImport.ScrollBounds = Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill;
            this.treeImport.ShowRootLines = false;
            this.treeImport.Size = new System.Drawing.Size(325, 186);
            this.treeImport.SupportingValues = "";
            this.treeImport.TabIndex = 9;
            this.treeImport.Tag2 = null;
            this.treeImport.Value = null;
            this.treeImport.DoubleClick += new System.EventHandler(this.treeImport_DoubleClick);
            this.treeImport.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeImport_KeyDown);
            // 
            // pnlSegments
            // 
            this.pnlSegments.BackColor = System.Drawing.Color.Transparent;
            this.pnlSegments.Controls.Add(this.lblSegments);
            this.pnlSegments.Controls.Add(this.treeSegments);
            this.pnlSegments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSegments.Location = new System.Drawing.Point(0, 0);
            this.pnlSegments.Name = "pnlSegments";
            this.pnlSegments.Size = new System.Drawing.Size(328, 268);
            this.pnlSegments.TabIndex = 46;
            // 
            // treeSegments
            // 
            this.treeSegments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.Color.Transparent;
            this.treeSegments.Appearance = appearance6;
            this.treeSegments.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.treeSegments.DisableExcludedTypes = false;
            this.treeSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
            this.treeSegments.ExcludeNonMacPacTypes = false;
            this.treeSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
            this.treeSegments.FilterText = "";
            this.treeSegments.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeSegments.HideSelection = false;
            this.treeSegments.IsDirty = false;
            this.treeSegments.Location = new System.Drawing.Point(0, 22);
            this.treeSegments.Mode = LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FolderTree;
            this.treeSegments.Name = "treeSegments";
            _override2.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override2.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            this.treeSegments.Override = _override2;
            this.treeSegments.OwnerID = 0;
            this.treeSegments.ShowCheckboxes = false;
            this.treeSegments.ShowFindPaths = true;
            this.treeSegments.ShowTopUserFolderNode = false;
            this.treeSegments.Size = new System.Drawing.Size(328, 246);
            this.treeSegments.TabIndex = 5;
            this.treeSegments.TransparentBackground = true;
            this.treeSegments.DoubleClick += new System.EventHandler(this.treeSegments_DoubleClick);
            this.treeSegments.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeSegments_KeyDown);
            // 
            // DesignerLibraryManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnlSegments);
            this.Controls.Add(this.splitContents);
            this.Controls.Add(this.pnlVariables);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DesignerLibraryManager";
            this.Size = new System.Drawing.Size(328, 528);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VariableImportManager_KeyDown);
            this.pnlVariables.ResumeLayout(false);
            this.pnlVariables.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeImport)).EndInit();
            this.pnlSegments.ResumeLayout(false);
            this.pnlSegments.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeSegments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Splitter splitContents;
        private System.Windows.Forms.Label lblSegments;
        private LMP.Controls.FolderTreeView treeSegments;
        private System.Windows.Forms.Panel pnlVariables;
        private System.Windows.Forms.Panel pnlSegments;
        private System.Windows.Forms.ImageList ilImages;
        private LMP.Controls.TreeView treeImport;
        private LMP.Controls.ComboBox cmbCategory;

    }
}
