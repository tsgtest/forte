using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class ProgressList : Form
    {
        private List<string> m_aListItems = null;
        private bool m_bErrorOccurred = false;
        private int m_iStepsPerItem = 1;
        private int m_iCurIndex = 0;
        private int m_iCurSteps = 0;
        private bool m_bCanceled = false;

        public enum StatusTypes
        {
            Working = 1,
            Succeeded = 2,
            Failed = 3
        }
        public ProgressList()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Setup the properties of the dialog 
        /// </summary>
        /// <param name="xTitle"></param>
        /// <param name="xError"></param>
        /// <param name="aListItems"></param>
        /// <param name="iStepsPerItem"></param>
        public void Setup(string xTitle, string xDescription, string xError,
            List<string> aListItems, int iStepsPerItem)
        {
            Setup(xTitle, xDescription, xError, aListItems, iStepsPerItem, null, false);
        }
        public void Setup(string xTitle, string xDescription, string xError, 
            List<string> aListItems, int iStepsPerItem, IWin32Window iOwner, bool bAllowCancel)
        {
            btnOK.Visible = false;
            lblError.Visible = false;

            //Titlebar
            if (xTitle != null && xTitle != "")
                this.Text = xTitle;
            //Error message
            if (xError != null && xError != "")
                this.lblError.Text = xError;
            //Description
            if (xDescription != null && xDescription != "")
                this.lblProgress.Text = xDescription;

            //Number of ProgressBar ticks for each item
            if (iStepsPerItem > 0)
            {
                m_iStepsPerItem = iStepsPerItem;
            }
            //List of items to display
            m_aListItems = aListItems;

            if (!bAllowCancel)
            {
                this.btnCancel.Visible = false;
                this.lblCancel.Visible = false;
                this.pbStatus.Width = this.btnCancel.Left + this.btnCancel.Width -
                    this.pbStatus.Left;
            }

            pbStatus.Maximum = (m_aListItems.Count * iStepsPerItem) * 10;
            pbStatus.Step = 10;
            DataGridViewImageColumn oImageCol = new DataGridViewImageColumn();
            oImageCol.SortMode = DataGridViewColumnSortMode.NotSortable;
            oImageCol.DefaultCellStyle.NullValue = null;
            oImageCol.DefaultCellStyle.BackColor = System.Drawing.SystemColors.Control;
            oImageCol.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            oImageCol.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            oImageCol.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            oImageCol.Width = 25;
            DataGridViewTextBoxColumn oTextCol = new DataGridViewTextBoxColumn();
            oTextCol.Width = grdUpdateList.Width - 25;
            oTextCol.DefaultCellStyle.BackColor = System.Drawing.SystemColors.Control;
            oTextCol.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            oTextCol.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            oTextCol.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            oTextCol.SortMode = DataGridViewColumnSortMode.NotSortable;
            grdUpdateList.Columns.Clear();
            grdUpdateList.Columns.Add(oImageCol);
            grdUpdateList.Columns.Add(oTextCol);

            foreach (string xItem in m_aListItems)
            {
                grdUpdateList.Rows.Add(new object[] { null, xItem });
            }
            ////Default to centered position
            //this.Top = (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2;
            //this.Left = (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2;

            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
        }
        public void UpdateProgress()
        {
            UpdateProgress(null);
        }
        public void UpdateProgress(IWin32Window iOwner)
        {
            UpdateProgress(StatusTypes.Working, iOwner);
            System.Windows.Forms.Application.DoEvents();
        }
        public void UpdateProgress(StatusTypes iStatus)
        {
            UpdateProgress(iStatus, null);
        }
        /// <summary>
        /// Advances Progress bar and sets appropriate icon for current item
        /// </summary>
        /// <param name="iStatus"></param>
        public void UpdateProgress(StatusTypes iStatus, IWin32Window iOwner)
        {
            //Display if necessary
            if (!this.Visible)
                this.Show(iOwner);

            this.Cursor = Cursors.WaitCursor;
            DataGridViewRow oRow = grdUpdateList.Rows[m_iCurIndex];
            m_iCurSteps++;
            if (m_iCurSteps <= m_iStepsPerItem)
                pbStatus.PerformStep();
            
            switch (iStatus)
            {
                case StatusTypes.Failed:
                    //Last Action failed
                    oRow.Selected = true;
                    oRow.Cells[0].Value = imageList1.Images["Failed"];
                    this.Refresh();
                    m_bErrorOccurred = true;
                    m_iCurIndex++;
                    //Fill in any ticks skipped due to error condition
                    for (int i = m_iCurSteps; i < m_iStepsPerItem; i++)
                    {
                        pbStatus.PerformStep();
                    }
                    m_iCurSteps = 0;
                    break;
                case StatusTypes.Succeeded:
                    //Last Action completed successfully
                    oRow.Selected = true;
                    oRow.Cells[0].Value = imageList1.Images["Checked"];
                    this.Refresh();
                    m_iCurIndex++;
                    m_iCurSteps = 0;
                    break;
            }

            if (m_iCurIndex < m_aListItems.Count)
            {
                oRow = grdUpdateList.Rows[m_iCurIndex];
                if (m_iCurIndex + 1 > grdUpdateList.Height / oRow.Height)
                {
                    //Scroll up one to expose current row
                    grdUpdateList.FirstDisplayedScrollingRowIndex = 
                        m_iCurIndex - ((int)(grdUpdateList.Height / oRow.Height) - 1);
                }

                oRow.Selected = true;
                oRow.Cells[0].Value = imageList1.Images["Working"];

                //GLOG item #6701 - dcf
                if (!m_bCanceled)
                {
                    this.lblCurIndex.Text = (m_iCurIndex + 1).ToString() +
                        " of " + m_aListItems.Count.ToString();
                }
                else
                {
                    this.lblCancel.Text = "Canceled...";
                }
            }

            this.Refresh();
            System.Windows.Forms.Application.DoEvents();
            if (m_iCurIndex > m_aListItems.Count - 1)
                Finish();

        }
        /// <summary>
        /// Updating finished
        /// </summary>
        public void Finish()
        {
            pbStatus.PerformStep();
            this.Cursor = Cursors.Default;
            if (m_bErrorOccurred)
            {
                //An error occurred
                //Expose error label and OK button
                pbStatus.Visible = false;
                lblError.Visible = true;
                btnOK.Visible = true;
                grdUpdateList.ClearSelection();
                this.Refresh();
                System.Windows.Forms.Application.DoEvents();
            }
            else
                this.Hide();
        }
        public bool Canceled
        {
            get { return m_bCanceled; }
        }
        /// <summary>
        /// Clicked OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void ProgressList_Activated(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Refresh();
                System.Windows.Forms.Application.DoEvents();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_bCanceled = true;
            this.lblCurIndex.Text = "Canceled...";
        }

        private void btnCancel_MouseDown(object sender, MouseEventArgs e)
        {
            m_bCanceled = true;
            this.lblCurIndex.Text = "Canceled...";
        }
    }
}