using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Word = Microsoft.Office.Interop.Word;
using System.Drawing;
using Infragistics.Win;

namespace LMP.MacPac
{
    /// <summary>
    /// manages the collection of task pane controls -
    /// this is different than the collection of CustomTaskPanes -
    /// that collection is managed by LMP.Macpac102007.TaskPanes
    /// </summary>
    public static class TaskPanes
    {
        #region *********************fields**********************
        private static Hashtable m_oTaskPanes;
        #endregion
        #region *********************constructors**********************
        static TaskPanes()
        {
            m_oTaskPanes = new Hashtable();
        }
        #endregion
        #region *********************methods**********************
        /// <summary>
        /// Add entry to hashtable using oDoc as key
        /// </summary>
        /// <param name="oDoc"></param>
        /// <param name="oTaskpane"></param>
        public static void Add(Word.Document oDoc, TaskPane oTaskpane)
        {
            if (oDoc != null && oTaskpane != null)
                m_oTaskPanes.Add(oDoc, oTaskpane);

            LMP.Trace.WriteNameValuePairs("m_oTaskPanes.Count", m_oTaskPanes.Count);
        }
        /// <summary>
        /// Remove hashtable entry that has oDoc as the key
        /// </summary>
        /// <param name="oDoc"></param>
        public static void Remove(Word.Document oDoc)
        {
            try
            {
                if (oDoc != null && m_oTaskPanes.ContainsKey(oDoc))
                {
                    try
                    {
                        ((TaskPane)m_oTaskPanes[oDoc]).Dispose();
                    }
                    catch { }
                    m_oTaskPanes.Remove(oDoc);
                    LMP.Trace.WriteNameValuePairs("m_oTaskPanes", m_oTaskPanes.Count);
                }
            }
            catch { }
        }
        /// <summary>
        /// Return the Taskpane object connected with the specified doc
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static TaskPane Item(Word.Document oDoc)
        {
            if (oDoc != null)
                return (TaskPane)m_oTaskPanes[oDoc];
            else
                return null;
        }
        public static int Count
        {
            get { return m_oTaskPanes.Count; }
        }
        /// <summary>
        /// refreshes the appearance and data of all open task panes
        /// </summary>
        public static void RefreshAll(bool bContentManager, bool bEditorDesigner, bool bAppearance)
        {
            //get all task panes
            ICollection oTaskPanes = m_oTaskPanes.Values;

            //cycle through collection of task panes
            //refreshing each
            IEnumerator oEnum = oTaskPanes.GetEnumerator();
            while (oEnum.MoveNext())
            {
                TaskPane oTP = (TaskPane)oEnum.Current;
                oTP.Refresh(bContentManager, bEditorDesigner, bAppearance);
            }
        }
        /// <summary>
        /// refreshes content manager and designer of all open taskpanes
        /// </summary>
        public static void RefreshAll()
        {
            RefreshAll(true, false, true);
        }
        public static void RefreshAppearance()
        {
            // Obtain the backcolor and gradient that will be applied to the task pane and their tree.
            // Doing this optimizes performance since it prevents each task pane to hit the db for 
            // the back color and gradient settings.

            Color oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            Infragistics.Win.GradientStyle oGradient = 
                Session.CurrentUser.UserSettings.UseGradientInTaskPane ? 
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            //get all task panes
            ICollection oTaskPanes = m_oTaskPanes.Values;

            //cycle through collection of task panes
            //refreshing each
            IEnumerator oEnum = oTaskPanes.GetEnumerator();
            while (oEnum.MoveNext())
            {
                TaskPane oTP = (TaskPane)oEnum.Current;
                oTP.RefreshAppearance();
            }
        }
        public static void RemoveUnused()
        {
            //GLOG 3389: This function is used to remove unused TaskPane entries
            //from HashTable in Word 2003
            ICollection oDocs = m_oTaskPanes.Keys;
            List<Word.Document> oNonExistentDocs = new List<Word.Document>();

            //cycle through hashtable entries, 
            //looking for docs that have been closed
            foreach (Word.Document oDoc in oDocs)
            {
                try
                {
                    //this line will err if the doc
                    //no longer exists
                    string xName = oDoc.Name;
                }
                catch
                {
                    //add entry to the list of entries
                    //to be deleted from the hashtable
                    oNonExistentDocs.Add(oDoc);
                }
            }

            //cycle through list of entries to delete
            foreach (Word.Document oDoc in oNonExistentDocs)
            {
                Remove(oDoc);
            }
        }

        public static void SubscribeToXMLAfterInsertEvent(bool bSubscribe)
        {
            //get all task panes
            ICollection oTaskPanes = m_oTaskPanes.Values;

            //cycle through collection of task panes
            //refreshing each
            IEnumerator oEnum = oTaskPanes.GetEnumerator();
            while (oEnum.MoveNext())
            {
                TaskPane oTP = (TaskPane)oEnum.Current;
                oTP.SubscribeToXMLAfterInsertEvent(bSubscribe);
            }
        }
        #endregion
        #region *********************properties**********************
        #endregion
        #region *********************private methods**********************
        #endregion
    }
}
