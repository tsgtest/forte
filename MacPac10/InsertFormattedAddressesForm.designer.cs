namespace LMP.MacPac
{
    partial class InsertFormattedAddressesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFormat = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblInclude = new System.Windows.Forms.Label();
            this.radList = new System.Windows.Forms.RadioButton();
            this.radTable = new System.Windows.Forms.RadioButton();
            this.lblColumns = new System.Windows.Forms.Label();
            this.chkPhone = new System.Windows.Forms.CheckBox();
            this.chkFax = new System.Windows.Forms.CheckBox();
            this.chkEMail = new System.Windows.Forms.CheckBox();
            this.cmbSeparators = new System.Windows.Forms.ComboBox();
            this.cmbColumns = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblFormat
            // 
            this.lblFormat.AutoSize = true;
            this.lblFormat.Location = new System.Drawing.Point(9, 12);
            this.lblFormat.Name = "lblFormat";
            this.lblFormat.Size = new System.Drawing.Size(62, 15);
            this.lblFormat.TabIndex = 0;
            this.lblFormat.Text = "&Insert as a:";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(108, 156);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(76, 25);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(197, 156);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(76, 25);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblInclude
            // 
            this.lblInclude.AutoSize = true;
            this.lblInclude.Location = new System.Drawing.Point(9, 116);
            this.lblInclude.Name = "lblInclude";
            this.lblInclude.Size = new System.Drawing.Size(45, 15);
            this.lblInclude.TabIndex = 6;
            this.lblInclude.Text = "In&clude:";
            // 
            // radList
            // 
            this.radList.AutoSize = true;
            this.radList.Location = new System.Drawing.Point(27, 40);
            this.radList.Name = "radList";
            this.radList.Size = new System.Drawing.Size(109, 19);
            this.radList.TabIndex = 1;
            this.radList.TabStop = true;
            this.radList.Text = "&List separated by";
            this.radList.UseVisualStyleBackColor = true;
            // 
            // radTable
            // 
            this.radTable.AutoSize = true;
            this.radTable.Location = new System.Drawing.Point(27, 72);
            this.radTable.Name = "radTable";
            this.radTable.Size = new System.Drawing.Size(87, 19);
            this.radTable.TabIndex = 3;
            this.radTable.TabStop = true;
            this.radTable.Text = "&Table having";
            this.radTable.UseVisualStyleBackColor = true;
            // 
            // lblColumns
            // 
            this.lblColumns.AutoSize = true;
            this.lblColumns.Location = new System.Drawing.Point(165, 74);
            this.lblColumns.Name = "lblColumns";
            this.lblColumns.Size = new System.Drawing.Size(50, 15);
            this.lblColumns.TabIndex = 5;
            this.lblColumns.Text = "columns.";
            // 
            // chkPhone
            // 
            this.chkPhone.AutoSize = true;
            this.chkPhone.Location = new System.Drawing.Point(60, 115);
            this.chkPhone.Name = "chkPhone";
            this.chkPhone.Size = new System.Drawing.Size(57, 19);
            this.chkPhone.TabIndex = 7;
            this.chkPhone.Text = "&Phone";
            this.chkPhone.UseVisualStyleBackColor = true;
            // 
            // chkFax
            // 
            this.chkFax.AutoSize = true;
            this.chkFax.Location = new System.Drawing.Point(132, 115);
            this.chkFax.Name = "chkFax";
            this.chkFax.Size = new System.Drawing.Size(45, 19);
            this.chkFax.TabIndex = 8;
            this.chkFax.Text = "&Fax";
            this.chkFax.UseVisualStyleBackColor = true;
            // 
            // chkEMail
            // 
            this.chkEMail.AutoSize = true;
            this.chkEMail.Location = new System.Drawing.Point(200, 115);
            this.chkEMail.Name = "chkEMail";
            this.chkEMail.Size = new System.Drawing.Size(51, 19);
            this.chkEMail.TabIndex = 9;
            this.chkEMail.Text = "&Email";
            this.chkEMail.UseVisualStyleBackColor = true;
            // 
            // cmbSeparators
            // 
            this.cmbSeparators.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbSeparators.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSeparators.FormattingEnabled = true;
            this.cmbSeparators.Location = new System.Drawing.Point(142, 40);
            this.cmbSeparators.Name = "cmbSeparators";
            this.cmbSeparators.Size = new System.Drawing.Size(131, 23);
            this.cmbSeparators.TabIndex = 2;
            // 
            // cmbColumns
            // 
            this.cmbColumns.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbColumns.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbColumns.FormattingEnabled = true;
            this.cmbColumns.Location = new System.Drawing.Point(120, 72);
            this.cmbColumns.Name = "cmbColumns";
            this.cmbColumns.Size = new System.Drawing.Size(39, 23);
            this.cmbColumns.TabIndex = 4;
            // 
            // InsertFormattedAddressesForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(293, 197);
            this.Controls.Add(this.cmbColumns);
            this.Controls.Add(this.cmbSeparators);
            this.Controls.Add(this.chkEMail);
            this.Controls.Add(this.chkFax);
            this.Controls.Add(this.chkPhone);
            this.Controls.Add(this.lblColumns);
            this.Controls.Add(this.radTable);
            this.Controls.Add(this.radList);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblInclude);
            this.Controls.Add(this.lblFormat);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertFormattedAddressesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Insert Formatted Addresses";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFormat;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblInclude;
        private System.Windows.Forms.RadioButton radList;
        private System.Windows.Forms.RadioButton radTable;
        private System.Windows.Forms.Label lblColumns;
        private System.Windows.Forms.CheckBox chkPhone;
        private System.Windows.Forms.CheckBox chkFax;
        private System.Windows.Forms.CheckBox chkEMail;
        private System.Windows.Forms.ComboBox cmbSeparators;
        private System.Windows.Forms.ComboBox cmbColumns;
    }
}