namespace LMP.MacPac
{
    partial class InterrogatoriesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InterrogatoriesForm));
            this.grpSeparator = new System.Windows.Forms.GroupBox();
            this.optParaHeadingSeparator = new System.Windows.Forms.RadioButton();
            this.optNoHeadingSeparator = new System.Windows.Forms.RadioButton();
            this.btnUpdateFieldCodesStyles = new System.Windows.Forms.Button();
            this.btnFinish = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddCustom = new System.Windows.Forms.Button();
            this.grpCreateSequence = new System.Windows.Forms.GroupBox();
            this.txtEndingNumber = new System.Windows.Forms.TextBox();
            this.txtStartingNumber = new System.Windows.Forms.TextBox();
            this.lblEndNumber = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.chkInsertAsFieldCodes = new System.Windows.Forms.CheckBox();
            this.chkRepeatPreviousNumber = new System.Windows.Forms.CheckBox();
            this.chkAlternateBetweenQuestionsAndResponses = new System.Windows.Forms.CheckBox();
            this.lblType = new System.Windows.Forms.Label();
            this.grpFormatting = new System.Windows.Forms.GroupBox();
            this.btnModifyStyles = new System.Windows.Forms.Button();
            this.grpParagraph = new System.Windows.Forms.GroupBox();
            this.spnHeadingIndent = new LMP.Controls.Spinner();
            this.spnNextParaIndent = new LMP.Controls.Spinner();
            this.lblParagraphIndent = new System.Windows.Forms.Label();
            this.lblHeadingIndent = new System.Windows.Forms.Label();
            this.grpCharacter = new System.Windows.Forms.GroupBox();
            this.chkUnderline = new System.Windows.Forms.CheckBox();
            this.chkBold = new System.Windows.Forms.CheckBox();
            this.chkAllCaps = new System.Windows.Forms.CheckBox();
            this.chkSmallCaps = new System.Windows.Forms.CheckBox();
            this.optDirectFormat = new System.Windows.Forms.RadioButton();
            this.optStyleFormat = new System.Windows.Forms.RadioButton();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.chkShowAnswerNumber = new System.Windows.Forms.CheckBox();
            this.lstHeadings = new LMP.Controls.ListBox();
            this.grpSeparator.SuspendLayout();
            this.grpCreateSequence.SuspendLayout();
            this.grpFormatting.SuspendLayout();
            this.grpParagraph.SuspendLayout();
            this.grpCharacter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // grpSeparator
            // 
            this.grpSeparator.Controls.Add(this.optParaHeadingSeparator);
            this.grpSeparator.Controls.Add(this.optNoHeadingSeparator);
            this.grpSeparator.Location = new System.Drawing.Point(263, 138);
            this.grpSeparator.Name = "grpSeparator";
            this.grpSeparator.Size = new System.Drawing.Size(197, 70);
            this.grpSeparator.TabIndex = 10;
            this.grpSeparator.TabStop = false;
            this.grpSeparator.Text = "Heading Separator";
            // 
            // optParaHeadingSeparator
            // 
            this.optParaHeadingSeparator.AutoSize = true;
            this.optParaHeadingSeparator.Location = new System.Drawing.Point(11, 20);
            this.optParaHeadingSeparator.Name = "optParaHeadingSeparator";
            this.optParaHeadingSeparator.Size = new System.Drawing.Size(173, 17);
            this.optParaHeadingSeparator.TabIndex = 11;
            this.optParaHeadingSeparator.TabStop = true;
            this.optParaHeadingSeparator.Text = "Insert Heading On Its O&wn Line";
            this.optParaHeadingSeparator.UseVisualStyleBackColor = true;
            // 
            // optNoHeadingSeparator
            // 
            this.optNoHeadingSeparator.AutoSize = true;
            this.optNoHeadingSeparator.Location = new System.Drawing.Point(11, 43);
            this.optNoHeadingSeparator.Name = "optNoHeadingSeparator";
            this.optNoHeadingSeparator.Size = new System.Drawing.Size(143, 17);
            this.optNoHeadingSeparator.TabIndex = 12;
            this.optNoHeadingSeparator.TabStop = true;
            this.optNoHeadingSeparator.Text = "Insert as R&un-In Heading";
            this.optNoHeadingSeparator.UseVisualStyleBackColor = true;
            // 
            // btnUpdateFieldCodesStyles
            // 
            this.btnUpdateFieldCodesStyles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdateFieldCodesStyles.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btnUpdateFieldCodesStyles.Location = new System.Drawing.Point(49, 431);
            this.btnUpdateFieldCodesStyles.Name = "btnUpdateFieldCodesStyles";
            this.btnUpdateFieldCodesStyles.Size = new System.Drawing.Size(153, 25);
            this.btnUpdateFieldCodesStyles.TabIndex = 28;
            this.btnUpdateFieldCodesStyles.TabStop = false;
            this.btnUpdateFieldCodesStyles.Text = "U&pdate Field Codes";
            this.btnUpdateFieldCodesStyles.UseVisualStyleBackColor = true;
            this.btnUpdateFieldCodesStyles.Click += new System.EventHandler(this.btnUpdateFieldCodesStyles_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnFinish.Location = new System.Drawing.Point(308, 431);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(70, 25);
            this.btnFinish.TabIndex = 26;
            this.btnFinish.Text = "&Finish";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(385, 431);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(76, 25);
            this.btnCancel.TabIndex = 27;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddCustom
            // 
            this.btnAddCustom.Location = new System.Drawing.Point(14, 171);
            this.btnAddCustom.Name = "btnAddCustom";
            this.btnAddCustom.Size = new System.Drawing.Size(223, 23);
            this.btnAddCustom.TabIndex = 36;
            this.btnAddCustom.TabStop = false;
            this.btnAddCustom.Text = "Add &Custom Heading...";
            this.btnAddCustom.UseVisualStyleBackColor = true;
            this.btnAddCustom.Click += new System.EventHandler(this.btnAddCustom_Click);
            // 
            // grpCreateSequence
            // 
            this.grpCreateSequence.Controls.Add(this.txtEndingNumber);
            this.grpCreateSequence.Controls.Add(this.txtStartingNumber);
            this.grpCreateSequence.Controls.Add(this.lblEndNumber);
            this.grpCreateSequence.Controls.Add(this.lblStart);
            this.grpCreateSequence.Controls.Add(this.chkInsertAsFieldCodes);
            this.grpCreateSequence.Controls.Add(this.chkRepeatPreviousNumber);
            this.grpCreateSequence.Location = new System.Drawing.Point(264, 21);
            this.grpCreateSequence.Name = "grpCreateSequence";
            this.grpCreateSequence.Size = new System.Drawing.Size(197, 110);
            this.grpCreateSequence.TabIndex = 3;
            this.grpCreateSequence.TabStop = false;
            this.grpCreateSequence.Text = "Heading Numbering";
            // 
            // txtEndingNumber
            // 
            this.txtEndingNumber.Location = new System.Drawing.Point(136, 27);
            this.txtEndingNumber.Name = "txtEndingNumber";
            this.txtEndingNumber.Size = new System.Drawing.Size(43, 20);
            this.txtEndingNumber.TabIndex = 7;
            // 
            // txtStartingNumber
            // 
            this.txtStartingNumber.Location = new System.Drawing.Point(41, 27);
            this.txtStartingNumber.Name = "txtStartingNumber";
            this.txtStartingNumber.Size = new System.Drawing.Size(45, 20);
            this.txtStartingNumber.TabIndex = 5;
            this.txtStartingNumber.TextChanged += new System.EventHandler(this.txtStartingNumber_TextChanged);
            this.txtStartingNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtStartingNumber_Validating);
            // 
            // lblEndNumber
            // 
            this.lblEndNumber.AutoSize = true;
            this.lblEndNumber.Location = new System.Drawing.Point(110, 31);
            this.lblEndNumber.Name = "lblEndNumber";
            this.lblEndNumber.Size = new System.Drawing.Size(23, 13);
            this.lblEndNumber.TabIndex = 6;
            this.lblEndNumber.Text = "&To:";
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(8, 30);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(33, 13);
            this.lblStart.TabIndex = 4;
            this.lblStart.Text = "F&rom:";
            // 
            // chkInsertAsFieldCodes
            // 
            this.chkInsertAsFieldCodes.AutoSize = true;
            this.chkInsertAsFieldCodes.Checked = true;
            this.chkInsertAsFieldCodes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInsertAsFieldCodes.Location = new System.Drawing.Point(13, 60);
            this.chkInsertAsFieldCodes.Name = "chkInsertAsFieldCodes";
            this.chkInsertAsFieldCodes.Size = new System.Drawing.Size(169, 17);
            this.chkInsertAsFieldCodes.TabIndex = 8;
            this.chkInsertAsFieldCodes.Text = "&Insert Numbers as Field Codes";
            this.chkInsertAsFieldCodes.UseVisualStyleBackColor = true;
            // 
            // chkRepeatPreviousNumber
            // 
            this.chkRepeatPreviousNumber.AutoSize = true;
            this.chkRepeatPreviousNumber.Location = new System.Drawing.Point(13, 83);
            this.chkRepeatPreviousNumber.Name = "chkRepeatPreviousNumber";
            this.chkRepeatPreviousNumber.Size = new System.Drawing.Size(145, 17);
            this.chkRepeatPreviousNumber.TabIndex = 9;
            this.chkRepeatPreviousNumber.Text = "Repeat Pre&vious Number";
            this.chkRepeatPreviousNumber.UseVisualStyleBackColor = true;
            this.chkRepeatPreviousNumber.CheckedChanged += new System.EventHandler(this.chkRepeatPreviousNumber_CheckedChanged);
            // 
            // chkAlternateBetweenQuestionsAndResponses
            // 
            this.chkAlternateBetweenQuestionsAndResponses.AutoSize = true;
            this.chkAlternateBetweenQuestionsAndResponses.Location = new System.Drawing.Point(16, 207);
            this.chkAlternateBetweenQuestionsAndResponses.Name = "chkAlternateBetweenQuestionsAndResponses";
            this.chkAlternateBetweenQuestionsAndResponses.Size = new System.Drawing.Size(240, 17);
            this.chkAlternateBetweenQuestionsAndResponses.TabIndex = 2;
            this.chkAlternateBetweenQuestionsAndResponses.Text = "Alternate Between &Questions and Responses";
            this.chkAlternateBetweenQuestionsAndResponses.UseVisualStyleBackColor = true;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(14, 11);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(50, 13);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "&Heading:";
            // 
            // grpFormatting
            // 
            this.grpFormatting.Controls.Add(this.btnModifyStyles);
            this.grpFormatting.Controls.Add(this.grpParagraph);
            this.grpFormatting.Controls.Add(this.grpCharacter);
            this.grpFormatting.Controls.Add(this.optDirectFormat);
            this.grpFormatting.Controls.Add(this.optStyleFormat);
            this.grpFormatting.Location = new System.Drawing.Point(14, 262);
            this.grpFormatting.Name = "grpFormatting";
            this.grpFormatting.Size = new System.Drawing.Size(447, 162);
            this.grpFormatting.TabIndex = 13;
            this.grpFormatting.TabStop = false;
            this.grpFormatting.Text = "Formatting";
            // 
            // btnModifyStyles
            // 
            this.btnModifyStyles.Location = new System.Drawing.Point(318, 21);
            this.btnModifyStyles.Name = "btnModifyStyles";
            this.btnModifyStyles.Size = new System.Drawing.Size(95, 25);
            this.btnModifyStyles.TabIndex = 24;
            this.btnModifyStyles.TabStop = false;
            this.btnModifyStyles.Text = "&Modify Styles...";
            this.btnModifyStyles.UseVisualStyleBackColor = true;
            this.btnModifyStyles.Click += new System.EventHandler(this.btnModifyStyles_Click);
            // 
            // grpParagraph
            // 
            this.grpParagraph.Controls.Add(this.spnHeadingIndent);
            this.grpParagraph.Controls.Add(this.spnNextParaIndent);
            this.grpParagraph.Controls.Add(this.lblParagraphIndent);
            this.grpParagraph.Controls.Add(this.lblHeadingIndent);
            this.grpParagraph.Location = new System.Drawing.Point(225, 55);
            this.grpParagraph.Name = "grpParagraph";
            this.grpParagraph.Size = new System.Drawing.Size(210, 98);
            this.grpParagraph.TabIndex = 21;
            this.grpParagraph.TabStop = false;
            this.grpParagraph.Text = "Paragraph";
            // 
            // spnHeadingIndent
            // 
            this.spnHeadingIndent.AppendSymbol = true;
            this.spnHeadingIndent.AutoSize = true;
            this.spnHeadingIndent.BackColor = System.Drawing.SystemColors.Window;
            this.spnHeadingIndent.DecimalPlaces = 2;
            this.spnHeadingIndent.DisplayUnit = LMP.Data.mpMeasurementUnits.Inches;
            this.spnHeadingIndent.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spnHeadingIndent.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spnHeadingIndent.IsDirty = true;
            this.spnHeadingIndent.Location = new System.Drawing.Point(138, 21);
            this.spnHeadingIndent.Maximum = new decimal(new int[] {
            17,
            0,
            0,
            0});
            this.spnHeadingIndent.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnHeadingIndent.Name = "spnHeadingIndent";
            this.spnHeadingIndent.Size = new System.Drawing.Size(60, 22);
            this.spnHeadingIndent.SupportingValues = "";
            this.spnHeadingIndent.TabIndex = 26;
            this.spnHeadingIndent.Tag2 = null;
            this.spnHeadingIndent.Value = "0";
            // 
            // spnNextParaIndent
            // 
            this.spnNextParaIndent.AppendSymbol = true;
            this.spnNextParaIndent.AutoSize = true;
            this.spnNextParaIndent.BackColor = System.Drawing.SystemColors.Window;
            this.spnNextParaIndent.DecimalPlaces = 2;
            this.spnNextParaIndent.DisplayUnit = LMP.Data.mpMeasurementUnits.Inches;
            this.spnNextParaIndent.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spnNextParaIndent.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spnNextParaIndent.IsDirty = true;
            this.spnNextParaIndent.Location = new System.Drawing.Point(138, 56);
            this.spnNextParaIndent.Maximum = new decimal(new int[] {
            17,
            0,
            0,
            0});
            this.spnNextParaIndent.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnNextParaIndent.Name = "spnNextParaIndent";
            this.spnNextParaIndent.Size = new System.Drawing.Size(60, 22);
            this.spnNextParaIndent.SupportingValues = "";
            this.spnNextParaIndent.TabIndex = 27;
            this.spnNextParaIndent.Tag2 = null;
            this.spnNextParaIndent.Value = "0";
            // 
            // lblParagraphIndent
            // 
            this.lblParagraphIndent.AutoSize = true;
            this.lblParagraphIndent.Location = new System.Drawing.Point(8, 60);
            this.lblParagraphIndent.Name = "lblParagraphIndent";
            this.lblParagraphIndent.Size = new System.Drawing.Size(117, 13);
            this.lblParagraphIndent.TabIndex = 24;
            this.lblParagraphIndent.Text = "Ne&xt Paragraph Indent:";
            // 
            // lblHeadingIndent
            // 
            this.lblHeadingIndent.AutoSize = true;
            this.lblHeadingIndent.Location = new System.Drawing.Point(8, 26);
            this.lblHeadingIndent.Name = "lblHeadingIndent";
            this.lblHeadingIndent.Size = new System.Drawing.Size(83, 13);
            this.lblHeadingIndent.TabIndex = 22;
            this.lblHeadingIndent.Text = "Headin&g Indent:";
            // 
            // grpCharacter
            // 
            this.grpCharacter.Controls.Add(this.chkUnderline);
            this.grpCharacter.Controls.Add(this.chkBold);
            this.grpCharacter.Controls.Add(this.chkAllCaps);
            this.grpCharacter.Controls.Add(this.chkSmallCaps);
            this.grpCharacter.Location = new System.Drawing.Point(7, 55);
            this.grpCharacter.Name = "grpCharacter";
            this.grpCharacter.Size = new System.Drawing.Size(209, 98);
            this.grpCharacter.TabIndex = 16;
            this.grpCharacter.TabStop = false;
            this.grpCharacter.Text = "Character";
            // 
            // chkUnderline
            // 
            this.chkUnderline.AutoSize = true;
            this.chkUnderline.Location = new System.Drawing.Point(105, 33);
            this.chkUnderline.Name = "chkUnderline";
            this.chkUnderline.Size = new System.Drawing.Size(71, 17);
            this.chkUnderline.TabIndex = 18;
            this.chkUnderline.Text = "Under&line";
            this.chkUnderline.UseVisualStyleBackColor = true;
            // 
            // chkBold
            // 
            this.chkBold.AutoSize = true;
            this.chkBold.Location = new System.Drawing.Point(14, 33);
            this.chkBold.Name = "chkBold";
            this.chkBold.Size = new System.Drawing.Size(47, 17);
            this.chkBold.TabIndex = 17;
            this.chkBold.Text = "&Bold";
            this.chkBold.UseVisualStyleBackColor = true;
            // 
            // chkAllCaps
            // 
            this.chkAllCaps.AutoSize = true;
            this.chkAllCaps.Location = new System.Drawing.Point(14, 56);
            this.chkAllCaps.Name = "chkAllCaps";
            this.chkAllCaps.Size = new System.Drawing.Size(64, 17);
            this.chkAllCaps.TabIndex = 19;
            this.chkAllCaps.Text = "&All Caps";
            this.chkAllCaps.UseVisualStyleBackColor = true;
            this.chkAllCaps.CheckedChanged += new System.EventHandler(this.chkAllCaps_CheckedChanged);
            // 
            // chkSmallCaps
            // 
            this.chkSmallCaps.AutoSize = true;
            this.chkSmallCaps.Location = new System.Drawing.Point(105, 56);
            this.chkSmallCaps.Name = "chkSmallCaps";
            this.chkSmallCaps.Size = new System.Drawing.Size(78, 17);
            this.chkSmallCaps.TabIndex = 20;
            this.chkSmallCaps.Text = "&Small Caps";
            this.chkSmallCaps.UseVisualStyleBackColor = true;
            this.chkSmallCaps.CheckedChanged += new System.EventHandler(this.chkSmallCaps_CheckedChanged);
            // 
            // optDirectFormat
            // 
            this.optDirectFormat.AutoSize = true;
            this.optDirectFormat.Location = new System.Drawing.Point(15, 25);
            this.optDirectFormat.Name = "optDirectFormat";
            this.optDirectFormat.Size = new System.Drawing.Size(127, 17);
            this.optDirectFormat.TabIndex = 14;
            this.optDirectFormat.Text = "Use &Direct Formatting";
            this.optDirectFormat.UseVisualStyleBackColor = true;
            this.optDirectFormat.CheckedChanged += new System.EventHandler(this.optDirectFormat_CheckedChanged);
            // 
            // optStyleFormat
            // 
            this.optStyleFormat.AutoSize = true;
            this.optStyleFormat.Location = new System.Drawing.Point(158, 25);
            this.optStyleFormat.Name = "optStyleFormat";
            this.optStyleFormat.Size = new System.Drawing.Size(75, 17);
            this.optStyleFormat.TabIndex = 15;
            this.optStyleFormat.Text = "Use St&yles";
            this.optStyleFormat.UseVisualStyleBackColor = true;
            this.optStyleFormat.CheckedChanged += new System.EventHandler(this.optStyleFormat_CheckedChanged);
            // 
            // picHelp
            // 
            this.picHelp.Image = ((System.Drawing.Image)(resources.GetObject("picHelp.Image")));
            this.picHelp.Location = new System.Drawing.Point(14, 431);
            this.picHelp.Name = "picHelp";
            this.picHelp.Size = new System.Drawing.Size(26, 25);
            this.picHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picHelp.TabIndex = 37;
            this.picHelp.TabStop = false;
            this.picHelp.Click += new System.EventHandler(this.picHelp_Click);
            // 
            // chkShowAnswerNumber
            // 
            this.chkShowAnswerNumber.AutoSize = true;
            this.chkShowAnswerNumber.Location = new System.Drawing.Point(16, 231);
            this.chkShowAnswerNumber.Name = "chkShowAnswerNumber";
            this.chkShowAnswerNumber.Size = new System.Drawing.Size(286, 17);
            this.chkShowAnswerNumber.TabIndex = 3;
            this.chkShowAnswerNumber.Text = "&Show Number in Response heading (e.g. Response 1:)";
            this.chkShowAnswerNumber.UseVisualStyleBackColor = true;
            // 
            // lstHeadings
            // 
            this.lstHeadings.AllowEmptyValue = false;
            this.lstHeadings.FormattingEnabled = true;
            this.lstHeadings.IsDirty = false;
            this.lstHeadings.ListName = "";
            this.lstHeadings.Location = new System.Drawing.Point(14, 27);
            this.lstHeadings.Name = "lstHeadings";
            this.lstHeadings.ScrollAlwaysVisible = true;
            this.lstHeadings.Size = new System.Drawing.Size(223, 134);
            this.lstHeadings.SupportingValues = "";
            this.lstHeadings.TabIndex = 1;
            this.lstHeadings.Tag2 = null;
            this.lstHeadings.Value = "";
            this.lstHeadings.SelectedIndexChanged += new System.EventHandler(this.lstHeadings_SelectedIndexChanged);
            // 
            // InterrogatoriesForm
            // 
            this.AcceptButton = this.btnFinish;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(476, 468);
            this.Controls.Add(this.chkShowAnswerNumber);
            this.Controls.Add(this.picHelp);
            this.Controls.Add(this.grpSeparator);
            this.Controls.Add(this.btnUpdateFieldCodesStyles);
            this.Controls.Add(this.btnFinish);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAddCustom);
            this.Controls.Add(this.lstHeadings);
            this.Controls.Add(this.grpCreateSequence);
            this.Controls.Add(this.chkAlternateBetweenQuestionsAndResponses);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.grpFormatting);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InterrogatoriesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert Discovery Questions and Responses";
            this.Load += new System.EventHandler(this.InterrogatoriesForm_Load);
            this.grpSeparator.ResumeLayout(false);
            this.grpSeparator.PerformLayout();
            this.grpCreateSequence.ResumeLayout(false);
            this.grpCreateSequence.PerformLayout();
            this.grpFormatting.ResumeLayout(false);
            this.grpFormatting.PerformLayout();
            this.grpParagraph.ResumeLayout(false);
            this.grpParagraph.PerformLayout();
            this.grpCharacter.ResumeLayout(false);
            this.grpCharacter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        // JAB TODO: REMOVE : private System.Windows.Forms.ListBox lstTypes;
        private System.Windows.Forms.GroupBox grpSeparator;
        private System.Windows.Forms.RadioButton optParaHeadingSeparator;
        private System.Windows.Forms.RadioButton optNoHeadingSeparator;
        private System.Windows.Forms.Button btnUpdateFieldCodesStyles;
        private System.Windows.Forms.Button btnFinish;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAddCustom;
        private LMP.Controls.ListBox lstHeadings;
        private System.Windows.Forms.GroupBox grpCreateSequence;
        private System.Windows.Forms.CheckBox chkRepeatPreviousNumber;
        private System.Windows.Forms.CheckBox chkInsertAsFieldCodes;
        private System.Windows.Forms.CheckBox chkAlternateBetweenQuestionsAndResponses;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.GroupBox grpFormatting;
        private System.Windows.Forms.Button btnModifyStyles;
        private System.Windows.Forms.RadioButton optDirectFormat;
        private System.Windows.Forms.RadioButton optStyleFormat;
        private System.Windows.Forms.TextBox txtEndingNumber;
        private System.Windows.Forms.TextBox txtStartingNumber;
        private System.Windows.Forms.Label lblEndNumber;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.GroupBox grpParagraph;
        private System.Windows.Forms.Label lblParagraphIndent;
        private System.Windows.Forms.Label lblHeadingIndent;
        private System.Windows.Forms.GroupBox grpCharacter;
        private System.Windows.Forms.CheckBox chkUnderline;
        private System.Windows.Forms.CheckBox chkBold;
        private System.Windows.Forms.CheckBox chkAllCaps;
        private System.Windows.Forms.CheckBox chkSmallCaps;
        private System.Windows.Forms.PictureBox picHelp;
        private System.Windows.Forms.CheckBox chkShowAnswerNumber;
        private Controls.Spinner spnHeadingIndent;
        private Controls.Spinner spnNextParaIndent;
    }
}