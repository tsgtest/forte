namespace LMP.MacPac
{
    partial class OfficeAddressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstOffice = new System.Windows.Forms.ListBox();
            this.chkIncludeFirmName = new System.Windows.Forms.CheckBox();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblOffice = new System.Windows.Forms.Label();
            this.lblStyle = new System.Windows.Forms.Label();
            this.cmbSeparators = new System.Windows.Forms.ComboBox();
            this.lblSeparators = new System.Windows.Forms.Label();
            this.cmbStyle = new LMP.Controls.ComboBox();
            this.SuspendLayout();
            // 
            // lstOffice
            // 
            this.lstOffice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstOffice.FormattingEnabled = true;
            this.lstOffice.ItemHeight = 15;
            this.lstOffice.Location = new System.Drawing.Point(12, 29);
            this.lstOffice.Name = "lstOffice";
            this.lstOffice.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstOffice.Size = new System.Drawing.Size(214, 214);
            this.lstOffice.TabIndex = 1;
            this.lstOffice.SelectedIndexChanged += new System.EventHandler(this.lstOffice_SelectedIndexChanged_1);
            this.lstOffice.DoubleClick += new System.EventHandler(this.btnInsert_Click);
            // 
            // chkIncludeFirmName
            // 
            this.chkIncludeFirmName.AutoSize = true;
            this.chkIncludeFirmName.Location = new System.Drawing.Point(252, 110);
            this.chkIncludeFirmName.Name = "chkIncludeFirmName";
            this.chkIncludeFirmName.Size = new System.Drawing.Size(165, 19);
            this.chkIncludeFirmName.TabIndex = 6;
            this.chkIncludeFirmName.Text = "Include &Firm/Company Name";
            this.chkIncludeFirmName.UseVisualStyleBackColor = true;
            // 
            // btnInsert
            // 
            this.btnInsert.AutoSize = true;
            this.btnInsert.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnInsert.Location = new System.Drawing.Point(252, 218);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(81, 25);
            this.btnInsert.TabIndex = 7;
            this.btnInsert.Text = "&Insert";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(339, 218);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 25);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblOffice
            // 
            this.lblOffice.AutoSize = true;
            this.lblOffice.BackColor = System.Drawing.Color.Transparent;
            this.lblOffice.ForeColor = System.Drawing.Color.Black;
            this.lblOffice.Location = new System.Drawing.Point(12, 12);
            this.lblOffice.Name = "lblOffice";
            this.lblOffice.Size = new System.Drawing.Size(158, 15);
            this.lblOffice.TabIndex = 0;
            this.lblOffice.Text = "&Office (Ctl+Click to multiselect):";
            // 
            // lblStyle
            // 
            this.lblStyle.AutoSize = true;
            this.lblStyle.BackColor = System.Drawing.Color.Transparent;
            this.lblStyle.ForeColor = System.Drawing.Color.Black;
            this.lblStyle.Location = new System.Drawing.Point(246, 12);
            this.lblStyle.Name = "lblStyle";
            this.lblStyle.Size = new System.Drawing.Size(37, 15);
            this.lblStyle.TabIndex = 2;
            this.lblStyle.Text = "St&yle :";
            // 
            // cmbSeparators
            // 
            this.cmbSeparators.FormattingEnabled = true;
            this.cmbSeparators.Location = new System.Drawing.Point(248, 81);
            this.cmbSeparators.Name = "cmbSeparators";
            this.cmbSeparators.Size = new System.Drawing.Size(165, 23);
            this.cmbSeparators.TabIndex = 5;
            // 
            // lblSeparators
            // 
            this.lblSeparators.AutoSize = true;
            this.lblSeparators.BackColor = System.Drawing.Color.Transparent;
            this.lblSeparators.ForeColor = System.Drawing.Color.Black;
            this.lblSeparators.Location = new System.Drawing.Point(246, 65);
            this.lblSeparators.Name = "lblSeparators";
            this.lblSeparators.Size = new System.Drawing.Size(173, 15);
            this.lblSeparators.TabIndex = 4;
            this.lblSeparators.Text = "Separate Multiple Addresses With:";
            // 
            // cmbStyle
            // 
            this.cmbStyle.AllowEmptyValue = false;
            this.cmbStyle.AutoSize = true;
            this.cmbStyle.Borderless = false;
            this.cmbStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbStyle.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStyle.IsDirty = false;
            this.cmbStyle.LimitToList = true;
            this.cmbStyle.ListName = "";
            this.cmbStyle.Location = new System.Drawing.Point(248, 28);
            this.cmbStyle.MaxDropDownItems = 8;
            this.cmbStyle.Name = "cmbStyle";
            this.cmbStyle.SelectedIndex = -1;
            this.cmbStyle.SelectedValue = null;
            this.cmbStyle.SelectionLength = 0;
            this.cmbStyle.SelectionStart = 0;
            this.cmbStyle.Size = new System.Drawing.Size(165, 26);
            this.cmbStyle.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbStyle.SupportingValues = "";
            this.cmbStyle.TabIndex = 3;
            this.cmbStyle.Tag2 = null;
            this.cmbStyle.Value = "";
            // 
            // OfficeAddressForm
            // 
            this.AcceptButton = this.btnInsert;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(432, 261);
            this.Controls.Add(this.cmbSeparators);
            this.Controls.Add(this.lblSeparators);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.chkIncludeFirmName);
            this.Controls.Add(this.cmbStyle);
            this.Controls.Add(this.lstOffice);
            this.Controls.Add(this.lblOffice);
            this.Controls.Add(this.lblStyle);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OfficeAddressForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert Office Address";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstOffice;
        private System.Windows.Forms.CheckBox chkIncludeFirmName;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblOffice;
        private System.Windows.Forms.Label lblStyle;
        private System.Windows.Forms.ComboBox cmbSeparators;
        private System.Windows.Forms.Label lblSeparators;
        private Controls.ComboBox cmbStyle;
    }
}