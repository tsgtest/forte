﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class ImportDataFileMessageBox : Form
    {
        public ImportDataFileMessageBox()
        {
            InitializeComponent();
        }
        public string MessageText
        {
            get { return lblMessage.Text; }
            set
            {
                lblMessage.Text = value;
                Size sz = new Size(pnlLabel.ClientSize.Width, Int32.MaxValue);
                sz = TextRenderer.MeasureText(lblMessage.Text, lblMessage.Font, sz, TextFormatFlags.WordBreak | TextFormatFlags.Left | TextFormatFlags.TextBoxControl);
                int iHeight = sz.Height;
                pnlLabel.Height = iHeight;
                this.Height =  pnlLabel.Top + pnlLabel.Height + SystemInformation.CaptionHeight + SystemInformation.BorderSize.Height + 65;
                splitMain.SplitterDistance = splitMain.Height - 40;
            }
        }
        private void ImportDataFileMessageBox_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = LMP.ComponentProperties.ProductName;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void splitMain_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawIcon(SystemIcons.Information, new Rectangle(pnlIcon.Location, new Size(32, 32)));
        }
    }
}
