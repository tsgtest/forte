using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    internal partial class AssignmentsForm : Form
    {
        #region *********************constants*********************
        const int ORIG_STATE_COL = 0;
        const int ITEM_ID_COL = 1;
        #endregion
        #region *********************fields*********************
        private mpObjectTypes m_iTargetObjectType;
        private int m_iTargetObjectID;
        private mpObjectTypes m_iAssignedObjectType;
        private object[,] m_aStateArray;

        // GLOG : 3047 : JAB
        // Declare an array to store the items that exists for a given filter.
        private object[] m_aFilteredStateArray;

        // GLOG : 3047 : JAB
        // Introduce a field for the table containing the current assignments.

        DataTable m_oDT = null;
        #endregion
        #region *********************constructors*********************
        public AssignmentsForm(mpObjectTypes iTargetObjectType, int iTargetObjectID,
            mpObjectTypes iAssignedObjectType)
        {
            try
            {
                InitializeComponent();

                //set field values
                this.m_iAssignedObjectType = iAssignedObjectType;
                this.m_iTargetObjectID = iTargetObjectID;
                this.m_iTargetObjectType = iTargetObjectType;

                //get available assignments of the specified type
                m_oDT = Assignments.GetAvailableAssignments(
                    iTargetObjectType, iTargetObjectID, iAssignedObjectType);

                //create state array to store ID and original state
                m_aStateArray = new object[m_oDT.Rows.Count, 2];

                // GLOG : 3047 : JAB
                // Create an array to store the items that exists for a given filter.
                m_aFilteredStateArray = new object[m_oDT.Rows.Count];

                //cycle through table rows, adding each to
                //the list and state array
                int i = 0;
                foreach (DataRow oDR in m_oDT.Rows)
                {
                    //add to list
                    this.clstAssignments.Items.Add(oDR["DisplayName"].ToString());

                    //check item if it is already assigned
                    if (oDR["Assigned"].ToString() == "-1")
                        this.clstAssignments.SetItemChecked(i, true);

                    //add original state and ID to storage array
                    m_aStateArray[i, ITEM_ID_COL] = oDR["ID"];
                    m_aStateArray[i, ORIG_STATE_COL] = 
                        oDR["Assigned"] == null ? "0" : oDR["Assigned"];

                    // GLOG : 3047 : JAB
                    // Store the id of the items in this initial view.
                    this.m_aFilteredStateArray[i] = oDR["ID"];
                    
                    i++;
                }

                // GLOG : 3047 : JAB
                // Bring the first checked item into view.
                ShowFirstCheckedItem();

                // Localize the form
                LocalizeForm();

                // Only enable the clear button if there are items to clear.
                this.btnClear.Enabled = (this.clstAssignments.Items.Count > 0);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UIException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotInitializeAssignmentsForm"), oE);
            }
        }
        #endregion
        #region *********************event handlers*********************
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                EditAssignments();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// edits the assignments for the specified
        /// target object per changes in the form
        /// </summary>
        private void EditAssignments()
        {
            try
            {
                //cycle through items, comparing current
                //checked state to original state stored in state array
                for (int i = 0; i < this.m_oDT.Rows.Count; i++)
                {
                    object oID = m_aStateArray[i, ITEM_ID_COL];

                    // GLOG : 3047 : JAB
                    // Use the assignments datatable to determine which assignments have change.
                    bool bIsChecked = (m_oDT.Rows[i]["Assigned"].ToString() == "-1");
                    bool bIsCurrentlyAssigned = m_aStateArray[i, ORIG_STATE_COL].ToString() == "-1";

                    if (bIsChecked && !bIsCurrentlyAssigned)
                    {
                        //item is checked and original state 
                        //was not checked - add assignment
                        if (oID != null)
                            Assignments.Add(this.m_iTargetObjectType, this.m_iTargetObjectID,
                                this.m_iAssignedObjectType, (int)oID);
                    }
                    else if (!bIsChecked && bIsCurrentlyAssigned)
                    {
                        //item is not checked and original state 
                        //was checked - delete assignment
                        Assignments.Delete(this.m_iTargetObjectType, this.m_iTargetObjectID,
                            this.m_iAssignedObjectType, (int)oID);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        
        /// <summary>
        /// GLOG : 3047 : JAB
        /// Set all the check boxes to unchecked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.clstAssignments.Items.Count; i++)
            {
                clstAssignments.SetItemCheckState(i, CheckState.Unchecked);
            }            
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Display the first items that is checked.
        /// </summary>
        private void ShowFirstCheckedItem()
        {
            if (this.clstAssignments.Items.Count > 0)
            {
                if (this.clstAssignments.CheckedItems.Count > 0)
                {
                    this.clstAssignments.SetSelected(this.clstAssignments.CheckedIndices[0], true);
                }
                else
                {
                    this.clstAssignments.SetSelected(0, false);
                }
            }
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Localize the form.
        /// </summary>
        private void LocalizeForm()
        {
            this.btnClear.Text = LMP.Resources.GetLangString("AssignmentsFormClear");
            this.btnSearch.Text = LMP.Resources.GetLangString("AssignmentsFormSearch");
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Handle the enter key as a trigger to perform the search.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchForAssignment();
            }
            e.Handled = (e.KeyCode == Keys.Enter);
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Search for the assignment specified in the search text box.
        /// </summary>
        private void SearchForAssignment()
        {
            string xSearchText = this.txtSearch.Text;
            // JAB TODO :   Find out if LMP method exists that makes the search
            //              string sql safe.
            // Escape characters that are reserved by sql.
            //  Escape Apostrophe
            //xSearchText = xSearchText.Replace("'", "''");

            ////  Escape the square brackets
            //xSearchText = xSearchText.Replace("[", @"\[");
            //xSearchText = xSearchText.Replace("]", @"\]");

            ////  Escape the % character
            //xSearchText = xSearchText.Replace("%", @"\%");

            ////  Esacape the _ character
            //xSearchText = xSearchText.Replace("_", @"\_");

            DataRow[] oRows = m_oDT.Select("DisplayName LIKE '%" + xSearchText + "%'");
            this.clstAssignments.Items.Clear();

            // Set the checked boxs based on the state stored in the assignments table.
            int i = 0;
            foreach (DataRow oDR in oRows)
            {
                // Add a check box for this assignment to the list.
                this.clstAssignments.Items.Add(oDR["DisplayName"].ToString());

                // Set the item as checked if it is assigned.
                if (oDR["Assigned"].ToString() == "-1")
                    this.clstAssignments.SetItemChecked(i, true);

                // Store the id of each item so that it can be used to update the
                // corresponding row in the assignments table.
                this.m_aFilteredStateArray[i] = oDR["ID"];

                i++;
            }
            // Bring the first checked item into view.
            ShowFirstCheckedItem();
            return;
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Perform a search when the search button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.SearchForAssignment();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 3047 : JAB
        /// Disable the search button when no search text exists.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // Only enable the clear button if there are items to clear.
                this.btnClear.Enabled = (this.clstAssignments.Items.Count > 0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // GLOG : 3047 : JAB
        // As an assignment gets checked or unchecked, modify the table of assignments.
        private void clstAssignments_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            try
            {
                if (m_aStateArray[e.Index, ITEM_ID_COL] != null)
                {
                    DataRow[] oRows = m_oDT.Select("ID = " + (int)m_aFilteredStateArray[e.Index]);

                    if (oRows.Length > 0)
                    {
                        oRows[0]["Assigned"] = (e.NewValue == CheckState.Checked) ? (short)-1 : (short)0;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}