using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    internal partial class SegmentChooserForm : Form
    {
        #region *********************fields*********************
        private bool m_bCancel = true;
        #endregion
        #region ****************constructors********************
        public SegmentChooserForm(int iTargetObjectID, mpObjectTypes oTargetObjectType,
            mpObjectTypes oAssignedObjectType, int iDefaultAssignedObjectID, string xCaption)
        {
            InitializeComponent();
            Setup(iTargetObjectID, oTargetObjectType, oAssignedObjectType,
                iDefaultAssignedObjectID, xCaption);
        }
        #endregion
        #region *********************properties*********************
        public int Value
        {
            get { return Int32.Parse(this.chooser1.Value); }
        }
        public bool Cancelled
        {
            get { return m_bCancel; }
        }
        #endregion

        #region *********************methods*********************
        private void Setup(int iTargetObjectID, mpObjectTypes oTargetObjectType,
            mpObjectTypes oAssignedObjectType, int iDefaultAssignedObjectID, string xCaption)
        {
            //set chooser properties
            this.chooser1.TargetObjectID = iTargetObjectID;
            this.chooser1.TargetObjectType = oTargetObjectType;
            this.chooser1.AssignedObjectType = oAssignedObjectType;
            this.chooser1.ExecuteFinalSetup();
            if (iDefaultAssignedObjectID > 0)
                this.chooser1.Value = iDefaultAssignedObjectID.ToString();

            //set form caption
            this.Text = xCaption;
        }
        #endregion

        #region *********************events*********************
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                m_bCancel = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.Close();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                //GLOG 6388 (dm) - validate chooser
                if (string.IsNullOrEmpty(this.chooser1.Value))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_LimitedToList"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    this.chooser1.Focus();
                    return;
                }

                m_bCancel = false;
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void chooser1_ValueChanged(object sender, EventArgs e)
        {
            this.btnOK.Enabled = !string.IsNullOrEmpty(this.chooser1.Value);
        }
    }
}