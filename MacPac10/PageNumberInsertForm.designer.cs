namespace LMP.MacPac
{
    partial class PageNumberInsertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbFormat = new LMP.Controls.ComboBox();
            this.chkInsertOnFirstSectionPage = new System.Windows.Forms.CheckBox();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblFormat = new System.Windows.Forms.Label();
            this.cmbLocation = new LMP.Controls.ComboBox();
            this.lblLocation = new System.Windows.Forms.Label();
            this.cmbAlignment = new LMP.Controls.ComboBox();
            this.lblAlignment = new System.Windows.Forms.Label();
            this.lblTextBefore = new System.Windows.Forms.Label();
            this.lblTextAfter = new System.Windows.Forms.Label();
            this.optStartAtNumber = new System.Windows.Forms.RadioButton();
            this.optContinueFromPrevious = new System.Windows.Forms.RadioButton();
            this.lstStartAtNumber = new System.Windows.Forms.NumericUpDown();
            this.gbApplyTo = new System.Windows.Forms.GroupBox();
            this.txtMultiple = new System.Windows.Forms.TextBox();
            this.rbMultiple = new System.Windows.Forms.RadioButton();
            this.rbSection = new System.Windows.Forms.RadioButton();
            this.rbDocument = new System.Windows.Forms.RadioButton();
            this.gbOptions = new System.Windows.Forms.GroupBox();
            this.lblIncludeTotalPages = new System.Windows.Forms.Label();
            this.cmbIncludeTotalPages = new LMP.Controls.ComboBox();
            this.txtTextBefore = new System.Windows.Forms.TextBox();
            this.txtTextAfter = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lstStartAtNumber)).BeginInit();
            this.gbApplyTo.SuspendLayout();
            this.gbOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbFormat
            // 
            this.cmbFormat.AllowEmptyValue = false;
            this.cmbFormat.AutoSize = true;
            this.cmbFormat.Borderless = false;
            this.cmbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbFormat.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFormat.IsDirty = false;
            this.cmbFormat.LimitToList = true;
            this.cmbFormat.ListName = "";
            this.cmbFormat.Location = new System.Drawing.Point(16, 28);
            this.cmbFormat.MaxDropDownItems = 8;
            this.cmbFormat.Name = "cmbFormat";
            this.cmbFormat.SelectedIndex = -1;
            this.cmbFormat.SelectedValue = null;
            this.cmbFormat.SelectionLength = 0;
            this.cmbFormat.SelectionStart = 0;
            this.cmbFormat.Size = new System.Drawing.Size(254, 27);
            this.cmbFormat.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbFormat.SupportingValues = "";
            this.cmbFormat.TabIndex = 2;
            this.cmbFormat.Tag2 = null;
            this.cmbFormat.Value = "";
            // 
            // chkInsertOnFirstSectionPage
            // 
            this.chkInsertOnFirstSectionPage.AutoSize = true;
            this.chkInsertOnFirstSectionPage.Location = new System.Drawing.Point(13, 32);
            this.chkInsertOnFirstSectionPage.Name = "chkInsertOnFirstSectionPage";
            this.chkInsertOnFirstSectionPage.Size = new System.Drawing.Size(180, 19);
            this.chkInsertOnFirstSectionPage.TabIndex = 1;
            this.chkInsertOnFirstSectionPage.Text = "Include on &First Page of Section";
            this.chkInsertOnFirstSectionPage.UseVisualStyleBackColor = true;
            // 
            // btnInsert
            // 
            this.btnInsert.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnInsert.Location = new System.Drawing.Point(372, 353);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(81, 25);
            this.btnInsert.TabIndex = 19;
            this.btnInsert.Text = "&Insert";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            this.btnInsert.Enter += new System.EventHandler(this.btnInsert_Enter);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(459, 353);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 25);
            this.btnCancel.TabIndex = 20;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblFormat
            // 
            this.lblFormat.AutoSize = true;
            this.lblFormat.BackColor = System.Drawing.Color.Transparent;
            this.lblFormat.ForeColor = System.Drawing.Color.Black;
            this.lblFormat.Location = new System.Drawing.Point(15, 12);
            this.lblFormat.Name = "lblFormat";
            this.lblFormat.Size = new System.Drawing.Size(47, 15);
            this.lblFormat.TabIndex = 1;
            this.lblFormat.Text = "&Format :";
            // 
            // cmbLocation
            // 
            this.cmbLocation.AllowEmptyValue = false;
            this.cmbLocation.AutoSize = true;
            this.cmbLocation.Borderless = false;
            this.cmbLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLocation.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLocation.IsDirty = false;
            this.cmbLocation.LimitToList = true;
            this.cmbLocation.ListName = "";
            this.cmbLocation.Location = new System.Drawing.Point(16, 264);
            this.cmbLocation.MaxDropDownItems = 8;
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.SelectedIndex = -1;
            this.cmbLocation.SelectedValue = null;
            this.cmbLocation.SelectionLength = 0;
            this.cmbLocation.SelectionStart = 0;
            this.cmbLocation.Size = new System.Drawing.Size(254, 27);
            this.cmbLocation.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLocation.SupportingValues = "";
            this.cmbLocation.TabIndex = 10;
            this.cmbLocation.Tag2 = null;
            this.cmbLocation.Value = "";
            this.cmbLocation.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLocation_ValueChanged);
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.BackColor = System.Drawing.Color.Transparent;
            this.lblLocation.ForeColor = System.Drawing.Color.Black;
            this.lblLocation.Location = new System.Drawing.Point(15, 248);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(51, 15);
            this.lblLocation.TabIndex = 9;
            this.lblLocation.Text = "&Location:";
            // 
            // cmbAlignment
            // 
            this.cmbAlignment.AllowEmptyValue = false;
            this.cmbAlignment.AutoSize = true;
            this.cmbAlignment.Borderless = false;
            this.cmbAlignment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbAlignment.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAlignment.IsDirty = false;
            this.cmbAlignment.LimitToList = true;
            this.cmbAlignment.ListName = "";
            this.cmbAlignment.Location = new System.Drawing.Point(16, 314);
            this.cmbAlignment.MaxDropDownItems = 8;
            this.cmbAlignment.Name = "cmbAlignment";
            this.cmbAlignment.SelectedIndex = -1;
            this.cmbAlignment.SelectedValue = null;
            this.cmbAlignment.SelectionLength = 0;
            this.cmbAlignment.SelectionStart = 0;
            this.cmbAlignment.Size = new System.Drawing.Size(254, 26);
            this.cmbAlignment.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbAlignment.SupportingValues = "";
            this.cmbAlignment.TabIndex = 12;
            this.cmbAlignment.Tag2 = null;
            this.cmbAlignment.Value = "";
            // 
            // lblAlignment
            // 
            this.lblAlignment.AutoSize = true;
            this.lblAlignment.BackColor = System.Drawing.Color.Transparent;
            this.lblAlignment.ForeColor = System.Drawing.Color.Black;
            this.lblAlignment.Location = new System.Drawing.Point(15, 298);
            this.lblAlignment.Name = "lblAlignment";
            this.lblAlignment.Size = new System.Drawing.Size(56, 15);
            this.lblAlignment.TabIndex = 11;
            this.lblAlignment.Text = "&Alignment:";
            // 
            // lblTextBefore
            // 
            this.lblTextBefore.AutoSize = true;
            this.lblTextBefore.BackColor = System.Drawing.Color.Transparent;
            this.lblTextBefore.ForeColor = System.Drawing.Color.Black;
            this.lblTextBefore.Location = new System.Drawing.Point(15, 112);
            this.lblTextBefore.Name = "lblTextBefore";
            this.lblTextBefore.Size = new System.Drawing.Size(67, 15);
            this.lblTextBefore.TabIndex = 5;
            this.lblTextBefore.Text = "Text &Before:";
            // 
            // lblTextAfter
            // 
            this.lblTextAfter.AutoSize = true;
            this.lblTextAfter.BackColor = System.Drawing.Color.Transparent;
            this.lblTextAfter.ForeColor = System.Drawing.Color.Black;
            this.lblTextAfter.Location = new System.Drawing.Point(15, 178);
            this.lblTextAfter.Name = "lblTextAfter";
            this.lblTextAfter.Size = new System.Drawing.Size(58, 15);
            this.lblTextAfter.TabIndex = 7;
            this.lblTextAfter.Text = "Text &After:";
            // 
            // optStartAtNumber
            // 
            this.optStartAtNumber.AutoSize = true;
            this.optStartAtNumber.Location = new System.Drawing.Point(13, 67);
            this.optStartAtNumber.Name = "optStartAtNumber";
            this.optStartAtNumber.Size = new System.Drawing.Size(104, 19);
            this.optStartAtNumber.TabIndex = 2;
            this.optStartAtNumber.TabStop = true;
            this.optStartAtNumber.Text = "&Start Number at:";
            this.optStartAtNumber.UseVisualStyleBackColor = true;
            // 
            // optContinueFromPrevious
            // 
            this.optContinueFromPrevious.AutoSize = true;
            this.optContinueFromPrevious.Location = new System.Drawing.Point(13, 100);
            this.optContinueFromPrevious.Name = "optContinueFromPrevious";
            this.optContinueFromPrevious.Size = new System.Drawing.Size(138, 19);
            this.optContinueFromPrevious.TabIndex = 4;
            this.optContinueFromPrevious.TabStop = true;
            this.optContinueFromPrevious.Text = "&Continue from Previous";
            this.optContinueFromPrevious.UseVisualStyleBackColor = true;
            // 
            // lstStartAtNumber
            // 
            this.lstStartAtNumber.Location = new System.Drawing.Point(123, 66);
            this.lstStartAtNumber.Name = "lstStartAtNumber";
            this.lstStartAtNumber.Size = new System.Drawing.Size(53, 22);
            this.lstStartAtNumber.TabIndex = 3;
            this.lstStartAtNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // gbApplyTo
            // 
            this.gbApplyTo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.gbApplyTo.Controls.Add(this.txtMultiple);
            this.gbApplyTo.Controls.Add(this.rbMultiple);
            this.gbApplyTo.Controls.Add(this.rbSection);
            this.gbApplyTo.Controls.Add(this.rbDocument);
            this.gbApplyTo.Location = new System.Drawing.Point(291, 10);
            this.gbApplyTo.Name = "gbApplyTo";
            this.gbApplyTo.Size = new System.Drawing.Size(249, 153);
            this.gbApplyTo.TabIndex = 13;
            this.gbApplyTo.TabStop = false;
            this.gbApplyTo.Text = "Apply To";
            // 
            // txtMultiple
            // 
            this.txtMultiple.Enabled = false;
            this.txtMultiple.Location = new System.Drawing.Point(32, 108);
            this.txtMultiple.Name = "txtMultiple";
            this.txtMultiple.Size = new System.Drawing.Size(188, 22);
            this.txtMultiple.TabIndex = 4;
            this.txtMultiple.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMultiple_KeyPress);
            // 
            // rbMultiple
            // 
            this.rbMultiple.AutoSize = true;
            this.rbMultiple.Location = new System.Drawing.Point(13, 83);
            this.rbMultiple.Name = "rbMultiple";
            this.rbMultiple.Size = new System.Drawing.Size(181, 19);
            this.rbMultiple.TabIndex = 3;
            this.rbMultiple.Text = "M&ultiple Sections (e.g., 1, 3-5, 7)";
            this.rbMultiple.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbMultiple.UseVisualStyleBackColor = true;
            this.rbMultiple.CheckedChanged += new System.EventHandler(this.rbMultiple_CheckedChanged);
            // 
            // rbSection
            // 
            this.rbSection.AutoSize = true;
            this.rbSection.Location = new System.Drawing.Point(13, 54);
            this.rbSection.Name = "rbSection";
            this.rbSection.Size = new System.Drawing.Size(101, 19);
            this.rbSection.TabIndex = 2;
            this.rbSection.Text = "Current S&ection";
            this.rbSection.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbSection.UseVisualStyleBackColor = true;
            // 
            // rbDocument
            // 
            this.rbDocument.AutoSize = true;
            this.rbDocument.Checked = true;
            this.rbDocument.Location = new System.Drawing.Point(13, 25);
            this.rbDocument.Name = "rbDocument";
            this.rbDocument.Size = new System.Drawing.Size(105, 19);
            this.rbDocument.TabIndex = 1;
            this.rbDocument.TabStop = true;
            this.rbDocument.Text = "Entire &Document";
            this.rbDocument.UseVisualStyleBackColor = true;
            // 
            // gbOptions
            // 
            this.gbOptions.Controls.Add(this.chkInsertOnFirstSectionPage);
            this.gbOptions.Controls.Add(this.optStartAtNumber);
            this.gbOptions.Controls.Add(this.optContinueFromPrevious);
            this.gbOptions.Controls.Add(this.lstStartAtNumber);
            this.gbOptions.Location = new System.Drawing.Point(289, 176);
            this.gbOptions.Name = "gbOptions";
            this.gbOptions.Size = new System.Drawing.Size(251, 162);
            this.gbOptions.TabIndex = 14;
            this.gbOptions.TabStop = false;
            this.gbOptions.Text = "Options";
            // 
            // lblIncludeTotalPages
            // 
            this.lblIncludeTotalPages.AutoSize = true;
            this.lblIncludeTotalPages.BackColor = System.Drawing.Color.Transparent;
            this.lblIncludeTotalPages.ForeColor = System.Drawing.Color.Black;
            this.lblIncludeTotalPages.Location = new System.Drawing.Point(15, 65);
            this.lblIncludeTotalPages.Name = "lblIncludeTotalPages";
            this.lblIncludeTotalPages.Size = new System.Drawing.Size(196, 15);
            this.lblIncludeTotalPages.TabIndex = 3;
            this.lblIncludeTotalPages.Text = "&Include Total Number of Pages (X of Y)";
            // 
            // cmbIncludeTotalPages
            // 
            this.cmbIncludeTotalPages.AllowEmptyValue = false;
            this.cmbIncludeTotalPages.AutoSize = true;
            this.cmbIncludeTotalPages.Borderless = false;
            this.cmbIncludeTotalPages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbIncludeTotalPages.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIncludeTotalPages.IsDirty = false;
            this.cmbIncludeTotalPages.LimitToList = true;
            this.cmbIncludeTotalPages.ListName = "";
            this.cmbIncludeTotalPages.Location = new System.Drawing.Point(15, 82);
            this.cmbIncludeTotalPages.MaxDropDownItems = 8;
            this.cmbIncludeTotalPages.Name = "cmbIncludeTotalPages";
            this.cmbIncludeTotalPages.SelectedIndex = -1;
            this.cmbIncludeTotalPages.SelectedValue = null;
            this.cmbIncludeTotalPages.SelectionLength = 0;
            this.cmbIncludeTotalPages.SelectionStart = 0;
            this.cmbIncludeTotalPages.Size = new System.Drawing.Size(254, 26);
            this.cmbIncludeTotalPages.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbIncludeTotalPages.SupportingValues = "";
            this.cmbIncludeTotalPages.TabIndex = 4;
            this.cmbIncludeTotalPages.Tag2 = null;
            this.cmbIncludeTotalPages.Value = "";
            // 
            // txtTextBefore
            // 
            this.txtTextBefore.AcceptsReturn = true;
            this.txtTextBefore.Location = new System.Drawing.Point(16, 128);
            this.txtTextBefore.Multiline = true;
            this.txtTextBefore.Name = "txtTextBefore";
            this.txtTextBefore.Size = new System.Drawing.Size(253, 45);
            this.txtTextBefore.TabIndex = 6;
            // 
            // txtTextAfter
            // 
            this.txtTextAfter.AcceptsReturn = true;
            this.txtTextAfter.Location = new System.Drawing.Point(16, 196);
            this.txtTextAfter.Multiline = true;
            this.txtTextAfter.Name = "txtTextAfter";
            this.txtTextAfter.Size = new System.Drawing.Size(253, 45);
            this.txtTextAfter.TabIndex = 8;
            // 
            // PageNumberInsertForm
            // 
            this.AcceptButton = this.btnInsert;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(557, 392);
            this.Controls.Add(this.txtTextAfter);
            this.Controls.Add(this.txtTextBefore);
            this.Controls.Add(this.cmbIncludeTotalPages);
            this.Controls.Add(this.lblIncludeTotalPages);
            this.Controls.Add(this.gbOptions);
            this.Controls.Add(this.gbApplyTo);
            this.Controls.Add(this.cmbAlignment);
            this.Controls.Add(this.lblAlignment);
            this.Controls.Add(this.cmbLocation);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.cmbFormat);
            this.Controls.Add(this.lblFormat);
            this.Controls.Add(this.lblTextAfter);
            this.Controls.Add(this.lblTextBefore);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PageNumberInsertForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Page Number";
            ((System.ComponentModel.ISupportInitialize)(this.lstStartAtNumber)).EndInit();
            this.gbApplyTo.ResumeLayout(false);
            this.gbApplyTo.PerformLayout();
            this.gbOptions.ResumeLayout(false);
            this.gbOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LMP.Controls.ComboBox cmbFormat;
        private System.Windows.Forms.CheckBox chkInsertOnFirstSectionPage;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblFormat;
        private LMP.Controls.ComboBox cmbLocation;
        private System.Windows.Forms.Label lblLocation;
        private LMP.Controls.ComboBox cmbAlignment;
        private System.Windows.Forms.Label lblAlignment;
        private System.Windows.Forms.Label lblTextBefore;
        private System.Windows.Forms.Label lblTextAfter;
        private System.Windows.Forms.RadioButton optStartAtNumber;
        private System.Windows.Forms.RadioButton optContinueFromPrevious;
        private System.Windows.Forms.NumericUpDown lstStartAtNumber;
        private System.Windows.Forms.GroupBox gbApplyTo;
        private System.Windows.Forms.TextBox txtMultiple;
        private System.Windows.Forms.RadioButton rbMultiple;
        private System.Windows.Forms.RadioButton rbSection;
        private System.Windows.Forms.RadioButton rbDocument;
        private System.Windows.Forms.GroupBox gbOptions;
        private System.Windows.Forms.Label lblIncludeTotalPages;
        private Controls.ComboBox cmbIncludeTotalPages;
        private System.Windows.Forms.TextBox txtTextBefore;
        private System.Windows.Forms.TextBox txtTextAfter;
    }
}