﻿namespace LMP.MacPac
{
    partial class UpdateDocumentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkContacts = new System.Windows.Forms.CheckBox();
            this.chkAuthor = new System.Windows.Forms.CheckBox();
            this.chkLetterhead = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpUpdateItems = new System.Windows.Forms.GroupBox();
            this.grpUpdateItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkContacts
            // 
            this.chkContacts.AutoSize = true;
            this.chkContacts.Location = new System.Drawing.Point(11, 24);
            this.chkContacts.Name = "chkContacts";
            this.chkContacts.Size = new System.Drawing.Size(82, 17);
            this.chkContacts.TabIndex = 0;
            this.chkContacts.Text = "#Contacts#";
            this.chkContacts.UseVisualStyleBackColor = true;
            this.chkContacts.CheckedChanged += new System.EventHandler(this.chkContacts_CheckedChanged);
            // 
            // chkAuthor
            // 
            this.chkAuthor.AutoSize = true;
            this.chkAuthor.Location = new System.Drawing.Point(11, 52);
            this.chkAuthor.Name = "chkAuthor";
            this.chkAuthor.Size = new System.Drawing.Size(71, 17);
            this.chkAuthor.TabIndex = 1;
            this.chkAuthor.Text = "#Author#";
            this.chkAuthor.UseVisualStyleBackColor = true;
            this.chkAuthor.CheckedChanged += new System.EventHandler(this.chkAuthor_CheckedChanged);
            // 
            // chkLetterhead
            // 
            this.chkLetterhead.AutoSize = true;
            this.chkLetterhead.Location = new System.Drawing.Point(11, 82);
            this.chkLetterhead.Name = "chkLetterhead";
            this.chkLetterhead.Size = new System.Drawing.Size(91, 17);
            this.chkLetterhead.TabIndex = 2;
            this.chkLetterhead.Text = "#Letterhead#";
            this.chkLetterhead.UseVisualStyleBackColor = true;
            this.chkLetterhead.CheckedChanged += new System.EventHandler(this.chkLetterhead_CheckedChanged);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(39, 136);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(81, 25);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "#OK#";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(126, 136);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 25);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "#Cancel#";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // grpUpdateItems
            // 
            this.grpUpdateItems.Controls.Add(this.chkContacts);
            this.grpUpdateItems.Controls.Add(this.chkAuthor);
            this.grpUpdateItems.Controls.Add(this.chkLetterhead);
            this.grpUpdateItems.Location = new System.Drawing.Point(12, 12);
            this.grpUpdateItems.Name = "grpUpdateItems";
            this.grpUpdateItems.Size = new System.Drawing.Size(195, 112);
            this.grpUpdateItems.TabIndex = 5;
            this.grpUpdateItems.TabStop = false;
            this.grpUpdateItems.Text = "#Select Items To Update#";
            // 
            // UpdateDocumentForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(220, 173);
            this.Controls.Add(this.grpUpdateItems);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateDocumentForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "#Update Document#";
            this.Load += new System.EventHandler(this.UpdateDocumentForm_Load);
            this.grpUpdateItems.ResumeLayout(false);
            this.grpUpdateItems.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkContacts;
        private System.Windows.Forms.CheckBox chkAuthor;
        private System.Windows.Forms.CheckBox chkLetterhead;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox grpUpdateItems;
    }
}