using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using System.Collections;
using Word = Microsoft.Office.Interop.Word;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class OfficeAddressForm : Form
    {
        DataSet m_dsOffices;
        DataSet m_dsAddressFormats;
        Offices m_oOffices;
        AddressFormats m_oAddressFormats;

        // GLOG : 3116 : JAB
        // Introduce a table of separators for multiple address entries.
        DataTable m_dtSeparators = null;

        public OfficeAddressForm()
        {
            InitializeComponent();
            InitializeApplicationContent();

            //GLOG #8492
            this.lstOffice.Height = (this.btnInsert.Top + this.btnInsert.Height) - this.lstOffice.Top;
        }

        private void InitializeApplicationContent()
        {
            // Show all the offices.
            this.lstOffice.Items.Clear();
            m_oOffices = new Offices();
            m_dsOffices = m_oOffices.ToDataSet();
            this.lstOffice.DataSource = m_dsOffices.Tables[0];
            this.lstOffice.DisplayMember = "DisplayName";
            int selectedIndex = -1;

            // Select the default office location.
            //GLOG:  7231 :  jsw
            if (LMP.MacPac.MacPacImplementation.IsToolkit)
            {
                try
                {
                    int iOfficeId = Convert.ToInt32(Session.CurrentUser.GetPreferenceSet(LMP.Data.mpPreferenceSetTypes.UserApp, "").GetValue("OfficeID"));
                    Office oOffice = (Office)this.m_oOffices.ItemFromID(iOfficeId);
                    selectedIndex = this.lstOffice.FindString(oOffice.DisplayName);
                }
                catch{}
            }
            else
            {
                Office oUserDefaultOffice = Session.CurrentUser.Office;
                selectedIndex = this.lstOffice.FindString(oUserDefaultOffice.DisplayName);

            }
            
            // GLOG : 3116 : JAB 
            // Since we will allow multiple selections, deselect anything currently
            // selected.
            this.lstOffice.SelectedIndices.Clear();
            this.lstOffice.SelectedIndex = selectedIndex < 0 ? 0 : selectedIndex;

            // Show the styles
            this.cmbStyle.Items.Clear();
            m_oAddressFormats = new AddressFormats();
            m_dsAddressFormats = m_oAddressFormats.ToDataSet();

            // Show only the styles that are intended to be shown by
            // removing the address formats that are not to be shown.

            //GLOG : 8436 : JSW
            //have to use an ArrayList now instead of DataSource,
            //since the control is no longer a Windows combobox
            // Get the address formats that are not to be shown.
            //DataRow [] aoAddressFormatsToBeExcluded = m_dsAddressFormats
            //    .Tables[0].Select("ShowInAddressMacro = false");

            ArrayList aAddressList = m_oAddressFormats.ToArrayList();
            ArrayList aAddressListGoodFormats = new ArrayList(); 

            foreach (object[] oItem in aAddressList)
            {

                if (oItem[2].ToString() == "True")
                    aAddressListGoodFormats.Add(oItem);
            }

            this.cmbStyle.SetList(aAddressListGoodFormats);

            // Select the first item in the list of available styles.
            if (this.cmbStyle.Items.Count > 0)
            {
                this.cmbStyle.SelectedIndex = 0;
            }

            // GLOG : 3116 : JAB
            // Set the data source for the separators drop down.
            this.cmbSeparators.DataSource = SeparatorsTable;
            this.cmbSeparators.DisplayMember = "Name";
            this.cmbSeparators.ValueMember = "Value";

            // Disable the ability to insert the address if there are either no 
            // offices or styles.
            if (this.lstOffice.Items.Count == 0 || this.cmbStyle.Items.Count == 0)
            {
                this.btnInsert.Enabled = false;
            }

            // Localize the form.
            LocalizeForm();
        }

        // GLOG : 3116 : JAB
        // The are the fixed separator values.
        string[,] m_aSeparators = new string[,] {   {"Space", " "},
                                                    {"Paragraph", ((char)13).ToString()},
                                                    {"Double Paragraph", "\r\n\r\n"},
                                                    {"Soft Return", ((char)11).ToString()},
                                                    {"Tab", "\t" },
                                                    {"Comma + Space", ", "}};

        // GLOG : 3116 : JAB
        // Create the data source for the separators drop down. We are using 
        // static values now but if in the future this table is set by the 
        // administrator, we'd simple read in the table in this property.
        private DataTable SeparatorsTable
        {
            get
            {
                if (m_dtSeparators == null)
                {
                    m_dtSeparators = new DataTable();

                    DataColumn oNameCol = new DataColumn("Name", typeof(string));
                    m_dtSeparators.Columns.Add(oNameCol);

                    DataColumn oValueCol = new DataColumn("Value", typeof(string));
                    m_dtSeparators.Columns.Add(oValueCol);

                    // Populate the table with the values.
                    for (int i = 0; i < this.m_aSeparators.GetLength(0); i++)
                    {
                        this.m_dtSeparators.Rows.Add(this.m_aSeparators[i, 0], this.m_aSeparators[i, 1]);
                    }
                }

                return m_dtSeparators;
            }
        }

        private void LocalizeForm()
        {
            this.lblOffice.Text = LMP.Resources.GetLangString("OfficeAddressFormOffice");
            this.lblStyle.Text = LMP.Resources.GetLangString("OfficeAddressFormStyle");
            this.chkIncludeFirmName.Text = LMP.Resources.GetLangString("OfficeAddressFormIncludeFirmName");
            this.btnInsert.Text = LMP.Resources.GetLangString("OfficeAddressFormInsert");
            this.btnCancel.Text = LMP.Resources.GetLangString("OfficeAddressFormCancel");
            this.Text = LMP.Resources.GetLangString("OfficeAddressFormInsertOfficeAddress");

            // GLOG : 3116 : JAB
            // Multiple office address insertions require a separator and are listed in 
            // the separators list box. Localize the label.
            lblSeparators.Text = LMP.Resources.GetLangString("OfficeAddressFormSeparators");
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                // GLOG : 3116 : JAB
                // Handle multiple selection of offices.
                Word.Selection oSel = null;
                for (int i = 0; i < this.lstOffice.SelectedItems.Count; i++)
                {
                    int iSelectedIndex = (int)this.lstOffice.SelectedIndices[i];

                    int iOfficeId = (int)this.m_dsOffices.Tables[0].Rows[iSelectedIndex]["ID"];
                    Office oOffice = (Office)this.m_oOffices.ItemFromID(iOfficeId);

                    //GLOG:  7231 :  jsw
                    if (LMP.MacPac.MacPacImplementation.IsToolkit)
                    {
                        try
                        {
                            KeySet oPrefs = new KeySet(mpPreferenceSetTypes.UserApp, "", Session.CurrentUser.ID.ToString());
                            oPrefs.SetValue("OfficeID", iOfficeId.ToString());
                            oPrefs.Save();
                        }
                        catch { }
                    }

                    //GLOG 8515: Control list is only a subset of m_oAddressFormats - get ID from SelectedValue 
                    int iFormatId = Int32.Parse(this.cmbStyle.SelectedValue.ToString()); // this.m_dsAddressFormats.Tables[0].Rows[this.cmbStyle.SelectedIndex]["ID"];
                    AddressFormat oAddressFormat = (AddressFormat)this.m_oAddressFormats.ItemFromID(iFormatId);

                    //get delimiter character
                    string xDelimiterReplacement = Expression.Evaluate(
                        string.IsNullOrEmpty(oAddressFormat.DelimiterReplacement) ?
                        "[Char_11]" : oAddressFormat.DelimiterReplacement, null, null);

                    // Include the firm name per user's choice.
                    string xFirmName = "";
                    if (this.chkIncludeFirmName.Checked)
                    {
                        //JTS 11/16/11:  Honor Firm Name format of Address Format
                        if (oAddressFormat.FirmNameFormat == mpFirmNameFormats.UpperCase)
                            xFirmName = oOffice.FirmNameUpperCase + xDelimiterReplacement;
                        else
                            xFirmName = oOffice.FirmName + xDelimiterReplacement;

                        //GLOG 5742: Include Slogan if configured in Address Format
                        if (oAddressFormat.IncludeSlogan && oOffice.Slogan != "")
                        {
                            xFirmName = xFirmName + oOffice.Slogan + xDelimiterReplacement;
                        }
                        //GLOG 5743: Evaluate any Character codes in Firm Name or Slogan
                        xFirmName = Expression.EvaluateCharacterCodes(xFirmName, true);
                    }

                    // Obtain the office address entry in the appropriate format.
                    string xOfficeAddressEntry = oOffice.GetAddress().ToString(
                        oAddressFormat.AddressTokenString, xDelimiterReplacement);

                    // GLOG : 3116 : JAB
                    // Add the separator when multiple addresses are inserted.
                    string xSeparator = "";

                    if (i > 0)
                    {
                        xSeparator = (string)this.cmbSeparators.SelectedValue;
                    }
                    // Insert the office address entry.
                    oSel = LMP.MacPac.Session.CurrentWordApp.Selection;
                    oSel.InsertAfter(xSeparator + xFirmName + xOfficeAddressEntry);

                    //execute any embedded commands
                    LMP.Forte.MSWord.WordDoc.ExecuteEmbeddedCommands(oSel.Range);

                    // Deselect the entered office address
                    object oDir = Word.WdCollapseDirection.wdCollapseEnd;
                    oSel.Collapse(ref oDir);
                }

                if (oSel != null)
                {
                    oSel.Application.Activate();
                    oSel.Document.Activate();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            
            // Close the dialog
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                // Do nothing and close the dialog
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstOffice_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnInsert_Click(sender, e);
        }

        // GLOG : 3116 : JAB
        // Enable the separators combo box when more than 1 item is selected.
        private void lstOffice_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                this.cmbSeparators.Enabled = (this.lstOffice.SelectedItems.Count > 1);
            }
            catch(System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}