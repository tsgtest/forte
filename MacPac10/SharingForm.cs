using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Controls;
using Infragistics.Win.UltraWinTree;
// GLOG : 2797 : JAB
// We need a ListDictionary from this reference.
using System.Collections.Specialized;

namespace LMP.MacPac
{
    public partial class SharingForm : Form
    {
        mpObjectTypes m_iObjectType = 0;
        int m_iObjectID1 = 0;
        int m_iObjectID2 = 0;
        
        // GLOG : 2797 : JAB
        // Keep track of the original sharing permission to the segment.
        ListDictionary m_oOriginalPermissions = new ListDictionary();

        public SharingForm(mpObjectTypes iObjectType, int iObjectID1, int iObjectiD2)
        {
            InitializeComponent();
            m_iObjectType = iObjectType;
            m_iObjectID1 = iObjectID1;
            m_iObjectID2 = iObjectiD2;
            this.Text = LMP.Resources.GetLangString("Dialog_Sharing_Title");
            this.btnCancel.Text = LMP.Resources.GetLangString("Lbl_Cancel");
            this.btnOK.Text = LMP.Resources.GetLangString("Lbl_OK");
            this.chkOwnerFolder.Text = LMP.Resources.GetLangString("Dialog_Sharing_OwnerFolder");
            this.chkSharedFolder.Text = LMP.Resources.GetLangString("Dialog_Sharing_SelectedFolder");
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkSharedFolder.Checked)
                {
                    if (this.folderTreeView1.FolderList.Count == 0)
                    {
                        //No Folder selected, prompt and return to dialog
                        DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString("Prompt_Sharing_NoFoldersSelected"), 
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, 
                            MessageBoxIcon.Exclamation);
                        return;
                    }
                    SharedFolder.AssignToSharedFolder((int)((ArrayList)folderTreeView1.FolderList[0])[0], m_iObjectID1, m_iObjectID2, m_iObjectType);
                }
                else if (SharedFolder.GetSharedFolderID(m_iObjectID1, m_iObjectID2, m_iObjectType) > 0)
                {
                    //Reset existing Shared Folder ID to 0
                    SharedFolder.RemoveSharedFolderAssignment(m_iObjectID1, m_iObjectID2, m_iObjectType);
                }

                if (chkOwnerFolder.Checked)
                {
                    string xList = lstPeople.Value;
                    if (xList == "")
                    {
                        //No Users checked, prompt and return to dialog
                        DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString("Prompt_Sharing_NoUsersSelected"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    if (xList != "")
                    {
                        string[] aList = xList.Split('|');
                        foreach (string xID in aList)
                        {
                            // GLOG : 2797 : JAB
                            if (m_oOriginalPermissions.Contains(xID))
                            {
                                bool bHasSharingPermissions = (bool)m_oOriginalPermissions[xID];

                                // This user has sharing permission. If the user did not have sharing
                                // permission originally, grant the user sharing permission.
                                if (!bHasSharingPermissions)
                                {
                                    UserPermissions.GrantPermission(m_iObjectType, m_iObjectID1, m_iObjectID2, Int32.Parse(xID));
                                }
                                // Remove users that have sharing permission so that the collection is
                                // left only with users that don't have sharing permission.
                                m_oOriginalPermissions.Remove(xID);
                            }
                        }
                    }

                    // GLOG : 2797 : JAB
                    // The collection only has users that don't have sharing permission.
                    // If the user originally had sharing permission revoke the permission.
                    foreach (string xID in this.m_oOriginalPermissions.Keys)
                    {
                        bool bHasSharingPermissions = (bool)m_oOriginalPermissions[xID];

                        if (bHasSharingPermissions)
                        {
                            UserPermissions.RevokePermission(m_iObjectType, m_iObjectID1, m_iObjectID2, Int32.Parse(xID));
                        }
                    }
                }
                else
                {
                    if (UserPermissions.GetPermittedUsers(m_iObjectType, m_iObjectID1, m_iObjectID2).Count > 0)
                    {
                        //Clear all Permissions if owner folder option not checked
                        UserPermissions.ClearPermissions(m_iObjectType, m_iObjectID1, m_iObjectID2);
                    }
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void SharingForm_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                try
                {
                    this.folderTreeView1.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders;
                    this.folderTreeView1.OwnerID = Session.CurrentUser.ID;
                    this.folderTreeView1.ExecuteFinalSetup();
                    this.lstPeople.MultipleSelection = true;
                    this.lstPeople.FilterOption = PeopleListBox.mpPeopleFilterOptions.Users;

                    if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_CantShareWithNetworkUserList"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);

                        this.lstPeople.ListSource = PeopleListBox.mpPeopleListSources.Local;
                    }
                    else
                    {
                        int iPort;
                        string xHost;
                        LMP.Data.NetworkConnection oNetCnn = new NetworkConnection();
                        oNetCnn.ConnectionObject.ConnectionString = oNetCnn.ConnectionString;

                        LMP.Network.GetConnectionParameters(oNetCnn.ConnectionObject, out xHost, out iPort);

                        // GLOG 2728 : JAB : Specify a 3 second timeout to connect.
                        if (LMP.Network.IsServerConnectionAvailable(xHost, iPort, 3000))
                            this.lstPeople.ListSource = PeopleListBox.mpPeopleListSources.Network;
                        else
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_CantShareWithNetworkUserList"),
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            this.lstPeople.ListSource = PeopleListBox.mpPeopleListSources.Local;
                        }
                    }

                    // GLOG : 2797 : JAB
                    //Get List of all users ids.
                    string xAllUserIds = lstPeople.AllPeopleIDs;
                    string [] axAllUserIds = xAllUserIds.Split('|');

                    // GLOG : 2797 : JAB
                    // Initialize the collection of original sharing permissions. We will
                    // update the collection to reflect users that do have sharing 
                    // permission below. For now we will add all users and specify
                    // that they don't have permission to share this segment.
                    for (int i = 0; i < axAllUserIds.Length; i++)
                    {
                        m_oOriginalPermissions.Add(axAllUserIds[i], false);
                    }

                    //Get List of Users having permission to this Content
                    ArrayList oUsers = UserPermissions.GetPermittedUsers(m_iObjectType, m_iObjectID1, m_iObjectID2);
                    string xTemp = "";
                    for (int i = 0; i < oUsers.Count; i++)
                    {
                        // GLOG : 2797 : JAB
                        // Update the collection with each user that has permission to share this segment.
                        string xUserId = ((object[])oUsers[i]).GetValue(0).ToString();

                        if (m_oOriginalPermissions.Contains(xUserId))
                        {
                            // Only modify the permission for users that are in the original permissions
                            // collection.
                            // The users shown in the people control can be a subset of those returned
                            // by the UserPermissions.GetPermittedUsers method. This occurs when there
                            // is no connection to the network db.
                            m_oOriginalPermissions.Remove(xUserId);
                            m_oOriginalPermissions.Add(xUserId, true);
                        }

                        xTemp = xTemp + ((object[])oUsers[i]).GetValue(0).ToString();
                        if (i < oUsers.Count - 1)
                            xTemp = xTemp + "|";
                    }
                    if (xTemp != "")
                    {
                        lstPeople.Value = xTemp;
                        this.chkOwnerFolder.Checked = true;
                    }
                    else
                        this.lstPeople.Enabled = false;
                    //Check if Content has a Shared Folder ID
                    int iSharedFolderID = SharedFolder.GetSharedFolderID(m_iObjectID1, m_iObjectID2, m_iObjectType);
                    if (iSharedFolderID > 0)
                    {
                        this.chkSharedFolder.Checked = true;
                        try
                        {
                            UltraTreeNode oNode = this.folderTreeView1.GetNodeByKey(iSharedFolderID.ToString());
                            oNode.Selected = true;
                            this.folderTreeView1.ActiveNode = oNode;
                        }
                        catch
                        {
                            //Specified shared folder doesn't exist
                            this.chkSharedFolder.Checked = false;
                        }
                    }
                    else
                        this.folderTreeView1.Enabled = false;

                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }

        }

        private void chkSharedFolder_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                folderTreeView1.Enabled = chkSharedFolder.Checked;
                if (chkSharedFolder.Checked)
                {
                    folderTreeView1.Focus();
                }
                //GLOG 8309: Allow both types of sharing at same time
                //if (chkSharedFolder.Checked)
                //    chkOwnerFolder.Checked = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void chkOwnerFolder_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                lstPeople.Enabled = chkOwnerFolder.Checked;
                if (chkOwnerFolder.Checked)
                {
                    lstPeople.Focus();
                }
                //GLOG 8309: Allow both types of sharing at same time
                //if (chkOwnerFolder.Checked)
                //    chkSharedFolder.Checked = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
    }
}