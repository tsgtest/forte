using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace LMP.MacPac
{
    public partial class CalloutForm : Form
    {
        public CalloutForm()
        {
            InitializeComponent();
        }
        public override string Text
        {
            get { return lblText != null ? lblText.Text : ""; }
            set
            {
                if (lblText != null)
                    lblText.Text = value;
            }
        }
        private void lblText_Resize(object sender, EventArgs e)
        {
            try
            {
                this.Width = this.lblText.Width + 8;
                this.Height = this.lblText.Height + 8;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void CalloutForm_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                //draw arrow that points to editing target
                Rectangle oR = this.lblText.ClientRectangle;
                Rectangle oCalloutRect = this.ClientRectangle;

                int iTextBottom = this.ClientRectangle.Bottom - 4;
                int iArrowTipY = oCalloutRect.Bottom - 4;
                int iArrowTipX = oCalloutRect.Right;
                int iTextMidpt = (oR.Left + oR.Right) / 2;
                //int iArrowThickness = 2;
                Brush oBrush = Brushes.SlateBlue;

                ////draw arrow vertical line
                //e.Graphics.DrawLine(new Pen(oBrush, iArrowThickness),
                //    new Point(iTextMidpt, oR.Top + oR.Height),
                //    new Point(iTextMidpt, iTextBottom));

                ////draw arrow horizontal line
                //e.Graphics.DrawLine(new Pen(oBrush, iArrowThickness),
                //    new Point(iArrowTipX - 8, iArrowTipY),
                //    new Point(iTextMidpt, iArrowTipY));

                ////draw reverse arrow top
                //e.Graphics.DrawLine(new Pen(oBrush, iArrowThickness),
                //    new Point(iArrowTipX + 8, iArrowTipY + 4),
                //    new Point(iArrowTipX, iArrowTipY));

                ////draw reverse arrow bottom
                //e.Graphics.DrawLine(new Pen(oBrush, iArrowThickness),
                //    new Point(iArrowTipX + 8, iArrowTipY - 4),
                //    new Point(iArrowTipX, iArrowTipY));

                ////draw arrow tip
                //e.Graphics.FillPolygon(oBrush,
                //    new Point[]{new Point(iArrowTipX, iArrowTipY),
                //new Point(iArrowTipX - 8, iArrowTipY + 4),
                //new Point(iArrowTipX - 8, iArrowTipY - 4)});

                //draw arrow tip
                e.Graphics.FillPolygon(Brushes.Red,
                    new Point[]{new Point(oCalloutRect.Right, oCalloutRect.Bottom),
                new Point(oCalloutRect.Right - 8, oCalloutRect.Bottom ),
                new Point(oCalloutRect.Right, oCalloutRect.Bottom - 8)});

                //draw text border
                oR = this.lblText.ClientRectangle;
                oR.Inflate(2, 2);
                oR.Offset(-1, -1);
                e.Graphics.DrawRectangle(new Pen(Brushes.Silver), oR);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}