using System;
using System.Drawing;
using System.Collections;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;
using System.Text;
using Word=Microsoft.Office.Interop.Word;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using LMP.Data;
using LMP.Architect.Api;
using LMP.Architect.Oxml;
using LMP.MacPac.Sync;
using System.Xml;
using LMP.Controls;
using LMP.Grail;
using MSWord = Microsoft.Office.Interop.Word;
using Infragistics.Win.UltraWinTree;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.MacPac
{
    /// <summary>
    ///  class interface LMP.MacPac.Application -
    ///  allows COM applications, specifically
    ///  Word template VBA to act on the MacPac10 application
    /// </summary>
    [ComVisible(true)]
    [Guid("DBE28ECA-3B55-43b4-BD29-840F3D9E9045")]
    public interface IMP10WordCommands
    {
        void EditCopy(Word.Document oDocument);
        void EditPaste(Word.Document oDocument);
        void EditCut(Word.Document oDocument);
        void EditUndo(Word.Document oDocument);
        void EditRedo(Word.Document oDocument);
        void NextCell(Word.Document oDocument);
        void ToggleFormsCheckbox();
        void ToggleFormsCheckboxSet();
        void InsertSymbol(int ASCII, string FontName, bool Unicode);
        void ExecuteCommand(string CommandID);
        void SetXMLEventHandling(Word.Document oDocument, bool bOn);
        void ChangeCase();
        void InsertPleadingPaper(Word.Document oDocument);
        void RefreshTaskPane(Word.Document oDocument);
    }

    public delegate void OnDemandSyncHandler();
    public delegate LMP.MacPac.TaskPane ShowTaskPaneDelegate(Word.Document oDoc);

    /// <summary>
	/// LMP.MacPac.Application class-
	/// defines the top level class for MacPac 10 -
	/// </summary>
    [ComVisible(true)]
    [Guid("364CAAEC-1B64-4ba4-BD97-6DC34CC9BE0D"),
    ClassInterface(ClassInterfaceType.None)]
    public class Application : IMP10WordCommands
	{
        #region *********************constants*********************
        //public const int mpBeforePaste = 0x1731;
        //public const int mpAfterPaste = 0x1732;
        //public const int mpBeforeUndo = 0x1733;
        //public const int mpAfterUndo = 0x1734;
        //public const int mpBeforeRedo = 0x1735;
        //public const int mpAfterRedo = 0x1736;
        //public const int mpMouseDragStart = 0x1737;
        //public const int mpMouseDragComplete = 0x1738;
        //public const int mpFloatingFormRequested = 0x1739;
        #endregion
        #region *********************enumerations*********************
        public enum mpPseudoWordEvents
        {
            BeforeMenuPaste = 0x1731,
            AfterMenuPaste = 0x1732,
            BeforeUndo = 0x1733,
            AfterUndo = 0x1734,
            BeforeRedo = 0x1735,
            AfterRedo = 0x1736,
            MouseDragStart = 0x1737,
            MouseDragComplete = 0x1738,
            FloatingFormRequested = 0x1739,
            BeforeNextCell = 0x173A,
            AfterNextCell = 0x173B,
            ContentChanged = 0x173C,
            AuthorListUpdated = 0x173D,
            SuppressXMLEvents = 0x173E,
            ResumeXMLEvents = 0x173F,
            QueryExists = 0x1740,
            BeforeKeyboardPaste = 0x1741,
            SuppressXMLSelectionChangeEvent = 0x1742,
            PleadingPaperRequested = 0x1743,
            OnCopy = 0x1744,
            OnCut = 0x1745,
            UpdateVariableValueFromSelection = 0x1746,
            DeleteKeyPressed = 0x1747,
            BackspaceKeyPressed = 0x1748,
            RefreshRequested = 0x1749,
            QueryInputFocus = 0x174A,
            EnvelopesRequested = 0x174B,
            QueryUnfinishedContent = 0x174C
        }
        #endregion
        #region *********************fields**********************
        static List<string> m_lTemplateHistory;
        private const int m_iMaxTemplateHistory = 9;
        private static Microsoft.Office.Core.IRibbonUI m_oRibbon;
        private static DMSIntegrator m_oDMS;
        private bool m_bIsTaskPaneShowing;
        private static System.Diagnostics.Process m_oAdministratorProcess;
        private const int SW_SHOWNORMAL = 1;
        private static Timer m_oOpenTimer;
        private static bool m_bShowTaskpaneAfterOpen = false; //GLOG 3025
        private static bool m_bDisableTaskpaneEvents;
        private static bool m_bOnDemandSyncExecuting = false;
        private static bool m_bPaused = false;
        private static string m_xRibbonTabControlId = "";
        private static TaskPane m_oActiveTaskPane = null; //GLOG 7814
        private static bool m_bLicenseIsValid = false; //GLOG 8318

        // GLOG : 2386 : JAB
        // Use a boolean variable to control access to the handler of the Administrator startup.
        static bool bAdministratorStartComplete = true;

		#endregion
		#region *********************constructors*********************
        static Application()
        {
            m_oDMS = new LMP.MacPac.DMSIntegrator();
            m_oOpenTimer = new Timer();
            m_oOpenTimer.Interval = 250;
            m_oOpenTimer.Tick += new EventHandler(m_oOpenTimer_Tick);
            m_oOpenTimer.Stop();
        }
		public Application(){}
		#endregion
		#region *********************methods*********************
        /// <summary>
        /// creates a new, blank MacPac 10 document
        /// </summary>
        public static void CreateNewDocument()
        {
            LMP.Forte.MSWord.Application.NewForteDocument();

            LMP.MacPac.Application.SetXMLTagStateForEditing();

            //this prevents freezing of the screen
            //when no other document is open
            LMP.WinAPI.LockWindowUpdate(0);
        }
        public static void CreateNewWordDocument()
        {
            object oMissing  = System.Reflection.Missing.Value;
            // GLOG : 6059 : CEH
            // create new Word document based on 
            //Firm Application Settings' BlankWordDocTemplate
            string xWordUser = LMP.Data.Application.GetDirectory(mpDirectories.WordUser);
            int iUserID = LMP.MacPac.Session.CurrentUser.ID;
            string xTemplate = null;

            FirmApplicationSettings oSettings = new FirmApplicationSettings(iUserID);
            xTemplate = oSettings.BlankWordDocTemplate;

            //check for full path
            xTemplate = xTemplate.Contains("\\") ? xTemplate : xWordUser + "\\" + xTemplate;

            //check for validity
            if (!File.Exists(xTemplate))
            {
                throw new LMP.Exceptions.FileException(
                    LMP.Resources.GetLangString("Error_FileDoesNotExist") + xTemplate);
            }
            else
            {
                object oTemplate = (object)xTemplate;
                Session.CurrentWordApp.Documents.Add(ref oTemplate, ref oMissing, ref oMissing, ref oMissing);
            }
        }
        public static void CreateMasterData()
        {
            try
            {
                SegmentInsertionForm oForm = new SegmentInsertionForm(mpObjectTypes.MasterData, false);

                DialogResult iRes = oForm.ShowDialog();

                if (iRes == DialogResult.OK)
                {
                    InsertSegment(oForm.SegmentDef, Segment.InsertionLocations.InsertInNewDocument,
                        Segment.InsertionBehaviors.InsertInSeparateNextPageSection);
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public static void ForceProfileIfNecessary()
        {
            int iUserID = LMP.MacPac.Session.CurrentUser.ID;
            FirmApplicationSettings oSettings = new FirmApplicationSettings(iUserID);
            if (oSettings.DMSFrontEndProfiling)
            {
                m_oDMS.ProfileDocument();
                LMP.Trace.WriteNameValuePairs("DocNumber", m_oDMS.DocumentManager.DocNumber + "",
                    "Path", m_oDMS.DocumentManager.Path + "");
                //GLOG 5135: DocNumber may be null if Profile was cancelled
                if (string.IsNullOrEmpty(m_oDMS.DocumentManager.DocNumber) &&
                    string.IsNullOrEmpty(Session.CurrentWordApp.ActiveDocument.Path))
                {
                    LMP.Trace.WriteInfo("Profile Cancelled");
                    //GLOG 5316: Profiling was cancelled - close document if appropriate based on setting
                    if (oSettings.DMSCloseOnFEPCancel)
                    {
                        object oFalse = false;
                        object oMissing = System.Reflection.Missing.Value;
                        Session.CurrentWordApp.ActiveDocument.Saved = true;
                        Session.CurrentWordApp.ActiveDocument.Close(ref oFalse, ref oMissing, ref oMissing);
                    }
                }
            }
        }
        //GLOG 8213
        public static void ShowOrphanedDocVarsInDesign()
        {
            OrphanedDocVarsForm oOrphanForm = new OrphanedDocVarsForm();
            oOrphanForm.ShowDialog();

        }
        public static void ListVBAExpressions()
        {
            //GLOG 15734
            ProgressForm oForm = null;
            DateTime t0 = DateTime.Now;
            try
            {
                StringBuilder oSB = new StringBuilder();
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                string xTPI = LMP.Data.Application.CurrentClientTagPrefixID;
                string xDB = LMP.Data.Application.AdminDBName;
                string xTimeStamp = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
                string xFileName = LMP.Data.Application.WritableDBDirectory + "\\" + xTPI + "_VBAExpressions_" + xTimeStamp + ".txt";
                string xSegmentErrors = "";
                DialogResult iAnswer = MessageBox.Show("Search for VBA Expressions in " + oDefs.Count.ToString() + " Segments?", LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (iAnswer != DialogResult.Yes)
                    return;
                
                File.AppendAllText(xFileName,  string.Format("<{0} tagPrefix='{1}' runTime='{2}'>\r\n", xDB, xTPI, xTimeStamp), Encoding.UTF8);
                oForm = new ProgressForm(oDefs.Count, 1, "", "", true);
                oForm.Text = "Find VBA Expressions in Forte Segments";
                oForm.Show();
                System.Windows.Forms.Application.DoEvents();

                for (int i = 1; i <= oDefs.Count; i++)
                {
                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromIndex(i);
                    if (oDef.TypeID == mpObjectTypes.SegmentPacket || oDef.TypeID == mpObjectTypes.MasterData || oDef.TypeID == mpObjectTypes.NonMacPacContent || oDef.TypeID == 0
                         || oDef.IsTableCollectionItemType || oDef.IsTableCollectionType || oDef.XML == "")
                        continue;
                    oForm.ProgressLabelText = "Examining " + oDef.DisplayName + "...";
                    oForm.StatusText = "(" + i.ToString() + " of " + oDefs.Count.ToString() + ")";
                    oForm.UpdateProgress(i);
                    oForm.Update();
                    try
                    {
                        LMP.Architect.Oxml.Word.XmlForteDocument oForteDoc = new LMP.Architect.Oxml.Word.XmlForteDocument(null);

                        LMP.Architect.Oxml.XmlSegment oSegment = LMP.Architect.Oxml.Word.XmlSegmentInsertion.Create(oDef.ID.ToString(), null, oForteDoc, null, "", null, 
                            null, XmlSegment.OxmlCreationMode.Design, true);

                        StringBuilder oSBSegment = new StringBuilder();
                        StringBuilder oSBActions = new StringBuilder();
                        //
                        for (int a = 0; a < oSegment.Actions.Count; a++)
                        {
                            LMP.Architect.Oxml.XmlSegmentAction oSegAction = oSegment.Actions[a];
                            StringBuilder oSBExp = new StringBuilder();
                            Architect.Oxml.XmlVariableActions.Types iType = oSegAction.Type;
                            Architect.Base.Segment.Events iEvent = oSegAction.Event;
                            string xParams = oSegAction.Parameters;
                            string[] aParams = xParams.Split(LMP.StringArray.mpEndOfSubField);
                            string xCommand = "";
                            switch (iType)
                            {
                                case Architect.Oxml.XmlVariableActions.Types.ExecuteOnBookmark:
                                case Architect.Oxml.XmlVariableActions.Types.ExecuteOnSegment:
                                case Architect.Oxml.XmlVariableActions.Types.ExecuteOnBlock:
                                    xCommand = aParams[0];
                                    break;
                                default:
                                    xCommand = aParams[aParams.GetUpperBound(0)];
                                    break;
                            }
                            if (xCommand != "")
                            {
                                xCommand = String.ReplaceXMLChars(xCommand, true);
                                switch (iType)
                                {
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnApplication:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnBlock:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnBookmark:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnDocument:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnPageSetup:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnSegment:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnStyle:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnTag:
                                        oSBExp.AppendFormat("   <SegmentAction category='{0}' type='{1}' index='{2}'>{3}</SegmentAction>\r\n", iEvent, iType, oSegAction.ExecutionIndex, xCommand);
                                        break;
                                    default:
                                        if (xCommand.IndexOf("[EXEC", StringComparison.CurrentCultureIgnoreCase) > -1
                                            || LMP.String.ContainsWordObjectFieldCode(xCommand))
                                        {
                                            oSBExp.AppendFormat("   <SegmentAction category='{0}' type='{1}' index='{2}'>{3}</SegmentAction>\r\n", iEvent, iType, oSegAction.ExecutionIndex, xCommand);
                                        }
                                        break;
                                }
                            }
                            if (oSBExp.ToString() != "")
                            {
                                oSBActions.Append(oSBExp);
                            }
                        }
                        StringBuilder oSBVars = new StringBuilder();
                        for (int v = 0; v < oSegment.Variables.Count; v++)
                        {
                            StringBuilder oSBExp = new StringBuilder();
                            LMP.Architect.Oxml.XmlVariable oVar = oSegment.Variables[v];
                            string xDefValue = oVar.DefaultValue;
                            if (!string.IsNullOrEmpty(xDefValue) && (xDefValue.IndexOf("[EXEC", StringComparison.CurrentCultureIgnoreCase) > -1
                                || LMP.String.ContainsWordObjectFieldCode(xDefValue)))
                            {
                                oSBExp.AppendFormat("      <DefaultValue>{0}</DefaultValue>\r\n", xDefValue);
                            }
                            string xSecValue = oVar.SecondaryDefaultValue;
                            if (!string.IsNullOrEmpty(xSecValue) && (xSecValue.IndexOf("[EXEC", StringComparison.CurrentCultureIgnoreCase) > -1
                                || LMP.String.ContainsWordObjectFieldCode(xSecValue)))
                            {
                                oSBExp.AppendFormat("      <SecondaryDefaultValue>{0}</SecondaryDefaultValue>\r\n", xSecValue);
                            }
                            string xSource = oVar.ValueSourceExpression;
                            if (!string.IsNullOrEmpty(xSource) && LMP.String.ContainsWordObjectFieldCode(xSource))
                            {
                                oSBExp.AppendFormat("      <ValueSourceExpression>{0}</ValueSourceExpression>\r\n", xSource);
                            }
                            for (int a = 0; a < oVar.VariableActions.Count; a++)
                            {
                                Architect.Oxml.XmlVariableAction oVarAction = oVar.VariableActions[a];
                                string xParams = oVarAction.Parameters;
                                Architect.Oxml.XmlVariableActions.Types iType = oVarAction.Type;
                                string[] aParams = xParams.Split(LMP.StringArray.mpEndOfSubField);
                                string xCommand = "";
                                switch (iType)
                                {
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnBookmark:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnSegment:
                                    case Architect.Oxml.XmlVariableActions.Types.ExecuteOnBlock:
                                        xCommand = aParams[0];
                                        break;
                                    default:
                                        xCommand = aParams[aParams.GetUpperBound(0)];
                                        break;
                                }

                                if (xCommand != "")
                                {
                                    xCommand = String.ReplaceXMLChars(xCommand, true);
                                    switch (iType)
                                    {
                                        case Architect.Oxml.XmlVariableActions.Types.ExecuteOnApplication:
                                        case Architect.Oxml.XmlVariableActions.Types.ExecuteOnBlock:
                                        case Architect.Oxml.XmlVariableActions.Types.ExecuteOnBookmark:
                                        case Architect.Oxml.XmlVariableActions.Types.ExecuteOnDocument:
                                        case Architect.Oxml.XmlVariableActions.Types.ExecuteOnPageSetup:
                                        case Architect.Oxml.XmlVariableActions.Types.ExecuteOnSegment:
                                        case Architect.Oxml.XmlVariableActions.Types.ExecuteOnStyle:
                                        case Architect.Oxml.XmlVariableActions.Types.ExecuteOnTag:
                                            if (xCommand != "")
                                            {
                                                oSBExp.AppendFormat("      <VariableAction type='{0}' index='{1}'>{2}</VariableAction>\r\n", iType, oVarAction.ExecutionIndex, xCommand);
                                            }
                                            break;
                                        default:
                                            if (xCommand.IndexOf("[EXEC", StringComparison.CurrentCultureIgnoreCase) > -1
                                                || LMP.String.ContainsWordObjectFieldCode(xCommand))
                                            {
                                                oSBExp.AppendFormat("      <VariableAction type='{0}' index='{1}'>{2}</VariableAction>\r\n", iType, oVarAction.ExecutionIndex, xCommand);
                                            }
                                            break;
                                    }
                                }
                            }
                            if (oSBExp.ToString() != "")
                            {
                                oSBVars.AppendFormat("   <Variable name='{0}' index='{1}'>\r\n", oVar.Name, oVar.ExecutionIndex);
                                oSBVars.Append(oSBExp);
                                oSBVars.Append("   </Variable>\r\n");
                            }
                        }
                        if (oSBVars.ToString() != "" || oSBActions.ToString() != "")
                        {
                            oSBSegment.AppendFormat("<Segment id='{0}' name='{1}' displayName='{2}' typeId='{3}' >\r\n", oSegment.ID, oSegment.Name, oSegment.DisplayName, oSegment.TypeID);
                            oSBSegment.Append(oSBActions);
                            oSBSegment.Append(oSBVars);
                            oSBSegment.Append("</Segment>\r\n");
                            File.AppendAllText(xFileName, oSBSegment.ToString(), Encoding.UTF8);
                        }
                    }
                    catch (System.Exception oE)
                    {
                        xSegmentErrors +=  string.Format("   <Segment id='{0}' displayname='{1}'>\r\n      <ErrorNumber>{2}</ErrorNumber>\r\n      " + 
                            "<ErrorMessage>{3}</ErrorMessage>\r\n   </Segment>\r\n",
                            oDef.ID, oDef.DisplayName, oE.HResult, oE.Message);
                        continue;
                    }
                    System.Windows.Forms.Application.DoEvents();
                    if (oForm.Cancelled)
                    {
                        oForm.Close();
                        oForm = null;
                        MessageBox.Show("Process cancelled.");
                        break;
                    }
                }
                if (xSegmentErrors != "")
                {
                    xSegmentErrors = "<SegmentErrors>\r\n" + xSegmentErrors + "</SegmentErrors>\r\n";
                    File.AppendAllText(xFileName, xSegmentErrors, Encoding.UTF8);
                }
                File.AppendAllText(xFileName, string.Format("</{0}>", xDB), Encoding.UTF8);
                if (File.Exists(xFileName))
                {
                    //launch new text file
                    Process oProcess = new Process();
                    oProcess.StartInfo.FileName = xFileName;
                    oProcess.Start();

                    oProcess.WaitForInputIdle();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                if (oForm != null)
                {
                    oForm.Close();
                    oForm.Dispose();
                }
                LMP.Benchmarks.Print(t0);
            }
        }
        public static void CreateGrailDataDump()
        {
            string xClientMatterNumber = "";
            string xPrefill = null;

            InsertUsingClientMatterForm oLookupForm = 
                new InsertUsingClientMatterForm(null);

            //get GrailStore object
            GrailBackend oBackend = LMP.Grail.GrailStore.Backend;

            //connect to GrailBackend - we do this here
            //to avoid the ClientMatter lookup from having to
            //connect to the store again
            oBackend.Connect(null);

            if (oLookupForm.ShowDialog() == DialogResult.OK)
            {
                xClientMatterNumber = oLookupForm.Matter.Number;
                xPrefill = oBackend.GetData(xClientMatterNumber);
            }
            else
            {
                return;
            }

            oBackend.Disconnect();


            LMP.Forte.MSWord.WordApp.CreateGrailDataDumpDoc(xPrefill);

            LMP.MacPac.Application.ScreenUpdating = true;
        }

        /// <summary>
        /// Create a Prefill based on current segment
        /// </summary>
        public static void SaveData(Segment oSegment)
        {
            try
            {
                UserFolders oFolders = new UserFolders(Session.CurrentUser.ID);
                //Can't create Prefill if no User Folders exist
                //GLOG : 8152 : JSW
                //let ContentPropertyForm handle folder creation messages
                //if (oFolders.Count == 0)
                //{
                //    MessageBox.Show(LMP.Resources.GetLangString(
                //        "Msg_PrefillRequiresUserFolder"), LMP.ComponentProperties.ProductName,
                //        MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}
                // GLOG : 6985 : JSW
                //messages and enforcement for saved data limits
                User oUser = Session.CurrentUser;
                VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.User, oUser.ID);
                int iSavedDataItems = oDefs.Count;
                int iLimit = oUser.SavedDataLimit;

                // Check if the user has exceeded the maximum number of segments allowed.
                if ((iSavedDataItems >= iLimit))
                {
                    // The user has exceeded the maximum allowed segments. Notify the user
                    // and do not create new content.
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_SavedDataLimitReached"),
                        iLimit), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                // Check if the number of saved data items requires warning the user that
                // they are approaching the maximum user content limit.
                if (iSavedDataItems >= oUser.SavedDataThreshold)
                {
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_ApproachingSavedDataLimit"),
                        iSavedDataItems, iLimit), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                
                ContentPropertyForm oForm = new ContentPropertyForm();
                oForm.ShowForm(null, null, Form.MousePosition,
                    ContentPropertyForm.DialogMode.CreatePrefill,null, oSegment);
                System.Windows.Forms.Application.DoEvents();

                //GLOG 3064: refresh content manager tree in all
                //task panes to reflect new saved data
                TaskPanes.RefreshAll(true, false, false);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                //GLOG : 8000 : ceh - set focus back to the document
                if (Session.CurrentWordApp.Documents.Count > 0) //GLOG 8327
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
        }
        public static void EditFavoriteSegments()
        {
            MultipleSegmentsForm oForm = new MultipleSegmentsForm("Edit Favorite Segments", "O&K", null, false);
            if (oForm.ShowDialog() == DialogResult.OK)
            {
            }
        }
        public static void SaveSegmentWithData(Segment oSegment)
        {
            try
            {
                LMP.MacPac.DocumentEditor.SaveSegmentWithData(oSegment);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// attaches the segment corresponding to the specified
        /// node to the content of the active document
        /// </summary>
        /// <param name="oNode"></param>
        public static void Transform(UltraTreeNode oNode, TaskPane oTaskPane)
        {
            //if there is a segment in the source document,
            //we need to get a prefill - depending on the 
            //current selection, we need to get a range
            //to include in the transformed document
            Prefill oPrefill = null;
            Word.Range oRng = null;
            //GLOG 6530
            bool bReplaceAll = false;

            //GLOG #5925 CEH
            //turn off track changes if necessary
            bool bTrackChanges = Session.CurrentWordApp.ActiveDocument.TrackRevisions;
            if (bTrackChanges)
                Session.CurrentWordApp.ActiveDocument.TrackRevisions = false;

            try
            {
                //get selection
                Word.Selection oSel = Session.CurrentWordApp.Selection;

                ISegmentDef oSrcSeg = ContentTreeHelper.GetSegmentDefFromNode(oNode);

                //GLOG : 8208 : ceh
                //Prompt if transforming to non-Forte segment
                string xID = ContentTreeHelper.GetSegmentIDFromNode(oNode);
                if (!xID.Contains("."))
                {
                    if (oSrcSeg.TypeID == mpObjectTypes.NonMacPacContent)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_TransformToNonMacPacContent"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                //GLOG 2858: Prompt to continue if text is selected, but target Segment has no Body block to accept it
                if (oSel.Characters.Count > 1 && !String.XMLContainsBodyBlock(oSrcSeg.XML))
                {
                    DialogResult iYesNo = MessageBox.Show(LMP.Resources.GetLangString("Prompt_ContinueTransformWithNoBody"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                    if (iYesNo != DialogResult.Yes)
                        return;
                }
                LMP.MacPac.Application.ScreenUpdating = false;

                if (ContentTreeHelper.IsVariableSetNode(oNode))
                {
                    VariableSetDef oDef = null;

                    //prefill to apply to this transform
                    if (oNode.Tag is VariableSetDef)
                        oDef = (VariableSetDef)oNode.Tag;
                    else
                        oDef = ContentTreeHelper.GetVariableSetFromNode(oNode);

                    //GLOG 3475
                    oPrefill = new Prefill(oDef.ContentString, oDef.Name, oDef.SegmentID);
                }

                //if the source document contains a segment, prefill
                //transformed document with values from source
                //GLOG 2713: Document with Trailer only should be treated as containing no Segments
                if (LMP.Forte.MSWord.WordDoc.ContainsNonTrailerMPContent(Session.CurrentWordApp.ActiveDocument))
                {
                    //get segment containing selection
                    Segment oSegment = oTaskPane.ForteDocument.GetTopLevelSegmentFromSelection();

                    if (oSegment == null)
                        //prefill using the first segment in the document
                        oSegment = oTaskPane.ForteDocument.Segments[0];


                    if (oSel.Characters.Count <= 1)
                    {
                        //get the range to insert in the transformed document
                        oRng = oSegment.GetBody();
                        bReplaceAll = true;
                    }
                    else
                    {
                        //use selection only
                        oRng = oSel.Range;
                        if (oRng.Characters.Count > 1)
                            bReplaceAll = true;
                    }

                    if (oPrefill == null)
                    {
                        //create the prefill to use when transforming the segment
                        oPrefill = new Prefill(oSegment);
                    }
                    //GLOG 3159:  Prompt to use Court Levels from Document or Segment Def
                    if (oPrefill != null && (oSrcSeg.L0 + oSrcSeg.L1 + oSrcSeg.L2 + oSrcSeg.L3 + oSrcSeg.L4 > 0) &&
                        (oPrefill["L0"] + oPrefill["L1"] + oPrefill["L2"] + oPrefill["L3"] + oPrefill["L4"] != ""))
                    {
                        //GLOG 2914: Prompt before overwriting Court levels from Prefill
                        try
                        {
                            if ((oPrefill["L0"] != oSrcSeg.L0.ToString() && oSrcSeg.L0 > 0) ||
                                (oPrefill["L1"] != oSrcSeg.L1.ToString() && oSrcSeg.L1 > 0) ||
                                (oPrefill["L2"] != oSrcSeg.L2.ToString() && oSrcSeg.L2 > 0) ||
                                (oPrefill["L3"] != oSrcSeg.L3.ToString() && oSrcSeg.L3 > 0) ||
                                (oPrefill["L4"] != oSrcSeg.L4.ToString() && oSrcSeg.L4 > 0))
                            {
                                //GLOG : 3159 : CEH
                                //string xTargetCourt = LMP.Data.Application.GetCourtText(oSrcSeg.L0.ToString(), oSrcSeg.L1.ToString(),
                                //    oSrcSeg.L2.ToString(), oSrcSeg.L3.ToString(), oSrcSeg.L4.ToString());
                                //string xSavedCourt = LMP.Data.Application.GetCourtText(oPrefill["L0"], oPrefill["L1"], oPrefill["L2"],
                                //    oPrefill["L3"], oPrefill["L4"]);
                                string xTargetCourt = oSrcSeg.DisplayName;
                                string xSavedCourt = oSegment.DisplayName;

                                DialogResult iRet = MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_UseCourtLevelsFromSavedData"),
                                    xTargetCourt, xSavedCourt, LMP.Resources.GetLangString("Prompt_DocumentData")), LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                                if (iRet != DialogResult.Yes)
                                {
                                    //Remove Court Level values from Prefill so they won't be applied
                                    oPrefill.Delete("L0");
                                    oPrefill.Delete("L1");
                                    oPrefill.Delete("L2");
                                    oPrefill.Delete("L3");
                                    oPrefill.Delete("L4");
                                    oPrefill.Delete("CourtTitle");
                                }
                            }
                        }
                        catch { }
                    }
                }
                else
                {
                    ISegmentDef oSegDef = ContentTreeHelper.GetSegmentDefFromNode(oNode);

                    if (oPrefill == null && oSegDef.TypeID != mpObjectTypes.SavedContent)
                    {
                        //no prefill has been chosen yet - 
                        //prompt to choose a prefill
                        DialogResult iChoice = MessageBox.Show(
                            LMP.Resources.GetLangString("Prompt_UsePrefill"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (iChoice == DialogResult.Yes)
                        {
                            PrefillChooser oForm = new PrefillChooser();

                            if (oForm.ShowDialog() == DialogResult.OK)
                            {
                                string xVSetID = oForm.VariableSetID;
                                //GLOG 5676: Don't limit to this User's saved data, 
                                //since shortcuts to shared items may also be used
                                VariableSetDefs oDefs = new VariableSetDefs(
                                    mpVariableSetsFilterFields.Owner, 0);
                                VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(xVSetID);
                                //GLOG 3475
                                oPrefill = new Prefill(oDef.ContentString, oDef.Name, oDef.SegmentID);
                            }

                            System.Windows.Forms.Application.DoEvents();
                        }
                    }
                    if (oSel.Characters.Count <= 1)
                    {
                        //GLOG 6530:  If there are multiple sections, only first section
                        //will be replaced in the transformed document
                        oRng = oTaskPane.ForteDocument.WordDocument.Sections[1].Range;
                        bReplaceAll = false;
                    }
                    else
                    {
                        //include only the selection of the main story
                        //in the transformed document
                        oRng = oSel.Range;
                        if (oRng.Characters.Count > 1)
                            bReplaceAll = true;
                    }
                }

		//GLOG : 8208 : ceh
                FinishTransform(xID, oRng, oPrefill, oTaskPane, bReplaceAll);
            }
            finally
            {
                oTaskPane.DataReviewerVisible = oTaskPane.ForteDocument.ContainsFinishedSegments;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                LMP.MacPac.Application.ScreenUpdating = true;
                //GLOG #5925 CEH
                if (bTrackChanges)
                    Session.CurrentWordApp.ActiveDocument.TrackRevisions = true;

            }
        }
        private static void FinishTransform(string xSegmentID, Word.Range oRng, Prefill oPrefill, TaskPane oTaskPane, bool bReplaceAll)
        {
            //prepare document for new segment, 
            //and get the xml of the range
            string xBodyXML = "";

            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

            xBodyXML = LMP.Forte.MSWord.WordDoc.SetupAttachToSource(oRng, bReplaceAll ? 0 : 1, bReplaceAll ? 0 : 1);

            if (xBodyXML == null)
                xBodyXML = "";

            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

            //GLOG 7843: Use ActiveDocument if oRng is null
            Word.Document oDoc = null;
            if (oRng != null)
            {
                oDoc = oRng.Document;
            }
            else
            {
                oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            }
            
            //insert segment
            //string xID = GetSegmentIDFromNode(oNode);
#if Oxml
            if (LMP.MacPac.Application.IsOxmlSupported())
            {
                LMP.Architect.Oxml.XmlSegment oNewXmlSeg = oTaskPane.InsertXmlSegmentInCurrentDoc(xSegmentID, null, oPrefill, Architect.Base.Segment.InsertionLocations.InsertAtStartOfDocument,
                    Architect.Base.Segment.InsertionBehaviors.InsertInSeparateNextPageSection, xBodyXML, null, oRng);
                if (oNewXmlSeg != null)
                {
                    oTaskPane.ScreenUpdating = false;
                    //Refresh tags from WordprocessingDocument contents
                    oTaskPane.ForteDocument.RefreshTags(oNewXmlSeg.ForteDocument.ContentXml);
                    oTaskPane.ForteDocument.Refresh(Architect.Base.ForteDocument.RefreshOptions.None, false, false, false);
                    oTaskPane.Mode = Architect.Base.ForteDocument.Modes.Edit;
                    oTaskPane.DocEditor.RefreshTree(true, true);
                    oTaskPane.SelectTab(TaskPane.Tabs.Edit);
                    oTaskPane.ScreenUpdating = true;
                }

            }
            else
#endif
            {
                Segment oNewSeg = oTaskPane.InsertSegmentInCurrentDoc(xSegmentID, oPrefill, xBodyXML,
                    Segment.InsertionLocations.InsertAtStartOfDocument,
                    Segment.InsertionBehaviors.InsertInSeparateNextPageSection);

                oRng = oNewSeg.GetBody();

                //style body, if it exists
                if (oRng != null)
                {
                    //GLOG 5223 (dm) - we decided that there was no basis for
                    //different treatment depending on whether the source contains
                    //Forte content - we should always preserve formatting
                    //if (bSourceHasSegments)
                    //    LMP.Forte.MSWord.WordDoc.ApplyBodyTextStyle(oRng);
                    //else
                    LMP.Forte.MSWord.WordDoc.ApplyBodyTextStyleIfUnstyled(oRng);
                }
            }
        }
        //GLOG 7316
        public static ArrayList InsertMultipleSegments(string xSegmentInsertionInfo, Prefill oPrefill)
        {
            return InsertMultipleSegments(xSegmentInsertionInfo, oPrefill, null);
        }
        /// <summary>
        /// inserts multiple segments
        /// </summary>
        /// <param name="xSegmentInsertionInfo"></param>
        public static ArrayList InsertMultipleSegments(string xSegmentInsertionInfo, Prefill oPrefill, ShowTaskPaneDelegate oMethod)
        {
            //GLOG : 8019 : ceh
            TaskPane oTP = null;
            LMP.Architect.Base.Segment oSegment = null; //GLOG 8469
            Word.Document oFirstSegmentDocument = null;
            LMP.Architect.Base.Segment oFirstSegment = null; //GLOG 8469
            ArrayList oSegsInNewDoc = new ArrayList();

            if (string.IsNullOrEmpty(xSegmentInsertionInfo))
            {
        		//GLOG 8069 
                //GLOG 8055
		        MultipleSegmentsForm oForm = new MultipleSegmentsForm(
                    "Insert Segment(s)", "O&K", null, true, null, false, true, oPrefill);

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    xSegmentInsertionInfo = oForm.Value;
                    oPrefill = oForm.Prefill;
                }
                else
                {
                    return null;
                }
            }

            //parse out save/deautomate flags from xml if they exist -
            //this was added for Pierce InsertUsingClientMatter for segment packets
            if (!string.IsNullOrEmpty(xSegmentInsertionInfo) &&
                xSegmentInsertionInfo.Substring(1, 1) == "|" &&
                xSegmentInsertionInfo.Substring(3, 1) == "|")
            {
                xSegmentInsertionInfo = xSegmentInsertionInfo.Substring(4);
            }

            ProgressList oProgress = new ProgressList();

            //get list of segment names for progress list
            string[] aDetails = xSegmentInsertionInfo.Split('~');
            List<string> aNames = new List<string>();
            for (int i = 0; i < aDetails.Length; i++)
            {
                int iPos = aDetails[i].IndexOf(";");
                string xName = aDetails[i].Substring(0, iPos);
                aNames.Add(xName);
            }

            //GLOG : 8299 : ceh - added bAllowCancel = true parameter
            oProgress.Setup(LMP.Resources.GetLangString("Dlg_InsertingMultipleSegments"),
                LMP.Resources.GetLangString("Dlg_InsertingTheFollowingSegments"), null, aNames, 2, null, true);

            int iSegment = 0;

            //GLOG 7316: Need to keep track of Segment count
            for (int i = 0; i <= aDetails.GetUpperBound(0); i++)
            {
                string xDetail = aDetails[i];
                if (oProgress.Canceled)
                    break;

                iSegment++;
                string[] aIDs = xDetail.Split(';');
                string xID = aIDs[1];
                Segment.InsertionLocations iLoc = (Segment.InsertionLocations)int.Parse(aIDs[2]);

                //GLOG item #6432 - dcf
                int iBehaviorValue = (aIDs[3] == "True" ? 1 : 0) +
                    (aIDs[4] == "True" ? 4 : 0) + (aIDs[5] == "True" ? 2 : 0);
                Segment.InsertionBehaviors iBehavior = (Segment.InsertionBehaviors)iBehaviorValue;

                //Make sure no other processes can reenable screenupdating
                TaskPane.SkipScreenUpdates = true;
                LMP.MacPac.Application.ScreenUpdating = false;

                bool bReusedDocument = false;
                try
                {
                    oProgress.UpdateProgress();

                    //GKIG 7316: Create new document if necessary
                    if (iLoc == Segment.InsertionLocations.InsertInNewDocument || Session.CurrentWordApp.Documents.Count == 0)
                    {
                        if (bReusedDocument)
                        {
                            //Last segment was inserted in existing doc
                            //Refresh tree only after all segments have been inserted
                            oTP.SelectTab(TaskPane.Tabs.Edit);
                            System.Windows.Forms.Application.DoEvents();
                            oTP.DocEditor.RefreshTree(true);
                        }

                        //create a new document
                        //GLOG 8469
                        oSegment = TaskPane.InsertSegmentInNewDoc(xID, oPrefill, null);
                        System.Windows.Forms.Application.DoEvents();
                        try
                        {
                            //GLOG 8469
                            oTP = TaskPanes.Item(Session.CurrentWordApp.ActiveDocument);
                        }
                        catch
                        {
                        }
                        if (oTP == null && oMethod != null)
                        {
                            //GLOG 8469
                            oTP = oMethod(Session.CurrentWordApp.ActiveDocument);
                        }
                        bReusedDocument = false;
                        //GLOG 8469: oSegment may be API or Oxml Segment object
                        Segment oNewSegment = oSegment as Segment;
                        if (oSegment is LMP.Architect.Oxml.XmlSegment)
                            oNewSegment = oTP.ForteDocument.FindSegment(oSegment.TagID);
                        //add to collection of segments
                        //that were inserted in a new document -
                        //we return this collection
                        oSegsInNewDoc.Add(oNewSegment);
                    }
                    else
                    {
                        try
                        {

                            Word.Selection oSel = Session.CurrentWordApp.Selection;

                            if (LMP.MacPac.Application.TaskPaneExists(oSel.Document))
                            {
                                oTP = TaskPanes.Item(oSel.Document);
                            }
                            else if (oMethod != null)
                            {
                                oTP = oMethod(oSel.Document);
                            }
                            oSegment = oTP.InsertSegmentInCurrentDoc(xID, oPrefill, null, iLoc, iBehavior);

                            //Let other ContentManager finish its work
                            System.Windows.Forms.Application.DoEvents();
                            bReusedDocument = true;
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        }
                        finally
                        {
                            LMP.MacPac.Application.SetXMLTagStateForEditing();
                        }
                    }

                    TaskPane.SkipScreenUpdates = false;
                    oTP.SelectEditorFirstNodeOfSegment(oTP.ForteDocument.FindSegment(oSegment.TagID));

                    if (iSegment == 1)
                    {
                        oFirstSegment = oSegment;
                        oFirstSegmentDocument = oTP.ForteDocument.WordDocument;
                    }
                    oProgress.UpdateProgress(ProgressList.StatusTypes.Succeeded);
                }
                catch
                {
                    oProgress.UpdateProgress(ProgressList.StatusTypes.Failed);
                }
                finally
                {
                    System.Windows.Forms.Application.DoEvents();
                    oProgress.Hide();
                }
                //GLOG 7316: If next Segment is to be inserted at cursor,
                //make sure selection is moved to end of this segment first
                if (i < aDetails.GetUpperBound(0))
                {
                    string[] aNext = aDetails[i + 1].Split(';');
                    if ((Segment.InsertionLocations)int.Parse(aNext[2]) == Architect.Base.Segment.InsertionLocations.InsertAtSelection)
                    {
                        Word.Range oRng = oTP.ForteDocument.WordDocument.Range();
                        try
                        {
                            //GLOG 8469
                            if (oSegment is LMP.Architect.Oxml.XmlSegment)
                                oRng = oTP.ForteDocument.FindSegment(oSegment.TagID).PrimaryBookmark.Range;
                            else
                                oRng = ((Segment)oSegment).PrimaryBookmark.Range;
                        }
                        catch { }
                        object oDirection = Word.WdCollapseDirection.wdCollapseEnd;
                        oRng.Collapse(ref oDirection);
                        oRng.Select();
                    }
                }
            }

            try
            {
                LMP.MacPac.Application.ScreenUpdating = false;

                if (oFirstSegmentDocument != null)
                {
                    oFirstSegmentDocument.Activate();
                    //GLOG 8469
                    if (oSegment is LMP.Architect.Oxml.XmlSegment)
                        oTP.DocEditor.SelectFirstNodeForSegment(oTP.ForteDocument.FindSegment(oFirstSegment.TagID));
                    else
                        oTP.DocEditor.SelectFirstNodeForSegment((Segment)oSegment);
                    System.Windows.Forms.Application.DoEvents();
                }

                return oSegsInNewDoc;

            }
            finally
            {
                TaskPane.SkipScreenUpdates = false;
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// inserts the packet with the specified ID
        /// using the specified prefill string
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="xPrefillString"></param>
        public static Segment[] InsertSegmentPacket(string xID, string xPrefillString)
        {
            //get segment def
            AdminSegmentDefs oDefs = new AdminSegmentDefs();
            AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(int.Parse(xID));
            ArrayList oDocs = new ArrayList();

            Prefill oPrefill = new Prefill(xPrefillString, "Test", "1");

            //insert multiple segments using selected prefill
            ArrayList oNewDocSegments = InsertMultipleUsingPrefill(oDef.XML, oPrefill); //GLOG 7316
            Segment[] aNewDocSegments = (Segment[])oNewDocSegments.ToArray(typeof(Segment));

            return aNewDocSegments;
        }

        /// <summary>
        /// inserts the packet with the specified ID
        /// using the specified prefill string
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="xPrefillString"></param>
        public static Segment[] InsertSegmentPacket_Server(string xID, string xPrefillString, 
            string xClientMatterNumber, SaveFileDelegate oSaveFile, string xAdditionalInformation)
        {
            //get segment def
            AdminSegmentDefs oDefs = new AdminSegmentDefs();
            AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(int.Parse(xID));

            string xSegmentInsertionInfo = oDef.XML;
            bool bDeautomate = false;
            bool bSaveFile = false;

            //parse out save/deautomate flags from xml if they exist -
            //this was added for Pierce InsertUsingClientMatter for segment packets
            if (!string.IsNullOrEmpty(xSegmentInsertionInfo) &&
                xSegmentInsertionInfo.Substring(1, 1) == "|" &&
                xSegmentInsertionInfo.Substring(3, 1) == "|")
            {
                bDeautomate = xSegmentInsertionInfo.Substring(0, 1) == "1";
                bSaveFile = xSegmentInsertionInfo.Substring(2, 1) == "1";

                xSegmentInsertionInfo = xSegmentInsertionInfo.Substring(4);
            }

            ArrayList oDocs = new ArrayList();

            Prefill oPrefill = new Prefill(xPrefillString, "Test", "1");

            //insert multiple segments using selected prefill
			//GLOG 7316: Capitalization has been conformed to other instances
            ArrayList oNewDocSegments = InsertMultipleUsingPrefill(xSegmentInsertionInfo, oPrefill, bDeautomate, 
                bSaveFile ? oSaveFile : null, xClientMatterNumber, ref xAdditionalInformation);

            if (bSaveFile)
            {
                object oFalse = false;
                object oMissing = System.Reflection.Missing.Value;
                if (LMP.Forte.MSWord.GlobalMethods.CurWordApp.Documents.Count > 0)
                {
                    LMP.Forte.MSWord.WordApp.ActiveDocument().Close(ref oFalse, ref oMissing, ref oMissing);
                }
            }

            Segment[] aNewDocSegments = (Segment[])oNewDocSegments.ToArray(typeof(Segment));

            return aNewDocSegments;
        }

        /// <summary>
        /// inserts the specified segments
        /// using the specified prefill
        /// </summary>
        /// <param name="xSegmentInsertionInfo"></param>
        /// <param name="oPrefill"></param>
        /// <returns></returns>
		//GLOG 7316: Capitalization has been conformed to other instances
        public static ArrayList InsertMultipleUsingPrefill(string xSegmentInsertionInfo, Prefill oPrefill)
        {
            string xAdditionalDetail = null;
            return InsertMultipleUsingPrefill(xSegmentInsertionInfo, oPrefill, false, null, null, ref xAdditionalDetail);
        }

        /// <summary>
        /// inserts the specified segment
        /// using the specified prefill -
        /// deautomates and saves if specified
        /// </summary>
        /// <param name="xSegmentInsertionInfo"></param>
        /// <param name="oPrefill"></param>
        /// <param name="bDeautomate"></param>
        /// <param name="oSaveFile"></param>
        /// <returns></returns>
		//GLOG 7316: Capitalization has been conformed to other instances
        private static ArrayList InsertMultipleUsingPrefill(string xSegmentInsertionInfo, 
            Prefill oPrefill, bool bDeautomate, SaveFileDelegate oSaveFile, 
            string xClientMatterNumber, ref string xAdditionalInformation)
        {
            Segment oSegment = null;
            ArrayList oSegsInNewDoc = new ArrayList();

            //get list of segment names for progress list
            string[] aDetails = xSegmentInsertionInfo.Split('~');

            int iSegment = 0;

            //GLOG 7316: Need to keep track of Segment count
            for (int i = 0; i <= aDetails.GetUpperBound(0); i++)
            {
                string xDetail = aDetails[i];
                iSegment++;
                string[] aIDs = xDetail.Split(';');
                string xID = aIDs[1];
                Segment.InsertionLocations iLoc = (Segment.InsertionLocations)int.Parse(aIDs[2]);

                //GLOG item #6432 - dcf
                int iBehaviorValue = (aIDs[3] == "True" ? 1 : 0) +
                    (aIDs[4] == "True" ? 4 : 0) + (aIDs[5] == "True" ? 2 : 0);
                Segment.InsertionBehaviors iBehavior = (Segment.InsertionBehaviors)iBehaviorValue;

                try
                {
                    if (iLoc == Segment.InsertionLocations.InsertInNewDocument)
                    {
                        //close previous doc if we're saving documents
                        if (oSaveFile != null)
                        {
                            object oFalse = false;
                            object oMissing = System.Reflection.Missing.Value;
                            if (LMP.Forte.MSWord.GlobalMethods.CurWordApp.Documents.Count > 0)
                            {
                                LMP.Forte.MSWord.WordApp.ActiveDocument().Close(ref oFalse, ref oMissing, ref oMissing);
                            }
                        }

                        //create a new document
                        oSegment = (LMP.Architect.Api.Segment) TaskPane.InsertSegmentInNewDoc(xID, oPrefill, null);
                        System.Windows.Forms.Application.DoEvents();

                        //add to collection of segments
                        //that were inserted in a new document -
                        //we return this collection
                        oSegsInNewDoc.Add(oSegment);
                    }
                    else
                    {
                        try
                        {
                            oSegment = InsertSegmentInCurrentDoc(xID, oPrefill,
                                null, iLoc, iBehavior);

                            //Let other ContentManager finish its work
                            System.Windows.Forms.Application.DoEvents();
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        }
                        finally
                        {
                            LMP.MacPac.Application.SetXMLTagStateForEditing();
                        }
                    }

                    Word.Document oDoc = oSegment.ForteDocument.WordDocument;
                    //GLOG 7316: If next Segment is to be inserted at cursor,
                    //make sure selection is moved to end of this segment first
                    if (i < aDetails.GetUpperBound(0))
                    {
                        string[] aNext = aDetails[i + 1].Split(';');
                        if ((Segment.InsertionLocations)int.Parse(aNext[2]) == Architect.Base.Segment.InsertionLocations.InsertAtSelection)
                        {
                            Word.Range oRng = oSegment.PrimaryBookmark.Range;
                            object oDirection = Word.WdCollapseDirection.wdCollapseEnd;
                            oRng.Collapse(ref oDirection);
                            oRng.Select();
                        }
                    }
                    if (bDeautomate)
                    {
                        //clean up document of inserted segment
                        LMP.MacPac.Application.RemoveAutomation(oDoc, false);
                    }

                    if (oSaveFile != null)
                    {
                        oSaveFile(oDoc, xClientMatterNumber, oSegment.DisplayName, 
                            oSegment.TypeID.ToString(), xAdditionalInformation);
                    }

                }
                catch
                {
                }
                finally
                {
                    System.Windows.Forms.Application.DoEvents();
                    LMP.MacPac.Application.ScreenUpdating = true;
                }
            }

            try
            {
                System.Windows.Forms.Application.DoEvents();
                return oSegsInNewDoc;
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// inserts the segment with the specified ID
        /// into the current document using the specified
        /// prefill and options
        /// </summary>
        /// <param name="xID"></param>
        /// <param name="oPrefill"></param>
        /// <param name="xSelectedText"></param>
        /// <param name="iLocation"></param>
        /// <param name="iOptions"></param>
        /// <returns></returns>
        internal static Segment InsertSegmentInCurrentDoc(string xID, Prefill oPrefill, string xSelectedText,
            Segment.InsertionLocations iLocation, Segment.InsertionBehaviors iOptions)
        {
            //GLOG 4040: This function moved to TaskPane to be accessible in Editor or ContentManager
            DateTime t0 = DateTime.Now;
            ForteDocument oMPDocument = new ForteDocument(LMP.Forte.MSWord.WordApp.ActiveDocument());
            Segment oSegment = null;

            bool bScreenUpdating = LMP.MacPac.Application.ScreenUpdating;

            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oMPDocument.WordDocument);
            oEnv.SaveState();
            LMP.MacPac.Application.ScreenUpdating = false;
            oEnv.SetExecutionState(mpTriState.True, mpTriState.Undefined,
                mpTriState.Undefined, mpTriState.Undefined, mpTriState.True, true);

            try
            {
                //insert segment and refresh task pane
                //Word.Range oRange;

                //If inserting in empty document, attach to new template if specified
                bool bAttachTemplate = LMP.Forte.MSWord.WordDoc.DocumentIsEmpty(oMPDocument.WordDocument);

                //insert segment 
                oSegment = oMPDocument.InsertSegment(xID, null, iLocation, iOptions,
                    true, oPrefill, xSelectedText, bAttachTemplate, bAttachTemplate,
                    null, false, false, null);
            }
            finally
            {
                oEnv.RestoreState(oSegment is Paper || oSegment is LMP.Architect.Base.IDocumentStamp, false, false);

                //this fixes a problem with screen refreshing
                //whereby the task pane sometimes freezes
                if (bScreenUpdating)
                {
                    LMP.MacPac.Application.ScreenUpdating = false;
                    LMP.MacPac.Application.ScreenUpdating = true;
                }

                LMP.Benchmarks.Print(t0);

            }
            return oSegment;
        }

        /// displays the dialog box that inserts
        /// formatted contact addresses
        /// </summary>
        public static void InsertFormattedAddresses()
        {
            try
            {
                //get current document
                Word.Document oDoc = null;
                try
                {
                    oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
                }
                catch { }
                if (oDoc == null)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_ActiveDocumentRequired"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                InsertFormattedAddressesForm oForm = new InsertFormattedAddressesForm();

                DialogResult iRet = oForm.ShowDialog();
                System.Windows.Forms.Application.DoEvents();
                oDoc.ActiveWindow.SetFocus();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// inserts a TOA
        /// </summary>
        /// <param name="oSegment"></param>
        public static bool InsertTOA(Segment oSegment)
        {
            bool bInserted = false;

            try
            {
                SegmentInsertionForm oForm = new SegmentInsertionForm(mpObjectTypes.TOA, false);

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    System.Windows.Forms.Application.DoEvents();
                    //GLOG 4040: Call TaskPane functions to insert                    
                    if (oForm.InsertionLocation == Segment.InsertionLocations.InsertInNewDocument)
                    {
                        TaskPane.InsertSegmentInNewDoc(oForm.SegmentDef.ID.ToString(),
                            null, null);

                        bInserted = true;
                    }
                    else
                    {
                        LMP.MacPac.TaskPane oTP = null;

                        if (oSegment != null)
                        {
                            oTP = TaskPanes.Item(oSegment.ForteDocument.WordDocument);

                            oTP.InsertSegmentInCurrentDoc(oForm.SegmentDef.ID.ToString(), null,
                                null, oForm.InsertionLocation, oForm.InsertionBehavior);

                            bInserted = true;
                        }
                        else
                        {
                            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
                            oTP = TaskPanes.Item(oDoc);

                            if (oTP == null)
                            {
                                ForteDocument oMPDoc = new ForteDocument(oDoc);
                                //GLOG 7092: Make sure Tags collection is refreshed in Ribbon-only mode
                                if ((LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator) && oMPDoc.Tags.Count == 0)
                                    oMPDoc.Refresh(ForteDocument.RefreshOptions.None);

                                oSegment = oMPDoc.InsertSegment(oForm.SegmentDef.ID.ToString(), null,
                                    oForm.InsertionLocation, oForm.InsertionBehavior,
                                    true, null, "", false, false, null, false, false, null);

                                bInserted = true;
                            }
                            else
                            {
                                oTP.InsertSegmentInCurrentDoc(oForm.SegmentDef.ID.ToString(), null,
                                    null, oForm.InsertionLocation, oForm.InsertionBehavior);

                                bInserted = true;
                            }
                        }
                    }

                    LMP.Forte.MSWord.WordApp.ActiveDocument().ActiveWindow.View.ShowFieldCodes = false;
                }
                else
                {
                    //GLOG : 8000 : ceh - set focus back to the document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }

                return bInserted;
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }
        public static void UpdateTOA(Segment oSegment)
        {
            try
            {
                LMP.MacPac.Application.ScreenUpdating = false;

                //insert new toa
                //10-1-13 (dm) - fully reworked for GLOG 7105 - this method previously
                //created a disconnect between the variable values and document 
                LMP.Forte.MSWord.Pleading oPleading = new LMP.Forte.MSWord.Pleading();

                //GLOG 5103: Target first child mBlock for TOA insertion rather than mSEG
                Word.Range oRngTOA = null;
                Word.Range oPrimaryRng = oSegment.PrimaryRange;
                if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    for (int i = 1; i <= oPrimaryRng.XMLNodes.Count; i++)
                    {
                        if (oPrimaryRng.XMLNodes[i].BaseName == "mBlock")
                        {
                            oRngTOA = oPrimaryRng.XMLNodes[i].Range;
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 1; i <= oPrimaryRng.ContentControls.Count; i++)
                    {
                        object oIndex = i;
                        if (LMP.String.GetBoundingObjectBaseName(oPrimaryRng.ContentControls.get_Item(ref oIndex).Tag) == "mBlock")
                        {
                            oRngTOA = oPrimaryRng.ContentControls.get_Item(ref oIndex).Range;
                            break;
                        }
                    }
                }

                //get "section only" and "insert as field" variables -
                //other variable actions may depend on these
                bool bRefresh = false;
                Variable oSectionOnlyVar = null;
                Variable oUnlinkVar = null;
                for (int i = 0; i < oSegment.Variables.Count; i++)
                {
                    Variable oVar = oSegment.Variables[i];
                    for (int j = 0; j < oVar.VariableActions.Count; j++)
                    {
                        VariableAction oAction = oVar.VariableActions[j];
                        string xParams = oAction.Parameters;
                        if (oAction.Type == VariableActions.Types.IncludeExcludeBlocks)
                        {
                            if (xParams.Contains("TableOfAuthorities"))
                            {
                                oSectionOnlyVar = oVar;
                                break;
                            }
                        }
                        else if (xParams.Contains("Fields.Unlink"))
                        {
                            oUnlinkVar = oVar;
                            break;
                        }
                    }
                }

                if (oSectionOnlyVar != null)
                {
                    //reinsert scope if necessary
                    if (oRngTOA == null)
                    {
                        //flip include/exclude variable value
                        string xNewValue = (!System.Boolean.Parse(oSectionOnlyVar.Value)).ToString();
                        oSectionOnlyVar.SetValue(xNewValue, true);
                        bRefresh = true;

                        //expand primary range
                        object oUnit = Word.WdUnits.wdParagraph;
                        oPrimaryRng.Expand(ref oUnit);

                        //find block
                        if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        {
                            for (int i = 1; i <= oPrimaryRng.XMLNodes.Count; i++)
                            {
                                if (oPrimaryRng.XMLNodes[i].BaseName == "mBlock")
                                {
                                    oRngTOA = oPrimaryRng.XMLNodes[i].Range;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= oPrimaryRng.ContentControls.Count; i++)
                            {
                                object oIndex = i;
                                if (LMP.String.GetBoundingObjectBaseName(oPrimaryRng.ContentControls.get_Item(ref oIndex).Tag) == "mBlock")
                                {
                                    oRngTOA = oPrimaryRng.ContentControls.get_Item(ref oIndex).Range;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                    //no TOA block - target mSEG
                    oRngTOA = oPrimaryRng;

                //GLOG 7278 (dm) - store primary bookmark name
                string xBmk = oSegment.PrimaryBookmark.Name;

                //update
                oPleading.DeleteTOAFieldCodes(oRngTOA);
                oPleading.InsertTOAFieldCodes(oRngTOA, true);

                //GLOG 7278 (dm) - primary bookmark of finished TOA may get deleted
                //when field codes are inserted above - restore now
                if (oSegment.IsFinished)
                {
                    //GLOG 7293 (dm) - avoid error when there are no entries
                    Word.Range oRngBmk = null;
                    if (oRngTOA.Paragraphs[1].Range.Fields.Count > 0)
                    {
                        oRngBmk = oRngTOA.Paragraphs[1].Range.Fields[1].Code;
                        object oUnit = Word.WdUnits.wdParagraph;
                        object oCount = 1;
                        oRngBmk.MoveEnd(ref oUnit, ref oCount);
                    }
                    else
                        oRngBmk = oRngTOA.Duplicate;
                    object oTOA = (object)oRngBmk;
                    oRngTOA.Document.Bookmarks.Add(xBmk, ref oTOA);
                }

                //execute variable actions, skipping existential ones
                //GLOG 7184 (dm) - errors occur when there are no entries and thus no
                //field and also when type preferences have not yet been set - condition
                //on variable actions on existence of field and ignore errors (we're
                //never going to account for every possible configuration here)
                if (oPrimaryRng.Fields.Count > 0)
                {
                    for (int i = 0; i < oSegment.Variables.Count; i++)
                    {
                        Variable oVar = oSegment.Variables[i];
                        if (((oSectionOnlyVar == null) || (oVar.Name != oSectionOnlyVar.Name)) &&
                            ((oUnlinkVar == null) || (oVar.Name != oUnlinkVar.Name)))
                        {
                            try
                            {
                                oVar.VariableActions.Execute();
                            }
                            catch { }
                        }
                    }
                }

                //execute "insert as field" action last
                if (oUnlinkVar != null)
                    oUnlinkVar.VariableActions.Execute();

                //refresh task pane if necessary
                if (bRefresh)
                {
                    TaskPane oTP = null;
                    try
                    {
                        oTP = TaskPanes.Item(oSegment.ForteDocument.WordDocument);
                    }
                    catch { }
                    if (oTP != null)
                        oTP.DocEditor.RefreshTree(false, true);
                }

                //GLOG 7775 (dm)
                LMP.Forte.MSWord.GlobalMethods.CurWordApp.StatusBar =
                    LMP.Resources.GetLangString("Msg_TOAUpdated");
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// refreshes the Word field codes contained in the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public static void RefreshFieldCodes(Segment oSegment)
        {
            if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                foreach (Word.XMLNode oWordTag in oSegment.WordTags)
                {
                    oWordTag.Range.Fields.Update();
                }
            }
            else
            {
                foreach (Word.ContentControl oCC in oSegment.ContentControls)
                {
                    oCC.Range.Fields.Update();
                }
            }
        }
        // GLOG : 2340 : JAB
        // Set the quote style.
        public static void SetQuoteStyle(Segment oSegment)
        {
            try
            {
                //switched from style name to constant (2/19/09 dm)
                object oQuoteStyleID = Word.WdBuiltinStyle.wdStyleQuote;
                Word.Style oQuoteStyle = oSegment.ForteDocument.WordDocument.Styles.get_Item(
                    ref oQuoteStyleID);
                object o = (object)oQuoteStyle;
                LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.set_Style(ref o);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public static void RecreateSegment(Segment oSegment)
        {
            try
            {
                TaskPane oTP = TaskPanes.Item(oSegment.ForteDocument.WordDocument);
                oTP.DocEditor.RecreateSegment(oSegment);
                ActivateRibbonIfAppropriate(LMP.MacPac.Application.MacPacRibbonTabControlId);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG 8815
        private static string GetMacPac10RibbonTabControlID()
        {
            //GLOG 7914 (dm) - ribbon has new name in Tools
            string xRibbon = "";
            if (LMP.Registry.GetMacPac10Value("RibbonOnly") == "1")
                xRibbon = "ForteToolsRibbon.xml";
            else
                xRibbon = "ForteRibbon.xml";

            //GLOG 7914 (dm) - we weren't supporting ribbon in Data folder here
            string xFile = LMP.Data.Application.PublicDataDirectory + "\\" + xRibbon;
            if (!File.Exists(xFile))
                xFile = LMP.MacPac.Application.BinDirectory + @"\" + xRibbon;
            string xXML = File.OpenText(xFile).ReadToEnd();

            //First, test for idQ format (3/24/11 jts)
            int iPos = xXML.IndexOf("<tab idQ=\"x:MacPac\"");
            if (iPos == -1)
                return "MacPac";
            else
                return "x:MacPac";

        }
        private static string GetMacPac10RibbonTabDisplayName()
        {
            //using (StreamReader oReader = File.OpenText("ForteRibbon.xml"))
            //{
            //    XmlDocument oXML = new XmlDocument();
            //    oXML.LoadXml("<file>" + oReader.ReadToEnd() + "</file>");
            //    //oXML.LoadXml(oReader.ReadToEnd());
            //    XmlNode oNode = oXML.SelectSingleNode("customUI\ribbon\tabs\tab[@id='MacPac']").SelectSingleNode("@label");

            //    return oNode.Value;
            //}

            //GLOG 7914 (dm) - ribbon has new name in Tools
            string xRibbon = "";
            if (LMP.Registry.GetMacPac10Value("RibbonOnly") == "1")
                xRibbon = "ForteToolsRibbon.xml";
            else
                xRibbon = "ForteRibbon.xml";

            //GLOG 7914 (dm) - we weren't supporting ribbon in Data folder here
            string xFile = LMP.Data.Application.PublicDataDirectory + "\\" + xRibbon;
            if (!File.Exists(xFile))
                xFile = LMP.MacPac.Application.BinDirectory + @"\" + xRibbon;
            string xXML = File.OpenText(xFile).ReadToEnd();

            //First, test for idQ format (3/24/11 jts)
            int iPos = xXML.IndexOf("<tab idQ=\"x:MacPac\"");
            if (iPos == -1)
                iPos = xXML.IndexOf("<tab id=\"MacPac\"");
            if (iPos > -1)
            {
                iPos = xXML.IndexOf("label=", iPos);
                iPos = xXML.IndexOf("\"", iPos);
                int iPos2 = xXML.IndexOf("\"", iPos + 1);
                string xVal = xXML.Substring(iPos + 1, iPos2 - iPos - 1);
                return xVal;
            }
            else
            {
                //return default label (3/24/11 jts)
                //Return in correct case for Word 2013
				//GLOG: 8197 : jsw
				if (LMP.Forte.MSWord.WordApp.Version == 15)
                    return LMP.ComponentProperties.ProductName.ToUpper();
                else
                    return LMP.ComponentProperties.ProductName;
            }

        }

        public static string MacPacRibbonTabControlId
        {
            get
            {
                if (string.IsNullOrEmpty(m_xRibbonTabControlId))
                {
                    //GLOG 8815
                    m_xRibbonTabControlId = GetMacPac10RibbonTabControlID();
                    //m_xRibbonTabName = GetMacPac10RibbonTabDisplayName();
                }

                return m_xRibbonTabControlId;
            }
        }
        /// <summary>
        /// Capture XML of segment for future paste operation
        /// </summary>
        /// <param name="oSeg"></param>
        public static void CopySegmentContent(Segment oSeg)
        {
            try
            {
                string xSegmentXML = DocumentEditor.GetSegmentXML(oSeg);

                string xOutputFile = TaskPane.SegmentPasteFile;
                System.IO.StreamWriter oWriter = new System.IO.StreamWriter(
                    xOutputFile, false, Encoding.UTF8);
                oWriter.Write(xSegmentXML);
                oWriter.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            try
            {
                //Force refresh of ribbon button
                MacPac.Application.InvalidateMacPacRibbonControl("PasteSegmentXML");
            }
            catch { }
        }
        /// <summary>
        /// Update styles in document from selected Segment XML
        /// </summary>
        /// <param name="oSegment"></param>
        public static void RefreshStyles(Segment oSegment)
        {

            string xMsg = LMP.Resources.GetLangString("Prompt_RefreshStyles");
            bool bAlternatePrompt = false;
            bool bRequiredOnly = false;
            //GLOG 4711: Don't overwrite Document Styles if oSegment is child segment
            if (oSegment.IsTopLevel && oSegment.ForteDocument.Segments.Count == 1)
            {
                //Always update all styles if this is the only top-level Segment
                bRequiredOnly = false;
            }
            else if (oSegment.IntendedUse != mpSegmentIntendedUses.AsDocument ||
                !oSegment.IsTopLevel)
            {
                //Update only Required Styles for non-Document or Child Segments
                bRequiredOnly = true;
            }
            else
            {
                //Use different Prompt if there is more than one top-level Segment
                xMsg = string.Format(LMP.Resources.GetLangString("Prompt_RefreshStylesOverwriteDocumentStyles"),
                    oSegment.DisplayName);
                bAlternatePrompt = true;
            }
            if (bAlternatePrompt)
            {
                DialogResult iRet = MessageBox.Show(xMsg,
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
                if (iRet == DialogResult.Cancel)
                {
                    return;
                }
                else if (iRet == DialogResult.Yes)
                {
                    bRequiredOnly = false;
                }
                else
                {
                    bRequiredOnly = true;
                }
            }
            else
            {
                if (MessageBox.Show(xMsg,
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.No)
                    return;
            }

            if (false)
            {
                //TODO:  Need to check if Segment originates from a different firm
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_SegmentNotInDatabase") + oSegment.DisplayName,
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else if (!oSegment.HasDefinition)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_SegmentNotInDatabase") + oSegment.DisplayName,
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                //Get latest Definition XML
                string xXML = "";
                if (oSegment is UserSegment)
                {
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                    UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(oSegment.ID);
                    xXML = oDef.XML;
                    
                }
                else
                {
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oSegment.ID1);
                    xXML = oDef.XML;
                }
                if (xXML != "")
                {
                    string[] aRequiredStyles = new string[0];
                    if (bRequiredOnly && oSegment is AdminSegment)
                    {
                        if (!string.IsNullOrEmpty(((AdminSegment)oSegment).RequiredStyles))
                            aRequiredStyles = ((AdminSegment)oSegment).RequiredStyles.Split(',');
                    }
                    if (!bRequiredOnly)
                    {
                        oSegment.ForteDocument.UpdateStylesFromXML(xXML);
                    }
                    else
                    {
                        if (aRequiredStyles != null && aRequiredStyles.GetLength(0) > 0)
                            oSegment.ForteDocument.UpdateStylesFromXML(xXML, aRequiredStyles);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6478 : jsw 
        public static void PrintSelectedLetterhead(Segment oSeg)
        {

            //save environment
            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oSeg.ForteDocument.WordDocument);
            oEnv.SaveState();

            try
            {
                //get currently assigned letterhead
                Segment[] oLetterheads = oSeg.FindChildren(mpObjectTypes.Letterhead);
                Segment oLetterhead = null;
                int iLetterheadID = 0;
                Prefill oPrefill = null; //GLOG 8493
                if (oLetterheads.Length > 0)
                {
                    oLetterhead = oLetterheads[0];
                    iLetterheadID = oLetterhead.ID1;
                    oPrefill = new Prefill(oLetterhead); //GLOG 8493
                }

                //present dialog for changing letterhead
                ChangeLetterhead(oSeg);
                //GLOG 8906: Get current parent segment, since original may have been reinitialized
                oSeg = oSeg.ForteDocument.GetSegment(oSeg.TagID, oSeg.ForteDocument);
                //bring up Print Final dialog
                LMP.MacPac.Application.PrintFinal(Session.CurrentWordApp.ActiveDocument);
                
                //turn off screen updating
                LMP.MacPac.Application.ScreenUpdating = false;

                oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.Undefined, mpTriState.Undefined,
                mpTriState.Undefined, mpTriState.True, false);
                //reassign original letterhead
                //GLOG 8493: Reapply Prefill from original Letterhead
                oSeg.ForteDocument.InsertSegment(iLetterheadID + ".0", oSeg,
                Segment.InsertionLocations.Default, Segment.InsertionBehaviors.Default, null, oPrefill);
            }    
            finally
            {    
                oEnv.RestoreState(true, true, false);
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }

        public static void PrintDraft(Word.Document oDoc)
        {
            //GLOG 6288
            if (oDoc != null && LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oDoc.ActiveWindow))
            {
                //need to select the main comparison window -
                //Word freezes below if the selection is in
                //a revision pane - dcf 7/21/11
                if (oDoc.ActiveWindow.ActivePane.Index > 1)
                {
                    oDoc.ActiveWindow.Panes[1].Activate();
                    oDoc = oDoc.ActiveWindow.ActivePane.Document;
                }
            }

            try
            {
                //GLOG : 6421 : CEH
                LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
                //GLOG 8185: Prevent Content Control event handlers from running when Page Layout changes
                ForteDocument.IgnoreWordXMLEvents = Architect.Base.ForteDocument.WordXMLEvents.All;
                oEnv.UnprotectDocument();
                LMP.Forte.MSWord.WordApp.FilePrintDraft(oDoc,
                    LMP.MacPac.Session.CurrentUser.FirmSettings.ProtectedDocumentsPassword);
                oEnv.RestoreProtection();
            }
            catch (System.Exception oE)
            {
                if (oE.Message == LMP.Resources.GetLangString("Error_InvalidProtectedDocPassword"))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_IncorrectFormsPassword"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }
            }
            finally
            {
                //GLOG 8185
                ForteDocument.IgnoreWordXMLEvents = Architect.Base.ForteDocument.WordXMLEvents.None;
            }
        }

        public static void PrintFinal(Word.Document oDoc)
        {
            //GLOG 6288
            if (oDoc != null && LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oDoc.ActiveWindow))
            {
                //need to select the main comparison window -
                //Word freezes below if the selection is in
                //a revision pane - dcf 7/21/11
                if (oDoc.ActiveWindow.ActivePane.Index > 1)
                {
                    oDoc.ActiveWindow.Panes[1].Activate();
                    oDoc = oDoc.ActiveWindow.ActivePane.Document;
                }
            }

            UserApplicationSettings oSettings = LMP.MacPac.Session.CurrentUser.UserSettings;
            int iFirstPageTray = oSettings.FirstPagePrinterTray;
            int iOtherPagesTray = oSettings.OtherPagesPrinterTray;
            //GLOG : 6421 : CEH
            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
            //GLOG 8185: Prevent Content Control event handlers from running when Page Layout changes
            ForteDocument.IgnoreWordXMLEvents = Architect.Base.ForteDocument.WordXMLEvents.All;
            try
            {
                oEnv.UnprotectDocument();
                LMP.Forte.MSWord.WordApp.FilePrintFinal(oDoc,
                    LMP.MacPac.Session.CurrentUser.FirmSettings.ProtectedDocumentsPassword,
                    ref iFirstPageTray, ref iOtherPagesTray);
                oEnv.RestoreProtection();
            }
            catch (System.Exception oE)
            {
                if (oE.Message == LMP.Resources.GetLangString("Error_InvalidProtectedDocPassword"))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_IncorrectFormsPassword"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }
            }
            finally
            {
                oSettings.FirstPagePrinterTray = iFirstPageTray;
                oSettings.OtherPagesPrinterTray = iOtherPagesTray;
                //GLOG 8185
                ForteDocument.IgnoreWordXMLEvents = Architect.Base.ForteDocument.WordXMLEvents.None;
            }
        }

        //GLOG : 7323 : ceh
        public static void PrintCurrentPage(Word.Document oDoc)
        {
            //GLOG 6288
            if (oDoc != null && LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oDoc.ActiveWindow))
            {
                //need to select the main comparison window -
                //Word freezes below if the selection is in
                //a revision pane - dcf 7/21/11
                if (oDoc.ActiveWindow.ActivePane.Index > 1)
                {
                    oDoc.ActiveWindow.Panes[1].Activate();
                    oDoc = oDoc.ActiveWindow.ActivePane.Document;
                }
            }

            //GLOG : 6421 : CEH
            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);

            try
            {
                oEnv.UnprotectDocument();
                LMP.Forte.MSWord.WordApp.FilePrintCurrentPage();
                oEnv.RestoreProtection();
            }
            catch (System.Exception oE)
            {
                if (oE.Message == LMP.Resources.GetLangString("Error_InvalidProtectedDocPassword"))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_IncorrectFormsPassword"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }
            }
        }
        #endregion
        #region *********************static methods*********************
        public static bool IsOxmlSupported()
        {
            //Only use Oxml for Word 2013 and higher
            //GLOG : 8465 : jsw
            //GLOG 8151: Don't use Oxml in Tools mode
            //GLOG 15852 (dm) - we should generally support oxml in Tools and just disable it on a case-by-case basis
            //return (LMP.Forte.MSWord.WordApp.Version > 14 && (!(LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)));  //&& LMP.Utility.InDevelopmentModeState(2));
            return (LMP.Forte.MSWord.WordApp.Version > 14);
        }
        public static bool OnDemandSyncExecuting
        {
            get { return m_bOnDemandSyncExecuting; }
        }
        //GLOG : 8318 : jsw
        public static bool LicenseIsValid
        {
            get { return m_bLicenseIsValid; }
            private set { m_bLicenseIsValid = value; }
           
        }

        public static void AdjustDialogWidthForWord15(System.Windows.Forms.Form oForm)
        {
            if (LMP.Forte.MSWord.WordApp.Version > 14)
            {
                oForm.Width = oForm.Width + 10;
            }
        }

        /// <summary>
        /// sends the specified 'pseudo-event' message to
        /// the task pane of the specified document - 
        /// returns true if the message was sent
        /// </summary>
        /// <param name="oDocument"></param>
        internal static bool SendMessageToTaskPane(Word.Document oDocument, mpPseudoWordEvents iEvent)
        {
            //get handle of task pane corresponding to oDocument -
            //it's held in a Word document variable
            int iHandle = 0;
            object xVar = "zzmp10TP";

            Word.Variable oVar = oDocument.Variables.get_Item(ref xVar);
            try
            {
                iHandle = int.Parse(oVar.Value);
            }
            catch { }

            //if there's an Forte task pane in oDocument, 
            //send message
            int iRet = 0;
            if (iHandle != 0)
                iRet = (int)LMP.WinAPI.SendMessage((IntPtr)iHandle, (int)iEvent, IntPtr.Zero, IntPtr.Zero);

            //return true if the message was processed
            return iRet == 1;
        }
        /// <summary>
        /// activates the MacPac ribbon tab if specifed by the user
        /// </summary>
        public static void ActivateRibbonIfAppropriate(string xTabControlId)
        {
            DateTime t0 = DateTime.Now;

            if (Session.CurrentUser.UserSettings.ActivateMacPacRibbonTab)
            {
                int iPos = xTabControlId.IndexOf(":");
                if (iPos > -1)
                {
                    //Control ID contains namespace prefix (idQ=)
                    xTabControlId = xTabControlId.Substring(iPos + 1);
                    MacPac10Ribbon.ActivateTabQ(xTabControlId, "MacPacRibbon");
                }
                else
                {
                    //id=
                    MacPac10Ribbon.ActivateTab(xTabControlId);
                }
                //activate MacPac ribbon tab
                //LMP.Forte.MSWord.WordApp.SelectRibbonTab(xTabName);
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// removes tags in the specified Macpac Document
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <param name="bSkipSegmentTags"></param>
        private static bool RemoveTags(ForteDocument oMPDocument, 
            LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType, bool bPrompt)
        {
            //GLOG 5770 (dm) - changed return type from void to bool
            //GLOG 5796 (dm) - changed conditional - we were removing all
            //tags when iRemovalType was TextSegmentsOnly
            if (iRemovalType != LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All)
            {
                DialogResult iRes = DialogResult.Yes;
                if (bPrompt)
                {
                    iRes = MessageBox.Show(LMP.Resources.GetLangString("Prompt_RemoveVarAndBlockTagsInDocument"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                }
                if (iRes == DialogResult.Yes)
                {
                    for (int i = 0; i < oMPDocument.Segments.Count; i++)
                    {
                        RemoveTags(oMPDocument.Segments[i], iRemovalType);
                    }
                    return true;
                }
                else
                    return false;
            }
            else
            {
                return RemoveTags(oMPDocument, bPrompt);
            }
        }
        /// <summary>
        /// removes the tags in the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="bSkipSegmentTags"></param>
        internal static void RemoveTags(Segment oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType)
        {
            //turn off all word event handling
            ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

            try
            {
                //recurse through children
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    RemoveTags(oSegment.Segments[i], iRemovalType);
                }

                //get segment bookmarks
                Word.Bookmark[] aBmks = oSegment.Nodes.GetBookmarks(oSegment.FullTagID);
                LMP.Forte.MSWord.Tags oTags = oSegment.ForteDocument.Tags;

                //GLOG 7394 (dm) - disable track changes while removing tags
                Word.Document oDoc = oSegment.ForteDocument.WordDocument;
                bool bTrackChanges = oDoc.TrackRevisions;
                if (bTrackChanges)
                    oDoc.TrackRevisions = false;

                //cycle through segment bookmarks, recursing
                for (int i = aBmks.GetUpperBound(0); i >= 0; i--)
                {
                    LMP.Forte.MSWord.WordDoc.RemoveTagsInBookmark(
                        aBmks[i], iRemovalType, true, ref oTags);
                }

                //GLOG 7394 (dm)
                if (bTrackChanges)
                    oDoc.TrackRevisions = true;
            }
            finally
            {
                //restore word event handling to original state
                ForteDocument.IgnoreWordXMLEvents = oEvents;
            }
        }
        private static void RemoveContentControlsRecurse(Segment oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType)
        {
            //cycle through child segments, removing tags
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Segment oChild = oSegment.Segments[i];

                //recurse child's children
                if (oChild.Segments.Count > 0)
                    RemoveContentControlsRecurse(oChild, iRemovalType);

                //remove child tags
                Word.ContentControl[] aCCs = oSegment.Nodes.GetContentControls(oChild.FullTagID);

                foreach (Word.ContentControl oCC in aCCs)
                {
                    LMP.Forte.MSWord.WordDoc.RemoveContentControls(oCC, iRemovalType, true);
                }
            }
        }
        private static void RemoveTagsRecurse(Segment oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType)
        {
            //cycle through child segments, removing tags
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Segment oChild = oSegment.Segments[i];

                //recurse child's children
                if (oChild.Segments.Count > 0)
                    RemoveTagsRecurse(oChild, iRemovalType);

                //remove child tags
                foreach (Word.XMLNode oTag in oChild.WordTags)
                {
                    LMP.Forte.MSWord.WordDoc.RemoveTags(oTag, iRemovalType, true);

                    //GLOG 5796 (dm) - the following code is no longer necessary and
                    //was erring on finish when oTag was a text segment - the tagless
                    //variables are now deleted elsewhere
                    //if (oTag.BaseName == "mSEG")
                    //{
                    //    //clear out all variable definitions
                    //    //GLOG 4441: Make sure ObjectData is decrypted if necessary
                    //    //10-17-11 (dm) - changed FastSearchSkippingTextNodes parameter to true,
                    //    //which is the default - false was crashing Word on a draft stamp
                    //    //10-18-11 (dm) - test for ObjectData attribute
                    //    Word.XMLNode oObjectData = null;
                    //    try
                    //    {
                    //        oObjectData = oTag.SelectSingleNode("@ObjectData", "", true);
                    //    }
                    //    catch { }
                    //    if (oObjectData != null)
                    //    {
                    //        string xValue = String.Decrypt(oObjectData.Text);
                    //        xValue = Regex.Replace(xValue, @"VariableDefinition=.*?\|", "");

                    //        //write to document
                    //        LMP.Forte.MSWord.WordDoc.SetAttributeValue(
                    //            oTag, "ObjectData", xValue, LMP.Data.Application.EncryptionPassword);
                    //    }
                    //}
                }
            }
        }
        /// <summary>
        /// activates the MacPac ribbon tab in Word 2007 on Open if specifed by the user
        /// </summary>
        //GLOG : 2620 : CEH
        public static void ActivateMacPacRibbonIfAppropriateOnOpen(Word.Document oDocument)
        {
            //ensure that the ActiveDocument property is available -
            //this is not available when opening an word document that
            //is an email attachment
            try
            {
                Word.Document oDoc = oDocument.Application.ActiveDocument;
            }
            catch
            {
                return;
            }
            if (Session.CurrentUser.UserSettings.ActivateMacPacRibbonTabOnOpen &&
                    LMP.Forte.MSWord.WordDoc.ContainsNonTrailerMPContent(oDocument))
            {
                //activate MacPac ribbon tab
                //LMP.Forte.MSWord.WordApp.SelectRibbonTab(LMP.MacPac.Application.MacPacRibbonTabName);
                string xTabControlID = Application.GetMacPac10RibbonTabControlID();
                int iPos = xTabControlID.IndexOf(":");
                if (iPos > -1)
                {
                    //Control ID contains namespace prefix (idQ=)
                    xTabControlID = xTabControlID.Substring(iPos + 1);
                    MacPac10Ribbon.ActivateTabQ(xTabControlID, "MacPacRibbon");
                }
                else
                {
                    //id=
                    MacPac10Ribbon.ActivateTab(xTabControlID);
                }
            }
        }
        /// <summary>
        /// displays the keyboard shortcuts in the application
        /// </summary>
        public static void ShowKeyboardShortcuts()
        {
            try
            {
                Help oForm = new Help("");

                //GLOG 7774 (dm) - different keyboard shortcuts for Forte Tools
                string xFile;
                if (LMP.MacPac.MacPacImplementation.IsToolkit)
                    xFile = "file:///ftGen_TaskPaneKeyboardShortcuts.html";
                else
                    xFile = "file:///Gen_TaskPaneKeyboardShortcuts.html";
                TaskPane.SetHelpText(oForm.Browser, xFile);

                oForm.Show();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// displays the specified help file
        /// </summary>
        /// <param name="xFile"></param>
        public static void ShowHelpFile(string xFile)
        {
            try
            {
                //GLOG : 3624 : CEH
                //xFile = LMP.Data.Application.GetDirectory(mpDirectories.Help) + 
                //    "\\Documents\\" + xFile;

                xFile = LMP.Data.Application.GetDirectory(mpDirectories.RootDirectory) +
                    @"\Tools\User Documentation\pdf\" + xFile;

                if (!File.Exists(xFile))
                    throw new LMP.Exceptions.FileException(
                        LMP.Resources.GetLangString("Error_CouldNotFindFile") + xFile);

                if (Path.GetExtension(xFile) == ".html" || Path.GetExtension(xFile) == ".htm")
                {
                    Help oForm = new Help("");
                    TaskPane.SetHelpText(oForm.Browser, "file:///" + xFile);
                    oForm.Show();
                }
                else
                {
                    //GLOG item #3818 - dcf
                    System.Diagnostics.Process.Start(xFile);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public static void ManagePeople()
        {
            try
            {
                PeopleManager oForm = new PeopleManager();
                oForm.ShowDialog();

                //cycle through the segments in the open document
                //and refresh all instances of author control
                if (Session.CurrentWordVersion == 11)
                {
                    //Look for first document with a Taskpane to send message to
                    foreach (Word.Document oDoc in Session.CurrentWordApp.Documents)
                    {
                        if (SendMessageToTaskPane(oDoc, mpPseudoWordEvents.AuthorListUpdated))
                            break;
                    }
                }
                else
                    RefreshCurrentAuthorControls();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

            finally
            {
                //GLOG : 8000 : ceh - set focus back to the document
                if (Session.CurrentWordApp.Documents.Count > 0) //GLOG 8327
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
        }

        public static void ConvertDBToContentControls()
        {
            if(!LMP.Data.Application.AdminMode)
            {
                MessageBox.Show("You must be logged in as administrator to convert the database.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            AdminSegmentDefs oDefs = new AdminSegmentDefs();

            int iSegs = oDefs.Count;

            DialogResult iRes = MessageBox.Show("You are about to convert the Segments table in this admin db.  There are " + iSegs.ToString() +
                " segments in Segments table.  Do you want to continue?", LMP.ComponentProperties.ProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (iRes == DialogResult.No)
                return;

            ProgressForm oForm = new ProgressForm(
                iSegs, 0, "", "Converting Admin Segments", false);

            oForm.StatusText = LMP.Resources.GetLangString("Lbl_BackingUpMacPacDatabase");
            oForm.Show();
            System.Windows.Forms.Application.DoEvents();

            string xDB = LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13
            
            //get backup db name
            int i = 1;

            while (File.Exists(xDB + ".bak" + i.ToString()))
                i++;

            string xBakDB = xDB + ".bak" + i.ToString();

            //back up db
            File.Copy(xDB, xBakDB);

            string xLog = LMP.Data.Application.PublicDataDirectory + "\\Conversion.log"; //JTS 3/28/13

            if (File.Exists(xLog))
            {
                File.Delete(xLog);
            }

            using (StreamWriter oSW = File.CreateText(xLog))
            {
                bool bBadConversions = false;

                try
                {
                    LMP.MacPac.Application.ScreenUpdating = false;
                    
                    for (int j = 1; j <= iSegs; j++)
                    {
                        LMP.MacPac.Application.ScreenUpdating = true;

                        AdminSegmentDef oDef = null;

                        try
                        {
                            oDef = (AdminSegmentDef)oDefs[j];

                            //skip segments that should not be converted
                            if (oDef.Name == "****MP10NormalTemplate****" || oDef.ID < 0 || 
                                oDef.ID == 959 || oDef.ID == 960 || oDef.ID == 961)
                                continue;

                            oForm.StatusText = j.ToString() + "/" + iSegs + ": (" + oDef.ID + ") " + oDef.DisplayName;
                            oForm.UpdateProgress(j);

                            //if (string.IsNullOrEmpty(oDef.XML) || !oDef.XML.Contains("<?xml version=\"1.0\" standalone=\"yes\"?>"))
                            if (string.IsNullOrEmpty(oDef.XML) || !oDef.XML.Contains("<?xml version=\"1.0\""))
                                {
                                oSW.WriteLine(j.ToString() + ".  SKIPPED - NOT AN XML SEGMENT - " +
                                    oDef.ID.ToString() + "- " + oDef.DisplayName);

                                continue;
                            }
                            else if (LMP.String.IsWordOpenXML(oDef.XML))
                            {
                                oSW.WriteLine(j.ToString() + ".  SKIPPED - PREVIOUSLY CONVERTED - " + 
                                    oDef.ID.ToString() + "- " + oDef.DisplayName);

                                continue;
                            }

                            LMP.MacPac.Application.ConvertToContentControls(oDef.ID.ToString(), j);

                            oSW.WriteLine(j.ToString() + ".  Converted - " + 
                                oDef.ID.ToString() + " - " + oDef.DisplayName);
                        }
                        catch(System.Exception oE)
                        {
                            bBadConversions = true;

                            oSW.WriteLine(j.ToString() + ".  FAILED**** - " + 
                                oDef.ID.ToString() + "- " + oDef.DisplayName + 
                                "\r\n" + oE.Message + "\r\n\r\n");
                        }
                    }
                }

                finally
                {
                    oForm.Close();
                    oForm.Dispose();
                    LMP.MacPac.Application.ScreenUpdating = true;
                    LMP.Forte.MSWord.GlobalMethods.CurWordApp.ScreenRefresh();
                }

                if (bBadConversions)
                {
                    MessageBox.Show("Some segments did not successfully convert. See Conversion.log for more details. The original db has been backed up to " +
                        xBakDB, LMP.ComponentProperties.ProductName,MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Conversion completed successfully. The original db has been backed up to" +
                        xBakDB, LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
        public static Segment ContentToContentControls(string xSegmentID)
        {
            return ConvertToContentControls(xSegmentID, 0);
        }

        private static Segment ConvertToContentControls(string xSegmentID, int iCounter)
        {
            string fileName = LMP.Data.Application.PublicDataDirectory + "\\OpenXML.xml"; //JTS 3/28/13
            ISegmentDef oDef = Segment.GetDefFromID(xSegmentID);

            if (LMP.String.IsWordOpenXML(oDef.XML))
            {
                return null;
            }

            string xXML = Conversion.PreconvertIfNecessary(oDef.XML, true);

            //GLOG 5112: Get current Compatiblity settings to be restored after conversion
            string xCompatOptions = Segment.GetCompatibilityOptions(xXML);

            if (oDef.TypeID == mpObjectTypes.Labels)
            {
                //content controls for label tables need
                //to include a trailing para mark to allow
                //for proper replication of label pages (copy/pasting)-
                //add trailing para mark
                xXML = xXML.Replace("</w:tbl>", "</w:tbl><w:p/>");
            }

            if (File.Exists(fileName))
                File.Delete(fileName);

            //write to file
            using (StreamWriter oSW = File.CreateText(fileName))
            {
                oSW.Write(xXML);
                oSW.Close();
            }

            bool bDisableTaskPaneEvents = LMP.MacPac.Application.DisableTaskpaneEvents;

            DisableTaskpaneEvents = true;

            Word.Options oWordOptions = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Options;

            try
            {
                bool bRSID = oWordOptions.StoreRSIDOnSave;
                oWordOptions.StoreRSIDOnSave = false;

                LMP.Forte.MSWord.GlobalMethods.CurWordApp.DefaultSaveFormat = "Doc";
                object oF = System.Reflection.Missing.Value; ;
                object oFN = fileName;
                object oFalse = false;
                Word.Document oDoc = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Documents.Open(
                    ref oFN, ref oFalse, ref oF, ref oF, ref oF, ref oF, ref oF, ref oF, ref oF,
                    ref oF, ref oF, ref oF, ref oF, ref oF, ref oF, ref oF);

                oDoc.ActiveWindow.Caption = "Segment " + iCounter.ToString();

                try
                {
                    LMP.Forte.MSWord.GlobalMethods.CurWordApp.DefaultSaveFormat = "Docx";
                    object oDocFormat = Word.WdSaveFormat.wdFormatDocument;
                    oDoc.SaveAs(ref oFN, ref oDocFormat, ref oF, ref oF, ref oFalse, ref oF, 
                        ref oF, ref oF, ref oF, ref oF, ref oF, ref oF, ref oF, ref oF, ref oF, ref oF);
                    
                    oDoc.Convert();

                    //if (oDef.TypeID != mpObjectTypes.Envelopes)
                        //RemoveSchemaIfNecessary(oDoc);

                    LMP.Forte.MSWord.Conversion oCOMConvert = new LMP.Forte.MSWord.Conversion();

                    //JTS 6/17/10: For some reason, even though document on screen appears to 
                    //have the correct default font, after saving and inserting the font may
                    //have changed (especially with pleadings).  I've used OrganizerCopy to
                    //force updating all style definitions from original XML, but it may
                    //be there's some additional massaging that needs to be done to the
                    //WordOpenXML instead.
                    LMP.Forte.MSWord.WordDoc.OrganizerCopyStyles(oDoc, fileName, null);
                    //GLOG 5112: Restore original Compatibility settings
                    LMP.Forte.MSWord.WordDoc.SetCompatiblity(oDoc, xCompatOptions);

                    oDoc.ActiveWindow.View.ShowXMLMarkup = -1;
                    oCOMConvert.AddContentControlsToBookmarkedDocument(oDoc, true,
                        false, false, true);

                    RemoveSchemaIfNecessary(oDoc);

                    xXML = oDoc.WordOpenXML;
                }
                finally
                {
                    oWordOptions.StoreRSIDOnSave = bRSID;

                    LMP.Forte.MSWord.WordApp.CloseDocument(oDoc, false);
                    File.Delete(fileName);
                }

                //GLOG 5149 (dm) - relink numbering schemes if necessary
                xXML = LMP.Conversion.RelinkNumberingSchemes(xXML);

                //prepend <mAuthorPrefs> code if the segment contains author preferences-
                if (Segment.DefinitionContainsAuthorPreferences(xXML))
                    xXML = "<mAuthorPrefs>" + xXML;

                oDef.XML = xXML;

                if (oDef is AdminSegmentDef)
                {
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    oDefs.Save((AdminSegmentDef)oDef);
                }
                else
                {
                    UserSegmentDefs oDefs = new UserSegmentDefs();
                    oDefs.Save((UserSegmentDef)oDef);
                }
            }
            finally
            {
                DisableTaskpaneEvents = bDisableTaskPaneEvents;
            }

            return null;
        }

        private static void RemoveSchemaIfNecessary(Word.Document oDoc)
        {
            //remove all macpac tags by removing macpac document scheme
            object oUrn = "urn-legalmacpac-data/10";

            Word.XMLSchemaReference oSchema = null;

            try
            {
                oSchema = oDoc.XMLSchemaReferences.get_Item(ref oUrn);
            }
            catch { }

            if (oSchema != null)
                oSchema.Delete();
        }

        /// <summary>
        /// if the document is a bookmarked MacPac document,
        /// adds either content controls or xml tags to a
        /// bookmarked document, and refreshes the task pane
        /// </summary>
        public static void AddBoundingObjectsToBookmarkedDocIfNecessary()
        {
            Word.Document oDoc = null;
            try
            {
                oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            }
            catch
            {
                return;
            }

            if (LMP.Forte.MSWord.WordDoc.IsBookmarkedForForte(oDoc) &&
                oDoc.ContentControls.Count == 0 && oDoc.XMLNodes.Count == 0)
            {
                //the document is bookmarked for MacPac - retag
                AddBoundingObjectsToBookmarkedDoc();
            }
        }

        /// <summary>
        /// adds either content controls or xml tags to a
        /// bookmarked document, and refreshes the task pane
        /// </summary>
        public static void AddBoundingObjectsToBookmarkedDoc()
        {
            DateTime t0 = DateTime.Now;
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();

            bool bTPDisabled = MacPac.Application.DisableTaskpaneEvents;
            LMP.MacPac.Application.DisableTaskpaneEvents = true;

            try
            {
                if (oDoc != null && oDoc.SaveFormat == 0)
                {
                    LMP.Forte.MSWord.WordApp.EnsureAttachedSchema();
                    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                    DateTime t1 = DateTime.Now;
                    //GLOG 6801 (dm): don't reconstitute mSEGs
                    oConvert.AddTagsToBookmarkedDocument(oDoc, false, false,
                        false, false, false);
                    LMP.Benchmarks.Print(t1);
                }
                else if (oDoc != null)
                {
                    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                    //GLOG 6801 (dm): don't reconstitute mSEGs
                    oConvert.AddContentControlsToBookmarkedDocument(oDoc, false,
                        false, false, false);
                }

                TaskPane oTP = null;

                try
                {
                    oTP = TaskPanes.Item(oDoc);
                }
                catch { }

                if (oTP != null)
                {
                    oTP.ForteDocument.RefreshTags();

                    for (int i = 0; i < oTP.ForteDocument.Segments.Count; i++)
                    {
                        oTP.ForteDocument.Segments[i].Refresh(Segment.RefreshTypes.All);
                    }
                    oTP.DocEditor.UpdateTreeNodeTags();
                }
            }
            catch (System.Exception oE)
            {
                //LMP.MacPac.Application.ScreenUpdating = true;
                LMP.Error.Show(oE);
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
                LMP.MacPac.Application.DisableTaskpaneEvents = bTPDisabled;
            }
        }
        public static void PreconvertDB()
        {
            if (!LMP.Data.Application.AdminMode)
            {
                MessageBox.Show("You must be logged in as administrator to convert the database.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string xDB = LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13

            //get backup db name
            int i = 1;

            while (File.Exists(xDB + ".bak" + i.ToString()))
                i++;

            string xBakDB = xDB + ".bak" + i.ToString();

            //back up db
            File.Copy(xDB, xBakDB);

            string xLog = LMP.Data.Application.PublicDataDirectory + "\\Conversion.log"; //JTS 3/28/13

            if (File.Exists(xLog))
            {
                File.Delete(xLog);
            }
            using (StreamWriter oSW = File.CreateText(xLog))
            {
                AdminSegmentDefs oDefs = new AdminSegmentDefs();

                int iSegs = oDefs.Count;
                bool bBadConversions = false;

                DialogResult iRes = MessageBox.Show("You are about to convert the Segments table in this admin db.  There are " + iSegs.ToString() +
                    " segments in Segments table.  Do you want to continue?", LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (iRes == DialogResult.No)
                    return;

                try
                {
                    LMP.MacPac.Application.ScreenUpdating = false;

                    for (int j = 1; j <= iSegs; j++)
                    {
                        AdminSegmentDef oDef = null;

                        try
                        {
                            oDef = (AdminSegmentDef)oDefs[j];

                            //skip built-in segments
                            //12-8-10 (dm) - built-in collections need to be preconverted -
                            //skip only the Word 2003 ones
                            if (oDef.Name == "****MP10NormalTemplate****" ||
                                oDef.ID < -3)
                                continue;

                            //oForm.ProgressLabelText = "Updating " + oDef.DisplayName;

                            if (string.IsNullOrEmpty(oDef.XML) || !oDef.XML.Contains("<?xml version=\"1.0\" standalone=\"yes\"?>"))
                            {
                                oSW.WriteLine(j.ToString() + ".  SKIPPED - NOT AN XML SEGMENT - " +
                                    oDef.ID.ToString() + "- " + oDef.DisplayName);

                                continue;
                            }
                            else if (LMP.String.IsWordOpenXML(oDef.XML))
                            {
                                oSW.WriteLine(j.ToString() + ".  SKIPPED - PREVIOUSLY CONVERTED - " +
                                    oDef.ID.ToString() + "- " + oDef.DisplayName);

                                continue;
                            }

                            LMP.MacPac.Application.PreconvertIfNecessary(oDef.ID.ToString());

                            oSW.WriteLine(j.ToString() + ".  Converted - " +
                                oDef.ID.ToString() + " - " + oDef.DisplayName);
                        }
                        catch (System.Exception oE)
                        {
                            bBadConversions = true;

                            oSW.WriteLine(j.ToString() + ".  FAILED**** - " +
                                oDef.ID.ToString() + "- " + oDef.DisplayName +
                                "\r\n" + oE.Message + "\r\n\r\n");
                        }
                    }
                }

                finally
                {
                    LMP.MacPac.Application.ScreenUpdating = true;
                    //oForm.Close();
                    //oForm.Dispose();
                }

                if (bBadConversions)
                {
                    MessageBox.Show("Some segments did not successfully convert. See Conversion.log for more details. The original db has been backed up to " +
                        xBakDB, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Conversion completed successfully. The original db has been backed up to" +
                        xBakDB, LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
        public static void CreateInterrogatories()
        {
            LMP.Forte.MSWord.Interrogatories oRogs = new LMP.Forte.MSWord.Interrogatories();

            string xPrevValues = oRogs.PreviousValues(
                LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Document);

            InterrogatoriesForm oForm = new InterrogatoriesForm(xPrevValues);
            DialogResult iRes = oForm.ShowDialog();

            if (iRes == DialogResult.OK)
            {
                oRogs.AlternateHeadingsAndResponses = oForm.AlternateQuestionsAndResponses;
                oRogs.UseFieldCodes = oForm.InsertAsFieldCodes;
                oRogs.StartNumber = (short)oForm.StartNumber;
                oRogs.EndNumber = (short)oForm.EndNumber;
                oRogs.RogHeading = oForm.HeadingText;
                oRogs.RogResponseHeading = oForm.ResponseHeadingText;
                oRogs.RogHeadingSeparation = oForm.HeadingSeparator;
                oRogs.HeadingLeftMargin = oForm.HeadingIndent;
                oRogs.NextParaLeftMargin = oForm.NextParagraphIndent;
                oRogs.Bold = oForm.Bold;
                oRogs.Underline = oForm.Underline;
                oRogs.AllCaps = oForm.AllCaps;
                oRogs.SmallCaps = oForm.SmallCaps;
                oRogs.FormatType = oForm.FormatType;
                oRogs.RepeatPreviousHeading = oForm.RepeatPreviousNumber;
                oRogs.UsesPlaceholder = oForm.UsesPlaceholder;
                oRogs.DefaultPlaceholder = InterrogatoriesForm.DefaultPlaceholder;
                oRogs.ShowAnswerNumber = oForm.ShowAnswerNumber;  //GLOG : 6746 : ceh
                oRogs.IsSelectedResponse = oForm.ResponseSelected;  //GLOG : 6746 : ceh

                if (oForm.HasCustomHeadings)
                {
                    oRogs.CustomQuestionHeading = oForm.Interrogatories[0];
                    oRogs.CustomResponseHeading = oForm.Interrogatories[1];
                }

                oRogs.StyleDefinitions = GetRogStyleDefinitions();

                //GLOG : 7300 : CEH - Make sure COM function doesn't trigger XML Event
                SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                                      mpPseudoWordEvents.SuppressXMLEvents);

                int iShowTags = Session.CurrentWordApp.ActiveDocument.ActiveWindow.View.ShowXMLMarkup;
                if (iShowTags == 0)
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.View.ShowXMLMarkup = -1;

                try
                {
                    oRogs.Create(LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Range);
                }
                finally
                {
                    //GLOG : 7300 : CEH - Restore event-handling
                    LMP.MacPac.Application.SetXMLTagStateForEditing();
                    SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                                          mpPseudoWordEvents.ResumeXMLEvents);

                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.View.ShowXMLMarkup = iShowTags;
                }
            }
            else if(iRes == DialogResult.Abort)
            {
                //GLOG item #5634 - update field codes was pressed - 
                //this code is here, not in the button because
                //it is the only way to get the cursor to flash in
                //in the document after the dialog is released - dcf
                LMP.MacPac.Session.CurrentWordApp.ActiveDocument.Fields.Update();
            }
            else
            {
                //GLOG : 8000 : ceh - set focus back to the document
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
        }

        internal static string GetRogStyleDefinitions()
        {
            //get style definitions-
            //get both firm and user defs, then
            //use the user defs if they exist-
            //otherwise, use the firm defs
            string xFirmStyleDefs = (new LMP.Data.FirmApplicationSettings(LMP.MacPac.Session.CurrentUser.ID)).InterrogatoryStyleDefs;
            string[] aFirmStyleDefs = xFirmStyleDefs.Split(';');

            string xUserStyleDefs = LMP.MacPac.Session.CurrentUser.UserSettings.InterrogatoryStyles;

            string[] xStyleDefs = xUserStyleDefs.Split(';');

            if (string.IsNullOrEmpty(xUserStyleDefs))
            {
                return xFirmStyleDefs;
            }
            else
            {
                for (int i = 0; i < xStyleDefs.Length; i++)
                {
                    if (xStyleDefs[i] == "NOT DEFINED")
                        xStyleDefs[i] = aFirmStyleDefs[i];
                }

                return string.Join(";", xStyleDefs);
            }
        }

        private static void PreconvertIfNecessary(string xSegmentID)
        {
            ISegmentDef oDef = Segment.GetDefFromID(xSegmentID);

            string xXML = oDef.XML;

            XmlDocument oXMLDoc = new XmlDocument();
            XmlNamespaceManager oNSMan = new XmlNamespaceManager(oXMLDoc.NameTable);
            oXMLDoc.LoadXml(xXML);

            if (LMP.String.IsWordOpenXML(xXML))
            {
                oNSMan.AddNamespace("w", LMP.Data.ForteConstants.WordOpenXMLNamespace);

                //exit if there are already conversion bookmarks
                if (oXMLDoc.SelectSingleNode("//w:bookmarkStart[starts-with(@w:name, \"_mp\")]", oNSMan) != null)
                    return;
            }
            else
            {
                oNSMan.AddNamespace("w", LMP.Data.ForteConstants.WordNamespace);
                oNSMan.AddNamespace("aml", LMP.Data.ForteConstants.AmlNamespace);

                //exit if there are already conversion bookmarks
                if (oXMLDoc.SelectSingleNode("//aml:annotation[starts-with(@w:name, \"_mp\")]", oNSMan) != null)
                    return;
            }

            xXML = Conversion.PreconvertIfNecessary(xXML, false, xSegmentID);

            //prepend <mAuthorPrefs> code if the segment contains author preferences-
            if (Segment.DefinitionContainsAuthorPreferences(xXML))
                xXML = "<mAuthorPrefs>" + xXML;

            oDef.XML = xXML;

            if (oDef is AdminSegmentDef)
            {
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                oDefs.Save((AdminSegmentDef)oDef);
            }
            else
            {
                UserSegmentDefs oDefs = new UserSegmentDefs();
                oDefs.Save((UserSegmentDef)oDef);
            }
        }

        /// <summary>
        /// convert user segments to content controls
        ///if working in Word Open XML format and
        ///user segments have not yet been converted
        /// </summary>
        public static bool ConvertUserSegmentsIfNecessary()
        {
            if (LMP.MacPac.Session.AdminMode)
                return false;

            string xFileFormat = LMP.Forte.MSWord.GlobalMethods.CurWordApp.DefaultSaveFormat.ToUpper();

            //GLOG 5500: DefaultFormat may be set to single space if Group Policy is set
            //Treat any setting other than "Doc" the same as "Docx"
            if ((xFileFormat != "DOC") &&
                !LMP.MacPac.Session.CurrentUser.UserSettings.UserSegmentsConverted)
            {
                //check that there are user segments not in the open xml format

                //convert all user segments to use content controls
                string xLog = LMP.Data.Application.PublicDataDirectory + "\\Conversion.log"; //JTS 3/28/13

                if (File.Exists(xLog))
                {
                    File.Delete(xLog);
                }
                using (StreamWriter oSW = File.CreateText(xLog))
                {
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner,
                        LMP.MacPac.Session.CurrentUser.ID);

                    int iSegs = oDefs.Count;
                    bool bBadConversions = false;

                    if (iSegs == 0)
                        return false;

                    ProgressForm oForm = new ProgressForm(
                        iSegs, 0, "", LMP.Resources.GetLangString("Lbl_ConvertingUserSegments"), false);

                    oForm.StatusText = LMP.Resources.GetLangString("Lbl_BackingUpMacPacDatabase");
                    oForm.Show();
                    System.Windows.Forms.Application.DoEvents();

                    string xDB = LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName; //JTS 3/28/13

                    //get backup db name
                    int i = 1;

                    while (File.Exists(xDB + ".bak" + i.ToString()))
                        i++;

                    string xBakDB = xDB + ".bak" + i.ToString();

                    //back up db
                    File.Copy(xDB, xBakDB);

                    try
                    {

                        for (int j = 1; j <= iSegs; j++)
                        {
                            LMP.MacPac.Application.ScreenUpdating = true;

                            oForm.StatusText = j.ToString() + "/" + iSegs;
                            oForm.UpdateProgress(j);

                            UserSegmentDef oDef = null;

                            try
                            {
                                oDef = (UserSegmentDef)oDefs[j];

                                if (string.IsNullOrEmpty(oDef.XML) ||
                                    !oDef.XML.Contains("<?xml version=\"1.0\" standalone=\"yes\"?>"))
                                {
                                    oSW.WriteLine(j.ToString() + ".  SKIPPED - NOT AN XML SEGMENT - " +
                                        oDef.ID.ToString() + "- " + oDef.DisplayName);

                                    continue;
                                }
                                else if (LMP.String.IsWordOpenXML(oDef.XML))
                                {
                                    oSW.WriteLine(j.ToString() + ".  SKIPPED - PREVIOUSLY CONVERTED - " +
                                        oDef.ID.ToString() + "- " + oDef.DisplayName);

                                    continue;
                                }

                                LMP.MacPac.Application.ConvertToContentControls(oDef.ID, j);

                                oSW.WriteLine(j.ToString() + ".  Converted - " +
                                    oDef.ID.ToString() + " - " + oDef.DisplayName);
                            }
                            catch (System.Exception oE)
                            {
                                bBadConversions = true;

                                oSW.WriteLine(j.ToString() + ".  FAILED**** - " +
                                    oDef.ID.ToString() + "- " + oDef.DisplayName +
                                    "\r\n" + oE.Message + "\r\n\r\n");
                            }
                        }

                        oSW.Close();

                        if (!bBadConversions)
                            LMP.MacPac.Session.CurrentUser.UserSettings.UserSegmentsConverted = true;
                    }

                    finally
                    {
                        oForm.Close();
                        oForm.Dispose();
                        LMP.MacPac.Application.ScreenUpdating = true;
                        LMP.Forte.MSWord.GlobalMethods.CurWordApp.ScreenRefresh();
                    }

                    if (bBadConversions)
                    {
                        MessageBox.Show(
                            string.Format(LMP.Resources.GetLangString("Msg_UserSegmentConversionFailed"),
                            xBakDB), LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_UserSegmentConversionFinished"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        File.Delete(xBakDB);
                        File.Delete(xLog);
                    }
                }

                if(LMP.Forte.MSWord.GlobalMethods.CurWordApp.Documents.Count > 0)
                    InvalidateMacPacRibbon();

                return true;
            }
            else
                return false;
        }
        ///<summary>
        /// this method will refresh all instances of the Author Selector
        /// in all segments in all open documents
        /// </summary>
        public static void RefreshCurrentAuthorControls()
        {
            try
            {
                //get all the current task panes
                List<LMP.MacPac.TaskPane> oTP = CurrentTaskPanes();

                //for each task panes instance of DocumentEditor, run
                //the RefreshAuthorControls method
                for (int i = 0; i < oTP.Count; i++)
                    if (oTP[i].DocEditor != null)
                        oTP[i].DocEditor.RefreshAuthorControls();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// returns list of all task panes from all open documents
        /// </summary>
        /// <returns></returns>
        internal static List<LMP.MacPac.TaskPane> CurrentTaskPanes()
        {
            Word.Global oWord = new Word.Global();
            List<LMP.MacPac.TaskPane> oTP = new List<TaskPane>();
            foreach (Word.Document oDoc in oWord.Documents)
            {
                if (TaskPanes.Item(oDoc) != null)
                    oTP.Add(TaskPanes.Item(oDoc));
            }
            return oTP;
        }
        public static TaskPane ActiveTaskPane //GLOG 7814
        {
            set {m_oActiveTaskPane = value;}
            get {return m_oActiveTaskPane;}
        }
        public static void ManageUserAppSettings()
        {
            try
            {
                UserPreferencesForm oForm = new UserPreferencesForm();
                oForm.ShowDialog();

                System.Windows.Forms.Application.DoEvents();

                //update all task panes with application changes
                TaskPanes.RefreshAppearance();

                // Store the Quick Access Segments
                FavoriteSegments.Save();

                // Invalidate the ribbon to update the favorite segments selection.
                InvalidateMacPacRibbon();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

            finally
            {
                //GLOG : 8000 : ceh - set focus back to the document
                if (Session.CurrentWordApp.Documents.Count > 0) //GLOG 8327
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
        }
        private static void InvalidateMacPacRibbon()
        {
            if (MacPac.Application.MacPac10Ribbon != null)
            {
                MacPac.Application.MacPac10Ribbon.Invalidate();
            }
        }

        /// <summary>
        /// creates envelopes using the data in the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns>TRUE if inserted</returns>
        public static bool CreateEnvelopes(Segment oSegment)
        {
            //GLOG 7795 (dm) - added return value
            return CreateEnvelopes(oSegment, null);
        }

        /// <summary>
        /// creates envelopes using the data in the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oMethod"></param>
        /// <returns>TRUE if inserted</returns>
        public static bool CreateEnvelopes(Segment oSegment, ShowTaskPaneDelegate oMethod)
        {
            //GLOG 7795 (dm) - added return value
            try
            {
                bool bInserted = false;

                SegmentInsertionForm oForm = new SegmentInsertionForm(mpObjectTypes.Envelopes, false);

                //get prefill using analyzer
                Word.Selection oSel = Session.CurrentWordApp.Selection;
                Prefill oPrefill = null;

                //GLOG 5942 (jw) - if the cursor is in an empty table cell,
                //or end of row marker, Selection.Text will be "\r\a", even though
                //nothing is selected             
                if (!string.IsNullOrEmpty(oSel.Text) && oSel.Text.Length > 1 && oSel.Text != "\r\a")
                {
                    DocAnalyzer.DocType oDocType = new LMP.DocAnalyzer.DocType("RecipientList", oSel);

                    string xRecipients = oDocType.AnalyzeDocument(
                        LMP.DocAnalyzer.DocType.OutputFormats.DelimitedString, false, false);

                    if (xRecipients == null)
                        return bInserted;

                    //GLOG 3475: Get related Segment ID for Prefill
                    //GLOG 7928 (dm) - segment of unsupported type won't have
                    //definition in Forte Tools
                    string xSegID = "";
                    if ((oSegment == null) || !oSegment.HasDefinition)
                    {
                        xSegID = oForm.SegmentDef.ID.ToString();
                    }
                    else
                    {
                        if (oSegment.TypeID == mpObjectTypes.UserSegment)
                            xSegID = ((UserSegmentDef)oSegment.Definition).ID;
                        else
                            xSegID = ((AdminSegmentDef)oSegment.Definition).ID.ToString();
                    }

                    oPrefill = new Prefill(xRecipients, "Recipients", xSegID);
                }
                else if(oSegment != null)
                    oPrefill = oSegment.CreatePrefill();

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    //System.Windows.Forms.Application.DoEvents();
                    //GLOG 4040: Call TaskPane functions to insert                    
                    if (oForm.InsertionLocation == Segment.InsertionLocations.InsertInNewDocument)
                    {
                        TaskPane.InsertSegmentInNewDoc(oForm.SegmentDef.ID.ToString(),
                            oPrefill, null);
                    }
                    else
                    {
                        TaskPane oTP = null;

                        if (LMP.MacPac.Application.TaskPaneExists(oSel.Document))
                        {
                            oTP = TaskPanes.Item(oSel.Document);
                        }
                        else if (oMethod != null)
                        {
                            oTP = oMethod(oSel.Document);
                        }
                        else
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_ShowTaskPane"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return bInserted;
                        }


                        Segment oSeg = oTP.InsertSegmentInCurrentDoc(oForm.SegmentDef.ID.ToString(), oPrefill,
                            null, oForm.InsertionLocation, oForm.InsertionBehavior);

                        oTP.SelectEditorFirstNodeOfSegment(oSeg);
                    }

                    bInserted = true;
                }
                else
                {
                    //GLOG : 8000 : ceh - set focus back to the document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }

                return bInserted;
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// creates labels using the data in the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns>TRUE if inserted</returns>
        public static bool CreateLabels(Segment oSegment)
        {
            //GLOG 7795 (dm) - added return value
            return CreateLabels(oSegment, null);
        }

        /// <summary>
        /// creates labels using the data in the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oMethod"></param>
        /// <returns>TRUE if inserted</returns>
        public static bool CreateLabels(Segment oSegment, ShowTaskPaneDelegate oMethod)
        {
            //GLOG item #6399 - added delegate parameter - dcf
            //GLOG 7795 (dm) - added return value
            try
            {
                bool bInserted = false;
                SegmentInsertionForm oForm = new SegmentInsertionForm(mpObjectTypes.Labels, false);
                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    LMP.MacPac.Application.ScreenUpdating = false;

                    //get prefill using analyzer
                    Word.Selection oSel = Session.CurrentWordApp.Selection;
                    Prefill oPrefill = null;

                    //GLOG 5030 (dm) - oSel.Text will be null when the cursor
                    //is in an empty content control
                    //GLOG 5942 (jw) - if the cursor is in an empty table cell,
                    //or end of row marker, xText will be "\r\a", even though
                    //nothing is selected
                    string xText = oSel.Text;
                    if ((xText != null) && (xText.Length > 1) && (xText != "\r\a"))
                    {
                        DocAnalyzer.DocType oDocType = new LMP.DocAnalyzer.DocType("RecipientList", oSel);

                        string xRecipients = oDocType.AnalyzeDocument(
                            LMP.DocAnalyzer.DocType.OutputFormats.DelimitedString, false, false);

                        if (xRecipients == null)
                            return bInserted;

                        //GLOG 3475: Get related Segment ID for Prefill
                        //GLOG 7928 (dm) - segment of unsupported type won't have
                        //definition in Forte Tools
                        string xSegID = "";
                        if ((oSegment == null) || !oSegment.HasDefinition)
                        {
                            xSegID = oForm.SegmentDef.ID.ToString();
                        }
                        else
                        {
                            if (oSegment.TypeID == mpObjectTypes.UserSegment)
                                xSegID = ((UserSegmentDef)oSegment.Definition).ID;
                            else
                                xSegID = ((AdminSegmentDef)oSegment.Definition).ID.ToString();
                        }

                        oPrefill = new Prefill(xRecipients, "Recipients", xSegID);
                    }
                    else if(oSegment != null)
                        oPrefill = oSegment.CreatePrefill();

                    //GLOG 4040: Call TaskPane functions to insert                    
                    if (oForm.InsertionLocation == Segment.InsertionLocations.InsertInNewDocument)
                    {
                        TaskPane.InsertSegmentInNewDoc(oForm.SegmentDef.ID.ToString(),
                            oPrefill, null);
                    }
                    else
                    {
                        TaskPane oTP = null;

                        if (LMP.MacPac.Application.TaskPaneExists(oSel.Document))
                        {
                            oTP = TaskPanes.Item(oSel.Document);
                        }
                        else if (oMethod != null)
                        {
                            oTP = oMethod(oSel.Document);
                        }
                        else
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_ShowTaskPane"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return bInserted;
                        }

                        oTP.InsertSegmentInCurrentDoc(oForm.SegmentDef.ID.ToString(), oPrefill,
                            null, oForm.InsertionLocation, oForm.InsertionBehavior);
                    }
                    bInserted = true;
                }
                else
                {
                    //GLOG : 8000 : ceh - set focus back to the document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }

                return bInserted;
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// removes the bounding objects in the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static bool RemoveBoundingObjects(Word.Document oDoc, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType)
        {
            return RemoveBoundingObjects(oDoc, iRemovalType, true);
        }

        public static bool RemoveBoundingObjects(Word.Document oDoc, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType, bool bPrompt)
        {
            TaskPane oTP = null;
            LMP.Architect.Api.ForteDocument oMPDocument = null;
            //GLOG 7887: Set to current state rather than 0
            LMP.Architect.Api.ForteDocument.WordXMLEvents iIgnoreXMLEvents = ForteDocument.IgnoreWordXMLEvents;

            try
            {
                try
                {
                    oTP = LMP.MacPac.TaskPanes.Item(oDoc);
                }
                catch { }

                if (oTP != null)
                {
                    oMPDocument = oTP.ForteDocument;
                    ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                }
                else
                {
                    oMPDocument = new ForteDocument(oDoc);
                    ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                    //GLOG : 8102 : ceh - don't prompt for missing authors in Tools
                    oMPDocument.Refresh(ForteDocument.RefreshOptions.None, 
                                        true, 
                                        !(LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator));
                }

                if (oMPDocument.FileFormat == mpFileFormats.OpenXML)
                {
                    //GLOG 6138 (dm) - in Word 2007, Remove All Automation should
                    //remove xml tags as well - the simplest way to do this without
                    //impacting content control removal or prompting is to reconstitute
                    if ((iRemovalType == LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All) &&
                        !LMP.Forte.MSWord.WordApp.IsPostInjunctionWordVersion())
                    {
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                        bool bReconstituted = oConvert.ReconstituteIfNecessary(oDoc, false, true);
                        if (bReconstituted)
                        {
                            oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes,
                                false, false);
                        }
                    }

                   return RemoveContentControls(oMPDocument, iRemovalType, bPrompt);
                }
                else
                {
                    //10-3-11 (dm) - include bPrompt argument
                    //GLOG 5770 (dm) - we were returning true regardless of how
                    //user responded to removal prompt
                    return RemoveTags(oMPDocument, iRemovalType, bPrompt);
                }

            }
            finally
            {
                //GLOG 7054 (dm) - changed the parameter from RefreshOptions.None so
                //that RefreshTags will be called
                //GLOG : 8102 : ceh - don't prompt for missing authors in Tools
                oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes, 
									true, 
									!(LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator));
                ForteDocument.IgnoreWordXMLEvents = iIgnoreXMLEvents;
            }
        }
        /// <summary>
        /// removes the bounding objects in the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        public static bool RemoveBoundingObjects(Segment oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType)
        {
            return RemoveBoundingObjects(oSegment, iRemovalType, true);
        }
        public static bool RemoveBoundingObjects(Segment oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType, bool bPrompt)
        {
            bool bRet = false;

            ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;

            if (oSegment.ForteDocument.FileFormat == mpFileFormats.OpenXML)
            {
                bRet = RemoveContentControls(oSegment, iRemovalType, bPrompt);
            }
            else
            {
                RemoveTags(oSegment, iRemovalType);
                bRet = true;
            }

            TaskPane oTP = null;

            try
            {
                oTP = LMP.MacPac.TaskPanes.Item(oSegment.ForteDocument.WordDocument);
            }
            catch { }

            if (oTP != null)
            {
                oTP.Refresh(false, true, false);
            }

            return bRet;
        }

        /// <summary>
        /// puts the specified Word document in a "Finished" state -
        /// i.e. strips all non-mSEG bounding objects
        /// </summary>
        /// <param name="oDoc"></param>
        public static void FinishDocument(Word.Document oDoc, bool bSkipPreAndPostFinish)
        {
            ForteDocument oMPDocument = new ForteDocument(oDoc);
            FinishDocument(oMPDocument, true, bSkipPreAndPostFinish);
        }

        public static bool FinishDocument(ForteDocument oMPDocument, bool bRefreshMacPacDocument)
        {
            return FinishDocument(oMPDocument, bRefreshMacPacDocument, false);
        }

        /// <summary>
        /// puts all segments in the specified MacPac document
        /// in a "Finished" state - i.e. strips all non-mSEG
        /// bounding objects
        /// GLOG 7916 (dm) - argument to skip pre and post finish
        /// </summary>
        /// <param name="oMPDocument"></param>
        /// <param name="bRefreshMacPacDocument"></param>
        /// <param name="bSkipPreAndPostFinish"></param>
        /// <returns></returns>
        public static bool FinishDocument(ForteDocument oMPDocument, bool bRefreshMacPacDocument,
            bool bSkipPreAndPostFinish)
        {
            DateTime t0 = DateTime.Now;

            Segment oSeg = null;

            //GLOG 8007 - refresh documents converted since opening in Word 2013 -
            //this will trigger reinsertion of textboxes to prevent access issues
            if (!bRefreshMacPacDocument)
                bRefreshMacPacDocument = LMP.Forte.MSWord.WordDoc.IsConvertedInWord2013(oMPDocument.WordDocument);

            if (bRefreshMacPacDocument)
            {
                oMPDocument.Refresh(ForteDocument.RefreshOptions.None, false, false);
            }

            if (oMPDocument.Segments.Count == 0)
                return true;

            //GLOG 7259 (dm) - toolkit should finish only supported types
            if (LMP.MacPac.MacPacImplementation.IsToolkit)
            {
                LMP.MacPac.Application.ScreenUpdating = false;
                //GLOG 7887: Prevent TaskPane events for MP10 content
                LMP.MacPac.Application.DisableTaskpaneEvents = true;
                try
                {
                    for (int i = 0; i < oMPDocument.Segments.Count; i++)
                    {
                        oSeg = oMPDocument.Segments[i];

                        //GLOG 15860 (dm) - skip static segments
                        if (LMP.Data.Application.DisplayInToolkitTaskPane(oSeg.TypeID) &&
                            (bSkipPreAndPostFinish || !(oSeg is LMP.Architect.Base.IStaticDistributedSegment)))
                            FinishSegment(oSeg, bRefreshMacPacDocument, bSkipPreAndPostFinish);
                    }
                }
                catch { }
                finally
                {
                    LMP.MacPac.Application.DisableTaskpaneEvents = false; //GLOG 7912 (dm)
                    LMP.MacPac.Application.ScreenUpdating = true;
                }
                return true;
            }

            if (!bSkipPreAndPostFinish && !oMPDocument.Segments.PrepareForFinish()) //GLOG 7916 (dm)
                return false;

            //GLOG : 6641 : jsw
            //warning prompt
            //Confirm that the user intends to finish this document.
            if (Session.CurrentUser.UserSettings.WarnOnFinish == true)
            {
                DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_FinishDocument"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oChoice == DialogResult.No)
                    return false;
            }
            //GLOG 7887: Minimize screen and TaskPane updates
            bool bScreenUpdating = LMP.MacPac.Application.ScreenUpdating;
            LMP.MacPac.Application.ScreenUpdating = false;
            bool bDisableEvents = LMP.MacPac.Application.DisableTaskpaneEvents;
            LMP.MacPac.Application.DisableTaskpaneEvents = true;
            ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

            try
            {
                //GLOG 6060 (dm) - "finish" trailers first
                FinishTrailers(oMPDocument);

                //re-bookmark document, as manual edits may have "shifted" bookmarks
                LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                oConvert.AddBookmarks(oMPDocument.WordDocument, false);
                LMP.MacPac.Application.ScreenUpdating = false;

                //GLOG 15860 (dm) - do only if finishing on open
                if (bSkipPreAndPostFinish)
                {
                    for (int i = oMPDocument.Segments.Count - 1; i >= 0; i--)
                    {
                        if (oMPDocument.Segments[i] is LMP.Architect.Base.IStaticDistributedSegment)
                            FinishSegment(oMPDocument.Segments[i], bRefreshMacPacDocument, bSkipPreAndPostFinish);
                    }
                }

                //create a snapshot of the ForteDocument
                oMPDocument.CreateSegmentSnapshots();

                //strip all non-mSEG bounding objects
                //GLOG 15860 (dm) - need to do for each individual segment in order to skip static segments
                for (int i = oMPDocument.Segments.Count - 1; i >= 0; i--)
                {
                    oSeg = oMPDocument.Segments[i];
                    if (bSkipPreAndPostFinish || !(oSeg is LMP.Architect.Base.IStaticDistributedSegment))
                    {
                        if (MacPac.Session.CurrentUser.FirmSettings.DeleteTextSegmentTagsOnFinish)
                        {
                            LMP.MacPac.Application.RemoveBoundingObjects(oSeg,
                                LMP.Forte.MSWord.mpSegmentTagRemovalOptions.TextSegmentsOnly, false);
                            //LMP.MacPac.Application.RemoveBoundingObjects(oMPDocument.WordDocument,
                            //    LMP.Forte.MSWord.mpSegmentTagRemovalOptions.TextSegmentsOnly, false);
                        }
                        else
                        {
                            LMP.MacPac.Application.RemoveBoundingObjects(oSeg,
                                LMP.Forte.MSWord.mpSegmentTagRemovalOptions.None, false);
                            //LMP.MacPac.Application.RemoveBoundingObjects(oMPDocument.WordDocument,
                            //    LMP.Forte.MSWord.mpSegmentTagRemovalOptions.None, false);
                        }
                    }
                }

                //GLOG 7916 (dm) - if skipping post-finish, labels and envelopes
                //need to be deautomated here
                if (bSkipPreAndPostFinish)
                {
                    for (int i = 0; i < oMPDocument.Segments.Count; i++)
                    {
                        if (oMPDocument.Segments[i] is LMP.Architect.Base.IStaticDistributedSegment)
                        {
                            foreach (Word.Bookmark oBmk in oMPDocument.Segments[i].Bookmarks)
                            {
                                LMP.Forte.MSWord.WordDoc.DeleteAssociatedDocVars_Bmk(oBmk.Range, null, null, null);
                                object oRef = oBmk;
                                oMPDocument.WordDocument.Bookmarks[ref oRef].Delete();
                            }
                        }
                    }
                }

                if (bRefreshMacPacDocument)
                {
                    oMPDocument.RefreshTags();
                    oMPDocument.Refresh(ForteDocument.RefreshOptions.None, false, false);
                }

                //8-18-11 (dm) - delete tagless variables
                for (int i = 0; i < oMPDocument.Segments.Count; i++)
                {
                    //GLOG 15860 (dm) - skip static segments
                    oSeg = oMPDocument.Segments[i];
                    if (bSkipPreAndPostFinish || !(oSeg is LMP.Architect.Base.IStaticDistributedSegment))
                        oSeg.DeleteVariables(true);
                }

                if (!bSkipPreAndPostFinish && !oMPDocument.Segments.ExecutePostFinish()) //GLOG 7916 (dm)
                {
                    return false;
                }
                LMP.Benchmarks.Print(t0);
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = bScreenUpdating;
                LMP.MacPac.Application.DisableTaskpaneEvents = bDisableEvents;
                ForteDocument.IgnoreWordXMLEvents = iIgnore;
            }
            return true;
        }

        /// <summary>
        /// puts all segments in the specified MacPac document
        /// in a "Finished" state - i.e. strips all non-mSEG
        /// bounding objects
        /// GLOG 7916 (dm) - argument to skip pre and post finish
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="bRefreshMacPacDocument"></param>
        /// <param name="bSkipPreAndPostFinish"></param>
        public static void FinishSegment(Segment oSegment, bool bRefreshMacPacDocument,
            bool bSkipPreAndPostFinish)
        {
            DateTime t0 = DateTime.Now;

            ForteDocument oMPDocument = oSegment.ForteDocument;

            //call any segment-specific finish code
            //GLOG 7259 (dm) - this conditional was backwards
            //GLOG 7916 (dm) - added option to not run pre and post finish
            if (!bSkipPreAndPostFinish)
            {
                if (oSegment is LMP.Architect.Base.IStaticDistributedSegment)
                {
                    //Use Oxml to Create Finished segment
                    XmlPrefill oPrefill = new XmlPrefill(new Prefill(oSegment));
                    XmlSegment oXmlSegment = null;
                    oXmlSegment = LMP.Architect.Oxml.Word.XmlSegmentInsertion.Create(oSegment.ID, null, null, oPrefill, "", null, oSegment.WordTemplate, XmlSegment.OxmlCreationMode.StaticFinish);
                    Word.Range oRng = oSegment.PrimaryBookmark.Range;
                    oRng.Collapse();
                    oMPDocument.DeleteSegment(oSegment, false, true, false);
                    XmlForteDocument.FinishStaticSegment(oXmlSegment);
                    if (!LMP.Forte.MSWord.WordDoc.DocumentIsEmpty(oMPDocument.WordDocument))
                    {
                        //If document has existing content, insert targeted to include headers/footers
                        string xXML = oXmlSegment.FlatOxml;
                        oSegment.InsertXML(xXML, null, oRng, Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Replace, true, mpSegmentIntendedUses.AsDocument);
                    }
                    else
                    {
                        LMP.Forte.MSWord.WordDoc.ClearHeaders(oRng.Sections[1]);
                        LMP.Forte.MSWord.WordDoc.ClearFooters(oRng.Sections[1]);
                        LMP.Architect.Oxml.Word.XmlSegmentInsertion.InsertAtRange(oXmlSegment, oRng);
                    }
                    //oRng.Select();
                    return;
                }
                else if (!oSegment.PrepareForFinish())
                {
                    return;
                }
            }
            if (bRefreshMacPacDocument)
            {
                oMPDocument.Refresh(ForteDocument.RefreshOptions.None, false, false);
            }

            if (oMPDocument.Segments.Count == 0)
                return;

            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

            //create a snapshot of the ForteDocument
            //GLOG 7874: Don't create snapshot variables for Labels and Envelopes
            if (!(oSegment is LMP.Architect.Base.IStaticDistributedSegment))
                Snapshot.Create(oSegment);

            //8-18-11 (dm) - delete tagless variables
            //GLOG 7259 (dm) - set bTaglessOnly to true
            //GLOG 7259 (dm, 10/29/13) - moved to before removal of bounding objects,
            //since oSegment will no longer be current after refresh
            oSegment.DeleteVariables(true);

            //strip all non-mSEG bounding objects
            if (MacPac.Session.CurrentUser.FirmSettings.DeleteTextSegmentTagsOnFinish)
            {
                LMP.MacPac.Application.RemoveBoundingObjects(oSegment,
                    LMP.Forte.MSWord.mpSegmentTagRemovalOptions.TextSegmentsOnly, false);
            }
            else
            {
                LMP.MacPac.Application.RemoveBoundingObjects(oSegment,
                    LMP.Forte.MSWord.mpSegmentTagRemovalOptions.None, false);
            }

            //GLOG 7916 (dm) - if skipping post-finish, labels and envelopes
            //need to be deautomated here
            if (bSkipPreAndPostFinish)
            {
                if (oSegment is LMP.Architect.Base.IStaticDistributedSegment)
                {
                    foreach (Word.Bookmark oBmk in oSegment.Bookmarks)
                    {
                        LMP.Forte.MSWord.WordDoc.DeleteAssociatedDocVars_Bmk(oBmk.Range, null, null, null);
                        object oRef = oBmk;
                        oMPDocument.WordDocument.Bookmarks[ref oRef].Delete();
                    }
                }
            }

            if (bRefreshMacPacDocument)
            {
                oMPDocument.RefreshTags();
                oMPDocument.Refresh(ForteDocument.RefreshOptions.None, false, false);
            }

            //GLOG item #7629 - dcf
            if (!bSkipPreAndPostFinish) //GLOG 7916 (dm)
            {
                try
                {
                    oSegment.ExecutePostFinish();
                }
                finally
                {
                    LMP.MacPac.Application.ScreenUpdating = true;
                }
            }

            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// "finishes" trailers by deleting bounding objects and
        /// setting last trailer doc var
        /// </summary>
        /// <param name="oMPDocument"></param>
        private static void FinishTrailers(ForteDocument oMPDocument)
        {
            //GLOG 6151 (dm) - turn off screen updating
            LMP.MacPac.Application.ScreenUpdating = false;
            try
            {
                bool bDocVarSet = false;
                for (int i = oMPDocument.Segments.Count - 1; i >= 0; i--)
                {
                    Segment oSegment = oMPDocument.Segments[i];
                    if (oSegment.TypeID == mpObjectTypes.Trailer)
                    {
                        string xDocVarID = "";
                        //create last trailer doc var
                        if (!bDocVarSet)
                        {
                            bool bTrailerIsCurrent = true;
                            try
                            {
                                //Test if all DMS-related variables are current
                                for (int j = 0; j < oSegment.Variables.Count; j++)
                                {
                                    Variable oVar = oSegment.Variables[j];
                                    //If any DMS-related variables don't match current Profile, trailer is not up-to-date
                                    if ((oVar.DefaultValue.Contains("[DMS") || oVar.DefaultValue.ToUpper().Contains("[DOCUMENT_")) &&
                                        oVar.Value.ToUpper() != Expression.Evaluate(oVar.DefaultValue, oSegment, oSegment.ForteDocument).ToUpper())
                                    {
                                        bTrailerIsCurrent = false;
                                        break;
                                    }
                                }
                            }
                            catch
                            {
                                bTrailerIsCurrent = false;
                            }
                            if (bTrailerIsCurrent)
                            {
                                //existing trailer is up-to-date, use current Profile info for doc var
                                xDocVarID = DocumentStampUI.GetCurrentDocID();
                            }
                            Prefill oPrefill = new Prefill(oSegment);
                            //Remove any Prefill items corresponding to Variables
                            //that would not appear in the trailer dialog
                            for (int j = 0; j < oSegment.Variables.Count; j++)
                            {
                                Variable oVar = oSegment.Variables[j];
                                if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) !=
                                    Variable.ControlHosts.DocumentEditor)
                                {
                                    //GLOG 6929 (dm) - get prefill item from tag id, not name - 
                                    //update existing wasn't updating doc id because we were failing
                                    //to delete it from the prefill here
                                    oPrefill.Delete(oVar.TagID);
                                }
                            }

                            DocumentStampUI.SetLastTrailerVariable(oPrefill.SegmentID +
                                DocumentStampUI.mpPrefillVariableSeparator.ToString() + oPrefill.Name +
                                DocumentStampUI.mpPrefillVariableSeparator.ToString() +
                                oPrefill.ToString(), xDocVarID, "", oSegment.ID1);
                            bDocVarSet = true;
                        }

                        //delete bounding objects
                        //GLOG 6836 (dm) - these will now be bookmarks
                        object oNodesArray = oSegment.Bookmarks;
                        //if (oSegment.ForteDocument.FileFormat == mpFileFormats.Binary)
                        //{
                        //    //xml tags
                        //    Word.XMLNode[] oNodes = oSegment.WordTags;
                        //    oNodesArray = oNodes;
                        //}
                        //else
                        //{
                        //    //content controls
                        //    Word.ContentControl[] oNodes = oSegment.ContentControls;
                        //    oNodesArray = oNodes;
                        //}

                        oMPDocument.Segments.Delete(oSegment.FullTagID);
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        try
                        {
                            //2-27-12 (dm) - pass in oTags, so that deleted tags/cc
                            //can be removed from the collection
                            LMP.Forte.MSWord.Tags oTags = oMPDocument.Tags;
                            object oMissing = System.Reflection.Missing.Value;
                            LMP.Forte.MSWord.WordDoc.DeleteTrailerBoundingObjects(
                                ref oNodesArray, ref oTags, ref oMissing);  //GLOG 7876
                        }
                        finally
                        {
                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                        }
                    }
                }
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// removes all MacPac automation in the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        public static bool RemoveAutomation(Word.Document oDoc)
        {
            return RemoveAutomation(oDoc, true);
        }

        /// <summary>
        /// removes all MacPac automation in the specified document
        /// </summary>
        /// <param name="oDoc"></param>
        public static bool RemoveAutomation(Word.Document oDoc, bool bPrompt)
        {
            if (LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, true) ||
                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oDoc, true) ||
                LMP.Architect.Api.Environment.BinaryDocDoesNotSupportXMLTags(oDoc, true)) //GLOG 5443 (dm)
                return false;

            Word.WdWindowState oState = oDoc.ActiveWindow.WindowState;
            oDoc.ActiveWindow.WindowState = Word.WdWindowState.wdWindowStateMinimize;

            bool bRemoved = LMP.MacPac.Application.RemoveBoundingObjects(oDoc,
                LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, bPrompt);

            if (bRemoved)
            {
                try
                {
                    //GLOG : 6534 : JSW 
                    //remove any leftover mp bookmarks
                    //LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.ConversionClass();
                    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                    oConvert.DeleteBookmarksAndDocVars(oDoc);

                    //setup taskpane to display the content manager
                    //and an empty editor
                    LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oDoc);
                    oTP.DocEditor.ClearTree();
                    oTP.SelectTab(TaskPane.Tabs.Content);

                    //ensure that task pane doesn't automatically
                    //show on doc change
                    oTP.DoNotShowOnDocChange = true;
                    oDoc.ActiveWindow.WindowState = oState;
                }
                catch { }
            }

            return bRemoved;
        }

        /// <summary>
        /// removes the MacPac 10 document schema from active document
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        private static bool RemoveTags(Word.Document oDoc)
        {
            ForteDocument oMPDocument = new ForteDocument(oDoc);
            oMPDocument.Refresh(ForteDocument.RefreshOptions.None);
            return RemoveTags(oMPDocument, true);
        }
        /// <summary>
        /// removes the MacPac 10 document schema - this
        /// will remove all MacPac tags - returns true
        /// iff tags were removed
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        private static bool RemoveTags(ForteDocument oMPDocument, bool bPrompt)
        {
            TaskPane oTP = null;

            try
            {
                oTP = TaskPanes.Item(oMPDocument.WordDocument);
            }
            catch { }

            if (oTP != null)
            {
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
            }

            object oUrn = "urn-legalmacpac-data/10";

            Word.XMLSchemaReference oSchema = null;

            try
            {
                oSchema = oMPDocument.WordDocument.XMLSchemaReferences.get_Item(ref oUrn);
            }
            catch { }

            if (oSchema != null)
            {
                //JTS 8/4/11:  Added parameter to allow removing without prompting
                DialogResult iRes = DialogResult.Yes;

                //prompt to create saved data if allowed by the admin
                //GLOG 6139 (dm) - don't prompt to save data if there are no segments
                if (LMP.MacPac.Session.CurrentUser.FirmSettings.AllowSavedDataCreationOnDeautomation &&
                    (oMPDocument.Segments.Count > 0))
                {
                    if (bPrompt)
                    {
                        iRes = MessageBox.Show(
                        LMP.Resources.GetLangString("Prompt_CreateSavedDataBeforeDeautomation"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button3);
                    }
                    else
                        iRes = DialogResult.No;

                    if (iRes == DialogResult.Yes)
                    {
                        oMPDocument.Refresh(ForteDocument.RefreshOptions.None, false, false);

                        for (int i = 0; i < oMPDocument.Segments.Count; i++)
                        {
                            SaveData(oMPDocument.Segments[i]);
                        }
                    }
                    else if (iRes == DialogResult.Cancel)
                    {
                        return false;
                    }
                }
                else
                {
                    if (bPrompt)
                    {
                        iRes = MessageBox.Show("Remove all " + LMP.ComponentProperties.ProductName + " tags in this document?",
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button2);
                    }
                    else
                        iRes = DialogResult.Yes;

                    if (iRes == DialogResult.No)
                        return false;
                }

                oSchema.Delete();

                //GLOG 5490 (dm) - delete bookmarks and doc vars
                LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                oConvert.DeleteBookmarksAndDocVars(oMPDocument.WordDocument);

                if (oTP != null)
                    oTP.Refresh(false, true, false);
            }

            return true;
        }
        /// <summary>
        /// removes the MacPac 10 document schema - this
        /// will remove all MacPac tags - returns true
        /// iff tags were removed
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        private static bool RemoveContentControls(ForteDocument oMPDocument, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType)
        {
            return RemoveContentControls(oMPDocument, iRemovalType, true);
        }

        private static bool RemoveContentControls(ForteDocument oMPDocument, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType, bool bPrompt)
        {
            bool bControlsRemoved = false;

            //GLOG 6139 (dm) - this condition was excluding documents with
            //content only in the headers, footers, or textboxes - I didn't
            //with another because the user may still want to remove the schema
            //if (oMPDocument.WordDocument.ContentControls.Count > 0)
            //{
                DialogResult iRes = DialogResult.Yes;

                if (iRemovalType == LMP.Forte.MSWord.mpSegmentTagRemovalOptions.None)
                {
                    //we're getting rid of variable and block ccs only
                    if (bPrompt)
                    {
                        iRes = MessageBox.Show(LMP.Resources.GetLangString(
                            "Prompt_RemoveVarAndBlockTagsInDocument"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);
                    }

                    if (iRes == DialogResult.No)
                    {
                        return false;
                    }
                }
                else
                {
                    //we're getting rid of all ccs - 
                    //prompt to create saved data if allowed by the admin
                    //GLOG 6139 (dm) - don't prompt to save data if there are no segments
                    //GLOG 6288: If this is being called for a Comparison result document, user may not 
                    //be logged in to MacPac Session.  Skip if bPrompt is false to avoid error caused by null CurrentUser
                    if (bPrompt && LMP.MacPac.Session.CurrentUser.FirmSettings.AllowSavedDataCreationOnDeautomation &&
                        (oMPDocument.Segments.Count > 0)) 
                    {
                        if (bPrompt)
                        {
                            iRes = MessageBox.Show(
                                LMP.Resources.GetLangString(
                                "Prompt_CreateSavedDataBeforeDeautomation"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.YesNoCancel,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button3);
                        }
                        else
                        {
                            iRes = DialogResult.No;
                        }

                        if (iRes == DialogResult.Yes)
                        {
                            for (int i = 0; i < oMPDocument.Segments.Count; i++)
                            {
                                SaveData(oMPDocument.Segments[i]);
                            }
                        }
                        else if (iRes == DialogResult.Cancel)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (bPrompt)
                        {
                            iRes = MessageBox.Show(LMP.Resources.GetLangString("Prompt_DeleteAllTags"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2);
                        }

                        if (iRes == DialogResult.No)
                            return false;
                    }
                }
                
                for (int i = 0; i < oMPDocument.Segments.Count; i++)
                {
                    Segment oSegment = oMPDocument.Segments[i];
                    bool bRet = RemoveContentControls(oSegment, iRemovalType, bPrompt); //JTS 8/4/11

                    if (bRet)
                        bControlsRemoved = true;
                }

                //refresh task pane if necessary
                TaskPane oTP = null;
                try
                {
                    oTP = LMP.MacPac.TaskPanes.Item(oMPDocument.WordDocument);
                }
                catch { }

                if (oTP != null)
                    oTP.Refresh(false, true, false);

                //GLOG 6140 (dm) - remove the schema
                //5-2-12 (dm) - added iRemovalType condition - schema was getting
                //inappropriately removed on Finish
                if (iRemovalType == LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All)
                {
                    object oUrn = "urn-legalmacpac-data/10";
                    Word.XMLSchemaReference oSchema = null;
                    try
                    {
                        oSchema = oMPDocument.WordDocument.XMLSchemaReferences.get_Item(ref oUrn);
                    }
                    catch { }
                    if (oSchema != null)
                    {
                        oSchema.Delete();
                        bControlsRemoved = true;
                    }

                    //GLOG 7480 (dm) - delete any remaining bookmarks and doc vars
                    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                    oConvert.DeleteBookmarksAndDocVars(oMPDocument.WordDocument);
                }
            //}
                return bControlsRemoved;
        }
        private static bool RemoveContentControls(Segment oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType)
        {
            return RemoveContentControls(oSegment, iRemovalType, true);
        }

        private static bool RemoveContentControls(Segment oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType, bool bPrompt)
        {
            //TODO: this is a temporary fix given that executing
            //this function on fax errs because some of the nodes are missing
            oSegment.RefreshNodes(true);
            
            //recurse through children
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                RemoveContentControls(oSegment.Segments[i], iRemovalType);
            }

            //get segment bookmarks
            Word.Bookmark[] aBmks = oSegment.Nodes.GetBookmarks(oSegment.FullTagID);
            LMP.Forte.MSWord.Tags oTags = oSegment.ForteDocument.Tags;

            //GLOG 7394 (dm) - disable track changes while removing content controls
            Word.Document oDoc = oSegment.ForteDocument.WordDocument;
            bool bTrackChanges = oDoc.TrackRevisions;
            if (bTrackChanges)
                oDoc.TrackRevisions = false;
            
            //cycle through segment bookmarks, recursing
            for (int i = aBmks.GetUpperBound(0); i >= 0; i--)
            {
                LMP.Forte.MSWord.WordDoc.RemoveContentControlsInBookmark(
                    aBmks[i], iRemovalType, true, ref oTags);
            }

            //GLOG 7394 (dm)
            if (bTrackChanges)
                oDoc.TrackRevisions = true;

            return true;
        }
        public static void PreconvertDocument(Word.Document oDoc)
        {
            
            //GLOG : 8326 : jw2
            //no longer supporting this in 11.3
            MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_FunctionalityNotSupported")), LMP.ComponentProperties.ProductName,
            MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
           
            //try
            //{
            //    //
            //    if (oDoc.Path == "")
            //    {
            //        MessageBox.Show(LMP.Resources.GetLangString("Msg_DocumentMustBeSaved"), LMP.ComponentProperties.ProductName,
            //            MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }
            //    DMSConvertor oConvertor = null;
            //    if (LMP.Architect.Base.DMS.IsProfiled)
            //    {
            //        string xDMS = "";
            //        switch (m_oDMS.DocumentManager.get_TypeID())
            //        {
            //            case fDMS.mpDMSs.mpDMS_IManage5x:
            //                xDMS = "Worksite";
            //                oConvertor = new WorksiteConvertor();
            //                break;
            //            case fDMS.mpDMSs.mpDMS_HummingbirdCOM:
            //            case fDMS.mpDMSs.mpDMS_Fusion:
            //                xDMS = "DM";
            //                break;
            //            case fDMS.mpDMSs.mpDMS_NetDocs:
            //                xDMS = "NetDocuments";
            //                break;
            //            case fDMS.mpDMSs.mpDMS_PCDOCS:
            //                xDMS = "DocsOpen";
            //                break;
            //        }
            //        if (xDMS != "Worksite")
            //        {
            //            MessageBox.Show(string.Format(LMP.Resources.GetLangString("Msg_PreconversionNotSupportedForDMS"), xDMS), LMP.ComponentProperties.ProductName,
            //            MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        oConvertor = new WordConvertor();
            //    }
            //    if (oConvertor != null)
            //        oConvertor.Preconvert(oDoc);
            //}
            //catch (System.Exception oE)
            //{
            //    LMP.Error.Show(oE);
            //}
        }
        public static void InvalidateMacPacRibbonControl(string xControlID)
        {
            if (MacPac.Application.MacPac10Ribbon != null)
            {
                MacPac.Application.MacPac10Ribbon.InvalidateControl(xControlID);
            }
        }

        /// <summary>
        /// allows the user to set the Normal style font name and font size
        /// </summary>
        public static void SetNormalStyleFont()
        {
            try
            {

                //LMP.Forte.MSWord.Undo oUndo = new LMP.Forte.MSWord.Undo();
                //oUndo.MarkStart("Normal Style");

                Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
                //GLOG 6288
                if (oDoc != null && LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oDoc.ActiveWindow))
                {
                    //need to select the main comparison window -
                    //Word freezes below if the selection is in
                    //a revision pane - dcf 7/21/11
                    if (oDoc.ActiveWindow.ActivePane.Index > 1)
                    {
                        oDoc.ActiveWindow.Panes[1].Activate();
                        oDoc = oDoc.ActiveWindow.ActivePane.Document;
                    }
                }
                object oStyleID = Word.WdBuiltinStyle.wdStyleNormal;
                Word.Style oStyle = oDoc.Styles.get_Item(ref oStyleID);

                FontForm oForm = new FontForm(oStyle.Font.Name, (int)oStyle.Font.Size,
                    oStyle, LMP.Resources.GetLangString("Dlg_DocumentFont_Title"));

                oForm.ShowDialog();

                // GLOG : 3115 : JAB
                // Set focus back to the document
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();

                //oUndo.MarkEnd();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordStyleException(
                    LMP.Resources.GetLangString("Error_CouldNotSetNormalStyle"), oE);
            }
        }
        public static void EditAuthorPreferences(bool bShowTree)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                int iSegID = 0;

                if (LMP.Forte.MSWord.GlobalMethods.CurWordApp.Documents.Count > 0)
                {
                    //get segment to select in author preferences dialog
                    TaskPane oTP = TaskPanes.Item(LMP.Forte.MSWord.WordApp.ActiveDocument());

                    if (oTP != null)
                    {
                        //get selected top-level segment
                        ForteDocument oMPDoc = oTP.ForteDocument;
                        Segment oSeg = oMPDoc.GetTopLevelSegmentFromSelection();

                        if (oSeg == null && oMPDoc.Segments.Count > 0)
                            //cursor is not in a MacPac segment -
                            //get first segment in document
                            oSeg = oMPDoc.Segments[0];

                        if (oSeg != null && oSeg is AdminSegment)
                            iSegID = oSeg.ID1;
                    }
                }
   
                AuthorPreferencesManager oForm = new AuthorPreferencesManager(iSegID, bShowTree);
                oForm.ShowDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

            finally
            {
                //GLOG : 8000 : ceh - set focus back to the document
                if (LMP.Forte.MSWord.GlobalMethods.CurWordApp.Documents.Count > 0)      //GLOG : 8148 : ceh
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
        }
        public static void EditAuthorPreferences()
        {
            EditAuthorPreferences(true);
        }
        public static void AttachToTemplate(string xTemplate)
        {
            Word.Global oWord = new Word.Global();

            Word.Document oDoc = oWord.ActiveDocument;
            if (oDoc != null)
            {
                if (xTemplate == "" || xTemplate == null)
                {
                    AttachToTemplateSelector oDlg = new AttachToTemplateSelector();
                    if (oDlg.ShowDialog() == DialogResult.OK)
                    {
                        xTemplate = oDlg.TemplatePath;
                    }
                    if (xTemplate == "")
                        return;
                }
                object oTemplate = (object)xTemplate;
                oDoc.set_AttachedTemplate(ref oTemplate);
                oDoc.UpdateStyles();
                UpdateTemplateHistory(xTemplate);
            }
        }
        /// <summary>
        /// Create a Prefill based on current segment
        /// </summary>
        public static void CreateLegacyPrefill()
        {
            try
            {
                if (Session.CurrentWordApp.ActiveDocument == null)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Error_DocumentRequiredForFunction"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                else if (Session.CurrentWordApp.ActiveDocument.Content.Characters.Count <= 1)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Error_NoDocumentContent"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                UserFolders oFolders = new UserFolders(Session.CurrentUser.ID);

                // Can't create Prefill if no User Folders exist
                //GLOG : 8152 : JSW
                //let ContentPropertyForm handle folder creation messages
                //if (oFolders.Count == 0)
                //{
                //    MessageBox.Show(LMP.Resources.GetLangString("Msg_PrefillRequiresUserFolder"),
                //        LMP.ComponentProperties.ProductName,
                //        MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}

                ContentPropertyForm oForm = new ContentPropertyForm();
                oForm.ShowForm(null, null, Form.MousePosition,
                    ContentPropertyForm.DialogMode.CreateLegacyPrefill);

                if (oForm.Canceled)
                    return;

                //TODO: get delimited string and segment id -
                //analyze document
                string xData = LMP.DocAnalyzer.Application.AnalyzeCurrentDocument(
                    LMP.DocAnalyzer.DocType.OutputFormats.DelimitedString, true, false);

                if (xData == null)
                    //analysis was canceled
                    return;

                //parse legacy doc type from returned data-
                //value is in the first "field"
                int iPos = xData.IndexOf("�");
                string xLegacyDocType = xData.Substring(0, iPos);
                xData = xData.Substring(iPos + 1);
                string xSegmentID = null;

                try
                {
                    //get Forte segment that is mapped to legacy doc type
                    xSegmentID = SimpleDataCollection.GetScalar(
                        "spSegmentIDFromLegacyDocType", new object[] { xLegacyDocType }).ToString() + ".0";
                }
                catch(System.Exception)
                {
                    //no segment ID is associated with doc type.
                }
                
                //create prefill
                //GLOG 3475
                Prefill oPrefill = new Prefill(xData, "", xSegmentID);

                //save as a variable set def
                VariableSetDefs oDefs = new VariableSetDefs(
                    mpVariableSetsFilterFields.Owner, Session.CurrentUser.ID);
                VariableSetDef oDef = null;

                if (oForm.FolderID != null)
                {
                    oDef = (VariableSetDef)oDefs.Create();

                    oDef.Name = oForm.ContentDisplayName;
                    oDef.Description = oForm.ContentDescription;
                    //Assign SegmentID if associated segment exists
                    if (xSegmentID != null)
                        oDef.SegmentID = xSegmentID;
                    oDef.OwnerID = Session.CurrentUser.ID;
                    oDef.ContentString = oPrefill.ToString();
                    oDefs.Save((StringIDSimpleDataItem)oDef);

                    // assign new prefill to selected user folder
                    int iFolderID1 = 0;
                    int iFolderID2 = 0;
                    LMP.Data.Application.SplitID(oForm.FolderID, out iFolderID1, out iFolderID2);
                    FolderMembers oMembers = new FolderMembers(iFolderID1, iFolderID2);
                    FolderMember oMember = (FolderMember)oMembers.Create();
                    int iID1 = 0;
                    int iID2 = 0;
                    LMP.Data.Application.SplitID(oDef.ID, out iID1, out iID2);
                    oMember.ObjectID1 = iID1;
                    oMember.ObjectID2 = iID2;
                    oMember.ObjectTypeID = mpFolderMemberTypes.VariableSet;
                    oMembers.Save((StringIDSimpleDataItem)oMember);
                }
                else
                {
                    FolderMember oFolderMember = oForm.SelectedFolderMember;

                    string xPrefillID = oFolderMember.ObjectID1.ToString() + "." + 
                        oFolderMember.ObjectID2.ToString();
                    oDef = (VariableSetDef)oDefs.ItemFromID(xPrefillID);

                    oDef.Name = oFolderMember.Name;
                    oDef.Description = oForm.ContentDescription;
                    oDef.SegmentID = xSegmentID;
                    oDef.OwnerID = Session.CurrentUser.ID;
                    oDef.ContentString = oPrefill.ToString();

                    oDefs.Save((StringIDSimpleDataItem)oDef);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString(
                    "Error_LegacyPrefill_Create"), oE);
            }
        }

        /// <summary>
        /// recreates the legacy document as an Forte segment -
        /// inserts in a new file
        /// </summary>
        public static void ConvertLegacyDocument()
        {
            try
            {
                //get current selection
                Word.Selection oSel = LMP.MacPac.Session.CurrentWordApp.Selection;

                if (Session.CurrentWordApp.ActiveDocument == null)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Error_DocumentRequiredForFunction"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                RecreateOptionsForm oForm = new RecreateOptionsForm(
                    RecreateOptionsForm.FormType.RecreateLegacyDocument);
                
                // If no text is selected, disable the option to use selected text.
                oForm.UseSelectedTextOption = (oSel.End - oSel.Start) > 0;

                if (oForm.ShowDialog() == DialogResult.Cancel)
                    return;

                //analyze document
                string xData = LMP.DocAnalyzer.Application.AnalyzeCurrentDocument(
                    LMP.DocAnalyzer.DocType.OutputFormats.DelimitedString, true, false);

                if (xData == null)
                {
                    //analysis was canceled
                    return;
                }
                else
                {
                    // GLOG : 2246 : JAB
                    // Prompt the prompt the user to verify that they want to replace existing.
                    if (oForm.InsertionLocation == RecreateOptionsForm.Locations.ReplaceExisting)
                    {
                        if (MessageBox.Show(LMP.Resources.GetLangString("Prompt_ReplaceDocumentContent"),
                                            LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    System.Windows.Forms.Application.DoEvents();

                    string xBodyXml = "";
                        
                    if(oForm.TextOption == RecreateOptionsForm.TextOptions.IncludeSelectedText)
                        xBodyXml = LMP.MacPac.Session.CurrentWordApp.Selection.WordOpenXML; //GLOG 15782

                    //parse doc type name from analyzer result
                    int iPos = xData.IndexOf(StringArray.mpEndOfElement);
                    string xDocType = xData.Substring(0, iPos);
                    string xContentString = xData.Substring(iPos + 1);

                    int iSegmentID = 0;
                    try
                    {
                        //get segment mapped to doc type
                        iSegmentID = LMP.Data.Application.GetLegacyMappedSegment(xDocType);
                    }
                    catch (System.Exception)
                    {
                        //no segment ID is associated with doc type.
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_LegacyDocNotMappedToSegment"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    //create prefill from analysis
                    //GLOG 3475
                    Prefill oPrefill = new Prefill(xContentString, "LegacyContent", iSegmentID.ToString());

                    Word.Document oDoc = null;

                    try
                    {
                        //GLOG 5671: Use internal TaskPane Insertion functions.
                        //New TaskPane may not be created automatically for new document, depending
                        //on User Application settings
                        if (oForm.InsertionLocation == RecreateOptionsForm.Locations.ReplaceExisting)
                        {
                            //clear out current section
                            Word.Section oSection = oSel.Sections.First;
                            bool bTrue = true;
                            LMP.Forte.MSWord.WordDoc.ClearSection(oSection, bTrue);

                            oDoc = oSel.Document;
                            TaskPane oTP = TaskPanes.Item(oDoc);

                            oTP.InsertSegmentInCurrentDoc(iSegmentID.ToString(), oPrefill, xBodyXml,
                                Segment.InsertionLocations.InsertAtEndOfCurrentSection, Segment.InsertionBehaviors.Default);
                        }
                        else if (oForm.InsertionLocation == RecreateOptionsForm.Locations.NewDocument)
                        {
                            TaskPane.InsertSegmentInNewDoc(iSegmentID.ToString(), oPrefill, xBodyXml);
                        }
                    }
                    finally
                    {
                        oForm.Dispose();
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString(
                    "Error_ConvertLegacyDocument"), oE);
            }
        }
        public static void ConvertLegacyDocumentOLD()
        {
            try
            {
                //get current selection
                Word.Selection oSel = LMP.MacPac.Session.CurrentWordApp.Selection;

                if (Session.CurrentWordApp.ActiveDocument == null)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Error_DocumentRequiredForFunction"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                DialogResult iChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Prompt_ReplaceSectionContent"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question);

                if (iChoice == DialogResult.Cancel)
                    return;

                //analyze document
                string xData = LMP.DocAnalyzer.Application.AnalyzeCurrentDocument(
                    LMP.DocAnalyzer.DocType.OutputFormats.DelimitedString, true, false);

                if (xData == null)
                {
                    //analysis was canceled
                    return;
                }
                else
                {
                    System.Windows.Forms.Application.DoEvents();

                    string xBodyXml = LMP.MacPac.Session.CurrentWordApp.Selection.get_XML(false);

                    //parse doc type name from analyzer result
                    int iPos = xData.IndexOf(StringArray.mpEndOfElement);
                    string xDocType = xData.Substring(0, iPos);
                    string xContentString = xData.Substring(iPos + 1);

                    //get segment mapped to doc type
                    int iSegmentID = LMP.Data.Application.GetLegacyMappedSegment(xDocType);

                    //create prefill from analysis
                    //GLOG 3475
                    Prefill oPrefill = new Prefill(xContentString, "LegacyContent", iSegmentID.ToString());

                    Word.Document oDoc = null;

                    if (iChoice == DialogResult.Yes)
                    {
                        //clear out current section
                        Word.Section oSection = oSel.Sections.First;
                        bool bTrue = true;
                        LMP.Forte.MSWord.WordDoc.ClearSection(oSection,  bTrue);

                        oDoc = oSel.Document;
                    }
                    else if (iChoice == DialogResult.No)
                    {
                        oDoc = LMP.Forte.MSWord.WordApp.CreateForteDocument();
                    }

                    TaskPane oTP = TaskPanes.Item(oDoc);

                    oTP.ScreenUpdating = false;

                    try
                    {
                        //insert segment into current document using prefill
                        Segment oSegment = oTP.ForteDocument.InsertSegment(iSegmentID.ToString(),
                            null, Segment.InsertionLocations.InsertAtStartOfCurrentSection,
                            Segment.InsertionBehaviors.Default, false, oPrefill, xBodyXml,
                            true, true, null, false, true, null);

                        SetXMLTagStateForEditing();

                        oTP.SelectTab(TaskPane.Tabs.Edit);
                        System.Windows.Forms.Application.DoEvents();
                        oTP.DocEditor.ExpandTopLevelNodes();
                        TaskPane.NewSegment = oSegment;
                    }
                    finally
                    {
                        oTP.ScreenUpdating = true;
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString(
                    "Error_ConvertLegacyDocument"), oE);
            }
        }
        /// <summary>
        /// List of recently-used templates used by the Attach To ribbon button
        /// </summary>
        public static List<string> TemplateHistory
        {
            get
            {
                if (m_lTemplateHistory == null)
                {
                    LoadTemplateHistory();
                    //m_lTemplateHistory.Add("D:\\Forte\\10.0\\Templates\\Letter.dotx");
                    //m_lTemplateHistory.Add("D:\\Forte\\10.0\\Templates\\Memo.dotx");
                    //m_lTemplateHistory.Add("D:\\Forte\\10.0\\Templates\\Pleading.dotx");
                    //m_lTemplateHistory.Add("D:\\Forte\\10.0\\Templates\\Fax.dotx");
                }
                return m_lTemplateHistory;
            }
        }
        /// <summary>
        /// Add last-used template to the list
        /// </summary>
        /// <param name="xLastUsed"></param>
        private static void UpdateTemplateHistory(string xLastUsed)
        {
            if (xLastUsed == "")
                return;

            int iHighPos = TemplateHistory.Count;
            string xTemp = "";
            if (iHighPos == 0)
            {
                //Currently list is empty - just add new item
                TemplateHistory.Add(xLastUsed);
            }
            else
            {
                for (int i = 0; i < TemplateHistory.Count; i++)
                {
                    xTemp = TemplateHistory[i];
                    //Mark position after which order will remain unchanged
                    if (xTemp.ToUpper() == xLastUsed.ToUpper())
                        iHighPos = i;
                }

                if (iHighPos == TemplateHistory.Count && TemplateHistory.Count < m_iMaxTemplateHistory)
                {
                    //Still room in the list, add a new entry
                    TemplateHistory.Add("");
                }

                for (int i = iHighPos; i > 0; i--)
                {
                    //Move all affected history items down one position
                    TemplateHistory[i] = TemplateHistory[i - 1];
                }
                TemplateHistory[0] = xLastUsed;
            }
            SaveTemplateHistory();
        }
        /// <summary>
        /// Reads templates MRU List from Registry
        /// </summary>
        static void LoadTemplateHistory()
        {
            m_lTemplateHistory = new List<string>();
            string xRegPath = LMP.Registry.MacPac10RegistryRoot + "TemplatesMRU\\";
            for (int i = 1; i <= m_iMaxTemplateHistory; i++)
            {
                string xValue = LMP.Registry.GetCurrentUserValue(xRegPath, "MRU" + i.ToString());
                if (xValue != "" && xValue != null)
                    m_lTemplateHistory.Add(xValue);
            }
        }
        /// <summary>
        /// Saves list of recently-used templates to the Registry
        /// </summary>
        static void SaveTemplateHistory()
        {
            if (m_lTemplateHistory != null && m_lTemplateHistory.Count > 0)
            {
                string xRegPath = LMP.Registry.MacPac10RegistryRoot + "TemplatesMRU\\";
                //Clear existing registry values
                LMP.Registry.SetCurrentUserValue(xRegPath, "", "");
                for (int i = 1; i <= m_lTemplateHistory.Count; i++)
                {
                    LMP.Registry.SetCurrentUserValue(xRegPath, "MRU" + i.ToString(), m_lTemplateHistory[i-1]);
                }
            }
        }
        /// <summary>
        /// Show/Hide XML Tags based on User Application Preference
        /// </summary>
        public static void SetXMLTagStateForEditing()
        {
            Word.Document oDoc = Session.CurrentWordApp.ActiveDocument;
            if (oDoc != null)
            {
                mpTriState bShowXML = mpTriState.False;
                LMP.Forte.MSWord.Environment oEnv = new LMP.Forte.MSWord.Environment();
                try
                {
                    if (Session.CurrentUser.UserSettings.ShowXMLTags)
                        bShowXML = mpTriState.True;
                }
                catch { }

                ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                try
                {
                    oEnv.SetExecutionState(oDoc, (int)mpTriState.Undefined, (int)mpTriState.Undefined, (int)mpTriState.Undefined,
                        (int)mpTriState.Undefined, (int)bShowXML, (int)mpTriState.Undefined);
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = oEvents;
                }
            }
        }
        public static void SelectTaskPane()
        {
            TaskPane.SelectTaskPane();
        }
        /// <summary>
        /// redefines the styles in the local Normal.dot
        /// with those defined by the administrator
        /// </summary>
        public static void ResetNormalStyles()
        {
            int iCount = Session.CurrentWordApp.Documents.Count;
            string xSourcePath = "";
            string xNormal = "";
            object oName;
            Word.Document oNormal = null;
            object[] aStyles = new object[0];
            DateTime t0 = DateTime.Now;
            try
            {
                //freeze Word screen
                Session.CurrentWordApp.ScreenUpdating = false;
                LMP.WinAPI.LockWindowUpdate(LMP.WinAPI.FindWindow("OpusApp", 0));

                object oMissing = System.Reflection.Missing.Value;
                object oFalse = false;

                //get defined normal template xml
                string xXML = LMP.Data.Application.GetFirmNormalTemplateXML();

                // Convert XML text to byte array
                byte[] bytXML = System.Text.Encoding.UTF8.GetBytes((xXML).ToCharArray());

                // Create temporary file
                xSourcePath = Path.GetTempFileName();

                try
                {
                    FileStream oStream = File.Open(xSourcePath, FileMode.Create);

                    // Write bytXML to temporary file
                    oStream.Write(bytXML, 0, bytXML.Length);
                    oStream.Close();
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.XMLException(oE.Message);
                }

                xNormal = Session.CurrentWordApp.NormalTemplate.FullName;
                LMP.Trace.WriteNameValuePairs("Normal", xNormal);

                //GLOG 5185: If Normal.dotm is on a Mapped drive, opening in Word will
                //result in a File In Use message.  In this case, we check for In Use 
                //styles in the XML, and then use OrganizerCopy to copy these individually,
                //without opening Normal.dotm
                if (xNormal.ToUpper() != OS.GetUNCPath(xNormal).ToUpper())
                {
                    //open Normal doc hidden
                    oName = xSourcePath;
                    //Open invisible to minimize screen updates
                    oNormal = Session.CurrentWordApp.Documents.Open(ref oName,
                        ref oMissing, ref oMissing, ref oFalse, ref oMissing, ref oMissing, ref oMissing,
                         ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oFalse, ref oMissing,
                          ref oMissing, ref oMissing, ref oMissing);
                    aStyles = (object[])LMP.Forte.MSWord.WordDoc.GetInUseStyleList(oNormal);
                    oNormal.Saved = true;
                    oNormal.Close(ref oMissing, ref oMissing, ref oMissing);
                    LMP.Trace.WriteNameValuePairs("aStyles.Length", aStyles.Length);
                    if (aStyles.GetLength(0) > 0)
                    {
                        LMP.Forte.MSWord.WordDoc.OrganizerCopyStylesByPath(xNormal, xSourcePath, aStyles);
                        Session.CurrentWordApp.Application.NormalTemplate.Save();
                    }
                }
                else
                {
                    //open Normal doc hidden
                    oName = xNormal;
                    //Open invisible to minimize screen updates
                    oNormal = Session.CurrentWordApp.Documents.Open(ref oName,
                        ref oMissing, ref oMissing, ref oFalse, ref oMissing, ref oMissing, ref oMissing,
                         ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oFalse, ref oMissing,
                          ref oMissing, ref oMissing, ref oMissing);

                    //GLOG - 3450 - CEH
                    //close & return if oNormal is ReadOnly -
                    //this can happen in a Citrix environment
                    if (oNormal.ReadOnly)
                    {
                        oNormal.Close(ref oFalse, ref oMissing, ref oMissing);
                        return;
                    }

                    LMP.Trace.WriteNameValuePairs("Normal", oNormal.FullName);
                    Session.CurrentWordApp.ScreenUpdating = false;
                    //Need to set window visible, otherwise view state will not be saved
                    oNormal.ActiveWindow.Visible = true;
                    //update normal
                    LMP.Forte.MSWord.WordDoc.OrganizerCopyStyles(oNormal, xSourcePath, oMissing);
                    //save and close
                    oNormal.Save();
                    oNormal.Close(ref oMissing, ref oMissing, ref oMissing);
                }

                Session.LastNormalStylesUpdate = LMP.Data.Application.GetFirmNormalLastEditTime();
                
                //open MacPacNormal doc hidden
                oName = LMP.Data.Application.BaseTemplate;

                //Open invisible to minimize screen updates
                oNormal = Session.CurrentWordApp.Documents.Open(ref oName,
                    ref oMissing, ref oMissing, ref oFalse, ref oMissing, ref oMissing, ref oMissing,
                     ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oFalse, ref oMissing,
                      ref oMissing, ref oMissing, ref oMissing);

                //GLOG - 6952 - ceh
                //close & return if MacPacNormal is ReadOnly -
                if (oNormal.ReadOnly)
                {
                    oNormal.Close(ref oFalse, ref oMissing, ref oMissing);
                    return;
                }

                LMP.Trace.WriteNameValuePairs("MacPacNormal", oNormal.FullName);
                
                Session.CurrentWordApp.ScreenUpdating = false;
                //Need to set window visible, otherwise view state will not be saved
                oNormal.ActiveWindow.Visible = true;
                //update macpac normal
                LMP.Forte.MSWord.WordDoc.OrganizerCopyStyles(oNormal, xSourcePath, oMissing);
                //save and close
                oNormal.Save();
                oNormal.Close(ref oMissing, ref oMissing, ref oMissing);

                //Add new blank document if necessary
                if (iCount > Session.CurrentWordApp.Documents.Count)
                {
                    Session.CurrentWordApp.ScreenUpdating = false;
                    Session.CurrentWordApp.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                try
                {
                    // clean up temporary file
                    if (File.Exists(xSourcePath))
                        File.Delete(xSourcePath);
                }
                catch (IOException)
                {
                    OS.AddFileToDeletionList(xSourcePath);
                }
                LMP.Benchmarks.Print(t0);
                //turn on screen updates
                LMP.WinAPI.LockWindowUpdate(0);
                Session.CurrentWordApp.ScreenUpdating = true;
            }
        }
        /// displays the dialog box that inserts
        /// office addresss
        /// </summary>
        public static void InsertOfficeAddress()
        {
            try
            {
                //LMP.Forte.MSWord.Undo oUndo = new LMP.Forte.MSWord.Undo();
                //oUndo.MarkStart("Office Address");
                OfficeAddressForm oForm = new OfficeAddressForm();
                oForm.ShowDialog();

                System.Windows.Forms.Application.DoEvents();
                // GLOG : 3115 : JAB
                // Set focus back to the document after inserting the address.
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                //oUndo.MarkEnd();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// displays the dialog box that inserts
        /// page numbers
        /// </summary>
        public static void InsertPageNumber()
        {
            try
            {
                //get current document
                Word.Document oDoc = null;
                try
                {
                    oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
                }
                catch { }
                if (oDoc == null)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_ActiveDocumentRequired"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //LMP.Forte.MSWord.Undo oUndo = new LMP.Forte.MSWord.Undo();
                //oUndo.MarkStart("Page Number");

                PageNumberInsertForm oForm = new PageNumberInsertForm();

                DialogResult iRet = oForm.ShowDialog();
                System.Windows.Forms.Application.DoEvents();
                if (iRet == DialogResult.OK)
                {
                    LMP.MacPac.Application.ScreenUpdating = false;
                    oForm.Finish();
                    LMP.MacPac.Application.ScreenUpdating = true;
                }

                System.Windows.Forms.Application.DoEvents();
                oDoc.ActiveWindow.SetFocus();
                //oUndo.MarkEnd();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6025 : CEH
        /// displays the dialog box that removes
        /// page numbers
        /// </summary>
        public static void RemovePageNumber()
        {
            try
            {
                //get current document
                Word.Document oDoc = null;
                try
                {
                    oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
                }
                catch { }
                if (oDoc == null)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_ActiveDocumentRequired"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //LMP.Forte.MSWord.Undo oUndo = new LMP.Forte.MSWord.Undo();
                //oUndo.MarkStart("Page Number");

                PageNumberRemoveForm oForm = new PageNumberRemoveForm();

                DialogResult iRet = oForm.ShowDialog();
                System.Windows.Forms.Application.DoEvents();
                if (iRet == DialogResult.OK)
                {
                    LMP.MacPac.Application.ScreenUpdating = false;
                    oForm.Finish();
                    LMP.MacPac.Application.ScreenUpdating = true;
                }

                System.Windows.Forms.Application.DoEvents();
                oDoc.ActiveWindow.SetFocus();
                //oUndo.MarkEnd();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// gets/sets the whether or not the screen will update
        /// </summary>
        public static bool ScreenUpdating
        {
            get { return LMP.MacPac.Session.CurrentWordApp.ScreenUpdating; }
            set
            {
                Word.Application oWord = LMP.MacPac.Session.CurrentWordApp;

                TaskPane oTP = null;

                //GLOG 6181: If Protected View is active, ActiveDocument object not available
                if (!LMP.Forte.MSWord.WordApp.ProtectedViewIsActive() && oWord.Documents.Count > 0)
                {
                    oTP = TaskPanes.Item(oWord.ActiveDocument);
                }

                if (oTP != null)
                    oTP.ScreenUpdating = value;
                else
                {
                    bool bScreenUpdating = oWord.ScreenUpdating;

                    if (!bScreenUpdating && value)
                    {
                        //unfreeze the editor
                        LMP.WinAPI.LockWindowUpdate(0);
                        oWord.ScreenUpdating = true;
                        LMP.WinAPI.LockWindowUpdate(0);
                    }
                    else if (bScreenUpdating && !value)
                    {
                        //freeze Word
                        LMP.WinAPI.LockWindowUpdate(LMP.WinAPI.FindWindow("OpusApp", 0));
                        oWord.ScreenUpdating = false;
                    }
                }
            }
        }
        public static void InsertContactDetail()
        {
            //GLOG : 7469 : ceh
            if (!Data.Application.AllowCIWithoutOutlookRunning())
                return;

            try
            {
                Data.Application.StartCIIfNecessary();

                //get position to "park" CI dialog
                System.Drawing.Point oCoords = LMP.Data.Application.User.UserSettings.CIDlgLocation;
                float x = (float)oCoords.X;
                float y = (float)oCoords.Y;

                //set CI dialog position
                ICSession oCI = Data.Application.CISession;
                oCI.FormLeft = x;
                oCI.FormTop = y;

                ICContacts oContacts = null;

                oContacts = oCI.GetContactsEx(ciRetrieveData.ciRetrieveData_All,
                    ciRetrieveData.ciRetrieveData_None, ciRetrieveData.ciRetrieveData_None,
                    ciRetrieveData.ciRetrieveData_None,
                    ICI.ciSelectionlists.ciSelectionList_To, 0, ciAlerts.ciAlert_NoAddresses,
                    "Selected Contacts", "", "", "",
                    0, 0, 0, 0);

                //save CI dialog coordinates for next use
                oCoords.X = (int)oCI.FormLeft;
                oCoords.Y = (int)oCI.FormTop;

                LMP.Data.Application.User.UserSettings.CIDlgLocation = oCoords;

                if ((oContacts != null) && (oContacts.Count() > 0))
                {
                    System.Text.StringBuilder oSB = new System.Text.StringBuilder();

                    //GLOG : 8031 : ceh
                    for (int i = 1; i <= oContacts.Count(); i++)
                    {
                        object o = i;
                        string xDetail = "";
                        string xTemplate = "";
                        //ICContact oContact = oContacts.Item(ref o);
                        ICContact oContact = oContacts.Item(o);

                        //GLOG 4825: Don't duplicate Company if same as FullName
                        if (oContact.Company != "" && oContact.FullName == oContact.Company)
                            xTemplate = "<COMPANY>\v<COREADDRESS>";
                        else
                        {
                            if (oContact.FullName != "")
                            {
                                xTemplate = "<FULLNAMEWITHPREFIXANDSUFFIX>\v";
                            }
                            if (oContact.Title != "")
                            {
                                xTemplate = xTemplate + "<TITLE>\v";
                            }
                            if (oContact.Company != "")
                            {
                                xTemplate = xTemplate + "<COMPANY>\v";
                            }
                            xTemplate = xTemplate + "<COREADDRESS>";
                        }
                        //GLOG : 8031 : ceh
                        //fill in CoreAddress first
                        xTemplate = xTemplate.Replace("<COREADDRESS>", LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));
                        //fill in other details
                        xDetail = oContact.GetDetail(xTemplate, true);

                        xDetail = xDetail.Replace("\r\n", "\v");
                        oSB.AppendFormat("{0}\r\n\r\n", xDetail);
                    }

                    //LMP.Forte.MSWord.Undo oUndo = new LMP.Forte.MSWord.Undo();
                    //oUndo.MarkStart("Contact Detail");

                    Word.Selection oSel = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection;
                    oSel.InsertAfter(oSB.ToString());
                    object oMissing = System.Reflection.Missing.Value;
                    oSel.EndOf(ref oMissing, ref oMissing);

                    //oUndo.MarkEnd();
                }


                //if (oContacts != null)
                    //System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContacts);

                oContacts = null;

                System.Windows.Forms.Application.DoEvents();

                // GLOG : 3115 : JAB
                // Set focus back to the document after inserting the address.
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public static void InsertTrailer()
        {
            InsertTrailer(true, false);
        }
        public static void InsertTrailer(TrailerInsertionOptions iTrailerDisplayOption, bool bAuto)
        {
            if (iTrailerDisplayOption == TrailerInsertionOptions.Silent)
            {
                //silent
                InsertTrailer(false, bAuto);
            }
            else
                //display dialog
                InsertTrailer(true, bAuto);            
        }
        public static void InsertTrailer(bool bDisplayDialog, bool bAuto)
        {
            //get current document
            Word.Document oDoc = null;
            try
            {
                oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            }
            catch { }

            if (oDoc == null)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_ActiveDocumentRequired"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //GLOG 7496 (dm) - turn on flag to treat all files as open xml
            //during trailer insertion process in Word 2013
            try
            {
                LMP.Forte.MSWord.Application.SetTrailerInProgressFlag(true);

                //JTS 9/5/11: Don't display dialog if all sections marked for no trailer and trailer has been run manually
                if (ForteDocument.AllSectionsMarkedForNoTrailer(oDoc) && bDisplayDialog && !bAuto)
                {
                    //Document has been marked for No Trailer at Document or Section level
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_AllSectionsMarkedForNoTrailer"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }


                //restored trailer in binary docs (1/17/11 dm)
                //ForteDocument oMPDoc = new ForteDocument(oDoc);
                //if (oMPDoc.FileFormat == mpFileFormats.Binary)
                //    return;

                try
                {
                    //suppress handling of Word XML Event handlers
                    //because this is session is being hosted in a 
                    //process outside IBF - there is no way to control
                    //IgnoreWordXMLEvents
                    SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                        mpPseudoWordEvents.SuppressXMLEvents);

                    //GLOG 6288
                    if (LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oDoc.ActiveWindow))
                    {
                        //need to select the main comparison window -
                        //Word freezes below if the selection is in
                        //a revision pane - dcf 7/21/11
                        if (oDoc.ActiveWindow.ActivePane.Index > 1)
                        {
                            oDoc.ActiveWindow.Panes[1].Activate();
                            oDoc = oDoc.ActiveWindow.ActivePane.Document;
                        }
                    }

                    //GLOG 4482: Save initial Saved state before dialog
                    //GLOG 7958: Moved above SetExecutionState, since that may change Saved state
                    bool bSaved = oDoc.Saved;
                    LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
                    oEnv.SaveState();
                    //JTS 3/26/10: Make sure Hidden Text is on
                    oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.True, mpTriState.Undefined, mpTriState.Undefined,
                        mpTriState.Undefined, true);
                    if (!bAuto)
                    {
                        //Delete temp variable used by Worksite - this should only exist
                        //during initial save, but sometimes remains when it shouldn't
                        try
                        {
                            object oVarName = "DMS_Temp";
                            oDoc.Variables.get_Item(ref oVarName).Delete();
                        }
                        catch { }
                    }
                    DocumentStampUI oForm = new DocumentStampUI(DocumentStampUI.Mode.Trailer);
                    DialogResult iRet = oForm.ShowDialog(oDoc, bDisplayDialog, bAuto);
                    if (bDisplayDialog) //GLOG 7473 (dm)
                        System.Windows.Forms.Application.DoEvents();
                    if (iRet == DialogResult.OK)
                    {
                        oForm.Finish();
                    }
                    else
                    {
                        //GLOG 3087: prevent display of empty header paragraph
                        LMP.Forte.MSWord.WordDoc.ResetEmptyHeader();
                        //GLOG 7958: This code was restored
                        //GLOG 4482: Restore initial Saved state if Trailer cancelled
                        //to avoid extra Save after InsertTrailerIfNecessary
                        oDoc.Saved = bSaved;
                    }
                    oDoc.ActiveWindow.SetFocus();

                    //#3211 - ceh
                    //prevent RestoreState from unnecessary flashing

                    Session.CurrentWordApp.ScreenUpdating = false;
                    //GLOG 3625: Make sure screen updating is restored if error occurs
                    try
                    {
                        //GLOG 7958: Get updated Saved status - may have been changed by Trailer insertion
                        bSaved = oDoc.Saved;
                        oEnv.RestoreState(true, false, false);
                    }
                    finally
                    {
                        //GLOG 4482: Restore initial Saved state if Trailer cancelled
                        //to avoid extra Save after InsertTrailerIfNecessary
                        //GLOG 7958: Moved to end
                        oDoc.Saved = bSaved;
                        Session.CurrentWordApp.ScreenUpdating = true;
                        Session.CurrentWordApp.ScreenRefresh();
                    }
                }
                catch (LMP.Exceptions.PasswordException)
                {
                    //GLOG 5684: Don't display message if Auto Trailer is running
                    if (!bAuto)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_ProtectedDocument"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
                finally
                {
                    LMP.MacPac.Application.SetXMLTagStateForEditing();
                    SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                        mpPseudoWordEvents.ResumeXMLEvents);
                }
            }
            finally
            {
                //GLOG 7496 (dm) - turn off flag
                LMP.Forte.MSWord.Application.SetTrailerInProgressFlag(false);
            }
        }
        //GLOG - 2476 - ceh
        //JTS 6/11/24 - modified to take Prefill argument instead of Segment
		//GLOG : 6262 : CEH - Modified to take 'object oTrailers' argument
        public static void InsertTrailer(object oTrailers, Prefill oExistingTrailer, int iScope) 
        {
            try
            {
                Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();

                //suppress handling of Word XML Event handlers
                //because this is session is being hosted in a 
                //process outside IBF - there is no way to control
                //IgnoreWordXMLEvents
                SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                    mpPseudoWordEvents.SuppressXMLEvents);

                LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
                oEnv.SaveState();
                //JTS 3/26/10: Make sure Hidden Text is on
                oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.True, mpTriState.Undefined, mpTriState.Undefined,
                    mpTriState.Undefined, true);

                DocumentStampUI oForm = new DocumentStampUI(DocumentStampUI.Mode.Trailer);
                DialogResult iRet = oForm.ShowDialog(oTrailers, oExistingTrailer, iScope, false, false);
                System.Windows.Forms.Application.DoEvents();
                if (iRet == DialogResult.OK)
                {
                    oForm.Finish();
                }
                else
                {
                    //GLOG 3087: prevent display of empty header paragraph
                    LMP.Forte.MSWord.WordDoc.ResetEmptyHeader();
                }
                oDoc.ActiveWindow.SetFocus();

                //#3211 - ceh
                //prevent RestoreState from unnecessary flashing

                Session.CurrentWordApp.ScreenUpdating = false;
                //GLOG 3625: Make sure screen updating is restored if error occurs
                try
                {
                    oEnv.RestoreState(true, false, false);
                }
                finally
                {
                    Session.CurrentWordApp.ScreenUpdating = true;
                    Session.CurrentWordApp.ScreenRefresh();
                }

                if (iRet == DialogResult.OK && Session.CurrentWordVersion == 11)
                {
                    //Notify Taskpane to refresh tree
                    SendMessageToTaskPane(oDoc, mpPseudoWordEvents.ContentChanged);
                }
            }
            catch (LMP.Exceptions.PasswordException)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_ProtectedDocument"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                LMP.MacPac.Application.SetXMLTagStateForEditing();
                SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                    mpPseudoWordEvents.ResumeXMLEvents);
            }
        }

        //GLOG : 8178 : ceh
        public static void InsertDraftStamp(object oDraftStamps, Prefill oExistingDraftStamp, int iScope)
        {
            try
            {
                Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();

                //suppress handling of Word XML Event handlers
                //because this is session is being hosted in a 
                //process outside IBF - there is no way to control
                //IgnoreWordXMLEvents
                SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                    mpPseudoWordEvents.SuppressXMLEvents);

                LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
                oEnv.SaveState();
                //JTS 3/26/10: Make sure Hidden Text is on
                oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.True, mpTriState.Undefined, mpTriState.Undefined,
                    mpTriState.Undefined, true);

                DocumentStampUI oForm = new DocumentStampUI(DocumentStampUI.Mode.DocumentStamp);
                DialogResult iRet = oForm.ShowDialog(oDraftStamps, oExistingDraftStamp, iScope, false, false);
                System.Windows.Forms.Application.DoEvents();
                if (iRet == DialogResult.OK)
                {
                    oForm.Finish();

                    //update TaskPane
                    Word.Application oApp = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
                    TaskPane oTP = TaskPanes.Item(oApp.ActiveDocument);
                    if (oTP != null)
                    {
                        //Update Editor tree
                        oTP.DocEditor.RefreshTree();
                    }

                }
                else
                {
                    //GLOG 3087: prevent display of empty header paragraph
                    LMP.Forte.MSWord.WordDoc.ResetEmptyHeader();
                }
                oDoc.ActiveWindow.SetFocus();

                //#3211 - ceh
                //prevent RestoreState from unnecessary flashing

                Session.CurrentWordApp.ScreenUpdating = false;
                //GLOG 3625: Make sure screen updating is restored if error occurs
                try
                {
                    oEnv.RestoreState(true, false, false);
                }
                finally
                {
                    Session.CurrentWordApp.ScreenUpdating = true;
                    Session.CurrentWordApp.ScreenRefresh();
                }

                if (iRet == DialogResult.OK && Session.CurrentWordVersion == 11)
                {
                    //Notify Taskpane to refresh tree
                    SendMessageToTaskPane(oDoc, mpPseudoWordEvents.ContentChanged);
                }
            }
            catch (LMP.Exceptions.PasswordException)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_ProtectedDocument"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                LMP.MacPac.Application.SetXMLTagStateForEditing();
                SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                    mpPseudoWordEvents.ResumeXMLEvents);
            }
        }

        public static void InsertDraftStamp()
        {
            try
            {
                //get current document
                Word.Document oDoc = null;
                try
                {
                    oDoc = Session.CurrentWordApp.ActiveDocument;
                }
                catch { }
                if (oDoc == null)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_ActiveDocumentRequired"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //suppress handling of Word XML Event handlers
                //because this is session is being hosted in a 
                //process outside IBF - there is no way to control
                //IgnoreWordXMLEvents
                SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                    mpPseudoWordEvents.SuppressXMLEvents);
                LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
                oEnv.SaveState();
                DocumentStampUI oForm = new DocumentStampUI(DocumentStampUI.Mode.DocumentStamp);
                DialogResult iRet = oForm.ShowDialog(oDoc, true, false);
                if (iRet == DialogResult.OK)
                {
                    System.Windows.Forms.Application.DoEvents();
                    oForm.Finish();
                }
                oEnv.RestoreState(true, false, false);
                if (iRet == DialogResult.OK && Session.CurrentWordVersion == 11)
                {
                    //Notify Taskpane to refresh tree
                    SendMessageToTaskPane(oDoc, mpPseudoWordEvents.ContentChanged);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                // GLOG : 8000 : ceh - set focus back to the document
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                    mpPseudoWordEvents.ResumeXMLEvents);
            }
        }
        /// <summary>
        /// Recreate Draft Stamp from existing
        /// </summary>
        //GLOG : 8178 : ceh
        public static void RecreateDraftStamp(object oDraftStamps, Prefill oDraftStampPrefill, int iScope)
        {
            InsertDraftStamp(oDraftStamps, oDraftStampPrefill, iScope);
        }

        /// <summary>
        /// aligns document text to existing pleading paper
        /// </summary>
        public static void AlignTextToPleadingPaper(LMP.Architect.Api.ForteDocument oMPDocument)
        {
            try
            {
                bool bEnableSelectionOptions = true;
                bool bEnableBodyOptions = true;
                bool bHasTaglessPleadingPaper = false;

                Word.Range oRng = null;
                Word.Range oBody = null;

                //get selection range
                Word.Range oSelRng = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Range;

                //get pleading body range
                //GLOG : 8254 : ceh
                //might be instances where Body cc has been deleted
                //and the tags collection wasn't refreshed
                try
                {
                    oBody = PleadingPaper.GetBodyFromSelection(oMPDocument);
                }
                catch {}
                
                
                //get pleading paper object
                LMP.Forte.MSWord.PleadingPaper oPPaper = new LMP.Forte.MSWord.PleadingPaper();

                //check if there is a pleading body
                if (oBody == null)
                {
                    //GLOG 5939 jsw:  might be a tagless pleading
                    //check if there is pleading body
                    oBody = oPPaper.GetTaglessPleadingBodyRange();
                    if (oBody == null)
                    {
                        bEnableBodyOptions = false;

                        //no pleading body, but there may be tagless pleading paper
                        bHasTaglessPleadingPaper = oPPaper.HasPleadingPaper();

                    }
                    else
                        //if there is a tagless pleading body, there is also pleading paper
                        bHasTaglessPleadingPaper = true;
                }

                if (!oMPDocument.ContainsSegmentOfType(mpObjectTypes.PleadingPaper) && bHasTaglessPleadingPaper == false)
                {
                    string xMsg = LMP.Resources.GetLangString("Prompt_FunctionNotAvailableBecauseNoPleadingPaper");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //GLOG 6068 jsw:  check if pleading paper line spacing is 24 pt.
                if (!oPPaper.Has24PtLineSpacing())
                {
                    string xMsg = LMP.Resources.GetLangString("Prompt_FunctionNotAvailableBecauseNo24PtPleadingPaper");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                
                //GLOG 5359:  if the first or last paragraph in the body range is selected
                //by double-clicking, paragraph start is oBody.Start - 1 or oBody.End + 2
                if (oBody != null && !(oSelRng.Start >= (oBody.Start -1) && oSelRng.End <= (oBody.End + 2)))
                {
                    //selection is not within pleading body
                    //do not display selection range options
                    bEnableSelectionOptions = false;
                }

                //Get alignment range choice from user
                AlignPleadingPaperTextForm oForm = new AlignPleadingPaperTextForm(bEnableSelectionOptions, bEnableBodyOptions);

                // If no text is selected in the document
                oForm.UseSelectedTextOption = bEnableSelectionOptions && ((oSelRng.End - oSelRng.Start) > 0);
                oForm.ShowDialog();

                if (!oForm.Cancelled)
                {
                    switch (oForm.Scope)
                    {
                        case PleadingPaper.mpPleadingPaperAlignmentScopes.PleadingBody:
                            oRng = oBody;
                            break;
                        case PleadingPaper.mpPleadingPaperAlignmentScopes.Paragraph:
                            oRng = oSelRng.Paragraphs[1].Range;
                            break;
                        case PleadingPaper.mpPleadingPaperAlignmentScopes.Selection:
                            oRng = oSelRng;
                            break;
                        case PleadingPaper.mpPleadingPaperAlignmentScopes.ToSignature:
                            oRng = oSelRng;
                            oRng.SetRange(oSelRng.Start, oBody.End);
                            break;
                    }
                    //GLOG 6827: Make sure COM function doesn't trigger XML Event
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    try
                    {
                        PleadingPaper.AlignText(oRng);
                    }
                    finally
                    {
                        //GLOG 6827: Restore event-handling
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    }
                }
                else
                {
                    //GLOG : 8000 : ceh - set focus back to the document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }
                //oUndo.MarkEnd();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// inserts the admin segment with the specified 
        /// definition using the specified options
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="iLocation"></param>
        /// <param name="iBehavior"></param>
        public static void InsertSegment(AdminSegmentDef oDef,
            Segment.InsertionLocations iLocation, Segment.InsertionBehaviors iBehavior)
        {
            //GLOG 4472: Use Static InsertInNewDocument if appropriate, 
            //so that a TaskPane object is not required
            string xID = oDef.ID.ToString();
            try
            {
                if (iLocation == Segment.InsertionLocations.InsertInNewDocument ||
                    LMP.MacPac.Session.CurrentWordApp.Documents.Count == 0)
                    TaskPane.InsertSegmentInNewDoc(xID, null, "");
                else
                {
                    TaskPane oTaskPane = (TaskPane)TaskPanes.Item(
                        LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
                    oTaskPane.InsertSegmentInCurrentDoc(xID, null, "",
                        iLocation, iBehavior, oDef);
                }
            }
            //GLOG : 8081 : jsw
            //if there is a user friendly message, show it.
            catch (LMP.Exceptions.SegmentInsertionException oE)
            {
                MessageBox.Show(oE.Message, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                    throw new LMP.Exceptions.SegmentException(
                       LMP.Resources.GetLangString("Error_Segment_Insertion") +
                        " " + xID, oE);
            }

        }
        /// <summary>
        /// inserts the user segment with the specified 
        /// definition using the specified options
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="iLocation"></param>
        /// <param name="iBehavior"></param>
        public static void InsertSegment(UserSegmentDef oDef,
            Segment.InsertionLocations iLocation, Segment.InsertionBehaviors iBehavior)
        {
            //GLOG 4472: Use Static InsertInNewDocument if appropriate, 
            //so that a TaskPane object is not required
            string xID = oDef.ID;
            try
            {
                if (iLocation == Segment.InsertionLocations.InsertInNewDocument ||
                    LMP.MacPac.Session.CurrentWordApp.Documents.Count == 0)
                    TaskPane.InsertSegmentInNewDoc(xID, null, "");
                else
                {
                    TaskPane oTaskPane = (TaskPane)TaskPanes.Item(
                        LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
                    oTaskPane.InsertSegmentInCurrentDoc(xID, null, "",
                        iLocation, iBehavior, oDef);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Insertion") +
                    " " + xID, oE);
            }
        }
        /// <summary>
        /// inserts the segment associated with the specified variable set
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="iLocation"></param>
        /// <param name="iBehavior"></param>
        public static void InsertSegment(VariableSetDef oDef,
            Segment.InsertionLocations iLocation, Segment.InsertionBehaviors iBehavior)
        {
            //GLOG 4472: restructured so that existing TaskPane is not required
            //for insertion in a new document
            try
            {
                Prefill oPrefill = new Prefill(oDef.ContentString, oDef.Name, oDef.SegmentID);
                string xID = oDef.SegmentID;

                if (!xID.Contains(".") || xID.EndsWith(".0"))
                {
                    //Admin Segment
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    int iID1 = 0;
                    int iID2 = 0;
                    LMP.String.SplitID(xID, out iID1, out iID2);
                    AdminSegmentDef oAdminDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);
                    //GLOG 4695: Handler Data Interview VariableSet differently
                    if (oAdminDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                    {
                        TaskPane.InsertAnswerFile(xID, oPrefill, "");
                    }
                    else if (iLocation == Segment.InsertionLocations.InsertInNewDocument ||
                        LMP.MacPac.Session.CurrentWordApp.Documents.Count == 0)
                        TaskPane.InsertSegmentInNewDoc(xID, oPrefill, "");
                    else
                    {
                        TaskPane oTaskPane = (TaskPane)TaskPanes.Item(
                            LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
                        oTaskPane.InsertSegmentInCurrentDoc(xID, oPrefill, "",
                            iLocation, iBehavior, oAdminDef);
                    }
                }
                else
                {
                    //user segment
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User,
                        LMP.MacPac.Session.CurrentUser.ID);

                    UserSegmentDef oUserDef = (UserSegmentDef)oDefs.ItemFromID(xID);

                    if (iLocation == Segment.InsertionLocations.InsertInNewDocument ||
                        LMP.MacPac.Session.CurrentWordApp.Documents.Count == 0)
                        TaskPane.InsertSegmentInNewDoc(xID, oPrefill, "");
                    else
                    {
                        TaskPane oTaskPane = (TaskPane)TaskPanes.Item(
                            LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
                        oTaskPane.InsertSegmentInCurrentDoc(xID, oPrefill, "",
                            iLocation, iBehavior, oUserDef);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Insertion") +
                    " " + oDef.SegmentID, oE);
            }
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Handle update of the contacts, author and letterhead content.
        /// </summary>
        public static void UpdateDocumentContent()
        {
            //5-30-11 (dm) - activate if necessary - disallow if any
            //segment is non-activatable
            TaskPane oTP = TaskPanes.Item(LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveDocument);
            if (oTP != null)
            {
                for (int i = 0; i < oTP.ForteDocument.Segments.Count; i++)
                {
                    bool bFailed = false;
                    oTP.ForteDocument.Segments[i].ActivateIfNecessary(ref bFailed);
                    if (bFailed)
                    {
                        string xMsg = "Prompt_FunctionNotAvailableBecauseSegmentCannotBeActivated";
                        MessageBox.Show(LMP.Resources.GetLangString(xMsg), LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }

            UpdateDocumentForm oForm = new UpdateDocumentForm();

            if (oForm.ShowDialog() == DialogResult.OK)
            {
                System.Windows.Forms.Application.DoEvents();

                if (oForm.UpdateAuthor)
                {
                    UpdateAuthor();
                }

                if (oForm.UpdateContacts)
                {
                    UpdateDocumentContactDetail();
                }

                if (oForm.UpdateLetterhead)
                {
                    UpdateLetterhead();
                }

                SetXMLTagStateForEditing();
            }
            else
            {
                //GLOG : 8000 : ceh - set focus back to the document
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
        }

        /// <summary>
        /// GLOG : 3135 :JAB
        /// Update the document content in one iterative loop.
        /// </summary>
        /// <param name="bUpdateAuthor"></param>
        /// <param name="bUpdateContacts"></param>
        /// <param name="bUpdateLetterhead"></param>
        private static void UpdateDocumentContent(bool bUpdateAuthor, bool bUpdateContacts, bool bUpdateLetterhead)
        {
            TaskPane oTP = TaskPanes.Item(LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveDocument);

            if (oTP != null)
            {
                if (bUpdateContacts)
                {
                    UpdateDocumentContactDetail();
                }

                if (oTP.ForteDocument.Segments != null)
                {
                    for (int i = 0; i < oTP.ForteDocument.Segments.Count; i++)
                    {
                        if (bUpdateAuthor)
                        {
                            UpdateAuthor(oTP.ForteDocument.Segments[i]);
                        }

                        if (bUpdateLetterhead)
                        {
                            UpdateLetterhead(oTP.ForteDocument.Segments[i]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Update the author content.
        /// </summary>
        public static void UpdateAuthor()
        {
            TaskPane oTP = TaskPanes.Item(LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveDocument);

            if (oTP != null)
            {
                if (oTP.ForteDocument.Segments != null)
                {
                    try
                    {
                        oTP.ScreenUpdating = false;
                        for (int i = 0; i < oTP.ForteDocument.Segments.Count; i++)
                        {
                            UpdateAuthor(oTP.ForteDocument.Segments[i]);
                        }
                    }
                    finally
                    {
                        SetXMLTagStateForEditing();
                        oTP.ScreenUpdating = true;
                    }
                }
            }
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Update the author for the given segment.
        /// </summary>
        /// <param name="oSeg"></param>
        private static void UpdateAuthor(Segment oSeg)
        {
            if (oSeg != null)
            {
                Authors oAuthors = oSeg.Authors;
                oAuthors.Refresh();

                // Recursively update the author for each child segment.
                for (int i = 0; oSeg.Segments != null && i < oSeg.Segments.Count; i++)
                {
                    UpdateAuthor(oSeg.Segments[i]);
                }
            }
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Update the letter head for all the segments in this content.
        /// </summary>
        public static void UpdateLetterhead()
        {
            TaskPane oTP = TaskPanes.Item(LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveDocument);

            if (oTP != null)
            {
                if (oTP.ForteDocument.Segments != null)
                {
                    try
                    {
                        oTP.ScreenUpdating = false;

                        for (int i = 0; i < oTP.ForteDocument.Segments.Count; i++)
                        {
                            UpdateLetterhead(oTP.ForteDocument.Segments[i]);
                        }
                    }
                    finally
                    {
                        oTP.ScreenUpdating = true;
                    }
                }
            }
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Update the letter head for the given segments.
        /// </summary>
        /// <param name="oSeg"></param>
        private static void UpdateLetterhead(Segment oSeg)
        {
            if (oSeg != null)
            {
                if (oSeg is Letterhead)
                {
                    Word.Document oDoc = oSeg.ForteDocument.WordDocument;
                    LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);

                    oEnv.SaveState();

                    Letterhead oLetterhead = (Letterhead)oSeg;

                    try
                    {
                        oLetterhead.RefreshContent();
                    }
                    finally
                    {
                        oEnv.RestoreState(true, true, false);
                    }
                }

                if (oSeg.Segments != null)
                {
                    for (int i = 0; i < oSeg.Segments.Count; i++)
                    {
                        UpdateLetterhead(oSeg.Segments[i]);
                    }
                }
            }
        }

        /// <summary>
        /// updates all contact detail in the document
        /// </summary>
        public static void UpdateDocumentContactDetail()
        {
            TaskPane oTP = TaskPanes.Item(LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveDocument);

            if (oTP != null)
            {
                if (oTP.ForteDocument.Segments.Count != 0)
                {
                    try
                    {
                        oTP.ScreenUpdating = false;
                        bool bDetailUpdated = oTP.ForteDocument.UpdateContactDetail();
                        if (bDetailUpdated)
                            LMP.Forte.MSWord.GlobalMethods.CurWordApp.StatusBar = LMP.Resources.GetLangString("Msg_ContactDetailUpdated");
                        else
                            LMP.Forte.MSWord.GlobalMethods.CurWordApp.StatusBar = LMP.Resources.GetLangString("Msg_ContactInformationNotFound");
                    }
                    finally
                    {
                        oTP.ScreenUpdating = true;
                    }
                }
                else
                    //no Forte content
                    LMP.Forte.MSWord.GlobalMethods.CurWordApp.StatusBar = LMP.Resources.GetLangString("Msg_ContactInformationNotFound");
            }
            else
                //no Forte content
                LMP.Forte.MSWord.GlobalMethods.CurWordApp.StatusBar = LMP.Resources.GetLangString("Msg_ContactInformationNotFound");
        }
        public static void ExecuteOnDemandSync()
        {
            ExecuteOnDemandSync(false);
        }
        /// <summary>
        /// synchronizes the current user's local data
        /// with the network database
        /// </summary>
        public static void ExecuteOnDemandSync(Boolean bOnExit)
        {
            if (Session.AdminMode)
            {
                MessageBox.Show("On demand synchronization is not available when logged into " + 
                    LMP.ComponentProperties.ProductName + " as an Administrator.",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }
            else if (LMP.Registry.GetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing") == "1")
            {
                //Another sync is already in progress
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_OnDemandSyncNotAllowedDuringUserSync"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //get connection parameters
            //GLOG 3523: Run sync as Login User, even if currently proxying
            int iUserID = LMP.MacPac.Session.LoginUser.ID;
            FirmApplicationSettings oSettings = new FirmApplicationSettings(iUserID);
            string xServer = oSettings.NetworkDatabaseServer;
            string xDatabase = oSettings.NetworkDatabaseName;
            string xIISAddress = oSettings.IISServerIPAddress;
            string xServerAppName = oSettings.ServerApplicationName;
            string xDateCreated = LMP.Data.Application.GetMetadata("DateCreated");

            try
            {
                ////JTS 08/12/04: Start DesktopSyncProcess passing arguments for On-Demand sync
                //string xArgs = "/u:" + iUserID + " /s:" + xServer + " /d:" + xDatabase +
                //        " /i:" + xIISAddress + " /c:" + xDateCreated + " /o";

                //System.Diagnostics.Process.Start(
                //    new ProcessStartInfo(LMP.Data.Application.AppDirectory +
                //    @"\MPDesktopSyncProcess.exe", xArgs));

                m_bOnDemandSyncExecuting = true;

                InvalidateMacPacRibbonControl("Synchronize");

                //call on demand sync asynchronously
                UserSyncClient oSyncClient = new UserSyncClient(iUserID, xServer,
                    xDatabase, xIISAddress, xServerAppName, xDateCreated, true);
                if (!bOnExit)
                {
                    OnDemandSyncHandler o = new OnDemandSyncHandler(oSyncClient.ExecuteOnDemandSync);

                    AsyncCallback oCallback = new AsyncCallback(OnOnDemandSyncComplete);
                    o.BeginInvoke(oCallback, o);
                }
                else
                {
                    //GLOG 3623: Perform Upload-only sync on exit of Word
                    oSyncClient.ExecuteOnDemandSync(true);
                }
            }
            catch (LMP.Exceptions.ServerException oE)
            {
                if (!bOnExit)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Message_NoServerConnection"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                    m_bOnDemandSyncExecuting = false;
                    InvalidateMacPacRibbonControl("Synchronize");
                }
            }
            catch (System.Exception oE)
            {
                m_bOnDemandSyncExecuting = false;
                InvalidateMacPacRibbonControl("Synchronize");
                throw oE;
            }
            finally
            {
                //GLOG 4192: This is now handled by Callback
                //m_bOnDemandSyncExecuting = false;
                //InvalidateMacPacRibbonControl("Synchronize");
            }
        }
        public static void ReleaseCIObjects()
        {
            //JTS 3/21/10: Release CI COM objects from memory
            try
            {
                if (LMP.Data.Application.CISession != null)
                {
                    //Marshal.ReleaseComObject((object)LMP.Data.Application.CISession);
                    LMP.Data.Application.CISession = null;
                }
            }
            catch { }
        }
        private static void OnOnDemandSyncComplete(IAsyncResult oRes)
        {
            //GLOG 6074: Deal with unhandled exception if IIS Server is not available
            //GLOG 6463: Rearranged to try and avoid Resources issue resulting in Red X in TaskPane
            try
            {
                m_bOnDemandSyncExecuting = false;
                ((OnDemandSyncHandler)oRes.AsyncState).EndInvoke(oRes);
            }
            catch
            {
                MessageBox.Show(LMP.Resources.GetLangString("Message_NoServerConnection"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }
            finally
            {
                InvalidateMacPacRibbon();
            }
            try
            {
                //GLOG #3551: refresh task pane
                LMP.MacPac.TaskPanes.RefreshAll(true, false, false);
            }
            catch { }
            //Don't need to account for Word 2003 anymore
            //if (LMP.MacPac.Session.CurrentWordVersion == 11)
            //{
            //    //JTS 12/06/08: Get Handle to Word Window for displaying Messagebox
            //    IntPtr iHandle = LMP.OS.GetWordHandle();

            //    if (iHandle != IntPtr.Zero)
            //    {
            //        //alert user that sync is complete
            //        MessageBox.Show(new WindowWrapper(iHandle),
            //            LMP.Resources.GetLangString("Msg_OnDemandSyncComplete"),
            //            LMP.ComponentProperties.ProductName,
            //            MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        //Set flag so additional messages won't appear this session
            //        Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "SyncDBWarningShown", "1");
            //    }
            //}

        }
        public static void ConvertSegmentToUseSupportedLanguages()
        {
            //determine if segment defines the legacy Language variable

            //if so
            
                //determine supported languages
                //convert all instances of [Variables_Language] to [SegmentLanguageName]
                
            //must determine default language
        }

        /// <summary>
        /// Show the administrator.
        /// </summary>
        /// Glog : 2386
        public static void ShowAdministrator()
        {
            // GLOG : 2386 : JAB
            // Introduce a boolean variable to delay access to the handler of the Administrator startup.
            // This is necessary because multiple entry into this method results in the Administrator
            // starting but not acquiring focus (ie, it stays minimized even when clicking the Administrator
            // button. Allowing the Administrator to complete its startup solves this issue.
            bool bDelay = false;

            // GLOG : 2386 : JAB
            // Control access to the handler of the Administrator startup.
            if (bAdministratorStartComplete)
            {
                // GLOG : 2386 : JAB
                // Lock out reentry into this method.
                bAdministratorStartComplete = false;

                try
                {
                    if (m_oAdministratorProcess == null || (m_oAdministratorProcess != null && m_oAdministratorProcess.HasExited))
                    {
                        m_oAdministratorProcess = System.Diagnostics.Process.Start(
                            new ProcessStartInfo(LMP.Data.Application.AppDirectory +
                            @"\ForteAdministrator.exe"));

                        // GLOG : 2386 : JAB
                        // Delay granting access to the handler of the Administrator startup. This allows the
                        // Administrator to complete its startup.
                        bDelay = true;
                    }
                    else
                    {
                        if (m_oAdministratorProcess != null && !m_oAdministratorProcess.HasExited)
                        {
                            IntPtr hWnd = m_oAdministratorProcess.MainWindowHandle;
                            // Normalize administrator app window just in case it was minimized.
                            Normalize(hWnd.ToInt32());

                            // Bring the administrator app into focus.
                            SetForegroundWindow(hWnd);
                        }
                    }

                }
                catch (System.Exception oE)
                {
                    //Ignore error caused by answering No to Windows security prompt
                    if (oE.Message != "The operation was canceled by the user")
                        throw oE;
                }

                // GLOG : 2386 : JAB
                // Delay access to the handler of the Administrator startup by 3 seconds.
                if (bDelay)
                {
                    System.Threading.Thread.Sleep(3000);
                    bDelay = false;
                }

                // GLOG : 2386 : JAB
                // Grant access to the handler of the Administrator.
                bAdministratorStartComplete = true;
            }
        }

        /// <summary>
        /// Show the specified window.
        /// </summary>
        /// <param name="handle"></param>
        /// Glog : 2386
        private static void Normalize(Int32 handle)
        {
            if (!IsWindowMaximized(handle))
            {
                // If the window is minimized (or if it is in a Normal state)
                // make the window state Normal.
                ShowWindow(handle, SW_SHOWNORMAL);
            }
        }

        /// <summary>
        /// Determine if the specified window is maximized.
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        /// Glog : 2386
        private static bool IsWindowMaximized(Int32 handle)
        {
            return (IsZoomed(handle) != 0);
        }

        /// <summary>
        /// p/invoke to determine if the specified window minimized or maximized. 
        /// </summary>
        /// <param name="hwnd"></param>
        /// <returns></returns>
        /// Glog : 2386
        [DllImport("user32")]
        private static extern int IsZoomed(int hwnd);

        /// <summary>
        /// p/invoke to show the specified window.
        /// </summary>
        /// <param name="hwnd">windowhandle</param>
        /// <param name="nCmdShow">parameter to set a special state</param>
        /// <returns></returns>
        /// Glog : 2386
        [DllImport("user32")]
        private static extern int ShowWindow(int hwnd, int nCmdShow);

        /// <summary>
        /// p/invoke to bring the specified window into focus.
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        /// Glog : 2386
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// returns true if MacPac is legally installed
        /// </summary>
        /// <returns>bool</returns>
        public static bool MacPacIsLegal()
        {
            DateTime d = DateTime.MinValue;
            string xCode = null;
            bool bIsValid = false;
            LMP.MacPac.Encrypt oEncrypt = new LMP.MacPac.Encrypt();
            TimeSpan oTSpan = new TimeSpan(); ;

            //get license key from registry
            xCode = Registry.GetMacPac10Value("LicNo");

            if (!string.IsNullOrEmpty(xCode))
            {
                //unencrypt expiration date
                //GLOG 8318 handle error if value isn't a valid date
                try
                {
                    d = oEncrypt.UnEncryptDate(xCode);
                }
                catch
                {
                    return false;
                }


                oTSpan = d.Subtract(DateTime.Now);

                //valid key?
                //permanent license represents an English date (GLOG 2965)
                //GLOG 4998: Compare individual properties rather than against a 
                //specific date format to avoid regional compatibility issues
                //in case the default short date format has been changed
                //GLOG : 8246 : JSW
                bIsValid = ((d.Day == 1 && d.Month == 4 && d.Year == 2099) ||
                    (d > DateTime.Now && oTSpan.Days < 366));
            }

            //load license key prompt
            if (!bIsValid)
            {
                //LMP.MacPac.Session.AddinIsValid = false;
                //GLOG 8318: Don't prompt for License, since in general users won't have necessary permissions to write to HKLM
                //LMP.MacPac.LicenseNumberForm oForm =
                //    new LMP.MacPac.LicenseNumberForm();

                //oForm.ShowDialog();

                //if (!oForm.Cancelled)
                //{
                //    //unencrypt date
                //    d = oEncrypt.UnEncryptDate(oForm.Key);

                //    //test for validity
                //    bIsValid = (d == DateTime.Parse("4/1/2099", LMP.Culture.USEnglishCulture) ||
                //        (d > DateTime.Now && oTSpan.Days < 183));

                //    //write to registry if valid
                //    if (bIsValid)
                //        LMP.Registry.SetLocalMachineValue(
                //            LMP.Registry.MacPac10RegistryRoot, "LicNo", oForm.Key);
                //    else
                //        //set message for next time
                //        oForm.Message = LMP.Resources.GetLangString(
                //            "Dialog_ControlPropertyInvalidLicenseKey");
                //}
            }
            else
            {
                //GLOG 8207: Warn if eval is nearing expiration
                if (oTSpan.Days < 7)
                {
                    string xMsg = string.Format(LMP.Resources.GetLangString("Prompt_EvalNearingExpiration"), LMP.ComponentProperties.ProductName, d.ToString(), oTSpan.Days.ToString());
                    ConditionalPromptDialog oDlg = new ConditionalPromptDialog();
                    oDlg.Text = LMP.ComponentProperties.ProductName;
                    oDlg.ShowDialog(xMsg, "Do not display this warning again", "SkipEvalWarning");
                }
            }
            //GLOG : 8318 : jsw
            //set property so Session.cs can access it
            LicenseIsValid = bIsValid;
            return bIsValid;
        }

        /// <summary>
        /// returns true if a segment is open in Design
        /// </summary>
        /// <returns>bool</returns>
        public static bool ContainsSegmentInDesign()
        {
            bool bRet = false;

            //get all the current task panes
            List<LMP.MacPac.TaskPane> oTP = CurrentTaskPanes();

            //for each task pane, check to see if segment is already opened in design
            for (int i = 0; i < oTP.Count; i++)
            {
                if (oTP[i].Mode == ForteDocument.Modes.Design)
                {
                    bRet = true;
                    return bRet;
                }
            }
            return bRet;
        }


        //GLOG item #4850 - jsw
        /// <summary>
        /// copies startup templates to startup template folder 
        /// if they are not already there
        /// </summary>
        /// <returns>void</returns> 
        public static void CopyStartupTemplates()
        {
            string xStartupTemplateFolder;
            string xUserCache;

            //get startup template directory
            xStartupTemplateFolder = LMP.Data.Application.GetDirectory(mpDirectories.WordStartup);

            //GLOG 5630:  Don't attempt copy if Registry location isn't specified
            if (string.IsNullOrEmpty(xStartupTemplateFolder))
                return;

            try
            {
                //GLOG 5714: Copy any templates from UserCache that don't exist in Startup or have an earlier Timestamp in Startup
                xUserCache = LMP.Data.Application.GetDirectory(mpDirectories.UserCache);
                if (!string.IsNullOrEmpty(xUserCache) && Directory.Exists(xUserCache))
                {
                    DirectoryInfo oDir = new DirectoryInfo(xUserCache);
                    FileInfo[] oFiles = oDir.GetFiles("*.dot*");
                    foreach (FileInfo oFile in oFiles)
                    {
                        //Don't include ForteNormal.dotx
                        //GLOG 8445
                        if (oFile.Name.ToUpper().StartsWith("MACPACNORMAL") || oFile.Name.ToUpper().StartsWith("FORTENORMAL"))
                            continue;

                        bool bCopy = false;
                        bool bExists = false;
                        bool bReadOnly = false;
                        string xTargetPath = xStartupTemplateFolder + "\\" + oFile.Name;
                        if (File.Exists(xTargetPath))
                        {
                            bExists = true;
                            FileAttributes oAttr = File.GetAttributes(xTargetPath);
                            bReadOnly = (oAttr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly;

                            //Copy file from UserCache if it has later date than corresponding file in Startup Location
                            if (File.GetLastWriteTimeUtc(xTargetPath) < oFile.LastWriteTimeUtc)
                                bCopy = true;
                        }
                        else
                            //Copy file from UserCache if it doesn't exist in Startup location
                            bCopy = true;
                        if (bCopy)
                        {
                            object oTrue = true;
                            object oTarget = xTargetPath;
                            //Need to unload existing Addin so file can be overwritten
                            if (bExists)
                            {
                                try
                                {
                                    LMP.Forte.MSWord.GlobalMethods.CurWordApp.AddIns.get_Item(ref oTarget).Delete();
                                }
                                catch { }
                                //Need to clear ReadOnly attribute of MacPac Numbering.dotm to allow overwriting
                                if (bReadOnly)
                                    File.SetAttributes(xTargetPath, File.GetAttributes(xTargetPath) & ~FileAttributes.ReadOnly);
                            }
                            File.Copy(oFile.DirectoryName + "\\" + oFile.Name, xTargetPath, true);
                            try
                            {
                                //Load Addin in current Word session
                                LMP.Forte.MSWord.GlobalMethods.CurWordApp.AddIns.Add(xTargetPath, ref oTrue);
                            }
                            catch { }
                        }
                    }
                }
            }
            catch
            {
            }
        }
        /// copies startup templates to startup template folder 
        /// if they are not already there
        /// </summary>
        /// <returns>void</returns> 
        public static void CopyMPNormal()
        {
            string xWordUserTemplateFolder;
            bool bMPNormalPresent = false;
            string xUserCache;

            //get startup template directory
            xWordUserTemplateFolder = LMP.Data.Application.GetDirectory(mpDirectories.WordUser);

            //check if Forte startup templates are there
            if (File.Exists(xWordUserTemplateFolder + "\\ForteNormal.dotx"))
                bMPNormalPresent = true;

            try
            {
                //if not, copy to startup template folder
                if (bMPNormalPresent == false)
                {
                    xUserCache = LMP.Data.Application.GetDirectory(mpDirectories.UserCache);
                    File.Copy(xUserCache + "\\ForteNormal.dotx", xWordUserTemplateFolder + "ForteNormal.dotx", false);
                }
            }
            catch { }
        }
        public static bool ShouldShowTaskPaneForProtectedDoc(ForteDocument oForteDoc)
        {
            //GLOG 15875: TaskPane will be displayed for protected form segment only if it meets these criteria:
            //  -- Form password is blank or matches one of the Admin passwords
            //  -- contains only Tagless variables and one or more of those is displayed in the Editor
            bool bShowTaskPane = true;
            Word.Document oDoc = oForteDoc.WordDocument;
            if (oDoc != null)
            {
                if (oDoc.ProtectionType != MSWord.WdProtectionType.wdNoProtection)
                {
                    //If document with Segments is already protected, continue showing TaskPane
                    return oForteDoc.Segments.Count > 0;
                }
                else
                {
                    bShowTaskPane = true;
                    for (int i = 0; i < oForteDoc.Segments.Count; i++)
                    {
                        Segment oSeg = oForteDoc.Segments[i];
                        if (!string.IsNullOrEmpty(oSeg.ProtectedFormPassword))
                        {
                            bShowTaskPane = false;
                            string xPwd = oSeg.ProtectedFormPassword;
                            FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                            List<string> oPasswords = new List<string>(oSettings.ProtectedDocumentsPassword.Split(LMP.StringArray.mpEndOfSubField));
                            if (xPwd != "" && (xPwd.ToUpper() == "[EMPTY]" || oPasswords.Contains(xPwd)))
                            {
                                for (int v = 0; v < oSeg.Variables.Count; v++)
                                {
                                    Variable oVar = oSeg.Variables[v];
                                    if (!oVar.IsTagless)
                                    {
                                        return false;
                                    }
                                    else if (oVar.DisplayIn != Architect.Base.Variable.ControlHosts.None)
                                    {
                                        bShowTaskPane = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return bShowTaskPane;
        }
        //public static void ShowTaskPane2003()
        //{
        //    Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
        //    //Can't show Task Pane for protected document
        //    if (oDoc == null || oDoc.ProtectionType
        //        != Word.WdProtectionType.wdNoProtection)
        //    {
        //        return;
        //    }
        //    //Check if there's already a task pane
        //    if (!SendMessageToTaskPane(oDoc, mpPseudoWordEvents.QueryExists))
        //    {
        //        if (!LMP.Forte.MSWord.WordDoc.IsForteDoc(oDoc))
        //            LMP.Forte.MSWord.WordDoc.AddMP10SchemaReference(oDoc);
        //        LMP.Forte.MSWord.WordApp.ShowTaskPaneWord11();
        //        System.Windows.Forms.Application.DoEvents();
        //    }
        //    else
        //    {
        //        //If so, make sure it's visible
        //        try
        //        {
        //            LMP.Forte.MSWord.GlobalMethods.CurWordApp.TaskPanes[
        //                Word.WdTaskPanes.wdTaskPaneDocumentActions].Visible = true;
        //        }
        //        catch
        //        {
        //            LMP.Forte.MSWord.WordApp.ShowTaskPaneWord11();
        //        }

        //        // GLOG : 2121 : JAB
        //        // Set the focus to the task pane.
        //        SendKeys.SendWait("{F10}^{TAB}");
        //    }
        //}
        public static void RefreshNormalTemplateStyles()
        {
        }
        public static void InsertTrailerIfNecessary()
        {
            InsertTrailerIfNecessary(false);
        }
        /// <summary>
        /// Display Trailer dialog if trailer is not up-to-date
        /// </summary>
        public static void InsertTrailerIfNecessary(bool bOnOpen)
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            if (oDoc == null)
                return;


            if (LMP.Forte.MSWord.WordApp.IsCompareResultsWindow(oDoc.ActiveWindow))
            {
                //need to select the main comparison window -
                //Word freezes below if the selection is in
                //a revision pane - dcf 7/21/11
                //GLOG 6288: Activate Pane before setting Document object -
                //otherwise ActiveDocument object may not be valid
                if (oDoc.ActiveWindow.ActivePane.Index > 1)
                {
                    oDoc.ActiveWindow.Panes[1].Activate();
                    oDoc = oDoc.ActiveWindow.ActivePane.Document;
                }
            }

            //GLOG 6567 (dm) - unremmed the compatibility mode condition
            //GLOG 7496 (dm) - removed condition that disallowed for .doc in Word 2013 -
            //we now allow trailer in .doc even when there's no support for xml tags
            if ((!ForteDocument.IsDocumentTrailerable(oDoc)) ||
                LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oDoc, false)) //GLOG 5443 (dm)
                return;

            //GLOG 7496 (dm) - turn on flag to treat all files as open xml
            //during trailer insertion process in Word 2013
            LMP.Forte.MSWord.Application oCOM = new LMP.Forte.MSWord.Application();
            try
            {
                LMP.Forte.MSWord.Application.SetTrailerInProgressFlag(true);

                if (bOnOpen && (Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningMPDoc
                                || Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningNonMPDoc))
                {
                    //JTS 5/27/09: If Document is being opened and TaskPane will display automatically,
                    //document variable will be set to have TaskPane.Setup launch zzmpInsertTrailerIfNecessary
                    //Otherwise in Word 2003, Setup doesn't get run at all
                    bool bSetVar = true;
                    if (!LMP.Forte.MSWord.WordDoc.IsForteDoc(oDoc))
                    {
                        //If no MP content, TaskPane won't show unless specified by user setting
                        if (!Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningNonMPDoc)
                            bSetVar = false;
                    }
                    else if (!LMP.Forte.MSWord.WordDoc.ContainsNonTrailerMPContent(oDoc))
                        //If no non-trailer MP content, TaskPane won't show
                        bSetVar = false;
                    else if (!Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningMPDoc)
                        //If MP content, don't show if not specified by user setting
                        bSetVar = false;
                    if (bSetVar)
                    {
                        //TaskPane will be displayed - set variable
                        object oVal = "true";
                        object oName = "zzmpTPInsertAutoTrailer";
                        oDoc.Variables.get_Item(ref oName).Value = "true";
                        return;
                    }
                    //Otherwise, Insert trailer immediately
                }

                ForteDocument oMPDoc = null;
                TaskPane oTP = TaskPanes.Item(oDoc);

                if (oTP != null)
                {
                    oMPDoc = oTP.ForteDocument;

                    //bounding objects need to be reconstituted before refreshing if the
                    //document has just been saved in the other file format (2/21/11 dm)
                    if (oMPDoc.Mode == ForteDocument.Modes.Edit)
                    {
                        mpFileFormats iDocFormat = oMPDoc.FileFormat;
                        LMP.Forte.MSWord.mpBoundingObjectTypes iBoundingObjectType = oMPDoc.Tags.BoundingObjectType;
                        if (((iDocFormat == mpFileFormats.OpenXML) &&
                            (iBoundingObjectType == LMP.Forte.MSWord.mpBoundingObjectTypes.WordXMLNodes)) ||
                            ((iDocFormat == mpFileFormats.Binary) &&
                            (iBoundingObjectType == LMP.Forte.MSWord.mpBoundingObjectTypes.ContentControls)))
                        {
                            ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                            LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                            //GLOG 6801 (dm): don't reconstitute mSEGs
                            oConvert.ReconstituteIfNecessary(oMPDoc.WordDocument, false, false);

                            oMPDoc.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes,
                                false, false);

                            for (int i = 0; i < oMPDoc.Segments.Count; i++)
                            {
                                oTP.DocEditor.UpdateSegmentImage(oMPDoc.Segments[i], true);
                            }

                            ForteDocument.IgnoreWordXMLEvents = iEvents;
                        }
                    }
                }
                else
                {
                    //freeze Word screen
                    LMP.WinAPI.LockWindowUpdate(LMP.WinAPI.FindWindow("OpusApp", 0));
                    LMP.Forte.MSWord.GlobalMethods.CurWordApp.ScreenUpdating = false;
                    bool bSaved = oDoc.Saved;
                    LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
                    oEnv.SaveState();
                    try
                    {
                        oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.Undefined, mpTriState.Undefined,
                            mpTriState.Undefined, mpTriState.Undefined, true);
                        oMPDoc = new ForteDocument(oDoc);
                        Word.Application oWord = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
                        //suppress handling of Word XML Event handlers
                        //because this is session is being hosted in a 
                        //process outside IBF - there is no way to control
                        //IgnoreWordXMLEvents
                        SendMessageToTaskPane(oDoc,
                            mpPseudoWordEvents.SuppressXMLEvents);
                        //SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                        //    mpPseudoWordEvents.SuppressXMLEvents);
                        //Need to refresh to be able to access Segment properties

                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        if (oMPDoc.Mode == ForteDocument.Modes.Design)
                            oMPDoc.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes, false, false);
                        else
                        {
                            //bounding objects need to be reconstituted before refreshing if the
                            //document has just been saved in the other file format (2/21/11 dm)
                            LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                            //GLOG 6801 (dm): don't reconstitute mSEGs
                            oConvert.ReconstituteIfNecessary(oMPDoc.WordDocument, false, false);

                            oMPDoc.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes, false, false);
                        }

                        LMP.MacPac.Application.SetXMLTagStateForEditing();
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                        SendMessageToTaskPane(oDoc,
                            mpPseudoWordEvents.ResumeXMLEvents);
                        //SendMessageToTaskPane(Session.CurrentWordApp.ActiveDocument,
                        //    mpPseudoWordEvents.ResumeXMLEvents);
                    }
                    catch (LMP.Exceptions.PasswordException)
                    {
                        //Document is password protected
                        return;
                    }
                    finally
                    {
                        //unfreeze Word screen
                        LMP.WinAPI.LockWindowUpdate(0);
                        LMP.Forte.MSWord.GlobalMethods.CurWordApp.ScreenUpdating = true;
                        //Restore saved state so 2nd save won't occur if no trailer update is required.
                        oEnv.RestoreState(true, false, false); //GLOG 4871
                        oDoc.Saved = bSaved;
                    }
                }
                Segment oTrailer = null;
                for (int i = 0; i < oMPDoc.Segments.Count; i++)
                {
                    if (oMPDoc.Segments[i].TypeID == mpObjectTypes.Trailer)
                    {
                        oTrailer = oMPDoc.Segments[i];
                        break;
                    }
                }
                Prefill oPrefill = DocumentStampUI.LastTrailerPrefill();

                bool bDo = (oTrailer == null) && (oPrefill == null);
                if (oTrailer != null)
                {
                    bool bShowRevisions = false;
                    bool bSaved = oDoc.Saved;
                    if (oTrailer.BelongsToCurrentClient)
                    {
                        LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
                        oEnv.SaveState();
                        try
                        {
                            //GLOG 5369: ShowRevisions not accessible in Protected Document
                            oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.Undefined, mpTriState.Undefined,
                                mpTriState.Undefined, mpTriState.Undefined, true);
                            //GLOG 3669: Turn off Revisions so deleted text won't affect comparison
                            bShowRevisions = oDoc.ShowRevisions;
                            if (bShowRevisions)
                                oDoc.ShowRevisions = false;

                            //Test if all DMS-related variables are current
                            for (int j = 0; j < oTrailer.Variables.Count; j++)
                            {
                                Variable oVar = oTrailer.Variables[j];
                                if ((oVar.DefaultValue.Contains("[DMS") || oVar.DefaultValue.ToUpper().Contains("[DOCUMENT_")) &&
                                    oVar.Value.ToUpper() != Expression.Evaluate(oVar.DefaultValue, oTrailer, oMPDoc).ToUpper())
                                {
                                    bDo = true;
                                    break;
                                }
                            }
                        }
                        catch (LMP.Exceptions.PasswordException)
                        {
                            //Document is password protected
                            return;
                        }
                        finally
                        {
                            //GLOG 3669: Restore revisions to original state
                            if (bShowRevisions)
                            {
                                oDoc.ShowRevisions = true;
                            }
                            //Restore saved state so 2nd save won't occur if no trailer update is required.
                            oEnv.RestoreState(true, false, false);
                            oDoc.Saved = bSaved;
                        }
                    }
                    else
                        //existing trailer belongs to a different client
                        bDo = true;
                }
                else if (oPrefill != null)
                {
                    //JTS 6/27/11: Bounding Objects have been stripped from trailer
                    //Compare Document ID of last inserted trailer with current Document ID
                    string xLastTrailer = DocumentStampUI.GetLastTrailerVariable();
                    if (xLastTrailer != "")
                    {
                        try
                        {
                            string[] aVals = xLastTrailer.Split(DocumentStampUI.mpPrefillVariableSeparator);
                            //Profile information has changed
                            if (aVals[3].ToUpper() != DocumentStampUI.GetCurrentDocID().ToUpper())
                                bDo = true;
                        }
                        catch
                        {
                            bDo = true;
                        }
                    }
                    else
                        bDo = true;
                }
                else
                {
                    //Check if No Trailer type was selected and if Doc ID has changed
                    string xDocID = DocumentStampUI.GetNoTrailerVariable();

                    //check for the "zzmp10Converted9xTrailerID" doc var that was added in 10.1.1.84 -
                    //prior to this we used "zzmp10NoTrailerPromptID" for both the "no trailer"
                    //type and converted 9.x trailers (dm 3/1/10)
                    if (xDocID == "")
                        xDocID = DocumentStampUI.GetConverted9xTrailerVariable();

                    if (xDocID != "" && (xDocID.ToUpper() == DocumentStampUI.GetCurrentDocID().ToUpper()))
                        bDo = false;
                }

                //GLOG - 3619 - CEH
                //GLOG - 3079 - CEH
                if (bDo)
                {
                    LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                    UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

                    //Use default trailer behavior if user choice not allowed or user settings have not been set
                    if (!oFirmSettings.AllowUsersToSetTrailerBehavior || oSettings.TrailerInsertionOption == 0)
                    {
                        if (oFirmSettings.DefaultTrailerBehavior != TrailerInsertionOptions.Manual)
                        {
                            InsertTrailer(oFirmSettings.DefaultTrailerBehavior == TrailerInsertionOptions.Prompt, true);
                        }
                    }
                    else
                    {
                        //we're using the user's setting -
                        //don't insert trailer if set to Manual
                        if (oSettings.TrailerInsertionOption != TrailerInsertionOptions.Manual)
                        {
                            //GLOG 4536: Indicate this is Auto Trailer
                            InsertTrailer(oSettings.TrailerInsertionOption, true);
                        }
                    }
                }
            }
            finally
            {
                //GLOG 7496 (dm) - turn off flag
                LMP.Forte.MSWord.Application.SetTrailerInProgressFlag(false);
            }
        }

        /// <summary>
        /// Recreate Trailer from existing
        /// </summary>
        //GLOG - 2476 - ceh
        //JTS 6/24/11: Modified to take Prefill argument instead of Segment
        //GLOG : 6262 : CEH - Modified to take 'object oTrailers' argument
        public static void RecreateTrailer(object oTrailers, Prefill oTrailerPrefill, int iScope)
        {
            InsertTrailer(oTrailers, oTrailerPrefill, iScope);
        }

        public static void FileSave()
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            if (oDoc == null)
                return;

            Word.Window oWindow = oDoc.ActiveWindow;

            TaskPane oTP = TaskPanes.Item(oDoc);

            if (oTP != null)
            {
                //Don't run DMS function if in Design mode
                if (oTP.Mode == ForteDocument.Modes.Design)
                    oTP.DocDesigner.SaveDesignIfNecessary(false, true);
                else
                    m_oDMS.FileSave();
            }
            else
            {
                m_oDMS.FileSave();
            }
        }
        public static void FileSaveAs()
        {
            m_oDMS.FileSaveAs();
        }
        public static void FileClose()
        {
            //GLOG 6851 (dm) - don't run DMS function if in Design mode
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            if (oDoc == null)
                return;

            Word.Window oWindow = oDoc.ActiveWindow;

            TaskPane oTP = TaskPanes.Item(oDoc);

            if (oTP != null)
            {
                if (oTP.Mode == ForteDocument.Modes.Design)
                    oTP.DocDesigner.CloseDocument();
                else
                    m_oDMS.FileClose();
            }
            else
            {
                m_oDMS.FileClose();
            }
        }
        public static void FileSaveAll()
        {
            m_oDMS.FileSaveAll();
        }
        public static void FileCloseAll()
        {
            m_oDMS.FileCloseAll();
        }
        public static void DocClose()
        {
            m_oDMS.DocClose();
        }
        public static void FileExit()
        {
            m_oDMS.FileExit();
        }
        public static void FilePrint()
        {
            m_oDMS.FilePrint();
        }
        public static void FilePrintDefault()
        {
            m_oDMS.FilePrintDefault();
        }
        public static void SetWordXMLEventHandling(Word.Document oDocument, bool bOn)
        {
            if (bOn)
                SendMessageToTaskPane(oDocument, mpPseudoWordEvents.ResumeXMLEvents);
            else
                SendMessageToTaskPane(oDocument, mpPseudoWordEvents.SuppressXMLEvents);
        }
        public static void SuppressWordXMLSelectionChangeEvent(Word.Document oDocument)
        {
            SendMessageToTaskPane(oDocument, mpPseudoWordEvents.SuppressXMLSelectionChangeEvent);
        }
        /// <summary>
        /// Prevent AddIn events from running during Designer updates
        /// </summary>
        public static bool DisableTaskpaneEvents
        {
            get { return m_bDisableTaskpaneEvents; }
            set { m_bDisableTaskpaneEvents = value; }
        }
        public static bool IsUserSyncInProgress(bool bShowWarning)
        {
            //true if registry entry indicating user sync in progress is set
            bool bExecuting = LMP.Registry.GetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing") == "1";
            //true if ForteSync.mdb exists (either due to executing sync or previously completed sync)
            bool bSyncDBExists = File.Exists(LMP.Data.Application.WritableDBDirectory + @"\ForteSync.mdb"); //JTS 3/28/13
            if (bShowWarning)
            {
                //Show appropriate warning message
                if (bExecuting)
                {
                    //sync currently in progress
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_ActionNotAllowedDuringUserSync"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (bSyncDBExists)
                {
                    //sync completed, but new user DB not loaded
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_ActionNotAllowedAfterUserSync"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            return bExecuting || bSyncDBExists;
        }

        /// <summary>
        /// updates value of variable associated with current Word selection
        /// </summary>
        public static void UpdateVariableValueFromSelection()
        {
            Word.Document oDoc = null;
            try
            {
                oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            }
            catch { }

            if (oDoc != null)
            {
                SendMessageToTaskPane(oDoc,
                    mpPseudoWordEvents.UpdateVariableValueFromSelection);
            }
        }

        //GLOG #2843 - dcf
        public static void Quit()
        {
            LMP.MacPac.Session.Quit();
        }
        public static void ShowAbout()
        {
            //show the "About" form
            AboutForm oForm = new AboutForm();
            oForm.ShowDialog();
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// returns the primary user of the application
        /// </summary>
        public static string PrimaryUser
        {
            get
            {
                try
                {
                    return Registry.GetLocalMachineValue(
                        LMP.Registry.MacPac10RegistryRoot, "PrimaryUser");
                }
                catch
                {
                    throw new LMP.Exceptions.RegistryKeyException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidOrMissingRegistrySetting") + "PrimaryUser");
                }
            }
        }
        /// <summary>
        /// returns true iff MacPac is on
        /// </summary>
        public static bool Paused
        {
            get { return m_bPaused; }
            set
            {
                m_bPaused = value;

                if (m_bPaused)
                {
                    //remove all taskpanes
                    TaskPane oTP = null;
                    foreach (Word.Document oDoc in LMP.MacPac.Session.CurrentWordApp.Documents)
                    {
                        try
                        {
                            oTP = TaskPanes.Item(oDoc);
                        }
                        catch { }

                        if (oTP != null)
                        {
                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                            TaskPanes.Remove(oDoc);
                        }

                    }
                }
            }
        }
        public static string BinDirectory
        {
            get
            {
                return LMP.Data.Application.AppDirectory;
            }
        }
        public static Microsoft.Office.Core.IRibbonUI MacPac10Ribbon
        {
            get { return m_oRibbon; }
            set { m_oRibbon = value; }
        }
		#endregion
        #region *********************IMP10WordCommands members*********************
        /// <summary>
        /// replaces Word's native EditPaste command as a workaround for native
        /// bug whereby XMLAfterInsert event is raised repeatedly (and
        /// sometimes endlessly) under a variety of inappropriate circumstances
        /// when a header/footer contains both an XMLNode and a Word field
        /// </summary>
        /// <param name="oDocument"></param>
        public void EditPaste(Word.Document oDocument)
        {
            Word.Application oApp = (Word.Application)oDocument.Parent;
            Word.Selection oSelection = null;

            //test to see whether there's a selection - user may be pasting
            //into a native dialog
            try
            {
                oSelection = oApp.Selection;
            }
            catch { }

            //GLOG 4483: Run native function if MacPac is Paused
            if (!MacPacIsPaused() && oSelection != null)
            {
                //send message to TaskPane that we're about to paste
                bool bMessageSent = SendMessageToTaskPane(oDocument,
                    mpPseudoWordEvents.BeforeMenuPaste);

                //execute paste - use SendKeys so as not to lose native paste options menu
                SendKeys.SendWait("^v");

                //send message that we're done with undo
                if (bMessageSent)
                    SendMessageToTaskPane(oDocument, mpPseudoWordEvents.AfterMenuPaste);
            }
            else
                //use WordBasic method
                LMP.Forte.MSWord.WordApp.EditPasteWordBasic();
        }
        /// <summary>
        /// replaces Word's native EditUndo command as a workaround for native
        /// bug whereby XMLAfterInsert event is raised repeatedly (and
        /// sometimes endlessly) under a variety of inappropriate circumstances
        /// when a header/footer contains both an XMLNode and a Word field
        /// </summary>
        /// <param name="oDocument"></param>
        public void EditUndo(Word.Document oDocument)
        {
            //GLOG 4483: Run native function without additional handling if MacPac is Paused
            if (!MacPacIsPaused())
            {
                //send message to TaskPane that we're about to undo
                bool bMessageSent = SendMessageToTaskPane(oDocument, mpPseudoWordEvents.BeforeUndo);

                //execute undo
                object oMissing = System.Reflection.Missing.Value;
                oDocument.Undo(ref oMissing);

                //tell it that we're done with undo
                if (bMessageSent)
                    SendMessageToTaskPane(oDocument, mpPseudoWordEvents.AfterUndo);
            }
            else
            {
                //execute undo
                object oMissing = System.Reflection.Missing.Value;
                oDocument.Undo(ref oMissing);
            }
        }
        /// <summary>
        /// replaces Word's native EditRedo command as a workaround for native
        /// bug whereby XMLAfterInsert event is raised repeatedly (and
        /// sometimes endlessly) under a variety of inappropriate circumstances
        /// when a header/footer contains both an XMLNode and a Word field
        /// </summary>
        /// <param name="oDocument"></param>
        public void EditRedo(Word.Document oDocument)
        {
            //GLOG 4483: Run native function without additional handling if MacPac is Paused
            if (!MacPacIsPaused())
            {
                //send message to TaskPane that we're about to redo
                bool bMessageSent = SendMessageToTaskPane(oDocument, mpPseudoWordEvents.BeforeUndo);

                //execute Redo
                object oMissing = System.Reflection.Missing.Value;
                oDocument.Redo(ref oMissing);

                //tell it that we're done with Redo
                if (bMessageSent)
                    SendMessageToTaskPane(oDocument, mpPseudoWordEvents.AfterRedo);
            }
            else
            {
                //execute Redo
                object oMissing = System.Reflection.Missing.Value;
                oDocument.Redo(ref oMissing);
            }
        }
        /// <summary>
        /// replaces Word's native NextCell command as a workaround for native
        /// bug whereby XMLAfterInsert event is raised repeatedly (and
        /// sometimes endlessly) under a variety of inappropriate circumstances
        /// when a header/footer contains both an XMLNode and a Word field
        /// </summary>
        /// <param name="oDocument"></param>
        public void NextCell(Word.Document oDocument)
        {
            //GLOG 4483: Run native function without additional handling if MacPac is Paused
            if (!MacPacIsPaused())
            {
                //send message to TaskPane that we're about to tab to next cell
                bool bMessageSent = SendMessageToTaskPane(
                        oDocument, mpPseudoWordEvents.BeforeNextCell);

                //TaskPane oTaskPane = TaskPanes.Item(oDocument);
                //ForteDocument.IgnoreWordXMLEvents = true;
                try
                {
                    //execute command
                    LMP.Forte.MSWord.WordDoc.NextCell();
                }

                finally
                {
                    //ForteDocument.IgnoreWordXMLEvents = false;

                    //tell it that we're done
                    if (bMessageSent)
                        SendMessageToTaskPane(oDocument,
                            mpPseudoWordEvents.AfterNextCell);
                }
            }
            else
                LMP.Forte.MSWord.WordDoc.NextCellWordBasic();

        }
        /// <summary>
        /// toggles a macpac forms checkbox
        /// </summary>
        public void ToggleFormsCheckbox()
        {
            try
            {
                LMP.Forte.MSWord.WordDoc.ToggleFormsCheckBox(
                    LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Range);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// toggles a macpac forms checkbox
        /// </summary>
        public void ToggleFormsCheckboxSet()
        {
            try
            {
                LMP.Forte.MSWord.WordDoc.ToggleFormsCheckboxSet(
                    LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Range);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private bool IsTaskPaneShowing
        {
            get
            {
                return m_bIsTaskPaneShowing;
            }

            set
            {
                m_bIsTaskPaneShowing = value;
            }
        }

        /// <summary>
        /// executes the specified MacPac 10 command
        /// </summary>
        /// <param name="Command"></param>
        public void ExecuteCommand(string Command)
        {
            try
            {
                Word.GlobalClass oGlobals = new Word.GlobalClass();
                Word.Application oWordApp = oGlobals.Application;

                if (Command == "WordAutoExec")
                {
                    //Clear any existing AdminMode key
                    LMP.Registry.SetCurrentUserValue(
                        @"Software\The Sackett Group\Deca", "AdminMode", "");
                    //GLOG 15900: Delay startup with modal form to avoid TaskPane display issue at Gowling
                    if (!string.IsNullOrEmpty( LMP.Registry.GetMacPac10Value("StartupDelay")))
                    {
                        int iWait = 0;
                        if (int.TryParse(LMP.Registry.GetMacPac10Value("StartupDelay"), out iWait))
                        {
                            LMP.Trace.WriteNameValuePairs("StartupDelay", iWait);
                            if (iWait > 0)
                            {
                                StartupForm oForm = new StartupForm();
                                oForm.TimerDelay = iWait;
                                oForm.OffScreen = true;
                                oForm.ShowDialog();
                                System.Windows.Forms.Application.DoEvents();
                            }
                        }
                    }
                    return;
                }
                else if (Command == "About")
                {
                    //show the "About" form
                    AboutForm oForm = new AboutForm();
                    oForm.ShowDialog();
                    return;
                }

                Session.StartIfNecessary(oWordApp);

                if (!Session.IsInitialized)
                {
                    //session is not initialized - run native file management functions
                    switch (Command)
                    {
                        case "FileSave":
                            LMP.Forte.MSWord.WordApp.FileSave();
                            return;
                        case "FileSaveAs":
                            LMP.Forte.MSWord.WordApp.FileSaveAs();
                            return;
                        case "FileClose":
                            LMP.Forte.MSWord.WordApp.FileClose();
                            return;
                        case "FileExit":
                            LMP.Forte.MSWord.WordApp.FileExit();
                            return;
                        case "DocClose":
                            LMP.Forte.MSWord.WordApp.DocClose();
                            return;
                        case "FileSaveAll":
                            LMP.Forte.MSWord.WordApp.FileSaveAll();
                            return;
                        case "FileCloseAll":
                            LMP.Forte.MSWord.WordApp.FileCloseAll();
                            return;
                        default:
                            return;
                    }
                }

                //GLOG 3491 - update variable value if necessary before a save or print -
                //this will ensure that all associated tags reflect the current value
                if (Command == "FileSave" || Command == "FileSaveAs" ||
                    Command == "FileSaveAll" || Command == "FileClose" ||
                    Command == "FileCloseAll" || Command == "DocClose" ||
                    Command == "FileExit" || Command == "FilePrint" ||
                    Command == "FilePrintDefault")
                    UpdateVariableValueFromSelection();

                FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                switch (Command)
                {
                    case "CreateBlankDocument":
                        LMP.MacPac.Application.CreateNewDocument();
                        //LMP.MacPac.Application.ShowTaskPane2003();
                        LMP.MacPac.Application.ForceProfileIfNecessary();
                        break;
                    case "ShowTaskPane":

                        if (oWordApp.Documents.Count == 0)
                        {
                            LMP.MacPac.Application.CreateNewDocument();
                            //LMP.MacPac.Application.ShowTaskPane2003();
                            LMP.MacPac.Application.ForceProfileIfNecessary();
                        }
                        else
                        {
                            if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                                return;
                            //LMP.MacPac.Application.ShowTaskPane2003();
                        }
                        IsTaskPaneShowing = true;
                        break;
                    case "RefreshTaskPane":
                        //TaskPanes.RefreshAll();
                        break;
                    case "CreateLegacyPrefill":
                        if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                            return;
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)
                                || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)) //GLOG 5443 (dm)

                            return;
                        LMP.MacPac.Application.CreateLegacyPrefill();
                        break;
                    case "Convert":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)
                                || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)) //GLOG 5443 (dm)

                            return;
                        LMP.MacPac.Application.ConvertLegacyDocument();
                        break;
                    case "SelectTaskPane":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)
                                || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))  //GLOG 5443 (dm)
                            return;
                        LMP.MacPac.Application.SelectTaskPane();
                        break;
                    case "ManagePeople":
                        if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                            return;
                        LMP.MacPac.Application.ManagePeople();
                        break;
                    case "EditAuthorPreferences":
                        if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                            return;
                        LMP.MacPac.Application.EditAuthorPreferences();
                        break;
                    case "ManageUserAppSettings":
                        if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                            return;
                        LMP.MacPac.Application.ManageUserAppSettings();
                        break;
                    // GLOG : 3135 : JAB
                    // Handle update of the document content.
                    case "UpdateDocumentContent":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, true)
                                || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)) //GLOG 5443 (dm)
                            return;
                        LMP.MacPac.Application.UpdateDocumentContent();
                        break;
                    case "UpdateContactDetail":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)
                                || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)) //GLOG 5443 (dm)
                            return;
                        LMP.MacPac.Application.UpdateDocumentContactDetail();
                        break;
                    case "PasteSegmentXML":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)
                                || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)) //GLOG 5443 (dm)
                            return;
                        LMP.MacPac.TaskPane.PasteSegmentXML();
                        break;
                    case "InsertTrailer":
                        //GLOG 5443 (dm): don't allow trailer if .docx file doesn't support content controls
                        if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                            return;
                        LMP.MacPac.Application.InsertTrailer();
                        break;
                    case "InsertDraftStamp":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)
                                || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(LMP.Forte.MSWord.WordApp.ActiveDocument(), true)) //GLOG 5443 (dm)
                            return;
                        LMP.MacPac.Application.InsertDraftStamp();
                        break;
                    case "OfficeAddress":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                            return;
                        LMP.MacPac.Application.InsertOfficeAddress();
                        break;
                    case "SetNormalStyleFont":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                            return;
                        LMP.MacPac.Application.SetNormalStyleFont();
                        break;
                    case "InsertPageNumber":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                            return;
                        LMP.MacPac.Application.InsertPageNumber();
                        break;
                    case "RemovePageNumber":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                            return;
                        LMP.MacPac.Application.RemovePageNumber();
                        break;
                    case "InsertContactDetail":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                            return;
                        LMP.MacPac.Application.InsertContactDetail();
                        break;
                    case "Synchronize":
                        if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                            return;
                        LMP.MacPac.Application.ExecuteOnDemandSync();
                        break;
                    case "ShowAdministrator":
                        try
                        {
                            System.Diagnostics.Process.Start(
                                new ProcessStartInfo(LMP.Data.Application.AppDirectory +
                                @"\ForteAdministrator.exe"));
                        }
                        catch (System.Exception oE)
                        {
                            //Ignore error caused by answering No to Windows security prompt
                            if (oE.Message != "The operation was canceled by the user")
                                throw oE;
                        }
                        break;
                    case "ShowTaskPaneIfSpecifiedOnDocOpen":
                        if (Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningMPDoc 
                            || Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningNonMPDoc)
                        {
                            if (oWordApp.Documents.Count == 0)
                            {
                                LMP.MacPac.Application.CreateNewDocument();
                                //LMP.MacPac.Application.ShowTaskPane2003();
                            }
                            else
                            {
                                //Don't show for protected document
                                if (LMP.Architect.Api.Environment.DocumentIsProtected(oWordApp.ActiveDocument, false)
                                || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oWordApp.ActiveDocument, false)) //GLOG 5443 (dm)
                                    return;
                                bool bShowTaskPane = true;
                                //Don't show if no MP content, or trailer only, 
                                if (!LMP.Forte.MSWord.WordDoc.IsForteDoc(oWordApp.ActiveDocument))
                                {
                                    //If no MP content, don't show unless specified by user setting
                                    if (!Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningNonMPDoc)
                                        return;
                                }
                                else if (!LMP.Forte.MSWord.WordDoc.ContainsNonTrailerMPContent(oWordApp.ActiveDocument))
                                {
                                    //If no non-trailer MP content, don't show
                                    bShowTaskPane = false;
                                }
                                else if (!Session.CurrentUser.UserSettings.ShowTaskPaneWhenOpeningMPDoc)
                                    //If MP content, don't show if not specified by user setting
                                    bShowTaskPane = false;

                                if (bShowTaskPane)
                                {
                                    //GLOG 3025: Also use timer for displaying TaskPane
                                    m_bShowTaskpaneAfterOpen = true;
                                    m_oOpenTimer.Start();
                                }
                                else
                                {
                                    //If not displaying Taskpane, still set XML tag state
                                    LMP.MacPac.Application.SetXMLTagStateForEditing();
                                    //If Document is attached to IBF Schema, Document Actions Taskpane
                                    //will always be displayed.  Since there's no way to prevent this
                                    //a timer is used to close it after a sufficient interval
                                    //GLOG 3025
                                    m_bShowTaskpaneAfterOpen = false;
                                    m_oOpenTimer.Start();
                                }
                            }
                        }
                        break;
                    case "RefreshNormalTemplateStyles":
                        if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                            return;
                        LMP.MacPac.Application.RefreshNormalTemplateStyles();
                        break;
                    case "FileSave":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileSave) > 0)
                            LMP.MacPac.Application.FileSave();
                        else
                            LMP.Forte.MSWord.WordApp.FileSave();
                        break;
                    case "FileSaveAs":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileSaveAs) > 0)
                            LMP.MacPac.Application.FileSaveAs();
                        else
                            LMP.Forte.MSWord.WordApp.FileSaveAs();
                        break;
                    case "FileClose":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileClose) > 0)
                            LMP.MacPac.Application.FileClose();
                        else
                            LMP.Forte.MSWord.WordApp.FileClose();
                        break;
                    case "FileExit":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileExit) > 0)
                            LMP.MacPac.Application.FileExit();
                        else
                            LMP.Forte.MSWord.WordApp.FileExit();

                        //GLOG #2843 - dcf
                        LMP.MacPac.Session.Quit();
                        break;
                    case "DocClose":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileClose) > 0)
                            LMP.MacPac.Application.DocClose();
                        else
                            LMP.Forte.MSWord.WordApp.DocClose();
                        break;
                    case "FileSaveAll":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileSaveAll) > 0)
                            LMP.MacPac.Application.FileSaveAll();
                        else
                            LMP.Forte.MSWord.WordApp.FileSaveAll();
                        break;
                    case "FileCloseAll":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileCloseAll) > 0)
                            LMP.MacPac.Application.FileCloseAll();
                        else
                            LMP.Forte.MSWord.WordApp.FileCloseAll();
                        break;
                    case "InsertTrailerIfNecessary":
                        //JTS 5/27/09: Trailer is handled differently at open in Word 2003
                        if (Session.CurrentWordVersion == 11)
                            LMP.MacPac.Application.InsertTrailerIfNecessary(true);
                        else
                            LMP.MacPac.Application.InsertTrailerIfNecessary(false);
                        break;
                    case "FilePrint":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FilePrint) > 0)
                            LMP.MacPac.Application.FilePrint();
                        else
                            LMP.Forte.MSWord.WordApp.FilePrint();
                        break;
                    case "FilePrintDefault":
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FilePrintDefault) > 0)
                            LMP.MacPac.Application.FilePrintDefault();
                        else
                            LMP.Forte.MSWord.WordApp.FilePrintDefault();
                        break;
                    case "MacPacAutoOpen":
                        //Called from DocumentOpen event in Forte.dot
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileOpen) > 0)
                            //JTS 5/27/09: Trailer is handled differently at open in Word 2003
                            if (Session.CurrentWordVersion == 11)
                                LMP.MacPac.Application.InsertTrailerIfNecessary(true);
                            else
                                LMP.MacPac.Application.InsertTrailerIfNecessary(false);
                        break;
                    case "MacPacAutoClose":
                        break;
                    //GLOG #2843 - dcf
                    case "Quit":
                        Quit();
                        break;
                    case "RemoveDocumentSchema":
                        if (!LMP.Architect.Api.Environment.DocumentIsProtected(LMP.MacPac.Session.CurrentWordApp.ActiveDocument, true))
                            LMP.MacPac.Application.RemoveTags(LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
                        break;
                    case "InsertInterrogatories":
                        if (!LMP.Architect.Api.Environment.DocumentIsProtected(LMP.MacPac.Session.CurrentWordApp.ActiveDocument, true))
                            LMP.MacPac.Application.CreateInterrogatories();
                        break;
                    case "InsertTrailerOnSave":
                        //GLOG 5665: Call InsertTrailerIfNecessary only if configured for Save integration
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileSave) > 0)
                            LMP.MacPac.Application.InsertTrailerIfNecessary();
                        break;
                    case "InsertTrailerOnSaveAs":
                        //GLOG 5665: Call InsertTrailerIfNecessary only if configured for SaveAs integration
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileSaveAs) > 0)
                            LMP.MacPac.Application.InsertTrailerIfNecessary();
                        break;
                    case "InsertTrailerOnPrint":
                        //GLOG 5665: Call InsertTrailerIfNecessary only if configured for Print integration
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FilePrint) > 0)
                            LMP.MacPac.Application.InsertTrailerIfNecessary();
                        break;
                    case "InsertTrailerOnPrintDefault":
                        //GLOG 5665: Call InsertTrailerIfNecessary only if configured for PrintDefault integration
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FilePrintDefault) > 0)
                            LMP.MacPac.Application.InsertTrailerIfNecessary();
                        break;
                    case "InsertTrailerOnClose":
                        //GLOG 5665: Call InsertTrailerIfNecessary only if configured for Close integration
                        if ((oSettings.TrailerIntegrationOptions & FirmApplicationSettings.mpTrailerIntegrationOptions.FileClose) > 0)
                            LMP.MacPac.Application.InsertTrailerIfNecessary();
                        break;

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Closes Document action taskpane when it has been automatically displayed by IBF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void m_oOpenTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                //GLOG 3025: IBF will show or hide taskpane based on whether IBF Schema was present
                //at start of open.  This code executed on a timer to allow IBF open process to complete
                if (m_bShowTaskpaneAfterOpen)
                {
                    //LMP.MacPac.Application.ShowTaskPane2003();
                }
                else
                    Session.CurrentWordApp.TaskPanes[Microsoft.Office.Interop.Word.WdTaskPanes.wdTaskPaneDocumentActions].Visible = false;
            }
            finally
            {
                m_oOpenTimer.Stop();
                m_bShowTaskpaneAfterOpen = false;
            }
        }
        /// <summary>
        /// inserts the symbol with the specified ascii number
        /// </summary>
        /// <param name="ASCII"></param>
        public void InsertSymbol(int ASCII, string FontName, bool Unicode)
        {
            try
            {
                if (LMP.Architect.Api.Environment.DocumentIsProtected(LMP.Forte.MSWord.WordApp.ActiveDocument(), true))
                    return;

                LMP.Forte.MSWord.WordDoc.InsertSymbol(ASCII, FontName,  Unicode);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditCopy(Word.Document oDocument)
        {
            try
            {
                Word.Application oApp = (Word.Application)oDocument.Parent;
                Word.Selection oSelection = null;

                //test to see whether there's a selection - user may be copying
                //from a native dialog
                try
                {
                    oSelection = oApp.Selection;
                }
                catch { }

                //GLOG 4483: Run native function if MacPac is Paused
                if (!MacPacIsPaused() && oSelection != null)
                {
                    //send message to TaskPane to do the copying - this
                    //will ensure that tags collection is properly maintained
                    bool bMessageSent = SendMessageToTaskPane(oDocument,
                        mpPseudoWordEvents.OnCopy);

                    if (!bMessageSent)
                    {
                        //there's no task pane or tags collection
                        LMP.Forte.MSWord.Tags oTags = null;
                        bool bRefreshRequired = false;
                        LMP.Forte.MSWord.WordApp.EditCopy(ref oTags, ref bRefreshRequired);
                    }
                }
                else
                    //use WordBasic method
                    LMP.Forte.MSWord.WordApp.EditCopyWordBasic();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void SetXMLEventHandling(Word.Document oDocument, bool bOn)
        {
            try
            {
                SetWordXMLEventHandling(oDocument, bOn);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void ChangeCase()
        {
            try
            {
                LMP.Forte.MSWord.WordApp.ChangeCase();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 7720 : ceh
        public static void SetSentenceSpacingToOne()
        {
            try
            {
                LMP.Forte.MSWord.WordDoc.SetSentenceSpacing(2, 1);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 7720 : ceh
        public static void SetSentenceSpacingToTwo()
        {
            try
            {
                LMP.Forte.MSWord.WordDoc.SetSentenceSpacing(1,2);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// changes the existing letterhead of the specified segment,
        /// if it exists - otherwise, adds letterhead
        /// </summary>
        /// <param name="oSegment"></param>
        public static bool ChangeLetterhead(Segment oSegment)
        {
            DialogResult iRet = DialogResult.Cancel;
            //GLOG 4028
            int iSection = 0;

            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oSegment.ForteDocument.WordDocument);
            oEnv.SaveState();

            try
            {
                //get existing letterhead segment
                Segment[] oLetterheads = oSegment.FindChildren(mpObjectTypes.Letterhead);
                Segment oLetterhead = null;
                int iLetterheadID = 0;
                string xCaption = null;
                Prefill oDraftStampPrefill = null; //GLOG 8552

                if (oLetterheads.Length > 0)
                {
                    oLetterhead = oLetterheads[0];
                    iLetterheadID = oLetterhead.ID1;
                    //GLOG 4028
                    iSection = oLetterhead.PrimaryRange.Sections[1].Index;
                    xCaption = LMP.Resources.GetLangString("Dlg_EditLetterhead");
                }
                else
                {
                    xCaption = LMP.Resources.GetLangString("Dlg_AddLetterhead");
                }

                //get new letterhead segment to insert
                SegmentChooserForm oForm = new SegmentChooserForm(oSegment.ID1,
                    oSegment.TypeID, mpObjectTypes.Letterhead, iLetterheadID, xCaption);

                iRet = oForm.ShowDialog();

                if (iRet == DialogResult.OK)
                {
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //turn off screen updating
                    LMP.MacPac.Application.ScreenUpdating = false;

                    oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.Undefined, mpTriState.Undefined,
                        mpTriState.Undefined, mpTriState.True, false);

                    //GLOG 4028: Get existing trailers
                    object oTrailers = null;
                    LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                    if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains(
                        "CONVERTTRAILERSTO9XFORMAT") || oFirmSettings.UseMacPac9xStyleTrailer)
                        oTrailers = LMP.Forte.MSWord.WordDoc.GetConverted9xTrailers(oSegment.ForteDocument.WordDocument, "zzmpTrailerItem");
                    else
                        oTrailers = LMP.Forte.MSWord.WordDoc.GetExistingTrailers(oSegment.ForteDocument.WordDocument, "MacPac Trailer"); //GLOG 8111 (dm)

                    //GLOG : 8178 : ceh
                    //Get existing draft stamp where PrimaryRange is within the section in which Letterhead is being changed
                    Segment oDraftStamp = null;
                    Segments oSegs = oSegment.ForteDocument.Segments;

                    for (int i = 0; i < oSegs.Count; i++)
                    {
                        Segment oSeg = oSegs[i];
                        if (oSegs[i] is DraftStamp &&
                            oSegs[i].PrimaryRange.Sections[1].Index == iSection)
                        {
                            oDraftStamp = oSeg;
                            //GLOG 8552: If DraftSTamp is in header, it may or may not get deleted with Letterhead, depending on LH design.
                            //Always delete and recreate in this case
                            if (oDraftStamp.DefaultDoubleClickLocation != Architect.Base.Segment.InsertionLocations.InsertAtSelection)
                            {
                                oDraftStampPrefill = new Prefill(oDraftStamp);
                                oSegment.ForteDocument.DeleteSegment(oDraftStamp, false);
                            }
                            else
                                //No need to recreate current page stamps
                                oDraftStamp = null;
                            break;
                        }
                    }

                    //GLOG : 6483 : ceh
                    //Get Prefill if Letterhead exists
                    Prefill oPrefill = null;

                    if (oLetterhead != null)
                        oPrefill = new Prefill(oLetterhead);

                    //GLOG 6958: Make sure current selection points to Parent Segment before inserting
                    Word.Range oRng = oSegment.PrimaryRange.Duplicate;
                    oRng.SetRange(oRng.Start, oRng.Start);
                    oRng.Select();
                    TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSegment.ForteDocument.WordDocument);
#if Oxml                    
                    if (LMP.MacPac.Application.IsOxmlSupported())
                    {
                        oTP.InsertXmlSegmentInCurrentDoc(oForm.Value + ".0", oLetterhead, oPrefill, Segment.InsertionLocations.Default, 
                            Segment.InsertionBehaviors.Default, "", null, oRng);
                    }
                    else
#endif
                    {
                        oSegment.ForteDocument.InsertSegment(oForm.Value + ".0", oSegment,
                            Segment.InsertionLocations.Default, Segment.InsertionBehaviors.Default, null, oPrefill);
                    }

                    //GLOG : 8028 : ceh - no need for this call due to changes below
                    //GLOG item #6231 - dcf
                    //GLOG 8116
                    if ((oSegment != null) && !LMP.MacPac.MacPacImplementation.IsToolkit)
                    {
                        //GLOG item #6231 - dcf
                        RefreshDataReviewerSegmentNodeIfNecessary(oSegment);
                    }

                    //GLOG 6373: Make sure node tags point to current objects
                    if (oTP != null && oTP.DocEditor != null)
                    {
                        //GLOG 8116: This will be handled by RefreshDataReviewerSegmentNodeIfNecessary above
                        ////GLOG : 8028 : ceh - update Data Reviewer to reflect changes
                        //if (oTP.DataReviewer != null)
                        //{
                        //    //Refresh Tree to ensure Letterhead child nodes reflect current Segments
                        //    oTP.DataReviewer.RefreshTree();
                        //}

                        //GLOG 7594: Refresh Tree to ensure Letterhead child nodes reflect current Segments
                        oTP.DocEditor.RefreshTree();
                        oTP.DocEditor.UpdateSegmentImage(oSegment, true);
                    }
					

                    //GLOG 4028: Recreate Trailer after changing letterhead
                    if (oTrailers != null)
                    {
                        try
                        {
                            //Check if current section contains a trailer - may be null if no trailer
                            string xTrailerID = ((object[])oTrailers)[iSection - 1].ToString();
                            if (LMP.String.IsNumericInt32(xTrailerID))
                            {
                                //GLOG 6917: Check that Trailer Segment currently exists
                                int iTrailerID = Int32.Parse(xTrailerID);
                                AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.Trailer);
                                AdminSegmentDef oTrailerDef = (AdminSegmentDef)oDefs.ItemFromID(iTrailerID);
                                //GLOG 6917: Don't need to recreate trailer if it's in body story
                                if (oTrailerDef != null && String.ContentContainsLinkedStories(oTrailerDef.XML, true, true, true, true, true, true))
                                {
                                    Prefill oTrailerPrefill = DocumentStampUI.LastTrailerPrefill(iTrailerID);
                                    if (oTrailerPrefill != null)
                                        LMP.MacPac.Application.RecreateTrailer(oTrailers, oTrailerPrefill, 0);
                                }
                            }
                        }
                        catch { }
                    }

                    //GLOG : 8178 : ceh
                    if (oDraftStampPrefill != null) //GLOG 8552
                    {
                        LMP.MacPac.Application.RecreateDraftStamp(oDraftStamp, oDraftStampPrefill, 0);
                    }
                }
                else
                {
                    //GLOG : 8000 : ceh - set focus back to the document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }
            }
            finally
            {
                oEnv.RestoreState(true, true, false);
                LMP.MacPac.Application.ScreenUpdating = true;
                //GLOG 7742: Enable events last
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
            }

            return iRet == DialogResult.OK;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oSegment"></param>
        private static void RefreshDataReviewerSegmentNodeIfNecessary(Segment oSegment)
        {
            TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSegment.ForteDocument.WordDocument);
            if (oTP.WordTaskPane.Visible && oTP.DataReviewerVisible)
            {
                oTP.DataReviewer.RefreshSegmentNode(oSegment.TagID);
            }
        }

        /// <summary>
        /// replaces the existing Pleading Paper segments for the specified document
        /// </summary>
        /// <param name="oSegment"></param>
		//GLOG : 7494 : CEH
        public static void ReplacePleadingPaper()
        {
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            LMP.Architect.Api.ForteDocument oMPDoc = new ForteDocument(oDoc);

            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
            oEnv.SaveState();

            MacPac.Application.ScreenUpdating = false;

            try
            {
                Segments oSegs = null;
                Prefill oPrefill = null;

                //cycle through sections, replacing Pleading Paper in each one
                for (int i = 1; i <= oDoc.Sections.Count; i++)
                {
                    oSegs = Segment.GetSegments(oMPDoc, oDoc.Sections[i], mpObjectTypes.PleadingPaper,1);

                    if (oSegs.Count > 0)
                    {
                        //get prefill
                        oPrefill = new Prefill(oSegs[0]);
                        //insert segment
                        oMPDoc.InsertSegment(oSegs[0].ID.ToString(),
                            oSegs[0].Parent, Segment.InsertionLocations.Default, 
                            Segment.InsertionBehaviors.Default, null, oPrefill);
                    }
                }

            }
            finally
            {
                oEnv.RestoreState(true, true, false);
                //GLOG : 8000 : ceh - set focus back to the document
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                LMP.MacPac.Application.ScreenUpdating = true;
                //GLOG 7742: Enable events last
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
            }
        }

        /// <summary>
        /// changes the existing pleading paper of the specified segment,
        /// if it exists - otherwise, adds pleading paper
        /// replaces in current section by default
        /// </summary>
        /// <param name="oSegment"></param>
        //GLOG : 7203 : ceh
        public static bool ChangePleadingPaper(Segment oSegment)
        {
            return ChangePleadingPaper(oSegment, false);
        }
        
        /// <summary>
        /// changes the existing pleading paper of the specified segment,
        /// if it exists - otherwise, adds pleading paper
        /// </summary>
        /// <param name="oSegment"></param>
        //GLOG : 7203 : ceh
        public static bool ChangePleadingPaper(Segment oSegment, bool bEntireDocument)
        {
            Segment oCurrentSectionSeg = null;

            DialogResult iRet = DialogResult.Cancel;

            //GLOG 6379
            int iSection = 0;

            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            LMP.Architect.Api.ForteDocument oMPDoc = new ForteDocument(oDoc);

            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(oDoc);
            oEnv.SaveState();

            try
            {
                //get existing PleadingPaper segment
                Segment[] oPleadingPapers = null;

                if (oSegment != null)
                {
                    oPleadingPapers = oSegment.FindChildren(mpObjectTypes.PleadingPaper);
                }

                Segment oPleadingPaper = null;
                //GLOG 6596
                Prefill oPrefill = null;
                int iPleadingPaperID = 0;
                string xCaption = null;
                SegmentChooserForm oForm = null;
                Word.Range oRng = null;

                if (oPleadingPapers != null && oPleadingPapers.Length > 0)
                {
                    oPleadingPaper = oPleadingPapers[0];
                    //GLOG 6379
                    iSection = oPleadingPaper.PrimaryRange.Sections[1].Index;
                    iPleadingPaperID = oPleadingPaper.ID1;
                    //GLOG 6596
                    oPrefill = new Prefill(oPleadingPaper);
                    //GLOG item #7075 - dcf
					//GLOG : 7998 : ceh
                    if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                    {
                        xCaption = LMP.Resources.GetLangString("Dlg_InsertPleadingPaper");
                    }
                    else
                    {
                        xCaption = LMP.Resources.GetLangString("Dlg_EditPleadingPaper");
                    }
                    oForm = new SegmentChooserForm(oSegment.ID1,
                        oSegment.TypeID, mpObjectTypes.PleadingPaper, iPleadingPaperID, xCaption);
                }
                else
                {
                    iSection = oDoc.Application.Selection.Sections.First.Index;
                    iPleadingPaperID = 0;
                    //GLOG item #7075 - dcf
					//GLOG : 7998 : ceh
                    if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                    {
                        xCaption = LMP.Resources.GetLangString("Dlg_InsertPleadingPaper");
                    }
                    else
                    {
                        xCaption = LMP.Resources.GetLangString("Dlg_EditPleadingPaper");
                    }
                    oForm = new SegmentChooserForm(0, 0, mpObjectTypes.PleadingPaper, iPleadingPaperID, xCaption);
                    //string xMsg = LMP.Resources.GetLangString("Msg_NoPleadingPaperExists");
                    //MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                    //    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //return false;
                }

                //get new PleadingPaper segment to insert

                iRet = oForm.ShowDialog();

                if (iRet == DialogResult.OK)
                {
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //turn off screen updating
                    LMP.MacPac.Application.ScreenUpdating = false;

                    oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.Undefined, mpTriState.Undefined,
                        mpTriState.Undefined, mpTriState.True, false);

                    //GLOG 6379: Get existing trailers
                    object oTrailers = null;
                    LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                    if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains(
                        "CONVERTTRAILERSTO9XFORMAT") || oFirmSettings.UseMacPac9xStyleTrailer)
                        oTrailers = LMP.Forte.MSWord.WordDoc.GetConverted9xTrailers(oDoc, "zzmpTrailerItem"); //GLOG 8052 (dm)
                    else
                        oTrailers = LMP.Forte.MSWord.WordDoc.GetExistingTrailers(oDoc, "MacPac Trailer"); //GLOG 8111 (dm)

                    Word.Section oSection = null;

                    //GLOG 7531: Unlink Headers/Footers in following section
                    //GLOG : 7203 : ceh - don't run if inserting in all sections
                    if (iSection < oDoc.Sections.Count && !bEntireDocument)
                    {
                        oSection = oDoc.Sections[iSection + 1];
                        LMP.Forte.MSWord.WordDoc.UnlinkHeadersFooters(oSection);
                    }
                    TaskPane oTP = LMP.MacPac.TaskPanes.Item(oMPDoc.WordDocument);
                    //GLOG : 7203 : ceh
                    if (bEntireDocument)
                    {
                        //cycle through sections, replacing Pleading Paper in each
                        for (int i = 1; i <= oDoc.Sections.Count; i++)
                        {
                            oSection = oDoc.Sections[i];

                            //unlink Headers/Footers
                            LMP.Forte.MSWord.WordDoc.UnlinkHeadersFooters(oSection);

                            //reset
                            oPleadingPapers = null;
                            oPrefill = null;

                            //get top level Segment for section being replaced
                            oCurrentSectionSeg = oMPDoc.GetTopLevelSegmentForRange(oSection.Range);

                            //get prefill from Pleading Paper segment
                            if (oCurrentSectionSeg != null)
                            {
                                oPleadingPapers = oCurrentSectionSeg.FindChildren(mpObjectTypes.PleadingPaper);
                            }

                            if (oPleadingPapers != null && oPleadingPapers.Length > 0)
                            {
                                oPleadingPaper = oPleadingPapers[0];
                                oPrefill = new Prefill(oPleadingPaper);
                            }

                            //GLOG 6958: Make sure current selection points to Parent Segment before inserting
                            if (oCurrentSectionSeg != null)
                            {
                                oRng = oCurrentSectionSeg.PrimaryRange.Duplicate;
                                oRng.SetRange(oRng.Start, oRng.Start);
                                oRng.Select();
                                oMPDoc.InsertSegment(oForm.Value + ".0", oCurrentSectionSeg,
                                    Segment.InsertionLocations.Default, Segment.InsertionBehaviors.Default, false, oPrefill, null, false, false, null); //GLOG 6596
                            }
                            else
                            {
                                oRng = oMPDoc.WordDocument.Sections[i].Range;
                                oRng.SetRange(oRng.Start, oRng.Start);
                                oRng.Select();
                                oMPDoc.InsertSegment(oForm.Value + ".0", null,
                                    Segment.InsertionLocations.Default, Segment.InsertionBehaviors.Default, false, oPrefill, null, false, false, null); //GLOG 6596
                            }
#if Oxml
                            if (LMP.MacPac.Application.IsOxmlSupported() && (!(LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator))) //GLOG 15852 (dm)
                            {
                                ForteDocument.IgnoreWordXMLEvents = Architect.Base.ForteDocument.WordXMLEvents.All;
                                oTP.InsertXmlSegmentInCurrentDoc(oForm.Value + ".0", oPleadingPaper, oPrefill, Segment.InsertionLocations.Default,
                                    Segment.InsertionBehaviors.Default, "", null, oRng);
                                ForteDocument.IgnoreWordXMLEvents = Architect.Base.ForteDocument.WordXMLEvents.None;
                            }
                            else
#endif
                            {
                                oMPDoc.InsertSegment(oForm.Value + ".0", oCurrentSectionSeg,
                                    Segment.InsertionLocations.Default, Segment.InsertionBehaviors.Default, false, oPrefill, null, false, false, null); //GLOG 6596
                            }
                            //GLOG 7273 (dm) - don't attempt to refresh data reviewer in Tools
                            if ((oSegment != null) && !LMP.MacPac.MacPacImplementation.IsToolkit)
                            {
                                //GLOG item #6231 - dcf
                                RefreshDataReviewerSegmentNodeIfNecessary(oCurrentSectionSeg);
                            }

                            //GLOG 6373: Make sure node tags point to current objects
                            if (oTP != null && oTP.DocEditor != null)
                            {
                                //GLOG 7594: Refresh Tree to ensure Pleading Paper child nodes reflect current Segments
                                oTP.DocEditor.RefreshTree();
                                //GLOG 8151
                                if (oCurrentSectionSeg != null)
                                {
                                    oTP.DocEditor.UpdateSegmentImage(oCurrentSectionSeg, true);
                                }
                            }
                        }                        
                    }
                    else
                    {
                        if (oSegment != null)
                        {
                            //GLOG 6958: Make sure current selection points to Parent Segment before inserting
                            oRng = oSegment.PrimaryRange.Duplicate;
                            oRng.SetRange(oRng.Start, oRng.Start);
                            oRng.Select();
                        }
                        else
                        {
                            oRng = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Range;
                            oRng.SetRange(oRng.Start, oRng.Start);
                            oRng.Select();
                        }
#if Oxml
                        if (LMP.MacPac.Application.IsOxmlSupported() && (!(LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator))) //GLOG 15852 (dm)
                        {
                            oTP.InsertXmlSegmentInCurrentDoc(oForm.Value + ".0", oPleadingPaper, oPrefill, Segment.InsertionLocations.Default,
                                Segment.InsertionBehaviors.Default, "", null, oRng);
                        }
                        else
#endif
                        {
                            oMPDoc.InsertSegment(oForm.Value + ".0", null,
                                Segment.InsertionLocations.Default, Segment.InsertionBehaviors.Default, false, oPrefill, null, false, false, null); //GLOG 6596
                        }
                        //GLOG 7273 (dm) - don't attempt to refresh data reviewer in Tools
                        //GLOG 8151: Also don't refresh for Tools Administrator
                        if ((oSegment != null) && (!(LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)))
                        {
                            //GLOG item #6231 - dcf
                            RefreshDataReviewerSegmentNodeIfNecessary(oSegment);
                        }

                        //GLOG 6373: Make sure node tags point to current objects
                        if (oTP != null && oTP.DocEditor != null)
                        {
                            //GLOG 7594: Refresh Tree to ensure Pleading Paper child nodes reflect current Segments
                            oTP.DocEditor.RefreshTree();
                            //GLOG 8151
                            if (oSegment != null)
                            {
                                oTP.DocEditor.UpdateSegmentImage(oSegment, true);
                            }
                        }
                    }
		
                    //GLOG 6379: Recreate Trailer after changing pleading paper
                    if (oTrailers != null)
                    {
                        try
                        {
                            //Check if current section contains a trailer - may be null if no trailer
                            string xTrailerID = ((object[])oTrailers)[iSection - 1].ToString();
                            if (LMP.String.IsNumericInt32(xTrailerID))
                            {
                                //GLOG 6917: Check that Trailer Segment currently exists
                                int iTrailerID = Int32.Parse(xTrailerID);
                                AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.Trailer);
                                AdminSegmentDef oTrailerDef = (AdminSegmentDef)oDefs.ItemFromID(iTrailerID);
                                //GLOG 6917: Don't need to recreate trailer if it's in body story
                                if (oTrailerDef != null && String.ContentContainsLinkedStories(oTrailerDef.XML, true, true, true, true, true, true))
                                {
                                    Prefill oTrailerPrefill = DocumentStampUI.LastTrailerPrefill(iTrailerID);
                                    if (oTrailerPrefill != null)
                                        LMP.MacPac.Application.RecreateTrailer(oTrailers, oTrailerPrefill, 0);
                                }
                            }
                        }
                        catch { }
                    }
                }
                else
                {
                    //GLOG : 8000 : ceh - set focus back to the document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }
            }
            finally
            {
                oEnv.RestoreState(true, true, false);
                LMP.MacPac.Application.ScreenUpdating = true;
                //GLOG 7742: Enable events last
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
            }

            return iRet == DialogResult.OK;
        }
        public static void InsertSectionBreak(Word.Document oDocument, Segment oSegment)
        {
            //GLOG 6266
            Word.Range oRng = null;
            short shLocation = 0;
            short shHeaderFooter1Option = 0;
            short shHeaderFooter2Option = 0;
            short shPageNumber1Option = 0;
            short shPageNumber2Option = 0;
            Segment oPleadingPaper = null;
            string[] aOldBlockNames = new string[0];

            ForteDocument oMPDoc;
            TaskPane oTP = null;
            oTP = TaskPanes.Item(oDocument);
            if (oTP != null)
                oMPDoc = oTP.ForteDocument;
            else if (oSegment != null)
                oMPDoc = oSegment.ForteDocument;
            else
                oMPDoc = new ForteDocument(oDocument);

            if (oSegment != null && oSegment.Contains(mpObjectTypes.PleadingPaper))
            {
                Segment[] aSegments = oSegment.FindChildren(mpObjectTypes.PleadingPaper);
                if (aSegments.Length > 0)
                {
                    oPleadingPaper = aSegments[0];
                    aOldBlockNames = new string[oPleadingPaper.Blocks.Count];
                    for (int b = 0; b < oPleadingPaper.Blocks.Count; b++)
                    {
                        Block oBlock = oPleadingPaper.Blocks[b];
                        string xName = oBlock.Name;
                        aOldBlockNames[b] = xName;
                    }
                }
            }
            InsertSectionBreakForm oForm = new InsertSectionBreakForm();
            oForm.Segment = oSegment;
            DialogResult oRet = oForm.ShowDialog();
            if (oRet == DialogResult.OK)
            {
                shPageNumber1Option = (short)oForm.Section1PageNumber;
                shPageNumber2Option = (short)oForm.Section2PageNumber;
                shHeaderFooter1Option = (short)oForm.Section1Option;
                shHeaderFooter2Option = (short)oForm.Section2Option;

                if (aOldBlockNames.Length > 0)
                {
                    for (int b = 0; b < aOldBlockNames.Length; b++)
                    {
                        string xName = aOldBlockNames[b];
                        for (int v = 0; v < oPleadingPaper.Variables.Count; v++)
                        {
                            Variable oVar = oPleadingPaper.Variables[v];
                            for (int a = 0; a < oVar.VariableActions.Count; a++)
                            {
                                VariableAction oAction = oVar.VariableActions[a];
                                VariableActions.Types iType = oAction.Type;
                                string xParams = oAction.Parameters;
                                //If Variable action parameters reference old Block Name, update to use wildcard
                                //Copies of original blocks will be indexed with _1, _2, etc.
                                if ((iType == VariableActions.Types.IncludeExcludeBlocks && xParams.StartsWith(xName + "�")) || 
                                    (iType == VariableActions.Types.ExecuteOnBlock && xParams.EndsWith("�" + xName)))
                                {
                                    string xParam = oAction.Parameters;
                                    xParam = xParam + "*";
                                    oAction.Parameters = xParam;
                                    oVar.Parent.Save(oVar);
                                }
                            }
                        }
                    }
                }
                if (oForm.InsertionLocation == InsertSectionBreakForm.BreakLocations.BeforeBodyBlock)
                {
                    try
                    {
                        oRng = oSegment.GetBody();
                        object oPara = Word.WdUnits.wdParagraph;
                        object oFalse = false;
                        oRng.StartOf(ref oPara, ref oFalse);
                    }
                    catch { }
                }
                if (oRng == null)
                {
                    switch (oForm.InsertionLocation)
                    {
                        case InsertSectionBreakForm.BreakLocations.EndOfDocument:
                            shLocation = (short)Segment.InsertionLocations.InsertAtEndOfDocument;
                            break;
                        case InsertSectionBreakForm.BreakLocations.StartOfDocument:
                            shLocation = (short)Segment.InsertionLocations.InsertAtStartOfDocument;
                            break;
                        default:
                            shLocation = (short)Segment.InsertionLocations.InsertAtSelection;
                            break;
                    }
                    System.Array aInsertionLoc = LMP.Forte.MSWord.WordDoc.GetInsertionLocations(oDocument, shLocation, new object[0], true); //GLOG 6021 //GLOG 6820
                    oRng = (Word.Range)aInsertionLoc.GetValue(0);
                }
                int iSecIndex = oRng.Sections[1].Index;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                ScreenUpdating = false;
                try
                {
                    //Insert section break with options
                    LMP.Forte.MSWord.WordDoc.InsertSectionEx(oRng, oForm.RestartPageNumbering, false, shHeaderFooter1Option, shHeaderFooter2Option, true);
                    //Set Page Number options
                    if (oForm.Section1PageNumber == InsertSectionBreakForm.PageNumberOptions.AddPage1)
                    {
                        oDocument.Sections[iSecIndex].PageSetup.DifferentFirstPageHeaderFooter = 0;
                    }
                    else if (oForm.Section1PageNumber == InsertSectionBreakForm.PageNumberOptions.RemovePage1)
                    {
                        oDocument.Sections[iSecIndex].PageSetup.DifferentFirstPageHeaderFooter = -1;
                        LMP.Forte.MSWord.WordDoc.RemovePageNumber(oDocument.Sections[iSecIndex], true, false);
                    }
                    if (oForm.Section2PageNumber == InsertSectionBreakForm.PageNumberOptions.AddPage1)
                    {
                        oDocument.Sections[iSecIndex+1].PageSetup.DifferentFirstPageHeaderFooter = 0;
                    }
                    else if (oForm.Section2PageNumber == InsertSectionBreakForm.PageNumberOptions.RemovePage1)
                    {
                        oDocument.Sections[iSecIndex+1].PageSetup.DifferentFirstPageHeaderFooter = -1;
                        LMP.Forte.MSWord.WordDoc.RemovePageNumber(oDocument.Sections[iSecIndex+1], true, false);
                    }

                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    oMPDoc.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes);

                    if (oTP != null && oTP.DocEditor != null)
                    {
                        for (int i = 0; i < oMPDoc.Segments.Count; i++)
                        {
                            oTP.DocEditor.UpdateSegmentImage(oMPDoc.Segments[i], true);
                        }
                    }
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    ScreenUpdating = true;
                }
            }
            else
            {
                //GLOG : 8000 : ceh - set focus back to the document
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
        }
        public void InsertPleadingPaper(Word.Document oDocument)
        {
            try
            {
                //GLOG 4483
                if (!MacPacIsPaused())
                    SendMessageToTaskPane(oDocument, mpPseudoWordEvents.PleadingPaperRequested);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void RefreshTaskPane(Word.Document oDocument)
        {
            try
            {
                SendMessageToTaskPane(oDocument, mpPseudoWordEvents.RefreshRequested);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        public void EditCut(Word.Document oDocument)
        {
            try
            {
                Word.Application oApp = (Word.Application)oDocument.Parent;
                Word.Selection oSelection = null;

                //test to see whether there's a selection - user may be cutting
                //from a native dialog
                try
                {
                    oSelection = oApp.Selection;
                }
                catch { }

                //GLOG 4483: Run native function if MacPac is Paused
                if (!MacPacIsPaused() && oSelection != null)
                {
                    //send message to TaskPane to do the cutting - this
                    //will ensure that tags collection is properly maintained
                    bool bMessageSent = SendMessageToTaskPane(oDocument,
                        mpPseudoWordEvents.OnCut);

                    if (!bMessageSent)
                    {
                        //there's no task pane or tags collection
                        LMP.Forte.MSWord.Tags oTags = null;
                        bool bRefreshRequired = false;
                        LMP.Forte.MSWord.WordApp.EditCut(ref oTags, ref bRefreshRequired);
                    }
                }
                else
                    //use WordBasic method
                    LMP.Forte.MSWord.WordApp.EditCutWordBasic();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG 4483: This function exists to ensure that Paused state is always read from
        //same Application object used by COM Addin, rather than that of the VBA session
        //private static bool MacPacIsPaused()
        public bool MacPacIsPaused()
        {
            try
            {
                object oAddin;
                object oName = "ForteAddIn";
                oAddin = LMP.Forte.MSWord.GlobalMethods.CurWordApp.COMAddIns.Item(ref oName).Object;
                return ((IAddInObject)oAddin).Paused();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return false;
            }
        }

        /// <summary>
        /// returns TRUE if the specified document has a MacPac task pane
        /// </summary>
        /// <param name="oDocument"></param>
        /// <returns></returns>
        public static bool TaskPaneExists(Word.Document oDocument)
        {
            return SendMessageToTaskPane(oDocument, mpPseudoWordEvents.QueryExists);
        }
        #endregion
    }
    //GLOG 4483: Class for object returned by RequestComAddInAutomationService in COM Addin
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    public class AddinObject : StandardOleMarshalObject, IAddInObject
    {
        public bool Paused()
        {
            return LMP.MacPac.Application.Paused;
        }
        public LMP.MacPac.Application MPApplication
        {
            get { return new LMP.MacPac.Application(); }
        }
    }
    //Interface must be used when casting from object
    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IAddInObject
    {
        bool Paused();
        LMP.MacPac.Application MPApplication { get; }
    }
}