using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Word = Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;
using LMP.Data;
using LMP.Architect.Api;
using Infragistics.Win;
using Infragistics.Win.UltraWinTree;
using System.Runtime.InteropServices;
using Microsoft.Office.Tools;
using LMP.Controls;
#if Oxml
using LMP.Architect.Oxml;
#endif

namespace LMP.MacPac
{
	/// <summary>
	///LMP.MacPac.TaskPane class -
	///defines the Main MacPac user interface -
	///created by Daniel Fisherman - 08/18/05
	/// </summary>
    public partial class TaskPane : System.Windows.Forms.UserControl
    {
        #region *********************enumerations*********************
        public enum Tabs
		{
			Content = 0,
			Edit = 1,
            Import = 2,
            Review = 3
		}
        #endregion
        #region *********************fields*********************
        private static PendingAction m_oPendingAction;
        private System.Xml.XmlNode m_oNode;
        private ForteDocument m_oForteDocument;
#if Oxml
        private LMP.Architect.Oxml.Word.XmlForteDocument m_oXmlForteDocument;
        private static XmlSegment m_oNewXmlSegment;
#endif
        static Word.ApplicationEvents4_XMLSelectionChangeEventHandler m_oXmlSelChangeHandler;
        static Word.ApplicationEvents4_DocumentBeforeSaveEventHandler m_oDocumentBeforeSaveHandler;
        static Word.ApplicationEvents4_WindowSelectionChangeEventHandler m_oSelectionHandler;
        static Word.ApplicationEvents2_QuitEventHandler m_oQuitHandler;
        private bool m_bWordTagSelectionOccuring;
        private ForteDocument.Modes m_iCurMode;
        private bool m_bDocRefreshRequired = false;
        private string m_xTagsInScope = null;
        private string m_xFirstTagInScope = null;
        private string m_xLastTagInScope = null;
        private bool m_bInsertionIsInvalid = false;
        private System.Timers.Timer m_oInsertionUndoTimer;
        private System.Timers.Timer m_oRefreshTimer;
        private System.Timers.Timer m_oPasteTimer;
        private System.Timers.Timer m_oSaveTimer;
        private Microsoft.Office.Tools.CustomTaskPane m_oWordTaskPane;
        private int m_iExistingShapes = 0;
        private static Segment m_oNewSegment;
        private static bool m_bSkipScreenUpdates = false;
        private static Point oLastPoint;
        private int m_iShowXMLMarkupBeforePaste = 0;
        static Timer m_oTaskPaneTimer = null;
        private Prefill m_oTransientPrefill = null;
        private string m_xTransientBodyText = "";
        private bool m_bIsSetUp = false;
        private bool m_bAutoClosed = false;
        private static string m_xErrorList = "";
        private bool m_bSaveInProgress = false;
        private int m_iSaveCounter = 0;
        private bool m_bSuspendDeleteKeyHandler = false;
        private static bool m_bRibbonTabSelectRequired = false;

        // GLOG : 3315 : JAB
        // Introduce a field for the firm application settings. This
        // will be use for obtaining the help font size and name.
        private static FirmApplicationSettings m_oFirmAppSettings;

        public delegate int HookProc(int nCode, IntPtr wParam, IntPtr lParam);

        //This is the Import for the SetWindowsHookEx function.
        //Use this function to install a thread-specific hook.
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookProc lpfn,
        IntPtr hInstance, int threadId);

        //This is the Import for the UnhookWindowsHookEx function.
        //Call this function to uninstall the hook.
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);

        //This is the Import for the CallNextHookEx function.
        //Use this function to pass the hook information to the next hook procedure in chain.
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode,
        IntPtr wParam, IntPtr lParam);

        //Declare the hook handle as an int.
        static int iMouseHook = 0;
        //Declare the mouse hook constant.
        //For other hook types, you can obtain these values from Winuser.h in the Microsoft SDK.
        private const int WH_MOUSE = 7;
        private const int WM_MOUSEMOVE = 0x0200;
        private const int WM_LBUTTONDOWN = 0x0201;
        private const int WM_LBUTTONUP = 0x0202;
        private const int WM_RBUTTONDOWN = 0x0204;
        private const int WM_MBUTTONDOWN = 0x0207;
        //flags indicating current mouse status
        private static bool m_bMouseButtonDown = false;
        private static bool m_bMoveStartedFromTaskPane = false; //GLOG 8388
        private static bool m_bMouseIsMoving = false;
        private static bool m_bXMLAfterInsertHandledForMouseDrag = false;
        //Declare MouseHookProcedure as a HookProc type.
        private static HookProc MouseHookProcedure;

        static int iKeyboardHook = 0;
        private static HookProc KeyboardHookProcedure;
        private const int WH_KEYBOARD = 2;
        private const int KF_UP = 1 << 31;
        private const int KF_ALTDOWN = 1 << 29;
        private static bool m_bCtrlPressed = false;
        private static bool m_bShiftPressed = false;

        public event LMP.Architect.Api.CurrentSegmentChangedEventHandler CurrentTopLevelSegmentSwitched;
        static DateTime m_dtLastBeforeSaveEvent = DateTime.MinValue; //GLOG 8272
        #endregion
        #region *********************constructors*********************
		//the task pane instance is specific to a document, hence
		//it can store the document nodes - will be passed to a 
		//segment when required
		public TaskPane()
		{
			InitializeComponent();
        }
        /// <summary>
        /// Static constructor, used to hook Mouse events for session
        /// </summary>
        static TaskPane()
        {
            // Only hook once
            if (iMouseHook != 0)
                return;

            // Create an instance of HookProc.
            MouseHookProcedure = new HookProc(MouseHookProc);

            //call the mouse hook when the mouse is sending messages
            iMouseHook = SetWindowsHookEx(WH_MOUSE,
                        MouseHookProcedure,
                        (IntPtr)0,
                        AppDomain.GetCurrentThreadId());

            // Create keyboard hook
            if (LMP.Forte.MSWord.WordApp.Version < 15) //GLOG 7811 (dm)
            {
                KeyboardHookProcedure = new HookProc(KeyboardHookProc);

                //call the keyboard hook when the keyborad is sending messages
                iKeyboardHook = SetWindowsHookEx(WH_KEYBOARD,
                            KeyboardHookProcedure,
                            (IntPtr)0,
                            AppDomain.GetCurrentThreadId());
            }

            Word.Application oWord = Session.CurrentWordApp;
            if (oWord == null)
                oWord = LMP.Forte.MSWord.GlobalMethods.CurWordApp;

            //GLOG 3389: These event handlers have been made static.
            //Since they are subscribed to by the Word Application, 
            //there's no need to have a separate instance for each Taskpane object
            
            //handle Word Tag selection change
            m_oXmlSelChangeHandler = new Word.ApplicationEvents4_XMLSelectionChangeEventHandler(
                CurrentWordApp_XMLSelectionChange);

            oWord.XMLSelectionChange += m_oXmlSelChangeHandler;

            //handle Word Quit
            m_oQuitHandler = new Word.ApplicationEvents2_QuitEventHandler(OnAppQuitHandler);
            Word.ApplicationEvents2_Event oEvents =
                (Word.ApplicationEvents2_Event)oWord;
            oEvents.Quit += m_oQuitHandler;

            //handle document save
            m_oDocumentBeforeSaveHandler = new Word.ApplicationEvents4_DocumentBeforeSaveEventHandler(
                CurrentWordApp_DocumentBeforeSave);
            oWord.DocumentBeforeSave += m_oDocumentBeforeSaveHandler;

            //handle selection change
            m_oSelectionHandler = new Word.ApplicationEvents4_WindowSelectionChangeEventHandler(
                CurrentWordApp_SelectionChanged);
            oWord.WindowSelectionChange += m_oSelectionHandler;

        }

        static void CurrentWordApp_SelectionChanged(Word.Selection oSel)
        {
            //GLOG 8272
            if (LastSaveIsWithinThreshold())
            {
                return;
            }
            DateTime t0 = DateTime.Now;

            LMP.MacPac.TaskPane oTP = null;
            Segment oCurSegment = null;

            try
            {
                try
                {
                    oTP = TaskPanes.Item(oSel.Document);
                }
                catch { }

                if (oTP == null || ((ForteDocument.IgnoreWordXMLEvents &
                    ForteDocument.WordXMLEvents.XMLSelectionChange) ==
                    ForteDocument.WordXMLEvents.XMLSelectionChange))
                    return;

                //get current and new segment-
                Word.Bookmark oTopBmk = LMP.Forte.MSWord.WordDoc.GetTopLevelSegmentBookmark(oSel.Range);

                //GLOG 7297 (dm) - in tookit mode, current segment objects should be cleared out
                //when there's no longer a top level segment
                Segment oNewSegment = null;
                if (oTopBmk == null)
                {
                    if (!LMP.MacPac.MacPacImplementation.IsToolkit)
                        return;
                }
                else
                {
                    string xFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oTopBmk);
                    if (string.IsNullOrEmpty(xFullTagID))
                    {
                        if (!LMP.MacPac.MacPacImplementation.IsToolkit)
                            return;
                    }
                    else
                        oNewSegment = oTP.ForteDocument.FindSegment(xFullTagID);
                }

                if (true)
                {
                    //current segment has been switched -
                    //raise event if necessary
                    if (oTP.CurrentTopLevelSegmentSwitched != null)
                    {
                        LMP.Architect.Api.CurrentSegmentSwitchedEventArgs oArgs = new LMP.Architect.Api.CurrentSegmentSwitchedEventArgs(
                            oCurSegment, oNewSegment);
                        oTP.CurrentTopLevelSegmentSwitched(oTP, oArgs);
                    }
                }
            }
            catch (System.Exception oE)
            {
                //GLOG 6529: Ignore specific error encountered when undoing Insert Footnote/Endnote within XML Tag
                while (oE.InnerException != null)
                    oE = oE.InnerException;
                if (oE.Message.Contains("This object model command is not available"))
                {
                    return;
                }
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
            }
        }

        static void m_oTaskPaneTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (Session.CurrentWordApp.Documents.Count == 0)
                    return;

                Word.Document oDoc = null;
                try
                {
                    oDoc = Session.CurrentWordApp.ActiveDocument;
                }
                catch { }

                //Hide taskpane if document is protected
                if (oDoc != null && oDoc.ProtectionType !=
                    Word.WdProtectionType.wdNoProtection)
                {
                    TaskPane oTP = TaskPanes.Item(oDoc);
                    if (oTP != null)
                        TaskPanes.Remove(oDoc);
                }
            }

            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        #endregion
		#region *********************properties*********************
        public bool ContentManagerVisible
        {
            get { return this.tabMain.Tabs["ContentManager"].Visible; }
            set { this.tabMain.Tabs["ContentManager"].Visible = value; }
        }
        public bool DocumentEditorVisible
        {
            get { return this.tabMain.Tabs["DocumentEditor"].Visible; }
            set { this.tabMain.Tabs["DocumentEditor"].Visible = value; }
        }
        public bool DataReviewerVisible
        {
            get { return this.tabMain.Tabs["DataReviewer"].Visible; }
            set
            {
                //GLOG 5917 (dm) - only display if turned on in registry
                //GLOG 7103 (dm - never display for toolkit
				//GLOG : 7998 : ceh
                if (LMP.MacPac.Session.DisplayFinishButton &&
                    !LMP.MacPac.MacPacImplementation.IsToolkit && 
                    !LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                {
                    this.tabMain.Tabs["DataReviewer"].Visible = value;
                }
            }
        }
        /// <summary>
        /// gets/sets the Word task pane associated with this MacPac taskpane
        /// </summary>
        public Microsoft.Office.Tools.CustomTaskPane WordTaskPane
        {
            get { return m_oWordTaskPane; }
            set { m_oWordTaskPane = value; }
        }

        /// <summary>
        /// returns true iff the task pane is fully set up for use
        /// </summary>
        public bool IsSetUp
        {
            get { return m_bIsSetUp; }
        }
        /// <summary>
        /// returns the active tab
        /// </summary>
        public int ActiveTab
        {
            get { return this.tabMain.SelectedTab.Index; }
        }
        public ForteDocument ForteDocument
        {
            get { return m_oForteDocument; }
        }
#if Oxml
        public XmlForteDocument XmlForteDocument
        {
            get { return m_oXmlForteDocument; }
        }
#endif
        public bool DoNotShowOnDocChange { get; set; }
        /// <summary>
        /// returns the target segment of the document -
        /// prompts the user if necessary
        /// </summary>
        internal Segment GetTargetSegment()
        {
            Segments oSegments = this.ForteDocument.Segments;
            Segment oSeg = null;

            int iSegs = oSegments.Count;

            //count segments - if more than one, prompt the user to choose one
            if (iSegs > 1)
            {
                int iCount = oSegments.Count;
                int iPaperItems = 0;

                if (iCount > 1)
                {
                    //count the number of paper items
                    for (int i = 0; i < iCount; i++)
                    {
                        if (oSegments[i] is Paper)
                        {
                            iPaperItems++;
                        }
                        else
                        {
                            oSeg = oSegments[i];
                        }
                    }
                }

                //if there is only one non-paper segment, use it-
                //otherwise present a dialog for user to choose
                //which segment to target
                if (iCount - iPaperItems != 1)
                {
                    //prompt for one segment
                    SegmentSelectorForm oSelSegForm = new SegmentSelectorForm(this.ForteDocument.Segments);
                    if (oSelSegForm.ShowDialog() == DialogResult.OK)
                    {
                        return oSelSegForm.SelectedSegment;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return oSeg;
                }
            }
            else if (iSegs == 1)
            {
                //use the one segment
                return this.ForteDocument.Segments[0];
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// returns whether or not a Word tag
        /// selection is occuring
        /// </summary>
		internal bool WordTagChangeOccurring
		{
            get { return m_bWordTagSelectionOccuring; }
            set { m_bWordTagSelectionOccuring = value; }
		}
        /// <summary>
        /// gets/sets a pending action for this task pane
        /// </summary>
        internal static PendingAction PendingAction
        {
            get { return m_oPendingAction; }
            set { m_oPendingAction = value; }
        }
        public static bool HasPendingAction
        {
            get { return m_oPendingAction != null; }
        }
        /// <summary>
        /// gets/sets the mode of the task pane
        /// </summary>
        public ForteDocument.Modes Mode
        {
            get
            {
                return m_iCurMode;
            }
            set
            {
                //set mode of associated ForteDocument, if possible
                if (this.ForteDocument != null)
                    this.ForteDocument.Mode = value;

                //get current title bar
                string xTitleBar = Session.CurrentWordApp.ActiveWindow.Caption;

                string xDocPreview = LMP.Resources.GetLangString("Msg_DocumentPreview");
                string xDocDesign = LMP.Resources.GetLangString("Msg_DocumentDesign");

                if (value == ForteDocument.Modes.Design && m_iCurMode != ForteDocument.Modes.Design)
                {
                    //switch to design mode
                    if (this.DocDesigner == null)
                    {
                        //create the document designer control
                        this.DocDesigner = new DocumentDesigner();

                        //give the designer access to the task pane
                        this.DocDesigner.TaskPane = this;

                        //add to task pane
                        this.pageEdit.Controls.Add(this.DocDesigner);

                        //position the control
                        this.DocDesigner.Dock = DockStyle.Fill;
                    }

                    //set tab text to "Design"
                    this.tabMain.Tabs[1].Text = LMP.Resources.GetLangString(
                        "Prompt_DocumentDesignerTabText");

                    //Session.CurrentWordApp.ActiveWindow.Caption = xTitleBar;

                    if(this.DocEditor != null)
                        //hide document editor
                        this.DocEditor.Visible = false;
                }
                else if (value == ForteDocument.Modes.Edit && m_iCurMode != ForteDocument.Modes.Edit)
                {
                    //switch to edit mode
                    if (this.DocEditor == null)
                    {
                        //create new doc editor
                        this.DocEditor = new DocumentEditor();

                        //allows doc editor to know when the top level
                        //segment was switched due to selection
                        this.DocEditor.CurrentTopLevelSegmentSwitched +=
                            new LMP.Architect.Api.CurrentSegmentChangedEventHandler(DocEditor_CurrentTopLevelSegmentSwitched);

                        //give the designer access to the task pane
                        this.DocEditor.TaskPane = this;

                        //add to task pane
                        this.pageEdit.Controls.Add(this.DocEditor);

                        //position the control
                        this.DocEditor.Dock = DockStyle.Fill;

                    }

                    if(this.DocDesigner != null)
                        this.DocDesigner.Visible = false;

                    this.DocEditor.Visible = true;

                    //set word application title bar - only if modification is required
                    if (xTitleBar.Contains(xDocPreview) || xTitleBar.Contains(xDocDesign))
                    {
                        xTitleBar.Replace(xDocPreview, "");
                        xTitleBar.Replace(xDocDesign, "");
                        Session.CurrentWordApp.ActiveWindow.Caption = xTitleBar;
                    }

                    //set tab text to "Edit"
                    this.tabMain.Tabs[1].Text = LMP.Resources.GetLangString(
                        "Prompt_DocumentEditorTabText");

                }
                else if (value == ForteDocument.Modes.Preview && m_iCurMode != ForteDocument.Modes.Preview)
                {
                    if (this.DocEditor == null)
                    {
                        //create new doc editor
                        this.DocEditor = new DocumentEditor();

                        //allows doc editor to know when the top level
                        //segment was switched due to selection
                        this.DocEditor.CurrentTopLevelSegmentSwitched +=
                            new LMP.Architect.Api.CurrentSegmentChangedEventHandler(DocEditor_CurrentTopLevelSegmentSwitched);

                        //give the designer access to the task pane
                        this.DocEditor.TaskPane = this;

                        //add to task pane
                        this.pageEdit.Controls.Add(this.DocEditor);

                        //position the control
                        this.DocEditor.Dock = DockStyle.Fill;

                    }

                    if (this.DocDesigner != null)
                        this.DocDesigner.Visible = false;

                    //set tab text to "Edit"
                    this.tabMain.Tabs[1].Text = LMP.Resources.GetLangString(
                        "Prompt_DocumentEditorTabText");

                    this.DocEditor.RefreshTopLevelNodes(true);
                }
                m_iCurMode = value;
            }
        }

        void DocEditor_CurrentTopLevelSegmentSwitched(object sender, LMP.Architect.Api.CurrentSegmentSwitchedEventArgs args)
        {
            if (this.CurrentTopLevelSegmentSwitched != null)
            {
                this.CurrentTopLevelSegmentSwitched(this, args);
            }
        }
        /// <summary>
        /// sets whether or not the editor will refresh itself
        /// </summary>
        public System.Xml.XmlNode Data
        {
            set
            {
                this.m_oNode = value;
            }
        }

        /// <summary>
        /// gets/sets the whether or not the screen will update
        /// </summary>
        internal bool ScreenUpdating
        {
            get { return LMP.MacPac.Session.CurrentWordApp.ScreenUpdating; }
            set
            {
                Word.Application oWord = LMP.MacPac.Session.CurrentWordApp;
                bool bScreenUpdating = oWord.ScreenUpdating;

                //GLOG item #5541 - dcf -
                //not a big deal to broaden the condition, as 
                //benchmarks show no increase in time when setting
                //screenupdating=true when it is already true
                //if (!bScreenUpdating && value && !m_bSkipScreenUpdates)
                if (value && !m_bSkipScreenUpdates)
                {
                    //unfreeze the editor
                    LMP.WinAPI.LockWindowUpdate(0);
                    oWord.ScreenUpdating = true;
                    LMP.WinAPI.LockWindowUpdate(0);
                }
                else if (m_bSkipScreenUpdates || (bScreenUpdating && !value))
                {
                    try
                    {
                        //freeze the task pane control
                        LMP.WinAPI.LockWindowUpdate(this.Handle.ToInt32());
                    }
                    catch { }

                    //freeze Word
                    LMP.WinAPI.LockWindowUpdate(LMP.WinAPI.FindWindow("OpusApp", 0));
                    oWord.ScreenUpdating = false;
                }
            }
        }
        /// <summary>
        /// refreshes the screen if screen updating is turned off
        /// </summary>
        internal void ScreenRefresh()
        {
            if (!this.ScreenUpdating)
            {
                this.ScreenUpdating = true;
                System.Windows.Forms.Application.DoEvents();
                this.ScreenUpdating = false;
            }
        }
        internal Prefill TransientPrefill
        {
            get { return m_oTransientPrefill; }
            set { m_oTransientPrefill = value; }
        }
        internal string TransientBodyText
        {
            get { return m_xTransientBodyText; }
            set { m_xTransientBodyText = value; }
        }

        internal LMP.MacPac.DocumentDesigner DocDesigner
        {
            get { return ucDocDesigner; }
            set { ucDocDesigner = value; }
        }

        internal LMP.MacPac.ContentManager ContentManager
        {
            get { return ucContentManager; }
            set { ucContentManager = value; }
        }

        internal LMP.MacPac.DocumentEditor DocEditor
        {
            get { return ucDocEditor; }
            set { ucDocEditor = value; }
        }

        internal LMP.MacPac.DataReviewer DataReviewer
        {
            get { return ucDataReviewer; }
            set { ucDataReviewer = value; }
        }
        /// <summary>
        /// True if a context menu is displayed
        /// </summary>
        /// <returns></returns>
        internal bool MenuIsVisible()
        {
            //GLOG 3721: Check if context menu is visible in current tab
            if (mnuMacPac.Visible)
                return true;
            else if (this.ucDocEditor != null && this.ucDocEditor.Visible)
            {
                return this.ucDocEditor.MenuIsVisible();
            }
            else if (this.ucDocDesigner != null && this.ucDocDesigner.Visible)
            {
                return this.ucDocDesigner.MenuIsVisible();
            }
            else if (this.ucContentManager != null)
                return this.ucContentManager.MenuIsVisible();
            else
                return false;
        }

        #endregion
        #region *********************methods*********************
        public void ShowMacPacMenu()
        {
            SetupMacPacMenu();
            TaskPane oTP = TaskPanes.Item(Session.CurrentWordApp.ActiveDocument);
            this.mnuMacPac.Show(oTP.picMenu, new Point(0, 0));
        }
        /// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(components != null)
					components.Dispose();
			}
			base.Dispose(disposing);
		}
		/// <summary>
		/// selects the specified tab
		/// </summary>
		/// <param name="iTab"></param>
		public void SelectTab(TaskPane.Tabs iTab)
		{
            DateTime t0 = DateTime.Now;

            if (this.tabMain.SelectedTab != this.tabMain.Tabs[(int)iTab])
            {
                this.tabMain.SelectedTab = this.tabMain.Tabs[(int)iTab];

                if (iTab == Tabs.Review)
                {
                    this.DataReviewerVisible = true;
                    this.DataReviewer.RefreshTree(true);
                }
            }

            LMP.Benchmarks.Print(t0, iTab.ToString());
        }
        /// <summary>
        /// True if this TaskPane has been hidden due to a DocumentChange event
        /// </summary>
        public bool AutoClosed
        {
            get { return m_bAutoClosed; }
            set 
            { 
                m_bAutoClosed = value;
                if (!m_bAutoClosed)
                {
                    //GLOG 2733: If Taskpane has been redisplayed after DocumentChange,
                    //Update display to ensure Tree is visible
                    this.ScreenUpdating = true;
                    this.ScreenUpdating = false;
                    this.ScreenUpdating = true;
                }
            }
        }

        /// <summary>
        /// updates the task pane after the document editor has been finished
        /// </summary>
        public void UpdateAfterFinish()
        {
            //GLOG item #6249 - dcf
            this.DataReviewerVisible = true;

            //GLOG 5923 (dm) - we're now hiding the task pane, but it still
            //needs to be refreshed to remove tagless variable nodes
            this.Refresh(false, true, false);

            //GLOG 5821 (dm) - expand top level nodes - this fixes errors
            //resulting from not being aware of which nodes are transparent
            //and is also consistent with the way the editor will appear
            //after closing and reopening the document
            this.DocEditor.CollapseAll(null);
            this.DocEditor.ExpandTopLevelNodes();
            //GLOG 15906
            SendKeys.Send("%");
            SendKeys.Send("%");
        }
        /// <summary>
        /// inserts the admin segment with the specified 
        /// definition using the specified options
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="iLocation"></param>
        /// <param name="iBehavior"></param>
        public void InsertSegment(AdminSegmentDef oDef, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iBehavior)
        {
            this.ContentManager.InsertSegment(oDef, iLocation, iBehavior, null, false);
        }
        /// <summary>
        /// inserts the user segment with the specified 
        /// definition using the specified options
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="iLocation"></param>
        /// <param name="iBehavior"></param>
        public void InsertSegment(UserSegmentDef oDef, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iBehavior)
        {
            this.ContentManager.InsertSegment(oDef, iLocation, iBehavior, null, false);
        }

        /// <summary>
        /// inserts the segment associated with
        /// the specified variable set def
        /// </summary>
        /// <param name="oDef"></param>
        public void InsertSegment(VariableSetDef oDef)
        {
            this.ContentManager.InsertSegment(oDef, false);
        }

        /// <summary>
        /// inserts the segment with the specified parameterss
        /// </summary>
        /// <param name="oDef"></param>
        /// <param name="iLocation"></param>
        /// <param name="iBehavior"></param>
        /// <param name="oPrefill"></param>
        /// <param name="bInsertSelectionAsBody"></param>
        public void InsertSegment(AdminSegmentDef oDef, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iBehavior, Prefill oPrefill, bool bInsertSelectionAsBody)
        {
            this.ContentManager.InsertSegment(oDef, iLocation, iBehavior, oPrefill, bInsertSelectionAsBody);
        }

        public void EditVariable()
        {
            this.DocEditor.ShowFloatingControl();
        }
        /// <summary>
        /// Inserts exhibits
        /// </summary>
        /// <param name="oSegment"></param>
        public void InsertExhibits(Segment oSegment)
        {
            try
            {
                mpObjectTypes iType = 0;

                if (oSegment.TypeID.ToString().Contains("Pleading") || oSegment.TypeID == mpObjectTypes.Service)
                    iType = mpObjectTypes.PleadingExhibit;
                else
                    iType = mpObjectTypes.AgreementExhibit;

                AdminSegmentDefs m_oDefs = new AdminSegmentDefs(iType);

                if (m_oDefs.Count == 0)
                {
                    //GLOG 4746
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_FeatureUnderConstruction"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);

                    return;
                }
                //else if (m_oDefs.Count == 1)
                //{
                //    //just insert the exhibit
                //    AdminSegmentDef oDef = (AdminSegmentDef)m_oDefs.ItemFromIndex(1);

                //    if (oDef.DefaultDoubleClickLocation == (short)Segment.InsertionLocations.InsertInNewDocument)
                //    {
                //        TaskPane.InsertSegmentInNewDoc(oDef.ID.ToString(),
                //            null, null);
                //    }
                //    else
                //    {
                //        this.InsertSegmentInCurrentDoc(oDef.ID.ToString(), null,
                //            null, (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation,
                //            (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior);
                //    }
                //}
                else
                {
                    SegmentInsertionForm oForm = new SegmentInsertionForm(iType, false);

                    if (oForm.ShowDialog() == DialogResult.OK)
                    {
                        System.Windows.Forms.Application.DoEvents();

                        LMP.MacPac.Application.ScreenUpdating = false;

                        //get prefill using analyzer
                        Word.Selection oSel = Session.CurrentWordApp.Selection;

                        //GLOG 4040: Call TaskPane functions to insert                    
                        if (oForm.InsertionLocation == Segment.InsertionLocations.InsertInNewDocument)
                        {
                            TaskPane.InsertSegmentInNewDoc(oForm.SegmentDef.ID.ToString(),
                                null, null);
                        }
                        else
                        {
                            this.InsertSegmentInCurrentDoc(oForm.SegmentDef.ID.ToString(), null,
                                null, oForm.InsertionLocation, oForm.InsertionBehavior);
                        }
                    }
                    else
                    {
                        //GLOG : 8000 : ceh - set focus back to the document
                        Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                    }
                }
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// Inserts a agreement title page using the data in the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void InsertAgreementTitlePage(Segment oSegment)
        {
            try
            {
                Prefill oPrefill = null;

                if (oSegment.IsFinished)
                {
                    oPrefill = new Prefill(oSegment);
                }

                AdminSegmentDefs m_oDefs = new AdminSegmentDefs(mpObjectTypes.AgreementTitlePage);

                if (m_oDefs.Count == 0)
                {
                    //GLOG 4746
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_FeatureUnderConstruction"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);

                    return;
                }
                //GLOG #6238 - dcf
                //else if (m_oDefs.Count == 1)
                //{
                //    //just insert the title page
                //    AdminSegmentDef oDef = (AdminSegmentDef)m_oDefs.ItemFromIndex(1);

                //    if (oDef.DefaultDoubleClickLocation == (short)Segment.InsertionLocations.InsertInNewDocument)
                //    {
                //        TaskPane.InsertSegmentInNewDoc(oDef.ID.ToString(), oPrefill, null);
                //    }
                //    else
                //    {
                //        this.InsertSegmentInCurrentDoc(oDef.ID.ToString(), oPrefill,
                //            null, (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation,
                //            (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior);
                //    }
                //}
                else
                {
                    SegmentInsertionForm oForm = new SegmentInsertionForm(mpObjectTypes.AgreementTitlePage, false);
                    if (oForm.ShowDialog() == DialogResult.OK)
                    {
                        System.Windows.Forms.Application.DoEvents();

                        LMP.MacPac.Application.ScreenUpdating = false;

                        //get prefill using analyzer
                        Word.Selection oSel = Session.CurrentWordApp.Selection;

                        //GLOG 4040: Call TaskPane functions to insert                    
                        if (oForm.InsertionLocation == Segment.InsertionLocations.InsertInNewDocument)
                        {
                            TaskPane.InsertSegmentInNewDoc(oForm.SegmentDef.ID.ToString(), oPrefill, null);
                        }
                        else
                        {
                            this.InsertSegmentInCurrentDoc(oForm.SegmentDef.ID.ToString(), oPrefill,
                                null, oForm.InsertionLocation, oForm.InsertionBehavior);
                        }
                    }
                }
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }

        public static string SegmentPasteFile
        {
            get { return LMP.Data.Application.GetDirectory(mpDirectories.WordUser) + @"\ForteSegment.001"; }
        }
        /// <summary>
        /// Selected to Insert segment XML previously copied in Editor
        /// </summary>
        public static void PasteSegmentXML()
        {
            PasteSegmentXML("", "");
        }
        public static void PasteSegmentXML(string xSegmentID, string xXMLForInsert)
        {
            TaskPane oCurTP = TaskPanes.Item(Session.CurrentWordApp.ActiveDocument);
            //Taskpane object is required
            if (oCurTP == null)
                return;
            try
            {
                if (xXMLForInsert == "")
                {
                    string xFilePath = SegmentPasteFile;
                    if (xFilePath == "" || !System.IO.File.Exists(xFilePath))
                        return;
                    System.IO.StreamReader oReader = new StreamReader(xFilePath, System.Text.Encoding.UTF8);
                    xXMLForInsert = oReader.ReadToEnd();
                    oReader.Close();
                }
                if (Session.CurrentWordApp.Selection.Sections[1].Range.Text.Length < 3)
                {
                    //If current selection is in empty section, use that as the target
                    oCurTP.ScreenUpdating = false;
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    //Insert using targeted method to include headers/footers
                    LMP.Architect.Api.Architect oSeg = new LMP.Architect.Api.Architect();
                    oSeg.InsertXML(xXMLForInsert, null, Session.CurrentWordApp.Selection.Range,
                        LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Replace, true, 0);
                    //GLOG 3212: Update styles from inserted XML
                    ForteDocument oMPDoc = new ForteDocument(Session.CurrentWordApp.ActiveDocument);
                    oMPDoc.UpdateStylesFromXML(xXMLForInsert);
                }
                else
                {
                    LMP.Data.ISegmentDef oDef = null;
                    //Parse SegmentID from XML
                    if (xSegmentID == "")
                    {
                        //GLOG 3076: SegmentID needs to be parsed from XML, and may be encrypted
                        xSegmentID = Segment.GetSegmentIDFromXML(xXMLForInsert);
                    }
                    if (xSegmentID.IndexOf(".") == -1 || xSegmentID.EndsWith(".0"))
                    {
                        int iID1 = 0;
                        int iID2 = 0;
                        String.SplitID(xSegmentID, out iID1, out iID2);
                        AdminSegmentDefs oASegs = new AdminSegmentDefs();
                        AdminSegmentDef oASeg = null;
                        try
                        {
                            oASeg = (AdminSegmentDef)oASegs.ItemFromID(iID1);
                        }
                        catch { }
                        if (oASeg != null)
                        {
                            oDef = oASeg;
                        }
                    }
                    else
                    {
                        UserSegmentDefs oUSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.User,
                            Session.CurrentUser.ID);
                        UserSegmentDef oUSeg = null;
                        try
                        {
                            oUSeg = (UserSegmentDef)oUSegs.ItemFromID(xSegmentID);
                        }
                        catch { }
                        if (oUSeg != null)
                        {
                            oDef = oUSeg;
                        }
                    }
                    PrefillChooser oForm = new PrefillChooser(oDef, PrefillChooser.PrefillSources.PastedContent);
                    Segment.InsertionLocations iLocation = 0;
                    Segment.InsertionBehaviors iBehavior = 0;
                    if (oForm.ShowDialog() == DialogResult.OK)
                    {
                        iLocation = (Segment.InsertionLocations)(int)oForm.InsertionLocation;
                        iBehavior = (Segment.InsertionBehaviors)(int)oForm.InsertionBehavior;
                    }
                    else
                        return;

                    if (iLocation == Segment.InsertionLocations.InsertInNewDocument)
                    {
                        //we need to insert in a new document - that document's 
                        //ForteDocument object will need to insert the segment, as 
                        //the current doc's ForteDocument points to the current doc - 
                        //specify a pending action that the new content manager instance 
                        //will process 
                        TaskPane.PendingAction = new PendingAction(
                            PendingAction.Types.PasteSegmentContent, xSegmentID,  xXMLForInsert);
                        //GLOG 3088: Create document based on appropriate template
                        string xTemplate = Segment.GetNewDocumentTemplate(xSegmentID, xXMLForInsert);
                        //create a new document
                        Word.Document oNewDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                            xTemplate, true, true);
                        oNewDoc.Activate();
                        LMP.Forte.MSWord.WordDoc.ResetEmptyHeader();
                        //Allow TaskPane Setup to complete
                        System.Windows.Forms.Application.DoEvents();
                        return;
                    }
                    else
                    {
                        oCurTP.ScreenUpdating = false;
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        Word.Document oWordDoc = oCurTP.ForteDocument.WordDocument;
                        //deal with insertion location
                        System.Array aInsertionLocations = LMP.Forte.MSWord.WordDoc.GetInsertionLocations(oWordDoc, (short)iLocation, null, true); //GLOG 6021 //GLOG 6820

                        foreach (Word.Range oLocation in aInsertionLocations)
                        {
                            Word.Range oInsertRange = oLocation.Duplicate;

                            //deal with insertion behavior
                            //GLOG - 3590 - CEH
                            bool bInsertInNextPageSection = (iBehavior &
                                Segment.InsertionBehaviors.InsertInSeparateNextPageSection) ==
                                Segment.InsertionBehaviors.InsertInSeparateNextPageSection;

                            bool bInsertInContinuousSection = (iBehavior &
                                Segment.InsertionBehaviors.InsertInSeparateContinuousSection) ==
                                Segment.InsertionBehaviors.InsertInSeparateContinuousSection;

                            LMP.Forte.MSWord.mpHeaderFooterInsertion iHF = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_None;
                            if (bInsertInNextPageSection || bInsertInContinuousSection)
                            {
                                //insert section at location
                                //GLOG 3165: Use selected option for Restart Page Numbering
                                oInsertRange = LMP.Forte.MSWord.WordDoc.InsertSection(oLocation, 
                                    (iBehavior & Segment.InsertionBehaviors.RestartPageNumbering) > 0, bInsertInContinuousSection);
                                // GLOG 2829: Honor selection for Keep Existing Headers
                                if ((iBehavior & Segment.InsertionBehaviors.KeepExistingHeadersFooters) == 0)
                                    iHF = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Replace;
                            }
                            //Insert using targeted method to include headers/footers
                            LMP.Architect.Api.Architect oSeg = new LMP.Architect.Api.Architect();
                            oSeg.InsertXML(xXMLForInsert, null, oInsertRange, iHF, true, 0);
                        }
                    }
                }
                if (Session.CurrentWordApp.ActiveDocument.Bookmarks.Exists("mpNewSegment"))
                {
                    object oBookmark = "mpNewSegment";
                    Session.CurrentWordApp.ActiveDocument.Bookmarks.get_Item(ref oBookmark).Delete();
                }

                //GLOG 5260 (dm) - add document variables
                LMP.Forte.MSWord.WordDoc.AddDocumentVariablesFromXML(
                    Session.CurrentWordApp.ActiveDocument, xXMLForInsert);

                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                //switch to Edit mode if Content Manager is displayed
                if (oCurTP.Mode != ForteDocument.Modes.Design && oCurTP.tabMain.SelectedTab.Index == (int)Tabs.Content)
                {
                    oCurTP.Mode = ForteDocument.Modes.Edit;
                    oCurTP.SelectTab(TaskPane.Tabs.Edit);
                }
                oCurTP.Refresh(false, true, false);
                if (oCurTP.Mode == ForteDocument.Modes.Edit)
                {
                    //JTS 1/06/09: Not sure of the purpose of remmed out code below.
                    //This was causing an error because TaskPane.NewSegment wasn't previously
                    //assigned in this situation.  But is seems like if we're going to select
                    //a tree node here, it should be the first node of the Pasted segment, not
                    //the first node in the tree
                    TaskPane.NewSegment = oCurTP.ForteDocument.GetTopLevelSegmentFromSelection();
                    oCurTP.DocEditor.ExpandTopLevelNodes();
                    if (TaskPane.NewSegment != null)
                        oCurTP.DocEditor.SelectFirstNodeForSegment(TaskPane.NewSegment);


                    System.Windows.Forms.Application.DoEvents();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(LMP.Resources.GetLangString("Error_UnableToPasteSegment"), oE);
            }
            finally
            {
                oCurTP.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// processes the pending action 
        /// </summary>
        public void ProcessPendingAction()
        {
            string xID = "";
            bool bInvalidSegment = false;

            DateTime t0 = DateTime.Now;

            try
            {
                //exit if there's no pending action
                if (TaskPane.PendingAction == null)
                    return;

                //pause timer while action is processing
                if (m_oTaskPaneTimer != null)
                    m_oTaskPaneTimer.Stop();
                xID = TaskPane.PendingAction.SegmentID;
                //GLOG 6966: Make sure string can be parsed as a Double even if
                //the Decimal separator is something other than '.' in Regional Settings
                if (xID.EndsWith(".0"))
                    xID = xID.Replace(".0", "");
                if (!m_bSkipScreenUpdates)
                {
                    this.ScreenUpdating = true;
                    this.ScreenRefresh();
                    this.ScreenUpdating = false;
                }

                TaskPane oActiveTaskPane = TaskPanes.Item(Session.CurrentWordApp.ActiveDocument);
                TaskPane.NewSegment = null;
                TaskPane.NewXmlSegment = null;
                switch (TaskPane.PendingAction.Type)
                {
                    case PendingAction.Types.SetupOxmlSegment:
                        {
                            try
                            {
                                this.Mode = ForteDocument.Modes.Edit;
                                if (TaskPane.PendingAction.XmlSegment != null)
                                {
                                    XmlSegment oXmlSegment = TaskPane.PendingAction.XmlSegment;
                                    TaskPane.NewXmlSegment = oXmlSegment;
                                    this.ForteDocument.RefreshTags(oXmlSegment.ForteDocument.ContentXml);
                                    this.ForteDocument.Refresh(Architect.Base.ForteDocument.RefreshOptions.None, false, false, false);
                                    if (!LMP.MacPac.MacPacImplementation.IsServer)
                                    {
                                        Segment oSegment = this.ForteDocument.FindSegment(oXmlSegment.TagID);
                                        //GLOG 2676 06/24/08 JTS:
                                        //If inserted Segment is Content Shell, save BodyText and Prefill
                                        //to be passed to first sub-type selected in Chooser of Shell segment
                                        if (Segment.IsShellSegment(oSegment))
                                        {
                                            this.TransientBodyText = TaskPane.PendingAction.BodyText;
                                            this.TransientPrefill = TaskPane.PendingAction.Prefill;
                                        }
                                        //GLOG 3537: Select start of new Segment by default
                                        //to ensure 1st section of multisection content is selected
                                        if (oSegment != null)
                                        {
                                            ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents |
                                                ForteDocument.WordXMLEvents.XMLSelectionChange;
                                            Word.Range oRange = null;

                                            try
                                            {
                                                oRange = oSegment.PrimaryRange;
                                            }
                                            catch { }

                                            if (oRange == null)
                                                break;

                                            //don't select the segment unless it's
                                            //being inserted into the main story of the document
                                            if (oRange.StoryType == Word.WdStoryType.wdMainTextStory)
                                            {
                                                object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                                                oRange.Collapse(ref oDirection);
                                                oRange.Select();
                                            }
                                            ForteDocument.IgnoreWordXMLEvents = iEvents;
                                        }

                                        oActiveTaskPane.SelectTab(TaskPane.Tabs.Edit);
                                        //System.Windows.Forms.Application.DoEvents();

                                        //GLOG 8508: Forced Refresh not necessary for Oxml segments
                                        oActiveTaskPane.DocEditor.ExpandTopLevelNodes(false);

                                        oActiveTaskPane.DocEditor.UpdateSegmentImage(oXmlSegment, true, false);
                                        //DisplaySegmentWizard(oSegment);
                                    }
                                    TaskPane.NewXmlSegment = null;
                                }
                                else
                                {
                                    TaskPane.PendingAction = null;
                                }
                            }
                            catch (System.Exception oE)
                            {
                                bInvalidSegment = true;
                                throw new LMP.Exceptions.SegmentDefinitionException(
                                  LMP.Resources.GetLangString(
                                  "Error_InvalidSegmentIDCantUseFeature") + oE);
                            }
                            break;
                        }
                    case PendingAction.Types.Insert:
                        {
                            try
                            {
                                //switch to edit mode
                                this.Mode = ForteDocument.Modes.Edit;
                                LMP.MacPac.Application.SetXMLTagStateForEditing();

                                //GLOG 7995 (dm) - remmed in light of Microsoft hotfix
                                ////GLOG 6967 (dm) - if segment has pleading paper, revert to
                                ////Word 2010 compatibility mode and suppress extra space at top
                                ////GLOG 7845 (dm) - moved this code to before content is inserted
                                ////because setting compatibility mode later was corrupting the
                                ////content controls in the sidebar
                                //Word.Document oNewDoc = this.ForteDocument.WordDocument;
                                //if (LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(oNewDoc) > 14)
                                //{
                                //    //GLOG 7821 (dm) - search xml, not document, for pleading paper
                                //    ISegmentDef oDef = Segment.GetDefFromID(xID);
                                //    string xSearch = "|ObjectTypeID=" + ((Int32)LMP.Data.mpObjectTypes.PleadingPaper).ToString() + "|";
                                //    if (oDef.XML.Contains(xSearch))
                                //    {
                                //        LMP.fWordObjects.cWord14.SetCompatibilityMode(oNewDoc, 14);
                                //        oNewDoc.Compatibility[Word.WdCompatibility.wdSuppressTopSpacing] = true;
                                //    }
                                //}

                                    //insert the specified segment into the document
                                    Segment oSegment = this.ForteDocument.InsertSegment(xID, null,
                                        Segment.InsertionLocations.InsertInNewDocument,
                                        Segment.InsertionBehaviors.Default, true,
                                        TaskPane.PendingAction.Prefill, TaskPane.PendingAction.BodyText,
                                        true, true, TaskPane.PendingAction.AlternateXML, false,
                                        TaskPane.PendingAction.ForcePrefillOverride,
                                        TaskPane.PendingAction.ChildStructure);

                                    if (oSegment == null)
                                    {
                                        TaskPane.PendingAction = null;
                                        return;
                                    }

                                    if (!LMP.MacPac.MacPacImplementation.IsServer)
                                    {
                                        //GLOG 2676 06/24/08 JTS:
                                        //If inserted Segment is Content Shell, save BodyText and Prefill
                                        //to be passed to first sub-type selected in Chooser of Shell segment
                                        if (Segment.IsShellSegment(oSegment))
                                        {
                                            this.TransientBodyText = TaskPane.PendingAction.BodyText;
                                            this.TransientPrefill = TaskPane.PendingAction.Prefill;
                                        }
                                        //GLOG 3537: Select start of new Segment by default
                                        //to ensure 1st section of multisection content is selected
                                        if (oSegment != null)
                                        {
                                            ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents |
                                                ForteDocument.WordXMLEvents.XMLSelectionChange;
                                            Word.Range oRange = null;

                                            try
                                            {
                                                oRange = oSegment.PrimaryRange;
                                            }
                                            catch { }

                                            if (oRange == null)
                                                break;

                                            //don't select the segment unless it's
                                            //being inserted into the main story of the document
                                            if (oRange.StoryType == Word.WdStoryType.wdMainTextStory)
                                            {
                                                object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                                                oRange.Collapse(ref oDirection);
                                                oRange.Select();
                                            }
                                            ForteDocument.IgnoreWordXMLEvents = iEvents;
                                        }

                                        oActiveTaskPane.SelectTab(TaskPane.Tabs.Edit);
                                        //System.Windows.Forms.Application.DoEvents();

                                        //12-15-11 (dm) - force refresh - this is necessary
                                        //because insertion of collection table structure
                                        //may result in ui updates that prevent ultimate refresh
                                        oActiveTaskPane.DocEditor.ExpandTopLevelNodes(true);

                                        oActiveTaskPane.DocEditor.UpdateSegmentImage(oSegment, true);
                                        //DisplaySegmentWizard(oSegment);
                                    }

                                    TaskPane.NewSegment = oSegment;
                                break;
                            }
                            //GLOG 6250 jsw - if segment is invalid, throw appropriate exception
                            catch (System.Exception oE)
                            {
                                bInvalidSegment = true;
                                throw new LMP.Exceptions.SegmentDefinitionException(
                                  LMP.Resources.GetLangString(
                                  "Error_InvalidSegmentIDCantUseFeature") + oE);
                            }
                        }
                    case PendingAction.Types.RecreateLegacyDocument:
                        {
                            //switch to edit mode
                            this.Mode = ForteDocument.Modes.Edit;
                            LMP.MacPac.Application.SetXMLTagStateForEditing();

                            //insert segment into current document using prefill
                            Segment oSegment = this.ForteDocument.InsertSegment(xID,
                                null, Segment.InsertionLocations.InsertAtStartOfCurrentSection,
                                Segment.InsertionBehaviors.Default, false, TaskPane.PendingAction.Prefill,
                                TaskPane.PendingAction.BodyText,
                                true, true, null, false, true, null);

                            oActiveTaskPane.SelectTab(TaskPane.Tabs.Edit);
                            System.Windows.Forms.Application.DoEvents();
                            oActiveTaskPane.DocEditor.ExpandTopLevelNodes();
                            TaskPane.NewSegment = oSegment;
                            break;
                        }
                    case PendingAction.Types.InsertForPreview:
                        {
                            //switch to preview mode
                            this.Mode = ForteDocument.Modes.Preview;
                            LMP.MacPac.Application.SetXMLTagStateForEditing();

                            //insert the specified segment into the document
                            Segment oSeg = this.ForteDocument.InsertSegment(xID, null,
                                Segment.InsertionLocations.InsertInNewDocument,
                                Segment.InsertionBehaviors.Default, true,
                                TaskPane.PendingAction.Prefill, TaskPane.PendingAction.BodyText,
                                true, true, TaskPane.PendingAction.AlternateXML);

                            if (oSeg.HasDefinition)
                            {
                                string xDocPreview = LMP.Resources.GetLangString("Msg_DocumentPreview");

                                Session.CurrentWordApp.ActiveWindow.Caption =
                                    oSeg.Definition.DisplayName + xDocPreview;
                            }

                            oActiveTaskPane.SelectTab(TaskPane.Tabs.Edit);
                            oActiveTaskPane.tabMain.Tabs[0].Visible = false;
                            oActiveTaskPane.DocEditor.ExpandTopLevelNodes();
                            //GLOG - 3344 - CEH
                            oActiveTaskPane.DocEditor.RefreshTree();

                            TaskPane.NewSegment = oSeg;
                            break;
                        }
                    case PendingAction.Types.InsertForDesign:
                        {
                            //switch to design mode
                            this.Mode = ForteDocument.Modes.Design;

                            //insert the segment for design
                            Segment oSeg = null;
#if Oxml
                            if (LMP.MacPac.Application.IsOxmlSupported())
                            {
                                //JTS: Oxml content already inserted, just refresh
                                this.ForteDocument.Refresh(Architect.Base.ForteDocument.RefreshOptions.None);
                                oSeg = this.ForteDocument.Segments[0];
                                //GLOG 8744: select start of mSEG
                                Word.Range oSegmentRng = oSeg.PrimaryRange;
                                object oMissing = System.Reflection.Missing.Value;
                                oSegmentRng.Collapse(ref oMissing);
                                oSegmentRng.Select();
                                this.ForteDocument.WordDocument.Saved = true;
                            }
                            else
#endif
                            {

                                oSeg = this.ForteDocument.InsertSegmentForDesign(xID);
                            }


                            if (oSeg.HasDefinition)
                            {
                                string xDocDesign = LMP.Resources.GetLangString("Msg_DocumentDesign");

                                Session.CurrentWordApp.ActiveWindow.Caption =
                                    oSeg.Definition.DisplayName + xDocDesign;
                            }

                            //show ImportVariables tab only if designing an admin segment
                            if (oSeg is AdminSegment || (Session.CurrentUser.IsContentDesigner && oSeg.TypeID != mpObjectTypes.UserSegment)) //GLOG 8376
                            {
                                AddDesignerLibraryIfNecessary();

                                this.tabMain.Tabs[2].Visible = true;
                                this.ucVariableImportManager.Setup();
                                this.ucVariableImportManager.TaskPane = this;
                            }
                            //select tab
                            oActiveTaskPane.SelectTab(TaskPane.Tabs.Edit);
                            TaskPane.NewSegment = oSeg;
                            break;
                        }
                    case PendingAction.Types.SaveSegmentDesign:
                        {
                            try
                            {
                                //switch to design mode
                                this.Mode = ForteDocument.Modes.Design;

                                //insert the segment for design
                                Segment oSeg = this.ForteDocument.InsertSegmentForDesign(xID);

                                if (oSeg.HasDefinition)
                                {
                                    string xDocDesign = LMP.Resources.GetLangString("Msg_DocumentDesign");

                                    Session.CurrentWordApp.ActiveWindow.Caption =
                                        oSeg.Definition.DisplayName + xDocDesign;
                                }

                                //show ImportVariables tab only if designing an admin segment
                                if (oSeg is AdminSegment)
                                {
                                    AddDesignerLibraryIfNecessary();

                                    this.tabMain.Tabs[2].Visible = true;
                                    this.ucVariableImportManager.Setup();
                                    this.ucVariableImportManager.TaskPane = this;
                                }
                                //select tab
                                oActiveTaskPane.SelectTab(TaskPane.Tabs.Edit);
                                System.Windows.Forms.Application.DoEvents();
                                TaskPane.NewSegment = oSeg;

                                //note that task pane is set up
                                m_bIsSetUp = true;


                                //save design
                                oActiveTaskPane.DocDesigner.SaveSegmentDesign(false,true);

                                break;
                            }
                            catch
                            {
                                //keep track of ID/Display Name for Segments where error occured
                                m_xErrorList = m_xErrorList + xID + " - " + LMP.Data.Application.GetSegmentNameFromID((int)Single.Parse(xID)) + "\n";
                                break;
                            }
                        }
                    case PendingAction.Types.SetUpForDesign:
                        {
                            //insert the segment for design
                            Segment oSeg = this.ForteDocument.SetUpSegmentForDesign(xID);

                            if (oSeg.HasDefinition)
                                Session.CurrentWordApp.ActiveWindow.Caption =
                                    oSeg.Definition.DisplayName;

                            //switch to design mode
                            this.Mode = ForteDocument.Modes.Design;

                            //select tab
                            oActiveTaskPane.SelectTab(TaskPane.Tabs.Edit);
                            TaskPane.NewSegment = oSeg;
                            break;
                        }
                    case PendingAction.Types.PasteSegmentContent:
                        {
                            //Pasting Segment into new document
                            TaskPane.PasteSegmentXML(TaskPane.PendingAction.SegmentID, TaskPane.PendingAction.AlternateXML);
                            break;
                        }
                    case PendingAction.Types.SetDesignMode:
                        {
                            //switch to design mode
                            this.Mode = ForteDocument.Modes.Design;
                            break;
                        }
                }

                //clear out pending action
                TaskPane.PendingAction = null;
            }
            catch (System.Exception oE)
            {
                TaskPane.PendingAction = null;
                //GLOG 6250 jsw - if segment is invalid, throw appropriate exception
                if (bInvalidSegment)
                {
                    throw new LMP.Exceptions.SegmentDefinitionException(
                        LMP.Resources.GetLangString(
                        "Error_InvalidSegmentIDCantUseFeature") + oE);

                }
                else
                {
                    throw new LMP.Exceptions.PendingActionException(
                        LMP.Resources.GetLangString("Error_PendingActionCouldNotExecute") +
                        PendingAction.Types.Insert.ToString() + " " + xID, oE);

                }
            }
            finally
            {
                if (!m_bSkipScreenUpdates)
                {
                    this.ScreenUpdating = true;
                    this.Cursor = Cursors.Default;
                }

                if (m_oTaskPaneTimer != null)
                    m_oTaskPaneTimer.Start();

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// sets up the designer library control
        /// if it hasn't already been setup
        /// </summary>
        private void AddDesignerLibraryIfNecessary()
        {
            if (this.ucVariableImportManager != null)
                return;

            this.ucVariableImportManager = new LMP.MacPac.DesignerLibraryManager();

            // 
            // pageImportVariables
            // 
            this.pageImportVariables.Controls.Add(this.ucVariableImportManager);
            this.pageImportVariables.Location = new System.Drawing.Point(-10000, -10000);
            this.pageImportVariables.Name = "pageImportVariables";
            this.pageImportVariables.Size = new System.Drawing.Size(312, 698);
            // 
            // ucVariableImportManager
            // 
            this.ucVariableImportManager.BackColor = System.Drawing.Color.Transparent;
            this.ucVariableImportManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucVariableImportManager.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucVariableImportManager.Location = new System.Drawing.Point(0, 0);
            this.ucVariableImportManager.Name = "ucVariableImportManager";
            this.ucVariableImportManager.Size = new System.Drawing.Size(312, 698);
            this.ucVariableImportManager.TabIndex = 0;
        }
        /// <summary>
        /// When true, calls to ScreenUpdating will always set to the false value
        /// </summary>
        internal static bool SkipScreenUpdates
        {
            get { return m_bSkipScreenUpdates; }
            set { m_bSkipScreenUpdates = value; }
        }
        internal static Segment NewSegment
        {
            get { return m_oNewSegment; }
            set { m_oNewSegment = value; }
        }
        internal static XmlSegment NewXmlSegment
        {
            get { return m_oNewXmlSegment; }
            set { m_oNewXmlSegment = value; }
        }

        /// <summary>
        /// Do not display TaskPane for new document if Segment has no automation
        /// </summary>
        public static bool HideForNewDocument
        {
            //GLOG 15916
            get
            {
                //GLOG 15933: Only hide if User Application Setting is unchecked
                UserApplicationSettings oSettings = new UserApplicationSettings(Session.CurrentUser.ID);
                if (oSettings.ShowTaskPaneWhenNoVariables)
                    return false;
                //GLOG 15931: Never hide when designing or previewing segment
                if (TaskPane.NewSegment != null && TaskPane.NewSegment.ForteDocument.Mode == Architect.Base.ForteDocument.Modes.Edit)
                    return !TaskPane.NewSegment.HasVisibleUIElements;
                else if (TaskPane.NewXmlSegment != null && TaskPane.NewXmlSegment.ForteDocument.Mode == Architect.Base.ForteDocument.Modes.Edit)
                    return !TaskPane.NewXmlSegment.HasVisibleUIElements;
                else
                    return false;
            }
        }
        public static bool SegmentRibbonTabSelectionRequired
        {
            get { return m_bRibbonTabSelectRequired; }
            set { m_bRibbonTabSelectRequired = value; }
        }
        /// <summary>
        /// Display the Segment Wizard for the specified Segment object
        /// </summary>
        /// <param name="oSeg"></param>
        public void DisplaySegmentWizard(Segment oSeg)
        {
            if (oSeg != null && oSeg.DisplayWizard)
            {
                this.ScreenUpdating = true;
                this.ScreenUpdating = false;
                this.ScreenUpdating = true;
                SegmentWizard oWiz = new SegmentWizard(oSeg);
                DialogResult iRet = oWiz.ShowDialog();
            }
        }
        /// <summary>
        /// sets up the MacPac menu
        /// </summary>
        public void SetupMacPacMenu()
        {
            this.mnuMacPac_RefreshTree.Text = LMP.Resources.GetLangString("Menu_MacPac_RefreshTree");
            this.mnuMacPac_ShowAbout.Text = LMP.Resources.GetLangString("Menu_MacPac_ShowAbout");
            this.mnuMacPac_ShowKeyboardShortcuts.Text = LMP.Resources.GetLangString("Menu_MacPac_ShowKeyboardShortcuts");
            //GLOG : 7998 : ceh
			this.mnuMacPac_ShowAdministrator.Text = LMP.Resources.GetLangString("Menu_MacPac_ShowAdministrator");

            //GLOG : 7260 : jsw
			//GLOG : 7998 : ceh
            if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
            {
                this.mnuMacPac_PasteSegment.Visible = false;
                this.mnuMacPac_ManagePeople.Visible = false;
                this.mnuMacPac_EditAuthorPrefs.Visible = false;
                this.mnuMacPac_RecreateLegacyContent.Visible = false;
                this.mnuMacPac_SaveLegacyData.Visible = false;
                this.mnuMacPac_AppSettings.Visible = false;
                this.mnuMacPac_ChangeUser.Visible = false;
                this.mnuMacPac_UpdateCI.Visible = false;
                this.mnuMacPac_FavoriteSegments.Visible = false;
                this.mnuMacPac_ShowAdministrator.Visible = LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
                this.mnuMacPac_ImportSegment.Visible = false;
                this.mnuMacPac_PauseResume.Visible = false;
                this.mnuMacPac_RecreateLegacyContent.Visible = false;
                this.mnuMacPac_SaveLegacyData.Visible = false;
                this.mnuMacPac_UpdateDocumentContent.Visible = false;
                this.mnuMacPac_Sep1.Visible = false;
                this.mnuMacPac_Sep2.Visible = false;
                this.mnuMacPac_Sep3.Visible = false;
                this.mnuMacPac_Sep4.Visible = false;
                this.mnuMacPac_Sep5.Visible = LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
                this.mnuMacPac_Sep7.Visible = false;
            }
            else
            {

                this.mnuMacPac_PasteSegment.Text = LMP.Resources.GetLangString("Menu_MacPac_PasteSegment");
                this.mnuMacPac_ManagePeople.Text = LMP.Resources.GetLangString("Menu_MacPac_ManagePeople");
                this.mnuMacPac_EditAuthorPrefs.Text = LMP.Resources.GetLangString("Menu_MacPac_EditAuthorPreferences");
                this.mnuMacPac_RecreateLegacyContent.Text = LMP.Resources.GetLangString("Menu_MacPac_RecreateLegacyContent");
                this.mnuMacPac_SaveLegacyData.Text = LMP.Resources.GetLangString("Menu_MacPac_SaveLegacyData");
                this.mnuMacPac_AppSettings.Text = LMP.Resources.GetLangString("Menu_MacPac_ApplicationSettings");
                this.mnuMacPac_ChangeUser.Text = LMP.Resources.GetLangString("Menu_MacPac_ChangeUser");
                this.mnuMacPac_UpdateCI.Text = LMP.Resources.GetLangString("Menu_MacPac_UpdateCI");
                this.mnuMacPac_FavoriteSegments.Text = LMP.Resources.GetLangString("Menu_MacPac_FavoriteSegments");
                this.mnuMacPac_PasteSegment.Visible = false;
                
                //show the administrator menu item only if the Forte admin exists
                this.mnuMacPac_Sep5.Visible = LMP.MacPac.Session.AdminMode;
                this.mnuMacPac_ShowAdministrator.Visible = LMP.MacPac.Session.AdminMode;
                this.mnuMacPac_ImportSegment.Visible = false;
                this.mnuMacPac_PauseResume.Visible = false;

                //GLOG item #4411 - dcf
                this.mnuMacPac_UpdateCI.Visible = false;

                // JTS 09/03/08: Make sure m_oFirmAppSettings has been initialized
                if (m_oFirmAppSettings == null)
                {
                    m_oFirmAppSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                }

                //GLOG item #4273 - dcf
                this.mnuMacPac_ChangeUser.Visible = (!Session.AdminMode) && (m_oFirmAppSettings.AllowProxying);

                // GLOG : 3135 : JAB
                // Add a menu item for updating document content.
                this.mnuMacPac_UpdateDocumentContent.Text = LMP.Resources.GetLangString(
                    "Menu_MacPac_UpdateDocumentContent");

                //GLOG item #4400 - dcf
                this.mnuMacPac_RecreateLegacyContent.Visible = m_oFirmAppSettings.AllowLegacyDocumentFunctionality;
                this.mnuMacPac_SaveLegacyData.Visible = m_oFirmAppSettings.AllowLegacyDocumentFunctionality;
                this.mnuMacPac_Sep3.Visible = m_oFirmAppSettings.AllowLegacyDocumentFunctionality;
            }
        }
        public void UpdateEditorSegmentImages()
        {
            this.DocEditor.UpdateSegmentImages();
        }
        public void Refresh(bool bContentManager, bool bEditorDesigner, bool bAppearance)
        {
            Refresh(bContentManager, bEditorDesigner, bAppearance, false);
        }
        /// <summary>
        /// refreshes specified visible components of the task pane
        /// </summary>
        public void Refresh(bool bContentManager, bool bEditorDesigner, bool bAppearance, bool bSelectedSegment)
        {
            try
            {
                if (bContentManager)
                    this.ContentManager.RefreshTree();

                if (bEditorDesigner)
                {
                    //GLOG 5889: Need to differentiate between Design and Edit mode
                    if (Mode == ForteDocument.Modes.Design)
                    {
                        this.DocDesigner.RefreshTree();
                        Segment oSeg = this.ForteDocument.GetTopLevelSegmentFromSelection();
                        if (oSeg != null)
                            this.DocDesigner.SelectNode(oSeg.FullTagID);
                    }
                    else
                    {
                        this.DocEditor.RefreshTree(bSelectedSegment); //7618: Expand nodes if segment selected

                        //GLOG 4766: After refresh, select top level node from current Selection
                        //so that Segment Ribbon matches selection
                        if (bSelectedSegment)
                        {
                            Segment oSeg = this.ForteDocument.GetTopLevelSegmentFromSelection();
                            //JTS 6/30/11:  Trailers won't be displayed in Tree, so don't try to select
                            if (oSeg != null && !(oSeg is Trailer))
                                this.DocEditor.SelectNode(oSeg.FullTagID);
                        }
                    }
                }

                if (bAppearance)
                {
                    //GLOG - 3615 - CEH
                    //SetBackgroundAppearance();
                    this.RefreshAppearance();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        /// <summary>
        /// selects the tree node that corresponds to the selected segment
        /// </summary>
        public void SelectEditorNodeOfSelectedSegment()
        {
            Segment oSeg = this.ForteDocument.GetTopLevelSegmentFromSelection();
            //JTS 6/30/11:  Trailers won't be displayed in Tree, so don't try to select
            if (oSeg != null && !(oSeg is Trailer))
                this.DocEditor.SelectNode(oSeg.FullTagID);
        }
        /// <summary>
        /// selects the first editor node of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void SelectEditorFirstNodeOfSegment(Segment oSegment)
        {
            if (this.DocEditor != null)
            {
                this.SelectTab(Tabs.Edit);
                this.DocEditor.SelectFirstNodeForSegment(oSegment);
            }
        }
        /// <summary>
        /// deletes the specified segment from 
        /// the document associated with this task pane
        /// </summary>
        /// <param name="oSegment"></param>
        public void DeleteSegment(Segment oSegment)
        {
            this.DocEditor.DeleteSegment(oSegment);
        }
        public void RefreshAppearance()
        {
            DateTime t0 = DateTime.Now;

            Color oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            GradientStyle oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            this.ContentManager.RefreshAppearance(oBackcolor, oGradient);

            if (this.DataReviewer != null)
                this.DataReviewer.RefreshAppearance(oBackcolor, oGradient);

            if (Mode == ForteDocument.Modes.Edit)
                this.DocEditor.RefreshAppearance(oBackcolor, oGradient);
            else if(Mode == ForteDocument.Modes.Design)
                this.DocDesigner.RefreshAppearance(oBackcolor, oGradient);

            SetBackgroundAppearance(oBackcolor, oGradient);

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// finishes the children of the specified segment
        /// if segment is finished with unfinished children -
        /// returns true if the operation was not canceled
        /// by the user
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        internal bool FinishChildrenIfNecessary(Segment oSegment)
        {
            //GLOG 5878 (dm) - removed condition, as it was preventing a proper snapshot
            //getting taken of unfinished children without any variables or blocks -
            //Snapshot.Create() will only create snapshots where they don't already exist
            //12-20-11 (dm) - this does need to be conditioned on oSegment itself
            //already being finished
            //if (oSegment.IsFinishedWithUnfinishedChildren)
            if (Snapshot.Exists(oSegment))
            {
                Snapshot.Create(oSegment);
                LMP.MacPac.Application.RemoveBoundingObjects(
                    oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions.TextSegmentsOnly);
                this.DocEditor.RefreshSegmentNode(oSegment.TagID);
            }

            return true;
        }
        public void UpdateAllHelpWindowsView()
        {
            UpdateContentManagerHelpWindowView();
            UpdateDocumentEditorHelpWindowView();
            UpdateDocumentDesignerHelpWindowView();
        }

        public void UpdateContentManagerHelpWindowView()
        {
            if (this.ContentManager != null)
            {
                try
                {
                    this.ContentManager.SuspendLayout();
                    this.ContentManager.UpdateHelpWindowView();
                }
                finally
                {
                    this.ContentManager.ResumeLayout();
                }
            }
        }

        public void UpdateDocumentEditorHelpWindowView()
        {
            if (this.DocEditor != null)
            {
                try
                {
                    this.DocEditor.SuspendLayout();
                    this.DocEditor.UpdateHelpWindowView();
                }
                finally
                {
                    this.DocEditor.ResumeLayout();
                }
            }
        }

        public void UpdateDataReviewerHelpWindowView()
        {
            if (this.DataReviewer != null)
            {
                try
                {
                    this.DataReviewer.SuspendLayout();
                }
                finally
                {
                    this.DocEditor.ResumeLayout();
                }
            }
        }

        public void UpdateDocumentDesignerHelpWindowView()
        {
            if (this.DocDesigner != null)
            {
                try
                {
                    this.DocDesigner.SuspendLayout();
                    this.DocDesigner.UpdateHelpWindowView();
                }
                finally
                {
                    this.DocDesigner.ResumeLayout();
                }
            }
        }

        public void ActivateFind()
        {
            this.tabMain.SelectedTab = this.pageContent.Tab;
            this.pageContent.Select();
            this.ContentManager.ActivateFind();
        }

        public void Toggle()
        {
            // Toggle to the next task pane tab.

            //GLOG 7774 (dm) - there's only one tab in Forte Tools
            if (LMP.MacPac.MacPacImplementation.IsToolkit)
                return;

            int iNextTabIndex = this.tabMain.SelectedTab.Index + 1;

            if (iNextTabIndex >= this.tabMain.Tabs.Count)
            {
                iNextTabIndex = 0;
            }

            while (iNextTabIndex < this.tabMain.Tabs.Count && !this.tabMain.Tabs[iNextTabIndex].Visible)
            {
                iNextTabIndex++;
            }

            if (iNextTabIndex >= this.tabMain.Tabs.Count)
            {
                iNextTabIndex = 0;
            }

            this.tabMain.SelectedTab = this.tabMain.Tabs[iNextTabIndex];

            switch (this.tabMain.SelectedTab.TabPage.Controls[0].Name)
            {
                //GLOG : 7749 : ceh
                case "ContentManager":
                case "ucContentManager":
                    this.ContentManager.Focus();
                    break;
                case "DocumentEditor":
                    this.DocEditor.Select();
                    this.DocEditor.SelectFirstVariableNode();
                    break;
                case "DocumentDesigner":
                    break;
                case "DataReviewer":
                    this.DataReviewer.Select();
                    this.DataReviewer.SelectFirstTreeNode();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// saves all segment designs
        /// </summary>
        public static void SaveAllSegments()
        {
            try
            {
                object oMissing = System.Reflection.Missing.Value;
                object oFalse = false;
                ArrayList oList = null;
                string xID = "";

                //reset error list
                m_xErrorList = "";

                oList = LMP.Data.Application.GetAllSegments();

                System.Windows.Forms.Application.DoEvents();

                if (oList.Count > 0)
                {
                    //for (int i = 0; i < oList.Count; i++)
                    for (int i = 400; i < oList.Count; i++)
                        {
                        //get id string
                        xID = ((Object[])oList[i])[0].ToString();

                        //1-20-11 (dm) - skip collection segments
                        int iID = System.Int32.Parse(xID);
                        if ((iID > 0) && ((iID < 959) || (iID > 967)))
                        {
                            //AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs();
                            //AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                            //if (oDef.XML.IndexOf("<mc:AlternateContent>") > -1)
                            //{
                                //initiate new pending action
                                TaskPane.PendingAction = new PendingAction(
                                   PendingAction.Types.SaveSegmentDesign, xID, null);

                                System.Windows.Forms.Application.DoEvents();

                                //create a new blank document
                                Word.Document oNewDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                                    LMP.Data.Application.BaseTemplate, true, true);

                                //Mark ActiveDocument as TEMP Doc, so that DocumentBeforeSave event will ignore it
                                oNewDoc.ActiveWindow.Caption = "mpTEMPDoc";

                                oNewDoc.Saved = true;

                                //close the document without saving changes
                                oNewDoc.Close(ref oFalse, ref oMissing, ref oMissing);
                                System.Windows.Forms.Application.DoEvents();
                            //}
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                if (m_xErrorList != "")
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CouldNotProcessFollowingSegments") + "\n\n" + m_xErrorList,
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        /// <summary>
        /// Update current document for 2013 compatibility
        /// </summary>
        //GLOG : 7494 : CEH
        public static void UpdateDocumentForWord2013Compatibility()
        {
            try
            {
                //Replace Pleading Paper
                LMP.MacPac.Application.ReplacePleadingPaper();

                //Convert Document Tables
                LMP.Forte.MSWord.WordDoc.ConvertDocumentTables();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            { }
        }
        /// <summary>
        /// converts all segment tables
        /// </summary>
        //GLOG : 6979 : CEH
        public static void ConvertAllTables()
        {
            ConvertAllTables(false);
        }
		//GLOG : 6979 : CEH
        public static void ConvertAllTables(bool bTest)
        {
            object oMissing = System.Reflection.Missing.Value;
            object oFalse = false;
            object oTrue = true;
            ArrayList oList = null;
            string xID = "";
            int count = 0;
            try
            {

                //prompt if not in .docx format
                if (LMP.Forte.MSWord.GlobalMethods.CurWordApp.DefaultSaveFormat.ToUpper() == "DOC")
                {
                    MessageBox.Show(LMP.Resources.GetLangString(
                            "Msg_ForceOpenXMLSaveFormat"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                //reset error list
                m_xErrorList = "";

                //get all db segments
                oList = LMP.Data.Application.GetAllSegments();

                //prompt user
                DialogResult iRet = MessageBox.Show("Total segment count is " + oList.Count.ToString() + ".  Continue?", 
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if ((oList.Count > 0) && (iRet == DialogResult.Yes))
                {

                    //GLOG : 7873 : CEH
                    FirmApplicationSettings oFirmAppSettings = new FirmApplicationSettings(ForteConstants.mpFirmRecordID);

                    //don't prompt during conversion
                    if (!bTest)
                        oFirmAppSettings.PromptAboutUnconvertedTablesInDesign = false;

                    MacPac.Application.ScreenUpdating = false;

                    for (int i = 0; i < oList.Count; i++)
                    {
                        //get id string
                        xID = ((Object[])oList[i])[0].ToString();

                        int iID = System.Int32.Parse(xID);

                        //if (iID != 3877)    //avoid UK Companies for now
                        if (iID != 3877)
                        {
                            AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs();
                            AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                            //only do if xlm contains tables
                            if ((oDef.XML.IndexOf("<w:tbl>") > -1) || (oDef.XML.IndexOf("¬w:tbl&gt;") > -1))
                                {
                                count ++;

                                //initiate new pending action
                                TaskPane.PendingAction = new PendingAction(
                                   PendingAction.Types.SaveSegmentDesign, xID, null);

                                //create a new blank document
                                Word.Document oNewDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                                    LMP.Data.Application.BaseTemplate, true, true);

                                //Mark ActiveDocument as TEMP Doc, so that DocumentBeforeSave event will ignore it
                                oNewDoc.ActiveWindow.Caption = "mpTEMPDoc";

                                try
                                {
                                    //Process in COM
                                    LMP.Forte.MSWord.WordDoc.ConvertDocumentTables(bTest);
                                }
                                catch
                                {
                                    //keep track of ID/Display Name for Segments where error occured
                                    m_xErrorList = m_xErrorList + xID + " - " + LMP.Data.Application.GetSegmentNameFromID((int)Single.Parse(xID)) + "\n";
                                }

                                //Get TaskPane
                                TaskPane oTP = TaskPanes.Item(oNewDoc);

                                //Save Design
                                oTP.DocDesigner.SaveSegmentDesign(false, true);
                                
                                //Finish
                                oNewDoc.Saved = true;

                                //close the document without saving changes
                                oNewDoc.Close(ref oTrue, ref oMissing, ref oMissing);
                            }
                        }
                    }
                                    
                    MessageBox.Show("Processed " + count.ToString() + " segments containing tables");

                    //GLOG : 7873 : CEH
                    //set Prompt to true
                    if (!bTest)
                        oFirmAppSettings.PromptAboutUnconvertedTablesInDesign = true;
                }
            }
            catch {}
            finally
            {
                if (m_xErrorList != "")
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CouldNotProcessFollowingSegments") + "\n\n" + m_xErrorList,
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }
#if Oxml
        internal XmlSegment InsertXmlSegmentInCurrentDoc(string xID, Segment oReplaceSegment, Prefill oPrefill, Segment.InsertionLocations iInsertionLocation, 
            Segment.InsertionBehaviors iInsertionBehavior, string xSelectedText, LMP.Architect.Base.CollectionTableStructure oStructure, Word.Range oRng)
        {

            Segment oParent = null;
            if (oRng == null)
            {
                    oRng = Session.CurrentWordApp.Selection.Range;
            }
            if (oReplaceSegment != null)
            {
                oParent = oReplaceSegment.Parent;
                oRng.SetRange(oReplaceSegment.PrimaryRange.Start, oReplaceSegment.PrimaryRange.Start);
                //GLOG 15785: Existing Paper will be deleted and reinserted for all sections by InsertXML
                if (!(oReplaceSegment is Paper))
                {
                    this.m_oForteDocument.DeleteSegment(oReplaceSegment, true, true, false);
                }
            }

            //Create XMLPrefill using oPrefill settings
            XmlPrefill oXMLPrefill = null;
            LMP.Architect.Oxml.Word.XmlForteDocument oForteDocument = new Architect.Oxml.Word.XmlForteDocument(null);
            if (oPrefill != null)
                oXMLPrefill = new XmlPrefill(oPrefill.ToString(), oPrefill.Name, oPrefill.SegmentID);

            XmlSegment oXmlParent = null;
            if (oParent != null)
            {
                oXmlParent = XmlSegment.CreateSegmentObject(oParent.TypeID, oForteDocument);
                oXmlParent.FullTagID = oParent.FullTagID;
                oXmlParent.Definition = oParent.Definition;
                oXmlParent.ID1 = oParent.ID1;
                oXmlParent.ID2 = oParent.ID2;
            }
            XmlSegment oSegment = LMP.Architect.Oxml.Word.XmlSegmentInsertion.Create(xID, oXmlParent, oForteDocument, oXMLPrefill, xSelectedText, oStructure, null, XmlSegment.OxmlCreationMode.Finalize);
            if (oSegment != null)
            {
                string xXML = oSegment.FlatOxml;
                ISegmentDef oDef = oSegment.Definition;
                Segment oSegmentInsert = Segment.CreateSegmentObject(oDef.TypeID, m_oForteDocument);
                oSegmentInsert.Definition = oDef;
                LMP.Forte.MSWord.mpHeaderFooterInsertion iHeaderFooterInsertionType;

                if (oSegment.IntendedUse != mpSegmentIntendedUses.AsDocument && !(oSegment is XmlPaper))
                    //segment is not a document segment and is not paper -
                    //don't remove header content
                    iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_None;
                else
                {
                    //segment is a document segment or is paper -
                    //set header/footer insertion behavior based on
                    //specified insertion behavior - if none specified, set behavior
                    //according to intended use - 
                    //If Segment is inserted in a header/footer, only insert main body XML
                    if ((iInsertionBehavior & Segment.InsertionBehaviors.KeepExistingHeadersFooters) > 0 ||
                        ((iInsertionLocation == Segment.InsertionLocations.InsertAtSelection)
                        || (iInsertionLocation == Segment.InsertionLocations.Default)) && oRng.StoryType != Word.WdStoryType.wdMainTextStory)
                        iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_None;
                    else if (((iInsertionBehavior & Segment.InsertionBehaviors.KeepExistingHeadersFooters) == 0) &&
                        ((iInsertionBehavior & Segment.InsertionBehaviors.InsertInSeparateNextPageSection) > 0))
                        //Replace existing header/footers if not explicitly set
                        iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion.mpHeaderFooterInsertion_Replace;
                    else
                    {
                        iHeaderFooterInsertionType = LMP.Forte.MSWord.mpHeaderFooterInsertion
                            .mpHeaderFooterInsertion_ReplaceSameOrNoType;
                    }
                }
                //GLOG 15753: Copy Custom Document Properties from Oxml document
                string[,] aProps = XmlForteDocument.GetCustomProperties(oSegment.WPDoc);
                LMP.Forte.MSWord.WordDoc.AddDocumentPropertiesFromArray(m_oForteDocument.WordDocument, aProps, true);
                LMP.Forte.MSWord.WordDoc.AddDocumentVariablesFromXML(m_oForteDocument.WordDocument, xXML, true);
                if (oSegmentInsert.Definition.IntendedUse == mpSegmentIntendedUses.AsDocument)
                {
                    string xName = System.Guid.NewGuid().ToString() + ".docx";
                    string xFileName = oSegment.WriteToTempDirectory(xName);
                    try
                    {
                        LMP.Forte.MSWord.WordDoc.OrganizerCopyStyles(m_oForteDocument.WordDocument, xFileName, null);
                    }
                    finally
                    {
                        System.IO.File.Delete(xFileName);
                    }
                }
                else
                {
                    //GLOG 8821: Update Styles definitions to match Target Doc, plus any styles unique to the source
                    //so that derived style attributes will match base styles in Target
                    xXML = String.MergeStylesWordOpenXML(xXML, m_oForteDocument.WordDocument.Paragraphs[1].Range.WordOpenXML, "");
                    //GLOG 15814: Required Styles should overwrite existing styles in current document
                    if (!string.IsNullOrEmpty(oSegment.RequiredStyles))
                    {
                        string[] aStyleList = oSegment.RequiredStyles.Split(new char[] { ',' });
                        m_oForteDocument.UpdateStylesFromXML(oSegment.Definition.XML, aStyleList);
                    }
                }
                oSegmentInsert.InsertXML(xXML, oParent, oRng, iHeaderFooterInsertionType, true, oSegmentInsert.IntendedUse);
                //GLOG 8477:  If single top-level segment has been inserted, update tags from WordProcessingDocument
                if (oSegmentInsert.IsTopLevel && m_oForteDocument.Segments.Count == 0)
                {
                    m_oForteDocument.RefreshTags(oSegment.ForteDocument.ContentXml);
                    m_oForteDocument.Refresh(Architect.Base.ForteDocument.RefreshOptions.None, false, false, false);
                }
                else
                {
                    m_oForteDocument.RefreshTags(true);
                }
            }
            return oSegment;
        }
#endif
        internal Segment InsertSegmentInCurrentDoc(string xID, Prefill oPrefill, string xSelectedText,
            Segment.InsertionLocations iLocation, Segment.InsertionBehaviors iOptions)
        {
            return InsertSegmentInCurrentDoc(xID, oPrefill, xSelectedText, iLocation, iOptions, null);
        }

        internal Segment InsertSegmentInCurrentDoc(string xID, Prefill oPrefill, string xSelectedText,
            Segment.InsertionLocations iLocation, Segment.InsertionBehaviors iOptions, ISegmentDef oDef)
        {
            //GLOG 7743 (dm) - prompt/disallow if user attempts to insert
            //content into a document that's in an unsupported format
            if ((LMP.Forte.MSWord.WordApp.Version >= 15) &&
                (m_oForteDocument.WordDocument.SaveFormat == 0))
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_DocAutomationNotSupportedInWord2013"),
                   LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
            else if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(
                m_oForteDocument.WordDocument, true))
                return null;

            //GLOG 4040: This function moved to TaskPane to be accessible in Editor or ContentManager
            DateTime t0 = DateTime.Now;
            ForteDocument oMPDocument = this.ForteDocument;
            Segment oSegment = null;

            //GLOG 7936 (dm) - alert user that need to finish existing labels and envelopes
            //before inserting adduitional ones
            if (!xID.Contains(".")) //GLOG 7993 (dm)
            {
                mpObjectTypes iType = LMP.Data.Application.GetSegmentTypeID(System.Int32.Parse(xID));
                if ((iType == mpObjectTypes.Labels || iType == mpObjectTypes.Envelopes) &&
                    (oMPDocument.ContainsSegmentOfType(mpObjectTypes.Labels) ||
                    oMPDocument.ContainsSegmentOfType(mpObjectTypes.Envelopes)))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_UnfinishedLabelsEnvelopes"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;
                }
            }

            int iXMLMarkup = oMPDocument.WordDocument.ActiveWindow.View.ShowXMLMarkup;

            bool bScreenUpdating = this.ScreenUpdating;

            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api
                .Environment(oMPDocument.WordDocument);
            oEnv.SaveState();
            this.ScreenUpdating = false;
            oEnv.SetExecutionState(mpTriState.True, mpTriState.Undefined,
                mpTriState.Undefined, mpTriState.Undefined, mpTriState.True, true);

            try
            {
                //insert segment and refresh task pane
                Word.Range oRange;
                if (this.Mode == ForteDocument.Modes.Design)
                {
                    oRange = oMPDocument.WordDocument.Application.Selection.Range;

                    //validate selection range prior to insertion
                    LMP.Forte.MSWord.TagInsertionValidityStates iValidityState =
                        LMP.Forte.MSWord.TagInsertionValidityStates.Valid;
                    if (m_oForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        //xml tag
                        Word.XMLNode oParentNode = LMP.Forte.MSWord.WordDoc.GetParentSegmentTag(oRange);
                        iValidityState = LMP.Forte.MSWord.WordDoc.ValidateTagInsertion(oRange, oParentNode,
                            LMP.Forte.MSWord.TagTypes.Segment);
                    }
                    else
                    {
                        //content control
                        Word.ContentControl oParentCC = LMP.Forte.MSWord.WordDoc.GetParentSegmentContentControl(oRange);
                        iValidityState = LMP.Forte.MSWord.WordDoc.ValidateContentControlInsertion(oRange,
                            oParentCC, LMP.Forte.MSWord.TagTypes.Segment);
                    }

                    if (iValidityState != LMP.Forte.MSWord.TagInsertionValidityStates.Valid)
                    {
                        //insertion is invalid - alert
                        string xMsg = this.DocDesigner.GetInsertionInvalidityMessage(
                            iValidityState, ForteDocument.TagTypes.Segment);

                        MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        return null;
                    }
                    //insert just the segment xml 
                    oSegment = oMPDocument.InsertSegment(xID, null, iLocation,
                        iOptions, true, oPrefill, "", false, false, null, true, false, null);

                    //refresh all top level segments 
                    oMPDocument.Refresh(ForteDocument.RefreshOptions.None);

                    //insert placeholder text
                    LMP.Forte.MSWord.WordDoc.InsertPlaceholderText(oMPDocument.Tags);
                }
                else
                {
                    //If inserting in empty document, attach to new template if specified
                    bool bAttachTemplate = LMP.Forte.MSWord.WordDoc.DocumentIsEmpty(oMPDocument.WordDocument);
                    Segment oParent = oMPDocument.GetTopLevelSegmentFromSelection();
                    if (!bAttachTemplate && oParent != null &&
                        iLocation == Segment.InsertionLocations.InsertAtSelection &&
                        iXMLMarkup == 0)
                    {
                        //If XML Tags are hidden and cursor is at end of existing Segment
                        //Prompt to insert as child or as separate segment -
                        //GLOG 4241 (dm) - limit prompt to where parent is "as document"
                        //and child is not "as sentence text" - also added code to move
                        //before the start of an existing segment, with the same exceptions
                        Word.Selection oSel = Session.CurrentWordApp.Selection;
                        if (((oDef == null) || (oDef.IntendedUse != mpSegmentIntendedUses.AsSentenceText)) &&
                            oSel.Type == Word.WdSelectionType.wdSelectionIP)
                        {
                            bool bParentIsAsDoc = (oParent.IntendedUse == mpSegmentIntendedUses.AsDocument);

                            //GLOG 6907
                            Word.Range oParentRange = oParent.PrimaryRange;

                            object oUnit = Word.WdUnits.wdCharacter;
                            object oCount = 1;

                            //GLOG 4279 (dm) - in determining whether selection is at end
                            //of parent, disregard empty tags after cursor
                            //if (oSel.End == oParentRange.End)
                            Word.Range oParaRange = oSel.Paragraphs[1].Range;
                            if ((oParaRange.Text == "\r") && (oParentRange.End < oParaRange.End - 1))
                            {
                                //selection is at end of existing mSEG
                                bool bMove = false;
                                {
                                    if (bParentIsAsDoc)
                                    {
                                        //prompt for whether to move
                                        DialogResult iRet = MessageBox.Show(LMP.Resources.GetLangString(
                                            "Prompt_InsertingAtEndOfContent"),
                                            LMP.ComponentProperties.ProductName,
                                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                        if (iRet == DialogResult.No)
                                            bMove = true;
                                    }
                                    else
                                        //move automatically
                                        bMove = true;
                                }

                                if (bMove)
                                {
                                    //Move Selection past closing mSEG tag
                                    oSel.Move(ref oUnit, ref oCount);
                                }
                            }
                            else if ((!bParentIsAsDoc) && (oSel.Start == oParentRange.Start))
                            {
                                //selection is at start of existing mSEG - move before start tag
                                oCount = -1;
                                oSel.Move(ref oUnit, ref oCount);
                            }
                        }
                    }
                    //GLOG 8132: If inserting Paper or Sidebar, make sure it will be handled as child of existing segment
                    if (oDef == null || (oDef.TypeID != mpObjectTypes.Letterhead && oDef.TypeID != mpObjectTypes.PleadingPaper && oDef.TypeID != mpObjectTypes.Sidebar))
                    {
                        oParent = null;
                    }
                    //insert segment 
                    oSegment = oMPDocument.InsertSegment(xID, oParent, iLocation, iOptions,
                        true, null, xSelectedText, bAttachTemplate, bAttachTemplate, 
                        null, false, false, null);

                    if (oSegment != null && oSegment.HasVariables(true))
                    {
                        this.SelectTab(TaskPane.Tabs.Edit);

                        //GLOG 2676 06/24/08 JTS:
                        //Save Body Text and Prefill values in TaskPane, 
                        //so they can be passed to first sub-type selected in Chooser of Shell segment
                        if (Segment.IsShellSegment(oSegment))
                        {
                            this.TransientBodyText = xSelectedText;
                            this.TransientPrefill = oPrefill;
                        }

                        //GLOG item #7770 - dcf -
                        //needed to apply prefill after the refresh,
                        //as the segment objects are completely rebuilt by refresh
                        ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.XMLSelectionChange;
                        //GLOG 8823: Shouldn't need to Refresh tags again, just call Refresh to initialize variables
                        this.ForteDocument.Refresh(Architect.Base.ForteDocument.RefreshOptions.None, false);
                        this.DocEditor.RefreshTree(true, true);
                        ForteDocument.IgnoreWordXMLEvents = oEvents;

                        //need to reference the segment object of the ForteDocument -
                        //the segment object returned by the call above is not the 
                        //object that is part of the ForteDocument
                        oSegment = oMPDocument.FindSegment(oSegment.FullTagID);

                        //GLOG 7842: If no Prefill was used, don't reinitialize variable values
                        if (oPrefill != null)
                        {
                            oSegment.ApplyPrefill(oPrefill);
                            //GLOG 8084: Make sure Language display value matches Prefill
                            this.DocEditor.UpdateLanguageDisplayValue(oSegment);
                        }

                        //Display segment Wizard if appropriate
                        if (oSegment.DisplayWizard)
                            this.DisplaySegmentWizard(oSegment);

                        if (!(oSegment is Paper || oSegment is LMP.Architect.Base.IDocumentStamp))
                        {
                            //GLOG - 3338 - CEH
                            if (oSegment.ShowChooser)
                                //set focus to chooser
                                this.DocEditor.SelectChooserNode();
                            else
                            {
                                if (bAttachTemplate)
                                    //Set focus to first variable control in tree
                                    this.DocEditor.SelectFirstVariableNode();
                                else
                                    this.DocEditor.SelectFirstNodeForSegment(oSegment);
                            }
                        }

                        if (Session.CurrentUser.UserSettings.FitToPageWidth)
                        {
                            oMPDocument.WordDocument.ActiveWindow.View.Zoom.PageFit =
                                Word.WdPageFit.wdPageFitBestFit;
                        }
                    }
                }

                //select start of segment
                ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                if (oSegment != null)
                {
                    oRange = oSegment.PrimaryRange;

                    //GLOG 3216 - don't select the segment unless it's
                    //being inserted into the main story of the document
                    if (oRange.StoryType == Word.WdStoryType.wdMainTextStory)
                    {
                        object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                        oRange.Collapse(ref oDirection);
                        oRange.Select();
                    }
                    //GLOG - 3590 - CEH
                    //protect if specified - 
                    //if password is [EMPTY], protect, but without password
                    string xPwd = oSegment.ProtectedFormPassword;

                    if (xPwd != null && this.Mode != ForteDocument.Modes.Design)
                    {
                        object oTrueArg = true;
                        object oFalseArg = false;
                        //GLOG 4229: Comparison should not be case sensitive
                        object oPwd = (xPwd.ToUpper() == "[EMPTY]") ? "" : xPwd;

                        //reset breaks prior to protecting if necessary
                        LMP.Forte.MSWord.WordDoc.ResetDocumentBreaks();

                        //protect oRange.Sections
                        for (int j = oRange.Sections.First.Index; j == oRange.Sections.Last.Index; j++)
                        {
                            oSegment.ForteDocument.WordDocument.Sections[j].ProtectedForForms = true;
                        }

                        //unprotect other sections 
                        //(otherwise, above code will protect entire document)
                        for (int i = 1; i <= oSegment.ForteDocument.WordDocument.Sections.Count; i++)
                        {
                            if (i < oRange.Sections.First.Index ||
                                i > oRange.Sections.Last.Index)
                            {
                                oSegment.ForteDocument.WordDocument.Sections[i].ProtectedForForms = false;
                            }
                        }
                        //JTS 8/4/11: Delete MacPac BoundingObjects before protecting document
                        LMP.MacPac.Application.RemoveBoundingObjects(oSegment, 
                            LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All);

                        //apply protection to document
                        oSegment.ForteDocument.WordDocument.Protect(Word.WdProtectionType.wdAllowOnlyFormFields, ref oTrueArg,
                            ref oPwd, ref oFalseArg, ref oFalseArg);

                    }
                }
                ForteDocument.IgnoreWordXMLEvents = iEvents;
            }
            finally
            {
                //display xml tags if specified
                int iDisplayXMLTags = 0;
                try
                {
                    iDisplayXMLTags = int.Parse(Session.CurrentUser
                        .GetUserAppKeys().GetValue("DisplayXMLTags"));
                }
                catch
                {
                    //do nothing - key may not yet exist
                }
                LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(oMPDocument.WordDocument, iDisplayXMLTags == 1, true);
                oMPDocument.WordDocument.ActiveWindow.View.ShowXMLMarkup = iDisplayXMLTags;

                oEnv.RestoreState(oSegment is Paper || oSegment is LMP.Architect.Base.IDocumentStamp, false, false);

                //this fixes a problem with screen refreshing
                //whereby the task pane sometimes freezes
                if (bScreenUpdating)
                {
                    this.ScreenUpdating = false;
                    this.ScreenUpdating = true;
                }
                if (this.Mode == ForteDocument.Modes.Edit || this.Mode == ForteDocument.Modes.Preview)
                    this.DocEditor.Focus();
                else if (this.Mode == ForteDocument.Modes.Design)
                    this.DocDesigner.Focus();
                else
                    this.ContentManager.Focus();

                LMP.Benchmarks.Print(t0);

            }

            return oSegment;
        }
        public static void CreateExternalContent(int iID)
        {
            AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.NonMacPacContent);
            AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);

            if (oDef != null)
            {
                string xFile = oDef.XML;

                if (!File.Exists(xFile))
                {
                    //GLOG 5107: Extract filename part of existing path to append to Templates path
                    xFile = System.IO.Path.GetFileName(xFile);
                    xFile = LMP.Data.Application.TemplatesDirectory + "\\" + xFile;
                    if (!File.Exists(xFile))
                        throw new LMP.Exceptions.FileException(
                            LMP.Resources.GetLangString("Error_CouldNotFindFile") + xFile);
                }

                System.Diagnostics.Process.Start(xFile);
            }
        }
        /// <summary>
        /// inserts the specified segment in a new document
        /// </summary>
        public static LMP.Architect.Base.Segment InsertSegmentInNewDoc(string xID, Prefill oPrefill, string xSelectedText)
        {
            return InsertSegmentInNewDoc(xID, oPrefill, xSelectedText, null, false);
        }
        public static LMP.Architect.Base.Segment InsertSegmentInNewDoc(string xID, Prefill oPrefill, string xSelectedText, LMP.Architect.Base.CollectionTableStructure oStructure)
        {
            return InsertSegmentInNewDoc(xID, oPrefill, xSelectedText, oStructure, false);
        }
        public static LMP.Architect.Base.Segment InsertSegmentInNewDoc(string xID, Prefill oPrefill, string xSelectedText, LMP.Architect.Base.CollectionTableStructure oStructure, bool bDesign)
        {
            string xSourceXML = "";

            //GLOG 7821 (dm) - get def here to search for pleading paper below and
            //avoid multiple queries
            ISegmentDef oDef = Segment.GetDefFromID(xID);
            xSourceXML = oDef.XML;

            string xTemplate = null;
#if Oxml
            if (bDesign)
            {
                string xDesignerTemplate = LMP.Data.Application.TemplatesDirectory +
                    @"\MacPacNormalDesigner.dotx";
                if (File.Exists(xDesignerTemplate))
                    xTemplate = xDesignerTemplate;
                else
                    xTemplate = LMP.Data.Application.BaseTemplate;
            }
            else if (String.IsBase64SegmentString(xSourceXML))
            {
                xTemplate = XmlSegment.GetNewDocumentTemplate(xID, xSourceXML);
            }
            else
            {
                xTemplate = Segment.GetNewDocumentTemplate(xID, xSourceXML);
            }

            if (LMP.MacPac.Application.IsOxmlSupported())
            {
                return InsertInNewDocument(xID, oPrefill, xSelectedText, xTemplate, oStructure, bDesign);
            }
            else
            {
                return InsertInNewDocument(xID, oPrefill, xSelectedText, xTemplate, oStructure);
            }
#else
            //GLOG 3088: Create document based on appropriate template
            xTemplate = Segment.GetNewDocumentTemplate(xID, xSourceXML);
            return InsertInNewDocument(xID, oPrefill, xSelectedText, xTemplate, oStructure);
#endif
        }
        private static LMP.Architect.Api.Segment InsertInNewDocument(string xID, Prefill oPrefill, string xSelectedText, string xTemplate)
        {
            return InsertInNewDocument(xID, oPrefill, xSelectedText, xTemplate, null);
        }
        private static LMP.Architect.Api.Segment InsertInNewDocument(string xID, Prefill oPrefill, string xSelectedText, string xTemplate, LMP.Architect.Base.CollectionTableStructure oStructure)
        {
            Word.Document oNewDoc = null;
            TaskPane.NewSegment = null;
            //GLOG 4040: This function moved to TaskPane to be accessible in Editor or ContentManager
            //we need to insert in a new document - that document's 
            //ForteDocument object will need to insert the segment, as 
            //the current doc's ForteDocument points to the current doc - 
            //specify a pending action that the new content manager instance 
            //will process 
            TaskPane.PendingAction = new PendingAction(
                PendingAction.Types.Insert, xID, oPrefill, xSelectedText, null, false, oStructure);

            try
            {
                //create a new document
                oNewDoc = LMP.Forte.MSWord.WordApp.CreateDocument(xTemplate, true, true);
                oNewDoc.Activate();
                //GLOG 8097: Make sure new document becomes the active document
                try
                {
                    oNewDoc.ActiveWindow.Activate();
                }
                catch { }


            }
            catch (System.Exception oE)
            {
                TaskPane.PendingAction = null;
                throw oE;
            }

            if (!LMP.MacPac.MacPacImplementation.IsServer)
            {
                Application.ActivateRibbonIfAppropriate(LMP.MacPac.Application.MacPacRibbonTabControlId);

                TaskPane oTP = TaskPanes.Item(oNewDoc);
                if (!TaskPane.SkipScreenUpdates)
                {

                    if (TaskPane.NewSegment == null)
                    {
                        oNewDoc.Application.ScreenUpdating = true;
                        return null;
                    }

                    //System.Windows.Forms.Application.DoEvents();

                    if (oTP != null && oTP.DocEditor != null)
                    {
                        //GLOG - 3338 - CEH
                        if (TaskPane.NewSegment.ShowChooser)
                            oTP.DocEditor.SelectChooserNode();
                        else
                        {
                            if (TaskPane.NewSegment.Variables.Count > 0)
                                //Set focus to first variable control in tree
                                oTP.DocEditor.SelectFirstVariableNode();
                            else
                                oTP.DocEditor.SelectFirstNodeForSegment(TaskPane.NewSegment);
                        }
                    }
                }
                else
                {
                    //Allow TaskPane Setup to complete
                    System.Windows.Forms.Application.DoEvents();
                }

                //Save/Profile document if Front-end profiling configured
                LMP.MacPac.Application.ForceProfileIfNecessary();

                //GLOG 7092:  TaskPane may not be displayed in Ribbon-only mode - treat same as Editor
                if (oTP == null || oTP.Mode != ForteDocument.Modes.Design)
                {
                    if (oTP != null && TaskPane.HideForNewDocument)
                    {
                        //GLOG 15933: Hide TaskPane if there are no variables
                        oTP.WordTaskPane.Visible = false;
                        oTP.AutoClosed = false;
                    }
                    else if (!LMP.MacPac.Application.ShouldShowTaskPaneForProtectedDoc(TaskPane.NewSegment.ForteDocument))
                    {
                        ProtectIfSpecified(TaskPane.NewSegment, true);
                    }
                    else
                    {
                        ProtectIfSpecified(TaskPane.NewSegment, false);
                    }
                }
            }

            return TaskPane.NewSegment;
        }
#if Oxml
        private static LMP.Architect.Oxml.XmlSegment InsertInNewDocument(string xID, Prefill oPrefill, string xSelectedText, string xTemplate, LMP.Architect.Base.CollectionTableStructure oStructure, bool bDesign)
        {
            DateTime t0 = DateTime.Now;
            
            WaitCursorForm oWaitCursor = new WaitCursorForm();
            string xDefXml = "";
            try
            {
                ISegmentDef oDef = Segment.GetDefFromID(xID);
                if (oDef != null)
                {
                    xDefXml = oDef.XML;
                }
            }
            catch { }
            //GLOG 8562: Only show insertion message if 
            //threshold of docvars and content controls is surpassed
            if (String.XMLComplexity(xDefXml) > 250)
            {
                oWaitCursor.Show();
                System.Windows.Forms.Application.DoEvents();
            }

            try
            {
                Word.Document oNewDoc = null;
                LMP.Architect.Oxml.Word.XmlForteDocument oForteDocument = null;
                ForteDocument.IgnoreWordXMLEvents = Architect.Base.ForteDocument.WordXMLEvents.All;
                //Create XMLPrefill using oPrefill settings
                XmlPrefill oXMLPrefill = null;
                if (oPrefill != null)
                    oXMLPrefill = new XmlPrefill(oPrefill.ToString(), oPrefill.Name, oPrefill.SegmentID);

                XmlSegment oSegment = null;
                XmlSegment.OxmlCreationMode iMode = bDesign ? XmlSegment.OxmlCreationMode.Design : XmlSegment.OxmlCreationMode.Finalize;
                oSegment = LMP.Architect.Oxml.Word.XmlSegmentInsertion.Create(xID, null, oForteDocument, oXMLPrefill, xSelectedText, oStructure, xTemplate, iMode, true);
                if (!bDesign)
                {
                    TaskPane.NewXmlSegment = oSegment; //GLOG 15916
                    TaskPane.PendingAction = new PendingAction(MacPac.PendingAction.Types.SetupOxmlSegment, oSegment, oXMLPrefill, xSelectedText, oStructure);
                }
                oNewDoc = LMP.Architect.Oxml.Word.XmlSegmentInsertion.OpenInWord(oSegment);
                
                ForteDocument.IgnoreWordXMLEvents = Architect.Base.ForteDocument.WordXMLEvents.None;

                if (!LMP.MacPac.MacPacImplementation.IsServer)
                {
                    //JTS: ProcessPendingAction will handle setup for Design
                    if (!bDesign)
                    {
                        Application.ActivateRibbonIfAppropriate(LMP.MacPac.Application.MacPacRibbonTabControlId);

                        TaskPane oTP = TaskPanes.Item(oNewDoc);
                        if (oTP != null && oSegment.ProtectedFormPassword != "" && !LMP.MacPac.Application.ShouldShowTaskPaneForProtectedDoc(oTP.ForteDocument))
                        {
                            Segment oAPISegment = oTP.ForteDocument.FindSegment(oSegment.TagID);
                            oTP.Visible = false;
                            oTP = null;
                            //TaskPanes.Item(oNewDoc).Visible = false;
                            ProtectIfSpecified(oAPISegment, true);
                        }
                        if (!TaskPane.SkipScreenUpdates)
                        {

                            if (oSegment == null)
                            {
                                oNewDoc.Application.ScreenUpdating = true;
                                return null;
                            }
                            else if (oSegment.Authors.Count == 0 && oSegment.DefinitionRequiresAuthor) //GLOG 8820
                            {
                                //GLOG 8820: If content requires Attorney Authors but there are
                                // none in list, don't display doc editor - just show message.
                                TaskPane.PendingAction = null;
                                if (oTP != null)
                                {
                                    oTP.SelectTab(Tabs.Content);
                                }
                                oNewDoc.Application.ScreenUpdating = true;
                                //author is required, but no authors are available - alert
                                MessageBox.Show(LMP.Resources.GetLangString("Msg_AuthorRequiredToInsertSegment"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                                return null;
                            }

                            if (oTP != null && oTP.Visible && oSegment.ProtectedFormPassword != "")
                            {
                                Segment oAPISegment = oTP.ForteDocument.FindSegment(oSegment.TagID);
                                if (m_oTaskPaneTimer != null)
                                    m_oTaskPaneTimer.Stop();
                                ProtectIfSpecified(oAPISegment, false);
                                //GLOG 8824: Select first protected form field
                                foreach (Word.Field oFld in oNewDoc.Content.Fields)
                                {
                                    if ((oFld.Type == Word.WdFieldType.wdFieldFormTextInput ||
                                        oFld.Type == Word.WdFieldType.wdFieldFormCheckBox ||
                                        oFld.Type == Word.WdFieldType.wdFieldFormDropDown) &&
                                        !oFld.Locked)
                                    {
                                        oFld.Result.Select();
                                        break;
                                    }

                                }
                            }
                            else if (oTP != null && oTP.DocEditor != null)
                            {
                                //GLOG - 3338 - CEH
                                if (oSegment.ShowChooser)
                                    oTP.DocEditor.SelectChooserNode();
                                else
                                {
                                    if (oSegment.Variables.Count > 0)
                                        //Set focus to first variable control in tree
                                        oTP.DocEditor.SelectFirstVariableNode();
                                    else
                                    {
                                        //TODO: THIS IS PROBLEMATIC IN REQUIRING AN Api.Segment object
                                        //oTP.DocEditor.SelectFirstNodeForSegment(oSegment);
                                    }
                                }
                            }
                            else if (oTP == null && oNewDoc.Sections[1].ProtectedForForms)
                            {
                                //GLOG 8824: Select first protected form field
                                foreach (Word.Field oFld in oNewDoc.Content.Fields)
                                {
                                    if ((oFld.Type == Word.WdFieldType.wdFieldFormTextInput ||
                                        oFld.Type == Word.WdFieldType.wdFieldFormCheckBox ||
                                        oFld.Type == Word.WdFieldType.wdFieldFormDropDown) &&
                                        !oFld.Locked)
                                    {
                                        oFld.Result.Select();
                                        break;
                                    }

                                }
                            }
                        }
                        else
                        {
                            //Allow TaskPane Setup to complete
                            System.Windows.Forms.Application.DoEvents();
                        }

                        //Save/Profile document if Front-end profiling configured
                        LMP.MacPac.Application.ForceProfileIfNecessary();

                    }
                }
                LMP.Benchmarks.Print(t0);

                return oSegment;
            }
            finally
            {
                oWaitCursor.Close();
            }
        }
#endif

        //GLOG - 3590 - CEH
        public static void ProtectIfSpecified(Segment oSegment, bool bRemoveBoundingObjects)
        {
            //if password is [EMPTY], protect, but without password
            DateTime t0 = DateTime.Now;

            if (oSegment == null)
                return;

            string xPwd = oSegment.ProtectedFormPassword;
            ForteDocument oMPDocument = oSegment.ForteDocument;

            if (xPwd != null)
            {
                ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                object oTrueArg = true;
                object oFalseArg = false;
                //GLOG 4229: Comparison should not be case sensitive
                object oPwd = (xPwd.ToUpper() == "[EMPTY]") ? "" : xPwd;

                //reset breaks prior to protecting if necessary
                LMP.Forte.MSWord.WordDoc.ResetDocumentBreaks();

                if (bRemoveBoundingObjects)
                {
                    //JTS 8/4/11: Delete MacPac BoundingObjects before protecting document
                    LMP.MacPac.Application.RemoveBoundingObjects(oMPDocument.WordDocument,
                        LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, false);
                }

                //apply protection to document
                oMPDocument.WordDocument.Protect(
                    Word.WdProtectionType.wdAllowOnlyFormFields, ref oTrueArg,
                    ref oPwd, ref oFalseArg, ref oFalseArg);

                ForteDocument.IgnoreWordXMLEvents = iEvents;
            }

            LMP.Benchmarks.Print(t0);
        }

        //GLOG : 8023 : ceh
        public static void InsertAnswerFile(string xID, Prefill oPrefill, string xSelectedText)
        {
            InsertAnswerFile(xID, oPrefill, xSelectedText, null);
        }

        //GLOG 4695: Handle Saved Data from Data Interview as Favorite
        //GLOG : 8023 : ceh - handle cases where Taskpane might not exist for active document
        public static void InsertAnswerFile(string xID, Prefill oPrefill, string xSelectedText, ShowTaskPaneDelegate oMethod)
        {
            TaskPane oTP = null;
            Word.Document oDoc = null;

            if (LMP.MacPac.Session.CurrentWordApp.Documents.Count == 0)
            {
                //create a new document to access ContentManager function
                oDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                    LMP.Data.Application.BaseTemplate, true, true);
                oDoc.Activate();
                //Allow TaskPane Setup to complete
                System.Windows.Forms.Application.DoEvents();
                oTP = TaskPanes.Item(oDoc);
            }
            else
            {
                oDoc = LMP.MacPac.Session.CurrentWordApp.ActiveDocument;

                if (LMP.MacPac.Application.TaskPaneExists(oDoc))
                    oTP = TaskPanes.Item(oDoc);
                else if (oMethod != null)
                    oTP = oMethod(oDoc);
                else
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_ShowTaskPane"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);

                    return;
                }
            }
            oTP.ucContentManager.InsertAnswerFile(xID, oPrefill, xSelectedText, false);
            oTP = TaskPanes.Item(LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
            System.Windows.Forms.Application.DoEvents();
            //Make sure TaskPane is displayed in final document
            oTP.ScreenUpdating = false;
            oTP.ScreenUpdating = true;
            oTP.ScreenRefresh();
        }
        internal void SubscribeToXMLAfterInsertEvent(bool bSubscribe)
        {
            if (bSubscribe)
            {
                m_oForteDocument.WordDocument.XMLAfterInsert +=
                    new Word.DocumentEvents2_XMLAfterInsertEventHandler(
                    oDoc_XMLAfterInsert);
            }
            else
                m_oForteDocument.WordDocument.XMLAfterInsert -= oDoc_XMLAfterInsert;
        }
        /// <summary>
        /// refreshes the tree node associated 
        /// with the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void RefreshSegmentNode(Segment oSegment)
        {
            this.DocEditor.RefreshSegmentNode(oSegment.FullTagID);
        }
        /// <summary>
        /// adds a letter signature to the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void AddLetterSignature(Segment oSegment)
        {
            this.DocEditor.AddCollectionTableItem(oSegment, mpObjectTypes.LetterSignature,
                oSegment.Contains(mpObjectTypes.LetterSignatures));
        }
        /// <summary>
        /// changes the letter signature of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void ChangeLetterSignatureType(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableItem(oSegment, mpObjectTypes.LetterSignature);
        }
        /// <summary>
        /// edits the letterhead of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void ChangeLetterheadType(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableItem(oSegment, mpObjectTypes.Letterhead);
        }
        public void AddAgreementSignature(Segment oSegment)
        {
            this.DocEditor.AddCollectionTableItem(oSegment, mpObjectTypes.AgreementSignature,
                oSegment.Contains(mpObjectTypes.AgreementSignatures));
        }

        public void LayoutAgreementSignatures(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableLayout(oSegment, mpObjectTypes.AgreementSignature);
        }
        public void LayoutLetterSignatures(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableLayout(oSegment, mpObjectTypes.LetterSignature);
        }
        public void LayoutPleadingSignatures(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableLayout(oSegment, mpObjectTypes.PleadingSignature);
        }
        public void LayoutPleadingCaptions(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableLayout(oSegment, mpObjectTypes.PleadingCaption);
        }
        public void LayoutPleadingCounsels(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableLayout(oSegment, mpObjectTypes.PleadingCounsel);
        }

        //selects the selected or first child segment of the specified type
        public void SelectChild(mpObjectTypes iType, Segment oParentSegment)
        {
            //check if segment of specified type is selected
            Segment oItem = oParentSegment.ForteDocument.GetSegmentFromSelection(iType);

            Segment[] aSegments = null;

            if (oItem == null)
            {
                //selection is not of specified type -
                //get first instance of type
                aSegments = oParentSegment.FindChildren(iType);

                if (aSegments.Length > 0)
                {
                    oItem = aSegments[0];
                }
            }

            if (oItem != null)
            {
                //GLOG 6202 (dm) - removed special handling for pleading counsel
                //if (iType == mpObjectTypes.PleadingCounsel)
                //{
                //    aSegments = oParentSegment.FindChildren(iType);
                //    if (aSegments.Length == 1)
                //    {
                //        Segment oPleading = null;

                //        try
                //        {
                //            oPleading = oItem.Parent.Parent;
                //        }
                //        catch { }

                //        if (oPleading != null)
                //        {
                //            //the counsel is the only counsel for this pleading -
                //            //select the pleading attorneys node
                            
                //        }
                //        else
                //        {
                //            this.SelectEditorFirstNodeOfSegment(oItem);
                //        }
                //    }
                //    else
                //    {
                //        this.SelectEditorFirstNodeOfSegment(oItem);
                //    }
                //}
                //else
                //{
                    this.SelectEditorFirstNodeOfSegment(oItem);
                //}
            }             
        }
        /// <summary>
        /// edits the Agreement signature of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void ChangeAgreementSignatureType(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableItem(oSegment, mpObjectTypes.AgreementSignature);
        }
        /// <summary>
        /// adds a counsel block to the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void AddCounsel(Segment oSegment)
        {
            this.DocEditor.AddCollectionTableItem(oSegment, mpObjectTypes.PleadingCounsel,
                oSegment.Contains(mpObjectTypes.PleadingCounsels));
        }
        /// <summary>
        /// adds a caption block to the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void AddCaption(Segment oSegment)
        {
            this.DocEditor.AddCollectionTableItem(oSegment, mpObjectTypes.PleadingCaption,
                oSegment.Contains(mpObjectTypes.PleadingCaptions));
        }
        /// <summary>
        /// adds a signature block to the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void AddPleadingSignature(Segment oSegment)
        {
            if (oSegment.Contains(mpObjectTypes.PleadingSignatures))
                this.DocEditor.AddCollectionTableItem(oSegment, mpObjectTypes.PleadingSignature, true);
            else if (oSegment.Contains(mpObjectTypes.PleadingSignatureNonTable))
                this.DocEditor.AddNonCollectionTableSignature(oSegment);
            else
                //temporary only - this assumes that if there are no non-table
                //signatures, we should insert a table-based signature
                this.DocEditor.AddCollectionTableItem(oSegment, mpObjectTypes.PleadingSignature, false);
        }
        /// <summary>
        /// Edits a counsel block of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void ChangePleadingCounselType(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableItem(oSegment, mpObjectTypes.PleadingCounsel);
        }
        /// <summary>
        /// Edits a Caption block of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void ChangePleadingCaptionType(Segment oSegment)
        {
            this.DocEditor.EditCollectionTableItem(oSegment, mpObjectTypes.PleadingCaption);
        }
        /// <summary>
        /// Edits a signature block of the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void ChangePleadingSignatureType(Segment oSegment)
        {
            if (oSegment.Contains(mpObjectTypes.PleadingSignature) || oSegment.Contains(mpObjectTypes.PleadingSignatures))
            {
                this.DocEditor.EditCollectionTableItem(oSegment, mpObjectTypes.PleadingSignature);
            }
            else
            {
                this.DocEditor.ChangeNonTableSignatureType(oSegment);
            }
        }
        #endregion
		#region *********************static members*********************
        /// <summary>
        /// Sends key combination to select the TaskPane
        /// </summary>
        internal static void SelectTaskPane()
        {
            Word.Application oWord = Session.CurrentWordApp;

            TaskPane oTP = null;
            try
            {
                oTP = TaskPanes.Item(oWord.ActiveDocument);
            }
            catch { }

            if (oTP != null)
            {
                //GLOG 15823 (dm) - sendkeys was sometimes adding a tab to the document -
                //it also wasn't working in 2016
                oTP.Focus();

                ////Make sure Ctrl in Sendkeys doesn't toggle hotkey display
                //if (oTP.Mode == ForteDocument.Modes.Edit)
                //{
                //    oTP.DocEditor.DisableHotKeyMode = true;
                //    if (oTP.DataReviewer != null)
                //        oTP.DataReviewer.DisableHotKeyMode = true; //GLOG 5782 (dm)
                //}
                //SendKeys.SendWait("{F10}^{TAB}");
                //if (oTP.Mode == ForteDocument.Modes.Edit)
                //{
                //    oTP.DocEditor.DisableHotKeyMode = false;
                //    if (oTP.DataReviewer != null)
                //        oTP.DataReviewer.DisableHotKeyMode = false; //GLOG 5782 (dm)
                //}
            }
        }
        public static void SetHelpText(WebBrowser oBrowser, string xText)
        {
            //GLOG 7214: Handle errors in this function
            try
            {
                if (oBrowser == null)
                    return;

                Trace.WriteNameValuePairs("xText", xText);

                //set help text or navigate to specified url - 
                //web browser control must
                //be set so that AllowNavigation = true -
                //otherwise, repeated DocumentText assignments fail
                if (string.IsNullOrEmpty(xText))
                {
                    oBrowser.DocumentText = "";
                }
                else
                {
                    string xDesc = xText.Replace((char)172, '<');
                    xDesc = String.RestoreMPReservedChars(xDesc);
                    Trace.WriteNameValuePairs("xDesc", xDesc);

                    if (xDesc.ToLower().StartsWith(@"file:\\\") ||
                        xDesc.ToLower().StartsWith(@"file:///"))
                    {
                        //remove protocol prefix
                        string xFullName = xDesc.Substring(8);

                        //replace %20 with space - this occurs only on some machines
                        xFullName = xFullName.Replace("%20", " ");

                        //test for existence of file
                        bool bFileExists = false;
                        try
                        {
                            FileInfo oFileInfo = new FileInfo(xFullName);
                            bFileExists = oFileInfo.Exists;
                        }
                        catch { }

                        //assume that the help file is in the help directory
                        //if no directory has been specified
                        if (!bFileExists)
                        {
                            xFullName = LMP.Data.Application.GetDirectory(mpDirectories.Help) + @"\" + xFullName;
                        }

                        if (File.Exists(xFullName))
                            xDesc = "file:///" + xFullName;
                        else
                        {
                            xDesc = "<span style='font-size: 8pt; font-family: MS Sans Serif'>" +
                                LMP.Resources.GetLangString("Msg_HelpUnavailable") + "</span>";
                            oBrowser.DocumentText = xDesc;
                            return;
                        }

                        try
                        {
                            //help text references a url - navigate -
                            //ignore bad urls to prevent interruption
                            //of user experience
                            oBrowser.Url = new Uri(xDesc);
                        }
                        catch { }
                    }
                    else if (xDesc.ToLower().StartsWith(@"http:\\") ||
                        xDesc.ToLower().StartsWith(@"http://"))
                    {
                        try
                        {
                            //help text references a url - navigate -
                            //ignore bad urls to prevent interruption
                            //of user experience
                            oBrowser.Url = new Uri(xDesc);
                        }
                        catch { }
                    }
                    else
                    {
                        // GLOG : 3315 : JAB
                        // Set the appropriate help font name and size.
                        if (m_oFirmAppSettings == null)
                        {
                            m_oFirmAppSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                        }

                        xDesc = string.Format("<span style='font-size: {0}pt; font-family: {1}'>",
                                                m_oFirmAppSettings.HelpFontSize,
                                                m_oFirmAppSettings.HelpFontName) +
                                                xDesc + "</span>";

                        xDesc = xDesc.Replace("\n", "<br>");
                        oBrowser.DocumentText = xDesc;
                    }
                }
            }
            catch (ObjectDisposedException)
            {
                //Ignore this error - can be generated first time a new WebBrowser object is accessed after a previous instance is destroyed
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        /// <summary>
        /// Check if threshold of time has passed since last DocumentBeforeSave event
        /// </summary>
        /// <returns></returns>
        private static bool LastSaveIsWithinThreshold() //GLOG 8272
        {
            if (m_dtLastBeforeSaveEvent == DateTime.MinValue)
                return false;

            long lThreshold = 0;
            string xVal = Registry.GetMacPac10Value("DisableEventsOnSaveThreshold");
            if (!string.IsNullOrEmpty(xVal) && long.TryParse(xVal, out lThreshold))
            {
                if (lThreshold < 1)
                    return false;

                //Convert from milliseconds to ticks
                lThreshold = lThreshold * 10000;
                TimeSpan oSpan = new TimeSpan(lThreshold);
                if (DateTime.Now <= m_dtLastBeforeSaveEvent.Add(oSpan))
                {
                    return true;
                }
                else
                {
                    m_dtLastBeforeSaveEvent = DateTime.MinValue;
                    return false;
                }
            }
            return false;
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// handles this class's load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void TaskPane_Load(object sender, System.EventArgs e)
		{
			try
			{
                Setup();
            }
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}

        /// <summary>
        /// handles Word's XMLSelectionChange event
        /// </summary>
        /// <param name="Sel"></param>
        /// <param name="OldXMLNode"></param>
        /// <param name="NewXMLNode"></param>
        /// <param name="Reason"></param>
        static void CurrentWordApp_XMLSelectionChange(Word.Selection Sel,
            Word.XMLNode OldXMLNode, Word.XMLNode NewXMLNode, ref int Reason)
        {
            if (MacPac.Application.DisableTaskpaneEvents || LMP.MacPac.Application.Paused)
                return;

            //GLOG 8272
            if (LastSaveIsWithinThreshold())
            {
                return;
            }
            DateTime t0 = DateTime.Now;
            try
            {
                Word.Document oDoc = null;
                try
                {
                    //GLOG 920 - the document isn't always available here, e.g.
                    //when editing a numbering scheme
                    oDoc = Sel.Document;
                }
                catch { }

                if (oDoc == null)
                    return;

                TaskPane oTP = null;
                try
                {
                    oTP = TaskPanes.Item(oDoc);
                }
                catch { }

                if (oTP == null)
                    return;
                else if (((ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLSelectionChange) > 0) ||
                    oTP.m_bInsertionIsInvalid || LMP.Forte.MSWord.WordDoc.IsTempDoc(oDoc))
                    return;

                //GLOG 4241 (dm) - store screen updating status
                bool bScreenUpdating = oTP.ScreenUpdating;

                //Don't run this handler in protected document -
                //GLOG 5443 (dm) - replaced revisions check with compatibility mode check
                if (LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false)
                    || LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(oDoc, false))
                    ////JTS 1/19/11: Also disable for Documents with Revision marks
                    //|| LMP.Architect.Api.Environment.DocumentHasRevisions(oDoc, false))
                    return;

                //mark that a tag selection is occuring
                oTP.WordTagChangeOccurring = true;

                if (oTP.Mode == ForteDocument.Modes.Edit || oTP.Mode == ForteDocument.Modes.Preview)
                    oTP.DocEditor.OnXMLSelectionChange(OldXMLNode, NewXMLNode);
                else if (oTP.Mode == ForteDocument.Modes.Design)
                    oTP.DocDesigner.OnXMLSelectionChange(OldXMLNode, NewXMLNode);

                oTP.WordTagChangeOccurring = false;

                if (!oTP.ScreenUpdating)
                    oTP.ScreenUpdating = bScreenUpdating;
                
                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handles Word's BeforeSave event
        /// </summary>
        /// <param name="Doc"></param>
        /// <param name="SaveAsUI"></param>
        /// <param name="Cancel"></param>
        static void CurrentWordApp_DocumentBeforeSave(Word.Document Doc, ref bool SaveAsUI, ref bool Cancel)
        {
            m_dtLastBeforeSaveEvent = DateTime.Now;
            try
            {
                //8-22-11 (dm) - exit if this is an autosave
                if (LMP.Forte.MSWord.WordApp.IsAutoSaveEvent())
                    return;

                //exit if a temp doc is being saved
                if (LMP.Forte.MSWord.WordDoc.IsTempDoc(Doc))
                    return;

                //exit if doc contains the var below -
                //server-created docs don't need to execute
                //this event handler, and doing so results in errors
                object oVarName = "mpBypassSaveEventHandler";
                Word.Variable oDocVar = null;

                string xDocVarVal = null;
                try
                {
                    oDocVar = Doc.Variables.get_Item(ref oVarName);
                    xDocVarVal = oDocVar.Value;

                    if (xDocVarVal == "1")
                    {
                        oDocVar.Delete();
                        return;
                    }
                }
                catch { }

                TaskPane oTP = null;
                try
                {
                    oTP = TaskPanes.Item(Doc);
                }
                catch { }

                if (oTP == null)
                    return;

                //GLOG 7621 - Save current control value
                if (oTP.Mode == ForteDocument.Modes.Edit)
                {
                    oTP.DocEditor.ExitEditMode();
                }

                if (oTP.Mode == ForteDocument.Modes.Design)
                {
                    //alert and return if design doc contains more than one top level segment
                    if (oTP.ForteDocument.Segments.Count > 1)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString(
                            "Msg_CantSaveMultSegmentDesignDoc"),
                            LMP.ComponentProperties.ProductName, 
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if(SaveAsUI)
                    {
                        //save was initiated by the user - save design
                        oTP.DocDesigner.SaveSegmentDesign();
                        Doc.Saved = true;
                    }

                    //Don't cancel AutoRecovery Save
                    Cancel = SaveAsUI;
                }
                else if (oTP.Mode == ForteDocument.Modes.Preview)
                {
                    //prevent save of a preview document
                    Doc.Saved = true;
                    Cancel = true;
                }
                else if (Doc.SaveFormat == 0 || Doc.SaveFormat == 12 || Doc.SaveFormat == 16) //GLOG 5760: Don't run for Template format
                {
                    //dm 8-23-11 - added conditions
                    //dm 9-27-11 - removed condition that Doc.Saved=False
                    //because it's never the case when saving into a different
                    //format with Worksite and no MacPac trailer integration
                    //if (!LMP.MacPac.Session.DisplayFinishButton)
                    //{
                        //add bookmarks (dm 8/23/10)
                        if (oTP.ForteDocument.FileFormat == mpFileFormats.OpenXML)
                            oTP.ForteDocument.Tags.AddBookmarks();
                        else
                        {
                            //stopped accessing xml tags via the tags collection here -
                            //saves occur at unpredictable times and we were seeing frequent
                            //"object has been deleted" errors due to Tag.WordXMLNode objects
                            //that were no longer valid - accessing via the document is slower
                            //but less prone to errors (dm 6/1/11)
                            LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                            oConvert.AddBookmarks(Doc, false);
                        }
                    //}

                    //start timer (dm 2/15/11)
                    oTP.m_iSaveCounter = 0;
                    oTP.m_bSaveInProgress = true;
                    oTP.m_oSaveTimer.Start();
                }

                LMP.Forte.MSWord.WordApp.SuppressAttachedTemplateSavePrompt(Doc);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles Word's document close event
        /// </summary>
		private void oDoc_DocumentEvents2_Event_Close()
		{
            try
            {
                //unsubscribe the handler for the document that is
                //about to be closed - otherwise the task pane associated
                //with the closing doc won't get garbage collected
                //Session.CurrentWordApp.XMLSelectionChange -= m_oXmlSelChangeHandler;
                //Session.CurrentWordApp.DocumentBeforeSave -= m_oDocumentBeforeSaveHandler;

                Word.Document oDoc = this.ForteDocument.WordDocument;

                //exit if a temp doc is being saved
                if (LMP.Forte.MSWord.WordDoc.IsTempDoc(oDoc))
                    return;

                if (this.Mode == ForteDocument.Modes.Design)
                {
                    //Prompt to save will have occurred in DocumentBeforeClose in Designer
                    oDoc.Saved = true;
                }
                else if (this.Mode == ForteDocument.Modes.Preview)
                    oDoc.Saved = true;
                else
                {
                    //GLOG 3193: This code now called from Document_BeforeClose in Forte.dot
                    //LMP.MacPac.Application.RemoveIBFReference(oDoc);
                }
                //JTS 7/26/10:  In some cases, when closing documents without saving
                //in Worksite, XMLAfterInsert will be raised before document closes,
                //causing errors if m_oRefreshTimer_Elapsed fires after document has
                //already closed.  Stop timer here to prevent possible errors.
                m_oRefreshTimer.Stop();
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                //ensure that DocXMl template doesn't prompt for save
                LMP.Forte.MSWord.WordApp.SuppressDocXMLTemplateSavePrompt();
                LMP.Forte.MSWord.WordApp.SuppressAttachedTemplateSavePrompt(oDoc);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
		}

        /// <summary>
        /// called when Word application quits
        /// </summary>
        static void OnAppQuitHandler()
        {
            try
            {
                //JTS 3/21/10: Cleanup not specifically related to TaskPane moved to MacPac102007
                //Clean up segment copy buffer file
                if (System.IO.File.Exists(SegmentPasteFile))
                {
                    System.IO.File.Delete(SegmentPasteFile);
                }

                // Release Mouse hook
                if (iMouseHook != 0)
                {
                    bool bRet = UnhookWindowsHookEx(iMouseHook);
                    iMouseHook = 0;
                }

                // Release keyboard hook
                if (iKeyboardHook != 0)
                {
                    bool bRet = UnhookWindowsHookEx(iKeyboardHook);
                    iKeyboardHook = 0;
                }

                //force garbage collection -
                //we do this twice because the first time,
                //those collectable objects that have finalizers
                //won't be collected - their finalizers are executed and
                //then moved to the finalization queue
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handles Word's XMLBeforeDelete event
        /// </summary>
        /// <param name="DeletedRange"></param>
        /// <param name="OldXMLNode"></param>
        /// <param name="InUndoRedo"></param>
        private void oDoc_XMLBeforeDelete(Word.Range DeletedRange,
            Word.XMLNode OldXMLNode, bool InUndoRedo)
        {
            try
            {
                //exit if a temp doc is being saved
                if (((ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLBeforeDelete) > 0) ||
                    LMP.MacPac.Application.Paused ||LMP.MacPac.Application.DisableTaskpaneEvents ||
                    LMP.Forte.MSWord.WordDoc.IsTempDoc(this.ForteDocument.WordDocument) || m_bSaveInProgress)
                    return;

                //JTS 7/26/10: Don't continue if Word Document has already been closed
                try
                {
                    string xTestPath = this.ForteDocument.WordDocument.Path;
                }
                catch
                {
                    return;
                }
                //we're using the refresh timer on Undo/Redo due to a quirk in Word whereby
                //undoing a tag deletion causes the other tags in the currently selected paragraph
                //to be deleted and reinserted, creating problems for our targeted refreshing
                if (InUndoRedo)
                {
                    //disable event handling
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //start timer
                    m_oRefreshTimer.Start();

                    //exit
                    return;
                }

                if (this.Mode == ForteDocument.Modes.Design)
                    //design mode
                    OnXMLBeforeDelete_Design(DeletedRange, OldXMLNode, InUndoRedo);
                else
                    //edit mode
                    OnXMLBeforeDelete_Edit(DeletedRange, OldXMLNode, InUndoRedo);
            }
            catch (System.Exception oE)
            {
                //5-6-11 (dm) - don't show errors in edit mode - refresh instead
                if (this.Mode == ForteDocument.Modes.Edit)
                {
                    //disable further event handling
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //start timer
                    m_oRefreshTimer.Start();
                }
                else
                    LMP.Error.Show(oE);
            }
            //catch (System.Exception oE)
            //{
                //LMP.Error.Show(oE);
            //}
        }

        /// <summary>
        /// handles Word's XMLAfterInsert event
        /// </summary>
        /// <param name="NewXMLNode"></param>
        /// <param name="InUndoRedo"></param>
        private void oDoc_XMLAfterInsert(Word.XMLNode NewXMLNode, bool InUndoRedo)
        {
            // If Mouse drag is occurring, don't trigger code
            // until drag has completed and mouse button has been released - 
            // also, ignore in temp docs, or when specified
            if (((ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLAfterInsert) > 0) ||
                m_bMouseIsMoving || LMP.Forte.MSWord.WordDoc.IsTempDoc(this.ForteDocument.WordDocument) ||
                LMP.MacPac.Application.DisableTaskpaneEvents ||LMP.MacPac.Application.Paused)
                return;

            //JTS 7/26/10: Don't continue if Word Document has already been closed
            try
            {
                string xTestPath = this.ForteDocument.WordDocument.Path;
            }
            catch
            {
                return;
            }
            LMP.Trace.WriteNameValuePairs("NewXMLNode", 
                NewXMLNode.BaseName, "InUndoRedo", InUndoRedo);

            try
            {
                //in Word 2007, an access violation exception if you attempt to access a
                //node inside a textbox on XMLAfterInsert - instead, we'll use a timer to do
                //a complete refresh in lieu of multiple targeted refreshes -
                //we're also using the timer on Undo/Redo due to a quirk in Word whereby
                //undoing a tag deletion causes the other tags in the currently selected paragraph
                //to be deleted and reinserted, creating problems for our targeted refreshing
                if (InUndoRedo || ((NewXMLNode.Range.StoryType == Word.WdStoryType.wdTextFrameStory)))
                {
                    //disable event handling
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //start timer
                    m_oRefreshTimer.Start();

                    //exit
                    return;
                }

                if (this.Mode == ForteDocument.Modes.Design)
                    //design mode
                    OnXMLAfterInsert_Design(NewXMLNode, InUndoRedo);
                else
                    //edit mode
                    OnXMLAfterInsert_Edit(NewXMLNode, InUndoRedo);

                if (m_bXMLAfterInsertHandledForMouseDrag)
                {
                    // Terminate mouse drag handling if necessary
                    // so no additional XMLAfterInsert events will occur
                    // Event doesn't fire until after MouseUp, so we can't 
                    // use the MouseHookProc for this
                    HandleMouseDrag(false);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handles Word's ContentControlBeforeDelete event
        /// </summary>
        /// <param name="OldContentControl"></param>
        /// <param name="InUndoRedo"></param>
        private void oDoc_ContentControlBeforeDelete(Word.ContentControl OldContentControl, bool InUndoRedo)
        {
            try
            {
                //GLOG : 7487 : CEH - prevent event to run when just resizing tables in Word 2013
                //GLOG 7812 (dm) - modified conditional to restore handler as appropriate in tables
                Word.Range oRng = Session.CurrentWordApp.Selection.Range;
                if ((oRng.Tables.Count > 0) && (oRng.ContentControls.Count == 0) && (LMP.Forte.MSWord.WordApp.Version >= 15))
                    return;

                //exit if a temp doc is being saved -
                //this event also fires when xml tag is deleted, so also make sure
                //that tag is non-null
                if (((ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLBeforeDelete) > 0) ||
                    LMP.MacPac.Application.Paused || LMP.MacPac.Application.DisableTaskpaneEvents ||
                    (OldContentControl.Tag == null) || m_bSaveInProgress ||
                    LMP.Forte.MSWord.WordDoc.IsTempDoc(this.ForteDocument.WordDocument))
                    return;

                //GLOG 5666: Exit handler if Paste Options button is visible
                if (InUndoRedo && PasteOptionsVisible(OldContentControl.Application.ActiveDocument))
                    return;

                //we're using the refresh timer on Undo/Redo due to a quirk in Word whereby
                //undoing a tag deletion causes the other tags in the currently selected paragraph
                //to be deleted and reinserted, creating problems for our targeted refreshing
                if (InUndoRedo)
                {
                    //disable event handling
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //start timer
                    m_oRefreshTimer.Start();

                    //exit
                    return;
                }

                if (this.Mode == ForteDocument.Modes.Design)
                    //design mode
                    OnContentControlBeforeDelete_Design(OldContentControl, InUndoRedo);
                else
                    //edit mode
                    OnContentControlBeforeDelete_Edit(OldContentControl, InUndoRedo);
            }
            catch (System.Exception oE)
            {
                //5-6-11 (dm) - don't show errors in edit mode - refresh instead
                if (this.Mode == ForteDocument.Modes.Edit)
                {
                    //disable further event handling
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //start timer
                    m_oRefreshTimer.Start();
                }
                else
                    LMP.Error.Show(oE);
            }
            //catch (System.Exception oE)
            //{
            //LMP.Error.Show(oE);
            //}
        }

        /// <summary>
        /// handles Word's ContentControlAfterAdd event
        /// </summary>
        /// <param name="NewContentControl"></param>
        /// <param name="InUndoRedo"></param>
        private void oDoc_ContentControlAfterAdd(Word.ContentControl NewContentControl, bool InUndoRedo)
        {
            // If Mouse drag is occurring, don't trigger code
            // until drag has completed and mouse button has been released - 
            // also, ignore in temp docs, or when specified
            if (((ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLAfterInsert) > 0) || LMP.MacPac.Application.Paused ||
                m_bMouseIsMoving || LMP.Forte.MSWord.WordDoc.IsTempDoc(this.ForteDocument.WordDocument) ||
                LMP.MacPac.Application.DisableTaskpaneEvents)
                return;

            //GLOG 5666: Exit handler if Paste Options button is visible
            if (InUndoRedo && PasteOptionsVisible(NewContentControl.Application.ActiveDocument))
                return;

            LMP.Trace.WriteNameValuePairs("NewContentControl",
                NewContentControl.Tag, "InUndoRedo", InUndoRedo);

            try
            {
                //in Word 2007, an access violation exception if you attempt to access a
                //node inside a textbox on XMLAfterInsert - instead, we'll use a timer to do
                //a complete refresh in lieu of multiple targeted refreshes -
                //we're also using the timer on Undo/Redo due to a quirk in Word whereby
                //undoing a tag deletion causes the other tags in the currently selected paragraph
                //to be deleted and reinserted, creating problems for our targeted refreshing
                if (InUndoRedo || (NewContentControl.Range.StoryType == Word.WdStoryType.wdTextFrameStory))
                {
                    //disable event handling
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //start timer
                    m_oRefreshTimer.Start();

                    //exit
                    return;
                }

                this.OnContentControlAfterAdd(NewContentControl, InUndoRedo);

                if (m_bXMLAfterInsertHandledForMouseDrag)
                {
                    // Terminate mouse drag handling if necessary
                    // so no additional XMLAfterInsert events will occur
                    // Event doesn't fire until after MouseUp, so we can't 
                    // use the MouseHookProc for this
                    HandleMouseDrag(false);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// override that processes messages to this window
        /// </summary>
        /// <param name="m"></param>
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void WndProc(ref Message m)
        {
            try
            {
                Application.mpPseudoWordEvents oMsg = (Application.mpPseudoWordEvents)m.Msg;
                switch (oMsg)
                {
                    //Return 1 for each handled event to SendMessageToTaskPane will know message was processed
                    case Application.mpPseudoWordEvents.BeforeNextCell:
                        //Bypass event handler in case Tags need to be
                        //deleted from a new table row
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.MouseDragStart:
                        //subscribe to XMLAfterInsert event
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.BeforeMenuPaste:
                    case Application.mpPseudoWordEvents.BeforeKeyboardPaste:
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.BeforeUndo:
                    case Application.mpPseudoWordEvents.BeforeRedo:
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.AfterNextCell:
                        //unsubscribe from XMLAfterInsert event
                        //Re-enable event handler
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.MouseDragComplete:
                        //unsubscribe from XMLAfterInsert event
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.AfterMenuPaste:
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.AfterUndo:
                    case Application.mpPseudoWordEvents.AfterRedo:
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.FloatingFormRequested:
                        this.DocEditor.ShowFloatingControl();
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.ContentChanged:
                        this.Refresh(false, true, false);
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.AuthorListUpdated:
                        LMP.MacPac.Application.RefreshCurrentAuthorControls();
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.SuppressXMLEvents:
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.ResumeXMLEvents:
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.SuppressXMLSelectionChangeEvent:
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents |
                            ForteDocument.WordXMLEvents.XMLSelectionChange;
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.QueryExists:
                        int iResult = 0;
                        try
                        {
                            //Test that TaskPane is still in the Hash Table
                            TaskPane oTest = TaskPanes.Item(m_oForteDocument.WordDocument);
                            if (oTest != null)
                            {
                                //JTS 5/27/09: In Word 2003, Hash Table entry may point to previously closed document
                                //Test Name property to verify object is still valid
                                string xName = "";
                                try
                                {
                                    xName = m_oForteDocument.WordDocument.Name;
                                }
                                catch { }
                                if (xName != "")
                                    iResult = 1;
                            }
                        }
                        catch { }
                        m.Result = (IntPtr)iResult;
                        break;
                    case Application.mpPseudoWordEvents.PleadingPaperRequested:
                        //GLOG : 7187 : CEH
                        int iSection = Session.CurrentWordApp.Selection.Sections[1].Index;

                        //insert pleading paper
                        Segment oPaper = PleadingPaper.InsertExistingInCurrentSection(m_oForteDocument, null);

                        //GLOG : 7187 : CEH
                        //if TOC has inserted a new section at the start of the document,
                        //or at insertion point, oMPDocument.Refresh in 
                        //PleadingPaper.InsertExistingInCurrentSection will show invalid segments in Editor
                        if ((this.DocEditor != null) && (iSection < m_oForteDocument.WordDocument.Sections.Count))
                        {
                            this.DocEditor.UpdateSegmentImages();
                        }

                        //GLOG item #6921 - dcf
                        if (this.DocEditor != null && oPaper != null)
                        {
                            this.DocEditor.UpdateSegmentImage(oPaper, true, true); //GLOG 6945
                        }

                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.OnCopy:
                        //copy
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        LMP.Forte.MSWord.Tags oTags = m_oForteDocument.Tags;
                        bool bRefreshRequired = false;
                        LMP.Forte.MSWord.WordApp.EditCopy(ref oTags, ref bRefreshRequired);
                        if (bRefreshRequired)
                        {
                            //GLOG 3727 (dm) - tags collection has been modified
                            m_oForteDocument.Refresh(ForteDocument.RefreshOptions.None,
                                false, false);
                        }
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.OnCut:
                        //cut - we split this up so that we can turn off XML event handling
                        //while adjusting the content to be cut and back on to monitor
                        //the cut itself
                        LMP.Forte.MSWord.Tags oTagsForCut = m_oForteDocument.Tags;
                        string xmDels = "";
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        Word.Range oAdjustedRange = LMP.Forte.MSWord.WordApp.GetCopyRange(
                            ref oTagsForCut, ref xmDels);
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                        //do the cut - error trapped for Word 2003 as macro gets called
                        //regardless of whether the command is available (GLOG 4205, dm)
                        try
                        {
                            oAdjustedRange.Cut();
                        }
                        catch { }

                        //restore mDels excluded from cut content
                        if (xmDels != "")
                        {
                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                            LMP.Forte.MSWord.WordDoc.RestoremDels(oAdjustedRange, xmDels,
                                ref oTagsForCut);
                            //GLOG 3727 (dm) - tags collection has been modified
                            m_oForteDocument.Refresh(ForteDocument.RefreshOptions.None,
                                false, false);
                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                        }
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.UpdateVariableValueFromSelection:
                        //update variable value from selection
                        if (m_oForteDocument.Mode == ForteDocument.Modes.Edit)
                            this.DocEditor.UpdateVariableValueFromSelection();
                        break;
                    case Application.mpPseudoWordEvents.QueryInputFocus:
                        int iHasFocus = 0;
                        if ((LMP.WinAPI.GetFocus() == m_oForteDocument.WindowHandle) &&
                            (m_oForteDocument.Mode != ForteDocument.Modes.Design))
                        {
                            //runtime document has input focus - make sure
                            //there's a selection
                            Word.Selection oWordSelection = null;
                            Word.Application oWordApp = Session.CurrentWordApp;
                            if (oWordApp == null)
                                oWordApp = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
                            try
                            {
                                oWordSelection = oWordApp.Selection;
                            }
                            catch { }
                            if (oWordSelection != null)
                                iHasFocus = 1;
                        }

                        m.Result = (IntPtr)iHasFocus;
                        break;
                    case Application.mpPseudoWordEvents.DeleteKeyPressed:
                    case Application.mpPseudoWordEvents.BackspaceKeyPressed:
                        //display warning if the user is deleting the paragraph
                        //mark between block tags
                        this.HandleDeleteKeyPressed(oMsg);
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.RefreshRequested:
                        //refresh task pane
                        if (m_oForteDocument.Mode == ForteDocument.Modes.Design)
                            this.DocDesigner.RefreshTree();
                        else
                            this.DocEditor.RefreshTree();
                        m.Result = (IntPtr)1;
                        break;
                    case Application.mpPseudoWordEvents.EnvelopesRequested:
                        //create envelopes
                        this.CreateEnvelopes();
                        break;
                    case Application.mpPseudoWordEvents.QueryUnfinishedContent:
                        //returns whether document contains unfished content
                        int iContent = 0;
                        if (this.ForteDocument.ContainsUnfinishedContent())
                            iContent = 1;
                        m.Result = (IntPtr) iContent;
                        break;
                    default:
                        //Don't call this for our Pseudo events, or return value will be lost
                        base.WndProc(ref m);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void CreateEnvelopes()
        {
            SegmentInsertionForm oForm = new SegmentInsertionForm(mpObjectTypes.Envelopes, true);
            if (oForm.ShowDialog() == DialogResult.OK)
            {
                this.InsertSegment(oForm.SegmentDef,
                    oForm.InsertionLocation, oForm.InsertionBehavior, null, false);
            }
        }

        /// <summary>
        /// calling Word.Document.Undo() in an XMLAfterInsert event handler is problematic,
        /// so when the handler fires for the last tag in an illegal insertion, we use a timer
        /// to execute the undo after a short duration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oInsertionUndoTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (m_oInsertionUndoTimer.Enabled)
                {
                    //stop timer
                    m_oInsertionUndoTimer.Stop();

                    //display message
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_InsertionNotAllowedDueToInvalidTags"),
                        LMP.String.MacPacProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    //undo insertion
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    object oMissing = System.Reflection.Missing.Value;
                    m_oForteDocument.WordDocument.Undo(ref oMissing);
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                    //reset flags
                    ResetFlags();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ScreenUpdating = true;
            }
        }

        private void m_oRefreshTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (m_oRefreshTimer.Enabled)
                {
                    //stop timer
                    m_oRefreshTimer.Stop();

                    //refresh
                    if (this.Mode == ForteDocument.Modes.Design)
                        this.DocDesigner.RefreshTree();
                    else
                        this.DocEditor.RefreshTree();

                    //restore event handling
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void m_oPasteTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (m_oPasteTimer.Enabled)
                {
                    //stop timer
                    m_oPasteTimer.Stop();

                    //unsubscribe from XMLAfterInsert event
                    m_oForteDocument.WordDocument.XMLAfterInsert -= oDoc_XMLAfterInsert;

                    //handle tags inside inserted textboxes
                    this.HandleInsertedShapes(false);

                    //restore all xml event handling
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                    //rehide tags
                    if (m_iShowXMLMarkupBeforePaste == 0)
                        m_oForteDocument.WordDocument.ActiveWindow.View.ShowXMLMarkup = 0;

                    //restore screen updating
                    this.ScreenUpdating = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// shows MacPac menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picMenu_Click(object sender, EventArgs e)
        {
            try
            {
                ShowMacPacMenu();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMacPac_ManagePeople_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                PeopleManager oForm = new PeopleManager();
                oForm.ShowDialog();
                //call static routine to refresh all author controls in all segments
                //in all documents
                Application.RefreshCurrentAuthorControls();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMacPac_PasteSegment_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                TaskPane.PasteSegmentXML();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMacPac_AppSettings_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                LMP.MacPac.Application.ManageUserAppSettings();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuMacPac_EditAuthorPrefs_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                AuthorPreferencesManager oForm = new AuthorPreferencesManager(0, true);
                oForm.ShowDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMacPac_PauseResume_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();
                if (LMP.MacPac.Application.Paused)
                {
                    LMP.MacPac.Application.Paused = false;
                    LMP.MacPac.Application.DisableTaskpaneEvents = false;
                }
                else
                {
                    LMP.MacPac.Application.Paused = true;
                    LMP.MacPac.Application.DisableTaskpaneEvents = true;
                    LMP.MacPac.TaskPanes.Remove(LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
                    LMP.MacPac.Session.CurrentWordApp.TaskPanes[
                        Word.WdTaskPanes.wdTaskPaneDocumentActions].Visible = false;
                    LMP.Forte.MSWord.WordApp.DisableForteMenuItems();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuMacPac_RefreshTree_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                //GLOG 2768, 8/1/08
                TaskPanes.RefreshAll();

                switch (this.tabMain.ActiveTab.Index)
                {
                    case ((int)Tabs.Content):
                        //this.ucContentManager.RefreshTree();
                        break;
                    case ((int)Tabs.Edit):
                        if (Mode == ForteDocument.Modes.Edit)
                        {
                            this.DocEditor.RefreshTree();
                            this.DocEditor.Focus();
                        }
                        else
                        {
                            this.DocDesigner.RefreshTree();
                            this.DocDesigner.Focus();
                        }
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void mnuMacPac_ChangeUser_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                LMP.Controls.TimedContextMenuStrip oDropDown = new LMP.Controls.TimedContextMenuStrip();

                this.mnuMacPac_ChangeUser.DropDown = oDropDown; // .DropDownItems.Clear();

                //get proxies
                LMP.Data.LocalPersons oPossibleUsers = LMP.MacPac.Session.LoginUser.GetProxiableUsers();
                int iCount = oPossibleUsers.Count;

                if (iCount > 0)
                {
                    ToolStripMenuItem oItem = new ToolStripMenuItem(LMP.MacPac.Session.LoginUser.PersonObject.DisplayName);
                    oItem.Tag = LMP.MacPac.Session.LoginUser.ID;
                    oItem.Click += new EventHandler(OnProxyButtonClick);

                    if (LMP.MacPac.Session.CurrentUser.ID == LMP.MacPac.Session.LoginUser.ID)
                        oItem.Checked = true;

                    oDropDown.Items.Add(oItem);

                    for (int i = 1; i <= iCount; i++)
                    {
                        LMP.Data.Person oPerson = oPossibleUsers.ItemFromIndex(i);
                        oItem = new ToolStripMenuItem(oPerson.DisplayName);
                        oItem.Tag = oPerson.ID;
                        oItem.Click += new EventHandler(OnProxyButtonClick);

                        if (LMP.MacPac.Session.CurrentUser.PersonObject.ID == oPerson.ID)
                            oItem.Checked = true;
                        oDropDown.Items.Add(oItem);
                    }
                }
                else
                {
                    //alert user
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_NoProxyRights"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void OnProxyButtonClick(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();
                this.Cursor = Cursors.WaitCursor;
                this.ScreenUpdating = false;

                object oID = (((System.Windows.Forms.ToolStripItem)sender).Tag);
                int iID = (int)(float.Parse(oID.ToString()));

                LMP.MacPac.Session.SetProxy(iID);
                LMP.MacPac.TaskPanes.RefreshAll();
                LMP.MacPac.TaskPanes.RefreshAppearance();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                this.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// MacPac menu is opening
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuMacPac_Opening(object sender, CancelEventArgs e)
        {
            //Set visibility for Paste Segment item
            mnuMacPac_PasteSegment.Visible = System.IO.File.Exists(SegmentPasteFile);

            this.mnuMacPac_PauseResume.Visible = LMP.MacPac.Session.CurrentUser.FirmSettings.AllowPauseResume;

            if (LMP.MacPac.Application.Paused)
                this.mnuMacPac_PauseResume.Text = LMP.Resources.GetLangString("Menu_MacPac_Resume");
            else
                this.mnuMacPac_PauseResume.Text = LMP.Resources.GetLangString("Menu_MacPac_Pause");

            //Set visibility for Document/Segment trailer options
            if (this.Mode == ForteDocument.Modes.Edit || this.Mode == ForteDocument.Modes.Design)
            {
                mnuMacPac_Sep2.Visible = true;
				//GLOG : 7998 : ceh
                this.mnuMacPac_ToggleDocTrailer.Visible = LMP.MacPac.Session.AdminMode && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
                this.mnuMacPac_ToggleSecTrailer.Visible = LMP.MacPac.Session.AdminMode && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
                this.mnuMacPac_Sep2.Visible = LMP.MacPac.Session.AdminMode && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
                if (this.ForteDocument.AllowDocumentTrailer)
                    mnuMacPac_ToggleDocTrailer.Text = LMP.Resources.GetLangString("Menu_MacPac_SuppressDocumentTrailer");
                else
                    mnuMacPac_ToggleDocTrailer.Text = LMP.Resources.GetLangString("Menu_MacPac_AllowDocumentTrailer");
                if (this.ForteDocument.IsSectionMarkedForNoTrailer(this.ForteDocument.WordDocument.Application.Selection.Sections[1].Index))
                    mnuMacPac_ToggleSecTrailer.Text = LMP.Resources.GetLangString("Menu_MacPac_AllowSectionTrailer");
                else
                    mnuMacPac_ToggleSecTrailer.Text = LMP.Resources.GetLangString("Menu_MacPac_SuppressSectionTrailer");
            }
            else
            {
                mnuMacPac_Sep2.Visible = false;
                mnuMacPac_ToggleDocTrailer.Visible = false;
                mnuMacPac_ToggleSecTrailer.Visible = false;
                this.mnuMacPac_Sep2.Visible = false;
            }
        }
        /// <summary>
        /// handler deals with focus issue, provides one-click access
        /// to available tabs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabMain_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                //if we click in tab area, use this method to derive ultra tab
                Infragistics.Win.UltraWinTabControl.UltraTab oTab = tabMain.TabFromPoint(e.Location);

                if (oTab == null)
                    return;

                //GLOG 4611: Use Tab index instead of Caption
                if (oTab.Index == pageContent.Tab.Index)
                {
                    SelectTab(Tabs.Content);
                }
                else if (oTab.Index == pageEdit.Tab.Index)
                {
                    SelectTab(Tabs.Edit);
                }
                else if (oTab.Index == pageReview.Tab.Index)
                {
                    //this.DataReviewer.ReviewMode = DataReviewer.Modes.Edit;
                    SelectTab(Tabs.Review);
                }
                else if (oTab.Index == pageImportVariables.Tab.Index)
                {
                    SelectTab(Tabs.Import);
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMacPac_ShowKeyboardShortcuts_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                LMP.MacPac.Application.ShowKeyboardShortcuts();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuMacPac_UpdateCI_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                LMP.MacPac.Application.InsertContactDetail();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void FavoriteSegmentMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                int iSegmentIndex = (int)((System.Windows.Forms.ToolStripItem)sender).Tag;
                InsertFavoriteSegment(iSegmentIndex);
            }
            catch(System.Exception oE){
                LMP.Error.Show(oE);
                }
        }

        /// <summary>
        /// inserts the favorite segment with the specified index
        /// </summary>
        /// <param name="iSegmentIndex"></param>
        private void InsertFavoriteSegment(int iSegmentIndex)
        {
            string xSegmentID = FavoriteSegments.SegmentID(iSegmentIndex);
            //GLOG 6966: Make sure string can be parsed as a Double even if
            //the Decimal separator is something other than '.' in Regional Settings
            if (xSegmentID.EndsWith(".0"))
            {
                xSegmentID = xSegmentID.Replace(".0", "");
            }
            double dSegmentID = double.Parse(xSegmentID);
            int iSegmentID = (int)dSegmentID;

            // Access the task pane's segment and reuse that code to do the insertion.
            // We need the task pane to be loaded in order to reuse the logic
            // employed by the content manager when the user selects a node.
            // Showing the task pane will do this.
            Word.Application oWord = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
            if (oWord.Documents.Count == 0)
            {
                LMP.MacPac.Application.CreateNewDocument();
            }

            mpFolderMemberTypes oType = FavoriteSegments.SegmentFolderMemberType(iSegmentIndex);

            switch (oType)
            {
                case mpFolderMemberTypes.AdminSegment:
                    {
                        AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs();
                        AdminSegmentDef oDef = null;

                        try
                        {
                            oDef = (AdminSegmentDef)oDefs.ItemFromID(iSegmentID);
                        }
                        catch
                        {
                            MessageBox.Show(Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                                ComponentProperties.ProductName, MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            FavoriteSegments.Remove(iSegmentIndex);
                            FavoriteSegments.Save();
                            return;
                        }

                        InsertSegment(oDef, (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation,
                            (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior);

                        break;
                    }
                case mpFolderMemberTypes.UserSegment:
                    {
                        UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                        UserSegmentDef oDef = null;

                        try
                        {
                            oDef = (UserSegmentDef)oDefs.ItemFromID(xSegmentID);
                        }
                        catch
                        {
                            MessageBox.Show(Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                                ComponentProperties.ProductName, MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            FavoriteSegments.Remove(iSegmentIndex);
                            FavoriteSegments.Save();
                            return;
                        }

                        InsertSegment(oDef, (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation,
                            (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior);
                        break;
                    }
                case mpFolderMemberTypes.VariableSet:
                    {
                        VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.User,
                            LMP.MacPac.Session.CurrentUser.ID);
                        VariableSetDef oDef = null;

                        try
                        {
                            oDef = (VariableSetDef)oDefs.ItemFromID(xSegmentID);
                        }
                        catch
                        {
                            MessageBox.Show(Resources.GetLangString("Msg_RemoveObsoleteFavoriteSegment"),
                                ComponentProperties.ProductName, MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            FavoriteSegments.Remove(iSegmentIndex);
                            FavoriteSegments.Save();
                            return;
                        }

                        InsertSegment(oDef);
                        break;
                    }
            }
        }
        void mnuMacPac_FavoriteSegments_DropDownOpening(object sender, EventArgs e)
        {
            LMP.Controls.TimedContextMenuStrip oDropDown = new LMP.Controls.TimedContextMenuStrip();

            this.mnuMacPac_FavoriteSegments.DropDown = oDropDown;

            // Repopulate the menu with the current favorite segments.
            ToolStripMenuItem oItem = null; ;

            oDropDown.Items.Clear();

            for (int i = 0; i < FavoriteSegments.Count; i++)
            {
                oItem = new System.Windows.Forms.ToolStripMenuItem();
                oItem.Name = FavoriteSegments.SegmentDisplayName(i);
                oItem.Size = new System.Drawing.Size(152, 22);
                oItem.Text = Convert.ToString(i + 1) + ". " + FavoriteSegments.SegmentDisplayName(i);
                oItem.Tag = i;
                oItem.Click += new EventHandler(FavoriteSegmentMenuItem_Click);
                oDropDown.Items.Add(oItem);
            }
        }

        private void TaskPane_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3 && e.Control)
            {
                // Toggle to the next task pane tab.
                this.Toggle();
            }
        }

        private void tabMain_KeyDown(object sender, KeyEventArgs e)
        {
            // GLOG : 1176 : CEH
            if (e.Control && (e.KeyCode == Keys.F1))
            {
                //Ctl+F1 selects the find text box
                this.ContentManager.ActivateFind();
            }
            else if(e.KeyCode == Keys.F3 && e.Control)
            {
                // Toggle to the next task pane tab.
                this.Toggle();
            }
        }

        private void mnuMacPac_RecreateLegacyContent_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                LMP.MacPac.Application.ConvertLegacyDocument();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuMacPac_SaveLegacyData_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                LMP.MacPac.Application.CreateLegacyPrefill();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuMacPac_ShowAdministrator_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.DoEvents();

            LMP.MacPac.Application.ShowAdministrator();
        }

        private void tabMain_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            LMP.MacPac.Application.MacPac10Ribbon.Invalidate();

            if (e.Tab == this.pageEdit.Tab)
            {
                if (this.DocEditor != null)
                {
                    this.DocEditor.UpdateNodeView();
                }
            }
            //GLOG item #6258 - dcf
            if (e.Tab == this.pageReview.Tab)
            {
                if (this.DataReviewer != null)
                {
                    this.DataReviewer.SetupTreeIfNecessary();
                }
            }
            else if (e.Tab == this.pageContent.Tab)
            {
                if (this.ContentManager != null)
                {
                    this.ContentManager.SetupIfNecessary();
                }
            }
        }

        private void mnuMacPac_ShowAbout_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.DoEvents();

            AboutForm oForm = new AboutForm();
            oForm.ShowDialog();
        }

        private void mnuMacPac_ToggleDocTrailer_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                this.ForteDocument.AllowDocumentTrailer = !this.ForteDocument.AllowDocumentTrailer;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuMacPac_ToggleSecTrailer_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                int iSecIndex = this.ForteDocument.WordDocument.Application.Selection.Sections[1].Index;
                this.ForteDocument.MarkSectionForNoTrailer(iSecIndex, this.ForteDocument.IsSectionMarkedForNoTrailer(iSecIndex));
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // GLOG : 3135 : JAB
        // Handle selection of the update document content menu item.
        private void mnuMacPac_UpdateDocumentContent_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                LMP.MacPac.Application.UpdateDocumentContent();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuMacPac_ConvertToLanguageSupport_Click(object sender, EventArgs e)
        {
            try
            {
                this.DocDesigner.ConvertToLanguageSupport();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************private functions*********************
		/// <summary>
		/// initializes and LMP.Session object if necessary
		/// </summary>
        private void InitializeSessionIfNecessary()
		{
            Trace.WriteInfo("Executing LMP.MacPac.TaskPane.InitializeSessionIfNecessary");
			
			try
			{
                Word.Application oWord = GetHostingWordAppInstance();
				Session.StartIfNecessary(oWord);
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.ApplicationInitializationException(
					"Error_CouldNotInitializeMacPac",oE);
			}
		}
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                //this.Refresh(); - replaced with line below (GLOG 2768, 8/1/08)
                TaskPanes.RefreshAll();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

        /// <summary>
        /// returns the Word application that is hosting this task pane
        /// </summary>
        /// <returns></returns>
        //abstract protected Word.Application GetHostingWordAppInstance();

        protected Word.Application GetHostingWordAppInstance()
        {
            return LMP.MacPac.Session.CurrentWordApp;
        }

        /// <summary>
        /// sets up the task pane
        /// </summary>
        private void Setup()
        {
#if Oxml
            if (LMP.MacPac.Application.IsOxmlSupported())
            {
                //for now, always run the api setup of the task pane-
                //we'll work on this in a future release
                //Setup_Api();
                Setup_Oxml();
            }
            else
            {
#endif
                Setup_Api();
#if Oxml
            }
#endif
        }

        private void Setup_Api()
        {
            DateTime t0 = DateTime.Now;

            Trace.WriteInfo("Executing LMP.MacPac.TaskPane.Setup");

            //Stop timer while taskpane is initializing
            if (m_oTaskPaneTimer != null)
                m_oTaskPaneTimer.Stop();

            Word.Document oWordDoc = null;
            Word.Document oActiveDoc = null;

            this.ContentManager = new ContentManager();

            bool bRibbonOnlyImplementation = LMP.MacPac.MacPacImplementation.IsToolkit;

            if (!bRibbonOnlyImplementation)
            {
                this.ContentManager.BackColor = System.Drawing.Color.Transparent;
                this.ContentManager.Dock = System.Windows.Forms.DockStyle.Fill;
                this.ContentManager.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F,
                    System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.ContentManager.Location = new System.Drawing.Point(0, 0);
                this.ContentManager.Name = "ucContentManager";
                this.ContentManager.Size = new System.Drawing.Size(312, 698);
                this.ContentManager.TabIndex = 0;
                this.pageContent.Controls.Add(this.ContentManager);
            }

            try
            {
                oActiveDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            }
            catch
            {
                return;
            }

            oActiveDoc.ActiveWindow.View.Zoom.Percentage = 100;

            try
            {
                //initialize a MacPac session if necessary
                InitializeSessionIfNecessary();

                m_oForteDocument = new ForteDocument(oActiveDoc);

                //Link Taskpane to current document in Hashtable
                TaskPane oTP = TaskPanes.Item(oActiveDoc);
                if (oTP == null)
                    TaskPanes.Add(m_oForteDocument.WordDocument, this);
                else if (oTP.IsDisposed)
                {
                    //replace with a new one
                    TaskPanes.Remove(m_oForteDocument.WordDocument);
                    TaskPanes.Add(m_oForteDocument.WordDocument, this);
                }

                //get start dirty state of document
                bool bSaved = this.ForteDocument.WordDocument.Saved;

                //put handle for this TaskPane in Word variable
                object xVar = "zzmp10TP";
                string xHandle = this.Handle.ToString();

                m_oForteDocument.WordDocument.Variables.get_Item(ref xVar).Value = xHandle;
                this.ForteDocument.WordDocument.Saved = bSaved;

                if (!LMP.MacPac.MacPacImplementation.IsServer)
                {
                    //create new timer object - used to undo illegal insertions
                    m_oInsertionUndoTimer = new System.Timers.Timer(10);
                    m_oInsertionUndoTimer.SynchronizingObject = this;
                    m_oInsertionUndoTimer.Elapsed += new System.Timers.ElapsedEventHandler(
                        m_oInsertionUndoTimer_Elapsed);

                    //create new timer object - used when targeted refresh fails
                    m_oRefreshTimer = new System.Timers.Timer(10);
                    m_oRefreshTimer.SynchronizingObject = this;
                    m_oRefreshTimer.Elapsed += new System.Timers.ElapsedEventHandler(
                        m_oRefreshTimer_Elapsed);

                    //create new timer object - used on Word 2003 paste
                    m_oPasteTimer = new System.Timers.Timer(10);
                    m_oPasteTimer.SynchronizingObject = this;
                    m_oPasteTimer.Elapsed += new System.Timers.ElapsedEventHandler(
                        m_oPasteTimer_Elapsed);

                    //create new timer object - used to retag after save
                    m_oSaveTimer = new System.Timers.Timer(50);
                    m_oSaveTimer.SynchronizingObject = this;
                    m_oSaveTimer.Elapsed += new System.Timers.ElapsedEventHandler(m_oSaveTimer_Elapsed);

                    //handle doc close event - we'll unsubscribe the sel change handler 
                    //and document save handler at that point
                    Word.DocumentClass oDoc = (Word.DocumentClass)m_oForteDocument.WordDocument;
                    oDoc.DocumentEvents2_Event_Close +=
                        new Word.DocumentEvents2_CloseEventHandler(
                        oDoc_DocumentEvents2_Event_Close);

                    //handle XMLBeforeDelete event
                    oDoc.XMLBeforeDelete +=
                        new Word.DocumentEvents2_XMLBeforeDeleteEventHandler(oDoc_XMLBeforeDelete);

                    //handle XMLAfterInsert event -
                    m_oForteDocument.WordDocument.XMLAfterInsert +=
                        new Word.DocumentEvents2_XMLAfterInsertEventHandler(oDoc_XMLAfterInsert);

                    //handle ContentControlBeforeDelete event
                    oDoc.ContentControlBeforeDelete +=
                        new Word.DocumentEvents2_ContentControlBeforeDeleteEventHandler(
                        oDoc_ContentControlBeforeDelete);

                    //handle ContentControlAfterAdd event
                    oDoc.ContentControlAfterAdd +=
                        new Word.DocumentEvents2_ContentControlAfterAddEventHandler(
                        oDoc_ContentControlAfterAdd);

                    //handle ContentControlOnEnter event
                    oDoc.ContentControlOnEnter +=
                        new Microsoft.Office.Interop.Word.DocumentEvents2_ContentControlOnEnterEventHandler(
                            oDoc_ContentControlOnEnter);

                    //handle ContentControlOnExit event
                    oDoc.ContentControlOnExit +=
                        new Microsoft.Office.Interop.Word.DocumentEvents2_ContentControlOnExitEventHandler(
                            oDoc_ContentControlOnExit);
                }

                this.ContentManager.TaskPane = this;

                //process any pending actions
                if (TaskPane.PendingAction != null)
                {
                    ProcessPendingAction();
                }
                else
                {
                    //if no pending actions, we must be in edit mode
                    this.Mode = ForteDocument.Modes.Edit;
                    //this.ContentManager.SetupIfNecessary();
                }

                if (LMP.MacPac.MacPacImplementation.IsServer)
                    return;

                //set ForteDocument mode property
                this.ForteDocument.Mode = this.Mode;

                if (this.Mode == ForteDocument.Modes.Edit)
                {
                    //refresh task pane to display
                    //current content if we're in edit mode -
                    //this allows the task pane to display
                    //contents when a document is opened
                    if (this.ScreenUpdating)
                    {
                        this.ScreenUpdating = false;
                    }

                    //add a data reviewer to the review tab
                    DataReviewer oReviewer = new DataReviewer(this.ForteDocument);
                    oReviewer.Dock = DockStyle.Fill;
                    oReviewer.TaskPane = this;
                    this.DataReviewer = oReviewer;
                    this.pageReview.Controls.Add(oReviewer);

                    //GLOG item #3496 - we conditionalize
                    //on whether tags have been retreived -
                    //if a document is being opened, tags
                    //will not have been retrieved -
                    //if a document is being created tags
                    //will have been retreived as part of the
                    //creation process. Hence, there's no
                    //need to refresh
                    if (!this.ForteDocument.TagsRetrieved)
                    {
                        //reconstitute if necessary (dm 2/21/11)
                        //GLOG 5803 (dm) - we need to reconstitute everything in order
                        //to finish document below - modified call accordingly
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                        //GLOG 6801 (dm): don't reconstitute mSEGs
                        bool bReconstituted = oConvert.ReconstituteIfNecessary(
                            this.ForteDocument.WordDocument, false, false);
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                        //if (LMP.Utility.InDevelopmentModeState(2))
                        //{
                        //    //MessageBox.Show("Search for here");
                        //}
                        //else
                        //{
                        //get document tags
                        this.ForteDocument.RefreshTags();

                        //GLOG 5803 (dm) - document won't have been successfully finished
                        //on DocumentOpen if the bounding objects were missing - do now
                        //GLOG 6552 (dm) - this block was erroneously referencing
                        //Session.DisplayFinishButton instead of FinishDocumentsUponOpen
                        FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                        if (oSettings.FinishDocumentsUponOpen && bReconstituted)
                            LMP.MacPac.Application.FinishDocument(this.ForteDocument, true);

                        //if the document already contains 
                        //content, refresh variables tab
                        if (this.ForteDocument.Tags.Count > 0)
                        {
                            this.ForteDocument.Refresh(ForteDocument.RefreshOptions.None);

                            LMP.MacPac.Application.SetXMLTagStateForEditing();

                            //JTS 6/30/11:  Don't display editor if only segments are Trailers
                            if (this.ForteDocument.ContainsClientContent() &&
                                LMP.Forte.MSWord.WordDoc.ContainsNonTrailerMPContent(this.ForteDocument.WordDocument))
                            {
                                this.SelectTab(Tabs.Edit);

                                //GLOG 3070: select segment node if possible
                                if (this.ForteDocument.Segments.Count > 0)
                                {
                                    //JTS 6/30/11:  Trailers won't have node, so don't try to select
                                    //GLOG 7103 (dm) - in toolkit mode, display only segments
                                    //specified in metadata
                                    for (int c = 0; c < this.ForteDocument.Segments.Count; c++)
                                    {
                                        Segment oSeg = this.ForteDocument.Segments[c];
                                        if ((!(oSeg is Trailer)) && ((!LMP.MacPac.MacPacImplementation.IsToolkit) ||
                                            LMP.Data.Application.DisplayInToolkitTaskPane(oSeg.TypeID)))
                                        {
                                            //GLOG 7521
                                            this.DocEditor.SelectFirstNodeForSegment(oSeg);
                                            //this.DocEditor.SelectNode(oSeg.FullTagID);

                                            //GLOG item #5832 - dcf - 11/29/11
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Infragistics.Win.UltraWinTabControl.UltraTabsCollection oTabs = this.tabMain.Tabs;

                    if (bRibbonOnlyImplementation)
                    {
                        //hide tabs
                        oTabs[0].Visible = false;
                    }
                    else
                    {
                        //set tabs text
                        oTabs[0].Text = LMP.Resources.GetLangString("Prompt_ContentManagerTabText");
                        oTabs[2].Text = LMP.Resources.GetLangString("Prompt_ImportVariablesTabText");
                        oTabs[3].Text = LMP.Resources.GetLangString("Prompt_ReviewDataTabText");
                    }

                    if (this.tabMain.SelectedTab == oTabs[0])
                        this.ContentManager.SetupIfNecessary();

                    //check if there are any finished segments - 
                    //if so, display compare screen
                    //GLOG 5966 (dm) - show data reviewer only if there are segments -
                    //the snapshot doc vars may correspond to content that no longer exists
                    if ((this.ForteDocument.Segments.Count > 0) &&
                        this.ForteDocument.ContainsFinishedSegments)
                    {
                        this.DataReviewerVisible = true;
                    }

                    //resize the task pane based on the available window height
                    this.Height = Session.CurrentWordApp.ActiveWindow.Height - 25;

                    //GLOG 5812 (dm) - scroll to first tab
                    this.tabMain.Scroll(Infragistics.Win.UltraWinTabs.ScrollType.First);

                    //GLOG item #4084
                    ////return doc to original dirty state
                    //this.ForteDocument.WordDocument.Saved = bSaved;

                    oWordDoc = m_oForteDocument.WordDocument;

                    //fit to document page width if specified
                    if (Session.CurrentUser.UserSettings.FitToPageWidth)
                        oWordDoc.ActiveWindow.View.Zoom.PageFit =
                            Word.WdPageFit.wdPageFitBestFit;

                    if (this.Mode == ForteDocument.Modes.Edit)
                        this.DocEditor.ResizeTreeControl();

                    // Set the backcolor and the gradient according to the user's preference.
                    SetBackgroundAppearance(Color.Empty, 0);

                    if (m_oTaskPaneTimer != null)
                        m_oTaskPaneTimer.Start();

                    //GLOG item #6690 - dcf - 04/12/13
                    if (m_oForteDocument.Mode != ForteDocument.Modes.Design)
                    {
                        bool bDocDirty = m_oForteDocument.WordDocument.Saved;

                        for (int i = 0; i < m_oForteDocument.Segments.Count; i++)
                        {
                            this.DocEditor.UpdateSegmentImage(m_oForteDocument.Segments[i], true);
                        }

                        m_oForteDocument.WordDocument.Saved = bDocDirty;
                    }
                }
            }
            catch (System.Exception oE)
            {
                this.ForteDocument.MacPacFunctionalityIsAvailable = false;
                LMP.MacPac.Application.MacPac10Ribbon.Invalidate();

                //GLOG 6250 jsw - if segment is invalid, show appropriate message
                if (oE.Message.Contains(LMP.Resources.GetLangString("Error_InvalidSegmentID")))
                {
                    //parse out segment id
                    string xID = oE.ToString();
                    string xError = LMP.Resources.GetLangString("Error_InvalidSegmentID");
                    int iPos = xID.LastIndexOf(xError);
                    xID = xID.Substring(iPos + xError.Length);
                    iPos = xID.IndexOf(" ");
                    xID = xID.Substring(0, iPos);

                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Error_InvalidSegmentIDCantExecuteFeature"), xID),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;

                }
                else
                {
                    DialogResult iRes = MessageBox.Show(
                    LMP.Resources.GetLangString("Msg_MacPacNotAvailableInThisDocument"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);

                    if (iRes == DialogResult.No)
                        return;
                    else
                        throw new LMP.Exceptions.UIException(
                            LMP.Resources.GetLangString("Error_CouldNotSetUpTaskPane"), oE);
                }
            }
            finally
            {
                //note that task pane is set up
                m_bIsSetUp = true;

                if (!m_bSkipScreenUpdates && !this.ScreenUpdating)
                    this.ScreenUpdating = true;

                LMP.Benchmarks.Print(t0);
            }
        }

#if Oxml
        private void Setup_Oxml()
		{
            DateTime t0 = DateTime.Now;

            Trace.WriteInfo("Executing LMP.MacPac.TaskPane.Setup_Oxml");

            //Stop timer while taskpane is initializing
            if (m_oTaskPaneTimer != null)
                m_oTaskPaneTimer.Stop();

            Word.Document oWordDoc = null;
            Word.Document oActiveDoc = null;

            this.ContentManager = new ContentManager();

            bool bRibbonOnlyImplementation = LMP.MacPac.MacPacImplementation.IsToolkit;

            if (!bRibbonOnlyImplementation)
            {
                this.ContentManager.BackColor = System.Drawing.Color.Transparent;
                this.ContentManager.Dock = System.Windows.Forms.DockStyle.Fill;
                this.ContentManager.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F,
                    System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.ContentManager.Location = new System.Drawing.Point(0, 0);
                this.ContentManager.Name = "ucContentManager";
                this.ContentManager.Size = new System.Drawing.Size(312, 698);
                this.ContentManager.TabIndex = 0;
                this.pageContent.Controls.Add(this.ContentManager);
            }

            try
            {
                oActiveDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            }
            catch
            {
                return;
            }

            oActiveDoc.ActiveWindow.View.Zoom.Percentage = 100;

            try
            {
                //initialize a MacPac session if necessary
                InitializeSessionIfNecessary();

                m_oForteDocument = new ForteDocument(oActiveDoc);

                //Link Taskpane to current document in Hashtable
                TaskPane oTP = TaskPanes.Item(oActiveDoc);
                if (oTP == null)
                    TaskPanes.Add(m_oForteDocument.WordDocument, this);
                else if (oTP.IsDisposed)
                {
                    //replace with a new one
                    TaskPanes.Remove(m_oForteDocument.WordDocument);
                    TaskPanes.Add(m_oForteDocument.WordDocument, this);
                }

                //get start dirty state of document
                bool bSaved = this.ForteDocument.WordDocument.Saved;

                //put handle for this TaskPane in Word variable
                object xVar = "zzmp10TP";
                string xHandle = this.Handle.ToString();

                m_oForteDocument.WordDocument.Variables.get_Item(ref xVar).Value = xHandle;
                this.ForteDocument.WordDocument.Saved = bSaved;

                if (!LMP.MacPac.MacPacImplementation.IsServer)
                {
                    //create new timer object - used to undo illegal insertions
                    m_oInsertionUndoTimer = new System.Timers.Timer(10);
                    m_oInsertionUndoTimer.SynchronizingObject = this;
                    m_oInsertionUndoTimer.Elapsed += new System.Timers.ElapsedEventHandler(
                        m_oInsertionUndoTimer_Elapsed);

                    //create new timer object - used when targeted refresh fails
                    m_oRefreshTimer = new System.Timers.Timer(10);
                    m_oRefreshTimer.SynchronizingObject = this;
                    m_oRefreshTimer.Elapsed += new System.Timers.ElapsedEventHandler(
                        m_oRefreshTimer_Elapsed);

                    //create new timer object - used on Word 2003 paste
                    m_oPasteTimer = new System.Timers.Timer(10);
                    m_oPasteTimer.SynchronizingObject = this;
                    m_oPasteTimer.Elapsed += new System.Timers.ElapsedEventHandler(
                        m_oPasteTimer_Elapsed);

                    //create new timer object - used to retag after save
                    m_oSaveTimer = new System.Timers.Timer(50);
                    m_oSaveTimer.SynchronizingObject = this;
                    m_oSaveTimer.Elapsed += new System.Timers.ElapsedEventHandler(m_oSaveTimer_Elapsed);

                    //handle doc close event - we'll unsubscribe the sel change handler 
                    //and document save handler at that point
                    Word.DocumentClass oDoc = (Word.DocumentClass)m_oForteDocument.WordDocument;
                    oDoc.DocumentEvents2_Event_Close +=
                        new Word.DocumentEvents2_CloseEventHandler(
                        oDoc_DocumentEvents2_Event_Close);

                    //JTS: Oxml document will never have XML Tags, so no need for these events
                    ////handle XMLBeforeDelete event
                    //oDoc.XMLBeforeDelete +=
                    //    new Word.DocumentEvents2_XMLBeforeDeleteEventHandler(oDoc_XMLBeforeDelete);

                    ////handle XMLAfterInsert event -
                    //m_oForteDocument.WordDocument.XMLAfterInsert +=
                    //    new Word.DocumentEvents2_XMLAfterInsertEventHandler(oDoc_XMLAfterInsert);

                    //handle ContentControlBeforeDelete event
                    oDoc.ContentControlBeforeDelete +=
                        new Word.DocumentEvents2_ContentControlBeforeDeleteEventHandler(
                        oDoc_ContentControlBeforeDelete);

                    //handle ContentControlAfterAdd event
                    oDoc.ContentControlAfterAdd +=
                        new Word.DocumentEvents2_ContentControlAfterAddEventHandler(
                        oDoc_ContentControlAfterAdd);

                    //handle ContentControlOnEnter event
                    oDoc.ContentControlOnEnter +=
                        new Microsoft.Office.Interop.Word.DocumentEvents2_ContentControlOnEnterEventHandler(
                            oDoc_ContentControlOnEnter);

                    //handle ContentControlOnExit event
                    oDoc.ContentControlOnExit +=
                        new Microsoft.Office.Interop.Word.DocumentEvents2_ContentControlOnExitEventHandler(
                            oDoc_ContentControlOnExit);
                }

                this.ContentManager.TaskPane = this;

                //process any pending actions
                if (TaskPane.PendingAction != null)
                {
                    ProcessPendingAction();
                }
                else
                {
                    //if no pending actions, we must be in edit mode
                    this.Mode = ForteDocument.Modes.Edit;
                    //this.ContentManager.SetupIfNecessary();
                }

                if (LMP.MacPac.MacPacImplementation.IsServer)
                    return;

                //set ForteDocument mode property
                this.ForteDocument.Mode = this.Mode;

                if (this.Mode == ForteDocument.Modes.Edit)
                {
                    //refresh task pane to display
                    //current content if we're in edit mode -
                    //this allows the task pane to display
                    //contents when a document is opened
                    if (this.ScreenUpdating)
                    {
                        this.ScreenUpdating = false;
                    }

                    //add a data reviewer to the review tab
                    DataReviewer oReviewer = new DataReviewer(this.ForteDocument);
                    oReviewer.Dock = DockStyle.Fill;
                    oReviewer.TaskPane = this;
                    this.DataReviewer = oReviewer;
                    this.pageReview.Controls.Add(oReviewer);

                    //GLOG item #3496 - we conditionalize
                    //on whether tags have been retreived -
                    //if a document is being opened, tags
                    //will not have been retrieved -
                    //if a document is being created tags
                    //will have been retreived as part of the
                    //creation process. Hence, there's no
                    //need to refresh
                    if (!this.ForteDocument.TagsRetrieved)
                    {
                        //reconstitute if necessary (dm 2/21/11)
                        //GLOG 5803 (dm) - we need to reconstitute everything in order
                        //to finish document below - modified call accordingly
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                        //GLOG 6801 (dm): don't reconstitute mSEGs
                        bool bReconstituted = oConvert.ReconstituteIfNecessary(
                            this.ForteDocument.WordDocument, false, false);
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                        //if (LMP.Utility.InDevelopmentModeState(2))
                        //{
                        //    //MessageBox.Show("Search for here");
                        //}
                        //else
                        //{
                        //get document tags
                        this.ForteDocument.RefreshTags();

                        //GLOG 5803 (dm) - document won't have been successfully finished
                        //on DocumentOpen if the bounding objects were missing - do now
                        //GLOG 6552 (dm) - this block was erroneously referencing
                        //Session.DisplayFinishButton instead of FinishDocumentsUponOpen
                        FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                        if (oSettings.FinishDocumentsUponOpen && bReconstituted)
                            LMP.MacPac.Application.FinishDocument(this.ForteDocument, true);

                        //if the document already contains 
                        //content, refresh variables tab
                        if (this.ForteDocument.Tags.Count > 0)
                        {
                            this.ForteDocument.Refresh(ForteDocument.RefreshOptions.None);

                            LMP.MacPac.Application.SetXMLTagStateForEditing();

                            //JTS 6/30/11:  Don't display editor if only segments are Trailers
                            if (this.ForteDocument.ContainsClientContent() &&
                                LMP.Forte.MSWord.WordDoc.ContainsNonTrailerMPContent(this.ForteDocument.WordDocument))
                            {
                                this.SelectTab(Tabs.Edit);

                                //GLOG 3070: select segment node if possible
                                if (this.ForteDocument.Segments.Count > 0)
                                {
                                    //JTS 6/30/11:  Trailers won't have node, so don't try to select
                                    //GLOG 7103 (dm) - in toolkit mode, display only segments
                                    //specified in metadata
                                    for (int c = 0; c < this.ForteDocument.Segments.Count; c++)
                                    {
                                        Segment oSeg = this.ForteDocument.Segments[c];
                                        if ((!(oSeg is Trailer)) && ((!LMP.MacPac.MacPacImplementation.IsToolkit) ||
                                            LMP.Data.Application.DisplayInToolkitTaskPane(oSeg.TypeID)))
                                        {
                                            //GLOG 7521
                                            this.DocEditor.SelectFirstNodeForSegment(oSeg);
                                            //this.DocEditor.SelectNode(oSeg.FullTagID);

                                            //GLOG item #5832 - dcf - 11/29/11
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Infragistics.Win.UltraWinTabControl.UltraTabsCollection oTabs = this.tabMain.Tabs;

                    if (bRibbonOnlyImplementation)
                    {
                        //hide tabs
                        oTabs[0].Visible = false;
                    }
                    else
                    {
                        //set tabs text
                        oTabs[0].Text = LMP.Resources.GetLangString("Prompt_ContentManagerTabText");
                        oTabs[2].Text = LMP.Resources.GetLangString("Prompt_ImportVariablesTabText");
                        oTabs[3].Text = LMP.Resources.GetLangString("Prompt_ReviewDataTabText");
                    }

                    if (this.tabMain.SelectedTab == oTabs[0])
                        this.ContentManager.SetupIfNecessary();

                    //check if there are any finished segments - 
                    //if so, display compare screen
                    //GLOG 5966 (dm) - show data reviewer only if there are segments -
                    //the snapshot doc vars may correspond to content that no longer exists
                    if ((this.ForteDocument.Segments.Count > 0) &&
                        this.ForteDocument.ContainsFinishedSegments)
                    {
                        this.DataReviewerVisible = true;
                    }

                    //resize the task pane based on the available window height
                    this.Height = Session.CurrentWordApp.ActiveWindow.Height - 25;

                    //GLOG 5812 (dm) - scroll to first tab
                    this.tabMain.Scroll(Infragistics.Win.UltraWinTabs.ScrollType.First);

                    //GLOG item #4084
                    ////return doc to original dirty state
                    //this.ForteDocument.WordDocument.Saved = bSaved;

                    oWordDoc = m_oForteDocument.WordDocument;

                    //fit to document page width if specified
                    if (Session.CurrentUser.UserSettings.FitToPageWidth)
                        oWordDoc.ActiveWindow.View.Zoom.PageFit =
                            Word.WdPageFit.wdPageFitBestFit;

                    if (this.Mode == ForteDocument.Modes.Edit)
                        this.DocEditor.ResizeTreeControl();

                    // Set the backcolor and the gradient according to the user's preference.
                    SetBackgroundAppearance(Color.Empty, 0);

                    if (m_oTaskPaneTimer != null)
                        m_oTaskPaneTimer.Start();

                    //GLOG item #6690 - dcf - 04/12/13
                    if (m_oForteDocument.Mode != ForteDocument.Modes.Design)
                    {
                        bool bDocDirty = m_oForteDocument.WordDocument.Saved;

                        if (TaskPane.NewXmlSegment != null)
                        {
                            this.DocEditor.UpdateSegmentImage(TaskPane.NewXmlSegment, true, false);
                        }
                        else
                        {
                            for (int i = 0; i < m_oForteDocument.Segments.Count; i++)
                            {
                                this.DocEditor.UpdateSegmentImage(m_oForteDocument.Segments[i], true);
                            }
                        }
                        m_oForteDocument.WordDocument.Saved = bDocDirty;
                    }
                }
            }
            catch (System.Exception oE)
            {
                this.ForteDocument.MacPacFunctionalityIsAvailable = false;
                LMP.MacPac.Application.MacPac10Ribbon.Invalidate();

                //GLOG 6250 jsw - if segment is invalid, show appropriate message
                if (oE.Message.Contains(LMP.Resources.GetLangString("Error_InvalidSegmentID")))
                {
                    //parse out segment id
                    string xID = oE.ToString();
                    string xError = LMP.Resources.GetLangString("Error_InvalidSegmentID");
                    int iPos = xID.LastIndexOf(xError);
                    xID = xID.Substring(iPos + xError.Length);
                    iPos = xID.IndexOf(" ");
                    xID = xID.Substring(0, iPos);

                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Error_InvalidSegmentIDCantExecuteFeature"), xID),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;

                }
                else
                {
                    DialogResult iRes = MessageBox.Show(
                    LMP.Resources.GetLangString("Msg_MacPacNotAvailableInThisDocument"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);

                    if (iRes == DialogResult.No)
                        return;
                    else
                        throw new LMP.Exceptions.UIException(
                            LMP.Resources.GetLangString("Error_CouldNotSetUpTaskPane"), oE);
                }
            }
            finally
            {
                //note that task pane is set up
                m_bIsSetUp = true;

                if (!m_bSkipScreenUpdates && !this.ScreenUpdating)
                    this.ScreenUpdating = true;

                LMP.Benchmarks.Print(t0);
            }
        }
#endif

        void m_oSaveTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (m_oSaveTimer.Enabled)
                {
                    bool bSaved = false;
                    try
                    {
                        bSaved = this.ForteDocument.WordDocument.Saved;
                    }
                    catch
                    {
                        //ignore error - user may have cancelled save dialog and
                        //closed the document without saving it
                        m_oSaveTimer.Stop();
                        return;
                    }

                    if (bSaved)
                    {
                        m_oSaveTimer.Stop();
                        m_bSaveInProgress = false;

                        //GLOG 7498 (dm) - hide taskpane if document has been saved
                        //as .doc in Word 2013 - note that we can't use
                        //ForteDocument.FileFormat here because it might be
                        //flagged to falsely return open xml for trailer purposes
                        if ((LMP.Forte.MSWord.WordApp.Version >= 15) &&
                            (this.ForteDocument.WordDocument.SaveFormat == 0))
                        {
                            WordTaskPane.Visible = false;
                            return;
                        }

                        //if doc format has changed, refresh task pane -
                        //this will reconstitute Forte content as necessary
                        mpFileFormats iDocFormat = this.ForteDocument.FileFormat;
                        LMP.Forte.MSWord.mpBoundingObjectTypes iBoundingObjectType =
                            this.ForteDocument.Tags.BoundingObjectType;
                        if (((iDocFormat == mpFileFormats.OpenXML) &&
                            (iBoundingObjectType == LMP.Forte.MSWord.mpBoundingObjectTypes.WordXMLNodes)) ||
                            ((iDocFormat == mpFileFormats.Binary) &&
                            (iBoundingObjectType == LMP.Forte.MSWord.mpBoundingObjectTypes.ContentControls)))
                        {
                            DateTime t0 = DateTime.Now;

                            //GLOG 6578 (dm) - user has saved as .docx and maintained compatibility - hide task pane
                            if (LMP.Architect.Api.Environment.DocxIsIn2003CompatibilityMode(this.ForteDocument.WordDocument, false))
                            {
                                WordTaskPane.Visible = false;
                                return;
                            }

                            //reconstitute
                            ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                            LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                            //GLOG 6801 (dm): don't reconstitute mSEGs
                            oConvert.ReconstituteIfNecessary(this.ForteDocument.WordDocument, false, false);
                            ForteDocument.IgnoreWordXMLEvents = iEvents;

                            //refresh
                            this.ForteDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes,
                                false, false);
                            this.ucDocEditor.UpdateTreeNodeTags();

                            LMP.Benchmarks.Print(t0);
                        }

                        this.ForteDocument.WordDocument.Saved = true;
                    }
                    else if (LMP.MacPac.Application.SendMessageToTaskPane(this.ForteDocument.WordDocument,
                        LMP.MacPac.Application.mpPseudoWordEvents.QueryInputFocus))
                    {
                        //this branch stops the timer after the focus has been back
                        //in the document for 3 seconds, allowing ContentControlBeforeDelete
                        //handling to resume in the case that the save dialog is cancelled -
                        //hopefully, 3 seconds leaves enough time for Word to delete the
                        //content controls before carrying out a .docx to .doc save
                        m_iSaveCounter++;
                        if (m_iSaveCounter == 60)
                        {
                            m_oSaveTimer.Stop();
                            m_bSaveInProgress = false;
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void oDoc_ContentControlOnExit(Word.ContentControl ContentControl, ref bool Cancel)
        {
            if (MacPac.Application.DisableTaskpaneEvents || LMP.MacPac.Application.Paused)
                return;

            //GLOG 8272
            if (LastSaveIsWithinThreshold())
            {
                return;
            }

            DateTime t0 = DateTime.Now;
            try
            {
                Word.Document oDoc = this.ForteDocument.WordDocument;
                if (oDoc == null)
                    return;
                else if (((ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLSelectionChange) > 0) ||
                    this.m_bInsertionIsInvalid || LMP.Forte.MSWord.WordDoc.IsTempDoc(oDoc))
                    return;

                //GLOG 4241 (dm) - store screen updating status
                bool bScreenUpdating = this.ScreenUpdating;

                //Don't run this handler in protected document
                if (LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false))
                    return;

                //mark that a tag selection is occuring
                this.WordTagChangeOccurring = true;

                if (this.Mode == ForteDocument.Modes.Edit || this.Mode == ForteDocument.Modes.Preview)
                    this.DocEditor.OnContentControlExit(ContentControl, ref Cancel, false);
                else if (this.Mode == ForteDocument.Modes.Design)
                    this.DocDesigner.OnContentControlExit(ContentControl, ref Cancel, false);

                this.WordTagChangeOccurring = false;

                if (!this.ScreenUpdating)
                    this.ScreenUpdating = bScreenUpdating;

                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void oDoc_ContentControlOnEnter(Word.ContentControl ContentControl)
        {
            DateTime t0 = DateTime.Now;
            if (MacPac.Application.DisableTaskpaneEvents || LMP.MacPac.Application.Paused)
                return;
            
            //GLOG 8272
            if (LastSaveIsWithinThreshold())
            {
                return;
            }

            try
            {
                Word.Document oDoc = this.ForteDocument.WordDocument;

                if (oDoc == null)
                    return;

                //GLOG : 6398 : CEH
                //don't run event when in Reviewing pane - 
                //GLOG 6669: Test InReviewingPane last, so that it won't get called during actions when IgnoreWordXMLEvents is set
                else if (((ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLSelectionChange) > 0) ||
                    this.m_bInsertionIsInvalid || LMP.Forte.MSWord.WordDoc.IsTempDoc(oDoc) || LMP.Forte.MSWord.WordDoc.InReviewingPane(oDoc)) 
                    return;

                //GLOG 4241 (dm) - store screen updating status
                bool bScreenUpdating = this.ScreenUpdating;

                //Don't run this handler in protected document
                if (LMP.Architect.Api.Environment.DocumentIsProtected(oDoc, false))
                    return;

                //mark that a tag selection is occuring
                this.WordTagChangeOccurring = true;

                if (this.Mode == ForteDocument.Modes.Edit || this.Mode == ForteDocument.Modes.Preview)
                    this.DocEditor.OnContentControlEnter(ContentControl);
                else if (this.Mode == ForteDocument.Modes.Design)
                    this.DocDesigner.OnContentControlEnter(ContentControl);

                this.WordTagChangeOccurring = false;

                if (!this.ScreenUpdating)
                    this.ScreenUpdating = bScreenUpdating;

                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Set the background color and gradient for the task pane.
        /// </summary>
        private void SetBackgroundAppearance()
        {
            DateTime t0 = DateTime.Now;

            // Set the backcolor and gradient for the tree per user's prefs.
            Color oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            GradientStyle oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            SetBackgroundAppearance(oBackcolor, oGradient);

            LMP.Benchmarks.Print(t0);
        }

        private void SetBackgroundAppearance(Color oBackcolor, GradientStyle oGradient)
        {
            DateTime t0 = DateTime.Now;


            // Set the backcolor and gradient for the tree per user's prefs.
            if (oBackcolor == Color.Empty)
            {
                oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            }

            if (oGradient == 0)
            {
                oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                    GradientStyle.BackwardDiagonal : GradientStyle.None;
            }

            //set the gradient according to the user's preference if necessary
            if (this.tabMain.Appearance.BackColor != oBackcolor)
            {
                this.tabMain.Appearance.BackColor = oBackcolor;
            }

            if (this.tabMain.Appearance.BackGradientStyle != oGradient)
            {
               this.tabMain.Appearance.BackGradientStyle = oGradient;
            }

            ////GLOG - 3587 - CEH
            ////help smooth out operation
            //System.Windows.Forms.Application.DoEvents();

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// returns true IFF the selection being processed for deletion will
        /// leave any orphans or parts of oSegment behind
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool IsPartialDeletion(Segment oSegment)
        {
            //cycle through segment parts, checking whether they're included
            //in the selection being deleted
            string xParts = oSegment.Nodes.GetItemPartNumbers(oSegment.FullTagID);
            string[] aParts = xParts.Split(';');
            if (aParts.Length > 1)
            {
                for (int i = 0; i < aParts.Length; i++)
                {
                    if (!m_xTagsInScope.Contains('|' + oSegment.FullTagID + '¦' + aParts[i]))
                        //not all parts will be deleted
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user deletion of oTag
        /// </summary>
        /// <param name="oTag"></param>
        /// <param name="oDeletedRange"></param>
        /// <param name="oSegment"></param>
        /// <param name="xFullTagID"></param>
        /// <param name="xElement"></param>
        private void RefreshBeforeTagDeletion_Edit(Word.XMLNode oTag, Word.Range oDeletedRange,
            Segment oSegment, string xFullTagID, string xElement)
        {
            //get tags in user's selection - since this event fires from bottom
            //to top, m_xFirstTagInScope will tell us when we've hit all the
            //tags in the user's selection - m_xTagsInScope will allow us to
            //skip items that will be deleted along with parent
            if (string.IsNullOrEmpty(m_xFirstTagInScope))
            {
                if (oDeletedRange != null)
                {
                    m_xTagsInScope = LMP.Forte.MSWord.WordDoc.GetTagsInRange(oDeletedRange, "");
                    string[] xTags = m_xTagsInScope.Split('|');
                    string[] xFields = xTags[1].Split('¦');
                    m_xFirstTagInScope = xFields[0];
                }
                else
                {
                    //this can occur if a user deletes a tag by backspacing -
                    //initialize variables empty to avoid errors elsewhere
                    m_xTagsInScope = "";
                    m_xFirstTagInScope = "";
                }
            }

            //get tag id of segment
            string xSegmentTagID = "";
            if (oSegment != null)
                xSegmentTagID = oSegment.FullTagID;

            //determine whether to refresh immediately
            bool bDoNow = false;

            if (xElement == "mSEG")
            {
                //mSEG - move external children if necessary
                bool bHasExternalChildren = (oSegment.Nodes.GetItemObjectDataValue(
                    xSegmentTagID, "ExternalChildren").ToUpper() == "TRUE");
                if ((bHasExternalChildren) && (this.ForteDocument.Tags
                    .MoveExternalChildren(oTag) == 1))
                {
                    //there were external children in the tags collection of
                    //the mSEG being deleted - determine whether all parts of
                    //the segment will be deleted
                    if (!IsPartialDeletion(oSegment))
                    {
                        //external children will be orphaned -
                        //a full document refresh is required
                        m_bDocRefreshRequired = true;
                    }
                }

                //refresh now - it would take as long to determine
                //whether this is necessary as it does to just do it
                bDoNow = true;
            }
            else
                //refresh now if this mVar or mBlock is standalone or if the
                //segment to which it belongs is not getting deleted
                bDoNow = ((xSegmentTagID == "") ||
                    (!m_xTagsInScope.Contains('|' + xSegmentTagID + '¦')) ||
                    IsPartialDeletion(oSegment));

            //refresh now
            if (bDoNow)
            {
                //remove tag from collection
                m_oForteDocument.Tags.Delete(oTag);

                //refresh impacted MP objects and ui elements
                if ((xElement == "mSEG") && !m_bDocRefreshRequired)
                    //segment
                    RefreshBeforemSEGDeletion(oSegment);
                else if (xElement == "mVar" || xElement == "mDel")
                    //variable
                    RefreshBeforemVarDeletion(oSegment, oTag);
                else if (xElement == "mBlock")
                    //block
                    RefreshBeforemBlockDeletion(oSegment, oTag);
                //delete doc vars (12-23-10 dm)
                //GLOG 5662:  Restructured for consistency with RefreshBeforeTagDeletion_Design
                Word.XMLNode oReserved = null;
                for (int c = 1; c <= oTag.Attributes.Count; c++)
                {
                    if (oTag.Attributes[c].BaseName == "Reserved")
                    {
                        oReserved = oTag.Attributes[c];
                        break;
                    }
                }
                if (oReserved != null)
                {
                    LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oReserved.NodeValue,
                        m_oForteDocument.WordDocument, true);
                }
            }

            //handle last tag in cycle
            if (m_xFirstTagInScope == xFullTagID)
            {
                //refresh document if required
                if (m_bDocRefreshRequired)
                    //refresh document
                    this.ForteDocument.Refresh(ForteDocument.RefreshOptions.None);

                //reset flags
                ResetFlags();
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user
        /// deletion of content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="oSegment"></param>
        /// <param name="xFullTagID"></param>
        /// <param name="xElement"></param>
        private void RefreshBeforeTagDeletion_Edit(Word.ContentControl oCC,
            Segment oSegment, string xFullTagID, string xElement)
        {
            //determine whether to refresh immediately -
            //TODO: see if there's a way to compensate for absence of DeletedRange
            //parameter on content control side
            bool bDoNow = true;

            if (xElement == "mSEG")
            {
                //mSEG - move external children if necessary
                string xSegmentTagID = "";
                if (oSegment != null)
                    xSegmentTagID = oSegment.FullTagID;
                bool bHasExternalChildren = (oSegment.Nodes.GetItemObjectDataValue(
                    xSegmentTagID, "ExternalChildren").ToUpper() == "TRUE");
                if (bHasExternalChildren)
                    this.ForteDocument.Tags.MoveExternalChildren_CC(oCC);
            }

            //refresh now
            if (bDoNow)
            {
                //remove tag from collection
                m_oForteDocument.Tags.Delete_CC(oCC);

                //refresh impacted MP objects and ui elements
                if (xElement == "mSEG")
                    //segment
                    RefreshBeforemSEGDeletion(oSegment);
                else if (xElement == "mVar" || xElement == "mDel")
                    //variable
                    RefreshBeforemVarDeletion(oSegment, oCC);
                else if (xElement == "mBlock")
                    //block
                    RefreshBeforemBlockDeletion(oSegment, oCC);

                //delete doc vars
                LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oCC.Tag,
                    m_oForteDocument.WordDocument, true);
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user
        /// deletion of content control
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="oSegment"></param>
        /// <param name="xFullTagID"></param>
        /// <param name="xElement"></param>
        private void RefreshBeforeTagDeletion_Design(Word.ContentControl oCC,
            Segment oSegment, string xFullTagID, string xElement)
        {
            //determine whether to refresh immediately -
            //TODO: see if there's a way to compensate for absence of DeletedRange
            //parameter on content control side
            bool bDoNow = true;

            if (xElement == "mSEG")
            {
                //mSEG - move external children if necessary
                string xSegmentTagID = "";
                if (oSegment != null)
                    xSegmentTagID = oSegment.FullTagID;
                bool bHasExternalChildren = (oSegment.Nodes.GetItemObjectDataValue(
                    xSegmentTagID, "ExternalChildren").ToUpper() == "TRUE");
                if (bHasExternalChildren)
                    this.ForteDocument.Tags.MoveExternalChildren_CC(oCC);
            }

            //refresh now
            if (bDoNow)
            {
                //remove tag from collection
                m_oForteDocument.Tags.Delete_CC(oCC);

                //refresh impacted MP objects and ui elements
                if (xElement == "mSEG")
                    //segment
                    RefreshBeforemSEGDeletion(oSegment);
                else if (xElement == "mVar")
                    //variable
                    RefreshBeforemVarDeletion(oSegment, oCC);
                else if (xElement == "mBlock")
                    //block
                    RefreshBeforemBlockDeletion(oSegment, oCC);

                //delete doc vars
                LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oCC.Tag,
                    m_oForteDocument.WordDocument, true);
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user deletion of oTag
        /// </summary>
        /// <param name="oTag"></param>
        /// <param name="oDeletedRange"></param>
        /// <param name="oSegment"></param>
        /// <param name="xFullTagID"></param>
        /// <param name="xElement"></param>
        private void RefreshBeforeTagDeletion_Design(Word.XMLNode oTag, Word.Range oDeletedRange,
            Segment oSegment, string xFullTagID, string xElement)
        {
            //get tags in user's selection - since this event fires from bottom
            //to top, m_xFirstTagInScope will tell us when we've hit all the
            //tags in the user's selection - m_xTagsInScope will allow us to
            //skip items that will be deleted along with parent
            if (string.IsNullOrEmpty(m_xFirstTagInScope))
            {
                if (oDeletedRange != null)
                {
                    m_xTagsInScope = LMP.Forte.MSWord.WordDoc.GetTagsInRange(oDeletedRange, "");
                    string[] xTags = m_xTagsInScope.Split('|');
                    string[] xFields = xTags[1].Split('¦');
                    m_xFirstTagInScope = xFields[0];
                }
                else
                {
                    //this can occur if a user deletes a tag by backspacing -
                    //initialize variables empty to avoid errors elsewhere
                    m_xTagsInScope = "";
                    m_xFirstTagInScope = "";
                }
            }

            //get tag id of segment
            string xSegmentTagID = "";
            if (oSegment != null)
                xSegmentTagID = oSegment.FullTagID;

            //determine whether to refresh immediately
            bool bDoNow = false;

            if (xElement == "mSEG")
            {
                //mSEG - move external children if necessary
                bool bHasExternalChildren = (oSegment.Nodes.GetItemObjectDataValue(
                    xSegmentTagID, "ExternalChildren").ToUpper() == "TRUE");
                if ((bHasExternalChildren) && (this.ForteDocument.Tags
                    .MoveExternalChildren(oTag) == 1))
                {
                    //there were external children in the tags collection of
                    //the mSEG being deleted - determine whether all parts of
                    //the segment will be deleted
                    if (!IsPartialDeletion(oSegment))
                    {
                        //external children will be orphaned -
                        //a full document refresh is required
                        m_bDocRefreshRequired = true;
                    }
                }

                //refresh now - it would take as long to determine
                //whether this is necessary as it does to just do it
                bDoNow = true;
            }
            else
            {
                if (xSegmentTagID == "")
                    //there is no containing segment -
                    //refresh now
                    bDoNow = true;
                else
                {
                    //refresh now if this mVar or mBlock is standalone or if the
                    //segment to which it belongs is not getting deleted
                    bool bIsNotTopLevel = xSegmentTagID.Contains(".");
                    bool bContainingSegInDelScope = 
                        m_xTagsInScope.Contains('|' + xSegmentTagID + '¦');

                    //refresh task pane only if the containing 
                    //segment might not be deleted
                    bDoNow = !(bContainingSegInDelScope && bIsNotTopLevel);

                    //TODO: currently, all vars/block top level node deletions will force
                    //DoNow = true.  This is true even when all content is being
                    //deleted from a segment (the last mSEG won't get deleted).  
                    //Better would be that the refresh
                    //is done once, when the last of the var/block nodes is deleted.
                }
            }

            //refresh now
            if (bDoNow)
            {
                //remove tag from collection
                m_oForteDocument.Tags.Delete(oTag);

                //refresh impacted MP objects and ui elements
                if ((xElement == "mSEG") && !m_bDocRefreshRequired)
                    //segment
                    RefreshBeforemSEGDeletion(oSegment);
                else if (xElement == "mVar")
                    //variable
                    RefreshBeforemVarDeletion(oSegment, oTag);
                else if (xElement == "mBlock")
                    //block
                    RefreshBeforemBlockDeletion(oSegment, oTag);

                //delete doc vars (12-23-10 dm)
                //GLOG 5662:  oTag.SelectSingleNode causes an Access violation here.
                //Cycle through Attribute Nodes instead
                Word.XMLNode oReserved = null;
                for (int c = 1; c <= oTag.Attributes.Count; c++)
                {
                    if (oTag.Attributes[c].BaseName == "Reserved")
                    {
                        oReserved = oTag.Attributes[c];
                        break;
                    }
                }
                if (oReserved != null)
                {
                    LMP.Forte.MSWord.WordDoc.DeleteRelatedObjects(oReserved.NodeValue,
                        m_oForteDocument.WordDocument, true);
                }
            }

            //handle last tag in cycle
            if (m_xFirstTagInScope == xFullTagID)
            {
                //refresh document if required
                if (m_bDocRefreshRequired)
                {
                    //refresh document
                    this.ForteDocument.Refresh(ForteDocument.RefreshOptions.None);
                }

                //reset flags
                ResetFlags();
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user deletion of
        /// all or part of oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        private void RefreshBeforemSEGDeletion(Segment oSegment)
        {
            string xSegmentTagID = oSegment.FullTagID;
            Segment oParent = oSegment.Parent;
            Segments oSegments = null;
            if (oParent != null)
            {
                //segment is part of parent's collection
                oSegments = oParent.Segments;

                //refresh parent's node store
                oParent.RefreshNodes();
            }
            else
                //segment is top-level
                oSegments = m_oForteDocument.Segments;

            //determine whether segment has other parts
            int iParts = oSegment.Nodes.GetItemPartsCount(xSegmentTagID);
            if (iParts > 1)
            {
                //there are other parts remaining - refresh segment
                oSegment.Refresh();

                //update this segment's tree node to reflect new
                //variable and block objects and remove the nodes for
                //the variables and blocks that were in this part
                if (this.Mode == ForteDocument.Modes.Design)
                    this.DocDesigner.UpdateTreeNodeTags(oSegment);
                else
                    this.DocEditor.UpdateTreeNodeTags(oSegment);
            }
            else
            {
                //10-21-11 (dm) - delete snapshot doc vars
                if (Snapshot.Exists(oSegment))
                    Snapshot.Delete(oSegment, false);

                //this is the only part - delete segment
                oSegments.Delete(xSegmentTagID);
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user deletion of
        /// an mVar belonging to oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oTag"></param>
        private void RefreshBeforemVarDeletion(Segment oSegment, Word.XMLNode oTag)
        {
            //get target variable and variables collection
            Variables oVariables = null;
            if (oSegment != null)
                oVariables = oSegment.Variables;
            else
                //oVariables = m_oMPDocument.Variables;
                return;

            Variable oVar = oVariables.ItemFromAssociatedTag(oTag);

            //delete variable only if this is its only associated
            //tag - otherwise, just refresh the node store
            if (oVar != null && oVar.AssociatedWordTags.Length == 1)
                //delete -
                //do not remove deleted scopes xml, because user may be moving
                //a block of text containing an mDel elsewhere within the segment or
                //may choose to undo the tag deletion
                oVariables.Delete(oVar, false, false, false);
            else if (oSegment != null)
                //refresh segment node store
                oSegment.RefreshNodes();
            else
                //refresh standalone node store
                m_oForteDocument.RefreshOrphanNodes();
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user deletion of
        /// an mVar belonging to oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oTag"></param>
        private void RefreshBeforemVarDeletion(Segment oSegment, Word.ContentControl oCC)
        {
            //get target variable and variables collection
            Variables oVariables = null;
            if (oSegment != null)
                oVariables = oSegment.Variables;
            else
                //oVariables = m_oMPDocument.Variables;
                return;

            Variable oVar = oVariables.ItemFromAssociatedContentControl(oCC);

            //delete variable only if this is its only associated
            //tag - otherwise, just refresh the node store
            if (oVar != null && oVar.AssociatedContentControls.Length == 1)
                //delete -
                //do not remove deleted scopes xml, because user may be moving
                //a block of text containing an mDel elsewhere within the segment or
                //may choose to undo the tag deletion
                oVariables.Delete(oVar, false, false, false);
            else if (oSegment != null)
                //refresh segment node store
                oSegment.RefreshNodes();
            else
                //refresh standalone node store
                m_oForteDocument.RefreshOrphanNodes();
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user deletion of
        /// an mBlock belonging to oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oTag"></param>
        private void RefreshBeforemBlockDeletion(Segment oSegment, Word.XMLNode oTag)
        {
            //get target block and blocks collection
            Blocks oBlocks = null;
            if (oSegment != null)
            {
                oBlocks = oSegment.Blocks;
                //else
                //    oBlocks = m_oMPDocument.Blocks;
                Block oBlock = oBlocks.ItemFromAssociatedTag(oTag);

                if (oBlock != null)
                {
                    //delete
                    oBlocks.Delete(oBlock, false);
                }
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui to reflect pending user deletion of
        /// an mBlock belonging to oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oTag"></param>
        private void RefreshBeforemBlockDeletion(Segment oSegment, Word.ContentControl oCC)
        {
            //get target block and blocks collection
            Blocks oBlocks = null;
            if (oSegment != null)
            {
                oBlocks = oSegment.Blocks;
                //else
                //    oBlocks = m_oMPDocument.Blocks;
                Block oBlock = oBlocks.ItemFromAssociatedContentControl(oCC);

                if (oBlock != null)
                {
                    //delete
                    oBlocks.Delete(oBlock, false);
                }
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui after user insertion of oTag
        /// </summary>
        /// <param name="oTag"></param>
        private void RefreshAfterTagInsertion(Word.XMLNode oTag, string xElement)
        {
            //add tag to collection
            //GLOG 6970 (dm) - in 10.5, Insert will err when pasting mSEGs because
            //this handler will fire for mVars, mBlocks, and mDels before the parent
            //mSEG is in the collection - we're accepting that we can no longer refresh
            //automatically, so just avoid error
            //GLOG 6970 (dm, 11/5/13) - after dealing with errors resulting from ignoring
            //this error, I realized that there's no reason to not just do a complete
            //refresh if an error occurs here
            LMP.Forte.MSWord.Tag oMPTag = null;
            try
            {
                oMPTag = m_oForteDocument.Tags.Insert(oTag, false);
            }
            catch (System.Exception oE)
            {
                if (m_oForteDocument.Mode == ForteDocument.Modes.Edit)
                {
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    m_oRefreshTimer.Start();
                    return;
                }
                else
                    throw oE;
            }

            //refresh impacted MP objects and ui elements
            if (xElement == "mSEG")
                //segment
                RefreshAftermSEGInsertion(oMPTag);
            else if (xElement == "mBlock")
                //block
                RefreshAftermBlockInsertion(oMPTag, false);
            else
                //variable
                RefreshAftermVarInsertion(oMPTag, false);
        }

        /// <summary>
        /// refreshes MacPac objects and ui after user insertion of oTag
        /// </summary>
        /// <param name="oTag"></param>
        private void RefreshAfterTagInsertion(Word.ContentControl oCC, string xElement)
        {
            //add tag to collection
            //GLOG 6970 (dm) - in 10.5, Insert_CC will err when pasting mSEGs because
            //this handler will fire for mVars, mBlocks, and mDels before the parent
            //mSEG is in the collection - we're accepting that we can no longer refresh
            //automatically, so just avoid error
            //GLOG 6970 (dm, 11/5/13) - after dealing with errors resulting from ignoring
            //this error, I realized that there's no reason to not just do a complete
            //refresh if an error occurs here
            LMP.Forte.MSWord.Tag oMPTag = null;
            try
            {
                oMPTag = m_oForteDocument.Tags.Insert_CC(oCC, false);
            }
            catch (System.Exception oE)
            {
                if (m_oForteDocument.Mode == ForteDocument.Modes.Edit)
                {
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    m_oRefreshTimer.Start();
                    return;
                }
                else
                    throw oE;
            }

            //refresh impacted MP objects and ui elements
            if (xElement == "mSEG")
                //segment
                RefreshAftermSEGInsertion(oMPTag);
            else if (xElement == "mBlock")
                //block
                RefreshAftermBlockInsertion(oMPTag, true);
            else
                //variable
                RefreshAftermVarInsertion(oMPTag, true);
        }

        /// <summary>
        /// refreshes MacPac objects and ui after user insertion of oTag
        /// </summary>
        /// <param name="oTag"></param>
        private void RefreshAftermSEGInsertion(LMP.Forte.MSWord.Tag oTag)
        {
            //parse tag id to get parent segment and target segments collection
            string xFullTagID = oTag.FullTagID;
            Segment oParent = null;
            Segments oSegments = null;
            string xParentID = "";
            
            int iPos = xFullTagID.LastIndexOf('.');
            if (iPos > -1)
            {
                //segment has parent
                xParentID = xFullTagID.Substring(0, iPos);
                oParent = m_oForteDocument.FindSegment(xParentID);
                oSegments = oParent.Segments;

                //refresh parent's node store
                oParent.RefreshNodes();
            }
            else
                //segment is top-level
                oSegments = m_oForteDocument.Segments;

            //determine whether the document already contains other
            //parts of the same segment
            Segment oSegment = null;
            try
            {
                oSegment = oSegments.ItemFromID(xFullTagID);
            }
            catch { }

            if (oSegment != null)
            {
                //refresh existing segment
                oSegment.Refresh();
            }
            else
            {
                //new tag is not a part of an existing segment - 
                //add new segment
                oSegment = m_oForteDocument.GetSegment(xFullTagID, m_oForteDocument);
                oSegments.Add(oSegment);

                //GLOG 3482 (3/20/09 dm) - authors will already be set at this
                //point if not linked to parent
                if ((this.Mode != ForteDocument.Modes.Design) &&
                    (oParent != null) && oSegment.LinkAuthorsToParent)
                    oSegment.Authors.SetAuthors(oParent.Authors);
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui after user insertion of oTag
        /// </summary>
        /// <param name="oTag"></param>
        /// <param name="bIsContentControl"></param>
        private void RefreshAftermVarInsertion(LMP.Forte.MSWord.Tag oTag, bool bIsContentControl)
        {
            //parse tag id to get parent segment and target variables collection
            string xFullTagID = oTag.FullTagID;
            Segment oSegment = null;
            Variables oVariables = null;
            string xTagID = "";
            string xSegmentID = "";
            bool bExistingVariables = false;

            //GLOG : 8085 : ceh - account for missing doc vars
            if (xFullTagID != null)
            {
                int iPos = xFullTagID.LastIndexOf('.');
                if (iPos > -1)
                {
                    //there's a parent segment
                    xTagID = xFullTagID.Substring(iPos + 1);
                    xSegmentID = xFullTagID.Substring(0, iPos);
                    oSegment = m_oForteDocument.FindSegment(xSegmentID);
                    bExistingVariables = oSegment.HasVariables(false);
                    oVariables = oSegment.Variables;
                }
            }

            //10-12-11 (dm) - also delete if parent segment is finished
            if ((oSegment == null) || Snapshot.Exists(oSegment))
            {
                //delete standalone
                if (bIsContentControl)
                    oTag.ContentControl.Delete(false);
                else
                    oTag.WordXMLNode.Delete();

                return;
                //xTagID = xFullTagID;

                //bExistingVariables = m_oMPDocument.HasVariables;
                //oVariables = m_oMPDocument.Variables;
                //bExistingVariables = false;
                //oVariables = null;
            }

            if ((!bExistingVariables) && (oVariables.Count == 1))
            {
                //a new variable was created in the process of
                //initializing the variables collection
                Variable oVar = oVariables.ItemFromIndex(0);

                //add to tree
                if (this.Mode == ForteDocument.Modes.Design)
                    this.DocDesigner.InsertNode(oVar);
                else if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                    Variable.ControlHosts.DocumentEditor)
                    this.DocEditor.InsertNode(oVar);
            }
            else if (oVariables.VariableExists(xTagID))
            {
                //variable already has another associated tag -
                //just refresh node store
                if (oSegment != null)
                    oSegment.RefreshNodes();
                else
                    m_oForteDocument.RefreshOrphanNodes();

                //remove TagExpandedValue field code from value source expression -
                //we don't support this functionality for multiple associated tags
                Variable oVar = oVariables.ItemFromName(xTagID);
                if (oVar.ValueSourceExpression.IndexOf("[TagExpandedValue") > -1)
                {
                    oVar.ValueSourceExpression = "";
                    oVariables.Save(oVar);
                }

                //GLOG 2622 (dm) - adjust multivalue tags if necessary
                string xValue = oVar.ValueSourceExpression;
                if ((this.Mode == ForteDocument.Modes.Edit) && oVar.IsMultiValue &&
                    xValue.Contains("[DetailValue"))
                {
                    if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        //reindex sub vars to ensure uniqueness
                        Word.XMLNode oTargetNode = oTag.WordXMLNode;
                        Word.XMLNode[] oNodes = oVar.AssociatedWordTags;
                        Word.XMLNode oPreviousNode = oNodes[oNodes.Length - 2];
                        LMP.Forte.MSWord.WordDoc.ReindexSubVars(oTargetNode, oPreviousNode);

                        //if there was previously only a single tag - we need to get
                        //everything back into one tag or table, since this is what
                        //InsertDetail() is expecting
                        if (oNodes.Length == 2)
                        {
                            //determine whether new tag is in a table
                            bool bInTable = (oTargetNode.Range.Tables.Count > 0);
                            if (bInTable)
                            {
                                //table - if original tag is empty, target it for deletion below
                                if (System.String.IsNullOrEmpty(oPreviousNode.Range.Text))
                                    oTargetNode = oPreviousNode;
                                else
                                    oTargetNode = null;
                            }
                            else
                            {
                                //not in table - get new detail value
                                string xDetailValue = LMP.Architect.Api.Variable.GetDetailValue(
                                    oVar, oTargetNode, null, true);

                                //we support this field code with or w/o the variable name
                                xValue = xValue.Replace("[DetailValue]", xDetailValue);
                                xValue = xValue.Replace("[DetailValue" + "__" + this.Name + "]",
                                    xDetailValue);
                                if (LMP.String.ContainsOnlySpaces(xValue))
                                    xValue = "";
                                xValue = Expression.Evaluate(xValue, oSegment, m_oForteDocument);

                                //set value
                                oVar.SetValue(xValue, false);
                            }

                            //delete target tag
                            if (oTargetNode != null)
                            {
                                try
                                {
                                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                                    this.ScreenUpdating = false;

                                    //delete target tag
                                    LMP.Forte.MSWord.Tags oTags = this.m_oForteDocument.Tags;
                                    oTags.Delete(oTargetNode);
                                    Word.Range oRange = oTargetNode.Range;

                                    //when the new tag is in a table, we're deleting the
                                    //original tag, so we need to preserve mDels
                                    string xmDels = null;
                                    if (bInTable)
                                    {
                                        xmDels = LMP.Forte.MSWord.WordDoc.GetmDels(oRange,
                                            ref oTags, -1, false, false, false, "");
                                    }

                                    //delete contents of Word tag
                                    object oMissing = System.Reflection.Missing.Value;
                                    oRange.Delete(ref oMissing, ref oMissing);

                                    //restore mDels if necessary
                                    if (!System.String.IsNullOrEmpty(xmDels))
                                    {
                                        LMP.Forte.MSWord.WordDoc.RestoremDels(oRange,
                                            xmDels, ref oTags);
                                    }

                                    //delete Word tag itself
                                    try
                                    {
                                        oTargetNode.Delete();
                                    }
                                    catch { }

                                    //delete paragraph if empty
                                    object oUnit = Word.WdUnits.wdParagraph;
                                    oRange.Expand(ref oUnit);
                                    if (oRange.End - oRange.Start == 1)
                                        oRange.Delete(ref oMissing, ref oMissing);

                                    //refresh node store
                                    if (oSegment != null)
                                        oSegment.RefreshNodes();
                                    else
                                        m_oForteDocument.RefreshOrphanNodes();

                                    //reinsert value
                                    if (!bInTable)
                                        oVar.VariableActions.Execute();
                                }
                                finally
                                {
                                    this.ScreenUpdating = true;
                                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                                }
                            }
                        }
                    }
                    else
                    {
                        //reindex sub vars to ensure uniqueness
                        Word.ContentControl oTargetCC = oTag.ContentControl;
                        Word.ContentControl[] oCCs = oVar.AssociatedContentControls;
                        Word.ContentControl oPreviousCC = oCCs[oCCs.Length - 2];

                        //GLOG 5180 (dm) - due to extra firing of the Word events
                        //when a cc is pasted into table, oPreviousCC may be null or
                        //have a null range here, even when nothing's ultimately
                        //wrong - if this is the case, there's no reason to proceed
                        if (oPreviousCC != null)
                        {
                            Word.Range oPreviousCCRange = null;
                            try
                            {
                                oPreviousCCRange = oPreviousCC.Range;
                            }
                            catch { }
                            if (oPreviousCCRange == null)
                                return;

                            //reindex
                            LMP.Forte.MSWord.WordDoc.ReindexSubVars_CC(oTargetCC, oPreviousCC);

                            //if there was previously only a single tag - we need to get
                            //everything back into one tag or table, since this is what
                            //InsertDetail() is expecting
                            if (oCCs.Length == 2)
                            {
                                //determine whether new tag is in a table
                                bool bInTable = (oTargetCC.Range.Tables.Count > 0);
                                if (bInTable)
                                {
                                    //table - if original tag is empty, target it for deletion below
                                    if (System.String.IsNullOrEmpty(oPreviousCCRange.Text))
                                        oTargetCC = oPreviousCC;
                                    else
                                        oTargetCC = null;
                                }
                                else
                                {
                                    //not in table - get new detail value
                                    string xDetailValue = LMP.Architect.Api.Variable.GetDetailValue(
                                        oVar, null, oTargetCC, true);

                                    //we support this field code with or w/o the variable name
                                    xValue = xValue.Replace("[DetailValue]", xDetailValue);
                                    xValue = xValue.Replace("[DetailValue" + "__" + this.Name + "]",
                                        xDetailValue);
                                    if (LMP.String.ContainsOnlySpaces(xValue))
                                        xValue = "";
                                    xValue = Expression.Evaluate(xValue, oSegment, m_oForteDocument);

                                    //set value
                                    oVar.SetValue(xValue, false);
                                }

                                //delete target tag
                                if (oTargetCC != null)
                                {
                                    try
                                    {
                                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                                        this.ScreenUpdating = false;

                                        //delete target tag
                                        LMP.Forte.MSWord.Tags oTags = this.m_oForteDocument.Tags;
                                        oTags.Delete_CC(oTargetCC);
                                        Word.Range oRange = oTargetCC.Range;

                                        //when the new tag is in a table, we're deleting the
                                        //original tag, so we need to preserve mDels
                                        string xmDels = null;
                                        if (bInTable)
                                        {
                                            xmDels = LMP.Forte.MSWord.WordDoc.GetmDels(oRange,
                                                ref oTags, -1, false, false, false, "");
                                        }

                                        //delete contents of Word tag
                                        object oMissing = System.Reflection.Missing.Value;
                                        oRange.Delete(ref oMissing, ref oMissing);

                                        //restore mDels if necessary
                                        if (!System.String.IsNullOrEmpty(xmDels))
                                        {
                                            LMP.Forte.MSWord.WordDoc.RestoremDels(oRange,
                                                xmDels, ref oTags);
                                        }

                                        //delete Word tag itself
                                        try
                                        {
                                            oTargetCC.Delete(false);
                                        }
                                        catch { }

                                        //delete paragraph if empty
                                        object oUnit = Word.WdUnits.wdParagraph;
                                        oRange.Expand(ref oUnit);
                                        if (oRange.End - oRange.Start == 1)
                                            oRange.Delete(ref oMissing, ref oMissing);

                                        //refresh node store
                                        if (oSegment != null)
                                            oSegment.RefreshNodes();
                                        else
                                            m_oForteDocument.RefreshOrphanNodes();

                                        //reinsert value
                                        if (!bInTable)
                                            oVar.VariableActions.Execute();
                                    }
                                    finally
                                    {
                                        this.ScreenUpdating = true;
                                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //create new variable - get definition from object data
                string xDef = "";
                string xObjectData = "";
                string xElementName = oTag.ElementName;
                if (xElementName == "mVar")
                    xObjectData = oTag.ObjectData;
                else if (oSegment != null)
                {
                    xObjectData = oSegment.Nodes.GetItemDeletedScopes(xSegmentID);
                }

                //10-20-10 (dm) - the delete scope xml doesn't contain the object
                //data for content controls - we need to go from the tag to the doc var
                bool bUseDocVar = ((xElementName == "mDel") && bIsContentControl);
                string xPattern = "";
                if (bUseDocVar)
                    xPattern = "¬w:sdtPr>¬w:tag w:val=\"mpv.*?\"";
                else
                    xPattern = @"VariableDefinition=.*?\|";
                Regex oRegex = new Regex(xPattern, RegexOptions.Singleline);
                MatchCollection oMatches = oRegex.Matches(xObjectData);
                foreach (Match oMatch in oMatches)
                {
                    string xMatch = oMatch.Value;
                    if (bUseDocVar)
                    {
                        //get object data from the associated doc var
                        string xTag = xMatch.Substring(23, 39);
                        xMatch = LMP.Forte.MSWord.WordDoc.GetAttributeValueFromTag(
                            xTag, "ObjectData");
                    }
                    if ((xMatch != null) && (xMatch.IndexOf('¦' + xTagID + '¦') > -1))
                    {
                        xDef = xMatch;
                        break;
                    }
                }

                if (xDef != "")
                {
                    //strip label
                    xDef = xDef.Substring(19);

                    //restore any pipe characters within the Variable definition
                    xDef = xDef.Replace(String.mpPipeReplacementChars, "|");

                    //get variable
                    Variable oVar = oVariables.GetVariableFromDefinition(xDef);

                    //set runtime properties
                    oVar.SegmentName = xSegmentID;
                    oVar.TagID = xFullTagID;
                    if (xElementName == "mVar")
                        oVar.TagType = ForteDocument.TagTypes.Variable;
                    else
                        oVar.TagType = ForteDocument.TagTypes.DeletedBlock;
                    string xPart = oTag.ParentPart;
                    if (xPart != null)
                        oVar.TagParentPartNumbers = xPart;

                    //10-7-10 (dm) - additional handling is needed for sub vars
                    string xValue = oVar.ValueSourceExpression;
                    if ((this.Mode == ForteDocument.Modes.Edit) && oVar.IsMultiValue &&
                        xValue.Contains("[DetailValue"))
                    {
                        //12-24-10 (dm) - implemented on binary side
                        if (bIsContentControl)
                            LMP.Forte.MSWord.WordDoc.UpdateSubVars_CC(oTag.ContentControl);
                        else
                            LMP.Forte.MSWord.WordDoc.UpdateSubVars(oTag.WordXMLNode);
                    }

                    //save - this will also take care of adding to the tree
                    oVariables.Save(oVar);
                }
            }
        }

        /// <summary>
        /// refreshes MacPac objects and ui after user insertion of oTag
        /// </summary>
        /// <param name="oTag"></param>
        /// <param name="bIsContentControl"></param>
        private void RefreshAftermBlockInsertion(LMP.Forte.MSWord.Tag oTag, bool bIsContentControl)
        {
            //parse tag id to get parent segment and target blocks collection
            string xFullTagID = oTag.FullTagID;
            Segment oSegment = null;
            Blocks oBlocks = null;
            string xSegmentID = "";

            int iPos = xFullTagID.LastIndexOf('.');
            if (iPos > -1)
            {
                //there's a parent segment
                xSegmentID = xFullTagID.Substring(0, iPos);
                oSegment = m_oForteDocument.FindSegment(xSegmentID);
                oBlocks = oSegment.Blocks;
            }

            //10-12-11 (dm) - also delete if parent segment is finished
            if ((oSegment == null) || Snapshot.Exists(oSegment))
            {
                //delete standalone
                if (bIsContentControl)
                    oTag.ContentControl.Delete(false);
                else
                    oTag.WordXMLNode.Delete();
                return;

                //oBlocks = m_oMPDocument.Blocks;
            }

            //get block from definition in object data
            Block oBlock = oBlocks.GetBlockFromDefinition(oTag.ObjectData);

            //set runtime properties
            oBlock.SegmentName = xSegmentID;
            oBlock.TagID = xFullTagID;
            oBlock.TagParentPartNumber = oTag.ParentPart;

            //save
            oBlocks.Save(oBlock);
        }

        /// <summary>
        /// prevents top level segment tag from being deleted
        /// called from OnXMLBeforeDeleteDesign
        /// </summary>
        /// <param name="oTag"></param>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool PreventTopLevelSegmentTagDeletion(ref Word.XMLNode oTag, Segment oSegment)
        {
            //allow deletion if tag is not a segment, segment is not
            //top level, or the segment has multiple parts
            if (oTag.BaseName != "mSEG" || oSegment.Parent != null ||
                oSegment.Nodes.GetItemPartsCount(oSegment.FullTagID) > 1)
                return false;

            //get screen updating state for later use
            bool bScreenUpdating = this.ScreenUpdating;

            try
            {
                //turn off screen updating and Word event handlers
                this.ScreenUpdating = false;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //prevent illegal deletions
                bool bPrevented = LMP.Forte.MSWord.WordDoc.PreventTagDeletion(ref oTag);

                //restore event handlers
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                if (bPrevented)
                {
                    //deletion was prevented -
                    //update tags collection and refresh node store
                    m_oForteDocument.Tags.Update(oTag, false, "", "");
                    if (oSegment != null)
                        oSegment.RefreshNodes();
                    else
                        m_oForteDocument.RefreshOrphanNodes();
                }

                return bPrevented;
            }
            finally
            {
                //restore screen updating setting
                this.ScreenUpdating = bScreenUpdating;
            }
        }

        /// <summary>
        /// prevents top level segment tag from being deleted
        /// called from OnContentControlBeforeDelete_Design
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool PreventTopLevelSegmentTagDeletion(ref Word.ContentControl oCC, Segment oSegment)
        {
            //allow deletion if tag is not a segment, segment is not
            //top level, or the segment has multiple parts
            if (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mSEG" ||
                oSegment.Parent != null ||
                oSegment.Nodes.GetItemPartsCount(oSegment.FullTagID) > 1)
                return false;

            //get screen updating state for later use
            bool bScreenUpdating = this.ScreenUpdating;

            try
            {
                //turn off screen updating and Word event handlers
                this.ScreenUpdating = false;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //prevent illegal deletions
                bool bPrevented = LMP.Forte.MSWord.WordDoc.PreventTagDeletion_CC(ref oCC);

                //restore event handlers
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                if (bPrevented)
                {
                    //deletion was prevented -
                    //update tags collection and refresh node store
                    m_oForteDocument.Tags.Update_CC(oCC, false, "", "");
                    if (oSegment != null)
                        oSegment.RefreshNodes();
                    else
                        m_oForteDocument.RefreshOrphanNodes();
                }

                return bPrevented;
            }
            finally
            {
                //restore screen updating setting
                this.ScreenUpdating = bScreenUpdating;
            }
        }

        /// prevents tag from being deleted when the selection suggests that this is
        /// not the user's intent or in the user's best interest
        /// </summary>
        /// <param name="oTag"></param>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool PreventAccidentalTagDeletion(ref Word.XMLNode oTag, Segment oSegment)
        {
            bool bScreenUpdating = this.ScreenUpdating;
            try
            {
                //turn off screen updating and Word event handlers
                this.ScreenUpdating = false;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                string xTempID = "";
                //assign temporary id
                //GLOG 4531: If error occurs attempting to add TempID, Tag may have already been
                //deleted by Word Mail Merge process.  Ignore error and return true to ignore this tag
                try
                {
                    xTempID = m_oForteDocument.Tags.AddTempID(oTag);
                }
                catch
                {
                    return true;
                }

                //prevent illegal deletions
                bool bRefreshRequired = false;

                //GLOG 5507 (dm) - simultaneous handlers were causing the
                //insertion of invalid replacement tags and resetting
                //IgnoreWordXMLEvents, leading to errors in the XMLAfterInsert handler
                m_bSuspendDeleteKeyHandler = true;

                bool bPrevented = LMP.Forte.MSWord.WordDoc.PreventAccidentalTagDeletion(
                    (m_iCurMode == ForteDocument.Modes.Design),
                    ref oTag, ref bRefreshRequired);

                //restore event handlers
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                //GLOG 3865 (dm) - reversed the order of the bRefreshRequired and
                //bPrevented branches to deal with situations where the parent tag
                //had to also be recreated
                if (bRefreshRequired)
                {
                    //backspace is deleting block tag(s) - since we can't accurately
                    //recreate the pre-deletion state, we allow the deletion to proceed
                    //and just guarantee by refreshing that we're aware of what occurred -
                    //a full refresh might also be required as per GLOG 3865
                    m_oRefreshTimer.Start();
                    bPrevented = true;
                }
                else if (bPrevented)
                {
                    //deletion was prevented -
                    //update tags collection and refresh node store
                    m_oForteDocument.Tags.Update(oTag, false, xTempID, "");
                    if (oSegment != null)
                        oSegment.RefreshNodes();
                    else
                        m_oForteDocument.RefreshOrphanNodes();
                }

                return bPrevented;
            }
            finally
            {
                //restore delete key handler
                m_bSuspendDeleteKeyHandler = false;

                //restore screen updating setting
                this.ScreenUpdating = bScreenUpdating;
            }
        }

        /// prevents tag from being deleted when the selection suggests that this is
        /// not the user's intent or in the user's best interest
        /// </summary>
        /// <param name="oTag"></param>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool PreventAccidentalTagDeletion(ref Word.ContentControl oCC, Segment oSegment)
        {
            bool bScreenUpdating = this.ScreenUpdating;
            try
            {
                //turn off screen updating and Word event handlers
                this.ScreenUpdating = false;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                string xTempID = "";
                //assign temporary id
                //GLOG 4531: If error occurs attempting to add TempID, Tag may have already been
                //deleted by Word Mail Merge process.  Ignore error and return true to ignore this tag
                try
                {
                    xTempID = m_oForteDocument.Tags.AddTempID_CC(oCC);
                }
                catch
                {
                    return true;
                }

                //prevent illegal deletions
                bool bRefreshRequired = false;
                bool bPrevented = LMP.Forte.MSWord.WordDoc.PreventAccidentalTagDeletion_CC(
                    (m_iCurMode == ForteDocument.Modes.Design),
                    ref oCC, ref bRefreshRequired);

                //restore event handlers
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                //GLOG 3865 (dm) - reversed the order of the bRefreshRequired and
                //bPrevented branches to deal with situations where the parent tag
                //had to also be recreated
                if (bRefreshRequired)
                {
                    //backspace is deleting block tag(s) - since we can't accurately
                    //recreate the pre-deletion state, we allow the deletion to proceed
                    //and just guarantee by refreshing that we're aware of what occurred -
                    //a full refresh might also be required as per GLOG 3865
                    m_oRefreshTimer.Start();
                    bPrevented = true;
                }
                else if (bPrevented)
                {
                    //deletion was prevented -
                    //update tags collection and refresh node store
                    m_oForteDocument.Tags.Update_CC(oCC, false, xTempID, "");
                    if (oSegment != null)
                        oSegment.RefreshNodes();
                    else
                        m_oForteDocument.RefreshOrphanNodes();
                }

                return bPrevented;
            }
            finally
            {
                //restore screen updating setting
                this.ScreenUpdating = bScreenUpdating;
            }
        }

        /// <summary>
        /// returns TRUE if xBaseName is an element in the MacPac10 schema
        /// </summary>
        /// <param name="xBaseName"></param>
        /// <returns></returns>
        private bool IsMacPaTag(string xBaseName)
        {
            switch (xBaseName)
            {
                case "mSEG":
                case "mVar":
                case "mBlock":
                case "mSubVar":
                case "mDel":
                case "mSecProps":
                case "mDocProps":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// handles Word XMLBeforeDelete event in edit mode
        /// </summary>
        /// <param name="DeletedRange"></param>
        /// <param name="OldXMLNode"></param>
        /// <param name="InUndoRedo"></param>
        private void OnXMLBeforeDelete_Edit(Word.Range DeletedRange,
            Word.XMLNode OldXMLNode, bool InUndoRedo)
        {
            //exit if tag is getting deleted programatically
            if ((ForteDocument.IgnoreWordXMLEvents &
                ForteDocument.WordXMLEvents.XMLBeforeDelete) > 0)
                return;

            //get element name
            string xElement = OldXMLNode.BaseName;

            //no handling required for non-MacPac tags or mSubVars
            if (!IsMacPaTag(xElement) || (xElement == "mSubVar"))
                return;

            //exit if tag doesn't belong to client -
            //TODO: this should be removed if we ever add support
            //for shared Forte functionality between clients
            if (!LMP.Forte.MSWord.WordDoc.TagIsAvailableToCurrentClient(OldXMLNode))
                return;

            //get full tag id
            string xFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(OldXMLNode);

            //exit if there's no tag id
            if (System.String.IsNullOrEmpty(xFullTagID))
                return;

            //get segment tag id
            string xSegmentTagID = "";
            if (xElement == "mSEG")
                xSegmentTagID = xFullTagID;
            else
            {
                //parse full tag id to get parent
                int iPos = xFullTagID.LastIndexOf('.');
                if (iPos != -1)
                    xSegmentTagID = xFullTagID.Substring(0, iPos);
            }

            Segment oSegment = null;
            string xParentSegmentID = "";
            string xParentVariableID = "";

            //get segment
            if (xSegmentTagID != "")
            {
                oSegment = m_oForteDocument.FindSegment(xSegmentTagID);
                //GLOG 4531: When performing Mail Merge, Word will strip Tags out of main
                //body of document, causing this event to be raised.  If we find mSEG corresponding to
                //TagID doesn't exist in the current MPDocuments Segments collection, exit immediately
                if (oSegment == null)
                    return;
                if (xElement == "mSEG")
                {
                    xParentVariableID = oSegment.ParentVariableID;
                    if (oSegment.Parent != null)
                        xParentSegmentID = oSegment.Parent.FullTagID;
                }
            }

            //prevent accidental tag deletion - exit if prevented
            if (!InUndoRedo && (PreventAccidentalTagDeletion(ref OldXMLNode, oSegment)))
                return;

            //refresh impacted MacPac objects and ui elements
            RefreshBeforeTagDeletion_Edit(OldXMLNode, DeletedRange, oSegment, xFullTagID, xElement);
            if (xParentVariableID != "" && xParentSegmentID != "")
            {
                Segment oParentSeg = m_oForteDocument.FindSegment(xParentSegmentID);
                oSegment = oParentSeg.Segments[xSegmentTagID];
                //Segment related to a Parent Variable has been deleted
                //Re-insert the mVar
                if (oSegment == null)
                {
                    Variable oVar = oParentSeg.Variables.ItemFromID(xParentVariableID);
                    Word.Range oRng = Session.CurrentWordApp.Selection.Range;
                    oRng.InsertParagraphBefore();
                    object oDirection = (object)Word.WdCollapseDirection.wdCollapseStart;
                    oRng.Collapse(ref oDirection);
                    if (oVar.SetTagState(true, false, oRng, true))
                        //Reset variable to default value
                        oVar.SetValue(oVar.DefaultValue);
                }
            }
        }

        /// <summary>
        /// handles Word ContentControlBeforeDelete event in edit mode
        /// </summary>
        /// <param name="OldCC"></param>
        /// <param name="InUndoRedo"></param>
        private void OnContentControlBeforeDelete_Edit(Word.ContentControl OldCC,
            bool InUndoRedo)
        {
            //exit if tag is getting deleted programatically
            if ((ForteDocument.IgnoreWordXMLEvents &
                ForteDocument.WordXMLEvents.XMLBeforeDelete) > 0)
                return;

            //get element name
            string xElement = LMP.String.GetBoundingObjectBaseName(OldCC.Tag);

            //no handling required for non-MacPac tags or mSubVars
            if (!IsMacPaTag(xElement) || (xElement == "mSubVar"))
                return;

            //exit if tag doesn't belong to client -
            //TODO: this should be removed if we ever add support
            //for shared Forte functionality between clients
            if (!LMP.Forte.MSWord.WordDoc.ControlIsAvailableToCurrentClient(OldCC))
                return;

            //get full tag id
            string xFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(OldCC);

            //exit if there's no tag id
            if (System.String.IsNullOrEmpty(xFullTagID))
                return;

            //get segment tag id
            string xSegmentTagID = "";
            if (xElement == "mSEG")
                xSegmentTagID = xFullTagID;
            else
            {
                //parse full tag id to get parent
                int iPos = xFullTagID.LastIndexOf('.');
                if (iPos != -1)
                    xSegmentTagID = xFullTagID.Substring(0, iPos);
            }

            Segment oSegment = null;
            string xParentSegmentID = "";
            string xParentVariableID = "";

            //get segment
            if (xSegmentTagID != "")
            {
                oSegment = m_oForteDocument.FindSegment(xSegmentTagID);
                //GLOG 4531: When performing Mail Merge, Word will strip Tags out of main
                //body of document, causing this event to be raised.  If we find mSEG corresponding to
                //TagID doesn't exist in the current MPDocuments Segments collection, exit immediately
                if (oSegment == null)
                    return;
                if (xElement == "mSEG")
                {
                    xParentVariableID = oSegment.ParentVariableID;
                    if (oSegment.Parent != null)
                        xParentSegmentID = oSegment.Parent.FullTagID;
                }
            }

            //prevent accidental tag deletion - exit if prevented
            if (!InUndoRedo && (PreventAccidentalTagDeletion(ref OldCC, oSegment)))
                return;

            //refresh impacted MacPac objects and ui elements
            RefreshBeforeTagDeletion_Edit(OldCC, oSegment, xFullTagID, xElement);
            if (xParentVariableID != "" && xParentSegmentID != "")
            {
                Segment oParentSeg = m_oForteDocument.FindSegment(xParentSegmentID);
                oSegment = oParentSeg.Segments[xSegmentTagID];
                //Segment related to a Parent Variable has been deleted
                //Re-insert the mVar
                if (oSegment == null)
                {
                    Variable oVar = oParentSeg.Variables.ItemFromID(xParentVariableID);
                    Word.Range oRng = Session.CurrentWordApp.Selection.Range;
                    oRng.InsertParagraphBefore();
                    object oDirection = (object)Word.WdCollapseDirection.wdCollapseStart;
                    oRng.Collapse(ref oDirection);
                    if (oVar.SetTagState(true, false, oRng, true))
                        //Reset variable to default value
                        oVar.SetValue(oVar.DefaultValue);
                }
            }
        }

        /// <summary>
        /// handles Word ContentControlBeforeDelete event in design mode
        /// </summary>
        /// <param name="OldCC"></param>
        /// <param name="InUndoRedo"></param>
        private void OnContentControlBeforeDelete_Design(Word.ContentControl OldCC,
            bool InUndoRedo)
        {
            //exit if tag is getting deleted programatically
            if ((ForteDocument.IgnoreWordXMLEvents &
                ForteDocument.WordXMLEvents.XMLBeforeDelete) > 0)
                return;

            //get element name
            string xElement = LMP.String.GetBoundingObjectBaseName(OldCC.Tag);

            //no handling required for non-MacPac tags or mSubVars
            if (!IsMacPaTag(xElement) || (xElement == "mSubVar"))
                return;

            //get full tag id
            string xFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(OldCC);

            //exit if there's no tag id
            if (xFullTagID == null || xFullTagID == "")
                return;

            //get segment tag id
            string xSegmentTagID = "";
            if (xElement == "mSEG")
                xSegmentTagID = xFullTagID;
            else
            {
                //parse full tag id to get parent
                int iPos = xFullTagID.LastIndexOf('.');
                if (iPos != -1)
                    xSegmentTagID = xFullTagID.Substring(0, iPos);
            }

            //get segment
            Segment oSegment = null;
            if (xSegmentTagID != "")
                oSegment = m_oForteDocument.FindSegment(xSegmentTagID);

            bool bTopLevelSegmentDeletionPrevented = false;
            //prevent deletion of top level containing segment
            if (xElement == "mSEG")
            {
                bTopLevelSegmentDeletionPrevented = PreventTopLevelSegmentTagDeletion(
                    ref OldCC, oSegment);
                if (!InUndoRedo && bTopLevelSegmentDeletionPrevented)
                {
                    //we're on the last call to this handler; everything has been 
                    //deleted and refreshed so we can refresh UI
                    //if (m_xFirstTagInScope == xFullTagID)
                        return;
                }
            }

            //5-24-11 (dm) - on the content controls side, we're only preventing
            //the deletion of mDels - if there are mDels in design mode, something is
            //wrong, so this code was only preventing troubleshooters from cleaning up
            ////prevent accidental tag deletion - exit if prevented
            //if (!InUndoRedo && (PreventAccidentalTagDeletion(ref OldCC, oSegment)))
            //    return;

            //refresh impacted MacPac objects and ui elements
            RefreshBeforeTagDeletion_Design(OldCC, oSegment, xFullTagID, xElement);
            //Refresh if top-level segment was recreated
            if (bTopLevelSegmentDeletionPrevented)
                this.DocDesigner.RefreshTree();
        }

        /// <summary>
        /// handles Word XMLAfterInsert event in edit mode
        /// </summary>
        /// <param name="NewXMLNode"></param>
        /// <param name="InUndoRedo"></param>
        private void OnXMLAfterInsert_Edit(Microsoft.Office.Interop.Word.XMLNode NewXMLNode,
            bool InUndoRedo)
        {
            //exit if tag is getting inserted programatically
            if ((ForteDocument.IgnoreWordXMLEvents &
                ForteDocument.WordXMLEvents.XMLAfterInsert) > 0)
                return;

            //Prevent firing event in Word 2003 if it's been incorrectly triggered by 
            //switching between header/footer stories.  In this case, story of activated node
            //will not match story of current selection
            //if (m_bXMLAfterInsertHandledForMouseDrag &&
            //    (NewXMLNode.Range.StoryType != Session.CurrentWordApp.Selection.StoryType))
            //    return;
            //GLOG 3810 (dm) - extended the story validation to all the time in Word 2003 -
            //excluded tags in the text frame story because these never incorrectly trigger
            //the event and may be encountered when the selection is properly in the
            //story containing the text box
            Word.Application oWord = Session.CurrentWordApp;
            Word.WdStoryType oStoryType = NewXMLNode.Range.StoryType;
            if (m_bInsertionIsInvalid)
            {
                //the insertion has already been found to be invalid
                string xFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(NewXMLNode);
                if (xFullTagID == m_xLastTagInScope)
                    //last tag in insertion - start timer
                    m_oInsertionUndoTimer.Start();
                return;
            }

            //no handling required for non-MacPac tags
            string xElement = NewXMLNode.BaseName;
            if (!IsMacPaTag(xElement))
                return;

            //exit if tag doesn't belong to client -
            //TODO: this should be removed if we ever add support
            //for shared Forte functionality between clients
            if (!LMP.Forte.MSWord.WordDoc.TagIsAvailableToCurrentClient(NewXMLNode))
                return;

            //validate
            if (!InUndoRedo)
            {
                //GLOG 7153: Shouldn't ever reach this code for Word 2013, but just in case
                //added check since XMLSchemaViolations property is not supported
                if ((LMP.Forte.MSWord.WordApp.Version < 15) && (m_oForteDocument.WordDocument.XMLSchemaViolations.Count > 0) &&
                    (xElement == "mSEG") || (xElement == "mBlock") || (xElement == "mVar"))
                {
                    //the insertion violates the schema - disallow entirely if an mSEG,
                    //mBlock, or mVar is being pasted inside an mVar, mSubVar, or mDel
                    if (LMP.Forte.MSWord.WordDoc.DisallowPaste(NewXMLNode))
                    {
                        this.ScreenUpdating = false;
                        m_bInsertionIsInvalid = true;
                        m_xTagsInScope = LMP.Forte.MSWord.WordDoc.GetTagsInInsertion(NewXMLNode, "");
                        string[] xTags = m_xTagsInScope.Split('|');
                        int iCount = xTags.Length - 1;
                        if (iCount > 1)
                        {
                            //this is the first of multiple tags that were inserted - we'll
                            //start the undo timer when we hit the last tag
                            string[] xFields = xTags[iCount].Split('¦');
                            m_xLastTagInScope = xFields[0];
                        }
                        else
                        {
                            //this is the only tag that was inserted - start timer immediately
                            m_oInsertionUndoTimer.Start();
                        }
                        return;
                    }
                }

                if (LMP.Forte.MSWord.WordDoc.TagIsInvalid(NewXMLNode))
                {
                    //the tag itself is invalid - delete immediately
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    NewXMLNode.Delete();
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    return;
                }
            }

            //refresh impacted MacPac objects and ui elements
            if (xElement != "mSubVar")
            {
                //12-23-10 (dm) - add associated doc vars
                LMP.Forte.MSWord.WordDoc.OnXMLNodeAfterInsert(NewXMLNode);

                RefreshAfterTagInsertion(NewXMLNode, xElement);
            }
        }

        /// <summary>
        /// handles Word ContentControlAfterAdd event
        /// </summary>
        /// <param name="NewCC"></param>
        /// <param name="InUndoRedo"></param>
        private void OnContentControlAfterAdd(Word.ContentControl oNewCC, bool InUndoRedo)
        {
            //exit if tag is getting inserted programatically
            if ((ForteDocument.IgnoreWordXMLEvents &
                ForteDocument.WordXMLEvents.XMLAfterInsert) > 0 || LMP.MacPac.Application.Paused)
                return;
            if (m_bInsertionIsInvalid)
            {
                //the insertion has already been found to be invalid -
                //10-7-10 (dm) - modified to use tag, rather than tag id - the latter
                //won't be available if oNewCC was copied from adifferent document
                if (oNewCC.Tag == m_xLastTagInScope)
                    //last tag in insertion - start timer
                    m_oInsertionUndoTimer.Start();
                return;
            }

            //no handling required for non-MacPac tags
            string xElement = LMP.String.GetBoundingObjectBaseName(oNewCC.Tag);
            if (!IsMacPaTag(xElement))
                return;

            //exit if tag doesn't belong to client -
            //TODO: this should be removed if we ever add support
            //for shared Forte functionality between clients
            if ((this.Mode != ForteDocument.Modes.Design) &&
                !LMP.Forte.MSWord.WordDoc.ControlIsAvailableToCurrentClient(oNewCC))
                return;

            //validate
            if (!InUndoRedo)
            {
                if ((xElement == "mSEG") || (xElement == "mBlock") || (xElement == "mVar"))
                {
                    //disallow if an mSEG, mBlock, or mVar is being pasted
                    //into an mVar, mSubVar, or mDel
                    if (LMP.Forte.MSWord.WordDoc.DisallowPaste_CC(oNewCC))
                    {
                        this.ScreenUpdating = false;
                        m_bInsertionIsInvalid = true;
                        m_xTagsInScope = LMP.Forte.MSWord.WordDoc.GetContentControlsInInsertion(oNewCC, "");
                        string[] xTags = m_xTagsInScope.Split('|');
                        int iCount = xTags.Length - 1;
                        if (iCount > 1)
                        {
                            //this is the first of multiple tags that were inserted - we'll
                            //start the undo timer when we hit the last tag
                            string[] xFields = xTags[iCount].Split('¦');
                            m_xLastTagInScope = xFields[0];
                        }
                        else
                        {
                            //this is the only tag that was inserted - start timer immediately
                            m_oInsertionUndoTimer.Start();
                        }
                        return;
                    }
                }

                if (LMP.Forte.MSWord.WordDoc.ContentControlIsInvalid(oNewCC))
                {
                    //the tag itself is invalid - delete immediately
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    oNewCC.Delete(false);
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    return;
                }
            }

            //refresh impacted MacPac objects and ui elements
            if (xElement != "mSubVar")
            {
                //update tag, doc vars, and bookmark
                LMP.Forte.MSWord.WordDoc.OnContentControlAfterAdd(oNewCC);

                //refresh
                RefreshAfterTagInsertion(oNewCC, xElement);
            }
        }

        /// <summary>
        /// handles Word XMLBeforeDelete event in design mode
        /// </summary>
        /// <param name="DeletedRange"></param>
        /// <param name="OldXMLNode"></param>
        /// <param name="InUndoRedo"></param>
        private void OnXMLBeforeDelete_Design(Word.Range DeletedRange,
            Word.XMLNode OldXMLNode, bool InUndoRedo)
        {
            //exit if tag is getting deleted programatically
            if ((ForteDocument.IgnoreWordXMLEvents &
                ForteDocument.WordXMLEvents.XMLBeforeDelete) > 0)
                return;

            //get element name
            string xElement = OldXMLNode.BaseName;

            //no handling required for non-MacPac tags or mSubVars
            if (!IsMacPaTag(xElement) || (xElement == "mSubVar"))
                return;

            //get full tag id
            string xFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(OldXMLNode);

            //exit if there's no tag id
            if (xFullTagID == null || xFullTagID == "")
                return;

            //get segment tag id
            string xSegmentTagID = "";
            if (xElement == "mSEG")
                xSegmentTagID = xFullTagID;
            else
            {
                //parse full tag id to get parent
                int iPos = xFullTagID.LastIndexOf('.');
                if (iPos != -1)
                    xSegmentTagID = xFullTagID.Substring(0, iPos);
            }

            //get segment
            Segment oSegment = null;
            if (xSegmentTagID != "")
                oSegment = m_oForteDocument.FindSegment(xSegmentTagID);

            bool bTopLevelSegmentDeletionPrevented = false;
            //prevent deletion of top level containing segment
            if (xElement == "mSEG")
            {
                bTopLevelSegmentDeletionPrevented = PreventTopLevelSegmentTagDeletion(ref OldXMLNode, oSegment);
                if (!InUndoRedo && bTopLevelSegmentDeletionPrevented)
                {
                    //we're on the last call to this handler; everything has been 
                    //deleted and refreshed so we can refresh UI
                    if (m_xFirstTagInScope == xFullTagID)
                        return;
                }
            }
            
            //prevent accidental tag deletion - exit if prevented
            if (!InUndoRedo && (PreventAccidentalTagDeletion(ref OldXMLNode, oSegment)))
                return;

            //refresh impacted MacPac objects and ui elements
            RefreshBeforeTagDeletion_Design(OldXMLNode, DeletedRange, oSegment, xFullTagID, xElement);
            //Refresh if top-level segment was recreated
            if (bTopLevelSegmentDeletionPrevented)
                this.DocDesigner.RefreshTree();
        }

        /// <summary>
        /// handles Word XMLAfterInsert event in design mode
        /// </summary>
        /// <param name="NewXMLNode"></param>
        /// <param name="InUndoRedo"></param>
        private void OnXMLAfterInsert_Design(Word.XMLNode NewXMLNode,
            bool InUndoRedo)
        {
            //exit if tag is getting inserted programatically
            if ((ForteDocument.IgnoreWordXMLEvents &
                ForteDocument.WordXMLEvents.XMLAfterInsert) > 0)
                return;

            //GLOG 3810 (dm) - skip existing header/footer tags that have incorrectly
            //triggered XMLAfterInsert due to the known native bug in Word 2003
            Word.Application oWord = Session.CurrentWordApp;
            Word.WdStoryType oStoryType = NewXMLNode.Range.StoryType;
            if (m_bInsertionIsInvalid)
            {
                //the insertion has already been found to be invalid
                string xFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(NewXMLNode);
                if (xFullTagID == m_xLastTagInScope)
                    //last tag in insertion - start timer
                    m_oInsertionUndoTimer.Start();
                return;
            }

            //no handling required for non-MacPac tags
            string xElement = NewXMLNode.BaseName;
            if (!IsMacPaTag(xElement))
                return;

            //validate
            if (!InUndoRedo)
            {
                //GLOG 7153: XMLSchemaViolations property not supported in Word 2013
                if ((LMP.Forte.MSWord.WordApp.Version < 15) && (m_oForteDocument.WordDocument.XMLSchemaViolations.Count > 0) &&
                    (xElement == "mSEG") || (xElement == "mBlock") || (xElement == "mVar"))
                {
                    //the insertion violates the schema - disallow entirely if an mSEG,
                    //mBlock, or mVar is being pasted inside an mVar, mSubVar, or mDel
                    if (LMP.Forte.MSWord.WordDoc.DisallowPaste(NewXMLNode))
                    {
                        this.ScreenUpdating = false;
                        m_bInsertionIsInvalid = true;
                        m_xTagsInScope = LMP.Forte.MSWord.WordDoc.GetTagsInInsertion(NewXMLNode, "");
                        string[] xTags = m_xTagsInScope.Split('|');
                        int iCount = xTags.Length - 1;
                        if (iCount > 1)
                        {
                            //this is the first of multiple tags that were inserted - we'll
                            //start the undo timer when we hit the last tag
                            string[] xFields = xTags[iCount].Split('¦');
                            m_xLastTagInScope = xFields[0];
                        }
                        else
                        {
                            //this is the only tag that was inserted - start timer immediately
                            m_oInsertionUndoTimer.Start();
                        }
                        return;
                    }
                }

                if (LMP.Forte.MSWord.WordDoc.TagIsInvalid(NewXMLNode))
                {
                    //the tag itself is invalid - delete immediately
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    NewXMLNode.Delete();
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                    return;
                }
            }

            //refresh impacted MacPac objects and ui elements
            if (xElement != "mSubVar")
            {
                //12-23-10 (dm) - add associated doc vars
                LMP.Forte.MSWord.WordDoc.OnXMLNodeAfterInsert(NewXMLNode);

                RefreshAfterTagInsertion(NewXMLNode, xElement);
            }
        }

        private void ResetFlags()
        {
            m_xTagsInScope = null;
            m_xFirstTagInScope = null;
            m_xLastTagInScope = null;
            m_bDocRefreshRequired = false;
            m_bInsertionIsInvalid = false;
        }
        /// <summary>
        /// Method hooked using 
        /// </summary>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        public static int MouseHookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode < 0)
            {
                return CallNextHookEx(iMouseHook, nCode, wParam, lParam);
            }
            else
            {
                try
                {
                    TaskPane oActiveTaskPane = null;
                    switch (wParam.ToInt32())
                    {
                        case WM_LBUTTONDOWN:
                            //GLOG 3721: Don't start drag if context menu is displayed
                            //GLOG 7814: Referencing ActiveDocument here could freeze Word screen
                            oActiveTaskPane = LMP.MacPac.Application.ActiveTaskPane;
                            if (oActiveTaskPane == null || (oActiveTaskPane != null && !oActiveTaskPane.MenuIsVisible()))
                            {
                                m_bMouseButtonDown = true;
                                m_bMoveStartedFromTaskPane = false; //GLOG 8388
                                m_bMouseIsMoving = true;
                                HandleMouseDrag(false);
                            }
                            break;
                        case WM_LBUTTONUP:
                            m_bMouseButtonDown = false;
                            m_bMoveStartedFromTaskPane = false; //GLOG 8388
                            m_bMouseIsMoving = false;
                            break;
                        case WM_MOUSEMOVE:
                            //If Document Editor is active,
                            //GLOG 7814: Referencing ActiveDocument here could freeze Word screen
                            oActiveTaskPane = LMP.MacPac.Application.ActiveTaskPane;
                            if (oActiveTaskPane != null && oActiveTaskPane.Mode == ForteDocument.Modes.Edit)
                            {
                                //Move message may be sent even if mouse is static
                                //Check if cursor has really moved before changing callout status
                                Point oPt = Cursor.Position;
                                if (oPt.X != oLastPoint.X || oPt.Y != oLastPoint.Y)
                                {
                                    oActiveTaskPane.DocEditor.ShowHideCalloutIfNecessary();

                                    //GLOG 8388 - JTS 4/29/16: Don't update variable value if mouse has moved
									//out of Taskpane and back in without releasing or clicking
                                    if (!m_bMoveStartedFromTaskPane)
                                    {
                                        m_bMoveStartedFromTaskPane = oActiveTaskPane.ClientRectangle.Contains(oActiveTaskPane.PointToClient(oPt));
                                        //the code to switch focus was removed because it was doing more harm than good -
                                        //see GLOG item 2601
                                        if ( m_bMoveStartedFromTaskPane &&
                                            !oActiveTaskPane.ClientRectangle.Contains(oActiveTaskPane.PointToClient(oLastPoint)))
                                        {
                                            //Mouse has moved from outside of into TaskPane
                                            oActiveTaskPane.DocEditor.UpdateCurrentVariableValueFromDocument();
                                        }
                                    }
                                }
                                oLastPoint = oPt;
                            }
                            //GLOG 3721: if Context menu has been displayed, cancel any ongoing MouseDrag event
                            if (m_bMouseButtonDown && (oActiveTaskPane == null ||
                                (oActiveTaskPane != null && !oActiveTaskPane.MenuIsVisible())))
                            {
                                m_bMouseIsMoving = true;
                                // Subscribe to XMLAfterInsert while drag is occurring
                                HandleMouseDrag(true);
                            }
                            else
                            {
                                m_bMouseIsMoving = false;
                                HandleMouseDrag(false);
                            }
                            break;
                        case WM_RBUTTONDOWN:
                            m_bMoveStartedFromTaskPane = false; //GLOG 8388
                            //Application.SendMessageToTaskPane(LMP.MacPac.Session.CurrentWordApp.ActiveDocument,
                            //    Application.mpPseudoWordEvents.FloatingFormRequested);

                            //turn off handling if any other button is pressed
                            HandleMouseDrag(false);
                            break;
                        case WM_MBUTTONDOWN:
                            m_bMoveStartedFromTaskPane = false; //GLOG 8388
                            //turn off handling if any other button is pressed
                            HandleMouseDrag(false);
                            break;
                    }
                }
                catch { }
                return CallNextHookEx(iMouseHook, nCode, wParam, lParam);
            }
        }

        public static int KeyboardHookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode < 0)
            {
                return CallNextHookEx(iKeyboardHook, nCode, wParam, lParam);
            }

            try
            {
                int iFlags = (int)lParam;
                char keycode = (char)wParam;

                //determine whether key is going up
                bool bUp = (iFlags & KF_UP) != 0;

                bool bAltPressed = (iFlags & KF_ALTDOWN) != 0;

                if (keycode == (char)Keys.ControlKey)
                    m_bCtrlPressed = !bUp;

                if (keycode == (char)Keys.ShiftKey)
                    m_bShiftPressed = !bUp;

                if ((keycode == '.') || (keycode == '\b'))
                {
                    //delete or backspace key - warn user if necessary -
                    //GLOG 4064 - don't handle if alt, ctrl, or shift also pressed,
                    //so as not to override native shortcuts
                    if ((!bUp) && (!bAltPressed) && (!m_bCtrlPressed) && (!m_bShiftPressed))
                    {
                        //key down
                        Word.Application oWord = Session.CurrentWordApp;
                        if (oWord == null)
                            oWord = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
                        Word.Document oDoc = oWord.ActiveDocument;

                        //GLOG 3969 (dm) - we decided not to intervene if user has tags showing
                        //10-7-11 (dm) - only handle if there's unfinished content
                        //GLOG 8247 (dm) - limit handling to .doc documents
                        if ((oDoc != null) && (oDoc.SaveFormat == 0) &&
                            (oDoc.ActiveWindow.View.ShowXMLMarkup == 0) &&
                            (oDoc.ProtectionType == Word.WdProtectionType.wdNoProtection) &&
                            LMP.MacPac.Application.SendMessageToTaskPane(oDoc,
                            LMP.MacPac.Application.mpPseudoWordEvents.QueryInputFocus) &&
                            LMP.MacPac.Application.SendMessageToTaskPane(oDoc,
                            LMP.MacPac.Application.mpPseudoWordEvents.QueryUnfinishedContent))
                        {
                            //GLOG 5667 (dm) - don't intervene when backspacing
                            //into a Word field
                            //GLOG 5720 (dm) - or when deleting immediately after one
                            //10-7-11 (dm) - made this block conditional on first meeting
                            //other criteria for intervention
                            bool bCursorInField = false;
                            try
                            {
                                if (oWord.Selection.Type == Word.WdSelectionType.wdSelectionIP)
                                {
                                    object oUnit = Word.WdUnits.wdCharacter;
                                    object oCount = 1;
                                    Word.Range oRng = oWord.Selection.Previous(ref oUnit, ref oCount);
                                    bCursorInField = ((oRng != null) && (oRng.Fields.Count > 0));
                                }
                            }
                            catch { }

                            if (!bCursorInField)
                            {
                                //runtime document has input focus - send message to TaskPane
                                if (keycode == '.')
                                {
                                    //delete
                                    LMP.MacPac.Application.SendMessageToTaskPane(oDoc,
                                        LMP.MacPac.Application.mpPseudoWordEvents.DeleteKeyPressed);
                                }
                                else
                                {
                                    //backspace
                                    LMP.MacPac.Application.SendMessageToTaskPane(oDoc,
                                        LMP.MacPac.Application.mpPseudoWordEvents.BackspaceKeyPressed);
                                }

                                //cancel key
                                return 1;
                            }
                        }
                    }
                }
            }
            catch { }
            return CallNextHookEx(iKeyboardHook, nCode, wParam, lParam);
        }

        /// <summary>
        /// Sends message to current TaskPane to subscribe to XMLAfterInsert event
        /// after mouse drag starts, then unsubscribe after mouse button is released
        /// and event has been handled
        /// </summary>
        /// <param name="bSubscribe"></param>
        static void HandleMouseDrag(bool bSubscribe)
        {
            //GLOG 7814 (dm) - this code is unnecessary and causes the screen to freeze in Word 2013
            if (LMP.MacPac.Session.CurrentWordVersion > 14)
                return;

            //Make sure we only subscribe once
            if (bSubscribe && !m_bXMLAfterInsertHandledForMouseDrag)
            {
                //get initial state of var
                bool bInitialState = m_bXMLAfterInsertHandledForMouseDrag;

                m_bXMLAfterInsertHandledForMouseDrag = true;

                bool bMessageSent = Application.SendMessageToTaskPane(
                    Session.CurrentWordApp.ActiveDocument, 
                    Application.mpPseudoWordEvents.MouseDragStart);

                if (!bMessageSent)
                    //message wasn't sent to task pane
                    m_bXMLAfterInsertHandledForMouseDrag = bInitialState;
            }
            //If event is currently being handled, unsubscribe
            else if (!bSubscribe && m_bXMLAfterInsertHandledForMouseDrag)
            {
                bool bMessageSent = Application.SendMessageToTaskPane(
                    Session.CurrentWordApp.ActiveDocument, 
                    Application.mpPseudoWordEvents.MouseDragComplete);

                if(bMessageSent)
                    //message was sent to task pane
                    m_bXMLAfterInsertHandledForMouseDrag = false;
            }
        }

        /// <summary>
        /// the XMLAfterInsert event isn't raised for the tags contained in a
        /// pasted textbox - to address this shortcoming, we count the shapes
        /// in the story before and after the paste, and call our XMLAfterInsert
        /// handlers directly for any newly added tags
        /// </summary>
        /// <param name="InUndoRedo"></param>
        private void HandleInsertedShapes(bool InUndoRedo)
        {
            //get shapes collection for selection story type

            Word.Shapes oShapes = LMP.Forte.MSWord.WordDoc.GetShapesInSelectionStory();
            int iShapes = oShapes.Count;

            if (iShapes > m_iExistingShapes)
            {
                //one or more textboxes has been inserted
                for (int i = m_iExistingShapes + 1; i <= iShapes; i++)
                {
                    //get new shape if accessible
                    object oIndex = (object)i;
                    Word.Shape oShape = null;
                    try
                    {
                        oShape = oShapes.get_Item(ref oIndex);
                    }
                    catch { }

                    if (oShape != null)
                    {
                        //shape is accessible - refresh for contained tags
                        if (LMP.Forte.MSWord.WordDoc.ShapeType(oShape) == 6) // Microsoft.Office.Core.MsoShapeType.msoGroup
                        {
                            //grouped
                            foreach (Word.Shape oGroupItem in oShape.GroupItems)
                                this.RefreshAfterShapeInsertion(oGroupItem, InUndoRedo);
                        }
                        else
                            //ungrouped
                            this.RefreshAfterShapeInsertion(oShape, InUndoRedo);
                    }
                }
            }
        }

        /// <summary>
        /// calls appropriate handler for each tag contained in the specified shape
        /// </summary>
        /// <param name="oShape"></param>
        /// <param name="InUndoRedo"></param>
        private void RefreshAfterShapeInsertion(Word.Shape oShape, bool InUndoRedo)
        {
            Word.TextFrame oFrame = oShape.TextFrame;
            if (oFrame.HasText == -1)
            {
                foreach (Word.XMLNode oTag in oFrame.TextRange.XMLNodes)
                {
                    if (this.Mode == ForteDocument.Modes.Design)
                    {
                        //design mode
                        OnXMLAfterInsert_Design(oTag, InUndoRedo);
                    }
                    else
                    {
                        //edit mode
                        OnXMLAfterInsert_Edit(oTag, InUndoRedo);
                    }
                }
            }
        }

        /// <summary>
        /// handles mpPseudoWordEvents.DeleteKeyPressed or
        /// mpPseudoWordEvents.BackspaceKeyPressed
        /// </summary>
        /// <param name="oMsg"></param>
        private void HandleDeleteKeyPressed(Application.mpPseudoWordEvents oMsg)
        {
            //GLOG 5507 (dm) - prevent code from running during
            //PreventAccidentalTagDeletion()
            if (m_bSuspendDeleteKeyHandler)
                return;

            //get current selection
            Word.Selection oSelection = null;
            Word.Application oWord = Session.CurrentWordApp;
            if (oWord == null)
                oWord = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
            try
            {
                oSelection = oWord.Selection;
            }
            catch { }

            if (oSelection == null)
                return;

            try
            {
                //2-18-11 (dm) - reset save flag
                m_bSaveInProgress = false;

                LMP.Forte.MSWord.mpDeleteKeyMessageTypes iWarn =
                    LMP.Forte.MSWord.mpDeleteKeyMessageTypes.None;
                Word.XMLNode oTargetTag = null;
                Word.ContentControl oTargetCC = null;
                bool bNoDeleteMethod = false; //GLOG 5121 (dm)
                bool bIsBackspace = (oMsg ==
                    Application.mpPseudoWordEvents.BackspaceKeyPressed);
                string xSegmentTagID = "";

                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                this.ScreenUpdating = false;

                //GLOG 5499 (dm) - when track changes is on and revisions are set to
                //display inline, we need to programatically advance the cursor -
                //this is not necessary, however, when revisions are set to display in
                //balloons and doing so was causing characters to be skipped
                //GLOG 5683 (dm) - this is also not necessary when revisions
                //aren't showing at all
                bool bTrackChanges = (oSelection.Document.TrackRevisions &&
                    (oWord.ActiveWindow.View.MarkupMode !=
                    Word.WdRevisionsMode.wdBalloonRevisions) &&
                    (oWord.ActiveWindow.View.ShowRevisionsAndComments == true));

                try
                {
                    if (m_oForteDocument.FileFormat == mpFileFormats.Binary)
                        iWarn = LMP.Forte.MSWord.WordDoc.WarnAboutDeletion(bIsBackspace, ref oTargetTag,
                            ref bNoDeleteMethod, ref xSegmentTagID);
                    else
                        //GLOG 4894 (dm)
                        iWarn = LMP.Forte.MSWord.WordDoc.WarnAboutDeletion_CC(bIsBackspace, ref oTargetCC,
                            ref bNoDeleteMethod, ref xSegmentTagID);
                }
                catch { }

                if (iWarn != LMP.Forte.MSWord.mpDeleteKeyMessageTypes.EmptymVar)
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                DialogResult iDlgResult = DialogResult.No;
                if (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.DetailVariableInTable)
                {
                    if (Registry.GetMacPac10Value("EnableDeactivationCode") == "1")
                    {
                        //5-2-11 (dm) - never disallow, warn conditionally,
                        //deactivate if user chooses to continue
                        if (!System.String.IsNullOrEmpty(xSegmentTagID))
                        {
                            Segment oSegment = m_oForteDocument.FindSegment(xSegmentTagID);
                            if (oSegment.Activated)
                            {
                                if (Session.CurrentUser.UserSettings.WarnBeforeDeactivation)
                                {
                                    string xMsg = LMP.Resources.GetLangString(
                                        "Prompt_DeleteKeyDeactivationWarning_DetailVariableInTable");
                                    iDlgResult = MessageBox.Show(xMsg,
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                }
                                else
                                    //user has turned off the warning
                                    iDlgResult = DialogResult.Yes;

                                //deactivate if proceeding with deletion
                                if (iDlgResult == DialogResult.Yes)
                                    oSegment.Activated = false;
                            }
                            else
                                //segment is already deactivated
                                iDlgResult = DialogResult.Yes;
                        }
                        else
                            //no parent segment - just allow
                            iDlgResult = DialogResult.Yes;
                    }
                    else
                    {
                        //selection is in a table and includes a detail variable -
                        //disallow and advise user to delete via the task pane
                        string xMsg = LMP.Resources.GetLangString(
                            "Prompt_DeleteKeyOrBackspaceNotAllowed_DetailVariableInTable");
                        MessageBox.Show(xMsg,
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else if ((iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.Warn) ||
                    (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.EmptyParagraphWithCCs))
                {
                    if (Registry.GetMacPac10Value("EnableDeactivationCode") == "1")
                    {
                        //5-2-11 (dm) - never disallow, warn conditionally,
                        //deactivate if user chooses to continue
                        if (!System.String.IsNullOrEmpty(xSegmentTagID))
                        {
                            Segment oSegment = m_oForteDocument.FindSegment(xSegmentTagID);
                            if (oSegment.Activated)
                            {
                                if (Session.CurrentUser.UserSettings.WarnBeforeDeactivation)
                                {
                                    string xMsg = "Prompt_DeleteKeyDeactivationWarning";
                                    if (oSelection.Paragraphs.First.Range.Text == "\r")
                                        //this is an empty paragraph - add suggestion to
                                        //try deleting the preceding paragraph mark
                                        xMsg += "_EmptyParagraph";
                                    xMsg = LMP.Resources.GetLangString(xMsg);
                                    iDlgResult = MessageBox.Show(xMsg,
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                }
                                else
                                    //user has turned off the warning
                                    iDlgResult = DialogResult.Yes;

                                //deactivate if proceeding with deletion
                                if (iDlgResult == DialogResult.Yes)
                                    oSegment.Activated = false;
                            }
                            else
                                //segment is already deactivated
                                iDlgResult = DialogResult.Yes;
                        }
                        else
                            //no parent segment - just allow
                            iDlgResult = DialogResult.Yes;
                    }
                    else
                    {
                        //warn about potential consequences -
                        //9-28-10 (dm) - I'm warning in the case of EmptyParagraphWithCCs only
                        //because I haven't found another way to avoid a Warn message from
                        //being sent on a subsequent handling of the same key press event
                        string xMsg = LMP.Resources.GetLangString(
                            "Prompt_DeleteKeyOrBackspaceWarning");
                        iDlgResult = MessageBox.Show(xMsg,
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                }
                else if (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.Disallow)
                {
                    if (Registry.GetMacPac10Value("EnableDeactivationCode") == "1")
                    {
                        //5-2-11 (dm) - never disallow, warn conditionally,
                        //deactivate if user chooses to continue
                        if (!System.String.IsNullOrEmpty(xSegmentTagID))
                        {
                            Segment oSegment = m_oForteDocument.FindSegment(xSegmentTagID);
                            if (oSegment.Activated)
                            {
                                if (Session.CurrentUser.UserSettings.WarnBeforeDeactivation)
                                {
                                    string xMsg = "Prompt_DeleteKeyDeactivationWarning";
                                    if (oSelection.Paragraphs.First.Range.Text == "\r")
                                        //this is an empty paragraph - add suggestion to
                                        //try deleting the preceding paragraph mark
                                        xMsg += "_EmptyParagraph";
                                    xMsg = LMP.Resources.GetLangString(xMsg);
                                    iDlgResult = MessageBox.Show(xMsg,
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                }
                                else
                                    //user has turned off the warning
                                    iDlgResult = DialogResult.Yes;

                                //deactivate if proceeding with deletion
                                if (iDlgResult == DialogResult.Yes)
                                    oSegment.Activated = false;
                            }
                            else
                                //segment is already deactivated
                                iDlgResult = DialogResult.Yes;
                        }
                        else
                            //no parent segment - just allow
                            iDlgResult = DialogResult.Yes;
                    }
                    else
                    {
                        //inform that not allowed
                        string xMsg = "Prompt_DeleteKeyOrBackspaceNotAllowed";
                        if (oSelection.Paragraphs.First.Range.Text == "\r")
                            //this is an empty paragraph - add suggestion to
                            //try deleting the preceding paragraph mark
                            xMsg += "_EmptyParagraph";
                        xMsg = LMP.Resources.GetLangString(xMsg);
                        MessageBox.Show(xMsg,
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else if (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.EmptymVar)
                {
                    //user is clearing out an inline mVar - we'll want to
                    //either delete the tag or refresh the variable value
                    //when we're satisfied that they're finished with the edit
                    string xTempID = oTargetTag.SelectSingleNode(
                        "@TempID", "", false).NodeValue;
                    if (!this.ucDocEditor.UnresolvedEmptyTags.ContainsKey(xTempID))
                    {
                        this.ucDocEditor.UnresolvedEmptyTags.Add(xTempID,
                            oTargetTag);
                    }
                    iDlgResult = DialogResult.Yes;
                }
                else if (iWarn != LMP.Forte.MSWord.mpDeleteKeyMessageTypes.DisallowSilently)
                    iDlgResult = DialogResult.Yes;

                if (iDlgResult == DialogResult.Yes)
                {
                    //deletion is non-problematic or user has chosen to
                    //proceed after being warned - we've cancelled the key,
                    //so simulate it now
                    object oMissing = System.Reflection.Missing.Value;
                    string xText = oSelection.Text;
                    bool bIsInsertionPoint = (oSelection.Type ==
                        Word.WdSelectionType.wdSelectionIP);

                    //GLOG 6829 (dm) - get mSEG bookmarks at selection before deletion
                    string xBmks = "";
                    if ((this.Mode != ForteDocument.Modes.Design)  && !bIsInsertionPoint)
                    {
                        try
                        {
                            xBmks = LMP.Forte.MSWord.WordDoc.GetSegmentBookmarksAtSelection(
                                 LMP.Forte.MSWord.mpRangeExpansionOptions.PreviousAndNextParagraphs);
                        }
                        catch { }
                    }

                    if (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.EmptyContentControl)
                    {
                        //GLOG 5039 (dm) - replicate native Word behavior whereby
                        //backspacing or deleting with the cursor in an empty
                        //content control deletes the content control
                        oSelection.MoveEnd(ref oMissing, ref oMissing);
                        oSelection.Text = "";
                    }
                    else if (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.EmptyParagraphWithCCs)
                    {
                        //insertion point is in a paragraph containing nothing but empty
                        //content controls - specific handling is necessary to ensure an
                        //effective and immediate deletion of the paragraph and to
                        //prevent errors when preserving mDels
                        Word.Range oRng = oSelection.Paragraphs[1].Range;

                        //delete content controls
                        foreach (Word.ContentControl oCC in oRng.ContentControls)
                        {
                            if (LMP.String.GetBoundingObjectBaseName(oCC.Tag) != "mDel")
                                oCC.Delete(false);
                        }

                        //delete paragraph
                        object oUnit = Word.WdUnits.wdParagraph;
                        oSelection.Expand(ref oUnit);
                        oSelection.Text = "";

                        //adjust selection for backspace
                        if (bIsBackspace)
                        {
                            oUnit = Word.WdUnits.wdCharacter;
                            object oCount = -1;
                            oSelection.Move(ref oUnit, ref oCount);
                        }
                    }
                    else if (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.TaggedMultipleCells)
                    {
                        //GLOG 5139 (dm)
                        LMP.Forte.MSWord.WordDoc.ClearTaggedCells(
                            m_oForteDocument.FileFormat == mpFileFormats.OpenXML);
                    }
                    else if (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.EmptyLastParaOfmSEG)
                    {
                        //GLOG 5203 (dm)
                        //4-12-11 (dm) - handle multiple empty paragraphs at end of mSEG
                        int iParas = oSelection.Paragraphs.Count;
                        oSelection.StartOf(ref oMissing, ref oMissing);
                        for (int i = 1; i < iParas; i++)
                            oSelection.Move(ref oMissing, ref oMissing);
                        for (int i = 1; i < iParas; i++)
                            oSelection.TypeBackspace();
                        oSelection.TypeBackspace();
                    }
                    else if (bIsBackspace)
                    {
                        //backspace key
                        //GLOG 5507 (dm) - added !bNoDeleteMethod condition to avoid
                        //crashing when backspacing into an empty paragraph containing
                        //mDels
                        if (bIsInsertionPoint && ((iWarn ==
                            LMP.Forte.MSWord.mpDeleteKeyMessageTypes.None) ||
                            (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.EmptymVar)) &&
                            !bNoDeleteMethod)
                        {
                            //this is a workaround for a problem in which
                            //you wind up with a string of null characters if
                            //you type some text and backspace without moving the
                            //cursor - Word seems to treat this as a single
                            //transaction, since everything but the first typed
                            //character gets replaced by the null-character string
                            object oUnit = Word.WdUnits.wdCharacter;
                            object oCount = 1;
                            bool bSmartCutPaste = oWord.Options.SmartCutPaste;
                            try
                            {
                                //6-11-10 (dm) - on some machines, the Delete() method
                                //doesn't work on the space immediately after a punctuation
                                //mark, causing the backspace key to stop there -
                                //this isn't an issue inside of tags, and setting text to
                                //empty at the end of a tag sometimes deletes the tag, so
                                //this workaround is limited accordingly -
                                //7-16-10 (dm) - after discovering that the 6/11 workaround,
                                //was causing Word to freeze when backspacing over a space
                                //at the end of a document, we determined that the original
                                //behavioral difference was the result of the "smart cut &
                                //paste" "adjust sentence & word spacing automatically" sub
                                //option - temporarily disable smart cut & paste
                                oWord.Options.SmartCutPaste = false;
                                Word.Range oRng = oSelection.Previous(ref oUnit, ref oCount);
                                //if ((oRng.Text == " ") && (oSelection.XMLParentNode == null))
                                //    oRng.Text = "";
                                //else
                                
                                if (oRng != null)
                                {
                                    //GLOG 5667 (dm) - handler no longer runs in the
                                    //scenario addressed by GLOG 5171
                                    ////GLOG 5171 (dm) - Delete() errs when the previous
                                    ////character is a Word field code
                                    //if (oRng.Fields.Count > 0)
                                    //    oSelection.TypeBackspace();
                                    //else
                                    //{
                                        //GLOG 5324 (dm) - if track changes is on,
                                        //Delete() won't advance cursor
                                        int iStart = 0;
                                        if (bTrackChanges)
                                            iStart = oSelection.Start;
                                        oRng.Delete(ref oMissing, ref oMissing);
                                        if (bTrackChanges && (oSelection.Start == iStart))
                                        {
                                            oCount = -1;
                                            oSelection.Move(ref oUnit, ref oCount);
                                        }
                                    //}
                                }
                            }
                            catch { }
                            finally
                            {
                                oWord.Options.SmartCutPaste = bSmartCutPaste;
                            }
                        }
                        else
                            oSelection.TypeBackspace();
                    }
                    else
                    {
                        //delete key
                        if (bIsInsertionPoint)
                            oSelection.MoveEnd(ref oMissing, ref oMissing);
                        if (xText == "\r")
                        {
                            //GLOG 4466 (dm) - limited the GLOG 4059 workaround to
                            //insertion points - it was interfering with mDel preservation
                            if (bNoDeleteMethod)
                            {
                                //GLOG 5110 (dm)
                                oSelection.Text = "";
                            }
                            else if ((iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.Warn) ||
                                !bIsInsertionPoint)
                            {
                                //if just the paragraph mark is selected, use
                                //backspace, which 1) provides behavior that is more
                                //consistent with the way that both MacPac and Word
                                //generally handle paragraph mark deletion when an
                                //inline tag is followed by a block tag and 2)
                                //avoids intermittent error when attempting to set
                                //the text outside of a block tag
                                //GLOG 5324 (dm) - if track changes is on,
                                //cursor won't advance with deletion
                                int iStart = 0;
                                if (bTrackChanges)
                                    iStart = oSelection.Start;
                                oSelection.EndOf(ref oMissing, ref oMissing);
                                oSelection.TypeBackspace();
                                if (bTrackChanges && bIsInsertionPoint &&
                                    (oSelection.Start == iStart))
                                    oSelection.Move(ref oMissing, ref oMissing);
                            }
                            else
                            {
                                //GLOG 4059 (dm) - TypeBackspace() was causing the same
                                //null characters issue described in backspace key block
                                //above, but the same workaround won't work here -
                                //fortunately, there's no need to switch to a backspace when
                                //we're not between tags
                                oSelection.Delete(ref oMissing, ref oMissing);
                            }

                            //GLOG 3457 (dm) - oTargetTag was an empty block tag -
                            //delete it if it now contains text as a result of the deletion
                            if (iWarn == LMP.Forte.MSWord.mpDeleteKeyMessageTypes.DeleteTagIfNotEmpty)
                            {
                                string xTagText = null;
                                try
                                {
                                    xTagText = oTargetTag.Text;
                                }
                                catch { }
                                if (!System.String.IsNullOrEmpty(xTagText))
                                    oTargetTag.Delete();
                            }
                        }
                        else
                        {
                            //7-20-10 (dm) - removed DoEvents after conflict
                            //with WestCheck caused Word to freeze here
                            //if (!bIsInsertionPoint)
                            //{
                            //    //we need to slow things down to prevent an
                            //    //extra character from getting deleted here -
                            //    //DoEvents causes a slight hang, but is more
                            //    //predictable than an empty loop or timer
                            //    System.Windows.Forms.Application.DoEvents();
                            //}

                            if ((!bIsInsertionPoint) && (!bNoDeleteMethod))
                            {
                                //7-20-10 (dm) - added this branch because without
                                //the call to DoEvents above, deleting the paragraph
                                //mark at the end of the document, plus any preceding text,
                                //leaves that paragraph mark undeleted and selected -
                                //Selection.Text = "" has this effect even in VBA -
                                //oddly enough, I can't reproduce the original reason
                                //for calling DoEvents, i.e. the extra character getting deleted -
                                //GLOG 5121 (dm) - extended this branch to MacPac segments,
                                //except where an mDel to be preserved is nested in a tag that's
                                //opening or closing tag is also in the selection - I've concluded
                                //that we should use the Delete() method for all other block
                                //deletions, because it generally reflects native delete key
                                //behavior more closely than .Text = ""
                                //9/17/10 (dm) - I discovered that the GLOG 5121 change was causing
                                //a problem unrelated to mDels, but also involving PreventAccidentalTagDeletion(),
                                //when a table with tags in multiple cells was selected - thus, this
                                //branch has now been limited to untagged selections
                                try
                                {
                                    oSelection.Delete(ref oMissing, ref oMissing);
                                }
                                catch (System.Exception oE)
                                {
                                    //11/10/10 (dm) - ignore intermittent HRESULT E_FAIL error,
                                    //since it doesn't prevent successful deletion
                                    if (oE.Message != "Error HRESULT E_FAIL has been returned from a call to a COM component.")
                                        throw oE;
                                }
                            }
                            else
                            {
                                //setting text to empty avoids error caused by
                                //Selection.Delete() when we attempt to
                                //preserve nested mDels
                                //GLOG 5324 (dm) - if track changes is on,
                                //cursor won't advance with deletion
                                //GLOG 6150 (dm) - modified the test to determine
                                //whether the cursor has advanced automatically to
                                //compare the distance to the end of the paragraph
                                //before after the deletion
                                int iStart = 0;
                                if (bTrackChanges)
                                {
                                    iStart = oSelection.Paragraphs[1].Range.End -
                                        oSelection.Start;
                                }

                                try
                                {
                                    oSelection.Text = "";
                                }
                                catch (System.Exception oE)
                                {
                                    //9-21-10 (dm) - for the time being, this is how we're
                                    //dealing with the delete key at the end of a cc containing
                                    //nothing but empty children - I can't find a way to safely
                                    //delete the cc or the entire paragraph in this case, and if
                                    //we move the cursor past the boundary, the cc won't be deleted
                                    //when the cursor reaches and deletes the paragraph - doing
                                    //nothing is consistent with a backspace at the start of such a cc,
                                    //where native behavior causes the same result
                                    if (oE.Message != "This action is not valid outside of a block-level XML element.")
                                        throw oE;
                                }
                                if (bTrackChanges && bIsInsertionPoint &&
                                    (oSelection.Paragraphs[1].Range.End -
                                    oSelection.Start == iStart))
                                    oSelection.Move(ref oMissing, ref oMissing);
                            }
                        }
                    }

                    //GLOG 6829 (dm) - check whether any segments are gone and refresh if necessary
                    if (xBmks != "")
                    {
                        string xBmksAfter = null;
                        try
                        {
                            xBmksAfter = LMP.Forte.MSWord.WordDoc.GetSegmentBookmarksAtSelection(
                                 LMP.Forte.MSWord.mpRangeExpansionOptions.PreviousAndNextParagraphs);
                        }
                        catch { }
                        if ((xBmksAfter != null) && (xBmksAfter.Length < xBmks.Length))
                            m_oRefreshTimer.Start();
                    }
                }
            }
            finally
            {
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                this.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// Returns true if Paste Options button is visible in specified document
        /// </summary>
        /// <param name="oDoc"></param>
        /// <returns></returns>
        internal static bool PasteOptionsVisible(Word.Document oDoc)
        {
            //GLOG 5666: Paste Options button exists as Window with "OOCWindow" as Class and Title
            //Get window handle for current document
            int iParentHandle = LMP.WinAPI.FindWindow("OpusApp", oDoc.ActiveWindow.Caption + " - Microsoft Word");
            if (LMP.OS.FindChildWindow((IntPtr)iParentHandle, "OOCWindow", "OOCWindow") != IntPtr.Zero)
                return true;
            else
                return false;
        }
        #endregion
    }
    internal class PendingAction
    {
        #region *********************enumerations*********************
        public enum Types
        {
            Insert = 1,
            InsertForDesign = 2,
            SetUpForDesign = 3,
            InsertForPreview = 4,
            PasteSegmentContent = 5,
            RecreateLegacyDocument = 6,
            SaveSegmentDesign = 7,
            SetDesignMode = 8,
            SetupOxmlSegment = 9
        }
        #endregion
        #region *********************fields*********************
        public Types Type;
        public string SegmentID;
        public Prefill Prefill = null;
        public bool ForcePrefillOverride = false;
        public string BodyText = "";
        public string AlternateXML = "";
        public LMP.Architect.Base.CollectionTableStructure ChildStructure = null;
#if Oxml
        public XmlPrefill XmlPrefill = null;
        public XmlSegment XmlSegment = null;
#endif
        #endregion
        #region *********************constructors*********************
        public PendingAction(Types oType, FolderMember oFolderMember)
        {
            this.Type = oType;

            //get ID of target
            this.SegmentID = oFolderMember.ObjectID1 + "." + oFolderMember.ObjectID2;
            this.Prefill = null;
#if Oxml
            this.XmlPrefill = null;
#endif
            this.BodyText = "";
        }
        public PendingAction(Types oType, string xID, string xAlternateXML)
        {
            this.Type = oType;
            this.SegmentID = xID;
            this.Prefill = null;
            this.BodyText = "";
            this.AlternateXML = xAlternateXML;
#if Oxml
            this.XmlPrefill = null;
#endif
        }
        public PendingAction(Types oType, string xID, Prefill oPrefill, string xBodyText, 
            string xAlternateXML, bool bForcePrefillOverride, LMP.Architect.Base.CollectionTableStructure oChildStructure)
        {
            this.Type = oType;
            this.SegmentID = xID;
            this.Prefill = oPrefill;
            this.BodyText = xBodyText;
            this.AlternateXML = xAlternateXML;
            this.ForcePrefillOverride = bForcePrefillOverride;
            this.ChildStructure = oChildStructure;
#if Oxml
            //Create XML Prefill with oPrefill values
            if (oPrefill != null)
                this.XmlPrefill = new XmlPrefill(oPrefill.ToString(), oPrefill.Name, oPrefill.SegmentID);
            else
                this.XmlPrefill = null;
        }
        public PendingAction(Types oType, XmlSegment oSegment, XmlPrefill oPrefill, string xBodyText, LMP.Architect.Base.CollectionTableStructure oChildStructure)
        {
            this.Type = oType;
            this.XmlSegment = oSegment;
            this.SegmentID = oSegment.ID;
            this.XmlPrefill = oPrefill;
            this.BodyText = xBodyText;
            this.ChildStructure = oChildStructure;
#endif
        }
        #endregion
    }
}
