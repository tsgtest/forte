﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class SaveAsUserSegmentForm : Form
    {
        private enum InvalidDisplayNames
        {
            ValidName = 0,
            NullString = 1,
            ContainsSpaces = 2,
            DuplicateName = 3,
            IllegalName = 4
        }

        //LMP.Architect.Api.Segment m_oSeg = null;

        public SaveAsUserSegmentForm()
        {
            InitializeComponent();
            this.ftvPrefill.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders;
            this.ftvPrefill.OwnerID = Session.CurrentUser.ID;
            this.ftvPrefill.ExecuteFinalSetup();
            this.ftvPrefill.Nodes[0].Selected = true;
            this.ftvPrefill.ActiveNode = this.ftvPrefill.SelectedNodes[0];
        }

        public string SelectedFolderID
        {
            get
            {
                string xFolderID = this.ftvPrefill.Value;
                LMP.MacPac.Session.CurrentUser.UserSettings.DefaultDesignerFolderID = xFolderID;
                return xFolderID;
            }
        }

        public string SelectedFolderNodeKey
        {
            get
            {
                if ((this.ftvPrefill.DisplayOptions & LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) ==
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders))
                {
                    //only user folders are showing - append user folder root key
                    return "0.0\\\\" + this.ftvPrefill.SelectedNodes[0].Key;
                }
                else if ((this.ftvPrefill.DisplayOptions & LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) ==
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders))
                {
                    //only admin folders are showing - append admin folder root key
                    return "0\\\\" + this.ftvPrefill.SelectedNodes[0].Key;
                }
                else if ((this.ftvPrefill.DisplayOptions & LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders) ==
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SharedFolders && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders) && ((this.ftvPrefill.DisplayOptions &
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) !=
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders))
                {
                    //only shared folders are showing - append shared folder root key
                    return "-2\\\\" + this.ftvPrefill.SelectedNodes[0].Key;
                }
                else
                    //root keys are showing - don't append anything
                    return this.ftvPrefill.SelectedNodes[0].Key;
            }
        }

        public string ItemDisplayName
        {
            get
            {
                return this.txtDisplayName.Text;
            }
            set
            {
                this.txtDisplayName.Text = value;
            }
        }

        public string ItemName
        {
            get
            {
                return this.txtName.Text;
            }
            set
            {
                this.txtName.Text = value;
            }
        }
        ///// <summary>
        ///// validates designated name by attempting to create instance
        ///// of variable using Variables.ItemByName
        ///// </summary>
        //private InvalidDisplayNames ValidateName()
        //{
        //    LMP.Architect.Api.Variable oVar = null;
        //    LMP.Architect.Api.Block oBlock = null;

        //    //empty text box is invalid name
        //    if (this.txtDisplayName.Text == "" || this.txtName.Text == "")
        //        return InvalidDisplayNames.NullString;

        //    try
        //    {
        //        //attempt to create variable w/ proposed name
        //        oVar = this.m_oSeg.Variables.ItemFromName(this.txtName.Text);
        //        if (oVar != null)
        //            //variable name already exists
        //            return InvalidDisplayNames.DuplicateName;

        //        //attempt to create variable w/ proposed name
        //        oBlock = this.m_oSeg.Blocks.ItemFromName(this.txtName.Text);
        //        if (oBlock != null)
        //            //variable name already exists
        //            return InvalidDisplayNames.DuplicateName;
        //    }
        //    catch
        //    {
        //        return InvalidDisplayNames.ValidName;
        //    }
        //    return InvalidDisplayNames.IllegalName;
        //}
        #region************************events***********************
        private void txtDisplayName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtName.Text = String.RemoveIllegalNameChars(this.txtDisplayName.Text);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Disallow characters illegal for Name property
            if (e.KeyChar == ' ' ||
               e.KeyChar == '.' ||
               e.KeyChar == '\'' ||
               e.KeyChar == '>' ||
               e.KeyChar == '<' ||
               e.KeyChar == '&' ||
               e.KeyChar == '[' ||
               e.KeyChar == ']' ||
               e.KeyChar == '^')
                e.Handled = true;
        }

        private void txtDisplayName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //disallow MacPac reserved characters in Display Name
            if (e.KeyChar == '[' ||
                e.KeyChar == ']' ||
                e.KeyChar == '^')
                e.Handled = true;

        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            //GLOG 6402: Make sure Display Name and Name have been entered
            if (!ValidateControls())
            {
                this.DialogResult = DialogResult.None;
                return;
            }
        }
        private bool ValidateControls()
        {
            if (this.txtDisplayName.Text == "")
            {
                string xMsg = LMP.Resources.GetLangString("Msg_Segment_Name_Required");
                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.txtDisplayName.Focus();
                return false;
            }
            else if (this.txtName.Text == "")
            {
                string xMsg = LMP.Resources.GetLangString("Error_NameFieldCannotBeNull");
                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.txtName.Focus();
                return false;
            }

            return true;
        }

        #endregion
    }
}
