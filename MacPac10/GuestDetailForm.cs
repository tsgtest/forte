using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    internal partial class GuestDetailForm : Form
    {
        //TODO: initialize person detail fields in grid

        public GuestDetailForm()
        {
            InitializeComponent();

            this.lblGuest.Text = LMP.Resources.GetLangString("Prompt_LoggedInAsGuest");
        }

        private void GuestDetailForm_Load(object sender, EventArgs e)
        {
            this.picSidebar.Image = LMP.MacPac.Properties.Resources.ForteLogoLarge;
        }

    }
}