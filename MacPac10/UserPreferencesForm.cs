using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Infragistics.Win.UltraWinTree;
using LMP.Data;

namespace LMP.MacPac
{
    internal partial class UserPreferencesForm : Form
    {
        #region *********************constructors*********************
        public UserPreferencesForm()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************fields*********************
        #endregion
        #region *********************event handlers*********************
        private void UserApplicationSettingsForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCategories();
                UltraTreeNode oTreeNode = this.treeCategories.Nodes[0];
                oTreeNode.Selected = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeCategories_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                Control oCtl;

                //GLOG item #4337 - dcf
                if (e.NewSelections.Count == 0)
                    return;

                //get currently selected node
                UltraTreeNode oNode = e.NewSelections[0];

                //remove the previous control, if any
                this.scPanels.Panel2.Controls.Clear();

                if (oNode.Tag is UserControl)
                    //the control has already been created - just show
                    oCtl = (Control)oNode.Tag;
                else
                {
                    //control has not yet been created - 
                    //use reflection to create instance
                    string xClassName = oNode.Tag.ToString();

                    Assembly oA = Assembly.GetExecutingAssembly();

                    oCtl = (Control)oA.CreateInstance("LMP.MacPac." + xClassName);

                    if (xClassName == "MultipleSegmentsManager")
                    {
                        ((MultipleSegmentsManager)oCtl).SetupDialog("Save", "", false, false);
                        oCtl.Dock = DockStyle.Fill;
                    }

                    //add control to node tag for future use
                    oNode.Tag = oCtl;
                }

                //add control to collection
                this.scPanels.Panel2.Controls.Add(oCtl);

                //fill panel with control
                oCtl.Dock = DockStyle.Fill;

                foreach (Control oChild in oCtl.Controls)
                {
                    if (oChild.TabIndex == 0)
                    {
                        oChild.Select();
                        break;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************private methods*********************
        private void LoadCategories()
        {
            TreeNodesCollection oNodes = this.treeCategories.Nodes;
            UltraTreeNode oNode;
            oNode = oNodes.Add(LMP.Resources.GetLangString("Dialog_UserPreferences_Display"));
            oNode.Tag = "UserApplicationSettingsManager";
            oNode = oNodes.Add(LMP.Resources.GetLangString("Dialog_UserPreferences_FavoriteSegments"));
            oNode.Tag = "FavoriteSegmentsManager";
            //oNode.Tag = "MultipleSegmentsManager";

            //GLOG : 3079 : CEH
            //Only display if Firm Application Settings allow it
            FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
            if (oSettings.AllowUsersToSetTrailerBehavior)
            {
                oNode = oNodes.Add(LMP.Resources.GetLangString("Dialog_UserPreferences_TrailerDisplayOptions"));
                oNode.Tag = "TrailerDisplayOptionsManager";
            }
        }
        #endregion
    }
}