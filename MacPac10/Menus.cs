using System;

namespace LMP.MacPac
{
	/// <summary>
	/// Summary description for Menus.
	/// </summary>
	internal class Menus
	{
		public Menus()
		{
		}

		public void Help()
		{
			System.Windows.Forms.MessageBox.Show(LMP.ComponentProperties.ProductName + " Help.");
		}

		public void AboutMacPac()
		{
			System.Windows.Forms.MessageBox.Show("About MacPac.");
		}

	}
}
