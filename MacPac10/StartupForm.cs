﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class StartupForm : Form
    {
        private Timer m_oTimer = null;
        public int TimerDelay = 0;
        public bool OffScreen = false;
        public StartupForm()
        {
            InitializeComponent();
        }

        private void StartupForm_Load(object sender, EventArgs e)
        {
            if (OffScreen)
            {
                this.Top = 0;
                this.Left = 0 - (this.Width + 50);
            }
        }

        void m_oTimer_Tick(object sender, EventArgs e)
        {
            m_oTimer.Stop();
            this.Hide();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (m_oTimer != null)
                m_oTimer.Stop();
            this.Hide();
        }

        private void StartupForm_Shown(object sender, EventArgs e)
        {
            if (TimerDelay > 0)
            {
                btnOK.Enabled = false;
                m_oTimer = new Timer();
                m_oTimer.Interval = TimerDelay;
                m_oTimer.Tick += m_oTimer_Tick;
                m_oTimer.Start();
            }
        }
    }
}
