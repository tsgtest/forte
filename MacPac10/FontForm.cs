using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
namespace LMP.MacPac
{
    public partial class FontForm : Form
    {
        Word.Style m_oStyle;
        bool m_bPreviewExecuted = false;
        bool m_bInitialized = false;

        DateTime m_oLastKeystrokeTime = System.DateTime.MinValue;

        // Allow the user a maximum interval of 1.5 seconds between key strokes
        // during the incremental search for a font.
        System.TimeSpan m_oMaxKeyStrokeInterval = new TimeSpan(0, 0, 0, 1, 500);
        StringBuilder m_oSBFindText = new StringBuilder();
        DataTable m_dtFonts = null;

        public FontForm(string xFontName, int iFontSize, Word.Style oStyle, string xDialogTitle)
        {
            InitializeComponent();

            try
            {
                this.LoadFontList();

                //set selected font name and size
                this.FontName = xFontName;
                this.FontSize = iFontSize;
                m_oStyle = oStyle;
                this.Text = xDialogTitle;
                m_bInitialized = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void DocumentFont_Load(object sender, EventArgs e)
        {
            //GLOG #8492 - dcf
            this.lstFontNames.Height = this.btnCancel.Top - this.lstFontNames.Top - 10;
            this.lstFontSize.Height = this.lstFontNames.Height;
        }

        DataTable FontTable
        {
            get
            {
                if (m_dtFonts == null)
                {
                    m_dtFonts = new DataTable();
                    m_dtFonts.CaseSensitive = false;

                    DataColumn dcFontName = new DataColumn("FontName", typeof(string));
                    m_dtFonts.Columns.Add(dcFontName);

                    //GLOG 5908: InstalledFontCollection does not include PostScript,
                    //Raster, or non-Microsoft OpenType fonts - use Screen.Fonts in VB instead

                    //System.Drawing.Text.InstalledFontCollection oFonts = new System.Drawing.Text.InstalledFontCollection();
                    //FontFamily[] oFamilies = oFonts.Families;
                    ////underlying combo requires a 2 col list, so duplicate both
                    //for (int i = 0; i < oFamilies.Length; i++)
                    //{
                    //    m_dtFonts.Rows.Add(oFamilies[i].Name);
                    //}
                    LMP.Forte.MSWord.Application oApp = new LMP.Forte.MSWord.Application();
                    object[] oFonts = (object[])LMP.Forte.MSWord.Application.ScreenFonts();
                    Array.Sort(oFonts);
                    for (int i = 0; i < oFonts.Length; i++)
                    {
                        m_dtFonts.Rows.Add(oFonts[i].ToString());
                    }
                }

                return m_dtFonts;
            }
        }
        /// <summary>
        /// loads font list from existing system fonts
        /// </summary>
        private void LoadFontList()
        {
            this.lstFontNames.DataSource = this.FontTable;
            this.lstFontNames.DisplayMember = this.FontTable.Columns[0].ColumnName;
        }

        public string FontName
        {
            get { return this.lstFontNames.Text; }
            set { this.lstFontNames.Text = value; }
        }
        public int FontSize
        {
            get { return int.Parse(this.lstFontSize.SelectedItem.ToString()); }
            set { this.lstFontSize.SelectedItem = value.ToString(); }
        }

        private void lstFontNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PreviewChanges();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstFontSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PreviewChanges();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void PreviewChanges()
        {
            if (!m_bInitialized)
                return;

            ClearUndoIfNecessary();

            m_oStyle.Font.Name = this.FontName;
            m_oStyle.Font.Size = this.FontSize;

            Session.CurrentWordApp.ScreenRefresh();

            m_bPreviewExecuted = true;
        }

        private void ClearUndoIfNecessary()
        {
            if (m_bPreviewExecuted)
            {
                object oTimes = 2;
                m_oStyle.Application.ActiveDocument.Undo(ref oTimes);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ClearUndoIfNecessary();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstFontNames_KeyPress(object sender, KeyPressEventArgs e)
        {
            System.DateTime oNow = System.DateTime.Now;

            if (Char.IsLetterOrDigit(e.KeyChar) || Char.IsNumber(e.KeyChar) || Char.IsPunctuation(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Escape)
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    if (m_oSBFindText.Length > 0)
                    {
                        string xFindText = this.m_oSBFindText.ToString();
                        
                        // Remove the last char from the find text and reset the string builders text.
                        this.m_oSBFindText.Length = 0;
                        this.m_oSBFindText.Append(xFindText.Remove(xFindText.Length - 1));
                    }
                }
                else
                {
                    if (e.KeyChar == (char)Keys.Escape)
                    {
                        this.m_oSBFindText.Length = 0;
                    }
                    else
                    {
                        if (m_oLastKeystrokeTime == System.DateTime.MinValue)
                        {
                            // This is the first time a char is added to the search text.
                            this.m_oSBFindText.Append(e.KeyChar.ToString());
                        }
                        else
                        {
                            TimeSpan oKeyStrokeInterval = oNow.Subtract(m_oLastKeystrokeTime);

                            if (oKeyStrokeInterval.Ticks < m_oMaxKeyStrokeInterval.Ticks)
                            {
                                this.m_oSBFindText.Append(e.KeyChar.ToString());
                            }
                            else
                            {
                                // A period greater than the allowed max interval has elapsed since
                                // the last keystroke. Clear the current text and start a new find
                                // string with this char.
                                this.m_oSBFindText.Length = 0;
                                this.m_oSBFindText.Append(e.KeyChar.ToString());
                            }
                        }
                    }
                }

                string xFontName = m_oSBFindText.ToString().ToUpper();

                DataRow[] oRows = this.m_dtFonts.Select("FontName LIKE '" + xFontName + "%'");

                if (oRows.Length > 0)
                {
                    this.lstFontNames.SelectedIndex = oRows[0].Table.Rows.IndexOf(oRows[0]);
                }

                m_oLastKeystrokeTime = oNow;
                e.Handled = true;
 
            }
        }
    }
}