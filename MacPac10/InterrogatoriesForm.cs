using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace LMP.MacPac
{
    public partial class InterrogatoriesForm : Form
    {
        #region*********************enums*********************
        #endregion
        #region*********************constants*********************
        private const string INTERROGATORIES_SETID = "-4";
        public const string DefaultPlaceholder = "{#}";
        #endregion
        #region*********************fields*********************
        string m_xPrevValues;
        bool m_bHasCustomHeadings;
        #endregion
        #region*********************constructors*********************
        public InterrogatoriesForm(string xPreviousValues)
        {
            InitializeComponent();

            m_xPrevValues = xPreviousValues;
        }
        #endregion
        #region*********************properties*********************
        public bool HasCustomHeadings
        {
            get { return m_bHasCustomHeadings; }
        }
        public string HeadingText
        {
            get
            {
                return this.lstHeadings.Items[this.lstHeadings.SelectedIndex].ToString();
            }

            set
            {
                this.lstHeadings.Value = value;
            }
        }

        public string ResponseHeadingText
        {
            get
            {
                if (this.lstHeadings.SelectedIndex % 2 == 0)
                    return this.lstHeadings.Items[this.lstHeadings.SelectedIndex + 1].ToString();
                else
                    return null;
            }
        }

        //GLOG : 5696 : CEH
        //returns True if at least question and/or response
        //uses the default Number placeholder
        public bool UsesPlaceholder
        {
            get
            {
                try
                {
                    string xHeadingText = "";
                    string xResponseHeadingText = "";

                    if (this.lstHeadings.SelectedIndex % 2 == 0)
                    {
                        xHeadingText = this.lstHeadings.Items[this.lstHeadings.SelectedIndex].ToString();
                        xResponseHeadingText = this.lstHeadings.Items[this.lstHeadings.SelectedIndex + 1].ToString();
                    }
                    else
                    {
                        xHeadingText = this.lstHeadings.Items[this.lstHeadings.SelectedIndex - 1].ToString();
                        xResponseHeadingText = this.lstHeadings.Items[this.lstHeadings.SelectedIndex].ToString();
                    }

                    return xHeadingText.Contains(DefaultPlaceholder) || xResponseHeadingText.Contains(DefaultPlaceholder);
                }
                catch { }
                
                return false;
            }

        }

        private bool IsValidNumberEntries
        {
            get
            {
                try
                {
                    int iStartNumber = 0;
                    int iEndNunmber = 0;

                    if (string.IsNullOrEmpty(this.txtStartingNumber.Text) &&
                        string.IsNullOrEmpty(this.txtEndingNumber.Text))
                        return true;

                    if (!string.IsNullOrEmpty(this.txtStartingNumber.Text))
                    {
                        iStartNumber = int.Parse(this.txtStartingNumber.Text);
                    }

                    if (!string.IsNullOrEmpty(this.txtEndingNumber.Text))
                    {
                        iEndNunmber = int.Parse(this.txtEndingNumber.Text);
                    }

                    return (iEndNunmber > iStartNumber) || string.IsNullOrEmpty(this.txtEndingNumber.Text);
                }
                catch
                {
                }
                    
                return false;
            }
        }

        public bool AlternateQuestionsAndResponses
        {
            get
            {
                return this.chkAlternateBetweenQuestionsAndResponses.Checked;
            }
            set
            {
                this.chkAlternateBetweenQuestionsAndResponses.Checked = value;
            }
        }

        public LMP.Forte.MSWord.Interrogatories.mpRogHeadingSeparation HeadingSeparator
        {
            get
            {
                return this.optNoHeadingSeparator.Checked ?
                    LMP.Forte.MSWord.Interrogatories.mpRogHeadingSeparation.RunIn : 
                    LMP.Forte.MSWord.Interrogatories.mpRogHeadingSeparation.SeparateLine;
            }
            set
            {
                if(value == LMP.Forte.MSWord.Interrogatories.mpRogHeadingSeparation.RunIn)
                    this.optNoHeadingSeparator.Checked = true;

                if(value == LMP.Forte.MSWord.Interrogatories.mpRogHeadingSeparation.SeparateLine)
                    this.optParaHeadingSeparator.Checked = true;
            }
        }

        public int EndNumber
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(this.txtEndingNumber.Text))
                    {
                        return 0;
                    }
                    if (!string.IsNullOrEmpty(this.txtStartingNumber.Text))
                    {
                        return int.Parse(this.txtEndingNumber.Text);
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_InvalidNonIntegerValue"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }

                return 0;
            }
            set
            {
                if (value > 0)
                    this.txtEndingNumber.Text = value.ToString();
                else
                    this.txtEndingNumber.Text = "";
            }
        }

        public int StartNumber
        {
            get
            {
                try
                {
                    if(!string.IsNullOrEmpty(this.txtStartingNumber.Text))
                        return int.Parse(this.txtStartingNumber.Text);
                    else
                        return 0;
                }
                catch
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_InvalidNonIntegerValue"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }

                return 0;
            }
            set
            {
                if (value > 0)
                    this.txtStartingNumber.Text = value.ToString();
                else
                    this.txtStartingNumber.Text = "";
            }
        }

        public LMP.Forte.MSWord.Interrogatories.mpRogFormatTypes FormatType
        {
            get
            {
                return this.optDirectFormat.Checked ? 
                    LMP.Forte.MSWord.Interrogatories.mpRogFormatTypes.DirectFormat : LMP.Forte.MSWord.Interrogatories.mpRogFormatTypes.Style;
            }
            set
            {
                if(value == LMP.Forte.MSWord.Interrogatories.mpRogFormatTypes.DirectFormat)
                    this.optDirectFormat.Checked = true;

                if(value == LMP.Forte.MSWord.Interrogatories.mpRogFormatTypes.Style)
                    this.optStyleFormat.Checked = true;
            }
        }

        public bool RepeatPreviousNumber
        {
            get
            {
                return chkRepeatPreviousNumber.Checked;
            }
            set
            {
                this.chkRepeatPreviousNumber.Checked = value;
            }
        }

        public string[] Interrogatories
        {
            get
            {
                string[] axInterrogatories = new string[this.lstHeadings.Items.Count];

                for (int i = 0; i < axInterrogatories.Length; i++)
                {
                    axInterrogatories[i] = (string)this.lstHeadings.Items[i];
                }

                return axInterrogatories;
            }
        }

        public double HeadingIndent
        {
            get
            {
                return double.Parse(this.spnHeadingIndent.Value, LMP.Culture.USEnglishCulture); //GLOG 6966
            }
            set
            {
                this.spnHeadingIndent.Value = value.ToString(LMP.Culture.USEnglishCulture); //GLOG 6966
            }
        }

        public bool InsertAsFieldCodes
        {
            get
            {
                return chkInsertAsFieldCodes.Checked;
            }
            set
            {
                this.chkInsertAsFieldCodes.Checked = value;
            }
        }
        //GLOG : 6746 : CEH
        public bool ShowAnswerNumber
        {
            get
            {
                return chkShowAnswerNumber.Checked;
            }
            set
            {
                this.chkShowAnswerNumber.Checked = value;
            }
        }

        public bool ResponseSelected
        {
            get
            {
                return this.lstHeadings.SelectedIndex % 2 != 0;
            }
        }


        public double NextParagraphIndent
        {
            get
            {
                return double.Parse(this.spnNextParaIndent.Value, LMP.Culture.USEnglishCulture); //GLOG 6966
            }
            set
            {
                this.spnNextParaIndent.Value = value.ToString(LMP.Culture.USEnglishCulture); //GLOG 6966
            }
        }

        public bool Bold
        {
            get
            {
                return this.chkBold.Checked;
            }
            set
            {
                this.chkBold.Checked = value;
            }
        }

        public bool Underline
        {
            get
            {
                return this.chkUnderline.Checked;
            }
            set
            {
                this.chkUnderline.Checked = value;
            }
        }

        public bool AllCaps
        {
            get
            {
                return this.chkAllCaps.Checked;
            }
            set
            {
                this.chkAllCaps.Checked = value;
            }
        }

        public bool SmallCaps
        {
            get
            {
                return this.chkSmallCaps.Checked;
            }
            set
            {
                this.chkSmallCaps.Checked = value;
            }
        }
        #endregion
        #region*********************methods*********************
        #endregion
        #region*********************private methods*********************
        private void LoadInterrogatoriesTypes()
        {
            //GLOG item #5673 - restructure rog heading storage to
            //allow change of order - move from ValueSets table to firm
            //app keyset
            string xHeadings = null;

            try
            {
                xHeadings = LMP.Data.KeySet.GetKeyValue("Interrogatories",
                    LMP.Data.mpKeySetTypes.FirmApp, "0", LMP.Data.ForteConstants.mpFirmRecordID);
            }
            catch { }

            DataTable oDT = null;

            if (string.IsNullOrEmpty(xHeadings))
            {
                //no keyset-based rog headings - load from ValueSets table
                DataSet oDS = GetValueSet(INTERROGATORIES_SETID);

                //get data table
                oDT = oDS.Tables[0];
            }
            else
            {
                //get data table for interrogatory heading items
                oDT = new DataTable();
                oDT.Columns.Add("Value1");
                oDT.Columns.Add("Value2");

                string[] aHeadings = xHeadings.Split('�');
                for (int i = 0; i < aHeadings.Length; i = i + 2)
                {
                    oDT.Rows.Add(aHeadings[i], aHeadings[i + 1]);
                }
            }

            if (oDT.Rows.Count == 0)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString("Error_NoInterrogatoryHeadingsDefined"));
            }

            foreach (DataRow oRow in oDT.Rows)
            {
                AddToHeadingList((string)oRow[0]);
                AddToHeadingList((string)oRow[1]);
            }

            return;
        }

        private void SetInitialValues()
        {
            if (!string.IsNullOrEmpty(m_xPrevValues))
            {
                string[] aPrevValues = m_xPrevValues.Split('|');

                int iStartNumber = int.Parse(aPrevValues[12]);
                int iEndNumber = int.Parse(aPrevValues[3]);
                this.AllCaps = bool.Parse(aPrevValues[0]);
                this.AlternateQuestionsAndResponses = bool.Parse(aPrevValues[1]);
                this.Bold = bool.Parse(aPrevValues[2]);
                //this.EndNumber = int.Parse(aPrevValues[3]);
                this.FormatType = (LMP.Forte.MSWord.Interrogatories.mpRogFormatTypes)int.Parse(aPrevValues[4]);
                this.HeadingIndent = double.Parse(aPrevValues[5]);
                this.NextParagraphIndent = double.Parse(aPrevValues[6]);
                this.RepeatPreviousNumber = bool.Parse(aPrevValues[7]);
                this.HeadingText = aPrevValues[8];
                this.HeadingSeparator = (LMP.Forte.MSWord.Interrogatories.mpRogHeadingSeparation)int.Parse(aPrevValues[9]);
                this.SmallCaps = bool.Parse(aPrevValues[11]);
                //this.StartNumber = int.Parse(aPrevValues[12]);
                this.Underline = bool.Parse(aPrevValues[13]);
                this.InsertAsFieldCodes = bool.Parse(aPrevValues[14]);
                this.ShowAnswerNumber = bool.Parse(aPrevValues[18]);    //GLOG : 6746 : CEH

                if (this.lstHeadings.SelectedIndex == -1)
                {
                    //a custom heading was inserted the previous time -
                    //reload the custom headings into the list
                    this.lstHeadings.Items.Insert(0, aPrevValues[16]);
                    this.lstHeadings.Items.Insert(0, aPrevValues[15]);

                    m_bHasCustomHeadings = true;
                    this.HeadingText = aPrevValues[8];

                    this.btnAddCustom.Text = "Edit &Custom Headings...";
                }

                //if the previous value was a question and the user did not
                //previously create an interrogatory "set", select the response
                //to the question - if the previous value was the response and a
                ////set was not created, select the question
                //if (this.lstHeadings.SelectedIndex % 2 == 0 &&
                //    (this.EndNumber - this.StartNumber == 0) && !this.CreateQuestionsAndResponses)
                //    this.lstHeadings.SelectedIndex = this.lstHeadings.SelectedIndex + 1;
                //else if (this.lstHeadings.SelectedIndex % 2 == 1 &&
                //    (this.EndNumber - this.StartNumber == 0) && !this.CreateQuestionsAndResponses)
                //    this.lstHeadings.SelectedIndex = this.lstHeadings.SelectedIndex - 1;
                bool bSetCreatedPreviously = iStartNumber != iEndNumber;

                if (!bSetCreatedPreviously)
                {
                    if (this.lstHeadings.SelectedIndex % 2 == 0 && this.AlternateQuestionsAndResponses)
                        this.lstHeadings.SelectedIndex = this.lstHeadings.SelectedIndex + 1;
                    else if (this.lstHeadings.SelectedIndex % 2 == 1 && this.AlternateQuestionsAndResponses)
                        this.lstHeadings.SelectedIndex = this.lstHeadings.SelectedIndex - 1;
                }
            }
            else
            {
                this.lstHeadings.SelectedIndex = 0;
                //GLOG item #5957 - dcf
                this.chkAlternateBetweenQuestionsAndResponses.Checked =
                    Session.CurrentUser.UserSettings.AlternateBetweenRogsQAndR;
                this.optStyleFormat.Checked = true;
                this.optParaHeadingSeparator.Checked = true;
                //GLOG : 6746 : ceh
                this.chkShowAnswerNumber.Checked = true;
            }
        }

        private DataSet GetValueSet(string xSetID)
        {
            // JAB TODO: RENAME THESE FIELDS AS LOCAL VARS
            OleDbDataAdapter m_oAdapter = null;
            OleDbCommandBuilder m_oCmdBuilder = null;

            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = "Select Value1, Value2, SetID FROM ValueSets WHERE SetID = " + xSetID + ";";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                if (m_oAdapter == null)
                    m_oAdapter = new OleDbDataAdapter(oCmd);
                if (m_oCmdBuilder == null)
                    m_oCmdBuilder = new OleDbCommandBuilder(m_oAdapter);

                DataSet oDS = new DataSet();
                using (m_oAdapter)
                {
                    //create and fill data set using adapter
                    m_oAdapter.Fill(oDS);
                }
                return oDS;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }

        private void AlertInvalidHeadingNumbers()
        {
            string xNumberEntriesText = null;
            int iStartNumber = 0;
            int iEndNumber = 0;

            try
            {
                if(!string.IsNullOrEmpty(this.txtStartingNumber.Text))
                    iStartNumber = int.Parse(this.txtStartingNumber.Text);

                try
                {
                    if(!string.IsNullOrEmpty(this.txtStartingNumber.Text))
                        iEndNumber = int.Parse(this.txtEndingNumber.Text);
                }
                catch
                {
                    xNumberEntriesText = "Value must be a number.";
                    this.txtEndingNumber.Focus();
                }
            }
            catch
            {
                xNumberEntriesText = "Value must be a number.";
                this.txtStartingNumber.Focus();
            }

            if (iStartNumber > iEndNumber)
            {
                xNumberEntriesText = "Ending number must be greater than starting number.";
                this.txtEndingNumber.Focus();
            }

            if (xNumberEntriesText != null)
            {
                MessageBox.Show(xNumberEntriesText, "Invalid Numerical Entries",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void AddToHeadingList(string xHeading)
        {
            this.lstHeadings.Items.Add(xHeading);
        }

        private void EnableDisableDirectFormatOptions()
        {
            this.grpCharacter.Enabled = this.optDirectFormat.Checked;
            this.grpParagraph.Enabled = this.optDirectFormat.Checked;
        }

        private void EnableDisableEndNumber()
        {
            ////GLOG 7662 jsw 12/30/14
            ////enable txtEndingNumber only if txtStartingNumber is numeric.
            //int iTest;
            //bool bStartIsNumeric;

            //bStartIsNumeric = int.TryParse(this.txtStartingNumber.Text, out iTest);

            this.txtEndingNumber.Enabled = this.txtStartingNumber.Text != "";
            this.lblEndNumber.Enabled = this.txtStartingNumber.Text != "";

            if (this.txtStartingNumber.Text == "")
                this.txtEndingNumber.Text = "";
        }
        #endregion
        #region*********************event handlers*********************
        private void InterrogatoriesForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.LoadInterrogatoriesTypes();
                this.SetInitialValues();
                this.EnableDisableDirectFormatOptions();
                this.EnableDisableEndNumber();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate values entries.
                if (!this.IsValidNumberEntries)
                {
                    AlertInvalidHeadingNumbers();
                    
                    return;
                }
                else
                {
                    //GLOG item #5957 - dcf
                    //save value for next use
                    Session.CurrentUser.UserSettings.AlternateBetweenRogsQAndR =
                        this.chkAlternateBetweenQuestionsAndResponses.Checked;
                }

                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnUpdateFieldCodesStyles_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
        }

        private void btnAddCustom_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet oDS = GetValueSet(INTERROGATORIES_SETID);
                int iCount = oDS.Tables[0].Rows.Count;

                CustomInterrogatoryHeadingsForm oForm = new CustomInterrogatoryHeadingsForm();

                if (this.HasCustomHeadings)
                {
                    //load custom headings into form
                    oForm.CustomQuestion = this.lstHeadings.Items[0].ToString();
                    oForm.CustomResponse = this.lstHeadings.Items[1].ToString();
                }

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    if (oForm.CustomResponse != "" & oForm.CustomQuestion != "")
                    {
                        if (this.HasCustomHeadings)
                        {
                            //edit the custom headings
                            this.lstHeadings.Items[0] = oForm.CustomQuestion;
                            this.lstHeadings.Items[1] = oForm.CustomResponse;
                        }
                        else
                        {
                            //add the custom headings to the list
                            this.lstHeadings.Items.Insert(0, oForm.CustomResponse);
                            this.lstHeadings.Items.Insert(0, oForm.CustomQuestion);

                            //change button text
                            this.btnAddCustom.Text = "Edit &Custom Headings...";
                        }

                        m_bHasCustomHeadings = true;

                        this.chkAlternateBetweenQuestionsAndResponses.Checked = true;
                        this.lstHeadings.SelectedIndex = 0;
                        this.lstHeadings.Select();
                    }
                    else
                    {
                        if (this.HasCustomHeadings)
                        {
                            //delete custom headings
                            this.lstHeadings.Items.RemoveAt(1);
                            this.lstHeadings.Items.RemoveAt(0);
                            m_bHasCustomHeadings = false;
                            this.lstHeadings.SelectedIndex = 0;
                        }
                    }

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void optDirectFormat_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.EnableDisableDirectFormatOptions();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void optStyleFormat_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.EnableDisableDirectFormatOptions();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void txtStartingNumber_Validating(object sender, CancelEventArgs e)
            {
            try
            {
                if (this.txtStartingNumber.Text == "")
                    e.Cancel = false;
                else
                {
                    int i;

                    bool b = int.TryParse(this.txtStartingNumber.Text, out i);
 
                    //GLOG 7662 JSW 1/5/15
                    //set e.Cancel to false if non-numeric value,
                    //but provide explanation
                    if (b==false)
                    {
                        MessageBox.Show("Starting value must be a number.", "Invalid Numerical Entries",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    e.Cancel = !b;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstHeadings_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bool bIsQuestion = this.lstHeadings.SelectedIndex % 2 == 0;

                if (!bIsQuestion)
                    this.chkRepeatPreviousNumber.Checked = false;
    
                this.chkRepeatPreviousNumber.Enabled = !bIsQuestion;
       
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void txtStartingNumber_TextChanged(object sender, EventArgs e)
        {
            try
            {
                EnableDisableEndNumber();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnModifyStyles_Click(object sender, EventArgs e)
        {
            try
            {
                using (InterrogatoryStylesForm oForm = new InterrogatoryStylesForm())
                {
                    oForm.ShowDialog();

                    //update style definitions in the active document
                    LMP.Forte.MSWord.Interrogatories oRogs = new LMP.Forte.MSWord.Interrogatories();
                    oRogs.StyleDefinitions = LMP.MacPac.Application.GetRogStyleDefinitions();
                    oRogs.SetupStyles(LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void picHelp_Click(object sender, EventArgs e)
        {
            try
            {
                //////MessageBox.Show(LMP.Resources.GetLangString("Msg_InterrogatoriesHelp"),
                //////    LMP.ComponentProperties.ProductName,
                //////    MessageBoxButtons.OK,
                //////    MessageBoxIcon.Information);

                //GLOG : 5696 : CEH
                Help oHelp = new Help("file:///Interrogs.html");
                oHelp.Text = LMP.Resources.GetLangString("Msg_InterrogatoriesHelpTitle");
                oHelp.Show();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkAllCaps_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.chkAllCaps.Checked)
                    this.chkSmallCaps.Checked = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkSmallCaps_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.chkSmallCaps.Checked)
                    this.chkAllCaps.Checked = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkRepeatPreviousNumber_CheckedChanged(object sender, EventArgs e)
        {
            this.txtStartingNumber.Enabled = !this.chkRepeatPreviousNumber.Checked;
        }
        #endregion
    }
}