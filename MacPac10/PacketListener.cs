﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public delegate void SaveFileDelegate(Word.Document oDoc, string xClientMatterID, string xDocName, string xDocType, string xAdditionalDetail);

    public class PacketRequestListener
    {
        private IPacketBuilderHelper m_oPacketBuilderBackend;
        private int m_iPacketsCreated = 0;
        private int m_iMaxPackets = 0;

        public PacketRequestListener(int iInterval)
        {
            TimerCallback oCallback = new TimerCallback(m_oTimer_Elapsed);
            System.Threading.Timer oTimer = new System.Threading.Timer(oCallback);
            oTimer.Change(iInterval, Timeout.Infinite);

            string xMaxPacketsCreated = LMP.Registry.GetMacPac10Value("MaxPacketsBeforeReboot");
            if (!string.IsNullOrEmpty(xMaxPacketsCreated))
            {
                m_iMaxPackets = int.Parse(xMaxPacketsCreated);
            }
        }

        void m_oTimer_Elapsed(object state)
        {
            string xClientMatterID = null;
            string xPacketID = null;
            string xPrefill = null;
            string xAdditionalDetail = null;

            //allow suspension of unattended mode
            if (!LMP.MacPac.MacPacImplementation.IsServer)
            {
                return;
            }
            try
            {
                ((System.Threading.Timer)state).Change(Timeout.Infinite, 0);
                LMP.MacPac.IPacketBuilderHelper oHelper = GetPacketBuilderBackend();

                try
                {
                    //get packet detail
                    oHelper.GetPendingPacketInformation(ref xClientMatterID, ref xPacketID, ref  xPrefill, ref xAdditionalDetail);

                    if (!string.IsNullOrEmpty(xClientMatterID))
                    {
                        //insert packet
                        Segment[] aSegments = LMP.MacPac.Application.InsertSegmentPacket_Server(xPacketID, xPrefill,
                            xClientMatterID, oHelper.SaveFile, xAdditionalDetail);

                        m_iPacketsCreated++;

                        //write back to db
                        oHelper.WriteBack(true, ref xAdditionalDetail);
                    }
                }
                catch (System.Exception oE)
                {
                    object oMissing = System.Reflection.Missing.Value;
                    object oFalse = false;

                    if (LMP.Forte.MSWord.GlobalMethods.CurWordApp.Documents.Count > 0)
                    {
                        //close open documents
                        LMP.Forte.MSWord.GlobalMethods.CurWordApp
                            .Documents.Close(ref oFalse, ref oMissing, ref oMissing);
                    }

                    //write error message back to table
                    xAdditionalDetail += "|" + oE.Message + "\v" + oE.StackTrace;
                    oHelper.WriteBack(false, ref xAdditionalDetail);

                    //write message through normal Forte channel - this will
                    //send an e-mail to the administrator
                    //if Forte is configure to do so
                    LMP.Error.Show(oE, "Error in PacketRequestListener: ClientMatter/Case Number: " +
                        xClientMatterID + "; PacketID: " + xPacketID);
                }
                finally
                {
                    if (m_iMaxPackets > 0 && (m_iPacketsCreated >= m_iMaxPackets))
                    {
                        //reboot Word - shutdown here -
                        //resident monitor app will restart
                        object oFalse = false;
                        object oMissing = System.Reflection.Missing.Value;
                        LMP.Forte.MSWord.GlobalMethods.CurWordApp.Quit(
                            ref oFalse, ref oMissing, ref oMissing);
                    }

                    ((System.Threading.Timer)state).Change(5000, Timeout.Infinite);
                }
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message + ": " + oE.StackTrace);
            }
        }

        public void StartListening()
        {
            //m_oTimer.Change(10000, 10000);
        }

        public void StopListening()
        {
           // m_oTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private IPacketBuilderHelper GetPacketBuilderBackend()
        {
            if (m_oPacketBuilderBackend == null)
            {
                //retrieve assembly and class name of object to create
                string xAsmName = LMP.Registry.GetMacPac10Value("GrailAssemblyName");
                string xClassName = LMP.Registry.GetMacPac10Value("PacketBuilderClassName");

                if (string.IsNullOrEmpty(xAsmName))
                {
                    throw new LMP.Exceptions.RegistryKeyException(
                        LMP.Resources.GetLangString("Msg_MissingAsmNameRegKey"));
                }
                else if (string.IsNullOrEmpty(xClassName))
                {
                    throw new LMP.Exceptions.RegistryKeyException(
                        LMP.Resources.GetLangString("Msg_MissingClassNameRegKey"));
                }

                string xAsmDllFullName = LMP.Data.Application.AppDirectory + "\\" + xAsmName;

                Assembly oAsm = null;

                try
                {
                    //load assembly
                    oAsm = Assembly.LoadFrom(xAsmDllFullName);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.ResourceException(
                        LMP.Resources.GetLangString("Error_CouldNotLoadAssembly") + xAsmDllFullName, oE);
                }

                try
                {
                    //create class instance
                    m_oPacketBuilderBackend = (IPacketBuilderHelper)oAsm.CreateInstance(xClassName);

                    if (m_oPacketBuilderBackend == null)
                    {
                        throw new LMP.Exceptions.ResourceException(
                            LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName);
                    }

                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.ResourceException(
                        LMP.Resources.GetLangString("Error_CouldNotCreateClassInstance") + xClassName, oE);
                }
            }

            return m_oPacketBuilderBackend;
        }
    }
}
