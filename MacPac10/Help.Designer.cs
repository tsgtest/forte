namespace LMP.MacPac
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wbHelp = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // wbHelp
            // 
            this.wbHelp.AllowWebBrowserDrop = false;
            this.wbHelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbHelp.IsWebBrowserContextMenuEnabled = false;
            this.wbHelp.Location = new System.Drawing.Point(0, 0);
            this.wbHelp.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbHelp.Name = "wbHelp";
            this.wbHelp.ScriptErrorsSuppressed = true;
            this.wbHelp.Size = new System.Drawing.Size(606, 496);
            this.wbHelp.TabIndex = 0;
            this.wbHelp.WebBrowserShortcutsEnabled = false;
            // 
            // Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(606, 496);
            this.Controls.Add(this.wbHelp);
            this.Name = "Help";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Forte Help";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser wbHelp;
    }
}