using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using System.Collections;
using Word = Microsoft.Office.Interop.Word;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class PageNumberRemoveForm : Form
    {
        #region ****************fields********************
        //DataSet m_dsOffices;

        //introduce a data table for drop down values
        DataTable m_dtValues = null;

        //Fixed Page Format values
        //todo should be value set/application settings?
        string[,] m_oPageNumberRemoveOptions = new string[,] {   {"All Pages", "1"},
                                                                 {"First Page", "2"}};

        //remove variable, default to false
        bool m_bRemovePageNumber = false;

        #endregion
        #region ****************constructors********************
        public PageNumberRemoveForm()
        {
            InitializeComponent();
            InitializeApplicationContent();
        }
        #endregion
        #region *********************events*********************

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                // Do nothing and close the dialog
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            //GLOG : 6411 : CEH - Enter event isn't reached if user finishes
            //by pressing Enter - validate here as well
            if (this.rbMultiple.Checked)
            {
                if (!ValidateMultipleSectionsControl(true))
                {
                    this.DialogResult = DialogResult.None;
                    return;
                }
            } 
            
            this.Close();
        }

        private void btnRemove_Enter(object sender, EventArgs e)
        {
            if (this.rbMultiple.Checked)
                ValidateMultipleSectionsControl(true);
        }

        /// <summary>
        /// Validates txtMultiple control contents.
        /// </summary>
        private bool ValidateMultipleSectionsControl(bool bCheckForEmpty)
        {
            int iSections = Session.CurrentWordApp.ActiveDocument.Sections.Count;

            //GLOG 6107 (dm) - strip spaces
            string xMultiple = this.txtMultiple.Text.Replace(" ", "");

            //GLOG 6154 (dm) - prompt if no section specified
            if (xMultiple == "" && bCheckForEmpty == true)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_NoSectionSpecified"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                this.txtMultiple.SelectAll();
                this.txtMultiple.Focus();
                return false;
            }

            string[] xSectionList = xMultiple.Split(',');
            foreach (string xItem in xSectionList)
            {
                if (xItem.Contains("-"))
                {
                    int iPos = xItem.IndexOf("-");
                    string xStart = xItem.Substring(0, iPos);
                    if (iPos < xItem.Length)
                    {
                        string xEnd = xItem.Substring(iPos + 1);
                        if (String.IsNumericInt32(xStart) && String.IsNumericInt32(xEnd))
                        {
                            for (int i = Int32.Parse(xStart); i <= Int32.Parse(xEnd); i++)
                            {
                                //GLOG 6078 (dm) - don't accept "0"
                                if ((i > iSections) || (xStart == "0"))
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString(
                                        "Prompt_InvalidSectionSpecified") + i.ToString(),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    this.txtMultiple.SelectAll();
                                    this.txtMultiple.Focus();
                                    return false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (String.IsNumericInt32(xItem))
                    {
                        //GLOG 6078 (dm) - don't accept "0"
                        if ((Int32.Parse(xItem) > iSections) || (xItem == "0"))
                        {
                            MessageBox.Show(LMP.Resources.GetLangString(
                                "Prompt_InvalidSectionSpecified") + xItem,
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            this.txtMultiple.SelectAll();
                            this.txtMultiple.Focus();
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void rbMultiple_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtMultiple.Enabled = this.rbMultiple.Checked;
                if (this.rbMultiple.Checked == true)
                {
                    this.txtMultiple.Focus();
                }
            }
            catch { }
        }

        private void txtMultiple_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Ignore all input except numeric, '-' or ','
            //GLOG 6107 (dm) - allow spaces
            if (!((e.KeyChar >= 48 && e.KeyChar <= 57) || (char)e.KeyChar == '-' ||
                (char)e.KeyChar == ',' || (char)e.KeyChar == (char)Keys.Back ||
                e.KeyChar == 32))
            {
                e.Handled = true;
            }
        }

        #endregion
        #region *********************methods*********************
        private void InitializeApplicationContent()
        {
            // Set the data source for form drop downs

            this.cmbRemoveOptions.DataSource = GetDataTable(m_oPageNumberRemoveOptions);
            this.cmbRemoveOptions.DisplayMember = "Name";
            this.cmbRemoveOptions.ValueMember = "Value";

            //Get User Settings
            UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

            //GLOG : 6025 : CEH
            this.cmbRemoveOptions.SelectedValue = oSettings.PageNumberRemoveOption.ToString();

            if (oSettings.PageNumberRemoveScope == PageNumberScopes.Section)
                this.rbSection.Checked = true;
            else
                this.rbDocument.Checked = true;

            // Localize the form.
            LocalizeForm();

            //Get Section for current selection
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            Word.Section oSection = oDoc.Application.Selection.Range.Sections[1];
            rbSection.Text = rbSection.Text + " (Section " + oSection.Index.ToString() + ")";
        }


        // Create data source for form drop downs
        private DataTable GetDataTable(string[,] oValues)
        {
            m_dtValues = new DataTable();

            DataColumn oNameCol = new DataColumn("Name", typeof(string));
            m_dtValues.Columns.Add(oNameCol);

            DataColumn oValueCol = new DataColumn("Value", typeof(string));
            m_dtValues.Columns.Add(oValueCol);

            // Populate the table with the values.
            for (int i = 0; i < oValues.GetLength(0); i++)
            {
                this.m_dtValues.Rows.Add(oValues[i, 0], oValues[i, 1]);
            }
            return m_dtValues;
        }

        private void LocalizeForm()
        {
            this.Text = LMP.Resources.GetLangString("Dialog_PageNumberRemove_TitleBar");
            this.btnCancel.Text = LMP.Resources.GetLangString("PageNumberFormCancel");
            this.btnRemove.Text = LMP.Resources.GetLangString("PageNumberFormRemove");
            this.lblRemoveOptions.Text = LMP.Resources.GetLangString("PageNumberFormRemoveOptions");
            this.lblRemoveInfo.Text = LMP.Resources.GetLangString("PageNumberFormRemoveInfo");
            this.gbApplyTo.Text = LMP.Resources.GetLangString("PageNumberRemoveFormApplyTo");
            //this.gbOptions.Text = LMP.Resources.GetLangString("PageNumberFormOptions");
            this.rbDocument.Text = LMP.Resources.GetLangString("PageNumberFormApplyToDocumentOption");
            this.rbSection.Text = LMP.Resources.GetLangString("PageNumberFormApplyToSectionOption");
            this.rbMultiple.Text = LMP.Resources.GetLangString("PageNumberFormApplyToMultipleOption");
        }
        
        public void Finish()
        {
                //Save User Settings
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

                //GLOG : 6407 : CEH
                if (this.cmbRemoveOptions.SelectedIndex >= 0)
                    oSettings.PageNumberRemoveOption = Int32.Parse(this.cmbRemoveOptions.SelectedValue.ToString());

                if (this.rbSection.Checked)
                    oSettings.PageNumberRemoveScope = PageNumberScopes.Section;
                else
                    oSettings.PageNumberRemoveScope = PageNumberScopes.Document;

                try
                {
                    TaskPane oTP = null;
                    ForteDocument oMPDocument = null;

                    Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();

                    //get current task pane
                    try
                    {
                        oTP = TaskPanes.Item(oDoc);
                    }
                    catch { }

                    //get associated MacPac document
                    if (oTP != null)
                        oMPDocument = oTP.ForteDocument;
                    else
                    {
                        oMPDocument = new ForteDocument(oDoc);
                    }

                    //GLOG 5905 (dm) - since tags/cc may be deleted by this method,
                    //we need to disable xml events and refresh tags
                    ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                    try
                    {
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                        LMP.Forte.MSWord.PageNumbering oPageNumbering = new LMP.Forte.MSWord.PageNumbering();

                        //GLOG : 6262 : CEH
                        //grab sections
                        ArrayList aSections = new ArrayList();

                        if (this.rbSection.Checked)
                        {
                            int iSecIndex = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection.Sections[1].Index;
                            aSections.Add(iSecIndex);
                        }
                        else if (this.rbMultiple.Checked)
                        {
                            string xMultiple = this.txtMultiple.Text.Replace(" ", "");
                            string[] xSectionList = xMultiple.Split(',');
                            foreach (string xItem in xSectionList)
                            {
                                if (xItem.Contains("-"))
                                {
                                    int iPos = xItem.IndexOf("-");
                                    string xStart = xItem.Substring(0, iPos);
                                    if (iPos < xItem.Length)
                                    {
                                        string xEnd = xItem.Substring(iPos + 1);
                                        if (String.IsNumericInt32(xStart) && String.IsNumericInt32(xEnd))
                                        {
                                            for (int i = Int32.Parse(xStart); i <= Int32.Parse(xEnd); i++)
                                            {
                                                //GLOG : 6406 : CEH
                                                //do not add duplicates
                                                if (!aSections.Contains(i))
                                                    aSections.Add(i);
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    //GLOG : 6406 : CEH
                                    //do not add duplicates
                                    if (String.IsNumericInt32(xItem) && !aSections.Contains(Int32.Parse(xItem)))
                                        aSections.Add(Int32.Parse(xItem));
                                }
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveDocument.Sections.Count; i++)
                            {
                                aSections.Add(i);
                            }
                        }

                        object[] oSections = aSections.ToArray();

                        //remove
                        oPageNumbering.Remove((short)oSettings.PageNumberRemoveOption,
                                            oSections);

                        if (oMPDocument.Mode == ForteDocument.Modes.Design)
                            oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes, false);
                        else
                            oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes, false);
                    }
                    finally
                    {
                        ForteDocument.IgnoreWordXMLEvents = iEvents;
                    }

                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.MethodExecutionException(
                        LMP.Resources.GetLangString("Error_CouldNotSuccessfullyRemovePageNumber"), oE);
                }
        }
        #endregion


    }
}