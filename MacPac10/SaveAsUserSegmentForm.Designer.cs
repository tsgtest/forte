﻿namespace LMP.MacPac
{
    partial class SaveAsUserSegmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblFolder = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDisplayName = new System.Windows.Forms.Label();
            this.txtName = new LMP.Controls.TextBox();
            this.txtDisplayName = new LMP.Controls.TextBox();
            this.ftvPrefill = new LMP.Controls.FolderTreeView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ftvPrefill)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(217, 311);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(136, 311);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblFolder
            // 
            this.lblFolder.AutoSize = true;
            this.lblFolder.BackColor = System.Drawing.Color.Transparent;
            this.lblFolder.Location = new System.Drawing.Point(12, 103);
            this.lblFolder.Name = "lblFolder";
            this.lblFolder.Size = new System.Drawing.Size(39, 13);
            this.lblFolder.TabIndex = 4;
            this.lblFolder.Text = "&Folder:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 57);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "&Name:";
            // 
            // lblDisplayName
            // 
            this.lblDisplayName.AutoSize = true;
            this.lblDisplayName.Location = new System.Drawing.Point(12, 14);
            this.lblDisplayName.Name = "lblDisplayName";
            this.lblDisplayName.Size = new System.Drawing.Size(75, 13);
            this.lblDisplayName.TabIndex = 0;
            this.lblDisplayName.Text = "&Display Name:";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.IsDirty = true;
            this.txtName.Location = new System.Drawing.Point(12, 72);
            this.txtName.MaxLength = 255;
            this.txtName.Name = "txtName";
            this.txtName.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtName.Size = new System.Drawing.Size(278, 20);
            this.txtName.SupportingValues = "";
            this.txtName.TabIndex = 3;
            this.txtName.Tag2 = null;
            this.txtName.Value = "";
            this.txtName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtName_KeyPress);
            // 
            // txtDisplayName
            // 
            this.txtDisplayName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDisplayName.IsDirty = true;
            this.txtDisplayName.Location = new System.Drawing.Point(12, 30);
            this.txtDisplayName.MaxLength = 255;
            this.txtDisplayName.Name = "txtDisplayName";
            this.txtDisplayName.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtDisplayName.Size = new System.Drawing.Size(278, 20);
            this.txtDisplayName.SupportingValues = "";
            this.txtDisplayName.TabIndex = 1;
            this.txtDisplayName.Tag2 = null;
            this.txtDisplayName.Value = "";
            this.txtDisplayName.TextChanged += new System.EventHandler(this.txtDisplayName_TextChanged);
            this.txtDisplayName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDisplayName_KeyPress);
            // 
            // ftvPrefill
            // 
            appearance1.BackColor = System.Drawing.Color.White;
            this.ftvPrefill.Appearance = appearance1;
            this.ftvPrefill.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.ftvPrefill.DisableExcludedTypes = false;
            this.ftvPrefill.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders;
            this.ftvPrefill.ExcludeNonMacPacTypes = false;
            this.ftvPrefill.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvPrefill.FilterText = "";
            this.ftvPrefill.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ftvPrefill.HideSelection = false;
            this.ftvPrefill.IsDirty = false;
            this.ftvPrefill.Location = new System.Drawing.Point(12, 119);
            this.ftvPrefill.Mode = LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FolderTree;
            this.ftvPrefill.Name = "ftvPrefill";
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.ftvPrefill.Override = _override1;
            this.ftvPrefill.OwnerID = 0;
            this.ftvPrefill.ShowCheckboxes = false;
            this.ftvPrefill.ShowFindPaths = true;
            this.ftvPrefill.ShowTopUserFolderNode = false;
            this.ftvPrefill.Size = new System.Drawing.Size(278, 180);
            this.ftvPrefill.TabIndex = 5;
            this.ftvPrefill.TransparentBackground = false;
            // 
            // SaveAsUserSegmentForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(304, 348);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtDisplayName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblDisplayName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.ftvPrefill);
            this.Controls.Add(this.lblFolder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveAsUserSegmentForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Save As User Segment";
            ((System.ComponentModel.ISupportInitialize)(this.ftvPrefill)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private LMP.Controls.FolderTreeView ftvPrefill;
        private System.Windows.Forms.Label lblFolder;
        private LMP.Controls.TextBox txtName;
        private LMP.Controls.TextBox txtDisplayName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDisplayName;
    }
}