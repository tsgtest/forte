namespace LMP.MacPac
{
    partial class ImportedPeopleManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportedPeopleManager));
            this.lblPeople = new System.Windows.Forms.Label();
            this.lblLocalPeople = new System.Windows.Forms.Label();
            this.btnDeletePerson = new System.Windows.Forms.Button();
            this.btnAddPerson = new System.Windows.Forms.Button();
            this.lstPeople = new LMP.Controls.SortedListBox();
            this.lstLocalPeople = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblPeople
            // 
            this.lblPeople.AutoSize = true;
            this.lblPeople.Location = new System.Drawing.Point(11, 8);
            this.lblPeople.Name = "lblPeople";
            this.lblPeople.Size = new System.Drawing.Size(57, 15);
            this.lblPeople.TabIndex = 0;
            this.lblPeople.Text = "&All People:";
            // 
            // lblLocalPeople
            // 
            this.lblLocalPeople.AutoSize = true;
            this.lblLocalPeople.Location = new System.Drawing.Point(282, 8);
            this.lblLocalPeople.Name = "lblLocalPeople";
            this.lblLocalPeople.Size = new System.Drawing.Size(81, 15);
            this.lblLocalPeople.TabIndex = 4;
            this.lblLocalPeople.Text = "&My People List:";
            // 
            // btnDeletePerson
            // 
            this.btnDeletePerson.AutoSize = true;
            this.btnDeletePerson.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePerson.Image")));
            this.btnDeletePerson.Location = new System.Drawing.Point(225, 83);
            this.btnDeletePerson.Name = "btnDeletePerson";
            this.btnDeletePerson.Size = new System.Drawing.Size(44, 30);
            this.btnDeletePerson.TabIndex = 3;
            this.btnDeletePerson.UseVisualStyleBackColor = true;
            this.btnDeletePerson.Click += new System.EventHandler(this.btnDeleteLocalPerson_Click);
            // 
            // btnAddPerson
            // 
            this.btnAddPerson.AutoSize = true;
            this.btnAddPerson.Image = ((System.Drawing.Image)(resources.GetObject("btnAddPerson.Image")));
            this.btnAddPerson.Location = new System.Drawing.Point(225, 46);
            this.btnAddPerson.Name = "btnAddPerson";
            this.btnAddPerson.Size = new System.Drawing.Size(44, 30);
            this.btnAddPerson.TabIndex = 2;
            this.btnAddPerson.UseVisualStyleBackColor = true;
            this.btnAddPerson.Click += new System.EventHandler(this.btnAddLocalPerson_Click);
            // 
            // lstPeople
            // 
            this.lstPeople.AllowEmptyValue = false;
            this.lstPeople.FormattingEnabled = true;
            this.lstPeople.IsDirty = false;
            this.lstPeople.ItemHeight = 15;
            this.lstPeople.ListName = "";
            this.lstPeople.Location = new System.Drawing.Point(12, 24);
            this.lstPeople.Name = "lstPeople";
            this.lstPeople.Size = new System.Drawing.Size(199, 304);
            this.lstPeople.Sorted = true;
            this.lstPeople.SupportingValues = "";
            this.lstPeople.TabIndex = 1;
            this.lstPeople.Tag2 = null;
            this.lstPeople.Value = "";
            this.lstPeople.DoubleClick += new System.EventHandler(this.lstPeople_DoubleClick);
            // 
            // lstLocalPeople
            // 
            this.lstLocalPeople.FormattingEnabled = true;
            this.lstLocalPeople.ItemHeight = 15;
            this.lstLocalPeople.Location = new System.Drawing.Point(282, 24);
            this.lstLocalPeople.Name = "lstLocalPeople";
            this.lstLocalPeople.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstLocalPeople.Size = new System.Drawing.Size(199, 304);
            this.lstLocalPeople.TabIndex = 5;
            // 
            // ImportedPeopleManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lstPeople);
            this.Controls.Add(this.lstLocalPeople);
            this.Controls.Add(this.btnDeletePerson);
            this.Controls.Add(this.btnAddPerson);
            this.Controls.Add(this.lblPeople);
            this.Controls.Add(this.lblLocalPeople);
            this.Name = "ImportedPeopleManager";
            this.Size = new System.Drawing.Size(501, 351);
            this.Load += new System.EventHandler(this.ImportedPeopleManager_Load);
            this.Resize += new System.EventHandler(this.ImportedPeopleManager_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPeople;
        private System.Windows.Forms.Label lblLocalPeople;
        private System.Windows.Forms.Button btnDeletePerson;
        private System.Windows.Forms.Button btnAddPerson;
        private LMP.Controls.SortedListBox lstPeople;
        private System.Windows.Forms.ListBox lstLocalPeople;
    }
}
