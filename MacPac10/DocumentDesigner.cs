using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;
using LMP.Architect.Api;
using LMP.Controls;
using Infragistics.Win.UltraWinTree;
using Infragistics.Win;
using XML = System.Xml;
using System.Text.RegularExpressions;
using TSG.CI;   //GLOG : 8819 : ceh

namespace LMP.MacPac
{

    /// <summary>
    /// Summary description for DocumentDesigner.
	/// ********************************************************************************
	/// NOTES: 
	/// 1) in the tree, node keys have the following forms:
	/// for segment nodes, key is the tag id.  for variable nodes, key is the 
	/// parent segment tag id + "." + variable id
	/// for author nodes, key is parent segment tag id + ".Authors".
	/// 2) tree node tags contain the following object references:
	/// segment node tags contain a ref to the segment.
	/// variable node tags contain a ref to the variable.
	/// author node tags contain a ref to the segment authors.
	/// value node tags contain a ref to associated control, if there is one.
	/// ********************************************************************************
	/// </summary>
	internal partial class DocumentDesigner : System.Windows.Forms.UserControl
	{
        #region *********************constants*********************
        //empty value placeholder
        const string EMPTY_VALUE = "Null";
        const string DESIGN_HELP_PREF_NONE = "Msg_DesignPropHelpTextNone";
        const string DESIGN_HELP_PREF_SEG = "Dsgn_Seg_Prop_";
        const string DESIGN_HELP_PREF_VAR = "Dsgn_Var_Prop_";
        const string DESIGN_HELP_PREF_VARACT = "Dsgn_Var_Action_";
        const string DESIGN_HELP_PREF_BLOCK = "Dsgn_Block_Prop_";
        const string DESIGN_HELP_PREF_VARACTPROP = "Dsgn_Var_Action_Prop_";
        const string DESIGN_HELP_PREF_CTLACTPROP = "Dsgn_Ctl_Action_Prop_";
        const string DESIGN_HELP_PREF_VARACTPARAM = "Dsgn_Var_Action_Param_";
        const string DESIGN_HELP_PREF_CTLACTPARAM = "Dsgn_Ctl_Action_Param_";
        const string DESIGN_HELP_PREF_ANSWERFILESEG = "Dsgn_Answer_File_Prop_";
        #endregion
        #region *********************fields*********************
        private Control m_oCurCtl;
		private UltraTreeNode m_oCurNode;
		private TaskPane m_oTaskPane;
		private ForteDocument m_oMPDocument;
        private WebBrowser wbHelpText;
        private bool m_bProgrammaticallySelectingWordTag;
        private bool m_bDocIsDirty;
        private AddActionsMenuCaller m_iAddActionsMenuCaller;
        private bool m_bInvalidControlValue;
        private Word.ApplicationClass m_oApp;
        private bool m_IgnoreTreeViewEvents;
        private List<string> m_aUpdateList = null;
        private List<string> m_aChangedSegments = null;
        private Rectangle m_oRectForDrag = Rectangle.Empty;
        private Point m_oLastPointOnScreen;
        private UltraTreeNode m_oDraggedNode = null;
        private Point m_oMouseLocation;
        private int m_iNeighboringNodeOffset = 0;
        private const double LOWER_NODE_REGION_COEFFICIENT = 0.30;
        private const double MIDDLE_NODE_REGION_COEFFICIENT = 0.40;
        private const double UPPER_NODE_REGION_COEFFICIENT = 0.30;
        private static Cursor m_oBetweenCursor = null;
        private static bool m_bDesignUpdateInterrupted = false;
        private AnswerFileSegments m_oCurAnswerFileSegments = null;
        private bool m_bIsContentDesignerSegment = false; //GLOG 8376
        #endregion
		#region *********************enumerations*********************
        private enum mpScrollbarType
        {
            Vertical = 1,
            Horizontal = 2
        }
		private enum Images
		{
            NoImage = 9999,
			DocumentSegment = 0,
			ComponentSegment = 1,
            ParagraphTextSegment = 2,
            StyleSheetSegment = 3,
			Variable = 4,
            TaggedVariable = 5,
            SentenceTextSegment = 6,
            MasterDataForm = 7
        }
        private enum NodeTypes
        {
            Segment = 1,
            SegmentPropertiesCategory = 2,
            SegmentProperty = 3,
            SegmentPropertyValue = 4,
            SegmentActionsCategory = 5,
            SegmentEventsCategory = 6,
            SegmentAction = 7,
            SegmentActionProperty = 8,
            SegmentActionParametersCategory = 9,
            SegmentActionParameter = 10,
            VariablesCategory = 11,
            Variable = 12,
            VariableProperty = 13,
            VariablePropertyValue = 14,
            VariableActionsCategory = 15,
            VariableAction = 16,
            VariableActionProperty = 17,
            VariableActionParametersCategory = 18,
            VariableActionParameter = 19,
            VariableCICategory = 20,
            VariableCIProperty = 21,
            VariableCIPropertyValue = 22,
            ControlActionsCategory = 23,
            ControlAction = 24,
            ControlActionProperty = 25,
            ControlActionParametersCategory = 26,
            ControlActionParameter = 27,
            BlocksCategory = 28,
            Block = 29,
            BlockProperty = 30,
            BlockPropertyValue = 31,
            Value = 32,
            AuthorsCategory = 33,
            AuthorsProperty = 34,
            SegmentAuthorsControlActionsCategory = 35,
            SegmentAuthorsControlAction = 36,
            SegmentAuthorsControlActionProperty = 37,
            SegmentAuthorsControlActionParametersCategory = 38,
            SegmentAuthorsControlActionParameter = 39,
            JurisdictionCategory = 40,
            JurisdictionProperty = 41,
            SegmentJurisdictionControlActionsCategory = 42,
            SegmentJurisdictionControlAction = 43,
            SegmentJurisdictionControlActionProperty = 44,
            SegmentJurisdictionControlActionParametersCategory = 45,
            SegmentJurisdictionControlActionParameter = 46,
            SegmentDialogCategory = 47,
            SegmentDialogProperty = 48,
            AnswerFileSegmentsCategory = 49,
            AnswerFileSegment = 50,
            AnswerFileSegmentProperty = 51,
            AnswerFileSegmentPropertyValue = 52,
            LanguageCategory = 53,
            LanguageProperty = 54,
            LanguageActionsCategory = 55,
            LanguageAction = 56,
            LanguageControlActionsCategory = 57,
            LanguageControlAction = 58,
            LanguageControlActionProperty = 59,
            LanguageControlActionParametersCategory = 60,
            LanguageControlActionParameter = 61,
            AuthorsActionsCategory = 62,
            AuthorsAction = 63
        }

        private enum SegmentMenuItems
        {
            None = 0,
            Add = 1,
            Copy = 2,
            Paste = 3,
            Rename = 4,
            Delete = 5,
            Delete1 = 6,
            DeleteAll = 7,
            Refresh = 8,
            MoveUp = 9,
            MoveDown = 10
        }

        private enum VariableMenuItems
        {
            None = 0,
            Add = 1,
            Copy = 2,
            Paste = 3,
            Rename = 4,
            Delete = 5,
            Delete1 = 6,
            DeleteAll = 7,
            Refresh = 8,
            MoveUp = 9,
            MoveDown = 10
        }

        private enum BlockMenuItems
        {
            None = 0,
            Add = 1,
            Copy = 2,
            Paste = 3,
            Rename = 4,
            Delete = 5,
            Delete1 = 6,
            DeleteAll = 7,
            Refresh = 8,
            MoveUp = 9,
            MoveDown = 10
        }
        private enum AddActionsMenuCaller
        {
            None = 0,
            Variable = 1,
            Segment = 2,
            ControlAction = 3,
            AuthorsControlAction = 4,
            JurisdictionControlAction = 5,
            LanguageControlAction = 6
        }

        private enum TreeNodeMoveDirections
        {
            Up = 1,
            Down = 2
        }
        private enum NodeDropLocation
        {
            Above,
            Inside,
            Below
        }
        private enum UpdateListStatus
        {
            NothingToDo,
            Succeeded,
            Failed
        }
        #endregion
		#region *********************constructors*********************
		static DocumentDesigner()
		{
		}
        public DocumentDesigner()
		{
			InitializeComponent();
            //hook up to tree's TabPressed event
            this.treeDocContents.TabPressed +=
                new TabPressedHandler(oIControl_TabPressed);

            SetupMenus();
            this.UpdateHelpWindowView();

            // Set the backcolor and gradient for the tree per user's prefs.
            SetTreeAppearance();

            //GLOG : 8546 : ceh
            this.treeDocContents.Override.ItemHeight = (int)(16 * Session.ScreenScalingFactor);

        }

        private void SetTreeAppearance()
        {
            // Set the backcolor and gradient of the tree according to the user's prefs
            this.treeDocContents.Appearance.BackColor =
                Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;

            GradientStyle oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            //set the gradient according to the user's preference.
            this.treeDocContents.Appearance.BackGradientStyle = oGradient;
        }

		#endregion
		#region *********************properties*********************
        /// <summary>
        /// instantiates global Task Pane and Task Pane Document objects
        /// </summary>
        internal TaskPane TaskPane
		{
			get{return m_oTaskPane;}
			set
			{
				m_oTaskPane = value;
				m_oMPDocument = m_oTaskPane.ForteDocument;

                //subscribe to events of the ForteDocument
                m_oMPDocument.VariableAdded += new VariableAddedHandler(
                    m_oMPDocument_VariableAdded);
                m_oMPDocument.VariableDeleted += new VariableDeletedHandler(
                    m_oMPDocument_VariableDeleted);
                m_oMPDocument.VariableDefinitionChanged += new VariableDefinitionChangedHandler(
                    m_oMPDocument_VariableDefinitionChanged);
                m_oMPDocument.BlockAdded += new BlockAddedHandler(
                    m_oMPDocument_BlockAdded);
                m_oMPDocument.BlockDeleted += new BlockDeletedHandler(
                    m_oMPDocument_BlockDeleted);
                m_oMPDocument.BlockDefinitionChanged += new BlockDefinitionChangedHandler(
                    m_oMPDocument_BlockDefinitionChanged);
                m_oMPDocument.SegmentAdded += new SegmentAddedHandler(
                    m_oMPDocument_SegmentAdded);
                m_oMPDocument.SegmentDeleted += new SegmentDeletedHandler(
                    m_oMPDocument_SegmentDeleted);

                //subscribe to Word App DocumentBeforeClose Event
                m_oApp = (Word.ApplicationClass)m_oMPDocument.WordDocument.Application;
                m_oApp.DocumentBeforeClose += new Word.
                    ApplicationEvents4_DocumentBeforeCloseEventHandler(m_oApp_DocumentBeforeClose);
            }
		}
        /// <summary>
        /// <summary>
		/// returns true if the scroll bars are visible, else false
		/// </summary>
		public bool ScrollBarsVisible
		{
			get
			{
				//get the top level nodes of the tree
				TreeNodesCollection oNodes = this.treeDocContents.Nodes;

				//cycle through nodes, checking if last node is visible -
				//if not, scroll bars must be visible
				while(oNodes != null && oNodes.Count > 0)
				{
					//get last node in collection
					UltraTreeNode oLastNode = oNodes[oNodes.Count - 1];

					if(!oLastNode.IsInView)
						//last node is not visible -
						//scroll bars are visible
						return true;
					else
						//get child nodes of last node
						oNodes = oLastNode.Nodes;
				}

				//if we got here, all nodes are visible
				return false;
			}
		}
		/// <summary>
		/// status flag for Word Tag selected programatically
		/// </summary>
        internal bool ProgrammaticallySelectingWordTag
		{
			get{return this.m_bProgrammaticallySelectingWordTag;}
            set { this.m_bProgrammaticallySelectingWordTag = value; } 
		}

        /// <summary>
        /// flag for invalid control value
        /// </summary>
        private bool InvalidControlValue
        {
            get { return m_bInvalidControlValue; }
            set { m_bInvalidControlValue = value; }
        }
        /// <summary>
        /// sets flag used in treeDocContents event handlers
        /// </summary>
        private bool IgnoreTreeViewEvents
        {
            get { return m_IgnoreTreeViewEvents; }
            set { m_IgnoreTreeViewEvents = value; }
        }

        /// <summary>
        /// GLOG : 2989 : JAB
        /// Determine if the treeDocContent control's vertical scrollbar is visible.
        /// This is used in determining the location of the designer menu 
        /// picture control. 
        /// </summary>
        private bool IsTreeContentVerticalScrollbarVisible
        {
            get
            {
                UltraTreeNode oLastExposedNode = GetLastExposedNode(this.treeDocContents);
                return !(this.treeDocContents.Nodes[0].IsInView && oLastExposedNode.IsInView);
            }
        }
        #endregion
		#region *********************methods*********************
        /// <summary>
        /// sets menu captions using LMP resource strings
        /// </summary>
        private void SetupMenus()
        {
            // Set Text of Segment Menu
            mnuSegments_Add.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsAdd");
            mnuSegments_Copy2.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsCopy");
            mnuSegments_Paste2.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsPaste");
            mnuSegments_Delete.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsDelete");
            mnuSegments_MoveDown.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsMoveDown");
            mnuSegments_MoveUp.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsMoveUp");
            mnuSegments_Refresh.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsRefresh");
            mnuSegments_Rename.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsRename");
            mnuSegments_Assignments.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsAssignments");
            mnuSegments_ParentAssignments.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsParentAssignments");
            mnuSegments_CreateNewChildSegment.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsCreateChildSegment");
            mnuSegments_DeleteSegment.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsDeleteSegment");
            mnuSegments_TrailerAssignments.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsTrailerAssignments");
            mnuSegments_DraftStampAssignments.Text = LMP.Resources.GetLangString("Menu_DesignerSegmentsDraftStampAssignments");

            // Set Text of Variables Menu
            mnuVariables_Add.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAdd");
            mnuVariables_AddVariable.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAdd");
            mnuVariables_Copy.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesCopy");
            mnuVariables_Delete.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesDelete");
            mnuVariables_DeleteAll.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesDeleteAll");
            mnuVariables_MoveDown.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesMoveDown");
            mnuVariables_MoveUp.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesMoveUp");
            mnuVariables_Paste.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesPaste");
            mnuVariables_Refresh.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesRefresh");
            mnuVariables_Rename.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesRename");

            //JTS 5/10/10: 10.2
            //if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
            //GLOG 5500:  Use Content Control options if DefaultSaveFormat is anything other than "Doc"
            if (LMP.Forte.MSWord.GlobalMethods.CurWordApp.DefaultSaveFormat.ToUpper() == "DOC") //JTS 2/3/12: Case insensitive
                mnuVariables_AddAssociatedWordTag.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddAssociatedWordTag");
            else
                mnuVariables_AddAssociatedWordTag.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddAssociatedContentControl");
            mnuVariables_AddTaglessVariable.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddTaglessVariable"); 
            
            // Set Text of Add VariableActions Submenu
            mnuVariables_AddAction_EndExecution.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionEndExecution");
            mnuVariables_AddAction_ExecuteOnApplication.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnApplication");
            mnuVariables_AddAction_ExecuteOnBookmark.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnBookmark");
            mnuVariables_AddAction_ExecuteOnDocument.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnDocument");
            mnuVariables_AddAction_ExecuteOnPageSetup.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnPageSetup");
            mnuVariables_AddAction_ExecuteOnSegment.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnSegment"); //GLOG 7229
            mnuVariables_AddAction_ExecuteOnStyle.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnStyle");
            mnuVariables_AddAction_SetPaperSource.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionSetPaperSource");
            //10.2 - JTS 3/29/10
            if (LMP.Forte.MSWord.GlobalMethods.CurWordApp.DefaultSaveFormat.ToUpper() == "DOC")  //JTS 2/3/12: Case insensitive
                mnuVariables_AddAction_ExecuteOnTag.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnTag");
            else
                mnuVariables_AddAction_ExecuteOnTag.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnContentControl");
            mnuVariables_AddAction_ExecuteOnBlock.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionExecuteOnBlock");
            mnuVariables_AddAction_IncludeExcludeBlocks.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionIncludeExcludeBlocks");
            mnuVariables_AddAction_IncludeExcludeText.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionIncludeExcludeText");
            mnuVariables_AddAction_InsertCheckbox.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionInsertCheckbox");
            mnuVariables_AddAction_InsertDetail.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionInsertDetail");
            mnuVariables_AddAction_InsertDetailIntoTable.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionInsertDetailIntoTable");
            mnuVariables_AddAction_InsertDate.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionInsertDate");
            mnuVariables_AddAction_InsertReline.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionInsertReline");
            mnuVariables_AddAction_InsertSegment.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionInsertSegment");
            mnuVariables_AddAction_InsertText.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionInsertText");
            mnuVariables_AddAction_InsertTextAsTable.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionInsertTextAsTable");
            mnuVariables_AddAction_ReplaceSegment.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionReplaceSegment");
            mnuVariables_AddAction_RunMacro.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionRunMacro");
            mnuVariables_AddAction_RunMethod.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionRunMethod");
            mnuVariables_AddAction_RunVariableActions.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionRunVariableActions");
            mnuVariables_AddAction_SetAsDefaultValue.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionSetAsDefaultValue");
            mnuVariables_AddAction_SetDocPropValue.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionSetDocPropValue"); //GLOG 7495
            mnuVariables_AddAction_SetDocVarValue.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionSetDocVarValue");
            mnuVariables_AddAction_SetVariableValue.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionSetVariableValue");
            mnuVariables_AddAction_SetupDistributedDetailTable.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionSetupDistributedDetailTable");
            mnuVariables_AddAction_SetupLabelTable.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionSetupLabelRecipients");
            mnuVariables_AddAction_InsertTOA.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddInsertTOA");
            mnuVariables_AddAction_SetupDistributedSegmentSections.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionSetupDistributedSegmentSections");
            mnuVariables_AddAction_UpdateMemoTypePeople.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionUpdateMemoTypePeople");
            //GLOG 3220
            mnuVariables_AddAction_ApplyStyleSheet.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionApplyStyleSheet");
            //GLOG 3583
            mnuVariables_AddAction_UnderlineToLongest.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddActionUnderlineToLongest");

            // Set Text of Add ControlActions Submenu
            mnuVariables_AddControlAction_Enable.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionEnable");
            mnuVariables_AddControlAction_EndExecution.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionEndExecution");
            mnuVariables_AddControlAction_SetVariableValue.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionSetVariableValue");
            mnuVariables_AddControlAction_ChangeLabel.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionChangeLabel");
            mnuVariables_AddControlAction_RunMethod.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionRunMethod");
            mnuVariables_AddControlAction_Visible.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionVisible");
            mnuVariables_AddControlAction_SetFocus.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionSetFocus");
            mnuVariables_AddControlAction_SetDefaultValue.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionSetDefaultValue");
            mnuVariables_AddControlAction_DisplayMessage.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionDisplayMessage");
            mnuVariables_AddControlAction_RefreshControl.Text = LMP.Resources.GetLangString("Menu_DesignerVariablesAddControlActionRefreshControl");

            // Set Text of Blocks Menu
            mnuBlocks_Add.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksAdd");
            mnuBlocks_Copy.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksCopy");
            mnuBlocks_Delete.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksDelete");
            mnuBlocks_DeleteAll.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksDeleteAll");
            mnuBlocks_MoveDown.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksMoveDown");
            mnuBlocks_MoveUp.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksMoveUp");
            mnuBlocks_Paste.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksPaste");
            mnuBlocks_Refresh.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksRefresh");
            mnuBlocks_Rename.Text = LMP.Resources.GetLangString("Menu_DesignerBlocksRename");

            mnuAnswerFileSegments_Add.Text = LMP.Resources.GetLangString("Menu_DesignerAnswerFileSegmentAdd");
            mnuAnswerFileSegments_Delete.Text = LMP.Resources.GetLangString("Menu_DesignerAnswerFileSegmentDelete");
            mnuAnswerFileSegments_MoveUp.Text = LMP.Resources.GetLangString("Menu_DesignerAnswerFileSegmentMoveUp");
            mnuAnswerFileSegments_MoveDown.Text = LMP.Resources.GetLangString("Menu_DesignerAnswerFileSegmentMoveDown");
        }

        /// <summary>
        /// refreshes the document designer based on the current document
        /// </summary>
        public void RefreshTree()
        {
            // Set the backcolor and gradient of the tree according to the user's prefs
            Color oBackColor =
                Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;

            GradientStyle oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            //set the gradient according to the user's preference.
            RefreshTree(oBackColor, oGradient);
        }

        public void RefreshTree(Color oBackcolor, GradientStyle oGradient)
        {
            //get collection of expanded nodes
            List<string> oExpandedNodes = new List<string>();
            GetExpandedNodes(oExpandedNodes, this.treeDocContents.Nodes);

            this.TaskPane.ForteDocument.Refresh(ForteDocument
                .RefreshOptions.RefreshTagsWithIntegerIndexes);
            this.RefreshTopLevelNodes();

            //restore nodes to expansion state
            foreach (string xNodeKey in oExpandedNodes)
            {
                UltraTreeNode oNode = this.treeDocContents.GetNodeByKey(xNodeKey);
                if (oNode != null)
                    oNode.Expanded = true;
            }
        }

        /// <summary>
        /// Refresh the backcolor and gradient of the tree and the help view.
        /// </summary>
        /// <param name="oBackcolor"></param>
        /// <param name="oGradient"></param>
        public void RefreshAppearance(Color oBackcolor, GradientStyle oGradient)
        {
            // Display it with the appropriate size.
            this.UpdateHelpWindowView();

            // Set the backcolor and gradient for the tree per user's prefs.
            if (oBackcolor == null)
            {
                oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            }

            if (oGradient == null)
            {
                oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                    GradientStyle.BackwardDiagonal : GradientStyle.None;
            }

            if (this.treeDocContents.Appearance.BackColor != oBackcolor)
            {
                // Set the backcolor
                this.treeDocContents.Appearance.BackColor = oBackcolor;
            }

            if (this.treeDocContents.Appearance.BackGradientStyle != oGradient)
            {
                //set the gradient according to the user's preference.
                this.treeDocContents.Appearance.BackGradientStyle = oGradient;
            }
        }

        /// <summary>
        /// populates the supplied list with a list of expanded node keys
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="oNodes"></param>
        private void GetExpandedNodes(List<string> oList, TreeNodesCollection oNodes)
        {
            foreach (UltraTreeNode oNode in oNodes)
            {
                if (oNode.Expanded)
                {
                    //add node key to list
                    oList.Add(oNode.Key);

                    //add expanded child nodes
                    GetExpandedNodes(oList, oNode.Nodes);
                }
            }
        }
        /// <summary>
		/// refreshes top level nodes in tree
		/// </summary>
		public void RefreshTopLevelNodes()
		{
			DateTime t0 = DateTime.Now;

			//display top level nodes - first clear tree
			this.treeDocContents.Nodes.Clear();

			//get top level nodes
			Segments oSegments = this.m_oMPDocument.Segments;

			//cycle through nodes, adding each to tree
			for(int i=0; i<oSegments.Count; i++)
			{
				//get node
				Segment oSegment = oSegments[i];
				
				//add node to tree
				UltraTreeNode oTreeNode = this.treeDocContents.Nodes.Add(
					oSegment.FullTagID, oSegment.Definition.Name);

                //get node image to display
                Images iImage = GetSegmentImage(oSegment);

                //set appearance properties
                SetNodeAppearance(oTreeNode, true, true, true, Color.Black, iImage); 

				//add reference to segment in the tag
				oTreeNode.Tag = oSegment;

                //add child nodes for segment(s)
                RefreshSegmentNode(oTreeNode, true); 
            }
			LMP.Benchmarks.Print(t0);
		}
        private void SetPreferencesVisibility()
        {
            //Show Author Preferences button only if Admin or SegmentDesigner segment contains preferences variables
            bool bShow = false;
            if (m_oMPDocument.Segments.Count == 1)
            {
                Segment oSegment = m_oMPDocument.Segments[0];
                if ((oSegment is AdminSegment || m_bIsContentDesignerSegment) &&
                    Segment.DefinitionContainsAuthorPreferences(oSegment.Definition.XML))
                {
                    bShow = true;
                }
            }
            picPrefs.Visible = bShow;
        }
        /// <summary>
        /// selects the node with the specified tag id
        /// </summary>
        /// <param name="xTagID"></param>
		public void SelectNode(string xTagID)
		{
			try
			{
				//get node with the specified tag id
				UltraTreeNode oNode = this.GetNodeFromTagID(xTagID);

                this.treeDocContents.ActiveNode = oNode;
			}
			catch(System.Exception oE)
			{
				throw new LMP.Exceptions.UINodeException(
					LMP.Resources.GetLangString(
						"Error_CouldNotSelectDocContentsTreeNode") + xTagID, oE);
			}
		}
        /// <summary>
        /// configures default values for LMP.Controls.Spinner
        /// </summary>
        /// <param name="oCtl">LMP.Controls.Spinner</param>
        /// <param name="iNumberofPlaces">number of decimals.  0 suppresses "." key</param>
        /// <param name="dIncrement">   increment for spinner click.
        ///                             Use .01M for thousands, .1M for hundreds</param>
        /// <param name="iMaxValue">max control value</param>
        /// <param name="iMinValue">min control value</param>
        internal void ConfigureSpinner(LMP.Controls.Spinner oCtl, 
            int iNumberofPlaces, decimal dIncrement, int iMaxValue, int iMinValue)
        {
            oCtl.DecimalPlaces = iNumberofPlaces;
            oCtl.Increment = dIncrement;
            oCtl.Maximum = iMaxValue;
            oCtl.Minimum = iMinValue; 
        }
        /// <summary>
        /// overloaded method, automatically shows save prompt
        /// </summary>
        internal bool SaveDesignIfNecessary()
        {
            return this.SaveDesignIfNecessary(true, true);
        }
        /// <summary>
        /// saves the design of the current design
        /// segment if the segment is dirty
        /// </summary>
        /// <param name="bShowPrompt"></param>
        internal bool SaveDesignIfNecessary(bool bShowPrompt, bool bUpdatedRelated)
        {
            try
            {
                //prompt to save if either the current control or doc itself is
                //dirty, since edits have been made in either case
                Word.Document oDoc = m_oMPDocument.WordDocument;

                if ((!oDoc.Saved) || (m_oCurCtl != null && ((IControl)m_oCurCtl).IsDirty))
                    //there's a change to the xml of the doc - 
                    //force a save of the segment 
                    m_bDocIsDirty = true;

                //run only if design prop has been modified.  Flag set in 
                //ExitEditMode, reset to default in SaveSegmentDesign
                if (m_bDocIsDirty == true)
                {
                    //exit invalid control value - user has been messaged
                    if (this.IsValidControlValue((IControl)m_oCurCtl, m_oCurNode) == false)
                        return false;
                    
                    if (bShowPrompt == true)
                    {
                        DialogResult iRes = MessageBox.Show(
                            LMP.Resources.GetLangString("Prompt_SaveDesignChanges"),
                            LMP.String.MacPacProductName, MessageBoxButtons.YesNoCancel, 
                            MessageBoxIcon.Question);

                        if (iRes == DialogResult.Yes)
                        {
                            bool bSaveResult = SaveSegmentDesign(bUpdatedRelated,false);

                            // Update the taskpanes with the these modifications.
                            TaskPanes.RefreshAll();
                            return bSaveResult;
                        }
                        else if (iRes == DialogResult.No)
                            m_bDocIsDirty = false;
                        else
                            //Cancels close if in progress
                            return false;
                    }
                    else
                    {
                        return SaveSegmentDesign(bUpdatedRelated,false); 
                    }
                }
                return true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return false;
            }
        }
        /// <summary>
        /// wrapper from COM routine that inserts containing mSeg
        /// in all story ranges that contain text
        /// </summary>
        private void InsertmSEGInAllStoryRanges(bool bIncludeEmptyRanges, bool bNoPrompt)
        {
            //use top level node to find segment - this will ensure we get
            //a valid tag ID to pass to MSWord metnod
            Segment oSeg = GetSegmentFromNode(treeDocContents.Nodes[0]);
            if (oSeg != null)
            {
                //save app state
                LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                    m_oMPDocument.WordDocument);
                oEnv.SaveState();

                string xObjectData = null;
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    xObjectData = this.m_oMPDocument.GetAttributeValue(
                         oSeg.PrimaryWordTag, "ObjectData");
                else
                    xObjectData = this.m_oMPDocument.GetAttributeValue(
                         oSeg.PrimaryContentControl, "ObjectData");

                //get next part number and insert mSEGs
                int iStartingPartNumber = this.GetNewPartNumber(oSeg);

                //prevent xml events from being handled - this is only important
                //in 12 because we subscribe to the XMLAfterInsert event at all times -
                //in 11, we subscribe as needed due to bugs in that version
                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvent = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                int iAddedParts = LMP.Forte.MSWord.WordDoc.InsertmSEGInAllStoryRanges(
                    oSeg.FullTagID, (short)iStartingPartNumber, xObjectData, bIncludeEmptyRanges, bNoPrompt);

                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvent;

                //refresh only if part added
                if (iAddedParts > 0)
                    m_oMPDocument.Refresh(ForteDocument
                       .RefreshOptions.RefreshTagsWithIntegerIndexes);

                //restore app state
                oEnv.RestoreState(false, false, false);
            }
        }
        /// <summary>
        /// deletes the nodes with the specified tag id
        /// </summary>
        /// <param name="xTagID"></param>
        public void DeleteNode(string xTagID)
        {
            UltraTreeNode oNode = null;
            try
            {
                try
                {
                    oNode = this.GetNodeFromTagID(xTagID);
                }
                catch { }

                if (oNode != null)
                {
                    if ((this.treeDocContents.Nodes.Count == 1) &&
                        (oNode.Parent == null))
                    {
                        //this is the only node on the tree - to avoid error,
                        //use Clear() instead of Remove()
                        this.treeDocContents.Nodes.Clear();
                        //reset module level variable
                        m_oCurNode = null;
                    }
                    else
                    {
                        bool bDeletingCurrentNode = false;
                        try
                        {
                            bDeletingCurrentNode = (oNode == m_oCurNode);
                            //remove node
                            oNode.Remove();

                            if (bDeletingCurrentNode)
                                //reset module level variable if current node was deleted
                                m_oCurNode = null;
                        }
                        catch (System.ArgumentOutOfRangeException)
                        {
                            //due to a bug in UltraTreeNode, this exception may be raised (for no
                            //apparent reason) in the course of removing multiple nodes -
                            //it works to simply call the Remove() method again
                            oNode.Remove();
                            if (bDeletingCurrentNode)
                                //reset module level variable if current node was deleted
                                m_oCurNode = null;
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotSelectDocContentsTreeNode") + xTagID, oE);
            }
        }
        /// <summary>
        /// inserts node for specified variable on node for specified segment
        /// at the location indicated by its execution index
        /// </summary>
        /// <param name="oVar"></param>
        public UltraTreeNode InsertNode(Variable oVar)
        {   
            UltraTreeNode oVarNode = null;
            string xKey = null;

            //get segment node
            UltraTreeNode oSegmentNode = null;
            Segment oSegment = oVar.Segment;
            string xSegmentTagID = oSegment.FullTagID;
            try
            {
                oSegmentNode = GetNodeFromTagID(xSegmentTagID);
            }
            catch { }

            //exit if segment node does not yet exist 
            //or has not yet been populated
            if (oSegmentNode == null)
                return oVarNode;
            else if (oSegmentNode.Nodes.Count == 0)
                return oVarNode;

            //insert node at appropriate location in variables category
            xKey = xSegmentTagID + ".Variables." + oVar.ID;

            oVarNode = oSegmentNode.Nodes[xSegmentTagID + ".Variables"].Nodes
                .Insert(Math.Min(oVar.ExecutionIndex - 1, oSegmentNode.Nodes[xSegmentTagID + ".Variables"].Nodes.Count), xKey, oVar.Name); //GLOG 6658
             
            //initialize new node
            InitializeNode(oVar, oVarNode);

            return oVarNode;
        }
        /// <summary>
        /// adds node for specified block to end of node for specified segment
        /// </summary>
        /// <param name="oBlock"></param>
        public UltraTreeNode InsertNode(Block oBlock)
        {
            UltraTreeNode oBlockNode = null;

            //get segment node
            UltraTreeNode oSegmentNode = null;
            Segment oSegment = oBlock.Segment;
            string xSegmentTagID = oSegment.FullTagID;
            try
            {
                oSegmentNode = GetNodeFromTagID(xSegmentTagID);
            }
            catch { }

            //exit if segment node does not yet exist or has not yet been populated
            if (oSegmentNode == null)
                return oBlockNode;
            else if (oSegmentNode.Nodes.Count == 0)
                return oBlockNode;

            //add node
            oBlockNode = oSegmentNode.Nodes[xSegmentTagID + ".Blocks"]
                .Nodes.Add(oBlock.TagID, oBlock.Name);

            //add image
            InitializeNode(oBlock, oBlockNode);

            return oBlockNode;
        }
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oParent"></param>
        public UltraTreeNode InsertNode(Segment oSegment, Segment oParent)
        {
            //add node
            UltraTreeNode oNode = null;

            if (oParent != null)
            {
                //get parent node
                UltraTreeNode oParentNode = null;
                try
                {
                    oParentNode = GetNodeFromTagID(oParent.FullTagID);
                }
                catch { }

                //exit if segment node does not yet exist or has not yet been populated
                if (oParentNode == null)
                    return oNode;
                else if (oParentNode.Nodes.Count == 0)
                    return oNode;

                oNode = oParentNode.Nodes.Add(oSegment.FullTagID, oSegment.Definition.Name);
            }
            else
                //add to top level
                oNode = treeDocContents.Nodes.Add(oSegment.FullTagID, oSegment.Definition.Name);

            //add image
            InitializeNode(oSegment, oNode);
            //Child Admin Segments of User Segments cannot be changed in Design
            if (oParent != null && oParent.TypeID == mpObjectTypes.UserSegment && oSegment.TypeID != mpObjectTypes.UserSegment)
            {
                oNode.Enabled = false;
                oNode.Override.NodeAppearance.ForeColorDisabled = oNode.Override.NodeAppearance.ForeColor;
                oNode.Override.NodeAppearance.BackColorDisabled = oNode.Override.NodeAppearance.BackColor;
                oNode.Override.NodeAppearance.FontData.Italic = Infragistics.Win.DefaultableBoolean.True;
                oNode.Text = oNode.Text + " (Read-only)";
            }
            return oNode;
        }
        /// <summary>
        /// retags that variable and block nodes on the node for the specified segment
        /// called from TaskPane.RefreshBeforeemSegDeletion
        /// </summary>
        /// <param name="oSegment"></param>
        public void UpdateTreeNodeTags(Segment oSegment)
        {
            //get segment node
            UltraTreeNode oSegmentNode = null;
            try
            {
                oSegmentNode = GetNodeFromTagID(oSegment.FullTagID);
            }
            catch { }

            //exit if segment node does not yet exist or has not yet been populated
            if (oSegmentNode == null)
                return;
            else if (oSegmentNode.Nodes.Count == 0)
                return;

            //update tree node tags
            for (int i = oSegmentNode.Nodes.Count - 1; i >= 0; i--)
            {
                UltraTreeNode oNode = oSegmentNode.Nodes[i];
                if (oNode.Tag is Variable)
                {
                    //get variable from existing tag
                    Variable oVar = (Variable)oNode.Tag;
                    string xName = oVar.Name;
                    oVar = null;
                    try
                    {
                        //get new variable
                        oVar = oSegment.Variables.ItemFromName(xName);
                    }
                    catch { }

                    if (oVar != null)
                        //update tag
                        oNode.Tag = oVar;
                    else
                        //delete node
                        oNode.Remove();
                }
                else if (oNode.Tag is Block)
                {
                    //get block from existing tag
                    Block oBlock = (Block)oNode.Tag;
                    string xName = oBlock.Name;
                    oBlock = null;
                    try
                    {
                        //get new block
                        oBlock = oSegment.Blocks.ItemFromName(xName);
                    }
                    catch { }

                    if (oBlock != null)
                        //update tag
                        oNode.Tag = oBlock;
                    else
                        //delete node
                        oNode.Remove();
                }
            }
        }
        /// <summary>
        /// Overridden method appends variable data to containing segment object
        /// data to create tagless variable
        /// </summary>
        /// <param name="oVar"></param>
        public void StoreTaglessVariable(Segment oSeg, ref Variable oVar)
        {
            string xVarObjectData = "VariableDefinition=" + oVar.ToString(true) + "|";
            string[] aVarObjectData = xVarObjectData.Split('¦');
            if (oSeg.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
            {
                //exclude variable action definitions
                xVarObjectData = null;
                for (int i = 0; i < aVarObjectData.Length; i++)
                {
                    if (i != 14)
                        xVarObjectData += aVarObjectData[i] + "¦";
                    else
                        xVarObjectData += "¦";
                }

                xVarObjectData += "|";
            }

            string xSegObjectData = null;
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //xml tag
                xSegObjectData = this.m_oMPDocument.GetAttributeValue(
                    oSeg.PrimaryWordTag, "ObjectData");
            }
            else
            {
                //content control
                xSegObjectData = this.m_oMPDocument.GetAttributeValue(
                    oSeg.PrimaryContentControl, "ObjectData");
            }

            //dm (2/11/10) - ensure that there's a trailing pipe between variable def
            //and the preceding object data- for some reason, the object data for generic
            //letter was missing this, causing the variable defs to merge
            if (xSegObjectData.Substring(xSegObjectData.Length - 1, 1) != "|")
                xSegObjectData += "|";
            xSegObjectData += xVarObjectData;

            //set object data for each mSEG
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //xml tags
                for (int i = 0; i < oSeg.WordTags.Length; i++)
                {
                    Word.XMLNode oWordTag = oSeg.WordTags[i];

                    //append Variable objectdata to existing PrimaryWordTag object data
                    this.SetAttributeValue(oWordTag, "ObjectData", xSegObjectData);

                    //update segment tag and refresh node store
                    oSeg.ForteDocument.Tags.Update(oWordTag, false, "", "");
                }
            }
            else
            {
                //content controls - is there a reason that this branch can't be used
                //for xml tags as well? 
                oSeg.Nodes.SetItemObjectData(oSeg.FullTagID, xSegObjectData);
            }

            oSeg.Refresh(Segment.RefreshTypes.NoChildSegments);
            oVar = oSeg.Variables[oVar.ID];
        }
        /// <summary>
        /// moves segment actions and refreshes tree node
        /// </summary>
        /// <param name="MoveDirection"></param>
        private void MoveSegmentActionsNode(TreeNodeMoveDirections MoveDirection)
        {
            Segment oSeg = GetSegmentFromNode(m_oCurNode);
            SegmentAction oSegAction = GetSegmentActionFromNode(m_oCurNode);

            //reindex the segment actions
            if (MoveDirection == TreeNodeMoveDirections.Up)
                oSeg.Actions.MoveUp(oSegAction);
            else
                oSeg.Actions.MoveDown(oSegAction);


            //capture key of current node
            string xKey = m_oCurNode.Key;
            //capture # of VA nodes
            int iCount = m_oCurNode.Parent.Nodes.Count;
            //capture expanded state
            bool bExpanded = m_oCurNode.Expanded;

            //refresh segment event actions nodes
            if ((m_oCurNode.Parent.Text == "Language Actions") ||
                (m_oCurNode.Parent.Text == "Authors Actions"))
            {
                //add directly to root node
                this.AddSegmentEventNodes(oSeg, m_oCurNode.Parent);
            }
            else
                this.AddSegmentEventNodes(oSeg, m_oCurNode.Parent.Parent);

            //recreate node variable, necessary after refresh
            int iLastIndex = xKey.LastIndexOf(".");
            int iIndex = 0;

            if (MoveDirection == TreeNodeMoveDirections.Up)
            {
                //we've moved up, so we will de-increment the index bit
                //contained in the key
                iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) - 1;
                if (iIndex >= 0)
                    xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
            }
            else
            {
                //we've moved down, so we will increment the index bit
                //contained in the key
                iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) + 1;
                if (iIndex < iCount)
                    xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
            }

            //refresh the treenode variable pointer
            //with revised key
            m_oCurNode = treeDocContents.GetNodeByKey(xKey);

            //ensure actions collection displays in expanded form
            m_oCurNode.Parent.Expanded = true;

            //select and expand moved node
            treeDocContents.ActiveNode = m_oCurNode;
            m_oCurNode.Expanded = bExpanded;
            if (bExpanded == true)
                m_oCurNode.ExpandAll(ExpandAllType.OnlyNodesWithChildren);
        }
        private void MoveVariablesOrActionsNode(TreeNodeMoveDirections MoveDirection)
        {
            switch (MoveDirection)
            {
                case TreeNodeMoveDirections.Up:
                    MoveVariablesOrActionsNode(m_oCurNode, -1);
                    break;
                case TreeNodeMoveDirections.Down:
                    MoveVariablesOrActionsNode(m_oCurNode, 1);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// moves variable or variable actions and refreshes tree node
        /// </summary>
        /// <param name="MoveDirection"></param>
        private void MoveVariablesOrActionsNode(UltraTreeNode oNode, int iOffset)
        {
            Segment oSeg = GetSegmentFromNode(oNode);
            Variable oVar = GetVariableFromNode(oNode);
            Variable oVar2 = null;

            //capture key of current node
            string xKey = oNode.Key;

            //capture expanded state
            bool bExpanded = oNode.Expanded;

            if (NodeType(oNode) == NodeTypes.Variable)
            {
                //change execution indices
                int iStartInd = oNode.Index;
                int iTempInd = 9999;

                if (iOffset < 0)
                {
                    int iNewInd = iStartInd + 1;
                    //move variable up
                    if (iStartInd + iOffset >= 0)
                    {
                        oVar.ExecutionIndex = iTempInd;
                        for (int i = -1; i >= iOffset; i--)
                        {
                            //Move all Variables between old and new Index down one position
                            oVar2 = oSeg.Variables.ItemFromIndex(iStartInd + i);
                            oVar2.ExecutionIndex = iNewInd--;
                            oSeg.Variables.Save(oVar2);
                        }
                        oVar.ExecutionIndex = iStartInd + 1 + iOffset;
                        oSeg.Variables.Save(oVar);
                    }
                    else
                        return;
                }
                else if (iOffset > 0)
                {
                    //move variable down
                    int iNewInd = iStartInd + 1;
                    if (iStartInd + iOffset < oNode.Parent.Nodes.Count)
                    {
                        oVar.ExecutionIndex = iTempInd;
                        for (int i = 1; i <= iOffset; i++)
                        {
                            //Move all Variables between old and new Index down one position
                            oVar2 = oSeg.Variables.ItemFromIndex(iStartInd + i);
                            oVar2.ExecutionIndex = iNewInd++;
                            oSeg.Variables.Save(oVar2);
                        }
                        oVar.ExecutionIndex = iStartInd + iOffset + 1;
                        oSeg.Variables.Save(oVar);
                    }
                    else
                        return;
                }
                else
                    //no change
                    return;
                //refresh tags to ensure latest ObjectData is reflected
                oSeg.Refresh();
                RefreshSegmentNode(oNode.Parent.Parent, true);
            }
            else if (NodeType(oNode) == NodeTypes.ControlAction)
            {
                ControlAction oCtlAction = GetControlActionFromNode(oNode);
                //we are moving a control action in this branch
                int iLastIndex = xKey.LastIndexOf(".");
                if (iOffset < 0)
                {
                    //control actions are handled with ControlActions method.
                    oVar.ControlActions.MoveUp(oCtlAction);

                    //refresh actions nodes
                    this.AddControlActionNodes(oNode.Parent, oVar);

                    //we've moved up, so we will de-increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var
                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) - 1;
                    if (iIndex >= 0)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                else
                {

                    //control actions are handled with ControlActions method.
                    oVar.ControlActions.MoveDown(oCtlAction);

                    int iCount = oNode.Parent.Nodes.Count;

                    //refresh actions nodes
                    this.AddControlActionNodes(oNode.Parent, oVar);

                    //we've moved up, so we will increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var

                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) + 1;
                    if (iIndex < iCount)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }

            }

            //handle segment authors control actions
            else if (NodeType(oNode) == NodeTypes.SegmentAuthorsControlAction)
            {
                ControlAction oCtlAction = GetControlActionFromNode(oNode);
                //we are moving a control action in this branch
                int iLastIndex = xKey.LastIndexOf(".");
                if (iOffset < 0)
                {
                    //control actions are handled with ControlActions method.
                    oSeg.Authors.ControlActions.MoveUp(oCtlAction);

                    //refresh actions nodes
                    this.AddSegmentControlActionNodes(oSeg.Authors.ControlActions,
                        oNode.Parent);

                    //we've moved up, so we will de-increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var
                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) - 1;
                    if (iIndex >= 0)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                else
                {
                    //control actions are handled with ControlActions method.
                    oSeg.Authors.ControlActions.MoveDown(oCtlAction);

                    int iCount = oNode.Parent.Nodes.Count;

                    //refresh actions nodes
                    this.AddSegmentControlActionNodes(oSeg.Authors.ControlActions, oNode.Parent);

                    //we've moved up, so we will increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var

                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) + 1;
                    if (iIndex < iCount)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                //save the segment authors control action XML
                SaveSegmentAuthorsControlActions(oSeg);
            
            }
            //handle segment jurisdiction control actions
            else if (NodeType(oNode) == NodeTypes.SegmentJurisdictionControlAction)
            {
                ControlAction oCtlAction = GetControlActionFromNode(oNode);
                //we are moving a control action in this branch
                int iLastIndex = xKey.LastIndexOf(".");
                if (iOffset < 0)
                {
                    //control actions are handled with ControlActions method.
                    oSeg.CourtChooserControlActions.MoveUp(oCtlAction);

                    //refresh actions nodes
                    this.AddSegmentControlActionNodes(oSeg.CourtChooserControlActions,
                        oNode.Parent);

                    //we've moved up, so we will de-increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var
                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) - 1;
                    if (iIndex >= 0)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                else
                {
                    //control actions are handled with ControlActions method.
                    oSeg.CourtChooserControlActions.MoveDown(oCtlAction);

                    int iCount = oNode.Parent.Nodes.Count;

                    //refresh actions nodes
                    this.AddSegmentControlActionNodes(oSeg.CourtChooserControlActions,
                        oNode.Parent);

                    //we've moved up, so we will increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var

                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) + 1;
                    if (iIndex < iCount)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                //save the segment authors control action XML
                SaveSegmentJurisdictionControlActions(oSeg);

            }
            //handle segment language control actions
            else if (NodeType(oNode) == NodeTypes.LanguageControlAction)
            {
                ControlAction oCtlAction = GetControlActionFromNode(oNode);
                //we are moving a control action in this branch
                int iLastIndex = xKey.LastIndexOf(".");
                if (iOffset < 0)
                {
                    //control actions are handled with ControlActions method.
                    oSeg.LanguageControlActions.MoveUp(oCtlAction);

                    //refresh actions nodes
                    this.AddSegmentControlActionNodes(oSeg.LanguageControlActions,
                        oNode.Parent);

                    //we've moved up, so we will de-increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var
                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) - 1;
                    if (iIndex >= 0)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                else
                {
                    //control actions are handled with ControlActions method.
                    oSeg.LanguageControlActions.MoveDown(oCtlAction);

                    int iCount = oNode.Parent.Nodes.Count;

                    //refresh actions nodes
                    this.AddSegmentControlActionNodes(oSeg.LanguageControlActions,
                        oNode.Parent);

                    //we've moved up, so we will increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var

                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) + 1;
                    if (iIndex < iCount)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                //save the segment authors control action XML
                SaveSegmentLanguageControlActions(oSeg);

            }
            else
            {
                VariableAction oVarAction = GetVariableActionFromNode(oNode);
                //we are moving a variable action in this branch
                int iLastIndex = xKey.LastIndexOf(".");
                if (iOffset < 0)
                {
                    //variable actions are handled with VariableActions method.
                    oVar.VariableActions.MoveUp(oVarAction);

                    //refresh actions nodes
                    this.AddVariableActionNodes(oNode.Parent, oVar);

                    //we've moved up, so we will de-increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var
                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) - 1;
                    if (iIndex >= 0)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                else
                {

                    //variable actions are handled with VariableActions method.
                    oVar.VariableActions.MoveDown(oVarAction);

                    int iCount = oNode.Parent.Nodes.Count;

                    //refresh actions nodes
                    this.AddVariableActionNodes(oNode.Parent, oVar);

                    //we've moved up, so we will increment the index bit
                    //contained in the key - new key will be used below to repoint treenode var

                    int iIndex = Int32.Parse(xKey.Substring(iLastIndex + 1)) + 1;
                    if (iIndex < iCount)
                        xKey = xKey.Substring(0, iLastIndex + 1) + iIndex.ToString();
                }
                // GLOG : 3430 : JAB
                // Save the variable to save the order of the variable actions.
                oSeg.Variables.Save(oVar);
            }

            //recreate valid node variable pointer
            oNode = treeDocContents.GetNodeByKey(xKey);

            //ensure desired collection displays in expanded form
            oNode.Parent.Expanded = true;

            //select and expand moved node
            treeDocContents.ActiveNode = oNode;
            oNode.Expanded = bExpanded;
            if (bExpanded == true)
                oNode.ExpandAll(ExpandAllType.OnlyNodesWithChildren);
            
            // GLOG : 3430 : JAB
            // Set the field which indicates that the document design has been modified.
            m_bDocIsDirty = true;
        }
        /// <summary>
        /// overloaded method - refreshes segment node
        /// </summary>
        /// <param name="oNode"></param>
        private void RefreshSegmentNode(UltraTreeNode oNode)
        {
            this.RefreshSegmentNode(oNode, false);
        }
        /// <summary>
        /// refreshes the portion of the tree
        /// starting with the specified segment tree node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh">forces the node to refresh, even if the node already has children</param>
        private void RefreshSegmentNode(UltraTreeNode oSegmentNode, bool bForceRefresh)
        {
            DateTime t0 = DateTime.Now;
            Segment oSegment = null;
            UltraTreeNode oVarCatNode = null;
            UltraTreeNode oBlockCatNode = null;
            UltraTreeNode oSegPropCatNode = null;
            UltraTreeNode oSegActionsCatNode = null;
            UltraTreeNode oSegJurisdictionNode = null;
            UltraTreeNode oSegAuthorsCatNode = null;
            UltraTreeNode oSegLanguageCatNode = null;
            UltraTreeNode oSegDialogCatNode = null;
            UltraTreeNode oSegAnswerFileSegmentsNode = null;
            TreeNodesCollection oSegNodes = oSegmentNode.Nodes;
            char cSep = '.';

            //exit if node is the same
            if (oSegmentNode == this.m_oCurNode && !bForceRefresh)
                return;

            //get referred segment
            oSegment = oSegmentNode.Tag as Segment;
            string xSegID = oSegment.FullTagID;

            if (oSegment == null)
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_NodeIsNotSegmentNode"));

            //clear any existing child nodes
            oSegmentNode.Nodes.Clear();
            try
            {
                this.treeDocContents.SuspendLayout();

                try
                {
                    //create and configure category nodes for AdminSegments
                    //and, if User is a Power User, User Segments
                    if (oSegment is AdminSegment || m_bIsContentDesignerSegment)
                    {
                        //GLOG : 8406 : JSW
                        //Allow property nodes for MasterData
                        if (!(oSegment is CollectionTable))
                        {
                            oSegPropCatNode = oSegNodes.Add(xSegID + cSep + "Properties", "Properties");
                            //Expand Properties node for Style Sheet
                            SetNodeAppearance(oSegPropCatNode, true, oSegment.IntendedUse == mpSegmentIntendedUses.AsStyleSheet,
                                true, Color.Black, Images.NoImage);
                        }

                        //Don't show these nodes for Style Sheet
                        if (oSegment.IntendedUse != mpSegmentIntendedUses.AsStyleSheet)
                        {
                            if ((oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile ||
                                oSegment.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm) &&
                                (!(oSegment is CollectionTable)))
                                {
                                //only certain Admin Segment types will allow jurisdiction chooser
                                if (oSegment.LevelChooserRequired())
                                {
                                    oSegJurisdictionNode = oSegNodes.Add(xSegID + cSep + "Jurisdiction", 
                                        "Court");
                                    SetNodeAppearance(oSegJurisdictionNode, true, false, true,
                                        Color.Black, Images.NoImage);
                                }

                            }
                            if (oSegment.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                            {
                                //Setup Dialog Properties Node
                                oSegDialogCatNode = oSegNodes.Add(xSegID + cSep + "DialogOptions", "Dialog Options");
                                SetNodeAppearance(oSegDialogCatNode, true, false, true, Color.Black, Images.NoImage);

                            }
                            if (!(oSegment is CollectionTable) && oSegment.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm)
                                {
                                //Add Authors category node
                                oSegAuthorsCatNode = oSegNodes.Add(xSegID + cSep + "Authors", "Authors");
                                SetNodeAppearance(oSegAuthorsCatNode, true, false, true,
                                    Color.Black, Images.NoImage);

                                //add Language category node
                                oSegLanguageCatNode = oSegNodes.Add(xSegID + cSep + "Language", "Language");
                                SetNodeAppearance(oSegLanguageCatNode, true, false, true,
                                    Color.Black, Images.NoImage);
                            }
                            
                            if (oSegment.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                            {
                                //Setup Answer File Segments
                                oSegAnswerFileSegmentsNode = oSegNodes.Add(xSegID + cSep + "AnswerFileSegments", "Target Segments");
                                SetNodeAppearance(oSegAnswerFileSegmentsNode, true, true, true, Color.Black, Images.NoImage);
                            }

                            oVarCatNode = oSegNodes.Add(xSegID + cSep + "Variables", "Variables");
                            //variables node is expanded
                            SetNodeAppearance(oVarCatNode, true, true, true,
                                Color.Black, Images.NoImage);

                            if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile && 
                                oSegment.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm)
                            {
                                oBlockCatNode = oSegNodes.Add(xSegID + cSep + "Blocks", "Blocks");
                                SetNodeAppearance(oBlockCatNode, true, false, true,
                                    Color.Black, Images.NoImage);

                                if (!(oSegment is CollectionTable))
                                {
                                    oSegActionsCatNode = oSegNodes.Add(xSegID + cSep + "SegmentActions", "Segment Actions");
                                    SetNodeAppearance(oSegActionsCatNode, true, false, true,
                                        Color.Black, Images.NoImage);
                                }
                            }
                        }
                    }
                    else
                    {
                        oSegPropCatNode = oSegNodes.Add(xSegID + cSep + "Properties", "Properties");
                        SetNodeAppearance(oSegPropCatNode, true, false, true,
                            Color.Black, Images.NoImage);

                        //Don't show these nodes for Style Sheet
                        if (oSegment.IntendedUse != mpSegmentIntendedUses.AsStyleSheet)
                        {
                            //create and configure category nodes for UserSegments 
                            oVarCatNode = oSegNodes.Add(xSegID + cSep + "Variables", "Variables");
                            //variables node is expanded
                            SetNodeAppearance(oVarCatNode, true, true, true,
                                Color.Black, Images.NoImage);

                            oBlockCatNode = oSegNodes.Add(xSegID + cSep + "Blocks", "Blocks");
                            SetNodeAppearance(oBlockCatNode, true, false, true,
                                Color.Black, Images.NoImage);
                        }
                    }
                }
                catch (Exception oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                       xSegID, oE);
                }

                try
                {
                    //add variable, block and child segments to each category node
                    if (oSegment.IntendedUse != mpSegmentIntendedUses.AsStyleSheet)
                    {
                        //GLOG : 8406 :JSW
                        //alow childsegment nodes for MasterDataForm
                        if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile )
                        {
                            AddSegmentChildSegmentNodes(oSegment, oSegmentNode);
                        }

                        AddSegmentVariableNodes(oSegment, oVarCatNode);

                        if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile &&
                            oSegment.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm)
                        {
                            AddSegmentBlockNodes(oSegment, oBlockCatNode);
                        }
                    }

                    //add segment property nodes
                    //GLOG : 8406 :JSW
                    //Allow properties for master data
                    if (!(oSegment is CollectionTable))
                        AddSegmentPropertyNodes(oSegment, oSegPropCatNode);

                    //add dialog property nodes
                    if (oSegment.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                    {
                        AddSegmentDialogPropertyNodes(oSegment, oSegDialogCatNode);
                        AddSegmentAnswerFileSegmentsNodes(oSegment, oSegAnswerFileSegmentsNode);
                    }

                    //add child nodes to category nodes - these are for Admin Segments only
                    if ((oSegment is AdminSegment || m_bIsContentDesignerSegment)
                        && (oSegment.IntendedUse != mpSegmentIntendedUses.AsStyleSheet) &&
                        (oSegment.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm) &&
                        (!(oSegment is CollectionTable)))
                    {
                        if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
                            AddSegmentJurisdictionPropertyNodes(oSegment, oSegJurisdictionNode);
                        AddSegmentAuthorsPropertyNodes(oSegment, oSegAuthorsCatNode);
                        AddSegmentLanguagePropertyNodes(oSegment, oSegLanguageCatNode);
                        if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
                            AddSegmentEventNodes(oSegment, oSegActionsCatNode);
                    }
                }
                catch (SystemException oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                       "Segment child nodes", oE);
                }
                SetPreferencesVisibility();
            }
            finally
            {
                this.treeDocContents.ResumeLayout();
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// adds property treenodes for designated segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oPropNode"></param>
        private void AddSegmentPropertyNodes(Segment oSegment, UltraTreeNode oPropCatNode)
        {
            UltraTreeNode oPropNode = null;
            string xSegID = oSegment.FullTagID;
            char cSep = '.';

            //build storage of property names: array of property name, display name
            //admin segments and user segments contain different properties
            string[,] oProps = null;; 

            //UserSegment properties are currently not exposed
            if (oSegment is UserSegment && !m_bIsContentDesignerSegment)
                oProps = GetUserSegmentPropertyNameArray();
            else
            {
                oProps = GetAdminSegmentPropertyNameArray(oSegment);
            }
            

            //cycle through props array and add/configure prop and prop value nodes
            for (int i = 0; i <= oProps.GetUpperBound(0); i++)
            {
                //add property node
                oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + oProps[i, 0], oProps[i, 1]);

                //configure node
                SetNodeAppearance(oPropNode, false, true, true,
                    Color.Black, Images.NoImage);

                //add design property name to the node's tag
                oPropNode.Tag = oProps[i, 0];

                string xValue = null;
                string xPropName = oProps[i, 1];

                try
                {
                    xValue = GetSegmentPropertyValue(oSegment, xPropName, false);
                }
                catch { }

                //replace empty value with standard designator
                xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

                //trim value for UI - Jurisdictional Domain and others below will have a longer string
                if (xValue.Length > 30)
                {
                    if (xPropName != "Insertion Locations" &&
                        xPropName != "Insertion Behavior" &&
                        xPropName != "Default Drag Location" &&
                        xPropName != "Default Drag Behavior" &&
                        xPropName != "Default Click Location" &&
                        xPropName != "Default Click Behavior")
                    {
                        xValue = xValue.Substring(0, 30) + "...";
                    }
                }

                LMP.Trace.WriteNameValuePairs("Property: " + oProps[i, 0], xValue);

                //add value node
                UltraTreeNode oValueNode = oPropNode.Nodes.Add(
                    oPropNode.Key + "_Value", xValue);

                //set value node appearance
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, 1, 10);

                //GLOG 3277: Always display single line for Value
                ////set node  to multiline if necessary
                //if ((xPropName == "Insertion Locations") ||
                //    (xPropName == "Insertion Behavior") ||
                //    (xPropName == "Default Drag Location") ||
                //    (xPropName == "Default Drag Behavior") ||
                //    (xPropName == "Default Click Location") ||
                //    (xPropName == "Default Click Behavior"))

                //    oValueNode.Override.Multiline = Infragistics.Win.DefaultableBoolean.True;
            }
        }
        /// <summary>
        /// adds author properties nodes
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oPropNode"></param>
        private void AddSegmentAuthorsPropertyNodes(Segment oSegment, UltraTreeNode oPropCatNode)
        {
            UltraTreeNode oPropNode = null;
            string xSegID = oSegment.FullTagID;
            char cSep = '.';
            string xValue = null;

            //cycle through props array and add/configure prop and prop value nodes
            //add MaxAuthors property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "MaxAuthors", "Maximum Authors");

            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

            //add design property name to the node's tag
            oPropNode.Tag = "MaxAuthors";

            xValue = oSegment.MaxAuthors.ToString();
            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            UltraTreeNode oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);
            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //add AuthorsUILabel property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "AuthorNodeUILabel", "Author Node UI Label");
            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);
            //add design property name to the node's tag
            oPropNode.Tag = "AuthorNodeUILabel";

            xValue = oSegment.AuthorsNodeUILabel.ToString();
            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //add AuthorsHelpText property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "AuthorsHelpText", "Authors Help Text");
            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);
            //add design property name to the node's tag
            oPropNode.Tag = "AuthorsHelpText";

            xValue = oSegment.AuthorsHelpText;
            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //add AuthorControl properties node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "AuthorControlProperties", 
                "Author Control Properties");
            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);
            
            //add design property name to the node's tag
            oPropNode.Tag = "AuthorControlProperties";

            xValue = oSegment.AuthorControlProperties;

            //replace empty value with standard designator if control properties grid 
            //default value for Author selector is null
            if (System.String.IsNullOrEmpty(xValue))
            {
                LMP.Controls.ControlPropertiesGrid oGrid = new ControlPropertiesGrid();
                xValue = oGrid.GetDefaultValues(LMP.Data.mpControlTypes.AuthorSelector);
            }

            //set this particular property = default value
            oSegment.AuthorControlProperties = xValue;
            //replace delimiters with escaped lf chars
            //GLOG 3277: Display single line
            xValue = xValue.Replace(StringArray.mpEndOfSubValue.ToString(), "\r\n");
            if (xValue.Contains("\r\n"))
                xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);
            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //set node  to multiline
            //oValueNode.Override.Multiline = Infragistics.Win.DefaultableBoolean.True;

            if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
            {
                //add authors actions node
                UltraTreeNode oSegActionsCatNode = oPropCatNode.Nodes.Add(
                    xSegID + cSep + "AuthorsActions", "Authors Actions");
                SetNodeAppearance(oSegActionsCatNode, true, false, true,
                    Color.Black, Images.NoImage);
                AddSegmentEventNodes(oSegment, oSegActionsCatNode);

                //add author control actions category node
                UltraTreeNode oCtlActionsCatNode = oPropCatNode.Nodes.Add(oPropNode.Key + cSep + "ControlActions", "Control Actions");
                SetNodeAppearance(oCtlActionsCatNode, true, false, true,
                    Color.Black, Images.NoImage);

                //add author control actions nodes
                AddSegmentControlActionNodes(oSegment.Authors.ControlActions, oCtlActionsCatNode);
            }
        }

        /// <summary>
        /// adds language properties nodes
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oPropNode"></param>
        private void AddSegmentLanguagePropertyNodes(Segment oSegment, UltraTreeNode oPropCatNode)
        {
            UltraTreeNode oPropNode = null;
            string xSegID = oSegment.FullTagID;
            char cSep = '.';
            string xValue = null;

            //add supported languages property
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "SupportedLanguages", "Supported Languages");

            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

            //add design property name to the node's tag
            oPropNode.Tag = "SupportedLanguages";

            //add value node
            xValue = LMP.Data.Application.GetLanguagesDisplayValue(oSegment.SupportedLanguages);
            UltraTreeNode oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //add culture property
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "Culture", "Default Language");

            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

            //add design property name to the node's tag
            oPropNode.Tag = "Culture";

            //add value node
            xValue = LMP.Data.Application.GetLanguagesDisplayValue(oSegment.Culture.ToString());
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
            {
                //add language actions node
                UltraTreeNode oSegActionsCatNode = oPropCatNode.Nodes.Add(
                    xSegID + cSep + "LanguageActions", "Language Actions");
                SetNodeAppearance(oSegActionsCatNode, true, false, true,
                    Color.Black, Images.NoImage);
                AddSegmentEventNodes(oSegment, oSegActionsCatNode);

                //add author control actions category node
                UltraTreeNode oCtlActionsCatNode = oPropCatNode.Nodes.Add(xSegID +
                    cSep + "LanguageControlProperties" + cSep + "ControlActions",
                    "Control Actions");
                SetNodeAppearance(oCtlActionsCatNode, true, false, true,
                    Color.Black, Images.NoImage);

                //add author control actions nodes
                AddSegmentControlActionNodes(oSegment.LanguageControlActions,
                    oCtlActionsCatNode);
            }
        }

        /// <summary>
        /// adds author properties nodes
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oPropNode"></param>
        private void AddSegmentJurisdictionPropertyNodes(Segment oSegment, UltraTreeNode oPropCatNode)
        {
            UltraTreeNode oPropNode = null;
            string xSegID = oSegment.FullTagID;
            char cSep = '.';
            string xValue = null;

            if (oPropCatNode == null)
                return;

            //add ShowCourtChooser property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "ShowCourtChooser", "Show Court Chooser");

            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

            //add design property name to the node's tag
            oPropNode.Tag = "ShowCourtChooser";

            xValue = oSegment.ShowCourtChooser.ToString();

            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            UltraTreeNode oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);
            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //add Jurisdiction property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "JurisdictionDefaults", "Court Defaults");

            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

            //add design property name to the node's tag
            oPropNode.Tag = "JurisdictionDefaults";

            //GLOG 3277: Display on one line
            xValue = oSegment.GetJurisdictionText().Replace("\r\n", "/");

            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);
            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);
            //set node  to multiline
            //oValueNode.Override.Multiline = Infragistics.Win.DefaultableBoolean.True;

            //add CourtChooserUILabel property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "CourtChooserUILabel", "Court Chooser UI Label");
            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);
            //add design property name to the node's tag
            oPropNode.Tag = "CourtChooserUILabel";

            xValue = oSegment.CourtChooserUILabel.ToString();
            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //add CourtChooserHelpText property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "CourtChooserHelpText", "Court Chooser Help Text");
            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);
            //add design property name to the node's tag
            oPropNode.Tag = "CourtChooserHelpText";

            xValue = oSegment.CourtChooserHelpText;
            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //add AuthorControl properties node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "CourtChooserControlProperties",
                "Court Chooser Control Properties");
            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

            //add design property name to the node's tag
            oPropNode.Tag = "CourtChooserControlProperties";

            xValue = oSegment.CourtChooserControlProperties;

            //replace empty value with standard designator if control properties grid 
            //default value for Court Chooser is null
            if (System.String.IsNullOrEmpty(xValue))
            {
                LMP.Controls.ControlPropertiesGrid oGrid = new ControlPropertiesGrid();
                xValue = oGrid.GetDefaultValues(LMP.Data.mpControlTypes.JurisdictionChooser);
            }

            //set this particular property = default value
            oSegment.CourtChooserControlProperties = xValue;
            //GLOG 3277: Display on single line
            xValue = xValue.TrimEnd(StringArray.mpEndOfSubValue);
            if (xValue.Contains(StringArray.mpEndOfSubValue.ToString()))
                xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);
            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //set node  to multiline
            //oValueNode.Override.Multiline = Infragistics.Win.DefaultableBoolean.True;

            //add juridiction control actions category node
            UltraTreeNode oCtlActionsCatNode = oPropCatNode.Nodes.Add(oPropNode.Key + cSep + "ControlActions", "Control Actions");
            SetNodeAppearance(oCtlActionsCatNode, true, false, true,
                Color.Black, Images.NoImage);

            //add author control actions nodes
            AddSegmentControlActionNodes(oSegment.CourtChooserControlActions, oCtlActionsCatNode);
        }
        /// <summary>
        /// adds author properties nodes
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oPropNode"></param>
        private void AddSegmentDialogPropertyNodes(Segment oSegment, UltraTreeNode oPropCatNode)
        {
            UltraTreeNode oPropNode = null;
            UltraTreeNode oValueNode = null;
            string xSegID = oSegment.FullTagID;
            char cSep = '.';
            string xValue = null;

            if (oPropCatNode == null)
                return;

            //Dialog not optional for AnswerFile            
            if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
            {
                //add ShowSegmentDialog property node
                oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "ShowSegmentDialog", "Show Segment Dialog");

                //configure node
                SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

                //add design property name to the node's tag
                oPropNode.Tag = "ShowSegmentDialog";

                xValue = oSegment.ShowSegmentDialog.ToString();

                //replace empty value with standard designator
                xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

                //add value node
                oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);
                //set value node appearance
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);
            }
            //add DialogCaption property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "DialogCaption", "Dialog Caption");
            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);
            //add design property name to the node's tag
            oPropNode.Tag = "DialogCaption";

            xValue = oSegment.DialogCaption;

            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            //add DialogTabCaptions property node
            oPropNode = oPropCatNode.Nodes.Add(xSegID + cSep + "DialogTabCaptions", "Dialog Tab Captions");

            //configure node
            SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

            //add design property name to the node's tag
            oPropNode.Tag = "DialogTabCaptions";

            xValue = oSegment.DialogTabCaptions;

            //replace empty value with standard designator
            xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

            //add value node
            oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);
            //set value node appearance
            SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

        }
        public void AddSegmentAnswerFileSegmentsNodes(Segment oSegment, UltraTreeNode oRootNode)
        {
            
            if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
                return;
            AdminSegment oASegment = (AdminSegment)oSegment;
            char cSep = '.';
            string xRootKey = oRootNode.Key;
            //GLOG 3904: Store AnswerFileSegments in private variable to
            //avoid losing unsaved items during refresh
            if (m_oCurAnswerFileSegments == null)
                m_oCurAnswerFileSegments = oASegment.AnswerFileSegments;

            if (oASegment.AnswerFileSegments == null)
                return;

            AdminSegmentDefs oSegDefs = new AdminSegmentDefs();
            bool bSegmentMissing = false;
            //GLOG 3904
            for (int iCount = 0; iCount < m_oCurAnswerFileSegments.Count; iCount++)
            {

                //GLOG 3904
                AnswerFileSegment oAFSeg = m_oCurAnswerFileSegments.Item(iCount);
                //AnswerFileSegment oAFSeg = oASegment.AnswerFileSegments.Item(iCount);
                AdminSegmentDef oSegDef = null;
                try
                {

                    //GLOG 6966: Make sure string can be parsed as a Double even if
                    //the Decimal separator is something other than '.' in Regional Settings
                    oSegDef = (AdminSegmentDef)oSegDefs.ItemFromID((int)Double.Parse(oAFSeg.SegmentID.Replace(".0", "")));
                }
                catch
                {
                }
                if (oSegDef != null)
                {
                    //add node for each Segment
                    UltraTreeNode oSegNode = oRootNode.Nodes.Add(xRootKey + cSep + "AnswerFileSegment" + iCount.ToString(), oSegDef.DisplayName);
                    //configure node
                    SetNodeAppearance(oSegNode, false, false, true, Color.Black, Images.DocumentSegment);
                    //add AnswerFileSegment object to the node's tag
                    oSegNode.Tag = oAFSeg;
                }
                else
                {
                    bSegmentMissing = true;
                    //GLOG 3904
                    m_oCurAnswerFileSegments.Remove(iCount--);
                    //oASegment.AnswerFileSegments.Remove(iCount--);
                    m_bDocIsDirty = true;
                }
            }
            //One or more of the originally-assigned segments have been deleted.
            if (bSegmentMissing)
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_DesignerAnswerFileSegmentsMissing"), 
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        /// <summary>
        /// adds control actions nodes to the specified root node
        /// </summary>
        /// <param name="oControlActions"></param>
        /// <param name="oRootNode"></param>
        private void AddSegmentControlActionNodes(ControlActions oControlActions,
            UltraTreeNode oRootNode)
        {

            //add nodes for Jurisdiction Control Actions - 
            int iCount = oControlActions.Count;

            //clear existing children
            oRootNode.Nodes.Clear();

            for (int i = 0; i < iCount; i++)
            {
                //get control action
                ControlAction oAction = oControlActions[i];
                string xActionKey = null;

                try
                {
                    //add action node -
                    //key is form Var.ID.CategoryName.ControlActionsType.Index
                    xActionKey = oRootNode.Key + "." + i.ToString();

                    UltraTreeNode oActionNode = oRootNode.Nodes.Add(xActionKey, oAction.Type.ToString());
                    SetNodeAppearance(oActionNode, true, false, true, Color.Black);

                    //add ValueSetID node as child
                    UltraTreeNode oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".Event", "Event Type");
                    SetNodeAppearance(oPropertyNode, false, true, true, Color.Black, Images.NoImage);

                    //add design property name to the node's tag
                    oPropertyNode.Tag = "Event";

                    //add Event type as child
                    string xEvent = GetControlActionPropertyValue(oAction, "Event", false);

                    //replace "0" with "1" (First Display)
                    xEvent = xEvent == "0" ? "1" : xEvent;

                    //add value node
                    UltraTreeNode oValueNode = oPropertyNode.Nodes.Add(
                        xActionKey + ".Event_Value", xEvent);
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                    //add ExecutionCondition node as child
                    oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".ExecutionCondition", "Execution Condition");
                    SetNodeAppearance(oPropertyNode, false, true, true, Color.Black);

                    //add design property name to the node's tag
                    oPropertyNode.Tag = "ExecutionCondition";

                    //add value node
                    string xValue = oAction.ExecutionCondition;
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    oValueNode = oPropertyNode.Nodes.Add(
                        xActionKey + ".ExecutionCondition_Value", xValue);
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                    //add Parameters category node as child
                    oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".Parameters", "Parameters");
                    SetNodeAppearance(oPropertyNode, true, false, true, Color.Black);

                    //add parameters
                    xValue = oAction.Parameters;
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    AddSegmentControlActionsParametersNodes(oPropertyNode, oAction);
                }
                catch (SystemException oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddControlActionsNode") +
                       xActionKey, oE);
                }
            }
        }

        /// <summary>
        /// Sets specified Variable CI property value
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        private void SetVariableCIPropertyValue(Variable oVariable, string xPropName, string xValue)
        {
            switch (xPropName)
            {
                case "Request CI For Entire Segment":
                case "RequestCIForEntireSegment":
                    oVariable.RequestCIForEntireSegment = System.Convert.ToBoolean(xValue);
                    break;
                case "CI Contact Type":
                case "CIContactType":
                    if (xValue != "0")
                        oVariable.CIContactType = ((ciContactTypes)(Int32.Parse(xValue)));
                    break;
                case "CIEntityName":
                    break;
                case "CIDetailTokenString":
                case "CI Detail Token String":
                    oVariable.CIDetailTokenString = xValue;
                    break;
                case "CIAlerts":
                case "CI Alerts":
                    oVariable.CIAlerts = ((ciAlerts)(Int32.Parse(xValue)));
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// overloaded method adds child segment treenodes for designated segment
        /// by instantiating segments object - this eventually will fire
        /// m_oMPDocument_SegmentedAdded event handler, which will refresh tree with
        /// child segment nodes
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oSegmentNode"></param>
        private void AddSegmentChildSegmentNodes(Segment oSegment)
        {
            //cycle through child segments, adding each to tree
            for (int i = 0; i < oSegment.Segments.Count; i++)
                this.InsertNode(oSegment.Segments[i], oSegment);
        }
        /// <summary>
        /// adds child segment treenodes for designated segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oSegmentNode"></param>
        private void AddSegmentChildSegmentNodes(Segment oSegment, UltraTreeNode oSegmentNode)
        {
            //TODO: code to manually add designated node
            AddSegmentChildSegmentNodes(oSegment);
        }
        /// <summary>
        /// adds block treenodes for designated segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oBlockCatNode"></param>
        private void AddSegmentBlockNodes(Segment oSegment, UltraTreeNode oBlockCatNode)
        {
            //cycle through segment blocks, adding each to tree
            for (int i = 0; i < oSegment.Blocks.Count; i++)
            {
                //get block
                LMP.Architect.Api.Block oBlock = oSegment.Blocks[i];

                //add block to tree
                string xKey = oBlock.TagID;

                LMP.Trace.WriteValues(oBlock.Name);

                UltraTreeNode oVarNode = oBlockCatNode.Nodes.Add(xKey, oBlock.Name);

                //add reference to block
                oVarNode.Tag = oBlock;

                //set node properties
                SetNodeAppearance(oVarNode, false, false, true, Color.Black, Images.Variable);
            }
        }
        /// <summary>
        /// clears child nodes, refreshes with segment variables collection
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oVarCatNode"></param>
        private void RefreshSegmentVariableNodes(Segment oSegment, UltraTreeNode oVarCatNode)
        {
            oVarCatNode.Nodes.Clear();
            AddSegmentVariableNodes(oSegment, oVarCatNode);
        }
        /// <summary>
        /// add variable treenodes to designated segment variables category node
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oVarCatNode"></param>
        private void AddSegmentVariableNodes(Segment oSegment, UltraTreeNode oVarCatNode)
        {
            //cycle through segment variables, adding each to tree
            for (int i = 0; i < oSegment.Variables.Count; i++)
            {
                //get variable
                LMP.Architect.Api.Variable oVar = oSegment.Variables[i];

                LMP.Trace.WriteValues(oVar.Name);

                //add to tree
                string xKey = oVarCatNode.Key + "." + oVar.ID;
                UltraTreeNode oVarNode = oVarCatNode.Nodes.Add(xKey, oVar.Name);

                // GLOG : 3309 : JAB
                // Use image for tagged variable accordingly.
                SetNodeAppearance(oVarNode, false, false, true, Color.Black, oVar.IsTagless ? Images.Variable : Images.TaggedVariable);

                //add reference to variable
                oVarNode.Tag = oVar;
            }
        }
        /// <summary>
        /// overloaded method - refreshes variable node without forcing refresh
        /// </summary>
        /// <param name="oNode"></param>
        private void RefreshVariableNode(UltraTreeNode oNode)
        {
            this.RefreshVariableNode(oNode, false);
        }
        /// <summary>
        /// refreshes the specified variable if necessary or forced - 
        /// adds the appropriate nodes as children of the variable node
        /// </summary>
        /// <param name="oVarNode"></param>
        /// <param name="bForceRefresh"></param>
        private void RefreshVariableNode(UltraTreeNode oVarNode, bool bForceRefresh)
        {
            //exit if node is the same
            if ((oVarNode == this.m_oCurNode) && !bForceRefresh)
                return;

            DateTime t0 = DateTime.Now;
            UltraTreeNode oCICatNode = null;
            UltraTreeNode oVarActionsCatNode = null;
            UltraTreeNode oCtlActionsCatNode = null;
            char cSep = '.';

            //get referred variable
            Variable oVar = oVarNode.Tag as Variable;
            string xVarID = oVar.ID;

            if (oVar == null)
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_NodeIsNotVariableNode"));

            //get children of variable node & clear
            TreeNodesCollection oNodes = oVarNode.Nodes;
            oNodes.Clear();

            try
            {
                //add variable prop nodes, add children to each category node
                AddVariablePropertyNodes(oVarNode, oVar);

                //these category nodes belong to AdminSegments only
                if (oVar is AdminVariable)
                {
                    //create and configure all the category nodes
                    oCICatNode = oNodes.Add(oVarNode.Key + cSep + "CI", "CI");
                    SetNodeAppearance(oCICatNode, true, false, true,
                        Color.Black, Images.NoImage);

                    oCtlActionsCatNode = oNodes.Add(oVarNode.Key + cSep + "ControlActions", "Control Actions");
                    SetNodeAppearance(oCtlActionsCatNode, true, false, true,
                        Color.Black, Images.NoImage);

                    //Don't add variable action nodes for Answer File
                    if (oVar.Segment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile &&
                        oVar.Segment.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm)
                    {
                        oVarActionsCatNode = oNodes.Add(oVarNode.Key + cSep + "VariableActions", "Variable Actions");
                        SetNodeAppearance(oVarActionsCatNode, true, false, true,
                            Color.Black, Images.NoImage);
                    }
                }
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                   LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                   xVarID, oE);
            }

            try
            {
                if (oVar is AdminVariable)
                {
                    //add children to each category node
                    AddCINodes(oCICatNode, oVar);
                    AddControlActionNodes(oCtlActionsCatNode, oVar);

                    if (oVar.Segment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile &&
                        oVar.Segment.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm)
                    {
                        AddVariableActionNodes(oVarActionsCatNode, oVar);
                    }
                }
            }
            catch (SystemException oE)
            {
                throw new LMP.Exceptions.UINodeException(
                   LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                   "xVarID", oE);
            }
            LMP.Benchmarks.Print(t0);
           
        }
        /// <summary>
        /// refreshes the specified segment node if necessary or forced - 
        /// adds the appropriate nodes as children of the segment node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh"></param>
        private void RefreshAnswerFileSegmentNode(UltraTreeNode oNode, bool bForceRefresh)
        {
            //exit if node is the same
            if ((oNode == this.m_oCurNode) && !bForceRefresh)
                return;

            char cSep = '.';

            //get referred variable
            AnswerFileSegment oAFSeg = oNode.Tag as AnswerFileSegment;

            if (oAFSeg == null)
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_NodeIsNotAnswerFileSegmentNode"));

            //get children of variable node & clear
            TreeNodesCollection oNodes = oNode.Nodes;
            oNodes.Clear();

            try
            {
                UltraTreeNode oPropNode = oNode.Nodes.Add(oNode.Key + cSep + "InsertionLocation", "Insertion Location");
                //configure node
                SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);
                //add design property name to the node's tag
                oPropNode.Tag = "InsertionLocation";

                //add Segment Insertion Location property nodes
                string xValue = oAFSeg.InsertionLocation.ToString();

                //replace empty value with standard designator
                xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

                //add value node
                UltraTreeNode oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

                //set value node appearance
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);


                //add Segment Insertion Behavior property nodes
                oPropNode = oNode.Nodes.Add(oNode.Key + cSep + "InsertionBehavior", "Insertion Behavior");
                //configure node
                SetNodeAppearance(oPropNode, false, true, true, Color.Black, Images.NoImage);

                //add design property name to the node's tag
                oPropNode.Tag = "InsertionBehavior";

                xValue = oAFSeg.InsertionBehavior.ToString();

                //replace empty value with standard designator
                xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

                //add value node
                oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

                //set value node appearance
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);
                //oValueNode.Override.Multiline = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                   LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                   "xVarID", oE);
            }
        }
        /// <summary>
        /// adds variable property and property value nodes to tree
        /// </summary>
        /// <param name="oVarNode"></param>
        /// <param name="oVar"></param>
        private void AddVariablePropertyNodes(UltraTreeNode oVarNode, Variable oVar)
        {
            UltraTreeNode oPropNode = null;
            string xVarID = oVar.TagID;
            bool bTrim = false;
            string xValue = null;
            
            DateTime t0 = DateTime.Now;

            string[,] oProps = null;

            //build storage of property names: array of property name, display name
            if(oVar is AdminVariable)
                oProps = GetAdminSegmentVariablePropertyNameArray(oVar.Segment.IntendedUse);
            else
                oProps = GetUserSegmentVariablePropertyNameArray();

            //cycle through properties array, 
            //adding properties to tree
            for (int i = 0; i <= oProps.GetUpperBound(0); i++)
            {
                //GLOG 1408: Display Navigation Tag property only for Tagless variable
                if (!oVar.IsTagless && oProps[i, 0] == "NavigationTag")
                    continue;
                try
                {
                    //add property node
                    oPropNode = oVarNode.Nodes.Add(oVarNode.Key + "." + oProps[i, 0], oProps[i, 1]);

                    //set configuration
                    SetNodeAppearance(oPropNode, false, true, true,
                        Color.Black, Images.NoImage);

                    //add design property name to the node's tag
                    oPropNode.Tag = oProps[i, 0];

                    try
                    {
                        //add node for property value - get property value
                        xValue = GetVariablePropertyValue(oVar, oProps[i, 0], false); 
                        //replace empty value with standard designator
                        xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;
                        bTrim = true;

                        //GLOG 3277: Always displayed on single line
                        //if ((oProps[i, 0] == "ControlProperties") && (xValue != null))
                        //{
                        //    //replace standard delimiters w/ line feeds for display
                        //    xValue = xValue.Replace(StringArray.mpEndOfSubValue.ToString(), "\r\n");
                        //    bTrim = false;
                        //}
                        if (oProps[i, 0] == "HelpText")
                        //handle special formatting
                        {
                            //deal with < character in help text, which could be html
                            xValue = xValue.Replace((char)172, '<');
                        }

                        LMP.Trace.WriteNameValuePairs("Property: " + oProps[i, 0], xValue);

                        //trim value for UI if required
                        if (xValue.Length > 30 && bTrim)
                            xValue = xValue.Substring(0, 30) + "...";

                        //add value node
                        UltraTreeNode oValueNode = oPropNode.Nodes.Add(oPropNode.Key + "_Value", xValue);

                        //set value node appearance
                        SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                        //set multi-line for control properties value node
                        //GLOG 3277: Always display single line
                        //switch (oValueNode.Parent.Text)
                        //{
                        //    case "Control Properties":
                        //    case "Display In":
                        //        oValueNode.Override.Multiline = Infragistics.Win.DefaultableBoolean.True;
                        //        break;
                        //}
                    }
                    catch (SystemException oE)
                    {
                         throw new LMP.Exceptions.UINodeException(
                            LMP.Resources.GetLangString("Error_CouldNotAddPropertyValueNode") +
                            oPropNode.Key + "_Value", oE);
                    }
                }
                catch (SystemException oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                       xVarID + oProps[1, 0], oE);
                }
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// adds control actions nodes to the specified
        /// node for the specified variable
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oVar"></param>
        private void AddControlActionNodes(UltraTreeNode oRootNode, Variable oVar)
        {

            //add nodes for VariableActions - 
            int iCount = oVar.ControlActions.Count;

            //clear existing children
            oRootNode.Nodes.Clear();

            for (int i = 0; i < iCount; i++)
            {
                //get control action
                ControlAction oAction = oVar.ControlActions[i];
                string xActionKey = null;

                try
                {
                    //add action node -
                    //key is form Var.ID.CategoryName.ControlActionsType.Index
                    xActionKey = oRootNode.Key + "." + i.ToString();

                    UltraTreeNode oActionNode = oRootNode.Nodes.Add(xActionKey, oAction.Type.ToString());
                    SetNodeAppearance(oActionNode, true, false, true, Color.Black);

                    //add ValueSetID node as child
                    UltraTreeNode oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".Event", "Event Type");
                    SetNodeAppearance(oPropertyNode, false, true, true, Color.Black, Images.NoImage);

                    //add design property name to the node's tag
                    oPropertyNode.Tag = "Event";

                    //add Event type as child
                    string xEvent = GetControlActionPropertyValue(oAction, "Event", false);

                    //replace "0" with "1" (First Display)
                    xEvent = xEvent == "0" ? "1" : xEvent;

                    //add value node
                    UltraTreeNode oValueNode = oPropertyNode.Nodes.Add(
                        xActionKey + ".Event_Value", xEvent);
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);
                    
                    //add ExecutionCondition node as child
                    oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".ExecutionCondition", "Execution Condition");
                    SetNodeAppearance(oPropertyNode, false, true, true, Color.Black);

                    //add design property name to the node's tag
                    oPropertyNode.Tag = "ExecutionCondition";

                    //add value node
                    string xValue = oAction.ExecutionCondition;
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    oValueNode = oPropertyNode.Nodes.Add(
                        xActionKey + ".ExecutionCondition_Value", xValue);
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                    //add Parameters category node as child
                    oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".Parameters", "Parameters");
                    SetNodeAppearance(oPropertyNode, true, false, true, Color.Black);

                    //add parameters
                    xValue = oAction.Parameters;
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    AddControlActionsParametersNodes(oPropertyNode, oAction);
                }
                catch (SystemException oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddControlActionsNode") +
                       xActionKey, oE);
                }
            }
        }
        /// <summary>
        /// adds variable actions nodes to the specified
        /// node for the specified variable
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oVar"></param>
        private void AddVariableActionNodes(UltraTreeNode oRootNode, Variable oVar)
        {
            //add nodes for VariableActions - 
            int iCount = oVar.VariableActions.Count;

            //clear existing children
            oRootNode.Nodes.Clear();

            for (int i = 0; i < iCount; i++)
            {
                //get variable action
                VariableAction oAction = oVar.VariableActions[i];
                string xActionKey = null;
                try
                {
                    //add action node -
                    //key is form Var.ID.CategoryName.VariableActionsType.Index
                    xActionKey = oRootNode.Key + "." + i.ToString();

                    UltraTreeNode oActionNode = oRootNode.Nodes.Add(xActionKey, InsertSpacesBeforeCaps(oAction.Type.ToString()));
                    SetNodeAppearance(oActionNode, true, false, true, Color.Black, Images.NoImage);

                    //add ExecutionCondition node as child
                    UltraTreeNode oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".ExecutionCondition", "Execution Condition");
                    SetNodeAppearance(oPropertyNode, false, true, true, Color.Black, Images.NoImage);

                    //add design property name to the node's tag
                    oPropertyNode.Tag = "ExecutionCondition";

                    //add value node
                    string xValue = oAction.ExecutionCondition;
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    UltraTreeNode oValueNode = oPropertyNode.Nodes.Add(
                        xActionKey + ".ExecutionCondition_Value", xValue);
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                    //add ValueSetID node as child
                    oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".ValueSetID", "Value Mapping List");
                    SetNodeAppearance(oPropertyNode, false, true, true, Color.Black, Images.NoImage);

                    //add design property name to the node's tag
                    oPropertyNode.Tag = "ValueSetID";

                    //for ValueSetIDs, retrieve name of value, if any
                    string xValueSetID = GetVariableActionPropertyValue(oAction, "ValueSetID", false); 
                    
                    //replace "0" with "None"
                    xValueSetID = xValueSetID == "0" ? "None" : xValueSetID;

                    //add value node
                    oValueNode = oPropertyNode.Nodes.Add(
                        xActionKey + ".ValueSetID_Value", xValueSetID);
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                    //add Parameters category node as child
                    oPropertyNode = oActionNode.Nodes.Add(
                        xActionKey + ".Parameters", "Parameters");
                    SetNodeAppearance(oPropertyNode, true, false, true, Color.Black, Images.NoImage);

                    //add parameters
                    AddVariableActionsParametersNodes(oPropertyNode, oAction);
                }
                catch (SystemException oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddVariableActionsNode") +
                       xActionKey, oE);
                }
            }
        }
        /// <summary>
        /// adds CI nodes to the specified variable
        /// node for the specified variable
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oVar"></param>
        private void AddCINodes(UltraTreeNode oRootNode, Variable oVar)
        {
            UltraTreeNode oPropertyNode = null;
            UltraTreeNode oValueNode = null;
            string xValue = null;

            //clear existing children
            oRootNode.Nodes.Clear();

            string xCIKey = null;
            try
            {
                //add CI node -
                //key is form RootNode.Key.PropName
                xCIKey = oRootNode.Key;

                //add CI Contact Type node as child
                oPropertyNode = oRootNode.Nodes.Add(
                    xCIKey + ".CIContactType", "CI Contact Type");
                //set appearance - node is expanded
                SetNodeAppearance(oPropertyNode, false, true, true, Color.Black, Images.NoImage);

                //add design property name to the node's tag
                oPropertyNode.Tag = "CIContactType";

                //add value node
                xValue = GetVariableCIPropertyValue(oVar, oPropertyNode.Tag.ToString(), false);
                xValue = xValue == null || xValue == "" ? "Null" : xValue;

                oValueNode = oPropertyNode.Nodes.Add(
                    xCIKey + ".CIContactType_Value", xValue);
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);


                //add CI Detail Token String node as child
                oPropertyNode = oRootNode.Nodes.Add(
                    xCIKey + ".CIDetailTokenString", "CI Detail Token String");
                SetNodeAppearance(oPropertyNode, false, true, true, Color.Black, Images.NoImage);

                //add design property name to the node's tag
                oPropertyNode.Tag = "CIDetailTokenString";

                //add value node
                xValue = GetVariableCIPropertyValue(oVar, oPropertyNode.Tag.ToString(), false);
                xValue = xValue == null || xValue == "" ? "Null" : xValue;

                //add value node
                oValueNode = oPropertyNode.Nodes.Add(
                    xCIKey + ".CIDetailTokenString_Value", xValue);
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                //add CI Alerts node as child
                oPropertyNode = oRootNode.Nodes.Add(
                    xCIKey + ".CIAlerts", "CI Alerts");
                SetNodeAppearance(oPropertyNode, false, true, true, Color.Black, Images.NoImage);

                //add design property name to the node's tag
                oPropertyNode.Tag = "CIAlerts";

                //add value node
                xValue = GetVariableCIPropertyValue(oVar, oPropertyNode.Tag.ToString(), false);
                xValue = xValue == null || xValue == "" ? "None" : xValue;

                //add value node
                oValueNode = oPropertyNode.Nodes.Add(
                    xCIKey + ".CIAlerts_Value", xValue);
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                //set node  to multiline for alerts checked listbox
                //oValueNode.Override.Multiline = Infragistics.Win.DefaultableBoolean.True;

                //add RequestCIForEntireSegment node as child
                oPropertyNode = oRootNode.Nodes.Add(
                    xCIKey + ".RequestCIForEntireSegment ", "Request CI For Entire Segment");
                SetNodeAppearance(oPropertyNode, false, true, true, Color.Black, Images.NoImage);

                //add design property name to the node's tag
                oPropertyNode.Tag = "RequestCIForEntireSegment";

                //add value node
                xValue = oVar.RequestCIForEntireSegment.ToString();

                xValue = GetVariableCIPropertyValue(oVar, oPropertyNode.Tag.ToString(), false);
                xValue = xValue == null || xValue == "" ? "Null" : xValue;

                oValueNode = oPropertyNode.Nodes.Add(
                    xCIKey + ".RequestCIForEntireSegment_Value", xValue);
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

            }
            catch (SystemException oE)
            {
                throw new LMP.Exceptions.UINodeException(
                   LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                   xCIKey, oE);
            }
        }
        /// <summary>
        /// adds parameters nodes at the specified
        /// node for the specified action
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oAction"></param>
        private void AddVariableActionsParametersNodes(UltraTreeNode oRootNode, VariableAction oAction)
        {
            //get parameter names and values - 
            //these vary based on the action type
            string[,] aParamNames = GetVariableActionParameters(oAction.Type);
            string[] aParamValues = oAction.Parameters.Split(StringArray.mpEndOfSubField);

            if (aParamNames == null)
                return;

            int iLength = 0;

            //2 dimensional array length /2 give equivilant of 1 dimensional array length
            if (aParamNames.Length / 2 != aParamValues.Length)
            {
                //invalid parameter string - it's possible that new parameters have been added
                //for this action - use default values from aParamNames array for any missing parameters
                string[] aParamValues2 = new string[aParamNames.GetUpperBound(0) + 1];
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    if (i < aParamValues.Length)
                        //load existing value
                        aParamValues2[i] = aParamValues[i];
                    else
                        //load default value
                        aParamValues2[i] = aParamNames[i, 1];
                }
                aParamValues = aParamValues2;

                //set the parameters property with new string if the existing string from XML was invalid
                oAction.Parameters = BuildParameterString(aParamValues);

                iLength = aParamValues.Length;

                //save to XML
                Variable oVar = GetVariableFromNode(oRootNode.Parent);
                Segment oSeg = GetSegmentFromNode(oRootNode.Parent);
                oSeg.Variables.Save(oVar);

            }
            else
            {
                iLength = aParamNames.Length / 2;
            }

            //cycle through VariableActions parameters array 
            //creating name node and value child node
            for (int i = 0; i < aParamNames.Length / 2; i++)
            {
                try
                {
                    //create parameter node
                    UltraTreeNode oParamNode = oRootNode.Nodes.Add(
                        oRootNode.Key + "." + aParamNames[i, 0], aParamNames[i, 0]);
                    SetNodeAppearance(oParamNode, false, true, true, Color.Black, Images.NoImage);

                    //add action type name and design property name to the node's tag
                    oParamNode.Tag = oAction.Type.ToString() + "." + aParamNames[i, 0];

                    //add value node - use default value if value string is invalid
                    string xValue = null;
                    if (iLength == 0)
                        xValue = aParamNames[i, 1];
                    else
                        xValue = aParamValues[i];

                    //display "Null" for empty value
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    LMP.Trace.WriteNameValuePairs(aParamNames[i, 0], xValue);

                    UltraTreeNode oValueNode = oParamNode.Nodes.Add(
                        oParamNode.Key + "_Value", xValue);

                    if (xValue != "Null")
                    {
                        //reset display text to parameter text value from enum
                        oValueNode.Text = GetVariableActionParameterTextFromNode(oValueNode);
                    }

                    //set appearance
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddParameterNode") +
                       " Variable Action." + oRootNode.Key, oE);
                }
            }
        }
        /// <summary>
        /// adds parameters nodes at the specified
        /// node for the specified control action
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oAction"></param>
        private void AddControlActionsParametersNodes(UltraTreeNode oRootNode, ControlAction oAction)
        {
            //get parameter names and values - 
            //these vary based on the action type
            string[,] aParamNames = GetControlActionParameters(oAction.Type);
            string[] aParamValues = oAction.Parameters.Split(StringArray.mpEndOfSubField);

            if (aParamNames == null)
                return;
            int iLength = 0;
            if (aParamNames.Length / 2 != aParamValues.Length)
            //invalid parameter string - fill aParamValues w/ default values from aParamNames array
            {
                aParamValues = new string[aParamNames.GetUpperBound(0) + 1];

                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    aParamValues[i] = aParamNames[i, 1];
                }

                //set the parameters property with new string if the existing string from XML was invalid
                oAction.Parameters = BuildParameterString(aParamValues);
                iLength = aParamValues.Length;
                //save to XML
                Variable oVar = GetVariableFromNode(oRootNode.Parent);
                Segment oSeg = GetSegmentFromNode(oRootNode.Parent);
                oSeg.Variables.Save(oVar);
            }
            else
            {
                iLength = aParamNames.Length / 2;
            }

            //cycle through ControlActions parameters array 
            //creating name node and value child
            for (int i = 0; i < aParamNames.Length / 2; i++)
            {
                try
                {
                    //create parameter node
                    UltraTreeNode oParamNode = oRootNode.Nodes.Add(
                        oRootNode.Key + "." + aParamNames[i, 0], aParamNames[i, 0]);
                    SetNodeAppearance(oParamNode, false, true, true, Color.Black, Images.NoImage);

                    //add action type name and design property name to the node's tag
                    oParamNode.Tag = oAction.Type.ToString() + "." + aParamNames[i, 0];

                    //add value node - use default value if value string is invalid
                    string xValue = null;
                    if (iLength == 0)
                        xValue = aParamNames[i, 1];
                    else
                        xValue = aParamValues[i];

                    //display "Null" for empty value
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    LMP.Trace.WriteNameValuePairs(aParamNames[i, 0], xValue);

                    UltraTreeNode oValueNode = oParamNode.Nodes.Add(
                        oParamNode.Key + "_Value", xValue);

                    if (xValue != "Null")
                    {
                        //reset display text to parameter text value from enum
                        oValueNode.Text = GetControlActionParameterTextFromNode(oValueNode);
                    }

                    //set appearance
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddParameterNode") +
                       " Variable Action." + oRootNode.Key, oE);
                }

            }
        }
        /// <summary>
        /// adds parameters nodes at the specified
        /// node for the specified control action
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oAction"></param>
        private void AddSegmentControlActionsParametersNodes(UltraTreeNode oRootNode, ControlAction oAction)
        {
            //get parameter names and values - 
            //these vary based on the action type
            string[,] aParamNames = GetControlActionParameters(oAction.Type);
            string[] aParamValues = oAction.Parameters.Split(StringArray.mpEndOfSubField);

            if (aParamNames == null)
                return;
            int iLength = 0;
            if (aParamNames.Length / 2 != aParamValues.Length)
            //invalid parameter string - fill aParamValues w/ default values from aParamNames array
            {
                aParamValues = new string[aParamNames.GetUpperBound(0) + 1];

                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    aParamValues[i] = aParamNames[i, 1];
                }

                //set the parameters property with new string if the existing string from XML was invalid
                oAction.Parameters = BuildParameterString(aParamValues);
                iLength = aParamValues.Length;
            }
            else
            {
                iLength = aParamNames.Length / 2;
            }

            //cycle through ControlActions parameters array 
            //creating name node and value child
            for (int i = 0; i < aParamNames.Length / 2; i++)
            {
                try
                {
                    //create parameter node
                    UltraTreeNode oParamNode = oRootNode.Nodes.Add(
                        oRootNode.Key + "." + aParamNames[i, 0], aParamNames[i, 0]);
                    SetNodeAppearance(oParamNode, false, true, true, Color.Black, Images.NoImage);

                    //add action type name and design property name to the node's tag
                    oParamNode.Tag = oAction.Type.ToString() + "." + aParamNames[i, 0];

                    //add value node - use default value if value string is invalid
                    string xValue = null;
                    if (iLength == 0)
                        xValue = aParamNames[i, 1];
                    else
                        xValue = aParamValues[i];

                    //display "Null" for empty value
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    LMP.Trace.WriteNameValuePairs(aParamNames[i, 0], xValue);

                    UltraTreeNode oValueNode = oParamNode.Nodes.Add(
                        oParamNode.Key + "_Value", xValue);

                    if (xValue != "Null")
                    {
                        //reset display text to parameter text value from enum
                        oValueNode.Text = GetControlActionParameterTextFromNode(oValueNode);
                    }

                    //set appearance
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddParameterNode") +
                       " Variable Action." + oRootNode.Key, oE);
                }

            }
        }
        /// <summary>
        /// adds segment event as category nodes
        /// these are variable actions but are triggered
        /// by segment events
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oSeg"></param>
        private void AddSegmentEventNodes(Segment oSeg, UltraTreeNode oRootNode)
        {
            UltraTreeNode oEventNode = null;
            oRootNode.Nodes.Clear();

            string[,] oEvents = null;
            if (oRootNode.Text == "Language Actions")
            {
                oEvents = new string[,] { { "After Language Updated", "8" } };
            }
            else if (oRootNode.Text == "Authors Actions")
            {
                oEvents = new string[,] { { "After Author Updated", "7" } };
            }
            else
            {
                oEvents = new string[,] {{"After Segment XML Insert", "4"},
			                        {"After Default Author Set", "1"},
			                        {"After Default Values Set", "2"},
			                        {"After Segment Generated", "5"},
			                        {"After Segment Valid", "6"}, 
                                    {"Before Segment Finish", "9"}}; //GLOG 8512
            }

            //cycle through properties array, 
            //adding properties to tree
            for (int i = 0; i <= oEvents.GetUpperBound(0); i++)
            {
                //add event category nodes
                try
                {
                    if (oRootNode.Text == "Segment Actions")
                    {
                        //add event category node
                        oEventNode = oRootNode.Nodes.Add(oRootNode.Key + "." +
                            oEvents[i, 1], oEvents[i, 0]);

                        //set node appearance
                        SetNodeAppearance(oEventNode, true, false, true, Color.Black,
                            Images.NoImage);
                    }
                    else
                        //language and authors have only a single event
                        oEventNode = oRootNode;

                    //cycle through the segment actions collection
                    //match action.event to event category node
                    //if matched, set up segment action(s) + parameter nodes as children 
                    //of segment event category node

                    for (int j = 0; j < oSeg.Actions.Count; j++)
                    {
                        if (oEvents[i, 0].Replace(" ", "") == oSeg.Actions[j].Event.ToString())
                            //pass index of segment event action
                            //index will be stored as part of action node key
                            AddSegmentActionNodes(oEventNode, oSeg.Actions[j], j);
                    }
                }
                catch (SystemException oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                       oSeg.FullTagID + oEvents[i, 0], oE);
                }
            }
        }
        /// <summary>
        /// adds single segment action node to the specified
        /// category node -- for segments, this will be a segment.event
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oAction"></param>
        /// <param name="iIndex"></param>
        private void AddSegmentActionNodes(UltraTreeNode oRootNode, SegmentAction oAction, int iIndex)
        {
            //add node for SegmentAction - 
            string xActionKey = null;
            try
            {
                //add action node -
                //key is form Var.ID.CategoryName.VariableActionsType.Index
                xActionKey = oRootNode.Key + "." + iIndex;

                UltraTreeNode oActionNode = oRootNode.Nodes.Add(xActionKey,
                    InsertSpacesBeforeCaps(oAction.Type.ToString()));
                SetNodeAppearance(oActionNode, true, false, true, Color.Black,
                    Images.NoImage);

                //add ExecutionCondition property node as child
                UltraTreeNode oPropertyNode = oActionNode.Nodes.Add(
                    xActionKey + ".ExecutionCondition", "Execution Condition");
                SetNodeAppearance(oPropertyNode, false, true, true, Color.Black,
                    Images.NoImage);

                //add design property name to the node's tag
                oPropertyNode.Tag = "ExecutionCondition";

                //add value node
                string xValue = oAction.ExecutionCondition;
                xValue = xValue == null || xValue == "" ? "Null" : xValue;

                UltraTreeNode oValueNode = oPropertyNode.Nodes.Add(
                    xActionKey + ".ExecutionCondition_Value", xValue);
                SetNodeAppearance(oValueNode, false, true, true, Color.Gray,
                    Images.NoImage, -1, 10);

                //add Parameters category node as child
                oPropertyNode = oActionNode.Nodes.Add(
                    xActionKey + ".Parameters", "Parameters");
                SetNodeAppearance(oPropertyNode, true, false, true, Color.Black, Images.NoImage);

                //add parameter nodes
                AddSegmentActionsParametersNodes(oPropertyNode, oAction);
            }
            catch (SystemException oE)
            {
                throw new LMP.Exceptions.UINodeException(
                   LMP.Resources.GetLangString("Error_CouldNotAddVariableActionsNode") +
                   xActionKey, oE);
            }
        }
        /// <summary>
        /// adds parameters nodes at the specified
        /// node for the specified action
        /// </summary>
        /// <param name="oRootNode"></param>
        /// <param name="oAction"></param>
        private void AddSegmentActionsParametersNodes(UltraTreeNode oRootNode, SegmentAction oAction)
        {
            //get parameter names and values - 
            //these vary based on the action type
            //use variable actions parameters
            string[,] aParamNames = GetVariableActionParameters(oAction.Type);
            string[] aParamValues = oAction.Parameters.Split(StringArray.mpEndOfSubField);

            if (aParamNames == null)
                return;

            int iLength = 0;

            if (aParamNames.Length / 2 != aParamValues.Length)
            {
                //invalid parameter string - it's possible that new parameters have been added
                //for this action - use default values from aParamNames array for any missing parameters
                string[] aParamValues2 = new string[aParamNames.GetUpperBound(0) + 1];
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    if (i < aParamValues.Length)
                        //load existing value
                        aParamValues2[i] = aParamValues[i];
                    else
                        //load default value
                        aParamValues2[i] = aParamNames[i, 1];
                }

                aParamValues = aParamValues2;

                //also set the parameters property if the string was invalid
                oAction.Parameters = BuildParameterString(aParamValues);

                iLength = aParamValues.Length;
            }
            else
            {
                iLength = aParamNames.Length / 2;
            }

            //cycle through SegmentEventActions parameters array 
            //creating name node and value child
            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
            {
                try
                {
                    //create parameter node
                    UltraTreeNode oParamNode = oRootNode.Nodes.Add(
                        oRootNode.Key + "." + aParamNames[i, 0], aParamNames[i, 0]);
                    SetNodeAppearance(oParamNode, false, true, true, Color.Black, Images.NoImage);

                    //add action type name and design property name to the node's tag
                    oParamNode.Tag = oAction.Type.ToString() + "." + aParamNames[i, 0];

                    //add value node - or assign default value from names array
                    string xValue = null;
                    if (iLength == 0)
                        xValue = aParamNames[i, 1];
                    else
                        xValue = aParamValues[i];

                    //display "Null" for empty value
                    xValue = xValue == null || xValue == "" ? "Null" : xValue;

                    LMP.Trace.WriteNameValuePairs(aParamNames[i, 0], xValue);

                    UltraTreeNode oValueNode = oParamNode.Nodes.Add(
                        oParamNode.Key + "_Value", xValue);

                    if (xValue != "Null")
                    {
                        //reset display text to parameter text value from enum
                        oValueNode.Text = GetSegmentActionParameterTextFromNode(oValueNode);
                    }

                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);

                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddParameterNode") +
                       " Variable Action." + oRootNode.Key, oE);
                }
            }
        }
        /// <summary>
        /// overloaded method - refreshes block node
        /// </summary>
        /// <param name="oNode"></param>
        private void RefreshBlockNode(UltraTreeNode oNode)
        {
            this.RefreshBlockNode(oNode, false);
        }
        /// <summary>
        /// refreshes the portion of the tree
        /// starting with the specified block tree node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh">forces the node to refresh, even if the node already has children</param>
        private void RefreshBlockNode(UltraTreeNode oBlockNode, bool bForceRefresh)
        {
            //exit if node is the same
            if (oBlockNode == this.m_oCurNode && !bForceRefresh)
                return;

            DateTime t0 = DateTime.Now;

            string[,] oProps = null;

            //get referred block
            Block oBlock = oBlockNode.Tag as Block;

            if (oBlock.Segment is AdminSegment || m_bIsContentDesignerSegment)
            {
                //build storage for property node info - key name, display name
                oProps = new string[,] {{"DisplayName","Display Name"},
                                        {"HotKey","Hot Key"},
                                        {"IsBody","Is Body"},
                                        {"StartingText", "Starting Text"}, 
                                        {"ShowInTree","Show In Tree"},
                                        {"DisplayLevel", "Display Level"},
                                        {"HelpText","Help Text"}};
            }
            else
            {
                oProps = new string[,] {{"DisplayName","Display Name"},
                                        {"HotKey","Hot Key"},
                                        {"HelpText","Help Text"}};
            }
            if (oBlock == null)
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_NodeIsNotBlockNode"));

            //get children of variable node
            TreeNodesCollection oNodes = oBlockNode.Nodes;

            UltraTreeNode oPropNode;
            string xBlockID = oBlock.TagID;

            //clear any existing child nodes
            oNodes.Clear();

            //cycle through properties array, 
            //adding properties to tree
            for (int i = 0; i <= oProps.GetUpperBound(0); i++)
            {
                //add node
                try
                {
                    oPropNode = oNodes.Add(xBlockID + "." + oProps[i, 0], oProps[i, 1]);
                }
                catch (SystemException oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddPropertyNode") +
                       xBlockID + oProps[1, 0], oE);
                }

                try
                {
                    //node is a property node - set up as appropriate
                    SetNodeAppearance(oPropNode, false, true, true,
                        Color.Black, Images.NoImage);

                    //add design property name to the node's tag
                    oPropNode.Tag = oProps[i, 0];

                    //add node for property value - get property value
                    string xValue = GetBlockPropertyValue(oBlock, oProps[i, 0], false);
                    
                    //replace empty value with standard designator
                    xValue = (xValue == null) || (xValue == "") ? EMPTY_VALUE : xValue;

                    //handle special formatting
                    if (oProps[i, 0] == "HelpText")
                        //deal with < character in help text, which could be html
                        xValue = xValue.Replace((char)172, '<');

                    LMP.Trace.WriteNameValuePairs("Property: " + oProps[i, 0], xValue);

                    //add value node
                    UltraTreeNode oValueNode = oPropNode.Nodes.Add(
                        oPropNode.Key + "_Value", xValue);

                    //set value node appearance
                    SetNodeAppearance(oValueNode, false, true, true, Color.Gray, Images.NoImage, -1, 10);
                }
                catch (SystemException oE)
                {
                    throw new LMP.Exceptions.UINodeException(
                       LMP.Resources.GetLangString("Error_CouldNotAddPropertyValueNode") +
                       oPropNode.Key + "_Value", oE);
                }
            }
            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// selects the specified node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh"></param>
        private void SelectNode(UltraTreeNode oNode, bool bSelectAssociatedTag)
        {
            this.SelectNode(oNode, false, bSelectAssociatedTag);
        }
        /// <summary>
        /// selects the specified node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bSuppressExpansionToggle"></param>
        private void SelectNode(UltraTreeNode oNode, bool bSuppressExpansionToggle, bool bSelectAssociatedTag)
        {
            try
            {
                this.pnlDocContents.SuspendLayout();
                this.treeDocContents.SuspendLayout();

                //exit if we're clicking the same node or no node is specified
                if (oNode == null || oNode == this.m_oCurNode)
                    return;
                else if (oNode == m_oCurNode && !oNode.Expanded && !bSuppressExpansionToggle)
                {
                    oNode.Expanded = true;
                    return;
                }
                string xKey = oNode.Key;

                //validate control value - exit if invalid - user has been messaged
                if (!IsValidControlValue((IControl)m_oCurCtl, m_oCurNode))
                {
                    this.treeDocContents.ActiveNode = m_oCurNode;
                    //ensure focus returns to current control
                    this.pnlDocContents.Focus();
                    m_oCurCtl.Focus();
                    this.InvalidControlValue = true;
                    return;
                }
                else
                    ExitEditMode();

                //refresh pointer - node has been deleted and rebuilt 
                //via definition changed event handlers for blocks, segs, and vars if property changed
                //node passed to EnterEditMode must be refreshed so that node coordinates are valid
                oNode = this.GetNodeFromTagID(xKey);

                //Node might not exist if tree contents have changed
                if (oNode == null)
                    return;

                this.IgnoreTreeViewEvents = true;
                CollapsePreviousNodeToCollapsibleParent(oNode);
                this.IgnoreTreeViewEvents = false;

                //toggle expansion only if not suppressed because
                //of right click selection of node
                if (!bSuppressExpansionToggle)
                {
                    //always expand nodes without expansion indicators
                    //we want one click either on node or its expansion indicator to work
                    if (!oNode.HasExpansionIndicator)
                        oNode.Expanded = true;
                }

                //set node as the current node for the edit tab
                this.m_oCurNode = oNode;

                //display node path as status text
                this.lblStatus.Text = this.m_oCurNode.FullPath;
                //also set tooltip, for very long paths
                this.ttStatus.SetToolTip(lblStatus, this.m_oCurNode.FullPath);
                //set up node for editing
                EnterEditMode(oNode, bSelectAssociatedTag);

                this.treeDocContents.ActiveNode = oNode;

            }
            catch (System.Exception oE)
            {
                this.IgnoreTreeViewEvents = false;
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }
            finally
            {
                this.pnlDocContents.ResumeLayout();
            }
        }
        /// <summary>
        /// collapses expanded category nodes of previous 
        /// active node upon activation of designated new node
        /// </summary>
        /// <param name="oNewNode"></param>
        private void CollapsePreviousNodeToCollapsibleParent(UltraTreeNode oNewNode)
        {
            //return if the module level treenode variable has not been set
            if (m_oCurNode == null || m_oCurNode == oNewNode)
                return;

            //get collapsible parent of previous node and new treenode
            UltraTreeNode oNewParent = GetCollapsibleParentNode(oNewNode);
            UltraTreeNode oTreeNodeToCollapse = GetCollapsibleParentNode(m_oCurNode);

            //don't try to collapse if the collapsible parent is the same as the 
            //module level treenode's collapsible parent
            if (oTreeNodeToCollapse == null || oTreeNodeToCollapse == oNewParent)
                return;
            else if (oTreeNodeToCollapse.IsAncestorOf(oNewParent))
                return;

            //TODO:  this screen refresh call doesn't seem to work here
            bool bUpdating = this.TaskPane.ScreenUpdating;
            this.TaskPane.ScreenUpdating = false;

            //perform the selective collapse from top level collapsible parent on down
            CollapseDesignatedTreeNodes(oTreeNodeToCollapse);
            
            this.TaskPane.ScreenUpdating = bUpdating;
        }
        /// <summary>
        /// returns specific nodes that are collapsed once that node or any if its children
        /// no longer have focus
        /// </summary>
        /// <param name="oCurNode"></param>
        /// <returns></returns>
        private UltraTreeNode GetCollapsibleParentNode(UltraTreeNode oCurNode)
        {
            //node returned varies according to its position in the 
            //tree node structure.  Category nodes will typically return
            //oCurNode, property nodes oCurNode.Parent, etc.
            switch (NodeType(oCurNode))
            {
                case NodeTypes.Segment:
                    return oCurNode;
                case NodeTypes.SegmentPropertiesCategory:
                    return oCurNode;
                case NodeTypes.SegmentProperty:
                    return oCurNode.Parent;
                case NodeTypes.SegmentPropertyValue:
                    return oCurNode.Parent.Parent;
                
                case NodeTypes.SegmentActionsCategory:
                case NodeTypes.LanguageActionsCategory:
                case NodeTypes.AuthorsActionsCategory:
                    return oCurNode;
                case NodeTypes.SegmentEventsCategory:
                    return oCurNode;
                case NodeTypes.SegmentAction:
                case NodeTypes.LanguageAction:
                case NodeTypes.AuthorsAction:
                    return oCurNode.Parent;
                case NodeTypes.SegmentActionProperty:
                    return oCurNode.Parent.Parent;
                case NodeTypes.SegmentActionParametersCategory:
                    return oCurNode.Parent.Parent.Parent;
                case NodeTypes.SegmentActionParameter:
                    return oCurNode.Parent.Parent.Parent.Parent;

                case NodeTypes.VariablesCategory:
                    return oCurNode;
                case NodeTypes.Variable:
                    return oCurNode;
                case NodeTypes.VariableProperty:
                    return oCurNode.Parent;
                case NodeTypes.VariablePropertyValue:
                    return oCurNode.Parent.Parent;
                case NodeTypes.VariableActionsCategory:
                    return oCurNode;
                case NodeTypes.VariableAction:
                    return oCurNode.Parent;
                case NodeTypes.VariableActionProperty:
                    return oCurNode.Parent.Parent;
                case NodeTypes.VariableActionParametersCategory:
                    return oCurNode.Parent.Parent;
                case NodeTypes.VariableActionParameter:
                    return oCurNode.Parent.Parent.Parent;
                
                
                case NodeTypes.VariableCICategory:
                    return oCurNode;
                case NodeTypes.VariableCIProperty:
                    return oCurNode.Parent;
                case NodeTypes.VariableCIPropertyValue:
                    return oCurNode.Parent.Parent;
                
                
                case NodeTypes.ControlActionsCategory:
                    return oCurNode;
                case NodeTypes.ControlAction:
                    return oCurNode;
                case NodeTypes.ControlActionProperty:
                    return oCurNode.Parent.Parent;
                case NodeTypes.ControlActionParametersCategory:
                    return oCurNode;
                case NodeTypes.ControlActionParameter:
                    return oCurNode.Parent.Parent;
                
                
                case NodeTypes.BlocksCategory:
                    return oCurNode;
                case NodeTypes.Block:
                    return oCurNode;
                case NodeTypes.BlockProperty:
                    return oCurNode.Parent;
                case NodeTypes.BlockPropertyValue:
                    return oCurNode.Parent.Parent;
                
                case NodeTypes.AuthorsProperty:
                case NodeTypes.JurisdictionProperty:
                case NodeTypes.SegmentDialogProperty:
                case NodeTypes.LanguageProperty:
                    return oCurNode.Parent;
                case NodeTypes.AuthorsCategory:
                case NodeTypes.JurisdictionCategory:
                case NodeTypes.AnswerFileSegmentsCategory:
                case NodeTypes.SegmentDialogCategory:
                case NodeTypes.LanguageCategory:
                    return oCurNode;
                case NodeTypes.SegmentAuthorsControlActionsCategory:
                case NodeTypes.SegmentJurisdictionControlActionsCategory:
                case NodeTypes.LanguageControlActionsCategory:
                    return oCurNode.Parent;
                case NodeTypes.SegmentAuthorsControlAction:
                case NodeTypes.SegmentJurisdictionControlAction:
                case NodeTypes.LanguageControlAction:
                    return oCurNode.Parent;
                case NodeTypes.SegmentAuthorsControlActionParameter:
                case NodeTypes.SegmentJurisdictionControlActionParameter:
                case NodeTypes.LanguageControlActionParameter:
                    return oCurNode.Parent;
                case NodeTypes.SegmentAuthorsControlActionProperty:
                case NodeTypes.SegmentJurisdictionControlActionProperty:
                case NodeTypes.LanguageControlActionProperty:
                    return oCurNode.Parent;
                case NodeTypes.AnswerFileSegment:
                    return oCurNode;
                case NodeTypes.AnswerFileSegmentProperty:
                    return oCurNode.Parent;
                case NodeTypes.AnswerFileSegmentPropertyValue:
                    return oCurNode.Parent.Parent;
                case NodeTypes.Value:
                    break;
                default:
                    break;
            }
            return null;
        }
        /// <summary>
        /// collapses certain children of designated top level tree node
        /// </summary>
        /// <param name="oCollapseNode"></param>
        private void CollapseDesignatedTreeNodes(UltraTreeNode oCollapseNode)
        {
            //each of these node types are top level nodes for a collapsed
            //tree node structuture.  This function will be called back below
            //to drill down through tree node heirarchy, collapsing as we go
            NodeTypes iType = NodeType(oCollapseNode);
            switch (iType)
            {
                case NodeTypes.SegmentPropertiesCategory:                            
                case NodeTypes.SegmentEventsCategory:
                case NodeTypes.SegmentAction:
                case NodeTypes.SegmentActionParametersCategory:
                case NodeTypes.SegmentAuthorsControlAction:
                case NodeTypes.SegmentAuthorsControlActionParametersCategory:
                case NodeTypes.SegmentAuthorsControlActionsCategory:
                case NodeTypes.Variable:
                case NodeTypes.VariableAction:
                case NodeTypes.VariableActionParametersCategory:
                case NodeTypes.VariableCICategory:
                case NodeTypes.AuthorsCategory:
                case NodeTypes.ControlAction:
                case NodeTypes.ControlActionParametersCategory:
                case NodeTypes.ControlActionsCategory:
                case NodeTypes.Block:
                case NodeTypes.SegmentActionsCategory:
                case NodeTypes.VariableActionsCategory:
                case NodeTypes.JurisdictionCategory:
                case NodeTypes.SegmentJurisdictionControlAction:
                case NodeTypes.SegmentJurisdictionControlActionParametersCategory:
                case NodeTypes.SegmentJurisdictionControlActionsCategory:
                case NodeTypes.SegmentDialogCategory:
                case NodeTypes.AnswerFileSegment:
                case NodeTypes.LanguageActionsCategory:
                case NodeTypes.LanguageAction:
                case NodeTypes.LanguageControlAction:
                case NodeTypes.LanguageControlActionParametersCategory:
                case NodeTypes.LanguageControlActionsCategory:
                case NodeTypes.LanguageCategory:
                case NodeTypes.AuthorsActionsCategory:
                case NodeTypes.AuthorsAction:
                    oCollapseNode.Expanded = false;
                    break;
                case NodeTypes.Segment:
                    if (oCollapseNode.Level > 0)
                        oCollapseNode.Expanded = false;
                    break;
                default:
                    break;
            }   

            //if node is expanded callback recursively -- each pass will collapse designated
            //node types in switch block above
            foreach (UltraTreeNode  oNode in oCollapseNode.Nodes)
            {
                if (oNode.HasNodes && oNode.Expanded)
                    CollapseDesignatedTreeNodes(oNode);       
            }
        }
        /// <summary>
        /// selects the variables tree node whose 
        /// selection tag is the specified Word Tag
        /// used to select tree node when specified XLMNode.Range is selected in document
        /// </summary>
        /// <param name="oWordTag"></param>
        private void SelectNode(Word.XMLNode oWordTag)
        {
            Word.XMLNode oTargetTag = null;
            string xElement = "";

            if (oWordTag != null)
            {
                //get base name
                xElement = oWordTag.BaseName;

                if (xElement == "mSubVar")
                {
                    //for mSubVars, we're actually interested in containing mVar -
                    //keep checking parent node until an mVar is found
                    oTargetTag = oWordTag.ParentNode;
                    while ((oTargetTag != null) && (oTargetTag.BaseName != "mVar"))
                        oTargetTag = oTargetTag.ParentNode;
                }
                else
                    //set target to specified tag
                    oTargetTag = oWordTag;
            }

            if (oTargetTag != null)
            {
                //get containing mSEG
                Word.XMLNode oSegTag = null;
                if (xElement != "mSEG")
                {
                    //keep checking parent node until an mSEG is found
                    oSegTag = oTargetTag.ParentNode;
                    while ((oSegTag != null) && (oSegTag.BaseName != "mSEG"))
                        oSegTag = oSegTag.ParentNode;
                }
                else
                    oSegTag = oTargetTag;

                //get segment
                Segment oSegment = null;
                if (oSegTag != null)
                {
                    string xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(oSegTag);
                    oSegment = this.m_oMPDocument.FindSegment(xSegFullTagID);
                    //populate child segment nodes so tree node exists to be selected
                    RefreshSegmentNodesIfNecessary(xSegFullTagID);
                }

                if (oSegment == null)
                    return;

                switch (xElement)
                {
                    case "mVar":
                    case "mDel":
                    case "mSubVar":
                    {
                        //variable
                        Variables oVariables = null;
                        //if (oSegment != null)
                            oVariables = oSegment.Variables;
                        //else
                            ////standalone
                            //oVariables = m_oMPDocument.Variables;

                        //get the variable whose selection tag is the currently selected node
                        Variable oVar = oVariables.ItemFromAssociatedTag(oTargetTag);

                        if (oVar != null)
                        {
                            //select the node associated with the variable
                            string xKey = "";
                            if (oSegment != null)
                                xKey = oSegment.FullTagID + ".Variables." + oVar.ID;
                            else
                                xKey = oVar.ID;
                            this.SelectNode(xKey);
                        }
                        else if (oSegment != null)
                            //select the segment containing the mVar
                            this.SelectNode(oSegment.FullTagID);
                        else
                            //select default node
                            oTargetTag = null;

                        break;
                    }
                    case "mBlock":
                    {
                        //block
                        Blocks oBlocks = null;
                        //if (oSegment != null)
                            oBlocks = oSegment.Blocks;
                        //else
                        //    //standalone
                        //    oBlocks = m_oMPDocument.Blocks;

                        //get the Block whose selection tag is the currently selected node
                        Block oBlock = oBlocks.ItemFromAssociatedTag(oTargetTag);

                        if (oBlock != null)
                            //select the Block node associated with the Block
                            this.SelectNode(oBlock.TagID);
                        else if (oSegment != null)
                            //select the segment containing the mVar
                            this.SelectNode(oSegment.FullTagID);
                        else
                            //select default node
                            oTargetTag = null;

                        break;
                    }
                    default:
                    {
                        if (oSegment != null)
                            //select the segment node in the variables tree
                            this.SelectNode(oSegment.FullTagID);
                        else
                            //select default node
                            oTargetTag = null;

                        break;
                    }
                }
            }

            //if there's no tree node associated with the selected element, select 
            //node for first document segment if possible
            if ((oTargetTag == null) && (m_oMPDocument.Segments.Count > 0))
                this.SelectNode(m_oMPDocument.Segments[0].FullTagID);
        }

        /// <summary>
        /// selects the tree node whose 
        /// selection tag is the specified content control
        /// used to select tree node when specified range is selected in document
        /// </summary>
        /// <param name="oContentControl"></param>
        private void SelectNode(Word.ContentControl oCC)
        {
            Word.ContentControl oTargetCC = null;
            string xElement = "";

            if (oCC != null)
            {
                //get base name
                xElement = LMP.String.GetBoundingObjectBaseName(oCC.Tag);

                if (xElement == "mSubVar")
                {
                    //for mSubVars, we're actually interested in containing mVar -
                    //keep checking parent node until an mVar is found
                    oTargetCC = oCC.ParentContentControl;
                    while ((oTargetCC != null) &&
                        (LMP.String.GetBoundingObjectBaseName(oTargetCC.Tag) != "mVar"))
                        oTargetCC = oTargetCC.ParentContentControl;
                }
                else
                    //set target to specified tag
                    oTargetCC = oCC;
            }

            if (oTargetCC != null)
            {
                //get containing mSEG
                Word.ContentControl oSegCC = null;
                if (xElement != "mSEG")
                {
                    //keep checking parent node until an mSEG is found
                    oSegCC = oTargetCC.ParentContentControl;
                    while ((oSegCC != null) &&
                        (LMP.String.GetBoundingObjectBaseName(oSegCC.Tag) != "mSEG"))
                        oSegCC = oSegCC.ParentContentControl;
                }
                else
                    oSegCC = oTargetCC;

                //get segment
                Segment oSegment = null;
                if (oSegCC != null)
                {
                    string xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oSegCC);
                    oSegment = this.m_oMPDocument.FindSegment(xSegFullTagID);
                    //populate child segment nodes so tree node exists to be selected
                    RefreshSegmentNodesIfNecessary(xSegFullTagID);
                }

                if (oSegment == null)
                    return;

                switch (xElement)
                {
                    case "mVar":
                    case "mDel":
                    case "mSubVar":
                        {
                            //variable
                            Variables oVariables = null;
                            //if (oSegment != null)
                            oVariables = oSegment.Variables;
                            //else
                            ////standalone
                            //oVariables = m_oMPDocument.Variables;

                            //get the variable whose selection tag is the currently selected node
                            Variable oVar = oVariables.ItemFromAssociatedContentControl(oTargetCC);

                            if (oVar != null)
                            {
                                //select the node associated with the variable
                                string xKey = "";
                                if (oSegment != null)
                                    xKey = oSegment.FullTagID + ".Variables." + oVar.ID;
                                else
                                    xKey = oVar.ID;
                                this.SelectNode(xKey);
                            }
                            else if (oSegment != null)
                                //select the segment containing the mVar
                                this.SelectNode(oSegment.FullTagID);
                            else
                                //select default node
                                oTargetCC = null;

                            break;
                        }
                    case "mBlock":
                        {
                            //block
                            Blocks oBlocks = null;
                            //if (oSegment != null)
                            oBlocks = oSegment.Blocks;
                            //else
                            //    //standalone
                            //    oBlocks = m_oMPDocument.Blocks;

                            //get the Block whose selection tag is the currently selected node
                            Block oBlock = oBlocks.ItemFromAssociatedContentControl(oTargetCC);

                            if (oBlock != null)
                                //select the Block node associated with the Block
                                this.SelectNode(oBlock.TagID);
                            else if (oSegment != null)
                                //select the segment containing the mVar
                                this.SelectNode(oSegment.FullTagID);
                            else
                                //select default node
                                oTargetCC = null;

                            break;
                        }
                    default:
                        {
                            if (oSegment != null)
                                //select the segment node in the variables tree
                                this.SelectNode(oSegment.FullTagID);
                            else
                                //select default node
                                oTargetCC = null;

                            break;
                        }
                }
            }

            //if there's no tree node associated with the selected element, select 
            //node for first document segment if possible
            if ((oTargetCC == null) && (m_oMPDocument.Segments.Count > 0))
                this.SelectNode(m_oMPDocument.Segments[0].FullTagID);
        }
        
        /// <summary>
        /// populates child segments of segment with designated full tag id
        /// if they do not already exist
        /// </summary>
        /// <param name="oSegment"></param>
        private void RefreshSegmentNodesIfNecessary(string xSegmentFullTagID)
        {
            //check if the segment node belonging to selected word tag actually exists
            UltraTreeNode oSegNode = this.treeDocContents.GetNodeByKey(xSegmentFullTagID);

            if (oSegNode == null)
            {
                //split the parent segment tag FullTagID and use to 
                //search for and if necessary create and populate
                //child segment nodes from the top down.  We assume
                //top level node already exists and is populated

                string[] xKeys = xSegmentFullTagID.Split('.');
                StringBuilder oSB = new StringBuilder();
                string xSegKey = oSB.Append(xKeys[0]).ToString();

                for (int i = 1; i < xKeys.Length; i++)
                {
                    oSegNode = null;
                    xSegKey = oSB.AppendFormat(".{0}", xKeys[i]).ToString();
                    //attempt to find associated node
                    oSegNode = this.treeDocContents.GetNodeByKey(xSegKey);

                    if (oSegNode == null)
                    {
                        //we have to insert and refresh a new child node
                        Segment oSegment = this.m_oMPDocument.FindSegment(xSegKey);
                        oSegNode = this.InsertNode(oSegment, oSegment.Parent);
                        this.RefreshSegmentNode(oSegNode);
                    }
                    else
                    {
                        //node has been created
                        //don't refresh if it already has been populated, i.e
                        //it has already been expanded by user
                        if (oSegNode.Nodes.Count == 0)
                            this.RefreshSegmentNode(oSegNode);
                    }
                }
            }
            else
            {
                //segment node exists, but has not been populated
                //ie, we're seeking a node in first child of top level segment
                if (!oSegNode.HasNodes)
                    this.RefreshSegmentNode(oSegNode); 
            }
        }
        /// <summary>
        /// selects the previous node on the tree
        /// </summary>
        private void SelectPreviousNode()
        {
            UltraTreeNode oPrevNode = this.m_oCurNode.PrevVisibleNode;

            if (oPrevNode != null)
            {
                //skip value nodes 
                while (oPrevNode.Key.EndsWith("_Value"))
                    oPrevNode = oPrevNode.PrevVisibleNode;
                this.treeDocContents.ActiveNode = oPrevNode;
            }
        }
        /// <summary>
        /// selects the next node on the tree
        /// </summary>
        private void SelectNextNode()
        {
            UltraTreeNode oNode = null;

            oNode = this.m_oCurNode.NextVisibleNode;


            if (oNode != null)
            {
                //skip value nodes 
                while (oNode.Key.EndsWith("_Value"))
                    oNode = oNode.NextVisibleNode;
                //there is a next node - select it 
                this.treeDocContents.ActiveNode = oNode;
            }
            else
                //there is no next node - select first node 
                this.treeDocContents.ActiveNode = this.treeDocContents.Nodes[0]; 
        }
        /// <summary>
        /// ensures that the specified node is visible in the tree
        /// </summary>
        /// <param name="oTreeNode"></param>
        private void EnsureVisibleNode(UltraTreeNode oTreeNode)
        {
            //UltraTreeNode oParent = oTreeNode;

            //while (!oTreeNode.IsInView)
            //{
            //    //get parent
            //    oParent = oParent.Parent;

            //    //exit if no more parents
            //    if (oParent == null)
            //        break;

            //    //expand parent
            //    oParent.Expanded = true;
            //}
            oTreeNode.BringIntoView();
        }
        /// <summary>
        /// sets up edit mode for the specified  
        /// tree node if necessary and appropriate
        /// </summary>
        /// <param name="oTreeNode"></param>
        private void EnterEditMode(UltraTreeNode oTreeNode, bool bSelectTag)
        {
            if (oTreeNode == null)
                //node is null - exit
                return;

            //ensure that node is visible
            //before entering edit mode
            EnsureVisibleNode(oTreeNode);

            //GLOG: 3084 - we should not be entering property edit mode
            //when the selected node is the Parameters node
            if (oTreeNode.Text != "Parameters")
            {
                string xName = null;

                //tags can be controls, segments, authors, etc; we only want strings for editing
                try
                {
                    if(oTreeNode.Tag != null)
                        xName = oTreeNode.Tag.GetType().Name;
                }
                catch { }

                if (xName == "String" || oTreeNode.Tag is Control || (xName == null && !oTreeNode.HasNodes))
                    EnterPropertyEditMode(oTreeNode);
            }

            //GLOG - 3403 - CEH
            if (bSelectTag && !this.TaskPane.WordTagChangeOccurring)
            {
                //select appropriate Word tag
                if (oTreeNode.Tag is AdminVariable || oTreeNode.Tag is UserVariable)
                    SelectVariableTag((Variable)oTreeNode.Tag);
                else if (oTreeNode.Tag is Block)
                    SelectBlockTag((Block)oTreeNode.Tag);
            }
        }

        /// <summary>
        /// select the tag associated with the specified block
        /// </summary>
        /// <param name="oBlock"></param>
        private void SelectBlockTag(Block oBlock)
        {
            //select range associated with block if there is one
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //xml tag
                Word.XMLNode aTag = oBlock.AssociatedWordTag;

                if (aTag != null)
                {
                    m_bProgrammaticallySelectingWordTag = true;

                    //select tag range
                    LMP.Forte.MSWord.WordDoc.SelectTag(aTag, false, false);

                    m_bProgrammaticallySelectingWordTag = false;
                }
            }
            else
            {
                //content control
                Word.ContentControl oCC = oBlock.AssociatedContentControl;

                if (oCC != null)
                {
                    m_bProgrammaticallySelectingWordTag = true;

                    //select tag range
                    LMP.Forte.MSWord.WordDoc.SelectContentControl(oCC, false, false);

                    m_bProgrammaticallySelectingWordTag = false;
                }
            }
        }

        /// <summary>
        /// selects the first mVar associated with the specified variable
        /// </summary>
        /// <param name="oVar"></param>
        private void SelectVariableTag(Variable oVar)
        {
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //xml tag
                Word.XMLNode[] aTags = oVar.AssociatedWordTags;

                if (aTags.Length > 0 && aTags[0] != null)
                {
                    m_bProgrammaticallySelectingWordTag = true;

                    //select tag range
                    LMP.Forte.MSWord.WordDoc.SelectTag(aTags[0], false, false);

                    m_bProgrammaticallySelectingWordTag = false;
                }
            }
            else
            {
                //content control
                Word.ContentControl[] aCCs = oVar.AssociatedContentControls;

                if (aCCs.Length > 0 && aCCs[0] != null)
                {
                    m_bProgrammaticallySelectingWordTag = true;

                    //select tag range
                    LMP.Forte.MSWord.WordDoc.SelectContentControl(aCCs[0], false, false);

                    m_bProgrammaticallySelectingWordTag = false;
                }
            }
        }

        /// <summary>
        /// sets up for design property editing
        /// <param name="oTreeNode"></param>
        private void EnterPropertyEditMode(UltraTreeNode oTreeNode)
        {
            string xPropName = null;

            //get value node
            UltraTreeNode oValueNode = null;
            if (oTreeNode.HasNodes)
                oValueNode = oTreeNode.Nodes[0];
            else
                oValueNode = oTreeNode;
            
            //TODO: Getproperty name from key only
            if (oValueNode.Parent.Tag is string)
                xPropName = oValueNode.Parent.Tag.ToString();
            else
                //get variable prop name from key - 
                xPropName = GetPropNameFromKey(oValueNode.Key);
            
            NodeTypes iType = NodeType(oValueNode);

            if (iType == NodeTypes.VariableProperty && xPropName == "ControlProperties")
            {
                //always reload control properties grid to ensure refresh of buttons
                oValueNode.Tag = SetupVariablePropertyControls
                    (xPropName, GetVariableFromNode(oTreeNode).ControlProperties);
            }
            else if (iType == NodeTypes.SegmentPropertyValue && xPropName == "Culture")
            {
                //always reload languages list to reflect supported languages
                oValueNode.Tag = SetupSegmentPropertyControls(xPropName, oValueNode.Text);
            }

            if (oValueNode.Tag == null)
            {
                switch (iType)
                {
                    case NodeTypes.SegmentPropertyValue:
                        //we've selected a parameter property node
                        oValueNode.Tag = SetupSegmentPropertyControls
                            (xPropName, oValueNode.Text);
                        if (oValueNode.Tag is IntendedUsesComboBox && !GetSegmentFromNode(oValueNode).IsTopLevel)
                            ((IntendedUsesComboBox)oValueNode.Tag).AllowStyleSheet = false;
                        break;
                    case NodeTypes.SegmentActionProperty:
                        //we've selected a parameter property node
                        //we can use variable actions parameters control
                        //since segment.action.type = variable.action.type
                        oValueNode.Tag = SetupVariableActionPropertyControls
                            (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.SegmentActionParameter:
                        //we've selected a parameter property node
                        //we can use variable actions parameters control
                        //since segment.action.type = variable.action.type
                        oValueNode.Tag = SetupVariableActionParameterControls
                            (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.VariableProperty:
                        oValueNode.Tag = SetupVariablePropertyControls
                               (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.VariableActionProperty:
                        oValueNode.Tag = SetupVariableActionPropertyControls
                            (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.VariableActionParameter:
                        //we've selected a parameter property node
                        oValueNode.Tag = SetupVariableActionParameterControls
                            (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.VariableCIPropertyValue:
                        //set up CI prop node
                        oValueNode.Tag = SetupCIPropertyControls
                            (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.ControlActionProperty:
                    case NodeTypes.SegmentAuthorsControlActionProperty:
                    case NodeTypes.SegmentJurisdictionControlActionProperty:
                    case NodeTypes.LanguageControlActionProperty:
                        //we've selected a control action property node
                        oValueNode.Tag = SetupControlActionPropertyControls
                            (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.ControlActionParameter:
                    case NodeTypes.SegmentAuthorsControlActionParameter:
                    case NodeTypes.SegmentJurisdictionControlActionParameter:
                    case NodeTypes.LanguageControlActionParameter:
                        //we've selected a parameter property node
                        oValueNode.Tag = SetupControlActionParameterControls
                            (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.BlockPropertyValue:
                        oValueNode.Tag = SetupBlockPropertyControls
                            (xPropName, oValueNode.Text);
                        break;
                    case NodeTypes.AnswerFileSegmentPropertyValue:
                        oValueNode.Tag = SetupAnswerFileSegmentPropertyControls
                            (xPropName, oValueNode.Text);
                        break;
                    default:
                        return;
                }
            }

            //get the control to show
            this.m_oCurCtl = (Control)oValueNode.Tag;

            //****THIS LINE CAUSES A REFRESH OF THE TREE, WHICH MAY CAUSE NODES TO BE REPOSITIONED
            //set underlying node transparent to prevent display behind control
            //oValueNode.Override.NodeAppearance.ForegroundAlpha =
            //    Infragistics.Win.Alpha.Transparent;

            ////disable expansion of parent node via double click
            oValueNode.Parent.Override.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //show help text in description pane
            //tag property should contain resource string help id filled in
            //one of the setup procs above

            string xHelpID = m_oCurCtl.Tag.ToString();
            string xHelpFile = "file:///" + 
                LMP.Data.Application.GetDirectory(mpDirectories.Help) + "\\designer\\" + xHelpID + ".html";

            //assign help text to description pane
            TaskPane.SetHelpText(wbHelpText, xHelpFile);

            int iTop = 0;
            int iLeft = 0;

            if (this.TaskPane.WordTagChangeOccurring)
            {
                //the user selected the word tag
                //TODO: we may want to expand appropriate design node
            }
            else
            {
                int iControlWidth = 0;
                
                //GLOG : 8546 : ceh
                //set control width based on scroll bar visibility
                if (this.IsScrollbarVisible(mpScrollbarType.Vertical))
                    iControlWidth = this.treeDocContents.Width -
                        oValueNode.Bounds.Left - ((int)(20 * Session.ScreenScalingFactor));
                else
                    iControlWidth = this.treeDocContents.Width -
                        oValueNode.Bounds.Left - ((int)(5 * Session.ScreenScalingFactor));

                //Adjust spacing after, taking into account multi-line labels
                oValueNode.Override.NodeSpacingAfter =
                   Math.Max(10, this.m_oCurCtl.Height - 
                   ((LMP.String.CountChrs(oValueNode.Text, "\r\n") + 1) * 12));

                if (oTreeNode == oValueNode)
                    //we're currently referencing the value node
                    iTop = oTreeNode.Parent.Bounds.Top + oTreeNode.Parent.Bounds.Height + 3;
                else
                    iTop = oTreeNode.Bounds.Top + oTreeNode.Bounds.Height + 3;

                iLeft = oValueNode.Bounds.Left;
                oValueNode.Override.NodeAppearance.ForeColor = Color.White;

                this.m_oCurCtl.SetBounds(iLeft, iTop, iControlWidth, this.m_oCurCtl.Height);

                //add control to container
                this.treeDocContents.Controls.Add(this.m_oCurCtl);
                //Scroll up until entire control is visible
                int iHeight = this.treeDocContents.Height;
                //Adjust if Horizontal Scrollbar is visible
                if (this.IsScrollbarVisible(mpScrollbarType.Horizontal))
                    iHeight = iHeight - 14;
                while (m_oCurCtl.Top + m_oCurCtl.Height > iHeight)
                {
                    UltraTreeNode oTopNode = this.treeDocContents.TopNode;
                    this.treeDocContents.TopNode = oTopNode.NextVisibleNode;
                    if (oTreeNode.HasNodes)
                        iTop = oTreeNode.Bounds.Top + oTreeNode.Bounds.Height + 3;
                    else 
                        iTop = oTreeNode.Bounds.Top - 10;
                    m_oCurCtl.Top = iTop;
                }
            }

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;
                //set control value
                string xValue = GetPropertyValueFromNode(oValueNode);

                //handle special cases
                //some controls have default value method if prop = null;
                switch (m_oCurCtl.GetType().Name)
                {
                    case "DefaultLocationComboBox":
                        xValue = xValue == "0" ? "1" : xValue;
                        oCtl.Value = xValue;
                        oCtl.IsDirty = false;
                        break;
                    case "ListCombo":
                        xValue = xValue == "0" ? xValue = "" : xValue;
                        oCtl.Value = xValue;
                        oCtl.IsDirty = false;
                        break;
                    case "ControlPropertiesGrid":
                        if (System.String.IsNullOrEmpty(xValue)) 
                        {
                            if (iType == NodeTypes.VariableProperty)
                            {
                                //load default value for variable control properties
                                //based on control type for variable - this allow us to
                                //refresh property grid if control type is edited by user

                                Variable oVar = GetVariableFromNode(m_oCurNode);
                                if (oVar != null)
                                {
                                    LMP.Data.mpControlTypes iCType = oVar.ControlType;
                                    ((LMP.Controls.ControlPropertiesGrid)m_oCurCtl).LoadDefaultValues(iCType);
                                }
                            }
                            else if (iType == NodeTypes.JurisdictionProperty)
                            {
                                ((LMP.Controls.ControlPropertiesGrid)m_oCurCtl).LoadDefaultValues(LMP.Data.mpControlTypes.JurisdictionChooser);
                            }
                            else if (iType == NodeTypes.SegmentProperty)
                            {
                                ((LMP.Controls.ControlPropertiesGrid)m_oCurCtl).LoadDefaultValues(LMP.Data.mpControlTypes.AuthorSelector);
                            }
                            //mark as edited
                            oCtl.IsDirty = true;
                        }
                        else
                        {
                            //xValue has been already been set by selecting control type for variable or 
                            //by initialization of segment author property nodes.
                            //pass control type for variable, or author selector for segment property
                            //to instance of control property grid
                            LMP.Data.mpControlTypes iCtlType = mpControlTypes.None;
                            if (iType == NodeTypes.SegmentPropertyValue)
                                if (NodeType(m_oCurNode) == NodeTypes.JurisdictionProperty)
                                    iCtlType = mpControlTypes.JurisdictionChooser;
                                else
                                    iCtlType = mpControlTypes.AuthorSelector;
                            else
                            {
                                Variable oVar = GetVariableFromNode(m_oCurNode);
                                if (oVar != null)
                                    iCtlType = oVar.ControlType;
                            }
                            
                            //set control type property for grid
                            ((LMP.Controls.ControlPropertiesGrid)oCtl).ControlType = iCtlType;
                            //set value for cp grid
                            oCtl.Value = xValue;
                        }
                        break;
                    default:
                        oCtl.Value = xValue;
                        //mark as not edited
                        oCtl.IsDirty = false;
                        break;
                }

            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }
            }
        }
        /// <summary>
        /// parses specified node key
        /// sets design property value of variable, block, variable/control actions
        /// or parameters, saves segment property XML if indicated
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oCtl"></param>
        /// <param name="bSaveSegmentProperty"></param>
        private void SetPropertyValueFromNode(UltraTreeNode oNode, LMP.Controls.IControl oCtl, bool bSaveSegmentProperty)
        {
            string xKey = oNode.Key;

            char cSep = '.';
            string xPropValue = null;

            Variable oVar = null;
            Block oBlock = null;
            Segment oSeg = null;
            VariableAction oVarAction = null;
            ControlAction oCtlAction = null;
            SegmentAction oSegAction = null;

            string[] aKeyItems = xKey.Split(cSep);
            //get prop name from key -- propname is upperbound
            int iIndex = aKeyItems.GetUpperBound(0);
            string xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

            switch (NodeType(oNode))
            {
                case NodeTypes.SegmentPropertyValue:
                    //segment design property value
                    SetSegmentPropertyValueFromNode(oNode, oCtl, xPropName, bSaveSegmentProperty);
                    break;

                case NodeTypes.VariableCIPropertyValue:
                case NodeTypes.VariableProperty:
                    //variable design property value
                    SetVariablePropertyValueFromNode(oNode, oCtl, xPropName);
                    break;
                
                case NodeTypes.AnswerFileSegmentProperty:
                case NodeTypes.AnswerFileSegmentPropertyValue:
                    AnswerFileSegment oAFSeg = GetAnswerFileSegmentFromNode(oNode);
                    if (oAFSeg != null && xPropName != null)
                    {
                        xPropValue = oCtl.Value;
                        SetAnswerFileSegmentPropertyValueFromNode(oNode, oCtl, xPropName);
                    }
                    break;
                case NodeTypes.BlockPropertyValue:
                    //block design property value
                    oBlock = GetBlockFromNode(oNode);
                    if (oBlock != null & xPropName != null)
                    {
                        //set design property
                        xPropValue = oCtl.Value;
                        SetBlockPropertyValue(oBlock, xPropName, xPropValue);
                    }
                    break;

                case NodeTypes.VariableActionProperty:
                    //variable action design property value
                    oVar = GetVariableFromNode(oNode);
                    oVarAction = oVar.VariableActions[Int32.Parse(aKeyItems[iIndex - 1])];
                    
                    //Variable Action property name is aKeyItems[upperbound] + "_Value"
                    xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

                    if (oVarAction != null & xPropName != null)
                    {
                        //set design property
                        xPropValue = oCtl.Value;
                        SetVariableActionPropertyValue(oVarAction, xPropName, xPropValue);  
                    }
                    break;

                case NodeTypes.SegmentActionProperty:
                    //segment action design property value
                    oSeg = GetSegmentFromNode(oNode);
                    oSegAction = oSeg.Actions[Int32.Parse(aKeyItems[iIndex - 1])];
                    
                    //property name is aKeyItems[upperbound] + "_Value"
                    xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

                    if (oSegAction != null & xPropName != null)
                    {
                        //set design property
                        xPropValue = oCtl.Value;
                        SetSegmentActionPropertyValue(oSegAction, xPropName, xPropValue);   
                    }
                    break;

                case NodeTypes.ControlActionProperty:
                    //control action design property value
                    oVar = GetVariableFromNode(oNode);
                    oCtlAction = oVar.ControlActions[Int32.Parse(aKeyItems[iIndex - 1])];

                    //property name is aKeyItems[3] + "_Value"
                    xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

                    if (oCtlAction != null & xPropName != null)
                    {
                        //set design property
                        xPropValue = oCtl.Value;
                        SetControlActionPropertyValue(oCtlAction, xPropName, xPropValue);  
                    }
                    break;

                case NodeTypes.SegmentAuthorsControlActionProperty:
                    //control action design property value
                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.Authors.ControlActions[Int32.Parse(aKeyItems[iIndex - 1])];

                    //property name is aKeyItems[3] + "_Value"
                    xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

                    if (oCtlAction != null & xPropName != null)
                    {
                        //set design property
                        xPropValue = oCtl.Value;
                        SetControlActionPropertyValue(oCtlAction, xPropName, xPropValue);
                    }
                    break;

                case NodeTypes.SegmentJurisdictionControlActionProperty:
                    //control action design property value
                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.CourtChooserControlActions[Int32.Parse(aKeyItems[iIndex - 1])];

                    //property name is aKeyItems[3] + "_Value"
                    xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

                    if (oCtlAction != null & xPropName != null)
                    {
                        //set design property
                        xPropValue = oCtl.Value;
                        SetControlActionPropertyValue(oCtlAction, xPropName, xPropValue);
                    }
                    break;

                case NodeTypes.LanguageControlActionProperty:
                    //control action design property value
                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.LanguageControlActions[Int32.Parse(aKeyItems[iIndex - 1])];

                    //property name is aKeyItems[3] + "_Value"
                    xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

                    if (oCtlAction != null & xPropName != null)
                    {
                        //set design property
                        xPropValue = oCtl.Value;
                        SetControlActionPropertyValue(oCtlAction, xPropName, xPropValue);
                    }
                    break;

                case NodeTypes.VariableActionParameter:
                    //variable action parameter value
                    SetVariableActionParameterValueFromNode(oNode, oCtl);
                    break;

                case NodeTypes.SegmentActionParameter:
                    //segment action parameter value
                    SetSegmentActionParameterValueFromNode(oNode, oCtl);
                    break;

                case NodeTypes.ControlActionParameter: 
                    //control action parameter value
                    SetControlActionParameterValueFromNode(oNode, oCtl);
                    break;
                
                case NodeTypes.SegmentAuthorsControlActionParameter:
                case NodeTypes.SegmentJurisdictionControlActionParameter:
                case NodeTypes.LanguageControlActionParameter:
                    //control action parameter value
                    SetControlActionParameterValueFromNode(oNode, oCtl);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// sets designated Segment Action parameter from Treenode control
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oCtl"></param>
        private void SetSegmentActionParameterValueFromNode(UltraTreeNode oNode, IControl oCtl)
        {
            string xPropValue = null;
            string[] aKeyItems = oNode.Key.Split('.');
            int iIndex = aKeyItems.GetUpperBound(0);
            
            Segment oSeg = GetSegmentFromNode(oNode);
            SegmentAction oSegAction = oSeg.Actions[Int32.Parse(aKeyItems[iIndex - 2])];

            //property name is aKeyItems[5] + "_Value"
            string xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

            //get parameter names and values - 
            //number of parameters and names vary according to variable action type
            //we can use variable action parameters since types are the same
            string[,] aParamNames = GetVariableActionParameters(oSegAction.Type);
            string[] aParamValues = oSegAction.Parameters.Split
                (StringArray.mpEndOfSubField);

            if (aParamNames.GetUpperBound(0) != aParamValues.GetUpperBound(0))
            {
                //invalid parameter string - assign default values
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    aParamValues[i] = aParamNames[i, 1];
                }
            }

            //set new value in aParamValues array, rebuild the parameter string
            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                if (xPropName == aParamNames[i, 0])
                {
                    aParamValues[i] = oCtl.Value;
                    xPropValue = BuildParameterString(aParamValues);
                    oSegAction.Parameters = xPropValue;
                    break;
                }
        }
        /// <summary>
        /// sets designated Variable Action parameter from Treenode control
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oCtl"></param>
        private void SetVariableActionParameterValueFromNode(UltraTreeNode oNode, IControl oCtl)
        {
            string xPropValue = null;
            string[] aKeyItems = oNode.Key.Split('.');
            int iIndex = aKeyItems.GetUpperBound(0);

            Variable oVar = GetVariableFromNode(oNode);
            //get variable action from key.  upperbound - 2 holds variable action index
            VariableAction oVarAction = oVar.VariableActions[Int32.Parse(aKeyItems[iIndex - 2])];

            //property name is aKeyItems[7] + "_Value" - its the last field so we use upper bound
            string xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

            //get parameter names and values - 
            //number of parameters and names vary according to variable action type
            string[,] aParamNames = GetVariableActionParameters
                (oVarAction.Type);
            string[] aParamValues = oVarAction.Parameters.Split
                (StringArray.mpEndOfSubField);

            if (aParamNames.GetUpperBound(0) != aParamValues.GetUpperBound(0))
            {
                aParamValues = new string[aParamNames.GetUpperBound(0) + 1];
                //invalid parameter string - assign default values from second array col
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    aParamValues[i] = aParamNames[i, 1];
                }
            }

            //set new value in aParamValues array, rebuild the parameter string
            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
            {
                if (xPropName == aParamNames[i, 0])
                {
                    aParamValues[i] = oCtl.Value;
                    xPropValue = BuildParameterString(aParamValues);
                    oVarAction.Parameters = xPropValue;
                    break;
                }
            }
        }
        /// <summary>
        /// sets designated Control Action parameter from Treenode control
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oCtl"></param>
        private void SetControlActionParameterValueFromNode(UltraTreeNode oNode, IControl oCtl)
        {
            string xPropValue = null;
            string[] aKeyItems = oNode.Key.Split('.');
            int iIndex = aKeyItems.GetUpperBound(0);
            ControlAction oCtlAction = null;

            if (NodeType(oNode) == NodeTypes.SegmentAuthorsControlActionParameter)
            {
                //get segment authors control action from key.  upperbound - 2 holds variable action index
                Segment oSeg = GetSegmentFromNode(oNode);
                oCtlAction = oSeg.Authors.ControlActions[Int32.Parse(aKeyItems[iIndex - 2])];
            }
            else if (NodeType(oNode) == NodeTypes.SegmentJurisdictionControlActionParameter)
            {
                //get segment jurisdiction control action from key.  upperbound - 2 holds variable action index
                Segment oSeg = GetSegmentFromNode(oNode);
                oCtlAction = oSeg.CourtChooserControlActions[Int32.Parse(aKeyItems[iIndex - 2])];
            }
            else if (NodeType(oNode) == NodeTypes.LanguageControlActionParameter)
            {
                //get segment language control action from key.  upperbound - 2 holds variable action index
                Segment oSeg = GetSegmentFromNode(oNode);
                oCtlAction = oSeg.LanguageControlActions[Int32.Parse(aKeyItems[iIndex - 2])];
            }
            else
            {
                //get variable control action from key.  upperbound - 2 holds variable action index
                Variable oVar = GetVariableFromNode(oNode);
                oCtlAction = oVar.ControlActions[Int32.Parse(aKeyItems[iIndex - 2])];
            }


            //property name is aKeyItems[7] + "_Value" - its the last field so we use upper bound
            string xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

            //get parameter names and values - 
            //number of parameters and names vary according to variable action type
            string[,] aParamNames = GetControlActionParameters
                (oCtlAction.Type);
            string[] aParamValues = oCtlAction.Parameters.Split
                (StringArray.mpEndOfSubField);

            if (aParamNames.GetUpperBound(0) != aParamValues.GetUpperBound(0))
            {
                aParamValues = new string[aParamNames.GetUpperBound(0) + 1];
                //invalid parameter string - assign default values from second array col
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    aParamValues[i] = aParamNames[i, 1];
                }
            }

            //set new value in aParamValues array, rebuild the parameter string
            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
            {
                if (xPropName == aParamNames[i, 0])
                {
                    aParamValues[i] = oCtl.Value;
                    xPropValue = BuildParameterString(aParamValues);
                    oCtlAction.Parameters = xPropValue;
                    break;
                }
            }
        }
        /// <summary>
        /// sets designated variable property from TreeNode control
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oCtl"></param>
        /// <param name="xPropName"></param>
        private void SetVariablePropertyValueFromNode(UltraTreeNode oNode, IControl oCtl, string xPropName)
        {
            string xPropValue = null;
            Variable oVar = GetVariableFromNode(oNode);
            if (oVar != null & xPropName != null)
            {
                //handle control type property -- we want to refresh the control properties property
                //and value node text when control type is set
                if (xPropName == "IsMultiValue")
                {
                    xPropValue = oCtl.Value;
                    SetVariablePropertyValue(oVar, xPropName, xPropValue);
                    string xCTKey = oNode.Key.Replace(".IsMultiValue", ".ControlType");
                    UltraTreeNode oCTNode = treeDocContents.GetNodeByKey(xCTKey);
                    if (oCTNode != null)
                    {
                        //Clear ControlType Node Tag so list will repopulate
                        oCTNode.Tag = null;
                    }
                    if (xPropValue.ToUpper() == "TRUE")
                    {
                        switch (oVar.ControlType)
                        {
                            //Only these control types are supported for Mult-Value variables
                            case mpControlTypes.DetailGrid:
                            case mpControlTypes.DetailList:
                            case mpControlTypes.Textbox:
                            case mpControlTypes.MultilineCombo:
                            case mpControlTypes.Combo:
                            case mpControlTypes.DropdownList:
                            case mpControlTypes.Checkbox:
                            case mpControlTypes.MultilineTextbox:
                                break;
                            default:
                                //If anything else, switch to Textbox and update ControlType and ControlProperties nodes
                                oVar.ControlType = mpControlTypes.Textbox;
                                if (oCTNode != null)
                                {
                                    //Update ControlType display value
                                    oCTNode.Text = oVar.ControlType.ToString();
                                }

                                //set control properties property value to default values based on control type
                                //default property setting values for each control type are stored in the 
                                //control properties grid control
                                LMP.Controls.ControlPropertiesGrid oCPCtl = new LMP.Controls.ControlPropertiesGrid();
                                LMP.Data.mpControlTypes iCType = oVar.ControlType;
                                string xProps = ((ControlPropertiesGrid)oCPCtl).GetDefaultValues(iCType);
                                oVar.ControlProperties = xProps;

                                //refresh the control properties node text
                                //get the value tree node via the key
                                string xCPKey = oNode.Key.Replace(".IsMultiValue", ".ControlProperties");
                                UltraTreeNode oCPNode = treeDocContents.GetNodeByKey(xCPKey);
                                if (oCPNode != null)
                                {
                                    //configure node text
                                    if (xProps == null || xProps == "")
                                        oCPNode.Text = "Null";
                                    else
                                    {
                                        string xSep = StringArray.mpEndOfSubValue.ToString();
                                        string xRep = "\r\n";
                                        oCPNode.Text = xProps.Replace(xSep, xRep);
                                    }
                                }
                                break;
                        }
                    }
                    else
                    {
                    }
                }
                else if (xPropName == "ControlType")
                {
                    //set control type property value
                    xPropValue = oCtl.Value;
                    oVar.ControlType = (LMP.Data.mpControlTypes)Int32.Parse(oCtl.Value);

                    //set control properties property value to default values based on control type
                    //default property setting values for each control type are stored in the 
                    //control properties grid control

                    LMP.Controls.ControlPropertiesGrid oCPCtl = new LMP.Controls.ControlPropertiesGrid();
                    LMP.Data.mpControlTypes iCType = oVar.ControlType;
                    string xProps = ((ControlPropertiesGrid)oCPCtl).GetDefaultValues(iCType);
                    oVar.ControlProperties = xProps;

                    //refresh the control properties node text
                    //get the value tree node via the key
                    string xNewKey = oNode.Key.Replace(".ControlType", ".ControlProperties");
                    UltraTreeNode oCPNode = treeDocContents.GetNodeByKey(xNewKey);
                    if (oCPNode != null)
                    {
                        //configure node text
                        if (xProps == null || xProps == "")
                            oCPNode.Text = "Null";
                        else
                        {
                            string xSep = StringArray.mpEndOfSubValue.ToString();
                            string xRep = "\r\n";
                            oCPNode.Text = xProps.Replace(xSep, xRep);
                        }
                    }
                }
                else
                {
                    //set design property for other variable properties
                    xPropValue = oCtl.Value;
                    SetVariablePropertyValue(oVar, xPropName, xPropValue);
                }
            }
        }
        /// <summary>
        /// sets designated variable property from TreeNode control
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oCtl"></param>
        /// <param name="xPropName"></param>
        private void SetAnswerFileSegmentPropertyValueFromNode(UltraTreeNode oNode, IControl oCtl, string xPropName)
        {
            string xPropValue = null;
            AnswerFileSegment oSeg = GetAnswerFileSegmentFromNode(oNode);
            if (oSeg != null & xPropName != null)
            {
                xPropValue = oCtl.Value;
                SetAnswerFileSegmentPropertyValue(oSeg, xPropName, xPropValue);
            }
        }
        /// <summary>
        /// sets designated segment property from TreeNode control
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oCtl"></param>
        /// <param name="xPropName"></param>
        /// <param name="bSaveSegmentProperty"></param>
        private void SetSegmentPropertyValueFromNode(UltraTreeNode oNode, IControl oCtl, string xPropName, bool bSaveSegmentProperty)
        {
            string xPropValue = null;
            Segment oSeg = GetSegmentFromNode(oNode);

            if (oSeg != null & xPropName != null)
            {
                bool bSetStyleSheet = false;
                //set other segment design properties
                xPropValue = oCtl.Value;

                if (xPropName == "IntendedUse" && xPropValue == ((int)mpSegmentIntendedUses.AsStyleSheet).ToString())
                {
                    //Intended Use has been changed to Style Sheet
                    //Prompt before deleting document content
                    DialogResult iRet = MessageBox.Show(LMP.Resources.GetLangString("Prompt_ChangeSegmentToStyleSheet"), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (iRet == DialogResult.No)
                    {
                        //Reset Control to original value (so label will be correct)
                        oCtl.Value = ((int)oSeg.IntendedUse).ToString();
                        return;
                    }
                    else
                        bSetStyleSheet = true;
                }

                //save segment prop
                SetSegmentPropertyValue(oSeg, xPropName, xPropValue);

                //save segment property XML
                if (bSaveSegmentProperty == true)
                    SaveSegmentPropertyXML(oSeg, xPropName, xPropValue);
                
                if (bSetStyleSheet)
                {
                    //If Style Sheet was set, remove all content except style definitions
                    LMP.Architect.Api.ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    if (oSeg.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        oSeg.WordTags[0].Range.Text = "";
                    }
                    else
                    {
                        oSeg.ContentControls[0].Range.Text = "";
                    }

                    LMP.Forte.MSWord.WordDoc.ClearHeaders(this.m_oMPDocument.WordDocument.Sections[1], false, false);
                    LMP.Forte.MSWord.WordDoc.ClearFooters(this.m_oMPDocument.WordDocument.Sections[1], false, false);
                    ForteDocument.IgnoreWordXMLEvents = iIgnore;
                }
            }
        }
        /// <summary>
        /// explicitly sets all Block property values
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        private void SetBlockPropertyValue(Block oBlock, string xPropName, string xPropValue)
        {
            switch (xPropName)
            {
                case "IsBody":
                    oBlock.IsBody = System.Convert.ToBoolean(xPropValue); 
                    break;
                case "ShowInTree":
                    oBlock.ShowInTree = System.Convert.ToBoolean(xPropValue);
                    break;
                case "DisplayName":
                   oBlock.DisplayName = xPropValue;
                    break;
                case "HelpText":
                    oBlock.HelpText = xPropValue;
                    break;
                case "DisplayLevel":
                    oBlock.DisplayLevel = (Variable.Levels)System.Convert.ToInt32(xPropValue);
                    break;
                case "HotKey":
                    oBlock.Hotkey = xPropValue;
                    break;
                //GLOG 2165: Set StartingText property
                case "StartingText":
                    oBlock.StartingText = xPropValue;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// explicitly sets all AnswerFileSegment property values
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        private void SetAnswerFileSegmentPropertyValue(AnswerFileSegment oSeg, string xPropName, string xPropValue)
        {
            switch (xPropName)
            {
                case "InsertionLocation":
                    oSeg.InsertionLocation = (Segment.InsertionLocations)Int32.Parse(xPropValue);
                    break;
                case "InsertionBehavior":
                    oSeg.InsertionBehavior = (Segment.InsertionBehaviors)Int32.Parse(xPropValue);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// sets the property with the specified name
        /// to the specified value
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        private void SetSegmentPropertyValue(Segment oSeg, string xPropName, string xPropValue)
        {
            switch (xPropName)
            {
                case "AuthorControlProperties":
                    oSeg.AuthorControlProperties = xPropValue;
                    break;
                case "MaxAuthors":
                    //GLOG 3412: Handle blanked out value
                    if (!string.IsNullOrEmpty(xPropValue))
                    {
                        oSeg.MaxAuthors = Int32.Parse(xPropValue);
                    }
                    else
                    {
                        oSeg.MaxAuthors = 0;
                    }
                    break;
                case "TypeID":
                    //GLOG 3412: Dont' change if value is blank
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.TypeID = (LMP.Data.mpObjectTypes)((short)Int32.Parse(xPropValue));
                    break;
                case "DisplayName":
                    oSeg.DisplayName = xPropValue;
                    break;
                case "ShowChooser":
                    oSeg.ShowChooser = System.Convert.ToBoolean(xPropValue);
                    break;
                case "LinkAuthorsToParentDef":
                    //GLOG 3412: Handle blanked out value
                    if (!string.IsNullOrEmpty(xPropValue))
                    {
                        oSeg.LinkAuthorsToParentDef = (Segment.LinkAuthorsToParentOptions)Int32.Parse(xPropValue);
                    }
                    else
                        oSeg.LinkAuthorsToParentDef = Segment.LinkAuthorsToParentOptions.Never;
                    break;
                case "MenuInsertionOptions":
                    oSeg.MenuInsertionOptions = (Segment.InsertionLocations)Int32.Parse(xPropValue);
                    break;
                case "DefaultMenuInsertionBehavior":
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.DefaultMenuInsertionBehavior = (Segment.InsertionBehaviors)Int32.Parse(xPropValue);
                    break;
                case "DefaultDragLocation":
                    //GLOG 3412: Dont' change if value is blank
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.DefaultDragLocation = (Segment.InsertionLocations)Int32.Parse(xPropValue);
                    break;
                case "DefaultDragBehavior":
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.DefaultDragBehavior = (Segment.InsertionBehaviors)Int32.Parse(xPropValue);
                    break;
                case "DefaultDoubleClickLocation":
                    //GLOG 3412: Dont' change if value is blank
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.DefaultDoubleClickLocation = (Segment.InsertionLocations)Int32.Parse(xPropValue);
                    break;
                case "DefaultDoubleClickBehavior":
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.DefaultDoubleClickBehavior = (Segment.InsertionBehaviors)Int32.Parse(xPropValue);
                    break;
                case "ProtectedFormPassword":
                    oSeg.ProtectedFormPassword = xPropValue;
                    break;
                case "HelpText":
                    oSeg.HelpText = xPropValue;
                    break;
                case "AuthorNodeUILabel":
                    //GLOG 6653
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.AuthorsNodeUILabel = xPropValue;
                    else
                        oSeg.AuthorsNodeUILabel = "Author";
                    break;
                case "AuthorsHelpText":
                    oSeg.AuthorsHelpText = xPropValue;
                    break;
                case "IsTransparentDef":
                    oSeg.IsTransparentDef = bool.Parse(xPropValue);
                    break;
                case "DefaultWrapperID":
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.DefaultWrapperID = Int32.Parse(xPropValue);
                    else
                        oSeg.DefaultWrapperID = 0;
                    break;
                case "DisplayWizard":
                    oSeg.DisplayWizard = bool.Parse(xPropValue);
                    break;
                case "WordTemplate":
                    oSeg.WordTemplate = xPropValue;
                    break;
                case "RequiredStyles":
                    oSeg.RequiredStyles = xPropValue;
                    break;
                case "IntendedUse":
                    //GLOG 3412: Dont' change if value is blank
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.IntendedUse = (mpSegmentIntendedUses)Int32.Parse(xPropValue);
                    break;
                case "ShowCourtChooser":
                    oSeg.ShowCourtChooser = bool.Parse(xPropValue);
                    break;
                case "ShowSegmentDialog":
                    oSeg.ShowSegmentDialog = bool.Parse(xPropValue);
                    break;
                case "DialogTabCaptions":
                    oSeg.DialogTabCaptions = xPropValue;
                    break;
                case "DialogCaption":
                    oSeg.DialogCaption = xPropValue;
                    break;
                case "CourtChooserUILabel":
                    oSeg.CourtChooserUILabel = xPropValue;
                    break;
                case "CourtChooserHelpText":
                    oSeg.CourtChooserHelpText = xPropValue;
                    break;
                case "CourtChooserControlProperties":
                    oSeg.CourtChooserControlProperties = xPropValue;
                    break;
                case "JurisdictionDefaults":
                    string[] aLevels = xPropValue.Split(StringArray.mpEndOfRecord);
                    oSeg.L0 = Int32.Parse(aLevels[0]);
                    if (aLevels.GetUpperBound(0) > 0)
                        oSeg.L1 = Int32.Parse(aLevels[1]);
                    else
                        oSeg.L1 = 0;
                    if (aLevels.GetUpperBound(0) > 1)
                        oSeg.L2 = Int32.Parse(aLevels[2]);
                    else
                        oSeg.L2 = 0;
                    if (aLevels.GetUpperBound(0) > 2)
                        oSeg.L3 = Int32.Parse(aLevels[3]);
                    else
                        oSeg.L3 = 0;
                    if (aLevels.GetUpperBound(0) > 0)
                        oSeg.L4 = Int32.Parse(aLevels[4]);
                    else
                        oSeg.L4 = 0;
                    break;
                case "DefaultTrailerID":
                    //GLOG 3412: Reset to 0 if control was blanked out
                    if (!string.IsNullOrEmpty(xPropValue))
                    {
                        oSeg.DefaultTrailerID = Int32.Parse(xPropValue);
                    }
                    else
                        oSeg.DefaultTrailerID = 0;
                    break;
                case "SupportedLanguages":
                    oSeg.SupportedLanguages = xPropValue;
                    break;
                case "Culture":
                    //GLOG 3412: Dont' change if value is blank
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.Culture = Int32.Parse(xPropValue);
                    break;
                case "AllowSideBySide":
                    ((CollectionTableItem)oSeg).AllowSideBySide =
                        bool.Parse(xPropValue);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// explitly sets all VariableAction property values
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        private void SetVariableActionPropertyValue(VariableAction oVA, string xPropName, string xPropValue)
        {
            switch (xPropName)
            {
                case "ExecutionCondition":
                    oVA.ExecutionCondition = xPropValue;
                    break;
                case "ValueSetID":
                    if (xPropValue == "")
                        oVA.LookupListID = 0;
                    else
                        oVA.LookupListID = Int32.Parse(xPropValue);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// explitly sets all SegmentEventAction property values
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        private void SetSegmentActionPropertyValue(SegmentAction oSA, string xPropName, string xPropValue)
        {
            switch (xPropName)
            {
                case "ExecutionCondition":
                    oSA.ExecutionCondition = xPropValue;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// explitly sets all ControlAction property values
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        private void SetControlActionPropertyValue(ControlAction oCA, string xPropName, string xPropValue)
        {
            switch (xPropName)
            {
                case "ExecutionCondition":
                    oCA.ExecutionCondition = xPropValue;
                    break;
                case "Event":
                    oCA.Event = (ControlActions.Events)Enum.Parse(typeof(ControlActions.Events), xPropValue);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// explitly sets all Variable property values
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        private void SetVariablePropertyValue(Variable oVar, string xPropName, string xPropValue)
        {
            switch (xPropName)
            {
                case "CIAlerts":
                    oVar.CIAlerts = (ciAlerts)Int32.Parse(xPropValue);
                    break;
                case "CIContactType":
                    oVar.CIContactType = (ciContactTypes)Int32.Parse(xPropValue);
                    break;
                case "CIDetailTokenString":
                    oVar.CIDetailTokenString = xPropValue;
                    break;
                case "RequestCIForEntireSegment":
                    oVar.RequestCIForEntireSegment = System.Convert.ToBoolean(xPropValue);
                    break;
                case "DisplayName":
                    oVar.DisplayName = xPropValue;
                    break;
                case "DisplayValue":
                    oVar.DisplayValue = xPropValue;
                    break;
                case "HotKey":
                    oVar.Hotkey = xPropValue;
                    break;
                case "DisplayLevel":
                    oVar.DisplayLevel = (Variable.Levels)Int32.Parse(xPropValue);
                    break;
                case "DisplayIn":
                    Variable.ControlHosts iOldDisplay = oVar.DisplayIn;
                    oVar.DisplayIn = (Variable.ControlHosts)Int32.Parse(xPropValue);
                    bool bDefaultChanged = false;
                    if ((oVar.DisplayIn & Variable.ControlHosts.AuthorPreferenceManager) !=
                        (iOldDisplay & Variable.ControlHosts.AuthorPreferenceManager))
                    {
                        //Bit for Display in Author Preferences has changed
                        if ((oVar.DisplayIn & Variable.ControlHosts.AuthorPreferenceManager) > 0)
                        {
                            //Also set DefaultValue to reference the Author Preference setting
                            if (oVar.DefaultValue == "")
                            {
                                //GLOG 2945
                                oVar.DefaultValue = "[LeadAuthorPreference__" + oVar.Name + "]";
                                bDefaultChanged = true;
                            }
                        }
                        else if (oVar.DefaultValue.ToUpper() == "[LEADAUTHORPREFERENCE__" + oVar.Name.ToUpper() + "]" || 
                            (oVar.DefaultValue.ToUpper() == "[LEADAUTHORPREFERENCE_" + oVar.Name.ToUpper() + "]"))
                        {
                            //Clear default value if it's currently the Author Preference
                            oVar.DefaultValue = "";
                            bDefaultChanged = true;
                        }
                        if (bDefaultChanged)
                        {
                            //Update display for DefaultValue node
                            UltraTreeNode oVarNode = GetTreeNodeFromVariable(oVar);
                            UltraTreeNode oValueNode = this.treeDocContents.GetNodeByKey(oVarNode.Key
                                + ".DefaultValue_Value");
                            oValueNode.Text = (oVar.DefaultValue == null) || (oVar.DefaultValue == "") ? EMPTY_VALUE : oVar.DefaultValue;
                        }
                    }
                    break;
                case "DefaultValue":
                    oVar.DefaultValue  = xPropValue;
                    break;
                case "SecondaryDefaultValue":
                    oVar.SecondaryDefaultValue = xPropValue;
                    break;
                case "ValueSourceExpression":
                    oVar.ValueSourceExpression = xPropValue;
                    break;
                case "ValidationCondition":
                    oVar.ValidationCondition = xPropValue;
                    break;
                case "ValidationResponse":
                    oVar.ValidationResponse = xPropValue;
                    break;
                case "HelpText":
                    oVar.HelpText = xPropValue;
                    break;
                case "ControlType":
                    oVar.ControlType = (LMP.Data.mpControlTypes)Int32.Parse(xPropValue);
                    break;
                case "ControlProperties":
                    oVar.ControlProperties  = xPropValue;
                    break;
                case "MustVisit":
                    oVar.MustVisit = Convert.ToBoolean(xPropValue);
                    break;
                case "AllowPrefillOverride":
                    oVar.AllowPrefillOverride =  (Variable.PrefillOverrideTypes)Int32.Parse(xPropValue);
                    break;
                case "IsMultiValue":
                    oVar.IsMultiValue = Convert.ToBoolean(xPropValue);
                    break;
                case "AssociatedPrefillNames":
                    oVar.AssociatedPrefillNames = xPropValue;
                    break;
                case "TabNumber":
                    oVar.TabNumber = Int32.Parse(xPropValue);
                    break;
                //GLOG 1408
                case "NavigationTag":
                    oVar.NavigationTag = xPropValue;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// saves design property value of designated segment to XML
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        private void SaveSegmentPropertyXML(Segment oSeg, string xPropName, string xPropValue)
        {
            switch (xPropName)
            {
                //Property name identifiers for these props may differ than the actual property names.
                case "MaxAuthors":
                    if (xPropValue != "")
                        oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "MaxAuthors", xPropValue);
                    else
                        oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "MaxAuthors", "1");
                    break;
                case "TypeID":
                    //GLOG 3412: Dont' change if value is blank
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "ObjectTypeID", xPropValue);
                    break;
                case "JurisdictionDefaults":
                    //split control value, assign segment objectdata properties
                    int[] iL = Segment.GetJurisdictionArray(xPropValue);
                    oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "L0", iL[0].ToString());
                    oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "L1", iL[1].ToString());
                    oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "L2", iL[2].ToString());
                    oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "L3", iL[3].ToString());
                    oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "L4", iL[4].ToString());
                    break;
                case "IntendedUse":
                case "DefaultDoubleClickLocation":
                case "DefaultDragLocation":
                case "Culture":
                    //GLOG 3412: Don't try to set unless a valid choice has been selected
                    if (!string.IsNullOrEmpty(xPropValue))
                        oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, xPropName, xPropValue);
                    break;
                default:
                    oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, xPropName, xPropValue);
                    break;
            }
        }
        /// <summary>
        /// saves Authors attribute control actions to document XML
        /// </summary>
        /// <param name="oSeg"></param>
        private void SaveSegmentAuthorsControlActions(Segment oSeg)
        {
            oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "AuthorControlActions", 
                oSeg.Authors.ControlActions.ToString());
        }
        /// <summary>
        /// saves Jurisdiction control actions to document XML
        /// </summary>
        /// <param name="oSeg"></param>
        private void SaveSegmentJurisdictionControlActions(Segment oSeg)
        {
            oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "CourtChooserControlActions",
                oSeg.CourtChooserControlActions.ToString());
        }
        /// <summary>
        /// saves language control actions to document XML
        /// </summary>
        /// <param name="oSeg"></param>
        private void SaveSegmentLanguageControlActions(Segment oSeg)
        {
            oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "LanguageControlActions",
                oSeg.LanguageControlActions.ToString());
        }

        ///// <summary>
        ///// saves Authors attribute control actions XML data
        ///// </summary>
        ///// <param name="oSeg"></param>
        //private void SaveSegmentAuthorsControlActions(Segment oSeg)
        //{
        //    //build new attribute data string with non persistant values from 
        //    //control actions collection
        //    StringBuilder oSB = new StringBuilder();
        //    ControlAction oCA = null;

        //    for (int i = 0; i < oSeg.Authors.ControlActions.Count; i++)
        //    {
        //        oCA = oSeg.Authors.ControlActions[i];
        //        oSB.AppendFormat("{0}{1}", oCA.ToString(), LMP.StringArray.mpEndOfRecord.ToString());
        //    }

        //    //add author data XML delimiter and trim trailing record separator
        //    string xAuthorData = "|" + oSB.ToString(0, oSB.Length - 1);

        //    //save XML to doc
        //    oSeg.Nodes.SetItemAuthorsData(oSeg.TagID, xAuthorData);
        //}
        /// <summary>
        /// overloaded method defaults to removing control after edit
        /// </summary>
        private void ExitEditMode()
        {
           ExitEditMode(true);
        }
        /// <summary>
        /// exits edit mode -
        /// resets the current node in the tree -
        /// i.e. if the current node is a variable,
        /// sets variable value if necessary,
        /// removes associated control, and respaces node
        /// </summary>
        private void ExitEditMode(bool bRemoveControl)
        {
            //exit if there is no current tree node
            if (this.m_oCurNode == null)
                return;

            UltraTreeNode oCurValueNode = null;
            Segment oCurSeg = null;

            try
            {
                //get current value node
                oCurValueNode = this.m_oCurNode.Nodes[0];
                if (!oCurValueNode.Key.EndsWith("_Value"))
                    //this is not a value node - there
                    //is no value node - current node
                    //isn't a property
                    return;
            }
            catch
            {
                if (oCurValueNode == null)
                    if (m_oCurCtl == null)
                        //we're leaving lowest level child node - do nothing
                        return;
                    else
                        //oCurValueNode is m_oCurNode
                        oCurValueNode = m_oCurNode;
            }

            IControl oICtl = (IControl)this.m_oCurCtl;
            NodeTypes oNT = NodeType(oCurValueNode);

            if (bRemoveControl)
            {
                //restore to original space after value
                oCurValueNode.Override.NodeSpacingAfter = 10;
                oCurValueNode.Override.NodeSpacingBefore = -1;
                oCurValueNode.Override.NodeAppearance.ForeColor = Color.Gray;

                //clear out help text
                //GLOG 7214
                TaskPane.SetHelpText(wbHelpText, "");

                ////reset transparency - set transparent in EnterPropertyEditMode
                //oCurValueNode.Override.NodeAppearance.ForegroundAlpha =
                //    Infragistics.Win.Alpha.Default;

                ////reset double click behavior
                oCurValueNode.Parent.Override.NodeDoubleClickAction =
                    NodeDoubleClickAction.ToggleExpansion;

                //reset forecolor - this will have been set to blue if user clicked directly on
                //value node rather than property node
                oCurValueNode.Parent.Override.NodeAppearance.ForeColor = Color.Black;
            }

            //deal with current control - set property value and 
            //refresh value node text with new property value if control has been edited
            if (this.m_oCurCtl != null)
            {
                if (oICtl.IsDirty)
                {
                    try
                    {
                        //get designated Segment, Variable or Block from designated node
                        //and attempt to save changes
                        oCurSeg = GetSegmentFromNode(oCurValueNode);
                        Variable oVar = GetVariableFromNode(oCurValueNode);
                        Block oBlock = GetBlockFromNode(oCurValueNode);
                        mpSegmentIntendedUses iOriginalUse = oCurSeg.IntendedUse;
                        mpObjectTypes iOriginalType = oCurSeg.TypeID;

                        //set the designated property from updated tree node control
                        SetPropertyValueFromNode(oCurValueNode, oICtl,
                            oNT == NodeTypes.SegmentPropertyValue);

                        //reset value node text to updated prop value
                        SetValueNodeText(oCurValueNode, oICtl);

                        if (oCurSeg != null)
                        {
                            //save segment actions if current value node = one of these types
                            if (oNT == NodeTypes.SegmentActionProperty ||
                                oNT == NodeTypes.SegmentActionParametersCategory ||
                                oNT == NodeTypes.SegmentAction ||
                                oNT == NodeTypes.SegmentActionParameter ||
                                oNT == NodeTypes.LanguageAction ||
                                oNT == NodeTypes.AuthorsAction)
                            {
                                oCurSeg.Actions.Save();
                            }

                            //save segment authors control actions if necessary
                            if (oNT == NodeTypes.SegmentAuthorsControlActionProperty ||
                                oNT == NodeTypes.SegmentAuthorsControlActionParameter)
                                SaveSegmentAuthorsControlActions(oCurSeg);
                            else if (oNT == NodeTypes.SegmentJurisdictionControlActionProperty ||
                                oNT == NodeTypes.SegmentJurisdictionControlActionParameter)
                                SaveSegmentJurisdictionControlActions(oCurSeg);
                            else if (oNT == NodeTypes.LanguageControlActionProperty ||
                                oNT == NodeTypes.LanguageControlActionParameter)
                                SaveSegmentLanguageControlActions(oCurSeg);

                            if (oVar != null)
                                oCurSeg.Variables.Save(oVar);
                            else if (oBlock != null)
                                oCurSeg.Blocks.Save(oBlock);

                            //GLOG 3640 (dm) - Allow Side-By-Side property should only
                            //display for collection table items
                            bool bOldInclude = (LMP.Data.Application.IsCollectionTableType(iOriginalType) &&
                                (iOriginalType != mpObjectTypes.PleadingCaption));
                            bool bNewInclude = (LMP.Data.Application.IsCollectionTableType(oCurSeg.TypeID) &&
                                (oCurSeg.TypeID != mpObjectTypes.PleadingCaption));

                            if (oNT == NodeTypes.SegmentPropertyValue && (
                                iOriginalUse != oCurSeg.IntendedUse) &&
                                (iOriginalUse == mpSegmentIntendedUses.AsStyleSheet ||
                                oCurSeg.IntendedUse == mpSegmentIntendedUses.AsStyleSheet) ||
                                (bOldInclude != bNewInclude))
                            {
                                //Refresh tree if Intended Use was changed to or from Style Sheet
                                this.m_oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes);
                            }
                            //flag current document as as dirty - XML has been changed by save
                            m_bDocIsDirty = true;
                        }
                    }
                    catch (System.Exception oE)
                    {
                        //clear out current control if exception has been handled
                        this.m_oCurCtl = null;

                        throw new LMP.Exceptions.SegmentException(
                            LMP.Resources.GetLangString("Error_CouldNotSaveDesignProperties") +
                            oCurSeg.DisplayName, oE);
                    }
                }

                //clear out current control
                if (bRemoveControl == true)
                {
                    //there is a current control - remove from form
                    this.treeDocContents.Controls.Remove(this.m_oCurCtl);

                    //force a redraw
                    this.treeDocContents.Refresh();

                    this.m_oCurCtl = null;
                }
            }
            else
                //just force a redraw - m_oCurCtl variable == null
                this.treeDocContents.Refresh();
        }
        /// <summary>
        /// sets tree node text for designated value node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oICtl"></param>
        private void SetValueNodeText(UltraTreeNode oNode, IControl oICtl)
        {
            string xValue = null;
            NodeTypes oNT = NodeType(oNode);
            string xRep = "\r\n";
            string xSep = null;

            //handle node types with various special return value formats
            switch (oNT)
            {
                case NodeTypes.SegmentPropertyValue:
                    //manipulate value strings - replace delimiters with line feeds
                    //or convert bitwise values to strings suitable for display
                    switch (m_oCurNode.Text)
                    {
                        case "Author Control Properties":
                            xSep = StringArray.mpEndOfSubValue.ToString();
                            xValue = ((LMP.Controls.ControlPropertiesGrid)oICtl).Value.Replace(xSep, xRep);
                            //GLOG 3277: Display single line only
                            if (xValue.Contains("\r\n"))
                                xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                            break;
                        case "Insertion Locations":
                            xValue = ConvertBitwiseValueToString
                                (LMP.Controls.InsertionLocationsList.List, Int32.Parse(oICtl.Value)).TrimEnd("\r\n".ToCharArray());
                            //GLOG 3277: Display single line only
                            if (xValue.Contains("\r\n"))
                                xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                            break;
                        case "Insertion Behavior":
                            xValue = ConvertBitwiseValueToString
                                (LMP.Controls.InsertionBehaviorsList.List, Int32.Parse(oICtl.Value)).TrimEnd("\r\n".ToCharArray());
                            //GLOG 3277: Display single line only
                            if (xValue.Contains("\r\n"))
                                xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                            break;
                        case "Default Drag Behavior":
                        case "Default Click Behavior":
                            xValue = ConvertBitwiseValueToString
                                (LMP.Controls.InsertionBehaviorsList.List, Int32.Parse(oICtl.Value)).TrimEnd("\r\n".ToCharArray());
                            //GLOG 3277: Display single line only
                            if (xValue.Contains("\r\n"))
                                xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                            break;
                        case "Default Click Location":
                        case "Default Drag Location":
                            if (oICtl.Value != "")
                                xValue = ConvertValueToString
                                    (LMP.Controls.DefaultLocationComboBox.List, Int32.Parse(oICtl.Value));
                            break;
                        case "Intended Use":
                            if (oICtl.Value != "")
                                xValue = ConvertValueToString
                                (LMP.Controls.IntendedUsesComboBox.List, Int32.Parse(oICtl.Value));
                            break;
                        case "Help Text":
                        default:
                            xValue = this.GetPropertyValueFromNode(oNode, false);
                            break;
                    }
                    break;
                case NodeTypes.SegmentActionParameter:
                    xValue = GetSegmentActionParameterTextFromNode(oNode);
                    break;
                case NodeTypes.VariableActionParameter:
                    xValue = GetVariableActionParameterTextFromNode(oNode);
                    break;
                case NodeTypes.ControlActionProperty:

                    xValue = this.GetPropertyValueFromNode(oNode, false);
                    break;

                case NodeTypes.ControlActionParameter:
                    xValue = this.GetControlActionParameterTextFromNode(oNode);
                    break;
                case NodeTypes.VariableProperty:
                    if (m_oCurNode.Text == "Control Properties")
                    {
                        //GLOG 3277: Don't display multiple lines
                        xValue = ((LMP.Controls.ControlPropertiesGrid)oICtl).Value;
                        if (xValue.Contains(StringArray.mpEndOfSubValue.ToString()))
                            xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");

                    }
                    else
                    {
                        xValue = this.GetPropertyValueFromNode(oNode, false);
                    }
                    break;
                default:
                    //no special manipulation required, just retrieve property value
                    xValue = this.GetPropertyValueFromNode(oNode, false);
                    break;
            }

            //reset display name node text if applicable - segments, blocks, and variables   
            if (m_oCurNode.Text == "Display Name")
            {
                switch (oNT)
                {
                    case NodeTypes.VariablePropertyValue:
                        m_oCurNode.Parent.Text = xValue;
                        break;
                    case NodeTypes.BlockProperty:
                        m_oCurNode.Parent.Text = xValue;
                        break;
                    default:
                        break;
                }
            }

            //handle list combo, which contains self generated array list of list names, IDs
            //and not enumeration
            if (oICtl.GetType().Name == "ListCombo")
                xValue = ((LMP.Controls.ListCombo)oICtl).Text;

            //force "None" for CIAlerts List w/ no checkboxes checked
            if (oICtl.GetType().Name == "CIAlertsList")
                if (xValue == "")
                    xValue = "None";

            //replace empty value with standard designator
            xValue = (xValue == null) ||
                (xValue == "") ? EMPTY_VALUE : xValue;

            //set the node text
            oNode.Text = xValue;
        }
        /// <summary> 
        /// displays the form to manage assignments of the 
        /// specified type for the specified target 
        /// </summary> 
        /// <param name="AssignedObjectType"></param> 
        /// <param name="oTargetSegment"></param> 
        private void ManageAssignments(mpObjectTypes AssignedObjectType, Segment oTargetSegment)
        {
            //pleading table items are assigned directly to pleadings
            if ((oTargetSegment is CollectionTable) && (oTargetSegment.Parent != null))
                oTargetSegment = oTargetSegment.Parent;

            AssignmentsForm oForm = new AssignmentsForm(oTargetSegment.TypeID,
                oTargetSegment.ID1, AssignedObjectType);

            oForm.ShowDialog();
            oForm.Close();
        }
        /// <summary>
        /// build XML string to load levels chooser
        /// L0-L4 read from admin seg instance - not def
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="oCtl"></param>
        private void LoadJurisdictionChooser(Segment oSeg, LMP.Controls.JurisdictionChooser oCtl)
        {

            oCtl.Value = oSeg.GetJurisdictionLevels();
        }
        /// <summary>
        ///loads mpControls combo with
        ///string list of specified name
        /// </summary>
        /// <param name="oCtl">mpControls combo</param>
        /// <param name="xListName">list name from tblLists</param>
        /// <param name="bLimitToList">must match entry in list</param>
        private void LoadComboList(LMP.Controls.ComboBox oCtl, string xListName, bool bLimitToList)
        {
            LoadComboList(oCtl, xListName, bLimitToList, false, false);
        }
        /// <summary>
        ///loads mpControls combo with
        ///MacPac list of specified name if bUseMacPacList == true
        /// </summary>
        /// <param name="oCtl">mpControls combo</param>
        /// <param name="xListName">list name from tblLists</param>
        /// <param name="bLimitToList">must match entry in list</param>
        /// <param name="bUseMacPacList">list is MacPac list</param>
        private void LoadComboList(LMP.Controls.ComboBox oCtl,
            string xListName, bool bLimitToList, bool bUseMacPacList)
        {
            LoadComboList(oCtl, xListName, bLimitToList, bUseMacPacList, false);
        }
        /// <summary>
        ///loads mpControls combo with
        ///string list of specified name
        /// </summary>
        /// <param name="oCtl"></param>
        /// <param name="xListName"></param>
        /// <param name="bLimitToList"></param>
        /// <param name="bUseMacPacList"></param>
        /// <param name="bAllowEmptyValue"></param>
        private void LoadComboList(LMP.Controls.ComboBox oCtl,
            string xListName, bool bLimitToList, bool bUseMacPacList, bool bAllowEmptyValue)
        {
            oCtl.LimitToList = bLimitToList;
            oCtl.AllowEmptyValue = bAllowEmptyValue;

            try
            {
                if (!bUseMacPacList)
                    //use Designer's hard-coded arrays
                    oCtl.SetList(GetPropertySettingsList(xListName));
                else
                {
                    //load designated MacPac list
                    Lists oLists = new Lists();
                    oCtl.SetList((List)oLists.ItemFromName(xListName));
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_ListNotFound") + " " + xListName, oE);
            }
        }
        /// <summary>
        /// sets up editing controls for control action property
        /// value nodes
        /// </summary>
        /// <param name="xPropName">variable action property name</param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private Control SetupControlActionPropertyControls(string xPropName, string xPropValue)
        {
            try
            {
                string xHelp = null;

                //create resource identifier for help text
                xHelp = DESIGN_HELP_PREF_CTLACTPROP + xPropName.Replace(" ", null);

                Control oCtl = null;

                //create new control
                switch (xPropName)
                {
                    case "ExecutionCondition":
                        oCtl = CreateExpressionTextBox();
                        break;
                    case "Event":
                        LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                        oCtl = oCombo;
                        LoadComboList(oCombo, "Control Events", true);
                        break;
                    default:
                        break;
                }
                if (oCtl != null)
                {
                    //subscribe to tab pressed event
                    IControl oICtl = (IControl)oCtl;

                    oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                    oICtl.ExecuteFinalSetup();

                    oCtl.Tag = xHelp;

                    //TODO: remove this if possible
                    oCtl.SuspendLayout();

                    try
                    {
                        return oCtl;
                    }
                    finally
                    {
                        oCtl.ResumeLayout();
                    }
                }
                else
                    return null;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// sizes and positions control at the specified node
        /// </summary>
        /// <param name="oUnderlyingNode"></param>
        private void SizeAndPositionControl()
        {
            if (this.m_oCurCtl == null || this.m_oCurNode == null)
                return;

            try
            {
                this.treeDocContents.SuspendLayout();
                this.m_oCurCtl.SuspendLayout();

                int iControlWidth = 0;

                //set control width based on scroll bar visibility
                if (this.IsScrollbarVisible(mpScrollbarType.Vertical))
                    iControlWidth = this.treeDocContents.Width -
                        this.m_oCurNode.Bounds.Left - this.treeDocContents.Indent - 20;
                else
                    iControlWidth = this.treeDocContents.Width -
                        this.m_oCurNode.Bounds.Left - this.treeDocContents.Indent - 5;

                int iTop;
                int iLeft = 0;

                //account for nodes selected by clicking directly on value nodes
                if (m_oCurNode.HasNodes)
                {
                    iTop = this.m_oCurNode.Bounds.Top + this.m_oCurNode.Bounds.Height + 3;
                    iLeft = m_oCurNode.Bounds.Left;
                }
                else
                {
                    iTop = m_oCurNode.Bounds.Top - 10;
                    iLeft = m_oCurNode.Parent.Bounds.Left;
                }

                //position control to display over variable value

                this.m_oCurCtl.SetBounds(iLeft + this.treeDocContents.Indent,
                    iTop, iControlWidth, this.m_oCurCtl.Height);
            }
            finally
            {
                this.treeDocContents.ResumeLayout();
                this.m_oCurCtl.ResumeLayout();
            }
        }
        /// <summary>
        /// overloaded method sets appearance properties of a node
        /// no image or spacing set
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oIndicator"></param>
        /// <param name="bExpanded"></param>
        /// <param name="bEnabled"></param>
        /// <param name="oForeColor"></param>
        /// <param name="oBackColor"></param>
        private void SetNodeAppearance(UltraTreeNode oNode, bool bShowIndicator,
             bool bExpanded, bool bEnabled, Color oForeColor)
        {
            this.SetNodeAppearance(oNode, bShowIndicator, bExpanded,
                bEnabled, oForeColor, Images.NoImage);
        }
        /// <summary>
        /// returns the image that should be
        /// displayed for the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private Images GetSegmentImage(Segment oSegment)
        {
            switch (oSegment.IntendedUse)
            {
                case mpSegmentIntendedUses.AsDocument:
                    return Images.DocumentSegment;
                case mpSegmentIntendedUses.AsDocumentComponent:
                    return Images.ComponentSegment;
                case mpSegmentIntendedUses.AsParagraphText:
                    return Images.ParagraphTextSegment;
                case mpSegmentIntendedUses.AsSentenceText:
                    return Images.SentenceTextSegment;
                case mpSegmentIntendedUses.AsAnswerFile:
                    //TODO: Customize image for Answer File node
                    return Images.DocumentSegment;
                case mpSegmentIntendedUses.AsStyleSheet:
                case mpSegmentIntendedUses.AsMasterDataForm:
                    return Images.NoImage;
                default:
                    return Images.StyleSheetSegment;
            }
        }
        /// <summary>
        /// sets appearance properties of a tree node - uses designated image
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bShowIndicator"></param>
        /// <param name="bExpanded"></param>
        /// <param name="bEnabled"></param>
        /// <param name="oForeColor"></param>
        /// <param name="iImage"></param>
        private void SetNodeAppearance(UltraTreeNode oNode, bool bShowIndicator,
             bool bExpanded, bool bEnabled, Color oForeColor, Images iImage)
        {
            this.SetNodeAppearance(oNode, bShowIndicator, bExpanded,
                bEnabled, oForeColor, iImage, -1, 10);
        }
        /// <summary>
        /// sets appearance properties of a tree node - uses designated image, designated
        /// before and after node spacing override
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="oIndicator">Expansion Indicator</param>
        /// <param name="bExpanded"></param>
        /// <param name="bEnabled"></param>
        /// <param name="oForeColor">selected ForeColor</param>
        /// <param name="oBackColor"></param>
        /// <param name="iImage"></param>
        private void SetNodeAppearance(UltraTreeNode oNode,
            bool bShowIndicator, bool bExpanded, bool bEnabled, Color oForeColor,
            Images iImage, int iSpaceBefore, int iSpaceAfter)
        {
            oNode.Override.ShowExpansionIndicator =
                (bShowIndicator ? ShowExpansionIndicator.Always :
                ShowExpansionIndicator.Never);

            //don't execute expand/collapse handlers
            bool bIgnore = m_IgnoreTreeViewEvents;
            m_IgnoreTreeViewEvents = true;
            oNode.Expanded = bExpanded;
            m_IgnoreTreeViewEvents = bIgnore;

            oNode.Enabled = bEnabled;
            if (iImage != Images.NoImage)
                oNode.LeftImages.Add(iImage);

            //default node spacing
            oNode.Override.NodeSpacingBefore = iSpaceBefore;
            oNode.Override.NodeSpacingAfter = iSpaceAfter;
            //set designated forecolor
            oNode.Override.NodeAppearance.ForeColor = oForeColor;
        }
        internal bool SaveSegmentDesign()
        {
            return SaveSegmentDesign(true,false);
        }
        /// <summary>
        /// saves the top level segment
        /// </summary>
        internal bool SaveSegmentDesign(bool bUpdateRelated, bool bNoPrompt)
        {
            Word.Document oWordDoc = m_oMPDocument.WordDocument;

            LMP.Forte.MSWord.Application oCOM = new LMP.Forte.MSWord.Application();

            //GLOG : 7873 : CEH
            FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
            if (oSettings.PromptAboutUnconvertedTablesInDesign)
                LMP.Forte.MSWord.WordDoc.AnalyseDocumentTables();
            
            LMP.Forte.MSWord.Clipboard.SaveClipboard();
            LMP.Forte.MSWord.Clipboard.DisableClipboardFunctions(true);
            //GLOG 7623
            WaitCursorForm oWaitCursor = new WaitCursorForm();
            if (!bNoPrompt)
            {
                string xDocXml = oWordDoc.WordOpenXML;
                //GLOG 8562: Only show insertion message if 
                //threshold of docvars and content controls is surpassed
                if (String.XMLComplexity(xDocXml) > 100)
                {
                    oWaitCursor.PromptText = "Saving segment design.";
                    oWaitCursor.Show();
                    System.Windows.Forms.Application.DoEvents();
                }
            }
            //get rsid state for later use
            Word.Options oOptions = oWordDoc.Application.Options;
            bool bSaveRSID = oOptions.StoreRSIDOnSave;

            try
            {
                //Switch back to Designer tab in case Variable Import tab is active
                this.TaskPane.SelectTab(TaskPane.Tabs.Edit);
                //get top level segment
                Segment oSegment = m_oMPDocument.Segments[0];
                //Initialize update list
                m_aUpdateList = new List<string>();
                m_aChangedSegments = new List<string>();

                //check for valid control value for current control - exit if invalid
                //otherwise run ExitEditMode to ensure update of segment properties
                try
                {
                    if (!this.IsValidControlValue((IControl)m_oCurCtl, m_oCurNode))
                    {
                        this.TaskPane.ScreenUpdating = true;
                        this.InvalidControlValue = true;
                        return false;
                    }
                    else
                    {
                        //lock screen updating
                        this.TaskPane.ScreenUpdating = false;

                        ExitEditMode(false);
                        this.InvalidControlValue = false;
                    }
                }
                catch { }

                //exit if design is not valid -
                //the method below will alert accordingly
                //2/19/09 (dm) - moved this block below IsValidControlValue block
                if (!DesignIsValid())
                    return false;

                if (!(oSegment is CollectionTable))
                {
                    AppendToUpdateList(oSegment.ID);
                }

                //save top-level segment -

                //temporarily turn off rsid saving -
                //we don't want to save rsid (revision ID) tags
                oOptions.StoreRSIDOnSave = false;

                //bookmark current selection - collapse selection
                Word.Range oRange = oWordDoc.Application.Selection.Range;
                string xName = "zzmpTemp";
                LMP.Forte.MSWord.WordDoc.AddBookMarkToSelection(xName, true);

                //prevent compressed attributes from getting into database -
                //check first if attributes are currently compressed
                bool bDisableEncryption = (LMP.Registry.GetMacPac10Value("DisableEncryption").ToLower() == "miller" &&
                    LMP.Data.Application.DisableEncryptionPassword.ToLower() == "fisherman") ||
                    LMP.Data.Application.EncryptionPassword == "" ;

                if (!bDisableEncryption)
                {
                    //document contains compressed object data -
                    //decompress Word tag attributes
                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression(m_oMPDocument.Tags, false, "");
                    else
                        //10.2 (dm) - use new data structure
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression_CC(m_oMPDocument.Tags, false, "");

                    //prevent further compression during save
                    LMP.Forte.MSWord.Application.SuspendCompression(true);                    
                }

                //GLOG 7479 (dm) - delete any remaining encrypted doc vars - these doc vars
                //would not be associated with anything still in the tags collection
                //11-4-15 (dm) - I determined that this was intentionally remmed out
                //when GLOG 7555 was added
                //oDoc.DeleteEncryptedDocVars(m_oMPDocument.WordDocument, true);

                //GLOG 7555: Changed to delete any mpo doc vars that don't have corresponding
                //Content Control or XML Tag
                //GLOG 8206 (dm) - restored 7555, which was inexplicably remmed out in 11.0
                LMP.Forte.MSWord.WordDoc.DeleteOrphanedDocVars(m_oMPDocument.WordDocument);

                //GLOG 3663: Refresh to ensure Nodestore reflects current ObjectData
                oSegment.Refresh();

                //add containing segments to all story ranges that contain text
                InsertmSEGInAllStoryRanges(false, bNoPrompt);

                //return to original selection, delete temp bookmark
                bool bDelete = true;
                LMP.Forte.MSWord.WordDoc.SelectBookmarkRange(xName, oRange, bDelete);

                //convert inline tags where possible
                LMP.Forte.MSWord.WordDoc.ConvertBlockableInlineTags(oWordDoc);

                //add TagExpandedValue field codes -
                //10.2 (dm) - skip this for now when using content controls -
                //it's not yet clear whether this will ultimately be necessary
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    oSegment.RefreshExpandedTagValueCodes(true);

                //execute actions of variables with static default values
                this.RefreshStaticDefaults(oSegment);

                //10.2 (dm) - bookmark all content controls
                //JTS 10/6/26: Bookmark XML Tags also
                m_oMPDocument.Tags.AddBookmarks();

                //GLOG 5295: Remove bookmark automatically added by Word to avoid extra tags in body XML
                try
                {
                    object oBmk = "_GoBack";
                    oWordDoc.Bookmarks.get_Item(ref oBmk).Delete();
                }
                catch { }
                System.Windows.Forms.Application.DoEvents();

                //GLOG : 7970 : ceh - "Save Child Segments" dialog should no longer be displayed on design save
                //if (bUpdateRelated)
                //{
                //    //save edits to child segments
                //    SaveChildSegments();
                //}

                //reset global flag
                m_bDocIsDirty = false;

                //reset control flag - this will prevent prompt if doc closed after a 
                //separate save
                if (m_oCurCtl != null && ((IControl)m_oCurCtl).IsDirty)
                    ((IControl)m_oCurCtl).IsDirty = false;

                //ensure that new segment bookmark is present
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    //xml node
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmarkIfNecessary(oWordDoc,
                        oSegment.PrimaryWordTag);
                else
                    //contentcontrol
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmarkIfNecessary_CC(oWordDoc,
                        oSegment.PrimaryContentControl);

                //save segment definition
                bool bSaved = SaveSegmentDefinition(oSegment, false, m_oMPDocument);

                //delete new segment bookmark - it's been saved with the segment def
                //and shouldn't be visible to the designer
                object oNewBookmark = "mpNewSegment";
                oSegment.ForteDocument.WordDocument.Bookmarks.get_Item(ref oNewBookmark).Delete();

                //restore deleted scopes for editing
                oSegment.RestoreDeletedScopesForDesign();

                //reinsert placeholder text
                LMP.Forte.MSWord.WordDoc.InsertPlaceholderText(m_oMPDocument.Tags);

                //hide TagExpandedValue field codes in designer -
                //10.2 (dm) - skip this for now when using content controls -
                //it's not yet clear whether this will ultimately be necessary
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    oSegment.RefreshExpandedTagValueCodes(false);

                //GLOG 6148: Make sure Author Preferences exist for all assigned languages
                bool bMissingPrefs = false;
                if (Segment.DefinitionContainsAuthorPreferences(oSegment.Definition.XML))
                {
                    string xLanguages = oSegment.SupportedLanguages;
                    string[] aLanguages = xLanguages.Split(StringArray.mpEndOfValue);

                    List<int> aCultures = new List<int>();
                    for (int i = 0; i < aLanguages.GetLength(0); i++)
                    {
                        aCultures.Add(Int32.Parse(aLanguages[i]));
                    }
                    if (aCultures.Count == 0)
                    {
                        aCultures.Add(LMP.Culture.USEnglishCulture.LCID);
                    }
                    foreach (int iCulture in aCultures)
                    {
                        //Skip any languages that have been removed in Administrator
                        if (LMP.Data.Application.SupportedLanguagesString.Contains(iCulture.ToString()))
                        {
                            LMP.Data.KeySet oPrefs = null;
                            try
                            {
                                oPrefs = new KeySet(mpKeySetTypes.AuthorPref, oSegment.ID, LMP.Data.ForteConstants.mpFirmRecordID.ToString(), 0, iCulture);
                            }
                            catch { }
                            //GLOG 8735: Always check variables, since preferences in XML may belong to child segments only
                            for (int i = 0; i < oSegment.Variables.Count; i++)
                            {
                                Variable oVar = oSegment.Variables[i];
                                string xPref = "";
                                if (((oVar.DisplayIn & Variable.ControlHosts.AuthorPreferenceManager) == Variable.ControlHosts.AuthorPreferenceManager) &&
                                    ((oVar.DefaultValue.ToUpper().Contains("[LEADAUTHORPREFERENCE_")) || (oVar.DefaultValue.ToUpper().Contains("[LEADAUTHORPARENTPREFERENCE_"))))
                                {
                                    int iPrefStart = -1;
                                    iPrefStart = oVar.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPREFERENCE_");
                                    if (iPrefStart == -1)
                                        iPrefStart = oVar.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPARENTPREFERENCE_");
                                    if (iPrefStart == -1)
                                        iPrefStart = oVar.DefaultValue.ToUpper().IndexOf("[LEADAUTHORPARENTPREFERENCE_");
                                    if (iPrefStart == -1)
                                        iPrefStart = oVar.DefaultValue.ToUpper().IndexOf("[AUTHORPARENTPREFERENCE_");
                                    if (iPrefStart > -1)
                                    {
                                        int iPrefEnd = -1;
                                        iPrefEnd = oVar.DefaultValue.IndexOf("]", iPrefStart);
                                        if (iPrefEnd > -1)
                                        {
                                            xPref = oVar.DefaultValue.Substring(oVar.DefaultValue.LastIndexOf('_', iPrefEnd) + 1);
                                            xPref = xPref.Substring(0, xPref.IndexOf(']'));
                                        }
                                    }
                                    string xValue = null;
                                    try
                                    {
                                        //GLOG 8735: this will cause error if oPrefs is null
                                        xValue = oPrefs.GetValue(xPref);
                                    }
                                    catch { }
                                    if (xValue == null)
                                    {
                                        bMissingPrefs = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (bMissingPrefs)
                    {
                        MessageBox.Show("Firm Author Preferences have not been set for all variables.  Please set defaults for all assigned languages.", "Segment Designer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        AuthorPreferencesManager oForm = new AuthorPreferencesManager(oSegment.ID, true, true);
                        oForm.ShowDialog();
                    }
                }
                //Add to list of changed segments
                m_aChangedSegments.Add(oSegment.ID);
                if (bUpdateRelated)
                {
                    try
                    {
                        MacPac.Application.DisableTaskpaneEvents = true;
                        if (ProcessUpdateList() == UpdateListStatus.Failed)
                        {
                            //GLOG 2787: Something interrupted Updating process
                            bSaved = false;
                            oWordDoc.Activate();
                            MessageBox.Show(LMP.Resources.GetLangString("Error_FocusChangeDuringDesignUpdate"),
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    finally
                    {
                        MacPac.Application.DisableTaskpaneEvents = false;
                    }
                }

                //recompress Word tag attributes
                if (!bDisableEncryption)
                {
                    LMP.Forte.MSWord.Application.SuspendCompression(false);
                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression(m_oMPDocument.Tags, true, 
                            LMP.Data.Application.EncryptionPassword);
                    else
                        //10.2 (dm) - use new data structure
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression_CC(m_oMPDocument.Tags, true,
                            LMP.Data.Application.EncryptionPassword);
                }

                //mark as saved
                if (bSaved)
                    oWordDoc.Saved = true;
                
                return bSaved;
            }
            finally
            {
                //return rsid state
                oOptions.StoreRSIDOnSave = bSaveRSID;

                //restore compression status
                LMP.Forte.MSWord.Application.SuspendCompression(false);

                //GLOG 6339 (dm) - force ShowXMLMarkup
                LMP.Forte.MSWord.WordDoc.SetXMLMarkupState(oWordDoc, true, true);

                //unlock screen updating - toggle to ensure refresh
                this.TaskPane.ScreenUpdating = true;
                LMP.Forte.MSWord.Clipboard.DisableClipboardFunctions(false);
                LMP.Forte.MSWord.Clipboard.RestoreClipboard();
                oWaitCursor.Close();
            }
        }

        /// <summary>
        /// saves properties belonging to segment.definition to DB -
        /// returns true if successful
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool SaveSegmentDefinition(Segment oSegment, bool bSegmentRangeOnly, ForteDocument oMPDocument)
        {
            bool bSuccess = false;

            //save doc XML to segment db record.  This will include all
            //property and parameter values for variables, segments, blocks
            //these have been captured to XML via ExitEditMode code
            string xXML = null;
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //.doc - get xml
                if (!bSegmentRangeOnly)
                    xXML = oMPDocument.WordDocument.Content.get_XML(false);
                else
                    xXML = oSegment.PrimaryWordTag.get_XML(false);
            }
            else
            {
                //.docx - get word open xml
                //if (!bSegmentRangeOnly)
                xXML = oMPDocument.WordDocument.Content.WordOpenXML;
                //else
                //TODO: note that this branch would behave slightly differently than with
                //.doc format/xml node in that it won't include the content control itself -
                //we don't currently use bSegmentRangeOnly=true anywhere, so I'm not sure
                //that we need to deal with this (10.2 dm)
                //    xXML = oSegment.PrimaryContentControl.Range.WordOpenXML;

                //GLOG 5468:  AlternateContent XML for Textboxes containing Graphics can result 
                //in portions of graphic being cut off.  To avoid, remove AlternatContent nodes
                //and replace with contents of <w:pict> nodes contained within.
                int iStart = xXML.IndexOf("<mc:AlternateContent");
                while (iStart > -1)
                {
                    int iEnd = xXML.IndexOf("</mc:AlternateContent>", iStart);
                    if (iEnd > -1)
                    {
                        string xPictXML = xXML.Substring(iStart, iEnd - iStart);
                        int iPictStart = xPictXML.IndexOf("<w:pict>");
                        if (iPictStart > -1)
                        {
                            int iPictEnd = xPictXML.IndexOf("</w:pict>");
                            if (iPictEnd > -1)
                            {
                                xPictXML = xPictXML.Substring(iPictStart, (iPictEnd + @"</w:pict>".Length) - iPictStart);
                                xXML = xXML.Substring(0, iStart) + xPictXML + xXML.Substring(iEnd + @"</mc:AlternateContent>".Length);
                            }
                        }
                    }
                    iStart = xXML.IndexOf("<mc:AlternateContent>", iStart + 1);
                }
            }

            //remove IBF namespace from xml
            xXML = Regex.Replace(xXML, "xmlns:ns[0-9]*=\"http://schemas.microsoft.com/InformationBridge/2004\"", "");

            //remove authors xml
            xXML = RemoveAuthorsXML(xXML);

            //remove unavailable xml schema - solves
            //Word freezing on cut/paste of XML tags
            LMP.Forte.MSWord.WordDoc.RemoveUnavailableXMLSchema(oMPDocument.WordDocument);

            //prepend <mAuthorPrefs> code if the segment contains author preferences-
            //this tag allows us to display in the Author Preferences Manager only
            //those segments that contain author prefs
            //GLOG 6148: UserSegments never have Author Preferences
            bool bContainsAuthorPrefs = oSegment is AdminSegment && (xXML.ToUpper().Contains("[AUTHORPREFERENCE_") || 
                xXML.ToUpper().Contains("[LEADAUTHORPREFERENCE_") || 
                xXML.ToUpper().Contains("[AUTHORPARENTPREFERENCE") ||
                xXML.ToUpper().Contains("[LEADAUTHORPARENTPREFERENCE_"));

            if (bContainsAuthorPrefs)
                xXML = "<mAuthorPrefs>" + xXML;

            //GLOG 4578 (dm) - changed DefinitionContainsAuthorPreferences from a
            //property to a method
            bool bPreviouslyContainedAuthorPrefs = Segment.DefinitionContainsAuthorPreferences(
                oSegment.Definition.XML);

            if (oSegment is AdminSegment)
            {
                //create and save admin segment def
                try
                {
                    //create and fill def object
                    AdminSegmentDef oDef = (AdminSegmentDef)oSegment.Definition;

                    //populate definition from segment properties
                    FillSegmentDef(oSegment, xXML, oDef);

                    //save to db
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    oDefs.Save(oDef);

                    bSuccess = true;
                }
                catch (Exception oE)
                {
                    m_bDocIsDirty = true;

                    //unlock screen updating
                    this.TaskPane.ScreenUpdating = true;

                    throw new LMP.Exceptions.UIException(LMP.Resources.GetLangString(
                        "Error_CouldNotSaveAdminSegmentDefinitionChanges"), oE);
                }

                if (bContainsAuthorPrefs && !bPreviouslyContainedAuthorPrefs)
                {
                    //tag folders that contain this segment
                    //so that they appear in the Author Preference Manager -
                    //get folders that contain this segment
                    Folders oFolders = new Folders();
                    ArrayList oList = Folders.GetSegmentFolderIDs(oSegment.ID1);
                    foreach (object o in oList)
                    {
                        //cycle through folders in ID list
                        int iFolderID = (int)((object[])o)[0];
                        Folder oFolder = (Folder)oFolders.ItemFromID(iFolderID);

                        //tag if necessary
                        if (!oFolder.XML.Contains("<mAuthorPrefs>"))
                            oFolder.XML += "<mAuthorPrefs>";

                        oFolders.Save(oFolder);

                        //get parent folder id
                        int iParentFolderID = oFolder.ParentFolderID;

                        //cycle through ancestors, tagging each
                        while (iParentFolderID != 0)
                        {
                            oFolder = (Folder)oFolders.ItemFromID(iParentFolderID);

                            if (!oFolder.XML.Contains("<mAuthorPrefs>"))
                                oFolder.XML += "<mAuthorPrefs>";

                            oFolders.Save();

                            iParentFolderID = oFolder.ParentFolderID;
                        }
                    }
                }
                else if(!bContainsAuthorPrefs && bPreviouslyContainedAuthorPrefs)
                {
                    //segment doesn't contain prefs and previously did -
                    //remove tag from all folders that no longer contain
                    //segments that have author preferences
                    Folders oFolders = new Folders();
                    ArrayList oList = Folders.GetSegmentFolderIDs(oSegment.ID1);
                    foreach (object o in oList)
                    {
                        //recursively cycle through folders in ID list and their parents
                        int iFolderID = (int)((object[])o)[0];
                        Folder oFolder = (Folder)oFolders.ItemFromID(iFolderID);

                        //get all folders in folder, seeing if any are tagged - 
                        //if so, we can stop

                        //get all segments in folder, seeing if any contain
                        //preferences - if so, we can stop

                        //
                    }
                }
            }
            else
            {
                //create and save user segment def
                try
                {
                    //create UserSegment Def object
                    UserSegmentDefs oDefs = new UserSegmentDefs(
                        mpUserSegmentsFilterFields.Owner, Session.CurrentUser.ID);
                    UserSegmentDef oDef = (UserSegmentDef)oDefs
                        .ItemFromID(oSegment.ID1 + "." + oSegment.ID2);

                    //populate definition from segment properties
                    FillSegmentDef(oSegment, xXML, oDef);

                    //save to db
                    oDefs.Save(oDef);

                    bSuccess = true;
                }
                catch (Exception oE)
                {
                    m_bDocIsDirty = true;

                    //unlock screen updating
                    this.TaskPane.ScreenUpdating = true;

                    throw new LMP.Exceptions.UIException(LMP.Resources.GetLangString(
                        "Error_CouldNotSaveUserSegmentDefinitionChanges"), oE);
                }
            }

            return bSuccess;
        }

        /// <summary>
        /// populates specified segment definition
        /// with values from specified segment and xml
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oDef"></param>
        private void FillSegmentDef(Segment oSegment, string xXML, ISegmentDef oDef)
        {
            oDef.XML = xXML;

            //set def properties to current prop values
            oDef.DisplayName = oSegment.DisplayName;
            oDef.TypeID = oSegment.TypeID;
            oDef.L0 = oSegment.L0;
            oDef.L1 = oSegment.L1;
            oDef.L2 = oSegment.L2;
            oDef.L3 = oSegment.L3;
            oDef.L4 = oSegment.L4;

            oDef.DefaultDoubleClickBehavior = (short)oSegment.DefaultDoubleClickBehavior;
            oDef.DefaultDoubleClickLocation = (short)oSegment.DefaultDoubleClickLocation;
            oDef.DefaultDragBehavior = (short)oSegment.DefaultDragBehavior;
            oDef.DefaultDragLocation = (short)oSegment.DefaultDragLocation;
            oDef.DefaultMenuInsertionBehavior = (short)oSegment.DefaultMenuInsertionBehavior;
            oDef.MenuInsertionOptions = (short)oSegment.MenuInsertionOptions;
            oDef.HelpText = oSegment.HelpText;
            oDef.IntendedUse = oSegment.IntendedUse;

            if (oDef.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
            {
            }
            if (oDef.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
            {
                oDef.ChildSegmentIDs = oSegment.ChildSegmentIDs(true);
            }
            else
                //currently, answer files are admin segments -
                //we have to cast as an admin segment 
                //GLOG 3904: Set from private variable rather than Segment object
                oDef.ChildSegmentIDs = m_oCurAnswerFileSegments.ToString();
                //oDef.ChildSegmentIDs = ((AdminSegment)oSegment).AnswerFileSegments.ToString();
        }
        private void AppendToUpdateList(string xSegmentID)
        {
            m_aUpdateList = Segment.GetParentSegmentIDs(xSegmentID);
        }
        private string GetChangedListText()
        {
            if (m_aChangedSegments == null || m_aChangedSegments.Count == 0)
                return "";
            else
            {
                string xList = "";
                foreach (string xItem in m_aChangedSegments)
                {
                    if (xList != "")
                        xList = xList + ", ";
                    int iID1 = 0;
                    int iID2 = 0;
                    LMP.String.SplitID(xItem, out iID1, out iID2);
                    xList = xList + LMP.Data.Application.GetSegmentNameFromID(iID1);
                }
                return xList;
            }
        }
        /// <summary>
        /// overloaded method called from right click menu pick
        /// calls AddControlActions
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddControlActions(string xActionName)
        {
            AddControlActions(xActionName, null, null);
        }
        /// <summary>
        /// adds designed action to ControlActions collection
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddControlActions(string xActionName, Segment oSeg, Variable oVar)
        {
            //get segment and designated variable from current TreeNode if 
            //these have not been passed in
            if (oSeg == null)
                oSeg = GetSegmentFromNode(m_oCurNode);
            if (oVar == null)
                oVar = GetVariableFromNode(m_oCurNode);

            ControlAction oCtlAction = oVar.ControlActions.Create();
            ControlActions.Types iType = GetControlActionTypeFromName(xActionName);

            try
            {
                if (iType != 0)
                {
                    //set variable action type
                    oCtlAction.Type = iType;

                    //get parameter names and values - 
                    //these vary based on the action type
                    string[,] aParamNames = GetControlActionParameters(oCtlAction.Type);
                    string[] aParamValues = oCtlAction.Parameters.Split(StringArray.mpEndOfSubField);

                    //Action takes parameters
                    if (aParamNames != null)
                    {
                        //create default values, set parameter
                        if ((aParamNames.Length / 2 != aParamValues.Length) || aParamValues.Length == 1)
                        {
                            aParamValues = new string[aParamNames.GetUpperBound(0) + 1];
                            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                            {
                                //default values are second element in names array
                                aParamValues[i] = aParamNames[i, 1];
                            }
                        }

                        oCtlAction.Parameters = BuildParameterString(aParamValues);
                    }
                    oSeg.Variables.Save(oVar);

                    if (oVar is AdminVariable)
                    {
                        //capture current node key - it will be used to repoint
                        //m_oCurNode variable when new VA nodes are added
                        string xKey = m_oCurNode.Key;
                        this.AddControlActionNodes(m_oCurNode, oVar);

                        //repoint module level node variable
                        m_oCurNode = treeDocContents.GetNodeByKey(xKey);
                        m_oCurNode = m_oCurNode.Nodes[m_oCurNode.Nodes.Count - 1];

                        //expand newly added node - it will be the last one in the collection
                        m_oCurNode.ExpandAll(ExpandAllType.Always);
                        treeDocContents.ActiveNode = m_oCurNode;
                    }
                }
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotAddControlAction") + m_oCurNode.Text, oE);
            }
        }
        /// <summary>
        /// overloaded method called from right click menu pick
        /// calls AddControlActions
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddSegmentAuthorsControlActions(string xActionName)
        {
            AddSegmentAuthorsControlActions(xActionName, null);
        }
        /// <summary>
        /// adds designed action to ControlActions collection
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddSegmentAuthorsControlActions(string xActionName, Segment oSeg)
        {
            //get segment and designated variable from current TreeNode if 
            //these have not been passed in
            if (oSeg == null)
                oSeg = GetSegmentFromNode(m_oCurNode);

            ControlAction oCtlAction = oSeg.Authors.ControlActions.Create();
            ControlActions.Types iType = GetControlActionTypeFromName(xActionName);

            try
            {
                if (iType != 0)
                {
                    //set variable action type
                    oCtlAction.Type = iType;

                    //get parameter names and values - 
                    //these vary based on the action type
                    string[,] aParamNames = GetControlActionParameters(oCtlAction.Type);
                    string[] aParamValues = oCtlAction.Parameters.Split(StringArray.mpEndOfSubField);

                    //Action takes parameters
                    if (aParamNames != null)
                    {
                        //create default values, set parameter
                        if ((aParamNames.Length / 2 != aParamValues.Length) || aParamValues.Length == 1)
                        {
                            aParamValues = new string[aParamNames.GetUpperBound(0) + 1];
                            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                            {
                                //default values are second element in names array
                                aParamValues[i] = aParamNames[i, 1];
                            }
                        }

                        oCtlAction.Parameters = BuildParameterString(aParamValues);
                    }
                    
                    //Save segment XML
                    SaveSegmentAuthorsControlActions(oSeg); 

                    //do not add treenode for user segments
                    //user segment variables are created with a default 
                    //InsertText variable action

                    if (oSeg is AdminSegment || m_bIsContentDesignerSegment)
                    {
                        //capture current node key - it will be used to repoint
                        //m_oCurNode variable when new VA nodes are added
                        string xKey = m_oCurNode.Key;
                        this.AddSegmentControlActionNodes(oSeg.Authors.ControlActions,
                            m_oCurNode);

                        //repoint module level node variable
                        m_oCurNode = treeDocContents.GetNodeByKey(xKey);
                        m_oCurNode = m_oCurNode.Nodes[m_oCurNode.Nodes.Count - 1];

                        //expand newly added node - it will be the last one in the collection
                        m_oCurNode.ExpandAll(ExpandAllType.Always);
                        treeDocContents.ActiveNode = m_oCurNode;
                    }
                }
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotAddControlAction") + m_oCurNode.Text, oE);
            }
        }
        /// <summary>
        /// overloaded method called from right click menu pick
        /// calls AddControlActions
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddSegmentJurisdictionControlActions(string xActionName)
        {
            AddSegmentJurisdictionControlActions(xActionName, null);
        }
        /// <summary>
        /// adds designed action to ControlActions collection
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddSegmentJurisdictionControlActions(string xActionName, Segment oSeg)
        {
            //get segment and designated variable from current TreeNode if 
            //these have not been passed in
            if (oSeg == null)
                oSeg = GetSegmentFromNode(m_oCurNode);

            ControlAction oCtlAction = oSeg.CourtChooserControlActions.Create();
            ControlActions.Types iType = GetControlActionTypeFromName(xActionName);

            try
            {
                if (iType != 0)
                {
                    //set variable action type
                    oCtlAction.Type = iType;

                    //get parameter names and values - 
                    //these vary based on the action type
                    string[,] aParamNames = GetControlActionParameters(oCtlAction.Type);
                    string[] aParamValues = oCtlAction.Parameters.Split(StringArray.mpEndOfSubField);

                    //Action takes parameters
                    if (aParamNames != null)
                    {
                        //create default values, set parameter
                        if ((aParamNames.Length / 2 != aParamValues.Length) || aParamValues.Length == 1)
                        {
                            aParamValues = new string[aParamNames.GetUpperBound(0) + 1];
                            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                            {
                                //default values are second element in names array
                                aParamValues[i] = aParamNames[i, 1];
                            }
                        }

                        oCtlAction.Parameters = BuildParameterString(aParamValues);
                    }

                    //Save segment XML
                    SaveSegmentJurisdictionControlActions(oSeg);

                    //do not add treenode for user segments
                    //user segment variables are created with a default 
                    //InsertText variable action

                    if (oSeg is AdminSegment || m_bIsContentDesignerSegment)
                    {
                        //capture current node key - it will be used to repoint
                        //m_oCurNode variable when new VA nodes are added
                        string xKey = m_oCurNode.Key;
                        this.AddSegmentControlActionNodes(oSeg.CourtChooserControlActions,
                            m_oCurNode);

                        //repoint module level node variable
                        m_oCurNode = treeDocContents.GetNodeByKey(xKey);
                        m_oCurNode = m_oCurNode.Nodes[m_oCurNode.Nodes.Count - 1];

                        //expand newly added node - it will be the last one in the collection
                        m_oCurNode.ExpandAll(ExpandAllType.Always);
                        treeDocContents.ActiveNode = m_oCurNode;
                    }
                }
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotAddControlAction") + m_oCurNode.Text, oE);
            }
        }
        /// <summary>
        /// overloaded method called from right click menu pick
        /// calls AddControlActions
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddSegmentLanguageControlActions(string xActionName)
        {
            AddSegmentLanguageControlActions(xActionName, null);
        }
        /// <summary>
        /// adds designed action to ControlActions collection
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddSegmentLanguageControlActions(string xActionName, Segment oSeg)
        {
            //get segment and designated variable from current TreeNode if 
            //these have not been passed in
            if (oSeg == null)
                oSeg = GetSegmentFromNode(m_oCurNode);

            ControlAction oCtlAction = oSeg.LanguageControlActions.Create();
            ControlActions.Types iType = GetControlActionTypeFromName(xActionName);

            try
            {
                if (iType != 0)
                {
                    //set variable action type
                    oCtlAction.Type = iType;

                    //get parameter names and values - 
                    //these vary based on the action type
                    string[,] aParamNames = GetControlActionParameters(oCtlAction.Type);
                    string[] aParamValues = oCtlAction.Parameters.Split(StringArray.mpEndOfSubField);

                    //Action takes parameters
                    if (aParamNames != null)
                    {
                        //create default values, set parameter
                        if ((aParamNames.Length / 2 != aParamValues.Length) || aParamValues.Length == 1)
                        {
                            aParamValues = new string[aParamNames.GetUpperBound(0) + 1];
                            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                            {
                                //default values are second element in names array
                                aParamValues[i] = aParamNames[i, 1];
                            }
                        }

                        oCtlAction.Parameters = BuildParameterString(aParamValues);
                    }

                    //Save segment XML
                    SaveSegmentLanguageControlActions(oSeg);

                    //do not add treenode for user segments
                    //user segment variables are created with a default 
                    //InsertText variable action

                    if (oSeg is AdminSegment || m_bIsContentDesignerSegment)
                    {
                        //capture current node key - it will be used to repoint
                        //m_oCurNode variable when new VA nodes are added
                        string xKey = m_oCurNode.Key;
                        this.AddSegmentControlActionNodes(oSeg.LanguageControlActions,
                            m_oCurNode);

                        //repoint module level node variable
                        m_oCurNode = treeDocContents.GetNodeByKey(xKey);
                        m_oCurNode = m_oCurNode.Nodes[m_oCurNode.Nodes.Count - 1];

                        //expand newly added node - it will be the last one in the collection
                        m_oCurNode.ExpandAll(ExpandAllType.Always);
                        treeDocContents.ActiveNode = m_oCurNode;
                    }
                }
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotAddControlAction") + m_oCurNode.Text, oE);
            }
        }
        /// <summary>
        /// overloaded method called from right click menu pick
        /// calls AddVariableActions
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddVariableActions(string xActionName)
        {
            AddVariableActions(xActionName, null, null);
        }
        /// <summary>
        /// adds designed action to VariableActions collection
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddVariableActions(string xActionName, Segment oSeg, Variable oVar)
        {
            //get segment and designated variable from current TreeNode if 
            //these have not been passed in
            if (oSeg == null)
                oSeg = GetSegmentFromNode(m_oCurNode);
            if (oVar == null)
                oVar = GetVariableFromNode(m_oCurNode);

            VariableAction oVarAction = oVar.VariableActions.Create();
            VariableActions.Types iType = GetVariableActionTypeFromName(xActionName);

            try
            {
                if (iType != 0)
                {
                    //set variable action type
                    oVarAction.Type = iType;

                    //get parameter names and values - 
                    //these vary based on the action type
                    string[,] aParamNames = GetVariableActionParameters(oVarAction.Type);
                    string[] aParamValues = oVarAction.Parameters.Split(StringArray.mpEndOfSubField);

                    // Action takes parameters defined
                    if (aParamNames != null)
                    {
                        //create default values, set parameter
                        if ((aParamNames.Length / 2 != aParamValues.Length) || aParamValues.Length == 1)
                        {
                            aParamValues = new string[aParamNames.GetUpperBound(0) + 1];
                            for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                            {
                                //default values are second element in names array
                                aParamValues[i] = aParamNames[i, 1];
                            }
                        }

                        oVarAction.Parameters = BuildParameterString(aParamValues);
                    }

                    oSeg.Variables.Save(oVar);

                    if (oVar is AdminVariable)
                    {
                        //capture current node key - it will be used to repoint
                        //m_oCurNode variable when new VA nodes are added
                        string xKey = m_oCurNode.Key;
                        this.AddVariableActionNodes(m_oCurNode, oVar);

                        //repoint module level node variable
                        m_oCurNode = treeDocContents.GetNodeByKey(xKey);
                        m_oCurNode = m_oCurNode.Nodes[m_oCurNode.Nodes.Count - 1];

                        //expand newly added node - it will be the last one in the collection
                        m_oCurNode.ExpandAll(ExpandAllType.Always);
                        treeDocContents.ActiveNode = m_oCurNode;
                    }
                }
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotAddVariableAction") + m_oCurNode.Text, oE);
            }
        }
        /// <summary>
        /// adds designed action to Segment.Actions collection
        /// </summary>
        /// <param name="xActionName"></param>
        private void AddSegmentActions(string xActionName)
        {
            Segment oSeg = GetSegmentFromNode(m_oCurNode);
            SegmentAction oSegAction = oSeg.Actions.Create();
            //Segment action types are variable action types
            VariableActions.Types iType = GetVariableActionTypeFromName(xActionName);
            Segment.Events oEvents = new Segment.Events();

            //get the designated Segment Event
            switch (m_oCurNode.Text)
            {
                case "After Default Author Set":
                    oEvents = Segment.Events.AfterDefaultAuthorSet;
                    break;
                case "After Default Values Set":
                    oEvents = Segment.Events.AfterDefaultValuesSet;
                    break;
                case "After Segment XML Insert":
                    oEvents = Segment.Events.AfterSegmentXMLInsert;
                    break;
                case "After Segment Generated":
                    oEvents = Segment.Events.AfterSegmentGenerated;
                    break;
                case "After Segment Valid":
                    oEvents = Segment.Events.AfterSegmentValid;
                    break;
                case "Authors Actions":
                    oEvents = Segment.Events.AfterAuthorUpdated;
                    break;
                case "Language Actions":
                    oEvents = Segment.Events.AfterLanguageUpdated;
                    break;
                case "Before Segment Finish": //GLOG 8512
                    oEvents = Segment.Events.BeforeSegmentFinish;
                    break;
                default:
                    break;
            }

            try
            {
                //add action if type is valid
                if (iType != 0 && oEvents != 0)
                {
                    oSegAction.Type = iType;
                    oSegAction.Event = oEvents;
                    oSeg.Actions.Save();

                    //capture key of current node
                    string xKey = m_oCurNode.Key;

                    //rebuild segment event nodes
                    if ((m_oCurNode.Text == "Language Actions") ||
                        (m_oCurNode.Text == "Authors Actions"))
                    {
                        //add directly to root node
                        this.AddSegmentEventNodes(oSeg, m_oCurNode);
                    }
                    else
                        this.AddSegmentEventNodes(oSeg, m_oCurNode.Parent);

                    //repoint module level node variable
                    m_oCurNode = treeDocContents.GetNodeByKey(xKey);
                    m_oCurNode.Expanded = true;
                    m_oCurNode = m_oCurNode.Nodes[m_oCurNode.Nodes.Count - 1];

                    //expand newly added node - it will be the last one in the collection
                    m_oCurNode.ExpandAll(ExpandAllType.Always);
                    treeDocContents.ActiveNode = m_oCurNode;
                }
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString(
                        "Error_CouldAddSegmentActionForEvent") + m_oCurNode.Text, oE);
            }
        }
        /// <summary>
        /// sets up preview doc w/ edited segment
        /// </summary>
        private void SetupPreviewMode()
        {
            Word.Document oWordDoc = m_oMPDocument.WordDocument;
            string xXML = null;

            //we need to insert in a new document - that document's 
            //ForteDocument object will need to insert the segment, as 
            //the current doc's ForteDocument points to the current doc - 
            //specify a pending action that the new content manager instance 
            //will process

            bool bSaved = oWordDoc.Saved;

            //get rsid state for later use
            Word.Options oOptions = m_oMPDocument.WordDocument.Application.Options;
            bool bSaveRSID = oOptions.StoreRSIDOnSave;

            try
            {
                //get top level segment
                Segment oSegment = m_oMPDocument.Segments[0];

                //check for valid control value for current control - exit if invalid
                //otherwise run ExitEditMode to ensure update of segment properties
                try
                {
                    if (!this.IsValidControlValue((IControl)m_oCurCtl, m_oCurNode))
                    {
                        this.TaskPane.ScreenUpdating = true;
                        this.InvalidControlValue = true;
                        return;
                    }
                    else
                    {
                        //lock screen updating
                        this.TaskPane.ScreenUpdating = false;

                        ExitEditMode(false);
                        this.InvalidControlValue = false;
                    }
                }
                catch { }

                //exit if design is not valid -
                //the method below will alert accordingly -
                //2/19/09 (dm) - moved this block below IsValidControlValue block
                if (!DesignIsValid())
                    return;

                //save top-level segment -

                //temporarily turn off rsid saving -
                //we don't want to save rsid (revision ID) tags
                oOptions.StoreRSIDOnSave = false;

                //ensure that new segment bookmark is present
                if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmarkIfNecessary(
                        m_oMPDocument.WordDocument, oSegment.PrimaryWordTag);
                }
                else
                {
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmarkIfNecessary_CC(
                        m_oMPDocument.WordDocument, oSegment.PrimaryContentControl);
                }

                //bookmark current selection - collapse selection
                Word.Range oRange = m_oMPDocument.WordDocument.Application.Selection.Range;
                string xName = "zzmpTemp";
                LMP.Forte.MSWord.WordDoc.AddBookMarkToSelection(xName, true);

                //GLOG 7536: Decrypt Object Data to ensure all document variables exist in Preview doc
                //check first if attributes are currently compressed
                bool bDisableEncryption = (LMP.Registry.GetMacPac10Value("DisableEncryption").ToLower() == "miller" &&
                    LMP.Data.Application.DisableEncryptionPassword.ToLower() == "fisherman") ||
                    LMP.Data.Application.EncryptionPassword == "";

                //LMP.Forte.MSWord.Application oCOM = new LMP.Forte.MSWord.ApplicationClass();
                LMP.Forte.MSWord.Application oCOM = new LMP.Forte.MSWord.Application();
                if (!bDisableEncryption)
                {
                    //document contains compressed object data -
                    //decompress Word tag attributes
                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression(m_oMPDocument.Tags, false, "");
                    else
                        //10.2 (dm) - use new data structure
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression_CC(m_oMPDocument.Tags, false, "");

                    //prevent further compression during save
                    LMP.Forte.MSWord.Application.SuspendCompression(true);
                } //END GLOG 7536

                //GLOG 3663: Refresh to ensure nodestore reflects current ObjectData
                oSegment.Refresh();
                //add containing segments to all story ranges that contain text
                InsertmSEGInAllStoryRanges(false,false);

                //return to original selection, delete temp bookmark
                bool bDelete = true;
                LMP.Forte.MSWord.WordDoc.SelectBookmarkRange(xName, oRange, bDelete);

                //convert inline tags where possible
                LMP.Forte.MSWord.WordDoc.ConvertBlockableInlineTags(m_oMPDocument.WordDocument);

                //add TagExpandedValue field codes
                //10.2 (dm) - skip this for now when using content controls -
                //it's not yet clear whether this will ultimately be necessary
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    oSegment.RefreshExpandedTagValueCodes(true);

                //execute actions of variables with static default values
                this.RefreshStaticDefaults(oSegment);

                //GLOG item #5070 - conditionalize on file format - dcf
                //get document content
                if (m_oMPDocument.FileFormat == mpFileFormats.Binary)
                {
                    //2-2-11 (dm) - add bookmarks before getting segment xml
                    m_oMPDocument.Tags.AddBookmarks();

                    xXML = oWordDoc.Content.get_XML(false);
                }
                else
                {
                    xXML = oWordDoc.WordOpenXML;
                }

                //restore deleted scopes for editing
                oSegment.RestoreDeletedScopesForDesign();

                //reinsert placeholder text
                LMP.Forte.MSWord.WordDoc.InsertPlaceholderText(m_oMPDocument.Tags); //GLOG 7536

                //hide TagExpandedValue field codes in designer -
                //10.2 (dm) - skip this for now when using content controls -
                //it's not yet clear whether this will ultimately be necessary
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    oSegment.RefreshExpandedTagValueCodes(false);

                //GLOG 7536: recompress Word tag attributes
                if (!bDisableEncryption)
                {
                    LMP.Forte.MSWord.Application.SuspendCompression(false);
                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression(m_oMPDocument.Tags, true,
                            LMP.Data.Application.EncryptionPassword);
                    else
                        //10.2 (dm) - use new data structure
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression_CC(m_oMPDocument.Tags, true,
                            LMP.Data.Application.EncryptionPassword);
                } //END GLOG 7536

                //create a pending action that gets the xml from the document -
                //this way, we don't have to save the design to get the latest
                //document edits
                if (this.m_oMPDocument.Segments[0].IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                {
                    this.TaskPane.ContentManager.InsertAnswerFile(this.m_oMPDocument.Segments[0].ID,
                        null, "", true);
                }
                else
                {
                    TaskPane.PendingAction = new PendingAction(PendingAction.Types
                        .InsertForPreview, this.m_oMPDocument.Segments[0].ID, null, "", xXML, false, null);

                    //return to initial save state
                    oWordDoc.Saved = bSaved;

                    //create a new document
                    Word.Document oNew = LMP.Forte.MSWord.WordApp.CreateForteDocument();

                    //Allow TaskPane Setup to complete
                    System.Windows.Forms.Application.DoEvents();

                    if (!TaskPane.SkipScreenUpdates)
                    {
                        TaskPane oTP = TaskPanes.Item(oNew);

						//GLOG - 3338 - CEH
                        if (TaskPane.NewSegment.ShowChooser)
                            oTP.DocEditor.SelectChooserNode();
                        else
                        {
                            if (TaskPane.NewSegment.Variables.Count > 0)
                                //Set focus to first variable control in tree
                                oTP.DocEditor.SelectFirstVariableNode();
                            else
                                oTP.DocEditor.SelectFirstNodeForSegment(TaskPane.NewSegment);
                        }

                        System.Windows.Forms.Application.DoEvents();
                    }
                }
            }
            finally
            {
                //return rsid state
                oOptions.StoreRSIDOnSave = bSaveRSID;

                //unlock screen updating - toggle to ensure refresh
                this.TaskPane.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// returns true iff the design of the segment is valid
        /// </summary>
        /// <returns></returns>
        private bool DesignIsValid()
        {
            Segment oSegment = m_oMPDocument.Segments[0];

            //alert and return if design doc contains more than one top level segment
            if (m_oMPDocument.Segments.Count > 1)
            {
                MessageBox.Show(LMP.Resources.GetLangString(
                    "Msg_CantSaveMultSegmentDesignDoc"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return false;
            }
            else if (oSegment.IntendedUse == mpSegmentIntendedUses.AsAnswerFile &&
                oSegment.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
            {
                if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    if (oSegment.WordTags[0].Range.Text != null)
                    {
                        string xMsg = "Msg_AnswerFileTextNotAllowed";
                        //Prompt to delete text
                        DialogResult iRet = MessageBox.Show(LMP.Resources.GetLangString(xMsg),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                        if (iRet == DialogResult.OK)
                        {
                            object oMissing = (object)System.Reflection.Missing.Value;
                            oSegment.WordTags[0].Range.Delete(ref oMissing, ref oMissing);
                            return true;
                        }
                        else
                            //Cancel save
                            return false;
                    }
                    else
                        return true;
                }
                else
                {
                    if (oSegment.ContentControls[0].Range.Text != null)
                    {
                        string xMsg = "Msg_AnswerFileTextNotAllowed";
                        //Prompt to delete text
                        DialogResult iRet = MessageBox.Show(LMP.Resources.GetLangString(xMsg),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                        if (iRet == DialogResult.OK)
                        {
                            object oMissing = (object)System.Reflection.Missing.Value;
                            oSegment.ContentControls[0].Range.Delete(ref oMissing, ref oMissing);
                            return true;
                        }
                        else
                            //Cancel save
                            return false;
                    }
                    else
                        return true;
                }
            }
            //GLOG 7153: XMLSchemaViolations property not supported in Word 2013
            else if ((LMP.Forte.MSWord.WordApp.Version < 15) && m_oMPDocument.WordDocument.XMLSchemaViolations.Count > 0)
            {
                //prompt to correct xml schema violation and cancel save
                LMP.Architect.Api.ForteDocument.WordXMLEvents iIgnoreWordXMLEvents =
                    ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                m_oMPDocument.WordDocument.XMLSchemaViolations[1].Range.Select();
                Word.Selection oSel = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection;
                oSel.SetRange(oSel.Start - 1, oSel.End + 1);
                MessageBox.Show(LMP.Resources.GetLangString("Msg_SchemaViolationInDesignDoc"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ForteDocument.IgnoreWordXMLEvents = iIgnoreWordXMLEvents;
                return false;
            }
            else if (DesignContainsUntaggedContent())
            {
                //GLOG 92 - disallow text in body outside of top-level mSEG
                return false;
            }
            else if (!CIEnabledVariablesAreValid(oSegment))
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_CIEnabledVariablesViolation"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (!CollectionTableTagsAreValid(oSegment))
                //ensure that all pleading table segments are entirely inside of tables
                return false;
            else if (!SentenceTextSegmentsAreValid(oSegment))
                //ensure that sentence text segments do not contain multiple paragraphs
                return false;

            return true;
        }

        /// <summary>
        /// returns true iff all of the pleading table mSEGs belonging to oSegment
        /// and its children are inside a table -
        /// if the closing tag is outside of the table, and contains no additional text,
        /// this method silently moves it into the table -
        /// if the opening tag is outside of the table, or if the closing tag contains
        /// trailing text, the user is prompted that the design can't be saved.
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool CollectionTableTagsAreValid(Segment oSegment)
        {
            //validate oSegment
            bool bIsValid = false;
            bool bRefreshRequired = false;
            LMP.Forte.MSWord.Pleading oCOM = new LMP.Forte.MSWord.Pleading();
            mpObjectTypes oType = oSegment.TypeID;
            if (LMP.Data.Application.IsCollectionTableType(oType))
            {
                //JTS 4/15/10: Content Control support added
                Word.Range oParentRng = null;
                bool bContentControls = false;
                Word.XMLNode oWordTag = null;
                Word.ContentControl oCC = null;
                if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                {
                    bContentControls = true;
                    oCC = oSegment.ContentControls[0];
                    oParentRng = oCC.Range;
                }
                else
                {
                    bContentControls = false;
                    oWordTag = oSegment.WordTags[0];
                    oParentRng = oWordTag.Range;
                }

                if (oParentRng.Tables.Count == 0)
                {
                    //segment is not in a table - switch to non-collection type if possible
                    switch (oType)
                    {
                        case mpObjectTypes.AgreementSignature:
                            oSegment.TypeID = mpObjectTypes.AgreementSignatureNonTable;
                            bIsValid = true;
                            break;
                        case mpObjectTypes.LetterSignature:
                            oSegment.TypeID = mpObjectTypes.LetterSignatureNonTable;
                            bIsValid = true;
                            break;
                        case mpObjectTypes.PleadingSignature:
                            oSegment.TypeID = mpObjectTypes.PleadingSignatureNonTable;
                            bIsValid = true;
                            break;
                        default:
                            break;
                    }

                    if (bIsValid)
                    {
                        //write new type to the document
                        oSegment.Nodes.SetItemObjectDataValue(oSegment.FullTagID,
                            "ObjectTypeID", ((Int32)oSegment.TypeID).ToString());
                    }
                }
                else if (!bContentControls)
                {
                    //validate table
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    LMP.Forte.MSWord.Tags oTags = m_oMPDocument.Tags;
                    oCOM.RetagCollectionTableItemIfNecessary(ref oWordTag, ref oTags,
                        ref bIsValid, ref bRefreshRequired);
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                }
                else
                {
                    bIsValid = true;
                    bRefreshRequired = false;
                }

                if (!bIsValid)
                {
                    string xMsg = "Msg_CantSaveInvalidCollectionTableItem";
                    MessageBox.Show(LMP.Resources.GetLangString(xMsg) + oSegment.Name,
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (bRefreshRequired)
                    //closing mSEG was moved into the table
                    oSegment.RefreshNodes();

                //validate allow side-by-side configuration
                if (oSegment is CollectionTableItem)
                {
                    bool bAllowSideBySide = ((CollectionTableItem)oSegment).AllowSideBySide;
                    if (bAllowSideBySide)
                    {
                        bool bDesignIsValid = false;
                        if (bContentControls)
                            bDesignIsValid = oCOM.SideBySideDesignIsValid_CC(oCC);
                        else
                            bDesignIsValid = oCOM.SideBySideDesignIsValid(oWordTag);
                        if (!bDesignIsValid)
                        {
                            string xMsg = "Msg_CantSaveDesign_AllowSideBySideInvalid";
                            MessageBox.Show(LMP.Resources.GetLangString(xMsg) + oSegment.Name,
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }
            }

            //validate children
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                bIsValid = CollectionTableTagsAreValid(oSegment.Segments[i]);
                if (!bIsValid)
                    return false;
            }

            return true;
        }


        /// <summary>
        /// returns false if oSegment contains a mixture of 
        /// CI enabled variables (To/From/CC/BCC) with one of type 'Other'
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool CIEnabledVariablesAreValid(Segment oSegment)
        {
            //validate oSegment
            ciRetrieveData iRetrieveDataTo = 0;
            ciRetrieveData iRetrieveDataFrom = 0;
            ciRetrieveData iRetrieveDataCC = 0;
            ciRetrieveData iRetrieveDataBCC = 0;
            object[,] aCustomEntities = null;
            ciAlerts iCIAlerts = 0;

            oSegment.GetCIFormat(ref iRetrieveDataTo, ref iRetrieveDataFrom, ref iRetrieveDataCC,
                ref iRetrieveDataBCC, ref aCustomEntities, ref iCIAlerts);
            
            bool bCustomEntitiesExist = (aCustomEntities != null && aCustomEntities.Length > 0);

            //mixture of CI-enabled variables exists
            if ((iRetrieveDataTo != 0 || iRetrieveDataFrom != 0 ||
                iRetrieveDataCC != 0 || iRetrieveDataBCC != 0) &&
                bCustomEntitiesExist)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// adds mSeg tag at designated range
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oRange"></param>
        private void AddContainingmSeg(Segment oSegment, Word.Range oRange)
        {
            StringBuilder oSB = new StringBuilder();

            bool bShowSegDialog = false;

            if (oSegment is AdminSegment || m_bIsContentDesignerSegment)
                bShowSegDialog = oSegment.ShowSegmentDialog;

            //get top level mSEG's object data 
            oSB.AppendFormat(Segment.ADMIN_SEG_OBJECT_DATA_TEMPLATE, oSegment.ID1, 
                ((short)oSegment.TypeID).ToString(), oSegment.Definition.Name, oSegment.DisplayName, "",
                (int)oSegment.IntendedUse, bShowSegDialog, oSegment.DisplayName,
                ((short)oSegment.DefaultDoubleClickLocation).ToString(), 1);    //GLOG : 15769 : ceh
            string xObjectData = oSB.ToString();
            string xPartNumber = GetNewPartNumber(oSegment).ToString();
            string xTagID = oSegment.FullTagID;

            //create containing tag
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //insert xml tag
                Word.XMLNode oTag = null;

                //prevent xml events from being handled
                LMP.Architect.Api.ForteDocument.WordXMLEvents iIgnoreWordXMLEvent = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                oTag = LMP.Forte.MSWord.WordDoc.InsertmSEGAtSelection(xTagID, xObjectData, xPartNumber);
                ForteDocument.IgnoreWordXMLEvents = iIgnoreWordXMLEvent;

                oTag.Range.Select();

                //add tag to collection
                oSegment.ForteDocument.Tags.Insert(oTag, false);
            }
            else
            {
                //insert content control
                Word.ContentControl oCC = null;

                LMP.Architect.Api.ForteDocument.WordXMLEvents iIgnoreWordXMLEvent = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                oCC = LMP.Forte.MSWord.WordDoc.InsertmSEGAtSelection_CC(xTagID, xObjectData, xPartNumber);
                ForteDocument.IgnoreWordXMLEvents = iIgnoreWordXMLEvent;

                oCC.Range.Select();

                //add tag to collection
                oSegment.ForteDocument.Tags.Insert_CC(oCC, false);
            }
        }
        /// <summary>
        /// adds mSeg tag at designated range
        /// creates mSeg for child segment that is not yet
        /// saved in db
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oRange"></param>
        private void InsertChildmSEG(Word.Range oRange, ISegmentDef oDef, string xTagID)
        {
            StringBuilder oSB = new StringBuilder();

            //create containing mSEG's object data 
            oSB.AppendFormat(Segment.ADMIN_SEG_OBJECT_DATA_TEMPLATE, Segment.UNSAVED_ID, 
                ((short)oDef.TypeID).ToString(), oDef.Name, oDef.DisplayName, "", 
                (int)LMP.Data.mpSegmentIntendedUses.AsDocumentComponent, "", oDef.DisplayName,
                oDef.DefaultDoubleClickLocation.ToString(), 1);
            
            string xObjectData = oSB.ToString();

            //create containing tag and select range
            Word.XMLNode oTag = null;
            Word.ContentControl oCC = null;

            //prevent xml events from being handled - this is only important
            //in 12 because we subscribe to the XMLAfterInsert event at all times -
            //in 11, we subscribe as needed due to bugs in that version
            LMP.Architect.Api.ForteDocument.WordXMLEvents iIgnoreWordXMLEvent = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //insert xml tag
                oTag = LMP.Forte.MSWord.WordDoc.InsertmSEGAtSelection(xTagID, xObjectData, "1");
                oTag.Range.Select();
            }
            else
            {
                //insert content control
                oCC = LMP.Forte.MSWord.WordDoc.InsertmSEGAtSelection_CC(xTagID, xObjectData, "1");
                oCC.Range.Select();
            }
            ForteDocument.IgnoreWordXMLEvents = iIgnoreWordXMLEvent;
            
            //add new tag to document's tag collection
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                m_oMPDocument.Tags.Insert(oTag, false);
            else
                m_oMPDocument.Tags.Insert_CC(oCC, false);

            m_oMPDocument.RefreshTags(false);
        }       
        /// <summary>
        /// initializes node corresponding to oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        private void InitializeNode(Segment oSegment, UltraTreeNode oNode)
        {
            //get image for node
            Images oImage = GetSegmentImage(oSegment);

            //configure node
            SetNodeAppearance(oNode, true, false, true, Color.Black, oImage);

            //add reference to segment
            oNode.Tag = oSegment;
        }
        /// <summary>
        /// initializes node corresponding to oVar
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="oNode"></param>
        private void InitializeNode(Variable oVar, UltraTreeNode oNode)
        {
            //configure node
            SetNodeAppearance(oNode, false, true, true, Color.Black, Images.Variable);
           
            //add reference to variable
            oNode.Tag = oVar;
        }
        /// <summary>
        /// initializes node corresponding to oBlock
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="oNode"></param>
        private void InitializeNode(Block oBlock, UltraTreeNode oNode)
        {
            //configure node
            SetNodeAppearance(oNode, false, true, true, Color.Black, Images.Variable);

            //add reference to variable
            oNode.Tag = oBlock;
        }
        /// <summary>
        /// creates variable after validating selection range and name
        /// </summary>
        /// <param name="bCreateAssociatedTag"></param>
        private void CreateVariable(bool bCreateAssociatedTag)
        {
            string xName = null;
            Segment oSeg = null;
            Word.Range oRange = null;
            bool bAddContainingSeg = false;
            string xSelection = "";

            LMP.Forte.MSWord.TagTypes iType = LMP.Forte.MSWord.TagTypes.Variable;

            try
            {
                TaskPane.ScreenUpdating = false;

                //get selection range
                oRange = m_oMPDocument.WordDocument.Application.Selection.Range;
                //get current edited segment
                oSeg = GetSegmentFromNode(m_oCurNode);

                if (oSeg == null)
                    return;
                
                //Answer File only allows tagless variables
                if (oSeg.IntendedUse == mpSegmentIntendedUses.AsAnswerFile ||
                    oSeg.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                    bCreateAssociatedTag = false;

                //tagless variables pass tagtypes none
                if (!bCreateAssociatedTag)
                    iType = LMP.Forte.MSWord.TagTypes.None;

                //validate selection
                LMP.Forte.MSWord.TagInsertionValidityStates iValid = 
                    ValidateInsertionLocation(oRange, oSeg, iType);

                if (iValid == LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoContainingSegmentExists)
                    bAddContainingSeg = true;
                else if (iValid != LMP.Forte.MSWord.TagInsertionValidityStates.Valid)
                {
                    //location is invalid - alert
                    string xMsg = GetInsertionInvalidityMessage(iValid,
                        ForteDocument.TagTypes.Variable);

                    MessageBox.Show(xMsg, LMP.String.MacPacProductName, 
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                try
                {
                   if (bCreateAssociatedTag)
                    xSelection = oRange.Text;
                }
                catch { }
                //display basic name form to launch var creation
                string xDisplayName = null;
                GetNewVarOrBlockName(oSeg, ForteDocument.TagTypes.Variable, out xName, out xDisplayName);

                //xName is null if user cancels name dialog; do not create variable                    
                if (xName == null)
                    return;

                Variable oVar = null;
                //create the varible - either user or admin
                if (oSeg is AdminSegment || m_bIsContentDesignerSegment)
                    oVar = oSeg.Variables.CreateAdminVariable();
                else
                    oVar = oSeg.Variables.CreateUserVariable();
                
                if (oVar != null)
                {
                    //name, display name & execution index are required
                    //for valid variable
                    oVar.Name = xName;
                    oVar.DisplayName = xDisplayName;
                    oVar.ExecutionIndex = oSeg.Variables.Count + 1;

                    //10.2 (dm) - assign object database id
                    oVar.ObjectDatabaseID = oSeg.GetNewObjectDatabaseID();

                    //Set default value = selected text, if any
                    string xSelText = null;

                    //if we have a user variable, add default InsertText variable Action
                    if (oVar is UserVariable)
                        AddVariableActions("InsertText", oSeg, oVar);

                    //freeze tree while node is added and expanded
                    this.SuspendLayout();

                    //create associated Word Tag if designated -
                    if (bCreateAssociatedTag)
                    {
                        //create a containing segment if flag set above
                        if (bAddContainingSeg == true)
                            this.AddContainingmSeg(oSeg, oRange);

                        //only set default value if there's text
                        if (IsValidDefaultValue(xSelText))
                            oVar.DefaultValue = xSelText;

                        //insert the associated word tag
                        //set ProgramaticallySelectingWordTag property to ensure 
                        //m_oCurNode does not get reset by OnXMLSelectionChange event handler
                        this.ProgrammaticallySelectingWordTag = true;
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            oVar.InsertAssociatedTag();
                        else
                            oVar.InsertAssociatedContentControl();
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                        this.ProgrammaticallySelectingWordTag = false;
                    }
                    else
                    {
                        //tagless variable storage will be attached to Segment XML
                        StoreTaglessVariable(oSeg, ref oVar);
                        oSeg.RefreshNodes();
                        InsertNode(oVar);
                    }

                    oSeg.Variables.Save(oVar);

                    //if we have an associated variable, set XMLNode text = variable name
                    if (bCreateAssociatedTag)
                    {
                        if (!string.IsNullOrEmpty(xSelection))
                        {
                            string xVarText = "";
                            //GLOG 8775: If variable text is same as original selection text,
                            //temporarily clear out tag so that same text will not be matched by find
                            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            {
                                xVarText =  oVar.AssociatedWordTags[0].Range.Text;
                                if (xSelection.ToLower() == xVarText.ToLower())
                                    oVar.AssociatedWordTags[0].Range.Text = "";
                            }
                            else
                            {
                                xVarText = oVar.AssociatedContentControls[0].Range.Text;
                                if (xSelection.ToLower() == xVarText.ToLower())
                                    oVar.AssociatedContentControls[0].Range.Text = "";
                            }
                        
                            object oSelText = xSelection;
                            object oTrue = true;
                            object oFalse = false;
                            object oMissing = System.Reflection.Missing.Value;
                            Word.Range oDocRange = oRange.Document.Range();
                            oDocRange.Find.ClearHitHighlight();
                            oDocRange.Find.ClearFormatting();
                            oDocRange.Find.ClearAllFuzzyOptions();
                            oDocRange.Find.Wrap = Word.WdFindWrap.wdFindStop;
                            oDocRange.Find.MatchWholeWord = true;
                            oDocRange.Find.MatchCase = false;
                            if (oDocRange.Find.HitHighlight(ref oSelText))
                            {
                                CustomMessageBox oForm = new CustomMessageBox();
                                oForm.YesText = "&Add for all instances";
                                oForm.NoText = "&Prompt for each instance";
                                oForm.CancelText = "&Don't Add";
                                DialogResult iRet = oForm.Show("Add Variable tag to additional instances of the same text?", "Create Variable",
                                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                                if (xSelection.ToLower() == xVarText.ToLower())
                                {
                                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                                    {
                                        oVar.AssociatedWordTags[0].Range.Text = xVarText;
                                    }
                                    else
                                    {
                                        oVar.AssociatedContentControls[0].Range.Text = xVarText;
                                    }
                                }
                                if (iRet != DialogResult.Cancel)
                                {
                                    bool bPrompt = (iRet == DialogResult.No);
                                    oDocRange.Find.ClearHitHighlight();
                                    bool bFound = oDocRange.Find.Execute();
                                    while (bFound)
                                    {
                                        bool bTag = true;
                                        if (ValidateInsertionLocation(oDocRange, oSeg, Forte.MSWord.TagTypes.Variable) == Forte.MSWord.TagInsertionValidityStates.Valid)
                                        {
                                            oDocRange.Select();
                                            if (bPrompt)
                                            {
                                                DialogResult iPrompt = MessageBox.Show("Tag this instance?", "Create Variable", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                                                if (iPrompt == DialogResult.No)
                                                {
                                                    bTag = false;
                                                }
                                                else if (iPrompt == DialogResult.Cancel)
                                                {
                                                    break;
                                                }
                                            }
                                            if (bTag)
                                            {
                                                this.ProgrammaticallySelectingWordTag = true;
                                                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                                                oDocRange.Text = xVarText;
                                                oDocRange.Select();
                                                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                                                    oVar.InsertAssociatedTag();
                                                else
                                                    oVar.InsertAssociatedContentControl();
                                                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                                                this.ProgrammaticallySelectingWordTag = false;
                                                oDocRange.SetRange(oDocRange.End, oDocRange.Document.Range().End);
                                            }
                                        }
                                        bFound = oDocRange.Find.Execute();
                                    }
                                }
                            }
                            else
                            {
                                if (xSelection.ToLower() == xVarText.ToLower())
                                {
                                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                                    {
                                        oVar.AssociatedWordTags[0].Range.Text = xVarText;
                                    }
                                    else
                                    {
                                        oVar.AssociatedContentControls[0].Range.Text = xVarText;
                                    }
                                }
                            }
                        }
                        if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            oRange = oVar.AssociatedWordTags[0].Range;
                        else
                            oRange = oVar.AssociatedContentControls[0].Range;

                        oRange.Text = oVar.DisplayName;
                    }

                    //flag doc as dirty
                    m_bDocIsDirty = true;

                    //get node for new variable
                    UltraTreeNode oNode = treeDocContents.GetNodeByKey(
                        oVar.SegmentName + ".Variables." + oVar.ID);

                    //select node
                    SelectNode(oNode, false);
                    oNode.Expanded = false;
                    oNode.Expanded = true;

                    this.ProgrammaticallySelectingWordTag = false;

                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.VariableException(
                    LMP.Resources.GetLangString("Error_CouldNotAddVariable"), oE);
            }
            finally
            {
                this.ResumeLayout();
                TaskPane.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// returns true iff the specified value is valid
        /// </summary>
        /// <param name="xValue"></param>
        /// <returns></returns>
        private bool IsValidDefaultValue(string xValue)
        {
            return xValue != null && xValue != "\r" && !xValue.Contains("\a");
        }

        /// <summary>
        /// creates block, after validating insertion location and name
        /// </summary>
        private void CreateBlock()
        {

            string xName = null;
            Segment oSeg = null;
            bool bAddContainingSeg = false;
            Word.Range oRange = null;

            try
            {
                TaskPane.ScreenUpdating = false;

                //get selection range
                oRange = m_oMPDocument.WordDocument.Application.Selection.Range;
                //get current edited segment
                oSeg = GetSegmentFromNode(m_oCurNode);

                if (oSeg == null)
                    return;

                //validate selection
                LMP.Forte.MSWord.TagInsertionValidityStates iValid =
                    ValidateInsertionLocation(oRange, oSeg, LMP.Forte.MSWord.TagTypes.Block);

                if (iValid == LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoContainingSegmentExists)
                    bAddContainingSeg = true;
                else if (iValid != LMP.Forte.MSWord.TagInsertionValidityStates.Valid)
                {
                    //location is invalid - alert
                    string xMsg = GetInsertionInvalidityMessage(iValid,
                        ForteDocument.TagTypes.Block);

                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                string xDisplayName;

                //show basic name form and get name for new block
                GetNewVarOrBlockName(oSeg, ForteDocument.TagTypes.Block, out xName, out xDisplayName);

                //xName is null if user cancels name dialog; do not create variable                    
                if (xName != null)
                {
                    //name, display name & execution index are required
                    //for valid variable
                    Block oBlock = oSeg.Blocks.CreateBlock();
                    if (oBlock != null)
                    {
                        oBlock.Name = xName;
                        oBlock.DisplayName = xDisplayName;
                        //GLOG : 8672 : jsw
                        //default ShowInTree = false
                        oBlock.ShowInTree = false;

                        //10.2 (dm) - assign object database id
                        oBlock.ObjectDatabaseID = oSeg.GetNewObjectDatabaseID();

                        if (bAddContainingSeg == true)
                            this.AddContainingmSeg(oSeg, oRange);

                        //create associated Word Tag by default -
                        //set ProgrammaticallySelectingWordTag property to ensure 
                        //m_oCurNode does not get reset by OnXMLSelectionChange event handler
                        
                        this.ProgrammaticallySelectingWordTag = true;
                        LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordEvents = ForteDocument.IgnoreWordXMLEvents;
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            oBlock.InsertAssociatedTag(xName, oBlock.ToString());
                        else
                            oBlock.InsertAssociatedContentControl(xName, oBlock.ToString());
                        ForteDocument.IgnoreWordXMLEvents = bIgnoreWordEvents;

                        //oBlock. .InsertAssociatedTag();
                        oSeg.Blocks.Save(oBlock);

                        //flag doc as dirty
                        m_bDocIsDirty = true;

                        //capture key for later refresh
                        string xKey = m_oCurNode.Key;

                        //refresh tree and select/expand newly added node
                        m_oCurNode = treeDocContents.GetNodeByKey(xKey);

                        //refresh m_oCurNode - it will be the last in the collection
                        m_oCurNode = m_oCurNode.Nodes[m_oCurNode.Nodes.Count - 1];

                        //refresh newly added block node
                        RefreshBlockNode(m_oCurNode, true);
                        m_oCurNode.Expanded = true;
                        m_oCurNode.ExpandAll(ExpandAllType.Always);
                        treeDocContents.ActiveNode = m_oCurNode;
                        //reset property to default value
                        this.ProgrammaticallySelectingWordTag = false;
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.VariableException(
                    LMP.Resources.GetLangString("Error_CouldNotAddBlock"), oE);
            }
            finally
            {
                TaskPane.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// sets value of designated word tag attribute
        /// </summary>
        /// <param name="oXMLNode"></param>
        /// <param name="xAttributeName"></param>
        /// <returns></returns>
        private void SetAttributeValue(Microsoft.Office.Interop.Word.XMLNode oXMLNode,
                                        string xAttributeName, string xAttributeValue)
        {
            LMP.Forte.MSWord.WordDoc.SetAttributeValue(oXMLNode, xAttributeName, xAttributeValue, 
                LMP.Data.Application.EncryptionPassword);
        }
        /// <summary>
        /// returns validity status of designated control
        /// </summary>
        /// <param name="oICtl"></param>
        /// <returns></returns>
        private bool IsValidControlValue(IControl oICtl, UltraTreeNode oValueNode)
        {
            if ((oICtl == null) || (oValueNode == null))
                return true;

            string xPropName = GetPropNameFromKey(oValueNode.Key); 
            string xControlValue = oICtl.Value.ToString();
            string xMsg = null;

            int iErrPos;
            Expression.SyntaxErrors bytErr;

            if (Expression.ContainsPreferenceCode(xControlValue) &&
                xPropName != "DefaultValue")
            {
                //preference field code can be used only as 
                //the default value of a Variable
                MessageBox.Show(
                    LMP.Resources.GetLangString(
                    "Error_PrefCodesOnlyAllowedAsDefaultValue"),
                    LMP.String.MacPacProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            else if (!Expression.SyntaxIsValid(xControlValue, out iErrPos, out bytErr))
            {
                //expression syntax is invalid
                switch (bytErr)
                {
                    case LMP.Architect.Api.Expression.SyntaxErrors.InvalidFieldCodeSyntax:
                        xMsg = LMP.Resources.GetLangString("Error_InvalidFieldCodeSyntax");
                        break;
                    case LMP.Architect.Api.Expression.SyntaxErrors.InvalidOperands:
                        xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionOperands");
                        break;
                    case LMP.Architect.Api.Expression.SyntaxErrors.InvalidOperator:
                        xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionOperator");
                        break;
                    case LMP.Architect.Api.Expression.SyntaxErrors.MissingSpace:
                        xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionMissingSpace");
                        break;
                    case LMP.Architect.Api.Expression.SyntaxErrors.UnmatchedParen:
                        xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionUnmatchedParenthesis");
                        break;
                    case LMP.Architect.Api.Expression.SyntaxErrors.InvalidPreferenceExpression:
                        xMsg = LMP.Resources.GetLangString("Error_InvalidExpressionPreferenceCodesNotAlone");
                        break;
                }

                MessageBox.Show(xMsg, LMP.String.MacPacProductName, 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return false;
            }
            else if (xPropName == "SupportedLanguages")
            {
                if (xControlValue != "")
                {
                    //make sure that default language is supported
                    Segment oSegment = GetSegmentFromNode(oValueNode);
                    string xCulture = oSegment.Culture.ToString();
                    if (xControlValue.IndexOf(xCulture) == -1)
                    {
                        //select first supported language
                        int iPos = xControlValue.IndexOf(LMP.StringArray.mpEndOfValue);
                        if (iPos > -1)
                            xCulture = xControlValue.Substring(0, iPos);
                        else
                            xCulture = xControlValue;
                        oSegment.Culture = System.Int32.Parse(xCulture);
                        SaveSegmentPropertyXML(oSegment, "Culture", xCulture);

                        //update value node
                        string xTagID = oSegment.FullTagID + ".Culture_Value";
                        UltraTreeNode oNode = GetNodeFromTagID(xTagID);
                        oNode.Text = LMP.Data.Application.GetLanguagesDisplayValue(xCulture);
                    }
                    return true;
                }
                else
                {
                    //prompt user to select at least one language
                    xMsg = LMP.Resources.GetLangString("Msg_SupportedLanguages_NoneSelected");
                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
                return true;
        }

        /// <summary>
        /// saves child segments
        /// </summary>
        private void SaveChildSegments()
        {
            //get segments to save
            ChildSegmentsForm oForm = new ChildSegmentsForm();
            oForm.ParentSegment = this.m_oMPDocument.Segments[0];

            //exit if there are no child segments
            if (oForm.ParentSegment.Segments.Count == 0)
                return;

            DialogResult iUserChoice = oForm.ShowDialog();

            this.TaskPane.ScreenRefresh();

            if (iUserChoice == DialogResult.OK && oForm.SelectedSegments.Count > 0)
            {
                this.TaskPane.ScreenUpdating = false;

                Word.Document oDesignDoc = this.m_oMPDocument.WordDocument;

                //save designer file to temp directory
                string xTempDir = System.IO.Path.GetTempPath();
                object oMissing = System.Reflection.Missing.Value;
                object oFileName = xTempDir + @"\ForteDesign" + DateTime.Now.Ticks.ToString() + ".doc";
                object oFalse = (object)false;

                //get current caption for later use
                string xCaption = oDesignDoc.ActiveWindow.Caption;
                //Mark ActiveDocument as TEMP Doc, so that DocumentBeforeSave event will ignore it
                oDesignDoc.ActiveWindow.Caption = "mpTEMPDoc";
                //save segment to a temp file for reuse
                oDesignDoc.SaveAs(ref oFileName, ref oMissing,
                    ref oMissing, ref oMissing, ref oFalse, ref oMissing, ref oFalse,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                //display original document caption
                oDesignDoc.ActiveWindow.Caption = xCaption;

                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //save children specified in form
                SaveSpecifiedChildren(this.m_oMPDocument.Segments[0], oForm.SelectedSegments);

                //return new segment bookmark to original location
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmark(oDesignDoc,
                        this.m_oMPDocument.Segments[0].PrimaryWordTag);
                }
                else
                {
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmark_CC(oDesignDoc,
                        this.m_oMPDocument.Segments[0].PrimaryContentControl);
                }

                //delete added bookmarks
                LMP.Forte.MSWord.WordDoc.DeleteFilterTargetBookmarks(oDesignDoc);

                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
            }

            //repoint module current node variable if necessary -
            //this can happen if InsertSegment runs inside designer
            //since tree will be rebuilt prior to the save operation
            if (m_oCurNode == null)
            {
                m_oCurNode = this.treeDocContents.ActiveNode;
                SelectNode(m_oCurNode,true);
                m_oCurNode.BringIntoView();
                this.SizeAndPositionControl();
            }

            //TODO:  we will need to refresh the content 
            //manager task pane via a pending action
            TaskPane.ContentManager.RefreshTree();
        }

        /// <summary>
        /// saves children of the specified segment 
        /// that have been marked for saving
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private void SaveSpecifiedChildren(Segment oSegment, ArrayList oSelectedSegments)
        {
            //cycle through children
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Segment oChildSegment = oSegment.Segments[i];

                //if child has children, recurse, else save child-
                //this guarantees that the most nested of children
                //are saved first, which is necessary, as unsaved
                //segments require a non-temp ID before the parente
                //segment can be saved
                if (oChildSegment.Segments.Count > 0)
                    SaveSpecifiedChildren(oSegment.Segments[i], oSelectedSegments);

                //cycle through selected segments - 
                //save child segment if found in the collection
                for (int j = 0; j < oSelectedSegments.Count; j++)
                {
                    if (((Segment)oSelectedSegments[j]).FullTagID == oChildSegment.FullTagID)
                    {
                        if (oChildSegment is AdminSegment)
                            SaveAdminChildSegment(oChildSegment);
                        else
                            SaveUserChildSegment(oChildSegment);

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Cycle through list of segments containing the current segment
        /// and update XML with changes
        /// </summary>
        /// <param name="oChildSegment"></param>
        private UpdateListStatus ProcessUpdateList()
        {
            if (m_aUpdateList == null || m_aUpdateList.Count == 0)
                return UpdateListStatus.NothingToDo;

            List<string> aProblemSegments = new List<string>();
            object oMissing = System.Reflection.Missing.Value;
            object oFalse = false;
            bool bCancelUpdate = false;
            bool bScreenUpdating = this.TaskPane.ScreenUpdating;
            this.TaskPane.ScreenUpdating = false;
            try
            {
                for (int i = m_aUpdateList.Count - 1; i >= 0; i--)
                {
                    string xItem = m_aUpdateList[i];
                    //Remove all segments in starting design doc from Update List
                    //This shouldn't occur unless ChildSegmentIDs field has incorrect value
                    if (SegmentHasBeenChanged(xItem))
                        m_aUpdateList.RemoveAt(i);
                }
                if (m_aUpdateList.Count == 0)
                    return UpdateListStatus.NothingToDo;
                //Create separate list of descriptions for Progress List
                List<string> aDescriptions = new List<string>();
                for (int i = 0; i < m_aUpdateList.Count; i++)
                {
                    string xItem = m_aUpdateList[i];
                    int iID1 = 0;
                    int iID2 = 0;
                    LMP.String.SplitID(xItem, out iID1, out iID2);
                    
                    if (iID2 == 0 || iID2 == LMP.Data.ForteConstants.mpFirmRecordID)
                        aDescriptions.Add(LMP.Data.Application.GetSegmentNameFromID(iID1));
                    else
                        aDescriptions.Add(LMP.Data.Application.GetUserSegmentNameFromID(iID1, iID2));
                }
                ProgressList oProgress = new ProgressList();
                oProgress.Setup(LMP.Resources.GetLangString("Dialog_UpdatingSegmentsTitle"), 
                    LMP.Resources.GetLangString("Dialog_UpdatingSegmentsDescription") + GetChangedListText(),
                    LMP.Resources.GetLangString("Dialog_UpdatingSegmentsError"), aDescriptions, 3);
                oProgress.UpdateProgress();
                foreach (string xItem in m_aUpdateList)
                {
                    bool bUpdateError = false;
                    //GLOG 8445: Use ForteNormalDesigner.dotx as base template instead of Normal.dotm
                    string xTemplate = "";
                    string xDesignerTemplate = Data.Application.TemplatesDirectory +
                        @"\ForteNormalDesigner.dotx";
                    if (System.IO.File.Exists(xDesignerTemplate))
                        xTemplate = xDesignerTemplate;
                    else
                        //Use ForteNormal.dotx if separate Designer template doesn't exist
                        xTemplate = LMP.Data.Application.BaseTemplate;

                    //create new hidden doc based on saved design doc
                    Word.Document oTempDoc = LMP.Forte.MSWord.WordApp
                        .CreateDocument(xTemplate, true, false);
                    
                    this.TaskPane.ScreenUpdating = false;
                    m_bDesignUpdateInterrupted = false;

                    oTempDoc.Application.DocumentChange += 
                        new Word.ApplicationEvents4_DocumentChangeEventHandler(HandleDesignerDocumentChange);

                    oTempDoc.ActiveWindow.Caption = "mpTEMPDoc";
                    ForteDocument oDesignDoc = new ForteDocument(oTempDoc);
                    oDesignDoc.Mode = ForteDocument.Modes.Design;

                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                    try
                    {
                        oDesignDoc.InsertSegmentForDesign(xItem);
                        oProgress.UpdateProgress();
                        Segment oSegment = oDesignDoc.Segments[0];
                        try
                        {
                            object oBMK = @"mpNewSegment";
                            //Need to delete the New Segment bookmark so that Segment.Insert
                            //routines will be able to locate newly inserted segment
                            oDesignDoc.WordDocument.Bookmarks.get_Item(ref oBMK).Delete();
                        }
                        catch { }
                        bool bChanged = UpdateExistingSegments(oSegment, oDesignDoc);
                        oProgress.UpdateProgress();
                        //save segment definition if changes occurred
                        if (bChanged)
                        {
                            //add TagExpandedValue field codes -
                            //10.2 (dm) - skip this for now when using content controls -
                            //it's not yet clear whether this will ultimately be necessary
                            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                                oSegment.RefreshExpandedTagValueCodes(true);

                            //execute actions of variables with static default values
                            this.RefreshStaticDefaults(oSegment);

                            //12-17-10 (dm)
                            oSegment.ForteDocument.Tags.AddBookmarks();

                            //ensure that new segment bookmark is present
                            if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            {
                                LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmarkIfNecessary(
                                    oDesignDoc.WordDocument, oSegment.PrimaryWordTag);
                            }
                            else
                            {
                                LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmarkIfNecessary_CC(
                                    oDesignDoc.WordDocument, oSegment.PrimaryContentControl);
                            }

                            //save
                            if (!m_bDesignUpdateInterrupted)
                                SaveSegmentDefinition(oSegment, false, oDesignDoc);
                            else
                                bCancelUpdate = true;
                        }
                    }
                    catch (LMP.Exceptions.SegmentException)
                    {
                        //Mark as problem
                        bUpdateError = true;
                    }
                    catch (System.Exception oE)
                    {
                        bCancelUpdate = true;
                    }
                    finally
                    {
                        //Unsubscribe from temporary DocumentChange event
                        if (Session.CurrentWordVersion != 11)
                            oTempDoc.Application.DocumentChange -= HandleDesignerDocumentChange;
                    }
                    oTempDoc.Saved = true;
                    //close the document without saving changes
                    oTempDoc.Close(ref oFalse, ref oMissing, ref oMissing);

                    if (bCancelUpdate)
                    {
                        //GLOG 2787: Focus change or other event interrupted update - stop processing list
                        oProgress.Hide();
                        System.Windows.Forms.Application.DoEvents();
                        break;
                    }
                    else if (bUpdateError)
                        //Error occurred saving individual segment
                        oProgress.UpdateProgress(ProgressList.StatusTypes.Failed);
                    else
                        oProgress.UpdateProgress(ProgressList.StatusTypes.Succeeded);
                }
            }
            catch (System.Exception oE)
            {
                bCancelUpdate = true;
            }
            finally
            {
                this.TaskPane.ScreenUpdating = bScreenUpdating;
            }
            if (bCancelUpdate)
                return UpdateListStatus.Failed;
            else
                return UpdateListStatus.Succeeded;
        }

        /// <summary>
        /// Event subscribed to temporarily during updating of related segment design
        /// </summary>
        void HandleDesignerDocumentChange()
        {
            m_bDesignUpdateInterrupted = true;
        }

        /// <summary>
        /// Cycle through segments in design doc
        /// and replace those that are in the list of changed segments
        /// </summary>
        /// <param name="oStartSegment"></param>
        /// <param name="oMPDocument"></param>
        /// <returns></returns>
        private bool UpdateExistingSegments(Segment oStartSegment, ForteDocument oMPDocument)
        {
            bool bUpdateRun = false;
            //Check if top segment is in list of Changed Segments
            if (SegmentHasBeenChanged(oStartSegment.ID))
            {
                oMPDocument.UpdateSegmentDesign(oStartSegment.ID, oStartSegment.Parent);
                oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes);
                bUpdateRun =  true;
            }
            else
            {
                // If Top level has not changed, search children
                for (int i = 0; i < oStartSegment.Segments.Count; i++)
                {
                    bUpdateRun = bUpdateRun || UpdateExistingSegments(oStartSegment.Segments[i], oMPDocument);
                }
            }
            return bUpdateRun;
        }
        /// <summary>
        /// True if segment was changed in design and selected to be saved
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool SegmentHasBeenChanged(string xSegmentID)
        {
            if (m_aChangedSegments.Contains(xSegmentID))
                return true;
            else
                return false;
        }
        /// <summary>
        /// saves the specified child segment
        /// </summary>
        /// <param name="oChildSegment"></param>
        private void SaveAdminChildSegment(Segment oChildSegment)
        {
            object oMissing = System.Reflection.Missing.Value;
            object oFalse = false;
            bool bIsFirstSave = false;
            int iID = 0;
            try
            {
                AdminSegmentDefs oDefs = new AdminSegmentDefs();

                //if the segment is currently unsaved, specify to
                //create a folder member for this segment after save
                bIsFirstSave = oChildSegment.ID1 == Segment.UNSAVED_ID;

                if (bIsFirstSave)
                {
                    //create segment def record
                    oDefs.Save((LongIDSimpleDataItem)oChildSegment.Definition);
                    iID = ((LongIDSimpleDataItem)oChildSegment.Definition).ID;

                    //write new ID back to mSEG object data
                    oChildSegment.Nodes.SetItemObjectDataValue(
                        oChildSegment.FullTagID, "SegmentID", iID.ToString());

                    //save ID to appropriate segment properties
                    oChildSegment.ID1 = iID;
                    oChildSegment.ID2 = 0;
                }

                string xParentTagID = "";
                try
                {
                    //clear out parent tag id, if one exists - 
                    //ignore an error - statement will err if
                    //no parent tag id key is found
                    xParentTagID = oChildSegment.Nodes.GetItemObjectDataValue(
                        oChildSegment.FullTagID, "ParentTagID");
                    if (xParentTagID != "")
                    {
                        oChildSegment.Nodes.SetItemObjectDataValue(
                            oChildSegment.FullTagID, "ParentTagID", "");
                    }
                }
                catch { }

                Word.Document oDesignDoc = this.m_oMPDocument.WordDocument;
                string xBookmarkRoot = null;

                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    //get tags in the saved document that have the tag id of segment
                    Word.XMLNode[] aTags = (Word.XMLNode[])oChildSegment.WordTags;

                    //ensure that new segment bookmark is present in segment
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmark(oDesignDoc, oChildSegment.PrimaryWordTag);

                    //encapsulate target tags with bookmarks
                    xBookmarkRoot = LMP.Forte.MSWord.WordDoc.BookmarkTags(aTags, oChildSegment.ID);
                }
                else
                {
                    //get content controls in the saved document that have the tag id of segment
                    Word.ContentControl[] aCCs = (Word.ContentControl[])oChildSegment.ContentControls;

                    //ensure that new segment bookmark is present in segment
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmark_CC(oDesignDoc, oChildSegment.PrimaryContentControl);

                    //encapsulate target tags with bookmarks
                    xBookmarkRoot = LMP.Forte.MSWord.WordDoc.BookmarkContentControls(aCCs, oChildSegment.ID);

                    //add segment id to tags if necessary
                    if (bIsFirstSave)
                    {
                        object oArray = aCCs;
                        LMP.Forte.MSWord.WordDoc.UpdateTagsWithSegmentID(ref oArray, iID.ToString(), oDesignDoc);
                    }
                }

                oDesignDoc.Save();

                //create new hidden doc based on saved design doc
                TaskPane.PendingAction = new PendingAction(PendingAction.Types.SetDesignMode,
                    this.m_oMPDocument.Segments[0].ID, "");
                Word.Document oTempDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                    oDesignDoc.FullName.ToString(), true, false);

                oTempDoc.ActiveWindow.Caption = "mpTEMPDoc";

                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //filter the document to contain
                //only the tags in the collection
                string xSegmentXML = null;
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    //xml tags
                    xSegmentXML = LMP.Forte.MSWord.WordDoc.Filter(oTempDoc, xBookmarkRoot);
                else
                    //content controls
                    xSegmentXML = LMP.Forte.MSWord.WordDoc.Filter_CC(oTempDoc, xBookmarkRoot);

                ForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;

                //remove authors xml
                xSegmentXML = RemoveAuthorsXML(xSegmentXML);

                //prepend <mAuthorPrefs> code if the segment contains author preferences-
                //this tag allows us to display in the Author Preferences Manager only
                //those segments that contain author prefs
                //GLOG 4578 (dm) - changed DefinitionContainsAuthorPreferences from a
                //property to a method
                if (Segment.DefinitionContainsAuthorPreferences(xSegmentXML))
                    xSegmentXML = "<mAuthorPrefs>" + xSegmentXML;

                //GLOG 4307: Create AdminSegmentDef object and populate with segment properties to update DB record
                AdminSegmentDef oDef = (AdminSegmentDef)oChildSegment.Definition;

                //populate definition from segment properties
                FillSegmentDef(oChildSegment, xSegmentXML, oDef);

                oDefs.Save(oDef);

                oTempDoc.Saved = true;

                //close the document without saving changes
                oTempDoc.Close(ref oFalse, ref oMissing, ref oMissing);

                //restore parent tag id
                if (xParentTagID != "")
                {
                    oChildSegment.Nodes.SetItemObjectDataValue(
                        oChildSegment.FullTagID, "ParentTagID", xParentTagID);
                }

                if (bIsFirstSave)
                {
                    FolderSelectionForm oForm = new FolderSelectionForm(
                        "Select a location for '" + oChildSegment.DisplayName + "'.", false, false);

                    oForm.ShowDialog();
                    string xID = oForm.SelectedFolderID;

                    Folders oFolders = new Folders();

                    //create new folder member in current folder, i.e. the parent folder of 
                    //the current Designer - this will create necessary assignments for newly
                    //created child segment
                    Folder oFolder = (Folder)oFolders.ItemFromID(int.Parse(xID));
                    FolderMember oMember = (FolderMember)(oFolder.GetMembers().Create());
                    oMember.ObjectID1 = iID;
                    oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);
                }
                else
                {
                    //Get any segments containing this segment
                    AppendToUpdateList(oChildSegment.ID);
                    m_aChangedSegments.Add(oChildSegment.ID);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentDefinitionException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChildSegment") +
                    oChildSegment.DisplayName, oE);
            }
        }

        /// <summary>
        /// removes authors xml from the specified XML - 
        /// necessary as a patch, as somehow author detail
        /// is getting inserted into design - once we
        /// figure out and remove the source of the input
        /// we can remove this method
        /// </summary>
        /// <param name="xXML"></param>
        private string RemoveAuthorsXML(string xXML)
        {
            //GLOG 6523: Match additional patterns in wich Authors XML may be found
            string xPattern = "Authors=\"¬Authors&amp;gt;.*?¬/Authors&amp;gt;\"|Authors=\"&amp;lt;Authors&amp;gt;.*?&amp;lt;/Authors&amp;gt;\"" +
                    "|Authors=&amp;lt;Authors&amp;gt;.*?&amp;lt;/Authors&amp;gt;|Authors=&lt;Authors&gt;.*?&lt;/Authors&gt;" +
                    "|Authors=\"&lt;Authors&gt;.*?&lt;/Authors&gt;\"";
            return Regex.Replace(xXML, xPattern, "");
        }
        /// <summary>
        /// saves the specified child segment
        /// </summary>
        /// <param name="oChildSegment"></param>
        private void SaveUserChildSegment(Segment oChildSegment)
        {
            object oMissing = System.Reflection.Missing.Value;
            object oFalse = false;
            bool bIsFirstSave = false;
            string xID = "0.0";
            try
            {
                UserSegmentDefs oDefs = new UserSegmentDefs(
                    mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);

                //if the segment is currently unsaved, specify to
                //create a folder member for this segment after save
                bIsFirstSave = oChildSegment.ID1 == Segment.UNSAVED_ID;

                if (bIsFirstSave)
                {
                    //create segment def record
                    oDefs.Save((StringIDSimpleDataItem)oChildSegment.Definition);
                    xID = ((StringIDSimpleDataItem)oChildSegment.Definition).ID;

                    int iID1;
                    int iID2;

                    LMP.Data.Application.SplitID(xID, out iID1, out iID2);

                    //write new ID back to mSEG object data
                    oChildSegment.Nodes.SetItemObjectDataValue(
                        oChildSegment.FullTagID, "SegmentID", xID);

                    //save ID to appropriate segment properties
                    oChildSegment.ID1 = iID1;
                    oChildSegment.ID2 = iID2;
                }

                string xParentTagID = "";
                try
                {
                    //clear out parent tag id, if one exists - 
                    //ignore an error - statement will err if
                    //no parent tag id key is found
                    xParentTagID = oChildSegment.Nodes.GetItemObjectDataValue(
                        oChildSegment.FullTagID, "ParentTagID");
                    if (xParentTagID != "")
                    {
                        oChildSegment.Nodes.SetItemObjectDataValue(
                            oChildSegment.FullTagID, "ParentTagID", "");
                    }
                }
                catch { }

                Word.Document oDesignDoc = this.m_oMPDocument.WordDocument;
                string xBookmarkRoot = null;

                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    //get tags in the saved document that have the tag id of segment
                    Word.XMLNode[] aTags = (Word.XMLNode[])oChildSegment.WordTags;

                    //ensure that new segment bookmark is present in segment
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmark(oDesignDoc, oChildSegment.PrimaryWordTag);

                    //encapsulate target tags with bookmarks
                    xBookmarkRoot = LMP.Forte.MSWord.WordDoc.BookmarkTags(aTags, oChildSegment.ID);
                    }
                else
                {
                    //get content controls in the saved document that have the tag id of segment
                    Word.ContentControl[] aCCs = (Word.ContentControl[])oChildSegment.ContentControls;

                    //ensure that new segment bookmark is present in segment
                    LMP.Forte.MSWord.WordDoc.InsertNewSegmentBookmark_CC(oDesignDoc, oChildSegment.PrimaryContentControl);

                    //encapsulate target tags with bookmarks
                    xBookmarkRoot = LMP.Forte.MSWord.WordDoc.BookmarkContentControls(aCCs, oChildSegment.ID);

                    //add segment id to tags if necessary
                    if (bIsFirstSave)
                    {
                        object oArray = aCCs;
                        LMP.Forte.MSWord.WordDoc.UpdateTagsWithSegmentID(ref oArray, xID, oDesignDoc);
                    }
                }

                oDesignDoc.Save();

                //create new hidden doc based on saved design doc
                TaskPane.PendingAction = new PendingAction(PendingAction.Types.SetDesignMode,
                    this.m_oMPDocument.Segments[0].ID, "");
                Word.Document oTempDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                    oDesignDoc.FullName.ToString(), true, false);

                oTempDoc.ActiveWindow.Caption = "mpTEMPDoc";

                LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //filter the document to contain
                //only the tags in the collection
                string xSegmentXML = null;
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    //xml tags
                    xSegmentXML = LMP.Forte.MSWord.WordDoc.Filter(oTempDoc, xBookmarkRoot);
                else
                    //content controls
                    xSegmentXML = LMP.Forte.MSWord.WordDoc.Filter_CC(oTempDoc, xBookmarkRoot);

                ForteDocument.IgnoreWordXMLEvents = bIgnoreEvents;

                //remove authors xml
                xSegmentXML = RemoveAuthorsXML(xSegmentXML);

                //GLOG 4307: Create UserSegmentDef object and populate with segment properties to update DB record
                UserSegmentDef oDef = (UserSegmentDef)oChildSegment.Definition;

                //populate definition from segment properties
                FillSegmentDef(oChildSegment, xSegmentXML, oDef);

                oDefs.Save(oDef);

                oTempDoc.Saved = true;

                //close the document without saving changes
                oTempDoc.Close(ref oFalse, ref oMissing, ref oMissing);

                //restore parent tag id
                if (xParentTagID != "")
                {
                    oChildSegment.Nodes.SetItemObjectDataValue(
                        oChildSegment.FullTagID, "ParentTagID", xParentTagID);
                }

                if (bIsFirstSave)
                {
                    //create new folder member in current folder, i.e. the parent folder of 
                    //the current Designer - this will create necessary assignments for newly
                    //created child segment
                    UserFolder oFolder = (UserFolder)TaskPane.ContentManager.ActiveFolder;
                    
                    FolderMember oMember = (FolderMember)(oFolder.GetMembers().Create());
                    oMember.ObjectID1 = oChildSegment.ID1;
                    oMember.ObjectID2 = oChildSegment.ID2;
                    oMember.ObjectTypeID = mpFolderMemberTypes.UserSegment;
                    oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);
                }
                else
                {
                    //Get any segments containing this segment
                    AppendToUpdateList(oChildSegment.ID);
                    m_aChangedSegments.Add(oChildSegment.ID);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentDefinitionException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChildSegment") +
                    oChildSegment.DisplayName, oE);
            }
        }

        /// <summary>
        /// creates new child segment, either of containing segment of selection
        /// or of top level segment if selection is uncontained, i.e., in header/footer ranges
        /// </summary>
        private void CreateNewChildSegment()
        {
            try
            {
                //validate selection range
                string xMsg = null;
                Word.Range oRange = Session.CurrentWordApp.Selection.Range;
                LMP.Forte.MSWord.TagTypes iType = LMP.Forte.MSWord.TagTypes.Segment;
                Segment oCurSeg = GetSegmentFromNode(this.treeDocContents.ActiveNode);

                //validate selection range prior to insertion
                LMP.Forte.MSWord.TagInsertionValidityStates iValidityState =
                    ValidateInsertionLocation(oRange, oCurSeg, iType);

                if (iValidityState != LMP.Forte.MSWord.TagInsertionValidityStates.Valid)
                {
                    //insertion is invalid - alert
                    xMsg = GetInsertionInvalidityMessage(iValidityState,
                        ForteDocument.TagTypes.Segment);

                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                //Selection okay - Display Segment Creator form - pass in current segment
                DesignerSegmentPropertyForm oForm = new DesignerSegmentPropertyForm(oCurSeg);
                
                DialogResult iRet = oForm.ShowDialog();

                this.TaskPane.ScreenUpdating = true;
                this.TaskPane.ScreenRefresh();
                this.TaskPane.ScreenUpdating = false;

                if (iRet == DialogResult.OK)
                {
                    string xTagID = oCurSeg.FullTagID + "." + oForm.SegmentName + "_1";
                    Segment oSeg = null;

                    if (oCurSeg is AdminSegment)
                    {
                        //create a new definition
                        AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.Architect);
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.Create();
                        oDef.Name = oForm.SegmentName;
                        oDef.DisplayName = oForm.SegmentDisplayName;
                        oDef.IntendedUse = mpSegmentIntendedUses.AsDocumentComponent;

                        //add mSeg to selected range
                        InsertChildmSEG(oRange, oDef, oForm.SegmentName + "_1");

                        //create a segment w/ temp ID of 0 and admin def created above
                        oSeg = m_oMPDocument.GetSegment(xTagID, m_oMPDocument);

                        oSeg.Definition = oDef;
                    }
                    else
                    {
                        //create a new definition
                        UserSegmentDefs oDefs = 
                            new UserSegmentDefs(mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);

                        UserSegmentDef oDef = (UserSegmentDef)oDefs.Create();
                        oDef.Name = oForm.SegmentName;
                        oDef.DisplayName = oForm.SegmentDisplayName;

                        if (oCurSeg.IntendedUse == mpSegmentIntendedUses.AsDocument)
                            oDef.IntendedUse = mpSegmentIntendedUses.AsDocumentComponent;
                        else
                            oDef.IntendedUse = mpSegmentIntendedUses.AsParagraphText;

                        //add mSeg to selected range
                        InsertChildmSEG(oRange, oDef, oForm.SegmentName + "_1");

                        //create a segment w/ temp ID of 0 and admin def created above
                        oSeg = m_oMPDocument.GetSegment(xTagID, m_oMPDocument);

                        oSeg.Definition = oDef;
                    }

                    //add segment to collection
                    oCurSeg.Segments.Add(oSeg);

                    //select the new word tag - this will refresh the tree
                    //and select its node
                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        SelectNode(oSeg.PrimaryWordTag);
                    else
                        SelectNode(oSeg.PrimaryContentControl);
                }
            }
            catch (System.Exception oE)
            {
                this.TaskPane.ScreenUpdating = true;
                this.TaskPane.ScreenRefresh();

                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Create"), oE);
            }
            finally
            {
                if (!this.TaskPane.ScreenUpdating)
                {
                    this.TaskPane.ScreenUpdating = true;
                    this.TaskPane.ScreenRefresh();
                }
            }
        }

        /// <summary>
        /// returns the appropriate message for the specified insertion invalidity state
        /// </summary>
        /// <param name="iValidityState"></param>
        /// <returns></returns>
        internal string GetInsertionInvalidityMessage(LMP.Forte.MSWord.TagInsertionValidityStates iValidityState,
            LMP.Architect.Api.ForteDocument.TagTypes iTagType)
        {
            switch (iValidityState)
            {
                case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidContainsPartialTag:
                    //selection contains only first or last tag marker of a contained tag
                    return LMP.Resources.GetLangString("Msg_InvalidContainsPartialTag");
                case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoContainingSegmentExists:
                    //selection is physically outside the story's existing segment tags
                    return LMP.Resources.GetLangString("Msg_InvalidVariableOrSegmentLocation");
                case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoParentTag:
                    //this return value is not invalid if selection contains top level segment
                    return LMP.Resources.GetLangString("Msg_InvalidSelectionContainsTopLevelSegment");
                case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNotInEditedSegment:
                    //the selection containing seg is different than the one selected in the tree
                    return LMP.Resources.GetLangString("Msg_RangeSegmentNotEditSegment");
                case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidOutsideStorySegment:
                    //selection is physically outside the story's existing segment tags
                    return LMP.Resources.GetLangString("Msg_InvalidVariableOrSegmentLocation");
                case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidParentTag:
                    {
                        //selection is nested inside mVar or msubVar
                        if (iTagType == ForteDocument.TagTypes.Segment)
                            return LMP.Resources.GetLangString("Msg_InvalidSegmentParentTag");
                        else if (iTagType == ForteDocument.TagTypes.Block)
                            return LMP.Resources.GetLangString("Msg_InvalidBlockParentTag");
                        else if (iTagType == ForteDocument.TagTypes.Variable)
                            return LMP.Resources.GetLangString("Msg_InvalidVariableParentTag");
                        else
                            return "";
                    }
                case LMP.Forte.MSWord.TagInsertionValidityStates.InvalidSpansParagraphMarker:
                    //selection is partial paragraph and spans paragraph marker
                    return LMP.Resources.GetLangString("Msg_RangeSpansParagraphMarker");
                case LMP.Forte.MSWord.TagInsertionValidityStates.Valid:
                    return "";
                default:
                    return LMP.Resources.GetLangString("Msg_InvalidTagInsertionLocation");
            }
        }
        /// <summary>
        /// deletes designated segment and all children
        /// </summary>
        /// <param name="oTagToDelete"></param>
        private void DeleteSegment(Segment oSeg)
        {
            try
            {
                this.TaskPane.ScreenUpdating = false;

                string xMsg = LMP.Resources.GetLangString(
                    "Prompt_DeleteSegmentAndAllContainedChildren");

                //insert segment display name into message
                xMsg = xMsg.Replace("{0}", oSeg.DisplayName);

                DialogResult oRes = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                if (oRes == DialogResult.OK)
                {
                    this.TaskPane.ScreenUpdating = false;
                    m_oMPDocument.DeleteSegment(oSeg);
                }
            }
            catch (System.Exception oE)
            {
                this.TaskPane.ScreenUpdating = true;
                this.TaskPane.Refresh();

                throw new LMP.Exceptions.ServerException(
                    LMP.Resources.GetLangString("Error_CouldNotDeleteSegment"), oE);
            }
            finally
            {
                if (!this.TaskPane.ScreenUpdating)
                {
                    this.TaskPane.ScreenUpdating = true;
                    this.TaskPane.Refresh();
                }
            }
        }

        internal void ConvertToLanguageSupport()
        {
            //convert top level segment
            ConvertToLanguageSupport(this.m_oMPDocument.Segments[0]);
        }
        private void ConvertToLanguageSupport(Segment oSeg)
        {
            for (int i = 0; i < oSeg.Segments.Count; i++)
            {
                Segment oChild = oSeg.Segments[i];
                ConvertToLanguageSupport(oChild);
            }

            //look for legacy language variable
            Variable oLanguageVar = null;

            try
            {
                oLanguageVar = oSeg.Variables.ItemFromName("Language");
            }
            catch { }

            //if found
            if (oLanguageVar != null)
            {
                //for each variable action
                //redefine as a language action
                for (int i = 0; i < oLanguageVar.VariableActions.Count; i++)
                {
                    VariableAction oVA = oLanguageVar.VariableActions[i];

                    string[] aParams = oVA.Parameters.Split('‡');

                    Variable oTargetVar = null;
                    try
                    {
                        oTargetVar = oSeg.Variables.ItemFromName(aParams[0]);
                    }
                    catch { }

                    bool bIsGenericSetValueAction = oVA.Type == VariableActions.Types.SetVariableValue &&
                        (oVA.ExecutionCondition == "") && (oVA.LookupListID == 0) && (oTargetVar != null) &&
                        (oTargetVar.DefaultValue == "");

                    if (bIsGenericSetValueAction)
                    {
                        //a language action is not needed -
                        //set the default value of the target variable to the value of action expression
                        oTargetVar.DefaultValue = aParams[1];
                    }
                    else if (oVA.Type != VariableActions.Types.RunVariableActions)
                    {
                        //create the language action

                        SegmentAction oSA = oSeg.Actions.Create();
                        oSA.Event = Segment.Events.AfterLanguageUpdated;
                        oSA.Type = oVA.Type;
                        oSA.ExecutionCondition = oVA.ExecutionCondition;
                        oSA.Parameters = oVA.Parameters;
                        oSeg.Actions.Save();
                    }
                }

                //convert all instances of [Variable_Language] to [SegmentLanguageName] -
                string xObjData = null;

                //update all variable object data
                for (int i = 0; i < oSeg.Variables.Count; i++)
                {
                    Variable oVar = oSeg.Variables[i];
                    xObjData = oSeg.Nodes.GetItemObjectData(oVar.TagID);
                    xObjData = xObjData.Replace("Variable_Language", "SegmentLanguageName");
                    xObjData = xObjData.Replace("Variable__Language", "SegmentLanguageName");
                    oSeg.Nodes.SetItemObjectData(oVar.TagID, xObjData);
                }

                //update segment object data
                xObjData = oSeg.Nodes.GetItemObjectData(oSeg.FullTagID);
                xObjData = xObjData.Replace("Variable_Language", "SegmentLanguageName");
                xObjData = xObjData.Replace("Variable__Language", "SegmentLanguageName");
                oSeg.Nodes.SetItemObjectData(oSeg.FullTagID, xObjData);


                oSeg.Refresh();
                this.RefreshTree();
            }
        }
        // GLOG : 2989 : JAB
        // Draw the dropdown arrow next to the node.
        private void DisplayContextMenuButton(UltraTreeNode oNode)
        {
            this.picMenuDropdown.Visible = false;
            int iMargin = IsTreeContentVerticalScrollbarVisible ? 17 : 2;

            int iMaxLeft = this.treeDocContents.Right - iMargin - this.picMenuDropdown.Width;
            this.picMenuDropdown.Left = oNode.Bounds.X + oNode.Bounds.Width + 4;

            if (picMenuDropdown.Left > iMaxLeft)
            {
                picMenuDropdown.Left = iMaxLeft;
            }
            this.picMenuDropdown.Top = oNode.Bounds.Y + 22;
            this.picMenuDropdown.Visible = IsContextMenuArrowNeeded(oNode);
        }

        /// <summary>
        /// GLOG : 2989 : JAB
        /// A context menu exists for only for some nodes. Determine if the
        /// node specified requires a context menu drop down arrow based on
        /// if the node has a context menu.
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private bool IsContextMenuArrowNeeded(UltraTreeNode oNode)
        {
            NodeTypes iType = NodeType(oNode);
            switch (iType)
            {
                case NodeTypes.SegmentActionProperty:
                    return false;
                    break;

                case NodeTypes.Segment:
                case NodeTypes.SegmentAction:
                case NodeTypes.SegmentEventsCategory:
                case NodeTypes.VariablesCategory:
                case NodeTypes.Variable:
                case NodeTypes.VariableActionsCategory:
                case NodeTypes.VariableAction:
                case NodeTypes.ControlActionsCategory:
                case NodeTypes.SegmentAuthorsControlActionsCategory:
                case NodeTypes.SegmentJurisdictionControlActionsCategory:
                case NodeTypes.LanguageControlActionsCategory:
                case NodeTypes.ControlAction:
                case NodeTypes.SegmentAuthorsControlAction:
                case NodeTypes.SegmentJurisdictionControlAction:
                case NodeTypes.LanguageControlAction:
                case NodeTypes.BlocksCategory:
                case NodeTypes.Block:
                case NodeTypes.AnswerFileSegmentsCategory:
                case NodeTypes.AnswerFileSegment:
                case NodeTypes.LanguageActionsCategory:
                case NodeTypes.AuthorsActionsCategory:
                    return true;
                    break;

                default:
                    return false;
                    break;
            }
        }

        /// <summary>
        /// GLOG : 2989 : JAB
        /// Get the last node that is exposed in the treeDocContent control. This
        /// is needed to determine if the treeDocContent's vertical scrollbar is 
        /// visible.
        /// </summary>
        /// <param name="treeContent"></param>
        /// <returns></returns>
        private UltraTreeNode GetLastExposedNode(UltraTree treeContent)
        {
            UltraTreeNode oLastExposedNode = null;

            if (treeContent.Nodes.Count > 0)
            {
                oLastExposedNode = this.treeDocContents.Nodes[this.treeDocContents.Nodes.Count - 1];

                while (oLastExposedNode.HasNodes && oLastExposedNode.Expanded)
                {
                    oLastExposedNode = oLastExposedNode.Nodes[oLastExposedNode.Nodes.Count - 1];
                }
            }

            return oLastExposedNode;
        }

        /// <summary>
        /// GLOG : 2989 : JAB
        /// Hide the designer menu picture control if the node is not
        /// visible and display it scrolling is complete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicMenuScroll()
        {
            if (this.treeDocContents.SelectedNodes.Count > 0)
            {
                if (!this.treeDocContents.SelectedNodes[0].IsInView)
                {
                    this.picMenuDropdown.Visible = false;
                }
                else
                {
                    if (MouseButtons == MouseButtons.None)
                    {
                        DisplayContextMenuButton(this.treeDocContents.SelectedNodes[0]);
                    }
                    else
                    {
                        this.picMenuDropdown.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// GLOG : 2989 : JAB
        /// Make the Designer Menu Picture control visible once the mouse is released.
        /// This picture control was made not visible to handle cases where the user is
        /// scrolling. If the picture control were visible it would be located outside
        /// of the treeContent control during scrolling.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicMenuVisible()
        {
            if (this.treeDocContents.SelectedNodes.Count > 0)
            {
                if (!this.treeDocContents.SelectedNodes[0].IsInView)
                {
                    this.picMenuDropdown.Visible = false;
                }
            }
        }
        /// <summary>
        /// True if a context menu is displayed
        /// </summary>
        /// <returns></returns>
        internal bool MenuIsVisible()
        {
            //GLOG 3721
            return (mnuAnswerFileSegments.Visible || mnuBlocks.Visible || 
                mnuSegments.Visible || mnuVariables.Visible);
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// executes code that must be run
        /// when the user selects a Word tag
        /// </summary>
        /// <param name="Sel"></param>
        /// <param name="OldXMLNode"></param>
        /// <param name="NewXMLNode"></param>
        /// <param name="Reason"></param>
        internal void OnXMLSelectionChange(Word.XMLNode OldXMLNode, Word.XMLNode NewXMLNode)
        {
            try
            {
                if (this.ProgrammaticallySelectingWordTag)
                {
                    //the selection change occurred because a 
                    //tree node was selected on the designer
                    //tab - don't execute the method, which
                    //is designed to select the appropriate tree node
                    return;
                }

                if ((NewXMLNode != null) &&
                    LMP.Forte.MSWord.WordDoc.ContainsInvalidlyNestedTags(NewXMLNode))
                {
                    NewXMLNode.Range.Select();
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_InvalidlyNestedTags"),
                        LMP.String.MacPacProductName, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }

                //exit invalid control value - user has been alerted
                //return focus to that control
                if (this.IsValidControlValue((IControl)m_oCurCtl, m_oCurNode) == false)
                {
                    this.treeDocContents.ActiveNode = m_oCurNode;
                    this.pnlDocContents.Focus();
                    m_oCurCtl.Focus();
                }
                else
                {
                    //select the variables tree node whose
                    //selection tag is the current Word tag
                    if (OldXMLNode != null)
                        SelectNode(NewXMLNode);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void OnContentControlExit(Word.ContentControl oCC, ref bool Cancel,
            bool bAdvanceExecution)
        {
            try
            {
                if (this.ProgrammaticallySelectingWordTag)
                {
                    //the selection change occurred because a 
                    //tree node was selected on the designer
                    //tab - don't execute the method, which
                    //is designed to select the appropriate tree node
                    return;
                }

                //handle only macpac content controls
                //GLOG 7969: Tag may not exist for native content control
                if (oCC.Tag == null || !oCC.Tag.ToString().StartsWith("mp"))
                    return;

                //exit invalid control value - user has been alerted
                //return focus to that control
                if (this.IsValidControlValue((IControl)m_oCurCtl, m_oCurNode) == false)
                {
                    this.treeDocContents.ActiveNode = m_oCurNode;
                    this.pnlDocContents.Focus();
                    m_oCurCtl.Focus();
                    //Cancel exit event
                    Cancel = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void OnContentControlEnter(Word.ContentControl oCC)
        {
            try
            {
                if (this.ProgrammaticallySelectingWordTag)
                {
                    //the selection change occurred because a 
                    //tree node was selected on the designer
                    //tab - don't execute the method, which
                    //is designed to select the appropriate tree node
                    return;
                }

                //handle only macpac content controls
                //GLOG 7969: Tag may not exist for native content control
                if (oCC.Tag == null || !oCC.Tag.ToString().StartsWith("mp"))
                    return;

                //ensure that the handler code runs only when
                //the passed content control is the control that
                //was actually entered
                Word.ContentControl oSelectionCC = oCC.Application
                    .Selection.Range.ParentContentControl;

                if (oSelectionCC != null && oSelectionCC.Tag == oCC.Tag)
                {
                    SelectNode(oCC);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Ultra Tree event handler
        /// sizes and positions visible controls on Task Pane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_Scroll(object sender, Infragistics.Win.UltraWinTree.TreeScrollEventArgs e)
		{
			try
			{
                if (this.m_oCurNode != null)
                    //move control with associated node
                    this.SizeAndPositionControl();

                // GLOG : 2989 : JAB
                // Handle scroll with respect to the drop down arrow.
                PicMenuScroll();
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		} 
        /// <summary>
        /// Ultra Tree event handler
        /// sizes and positions visible controls on Task Pane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_AfterExpand(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
        {
            try
            {
                if (this.IgnoreTreeViewEvents == true)
                    return;

                this.TaskPane.ScreenUpdating = false;

                if (this.m_oCurNode != null)
                    //move control with associated node
                    this.SizeAndPositionControl();
            }
            catch (System.Exception oE)
            {
                this.TaskPane.ScreenUpdating = true;
                LMP.Error.Show(oE);
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// Ultra Tree event handler
        /// assigns current node, ExitEditMode
        /// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void treeDocContents_BeforeCollapse(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
		{
			try
			{
                if (this.IgnoreTreeViewEvents == true)
                    return;
                
				if(e.TreeNode == this.m_oCurNode)
					return;

				if(e.TreeNode.IsAncestorOf(this.m_oCurNode))
				{
					//exit edit mode
					this.ExitEditMode();
				}
			}
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Ultra Tree event handler
        /// refreshes all node levels in Tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_BeforeExpand(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
		{
			try
			{
                if (this.IgnoreTreeViewEvents == true)
                    return;
                
				Segment oSeg = e.TreeNode.Tag as Segment;
                Variable oVar = e.TreeNode.Tag as Variable;
                Block oBlock = e.TreeNode.Tag as Block;
                AnswerFileSegment oAnswerFile = e.TreeNode.Tag as AnswerFileSegment;

                if (oSeg != null && !e.TreeNode.HasNodes)
					this.RefreshSegmentNode(e.TreeNode, true);

                if (oVar != null && !e.TreeNode.HasNodes)
					this.RefreshVariableNode(e.TreeNode, true);

                if (oBlock != null && !e.TreeNode.HasNodes)
                    this.RefreshBlockNode(e.TreeNode, true);

                if (oAnswerFile != null && !e.TreeNode.HasNodes)
                    this.RefreshAnswerFileSegmentNode(e.TreeNode, true);
            }
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
		/// <summary>
        /// Ultra Tree event handler
        /// sizes and positions visible controls on Task Pane
        /// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void treeDocContents_AfterCollapse(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
		{
			try
			{
                if (this.IgnoreTreeViewEvents == true)
                    return;

                if (this.m_oCurNode != null)
                    //move control with associated node
                    this.SizeAndPositionControl();
            }
			catch(System.Exception oE)
			{
				LMP.Error.Show(oE);
			}
		}
        /// <summary>
        /// Displays context menu on right-click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///
        private void treeDocContents_MouseDown(object sender, MouseEventArgs e)
        {
            m_oDraggedNode = null;
            m_oLastPointOnScreen = new Point(e.X, e.Y);
            m_oRectForDrag = Rectangle.Empty;
            UltraTreeNode oNode = this.treeDocContents.GetNodeFromPoint(e.X, e.Y);
            if (oNode == null) return;

            try
            {
                //call SelectNode function, which in turn will ensure
                //treeDocContents_BeforeSelect event code will fire
                //this will ensure property and tree node control updating
                //will take place if users leave an open control and right click on a node
                if ((e.Button & MouseButtons.Right) == MouseButtons.Right)
                {

                    //first set parameter to 
                    //suppress node collapse code when right clicking
                    //nodes that display menus - prevents "orphaned" menus

                    bool bSuppressExpansionToggle = false;
                    NodeTypes iType = NodeType(oNode);
                    switch (iType)
                    {
                        case NodeTypes.Segment:
                        case NodeTypes.SegmentAction:
                        case NodeTypes.SegmentEventsCategory:
                        case NodeTypes.VariablesCategory:
                        case NodeTypes.Variable:
                        case NodeTypes.VariableActionsCategory:
                        case NodeTypes.VariableAction:
                        case NodeTypes.ControlActionsCategory:
                        case NodeTypes.SegmentAuthorsControlActionsCategory:
                        case NodeTypes.SegmentJurisdictionControlActionsCategory:
                        case NodeTypes.LanguageControlActionsCategory:
                        case NodeTypes.ControlAction:
                        case NodeTypes.SegmentAuthorsControlAction:
                        case NodeTypes.SegmentJurisdictionControlAction:
                        case NodeTypes.LanguageControlAction:
                        case NodeTypes.BlocksCategory:
                        case NodeTypes.Block:
                        case NodeTypes.AnswerFileSegmentsCategory:
                        case NodeTypes.LanguageActionsCategory:
                        case NodeTypes.AuthorsActionsCategory:
                        case NodeTypes.LanguageAction:
                        case NodeTypes.AuthorsAction:
                            bSuppressExpansionToggle = true;
                            break;
                        default:
                            break;
                    }

                    //select the clicked node
                    this.SelectNode(oNode, bSuppressExpansionToggle, false);
                    m_oCurNode = oNode;
                    DisplayContextMenu(m_oCurNode, e.X, e.Y);
                }
                else if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                {
                    Size dragSize = SystemInformation.DragSize;
                    m_oRectForDrag = new Rectangle(new Point(e.X
                        - dragSize.Width / 2, e.Y
                        - dragSize.Height / 2), dragSize);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Check if drag action has been cancelled by Esc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            try
            {
                //Did the user press escape? 
                if (e.EscapePressed)
                {
                    //User pressed escape
                    //Cancel the Drag
                    e.Action = DragAction.Cancel;
                    m_oDraggedNode = null;
                    m_oRectForDrag = Rectangle.Empty;
                }
                // Keep track of where on screen the mouse is positioned
                m_oLastPointOnScreen.X = Control.MousePosition.X;
                m_oLastPointOnScreen.Y = Control.MousePosition.Y;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Mouse has been moved over UltraWinTree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                //Mouse has moved beyond minimum boundaries required to start drag
                if (m_oRectForDrag != Rectangle.Empty && !m_oRectForDrag.Contains(e.X, e.Y))
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        oNode = this.treeDocContents.SelectedNodes[0];
                        //oNode = m_oCurNode;
                    }
                    catch
                    {
                        //There may not be a selected node 
                    }
                    finally
                    {
                        //Start a DragDrop operation for Variable Node
                        if (NodeType(oNode) == NodeTypes.Variable)
                        {
                            CollapseDesignatedTreeNodes(oNode);
                            m_oCurNode = null;
                            m_oDraggedNode = oNode;

                            this.treeDocContents.DoDragDrop(m_oDraggedNode,
                                DragDropEffects.All | DragDropEffects.Move);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Mouse button has been released
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_MouseUp(object sender, MouseEventArgs e)
        {
            //reset Drag boundaries
            m_oRectForDrag = Rectangle.Empty;

            // GLOG : 2989 : JAB
            // On mouse up show the drop down arrow as needed.
            PicMenuVisible();
        }

        /// <summary>
        /// Update mouse pointer to reflect available actions when
        /// dragging over individual nodes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                //The Point that the mouse cursor is on, in Tree coords. 
                //This event passes X and Y in form coords. 
                System.Drawing.Point PointInTree;

                //Get the position of the mouse in the tree, as opposed
                //to form coords
                PointInTree = this.treeDocContents.PointToClient(new Point(e.X, e.Y));

                m_oMouseLocation = PointInTree;

                //Get the node the mouse is over.
                UltraTreeNode oNode = GetNearestNodeFromPoint(ref PointInTree);

                if (oNode == null || oNode == m_oDraggedNode || oNode.Parent != m_oDraggedNode.Parent)
                {
                    //Can only drop Variable with same Segment node
                    e.Effect = DragDropEffects.None;
                }
                else
                {
                    e.Effect = DragDropEffects.Move;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            //Get the position of the mouse in the tree, as opposed
            //to form coords
            Point oPointInTree = this.m_oMouseLocation;

            //Get the node the mouse is over.
            UltraTreeNode oNode = GetNearestNodeFromPoint(ref oPointInTree);


            if (m_oDraggedNode != oNode)
            {
                if (this.m_oDraggedNode == null || oNode == null)
                {
                    e.UseDefaultCursors = true;
                    Cursor.Current = Cursors.Default;
                }
                else
                {
                    if (e.Effect == DragDropEffects.Move)
                    {
                        if (IsUpperNodeRegion(oPointInTree, oNode) && !IsLowerNeighbor(oNode))
                        {
                            e.UseDefaultCursors = false;
                            Cursor.Current = DocumentDesigner.BetweenCursor;
                            return;
                        }

                        if (IsLowerNodeRegion(oPointInTree, oNode) && !IsUpperNeighbor(oNode))
                        {
                            e.UseDefaultCursors = false;
                            Cursor.Current = DocumentDesigner.BetweenCursor;
                            return;
                        }

                        e.UseDefaultCursors = true;
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }

        private UltraTreeNode GetNearestNodeFromPoint(ref Point oPointInTree)
        {
            UltraTreeNode oNode = this.treeDocContents.GetNodeFromPoint(oPointInTree);

            if (oNode == null)
            {
                // Check for a node just above this point.
                oPointInTree.Y -= NeighboringNodeOffset;
                oNode = this.treeDocContents.GetNodeFromPoint(oPointInTree);

                if (IsUpperNeighbor(oNode))
                {
                    // Return null if the nearest node to the point just above the
                    // current mouse location is the node just above the dragged
                    // node.
                    return null;
                }

                if (IsTopLevelNode(oNode))
                {
                    // Also return null if this is a neighboring folder since if the mouse location is 
                    // not directly over the folder node it would imply that we want to move 
                    // the dragged folder next to this folder when it is already a neighbor
                    // of this node.

                    return null;
                }

                if (oNode == null)
                {
                    // Check for a node just below this point. Since we've 
                    // decremented the Y value, we'll need to add the offset
                    // twice.

                    oPointInTree.Y += (2 * NeighboringNodeOffset);
                    oNode = this.treeDocContents.GetNodeFromPoint(oPointInTree);

                    if (IsLowerNeighbor(oNode))
                    {
                        // Return null if the nearest node to the point just below the
                        // current mouse location is the node just below the dragged
                        // node.
                        return null;
                    }

                    if (IsTopLevelNode(oNode))
                    {
                        // If the nearest node is a top level folder node, return 
                        // null since we should only be able to insert folders into these 
                        // folders and not be able to make this folder as a peer of a top
                        // folder node. 
                        //
                        // Also return null if this is a neighboring folder since if the mouse location is 
                        // not directly over the folder node it would imply that we want to move 
                        // the dragged folder next to this folder when it is already a neighbor
                        // of this node.

                        return null;
                    }
                }
            }

            return oNode;
        }
        /// <summary>
        /// True if Node is at top level of the tree ('My Folders' or 'Public Folders' node)
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private bool IsTopLevelNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            return (oNode.Level == 0);
        }

        private int NeighboringNodeOffset
        {
            get
            {
                if (m_iNeighboringNodeOffset == 0)
                {
                    // Calculate the m_iNeighboringNodeOffset based on a
                    // percentage of the height of the folder. This will hopefully
                    // account for situations where the folder size varies.

                    if (this.treeDocContents.Nodes.Count > 0)
                    {
                        this.m_iNeighboringNodeOffset = (int)(this.treeDocContents.Nodes[0].Bounds.Height * 0.50);
                    }
                }

                return this.m_iNeighboringNodeOffset;
            }
        }
        private NodeDropLocation GetNodeDropLocation(Point oPointInTree, UltraTreeNode oNode)
        {
            if (IsTopLevelNode(oNode))
            {
                return NodeDropLocation.Inside;
            }

            if (IsUpperNodeRegion(oPointInTree, oNode) && !IsLowerNeighbor(oNode))
            {
                return NodeDropLocation.Above;
            }

            if (IsLowerNodeRegion(oPointInTree, oNode) && !IsUpperNeighbor(oNode))
            {
                return NodeDropLocation.Below;
            }

            return NodeDropLocation.Inside;
        }

        private bool IsLowerNeighbor(UltraTreeNode oNode)
        {
            UltraTreeNode oNeighborNode = this.m_oDraggedNode.GetSibling(NodePosition.Next);
            return (oNeighborNode == oNode);
        }

        private bool IsUpperNeighbor(UltraTreeNode oNode)
        {
            UltraTreeNode oNeighborNode = this.m_oDraggedNode.GetSibling(NodePosition.Previous);
            return (oNeighborNode == oNode);
        }

        private bool IsLowerNodeRegion(Point oPointInTree, UltraTreeNode oNode)
        {
            int iLowerRegionHeight = (int)(oNode.Bounds.Height * LOWER_NODE_REGION_COEFFICIENT);
            int iLowerRegionBoundary = oNode.Bounds.Bottom - iLowerRegionHeight;
            return oPointInTree.Y <= oNode.Bounds.Bottom &&
                    oPointInTree.Y >= iLowerRegionBoundary;
        }

        private bool IsUpperNodeRegion(Point oPointInTree, UltraTreeNode oNode)
        {
            int iUpperRegionHeight = (int)(oNode.Bounds.Height * UPPER_NODE_REGION_COEFFICIENT);
            int iUpperRegionBoundary = oNode.Bounds.Top + iUpperRegionHeight;
            return oPointInTree.Y >= oNode.Bounds.Top &&
                oPointInTree.Y <= iUpperRegionBoundary;
        }

        private bool IsMiddleNodeRegion(Point oPointInTree, UltraTreeNode oNode)
        {
            return oPointInTree.Y >= oNode.Bounds.Top &&
                    oPointInTree.Y <= oNode.Bounds.Bottom &&
                    !IsUpperNodeRegion(oPointInTree, oNode) &&
                    !IsLowerNodeRegion(oPointInTree, oNode);
        }

        /// <summary>
        /// handles screen updating after node selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_BeforeActivate(object sender, CancelableNodeEventArgs e)
        {
            try
            {
                if (this.IgnoreTreeViewEvents)
                    return;
                
                //exit if the new selection is the current selection 
                if (m_oCurNode != null && m_oCurNode == e.TreeNode)
                    return;

                this.SelectNode(e.TreeNode, true, true);
                if (this.InvalidControlValue)
                {
                    e.Cancel = true;
                    this.InvalidControlValue = false;
                }
                this.TaskPane.ScreenUpdating = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        } 
        /// <summary>
		/// handles case when tab or shift-tab have been pressed-
		/// executes only when the ProcessDialogKey doesn't execute,
		/// as the key will get processed by the control, which raises
		/// the event that causes this handler to execute
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void oIControl_TabPressed(object sender, TabPressedEventArgs e)
		{
            try
            {
                if (e.ShiftPressed)
                    //Shift-Tab was pressed - select previous node
                    this.SelectPreviousNode();
                else
                    this.SelectNextNode(); 
            }
             catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
       }
        /// <summary>
        /// configures Add Segment Actions dropdown menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_Add_DropDownOpened(object sender, EventArgs e)
        {
            try
            {
                //set calling menu bit - used to determine whether segment or variable action
                //will be created later
                m_iAddActionsMenuCaller = AddActionsMenuCaller.Segment;
            }
             catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
       }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_Add_DropDownClosed(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// runs actions base move up method of actions collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_MoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                MoveSegmentActionsNode(TreeNodeMoveDirections.Up);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// moves segment action down in collection belonging to specified event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_MoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                MoveSegmentActionsNode(TreeNodeMoveDirections.Down); 
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// deletes specified segment action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                //propmpt user before deletion
                string xMsg = LMP.Resources.GetLangString("Prompt_DeleteSegmentAction");
                DialogResult oDR = MessageBox.Show( xMsg, LMP.String.MacPacProductName, 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (oDR == DialogResult.No)
                    return;

                Segment oSeg = GetSegmentFromNode(m_oCurNode);
                SegmentAction oSegAction = GetSegmentActionFromNode(m_oCurNode);
                //delete method takes index argument, which should be execution
                //index - 1.  Segment actions execution indices are contained in ToArray item 0
                oSeg.Actions.Delete(Int32.Parse((oSegAction.ToArray()[0])) - 1);

                //capture current node parent key
                string xKey = m_oCurNode.Parent.Key;

                //refresh actions nodes
                if ((m_oCurNode.Parent.Text == "Language Actions") ||
                    (m_oCurNode.Parent.Text == "Authors Actions"))
                {
                    //add directly to root node
                    this.AddSegmentEventNodes(oSeg, m_oCurNode.Parent);
                }
                else
                    this.AddSegmentEventNodes(oSeg, m_oCurNode.Parent.Parent);

                UltraTreeNode oCatNode = this.treeDocContents.GetNodeByKey(xKey);
                oCatNode.Expanded = true;
                //repoint active node and module level tree node variable
                m_oCurNode = oCatNode;
                this.treeDocContents.ActiveNode = oCatNode;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// closes menu when mouse leaves
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                if (mnuVariables_AddAction.Visible)
                {
                    //Don't close if mouse is over dropdown
                    if (PointIsWithinContextMenu(Form.MousePosition.X, Form.MousePosition.Y,
                        mnuVariables_AddAction))
                        return;
                }
                //GLOG 6305 (dm) - remmed the following line to prevent freezing
                //mnuSegments.Close(ToolStripDropDownCloseReason.CloseCalled);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handle checkboxes do not dismiss menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            //TODO:  for use with checkboxes - cancel menu dismissal if checkbox clicked
        }
        /// <summary>
        /// configures segment menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_Opening(object sender, CancelEventArgs e)
        {
            NodeTypes iType = (NodeType(m_oCurNode));
            try
            {
                switch (iType)
                {
                    case NodeTypes.Segment:
                        Segment oSegment = GetSegmentFromNode(m_oCurNode);
                        if (oSegment.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                            e.Cancel = true;

                        //CreateChildSegments pick only available to Admin Segments
                        mnuSegments_CreateNewChildSegment.Visible = 
                            (oSegment is AdminSegment || m_bIsContentDesignerSegment);
                        mnuSegments_Add.Visible = false;
                        mnuSegments_Delete.Visible = false;
                        mnuSegments_MoveUp.Visible = false;
                        mnuSegments_MoveDown.Visible = false;
                        mnuSegments_Copy2.Visible = false;
                        mnuSegments_Paste2.Visible = false;
                        mnuSegments_Paste2.Enabled = false;
                        mnuSegments_Separator1.Visible = false;
                        mnuSegments_Separator2.Visible = false;
                        mnuSegments_Rename.Visible = false;
                        mnuSegments_Refresh.Visible = false;

                        //Assignments menu pick available only to 
                        //AdminSegments if .ShowChooser = true
                        Segment oSeg = GetSegmentFromNode(m_oCurNode);
                        if (oSeg is AdminSegment && !m_bIsContentDesignerSegment)  //GLOG 6148: Not valid for Designer Segments, since Assignments table only accepts integer IDs
                        {
                            //mnuSegments_Assignments.Visible =
                            //    oSeg.ShowChooser == true && oSeg.Parent != null;
                            //mnuSegments_Assignments.Visible = oSeg.ShowChooser == true;
                            //GLOG : 3870 : JSW
                            mnuSegments_Assignments.Visible = (oSeg.ShowChooser == true && oSeg.IsTopLevel == false);

                            mnuSegments_ParentAssignments.Visible = oSeg.IsTopLevel &&
                                (oSeg.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent);
                            mnuSegments_TrailerAssignments.Visible = oSeg.IsTopLevel &&
                                (oSeg.IntendedUse == mpSegmentIntendedUses.AsDocument || oSeg is Paper);
                            mnuSegments_DraftStampAssignments.Visible = oSeg.IsTopLevel &&
                                (oSeg.IntendedUse == mpSegmentIntendedUses.AsDocument || oSeg is Paper);
                        }
                        else
                        {
                            mnuSegments_Assignments.Visible = false;
                            mnuSegments_TrailerAssignments.Visible = false;
                            mnuSegments_DraftStampAssignments.Visible = false;
                            mnuSegments_ParentAssignments.Visible = false;
                        }
                        //do not allow deletion pick for top level segments
                        mnuSegments_DeleteSegment.Visible = (oSeg.IsTopLevel == false);
                        break;
                    case NodeTypes.SegmentEventsCategory:
                        mnuSegments_Add.Visible = true;
                        mnuSegments_Add.Text = LMP.Resources.GetLangString(
                            "Menu_DesignerSegmentsAdd");
                        mnuSegments_Delete.Visible = false;
                        mnuSegments_DeleteSegment.Visible = false;
                        mnuSegments_CreateNewChildSegment.Visible = false;
                        mnuSegments_MoveUp.Visible = false;
                        mnuSegments_MoveDown.Visible = false;
                        mnuSegments_Copy2.Visible = false;
                        mnuSegments_Paste2.Visible = false;
                        mnuSegments_Paste2.Enabled = false;
                        mnuSegments_Separator1.Visible = false;
                        mnuSegments_Separator2.Visible = false;
                        mnuSegments_Rename.Visible = false;
                        mnuSegments_Refresh.Visible = false;
                        mnuSegments_Assignments.Visible = false;
                        mnuSegments_TrailerAssignments.Visible = false;
                        mnuSegments_DraftStampAssignments.Visible = false;
                        break;
                    case NodeTypes.LanguageActionsCategory:
                    case NodeTypes.AuthorsActionsCategory:
                        mnuSegments_Add.Visible = true;
                        mnuSegments_Add.Text = LMP.Resources.GetLangString(
                            "Menu_DesignerVariablesAdd");
                        mnuSegments_Delete.Visible = false;
                        mnuSegments_DeleteSegment.Visible = false;
                        mnuSegments_CreateNewChildSegment.Visible = false;
                        mnuSegments_MoveUp.Visible = false;
                        mnuSegments_MoveDown.Visible = false;
                        mnuSegments_Copy2.Visible = false;
                        mnuSegments_Paste2.Visible = false;
                        mnuSegments_Paste2.Enabled = false;
                        mnuSegments_Separator1.Visible = false;
                        mnuSegments_Separator2.Visible = false;
                        mnuSegments_Rename.Visible = false;
                        mnuSegments_Refresh.Visible = false;
                        mnuSegments_Assignments.Visible = false;
                        //GLOG : 3870 : JSW
                        mnuSegments_ParentAssignments.Visible = false;
                        mnuSegments_TrailerAssignments.Visible = false;
                        mnuSegments_DraftStampAssignments.Visible = false;
                        break;
                    case NodeTypes.SegmentAction:
                    case NodeTypes.LanguageAction:
                    case NodeTypes.AuthorsAction:
                        mnuSegments_Add.Visible = false;
                        mnuSegments_Delete.Visible = true;
                        mnuSegments_DeleteSegment.Visible = false;
                        mnuSegments_CreateNewChildSegment.Visible = false;
                        mnuSegments_MoveUp.Visible = true;
                        mnuSegments_MoveDown.Visible = true;
                        mnuSegments_Copy2.Visible = false;
                        mnuSegments_Paste2.Visible = false;
                        mnuSegments_Separator1.Visible = false;
                        mnuSegments_Separator2.Visible = false;
                        mnuSegments_Rename.Visible = false;
                        mnuSegments_Refresh.Visible = false;
                        mnuSegments_Assignments.Visible = false;
                        //GLOG : 3870 : JSW
                        mnuSegments_ParentAssignments.Visible = false;
                        mnuSegments_TrailerAssignments.Visible = false;
                        mnuSegments_DraftStampAssignments.Visible = false;
                        break;
                    default:
                        break;
                }
            }
            
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// closes menu when mouse leaves
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVariables_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                if (mnuVariables_AddAction.Visible)
                {
                    //Don't close if mouse is over New Segment dropdown
                    if (PointIsWithinContextMenu(Form.MousePosition.X, Form.MousePosition.Y,
                        mnuVariables_AddAction))
                        return;
                }
                else if (mnuVariables_AddControlAction.Visible)
                {
                    //Don't close if mouse is over New Segment dropdown
                    if (PointIsWithinContextMenu(Form.MousePosition.X, Form.MousePosition.Y,
                        mnuVariables_AddControlAction))
                        return;
                }
                //GLOG 6305 (dm) - remmed the following line to prevent freezing
                //mnuVariables.Close(ToolStripDropDownCloseReason.CloseCalled);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuVariables_Add_DropDownOpened(object sender, EventArgs e)
        {
            try
            {
                //set calling menu bit - used to determine whether segment or variable action
                //will be created later
                if (mnuVariables_Add.DropDown == mnuVariables_AddControlAction)
                {
                    if (NodeType(m_oCurNode) == NodeTypes.SegmentAuthorsControlActionsCategory)
                        m_iAddActionsMenuCaller = AddActionsMenuCaller.AuthorsControlAction;
                    else if (NodeType(m_oCurNode) == NodeTypes.SegmentJurisdictionControlActionsCategory)
                        m_iAddActionsMenuCaller = AddActionsMenuCaller.JurisdictionControlAction;
                    else if (NodeType(m_oCurNode) == NodeTypes.LanguageControlActionsCategory)
                        m_iAddActionsMenuCaller = AddActionsMenuCaller.LanguageControlAction;
                    else
                        m_iAddActionsMenuCaller = AddActionsMenuCaller.ControlAction;
                }
                else
                    m_iAddActionsMenuCaller = AddActionsMenuCaller.Variable;
            }
            
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuVariables_AddAction_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                if (!PointIsWithinContextMenu(Form.MousePosition.X, Form.MousePosition.Y, (ContextMenuStrip)sender))
                {
                    //this submenu is shared by Variables and Segments top level menu
                    //you can just close them both -- or you could check
                    //m_iAddActionsMenuCaller bit for caller of dropdown
                    //GLOG 6305 (dm) - remmed this code to prevent freezing
                    //mnuVariables.Close(ToolStripDropDownCloseReason.CloseCalled);
                    //mnuSegments.Close(ToolStripDropDownCloseReason.CloseCalled);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// adds variable without associated Word tag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVariables_AddTaglessVariable_Click(object sender, EventArgs e)
        {
            try
            {
                CreateVariable(false);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// configure variable actions add menu for variables or segment events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void mnuVariables_AddAction_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if ((NodeType(m_oCurNode) == NodeTypes.SegmentEventsCategory) ||
                    (NodeType(m_oCurNode) == NodeTypes.LanguageActionsCategory) ||
                    (NodeType(m_oCurNode) == NodeTypes.AuthorsActionsCategory))
                {
                    //we're showing only segment actions
                    mnuVariables_AddAction_IncludeExcludeText.Visible = false;
                    mnuVariables_AddAction_InsertCheckbox.Visible = false;
                    mnuVariables_AddAction_InsertDate.Visible = false;
                    mnuVariables_AddAction_InsertText.Visible = false;
                    mnuVariables_AddAction_InsertTextAsTable.Visible = false;
                    mnuVariables_AddAction_UpdateMemoTypePeople.Visible = false;
                    mnuVariables_AddAction_InsertDetail.Visible = false;
                    mnuVariables_AddAction_InsertDetailIntoTable.Visible = false;
                    mnuVariables_AddAction_SetupLabelTable.Visible = false;
                    mnuVariables_AddAction_InsertTOA.Visible = false;
                    mnuVariables_AddAction_InsertReline.Visible = false;
                    mnuVariables_AddAction_InsertSegment.Visible = false;
                    mnuVariables_AddAction_ExecuteOnTag.Visible = false;
                    mnuVariables_AddAction_ExecuteOnBlock.Visible = true;
                    mnuVariables_AddAction_IncludeExcludeBlocks.Visible = false;
                    mnuVariables_AddAction_ReplaceSegment.Visible = true;
                    mnuVariables_AddAction_RunMacro.Visible = true;
                    mnuVariables_AddAction_RunMethod.Visible = true;
                    mnuVariables_AddAction_RunVariableActions.Visible = 
                        (NodeType(m_oCurNode) == NodeTypes.LanguageActionsCategory) ||
                        (NodeType(m_oCurNode) == NodeTypes.AuthorsActionsCategory);
                    mnuVariables_AddAction_SetAsDefaultValue.Visible = true;
                    mnuVariables_AddAction_SetDocPropValue.Visible = true; //GLOG 7495
                    mnuVariables_AddAction_SetDocVarValue.Visible = true;
                    mnuVariables_AddAction_SetupDistributedDetailTable.Visible = false;
                    mnuVariables_AddAction_SetupDistributedSegmentSections.Visible = false;
                    mnuVariables_AddAction_SetVariableValue.Visible = true;
                    //GLOG 3220
                    mnuVariables_AddAction_ApplyStyleSheet.Visible = false;
                }
                else
                {
                    //we're showing variable actions
                    bool bIsTaglessVariable = false;
                    Variable oVar = this.m_oCurNode.Parent.Tag as LMP.Architect.Api.Variable;

                    if (oVar != null)
                        bIsTaglessVariable = oVar.IsTagless;

                    //show actions - some actions are available only for tagged variables
                    mnuVariables_AddAction_IncludeExcludeText.Visible = !bIsTaglessVariable;
                    mnuVariables_AddAction_InsertCheckbox.Visible = !bIsTaglessVariable;
                    mnuVariables_AddAction_InsertDate.Visible = !bIsTaglessVariable;
                    mnuVariables_AddAction_InsertText.Visible = !bIsTaglessVariable;
                    mnuVariables_AddAction_InsertTextAsTable.Visible = false; ;
                    mnuVariables_AddAction_UpdateMemoTypePeople.Visible = false;
                    mnuVariables_AddAction_InsertDetail.Visible = !bIsTaglessVariable;
                    mnuVariables_AddAction_InsertDetailIntoTable.Visible = false;
                    mnuVariables_AddAction_InsertReline.Visible = !bIsTaglessVariable;
                    mnuVariables_AddAction_SetupLabelTable.Visible = true;
                    mnuVariables_AddAction_InsertTOA.Visible = false;
                    mnuVariables_AddAction_InsertSegment.Visible = !bIsTaglessVariable;
                    mnuVariables_AddAction_ExecuteOnBlock.Visible = true;
                    mnuVariables_AddAction_ExecuteOnTag.Visible = true;
                    mnuVariables_AddAction_IncludeExcludeBlocks.Visible = true;
                    mnuVariables_AddAction_ReplaceSegment.Visible = true;
                    mnuVariables_AddAction_RunMacro.Visible = true;
                    mnuVariables_AddAction_RunMethod.Visible = true;
                    mnuVariables_AddAction_RunVariableActions.Visible = true;
                    mnuVariables_AddAction_SetAsDefaultValue.Visible = true;
                    mnuVariables_AddAction_SetDocPropValue.Visible = true; //GLOG 7495
                    mnuVariables_AddAction_SetDocVarValue.Visible = true;
                    mnuVariables_AddAction_SetupDistributedDetailTable.Visible = true;
                    mnuVariables_AddAction_SetupDistributedSegmentSections.Visible = true;
                    mnuVariables_AddAction_SetVariableValue.Visible = true;
                    //GLOG 3220
                    mnuVariables_AddAction_ApplyStyleSheet.Visible = true;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuVariables_Delete_Click(object sender, EventArgs e)
        {
            Segment oSeg = GetSegmentFromNode(m_oCurNode);
            Variable oVar = GetVariableFromNode(m_oCurNode);
            ControlAction oCtlAction = GetControlActionFromNode(m_oCurNode);
            VariableAction oVarAction = GetVariableActionFromNode(m_oCurNode);

            //capture current node parent key
            string xKey = m_oCurNode.Parent.Key;

            try
            {
                if (oCtlAction != null)
                {
                    //propmpt user before deletion
                    string xMsg = LMP.Resources.GetLangString("Prompt_DeleteControlAction");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.No)
                        return;

                    //process either variable control actions or segment authors control action
                    if (NodeType(m_oCurNode) == NodeTypes.ControlAction)
                    {

                        //delete method takes index argument, which should be execution
                        //index - 1.  Segment actions execution indices are contained in ToArray item 0
                        oVar.ControlActions.Delete(Int32.Parse((oCtlAction.ToArray()[0])) - 1);

                        //save change to XML
                        oSeg.Variables.Save(oVar);
                        
                        //refresh actions nodes
                        this.AddControlActionNodes(m_oCurNode.Parent, oVar);

                    }
                    else if (NodeType(m_oCurNode) == NodeTypes.SegmentJurisdictionControlAction)
                    {
                        oSeg.CourtChooserControlActions.Delete(Int32.Parse((oCtlAction.ToArray()[0])) - 1);

                        //save change to XML
                        SaveSegmentJurisdictionControlActions(oSeg);

                        //refresh actions nodes
                        this.AddSegmentControlActionNodes(oSeg.CourtChooserControlActions,
                            m_oCurNode.Parent); 
                    }
                    else if (NodeType(m_oCurNode) == NodeTypes.LanguageControlAction)
                    {
                        oSeg.LanguageControlActions.Delete(Int32.Parse((oCtlAction.ToArray()[0])) - 1);

                        //save change to XML
                        SaveSegmentLanguageControlActions(oSeg);

                        //refresh actions nodes
                        this.AddSegmentControlActionNodes(oSeg.LanguageControlActions,
                            m_oCurNode.Parent);
                    }
                    else
                    {
                        oSeg.Authors.ControlActions.Delete(Int32.Parse((oCtlAction.ToArray()[0])) - 1);

                        //save change to XML
                        SaveSegmentAuthorsControlActions(oSeg); 
                        
                        //refresh actions nodes
                        this.AddSegmentControlActionNodes(oSeg.Authors.ControlActions,
                            m_oCurNode.Parent); 
                    }

                    UltraTreeNode oCatNode = this.treeDocContents.GetNodeByKey(xKey);
                    oCatNode.Expanded = true;
                    //repoint active node and module level tree node variable
                    m_oCurNode = oCatNode;
                    this.treeDocContents.ActiveNode = oCatNode;
                }
                else if (oVarAction != null)
                {
                    //propmpt user before deletion
                    string xMsg = LMP.Resources.GetLangString("Prompt_DeleteVariableAction");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oDR == DialogResult.No)
                        return;

                    //delete method takes index argument, which should be execution
                    //index - 1.  Segment actions execution indices are contained in ToArray item 0
                    oVar.VariableActions.Delete(Int32.Parse((oVarAction.ToArray()[0])) - 1);

                    //save change to XML
                    oSeg.Variables.Save(oVar);


                    //refresh actions nodes
                    this.AddVariableActionNodes(m_oCurNode.Parent, oVar);

                    UltraTreeNode oCatNode = this.treeDocContents.GetNodeByKey(xKey);
                    oCatNode.Expanded = true;
                    //repoint active node and module level tree node variable
                    m_oCurNode = oCatNode;
                    this.treeDocContents.ActiveNode = oCatNode;
                }
                else
                {
                    //we're deleting a variable -
                    //prompt user before deletion
                    string xMsg = LMP.Resources.GetLangString("Prompt_DeleteVariable");
                    DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (oDR == DialogResult.Yes)
                    {
                        this.ProgrammaticallySelectingWordTag = true;
                        //delete variable plus all text contained within its tag
                        oSeg.Variables.Delete(oVar, true);
                        this.ProgrammaticallySelectingWordTag = false;
                    }
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// moves either variable or variable action up depending on node type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVariables_MoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                MoveVariablesOrActionsNode(TreeNodeMoveDirections.Up);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// moves either variable or variable action down depending on node type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVariables_MoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                MoveVariablesOrActionsNode(TreeNodeMoveDirections.Down);  
          
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// event fired by all variable action items in the add submenu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVariableActions_OnVAClick(object sender, EventArgs e)
        {
            string xName = null;
            try
            {
                //VariableAction or ControlAction name stored in menu item tag
                xName = (((ToolStripMenuItem)sender).Tag).ToString();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

            //add the designated variable or segment action
            //depending on calling menu

            try
            {
                switch (m_iAddActionsMenuCaller)
                {
                    case AddActionsMenuCaller.None:
                        break;
                    case AddActionsMenuCaller.Variable:
                        AddVariableActions(xName);
                        break;
                    case AddActionsMenuCaller.Segment:
                        AddSegmentActions(xName);
                        break;
                    case AddActionsMenuCaller.ControlAction:
                        AddControlActions(xName);
                        break;
                    case AddActionsMenuCaller.AuthorsControlAction:
                        AddSegmentAuthorsControlActions(xName);
                        break;
                    case AddActionsMenuCaller.JurisdictionControlAction:
                        AddSegmentJurisdictionControlActions(xName);
                        break;
                    case AddActionsMenuCaller.LanguageControlAction:
                        AddSegmentLanguageControlActions(xName);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
            
        }
        /// <summary>
        /// sets up right click variables menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVariables_Opening(object sender, CancelEventArgs e)
        {
            //TODO:  add SegmentAuthorsControlEvents types
            //you will have to rework NodeType to differentiate from
            //the two
            NodeTypes iType = (NodeType(m_oCurNode));
            Segment oSeg = GetSegmentFromNode(m_oCurNode);
            Variable oVar = GetVariableFromNode(m_oCurNode);
            try
            {
                switch (iType)
                {
                    case NodeTypes.VariablesCategory:
                        mnuVariables_Add.Visible = false;
                        mnuVariables_AddVariable.Visible = true;
                        //tagless variables are available only to content designers
                        mnuVariables_AddTaglessVariable.Visible = 
                            (oSeg is AdminSegment || m_bIsContentDesignerSegment) &&
                            (oSeg.IntendedUse != mpSegmentIntendedUses.AsAnswerFile &&
                             oSeg.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm);
                        mnuVariables_AddAssociatedWordTag.Visible = false;
                        mnuVariables_Delete.Visible = false;
                        mnuVariables_DeleteAll.Visible = false;
                        mnuVariables_MoveUp.Visible = false;
                        mnuVariables_MoveDown.Visible = false;
                        mnuVariables_Copy.Visible = false;
                        mnuVariables_Paste.Visible = false;
                        mnuVariables_Separator1.Visible = false;
                        mnuVariables_Separator2.Visible = false;
                        mnuVariables_Refresh.Visible = (oSeg.IntendedUse != mpSegmentIntendedUses.AsAnswerFile &&
                            oSeg.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm);
                        mnuVariables_Rename.Visible = false;
                        break;
                    case NodeTypes.Variable:
                        mnuVariables_Add.Visible = false;
                        mnuVariables_AddVariable.Visible = false;
                        mnuVariables_AddTaglessVariable.Visible = false;

                        //show add associated word tag item only if word tag count == 0
                        //some functionality is available only to admin variables
                        if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        {
                            mnuVariables_AddAssociatedWordTag.Visible =
                                oVar.AssociatedWordTags.GetUpperBound(0) > -1;
                        }
                        else
                        {
                            mnuVariables_AddAssociatedWordTag.Visible =
                                oVar.AssociatedContentControls.GetUpperBound(0) > -1;
                        }

                        mnuVariables_Delete.Visible = true;
                        mnuVariables_DeleteAll.Visible = false;
                        //move up move down only if variables count > 1
                        oSeg = GetSegmentFromNode(m_oCurNode);
                        mnuVariables_MoveUp.Visible = oSeg.Variables.Count > 1 && 
                            oVar.ExecutionIndex > 1;
                        mnuVariables_MoveDown.Visible = oSeg.Variables.Count > 1 && 
                            oVar.ExecutionIndex < oSeg.Variables.Count;
                        mnuVariables_Copy.Visible = false;
                        mnuVariables_Paste.Visible = false;
                        mnuVariables_Separator1.Visible = false;
                        mnuVariables_Separator2.Visible = false;
                        mnuVariables_Refresh.Visible = false;
                        mnuVariables_Rename.Visible = false;
                        break;
                    case NodeTypes.VariableActionsCategory:
                        mnuVariables_Add.Visible = true;
                        mnuVariables_Add.DropDown = mnuVariables_AddAction;
                        mnuVariables_AddVariable.Visible = false;
                        mnuVariables_AddTaglessVariable.Visible = false;
                        mnuVariables_AddAssociatedWordTag.Visible = false;
                        mnuVariables_Delete.Visible = false;
                        mnuVariables_DeleteAll.Visible = false;
                        mnuVariables_MoveUp.Visible = false;
                        mnuVariables_MoveDown.Visible = false;
                        mnuVariables_Copy.Visible = false;
                        mnuVariables_Paste.Visible = false;
                        mnuVariables_Separator1.Visible = false;
                        mnuVariables_Separator2.Visible = false;
                        mnuVariables_Refresh.Visible = false;
                        mnuVariables_Rename.Visible = false;
                        break;
                    case NodeTypes.VariableAction:
                        mnuVariables_Add.Visible = false;
                        mnuVariables_AddVariable.Visible = false;
                        mnuVariables_AddTaglessVariable.Visible = false;
                        mnuVariables_AddAssociatedWordTag.Visible = false;
                        mnuVariables_Delete.Visible = true;
                        mnuVariables_DeleteAll.Visible = false;
                        mnuVariables_MoveUp.Visible = true;
                        mnuVariables_MoveDown.Visible = true;
                        mnuVariables_Copy.Visible = false;
                        mnuVariables_Paste.Visible = false;
                        mnuVariables_Separator1.Visible = false;
                        mnuVariables_Separator2.Visible = false;
                        mnuVariables_Refresh.Visible = false;
                        mnuVariables_Rename.Visible = false;
                        break;
                    case NodeTypes.SegmentAuthorsControlActionsCategory:
                    case NodeTypes.ControlActionsCategory:
                    case NodeTypes.SegmentJurisdictionControlActionsCategory:
                    case NodeTypes.LanguageControlActionsCategory:
                        mnuVariables_Add.Visible = true;
                        mnuVariables_Add.DropDown = mnuVariables_AddControlAction;
                        mnuVariables_AddVariable.Visible = false;
                        mnuVariables_AddTaglessVariable.Visible = false;
                        mnuVariables_AddAssociatedWordTag.Visible = false;
                        mnuVariables_Delete.Visible = false;
                        mnuVariables_DeleteAll.Visible = false;
                        mnuVariables_MoveUp.Visible = false;
                        mnuVariables_MoveDown.Visible = false;
                        mnuVariables_Copy.Visible = false;
                        mnuVariables_Paste.Visible = false;
                        mnuVariables_Separator1.Visible = false;
                        mnuVariables_Separator2.Visible = false;
                        mnuVariables_Refresh.Visible = false;
                        mnuVariables_Rename.Visible = false;
                        break;
                    case NodeTypes.SegmentAuthorsControlAction: 
                    case NodeTypes.ControlAction:
                    case NodeTypes.SegmentJurisdictionControlAction:
                    case NodeTypes.LanguageControlAction:
                        mnuVariables_Add.Visible = false;
                        mnuVariables_AddVariable.Visible = false;
                        mnuVariables_AddTaglessVariable.Visible = false;
                        mnuVariables_AddAssociatedWordTag.Visible = false;
                        mnuVariables_Delete.Visible = true;
                        mnuVariables_DeleteAll.Visible = false;
                        mnuVariables_MoveUp.Visible = true;
                        mnuVariables_MoveDown.Visible = true;
                        mnuVariables_Copy.Visible = false;
                        mnuVariables_Paste.Visible = false;
                        mnuVariables_Separator1.Visible = false;
                        mnuVariables_Separator2.Visible = false;
                        mnuVariables_Refresh.Visible = false;
                        mnuVariables_Rename.Visible = false;
                        break;
                    default:
                        mnuVariables_Add.Visible = false;
                        mnuVariables_Delete.Visible = false;
                        mnuVariables_AddVariable.Visible = false;
                        mnuVariables_AddTaglessVariable.Visible = false;
                        mnuVariables_AddAssociatedWordTag.Visible = false;
                        mnuVariables_Delete.Visible = false;
                        mnuVariables_DeleteAll.Visible = false;
                        mnuVariables_MoveUp.Visible = false;
                        mnuVariables_MoveDown.Visible = false;
                        mnuVariables_Copy.Visible = false;
                        mnuVariables_Paste.Visible = false;
                        mnuVariables_Separator1.Visible = false;
                        mnuVariables_Separator2.Visible = false;
                        mnuVariables_Refresh.Visible = false;
                        mnuVariables_Rename.Visible = false;
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// displays different items for parent/child
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuBlocks_Opening(object sender, CancelEventArgs e)
        {
            NodeTypes iType = (NodeType(m_oCurNode));
            try
            {
                switch (iType)
                {
                    case NodeTypes.BlocksCategory:
                        mnuBlocks_Add.Visible = true;
                        mnuBlocks_Delete.Visible = false;
                        mnuBlocks_DeleteAll.Visible = false;
                        mnuBlocks_MoveUp.Visible = false;
                        mnuBlocks_MoveDown.Visible = false;
                        mnuBlocks_Copy.Visible = false;
                        mnuBlocks_Paste.Visible = false;
                        mnuBlocks_Separator1.Visible = false;
                        mnuBlocks_Separator2.Visible = false;
                        mnuBlocks_Refresh.Visible = true;
                        mnuBlocks_Rename.Visible = false;
                        break;
                    case NodeTypes.Block:
                        mnuBlocks_Add.Visible = false;
                        mnuBlocks_Delete.Visible = true;
                        mnuBlocks_DeleteAll.Visible = false;
                        mnuBlocks_MoveUp.Visible = false;
                        mnuBlocks_MoveDown.Visible = false;
                        mnuBlocks_Copy.Visible = false;
                        mnuBlocks_Paste.Visible = false;
                        mnuBlocks_Separator1.Visible = false;
                        mnuBlocks_Separator2.Visible = false;
                        mnuBlocks_Refresh.Visible = false;
                        mnuBlocks_Rename.Visible = false;
                        break;
                    default:
                        mnuBlocks_Delete.Visible = false;
                        mnuBlocks_Delete.Visible = false;
                        mnuBlocks_DeleteAll.Visible = false;
                        mnuBlocks_MoveUp.Visible = false;
                        mnuBlocks_MoveDown.Visible = false;
                        mnuBlocks_Copy.Visible = false;
                        mnuBlocks_Paste.Visible = false;
                        mnuBlocks_Separator1.Visible = false;
                        mnuBlocks_Separator2.Visible = false;
                        mnuBlocks_Refresh.Visible = false;
                        mnuBlocks_Rename.Visible = false;
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// closes menu when mouse leaves
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuBlocks_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                mnuBlocks.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// creates an admin variable, plus associated word tag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVariables_AddVariable_Click(object sender, EventArgs e)
        {
            try
            {
                CreateVariable(true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// adds associated mVar to tagless variable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVariables_AddAssociatedWordTag_Click(object sender, EventArgs e)
        {
            Variable oVar = null;
            Segment oSeg = null;
            bool bAddContainingSeg = false;
            Word.Range oRange = null;
            string xSelText = null;

            //JTS 5/10/10: 10.2
            bool bContentControls = m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML;
            try
            {
                oVar = GetVariableFromNode(m_oCurNode);
                oSeg = GetSegmentFromNode(m_oCurNode);

                //get selection range
                oRange = m_oMPDocument.WordDocument.Application.Selection.Range;

                //validate selection range
                LMP.Forte.MSWord.TagInsertionValidityStates iValid =
                  ValidateInsertionLocation(oRange, oSeg, LMP.Forte.MSWord.TagTypes.Variable);

                if (iValid == LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoContainingSegmentExists)
                    bAddContainingSeg = true;
                else if(iValid != LMP.Forte.MSWord.TagInsertionValidityStates.Valid)
                {
                    //variable location not allowed - alert
                    string xMsg = GetInsertionInvalidityMessage(iValid,
                        ForteDocument.TagTypes.Variable);

                    MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                //if insertion location is valid we now need to validate
                //selection text.  Selection text must match oVar.Value
                //if selection is a cursor point, we'll duplicate range automatically
                //is selection differs, we message user and give choice to reselect or
                //allow MacPac to duplicate the range text 
                xSelText = oRange.Text;
                string xVarText;
                if (bContentControls)
                    xVarText = oVar.AssociatedContentControls[0].Range.Text;
                else
                    xVarText = oVar.AssociatedWordTags[0].Range.Text;

                bool bReplace = false;

                if (xSelText == null)
                    //selection is cursor point, automatically duplicate oVar.Value
                    bReplace = true;
                else if (xSelText != xVarText)
                { 
                    //message user to replace selected range with oVar.Value
                    string xMsg = LMP.Resources.GetLangString("Msg_ReplaceSelectedAssociatedWordTagText");

                    DialogResult iRes = MessageBox.Show(xMsg, LMP.String.MacPacProductName, 
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    switch (iRes)
                    {
                        case DialogResult.No:
                            return;
                        case DialogResult.Yes:
                            bReplace = true;
                            break;
                        default:
                            return;
                    }
                }
 
                if (bReplace)
                {
                    //duplicate value and reset selection range
                    oRange.Text = xVarText;
                    oRange.Select();
                }

                //create a containing segment if flag set above
                if (bAddContainingSeg)
                {
                    //Set default value = selected text, if any
                    try
                    {
                        xSelText = oRange.Text;
                    }
                    catch { }

                    this.AddContainingmSeg(oSeg, oRange);
                }
                  
                //insert the associated word tag
                //set property to ensure m_oCurNode does not get reset
                //by OnXMLSelectionChange event handler
                this.ProgrammaticallySelectingWordTag = true;
                if (bContentControls)
                    oVar.InsertAssociatedContentControl();
                else
                    oVar.InsertAssociatedTag();
                this.ProgrammaticallySelectingWordTag = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// draws pic border to indicate button active
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picPreview_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                //TODO: we need standard button mouseover behavior
                //this.picPreview.BorderStyle = BorderStyle.FixedSingle;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// resets border style = none when mouse leaves pc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picPreview_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                //TODO: we need standard button mouseover behavior
                //this.picPreview.BorderStyle = BorderStyle.None;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// generates new document w/ edited segment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picPreview_Click(object sender, EventArgs e)
        {
            try
            {
                //handle invalid control value - user will be messaged
                if (this.IsValidControlValue((IControl)m_oCurCtl, m_oCurNode) == false)
                    return;

                //save any changes
                ExitEditMode(false);

                SetupPreviewMode();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// displays assignments form for segments
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_Assignments_Click(object sender, EventArgs e)
        {
            try
            {
                //get the current segment 
                Segment oAssignedSegment = (Segment)this.m_oCurNode.Tag;

                //call manage assignments 
                if(oAssignedSegment.Parent == null)
                    //this is a top level segment - assign self-replacement segments-
                    //ie those segments that can replace the segment when it is a top
                    //level segment
                    ManageAssignments(oAssignedSegment.TypeID, oAssignedSegment);
                else
                    //this is a child segment - assign replacement segments-
                    //ie those segments that can replace the segment when it is a child
                    //of the current parent
                    ManageAssignments(oAssignedSegment.TypeID, oAssignedSegment.Parent);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 3870 : JSW
        /// <summary>
        /// displays read-only assignments form to display parent segment assignments
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_ParentAssignments_Click(object sender, EventArgs e)
        {
            try
            {
                //get the current segment 
                Segment oSegment = (Segment)this.m_oCurNode.Tag;

                ParentAssignmentsForm oForm = new ParentAssignmentsForm(oSegment.ID1, oSegment.TypeID);

                oForm.ShowDialog();
                oForm.Close();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// displays assignments form to manage trailer assignments
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_TrailerAssignments_Click(object sender, EventArgs e)
        {
            try
            {
                //get the current segment 
                Segment oSegment = (Segment)this.m_oCurNode.Tag;

                //call manage assignments 
                ManageAssignments(mpObjectTypes.Trailer, oSegment);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// displays assignments form to manage draft stamp assignments
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_DraftStampAssignments_Click(object sender, EventArgs e)
        {
            try
            {
                //get the current segment 
                Segment oSegment = (Segment)this.m_oCurNode.Tag;

                //call manage assignments 
                ManageAssignments(mpObjectTypes.DraftStamp, oSegment);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// creates child segment of top level segment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_CreateNewChildSegment_Click(object sender, EventArgs e)
        {
            try
            {
                CreateNewChildSegment();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a Segment is deleted - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_SegmentDeleted(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                Segment oSeg = oArgs.Segment;
                this.DeleteNode(oSeg.FullTagID);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a Segment is added - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_SegmentAdded(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                Segment oSeg = oArgs.Segment;
                UltraTreeNode oNode = this.InsertNode(oSeg, oSeg.Parent);

                //populate and expand top-level node
                if (oSeg.Parent == null)
                {
                    m_bIsContentDesignerSegment = (oSeg is AdminSegment && oSeg.ID2 != 0) || (oSeg is UserSegment && oSeg.TypeID != mpObjectTypes.UserSegment); //GLOG 8376
                    RefreshSegmentNode(oNode);
                    oNode.Expanded = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a variable is deleted by a
        /// variable action with an expanded delete scope - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_VariableDeleted(object sender, VariableEventArgs oArgs)
        {
            try
            {
                string xKey = null;
                Variable oVar = oArgs.Variable;
                if (!oVar.IsStandalone)
                    //xKey = System.String.Concat(oVar.Segment.TagID, ".", oVar.ID);
                    xKey = System.String.Concat(oVar.Segment.FullTagID, ".Variables.", oVar.ID);
                else
                    xKey = oVar.ID;

                this.DeleteNode(xKey);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a variable is added - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_VariableAdded(object sender, VariableEventArgs oArgs)
        {
            try
            {
                //GLOG 5280: Don't attempt adding node for Variable if it already exists
                Variable oVar = oArgs.Variable;
                Segment oSegment = oVar.Segment;
                string xSegmentTagID = oSegment.FullTagID;
                UltraTreeNode oNode = null;
                try
                {
                    // Check for node matching Segment and Variable
                    oNode = this.GetNodeFromTagID(xSegmentTagID + ".Variables." + oVar.ID);
                }
                catch { }
                //if (oVar.ControlType != ControlTypes.None)
                if (oNode==null)
                    this.InsertNode(oVar);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when the definition of a variable
        /// is modified - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_VariableDefinitionChanged(object sender, VariableEventArgs oArgs)
        {
            try
            {
                //TODO: not implemented
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a Block is deleted - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_BlockDeleted(object sender, BlockEventArgs oArgs)
        {
            try
            {
                Block oBlock = oArgs.Block;
                this.DeleteNode(oBlock.TagID);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a Block is added - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_BlockAdded(object sender, BlockEventArgs oArgs)
        {
            try
            {
                Block oBlock = oArgs.Block;
                //if (oBlock.ShowInTree == true)
                this.InsertNode(oBlock);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when the definition of a Block
        /// is modified - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_BlockDefinitionChanged(object sender, BlockEventArgs oArgs)
        {
            try
            {
                //TODO: not implemented
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuBlocks_Add_Click(object sender, EventArgs e)
        {
            try
            {
                CreateBlock(); 
            }
            
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// delete block from task pane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuBlocks_Delete_Click(object sender, EventArgs e)
        {
            Segment oSeg = GetSegmentFromNode(m_oCurNode);
            Block oBlock = GetBlockFromNode(m_oCurNode);

            //propmpt user before deletion
            string xMsg = LMP.Resources.GetLangString("Prompt_DeleteBlock");
            DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (oDR == DialogResult.No)
                return;
            try
            {
                this.ProgrammaticallySelectingWordTag = true;
                oSeg.Blocks.Delete(oBlock);
                this.ProgrammaticallySelectingWordTag = false;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles reordering Variables by drag and drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent(typeof(UltraTreeNode)))
                {
                    //The Point that the mouse cursor is on, in Tree coords. 
                    //This event passes X and Y in form coords. 
                    System.Drawing.Point PointInTree;
                    //Get the position of the mouse in the tree, as opposed
                    //to form coords
                    PointInTree = this.treeDocContents.PointToClient(new Point(e.X, e.Y));
                    UltraTreeNode oDropNode = GetNearestNodeFromPoint(ref PointInTree);

                    if (oDropNode != null)
                    {
                        if (NodeType(oDropNode) != NodeTypes.Variable)
                        {
                            Variable oVar = GetVariableFromNode(oDropNode);
                            if (oVar != null)
                                oDropNode = GetNodeFromTagID(oVar.TagID);
                            else
                                return;
                        }
                        //Variable can only be moved within the same segment
                        if (oDropNode.Parent != m_oDraggedNode.Parent)
                            return;

                        int iDropIndex = oDropNode.Index;
                        switch (GetNodeDropLocation(PointInTree, oDropNode))
                        {
                            case NodeDropLocation.Below:
                                iDropIndex = oDropNode.Index + 1;
                                break;

                            case NodeDropLocation.Above:
                                iDropIndex = oDropNode.Index;
                                break;
                            case NodeDropLocation.Inside:
                                iDropIndex = oDropNode.Index + 1;
                                break;
                        }
                        MoveVariablesOrActionsNode(m_oDraggedNode, iDropIndex - m_oDraggedNode.Index);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                // End dragging
                m_oDraggedNode = null;
            }

        }
        /// handle close attempt with invalid control value
        /// </summary>
        /// <param name="Doc"></param>
        /// <param name="Cancel"></param>
        private void m_oApp_DocumentBeforeClose(Microsoft.Office.Interop.Word.Document Doc, ref bool Cancel)
        {
            try
            {
                //exit if the document being closed is not the
                //document associated with this designer - the
                //event was then triggered by the closing of a
                //different document
                if (this.m_oMPDocument.WordDocument != Doc)
                    return;


                //JTS 1/20/09: No need to run this if temp doc is being closed
                if (!LMP.Forte.MSWord.WordDoc.IsTempDoc(Doc))
                {
                    //Cancel close if Save Prompt is cancelled
                    Cancel = !SaveDesignIfNecessary();
                }
                //Unsubscribe to prevent multiple assignments
                if (!Cancel)
                    m_oApp.DocumentBeforeClose -= m_oApp_DocumentBeforeClose;

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
                Cancel = true;
            }
        }

        /// <summary>
        /// deletes designated segment plus all related parts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegments_DeleteSegment_Click(object sender, EventArgs e)
        {
            Segment oSeg = GetSegmentFromNode(m_oCurNode);
            if (oSeg == null)
                return;

            try
            {
                DeleteSegment(oSeg); 
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void DocumentDesigner_Resize(object sender, EventArgs e)
        {
            try
            {
                //GLOG : 8546 : ceh
                if (m_oCurCtl != null)
                {
                    int iControlWidth = 0;

                    if (this.ScrollBarsVisible)
                        iControlWidth = this.Width -
                            m_oCurCtl.Left - ((int)(25 * Session.ScreenScalingFactor));
                    else
                        iControlWidth = this.Width -
                            m_oCurCtl.Left - ((int)(10 * Session.ScreenScalingFactor));

                    m_oCurCtl.Width = iControlWidth;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_KeyDown(object sender, KeyEventArgs e)
        {
            //Key presses still get passed to tree, even if ContextMenuStrip is displayed
            if (mnuSegments.Visible)
            {
                if (e.KeyCode == Keys.Escape)
                    mnuSegments.Close(ToolStripDropDownCloseReason.Keyboard);
                else
                    mnuSegments.HandleKeyDown(e);
                e.SuppressKeyPress = true;
                return;
            }
            else if (mnuVariables.Visible)
            {
                if (e.KeyCode == Keys.Escape)
                    mnuVariables.Close(ToolStripDropDownCloseReason.Keyboard);
                else
                    mnuVariables.HandleKeyDown(e);
                e.SuppressKeyPress = true;
                return;
            }
            else if (mnuBlocks.Visible)
            {
                if (e.KeyCode == Keys.Escape)
                    mnuBlocks.Close(ToolStripDropDownCloseReason.Keyboard);
                else
                    mnuBlocks.HandleKeyDown(e);
                e.SuppressKeyPress = true;
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.Down:
                    //Ctrl+Down displays context menu
                    if (!e.Alt && !e.Shift && e.Control)
                    {
                        e.SuppressKeyPress = true;
                        e.Handled = true;
                        DisplayContextMenu(m_oCurNode);
                    }
                    break;
                case Keys.F3:
                    if (e.Control)
                    {
                        this.TaskPane.Toggle();
                    }
                    break;
            }
        }

        private void HandleControlF3(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3 && e.Control)
            {
                // Toggle to the next task pane tab.
                this.TaskPane.Toggle();
            }
        }

        private void splitContents_SplitterMoved(object sender, SplitterEventArgs e)
        {
            Session.CurrentUser.UserSettings.HelpWindowHeight = this.pnlDescription.Height;
            this.TaskPane.UpdateContentManagerHelpWindowView();
            this.TaskPane.UpdateDocumentEditorHelpWindowView();
        }
        internal void UpdateHelpWindowView()
        {
            this.pnlDescription.Height = Session.CurrentUser.UserSettings.HelpWindowHeight;
        }
        private void picHelp_Click(object sender, EventArgs e)
        {
            try
            {
                Help oHelp = null;
                //GLOG : 8354 : jsw 
                // if there is no help file, there's a NullReferenceException error
                if (this.wbHelpText.Url != null  ) //&& this.wbHelpText.Url.AbsolutePath != "blank"
                {
                    oHelp = new Help(this.wbHelpText.Url.AbsoluteUri);
                }
                else
                {
                    oHelp = new Help(this.wbHelpText.DocumentText);
                }
                oHelp.Text = LMP.ComponentProperties.ProductName + " Help - " + this.lblStatus.Text;
                oHelp.FormClosed += new FormClosedEventHandler(oHelp_FormClosed);
                oHelp.Show();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// After selecting help the focus needs to 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oHelp_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.m_oCurCtl != null)
            {
                this.m_oCurCtl.Focus();
            }
        }

        private void mnuAnswerFileSegments_Add_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder oSB = new StringBuilder();

                if (!m_oCurNode.Key.EndsWith("AnswerFileSegments"))
                    return;
                else
                {
                    //get current target segments
                    LMP.Data.AdminSegmentDefs oDefs = new AdminSegmentDefs();

                    foreach (UltraTreeNode oNode in m_oCurNode.Nodes)
                    {
                        //GLOG 6966: Make sure string can be parsed as a Double even if
                        //the Decimal separator is something other than '.' in Regional Settings
                        string xSegmentID = ((LMP.Architect.Api.AnswerFileSegment)oNode.Tag).SegmentID;
                        if (xSegmentID.EndsWith(".0"))
                            xSegmentID = xSegmentID.Replace(".0", "");
                        int iID = (int)double.Parse((xSegmentID));
                        LMP.Data.AdminSegmentDef oDef = (LMP.Data.AdminSegmentDef)oDefs.ItemFromID(iID);

                        oSB.AppendFormat("{0}{1}{2}{3}", oDef.DisplayName, SegmentTreeview.NAME_VALUE_SEP,
                            iID.ToString(), SegmentTreeview.ITEM_SEP);
                    }
                }

                string xCurSegments = oSB.ToString();

                //trim trailing item separator
                if(xCurSegments != "")
                    xCurSegments = xCurSegments.Substring(0, xCurSegments.Length - 1);

                SegmentTreeview oForm = new SegmentTreeview(true, xCurSegments);
                DialogResult iRet = oForm.ShowDialog();
                if (iRet != DialogResult.Cancel)
                {
                    AdminSegment oSeg = (AdminSegment)GetSegmentFromNode(m_oCurNode);

                    //GLOG 3904
                    int iNumCurSegments = m_oCurAnswerFileSegments.Count;
                    //int iNumCurSegments = oSeg.AnswerFileSegments.Count;

                    for (int i = iNumCurSegments - 1; i >= 0; i--)
                    {
                        //GLOG 3904
                        m_oCurAnswerFileSegments.Remove(i);
                        //oSeg.AnswerFileSegments.Remove(i);
                    }

                    string xSegmentsList = oForm.Value;
                    if (xSegmentsList != "")
                    {
                        string[] aSegments = xSegmentsList.Split('~');
                        foreach (string xItem in aSegments)
                        {
                            string[] aSeg = xItem.Split(';');
                            //GLOG 3904
                            m_oCurAnswerFileSegments.Add(aSeg[1], Segment.InsertionLocations.InsertInNewDocument, Segment.InsertionBehaviors.InsertInSeparateNextPageSection);
                            //oSeg.AnswerFileSegments.Add(aSeg[1], Segment.InsertionLocations.InsertInNewDocument, Segment.InsertionBehaviors.InsertInSeparateNextPageSection);
                        }
                        m_oCurNode.Nodes.Clear();
                        AddSegmentAnswerFileSegmentsNodes(oSeg, m_oCurNode);
                        m_bDocIsDirty = true;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                m_bDocIsDirty = true;
            }
        }
        private void mnuAnswerFileSegments_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(m_oCurNode.Tag is AnswerFileSegment))
                    return;

                //propmpt user before deletion
                string xMsg = LMP.Resources.GetLangString("Prompt_DeleteSegmentAction");
                DialogResult oDR = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (oDR == DialogResult.No)
                    return;
                //GLOG 3904
                m_oCurAnswerFileSegments.Remove(m_oCurNode.Index);
                //AdminSegment oSeg = (AdminSegment)GetSegmentFromNode(m_oCurNode);
                //oSeg.AnswerFileSegments.Remove(m_oCurNode.Index);

                m_oCurNode.Remove();
                m_oCurNode = null;
                m_bDocIsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                m_bDocIsDirty = true;
            }
        }
        private void mnuAnswerFileSegments_MoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(m_oCurNode.Tag is AnswerFileSegment))
                    return;

                if (m_oCurNode.Index == 0)
                    //Already first item
                    return;

                //GLOG 3904
                m_oCurAnswerFileSegments.Move(m_oCurNode.Index, m_oCurNode.Index - 1);
                //AdminSegment oSeg = (AdminSegment)GetSegmentFromNode(m_oCurNode);
                //oSeg.AnswerFileSegments.Move(m_oCurNode.Index, m_oCurNode.Index - 1);
                m_oCurNode.Reposition(m_oCurNode.GetSibling(NodePosition.Previous), NodePosition.Previous);
                m_bDocIsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                m_bDocIsDirty = true;
            }
        }
        private void mnuAnswerFileSegments_MoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(m_oCurNode.Tag is AnswerFileSegment))
                    return;

                if (m_oCurNode.GetSibling(NodePosition.Next) == null)
                    //Already at last position
                    return;

                //GLOG 3904
                m_oCurAnswerFileSegments.Move(m_oCurNode.Index, m_oCurNode.Index + 1);
                //AdminSegment oSeg = (AdminSegment)GetSegmentFromNode(m_oCurNode);
                //oSeg.AnswerFileSegments.Move(m_oCurNode.Index, m_oCurNode.Index + 1);
                m_oCurNode.Reposition(m_oCurNode.GetSibling(NodePosition.Next), NodePosition.Next);
                m_bDocIsDirty = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                m_bDocIsDirty = true;
            }

        }
        private void mnuAnswerFileSegments_Opening(object sender, CancelEventArgs e)
        {
            NodeTypes iType = (NodeType(m_oCurNode));
            //GLOG 3904: No longer needed since List is held in private variable
            //Segment oSeg = GetSegmentFromNode(m_oCurNode);
            try
            {
                switch (iType)
                {
                    case NodeTypes.AnswerFileSegmentsCategory:
                        mnuAnswerFileSegments_Add.Visible = true;
                        mnuAnswerFileSegments_Delete.Visible = false;
                        mnuAnswerFileSegments_MoveUp.Visible = false;
                        mnuAnswerFileSegments_MoveDown.Visible = false;
                        break;
                    case NodeTypes.AnswerFileSegment:
                        mnuAnswerFileSegments_Add.Visible = false;
                        mnuAnswerFileSegments_Delete.Visible = true;
                        //GLOG 3904
                        mnuAnswerFileSegments_MoveUp.Visible = m_oCurAnswerFileSegments.Count > 1 &&
                            m_oCurNode.Index > 0;
                        mnuAnswerFileSegments_MoveDown.Visible = m_oCurAnswerFileSegments.Count > 1 &&
                            m_oCurNode.Index < m_oCurNode.Parent.Nodes.Count - 1;
                        //AdminSegment oASeg = (AdminSegment)oSeg;
                        //mnuAnswerFileSegments_MoveUp.Visible = oASeg.AnswerFileSegments.Count > 1 &&
                        //    m_oCurNode.Index > 0;
                        //mnuAnswerFileSegments_MoveDown.Visible = oASeg.AnswerFileSegments.Count > 1 &&
                        //    m_oCurNode.Index < m_oCurNode.Parent.Nodes.Count - 1;
                        break;
                    default:
                        mnuAnswerFileSegments_Add.Visible = false;
                        mnuAnswerFileSegments_Delete.Visible = false;
                        mnuAnswerFileSegments_MoveUp.Visible = false;
                        mnuAnswerFileSegments_MoveDown.Visible = false;
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        private void DocumentDesigner_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3 && e.Control)
            {
                // Toggle to the next task pane tab.
                this.TaskPane.Toggle();
            }
        }

        private void wbHelpText_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F3)
            {
                this.TaskPane.Toggle();
            }
        }

        private void pnlDocContents_Resize(object sender, EventArgs e)
        {
            // GLOG : 2989 : JAB
            // Redraw the dropdown arrow accordingly due to size change.
            if (this.treeDocContents.SelectedNodes.Count > 0)
            {
                DisplayContextMenuButton(this.treeDocContents.SelectedNodes[0]);
            }
        }

        /// <summary>
        /// GLOG : 2989 : JAB
        /// Display the context menu button next to a selected node.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_AfterSelect(object sender, SelectEventArgs e)
        {
            if (this.treeDocContents.SelectedNodes.Count > 0)
            {
                this.DisplayContextMenuButton(this.treeDocContents.SelectedNodes[0]);
            }
        }

        /// <summary>
        /// GLOG : 2989 : JAB
        /// Display the context menu upon loading this control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DocumentDesigner_Load(object sender, EventArgs e)
        {
            if (this.treeDocContents.ActiveNode != null)
            {
                this.DisplayContextMenuButton(this.treeDocContents.ActiveNode);
            }
        }

        private void pnlDocContents_SizeChanged(object sender, EventArgs e)
        {
            // GLOG : 2989 : JAB
            // Redraw the dropdown arrow accordingly due to size change.
            if (this.treeDocContents.ActiveNode != null)
            {
                DisplayContextMenuButton(this.treeDocContents.ActiveNode);
            }
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            try
            {
                //handle invalid control value - user will be messaged
                if (!this.IsValidControlValue((IControl)m_oCurCtl, m_oCurNode))
                    return;

                //save any changes
                ExitEditMode(false);

                //GLOG 6851 (dm) - moved rest of code into separate method
                this.CloseDocument();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// added for GLOG 6851 (dm)
        /// </summary>
        public void CloseDocument()
        {
            Word.Document oDoc = this.m_oMPDocument.WordDocument;

            if (!oDoc.Saved)
            {
                //alert and return if design doc contains more than one top level segment
                if (this.m_oMPDocument.Segments.Count > 1)
                {
                    MessageBox.Show(LMP.Resources.GetLangString(
                        "Msg_CantSaveMultSegmentDesignDoc"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //prompt to save
                    DialogResult iRes = MessageBox.Show("Save the design of  '" + this.m_oMPDocument.Segments[0].DisplayName + "'?",
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question);

                    if (iRes == DialogResult.Yes)
                    {
                        //save design
                        this.SaveSegmentDesign();
                    }
                    else if (iRes == DialogResult.Cancel)
                    {
                        return;
                    }

                    oDoc.Saved = true;
                }
            }

            LMP.Forte.MSWord.WordApp.CloseDocument(oDoc, false);
        }

        private void picSave_Click(object sender, EventArgs e)
        {
            try
            {
                //handle invalid control value - user will be messaged
                if (!this.IsValidControlValue((IControl)m_oCurCtl, m_oCurNode))
                    return;

                //save any changes
                ExitEditMode(false);

                //alert and return if design doc contains more than one top level segment
                if (this.m_oMPDocument.Segments.Count > 1)
                {
                    MessageBox.Show(LMP.Resources.GetLangString(
                        "Msg_CantSaveMultSegmentDesignDoc"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //save design
                    this.SaveSegmentDesign();
                    this.m_oMPDocument.WordDocument.Saved = true;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 2989 : JAB
        /// When the drop down arrow is clicked, display the context menu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picMenuDropdown_Click(object sender, EventArgs e)
        {
            DisplayContextMenu(m_oCurNode, this.picMenuDropdown.Location.X, this.picMenuDropdown.Location.Y);
        }
        private void picPrefs_Click(object sender, EventArgs e)
        {
            try
            {
                Segment oSegment = m_oMPDocument.Segments[0];
                if (!Segment.DefinitionContainsAuthorPreferences(oSegment.Definition.XML))
                {
                    MessageBox.Show("Segment definition contains no preference variables.", LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                AuthorPreferencesManager oForm = new AuthorPreferencesManager(m_oMPDocument.Segments[0].ID, true, true);
                oForm.ShowDialog();
            }
            catch { }
        }
        #endregion
		#region *********************private functions*********************
        /// <summary>
        /// returns specified admin or user segment property value
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        private string GetSegmentPropertyValue(Segment oSegment, string xPropName, bool bReturnUnderlyingType)
        {
            string xValue = null;
            switch (xPropName)
            {
                case "Segment Type":
                case "TypeID":
                case "Type ID":

                    if (bReturnUnderlyingType != true)
                    {
                        //return name of prop
                        xValue = oSegment.TypeID.ToString();
                        break;
                    }
                    else
                    {
                        //property type is an enum - convert to underlying value
                        //before converting to string
                        object oValue = (object)oSegment.TypeID;
                        System.Type oUnderlyingType = Enum.GetUnderlyingType(oSegment.TypeID.GetType());
                        object oUnderlyingValue = Convert.ChangeType(oValue, oUnderlyingType);
                        xValue = oUnderlyingValue.ToString();
                    }
                    break;
                case "DisplayName":
                case "Display Name":
                    xValue = oSegment.DisplayName;
                    break;
                case "JurisdictionDefaults":
                    if (!bReturnUnderlyingType)
                        //GLOG 3277: Combine levels into single line
                        xValue = oSegment.GetJurisdictionText().Replace("\r\n", "/");
                    else
                        xValue = oSegment.GetJurisdictionLevels();
                    break;
                case "MaxAuthors":
                case "MaximumAuthors":
                case "Maximum Authors":
                    xValue = oSegment.MaxAuthors.ToString();
                    break;
                case "AuthorControlProperties":
                case "Author Control Properties":
                    xValue = oSegment.AuthorControlProperties.ToString();
                    break;
                case "ShowChooser":
                case "Show Chooser":
                    xValue = oSegment.ShowChooser.ToString();
                    break;
                case "LinkAuthorsToParentDef":
                case "Link Authors To Parent":
                    if (!bReturnUnderlyingType)
                        xValue = oSegment.LinkAuthorsToParentDef.ToString();
                    else
                    {
                        //property type is an enum - convert to underlying value
                        //before converting to string
                        object oValue = (object)oSegment.LinkAuthorsToParentDef;
                        System.Type oUnderlyingType =
                            Enum.GetUnderlyingType(oSegment.LinkAuthorsToParentDef.GetType());
                        object oUnderlyingValue = Convert.ChangeType(oValue, oUnderlyingType);
                        xValue = oUnderlyingValue.ToString();
                    }
                    break;
                //convert numeric values to text equivilants
                //we'll use list properties of the following 
                //controls, since their lists are built in
                case "MenuInsertionOptions":
                case "InsertionLocations":
                case "Insertion Locations":
                    xValue = ((int)oSegment.MenuInsertionOptions).ToString();
                    if (bReturnUnderlyingType == false)
                    {
                        xValue = ConvertBitwiseValueToString(LMP.Controls.
                            InsertionLocationsList.List, Int32.Parse(xValue), false);
                        //GLOG 3277: Don't display multiple lines
                        if (xValue.Contains("\r\n"))
                            xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                    }
                    break;
                case "DefaultMenuInsertionBehavior":
                case "Insertion Behavior":
                    xValue = ((int)oSegment.DefaultMenuInsertionBehavior).ToString();
                    if (bReturnUnderlyingType == false)
                    {
                        xValue = ConvertBitwiseValueToString(LMP.Controls.
                            InsertionBehaviorsList.List, Int32.Parse(xValue));
                        //GLOG 3277: Don't display multiple lines
                        if (xValue.Contains("\r\n"))
                            xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                    }
                    break;
                case "DefaultDragBehavior":
                case "Default Drag Behavior":
                    xValue = ((int)oSegment.DefaultDragBehavior).ToString();
                    if (bReturnUnderlyingType == false)
                        xValue = ConvertBitwiseValueToString(LMP.Controls.
                            InsertionBehaviorsList.List, Int32.Parse(xValue));
                    break;
                case "DefaultDoubleClickBehavior":
                case "Default Click Behavior":
                    xValue = ((int)oSegment.DefaultDoubleClickBehavior).ToString();
                    if (bReturnUnderlyingType == false)
                        xValue = ConvertBitwiseValueToString(LMP.Controls.
                            InsertionBehaviorsList.List, Int32.Parse(xValue));
                    break;
                case "DefaultDragLocation":
                case "Default Drag Location":
                    xValue = ((int)oSegment.DefaultDragLocation).ToString();
                    if (bReturnUnderlyingType == false)
                        xValue = ConvertValueToString(LMP.Controls.
                            DefaultLocationComboBox.List, Int32.Parse(xValue));
                    break;
                case "DefaultDoubleClickLocation":
                case "Default Click Location":
                    xValue = ((int)oSegment.DefaultDoubleClickLocation).ToString();
                    if (bReturnUnderlyingType == false)
                        xValue = ConvertValueToString(LMP.Controls.
                            DefaultLocationComboBox.List, Int32.Parse(xValue));
                    break;
                case "ProtectedFormPassword":
                case "Protected Form Password":
                    xValue = oSegment.ProtectedFormPassword;
                    break;
                case "HelpText":
                case "Help Text":
                    xValue = oSegment.HelpText;
                    break;
                case "AuthorNodeUILabel":
                case "Author Node UI Label":
                    //GLOG 6653
                    xValue = oSegment.AuthorsNodeUILabel;
                    //if (!bReturnUnderlyingType)
                    //    xValue = oSegment.AuthorsNodeUILabel.ToString();
                    //else
                    //{
                    //    //property type is an enum - convert to underlying value
                    //    //before converting to string
                    //    object oValue = (object)oSegment.AuthorsNodeUILabel;
                    //    System.Type oUnderlyingType =
                    //        Enum.GetUnderlyingType(oSegment.AuthorsNodeUILabel.GetType());
                    //    object oUnderlyingValue = Convert.ChangeType(oValue, oUnderlyingType);
                    //    xValue = oUnderlyingValue.ToString();
                    //}
                    break;
                case "AuthorsHelpText":
                case "Authors Help Text":
                    xValue = oSegment.AuthorsHelpText;
                    break;
                case "IsTransparentDef":
                case "Is Transparent":
                    xValue = oSegment.IsTransparentDef.ToString();
                    break;
                case "DefaultWrapperID":
                case "Default Wrapper ID":
                    if (bReturnUnderlyingType)
                    {
                        xValue = oSegment.DefaultWrapperID.ToString();
                    }
                    else
                    {
                        if (oSegment.DefaultWrapperID != 0)
                        {
                            AdminSegmentDefs oDefs = new AdminSegmentDefs();
                            try
                            {
                                xValue = ((AdminSegmentDef)oDefs.ItemFromID(oSegment.DefaultWrapperID)).DisplayName;
                            }
                            catch { }
                        }
                        else
                            xValue = null;
                    }
                    break;
                case "DisplayWizard":
                case "Display Wizard":
                    xValue = oSegment.DisplayWizard.ToString();
                    break;
                case "WordTemplate":
                case "Word Template":
                    xValue = oSegment.WordTemplate;
                    break;
                case "RequiredStyles":
                case "Required Styles":
                    xValue = oSegment.RequiredStyles;
                    break;
                case "IntendedUse":
                case "Intended Use":
                    xValue = ((int)oSegment.IntendedUse).ToString();
                    if (bReturnUnderlyingType == false)
                        xValue = ConvertValueToString(LMP.Controls.
                            IntendedUsesComboBox.List, Int32.Parse(xValue));
                    break;
                case "CourtChooserUILabel":
                case "Court Chooser UI Label":
                    xValue = oSegment.CourtChooserUILabel;
                    break;
                case "CourtChooserHelpText":
                case "Court Chooser Help Text":
                    xValue = oSegment.CourtChooserHelpText;
                    break;
                case "ShowCourtChooser":
                case "Show Court Chooser":
                    xValue = oSegment.ShowCourtChooser.ToString();
                    break;
                case "CourtChooserControlProperties":
                case "Court Chooser Control Properties":
                    xValue = oSegment.CourtChooserControlProperties;
                    if (!bReturnUnderlyingType)
                    {
                        //GLOG 3277: Don't display multiple lines
                        if (xValue.Contains(StringArray.mpEndOfSubValue.ToString()))
                            xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                    }
                    break;
                case "ShowSegmentDialog":
                case "Show Segment Dialog":
                    xValue = oSegment.ShowSegmentDialog.ToString();
                    break;
                case "DialogTabCaptions":
                case "Dialog Tab Captions":
                    xValue = oSegment.DialogTabCaptions;
                    break;
                case "DialogCaption":
                case "Dialog Caption":
                    xValue = oSegment.DialogCaption;
                    break;
                case "Default Trailer Type":
                case "DefaultTrailerID":
                    int iTrailerID = oSegment.DefaultTrailerID;
                    if (bReturnUnderlyingType == true)
                    {
                        //return numeric ID
                        xValue = iTrailerID.ToString();
                        break;
                    }
                    else
                    {
                        if (iTrailerID == DocumentStampUI.mpNoTrailerPromptSegmentID)
                            xValue = LMP.Resources.GetLangString("Prompt_NoTrailerDoNotPrompt");
                        else if (iTrailerID == DocumentStampUI.mpSkipTrailerSegmentID)
                            xValue = LMP.Resources.GetLangString("Prompt_NoTrailerIntegration");
                        else if (iTrailerID != 0)
                        {
                            //return display name for ID
                            AdminSegmentDefs oSegDefs = new AdminSegmentDefs(mpObjectTypes.Trailer);
                            AdminSegmentDef oSegDef = (AdminSegmentDef)oSegDefs.ItemFromID(iTrailerID);
                            if (oSegDef != null)
                            {
                                xValue = oSegDef.DisplayName;
                            }
                            else
                                xValue = "No Default";
                        }
                        else
                            xValue = "No Default";
                    }
                    break;
                case "SupportedLanguages":
                case "Supported Languages":
                    xValue = oSegment.SupportedLanguages;
                    if (!bReturnUnderlyingType)
                        xValue = LMP.Data.Application.GetLanguagesDisplayValue(xValue);
                    break;
                case "Culture":
                case "Default Language":
                    xValue = oSegment.Culture.ToString();
                    if (!bReturnUnderlyingType)
                        xValue = LMP.Data.Application.GetLanguagesDisplayValue(xValue);
                    break;
                case "AllowSideBySide":
                case "Allow Side-By-Side":
                    xValue = ((CollectionTableItem)oSegment).AllowSideBySide.ToString();
                    break;
                default:
                    break;
            }
            return xValue;
        
        }
        /// <summary>
        /// returns specified answer file segment property value
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        private string GetAnswerFileSegmentPropertyValue(AnswerFileSegment oSeg, string xPropName, bool bReturnUnderlyingType)
        {
            string xValue = null;
            switch (xPropName)
            {
                case "InsertionBehavior":
                case "Insertion Behavior":
                    xValue = ((int)oSeg.InsertionBehavior).ToString();
                    if (bReturnUnderlyingType == false)
                        xValue = ConvertBitwiseValueToString(LMP.Controls.
                            InsertionBehaviorsList.List, Int32.Parse(xValue));
                    break;
                case "InsertionLocation":
                case "Insertion Location":
                    xValue = ((int)oSeg.InsertionLocation).ToString();
                    if (bReturnUnderlyingType == false)
                        xValue = ConvertValueToString(LMP.Controls.
                            DefaultLocationComboBox.List, Int32.Parse(xValue));
                    break;
                default:
                    break;
            }
            return xValue;

        }
        /// <summary>
        /// returns specified Variable CI property value
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        private string GetVariableCIPropertyValue(Variable oVariable, string xPropName, bool bReturnUnderlyingType)
        {
            string xValue = null;

            switch (xPropName)
            {
                case "Request CI For Entire Segment":
                case "RequestCIForEntireSegment":
                    xValue = oVariable.RequestCIForEntireSegment.ToString(); 
                    break;
                case "CI Contact Type":
                case "CIContactType":
                    if (bReturnUnderlyingType != true)
                    {
                        //return name of prop
                        string [,] aValueNames = GetPropertySettingsList("CIContactTypes");

                        for (int i = 0; i <= aValueNames.GetUpperBound(0); i++)
                        {
                            if ((int)oVariable.CIContactType == Int32.Parse(aValueNames[i, 1]))
                                xValue = aValueNames[i, 0];
                        }
                        break;
                    }
                    else
                    {
                        //property type is an enum - convert to underlying value
                        //before converting to string
                        object oValue = (object)oVariable.CIContactType;
                        System.Type oUnderlyingType = Enum.GetUnderlyingType(oVariable.CIContactType.GetType());
                        object oUnderlyingValue = Convert.ChangeType(oValue, oUnderlyingType);
                        xValue = oUnderlyingValue.ToString();
                    }
                    break;
                case "CIDetailTokenString":
                case "CI Detail Token String":
                    xValue = oVariable.CIDetailTokenString;
                    break;

                case "CIAlerts":
                case "CI Alerts":
                //convert numeric values to text equivilants
                //we'll use list properties of the following 
                //controls, since their lists are built in
                    xValue = ((int)oVariable.CIAlerts).ToString();
                    if (bReturnUnderlyingType == false)
                    {
                        xValue = ConvertBitwiseValueToString(LMP.Controls
                            .CIAlertsList.List, Int32.Parse(xValue));
                        //GLOG 3277: Don't display multiple lines
                        if (xValue.Contains("\r\n"))
                            xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                    }
                    break;
                default:
                    break;
            }
            return xValue;

        }
        /// <summary>
        /// returns value of designated property of designated block
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="xPropName"></param>
        /// <param name="bReturnUnderlyingType">reserved for enum type property values</param>
        /// <returns></returns>
        private string GetBlockPropertyValue(Block oBlock, string xPropName, bool bReturnUnderlyingType)
        {
            string xValue = null;
            switch (xPropName)
            {
                case "IsBody":
                case "Is Body":
                    xValue = oBlock.IsBody.ToString();
                    break;
                case "Show In Tree":
                case "ShowInTree":
                    xValue = oBlock.ShowInTree.ToString();
                    break;
                case "Display Name":
                case "DisplayName":
                    xValue = oBlock.DisplayName; 
                    break;
                case "HelpText":
                case "Help Text":
                    xValue = oBlock.HelpText;
                    break;
                case "DisplayLevel":
                    if (bReturnUnderlyingType == true)
                    {
                        //return string representation of enum value rather than value name
                        Variable.Levels iLevel = oBlock.DisplayLevel;
                        xValue = ((int)iLevel).ToString();
                    }
                    else
                        xValue = oBlock.DisplayLevel.ToString();
                    break;
                case "Hot Key":
                case "HotKey":
                    xValue = oBlock.Hotkey;
                    break;
                case "Starting Text":
                case "StartingText":
                    xValue = oBlock.StartingText;
                    break;
                default:
                    break;
            }
            return xValue;
        }
        /// <summary>
        /// returns string value of designated property of designated variable
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="xPropName"></param>
        /// <param name="bReturnUnderlyingType">reserved for future enum type property values</param>
        /// <returns></returns>
        private string GetVariablePropertyValue(Variable oVar, string xPropName, bool bReturnUnderlyingType)
        {

            string xValue = null;
            switch (xPropName)
            {
                case "Display Name":
                case "DisplayName":
                    xValue = oVar.DisplayName;
                    break;
                case "Hot Key":
                case "HotKey":
                    xValue = oVar.Hotkey;
                    break;
                case "DefaultValue":
                case "Default Value":
                    xValue = oVar.DefaultValue;
                    break;
                case "SecondaryDefaultValue":
                case "Secondary Default Value":
                    xValue = oVar.SecondaryDefaultValue;
                    break;
                case "ValueSourceExpression":
                case "Value Source Expression":
                    xValue = oVar.ValueSourceExpression;
                    //GLOG 8317: Don't display [TagExpandedValue] field codes in Designer tree
                    if (xValue.ToUpper().StartsWith("[TAGEXPANDEDVALUE"))
                        xValue = "";
                    break;
                case "ValidationCondition":
                case "Validation Condition":
                    xValue = oVar.ValidationCondition;
                    break;
                case "ValidationResponse":
                case "Validation Response":
                    xValue = oVar.ValidationResponse;
                    break;
                case "HelpText":
                case "Help Text":
                    xValue = oVar.HelpText;
                    break;
                case "ControlType":
                case "Control Type":
                    if (bReturnUnderlyingType == true)
                    {
                        //return string representation of enum value rather than value name
                        LMP.Data.mpControlTypes iType = oVar.ControlType;
                        xValue = ((int)iType).ToString();
                    }
                    else
                        xValue = oVar.ControlType.ToString();
                    break;
                case "DisplayLevel":
                case "Display Level":
                    if (bReturnUnderlyingType == true)
                    {
                        //return string representation of enum value rather than value name
                        Variable.Levels iLevel = oVar.DisplayLevel;
                        xValue = ((int)iLevel).ToString();
                    }
                    else
                        xValue = (oVar.DisplayLevel == Variable.Levels.Basic) ? "In Main Section" : "In More Section" ;
                    break;
                case "DisplayIn":
                case "Display In":
                    xValue = ((int)(oVar.DisplayIn)).ToString();
                    if (bReturnUnderlyingType == false)
                    {
                        xValue = ConvertBitwiseValueToString(LMP.Controls.
                            DisplayLocationsList.List, Int32.Parse(xValue), false);
                        if (string.IsNullOrEmpty(xValue))
                            xValue = "Nowhere";
                        //GLOG 3277: Display on Single line
                        xValue = xValue.TrimEnd("\r\n".ToCharArray());
                        xValue = xValue.Replace("\r\n", "; ");
                    }
                    break;
                case "DisplayValue":
                case "Display Value":
                    xValue = oVar.DisplayValue;
                    break;
                case "ControlProperties":
                case "Control Properties":
                    xValue = oVar.ControlProperties;
                    if (!bReturnUnderlyingType)
                    {
                        //GLOG 3277: Don't display multiple lines
                        xValue = xValue.TrimEnd(StringArray.mpEndOfSubValue);
                        if (xValue.Contains(StringArray.mpEndOfSubValue.ToString()))
                            xValue = LMP.Resources.GetLangString("Prompt_DesignerMultipleValuesNodeText");
                    }
                    break;
                case "MustVisit":
                case "Must Visit":
                    xValue = oVar.MustVisit.ToString();
                    break;
                case "AllowPrefillOverride":
                case "Allow Prefill Override":
                    //GLOG 3475: Property changed from Boolean to Enum
                    if (bReturnUnderlyingType == true)
                    {
                        //return string representation of enum value rather than value name
                        Variable.PrefillOverrideTypes iOverride = oVar.AllowPrefillOverride;
                        xValue = ((int)iOverride).ToString();
                    }
                    else
                        xValue = oVar.AllowPrefillOverride.ToString();
                    break;
                case "IsMultiValue":
                case "Variable Type":
                    if (bReturnUnderlyingType == false)
                    {
                        xValue = oVar.IsMultiValue ? "Multiple Values" : "Single Value";
                    }
                    else
                    {
                        xValue = oVar.IsMultiValue.ToString();
                    }
                    break;
                case "AssociatedPrefillNames":
                case "Associated Prefill Names":
                    xValue = oVar.AssociatedPrefillNames;
                    break;
                case "TabNumber":
                case "Tab Number":
                    xValue = oVar.TabNumber.ToString();
                    break;
                //GLOG 1408
                case "NavigationTag":
                case "Navigation Tag":
                    xValue = oVar.NavigationTag;
                    break;
                default:
                    break;
            }
            return xValue;
        }
        /// <summary>
        /// explitly sets all VariableAction property values
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="bReturnUnderlyingType">reserved for enum type property values</param>
        private string GetVariableActionPropertyValue(VariableAction oVA, string xPropName, bool bReturnUnderlyingType)
        {
            string xPropValue = null;
            switch (xPropName)
            {
                case "ExecutionCondition":
                    xPropValue = oVA.ExecutionCondition;
                    break;
                case "ValueSetID":
                    if (bReturnUnderlyingType == true)
                        xPropValue = oVA.LookupListID.ToString();
                    else
                    {
                        //ValueSetIDs are dependent on records in tblLists
                        //we can retrieve these with an instance of a listcombo
                        //set to retrieve value lists only
                        LMP.Controls.ListCombo oCtl = new LMP.Controls.ListCombo();
                        oCtl.LimitToList = true;
                        oCtl.ValueSetListsOnly = true;
                        string[,] aNameValues = oCtl.ListArray;
                        string xValue = oVA.LookupListID.ToString();
                        //set to default (0 if no value set)
                        xPropValue = xValue;
                        for (int i = 0; i <= aNameValues.GetUpperBound(0); i++)
                        {
                            if (aNameValues[i, 1] == xValue)
                            {
                                xPropValue = aNameValues[i, 0];
                                break;
                            }
                        }
                    }
                    break;
                default:
                    break;
             }
             return xPropValue;
        }
        /// <summary>
        /// explitly sets all SegmentAction property values
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="bReturnUnderlyingType">reserved for enum type property values</param>
        private string GetSegmentActionPropertyValue(SegmentAction oSA, string xPropName, bool bReturnUnderlyingType)
        {
            string xPropValue = null;
            switch (xPropName)
            {
                case "ExecutionCondition":
                    xPropValue = oSA.ExecutionCondition;
                    break;
                default:
                    break;
            }
            return xPropValue;
        }
        /// <summary>
        /// explitly sets all ControlAction property values
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xPropName"></param>
        /// <param name="bReturnUnderlyingType">reserved for enum type property values</param>
        private string GetControlActionPropertyValue(ControlAction oCA, string xPropName, bool bReturnUnderlyingType)
        {
            string xPropValue = null;
            switch (xPropName)
            {
                case "ExecutionCondition":
                    xPropValue = oCA.ExecutionCondition;
                    break;
                case "Event":
                    if (bReturnUnderlyingType)
                    {
                        //return string representation of enum value rather than value name
                        ControlActions.Events iType = oCA.Event;
                        xPropValue = ((int)iType).ToString();
                    }
                    else
                    {
                        xPropValue = oCA.Event.ToString();
                    }
                    break;
                default:
                    break;
            }
            return xPropValue;
        }
        /// <summary>
        /// removes designated item from designated array
        /// </summary>
        /// <param name="xItemName"></param>
        /// <param name="aArray"></param>
        /// <returns></returns>
        private string[,] RemoveArrayItem(string xItemName, string[,]aArray)
        {
            //remove jurisdictional domain from oProps array
            //Create array with one less item
            string[,] aTemp = new string[aArray.GetUpperBound(0), 2];
            int iCount = 0;
            for (int i = 0; i <= aArray.GetUpperBound(0); i++)
            {
                if (aArray[i, 0] != xItemName)
                {
                    try
                    {
                        aTemp[iCount, 0] = aArray[i, 0];
                        aTemp[iCount, 1] = aArray[i, 1];
                    }
                    catch { }
                    iCount++;
                }
            }
            if (iCount > aTemp.GetUpperBound(0) + 1)
                //No match was found, return copy of original array
                return (string[,])aArray.Clone();
            else
                //Return resized array
                return (string[,])aTemp.Clone();
        }
        /// <summary>
        /// adds spaces to init capped unspaced strings
        /// </summary>
        /// <param name="xInputString"></param>
        /// <returns></returns>
        private string InsertSpacesBeforeCaps(string xInputString)
        {
            if (xInputString.IndexOf(" ") != -1)
                return xInputString;
            
            string xTemp = null;
            StringBuilder oSB = new StringBuilder();

            for (int i = 0; i < xInputString.Length; i++)
            {
                xTemp = xInputString.Substring(i, 1);
                if (xTemp.ToUpper() == xTemp && i > 0)
                    xTemp = " " + xTemp;
                oSB.Append(xTemp);
                xTemp = null;
            }
            return oSB.ToString();
        }
        /// <summary>
		/// returns the node with the specified tag ID
		/// </summary>
		/// <param name="xTagID"></param>
		/// <returns></returns>
		private UltraTreeNode GetNodeFromTagID(string xTagID)
		{
            UltraTreeNode oNode;

            try
            {
                
                oNode = this.treeDocContents.GetNodeByKey(xTagID);
            }
            catch
            {
                throw new LMP.Exceptions.NotInCollectionException(
                    LMP.Resources.GetLangString("Error_InvalidDocContentsTreeNode") + xTagID);
            }

            return oNode;

		}

        /// <summary>
        /// returns the ultra tree node belonging to the specified variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private UltraTreeNode GetTreeNodeFromVariable(Variable oVar)
        {
            string xKey = oVar.Segment.FullTagID + ".Variables." + oVar.ID;
            return GetNodeFromTagID(xKey); 
        }

        /// <summary>
        /// returns the ultra tree node belonging to the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private UltraTreeNode GetTreeNodeFromSegment(Segment oSeg)
        {
            string xKey = oSeg.FullTagID + ".Properties";
            return GetNodeFromTagID(xKey);
        }
        /// <summary>
        /// returns the ultra tree node belonging to the specified block
        /// </summary>
        /// <param name="oBlock"></param>
        /// <returns></returns>
        private UltraTreeNode GetTreeNodeFromBlock(Block oBlock)
        {
            string xKey = oBlock.Segment.FullTagID + "." + oBlock.Name;
            return GetNodeFromTagID(xKey);
        }
        
        /// <summary>
        /// sets node type based on level of node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        /// <summary>
        private NodeTypes NodeType(UltraTreeNode oNode)
        {
            if (oNode == null)
                return 0;

            string xKey = oNode.Key.ToString();
            
            //this code was the cause of GLOG item #3643 -
            //it also makes no sense to me, so I've removed it -
            //please restore if you find that it's absence is
            //causing other problems - df 3/3/09

            //oNode = treeDocContents.GetNodeByKey(oNode.Key);
            //if (oNode == null)
            //    //invalid node type - throw exception  
            //    throw new LMP.Exceptions.UINodeException(LMP.Resources.GetLangString
            //        ("Error_TreeNodeDoesNotExist") + oNode.Key);


            //GLOG - 3885 - ceh
            //restoring this code fixes GLOG item #3885 & doesn't break #3643
            oNode = treeDocContents.GetNodeByKey(oNode.Key);
            if (oNode == null)
                return 0;
            if (oNode.Tag is Segment)
                return NodeTypes.Segment;
            else if (oNode.Tag is Variable)
                return NodeTypes.Variable;
            else if (oNode.Tag is Block)
                return NodeTypes.Block;
            else if (oNode.Tag is AnswerFileSegment)
                return NodeTypes.AnswerFileSegment;

            else if (oNode.Parent.Tag is Variable)
            //variable/control actions category and variable prop name
            //are same level in treenode hierarchy
            {
                if (xKey.IndexOf(".VariableActions") != -1)
                    return NodeTypes.VariableActionsCategory;
                else if (xKey.IndexOf(".ControlActions") != -1)
                    return NodeTypes.ControlActionsCategory;
                else
                {
                    if (oNode.Key.EndsWith(".CI"))
                        return NodeTypes.VariableCICategory;
                    else
                        return NodeTypes.VariablePropertyValue;
                }
            }
            else if (oNode.Parent.Tag is AnswerFileSegment)
            {
                return NodeTypes.AnswerFileSegmentProperty;
            }
            else if (oNode.Parent.Tag is Segment)
            {
                if (xKey.EndsWith(".Variables"))
                    return NodeTypes.VariablesCategory;
                else if (xKey.EndsWith(".Blocks"))
                    return NodeTypes.BlocksCategory;
                else if (xKey.EndsWith(".Properties"))
                    return NodeTypes.SegmentPropertiesCategory;
                else if (xKey.EndsWith(".SegmentActions"))
                    return NodeTypes.SegmentActionsCategory;
                else if (xKey.EndsWith(".Authors"))
                    return NodeTypes.AuthorsCategory;
                else if (xKey.EndsWith(".Jurisdiction"))
                    return NodeTypes.JurisdictionCategory;
                else if (xKey.EndsWith(".Language"))
                    return NodeTypes.LanguageCategory;
                else if (xKey.EndsWith(".AnswerFileSegments"))
                    return NodeTypes.AnswerFileSegmentsCategory;
                else if (xKey.EndsWith(".DialogOptions"))
                    return NodeTypes.SegmentDialogCategory;
            }
            else if (oNode.Parent.Parent.Tag is Segment)
            {
                if ((oNode.Key).ToString().IndexOf(".SegmentActions") != -1)
                    return NodeTypes.SegmentEventsCategory;
                else
                {
                    //Segment Authors nodes - can be property or 
                    //Segment Author control actions category node
                    if (oNode.Parent.Key.EndsWith(".Authors"))
                    {
                        if (oNode.Key.EndsWith(".ControlActions"))
                            return NodeTypes.SegmentAuthorsControlActionsCategory;
                        else if (xKey.EndsWith(".AuthorsActions"))
                            return NodeTypes.AuthorsActionsCategory;
                        else
                            return NodeTypes.AuthorsProperty;
                    }
                    else if (oNode.Parent.Key.EndsWith(".Jurisdiction"))
                    {
                        if (oNode.Key.EndsWith(".ControlActions"))
                            return NodeTypes.SegmentJurisdictionControlActionsCategory;
                        else
                            return NodeTypes.JurisdictionProperty;
                    }
                    else if (oNode.Parent.Key.EndsWith(".DialogOptions"))
                    {
                        return NodeTypes.SegmentDialogProperty;
                    }
                    else if (oNode.Parent.Key.EndsWith(".Language"))
                    {
                        if (oNode.Key.EndsWith(".ControlActions"))
                            return NodeTypes.LanguageControlActionsCategory;
                        else if (xKey.EndsWith(".LanguageActions"))
                            return NodeTypes.LanguageActionsCategory;
                        else
                            return NodeTypes.LanguageProperty;
                    }
                    else
                        return NodeTypes.SegmentProperty;
                }
            }
            else if (oNode.Parent.Tag is Block)
                return NodeTypes.BlockProperty;

            else if (oNode.Parent.Parent.Tag is Block)
                return NodeTypes.BlockPropertyValue;
            else if (oNode.Parent.Parent.Tag is AnswerFileSegment)
                return NodeTypes.AnswerFileSegmentPropertyValue;
            else if (oNode.Parent.Parent.Tag is Variable)
            {    //variable property and variable/control action are the same
                //in treenode hierarchy
                if ((oNode.Key).ToString().IndexOf(".VariableActions") != -1)
                    if (xKey.EndsWith(".VariableActions"))
                        return NodeTypes.VariableActionsCategory;
                    else
                        return NodeTypes.VariableAction;
                else if ((oNode.Key).ToString().IndexOf(".ControlActions") != -1)
                    return NodeTypes.ControlAction;
                else
                {
                    if (oNode.Parent.Key.EndsWith(".CI"))
                        return NodeTypes.VariableCIProperty;
                    else
                        return NodeTypes.VariableProperty;
                }
            }
            else if (oNode.Parent.Parent.Parent.Tag is AdminSegment)
            {
                if (oNode.Key.IndexOf("_Value") != -1)
                    return NodeTypes.SegmentPropertyValue;
                else
                {
                    if (oNode.Key.Contains(".ControlActions"))
                        if (oNode.Key.Contains(".CourtChooserControlProperties"))
                            return NodeTypes.SegmentJurisdictionControlAction;
                        else if (oNode.Key.Contains(".Language"))
                            return NodeTypes.LanguageControlAction;
                        else
                            return NodeTypes.SegmentAuthorsControlAction;
                    else if (oNode.Key.Contains(".LanguageActions"))
                        return NodeTypes.LanguageAction;
                    if (oNode.Key.Contains(".AuthorsActions"))
                        return NodeTypes.AuthorsAction;
                    else
                        return NodeTypes.SegmentAction;
                }
            }
            else if (oNode.Parent.Parent.Parent.Tag is UserSegment)
            {
                if (oNode.Key.IndexOf("_Value") != -1)
                    return NodeTypes.SegmentPropertyValue;
                else
                    return NodeTypes.SegmentAction;
            }
            else if (oNode.Parent.Parent.Parent.Tag is Variable)
            {
                if (xKey.IndexOf(".VariableActions") != -1)
                    return NodeTypes.VariableActionProperty;
                else if (xKey.IndexOf(".ControlActions") != -1)
                {
                    if (xKey.Contains(".Parameters"))
                    {
                        if (xKey.EndsWith(".Parameters"))
                            return NodeTypes.ControlActionParametersCategory;
                        else
                            return NodeTypes.ControlActionParameter;
                    }
                    else
                        return NodeTypes.ControlActionProperty;
                }

                else if (xKey.IndexOf(".CI.") != -1)
                    return NodeTypes.VariableCIPropertyValue;
            }
            else if (oNode.Parent.Parent.Parent.Parent.Tag is Variable)
                if (xKey.IndexOf(".VariableActions") != -1)
                {
                    if (xKey.Contains(".Parameters"))
                        return NodeTypes.VariableActionParameter;
                    else
                        return NodeTypes.VariableActionProperty;
                }
                else
                {
                    if (xKey.Contains(".Parameters"))
                        return NodeTypes.ControlActionParameter;
                    else
                        return NodeTypes.ControlActionProperty;
                }
            else if (oNode.Parent.Parent.Parent.Parent.Tag is Segment)
            {
                if (oNode.Key.Contains(".ControlActions"))
                {
                    if (oNode.Key.Contains(".Parameters"))
                        return NodeTypes.SegmentActionParametersCategory;
                    else if (oNode.Key.Contains(".CourtChooserControlProperties"))
                        return NodeTypes.SegmentJurisdictionControlActionProperty;
                    else if (oNode.Key.Contains(".Language"))
                        return NodeTypes.LanguageControlActionProperty;
                    else
                        return NodeTypes.SegmentAuthorsControlActionProperty;
                }
                else
                {
                    if (oNode.Key.Contains(".Parameters"))
                        return NodeTypes.SegmentActionParametersCategory;
                    else
                        return NodeTypes.SegmentActionProperty;
                }
            }
            else if (oNode.Parent.Parent.Parent.Parent.Parent.Tag is Segment)
            {
                if (oNode.Key.Contains(".ControlActions") && oNode.Key.Contains(".CourtChooserControlProperties"))
                {
                    if (oNode.Key.Contains(".Parameters"))
                        return NodeTypes.SegmentJurisdictionControlActionParameter;
                    else
                        return NodeTypes.SegmentJurisdictionControlActionProperty;
                }
                else if (oNode.Key.Contains(".ControlActions") && oNode.Key.Contains(".LanguageControlProperties"))
                {
                    if (oNode.Key.Contains(".Parameters"))
                        return NodeTypes.LanguageControlActionParameter;
                    else
                        return NodeTypes.LanguageControlActionProperty;
                }
                else if (oNode.Key.Contains(".ControlActions"))
                {
                    if (oNode.Key.Contains(".Parameters"))
                        return NodeTypes.SegmentAuthorsControlActionParameter;
                    else
                        return NodeTypes.SegmentAuthorsControlActionProperty;
                }
                else
                {
                    if (oNode.Key.Contains(".Parameters"))
                        return NodeTypes.SegmentActionParameter;
                    else
                        return NodeTypes.SegmentActionProperty;
                }
            }
            else if (oNode.Parent.Parent.Parent.Parent.Parent.Tag is Variable == true)
                if (oNode.Parent.Parent.Parent.Parent.Key.ToUpper().EndsWith
                    (".VARIABLEACTIONS"))
                    //we've selected a parameter property node
                    return NodeTypes.VariableActionParameter;
                else
                    return NodeTypes.ControlActionParameter;

            else if (oNode.Parent.Parent.Parent.Parent.Parent.Parent.Tag is Segment)
            {
                if (oNode.Key.Contains(".ControlActions"))
                    if (oNode.Key.Contains(".CourtChooserControlProperties"))
                        return NodeTypes.SegmentJurisdictionControlActionParameter;
                    else if (oNode.Key.Contains(".Language"))
                        return NodeTypes.LanguageControlActionParameter;
                    else
                        return NodeTypes.SegmentAuthorsControlActionParameter;
                else
                    return NodeTypes.SegmentActionParameter;
            }
            else if (oNode.Key.EndsWith(".Authors"))
                return NodeTypes.AuthorsCategory;
            else if (oNode.Key.EndsWith(".Jurisdiction"))
                return NodeTypes.JurisdictionCategory;
            else if (oNode.Key.EndsWith(".Language"))
                return NodeTypes.LanguageCategory;
            return 0;
        }
        /// <summary>
        /// parses value node key
        /// gets design property value from specified node
        /// </summary>
        /// <param name="xKey"></param>
        /// <returns></returns>
        private string GetPropertyValueFromNode(UltraTreeNode oNode)
        {
            return GetPropertyValueFromNode(oNode, true);
        }
        /// <summary>
        /// parses value node key, uses LMP reflection
        /// to gets design property value from specified node
        /// overloaded method - optional return of underlying type
        /// </summary>
        /// <param name="xKey"></param>
        /// <returns></returns>
        private string GetPropertyValueFromNode(UltraTreeNode oNode, bool bReturnUnderlyingType)
        {
            
            char cSep = '.';
            string xPropValue = null;

            Variable oVar = null;
            Block oBlock = null;
            Segment oSeg = null;
            VariableAction oVarAction = null;
            ControlAction oCtlAction = null;
            SegmentAction oSegAction = null;

            string xKey = oNode.Key;
            string[] aKeyItems = xKey.Split(cSep);
            int iIndex = aKeyItems.GetUpperBound(0);
            //get prop name from key -- prop name is last field in aKeyItems[] so we use upperbound
            string xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

            switch (NodeType(oNode))
            {
                case NodeTypes.SegmentPropertyValue:
                    //segment design property value
                    oSeg = GetSegmentFromNode(oNode);
                    if (oSeg != null)
                    {
                        if (xPropName != "JurisdictionDefaults")
                            xPropValue = GetSegmentPropertyValue(oSeg, xPropName, bReturnUnderlyingType);
                        else
                        {
                            if (bReturnUnderlyingType)
                                return oSeg.GetJurisdictionLevels();
                            else
                                //GLOG 3277: Display Levels on single line
                                return oSeg.GetJurisdictionText().Replace("\r\n", "/");
                        }
                    }
                    break;
                case NodeTypes.AnswerFileSegmentProperty:
                case NodeTypes.AnswerFileSegmentPropertyValue:
                    AnswerFileSegment oAFSeg = GetAnswerFileSegmentFromNode(oNode);
                    if (oAFSeg != null)
                        xPropValue = GetAnswerFileSegmentPropertyValue(oAFSeg, xPropName, bReturnUnderlyingType);
                    break;
                case NodeTypes.VariableProperty: 
                    //variable design property value
                    oVar = GetVariableFromNode(oNode);

                    if (oVar != null)
                        xPropValue = GetVariablePropertyValue(oVar, xPropName, bReturnUnderlyingType);
                    break;

                case NodeTypes.VariableCIPropertyValue:
                    //variable CI design property value
                    oVar = GetVariableFromNode(oNode); 

                    if (oVar != null)
                        xPropValue = GetVariableCIPropertyValue(oVar, xPropName, bReturnUnderlyingType);
                    break;

                case NodeTypes.BlockPropertyValue:
                    //block design property value
                    oBlock = GetBlockFromNode(oNode);

                    if (oBlock != null & xPropName != null)
                        xPropValue = GetBlockPropertyValue(oBlock, xPropName, bReturnUnderlyingType); 
                    break;

                case NodeTypes.VariableActionProperty: 
                    //variable action design property value

                    oVar = GetVariableFromNode(oNode);
                    oVarAction = oVar.VariableActions[Int32.Parse(aKeyItems[iIndex - 1])];

                    if (oVarAction != null & xPropName != null)
                    {
                        xPropValue = GetVariableActionPropertyValue
                            (oVarAction, xPropName, bReturnUnderlyingType); 
                    }
                    break;

                case NodeTypes.SegmentActionProperty:
                    //variable action design property value

                    oSeg = GetSegmentFromNode(oNode);
                    oSegAction = oSeg.Actions[Int32.Parse(aKeyItems[iIndex - 1])];

                    if (oSegAction != null & xPropName != null)
                    {
                        xPropValue = GetSegmentActionPropertyValue
                            (oSegAction, xPropName, bReturnUnderlyingType); 
                    }
                    break;

                case NodeTypes.ControlActionProperty: 
                    //control action design property value

                    oVar = GetVariableFromNode(oNode);
                    oCtlAction = oVar.ControlActions[Int32.Parse(aKeyItems[iIndex - 1])];
                    
                    if (oCtlAction != null & xPropName != null)
                    {
                        xPropValue = GetControlActionPropertyValue
                            (oCtlAction, xPropName, bReturnUnderlyingType); 
                    }
                    break;

                case NodeTypes.SegmentAuthorsControlActionProperty:
                    //control action design property value

                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.Authors.ControlActions[Int32.Parse(aKeyItems[iIndex - 1])];

                    if (oCtlAction != null & xPropName != null)
                    {
                        xPropValue = GetControlActionPropertyValue
                            (oCtlAction, xPropName, bReturnUnderlyingType);
                    }
                    break;

                case NodeTypes.SegmentJurisdictionControlActionProperty:
                    //control action design property value

                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.CourtChooserControlActions[Int32.Parse(aKeyItems[iIndex - 1])];

                    if (oCtlAction != null & xPropName != null)
                    {
                        xPropValue = GetControlActionPropertyValue
                            (oCtlAction, xPropName, bReturnUnderlyingType);
                    }
                    break;

                case NodeTypes.LanguageControlActionProperty:
                    //control action design property value

                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.LanguageControlActions[Int32.Parse(aKeyItems[iIndex - 1])];

                    if (oCtlAction != null & xPropName != null)
                    {
                        xPropValue = GetControlActionPropertyValue
                            (oCtlAction, xPropName, bReturnUnderlyingType);
                    }
                    break;

                case NodeTypes.VariableActionParameter: //variable action parameter value

                    oVar = GetVariableFromNode(oNode);
                    oVarAction = oVar.VariableActions[Int32.Parse(aKeyItems[iIndex - 2])];
                    
                    //get parameter names and values - 
                    //number of parameters and names vary according to variable action type

                    if (oVarAction != null & xPropName != null)
                    {
                        string[,] aParamNames = GetVariableActionParameters
                        (oVarAction.Type);
                        string[] aParamValues = oVarAction.Parameters.Split
                            (StringArray.mpEndOfSubField);
                        //get designated parameter value
                        xPropValue = GetActionParameterValue(aParamNames, aParamValues, xPropName);
                    }
                    break;

                case NodeTypes.ControlActionParameter: //control action parameter value

                    oVar = GetVariableFromNode(oNode);
                    oCtlAction = oVar.ControlActions[Int32.Parse(aKeyItems[iIndex - 2])];
                    
                    //TODO: not fully implemented
                    if (oCtlAction != null & xPropName != null)
                    {
                        //get parameter names and values - 
                        //number of parameters and names vary according to variable action type
                        string[,] aParamNames = GetControlActionParameters
                            (oCtlAction.Type);
                        string[] aParamValues = oCtlAction.Parameters.Split
                            (StringArray.mpEndOfSubField);
                        //get designated parameter value
                        xPropValue = GetActionParameterValue(aParamNames, aParamValues, xPropName); 
                    }
                    break;

                case NodeTypes.SegmentAuthorsControlActionParameter: //control action parameter value

                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.Authors.ControlActions[Int32.Parse(aKeyItems[iIndex - 2])];

                    if (oCtlAction != null & xPropName != null)
                    {
                        //get parameter names and values - 
                        //number of parameters and names vary according to variable action type
                        string[,] aParamNames = GetControlActionParameters
                            (oCtlAction.Type);
                        string[] aParamValues = oCtlAction.Parameters.Split
                            (StringArray.mpEndOfSubField);
                        //get designated parameter value
                        xPropValue = GetActionParameterValue(aParamNames, aParamValues, xPropName);
                    }
                    break;
                
                case NodeTypes.SegmentActionParameter:
                    //variable action parameter value
                    oSeg = GetSegmentFromNode(oNode);
                    oSegAction = oSeg.Actions[Int32.Parse(aKeyItems[iIndex - 2])];

                    if (oSegAction != null & xPropName != null)
                    {
                        //get parameter names and values - 
                        //number of parameters and names vary according to variable action type
                        string[,] aParamNames = GetVariableActionParameters
                            (oSegAction.Type);
                        string[] aParamValues = oSegAction.Parameters.Split
                            (StringArray.mpEndOfSubField);
                        //get designated parameter value
                        xPropValue = GetActionParameterValue(aParamNames, aParamValues, xPropName);
                    }
                    break;
                case NodeTypes.SegmentJurisdictionControlActionParameter: //control action parameter value

                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.CourtChooserControlActions[Int32.Parse(aKeyItems[iIndex - 2])];

                    if (oCtlAction != null & xPropName != null)
                    {
                        //get parameter names and values - 
                        //number of parameters and names vary according to variable action type
                        string[,] aParamNames = GetControlActionParameters
                            (oCtlAction.Type);
                        string[] aParamValues = oCtlAction.Parameters.Split
                            (StringArray.mpEndOfSubField);
                        //get designated parameter value
                        xPropValue = GetActionParameterValue(aParamNames, aParamValues, xPropName);
                    }
                    break;
                case NodeTypes.LanguageControlActionParameter: //control action parameter value

                    oSeg = GetSegmentFromNode(oNode);
                    oCtlAction = oSeg.LanguageControlActions[Int32.Parse(aKeyItems[iIndex - 2])];

                    if (oCtlAction != null & xPropName != null)
                    {
                        //get parameter names and values - 
                        //number of parameters and names vary according to variable action type
                        string[,] aParamNames = GetControlActionParameters
                            (oCtlAction.Type);
                        string[] aParamValues = oCtlAction.Parameters.Split
                            (StringArray.mpEndOfSubField);
                        //get designated parameter value
                        xPropValue = GetActionParameterValue(aParamNames, aParamValues, xPropName);
                    }
                    break;
                default:
                    break;
            }
            return xPropValue;
        }
        /// <summary>
        /// returns name of designated parameter from value array
        /// </summary>
        /// <param name="aParamNames"></param>
        /// <param name="aParamValues"></param>
        /// <param name="xPropName"></param>
        /// <returns></returns>
        private string GetActionParameterValue(string[,] aParamNames, 
            string[] aParamValues, string xPropName)
        {
            string xPropValue = null;
            //get the parameter value from the array
            if (aParamNames.GetUpperBound(0) != aParamValues.GetUpperBound(0))
            {
                //get default value from paramNames array second index
                //if actual values don't exist -- indicated by 
                //fact paramNames length != paramValues length
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    if ((xPropName) == (aParamNames[i, 0]))
                    {
                        xPropValue = aParamNames[i, 1];
                    }
                }
            }
            else
            {
                //get the parameter value from the value array
                //compare param name name array.  if == return same index 
                //member of value array
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                    if ((xPropName) == (aParamNames[i, 0]))
                    {
                        xPropValue = aParamValues[i];
                    }
            }
            return xPropValue; 
        }
        /// <summary>
        /// recreates delimited parameter values string
        /// </summary>
        /// <param name="aParamValues">array of parameter values</param>
        /// <returns></returns>
        private string BuildParameterString(string[] aParamValues)
        {
            StringBuilder sParams = new StringBuilder();
            for (int i = 0; i <= aParamValues.GetUpperBound(0); i++)
            { 
                sParams.Append(aParamValues[i] + StringArray.mpEndOfSubField);
            }
            //trim end
            sParams.Remove(sParams.Length - 1, 1);
            
            return sParams.ToString();
        }
        /// <summary>
        /// parses node key for property name
        /// </summary>
        /// <param name="xKey"></param>
        /// <returns></returns>
        private string GetPropNameFromKey(string xKey)
        {
            string xName = null;
            char cSep = '.';
            string[] aTemp = xKey.Split(cSep);
            //property name is last item in aTemp array - remove "_value" if it exists
            if (aTemp != null)
            {
                xName = aTemp[aTemp.GetUpperBound(0)];
                xName = xName.IndexOf("_") > 0 ? xName = xName.Substring(0, xName.IndexOf("_")) : xName;
            }
            return xName;
        }
        /// <summary>
        /// parses tag string for action name
        /// </summary>
        /// <param name="xKey"></param>
        /// <returns></returns>
        private string GetActionNameFromKey(string xKey)
        {
            string xName = null;
            char cSep = '.';
            string[] aTemp = xKey.Split(cSep);
            //property name is second item in aTemp arrag
            if (aTemp != null)
                xName = aTemp[0];

            return xName;
        }
        /// <summary>
        /// converts delimited string to string delimited by line feed characters
        /// for designated iColumn
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="iColumn"></param>
        /// <param name="xLineFeed"></param>
        /// <param name="cRecordSeparator"></param>
        /// <param name="cColumnSeparator"></param>
        /// <returns></returns>
        private string ConvertValueToString(string xValue, int iColumn, string xLineFeed, 
                                            char cRecordSeparator, char cColumnSeparator)
        {
            //segment def L0-4 props are type int, so xValue must
            //be converted to name string from jurisidiction chooser value
            StringBuilder oSB = new StringBuilder();
            string[] aLevels = xValue.Split(cRecordSeparator);
            for (int i = 0; i <= aLevels.GetUpperBound(0); i++)
            {
                //each array element is also a delimited string
                oSB = oSB.Append((aLevels[i].Split(cColumnSeparator))[iColumn]);
                if (i < aLevels.GetUpperBound(0))
                    oSB.Append(xLineFeed);
            }
            return oSB.ToString();
        }
        /// <summary>
        /// returns string of member iValue
        /// </summary>
        /// <param name="aListArray"></param>
        /// <param name="iValue"></param>
        /// <returns></returns>
        private string ConvertValueToString(object[,] aListArray, int iValue)
        {
            int iTestValue = 0;
            for (int i = 0; i <= aListArray.GetUpperBound(0); i++)
            {
                iTestValue = (int) aListArray[i, 1];
                if (iValue == iTestValue)
                    return aListArray[i, 0].ToString();
            }
            return null;
        }
        /// <summary>
        /// builds string via bitwise comparison of iValue
        /// </summary>
        /// <param name="aListArray"></param>
        /// <param name="iValue"></param>
        /// <returns></returns>
        private string ConvertBitwiseValueToString(object[,] aListArray, int iValue)
        {
            return ConvertBitwiseValueToString(aListArray, iValue, true);
        }
        /// <summary>
        /// builds string via bitwise comparison of iValue
        /// </summary>
        /// <param name="aListArray"></param>
        /// <param name="iValue"></param>
        /// <param name="bIncludeZeroValue"></param>
        /// <returns></returns>
        private string ConvertBitwiseValueToString(object[,] aListArray, int iValue, bool bIncludeZeroValue)
        {
            StringBuilder oSB = new StringBuilder();
            int iTestValue = 0;

            //cycle through array, perform bitwise comparison between
            //array member and iValue

            for (int i = 0; i <= aListArray.GetUpperBound(0); i++)
            {
                iTestValue = (int)aListArray[i, 1];

                //skip value if it is zero, and we're not
                //including zero values in the string
                if (iTestValue == 0 && !bIncludeZeroValue)
                    continue;

                if ((iTestValue & iValue) == iTestValue)
                {
                    oSB.Append(aListArray[i, 0]);
                    if (i < aListArray.GetUpperBound(0))
                        oSB.Append("\r\n");
                }
            }
            return oSB.ToString();
        }
        /// <summary>
        /// gets parent segment from designated treenode
        /// </summary>
        /// <param name="oCurValueNode"></param>
        /// <returns></returns>
        private Segment GetSegmentFromNode(UltraTreeNode oNode)
        {
            switch (NodeType(oNode))
            {
                //Segment will be stored in top level node tag property
                case NodeTypes.Segment:
                    return (Segment)oNode.Tag;
                case NodeTypes.Variable:
                    return (Segment)oNode.Parent.Parent.Tag;
                case NodeTypes.VariablesCategory:
                    return (Segment)oNode.Parent.Tag;
                case NodeTypes.VariablePropertyValue:
                    return (Segment)oNode.Parent.Parent.Parent.Tag;
                case NodeTypes.Block:
                    return (Segment)oNode.Parent.Parent.Tag;
                case NodeTypes.BlocksCategory:
                    return (Segment)oNode.Parent.Tag;
                case NodeTypes.SegmentProperty:
                case NodeTypes.JurisdictionProperty:
                case NodeTypes.AuthorsProperty:
                case NodeTypes.LanguageProperty:
                    return (Segment)oNode.Parent.Parent.Tag;
                case NodeTypes.SegmentPropertyValue:
                    return (Segment)oNode.Parent.Parent.Parent.Tag;
                case NodeTypes.SegmentAction:
                case NodeTypes.LanguageAction:
                case NodeTypes.AuthorsAction:
                    return (Segment)oNode.Parent.Parent.Parent.Tag;
                case NodeTypes.SegmentEventsCategory:
                case NodeTypes.LanguageActionsCategory:
                case NodeTypes.AuthorsActionsCategory:
                    return (Segment)oNode.Parent.Parent.Tag;
                case NodeTypes.SegmentActionParametersCategory:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.SegmentActionProperty:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.SegmentActionParameter:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableProperty:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableCIPropertyValue:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.BlockProperty:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.BlockPropertyValue:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableActionsCategory:
                    return (Segment)oNode.Parent.Parent.Parent.Tag;
                case NodeTypes.ControlActionsCategory:
                    return (Segment)oNode.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableAction:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.ControlAction:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableActionProperty:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.ControlActionProperty:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableActionParametersCategory:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.ControlActionParametersCategory:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableActionParameter:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.ControlActionParameter:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.SegmentAuthorsControlActionsCategory:
                case NodeTypes.SegmentJurisdictionControlActionsCategory:
                case NodeTypes.LanguageControlActionsCategory:
                    return (Segment)oNode.Parent.Parent.Tag;
                case NodeTypes.SegmentAuthorsControlAction:
                case NodeTypes.SegmentJurisdictionControlAction:
                case NodeTypes.LanguageControlAction:
                    return (Segment)oNode.Parent.Parent.Parent.Tag;
                case NodeTypes.SegmentAuthorsControlActionProperty:
                case NodeTypes.SegmentJurisdictionControlActionProperty:
                case NodeTypes.LanguageControlActionProperty:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.SegmentAuthorsControlActionParameter:
                case NodeTypes.SegmentJurisdictionControlActionParameter:
                case NodeTypes.LanguageControlActionParameter:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.AnswerFileSegmentPropertyValue:
                    return (Segment)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.AnswerFileSegmentProperty:
                    return (Segment)oNode.Parent.Parent.Parent.Tag;
                case NodeTypes.AnswerFileSegment:
                    return (Segment)oNode.Parent.Parent.Tag;
                case NodeTypes.AnswerFileSegmentsCategory:
                    return (Segment)oNode.Parent.Tag;
                case NodeTypes.SegmentDialogCategory:
                    return (Segment)oNode.Parent.Tag;
                case NodeTypes.SegmentDialogProperty:
                    return (Segment)oNode.Parent.Parent.Tag;
                case NodeTypes.Value:
                    return null;
                default:
                    return null;
            }
        }
        /// <summary>
        /// gets parent variable from designated treenode
        /// </summary>
        /// <param name="oCurValueNode"></param>
        /// <returns></returns>
        private Variable GetVariableFromNode(UltraTreeNode oNode)
        {
            switch (NodeType(oNode))
            {
                // variable will be stored in top level node tag property
                case NodeTypes.Segment:
                    return null;
                case NodeTypes.Variable:
                    return (Variable)oNode.Tag;
                case NodeTypes.Block:
                    return null;
                case NodeTypes.VariablesCategory:
                    return null;
                case NodeTypes.VariableProperty:
                    return (Variable)oNode.Parent.Parent.Tag;
                case NodeTypes.VariablePropertyValue:
                    return (Variable)oNode.Parent.Tag;
                case NodeTypes.VariableCIPropertyValue:
                    return (Variable)oNode.Parent.Parent.Parent.Tag;
                case NodeTypes.BlockProperty:
                    return (Variable)oNode.Parent.Parent.Tag;
                case NodeTypes.VariableActionsCategory:
                    return (Variable)oNode.Parent.Tag;
                case NodeTypes.ControlActionsCategory:
                    return (Variable)oNode.Parent.Tag;
                case NodeTypes.VariableAction:
                    return (Variable)oNode.Parent.Parent.Tag;
                case NodeTypes.ControlAction:
                    return (Variable)oNode.Parent.Parent.Tag;
                case NodeTypes.VariableActionProperty:
                    return (Variable)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.ControlActionProperty:
                    return (Variable)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableActionParametersCategory:
                    return (Variable)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.ControlActionParametersCategory:
                    return (Variable)oNode.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.VariableActionParameter:
                    return (Variable)oNode.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.ControlActionParameter:
                    return (Variable)oNode.Parent.Parent.Parent.Parent.Parent.Tag;
                case NodeTypes.Value:
                    return null;
                default:
                    return null;
            }
        }
        /// <summary>
        /// gets parent block from designated treenode
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private Block GetBlockFromNode(UltraTreeNode oNode)
        {
            switch (NodeType(oNode))
            {
                //Segment will be stored in top level node tag property
                case NodeTypes.Segment:
                    return null;
                case NodeTypes.Variable:
                    return null;
                case NodeTypes.Block:
                    return (Block)oNode.Tag;
                case NodeTypes.BlockProperty:
                    return (Block)oNode.Parent.Tag;
                case NodeTypes.BlockPropertyValue:
                    return (Block)oNode.Parent.Parent.Tag;
                default:
                    return null;
            }
        }
        /// <summary>
        /// gets parent answer file segment object from designated treenode
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private AnswerFileSegment GetAnswerFileSegmentFromNode(UltraTreeNode oNode)
        {
            switch (NodeType(oNode))
            {
                //Segment will be stored in top level node tag property
                case NodeTypes.AnswerFileSegmentProperty:
                    return (AnswerFileSegment)oNode.Parent.Tag;
                case NodeTypes.AnswerFileSegmentPropertyValue:
                    return (AnswerFileSegment)oNode.Parent.Parent.Tag;
                default:
                    return null;
            }
        }
        /// <summary>
        /// Gets parent variable action from designated treenode
        /// returns null if designed node is parent or above
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private VariableAction GetVariableActionFromNode(UltraTreeNode oNode)
        {
            Variable oVar = null; 
            VariableAction oVarAction = null;
            string xKey = oNode.Key;
            char cSep = '.';

            switch (NodeType(oNode))
            {
                //TODO: replace with if block if calls remain the same
                case NodeTypes.VariableAction:
                case NodeTypes.VariableActionParametersCategory:
                case NodeTypes.VariableActionParameter:
                    oVar = GetVariableFromNode(oNode);
                    break;
                default:
                    break;
            }
                if (oVar != null)
                {
                    //trim segment fields from node key
                    xKey = xKey.Substring(xKey.IndexOf(".Variables") + 1);
                    
                    string[] aKeyItems = xKey.Split(cSep);
                    //get variable action from key.  [3] holds variable action index
                    int iIndex = 3;// aKeyItems.GetUpperBound(0);
                    oVarAction = oVar.VariableActions
                        [Int32.Parse(aKeyItems[iIndex])];
                }
                return oVarAction;
        }
        /// <summary>
        /// Gets parent variable action from designated treenode
        /// returns null if designed node is parent or above
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private SegmentAction GetSegmentActionFromNode(UltraTreeNode oNode)
        {
            Segment oSeg = null;
            SegmentAction oSegAction = null;
            string xKey = oNode.Key;
            char cSep = '.';
            switch (NodeType(oNode))
            {
                //TODO: replace with if block if calls remain the same
                case NodeTypes.SegmentAction:
                case NodeTypes.LanguageAction:
                case NodeTypes.AuthorsAction:
                    oSeg = GetSegmentFromNode(oNode);
                    break;
                case NodeTypes.SegmentActionParametersCategory:
                    oSeg = GetSegmentFromNode(oNode);
                    break;
                case NodeTypes.SegmentActionParameter:
                    oSeg = GetSegmentFromNode(oNode);
                    break;
                default:
                    break;
            }
            if (oSeg != null)
            {
                //trim segment fields from key
                int iIndex = 0;
                if (xKey.Contains("SegmentActions"))
                {
                    xKey = xKey.Substring(xKey.IndexOf("SegmentActions"));
                    iIndex = 2;
                }
                else if (xKey.Contains("LanguageActions"))
                {
                    //there's no segment events level
                    xKey = xKey.Substring(xKey.IndexOf("LanguageActions"));
                    iIndex = 1;
                }
                else if (xKey.Contains("AuthorsActions"))
                {
                    //there's no segment events level
                    xKey = xKey.Substring(xKey.IndexOf("AuthorsActions"));
                    iIndex = 1;
                }
                
                string[] aKeyItems = xKey.Split(cSep);
                //get segment action from key.  aKeyItems[2] holds variable action index
                oSegAction = oSeg.Actions[Int32.Parse(aKeyItems[iIndex])];
            }
            return oSegAction;
        }
        /// <summary>
        /// Gets parent control action from designated treenode
        /// returns null if designed node is parent or above
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private ControlAction GetControlActionFromNode(UltraTreeNode oNode)
        {
            Variable oVar = null;
            Segment oSeg = null;
            ControlAction oCtlAction = null;
            string xKey = oNode.Key;
            char cSep = '.';

            switch (NodeType(oNode))
            {
                //TODO: replace with if block if calls remain the same
                case NodeTypes.ControlAction:
                    oVar = GetVariableFromNode(oNode);
                    break;
                case NodeTypes.ControlActionParametersCategory:
                    oVar = GetVariableFromNode(oNode);
                    break;
                case NodeTypes.ControlActionParameter:
                    oVar = GetVariableFromNode(oNode);
                    break;
                case NodeTypes.SegmentAuthorsControlAction:
                case NodeTypes.SegmentJurisdictionControlAction:
                case NodeTypes.LanguageControlAction:
                    oSeg = GetSegmentFromNode(oNode);
                    break;
                case NodeTypes.SegmentAuthorsControlActionParametersCategory:
                case NodeTypes.SegmentJurisdictionControlActionParametersCategory:
                case NodeTypes.LanguageControlActionParametersCategory:
                    oSeg = GetSegmentFromNode(oNode);
                    break;
                case NodeTypes.SegmentAuthorsControlActionParameter:
                case NodeTypes.SegmentJurisdictionControlActionParameter:
                case NodeTypes.LanguageControlActionParameter:
                    oSeg = GetSegmentFromNode(oNode);
                    break;
                default:
                    break;
            }
            if (oVar != null)
            {
                //trim segment fields from node key
                xKey = xKey.Substring(xKey.IndexOf(".Variables") + 1);
                
                string[] aKeyItems = xKey.Split(cSep);
                //get variable action from key.  [3] holds control action action index
                int iIndex = 3;// aKeyItems.GetUpperBound(0);
                oCtlAction = oVar.ControlActions
                    [Int32.Parse(aKeyItems[iIndex])];
            }
            else if (oSeg != null)
            {
                if (xKey.Contains("CourtChooserControlProperties"))
                {
                    //trim segment fields from key string
                    xKey = xKey.Substring(xKey.IndexOf("CourtChooserControlProperties"));

                    //return CourtChooser control action
                    string[] aKeyItems = xKey.Split(cSep);
                    //get variable action from key.  [3] holds control action action index
                    int iIndex = 2;// aKeyItems.GetUpperBound(0);
                    oCtlAction = oSeg.CourtChooserControlActions
                        [Int32.Parse(aKeyItems[iIndex])];

                }
                else if (xKey.Contains("LanguageControlProperties"))
                {
                    //trim segment fields from key string
                    xKey = xKey.Substring(xKey.IndexOf("LanguageControlProperties"));

                    //return language control action
                    string[] aKeyItems = xKey.Split(cSep);
                    //get variable action from key.  [3] holds control action action index
                    int iIndex = 2;// aKeyItems.GetUpperBound(0);
                    oCtlAction = oSeg.LanguageControlActions
                        [Int32.Parse(aKeyItems[iIndex])];

                }
                else
                {
                    //trim segment fields from key string
                    xKey = xKey.Substring(xKey.IndexOf("AuthorControlProperties"));

                    //return SegmentAuthors control action
                    string[] aKeyItems = xKey.Split(cSep);
                    //get variable action from key.  [3] holds control action action index
                    int iIndex = 2;// aKeyItems.GetUpperBound(0);
                    oCtlAction = oSeg.Authors.ControlActions
                        [Int32.Parse(aKeyItems[iIndex])];
                }
            }
            
            return oCtlAction;
        }
        /// <summary>
        /// returns text value of parameter from specified node
        /// obtains enumeration name for specified value
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private string GetVariableActionParameterTextFromNode(UltraTreeNode oNode)
        {
            string xText = null;
            //get variable action
            VariableAction oVA = GetVariableActionFromNode(oNode);
            if (oVA == null)
                return xText;
            
            char cSep = '.';
            string xKey = oNode.Key;
            //create array of key items
            string[] aKeyItems = xKey.Split(cSep);
            //retrieve the actual property value
            string xValue = GetPropertyValueFromNode(oNode);
            //get the property value - contained at end of key items array
            int iIndex = aKeyItems.GetUpperBound(0);
            string xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

            //retrieve text value depending on variable action type
            //types inside switch block use enumerations - we'll map names to values
            switch (oVA.Type)
            {
                case VariableActions.Types.InsertText:
                case VariableActions.Types.InsertDetail:
                case VariableActions.Types.InsertDetailIntoTable:
                case VariableActions.Types.InsertTextAsTable:
                case VariableActions.Types.InsertDate:
                case VariableActions.Types.InsertSegment:
                    if (xPropName == "Deletion Scope" ||
                        xPropName == "Alternate Format Type")
                    {
                        return GetVariableActionParameterTextFromParameter
                            (xPropName, xValue);
                    }
                    else
                        return xValue;
                case VariableActions.Types.InsertReline:
                    if (xPropName == "Deletion Scope")
                    {
                        return GetVariableActionParameterTextFromParameter
                            (xPropName, xValue);
                    }
                    else if (xPropName == "Default Format")
                    {
                        string xTemp = "";
                        object[,] aOptions = LMP.Controls.RelineFormatOptionsList.List;
                        //Convert FormatOption value to descriptive text
                        xTemp = ConvertBitwiseValueToString(aOptions, Int32.Parse(xValue));
                        //Reformat options to display on single line
                        if (xTemp != "")
                            xTemp = xTemp.Replace("\r\n", ",");
                        else
                            xTemp = "None";
                        return xTemp;
                    }
                    else
                        return xValue;
                case VariableActions.Types.IncludeExcludeText:
                    //delete scope is sole parameter
                    if (xPropName == "Deletion Scope")
                        return GetVariableActionParameterTextFromParameter
                            ("Deletion Scopes", xValue);
                    else
                        return xValue;
                case VariableActions.Types.RunMethod:
                    if (xPropName == "Run Method")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Run Method Types", xValue);
                    }
                    else
                        return xValue;
                case VariableActions.Types.SetAsDefaultValue:
                    if (xPropName == "Key Type")
                    {
                        return GetVariableActionParameterTextFromParameter
                           ("Key Set Types", xValue);
                    }
                    else
                        return xValue;
                default:
                    break;
            }
            return xValue;
        }
        /// <summary>
        /// returns text value of parameter from specified node
        /// obtains enumeration name for specified value
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private string GetControlActionParameterTextFromNode(UltraTreeNode oNode)
        {
            string xText = null;
            //get variable action
            ControlAction oCA = GetControlActionFromNode(oNode);
            if (oCA == null)
                return xText;

            char cSep = '.';
            string xKey = oNode.Key;
            //create array of key items
            string[] aKeyItems = xKey.Split(cSep);
            //retrieve the actual property value
            string xValue = GetPropertyValueFromNode(oNode);
            //get the property value - contained at end of key items array
            int iIndex = aKeyItems.GetUpperBound(0);
            string xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

            //retrieve text value depending on control action type
            //types inside switch block use enumerations - we'll map names to values
            switch (oCA.Type)
            {
                case ControlActions.Types.Enable:
                    if (xPropName == "Target Variable" ||
                        xPropName == "Enable")
                    {
                        return GetVariableActionParameterTextFromParameter
                            (xPropName, xValue);
                    }
                    break;
                case ControlActions.Types.SetVariableValue:
                    if (xPropName == "Target Variable" ||
                        xPropName == "New Value")
                    {
                        return GetVariableActionParameterTextFromParameter
                            (xPropName, xValue);
                    }
                    break;
                case ControlActions.Types.SetDefaultValue:
                    if (xPropName == "Key Type")
                    {
                        return GetVariableActionParameterTextFromParameter
                           ("Key Set Types", xValue);
                    }
                    else
                        return xValue;
                case ControlActions.Types.RunMethod:
                    if (xPropName == "Run Method")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Run Method Types", xValue);
                    }
                    else
                        return xValue;
                default:
                    break;
            }
            return xValue;
        }
        /// <summary>
        /// returns text value of parameter from specified node
        /// obtains enumeration name for specified value
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private string GetSegmentActionParameterTextFromNode(UltraTreeNode oNode)
        {
            string xText = null;
            //get variable action
            SegmentAction  oSegA = GetSegmentActionFromNode(oNode);
            if (oSegA == null)
                return xText;

            char cSep = '.';
            string xKey = oNode.Key;
            //create array of key items
            string[] aKeyItems = xKey.Split(cSep);
            //retrieve the actual property value
            string xValue = GetPropertyValueFromNode(oNode);

            //property name is aKeyItems[upperbound] + "_Value"
            int iIndex = aKeyItems.GetUpperBound(0);
            string xPropName = aKeyItems[iIndex].Substring(0, aKeyItems[iIndex].IndexOf("_"));

            //retrieve text value depending on variable action type
            //types inside switch block use enumerations - we'll map names to values
            switch (oSegA.Type)
            {
                case VariableActions.Types.InsertText:
                    if (xPropName == "Deletion Scope")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Deletion Scopes", xValue);
                    }
                    else
                        return xValue;
                case VariableActions.Types.InsertReline:
                    if (xPropName == "Deletion Scope")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Deletion Scopes", xValue);
                    }
                    else if (xPropName == "Default Format")
                    {
                        string xTemp = "";
                        object[,] aOptions = LMP.Controls.RelineFormatOptionsList.List;
                        xTemp = ConvertBitwiseValueToString(aOptions, Int32.Parse(xValue));
                        if (xTemp != "")
                        {
                            xTemp = xTemp.TrimEnd("\r\n".ToCharArray());
                            xTemp = xTemp.Replace("\r\n", ",");
                        }
                        else
                            xTemp = "None";
                        return xTemp;
                    }
                    else
                        return xValue;
                case VariableActions.Types.InsertDetail:
                case VariableActions.Types.InsertDetailIntoTable:
                    if (xPropName == "Deletion Scope")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Deletion Scopes", xValue);
                    }
                    else
                        return xValue;
                case VariableActions.Types.IncludeExcludeText:
                    if (xPropName == "Deletion Scope")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Deletion Scopes", xValue);
                    }
                    else
                        return xValue;
                case VariableActions.Types.RunMethod:
                    if (xPropName == "Run Method")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Run Method Types", xValue);
                    }
                    else
                        return xValue;
                case VariableActions.Types.SetAsDefaultValue:
                    if (xPropName == "Key Type")
                    {
                        return GetVariableActionParameterTextFromParameter
                           ("Key Set Types", xValue);
                    }
                    else
                        return xValue;
                case VariableActions.Types.InsertTextAsTable:
                    if (xPropName == "Deletion Scope")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Deletion Scopes", xValue);
                    }
                    else
                        return xValue;
                case VariableActions.Types.InsertDate:
                    if (xPropName == "Deletion Scope")
                    {
                        return GetVariableActionParameterTextFromParameter
                            ("Deletion Scopes", xValue);
                    }
                    else
                        return xValue;
                //case VariableActions.Types.InsertBarCode:
                //This action no longer supported
                //    if (xPropName == "Position")
                //    {
                //        return GetVariableActionParameterTextFromParameter
                //           ("Bar Code Positions", xValue);
                //    }
                //    else
                //        return xValue;
                default:
                    break;
            }
            return xValue;
        }
        /// <summary>
        /// returns enum item name from associated value
        /// uses hard coded arrays from GetPropertySettingsList
        /// </summary>
        /// <param name="xParameterName"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        private string GetVariableActionParameterTextFromParameter(string xParameterName, string xValue)
        {
            string xText = null;
            string[,] aTemp = GetPropertySettingsList(xParameterName);
            for (int i = 0; i <= aTemp.GetUpperBound(0); i++)
            {
                if (aTemp[i, 1] == xValue)
                {
                    return aTemp[i, 0];
                }
            }
            return xText;
        }
        /// <summary>
        /// sets up appropriate controls for designated segment property
        /// </summary>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private object SetupSegmentPropertyControls(string xPropName, string xPropValue)
        {
            //create resource identifier for help text
            string xHelp = DESIGN_HELP_PREF_SEG + xPropName;
            try
            {
                Control oCtl = null;
                LMP.Controls.ComboBox oCombo = null;

                //create new control
                switch (xPropName)
                {
                    case "DisplayName":
                    case "DialogCaption":
                    case "DialogTabCaptions":
                    case "ProtectedFormPassword":
                        oCtl = new LMP.Controls.TextBox();
                        break;
                    case "ShowChooser":
                    case "IsTransparentDef":
                    case "DisplayWizard":
                    case "ShowCourtChooser":
                    case "ShowSegmentDialog":
                    case "AllowSideBySide":
                        oCtl = new LMP.Controls.BooleanComboBox();
                        break;
                    case "LinkAuthorsToParentDef":
                        oCombo = new LMP.Controls.ComboBox();
                        //GLOG 3413: Enforce selection of valid list item
                        oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
                        oCtl = oCombo;
                        LoadComboList(oCombo, "Link Authors To Parent", true);
                        break;
                    case "MaxAuthors":
                        LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                        oCtl = oSpinner;
                        ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                        break;
                    case "TypeID":
                        oCombo = new LMP.Controls.ComboBox();
                        //GLOG 3413: Enforce selection of valid list item
                        oCombo.DropDownStyle = ComboBoxStyle.DropDownList;
                        oCtl = oCombo;
                        LoadComboList(oCombo, "Segment Types", true);
                        break;
                    case "JurisdictionDefaults":
                        oCtl = new LMP.Controls.JurisdictionChooser();
                        LoadJurisdictionChooser(((Segment)GetSegmentFromNode(m_oCurNode)), 
                            (LMP.Controls.JurisdictionChooser)oCtl);
                        break;
                    case "AuthorControlProperties":
                        oCtl = new LMP.Controls.ControlPropertiesGrid();
                        ControlPropertiesGrid oCPGrid = (ControlPropertiesGrid)oCtl;
                        oCPGrid.LoadDefaultValues(LMP.Data.mpControlTypes.AuthorSelector);
                        //set height based on loaded default values, plus col header and empty grid row below
                        oCPGrid.Height = ((oCPGrid.GetDefaultValues(LMP.Data.mpControlTypes.AuthorSelector))
                            .Split(StringArray.mpEndOfSubValue).GetUpperBound(0) + 2) * 22; 
                        break;
                    case "CourtChooserControlProperties":
                        oCtl = new LMP.Controls.ControlPropertiesGrid();
                        oCPGrid = (ControlPropertiesGrid)oCtl;
                        oCPGrid.LoadDefaultValues(LMP.Data.mpControlTypes.JurisdictionChooser);
                        //set height based on loaded default values, plus col header and empty grid row below
                        oCPGrid.Height = ((oCPGrid.GetDefaultValues(LMP.Data.mpControlTypes.JurisdictionChooser))
                            .Split(StringArray.mpEndOfSubValue).GetUpperBound(0) + 2) * 22;
                        break;
                    case "MenuInsertionOptions":
                        oCtl = new LMP.Controls.InsertionLocationsList();
                        //set control height based on number of list items
                        // GLOG : 3324 : JAB
                        // Size the control such that no vertical scrollbar is visible.
                        oCtl.Height = 180;
                        break;
                    case "DefaultMenuInsertionBehavior":
                    case "DefaultDragBehavior":
                    case "DefaultDoubleClickBehavior":
                        oCtl = new LMP.Controls.InsertionBehaviorsList();
                        //set control height based on number of list items
                        oCtl.Height = LMP.Controls.InsertionBehaviorsList.List.GetUpperBound(0) * 30;
                        break;
                    case "DefaultDragLocation":
                    case "DefaultDoubleClickLocation":
                        oCtl = new LMP.Controls.DefaultLocationComboBox();
                        break;
                    case "HelpText":
                        //GLOG : 8071 : jsw
                        if (GetSegmentFromNode(m_oCurNode) is AdminSegment ||
                                m_bIsContentDesignerSegment)
                        {
                            oCtl = new LMP.Controls.HTMLTextBox();
                        }
                        else
                        {
                            oCtl = new LMP.Controls.MultilineTextBox();
                            oCtl.Height = 110;
                        }
                        break;
 
                    case "AuthorsHelpText":
                    case "CourtChooserHelpText":
                        if (GetSegmentFromNode(m_oCurNode) is AdminSegment ||
                                m_bIsContentDesignerSegment)
                        {
                            oCtl = new LMP.Controls.HTMLTextBox();
                        }
                        else
                        {
                            oCtl = new LMP.Controls.MultilineTextBox();
                            oCtl.Height = 44;
                        }
                        break;
                    case "AuthorNodeUILabel":
                        oCombo = new LMP.Controls.ComboBox();
                        //GLOG 3413: Enforce selection of valid list item
                        oCombo.DropDownStyle = ComboBoxStyle.DropDown;
                        oCombo.AllowEmptyValue = true;
                        oCtl = oCombo;
                        LoadComboList(oCombo, "AuthorNodeUILabel", false);
                        break;
                    case "DefaultWrapperID":
                        oCtl = new LMP.Controls.SegmentSelector();
                        break;
                    case "RequiredStyles":
                        oCtl = new LMP.Controls.MultiStringSelector(this.m_oMPDocument.GetStyleArray(true), 
                            LMP.Resources.GetLangString("Dialog_SelectStyles_Title"), 
                            LMP.Resources.GetLangString("Dialog_SelectStyles_Description"));
                        break;
                    case "IntendedUse":
                        object oSegment = GetSegmentFromNode(this.m_oCurNode);
                        oCtl = new LMP.Controls.IntendedUsesComboBox(oSegment is LMP.Architect.Api.UserSegment);
                        break;
                    case "DialogHeight":
                        oSpinner = new LMP.Controls.Spinner();
                        oCtl = oSpinner;
                        ConfigureSpinner(oSpinner, 0, 5, 800, 250);
                        break;
                    case "DefaultTrailerID":
                        Segment oSeg = GetSegmentFromNode(m_oCurNode);
                        Chooser oChooser = new Chooser();
                        oChooser.AllowEmptyValue = false;
                        oChooser.LimitToList = true;
                        //GLOG 3413: Enforce selection of valid list item
                        oChooser.DropDownStyle = ComboBoxStyle.DropDownList;
                        Assignments oAssigned = new Assignments(oSeg.TypeID, oSeg.ID1, mpObjectTypes.Trailer);
                        ArrayList oList = null;
                        if (oAssigned.Count > 0)
                        {
                            oList = oAssigned.ToArrayList();
                        }
                        else
                        {
                            //If no Segment-specific defaults, use Firm Application setting
                            FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                            AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.Trailer);
                            if (oSettings.DefaultTrailerIDs.GetLength(0) > 0)
                            {
                                oList = new ArrayList();
                                for (int i = 0; i < oSettings.DefaultTrailerIDs.GetLength(0); i++)
                                {
                                    int iID = oSettings.DefaultTrailerIDs[i];
                                    AdminSegmentDef oDef = null;
                                    try
                                    {
                                        oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                                    }
                                    catch { }
                                    if (oDef != null)
                                    {
                                        object[] oItem = new object[] { oDef.ID, oDef.DisplayName };
                                        oList.Add(oItem);
                                    }
                                }

                            }
                            else
                            {
                                oList = oDefs.ToArrayList(0, 1);
                            }
                        }

                        
                        //Add No Trailer item
                        object[] oNoTrailer = new object[] { DocumentStampUI.mpNoTrailerPromptSegmentID, 
                            LMP.Resources.GetLangString("Prompt_NoTrailerDoNotPrompt") };
                        oList.Add(oNoTrailer);
                        oNoTrailer = new object[] { DocumentStampUI.mpSkipTrailerSegmentID, LMP.Resources.GetLangString("Prompt_NoTrailerIntegration") };
                        oList.Insert(0, oNoTrailer);
                        oNoTrailer = new object[] { 0, "No Default" };
                        oList.Insert(0, oNoTrailer);
                        oChooser.SetList(oList);
                        oCtl = oChooser;
                        break;
                    case "SupportedLanguages":
                        oCtl = new LMP.Controls.SupportedLanguagesList();
                        //set control height based on number of list items
                        oCtl.Height = LMP.Controls.SupportedLanguagesList.List.GetUpperBound(0) * 30;
                        break;
                    case "Culture":
                        oSeg = GetSegmentFromNode(m_oCurNode);
                        oCtl = new LMP.Controls.LanguagesComboBox(oSeg.SupportedLanguages,
                            LMP.StringArray.mpEndOfValue);
                        break;
                    default:
                        oCtl = CreateExpressionTextBox();
                        break;
                }
                //subscribe to tab pressed event
                IControl oICtl = (IControl)oCtl;
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                oICtl.ExecuteFinalSetup();

                oCtl.SuspendLayout();

                try
                {
                    //add resource ID string as tag for later retrieval
                    if (!(oCtl == null))
                        oCtl.Tag = xHelp;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// sets up appropriate controls for designated segment property
        /// </summary>
        /// <param name="xPropName"></param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private object SetupAnswerFileSegmentPropertyControls(string xPropName, string xPropValue)
        {
            //create resource identifier for help text
            string xHelp = DESIGN_HELP_PREF_ANSWERFILESEG + xPropName;
            try
            {
                Control oCtl = null;

                //create new control
                switch (xPropName)
                {
                    case "InsertionBehavior":
                        oCtl = new LMP.Controls.InsertionBehaviorsList();
                        //set control height based on number of list items
                        oCtl.Height = LMP.Controls.InsertionBehaviorsList.List.GetUpperBound(0) * 30;
                        break;
                    case "InsertionLocation":
                        oCtl = new LMP.Controls.DefaultLocationComboBox();
                        break;
                    default:
                        oCtl = CreateExpressionTextBox();
                        break;
                }
                //subscribe to tab pressed event
                IControl oICtl = (IControl)oCtl;
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
                oCtl.SuspendLayout();

                try
                {
                    //add resource ID string as tag for later retrieval
                    if (!(oCtl == null))
                        oCtl.Tag = xHelp;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// sets up editing controls for variable property
        /// value nodes
        /// </summary>
        /// <param name="xPropName">variable property</param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private Control SetupVariablePropertyControls(string xPropName, string xPropValue)
        {
            //create resource identifier for help text
            string xHelp = DESIGN_HELP_PREF_VAR + xPropName;
            //identify Admin Segment
            bool bAdminSegment = GetSegmentFromNode(m_oCurNode) is AdminSegment;   
            
            try
            {
                Control oCtl = null;

                //create new control
                switch (xPropName)
                {
                    case "Name":
                        oCtl = new LMP.Controls.TextBox();
                        break;
                    case "DisplayName":
                        oCtl = new LMP.Controls.TextBox();
                        break;
                    case "HotKey":
                        LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                        oCtl = oCombo;
                        LoadComboList(oCombo, "Hot Keys", true, false, true);
                        break;
                    case "DisplayValue":
                        oCtl = CreateExpressionTextBox();
                        break;
                    case "DefaultValue":
                        //show expression textbox only if
                        //admin or content designer
                        if (bAdminSegment || m_bIsContentDesignerSegment)
                            oCtl = CreateExpressionTextBox();
                        else
                            oCtl = new LMP.Controls.TextBox();
                        break;
                    case "SecondaryDefaultValue":
                        oCtl = CreateExpressionTextBox();
                        break;
                    case "ValueSourceExpression":
                        oCtl = CreateExpressionTextBox();
                        break;
                    case "ValidationCondition":
                        oCtl = CreateExpressionTextBox();
                        break;
                    case "ValidationResponse":
                        oCtl = CreateExpressionTextBox();
                        break;
                    case "ControlType":
                        oCombo = new LMP.Controls.ComboBox();
                        oCtl = oCombo;
                        Variable oVar = GetVariableFromNode(m_oCurNode);
                        if (oVar != null && oVar.IsMultiValue)
                            //Show limited list for Multivalue variable
                            LoadComboList(oCombo, "Multivalue Control Types", true);
                        else
                        LoadComboList(oCombo, "Control Types", true);
                        break;
                    case "DisplayLevel":
                        oCombo = new LMP.Controls.ComboBox();
                        oCtl = oCombo;
                        LoadComboList(oCombo, "Display Levels", true);
                        break;
                    case "DisplayIn":
                        oCtl = new LMP.Controls.DisplayLocationsList();
                        //JTS 2/5/09: Height increased to display all three options
                        oCtl.Height = 55;
                        break;
                    case "HelpText":
                        oCtl = new LMP.Controls.HTMLTextBox();
                        break;
                    case "ControlProperties":
                        oCtl = new LMP.Controls.ControlPropertiesGrid();
                        ControlPropertiesGrid oGrid = (ControlPropertiesGrid)oCtl;
                        //JTS 2/5/09: Value passed no longer contains linebreaks
                        //set height based on number of properties, plus col header and empty grid row below
                        oGrid.Height = (xPropValue.Split(StringArray.mpEndOfSubValue).GetUpperBound(0) + 3) * 22;
                        break;
                    case "MustVisit":
                        oCtl = new LMP.Controls.BooleanComboBox();
                        break;
                    case "AllowPrefillOverride":
                        //GLOG 3475: Changed from boolean to Enum
                        LMP.Controls.ComboBox oCombo1 = new LMP.Controls.ComboBox();
                        oCtl = oCombo1;
                        LoadComboList(oCombo1, "Prefill Override Types", true, false, false);
                        break;
                    case "IsMultiValue":
                        oCtl = new LMP.Controls.BooleanComboBox();
                        ((BooleanComboBox)oCtl).TrueString = "Multiple Values";
                        ((BooleanComboBox)oCtl).FalseString = "Single Value";
                        break;
                    case "AssociatedPrefillNames":
                        oCtl = new LMP.Controls.TextBox();
                        break;
                    case "TabNumber":
                        LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                        oCtl = oSpinner;
                        ConfigureSpinner(oSpinner, 0, 1, 20, 1);
                        break;
                    //GLOG 1408
                    case "NavigationTag":
                        oCtl = CreateExpressionTextBox(); //GLOG 4
                        break;
                    default:
                        oCtl = CreateExpressionTextBox();
                        break;
                }
                //subscribe to tab pressed event
                IControl oICtl = (IControl)oCtl;
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                oICtl.ExecuteFinalSetup();

                oCtl.SuspendLayout();

                try
                {
                    //add resource ID string as tag for later retrieval
                    if (!(oCtl == null))
                        oCtl.Tag = xHelp;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// sets up editing controls for variable CI property
        /// value nodes
        /// </summary>
        /// <param name="xPropName">variable property</param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private Control SetupCIPropertyControls(string xPropName, string xPropValue)
        {
            //create resource identifier for help text
            string xHelp = DESIGN_HELP_PREF_VAR + xPropName;
            try
            {
                Control oCtl = null;

                //create new control
                switch (xPropName)
                {
                    case "RequestCIForEntireSegment":
                        oCtl = new LMP.Controls.BooleanComboBox();
                        break;
                    case "CIContactType":
                        LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                        oCtl = oCombo;
                        LoadComboList(oCombo, "CI Contact Types", true);
                        break;
                    case "CIDetailTokenString":
                        oCtl = CreateExpressionTextBox();
                        break;
                    case "CIAlerts":
                        oCtl = new LMP.Controls.CIAlertsList();
                        //set control height based on number of list items
                        oCtl.Height = LMP.Controls.CIAlertsList.List.GetUpperBound(0) * 20;
                        break;
                    default:
                        oCtl = CreateExpressionTextBox();
                        break;
                }
                //subscribe to tab pressed event
                IControl oICtl = (IControl)oCtl;
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                oICtl.ExecuteFinalSetup();

                oCtl.SuspendLayout();

                try
                {
                    //add resource ID string as tag for later retrieval
                    if (!(oCtl == null))
                        oCtl.Tag = xHelp;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// sets up editing controls for block property
        /// value nodes
        /// </summary>
        /// <param name="xPropName">block property name</param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private Control SetupBlockPropertyControls(string xPropName, string xPropValue)
        {
            //create resource identifier for help text
            string xHelp = DESIGN_HELP_PREF_BLOCK + xPropName;
            try
            {
                Control oCtl = null;

                //create new control
                switch (xPropName)
                {
                    case "Name":
                        oCtl = new LMP.Controls.TextBox();
                        break;
                    case "DisplayName":
                        oCtl = new LMP.Controls.TextBox();
                        break;
                    case "IsBody":
                        oCtl = new LMP.Controls.BooleanComboBox();
                        break;
                    case "ShowInTree":
                        oCtl = new LMP.Controls.BooleanComboBox();
                        break;
                    case "HelpText":
                        // GLOG : 3343 : JAB
                        // Block help text uses an html textbox.
                        oCtl = new LMP.Controls.HTMLTextBox();
                        break;
                    case "DisplayLevel":
                        oCtl = new LMP.Controls.ComboBox();
                        LoadComboList((LMP.Controls.ComboBox)oCtl, "Display Levels", true);
                        break;
                    case "HotKey":
                        LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                        oCtl = oCombo;
                        LoadComboList(oCombo, "Hot Keys", true, false, true);
                        break;
                    default:
                        oCtl = CreateExpressionTextBox();
                        break;
                }
                //subscribe to tab pressed event
                IControl oICtl = (IControl)oCtl;
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                oICtl.ExecuteFinalSetup();

                oCtl.SuspendLayout();

                try
                {
                    //add resource ID string as tag for later retrieval
                    if (!(oCtl == null))
                        oCtl.Tag = xHelp;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// sets up editing controls for variable action property
        /// value nodes
        /// </summary>
        /// <param name="xPropName">variable action property name</param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private Control SetupVariableActionPropertyControls(string xPropName, string xPropValue)
        {
            //create resource identifier for help text
            string xHelp = DESIGN_HELP_PREF_VARACTPROP + xPropName;
            try
            {
                if (xPropName == "Parameters")
                    return null;

                Control oCtl = null;

                //create new control
                switch (xPropName)
                {
                    case "ExecutionCondition":
                        oCtl = CreateExpressionTextBox();
                        break;
                    case "ValueSetID":
                        LMP.Controls.ListCombo oLC = new LMP.Controls.ListCombo();
                        oLC.LimitToList = true;
                        oLC.AllowEmptyValue = true;
                        oLC.ValueSetListsOnly = true;
                        oCtl = oLC;
                        break;
                }


                oCtl.SuspendLayout();

                try
                {
                    return oCtl;
                }
                finally
                {
                    //add resource ID string as tag for later retrieval
                    if (!(oCtl == null))
                        oCtl.Tag = xHelp;
                    //subscribe to tab pressed event
                    IControl oICtl = (IControl)oCtl;

                    oICtl.ExecuteFinalSetup();
                    oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// sets up editing controls for variable actions parameters
        /// value nodes
        /// </summary>
        /// <param name="xPropName">block property name</param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private Control SetupVariableActionParameterControls(string xPropName, string xPropValue)
        {
            try
            {
                Control oCtl = null;
                string xHelp = null;
                string xActionName = GetActionNameFromKey(xPropName);
                //create resource identifier for help text
                xHelp = DESIGN_HELP_PREF_VARACTPARAM + xPropName.Replace(" ", null);

                switch (xActionName)
                {
                    case "ExecuteOnApplication":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Command String":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "ExecuteOnBookmark":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Bookmark Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Command String":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "ExecuteOnTag":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Command String":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Variable Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "ExecuteOnBlock":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Command String":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Block Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "ExecuteOnDocument":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Command String":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }    
                        break;

                    case "ExecuteOnSegment": //GLOG 7229
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Command String":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Target Segment ID":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "ExecuteOnPageSetup":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Section Index":
                                LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Command String":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "ExecuteOnStyle":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Style Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Command String":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "IncludeExcludeText":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Deletion Scope":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Deletion Scopes", true);
                                break;
                            case "Delimiter Replacement":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "IncludeExcludeBlocks":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Block Names":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "InsertCheckbox":
                        break;
                    case "InsertDate":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Underline Length":
                                LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Deletion Scope":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Deletion Scopes", true);
                                break;
                            case "Prefix":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "InsertText":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Delimiter Replacement":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Underline Length":
                                LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Deletion Scope":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Deletion Scopes", true);
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetPaperSource":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Set Page One":
                            case "Set Other Pages":
                                oCtl = new LMP.Controls.BooleanComboBox();
                                break;
                            case "Expression": //GLOG 4752
                                oCtl = CreateExpressionTextBox();
                                break;
                        }
                        break;
                    case "InsertDetail":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Item Separator":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Entity Separator":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Underline Length":
                                LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Alternate Format Type":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Alternate Format Types", true);
                                break;
                            case "Alternate Format Threshold":
                                oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Deletion Scope":
                                oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Deletion Scopes", true);
                                break;
                            case "Template":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox(); ;
                                break;
                            default:
                                break;
                        }
                        break;
                    case "InsertDetailIntoTable":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Template":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "FirstDetailRow":
                                Spinner oSpn = new Spinner();
                                oSpn.Maximum = 10;
                                oSpn.Minimum = 1;
                                oSpn.Increment = 1;
                                oCtl = oSpn;
                                break;
                            default:
                                break;
                        }
                        break;
                    case "InsertReline":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Standard Delimiter":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Special Separator":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Special Delimiter":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Deletion Scope":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Deletion Scopes", true);
                                break;
                            case "Label Text":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Default Format":
                                oCtl = new LMP.Controls.RelineFormatOptionsList();
                                oCtl.Height = LMP.Controls.RelineFormatOptionsList.List.GetUpperBound(0) * 20;
                                break;
                            default:
                                break;
                        }
                        break;
                    case "InsertSegment":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Underline Length":
                                LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Deletion Scope":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Deletion Scopes", true);
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "InsertTextAsTable":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Item Separator":
                                //TODO: use a combo loaded with allowed separators, list-limited
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Underline Length":
                                LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Deletion Scope":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Deletion Scopes", true);
                                break;
                            case "Number of Columns":
                                oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "RunMacro":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Macro Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Arguments":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "RunMethod":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Type":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Run Method Types", true);
                                break;
                            case "Server Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Class Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Method Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Arguments":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "ReplaceSegment":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Existing Segment ID":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "New Segment ID":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Load Current Values":
                                //GLOG 3247: New action parameter
                                oCtl = new BooleanComboBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetAsDefaultValue":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Key Type":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Key Set Types", true);
                                break;
                            case "Key Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetDocPropValue":  //GLOG 7495
                        switch(GetPropNameFromKey(xPropName))
                        {
                            case "Property Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetDocVarValue":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetVariableValue":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "RunVariableActions":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Names":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetupLabelTable":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Starting Position":
                            case "Full Page":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Target Variable":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetupDetailTable":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Target Variable":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "First Position":
                            case "Header Rows":
                                LMP.Controls.Spinner oSpinner = new LMP.Controls.Spinner();
                                oCtl = oSpinner;
                                ConfigureSpinner(oSpinner, 0, 1, 1000, 0);
                                break;
                            case "Minimum Rows":
                            case "Maximum Rows":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Full Row Format":
                                oCtl = new BooleanComboBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetupDistributedSections":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Reference Variable":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Maximum Sections":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    //GLOG 3220
                    case "ApplyStyleSheet":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;

                            default:
                                break;
                        }
                        break;
                    //GLOG 3883
                    case "UnderlineToLongest":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Apply Underline":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Variable Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "InsertTOA":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Block Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Use Passim":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                }
                //subscribe to tab pressed event
                IControl oICtl = (IControl)oCtl;
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                oICtl.ExecuteFinalSetup();
                oCtl.SuspendLayout();

                try
                {
                    //add resource ID string as tag for later retrieval
                    if (!(oCtl == null))
                        oCtl.Tag = xHelp;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// sets up editing controls for control actions parameters
        /// value nodes
        /// </summary>
        /// <param name="xPropName">block property name</param>
        /// <param name="xPropValue"></param>
        /// <returns></returns>
        private Control SetupControlActionParameterControls(string xPropName, string xPropValue)
        {
            try
            {
                Control oCtl = null;
                string xHelp = null;
                string xActionName = GetActionNameFromKey(xPropName);

                //create resource identifier for help text
                xHelp = DESIGN_HELP_PREF_CTLACTPARAM + xPropName.Replace(" ", null);

                switch (xActionName)
                {
                    case "Enable":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Enable Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "Visible":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Names":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Visibility Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetVariableValue":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "ChangeLabel":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "New Label":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetFocus":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "RunMethod":
                        switch (GetPropNameFromKey(xPropName))
                        {                       
                            case "Type":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Run Method Types", true);
                                break;
                            case "Server Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Class Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Method Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Arguments":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "SetDefaultValue":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Key Type":
                                LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
                                oCtl = oCombo;
                                LoadComboList(oCombo, "Key Set Types", true);
                                break;
                            case "Key Name":
                                oCtl = new LMP.Controls.TextBox();
                                break;
                            case "Expression":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "DisplayMessage":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Message Text":
                                oCtl = CreateExpressionTextBox();
                                break;
                            case "Caption":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    case "RefreshControl":
                        switch (GetPropNameFromKey(xPropName))
                        {
                            case "Variable Name":
                                oCtl = CreateExpressionTextBox();
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                //subscribe to tab pressed event
                IControl oICtl = (IControl)oCtl;
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                oICtl.ExecuteFinalSetup();

                oCtl.SuspendLayout();

                try
                {
                    //add resource ID string as tag for later retrieval
                    if (!(oCtl == null))
                        oCtl.Tag = xHelp;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    xPropName, oE);
            }
        }
        /// <summary>
        /// returns 2-dimensional array of VariableAction Parameter names and default values
        /// </summary>
        /// <param name="oTypeName"></param>
        /// <param name="oParameters"></param>
        /// <returns></returns>
        private string[,] GetVariableActionParameters(VariableActions.Types iActionType)
        {
            switch (iActionType)
            {
                case VariableActions.Types.ExecuteOnApplication:
                    return new string[,] {  {"Command String", "" } };
                case VariableActions.Types.ExecuteOnBookmark:
                    return new string[,] {  {"Bookmark Name", ""},
                                            {"Command String", ""}};
                case VariableActions.Types.ExecuteOnDocument:
                    return new string[,] {  {"Command String", "" } };
                case VariableActions.Types.ExecuteOnPageSetup:
                    return new string[,] {  {"Section Index", "1"},
                                            {"Command String", ""}};
                case VariableActions.Types.ExecuteOnStyle:
                    return new string[,] {  {"Style Name", "Normal"},
                                            {"Command String", ""}};
                case VariableActions.Types.IncludeExcludeText:
                    return new string[,] {  {"Deletion Scope", "0"},
                                            {"Delimiter Replacement", ""},
                                            {"Expression", ""}};
                case VariableActions.Types.IncludeExcludeBlocks:
                    return new string[,] {  {"Block Names", ""},
                                            {"Expression", ""}};
                case VariableActions.Types.InsertCheckbox:
                    return null;
                    //return new string[,] {  {"Checkbox Font", "Wingdings"},
                    //                        {"Checkbox Checked ASCII", "254"},
                    //                        {"Checkbox Unchecked ASCII", "168"}};
                case VariableActions.Types.InsertDate:
                    return new string[,] {  {"Underline Length", "0"},
                                            {"Deletion Scope", "0"},
                                            {"Prefix", ""},
                                            {"Expression", ""}};
                //case VariableActions.Types.InsertBoilerplateAtLocation:
                    //TODO: this action type hasn't been built
                    //return new string[] {"Delimiter Replacement",
                    //                    "Underline Length",
                    //                    "Deletion Scope",
                    //                    "Expression"};
                //case VariableActions.Types.InsertBoilerplateAtTag:
                    //TODO: this action type hasn't been built
                    //return new string[] {"Delimiter Replacement",
                    //                    "Underline Length",
                    //                    "Deletion Scope",
                    //                    "Expression"};
                case VariableActions.Types.InsertText:
                    return new string[,] {  {"Delimiter Replacement", ""},
                                            {"Underline Length", "0"},
                                            {"Deletion Scope", "0"},
                                            {"Expression", ""}};
                case VariableActions.Types.InsertTextAsTable:
                    return new string[,] {  {"Item Separator", "~"},
                                            {"Underline Length", "0"},
                                            {"Deletion Scope", "0"},
                                            {"Number of Columns", "2"},
                                            {"Expression", ""}};
                case VariableActions.Types.InsertDetail:
                    return new string[,] {  {"Item Separator", "[Char_11]"},
                                            {"Entity Separator", "[Char_13]"},
                                            {"Template", ""},
                                            {"Alternate Format Type", "0"},
                                            {"Alternate Format Threshold", "0"},
                                            {"Underline Length", "0"},
                                            {"Deletion Scope", "0"},
                                            {"Expression", ""}};
                case VariableActions.Types.InsertDetailIntoTable:
                    return new string[,] {  {"Template", ""},
                                            {"FirstDetailRow", "2"}};
                case VariableActions.Types.InsertSegment:
                    return new string[,] {  {"Underline Length", "0"},
                                            {"Deletion Scope", "0"}, 
                                            {"Expression", ""}};
                case VariableActions.Types.InsertReline:
                    return new string[,] {  {"Label Text", "Re:[Char_9]"},
                                            {"Standard Delimiter", "[Char_13][Char_9]"},
                                            {"Special Separator", "[Char_9]:[Char_9]"},
                                            {"Special Delimiter", "[Char_11]"},
                                            {"Deletion Scope", "0"},
                                            {"Default Format", "0"}};
                case VariableActions.Types.RunMacro:
                    return new string[,] {  {"Macro Name", ""},
                                            {"Arguments", ""}};
                case VariableActions.Types.RunMethod:
                    return new string[,] {  {"Type", "1"},
                                            {"Server Name", ""},
                                            {"Class Name", ""},
                                            {"Method Name", ""},
                                            {"Arguments", ""}};
                case VariableActions.Types.SetAsDefaultValue:
                    return new string[,] {  {"Key Name", ""},
                                            {"Key Type", "0"},
                                            {"Expression", ""}};
                case VariableActions.Types.SetDocVarValue:
                    return new string[,] {  {"Variable Name", ""},
                                            {"Expression", ""}};
                case VariableActions.Types.SetDocPropValue: //GLOG 7495
                    return new string[,] {  {"Property Name", ""},
                                            {"Expression", ""}};
                case VariableActions.Types.SetVariableValue:
                    return new string[,] {  {"Variable Name", ""},
                                            {"Expression", ""}};
                case VariableActions.Types.ReplaceSegment:
                    return new string[,] { {"Existing Segment ID", ""},
                                           {"New Segment ID", ""},
                                           {"Load Current Values", ""}};
                case VariableActions.Types.ExecuteOnTag:
                    return new string[,] { { "Command String", "" },
                                           {"Variable Name", ""} };
                case VariableActions.Types.ExecuteOnBlock:
                    return new string[,] { { "Command String", "" },
                                           {"Block Name", ""} };
                case VariableActions.Types.RunVariableActions:
                    return new string[,] { { "Variable Names", "" } };
                case VariableActions.Types.SetupLabelTable:
                    return new string[,] {  {"Target Variable", ""},
                                            {"Starting Position", "1"},
                                            {"Full Page", ""}};
                //case VariableActions.Types.InsertBarCode:
                //This action no longer supported
                //    return new string[,] {  {"Address Variable", ""},
                //                            {"Position", "0"},
                //                            {"Extra Space", "6"}};
                case VariableActions.Types.SetupDetailTable:
                    return new string[,] {  {"Target Variable", ""},
                                            {"First Position", "1"},
                                            {"Full Row Format", "false"},
                                            {"Header Rows", "0"},
                                            {"Minimum Rows", ""},
                                            {"Maximum Rows", ""}};
                case VariableActions.Types.SetupDistributedSections:
                    return new string[,] {  {"Reference Variable", ""},
                                            {"Maximum Sections", ""}};
                case VariableActions.Types.SetPaperSource:
                    return new string[,] {  {"Set Page One", "true"},
                                            {"Set Other Pages", "true"},
                                            {"Expression", ""}}; //GLOG 4752
                //GLOG 3220
                case VariableActions.Types.ApplyStyleSheet:
                    return new string[,] {  {"Expression", ""}};
                //GLOG 3220
                case VariableActions.Types.UnderlineToLongest:
                    return new string[,] { { "Apply Underline", "" }, 
                                           { "Variable Name", "" }};
                case VariableActions.Types.InsertTOA:
                    return new string[,] { { "Block Name", "" },
                                           { "Use Passim", "true"}};
                case VariableActions.Types.ExecuteOnSegment: //GLOG 7229
                    return new string[,] { { "Command String", "" },
                                           { "Target Segment ID", "" }};
                default:
                    return null;
            }
        }
        /// <summary>
        /// returns 2-dimensional array of ControlAction Parameter names and values
        /// </summary>
        /// <param name="iActionType"></param>
        /// <returns></returns>
        private string[,] GetControlActionParameters(ControlActions.Types iActionType)
        {
            //TODO: ControlActions.Types has not been fully built -- 
            //only enum at this point
            switch (iActionType)
            {
                case ControlActions.Types.ChangeLabel:
                    return new string[,] {{ "Variable Name", "" },
                                          {"New Label", ""}};
                case ControlActions.Types.Enable:
                    return new string[,] {{ "Variable Name", "" },
                                          {"Enable Expression", ""}};
                case ControlActions.Types.ExecuteControlActionGroup:
                    return new string[,] {{ "Execute Control Action Group Parameter", "" }};
                case ControlActions.Types.RunMethod:
                    return new string[,] {  {"Type", "1"},
                                            {"Server Name", ""},
                                            {"Class Name", ""},
                                            {"Method Name", ""},
                                            {"Arguments", ""}};
                case ControlActions.Types.SelectDialogText:
                    return new string[,] {{ "Select Dialog Text Parameter", "" }};
                case ControlActions.Types.SetControlProperty:
                    return new string[,] {{ "Set Control Parameter", "" }};
                case ControlActions.Types.SetDefaultValue:
                    return new string[,] {  {"Key Name", ""},
                                            {"Key Type", "0"},
                                            {"Expression", ""}};
                case ControlActions.Types.SetFocus:
                    return new string[,] {{ "Variable Name", "" }};
                case ControlActions.Types.SetVariableValue:
                    return new string[,] {{ "Variable Name", "" }, 
                                          { "Expression", ""}};
                case ControlActions.Types.Validate:
                    return new string[,] {{ "Validate Parameter", "" }};
                case ControlActions.Types.Visible:
                    return new string[,] { { "Variable Names", "" },
                                           { "Visibility Expression", ""}};
                case ControlActions.Types.DisplayMessage:
                    return new string[,] {{ "Message Text", "" },
                                          {"Caption", ""}};
                case ControlActions.Types.RefreshControl:
                    return new string[,] { { "Variable Name", "" } };
                default:
                    return null;
            }
        }
        /// <summary>
        /// creates list array for use with Admin UI combo boxes
        /// </summary>
        /// <param name="xListName"></param>
        /// <returns></returns>
        private string[,] GetPropertySettingsList(string xListName)
        {
            
            switch (xListName)
            {

                case "CI Contact Types":
                case "CIContactTypes":
                    return new string[,]  { {"None", "0"},
                                            {"To", "1"},
                                            {"From", "2"},
                                            {"CC", "4"},
                                            {"BCC", "8"},
                                            {"Other", "16"}};

                case "Deletion Scope":
                case "Deletion Scopes":
                    return new string[,]  { {"Cell", "4"},
                                            {"Cell and Previous Cell", "5"},
                                            {"Target", "0"},
                                            {"Paragraph", "3"},
                                            {"Row", "6"},
                                            {"Table", "7"},
                                            {"With Preceding Space", "1"},
                                            {"With Trailing Space", "2"}};
                case "Key Set Types":
                    return new string[,]  { {"Author Preference", "2"},
                                            {"Firm App", "1"},
                                            {"None", "0"},
                                            {"User App", "5"},
                                            {"User App Preference", "6"},
                                            {"User Object Preference", "3"},
                                            {"User Type Preference", "4"}};

                case "Control Types":
                    return new string[,]  { {"Calendar", "20"},
                                            {"Checkbox", "8"},
                                            {"Client Matter Selector", "4"},
                                            {"Combo", "9"},
                                            {"Date Combo", "17"},
                                            {"Detail Grid", "3"},
                                            {"Detail List", "12"},
                                            {"Dropdown List", "15"},
                                            {"Font List", "21"},
                                            {"List", "11"},
                                            {"Locations Dropdown", "22"},
                                            {"Multiline Combo", "2"},
                                            {"Multiline Textbox", "14"},
                                            {"Name Value Pair Grid", "23"},
                                            {"Paper Tray Selector", "26"},
                                            {"People Dropdown", "19"},
                                            {"Reline Grid", "6"},
                                            {"Salutation Combo", "5"},
                                            {"Segment Chooser", "16"},
                                            {"Spinner", "18"},
                                            {"Starting Label Selector", "25"},
                                            {"Textbox", "7"}};

                case "Multivalue Control Types":
                    return new string[,]  { {"Checkbox", "8"},
                                            {"Combo", "9"},
                                            {"Detail Grid", "3"},
                                            {"Detail List", "12"},
                                            {"Dropdown List", "15"},
                                            {"Multiline Combo", "2"},
                                            {"Multiline Textbox", "14"},
                                            {"Textbox", "7"}};
                case "Run Method Types":
                    return new string[,]  { {"COM", "2"},
                                            {".NET", "1"}};

                case "Display Levels":
                    return new string[,]  { {"In Main Section", "1"},
                                            {"In More Section", "2"}};

                case "Alternate Format Type":
                case "Alternate Format Types":
                    return new string[,]  { {"None", "0"},
                                            {"Columns", "1"}, 
                                            {"Table", "2"}};

                case "Segment Types":
                    string[,] aTypes = new string[,]  { {"Architect", "4"},
                                            {"BatesLabels", "10"},
                                            {"Agreement", "11"},
                                            {"AgreementExhibit", "514"},
                                            {"AgreementSignature", "513"},
                                            {"AgreementSignatureNonTable", "531"},
                                            {"AgreementTitlePage", "515"},
                                            {"CollectionTableItem", "525"},
                                            {"DepoSummary", "9"},
                                            {"DisclaimerSidebar", "646"}, //GLOG 15915
                                            {"DisclaimerText", "647"}, //GLOG 15915
                                            {"DraftStamp", "510"},
                                            {"Envelopes", "402"},
                                            {"Exhibits", "514"},
                                            {"Fax", "3"},
                                            {"Labels", "403"},
                                            {"Letter", "1"},
                                            {"Letterhead", "404"},
                                            {"LetterSignature", "509"},
                                            {"LetterSignatureNonTable", "530"},
                                            {"LitigationBack","643"},   //GLOG 7154
                                            {"LogoSidebar", "648"}, //GLOG 15915
                                            {"Memo", "2"},
                                            {"Notary", "7"},
                                            {"NumberedLabels", "645"}, //GLOG 15750
                                            {"NumberingScheme", "105"},
                                            {"Pleading", "5"},
                                            {"PleadingCaption", "500"},
                                            {"PleadingCounsel", "501"},
                                            {"PleadingCoverPage", "505"},
                                            {"PleadingExhibit", "517"},
                                            {"PleadingPaper", "503"},
                                            {"PleadingSignature", "502"},
                                            {"PleadingSignatureNonTable", "529"},
                                            {"Service", "6"},
                                            {"ServiceList", "512"},
                                            {"ServiceListSeparatePage", "642"}, //GLOG 6094
                                            {"Sidebar", "511"},
                                            {"TOA", "516"},
                                            {"Trailer", "401"},
                                            {"Verification", "8"}};

                    //get types to exlude
                    string xTypeExclusions = LMP.Data.Application.GetMetadata("TypeExclusions");

                    if (string.IsNullOrEmpty(xTypeExclusions))
                        return aTypes;
                    else
                    {
                        //get number of exclusions
                        int iNumExclusions = LMP.String.CountChrs(xTypeExclusions, ",") + 1;
                        int iNumTypes = aTypes.GetUpperBound(0) + 1;

                        //create final array
                        string[,] aTypesMod = new string[iNumTypes - iNumExclusions, 2];

                        xTypeExclusions = "," + xTypeExclusions + ",";

                        //cycle through types, removing those
                        //that are in the list of exclusions
                        int j = 0;
                        for (int i = 0; i < iNumTypes; i++)
                        {
                            if (!xTypeExclusions.Contains("," + aTypes[i, 1] + ","))
                            {
                                aTypesMod[j, 0] = aTypes[i, 0];
                                aTypesMod[j, 1] = aTypes[i, 1];
                                j++;
                            }
                        }

                        return aTypesMod;
                    }
                case "Control Events":
                    return new string[,]  { {"First Display", "1"},
                                            {"Lost Focus", "2"},
                                            {"Got Focus", "3"},
                                            {"Value Changed", "4"}};

                case "Hot Keys":
                    if (this.m_oMPDocument.Segments[0].IntendedUse == mpSegmentIntendedUses.AsAnswerFile &&
                        this.m_oMPDocument.Segments[0].IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                    {
                        //we can include all alphanumeric characters in a data interview,
                        //as these display in dialog boxes instead of the task pane
                        return new string[,]  { {"A", "A"},
                                            {"B", "B"},
                                            {"C", "C"},
                                            {"D", "D"},
                                            {"E", "E"},
                                            {"F", "F"},
                                            {"G", "G"},
                                            {"H", "H"},
                                            {"I", "I"},
                                            {"J", "J"},
                                            {"K", "K"},
                                            {"L", "L"},
                                            {"M", "M"},
                                            {"N", "N"},
                                            {"O", "O"},
                                            {"P", "P"},
                                            {"Q", "Q"},
                                            {"R", "R"},
                                            {"S", "S"},
                                            {"N", "N"},
                                            {"O", "O"},
                                            {"P", "P"},
                                            {"Q", "Q"},
                                            {"R", "R"},
                                            {"S", "S"},
                                            {"T", "T"},
                                            {"U", "U"},
                                            {"V", "V"},
                                            {"W", "W"},
                                            {"X", "X"},
                                            {"Y", "Y"},
                                            {"Z", "Z"},
                                            {"1", "1"},
                                            {"2", "2"},
                                            {"3", "3"},
                                            {"4", "4"},
                                            {"5", "5"},
                                            {"6", "6"},
                                            {"7", "7"},
                                            {"8", "8"},
                                            {"9", "9"}};
                    }
                    else
                    {
                        return new string[,]  { {"A", "A"},
                                            {"B", "B"},
                                            {"D", "D"},
                                            {"E", "E"},
                                            {"F", "F"},
                                            {"G", "G"},
                                            {"H", "H"},
                                            {"I", "I"},
                                            {"J", "J"},
                                            {"K", "K"},
                                            {"L", "L"},
                                            {"M", "M"},
                                            {"N", "N"},
                                            {"O", "O"},
                                            {"P", "P"},
                                            {"Q", "Q"},
                                            {"R", "R"},
                                            {"S", "S"},
                                            {"N", "N"},
                                            {"O", "O"},
                                            {"P", "P"},
                                            {"Q", "Q"},
                                            {"R", "R"},
                                            {"S", "S"},
                                            {"T", "T"},
                                            {"U", "U"},
                                            {"W", "W"},
                                            {"1", "1"},
                                            {"2", "2"},
                                            {"3", "3"},
                                            {"4", "4"},
                                            {"5", "5"},
                                            {"6", "6"},
                                            {"7", "7"},
                                            {"8", "8"},
                                            {"9", "9"}};
                    }
                case "Link Authors To Parent":
                case "LinkAuthorsToParentDef":
                    return new string[,]  { {"Never", "0"},
                                            {"First Child of Type", "1"},
                                            {"Always", "2"}};
                case "AuthorNodeUILabel":
                    //GLOG 6653
                    return new string[,]  { {"Author", "Author"},
                                            {"Attorney", "Attorney"},
                                            {"", ""}};
                case "IntendedUse":
                    return new string[,]  { {"Document", "1"},
                                            {"Document Component", "2"},
                                            {"Text Block", "3"}, 
                                            {"Style Sheet", "4"}};
                case "Bar Code Positions":
                    return new string[,] { {"None", "0"},
                                           {"Above", "1"},
                                           {"Below", "2"}};
                case "Prefill Override Types":
                    return new string[,] { {"Never", "0"},
                                           {"Always", "1"},
                                           {"For Segment of Same Type", "2"},
                                           {"For Segment with Same ID", "3"}};
                default:
                    return null;
            }
        }
        /// <summary>
        /// creates a new expression textbox -
        /// subscribes to button pressed event
        /// </summary>
        /// <returns></returns>
        private ExpressionTextBox CreateExpressionTextBox()
        {
            ExpressionTextBox oExpTextBox = new ExpressionTextBox();
            return oExpTextBox;
        }
        /// <summary>
        /// Returns true if specified screen position is within the 
        /// area of oMenu
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="oMenu"></param>
        /// <returns></returns>
        private bool PointIsWithinContextMenu(int X, int Y, ContextMenuStrip oMenu)
        {
            if (oMenu.Visible && oMenu.RectangleToScreen(oMenu.ClientRectangle).Contains(X, Y))
                return true;
            else
                return false;
        }
        /// <summary>
        /// used for both VariableActions and Segment.Actions
        /// returns action type based on name
        /// </summary>
        /// <param name="xName">Varible Action Name</param>
        /// <returns>Variable Action Type</returns>
        private VariableActions.Types GetVariableActionTypeFromName(string xName)
        {
            return (VariableActions.Types)Enum.Parse(typeof(VariableActions.Types), xName);
        }
        /// <summary>
        /// used for ControlActions
        /// returns action type based on name
        /// </summary>
        /// <param name="xName">Varible Action Name</param>
        /// <returns>Variable Action Type</returns>
        private ControlActions.Types GetControlActionTypeFromName(string xName)
        {
            return (ControlActions.Types)Enum.Parse(typeof(ControlActions.Types), xName);
        }
        /// <summary>
        /// returns default new variable or block name w/ incremented ID
        /// </summary>
        /// <param name="oSegment">current segment</param>
        /// <param name="iTagType">mSeg or mVar</param>
        /// <returns></returns>
        private void GetNewVarOrBlockName(Segment oSegment, ForteDocument.TagTypes byteTagType, out string xName, out string xDisplayName)
        {
            string xFormCaption = null;

            if (byteTagType == ForteDocument.TagTypes.Variable)
            {
                xDisplayName = "New Variable " + GetNextVariableNameIdentifier(oSegment);
                xFormCaption = "Variable";
            }
            else if (byteTagType == ForteDocument.TagTypes.Block)
            {
                xDisplayName = "New Block " + GetNextBlockNameIdentifier(oSegment);
                xFormCaption = "Block";
            }
            else
                xDisplayName = "";

            BasicNameForm oForm = new BasicNameForm(oSegment);
            oForm.ItemDisplayName = xDisplayName;
            oForm.Text = oForm.Text + xFormCaption;

            if (byteTagType == ForteDocument.TagTypes.Block)
                oForm.TargetObjectType = BasicNameForm.TargetObjectTypes.Block;
            else
                oForm.TargetObjectType = BasicNameForm.TargetObjectTypes.Variable;

            DialogResult oResult = oForm.ShowDialog();

            if (oResult == DialogResult.OK)
            {
                xDisplayName = oForm.ItemDisplayName;
                xName = oForm.ItemName;
            }
            else
            {
                xDisplayName = null;
                xName = null;
            }

            oForm.Close();

        }
        /// <summary>
        /// returns incremented value of highest default variable name identifier
        /// i.e. "Variablenn"
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private int GetNextVariableNameIdentifier(Segment oSegment)
        {
            ArrayList aNums = new ArrayList();
            string xName = null;
            int iCount = oSegment.Variables.Count;
            for (int i = 0; i < iCount; i++)
            {
                xName = oSegment.Variables[i].Name;
                //GLOG 8775: Only count if variable name is in forat "NewVariableX" where "X" is numeric
                if (xName.ToLower().IndexOf("newvariable") == 0 && LMP.String.IsNumericInt32(xName.ToLower().Replace("newvariable", "")))
                    aNums.Add(xName.Replace("NewVariable", ""));
            }   
            aNums.Sort();
            if (aNums.Count == 0)
                return 1;
            else
                return Int32.Parse(aNums[aNums.Count - 1].ToString()) + 1;
        }
        /// <summary>
        /// returns incremented value of highest default block name identifier
        /// i.e. "Blocknn"
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private int GetNextBlockNameIdentifier(Segment oSegment)
        {
            ArrayList aNums = new ArrayList();
            string xName = null;
            int iCount = oSegment.Blocks.Count;
            for (int i = 0; i < iCount; i++)
            {
                xName = oSegment.Blocks[i].Name;
                if (xName.IndexOf("NewBlock") != -1)
                    aNums.Add(xName.Replace("NewBlock", ""));
            }
            aNums.Sort();
            if (aNums.Count == 0)
                return 1;
            else
                return Int32.Parse(aNums[aNums.Count - 1].ToString()) + 1;
        }
        /// <summary>
        /// returns next incremented part number from 
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private int GetNewPartNumber(Segment oSegment)
        {
            //create array list of part numbers, use sort method and return last item + 1
            string[] aPartNumbers = oSegment.Nodes.GetItemPartNumbers(oSegment.FullTagID).Split(';');
            ArrayList aSort = new ArrayList();
            for (int i = 0; i <= aPartNumbers.GetUpperBound(0); i++)
			{
                //GLOG 15895: Add to array as numeric values to ensure correct sorting
			    aSort.Add(Int32.Parse(aPartNumbers[i].Trim()));
			}
		    aSort.Sort();
            int iNew = Int32.Parse(aSort[aSort.Count - 1].ToString()) + 1; 
            
            return iNew;
        }
        /// <summary>
        /// tests range for containing segments, existing segments or mismatched 
        /// segment in range story
        /// </summary>
        /// <param name="oRange"></param>
        /// <param name="oSeg"></param>
        /// <returns>returns TagInsertionValidityStates</returns>
        /// <summary> 
        private LMP.Forte.MSWord.TagInsertionValidityStates ValidateInsertionLocation( 
            Word.Range oRange, Segment oSeg, LMP.Forte.MSWord.TagTypes iTagType)
        {
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //xml tag
                Word.XMLNode oNode = oSeg.PrimaryWordTag;
                return LMP.Forte.MSWord.WordDoc.ValidateTagInsertion(oRange, oNode, iTagType);
            }
            else
            {
                //content control
                Word.ContentControl oCC = oSeg.PrimaryContentControl;
                return LMP.Forte.MSWord.WordDoc.ValidateContentControlInsertion(oRange, oCC, iTagType);
            }
        }
        /// <summary>
        /// returns array of AdminSegment property names
        /// </summary>
        /// <returns></returns>
        private string[,] GetAdminSegmentPropertyNameArray(Segment oSegment)
        {
            //create storage for admin segment property names - property name, display name
            switch (oSegment.IntendedUse)
            {
                case mpSegmentIntendedUses.AsStyleSheet:
                    //Only show minimal set of properties for style sheet
                    return new string[,] {{"DisplayName","Display Name"}, 
                                        {"IntendedUse", "Intended Use"},
                                        {"HelpText","Help Text"}};
                case mpSegmentIntendedUses.AsAnswerFile:
                case mpSegmentIntendedUses.AsMasterDataForm:
                    return new string[,] {{"DisplayName","Display Name"}, 
                                {"HelpText","Help Text"}};
                default:
                    if ((oSegment is CollectionTableItem) && !(oSegment is PleadingCaption))
                    {
                        return new string[,] {{"TypeID","Segment Type"},
                                {"DisplayName","Display Name"}, 
                                {"IntendedUse", "Intended Use"},
                                {"IsTransparentDef", "Is Transparent"},
                                {"ShowChooser","Show Chooser"},
                                {"DisplayWizard", "Display Wizard"},
                                {"WordTemplate", "Word Template"},
                                {"RequiredStyles", "Required Styles"},
                                {"LinkAuthorsToParentDef","Link Authors To Parent"},
                                {"MenuInsertionOptions","Insertion Locations"},
                                {"DefaultMenuInsertionBehavior","Insertion Behavior"},
                                {"DefaultDragLocation","Default Drag Location"},
                                {"DefaultDragBehavior","Default Drag Behavior"},
                                {"DefaultDoubleClickLocation","Default Click Location"},
                                {"DefaultDoubleClickBehavior","Default Click Behavior"},
                                {"DefaultTrailerID", "Default Trailer Type"},
                                {"HelpText","Help Text"},
                                {"AllowSideBySide", "Allow Side-By-Side"}};
                    }
                    else
                    {
                        return new string[,] {{"TypeID","Segment Type"},
                                {"DisplayName","Display Name"}, 
                                {"IntendedUse", "Intended Use"},
                                {"IsTransparentDef", "Is Transparent"},
                                {"ShowChooser","Show Chooser"},
                                {"DisplayWizard", "Display Wizard"},
                                {"WordTemplate", "Word Template"},
                                {"RequiredStyles", "Required Styles"},
                                {"LinkAuthorsToParentDef","Link Authors To Parent"},
                                {"MenuInsertionOptions","Insertion Locations"},
                                {"DefaultMenuInsertionBehavior","Insertion Behavior"},
                                {"DefaultDragLocation","Default Drag Location"},
                                {"DefaultDragBehavior","Default Drag Behavior"},
                                {"DefaultDoubleClickLocation","Default Click Location"},
                                {"DefaultDoubleClickBehavior","Default Click Behavior"},
                                {"DefaultTrailerID", "Default Trailer Type"},
                                {"ProtectedFormPassword","Protected Form Password"},
                                {"HelpText","Help Text"}};
                    }
            }
        }
        /// <summary>
        /// returns array of UserSegment property names
        /// </summary>
        /// <returns></returns>
        private string[,] GetUserSegmentPropertyNameArray()
        {
            //create storage for user segment property names
            string[,] aProps ={ { "DisplayName", "Display Name"},
                                { "IntendedUse", "Intended Use"},
                                { "HelpText", "Help Text" }};
            return aProps;
        }
        /// <summary>
        /// returns array of UserSegment property names
        /// </summary>
        /// <returns></returns>
        private string[,] GetAdminSegmentVariablePropertyNameArray(mpSegmentIntendedUses iIntendedUse)
        {
            //build storage for property node info - property name, display name
            switch (iIntendedUse)
            {
                // GLOG : 3415 : JAB
                // Hide the Validation Response functionality until it's release to clients.
                //{"ValidationResponse","Validation Response"}};
                case mpSegmentIntendedUses.AsAnswerFile:
                    return new string[,] {{"DisplayName","Display Name"},
                                {"HotKey","Hot Key"},
                                {"HelpText","Help Text"},
                                {"DefaultValue","Default Value"},
                                {"TabNumber", "Tab Number"},
                                {"ControlType","Control Type"},
                                {"ControlProperties","Control Properties"},
                                {"ValidationCondition","Validation Condition"}};
                case mpSegmentIntendedUses.AsMasterDataForm:
                    return new string[,] {{"DisplayName","Display Name"},
                                {"HotKey","Hot Key"},
                                {"HelpText","Help Text"},
                                {"DefaultValue","Default Value"},
                                {"ControlType","Control Type"},
                                {"ControlProperties","Control Properties"}};
                                // GLOG : 3415 : JAB
                                // Hide the Validation Response functionality until it's release to clients.
                                //{"ValidationResponse","Validation Response"}};
                default:
                    return new string[,] {{"DisplayName","Display Name"},
                                {"HotKey","Hot Key"},
                                {"HelpText","Help Text"},
                                {"ProtectedFormPassword", "Protected Form Password"},
                                {"IsMultiValue", "Variable Type"},
                                {"DefaultValue","Default Value"},
                                {"SecondaryDefaultValue","Secondary Default Value"},
                                {"DisplayIn","Display In"},
                                {"DisplayLevel","Display Level"},
                                {"ControlType","Control Type"},
                                {"ControlProperties","Control Properties"},
                                {"DisplayValue","Display Value"},
                                {"ValueSourceExpression","Value Source Expression"},
                                {"ValidationCondition","Validation Condition"},
                                {"NavigationTag", "Navigation Tag"}, //GLOG 1408
                                // GLOG : 3415 : JAB
                                // Hide the Validation Response functionality until it's release to clients.
                                // {"ValidationResponse","Validation Response"},
                                {"AllowPrefillOverride","Allow Saved Data Override"},
                                {"AssociatedPrefillNames", "Associated Variable Names"},
                                {"MustVisit", "Must Visit"}};
            }
        }
        /// <summary>
        /// returns array of UserSegment variable property names
        /// </summary>
        /// <returns></returns>
        private string[,] GetUserSegmentVariablePropertyNameArray()
        {
            //build storage for property node info - property name, display name
            string[,] aProps = {{"DisplayName","Display Name"},
                                {"HotKey","Hot Key"},
                                {"DefaultValue","Default Value"},
                                {"HelpText","Help Text"}};
            return aProps;
        }
        /// <summary>
        /// executes the actions of all variables in the specified segment,
        /// excepting those which are author, user, or runtime variable
        /// value dependent - this will save processing at runtime
        /// </summary>
        /// <param name="oSegment"></param>
        private void RefreshStaticDefaults(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;

            //skip refresh if segment is created statically -
            //i.e. after the user clicks the finish button
            if (oSegment is LMP.Architect.Base.IStaticCreationSegment)
                return;

            //GLOG 1407 - set flag to avoid execution indexes from being adjusted
            //when variables are deleted with delete scopes
            m_oMPDocument.DeleteScopesUpdatingInDesign = true;
            Hashtable oDynamicVarHash = new Hashtable();

            //GLOG 5315 (dm) - postpone execution of IncludeExcludeBlocks execution
            //to ensure that nested variables are handled first
            
            //GLOG 8897
            List<string> oDeferredVars = new List<string>();
            List<string> oVarList = new List<string>();
            bool bRestartLoop = false;
            do
            {
                bRestartLoop = false;
                int iStartCount = oSegment.Variables.Count;
                //this segment
                for (int i = 0; i < iStartCount; i++)
                {
                    Variable oVar = oSegment.Variables[i];
                    if (oVarList.Contains(oVar.ID))
                        //Skip variables processed in a prior loop
                        continue;
                    else
                        oVarList.Add(oVar.ID);
                    //don't execute actions if there are any field codes in the default
                    //value or action parameters which are dependent on the authors, user,
                    //or the value of another variable - we make an exception for author prefs,
                    //using the firmwide default where available - we allow author/user codes
                    //in the secondary default value because a non-empty value is required
                    //to prevent an expanded deletion scope from being deleted by IncludeText -
                    //these codes will evaluate harmlessly to "AuthorData" or "UserData"
                    //at design time

                    bool bIsDynamic = IsVariableDynamic(oVar, ref oDynamicVarHash);
                    if (!bIsDynamic)
                    {
                        VariableActions oActions = oVar.VariableActions;
                        //skip refresh for this variable if it has any
                        //actions that may be problematic at design
                        bool bDo = true;
                        bool bHasBlockAction = false;
                        for (int j = 0; j < oActions.Count; j++)
                        {
                            VariableActions.Types iType = oActions[j].Type;
                            if ((iType == VariableActions.Types.SetVariableValue) ||
                                (iType == VariableActions.Types.InsertSegment) ||
                                (iType == VariableActions.Types.ReplaceSegment) ||
                                (iType == VariableActions.Types.InsertCheckbox) ||
                                (iType == VariableActions.Types.InsertTOA) ||
                                (iType == VariableActions.Types.SetupLabelTable)) //GLOG 6687
                            {
                                bDo = false;
                                break;
                            }
                            else if (iType == VariableActions.Types.IncludeExcludeBlocks)
                                bHasBlockAction = true; //GLOG 5315 (dm)
                        }

                        if (bDo)
                        {
                            if (!bHasBlockAction)
                            {
                                //set temp value to force actions to execute when
                                //and only when default is assigned
                                oVar.SetValue("zzmpTempValue", false);

                                //assign default value
                                oVar.AssignDefaultValue();
                                //GLOG 8897: If running action has changed variables collection, restart loop
                                if (iStartCount != oSegment.Variables.Count)
                                {
                                    bRestartLoop = true;
                                    break;
                                }
                            }
                            else if (!oDeferredVars.Contains(oVar.ID))
                            {
                                oDeferredVars.Add(oVar.ID); //GLOG 5315 (dm)    
                            }
                        }
                    }
                }
            } while (bRestartLoop == true);

            //GLOG 5315 (dm) - refresh variables with IncludeExcludeBlocks actions
            for (int i = 0; i < oDeferredVars.Count; i++)
            {
                Variable oVar = null;
                try
                {
                    oVar = oSegment.Variables[oDeferredVars[i]];
                }
                catch 
                {
                    continue;
                }

                oVar.SetValue("zzmpTempValue", false);
                oVar.AssignDefaultValue();
            }

            //GLOG 2165: Set Body Block StartingText to match Designer property
            for (int i = 0; i < oSegment.Blocks.Count; i++)
            {
                if (oSegment.Blocks[i].IsBody && !string.IsNullOrEmpty(oSegment.Blocks[i].StartingText))
                {
                    //GLOG 4944: 10.2
                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        oSegment.Blocks[i].AssociatedWordTag.Text = 
                        Expression.Evaluate(oSegment.Blocks[i].StartingText, oSegment, oSegment.ForteDocument);
                    else
                        oSegment.Blocks[i].AssociatedContentControl.Range.Text = Expression.Evaluate(oSegment.Blocks[i].StartingText, oSegment, oSegment.ForteDocument);

                }
            }
            oDynamicVarHash = null;
            //child segments
            for (int i = 0; i < oSegment.Segments.Count; i++)
                RefreshStaticDefaults(oSegment.Segments[i]);

            //GLOG 1407 - reset flag
            m_oMPDocument.DeleteScopesUpdatingInDesign = false;
            LMP.Benchmarks.Print(t0);
        }
        private bool IsVariableDynamic(Variable oVar, ref Hashtable oDynamicVarHash)
        {
            if (oDynamicVarHash.Contains(oVar.Name))
            {
                //Variable has already been tested previously
                return (bool)oDynamicVarHash[oVar.Name];
            }
            else
            {
                //JTS: Mark Variable as Dynamic to start.  If a referenced variable
                //also references this Variable, both will be set as dynamic,
                //and we won't go into an endless recursive loop
                oDynamicVarHash.Add(oVar.Name, true);
                VariableActions oActions = oVar.VariableActions;

                bool bIsDynamic = false;
                string xDefaultValue = oVar.DefaultValue;
                string xActions = oActions.ToString();
                string xPropString = xDefaultValue + xActions;
                int iPos = xPropString.IndexOf('[');
                if (iPos > -1)
                {
                    bIsDynamic = (Expression.ContainsUserCode(xPropString) ||
                        Expression.ContainsAuthorCode(xActions, 0, 0));
                    if (!bIsDynamic && Expression.ContainsVariableCode(xPropString))
                    {
                        string xTemp = xPropString;
                        while (xTemp.ToUpper().Contains("[VARIABLE_") && !bIsDynamic)
                        {
                            iPos = xTemp.ToUpper().IndexOf("[VARIABLE_");
                            if (iPos > -1)
                            {
                                int iPos2 = xTemp.IndexOf("]", iPos);
                                if (iPos2 > -1)
                                {
                                    iPos = xTemp.IndexOf("_", iPos, iPos2 - iPos) + 1;
                                    string xVar = xTemp.Substring(iPos, iPos2 - iPos);
                                    xVar = xVar.TrimStart('_');
                                    Variable oRefVar = null;
                                    try
                                    {
                                        oRefVar = oVar.Segment.Variables.ItemFromName(xVar);
                                    }
                                    catch { }
                                    if (oRefVar != null)
                                        bIsDynamic = IsVariableDynamic(oRefVar, ref oDynamicVarHash);
                                }
                                xTemp = xTemp.Remove(0, iPos2 + 1);
                            }
                            else
                                break;
                        }
                    }

                    if (!bIsDynamic)
                    {
                        //no prohibited codes found yet - check default value for
                        //author codes
                        bIsDynamic = Expression.ContainsAuthorCode(xDefaultValue, 0, 0);
                        if (bIsDynamic)
                        {
                            //author code found - check if this is a simple author pref
                            if (Expression.ContainsPreferenceCode(xDefaultValue,
                                Expression.PreferenceCategories.Author) &&
                                xDefaultValue.StartsWith("[") &&
                                xDefaultValue.EndsWith("]") &&
                                (LMP.String.CountChrs(xDefaultValue, "[") == 1))
                            {
                                //allow only if there's a firmwide default -
                                //if an error is generated here, just skip the variable -
                                //error will be generated when no firm preferences
                                //have been set - e.g. when creating a new segment
                                try
                                {
                                    bIsDynamic = (Expression.Evaluate(xDefaultValue,
                                        oVar.Segment, m_oMPDocument) == "null");
                                }
                                catch
                                {
                                    bIsDynamic = true;
                                }
                            }
                        }
                    }
                }
                if (!oDynamicVarHash.Contains(oVar.Name))
                    oDynamicVarHash.Add(oVar.Name, bIsDynamic);
                else
                    oDynamicVarHash[oVar.Name] = bIsDynamic;
                return bIsDynamic;
            }

        }
        //GLOG 8376: This function isn't used
        ///// <summary>
        ///// Get XML string to initialize xml field for Segment
        ///// </summary>
        ///// <param name="xID"></param>
        ///// <param name="iTypeID"></param>
        ///// <param name="xDisplay"></param>
        ///// <returns></returns>
        //public string GetWordXML(string xID, mpObjectTypes iTypeID, string xDisplayName, 
        //                           string xName, string xHelpText, mpSegmentIntendedUses iIntendedUse)
        //{
        //    string xXML = "";
        //    string xObjectData = "";
        //    string xTagID = "";

        //    Trace.WriteNameValuePairs("xID", xID,
        //        "iTypeID", iTypeID, "xDisplayName", xDisplayName, 
        //        "xName", xName, "iIntendedUse", iIntendedUse.ToString());

        //    try
        //    {
        //        // Set TagID
        //        xTagID = xName + "_1";

        //        //get mSEG object data 
        //        System.Text.StringBuilder oSB = new StringBuilder();
        //        oSB.AppendFormat(Segment.ADMIN_SEG_OBJECT_DATA_TEMPLATE, xID,
        //            ((short)iTypeID).ToString(), xName, xDisplayName, xHelpText, iIntendedUse,
        //            iIntendedUse == mpSegmentIntendedUses.AsAnswerFile || 
        //            iIntendedUse == mpSegmentIntendedUses.AsMasterDataForm, xDisplayName);

        //        xObjectData = oSB.ToString();

        //        //return formatted selection XML
        //        if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
        //            xXML = Session.CurrentWordApp.Selection.get_XML(false);
        //        else
        //            xXML = Session.CurrentWordApp.Selection.WordOpenXML;

        //        return Segment.GetFormattedNewSegmentXML(xXML, xTagID, 
        //            xObjectData, Segment.NEW_SEGMENT_BMK_XML, false);
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //    finally
        //    {
        //        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
        //    }

        //}
        /// <summary>
        /// Show the appropriate context menu based on node type
        /// </summary>
        /// <param name="iMenuType"></param>
        /// <param name="iX"></param>
        /// <param name="iY"></param>
        private void DisplayContextMenu(UltraTreeNode oNode)
        {
            int iX = oNode.UIElement.Rect.Left;
            int iY = oNode.UIElement.Rect.Top;
            DisplayContextMenu(oNode, iX, iY);
        }
        private void DisplayContextMenu(UltraTreeNode oNode, int iX, int iY)
        {

            NodeTypes iType = NodeType(oNode);
            switch (iType)
            {
                case NodeTypes.Segment:
                    mnuSegments.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.SegmentActionProperty:
                    break;
                case NodeTypes.SegmentAction:
                case NodeTypes.LanguageAction:
                case NodeTypes.AuthorsAction:
                case NodeTypes.SegmentEventsCategory:
                case NodeTypes.LanguageActionsCategory:
                case NodeTypes.AuthorsActionsCategory:
                    mnuSegments.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.VariablesCategory:
                    mnuVariables.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.Variable:
                    mnuVariables.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.VariableActionsCategory:
                    mnuVariables.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.VariableAction:
                    mnuVariables.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.ControlActionsCategory:
                    //TODO: implement CA menu
                    mnuVariables.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.SegmentAuthorsControlActionsCategory:
                case NodeTypes.SegmentJurisdictionControlActionsCategory:
                case NodeTypes.LanguageControlActionsCategory:
                    //TODO: implement CA menu
                    mnuVariables.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.ControlAction:
                    //TODO: implement CA menu
                    mnuVariables.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.SegmentAuthorsControlAction:
                case NodeTypes.SegmentJurisdictionControlAction:
                case NodeTypes.LanguageControlAction:
                    mnuVariables.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.BlocksCategory:
                    mnuBlocks.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.Block:
                    mnuBlocks.Show(this.treeDocContents, iX, iY);
                    break;
                case NodeTypes.AnswerFileSegmentsCategory:
                case NodeTypes.AnswerFileSegment:
                    mnuAnswerFileSegments.Show(this.treeDocContents, iX, iY);
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// Determines if scrollbar is visible by checking for existence of corresponding child UI element
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        private bool IsScrollbarVisible(mpScrollbarType iType)
        {
            string xElement = "";
            switch (iType)
            {
                case mpScrollbarType.Horizontal:
                    xElement = "Infragistics.Win.UltraWinTree.HorizontalScrollbarUIElement";
                    break;
                case mpScrollbarType.Vertical:
                    xElement = "Infragistics.Win.UltraWinTree.VerticalScrollbarUIElement";
                    break;
                default:
                    return false;
            }
            UIElementsCollection oChildren = this.treeDocContents.UIElement.ChildElements;
            for (int i = 0; i < oChildren.Count; i++)
            {
                if (oChildren[i].ToString() == xElement)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// returns the cursor used to show drag drop placement
        /// of items in the content manager
        /// </summary>
        private static Cursor BetweenCursor
        {
            get
            {
                if (m_oBetweenCursor == null)
                    m_oBetweenCursor = new Cursor(LMP.Data.Application.AppDirectory + @"\Between.cur");

                return m_oBetweenCursor;
            }
        }

        /// <summary>
        /// validates all sentence text segments
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool SentenceTextSegmentsAreValid(Segment oSegment)
        {
            if (oSegment.IntendedUse == mpSegmentIntendedUses.AsSentenceText)
            {
                Word.Range oRange = null;

                if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    //sentence text cannot contain multiple paragraphs
                    oRange = oSegment.WordTags[0].Range;
                }
                else
                {
                    oRange = oSegment.ContentControls[0].Range;
                }

                if ((oRange.Paragraphs.Count > 1) || (oRange.Characters.Last.Text == "\r"))
                {
                    string xMsg = "Msg_CantSaveMultiParaSentenceText";
                    MessageBox.Show(LMP.Resources.GetLangString(xMsg) + oSegment.Name,
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
            {
                //validate children
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    if (!SentenceTextSegmentsAreValid(oSegment.Segments[i]))
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// returns TRUE if there's text outside of top-level tag and user
        /// opts to not expand the tag to onclude it
        /// </summary>
        /// <returns></returns>
        private bool DesignContainsUntaggedContent()
        {
            if (LMP.Forte.MSWord.WordDoc.DesignContainsUntaggedContent())
            {
                //there's text in body outside of top-level mSEG - offer to
                //expand mSEG to include it
                //GLOG 4385 - don't offer to expand if there are multiple parts in the body
                DialogResult iResult = DialogResult.No;
                Word.Document oDoc = m_oMPDocument.WordDocument;
                Word.XMLNodes oWordTags = oDoc.SelectNodes("/*", "", true);
                if (oWordTags.Count == 1)
                {
                    if (LMP.Registry.GetMacPac10Value("ConfigureToRunReverseConversion") == "1")
                    {
                        iResult = DialogResult.Yes;
                    }
                    else
                    {
                        //offer to expand
                        iResult = MessageBox.Show(LMP.Resources.GetLangString(
                            "Prompt_DesignContainsUntaggedContent"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                }
                else
                {
                    //don't offer to expand
                    MessageBox.Show(LMP.Resources.GetLangString(
                        "Prompt_DesignContainsUntaggedContent_NoOffer"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (iResult == DialogResult.Yes)
                {
                    //user has chosen to expand mSEG to include external text
                    ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //expand the tag and refresh nodes
                    Word.XMLNode oWordTag = oDoc.XMLNodes[1];
                    Segment oSegment = m_oMPDocument.Segments[0];
                    m_oMPDocument.Tags.MoveWordTag(ref oWordTag, oDoc.Content);
                    oSegment.RefreshNodes();

                    //reclassify sentence text if it now contains multiple paragraphs
                    if (oSegment.IntendedUse == mpSegmentIntendedUses.AsSentenceText)
                    {
                        Word.Range oRange = oWordTag.Range;
                        if ((oRange.Paragraphs.Count > 1) ||
                            (oRange.Characters.Last.Text == "\r"))
                        {
                            oSegment.IntendedUse = mpSegmentIntendedUses.AsParagraphText;
                            oSegment.Nodes.SetItemObjectDataValue(oSegment.FullTagID,
                                "IntendedUse", ((short)oSegment.IntendedUse).ToString());
                            this.RefreshTree();
                        }
                    }
                    
                    //restore event handling
                    ForteDocument.IgnoreWordXMLEvents = iEvents;
                    return false;
                }
                else
                    //disallow save
                    return true;
            }
            else
                return false;
        }
        #endregion
        #region *********************protected methods*********************
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		/// <summary>
		/// processes the tab and shift-tab key combinations -
		/// selects next or previous node -
		/// unfortunately, there is an IBF bug that prevents
		/// this method from getting executed when appropriate -
		/// in cases when this method does not execute, the
		/// tab pressed event handler will execute
		/// </summary>
		/// <param name="keyData"></param>
		/// <returns></returns>
		protected override bool ProcessDialogKey(Keys keyData)
		{
            const int KEY_SHIFT = 16;
            const int KEY_TAB = 9;

            //get state of tab key - calling the API KeyState function
            //is the only reliable way to get this info
            int iTabKeyState = LMP.WinAPI.GetKeyState(KEY_TAB);
            bool bTabPressed = !(iTabKeyState == 1 || iTabKeyState == 0);

            if (bTabPressed)
            {
                //get state of shift key
                int iShiftKeyState = LMP.WinAPI.GetKeyState(KEY_SHIFT);
                bool bShiftPressed = !(iShiftKeyState == 1 || iShiftKeyState == 0);

                if (bShiftPressed)
                    //shift-tab was pressed - select previous node
                    this.SelectPreviousNode();
                else
                    //tab was pressed - select next node
                    this.SelectNextNode();

                return true;
            }
            else if (keyData == Keys.F5)
            {
                //GLOG 2768, 8/1/08
                TaskPanes.RefreshAll();
                this.RefreshTree();
                this.Focus();
                return true;
            }
            else
                return false;
		}
		#endregion
		#region *********************static methods*********************
        /// <summary>
        /// creates an instance of each control -
        /// called on a separate thread to increase
        /// performance of initial control display by
        /// forcing code to cache
        /// </summary>
		private static void InitializeControls()
		{
			LMP.Controls.TextBox oTB = new LMP.Controls.TextBox();
			LMP.Controls.MultilineTextBox oMLT = new MultilineTextBox();
			LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
            LMP.Controls.CIAlertsList oCIAList = new CIAlertsList();
            LMP.Controls.ControlPropertiesGrid oCPGrid = new ControlPropertiesGrid();
            LMP.Controls.Spinner oSpinner = new Spinner();
            LMP.Controls.JurisdictionChooser oJChooser = new JurisdictionChooser();
            LMP.Controls.ListCombo oLCombo = new ListCombo();
            LMP.Controls.ExpressionTextBox oXText = new ExpressionTextBox();
            LMP.Controls.DefaultLocationComboBox oDLC = new DefaultLocationComboBox();
            LMP.Controls.IntendedUsesComboBox oIUC = new IntendedUsesComboBox();
            LMP.Controls.InsertionBehaviorsList oIBL = new InsertionBehaviorsList();
            LMP.Controls.InsertionLocationsList oILL = new InsertionLocationsList();
            LMP.Controls.RelineFormatOptionsList oRLO = new RelineFormatOptionsList();
            LMP.Controls.SupportedLanguagesList oSLL = new SupportedLanguagesList();
            LMP.Controls.LanguagesComboBox oLCB = new LanguagesComboBox();


            oTB.Dispose();
            oMLT.Dispose();
            oCombo.Dispose();
            oCIAList.Dispose();
            oCPGrid.Dispose();
            oSpinner.Dispose();
            oJChooser.Dispose();
            oLCombo.Dispose();
            oXText.Dispose();
            oDLC.Dispose();
            oIBL.Dispose();
            oILL.Dispose();
            oIUC.Dispose();
            oRLO.Dispose();
            oSLL.Dispose();
            oLCB.Dispose();
        }
		#endregion

    }
}
