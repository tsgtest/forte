using System;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using LMP.Architect.Api;
using LMP.Data;
using System.Collections;
using System.Xml;
using Infragistics.Win.UltraWinTree;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac
{
    public partial class DesignerLibraryManager : UserControl
    {
        #region *********************enumerations*********************
        private enum Images
        {
            NoImage = 9999,
            DocumentSegment = 0,
            ComponentSegment = 1,
            TaggedVariable = 2,
            TaglessVariable = 3
        }
        #endregion
        #region *********************fields*********************
        private XmlNamespaceManager m_oNSM;
        private string m_xNSPrefix = "";
        private XmlDocument m_oXML;
        private TaskPane m_oTaskPane;
        private ForteDocument m_oMPDocument;
        private UltraTreeNode m_oDragNode = null;
        private Rectangle m_oRectForDrag = Rectangle.Empty;
        private Point m_oLastPointOnScreen = Point.Empty;
        private bool m_bWordOpenXML = false;
        #endregion
        #region *********************constructors*********************
        public DesignerLibraryManager()
        {
            InitializeComponent();

            this.cmbCategory.SetList(new string[,] { { "Blocks", "Blocks" }, 
                { "Properties/Authors", "Properties/Authors" }, { "Variables", "Variables" }});
            this.cmbCategory.SelectedIndex = 2;
            
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// instantiates global Task Pane and Task Pane Document objects
        /// </summary>
        internal TaskPane TaskPane
        {
            get { return m_oTaskPane; }
            set
            {
                m_oTaskPane = value;
                m_oMPDocument = m_oTaskPane.ForteDocument;
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// sets up the control
        /// </summary>
        public void Setup()
        {
            //GLOG 4473: Display Admin Segments under My Folders as well
            this.treeSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders | 
                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments | 
                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.DesignerSegments; //GLOG 8413
            this.treeSegments.OwnerID = LMP.Data.Application.User.ID;
            this.treeSegments.ExecuteFinalSetup();
            this.treeSegments.Indent = 19;
            this.treeSegments.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            this.treeSegments.Override.NodeSpacingAfter = 2;

            //ensure that variables panel is half the height of the designer library
            this.pnlVariables.Height = this.Height / 2;
        }
        #endregion
        #region *********************event handlers*********************
        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                ImportElement(this.treeImport.ActiveNode);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Tree node has been double-clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeImport_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                ImportElement(this.treeImport.ActiveNode);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeSegments_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                ShowElements();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// User has clicked on the UltraTreeView control
        /// May be beginning drag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeVariables_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                m_oLastPointOnScreen = new Point(e.X, e.Y);
                m_oRectForDrag = Rectangle.Empty;
                UltraTreeNode oNode = this.treeImport.GetNodeFromPoint(e.X, e.Y);
                if (oNode == null || !(oNode.Tag is LibraryVariable))
                {
                    return;
                }
                if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                {
                    oNode.Selected = true;
                    this.treeImport.ActiveNode = oNode;
                    m_oDragNode = oNode;
                    Size dragSize = SystemInformation.DragSize;
                    m_oRectForDrag = new Rectangle(new Point(e.X
                        - dragSize.Width / 2, e.Y
                        - dragSize.Height / 2), dragSize);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// Mouse button has been released
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeVariables_MouseUp(object sender, MouseEventArgs e)
        {
            //reset Drag boundaries
            m_oRectForDrag = Rectangle.Empty;
        }

        /// <summary>
        /// Mouse has been moved over UltraWinTree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeVariables_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                {
                    //Mouse has moved beyond minimum boundaries required to start drag
                    if (m_oRectForDrag != Rectangle.Empty && !m_oRectForDrag.Contains(e.X, e.Y))
                    {
                        //Start a DragDrop operation for Segments, owned UserFolder items 
                        //or AdminFolder items if in AdminMode
                        if (m_oDragNode != null)
                        {
                            // Set data to empty string
                            // This way Word will automatically recognize
                            // and allow dropping onto a document
                            this.treeImport.DoDragDrop("", DragDropEffects.Copy);

                            // If coordinates are to the left of control, node is being
                            // dropped on the Word document screen
                            // Point p = new Point(Control.MousePosition.X, Control.MousePosition.Y);
                            m_oLastPointOnScreen.X = Control.MousePosition.X;
                            m_oLastPointOnScreen.Y = Control.MousePosition.Y;
                            if (!this.TaskPane.RectangleToScreen(this.TaskPane.ClientRectangle).Contains(m_oLastPointOnScreen))
                            {
                                ImportElement(m_oDragNode);
                            }
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Check if drag action has been cancelled by Esc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeVariables_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            try
            {
                //Did the user press escape? 
                if (e.EscapePressed)
                {
                    //User pressed escape
                    //Cancel the Drag
                    e.Action = DragAction.Cancel;
                    m_oDragNode = null;
                    m_oRectForDrag = Rectangle.Empty;
                }
                // Keep track of where on screen the mouse is positioned
                m_oLastPointOnScreen.X = Control.MousePosition.X;
                m_oLastPointOnScreen.Y = Control.MousePosition.Y;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        #endregion
        #region *********************private functions*********************
        /// <summary>
        /// shows the available elements to import
        /// </summary>
        private void ShowElements()
        {
            if (treeSegments.MemberList.Count > 0)
            {
                //GLOG 4473: Elements can also be imported from User Segments
                string xImportID = "";
                ArrayList alMemberList = (ArrayList)this.treeSegments.MemberList[0];
                string xSegmentId = (string)alMemberList[0];
                string xSegmentName = (string)alMemberList[1];
                LMP.Data.mpFolderMemberTypes tSegmentType = (LMP.Data.mpFolderMemberTypes)alMemberList[2];

                ISegmentDef oDef;
                if (xSegmentId.EndsWith(".0") || !xSegmentId.Contains("."))
                {
                    //GLOG 6966: Make sure string can be parsed as a Double even if
                    //the Decimal separator is something other than '.' in Regional Settings
                    if (xSegmentId.EndsWith(".0"))
                    {
                        xSegmentId = xSegmentId.Replace(".0", "");
                    }
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    oDef = (ISegmentDef)oDefs.ItemFromID(Int32.Parse(xSegmentId));
                    xImportID = ((AdminSegmentDef)oDef).ID.ToString();
                }
                else
                {
                    UserSegmentDefs oUDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, LMP.MacPac.Session.CurrentUser.ID);
                    oDef = (ISegmentDef)oUDefs.ItemFromID(xSegmentId);
                    xImportID = ((UserSegmentDef)oDef).ID;
                }
                //create xml document for xpath searchablity
                m_oXML = new XmlDocument();
                m_oXML.LoadXml(oDef.XML);
                m_bWordOpenXML = LMP.String.IsWordOpenXML(oDef.XML); //10.2
                m_oNSM = new XmlNamespaceManager(m_oXML.NameTable);
                if (!LMP.String.IsWordOpenXML(oDef.XML))
                {
                    m_xNSPrefix = String.GetNamespacePrefix(oDef.XML, ForteConstants.MacPacNamespace);
                    m_oNSM.AddNamespace(m_xNSPrefix, ForteConstants.MacPacNamespace);
                }
                else
                    //JTS 07/06/10: Use constant
                    m_oNSM.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);
                treeImport.SuspendLayout();
                switch (this.cmbCategory.Text)
                {
                    case "Variables":
                        if (m_bWordOpenXML)
                            ShowVariablesCC(xImportID, oDef.DisplayName, null);
                        else
                            ShowVariables(xImportID, oDef.DisplayName, null);
                        break;
                    case "Blocks":
                        if (m_bWordOpenXML)
                            ShowBlocksCC(xImportID, oDef.DisplayName, null);
                        else
                            ShowBlocks(xImportID, oDef.DisplayName, null);
                        break;
                        break;
                    case "Properties/Authors":
                        if (m_bWordOpenXML)
                            ShowPropertiesCC(xImportID, oDef.DisplayName, null);
                        else
                            ShowProperties(xImportID, oDef.DisplayName, null);
                        break;
                    case "Segment Actions":
                        ShowSegmentActions(xImportID, oDef.DisplayName, null);
                        break;
                }

                treeImport.ExpandAll();
                treeImport.ResumeLayout();
            }
        }
        /// <summary>
        /// Adds Segment node with child nodes for each defined variable
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="oParent"></param>
        private void ShowVariables(string xSegmentID, string xDisplayName, UltraTreeNode oParent) //GLOG 4473
        {
            UltraTreeNode oNode;

            string xParentTagID = "";
            string xParentObjectData = "";

            ArrayList aVarList = LoadVariables(xSegmentID, ref xParentTagID, ref xParentObjectData);

            if (oParent == null)
            {
                treeImport.Nodes.Clear();
                oNode = treeImport.Nodes.Add(xParentTagID, xDisplayName);
                oNode.LeftImages.Add(Images.DocumentSegment);
            }
            else
            {
                oNode = oParent.Nodes.Add(oParent.Key + "." + xParentTagID, xDisplayName);
                oNode.LeftImages.Add(Images.ComponentSegment);
            }

            for (int i = 0; i < aVarList.Count; i++)
            {
                LibraryVariable oVar = (LibraryVariable)aVarList[i];
                string xName = oVar.Name;
                string xDisplay = oVar.DisplayName;
                //GLOG 4423: Skip if Variable with same name has already been added
                if (oNode.Nodes.Exists(oNode.Key + "." + xName))
                {
                    continue;
                }
                UltraTreeNode oVarNode = oNode.Nodes.Add(oNode.Key + "." + xName, xDisplay);
                oVarNode.Tag = oVar;
                if (oVar.IsTagless)
                    oVarNode.LeftImages.Add(Images.TaglessVariable);
                else
                    oVarNode.LeftImages.Add(Images.TaggedVariable);
            }

            //Add Variables for Child mSEGs belonging to target segment
            string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                xSegmentID + "|')]"; //GLOG 4473
            //Ignore any children nodes with same SegmentID
            string xXPath = @"//" + m_xNSPrefix + ":mSEG[ancestor::" + m_xNSPrefix + ":mSEG[1]" + xSegCondition + "]" +
                "[not(contains(@ObjectData,'SegmentID=" + xSegmentID + "|'))]"; //GLOG 4473
            XmlNodeList oChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
                XmlAttribute oTagID = oSegNode.Attributes["TagID"];
                string xTagID = oTagID.Value;
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;
                string xObjectData = oObjectData.Value;
                string xChildID = ""; //GLOG 4473
                string xChildDisplay = "";
                int iPos = xObjectData.IndexOf("SegmentID=");
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                iPos = xObjectData.IndexOf("DisplayName=");
                if (iPos > -1)
                {
                    iPos = iPos + @"DisplayName=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                    }
                    else
                    {
                        xChildDisplay = xObjectData.Substring(iPos);
                    }
                }
                if (xChildID != "" && xChildDisplay != "") //GLOG 4473
                    ShowVariables(xChildID, xChildDisplay, oNode);
                xChildTagList += xTagID;
            }

            if (xParentObjectData.Contains("ExternalChildren=true"))
            {
                //look for external children - these will be top-level mSEGS with ParentTagID in ObjectData
                string xExternalCondition = @"[contains(@ObjectData,'ParentTagID=" +
                    xParentTagID + "|')][count(ancestor::" + m_xNSPrefix + ":mSEG)=0]";
                xXPath = @"//" + m_xNSPrefix + ":mSEG" + xExternalCondition;

                XmlNodeList oExternalChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
                foreach (XmlNode oExternalNode in oExternalChildren)
                {
                    XmlAttribute oObjectData = oExternalNode.Attributes["ObjectData"];
                    XmlAttribute oTagID = oExternalNode.Attributes["TagID"];
                    string xObjectData = oObjectData.Value;
                    string xTagID = oTagID.Value;
                    //Skip if mSEG  with same TagID has already been added
                    if (xChildTagList.Contains(xTagID))
                        continue;
                    string xChildID = ""; //GLOG 4473
                    string xChildDisplay = "";
                    int iPos = xObjectData.IndexOf("SegmentID=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"SegmentID=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                        }
                        else
                        {
                            xChildID = xObjectData.Substring(iPos); //GLOG 4473
                        }
                    }
                    iPos = xObjectData.IndexOf("DisplayName=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"DisplayName=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                        }
                        else
                        {
                            xChildDisplay = xObjectData.Substring(iPos);
                        }
                    }
                    if (xChildID != "" && xChildDisplay != "")
                        ShowVariables(xChildID, xChildDisplay, oNode);
                    xChildTagList += xTagID;
                }
            }
        }
        /// <summary>
        /// Adds Segment node with child nodes for each defined variable
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="oParent"></param>
        private void ShowVariablesCC(string xSegmentID, string xDisplayName, UltraTreeNode oParent) //GLOG 4473
        {
            UltraTreeNode oNode;

            string xParentTagID = "";
            string xParentObjectData = "";
            //Format Segment ID as it appears in Content Control tag
            string xCCSegmentID = xSegmentID;
            if (xCCSegmentID.EndsWith(".0"))
                xCCSegmentID = xCCSegmentID.Substring(0, xCCSegmentID.Length - 2);
            //JTS 6/17/10:  Changed to reflect proper layout of Tag
            xCCSegmentID = xCCSegmentID.Replace('.', 'd');
            xCCSegmentID = xCCSegmentID.Replace('-', 'm');
            xCCSegmentID = xCCSegmentID.PadLeft(19, '0');

            //GLOG 5291: Use the 19-character tag ID here
            ArrayList aVarList = LoadVariablesCC(xCCSegmentID, ref xParentTagID, ref xParentObjectData);

            if (oParent == null)
            {
                treeImport.Nodes.Clear();
                oNode = treeImport.Nodes.Add(xParentTagID, xDisplayName);
                oNode.LeftImages.Add(Images.DocumentSegment);
            }
            else
            {
                oNode = oParent.Nodes.Add(oParent.Key + "." + xParentTagID, xDisplayName);
                oNode.LeftImages.Add(Images.ComponentSegment);
            }

            for (int i = 0; i < aVarList.Count; i++)
            {
                LibraryVariable oVar = (LibraryVariable)aVarList[i];
                string xName = oVar.Name;
                string xDisplay = oVar.DisplayName;
                //GLOG 4423: Skip if Variable with same name has already been added
                if (oNode.Nodes.Exists(oNode.Key + "." + xName))
                {
                    continue;
                }
                UltraTreeNode oVarNode = oNode.Nodes.Add(oNode.Key + "." + xName, xDisplay);
                oVarNode.Tag = oVar;
                if (oVar.IsTagless)
                    oVarNode.LeftImages.Add(Images.TaglessVariable);
                else
                    oVarNode.LeftImages.Add(Images.TaggedVariable);
            }

            //Add Variables for Child mSEGs belonging to target Content Control
            string xSegCondition = "[w:sdtPr/w:tag[contains(@w:val, '" + xCCSegmentID + "')]]";
            //Ignore any children nodes with same SegmentID
            string xXPath = @"//w:sdt[ancestor::w:sdt[1]" + xSegCondition + 
                "][w:sdtPr/w:tag[contains(@w:val,'mps') and not(contains(@w:val,'" + xCCSegmentID + "'))]]";
            XmlNodeList oChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                //Parse Document Variable ID from Tag
                XmlAttribute oTagID = oSegNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
                string xSegVarID = oTagID.Value.Substring(3, 8);
                XmlNode oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNSM);
                XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
                //Document Variable contains ObjectData for mSEG
                string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                //TagID at start of Doc Variable value
                string xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;
                string xChildID = ""; //GLOG 4473
                string xChildDisplay = "";
                int iPos = xObjectData.IndexOf("SegmentID=");
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                iPos = xObjectData.IndexOf("DisplayName=");
                if (iPos > -1)
                {
                    iPos = iPos + @"DisplayName=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                    }
                    else
                    {
                        xChildDisplay = xObjectData.Substring(iPos);
                    }
                }
                if (xChildID != "" && xChildDisplay != "") //GLOG 4473
                    ShowVariablesCC(xChildID, xChildDisplay, oNode);
                xChildTagList += xTagID;
            }

            if (xParentObjectData.Contains("ExternalChildren=true"))
            {
                //look for external children - these will be top-level mSEGS with ParentTagID in ObjectData
                xXPath = @"descendant::w:sdt[count(ancestor::w:sdt)=0]" +
                    "[w:sdtPr/w:tag[contains(@w:val,'mps') and not(contains(@w:val,'" + xCCSegmentID + "'))]]";

                //Get all top-level mps Content Controls not part of Parent Segment
                XmlNodeList oExternalChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
                foreach (XmlNode oExternalNode in oExternalChildren)
                {
                    //Parse Document Variable ID from Tag
                    XmlAttribute oTagID = oExternalNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
                    string xSegVarID = oTagID.Value.Substring(3, 8);
                    XmlNode oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNSM);
                    XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
                    //Document Variable contains ObjectData for mSEG
                    string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                    //Ignore if ParentTagID doesn't match current Segment
                    if (!xObjectData.Contains("ParentTagID=" + xParentTagID + "|"))
                        continue;
                    //TagID at start of Doc Variable value
                    string xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));
                    //Skip if mSEG with same TagID has already been added
                    if (xChildTagList.Contains(xTagID))
                        continue;
                    string xChildID = ""; //GLOG 4473
                    string xChildDisplay = "";
                    int iPos = xObjectData.IndexOf("SegmentID=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"SegmentID=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                        }
                        else
                        {
                            xChildID = xObjectData.Substring(iPos); //GLOG 4473
                        }
                    }
                    iPos = xObjectData.IndexOf("DisplayName=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"DisplayName=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                        }
                        else
                        {
                            xChildDisplay = xObjectData.Substring(iPos);
                        }
                    }
                    if (xChildID != "" && xChildDisplay != "")
                        ShowVariablesCC(xChildID, xChildDisplay, oNode);
                    xChildTagList += xTagID;
                }
            }
        }
        /// <summary>
        /// Adds Segment node with child nodes for each defined variable
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="oParent"></param>
        private void ShowPropertiesCC(string xSegmentID, string xDisplayName, UltraTreeNode oParent) //GLOG 4473
        {
            UltraTreeNode oNode;

            string xParentTagID = "";
            string xParentObjectData = "";
            //Format Segment ID as it appears in Content Control tag
            string xCCSegmentID = xSegmentID;
            if (xCCSegmentID.EndsWith(".0"))
                xCCSegmentID = xCCSegmentID.Substring(0, xCCSegmentID.Length - 2);
            //JTS 6/17/10:  Changed to reflect proper layout of Tag
            xCCSegmentID = xCCSegmentID.Replace('.', 'd');
            xCCSegmentID = xCCSegmentID.Replace('-', 'm');
            xCCSegmentID = xCCSegmentID.PadLeft(19, '0');

            //xPath to match Content Controls that contain specified Segment ID as part of Tag
            string xSegCondition = "[w:sdtPr/w:tag[contains(@w:val, '" + xCCSegmentID + "')]]";
            string xXPath = @"descendant::w:sdt" + xSegCondition;
            XmlNode oParentSegNode = m_oXML.SelectSingleNode(xXPath, m_oNSM);
            //Parse Document Variable ID from Tag
            XmlAttribute oParentTagID = oParentSegNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
            string xParentSegVarID = oParentTagID.Value.Substring(3, 8);
            XmlNode oParentDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xParentSegVarID + "')]", m_oNSM);
            XmlAttribute oParentVal = oParentDocVarNode.Attributes["w:val"];
            //Document Variable contains ObjectData for mSEG
            xParentObjectData = oParentVal.Value.Substring(oParentVal.Value.IndexOf("��ObjectData=") + 13);
            //TagID at start of Doc Variable value
            xParentTagID = oParentVal.Value.Substring(0, oParentVal.Value.IndexOf("��"));

            LibraryProperties oLP = new LibraryProperties();
            oLP.ObjectData = xParentObjectData;

            if (oParent == null)
            {
                treeImport.Nodes.Clear();


                oNode = treeImport.Nodes.Add(xParentTagID, xDisplayName);
                oNode.Tag = oLP;
                oNode.LeftImages.Add(Images.DocumentSegment);
            }
            else
            {
                oNode = oParent.Nodes.Add(oParent.Key + "." + xParentTagID, xDisplayName);
                oNode.Tag = oLP;
                oNode.LeftImages.Add(Images.ComponentSegment);
            }

            //Ignore any children nodes with same SegmentID
            xXPath = @"//w:sdt[ancestor::w:sdt[1]" + xSegCondition +
                "][w:sdtPr/w:tag[contains(@w:val,'mps') and not(contains(@w:val,'" + xCCSegmentID + "'))]]";
            XmlNodeList oChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                //Parse Document Variable ID from Tag
                XmlAttribute oTagID = oSegNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
                string xSegVarID = oTagID.Value.Substring(3, 8);
                XmlNode oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNSM);
                XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
                //Document Variable contains ObjectData for mSEG
                string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                //TagID at start of Doc Variable value
                string xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;
                string xChildID = ""; //GLOG 4473
                string xChildDisplay = "";
                int iPos = xObjectData.IndexOf("SegmentID=");
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                iPos = xObjectData.IndexOf("DisplayName=");
                if (iPos > -1)
                {
                    iPos = iPos + @"DisplayName=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                    }
                    else
                    {
                        xChildDisplay = xObjectData.Substring(iPos);
                    }
                }
                if (xChildID != "" && xChildDisplay != "") //GLOG 4473
                    ShowPropertiesCC(xChildID, xChildDisplay, oNode);
                xChildTagList += xTagID;
            }

            if (xParentObjectData.Contains("ExternalChildren=true"))
            {
                //look for external children - these will be top-level mSEGS with ParentTagID in ObjectData
                xXPath = @"descendant::w:sdt[count(ancestor::w:sdt)=0]" +
                    "[w:sdtPr/w:tag[contains(@w:val,'mps') and not(contains(@w:val,'" + xCCSegmentID + "'))]]";

                //Get all top-level mps Content Controls not part of Parent Segment
                XmlNodeList oExternalChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
                foreach (XmlNode oExternalNode in oExternalChildren)
                {
                    //Parse Document Variable ID from Tag
                    XmlAttribute oTagID = oExternalNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
                    string xSegVarID = oTagID.Value.Substring(3, 8);
                    XmlNode oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNSM);
                    XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
                    //Document Variable contains ObjectData for mSEG
                    string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                    //Ignore if ParentTagID doesn't match current Segment
                    if (!xObjectData.Contains("ParentTagID=" + xParentTagID + "|"))
                        continue;
                    //TagID at start of Doc Variable value
                    string xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));
                    //Skip if mSEG with same TagID has already been added
                    if (xChildTagList.Contains(xTagID))
                        continue;
                    string xChildID = ""; //GLOG 4473
                    string xChildDisplay = "";
                    int iPos = xObjectData.IndexOf("SegmentID=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"SegmentID=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                        }
                        else
                        {
                            xChildID = xObjectData.Substring(iPos); //GLOG 4473
                        }
                    }
                    iPos = xObjectData.IndexOf("DisplayName=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"DisplayName=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                        }
                        else
                        {
                            xChildDisplay = xObjectData.Substring(iPos);
                        }
                    }
                    if (xChildID != "" && xChildDisplay != "")
                        ShowPropertiesCC(xChildID, xChildDisplay, oNode);
                    xChildTagList += xTagID;
                }
            }
        }
        /// <summary>
        /// Adds Segment node with child nodes for each defined variable
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="oParent"></param>
        private void ShowBlocksCC(string xSegmentID, string xDisplayName, UltraTreeNode oParent) //GLOG 4473
        {
            UltraTreeNode oNode;

            string xParentTagID = "";
            string xParentObjectData = "";
            //Format Segment ID as it appears in Content Control tag
            string xCCSegmentID = xSegmentID;
            if (xCCSegmentID.EndsWith(".0"))
                xCCSegmentID = xCCSegmentID.Substring(0, xCCSegmentID.Length - 2);
            //JTS 6/17/10:  Changed to reflect proper layout of Tag
            xCCSegmentID = xCCSegmentID.Replace('.', 'd');
            xCCSegmentID = xCCSegmentID.Replace('-', 'm');
            xCCSegmentID = xCCSegmentID.PadLeft(19, '0');

            //GLOG 6992: Should be passing formatted Segment ID here
            ArrayList aVarList = LoadBlocksCC(xCCSegmentID, ref xParentTagID, ref xParentObjectData);

            if (oParent == null)
            {
                treeImport.Nodes.Clear();
                oNode = treeImport.Nodes.Add(xParentTagID, xDisplayName);
                oNode.LeftImages.Add(Images.DocumentSegment);
            }
            else
            {
                oNode = oParent.Nodes.Add(oParent.Key + "." + xParentTagID, xDisplayName);
                oNode.LeftImages.Add(Images.ComponentSegment);
            }

            for (int i = 0; i < aVarList.Count; i++)
            {
                LibraryBlock oVar = (LibraryBlock)aVarList[i];
                UltraTreeNode oVarNode = oNode.Nodes.Add(oNode.Key + "." + oVar.Name, oVar.DisplayName);
                oVarNode.Tag = oVar;
                oVarNode.LeftImages.Add(Images.TaggedVariable);
            }

            //Add Variables for Child mSEGs belonging to target Content Control
            string xSegCondition = "[w:sdtPr/w:tag[contains(@w:val, '" + xCCSegmentID + "')]]";
            //Ignore any children nodes with same SegmentID
            string xXPath = @"//w:sdt[ancestor::w:sdt[1]" + xSegCondition +
                "][w:sdtPr/w:tag[contains(@w:val,'mps') and not(contains(@w:val,'" + xCCSegmentID + "'))]]";
            XmlNodeList oChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                //Parse Document Variable ID from Tag
                XmlAttribute oTagID = oSegNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
                string xSegVarID = oTagID.Value.Substring(3, 8);
                XmlNode oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNSM);
                XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
                //Document Variable contains ObjectData for mSEG
                string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                //TagID at start of Doc Variable value
                string xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;
                string xChildID = ""; //GLOG 4473
                string xChildDisplay = "";
                int iPos = xObjectData.IndexOf("SegmentID=");
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                iPos = xObjectData.IndexOf("DisplayName=");
                if (iPos > -1)
                {
                    iPos = iPos + @"DisplayName=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                    }
                    else
                    {
                        xChildDisplay = xObjectData.Substring(iPos);
                    }
                }
                if (xChildID != "" && xChildDisplay != "") //GLOG 4473
                    ShowBlocksCC(xChildID, xChildDisplay, oNode);
                xChildTagList += xTagID;
            }

            if (xParentObjectData.Contains("ExternalChildren=true"))
            {
                //look for external children - these will be top-level mSEGS with ParentTagID in ObjectData
                xXPath = @"descendant::w:sdt[count(ancestor::w:sdt)=0]" +
                    "[w:sdtPr/w:tag[contains(@w:val,'mps') and not(contains(@w:val,'" + xCCSegmentID + "'))]]";

                //Get all top-level mps Content Controls not part of Parent Segment
                XmlNodeList oExternalChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
                foreach (XmlNode oExternalNode in oExternalChildren)
                {
                    //Parse Document Variable ID from Tag
                    XmlAttribute oTagID = oExternalNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
                    string xSegVarID = oTagID.Value.Substring(3, 8);
                    XmlNode oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNSM);
                    XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
                    //Document Variable contains ObjectData for mSEG
                    string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                    //Ignore if ParentTagID doesn't match current Segment
                    if (!xObjectData.Contains("ParentTagID=" + xParentTagID + "|"))
                        continue;
                    //TagID at start of Doc Variable value
                    string xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));
                    //Skip if mSEG with same TagID has already been added
                    if (xChildTagList.Contains(xTagID))
                        continue;
                    string xChildID = ""; //GLOG 4473
                    string xChildDisplay = "";
                    int iPos = xObjectData.IndexOf("SegmentID=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"SegmentID=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                        }
                        else
                        {
                            xChildID = xObjectData.Substring(iPos); //GLOG 4473
                        }
                    }
                    iPos = xObjectData.IndexOf("DisplayName=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"DisplayName=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                        }
                        else
                        {
                            xChildDisplay = xObjectData.Substring(iPos);
                        }
                    }
                    if (xChildID != "" && xChildDisplay != "")
                        ShowBlocksCC(xChildID, xChildDisplay, oNode);
                    xChildTagList += xTagID;
                }
            }
        }
        /// <summary>
        /// Adds Segment node with child nodes for each defined variable
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="oParent"></param>
        private void ShowBlocks(string xSegmentID, string xDisplayName, UltraTreeNode oParent) //GLOG 4473
        {
            UltraTreeNode oNode;

            string xParentTagID = "";
            string xParentObjectData = "";
            ArrayList aVarList = LoadBlocks(xSegmentID, ref xParentTagID, ref xParentObjectData);
            if (oParent == null)
            {
                treeImport.Nodes.Clear();
                oNode = treeImport.Nodes.Add(xParentTagID, xDisplayName);
                oNode.LeftImages.Add(Images.DocumentSegment);
            }
            else
            {
                oNode = oParent.Nodes.Add(oParent.Key + "." + xParentTagID, xDisplayName);
                oNode.LeftImages.Add(Images.ComponentSegment);
            }

            for (int i = 0; i < aVarList.Count; i++)
            {
                LibraryBlock oVar = (LibraryBlock)aVarList[i];
                UltraTreeNode oVarNode = oNode.Nodes.Add(oNode.Key + "." + oVar.Name, oVar.DisplayName);
                oVarNode.Tag = oVar;
                oVarNode.LeftImages.Add(Images.TaggedVariable);
            }

            //Add blocks for Child mSEGs belonging to target segment
            string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                xSegmentID + "|')]"; //GLOG 4473
            //Ignore any children nodes with same SegmentID
            string xXPath = @"//" + m_xNSPrefix + ":mSEG[ancestor::" + m_xNSPrefix + ":mSEG[1]" + xSegCondition + "]" +
                "[not(contains(@ObjectData,'SegmentID=" + xSegmentID + "|'))]"; //GLOG 4473
            XmlNodeList oChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
                XmlAttribute oTagID = oSegNode.Attributes["TagID"];
                string xTagID = oTagID.Value;
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;
                string xObjectData = oObjectData.Value;
                string xChildID = ""; //GLOG 4473
                string xChildDisplay = "";
                int iPos = xObjectData.IndexOf("SegmentID=");
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                iPos = xObjectData.IndexOf("DisplayName=");
                if (iPos > -1)
                {
                    iPos = iPos + @"DisplayName=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                    }
                    else
                    {
                        xChildDisplay = xObjectData.Substring(iPos);
                    }
                }
                if (xChildID != "" && xChildDisplay != "")
                    ShowBlocks(xChildID, xChildDisplay, oNode);
                xChildTagList += xTagID;
            }

            if (xParentObjectData.Contains("ExternalChildren=true"))
            {
                //look for external children - these will be top-level mSEGS with ParentTagID in ObjectData
                string xExternalCondition = @"[contains(@ObjectData,'ParentTagID=" +
                    xParentTagID + "|')][count(ancestor::" + m_xNSPrefix + ":mSEG)=0]";
                xXPath = @"//" + m_xNSPrefix + ":mSEG" + xExternalCondition;

                XmlNodeList oExternalChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
                foreach (XmlNode oExternalNode in oExternalChildren)
                {
                    XmlAttribute oObjectData = oExternalNode.Attributes["ObjectData"];
                    XmlAttribute oTagID = oExternalNode.Attributes["TagID"];
                    string xObjectData = oObjectData.Value;
                    string xTagID = oTagID.Value;
                    //Skip if mSEG  with same TagID has already been added
                    if (xChildTagList.Contains(xTagID))
                        continue;
                    string xChildID = ""; //GLOG 4473
                    string xChildDisplay = "";
                    int iPos = xObjectData.IndexOf("SegmentID=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"SegmentID=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                        }
                        else
                        {
                            xChildID = xObjectData.Substring(iPos); //GLOG 4473
                        }
                    }
                    iPos = xObjectData.IndexOf("DisplayName=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"DisplayName=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                        }
                        else
                        {
                            xChildDisplay = xObjectData.Substring(iPos);
                        }
                    }
                    if (xChildID != "" && xChildDisplay != "") //GLOG 4473
                        ShowBlocks(xChildID, xChildDisplay, oNode);
                    xChildTagList += xTagID;
                }
            }
        }
        /// <summary>
        /// Adds Segment node with child nodes for each defined variable
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="oParent"></param>
        private void ShowProperties(string xSegmentID, string xDisplayName, UltraTreeNode oParent)  //GLOG 4473
        {
            UltraTreeNode oNode;

            //get object data for mSEG
            string xParentSegXPath = @"//" + m_xNSPrefix + @":mSEG[contains(@ObjectData,'SegmentID=" + xSegmentID + "|')]";  //GLOG 4473
            XmlNode oParentSegNode = m_oXML.SelectSingleNode(xParentSegXPath, m_oNSM);
            XmlAttribute oObjectData = oParentSegNode.Attributes["ObjectData"];
            string xParentTagID = oParentSegNode.Attributes["TagID"].Value;
            string xParentObjectData = oObjectData.Value;

            LibraryProperties oLP = new LibraryProperties();
            oLP.ObjectData = oObjectData.Value;

            if (oParent == null)
            {
                treeImport.Nodes.Clear();


                oNode = treeImport.Nodes.Add(xParentTagID, xDisplayName);
                oNode.Tag = oLP;
                oNode.LeftImages.Add(Images.DocumentSegment);
            }
            else
            {
                oNode = oParent.Nodes.Add(oParent.Key + "." + xParentTagID, xDisplayName);
                oNode.Tag = oLP;
                oNode.LeftImages.Add(Images.ComponentSegment);
            }

            //Add nodes for Child mSEGs belonging to target segment
            string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                xSegmentID + "|')]";  //GLOG 4473
            //Ignore any children nodes with same SegmentID
            string xXPath = @"//" + m_xNSPrefix + ":mSEG[ancestor::" + m_xNSPrefix + ":mSEG[1]" + xSegCondition + "]" +
                "[not(contains(@ObjectData,'SegmentID=" + xSegmentID + "|'))]";  //GLOG 4473
            XmlNodeList oChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                oObjectData = oSegNode.Attributes["ObjectData"];
                XmlAttribute oTagID = oSegNode.Attributes["TagID"];
                string xTagID = oTagID.Value;
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;
                string xObjectData = oObjectData.Value;
                string xChildID = ""; //GLOG 4473
                string xChildDisplay = "";
                int iPos = xObjectData.IndexOf("SegmentID=");
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                iPos = xObjectData.IndexOf("DisplayName=");
                if (iPos > -1)
                {
                    iPos = iPos + @"DisplayName=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                    }
                    else
                    {
                        xChildDisplay = xObjectData.Substring(iPos);
                    }
                }
                if (xChildID != "" && xChildDisplay != "")  //GLOG 4473
                    ShowProperties(xChildID, xChildDisplay, oNode);
                xChildTagList += xTagID;
            }

            if (xParentObjectData.Contains("ExternalChildren=true"))
            {
                //look for external children - these will be top-level mSEGS with ParentTagID in ObjectData
                string xExternalCondition = @"[contains(@ObjectData,'ParentTagID=" +
                    xParentTagID + "|')][count(ancestor::" + m_xNSPrefix + ":mSEG)=0]";
                xXPath = @"//" + m_xNSPrefix + ":mSEG" + xExternalCondition;

                XmlNodeList oExternalChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
                foreach (XmlNode oExternalNode in oExternalChildren)
                {
                    oObjectData = oExternalNode.Attributes["ObjectData"];
                    XmlAttribute oTagID = oExternalNode.Attributes["TagID"];
                    string xObjectData = oObjectData.Value;
                    string xTagID = oTagID.Value;
                    //Skip if mSEG  with same TagID has already been added
                    if (xChildTagList.Contains(xTagID))
                        continue;
                    string xChildID = ""; //GLOG 4473
                    string xChildDisplay = "";
                    int iPos = xObjectData.IndexOf("SegmentID=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"SegmentID=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                        }
                        else
                        {
                            xChildID = xObjectData.Substring(iPos); //GLOG 4473
                        }
                    }
                    iPos = xObjectData.IndexOf("DisplayName=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"DisplayName=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                        }
                        else
                        {
                            xChildDisplay = xObjectData.Substring(iPos);
                        }
                    }
                    if (xChildID != "" && xChildDisplay != "") //GLOG 4473
                        ShowProperties(xChildID, xChildDisplay, oNode);
                    xChildTagList += xTagID;
                }
            }
        }
        /// <summary>
        /// shows tree to import author configuration
        /// </summary>
        /// <param name="iSegmentID"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="oParent"></param>
        private void ShowSegmentActions(string xSegmentID, string xDisplayName, UltraTreeNode oParent) //GLOG 4473
        {
            UltraTreeNode oNode;

            //get object data for mSEG
            string xParentSegXPath = @"//" + m_xNSPrefix + @":mSEG[contains(@ObjectData,'SegmentID=" + xSegmentID + "|')]"; //GLOG 4473
            XmlNode oParentSegNode = m_oXML.SelectSingleNode(xParentSegXPath, m_oNSM);
            XmlAttribute oObjectData = oParentSegNode.Attributes["ObjectData"];
            string xParentTagID = oParentSegNode.Attributes["TagID"].Value;
            string xParentObjectData = oObjectData.Value;

            MatchCollection oMatches = Regex.Matches(xParentObjectData,
                @"AuthorNodeUILabel=.*?\||MaxAuthors=.*?\|AuthorControlProperties=.*?\||AuthorsHelpText=.*?\|");

            foreach (Match oMatch in oMatches)
            {
                xParentObjectData += oMatch.Value;
            }

            LibraryAuthors oLP = new LibraryAuthors();
            oLP.ObjectData = xParentObjectData;

            if (oParent == null)
            {
                treeImport.Nodes.Clear();


                oNode = treeImport.Nodes.Add(xParentTagID, xDisplayName);
                oNode.Tag = oLP;
                oNode.LeftImages.Add(Images.DocumentSegment);
            }
            else
            {
                oNode = oParent.Nodes.Add(oParent.Key + "." + xParentTagID, xDisplayName);
                oNode.Tag = oLP;
                oNode.LeftImages.Add(Images.ComponentSegment);
            }

            //Add nodes for Child mSEGs belonging to target segment
            string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                xSegmentID + "|')]"; //GLOG 4473
            //Ignore any children nodes with same SegmentID
            string xXPath = @"//" + m_xNSPrefix + ":mSEG[ancestor::" + m_xNSPrefix + ":mSEG[1]" + xSegCondition + "]" +
                "[not(contains(@ObjectData,'SegmentID=" + xSegmentID + "|'))]"; //GLOG 4473
            XmlNodeList oChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                oObjectData = oSegNode.Attributes["ObjectData"];
                XmlAttribute oTagID = oSegNode.Attributes["TagID"];
                string xTagID = oTagID.Value;
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;
                string xObjectData = oObjectData.Value;
                string xChildID = ""; //GLOG 4473
                string xChildDisplay = "";
                int iPos = xObjectData.IndexOf("SegmentID=");
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                iPos = xObjectData.IndexOf("DisplayName=");
                if (iPos > -1)
                {
                    iPos = iPos + @"DisplayName=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                    }
                    else
                    {
                        xChildDisplay = xObjectData.Substring(iPos);
                    }
                }
                if (xChildID != "" && xChildDisplay != "") //GLOG 4473
                    ShowProperties(xChildID, xChildDisplay, oNode);
                xChildTagList += xTagID;
            }

            if (xParentObjectData.Contains("ExternalChildren=true"))
            {
                //look for external children - these will be top-level mSEGS with ParentTagID in ObjectData
                string xExternalCondition = @"[contains(@ObjectData,'ParentTagID=" +
                    xParentTagID + "|')][count(ancestor::" + m_xNSPrefix + ":mSEG)=0]";
                xXPath = @"//" + m_xNSPrefix + ":mSEG" + xExternalCondition;

                XmlNodeList oExternalChildren = m_oXML.SelectNodes(xXPath, m_oNSM);
                foreach (XmlNode oExternalNode in oExternalChildren)
                {
                    oObjectData = oExternalNode.Attributes["ObjectData"];
                    XmlAttribute oTagID = oExternalNode.Attributes["TagID"];
                    string xObjectData = oObjectData.Value;
                    string xTagID = oTagID.Value;
                    //Skip if mSEG  with same TagID has already been added
                    if (xChildTagList.Contains(xTagID))
                        continue;
                    string xChildID = ""; //GLOG 4473
                    string xChildDisplay = "";
                    int iPos = xObjectData.IndexOf("SegmentID=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"SegmentID=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                        }
                        else
                        {
                            xChildID = xObjectData.Substring(iPos); //GLOG 4473
                        }
                    }
                    iPos = xObjectData.IndexOf("DisplayName=");
                    if (iPos > -1)
                    {
                        iPos = iPos + @"DisplayName=".Length;
                        int iEndPos = xObjectData.IndexOf('|', iPos);
                        if (iEndPos > -1)
                        {
                            xChildDisplay = xObjectData.Substring(iPos, iEndPos - iPos);
                        }
                        else
                        {
                            xChildDisplay = xObjectData.Substring(iPos);
                        }
                    }
                    if (xChildID != "" && xChildDisplay != "") //GLOG 4473
                        ShowProperties(xChildID, xChildDisplay, oNode);
                    xChildTagList += xTagID;
                }
            }
        }
        /// <summary>
        /// Retrieve list of Variables for the selected Segment ID
        /// </summary>
        /// <param name="xXML"></param>
        private ArrayList LoadVariables(string xSegmentID, ref string xTagID, ref string xObjectData) //GLOG 4473
        {

            ArrayList aVarList = new ArrayList();

            //get object data of target segment
            string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                xSegmentID + "|')]"; //GLOG 4473

            string xXPath = @"//" + m_xNSPrefix + ":mSEG" + xSegCondition;
            XmlNode oSegNode = m_oXML.SelectSingleNode(xXPath, m_oNSM);
            XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
            XmlAttribute oTagID = oSegNode.Attributes["TagID"];
            xObjectData = oObjectData.Value;
            xTagID = oTagID.Value;

            //get all mSEGS so that we can retrieve
            //deleted author preference variables
            XmlNodeList oNodes = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xTaggedVariableData = "";
            foreach (XmlNode oSegmentNode in oNodes)
            {
                XmlAttribute oDeletionScopes = oSegmentNode.Attributes["DeletedScopes"];
                if (oDeletionScopes != null)
                    xTaggedVariableData += oDeletionScopes.Value;
            }

            //append object data of mVars belonging to target segment
            xXPath = @"//" + m_xNSPrefix + ":mVar[ancestor::" + m_xNSPrefix + ":mSEG[1]" + xSegCondition + ']';
            XmlNodeList oVarNodes = m_oXML.SelectNodes(xXPath, m_oNSM);
            foreach (XmlNode oVarNode in oVarNodes)
            {
                oObjectData = oVarNode.Attributes["ObjectData"];
                xTaggedVariableData += oObjectData.Value;
            }

            //get variable definitions
            string xPattern = @"VariableDefinition=.*?\|";
            Regex oRegex = new Regex(xPattern, RegexOptions.Singleline);
            bool bTagless = true;
            //Read Tagless first, then Tagged
            foreach (string xData in new string[] { xObjectData, xTaggedVariableData })
            {
                MatchCollection oMatches = oRegex.Matches(xData);
                if (oMatches.Count > 0)
                {
                    string xDefs = "";
                    foreach (Match oMatch in oMatches)
                    {
                        string xDef = oMatch.Value;
                        //Remove "VariableDefition="
                        xDef = xDef.Substring(19);
                        //Skip if already added
                        if (xDefs.IndexOf(xDef) == -1)
                        {
                            //parse definition to get only the props we need
                            string[] aDef = xDef.Split('�');

                            LibraryVariable oVar = new LibraryVariable();

                            oVar.Name = aDef[1];
                            //GLOG 5028: Remove any XML Tokens from Display Name
                            oVar.DisplayName = LMP.String.RestoreXMLChars(aDef[2]);
                            oVar.ExecutionIndex = Int32.Parse(aDef[4]);
                            oVar.ObjectData = xDef;
                            oVar.IsTagless = bTagless;

                            //add to array list
                            aVarList.Add(oVar);

                            //avoid duplicates
                            xDefs += xDef;
                        }
                    }
                }
                //Set value for next pass
                bTagless = false;
            }
            aVarList.Sort(new VariableSorter());
            return aVarList;
        }
        /// <summary>
        /// Retrieve list of Variables for the selected Segment ID
        /// </summary>
        /// <param name="xXML"></param>
        private ArrayList LoadVariablesCC(string xCCSegmentID, ref string xTagID, ref string xObjectData) //GLOG 4473
        {

            ArrayList aVarList = new ArrayList();

            //xPath to match Content Controls that contain specified Segment ID as part of Tag
            string xSegCondition = "[w:sdtPr/w:tag[contains(@w:val, '" + xCCSegmentID + "')]]";
            string xXPath = @"descendant::w:sdt" + xSegCondition;
            XmlNode oSegNode = m_oXML.SelectSingleNode(xXPath, m_oNSM);
            //Parse Document Variable ID from Tag
            XmlAttribute oTagID = oSegNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
            string xSegVarID = oTagID.Value.Substring(3, 8);
            XmlNode oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNSM);
            XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
            //Document Variable contains ObjectData for mSEG
            xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
            //TagID at start of Doc Variable value
            xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));

            string xDeletedTags = "";
            string xTaggedVariableData = "";
            string xVarID = "";
            int iDeleted = 1;

            //Append all mpd Variables for this Segment to get DeletedTags
            do
            {
                string xDeleted = iDeleted++.ToString().PadLeft(2, '0');
                xXPath = @"//w:docVar[contains(@w:name,'mpd" + xSegVarID + xDeleted + "')]";
                oDocVarNode = m_oXML.SelectSingleNode(xXPath, m_oNSM);
                if (oDocVarNode != null)
                {
                    xDeletedTags += oDocVarNode.Attributes["w:val"].Value;
                }
            } while (oDocVarNode != null);
            if (xDeletedTags != "")
            {
                int iPos = xDeletedTags.IndexOf("w:tag w:val=\"mpv");
                while (iPos > -1)
                {
                    //ID of associated Doc Variable is in DeletedTag XML
                    xVarID = xDeletedTags.Substring(iPos + "w:tag w:val=\"mpv".Length, 8);
                    oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xVarID + "')]", m_oNSM);
                    oVal = oDocVarNode.Attributes["w:val"];
                    //Document Variable contains ObjectData for mVar
                    string xVarData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                    xTaggedVariableData += xVarData;
                    iPos = xDeletedTags.IndexOf("w:tag w:val=\"mpv", iPos + 1);
                }
            }
            
            //append object data of mVars belonging to target segment
            //xXPath = @"//w:sdt[ancestor::w:sdt[1]" + xSegCondition + " and w:sdtPr/w:tag[contains(@w:val,'mpv')]]";
            //GLOG 8182: select variables whose immediate ancestor is mSEG for current segment,
            //or whose immediate ancestor is an mBlock and ancestor above that is the mSEG for current segment
            xXPath = @"//w:sdt[(w:sdtPr/w:tag[contains(@w:val,'mpv')] and (ancestor::w:sdt[1]" + xSegCondition + ")) or " +
                "(w:sdtPr/w:tag[contains(@w:val,'mpv')] and (ancestor::w:sdt[2]" + xSegCondition + ") and (ancestor::w:sdt[1][w:sdtPr/w:tag[contains(@w:val, 'mpb')]]))]";
            XmlNodeList oVarNodes = m_oXML.SelectNodes(xXPath, m_oNSM);
            foreach (XmlNode oVarNode in oVarNodes)
            {
                //Check tags of child content controls
                oTagID = oVarNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
                if (oTagID.Value.StartsWith("mpv"))
                {
                    xVarID = oTagID.Value.Substring(3, 8);
                    oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xVarID + "')]", m_oNSM);
                    oVal = oDocVarNode.Attributes["w:val"];
                    //Document Variable contains ObjectData for mVar
                    string xVarData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                    xTaggedVariableData += xVarData;
                }
            }

            //get variable definitions
            string xPattern = @"VariableDefinition=.*?\|";
            Regex oRegex = new Regex(xPattern, RegexOptions.Singleline);
            bool bTagless = true;
            //Read Tagless first, then Tagged
            foreach (string xData in new string[] { xObjectData, xTaggedVariableData })
            {
                MatchCollection oMatches = oRegex.Matches(xData);
                if (oMatches.Count > 0)
                {
                    string xDefs = "";
                    foreach (Match oMatch in oMatches)
                    {
                        string xDef = oMatch.Value;
                        //Remove "VariableDefition="
                        xDef = xDef.Substring(19);
                        //Skip if already added
                        if (xDefs.IndexOf(xDef) == -1)
                        {
                            //parse definition to get only the props we need
                            string[] aDef = xDef.Split('�');

                            LibraryVariable oVar = new LibraryVariable();

                            oVar.Name = aDef[1];
                            oVar.DisplayName = aDef[2];
                            oVar.ExecutionIndex = Int32.Parse(aDef[4]);
                            oVar.ObjectData = xDef;
                            oVar.IsTagless = bTagless;

                            //add to array list
                            aVarList.Add(oVar);

                            //avoid duplicates
                            xDefs += xDef;
                        }
                    }
                }
                //Set value for next pass
                bTagless = false;
            }
            aVarList.Sort(new VariableSorter());
            return aVarList;
        }
        /// <summary>
        /// Retrieve list of Variables for the selected Segment ID
        /// </summary>
        /// <param name="xXML"></param>
        private ArrayList LoadBlocks(string xSegmentID, ref string xTagID, ref string xObjectData)
        {

            ArrayList aBlockList = new ArrayList();

            //get object data of target segment
            string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                xSegmentID + "|')]"; //GLOG 4473

            string xXPath = @"//" + m_xNSPrefix + ":mSEG" + xSegCondition;
            XmlNode oSegNode = m_oXML.SelectSingleNode(xXPath, m_oNSM);
            XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
            XmlAttribute oTagID = oSegNode.Attributes["TagID"];
            xObjectData = oObjectData.Value;
            xTagID = oTagID.Value;

            //get all mSEGS so that we can retrieve
            //deleted author preference variables
            XmlNodeList oNodes = m_oXML.SelectNodes(xXPath, m_oNSM);
            string xDeletedTags = "";
            string xDefs = "";
            foreach (XmlNode oSegmentNode in oNodes)
            {
                XmlAttribute oDeletionScopes = oSegmentNode.Attributes["DeletedScopes"];
                if (oDeletionScopes != null)
                    xDeletedTags += oDeletionScopes.Value;
            }
            //JTS 5/31/10: Show Deleted Blocks also
            if (xDeletedTags != "")
            {
                //Split into separate DeletedTag entries
                string[] aDeletedTags = xDeletedTags.Split(new string[] { "|DeletedTag=" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string xDeletedTag in aDeletedTags)
                {
                    int iPos = xDeletedTag.IndexOf(m_xNSPrefix + ":mBlock TagID=");
                    if (iPos > -1)
                    {

                        string xObjData = xDeletedTag.Substring(xDeletedTag.IndexOf(" ObjectData=\"", iPos) + 13);
                        xObjData = xObjData.Substring(0, xObjData.IndexOf("\">"));
                        if (xDefs.IndexOf(xObjData) == -1)
                        {
                            string[] aObjData = xObjData.Split('|');
                            LibraryBlock oLB = new LibraryBlock();
                            oLB.Name = aObjData[0];
                            oLB.DisplayName = aObjData[1];
                            oLB.ObjectData = xObjData;
                            string xXML = xDeletedTag.Substring(xDeletedTag.IndexOf("�") + 1);
                            //Replace opening XML tags
                            xXML = xDeletedTag.Replace("�", "<");
                            //Set XML to content of outermost mBlock tag
                            int iCC = xXML.IndexOf("<" + m_xNSPrefix + ":mBlock TagID=");
                            if (iCC > -1)
                            {
                                iCC = xXML.IndexOf(">", iCC) + 1;
                                int iCC2 = xXML.LastIndexOf("</" + m_xNSPrefix + ":mBlock>");
                                if (iCC2 > -1)
                                {
                                    xXML = xXML.Substring(iCC, iCC2 - iCC);
                                    oLB.XML = xXML;
                                    aBlockList.Add(oLB);
                                    xDefs += xObjData;
                                }
                            }
                        }
                    }
                }
            }

            //append object data of mVars belonging to target segment
            xXPath = @"//" + m_xNSPrefix + ":mBlock[ancestor::" + m_xNSPrefix + ":mSEG[1]" + xSegCondition + ']';
            XmlNodeList BlockNodes = m_oXML.SelectNodes(xXPath, m_oNSM);
            foreach (XmlNode oBlockNode in BlockNodes)
            {
                string xObjData = oBlockNode.Attributes["ObjectData"].Value;
                //JTS 5/31/10: Add block if not already added
                if (!xDefs.Contains(xObjectData))
                {
                    string[] aObjData = xObjData.Split('|');
                    LibraryBlock oLB = new LibraryBlock();
                    oLB.Name = aObjData[0];
                    //GLOG 5028: Remove any XML Tokens from Display Name
                    oLB.DisplayName = LMP.String.RestoreXMLChars(aObjData[1]);
                    oLB.ObjectData = xObjData;
                    oLB.XML = oBlockNode.InnerXml;
                    aBlockList.Add(oLB);
                    xDefs += xObjectData;
                }
            }

            aBlockList.Sort(new BlockSorter());
            return aBlockList;
        }
        /// <summary>
        /// Retrieve list of Variables for the selected Segment ID
        /// </summary>
        /// <param name="xXML"></param>
        private ArrayList LoadBlocksCC(string xCCSegmentID, ref string xTagID, ref string xObjectData)
        {

            ArrayList aBlockList = new ArrayList();

            //xPath to match Content Controls that contain specified Segment ID as part of Tag
            string xSegCondition = "[w:sdtPr/w:tag[contains(@w:val, '" + xCCSegmentID + "')]]";
            string xXPath = @"descendant::w:sdt" + xSegCondition;
            XmlNode oSegNode = m_oXML.SelectSingleNode(xXPath, m_oNSM);
            //Parse Document Variable ID from Tag
            XmlAttribute oTagID = oSegNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
            string xSegVarID = oTagID.Value.Substring(3, 8);
            XmlNode oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNSM);
            XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
            //Document Variable contains ObjectData for mSEG
            xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
            //TagID at start of Doc Variable value
            xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));

            string xDeletedTags = "";
            string xBlockID = "";
            int iDeleted = 1;
            string xDefs = "";

            //Append all mpd Variables for this Segment to get DeletedTags
            do
            {
                string xDeleted = iDeleted++.ToString().PadLeft(2, '0');
                xXPath = @"//w:docVar[contains(@w:name,'mpd" + xSegVarID + xDeleted + "')]";
                oDocVarNode = m_oXML.SelectSingleNode(xXPath, m_oNSM);
                if (oDocVarNode != null)
                {
                    xDeletedTags += oDocVarNode.Attributes["w:val"].Value;
                }
            } while (oDocVarNode != null);
            if (xDeletedTags != "")
            {
                //Split into separate DeletedTag entries
                string[] aDeletedTags = xDeletedTags.Split(new string[] {"|DeletedTag="}, StringSplitOptions.RemoveEmptyEntries);
                foreach (string xDeletedTag in aDeletedTags)
                {
                    int iPos = xDeletedTag.IndexOf("w:tag w:val=\"mpb");
                    if (iPos > -1)
                    {
                        //ID of associated Doc Variable is in DeletedTag XML
                        xBlockID = xDeletedTag.Substring(iPos + "w:tag w:val=\"mpb".Length, 8);
                        oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xBlockID + "')]", m_oNSM);
                        if (oDocVarNode != null)
                        {
                            oVal = oDocVarNode.Attributes["w:val"];
                            //Document Variable contains ObjectData for mVar
                            string xObjData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                            int iSep = xObjData.IndexOf("��");
                            if (iSep > -1)
                                xObjData = xObjData.Substring(0, iSep);
                            if (xDefs.IndexOf(xObjData) == -1)
                            {
                                string[] aObjData = xObjData.Split('|');
                                LibraryBlock oLB = new LibraryBlock();
                                oLB.Name = aObjData[0];
                                oLB.DisplayName = aObjData[1];
                                oLB.ObjectData = xObjData;
                                string xXML = xDeletedTag.Substring(xDeletedTag.IndexOf("�") + 1);
                                //Replace opening XML tags
                                xXML = xDeletedTag.Replace("�", "<");
                                //Set XML to content of outermost sdtContent node
                                int iCC = xXML.IndexOf("<w:sdtContent");
                                if (iCC > -1)
                                {
                                    iCC = xXML.IndexOf(">", iCC) + 1;
                                    int iCC2 = xXML.LastIndexOf("</w:sdtContent");
                                    if (iCC2 > -1)
                                    {
                                        xXML = xXML.Substring(iCC, iCC2 - iCC);
                                        oLB.XML = xXML;
                                        aBlockList.Add(oLB);
                                        xDefs += xObjData;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //append object data of mBlocks belonging to target segment
            xXPath = @"//w:sdt[ancestor::w:sdt[1]" + xSegCondition + " and w:sdtPr/w:tag[contains(@w:val,'mpb')]]";
            XmlNodeList BlockNodes = m_oXML.SelectNodes(xXPath, m_oNSM);
            foreach (XmlNode oBlockNode in BlockNodes)
            {
                string xObjData = "";
                //Check tags of child content controls
                oTagID = oBlockNode.SelectSingleNode("w:sdtPr/w:tag", m_oNSM).Attributes["w:val"];
                if (oTagID.Value.StartsWith("mpb"))
                {
                    xBlockID = oTagID.Value.Substring(3, 8);
                    oDocVarNode = m_oXML.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xBlockID + "')]", m_oNSM);
                    oVal = oDocVarNode.Attributes["w:val"];
                    //Document Variable contains ObjectData for mVar
                    xObjData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                    int iSep = xObjData.IndexOf("��");
                    if (iSep > -1)
                        xObjData = xObjData.Substring(0, iSep);
                    if (xDefs.IndexOf(xObjData) == -1)
                    {
                        string[] aObjData = xObjData.Split('|');
                        LibraryBlock oLB = new LibraryBlock();
                        oLB.Name = aObjData[0];
                        oLB.DisplayName = aObjData[1];
                        oLB.ObjectData = xObjData;
                        oLB.XML = oBlockNode.SelectSingleNode("w:sdtContent", m_oNSM).InnerXml;
                        aBlockList.Add(oLB);
                    }
                }
            }

            aBlockList.Sort(new BlockSorter());
            return aBlockList;
        }
        /// <summary>
        /// imports the element associated with the specified node
        /// </summary>
        /// <param name="oNode"></param>
        private void ImportElement(UltraTreeNode oNode)
        {
            //Only valid in Design Document
            if (this.TaskPane.Mode != ForteDocument.Modes.Design || oNode == null)
                return;

            LibraryElement oElement = (LibraryElement)oNode.Tag;

            if (oElement == null)
                return;

            switch (oElement.GetType().Name)
            {
                case "LibraryVariable":
                    ImportVariable((LibraryVariable)oElement);
                    break;
                case "LibraryBlock":
                    ImportBlock((LibraryBlock)oElement);
                    break;
                case "LibraryProperties":
                    ImportProperties((LibraryProperties)oElement);
                    break;
            }
        }

        //imports the specified properties to the designer segment
        private void ImportProperties(LibraryProperties oLibraryProperties)
        {
            //get object data from selected segment
            string xObjectData = oLibraryProperties.ObjectData;
            string[] aObjectData = xObjectData.Split('|');
            Segment oTargetSegment = this.m_oMPDocument.Segments[0];

            //replace appropriate properties with values from target segment
            string xSegmentID = "";
            //GLOG 8413: Allow importing into Segment Designer segments
            xSegmentID = oTargetSegment.ID;
            if (xSegmentID.EndsWith(".0"))
                xSegmentID = xSegmentID.Replace(".0", "");
            //if (oTargetSegment is AdminSegment)
            //    xSegmentID = oTargetSegment.ID1.ToString();
            //else
            //    xSegmentID = oTargetSegment.ID;

            string xTargetObjData = null;

            if (oTargetSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //get target object data
                Word.XMLNode oTargetObjDataNode = oTargetSegment
                    .PrimaryWordTag.SelectSingleNode("@ObjectData", "", true);
                xTargetObjData = oTargetObjDataNode.NodeValue;
            }
            else
            {
                //get target object data
                xTargetObjData = LMP.Forte.MSWord.WordDoc.GetAttributeValue_CC(
                    oTargetSegment.PrimaryContentControl, "ObjectData");
            }

            //get target segment action definitions
            Match oMatch = Regex.Match(xTargetObjData, @"SegmentActionDefinitions=.*?\|");
            string xTargetSegActionDefs = "";
            if (oMatch != null)
                xTargetSegActionDefs = oMatch.Value;

            //get target external children value - 
            //use false if no value found
            string xExternalChildren = "ExternalChildren=false|";
            oMatch = Regex.Match(xTargetObjData, @"ExternalChildren=.*?\|");
            if (oMatch != null)
                xExternalChildren = oMatch.Value;

            xObjectData = Regex.Replace(xObjectData, @"SegmentID=.*?\|", "SegmentID=" + xSegmentID + "|");
            xObjectData = Regex.Replace(xObjectData, @"Name=.*?\|", "Name=" + oTargetSegment.Name + "|");
            xObjectData = Regex.Replace(xObjectData, @"DisplayName=.*?\|", "DisplayName=" + oTargetSegment.DisplayName + "|");
            xObjectData = Regex.Replace(xObjectData, @"VariableDefinition=.*?\|", "");
            xObjectData = Regex.Replace(xObjectData, @"ExternalChildren=.*?\|", xExternalChildren);
            if(Regex.IsMatch(xObjectData, @"SegmentActionDefinitions="))
                //segment action definitions exist in the source - replace
                //with target definitions so as not to overwrite
                xObjectData = Regex.Replace(xObjectData, @"SegmentActionDefinitions=.*?\|", xTargetSegActionDefs);
            else
                //segment action definitions do not exist in the source-
                //add to the source so as not to overwrite actions already
                //defined in the target
                xObjectData += xTargetSegActionDefs;

            //replace object data of target segment with modified object data -
            //10.2: This works for XMLNodes or ContentControls
            oTargetSegment.Nodes.SetItemObjectData(oTargetSegment.FullTagID, xObjectData);

            this.TaskPane.Refresh(false, true, false);

            MessageBox.Show("Properties have been imported.", LMP.ComponentProperties.ProductName,
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        /// <summary>
        /// Creates new Variable using selected Variable node as model
        /// </summary>
        /// <param name="oNode"></param>
        private void ImportVariable(LibraryVariable oLibraryVariable)
        {
            Word.Range oRange = m_oMPDocument.WordDocument.Application.Selection.Range;
            Segment oSegment = m_oMPDocument.GetTopLevelSegmentFromSelection();

            LMP.Forte.MSWord.TagInsertionValidityStates iValid = LMP.Forte.MSWord.TagInsertionValidityStates.Valid;
            if (oSegment == null)
                iValid = LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoContainingSegmentExists;
            else if (!oLibraryVariable.IsTagless && oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile &&
                oSegment.IntendedUse != mpSegmentIntendedUses.AsMasterDataForm)
            {
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    //Verify valid location for mVar tag
                    Word.XMLNode oWordTag = oSegment.PrimaryWordTag;
                    iValid = LMP.Forte.MSWord.WordDoc.ValidateTagInsertion(oRange, oWordTag, LMP.Forte.MSWord.TagTypes.Variable);
                }
                else
                {
                    //Verify valid location for mVar cc
                    Word.ContentControl oWordCC = oSegment.PrimaryContentControl;
                    iValid = LMP.Forte.MSWord.WordDoc.ValidateContentControlInsertion(oRange, oWordCC, LMP.Forte.MSWord.TagTypes.Variable);
                }
            }
            if (iValid != LMP.Forte.MSWord.TagInsertionValidityStates.Valid)
            {
                //location is invalid - alert
                string xMsg = m_oTaskPane.DocDesigner.GetInsertionInvalidityMessage(iValid,
                    ForteDocument.TagTypes.Variable);

                MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }
            string xVarName = oLibraryVariable.Name;
            string xVarDisplay = oLibraryVariable.DisplayName;
            Variable oVarTest = null;
            try
            {
                oVarTest = oSegment.Variables.ItemFromName(xVarName);
            }
            catch { }

            while (oVarTest != null)
            {
                //Duplicate Variable name, prompt for new value
                BasicNameForm oForm = new BasicNameForm(oSegment);
                oForm.ItemDisplayName = xVarDisplay;
                oForm.Text = oForm.Text + "Variable";
                oForm.TargetObjectType = BasicNameForm.TargetObjectTypes.Variable;

                DialogResult oResult = oForm.ShowDialog();
                if (oResult == DialogResult.OK)
                    xVarDisplay = oForm.ItemDisplayName;
                else
                    xVarDisplay = null;
                oForm.Close();
                if (string.IsNullOrEmpty(xVarDisplay))
                    return;
                else
                    xVarName = String.RemoveIllegalNameChars(xVarDisplay);
                oVarTest = null;
                try
                {
                    //Test new name
                    oVarTest = oSegment.Variables.ItemFromName(xVarName);
                }
                catch { }
            }
            Variable oVarNew = oSegment.Variables.GetVariableFromDefinition(oLibraryVariable.ObjectData);
            oVarNew.Name = xVarName;
            oVarNew.DisplayName = xVarDisplay;
            //Assign new GUID to avoid conflicts
            oVarNew.ID = System.Guid.NewGuid().ToString();

            //remove TagExpandedValue field code from value source expression
            if (oVarNew.ValueSourceExpression.IndexOf("[TagExpandedValue") > -1)
                oVarNew.ValueSourceExpression = "";

            //Add at end of existing variable
            oVarNew.ExecutionIndex = oSegment.Variables.Count + 1;
            //GLOG 8560
            if (oSegment.IntendedUse == mpSegmentIntendedUses.AsAnswerFile || oSegment.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
            {
                //Properties not relevant for Answer File
                oVarNew.ValueSourceExpression = "";
                oVarNew.IsMultiValue = false;
                oVarNew.AllowPrefillOverride = Variable.PrefillOverrideTypes.Never;
                oVarNew.DisplayIn = Variable.ControlHosts.DocumentEditor;
                oVarNew.DisplayLevel = Variable.Levels.Basic;
                oVarNew.DisplayValue = "";
                oVarNew.AssociatedPrefillNames = "";
                oVarNew.MustVisit = false;
                //GLOG 8560: MasterData doesn't allow Author Preferences
                if (oVarNew.DefaultValue.ToUpper().Contains("AUTHORPREFERENCE_"))
                {
                    oVarNew.DefaultValue = "";
                }
            }
            //Only tagless variables in Data Interview
            bool bInsertTagless = oLibraryVariable.IsTagless || 
                oSegment.IntendedUse == mpSegmentIntendedUses.AsAnswerFile ||
                oSegment.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm;

            if (bInsertTagless)
            {
                //tagless variable storage will be attached to Segment XML
                this.TaskPane.DocDesigner.StoreTaglessVariable(oSegment, ref oVarNew);
                oSegment.RefreshNodes();
            }
            else
            {
                this.TaskPane.ScreenUpdating = false;
                this.TaskPane.DocDesigner.ProgrammaticallySelectingWordTag = true;
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
                try
                {
                    //10.2
                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                        oVarNew.InsertAssociatedContentControl();
                    else
                        oVarNew.InsertAssociatedTag();
                    m_oMPDocument.WordDocument.Activate();
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
                    this.TaskPane.DocDesigner.ProgrammaticallySelectingWordTag = false;
                    this.TaskPane.ScreenUpdating = true;
                }
            }
            oSegment.Variables.Save(oVarNew);

            //if we have an associated variable, set XMLNode text = variable name
            if (!bInsertTagless)
            {
                if (oVarNew.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    oVarNew.AssociatedWordTags[0].Range.Text = oVarNew.DisplayName;
                }
                else
                {
                    oVarNew.AssociatedContentControls[0].Range.Text = oVarNew.DisplayName;
                }
            }
            else
            {
                //Provide feedback
                MessageBox.Show("Imported Tagless Variable", LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            m_oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes);
        }
        /// <summary>
        /// Creates new Variable using selected Variable node as model
        /// </summary>
        /// <param name="oNode"></param>
        private void ImportBlock(LibraryBlock oLibraryBlock)
        {
            Word.Range oRange = m_oMPDocument.WordDocument.Application.Selection.Range;
            Segment oSegment = m_oMPDocument.GetTopLevelSegmentFromSelection();

            LMP.Forte.MSWord.TagInsertionValidityStates iValid = LMP.Forte.MSWord.TagInsertionValidityStates.Valid;
            if (oSegment == null)
                iValid = LMP.Forte.MSWord.TagInsertionValidityStates.InvalidNoContainingSegmentExists;
            else if (oSegment.IntendedUse != mpSegmentIntendedUses.AsAnswerFile)
            {
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                {
                    //Verify valid location for mBlock tag
                    Word.XMLNode oWordTag = oSegment.PrimaryWordTag;
                    iValid = LMP.Forte.MSWord.WordDoc.ValidateTagInsertion(oRange, oWordTag, LMP.Forte.MSWord.TagTypes.Block);
                }
                else
                {
                    //Verify valid location for mBlock tag
                    Word.ContentControl oWordCC = oSegment.PrimaryContentControl;
                    iValid = LMP.Forte.MSWord.WordDoc.ValidateContentControlInsertion(oRange, oWordCC, LMP.Forte.MSWord.TagTypes.Block);
                }
            }
            if (iValid != LMP.Forte.MSWord.TagInsertionValidityStates.Valid)
            {
                //location is invalid - alert
                string xMsg = m_oTaskPane.DocDesigner.GetInsertionInvalidityMessage(iValid,
                    ForteDocument.TagTypes.Block);

                MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }
            string xBlockName = oLibraryBlock.Name;
            string xBlockDisplay = oLibraryBlock.DisplayName;

            //test if block already exists
            Block oBlockTest = null;
            try
            {
                oBlockTest = oSegment.Blocks.ItemFromName(xBlockName);
            }
            catch { }

            while (oBlockTest != null)
            {
                //Duplicate block name, prompt for new value
                BasicNameForm oForm = new BasicNameForm(oSegment);
                oForm.ItemDisplayName = xBlockDisplay;
                oForm.Text = oForm.Text + "Block";
                oForm.TargetObjectType = BasicNameForm.TargetObjectTypes.Block;

                DialogResult oResult = oForm.ShowDialog();
                if (oResult == DialogResult.OK)
                    xBlockDisplay = oForm.ItemDisplayName;
                else
                    xBlockDisplay = null;
                oForm.Close();
                if (string.IsNullOrEmpty(xBlockDisplay))
                    return;
                else
                    xBlockName = String.RemoveIllegalNameChars(xBlockDisplay);
                oBlockTest = null;
                try
                {
                    //Test new name
                    oBlockTest = oSegment.Blocks.ItemFromName(xBlockName);
                }
                catch { }
            }
            Block oNewBlock = oSegment.Blocks.GetBlockFromDefinition(oLibraryBlock.ObjectData);
            oNewBlock.Name = xBlockName;
            oNewBlock.DisplayName = xBlockDisplay;

            this.TaskPane.ScreenUpdating = false;
            this.TaskPane.DocDesigner.ProgrammaticallySelectingWordTag = true;
            ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.All;
            try
            {
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                {
                    Word.ContentControl oCC = oNewBlock.InsertAssociatedContentControl(oNewBlock.TagID, oLibraryBlock.ObjectData);
                    object oMissing = System.Reflection.Missing.Value;
                    string xXML = LMP.String.GetMinimalOpeningXML(true) + "<w:body>" + oLibraryBlock.XML + "</w:body>" + LMP.String.GetMinimalClosingXML(true);
                    oCC.Range.InsertXML(xXML, ref oMissing);
                }
                else
                {
                    Word.XMLNode oTag = oNewBlock.InsertAssociatedTag(oNewBlock.TagID, oLibraryBlock.ObjectData);
                    object oMissing = System.Reflection.Missing.Value;
                    oTag.Range.InsertXML(oLibraryBlock.XML, ref oMissing);
                }
                m_oMPDocument.WordDocument.Activate();
            }
            finally
            {
                ForteDocument.IgnoreWordXMLEvents = LMP.Architect.Api.ForteDocument.WordXMLEvents.None;
                this.TaskPane.DocDesigner.ProgrammaticallySelectingWordTag = false;
                this.TaskPane.ScreenUpdating = true;
            }

            oSegment.Blocks.Save(oNewBlock);

            m_oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes);
        }
        #endregion
        #region *********************helper classes*********************
        /// <summary>
        /// Simple class to hold Variable definition values
        /// </summary>
        private class LibraryElement
        {
            public string ObjectData = "";
        }
        private class LibraryVariable:LibraryElement
        {
            public bool IsTagless = false;
            public string Name = "";
            public string DisplayName = "";
            public int ExecutionIndex = 0;
        }
        private class LibraryBlock : LibraryElement
        {
            public string Name = "";
            public string DisplayName = "";
            public string XML = "";
        }
        private class LibraryProperties : LibraryElement
        {
        }
        private class LibraryAuthors : LibraryElement
        {
        }
        private class LibrarySegmentActions : LibraryElement
        {
        }
        private class VariableSorter : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                LibraryVariable oVar1 = (LibraryVariable)x;
                LibraryVariable oVar2 = (LibraryVariable)y;

                int iIndex1 = oVar1.ExecutionIndex;
                int iIndex2 = oVar2.ExecutionIndex;

                if (iIndex1 == iIndex2)
                    return 0;
                else if (iIndex1 > iIndex2)
                    return 1;
                else
                    return -1;
            }
            #endregion
        }
        private class BlockSorter : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                LibraryBlock oBlock1 = (LibraryBlock)x;
                LibraryBlock oBlock2 = (LibraryBlock)y;

                Comparer o = new Comparer(System.Threading.Thread.CurrentThread.CurrentCulture);
                return o.Compare(oBlock1.DisplayName, oBlock2.DisplayName);
            }
            #endregion
        }
        private void cmbCategory_ValueChanged(object sender, EventArgs e)
        {
            ShowElements();
        }

        private void treeImport_KeyDown(object sender, KeyEventArgs e)
        {
            HandleKeyDown(e);
        }

        private void treeSegments_KeyDown(object sender, KeyEventArgs e)
        {
            HandleKeyDown(e);
        }

        private void VariableImportManager_KeyDown(object sender, KeyEventArgs e)
        {
            HandleKeyDown(e);
        }

        private void HandleKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    if (e.Control)
                    {
                        // Toggle to the next task pane tab.
                        this.TaskPane.Toggle();
                    }
                    break;
            }
        }

        private void btnImport_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F3)
            {
                this.TaskPane.Toggle();
            }
        }

        private void cmbCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F3)
            {
                this.TaskPane.Toggle();
            }
        }
        #endregion
    }
}
