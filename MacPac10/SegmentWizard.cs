using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Architect.Api;
using LMP.Controls;

namespace LMP.MacPac
{
    public partial class SegmentWizard : Form
    {

        #region *********************fields*********************
        Segment m_oSegment = null;
        List<Variable> m_aVars = null;
        int m_iCurVariable = -1;
        private Control m_oCurCtl = null;
        private Variable m_oCurVar = null;
        #endregion
        #region *********************constructors*********************
        public SegmentWizard(Segment oSegment)
        {
            InitializeComponent();
            m_oSegment = oSegment;
        }
        #endregion
        #region *********************methods*********************
        public new DialogResult ShowDialog()
        {
            if (m_oSegment == null || !m_oSegment.DisplayWizard)
                // Not appropriate for this segment
                return DialogResult.Abort;

            // Get list of Wizard-enabled variables for segment and children
            m_aVars = m_oSegment.GetWizardVariables(true);
            if (m_aVars.Count == 0)
                // No Wizard variables configured
                return DialogResult.Abort;
            else
                return base.ShowDialog();
        }
        /// <summary>
        /// Load next variable in list
        /// </summary>
        private void DisplayNext()
        {
            if (m_iCurVariable < m_aVars.Count - 1)
            {
                m_iCurVariable = m_iCurVariable + 1;
                if (m_iCurVariable >= m_aVars.Count - 1)
                {
                    //Only enable Finish after all variables have been visited
                    btnFinish.Enabled = true;
                }
                ShowVariable(m_iCurVariable);
            }
        }
        /// <summary>
        /// Load previous variable in list
        /// </summary>
        private void DisplayPrevious()
        {
            if (m_iCurVariable > 0)
            {
                m_iCurVariable = m_iCurVariable - 1;
                ShowVariable(m_iCurVariable);
            }
        }
        /// <summary>
        /// Set value of current variable from control
        /// </summary>
        private void UpdateCurrentVariable()
        {
            if (m_oCurVar != null && m_oCurCtl != null)
            {
                IControl oCtl = (IControl)m_oCurCtl;
                if (oCtl.IsDirty)
                {
                    m_oCurVar.SetValue(oCtl.Value, true);
                    Session.CurrentWordApp.ScreenUpdating = true;
                    Session.CurrentWordApp.ScreenRefresh();
                }
            }
        }
        /// <summary>
        /// Show control for specified Variable
        /// </summary>
        /// <param name="iItem"></param>
        private void ShowVariable(int iItem)
        {
            Control oICtl = null;
            //Save changes for current variable if necessary
            UpdateCurrentVariable();
            m_oCurVar = m_aVars[iItem];
            if (m_oCurVar.AssociatedControl == null)
            {
                DateTime t0 = DateTime.Now;

                oICtl = (Control)m_oCurVar.CreateAssociatedControl(this.pnlControl);

                //size control to panel
                oICtl.SetBounds(12, 25, pnlControl.Width - 24, oICtl.Height);
                ((IControl)oICtl).Value = m_oCurVar.Value;
                if (oICtl is ICIable)
                    ((ICIable)oICtl).CIRequested += new CIRequestedHandler(GetContacts);
                this.pnlControl.Controls.Add(oICtl);

                LMP.Benchmarks.Print(t0, oICtl.GetType().Name);
            }
            else
            {
                //get control
                oICtl = (Control)m_oCurVar.AssociatedControl;

                //refresh properties of associated control -
                //necessary because some control properties
                //have MacPac expressions as their value -
                //these need to get evaluated
                m_oCurVar.SetAssociatedControlProperties();
            }

            if (m_oCurCtl != null && m_oCurCtl != oICtl)
                m_oCurCtl.Visible = false;

            //store as current control
            this.m_oCurCtl = (Control)oICtl;
            m_oCurCtl.Visible = true;
            lblDisplayName.Text = m_oCurVar.DisplayName + ":";

            if (m_oCurVar.HelpText != "")
            {
                //set description - web browser control must
                //be set so that AllowNavigation = true -
                //otherwise, repeated DocumentText assignments fail
                string xDesc = m_oCurVar.HelpText.Replace((char)172, '<');
                xDesc = String.RestoreMPReservedChars(xDesc);
                xDesc = "<span style='font-size: 8pt; font-family: MS Sans Serif'>" +
                        xDesc + "</span>";

                this.wbHelpText.DocumentText = xDesc;
            }
            else
                this.wbHelpText.DocumentText = "";

            //Mark variable as visited
            m_oCurVar.MustVisit = false;
            m_oCurCtl.Focus();
        }
        private void GetContacts(object sender, EventArgs e)
        {
            try
            {
                //get contact detail for active segment
                Segment oSeg = m_oCurVar.Segment;

                if (m_oCurCtl is ICIable)
                {
                    // Update current variable's value from control
                    if (((IControl)m_oCurCtl).IsDirty)
                        m_oCurVar.SetValue(((IControl)m_oCurCtl).Value);
                }

                oSeg.GetContacts();
                foreach (Variable oVar in m_aVars)
                {
                    // Update control value if it's already been created
                    if (oVar.AssociatedControl != null && oVar.AssociatedControl is ICIable)
                          oVar.AssociatedControl.Value = oVar.Value;
                }

            }
            finally
            {
                Session.CurrentWordApp.ScreenUpdating = true;
                Session.CurrentWordApp.ScreenRefresh();
            }
        }
        #endregion
        #region *********************event handlers*********************
        private void SegmentWizard_Load(object sender, EventArgs e)
        {
            this.btnCancel.Text = LMP.Resources.GetLangString("Lbl_Cancel");
            this.btnFinish.Text = LMP.Resources.GetLangString("Lbl_Finish");
            this.btnNext.Text = LMP.Resources.GetLangString("Lbl_Next");
            this.btnPrevious.Text = LMP.Resources.GetLangString("Lbl_Previous");
            this.Text = m_oSegment.DisplayName + " Wizard";
            this.btnFinish.Enabled = false;
            Session.CurrentWordApp.ScreenUpdating = true;
            DisplayNext();
        }
        /// <summary>
        /// Clicked on Next
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayNext();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Clicked on Previous
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayPrevious();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Clicked on Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        /// <summary>
        /// Clicked on Finish
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFinish_Click(object sender, EventArgs e)
        {
            UpdateCurrentVariable();
            DialogResult = DialogResult.OK;
        }
        #endregion

        private void splitContainer1_Paint(object sender, PaintEventArgs e)
        {
            Pen oPen = new Pen(Brushes.Turquoise);
            Rectangle oR = this.splitContainer1.ClientRectangle;
            oR.Inflate(-1,-1);
            e.Graphics.DrawRectangle(oPen, oR);
            oPen = new Pen(Brushes.Turquoise, 4);
            e.Graphics.DrawLine(oPen, oR.Left, this.splitContainer1.Panel2.Top, 
                oR.Left + oR.Width, this.splitContainer1.Panel2.Top);
        }

    }
}