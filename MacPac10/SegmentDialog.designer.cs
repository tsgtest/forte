namespace LMP.MacPac
{
    partial class SegmentDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.btnFinish = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.tabPages = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.pnlHelp = new System.Windows.Forms.Panel();
            this.wbHelpText = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel1.SuspendLayout();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            this.pnlControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPages)).BeginInit();
            this.tabPages.SuspendLayout();
            this.pnlHelp.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Location = new System.Drawing.Point(0, 22);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(392, 370);
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Location = new System.Drawing.Point(242, 5);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(73, 27);
            this.btnFinish.TabIndex = 1;
            this.btnFinish.Text = "&Finish";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            this.btnFinish.Enter += new System.EventHandler(this.HandleFormButton_Enter);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(318, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(73, 27);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancel.Enter += new System.EventHandler(this.HandleFormButton_Enter);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(9, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 27);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Sa&ve Data...";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.Enter += new System.EventHandler(this.HandleFormButton_Enter);
            // 
            // scMain
            // 
            this.scMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scMain.BackColor = System.Drawing.Color.Transparent;
            this.scMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.scMain.IsSplitterFixed = true;
            this.scMain.Location = new System.Drawing.Point(1, 0);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            this.scMain.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.scMain.Panel1.Controls.Add(this.pnlControls);
            this.scMain.Panel1.Controls.Add(this.tabPages);
            this.scMain.Panel1.Padding = new System.Windows.Forms.Padding(1);
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.Controls.Add(this.pnlHelp);
            this.scMain.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.scMain.Panel2Collapsed = true;
            this.scMain.Size = new System.Drawing.Size(395, 438);
            this.scMain.SplitterDistance = 345;
            this.scMain.SplitterWidth = 1;
            this.scMain.TabIndex = 4;
            this.scMain.TabStop = false;
            this.scMain.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Paint);
            // 
            // pnlControls
            // 
            this.pnlControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlControls.BackColor = System.Drawing.Color.Transparent;
            this.pnlControls.Controls.Add(this.btnSave);
            this.pnlControls.Controls.Add(this.btnFinish);
            this.pnlControls.Controls.Add(this.btnCancel);
            this.pnlControls.Location = new System.Drawing.Point(-1, 400);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(398, 33);
            this.pnlControls.TabIndex = 3;
            // 
            // tabPages
            // 
            appearance1.BorderColor = System.Drawing.Color.DarkGray;
            appearance1.FontData.BoldAsString = "True";
            appearance1.ForeColor = System.Drawing.Color.Black;
            this.tabPages.ActiveTabAppearance = appearance1;
            this.tabPages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.BackColor2 = System.Drawing.Color.Transparent;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.BackwardDiagonal;
            appearance2.BorderColor3DBase = System.Drawing.Color.White;
            appearance2.FontData.Name = "Microsoft Sans Serif";
            appearance2.ForeColor = System.Drawing.Color.DimGray;
            this.tabPages.Appearance = appearance2;
            this.tabPages.Controls.Add(this.ultraTabSharedControlsPage1);
            this.tabPages.Controls.Add(this.ultraTabPageControl1);
            this.tabPages.Location = new System.Drawing.Point(1, 1);
            this.tabPages.Name = "tabPages";
            this.tabPages.ScrollArrowStyle = Infragistics.Win.UltraWinTabs.ScrollArrowStyle.VisualStudio;
            this.tabPages.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.tabPages.ShowButtonSeparators = true;
            this.tabPages.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
            this.tabPages.Size = new System.Drawing.Size(392, 392);
            this.tabPages.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.StateButtons;
            this.tabPages.TabButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button;
            this.tabPages.TabIndex = 0;
            appearance3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            appearance3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            appearance3.ForeColor = System.Drawing.Color.Black;
            this.tabPages.TabListButtonAppearance = appearance3;
            this.tabPages.TabPadding = new System.Drawing.Size(2, 2);
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Page &1";
            this.tabPages.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1});
            this.tabPages.TabsPerRow = 8;
            this.tabPages.TabStop = false;
            this.tabPages.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.tabPages_SelectedTabChanged);
            this.tabPages.Paint += new System.Windows.Forms.PaintEventHandler(this.tabPages_Paint);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(392, 370);
            // 
            // pnlHelp
            // 
            this.pnlHelp.BackColor = System.Drawing.Color.Transparent;
            this.pnlHelp.Controls.Add(this.wbHelpText);
            this.pnlHelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHelp.Location = new System.Drawing.Point(3, 3);
            this.pnlHelp.Name = "pnlHelp";
            this.pnlHelp.Padding = new System.Windows.Forms.Padding(1);
            this.pnlHelp.Size = new System.Drawing.Size(90, 94);
            this.pnlHelp.TabIndex = 0;
            // 
            // wbHelpText
            // 
            this.wbHelpText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbHelpText.Location = new System.Drawing.Point(1, 1);
            this.wbHelpText.MinimumSize = new System.Drawing.Size(20, 23);
            this.wbHelpText.Name = "wbHelpText";
            this.wbHelpText.Size = new System.Drawing.Size(88, 92);
            this.wbHelpText.TabIndex = 0;
            this.wbHelpText.TabStop = false;
            // 
            // SegmentDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(396, 438);
            this.Controls.Add(this.scMain);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SegmentDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SegmentDialog";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.SegmentDialog_HelpButtonClicked);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SegmentDialog_KeyUp);
            this.scMain.Panel1.ResumeLayout(false);
            this.scMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            this.pnlControls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPages)).EndInit();
            this.tabPages.ResumeLayout(false);
            this.pnlHelp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFinish;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.SplitContainer scMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl tabPages;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.Panel pnlHelp;
        private System.Windows.Forms.WebBrowser wbHelpText;
        private System.Windows.Forms.Panel pnlControls;

    }
}