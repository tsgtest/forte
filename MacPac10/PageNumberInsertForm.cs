using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using System.Collections;
using Word = Microsoft.Office.Interop.Word;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class PageNumberInsertForm : Form
    {

        #region ****************fields********************
        //introduce a data table for drop down values
        DataTable m_dtValues = null;

        //todo use enums - check values
        //Fixed Alignment values
        string[,] m_oAlignments = new string[,] {   {"Left", "1"},
                                                    {"Center", "2"},
                                                    {"Right", "3"}};

        //Fixed Location values
        string[,] m_oLocations = new string[,] {   {"Header", "1"},
                                                   {"Footer", "2"},
                                                   {"Insertion Point", "3"}};

        //Fixed Scope values
        string[,] m_oScopes = new string[,] {   {"Current Section", "0"},
                                                {"Entire Document", "1"}};

        //Fixed Page Number Format values
        //todo should be value set/application settings?
        string[,] m_oPageNumberFormat = new string[,] {   {LMP.Resources.GetLangString("PageNumberFormatArabic"), "1"},
                                                          {LMP.Resources.GetLangString("PageNumberFormatUppercaseRoman"), "2"},
                                                          {LMP.Resources.GetLangString("PageNumberFormatLowercaseRoman"), "3"},
                                                          {LMP.Resources.GetLangString("PageNumberFormatUppercaseLetter"), "4"},
                                                          {LMP.Resources.GetLangString("PageNumberFormatLowercaseLetter"), "5"},
                                                          {LMP.Resources.GetLangString("PageNumberFormatCardText"), "6"}};

        #endregion
        #region ****************constructors********************
        public PageNumberInsertForm()
        {
            InitializeComponent();

            //GLOG #8492
            this.txtTextAfter.Height = this.lblLocation.Top - this.txtTextAfter.Top - 5;
            this.txtTextBefore.Height = this.txtTextAfter.Height;

            InitializeApplicationContent();
        }
        #endregion
        #region *********************events*********************

        private void btnInsert_Click(object sender, EventArgs e)
        {
            //GLOG : 6411 : CEH - Enter event isn't reached if user finishes
            //by pressing Enter - validate here as well
            if (this.rbMultiple.Checked)
            {
                if (!ValidateMultipleSectionsControl(true))
                {
                    this.DialogResult = DialogResult.None;
                    return;
                }
            }
            
            // Close the dialog
            this.Close();
        }

        private void btnInsert_Enter(object sender, EventArgs e)
        {
            if (this.rbMultiple.Checked)
                ValidateMultipleSectionsControl(true);
        }

        /// <summary>
        /// Validates txtMultiple control contents.
        /// </summary>
        private bool ValidateMultipleSectionsControl(bool bCheckForEmpty)
        {
            int iSections = Session.CurrentWordApp.ActiveDocument.Sections.Count;

            //GLOG 6107 (dm) - strip spaces
            string xMultiple = this.txtMultiple.Text.Replace(" ", "");

            //GLOG 6154 (dm) - prompt if no section specified
            if (xMultiple == "" && bCheckForEmpty == true)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_NoSectionSpecified"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                this.txtMultiple.SelectAll();
                this.txtMultiple.Focus();
                return false;
            }

            string[] xSectionList = xMultiple.Split(',');
            foreach (string xItem in xSectionList)
            {
                if (xItem.Contains("-"))
                {
                    int iPos = xItem.IndexOf("-");
                    string xStart = xItem.Substring(0, iPos);
                    if (iPos < xItem.Length)
                    {
                        string xEnd = xItem.Substring(iPos + 1);
                        if (String.IsNumericInt32(xStart) && String.IsNumericInt32(xEnd))
                        {
                            for (int i = Int32.Parse(xStart); i <= Int32.Parse(xEnd); i++)
                            {
                                //GLOG 6078 (dm) - don't accept "0"
                                if ((i > iSections) || (xStart == "0"))
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString(
                                        "Prompt_InvalidSectionSpecified") + i.ToString(),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    this.txtMultiple.SelectAll();
                                    this.txtMultiple.Focus();
                                    return false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (String.IsNumericInt32(xItem))
                    {
                        //GLOG 6078 (dm) - don't accept "0"
                        if ((Int32.Parse(xItem) > iSections) || (xItem == "0"))
                        {
                            MessageBox.Show(LMP.Resources.GetLangString(
                                "Prompt_InvalidSectionSpecified") + xItem,
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            this.txtMultiple.SelectAll();
                            this.txtMultiple.Focus();
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void rbMultiple_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.txtMultiple.Enabled = this.rbMultiple.Checked;
                if (this.rbMultiple.Checked == true)
                {
                    this.txtMultiple.Focus();
                }
            }
            catch { }
        }

        private void txtMultiple_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Ignore all input except numeric, '-' or ','
            //GLOG 6107 (dm) - allow spaces
            if (!((e.KeyChar >= 48 && e.KeyChar <= 57) || (char)e.KeyChar == '-' ||
                (char)e.KeyChar == ',' || (char)e.KeyChar == (char)Keys.Back ||
                e.KeyChar == 32))
            {
                e.Handled = true;
            }
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                // Do nothing and close the dialog
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //private void cmbLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    bool bNotAtCursor = (this.cmbLocation.SelectedValue.ToString() != "3");

        //    //enable or disable controls
        //    this.cmbAlignment.Enabled = bNotAtCursor;
        //    this.chkInsertOnFirstSectionPage.Enabled = bNotAtCursor;
        //    this.gbApplyTo.Enabled = bNotAtCursor;

        //    if (!bNotAtCursor)
        //    {
        //        this.cmbAlignment.SelectedIndex = -1;
        //        this.chkInsertOnFirstSectionPage.CheckState = CheckState.Unchecked;
        //        this.rbDocument.Checked = true;
        //    }
        //    else
        //    {
        //        if (this.cmbAlignment.SelectedIndex < 0)
        //        {
        //            this.cmbAlignment.SelectedIndex = 0;
        //        }
        //    }
        //}
        //GLOG : 8436 : JSW
        private void cmbLocation_ValueChanged(object sender, EventArgs e)
        {
            
            bool bNotAtCursor;

            if (this.cmbLocation.SelectedValue == null)
                bNotAtCursor = true;
            else
                bNotAtCursor = (this.cmbLocation.SelectedValue.ToString() != "3");

            //enable or disable controls
            this.cmbAlignment.Enabled = bNotAtCursor;
            this.chkInsertOnFirstSectionPage.Enabled = bNotAtCursor;
            this.gbApplyTo.Enabled = bNotAtCursor;

            if (!bNotAtCursor)
            {
                this.cmbAlignment.SelectedIndex = -1;
                this.chkInsertOnFirstSectionPage.CheckState = CheckState.Unchecked;
                this.rbDocument.Checked = true;
            }
            else
            {
                if (this.cmbAlignment.SelectedIndex < 0)
                {
                    this.cmbAlignment.SelectedIndex = 0;
                }
            }
        }
        #endregion
        #region *********************methods*********************
        private void InitializeApplicationContent()
        {
            // Set the data source for form drop downs
            //GLOG : 8436 : JSW
            //this.cmbAlignment.DataSource = GetDataTable(m_oAlignments);
            //this.cmbAlignment.DisplayMember = "Name";
            //this.cmbAlignment.ValueMember = "Value";
            this.cmbAlignment.SetList(m_oAlignments);

            //this.cmbLocation.DataSource = GetDataTable(m_oLocations);
            this.cmbLocation.SetList(m_oLocations);
            //this.cmbLocation.DisplayMember = "Name";
            //this.cmbLocation.ValueMember = "Value";

            //this.cmbFormat.DataSource = GetDataTable(m_oPageNumberFormat);
            this.cmbFormat.SetList(m_oPageNumberFormat);
            //this.cmbFormat.DisplayMember = "Name";
            //this.cmbFormat.ValueMember = "Value";

			//GLOG : 6435 : ceh
            //load Total Page Number Formats
            List oList = null;
            try
            {
                oList = (List)LMP.Data.Application.GetLists(false)
                            .ItemFromName("TotalPageNumberFormats");
            }
            catch {}

            if (oList != null)
                this.cmbIncludeTotalPages.SetList(oList);
            else
            //disable control if list doesn't exist
            {
                this.cmbIncludeTotalPages.Enabled = false;
                this.lblIncludeTotalPages.Enabled = false;
            }

            //Get User Settings
            UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

            this.txtTextBefore.Text = oSettings.PageNumberTextBefore;
            this.txtTextAfter.Text = oSettings.PageNumberTextAfter;
            this.chkInsertOnFirstSectionPage.Checked = oSettings.PageNumberOnFirstPage;
            this.cmbIncludeTotalPages.Value = oSettings.PageNumberTotalFormat;  //GLOG : 6435 : ceh

            if (oSettings.PageNumberScope == PageNumberScopes.Section)
                this.rbSection.Checked = true;
            else
                this.rbDocument.Checked=true;

            this.cmbAlignment.SelectedValue = oSettings.PageNumberAlignment.ToString();

            //default to "Footer" if no preference specified
            string xLocation = oSettings.PageNumberLocation.ToString();
            if(string.IsNullOrEmpty(xLocation))
                this.cmbLocation.SelectedIndex = 1;
            else
                this.cmbLocation.SelectedValue = oSettings.PageNumberLocation.ToString();

            //GLOG : 6025 : CEH
            this.cmbFormat.SelectedValue = oSettings.PageNumberFormat.ToString();
            this.lstStartAtNumber.Value = decimal.Parse(oSettings.PageNumberStartAtNumber.ToString());
            if (oSettings.PageNumberRestartNumbering)
                this.optStartAtNumber.Checked = true;
            else
                this.optContinueFromPrevious.Checked = true;

            //Localize the form.
            LocalizeForm();

            //Get Section for current selection
            Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();
            //GLOG : 7726 : ceh - selection.information accurately returns section index when stories are linked
            string xSectionIndex = oDoc.Application.Selection.Information[Word.WdInformation.wdActiveEndSectionNumber].ToString();
            rbSection.Text = rbSection.Text + " (Section " + xSectionIndex + ")";
        }


        // Create data source for form drop downs
        private DataTable GetDataTable(string[,] oValues)
        {
            m_dtValues = new DataTable();

            DataColumn oNameCol = new DataColumn("Name", typeof(string));
            m_dtValues.Columns.Add(oNameCol);

            DataColumn oValueCol = new DataColumn("Value", typeof(string));
            m_dtValues.Columns.Add(oValueCol);

            // Populate the table with the values.
            for (int i = 0; i < oValues.GetLength(0); i++)
            {
                this.m_dtValues.Rows.Add(oValues[i, 0], oValues[i, 1]);
            }
            return m_dtValues;
        }

        private void LocalizeForm()
        {
            this.Text = LMP.Resources.GetLangString("Dialog_PageNumberInsert_TitleBar");
            this.lblFormat.Text = LMP.Resources.GetLangString("PageNumberFormFormat");
            this.lblTextBefore.Text = LMP.Resources.GetLangString("PageNumberFormTextBefore");
            this.lblTextAfter.Text = LMP.Resources.GetLangString("PageNumberFormTextAfter");
            this.lblLocation.Text = LMP.Resources.GetLangString("PageNumberFormLocation");
            this.lblAlignment.Text = LMP.Resources.GetLangString("PageNumberFormAlignment");
            this.gbApplyTo.Text = LMP.Resources.GetLangString("PageNumberFormApplyTo");
            this.gbOptions.Text = LMP.Resources.GetLangString("PageNumberFormOptions");
            this.chkInsertOnFirstSectionPage.Text = LMP.Resources.GetLangString("PageNumberFormInsertOnFirstSectionPage");
            this.btnInsert.Text = LMP.Resources.GetLangString("PageNumberFormInsert");
            this.btnCancel.Text = LMP.Resources.GetLangString("PageNumberFormCancel");
            this.optStartAtNumber.Text = LMP.Resources.GetLangString("PageNumberFormStartAtOption");
            this.optContinueFromPrevious.Text = LMP.Resources.GetLangString("PageNumberFormContinueFromPreviousOption");
            this.lblIncludeTotalPages.Text = LMP.Resources.GetLangString("PageNumberIncludeTotalPages");
            this.rbDocument.Text = LMP.Resources.GetLangString("PageNumberFormApplyToDocumentOption");
            this.rbSection.Text = LMP.Resources.GetLangString("PageNumberFormApplyToSectionOption");
            this.rbMultiple.Text = LMP.Resources.GetLangString("PageNumberFormApplyToMultipleOption");
        }
        
        public void Finish()
        {

                //Save User Settings
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

                oSettings.PageNumberTextBefore = this.txtTextBefore.Text;
                oSettings.PageNumberTextAfter = this.txtTextAfter.Text;
                oSettings.PageNumberOnFirstPage = this.chkInsertOnFirstSectionPage.Checked;
                oSettings.PageNumberTotalFormat = this.cmbIncludeTotalPages.Value;
                if (this.rbSection.Checked)
                    oSettings.PageNumberScope = PageNumberScopes.Section;
                else
                    oSettings.PageNumberScope = PageNumberScopes.Document;

                //GLOG : 6407 : CEH
                if (this.cmbAlignment.SelectedIndex >= 0)
                    oSettings.PageNumberAlignment = Int32.Parse(this.cmbAlignment.SelectedValue.ToString());
                if (this.cmbFormat.SelectedIndex >= 0)
                    oSettings.PageNumberFormat = Int32.Parse(this.cmbFormat.SelectedValue.ToString());
                if (this.cmbLocation.SelectedIndex >= 0)
                    oSettings.PageNumberLocation = Int32.Parse(this.cmbLocation.SelectedValue.ToString());
                oSettings.PageNumberStartAtNumber = Int32.Parse(this.lstStartAtNumber.Value.ToString());
                oSettings.PageNumberRestartNumbering = this.optStartAtNumber.Checked;

                try
                {
                    TaskPane oTP = null;
                    ForteDocument oMPDocument = null;
                    Segment oExistingStamp = null;
                    Prefill oTrailerPrefill = null; //JTS 6/24/11
                    object[] aExistingTrailers = null; //GLOG 6540
                    string xTrailerStyle = "MacPac Trailer"; //GLOG 6540

                    Word.Document oDoc = LMP.Forte.MSWord.WordApp.ActiveDocument();

                    //get current task pane
                    try
                    {
                        oTP = TaskPanes.Item(oDoc);
                    }
                    catch { }

                    //get associated MacPac document
                    if (oTP != null)
                        oMPDocument = oTP.ForteDocument;
                    else
                    {
                        oMPDocument = new ForteDocument(oDoc);
                    }

                    if (oMPDocument.Segments.Count == 0)
                    {
                        //Need to refresh to be able to access Segment collection
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        if (oMPDocument.Mode == ForteDocument.Modes.Design)
                            oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes, false);
                        else
                            oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes, false);
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                        LMP.MacPac.Application.SetXMLTagStateForEditing();
                    }

                    for (int i = 0; i < oMPDocument.Segments.Count; i++)
                    {
                        Segment oTestSegment = null;
                        Segment oFirstTrailer = null;
                        //Look for document stamp of selected type
                        if (oMPDocument.Segments[i].TypeID == mpObjectTypes.Trailer)
                        {
                            oTestSegment = oMPDocument.Segments[i];
                            if (oFirstTrailer == null)
                                oFirstTrailer = oTestSegment;

                            //JTS 6/13/13: Need to use PrimaryRange instead of WordTags or ContentControls collections
                            if (oTestSegment.PrimaryRange.Sections[1].Index == 
                                                oMPDocument.WordDocument.Application.Selection.Range.Sections[1].Index)
                            {
                                oExistingStamp = oTestSegment;
                                break;
                            }
                        }
                        //Scope is entire document and no specific trailer was found for current section,
                        //use first 
                        if (oExistingStamp == null && (int)oSettings.PageNumberScope == 1 && oFirstTrailer != null)
                        {
                            oExistingStamp = oFirstTrailer;
                        }
                    }

                    object oTrailers = null;
                    if (oExistingStamp == null)
                    {
                        //GLOG 6540: New-style trailers aren't included in Segments collection
                        //Populate array with existing trailer types for all sections
                        LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                        if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains(
                            "CONVERTTRAILERSTO9XFORMAT") || oFirmSettings.UseMacPac9xStyleTrailer)
                        {
                            oTrailers = LMP.Forte.MSWord.WordDoc.GetConverted9xTrailers(oDoc, "zzmpTrailerItem");
                            xTrailerStyle = "zzmpTrailerItem";
                        }
                        else
                        {
                            oTrailers = LMP.Forte.MSWord.WordDoc.GetExistingTrailers(oDoc, "MacPac Trailer");
                            xTrailerStyle = "MacPac Trailer";
                        }
                        if (oTrailers != null)
                            aExistingTrailers = (object[])oTrailers;
                    }

                    //GLOG 5905 (dm) - since tags/cc may be deleted by this method,
                    //we need to disable xml events and refresh tags
                    ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
                    try
                    {
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                        LMP.Forte.MSWord.PageNumbering oPageNumber = new LMP.Forte.MSWord.PageNumbering();

                        //GLOG : 6262 : CEH
                        ArrayList aSections = new ArrayList();

                        if (this.rbSection.Checked)
                        {
                            //GLOG : 7726 : ceh - selection.information accurately returns section index when stories are linked
                            object oSection = oDoc.Application.Selection.Information[Word.WdInformation.wdActiveEndSectionNumber];
                            int iSecIndex = System.Convert.ToInt32(oSection.ToString());
                            aSections.Add(iSecIndex);                            
                        }
                        else if (this.rbMultiple.Checked)
                        {
                            string xMultiple = this.txtMultiple.Text.Replace(" ", "");
                            string[] xSectionList = xMultiple.Split(',');
                            foreach (string xItem in xSectionList)
                            {
                                if (xItem.Contains("-"))
                                {
                                    int iPos = xItem.IndexOf("-");
                                    string xStart = xItem.Substring(0, iPos);
                                    if (iPos < xItem.Length)
                                    {
                                        string xEnd = xItem.Substring(iPos + 1);
                                        if (String.IsNumericInt32(xStart) && String.IsNumericInt32(xEnd))
                                        {
                                            for (int i = Int32.Parse(xStart); i <= Int32.Parse(xEnd); i++)
                                            {
                                                //GLOG : 6406 : CEH
                                                //do not add duplicates
                                                if (!aSections.Contains(i))
                                                   aSections.Add(i); 
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    //GLOG : 6406 : CEH
                                    //do not add duplicates
                                    if (String.IsNumericInt32(xItem) && !aSections.Contains(Int32.Parse(xItem)))
                                        aSections.Add(Int32.Parse(xItem));
                                }
                            }
                        }
                        else
                        {
                            for (int i = 1; i <= LMP.Forte.MSWord.GlobalMethods.CurWordApp.ActiveDocument.Sections.Count; i++)
                            {
                                aSections.Add(i);
                            }
                        }
                        
                        object[] oSections = aSections.ToArray();

                        oPageNumber.Insert((short)oSettings.PageNumberLocation,
                                            oSections,
                                            oSettings.PageNumberOnFirstPage,
                                            oSettings.PageNumberTotalFormat,
                                            (short)oSettings.PageNumberAlignment,
                                            oSettings.PageNumberTextBefore,
                                            oSettings.PageNumberTextAfter,
                                            (short)oSettings.PageNumberFormat,
                                            oSettings.PageNumberRestartNumbering,
                                            (short)oSettings.PageNumberStartAtNumber);

                        if (oMPDocument.Mode == ForteDocument.Modes.Design)
                            oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithIntegerIndexes, false);
                        else
                            oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes, false);

                        //GLOG item #7554 - dcf
                        if (oTP != null)
                        {
                            oTP.DocEditor.UpdateSegmentImages();
                        }

                        //GLOG 6540: Only recreate trailers if Page Number inserted in Footer
                        if (oSettings.PageNumberLocation == 2)
                        {
                            //recreate Doc ID if necessary
                            if (oExistingStamp != null)
                            {
                                //JTS 6/24/11:  Convert Segment to Prefill
                                oTrailerPrefill = new Prefill(oExistingStamp);
                                LMP.MacPac.Application.RecreateTrailer(oTrailers, oTrailerPrefill, (int)oSettings.PageNumberScope);
                            }
                            else
                            {
                                //GLOG 6540: Update Trailers only in those sections that previously contain a footer-type trailer
                                //in which a page number was inserted
                                if (aExistingTrailers != null)
                                {
                                    int iStart = 0;
                                    int iEnd = 0;
                                    if (oSettings.PageNumberScope != PageNumberScopes.Section)
                                    {
                                        iStart = 1;
                                        iEnd = oMPDocument.WordDocument.Sections.Count;
                                    }
                                    else
                                    {
                                        iStart = Session.CurrentWordApp.Selection.Sections[1].Index;
                                        iEnd = iStart;
                                    }
                                    for (int i = iStart - 1; i < iEnd; i++)
                                    {
                                        //Only include sections updated with page number 
                                        if (!aSections.Contains(i + 1))
                                            continue;

                                        if (aExistingTrailers[i] != null)
                                        {
                                            string xTrailerID = aExistingTrailers[i].ToString();
                                            if (xTrailerID == "Linked" && (iStart == iEnd))
                                            {
                                                //Footer in current section is linked, get Segment ID from first previous unlinked section
                                                int s = i;
                                                while (xTrailerID == "Linked" && s > 0)
                                                {
                                                    s = s - 1;
                                                    xTrailerID = aExistingTrailers[s].ToString();
                                                    iStart = s;
                                                    iEnd = s;
                                                }
                                            }
                                            int iTrailerID = 0;
                                            if (Int32.TryParse(xTrailerID, out iTrailerID))
                                            {
                                                AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.Trailer);
                                                AdminSegmentDef oDef = null;
                                                try
                                                {
                                                    oDef = (AdminSegmentDef)oDefs.ItemFromID(iTrailerID);
                                                    //If Trailer inserted in this section is not Footer-type, skip section
                                                    if (!LMP.String.ContentContainsLinkedStories(oDef.XML, true, false, true, false, true, false))
                                                        continue;
                                                }
                                                catch 
                                                {
                                                    continue;
                                                }
                                                //JTS 6/24/11:  Check for Prefill doc var from previous trailer insertion
                                                oTrailerPrefill = DocumentStampUI.LastTrailerPrefill(iTrailerID);
                                                if (oTrailerPrefill != null)
                                                {
                                                    ArrayList aTrailerSections = new ArrayList() { i + 1 };
                                                    //Remove existing trailer first
                                                    LMP.Forte.MSWord.WordDoc.RemoveOrphanedTrailers(
                                                        Session.CurrentWordApp.ActiveDocument, xTrailerStyle, new object[] {i + 1});
                                                    //Insert trailer from prefill in current section
                                                    Segment oStamp = oMPDocument.InsertSegment(iTrailerID.ToString(), null, Segment.InsertionLocations.InsertAtStartOfCurrentSection,
                                                        Segment.InsertionBehaviors.Default, true, oTrailerPrefill, "", false, false, "", false, true, null, aTrailerSections);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    finally
                    {
                        ForteDocument.IgnoreWordXMLEvents = iEvents;
                    }
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.MethodExecutionException(
                        LMP.Resources.GetLangString("Error_CouldNotSuccessfullyInsertPageNumber"), oE);
                }
        }
        #endregion

    }
}