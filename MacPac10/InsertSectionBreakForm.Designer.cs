﻿namespace LMP.MacPac
{
    partial class InsertSectionBreakForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLocation = new System.Windows.Forms.Label();
            this.chkRestartNumbering = new System.Windows.Forms.CheckBox();
            this.gbSection1 = new System.Windows.Forms.GroupBox();
            this.cmbPageNumbers1 = new LMP.Controls.ComboBox();
            this.cmbHeaderFooter1 = new LMP.Controls.ComboBox();
            this.lblPageNumbers1 = new System.Windows.Forms.Label();
            this.lblHeaderFooter1 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbSection2 = new System.Windows.Forms.GroupBox();
            this.cmbPageNumbers2 = new LMP.Controls.ComboBox();
            this.cmbHeaderFooter2 = new LMP.Controls.ComboBox();
            this.lblPageNumbers2 = new System.Windows.Forms.Label();
            this.lblHeaderFooter2 = new System.Windows.Forms.Label();
            this.cmbLocation = new LMP.Controls.ComboBox();
            this.gbSection1.SuspendLayout();
            this.gbSection2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(9, 15);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(136, 13);
            this.lblLocation.TabIndex = 0;
            this.lblLocation.Text = "&Location for Section Break:";
            // 
            // chkRestartNumbering
            // 
            this.chkRestartNumbering.AutoSize = true;
            this.chkRestartNumbering.Location = new System.Drawing.Point(14, 272);
            this.chkRestartNumbering.Name = "chkRestartNumbering";
            this.chkRestartNumbering.Size = new System.Drawing.Size(142, 17);
            this.chkRestartNumbering.TabIndex = 2;
            this.chkRestartNumbering.Text = "&Restart Page Numbering";
            this.chkRestartNumbering.UseVisualStyleBackColor = true;
            // 
            // gbSection1
            // 
            this.gbSection1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSection1.Controls.Add(this.cmbPageNumbers1);
            this.gbSection1.Controls.Add(this.cmbHeaderFooter1);
            this.gbSection1.Controls.Add(this.lblPageNumbers1);
            this.gbSection1.Controls.Add(this.lblHeaderFooter1);
            this.gbSection1.Location = new System.Drawing.Point(12, 66);
            this.gbSection1.Name = "gbSection1";
            this.gbSection1.Size = new System.Drawing.Size(303, 90);
            this.gbSection1.TabIndex = 3;
            this.gbSection1.TabStop = false;
            this.gbSection1.Text = "Before Section Break";
            // 
            // cmbPageNumbers1
            // 
            this.cmbPageNumbers1.AllowEmptyValue = false;
            this.cmbPageNumbers1.Borderless = false;
            this.cmbPageNumbers1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbPageNumbers1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPageNumbers1.IsDirty = false;
            this.cmbPageNumbers1.LimitToList = true;
            this.cmbPageNumbers1.ListName = "";
            this.cmbPageNumbers1.Location = new System.Drawing.Point(104, 55);
            this.cmbPageNumbers1.MaxDropDownItems = 8;
            this.cmbPageNumbers1.Name = "cmbPageNumbers1";
            this.cmbPageNumbers1.SelectedIndex = -1;
            this.cmbPageNumbers1.SelectedValue = null;
            this.cmbPageNumbers1.SelectionLength = 0;
            this.cmbPageNumbers1.SelectionStart = 0;
            this.cmbPageNumbers1.Size = new System.Drawing.Size(191, 23);
            this.cmbPageNumbers1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbPageNumbers1.SupportingValues = "";
            this.cmbPageNumbers1.TabIndex = 3;
            this.cmbPageNumbers1.Tag2 = null;
            this.cmbPageNumbers1.Value = "";
            // 
            // cmbHeaderFooter1
            // 
            this.cmbHeaderFooter1.AllowEmptyValue = false;
            this.cmbHeaderFooter1.Borderless = false;
            this.cmbHeaderFooter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbHeaderFooter1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbHeaderFooter1.IsDirty = false;
            this.cmbHeaderFooter1.LimitToList = true;
            this.cmbHeaderFooter1.ListName = "";
            this.cmbHeaderFooter1.Location = new System.Drawing.Point(104, 23);
            this.cmbHeaderFooter1.MaxDropDownItems = 8;
            this.cmbHeaderFooter1.Name = "cmbHeaderFooter1";
            this.cmbHeaderFooter1.SelectedIndex = -1;
            this.cmbHeaderFooter1.SelectedValue = null;
            this.cmbHeaderFooter1.SelectionLength = 0;
            this.cmbHeaderFooter1.SelectionStart = 0;
            this.cmbHeaderFooter1.Size = new System.Drawing.Size(190, 23);
            this.cmbHeaderFooter1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbHeaderFooter1.SupportingValues = "";
            this.cmbHeaderFooter1.TabIndex = 1;
            this.cmbHeaderFooter1.Tag2 = null;
            this.cmbHeaderFooter1.Value = "";
            this.cmbHeaderFooter1.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbHeaderFooter1_ValueChanged);
            // 
            // lblPageNumbers1
            // 
            this.lblPageNumbers1.AutoSize = true;
            this.lblPageNumbers1.Location = new System.Drawing.Point(3, 58);
            this.lblPageNumbers1.Name = "lblPageNumbers1";
            this.lblPageNumbers1.Size = new System.Drawing.Size(95, 13);
            this.lblPageNumbers1.TabIndex = 2;
            this.lblPageNumbers1.Text = "&Different 1st Page:";
            // 
            // lblHeaderFooter1
            // 
            this.lblHeaderFooter1.AutoSize = true;
            this.lblHeaderFooter1.Location = new System.Drawing.Point(3, 25);
            this.lblHeaderFooter1.Name = "lblHeaderFooter1";
            this.lblHeaderFooter1.Size = new System.Drawing.Size(90, 13);
            this.lblHeaderFooter1.TabIndex = 0;
            this.lblHeaderFooter1.Text = "&Headers/Footers:";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(124, 311);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(92, 29);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "&Insert";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(223, 311);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 29);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // gbSection2
            // 
            this.gbSection2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSection2.Controls.Add(this.cmbPageNumbers2);
            this.gbSection2.Controls.Add(this.cmbHeaderFooter2);
            this.gbSection2.Controls.Add(this.lblPageNumbers2);
            this.gbSection2.Controls.Add(this.lblHeaderFooter2);
            this.gbSection2.Location = new System.Drawing.Point(12, 168);
            this.gbSection2.Name = "gbSection2";
            this.gbSection2.Size = new System.Drawing.Size(303, 90);
            this.gbSection2.TabIndex = 4;
            this.gbSection2.TabStop = false;
            this.gbSection2.Text = "After Section Break";
            // 
            // cmbPageNumbers2
            // 
            this.cmbPageNumbers2.AllowEmptyValue = false;
            this.cmbPageNumbers2.Borderless = false;
            this.cmbPageNumbers2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbPageNumbers2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPageNumbers2.IsDirty = false;
            this.cmbPageNumbers2.LimitToList = true;
            this.cmbPageNumbers2.ListName = "";
            this.cmbPageNumbers2.Location = new System.Drawing.Point(104, 55);
            this.cmbPageNumbers2.MaxDropDownItems = 8;
            this.cmbPageNumbers2.Name = "cmbPageNumbers2";
            this.cmbPageNumbers2.SelectedIndex = -1;
            this.cmbPageNumbers2.SelectedValue = null;
            this.cmbPageNumbers2.SelectionLength = 0;
            this.cmbPageNumbers2.SelectionStart = 0;
            this.cmbPageNumbers2.Size = new System.Drawing.Size(191, 23);
            this.cmbPageNumbers2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbPageNumbers2.SupportingValues = "";
            this.cmbPageNumbers2.TabIndex = 3;
            this.cmbPageNumbers2.Tag2 = null;
            this.cmbPageNumbers2.Value = "";
            // 
            // cmbHeaderFooter2
            // 
            this.cmbHeaderFooter2.AllowEmptyValue = false;
            this.cmbHeaderFooter2.Borderless = false;
            this.cmbHeaderFooter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbHeaderFooter2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbHeaderFooter2.IsDirty = false;
            this.cmbHeaderFooter2.LimitToList = true;
            this.cmbHeaderFooter2.ListName = "";
            this.cmbHeaderFooter2.Location = new System.Drawing.Point(104, 22);
            this.cmbHeaderFooter2.MaxDropDownItems = 8;
            this.cmbHeaderFooter2.Name = "cmbHeaderFooter2";
            this.cmbHeaderFooter2.SelectedIndex = -1;
            this.cmbHeaderFooter2.SelectedValue = null;
            this.cmbHeaderFooter2.SelectionLength = 0;
            this.cmbHeaderFooter2.SelectionStart = 0;
            this.cmbHeaderFooter2.Size = new System.Drawing.Size(191, 23);
            this.cmbHeaderFooter2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbHeaderFooter2.SupportingValues = "";
            this.cmbHeaderFooter2.TabIndex = 1;
            this.cmbHeaderFooter2.Tag2 = null;
            this.cmbHeaderFooter2.Value = "";
            this.cmbHeaderFooter2.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbHeaderFooter2_ValueChanged);
            // 
            // lblPageNumbers2
            // 
            this.lblPageNumbers2.AutoSize = true;
            this.lblPageNumbers2.Location = new System.Drawing.Point(3, 58);
            this.lblPageNumbers2.Name = "lblPageNumbers2";
            this.lblPageNumbers2.Size = new System.Drawing.Size(95, 13);
            this.lblPageNumbers2.TabIndex = 2;
            this.lblPageNumbers2.Text = "Different 1st &Page:";
            // 
            // lblHeaderFooter2
            // 
            this.lblHeaderFooter2.AutoSize = true;
            this.lblHeaderFooter2.Location = new System.Drawing.Point(3, 25);
            this.lblHeaderFooter2.Name = "lblHeaderFooter2";
            this.lblHeaderFooter2.Size = new System.Drawing.Size(90, 13);
            this.lblHeaderFooter2.TabIndex = 0;
            this.lblHeaderFooter2.Text = "Headers/&Footers:";
            // 
            // cmbLocation
            // 
            this.cmbLocation.AllowEmptyValue = false;
            this.cmbLocation.Borderless = false;
            this.cmbLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLocation.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLocation.IsDirty = false;
            this.cmbLocation.LimitToList = true;
            this.cmbLocation.ListName = "";
            this.cmbLocation.Location = new System.Drawing.Point(12, 31);
            this.cmbLocation.MaxDropDownItems = 8;
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.SelectedIndex = -1;
            this.cmbLocation.SelectedValue = null;
            this.cmbLocation.SelectionLength = 0;
            this.cmbLocation.SelectionStart = 0;
            this.cmbLocation.Size = new System.Drawing.Size(246, 23);
            this.cmbLocation.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLocation.SupportingValues = "";
            this.cmbLocation.TabIndex = 1;
            this.cmbLocation.Tag2 = null;
            this.cmbLocation.Value = "";
            // 
            // InsertSectionBreakForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(330, 348);
            this.Controls.Add(this.cmbLocation);
            this.Controls.Add(this.gbSection2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbSection1);
            this.Controls.Add(this.chkRestartNumbering);
            this.Controls.Add(this.lblLocation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertSectionBreakForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert Section Break";
            this.Load += new System.EventHandler(this.InsertSectionBreakForm_Load);
            this.gbSection1.ResumeLayout(false);
            this.gbSection1.PerformLayout();
            this.gbSection2.ResumeLayout(false);
            this.gbSection2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.CheckBox chkRestartNumbering;
        private System.Windows.Forms.GroupBox gbSection1;
        private System.Windows.Forms.Label lblHeaderFooter1;
        private System.Windows.Forms.Label lblPageNumbers1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox gbSection2;
        private System.Windows.Forms.Label lblPageNumbers2;
        private System.Windows.Forms.Label lblHeaderFooter2;
        private LMP.Controls.ComboBox cmbPageNumbers1;
        private LMP.Controls.ComboBox cmbHeaderFooter1;
        private LMP.Controls.ComboBox cmbPageNumbers2;
        private LMP.Controls.ComboBox cmbHeaderFooter2;
        private LMP.Controls.ComboBox cmbLocation;
    }
}