﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using LMP.Architect.Api;
using LMP.Data;

namespace LMP.MacPac
{
    public static class ContentTreeHelper
    {
        /// <summary>
        /// Return iSegmentDef object corresponding to FolderMember Node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static ISegmentDef GetSegmentDefFromNode(UltraTreeNode oNode)
        {
            try
            {
                LoadNodeReference(oNode);
                if (oNode.Tag is FolderMember)
                {
                    FolderMember oMember = (FolderMember)oNode.Tag;
                    switch (oMember.ObjectTypeID)
                    {
                        case mpFolderMemberTypes.AdminSegment:
                            {
                                AdminSegmentDefs oSegs = new AdminSegmentDefs();
                                AdminSegmentDef oSeg = (AdminSegmentDef)oSegs.ItemFromID(oMember.ObjectID1);
                                return (ISegmentDef)oSeg;
                            }
                        case mpFolderMemberTypes.UserSegment:
                        case mpFolderMemberTypes.UserSegmentPacket:
                            {
                                UserSegmentDefs oSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                                UserSegmentDef oSeg = (UserSegmentDef)oSegs.ItemFromID(oMember.ObjectID1.ToString()
                                    + "." + oMember.ObjectID2.ToString());
                                return (ISegmentDef)oSeg;
                            }
                        case mpFolderMemberTypes.VariableSet:
                            {
                                VariableSetDef oDef = GetVariableSetFromNode(oNode);
                                ISegmentDef oIDef = null;

                                if (oDef.SegmentID.EndsWith(".0"))
                                {
                                    //variable set is associated to admin segment
                                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                                    oIDef = (ISegmentDef)oDefs.ItemFromID(
                                        (int)Double.Parse(oDef.SegmentID.Replace(".0", ""))); //GLOG 6966
                                }
                                else
                                {
                                    //variable set is associated to a user segment
                                    UserSegmentDefs oDefs = new UserSegmentDefs(
                                        mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);
                                    oIDef = (ISegmentDef)oDefs.ItemFromID(oDef.SegmentID);
                                }
                                return oIDef;
                            }
                        default:
                            return null;
                    }
                }
                else
                {
                    // When the node comes from the find resultTree and it is for a VariableSetDef
                    // the tag contains the VariableSetDef. Use it to get the segment.
                    if (oNode.Tag is VariableSetDef)
                    {
                        VariableSetDef oDef = (VariableSetDef)oNode.Tag;
                        AdminSegmentDefs oSegs = new AdminSegmentDefs();
                        //GLOG 6966: Make sure string can be parsed as a Double even if
                        //the Decimal separator is something other than '.' in Regional Settings
                        string xID = oDef.SegmentID;
                        if (xID.EndsWith(".0"))
                            xID = xID.Replace(".0", "");
                        AdminSegmentDef oSeg = (AdminSegmentDef)oSegs.ItemFromID(
                            (int)Double.Parse(xID));
                        return (ISegmentDef)oSeg;
                    }

                    return (ISegmentDef)oNode.Tag;
                }
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// True if Node corresponds to an AdminFolder object
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsAdminFolderNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            else if (oNode.Tag is Folder || oNode.Tag is int || oNode.Tag is Person)
                return true;
            else
                return false;
        }
        /// <summary>
        /// True if Node is at top level of the tree ('My Folders' or 'Public Folders' node)
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsTopLevelNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            return (oNode.Level == 0);
        }
        /// <summary>
        /// True if Node corresponds to a UserFolder object
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsUserFolderNode(UltraTreeNode oNode)
        {

            if (oNode == null)
                return false;
            else if (oNode.Tag is UserFolder || (oNode.Tag is string && !(oNode.Key.StartsWith("U") ||
                oNode.Key.StartsWith("A"))))
                return true;
            else
                return false;
        }
        /// <summary>
        /// True if Node corresponds to an VariableSet/Prefill object
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsVariableSetNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            else if ((oNode.Tag is string && ((string)oNode.Tag).StartsWith("P")) ||
                (oNode.Tag is FolderMember && ((FolderMember)oNode.Tag).ObjectTypeID == mpFolderMemberTypes.VariableSet) ||
                (oNode.Tag is VariableSetDef))
                return true;
            else
                return false;
        }
        /// <summary>
        /// True if Node corresponds to an AdminSegment object
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsAdminSegmentNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            else if ((oNode.Tag is string && oNode.Key.StartsWith("A")) ||
                (oNode.Tag is FolderMember && ((FolderMember)oNode.Tag).ObjectTypeID == mpFolderMemberTypes.AdminSegment) ||
                (oNode.Tag is AdminSegmentDef))
                return true;
            else
                return false;
        }
        /// <summary>
        /// True if Node corresponds to an UserSegment object
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsUserSegmentNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            else if ((oNode.Tag is string && oNode.Key.StartsWith("U")) ||
                (oNode.Tag is FolderMember && ((FolderMember)oNode.Tag).FolderType == mpFolderTypes.User && (((FolderMember)oNode.Tag).ObjectTypeID == mpFolderMemberTypes.UserSegment || ((FolderMember)oNode.Tag).ObjectTypeID == mpFolderMemberTypes.UserSegmentPacket)) ||
                (oNode.Tag is UserSegmentDef))
                return true;
            else
                return false;
        }
        /// <summary>
        /// True if Node corresponds to an Segment Designer object
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsDesignerSegmentNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            else if (IsUserSegmentNode(oNode))
            {
                ISegmentDef oDef = GetSegmentDefFromNode(oNode);
                if (oDef is UserSegmentDef && oDef.TypeID != mpObjectTypes.UserSegment)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        /// <summary>
        /// True if Node is a Master Data node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsMasterData(UltraTreeNode oNode)
        {
            if (IsVariableSetNode(oNode))
            {
                ISegmentDef oDef = GetSegmentDefFromNode(oNode);

                if (oDef != null)
                {
                    return oDef.TypeID == mpObjectTypes.MasterData;
                }
                else
                {
                    return false;
                }
            }
            else
                return false;
        }
        /// <summary>
        /// True if Node is a Segment Packet
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsSegmentPacket(UltraTreeNode oNode)
        {
            ISegmentDef oDef = GetSegmentDefFromNode(oNode);

            if (oDef != null)
            {
                return oDef.TypeID == mpObjectTypes.SegmentPacket;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// True if Node is the Top Level "My Folders" node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsMyFolderNode(UltraTreeNode oNode)
        {
            if (oNode != null && oNode.Key == "0.0")
                return true;
            else
                return false;
        }
        /// <summary>
        /// True if Node is the Top Level "Public Folders" node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsPublicFolderNode(UltraTreeNode oNode)
        {
            if (oNode != null && oNode.Key == "0")
                return true;
            else
                return false;
        }
        /// <summary>
        /// True if Node is the Top Level "Shared Folders" node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsSharedFolderRootNode(UltraTreeNode oNode)
        {
            if (oNode != null && (oNode.Key == "-2"))
                return true;
            else
                return false;
        }

        /// <summary>
        /// True if Node is a Shared Folder node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsSharedFolderNode(UltraTreeNode oNode)
        {
            //GLOG item #4872 - dcf -
            //was returning false for shared folder
            //nodes whose tag still contained the
            //ID of the shared folder
            int iID = 0;
            try
            {
                //node tag may still contain just the ID
                //instead of the actual folder reference -
                //test for numeric ID - folder members 
                //don't have numeric IDs - only folders do
                iID = (int)oNode.Tag;
            }
            catch { }

            if (oNode != null && (oNode.Tag is Folder || iID > 0) && oNode.RootNode.Key == "-2")
                return true;
            else
                return false;
        }
        /// <summary>
        /// True if Node is a virtual Owner Folder node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsOwnerFolderNode(UltraTreeNode oNode)
        {
            if (oNode != null && oNode.Tag is Person)
                return true;
            else
                return false;
        }
        internal static bool IsOwnerFolderMemberNode(UltraTreeNode oNode)
        {
            if (oNode != null && oNode.Parent != null && oNode.Parent.Tag is Person)
                return true;
            else
                return false;
        }
        internal static bool IsSharedFolderMemberNode(UltraTreeNode oNode)
        {
            //if (oNode != null && oNode.Parent != null && IsSharedFolderNode(oNode.Parent))
            if (oNode != null && oNode.Parent != null && !IsSharedFolderNode(oNode) && oNode.RootNode.Key == "-2")
                return true;
            else
                return false;
        }
        /// <summary>
        /// True for segment in Shared Folder or Owner Folder
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool IsSharedContentNode(UltraTreeNode oNode)
        {
            return (IsOwnerFolderMemberNode(oNode) || IsSharedFolderMemberNode(oNode));
        }
        internal static bool IsStyleSheetNode(UltraTreeNode oNode)
        {
            try
            {
                LoadNodeReference(oNode);
                if (!IsUserSegmentNode(oNode) && !IsAdminSegmentNode(oNode))
                    return false;

                ISegmentDef oDef = GetSegmentDefFromNode(oNode);

                if (oDef is UserSegmentDef)
                    return (((UserSegmentDef)oDef).IntendedUse == mpSegmentIntendedUses.AsStyleSheet);
                else
                    return (((AdminSegmentDef)oDef).IntendedUse == mpSegmentIntendedUses.AsStyleSheet);
            }
            catch
            {
                return false;
            }
        }
        internal static bool IsAnswerFileNode(UltraTreeNode oNode)
        {
            try
            {
                LoadNodeReference(oNode);
                if (IsUserSegmentNode(oNode) || !IsAdminSegmentNode(oNode))
                    return false;

                ISegmentDef oDef = GetSegmentDefFromNode(oNode);

                return (((AdminSegmentDef)oDef).IntendedUse == mpSegmentIntendedUses.AsAnswerFile);
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// True if Node corresponds to a UserFolder or FolderMember
        /// with OwnerID==Session.CurrentUser.ID
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static bool UserIsOwnerOf(UltraTreeNode oNode)
        {
            try
            {

                if (IsUserFolderNode(oNode))
                {
                    LoadNodeReference(oNode);
                    if (((UserFolder)oNode.Tag).OwnerID == Session.CurrentUser.ID)
                        return true;
                    else
                        return false;
                }
                else if (IsVariableSetNode(oNode))
                {
                    LoadNodeReference(oNode);
                    if (oNode.Tag is FolderMember)
                    {
                        FolderMember oMember = (FolderMember)oNode.Tag;
                        {
                            VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, Session.CurrentUser.ID);
                            VariableSetDef oDef = null;
                            try
                            {
                                oDef = (VariableSetDef)oDefs.ItemFromID(string.Concat(oMember.ObjectID1.ToString(), ".", oMember.ObjectID2.ToString()));
                            }
                            catch { }
                            if (oDef == null)
                                return false;
                            else
                                return true;
                        }
                    }
                    else if (oNode.Tag is VariableSetDef)
                    {
                        return ((VariableSetDef)oNode.Tag).OwnerID == Session.CurrentUser.ID;
                    }
                    else
                        return false;

                }
                else if (IsUserSegmentNode(oNode))
                {
                    LoadNodeReference(oNode);
                    if (oNode.Tag is FolderMember)
                    {
                        FolderMember oMember = (FolderMember)oNode.Tag;
                        if (oMember.ObjectID2 == 0)
                        {
                            // this is a copy of an Admin Segment
                            if (Session.AdminMode)
                                return true;
                            else
                                return false;
                        }
                        else
                        {
                            UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, Session.CurrentUser.ID);
                            UserSegmentDef oDef = null;
                            try
                            {
                                oDef = (UserSegmentDef)oDefs.ItemFromID(string.Concat(oMember.ObjectID1.ToString(), ".", oMember.ObjectID2.ToString()));
                            }
                            catch { }
                            if (oDef == null)
                                return false;
                            else
                                return true;
                        }
                    }
                    else if (oNode.Tag is UserSegmentDef)
                    {
                        return ((UserSegmentDef)oNode.Tag).OwnerID == Session.CurrentUser.ID;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Return iSegmentDef object corresponding to FolderMember Node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static VariableSetDef GetVariableSetFromNode(UltraTreeNode oNode)
        {
            try
            {
                LoadNodeReference(oNode);
                if (oNode.Tag is FolderMember)
                {
                    FolderMember oMember = (FolderMember)oNode.Tag;
                    if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
                    {
                        VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                        VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(oMember.ObjectID1.ToString()
                            + "." + oMember.ObjectID2.ToString());
                        return oDef;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return (VariableSetDef)oNode.Tag;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// returns the Segment ID of the specified node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        internal static string GetSegmentIDFromNode(UltraTreeNode oNode)
        {
            string xID = null;
            if (ContentTreeHelper.IsVariableSetNode(oNode))
            {
                xID = GetVariableSetFromNode(oNode).SegmentID.ToString();
            }
            else if (oNode.Tag is FolderMember)
            {
                FolderMember oMember = (FolderMember)oNode.Tag;
                if (oMember.FolderType == mpFolderTypes.Admin)
                    xID = oMember.ObjectID1.ToString();
                else
                    xID = oMember.ObjectID1.ToString() + "." + oMember.ObjectID2.ToString();
            }
            else if (oNode.Tag is AdminSegmentDef)
            {
                xID = ((AdminSegmentDef)oNode.Tag).ID.ToString();
            }
            else if (oNode.Tag is UserSegmentDef)
            {
                xID = ((UserSegmentDef)oNode.Tag).ID;
            }

            return xID;
        }

        /// <summary>
        /// Populates Tag of Node with appropriate reference object
        /// if current tag holds an int or string value
        /// Tag may be Folder, UserFolder or FolderMember object
        /// </summary>
        /// <param name="oNode"></param>
        internal static void LoadNodeReference(UltraTreeNode oNode)
        {
            try
            {
                if (oNode.Control is LMP.Controls.FolderTreeView && 
                    ((LMP.Controls.FolderTreeView)oNode.Control).Mode == LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FindResults)
                {
                    //Node is in Find Results of FolderTreeView
                    if (oNode.Tag is object[])
                    {
                        object[] aProps = (object[])oNode.Tag;
                        string xID = aProps[1].ToString();
                        mpFolderMemberTypes iType = (mpFolderMemberTypes)(Int32.Parse(aProps[2].ToString()));
                        //Node may be UserFolder or FolderMember
                        //Check value of Tag for 'S' marker
                        DateTime t0 = DateTime.Now;
                        if (iType == mpFolderMemberTypes.UserSegment)
                        {
                            UserSegmentDefs oSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                            UserSegmentDef oSeg = (UserSegmentDef)oSegs.ItemFromID(xID);
                            oNode.Tag = oSeg;
                        }
                        else if (iType == mpFolderMemberTypes.AdminSegment)
                        {
                            AdminSegmentDefs oSegs = new AdminSegmentDefs();
                            AdminSegmentDef oSeg = (AdminSegmentDef)oSegs.ItemFromID(Int32.Parse(xID));
                            oNode.Tag = oSeg;
                        }
                        else if (iType == mpFolderMemberTypes.VariableSet)
                        {
                            VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                            VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(xID);
                            oNode.Tag = oDef;
                        }
                        else
                            return;
                    }
                    else
                        return;

                }
                else if (oNode.Control.Name.ToLower() != "treeresults" && 
                    !(ContentTreeHelper.IsOwnerFolderMemberNode(oNode) || ContentTreeHelper.IsSharedFolderMemberNode(oNode)))
                {
                    //we're not in 'Find' mode or in a Shared Folder
                    if (oNode.Tag is int)
                    {
                        // Node is an Admin Folder
                        if ((int)oNode.Tag == 0 || (int)oNode.Tag == -2)
                            // This is the top level node
                            return;
                        DateTime t0 = DateTime.Now;
                        Folders oAdminFolders = new Folders();
                        Folder oFolder = (Folder)oAdminFolders.ItemFromID((int)oNode.Tag);
                        oNode.Tag = oFolder;
                        LMP.Benchmarks.Print(t0);
                        return;
                    }
                    else if (oNode.Tag is string)
                    {
                        string xTag = (string)oNode.Tag;
                        // Top level UserFolder node doesn't have an associated object
                        if (xTag == "0.0")
                            return;
                        if (oNode.Parent == null || oNode.Parent.Tag is UserFolder ||
                            ContentTreeHelper.IsMyFolderNode(oNode.Parent))
                        {
                            //Node may be UserFolder or FolderMember
                            //Check value of Tag for 'S' marker
                            DateTime t0 = DateTime.Now;
                            if (xTag.StartsWith("U") || xTag.StartsWith("A") || xTag.StartsWith("P"))
                            {
                                // Need to filter on Parent Folder ID for ItemFromID
                                UserFolder oParent = (UserFolder)oNode.Parent.Tag;
                                string xID1, xID2;
                                String.SplitString(oParent.ID, out xID1, out xID2, ".");
                                FolderMembers oMembers = new FolderMembers(Int32.Parse(xID1), Int32.Parse(xID2));
                                xTag = xTag.Substring(1);
                                FolderMember oMember = (FolderMember)oMembers.ItemFromID(xTag);
                                oNode.Tag = oMember;
                            }
                            else
                            {
                                string xID1, xID2;
                                if (oNode.Parent.Tag is string)
                                {
                                    xID1 = "0";
                                    xID2 = "0";
                                }
                                else
                                {
                                    UserFolder oParent = (UserFolder)oNode.Parent.Tag;
                                    String.SplitString(oParent.ID, out xID1, out xID2, ".");
                                }
                                UserFolders oUserFolders = new UserFolders(Session.CurrentUser.ID,
                                    Int32.Parse(xID1), Int32.Parse(xID2));
                                UserFolder oFolder = (UserFolder)oUserFolders.ItemFromID(xTag);
                                oNode.Tag = oFolder;
                            }
                            LMP.Benchmarks.Print(t0);
                            return;
                        }
                        else if (oNode.Parent.Tag is Folder)
                        {
                            //Node is FolderMember
                            DateTime t0 = DateTime.Now;
                            FolderMembers oMembers = new FolderMembers(0);
                            xTag = xTag.Substring(1);
                            FolderMember oMember = (FolderMember)oMembers.ItemFromID(xTag);
                            oNode.Tag = oMember;
                            LMP.Benchmarks.Print(t0);
                            return;
                        }
                    }
                    else
                        return;
                }
                else
                {
                    //we're in 'Find' mode or a Shared Folder
                    if (oNode.Tag is string)
                    {
                        string xTag = (string)oNode.Tag;
                        //Node may be UserFolder or FolderMember
                        //Check value of Tag for 'S' marker
                        DateTime t0 = DateTime.Now;
                        if (xTag.StartsWith("U"))
                        {
                            UserSegmentDefs oSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
                            UserSegmentDef oSeg = (UserSegmentDef)oSegs.ItemFromID(xTag.Substring(1));
                            oNode.Tag = oSeg;
                        }
                        else if (xTag.StartsWith("A"))
                        {
                            AdminSegmentDefs oSegs = new AdminSegmentDefs();
                            AdminSegmentDef oSeg = (AdminSegmentDef)oSegs.ItemFromID(Int32.Parse(xTag.Substring(1)));
                            oNode.Tag = oSeg;
                        }
                        else if (xTag.StartsWith("P"))
                        {
                            VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                            VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(xTag.Substring(1));
                            oNode.Tag = oDef;
                        }
                        else
                            return;
                    }

                }
            }
            catch (LMP.Exceptions.NotInCollectionException)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_Object_Deleted_From_DB"), LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                oNode.Remove();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }
        }
    }
}
