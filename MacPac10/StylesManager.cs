using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using System.IO;
using Word = Microsoft.Office.Interop.Word;

namespace LMP.MacPac
{
    public partial class StylesManager : Form
    {
        #region *********************fields*********************
        private ISegmentDef m_oSegmentDef = null;
        private bool m_bSegmentsLoaded = false;
        private bool m_bOutlineStylesOmitted = false;
        #endregion
        #region *********************enums*********************
        public enum mpDestinations
        {
            ActiveDocument = 1,
            SelectedSegments = 2
        }
        #endregion
        #region *********************constructor*********************
        public StylesManager(ISegmentDef oSegmentDef)
        {
            InitializeComponent();
            m_oSegmentDef = oSegmentDef;
            SetupControl();
        }
        #endregion
        #region *********************methods*********************
        public void SetupControl()
        {
            this.lblProgress.Visible = false;
            if (Session.AdminMode)
            {
                this.ftvSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders |
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments |
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserSegments; //GLOG 6208
            }
            else
            {
                //GLOG 5693: Don't show Admin Segment shortcuts as possible targets
                this.ftvSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders | 
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserSegments;
            }
            this.ftvSegments.OwnerID = Session.CurrentUser.ID;
            this.ftvSegments.ShowCheckboxes = true;
            this.ftvSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
            this.ftvSegments.Enabled = false;
            this.cmbCopyTo.SetList(new string[,]{{LMP.Resources.GetLangString("Dialog_StylesManager_ActiveDocument"), "1",},
                {LMP.Resources.GetLangString("Dialog_StylesManager_SelectedSegments"), "2"}});
            this.Text = LMP.Resources.GetLangString("Dialog_StylesManager_Title");

            if (m_oSegmentDef != null)
            {
                this.lblAvailableStyles.Text = string.Format(LMP.Resources.GetLangString("Dialog_StylesManager_AvailableStyles"));
                PopulateStyleList(m_oSegmentDef.XML);
            }

            this.btnSelectAll.Text = LMP.Resources.GetLangString("Dialog_StylesManager_SelectAll");
            this.btnDeselectAll.Text = LMP.Resources.GetLangString("Dialog_StylesManager_DeselectAll");
            this.lblCopyTo.Text = LMP.Resources.GetLangString("Dialog_StylesManager_CopyTo");
            this.btnOK.Text = LMP.Resources.GetLangString("Lbl_OK");
            this.btnCancel.Text = LMP.Resources.GetLangString("Lbl_Cancel");
            this.cmbCopyTo.SelectedIndex = 0;

            this.lbStyles.Height = this.btnOK.Top - this.lbStyles.Top - 6;
        }
        /// <summary>
        /// Populate Styles List from w:style nodes in document XML
        /// </summary>
        /// <param name="xSourceXML"></param>
        private void PopulateStyleList(string xSourceXML)
        {
            string xStyleXML = LMP.String.ExtractStyleXML(xSourceXML);
            m_bOutlineStylesOmitted = false;
            string[] aStyles = xStyleXML.Split(new string[] { "</w:style>" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string xStyle in aStyles)
            {
                //look first for defined uiName, since this is what OrganizerCopy will use
                int iPos = xStyle.IndexOf("<wx:uiName ");
                if (iPos == -1)
                    //look for style name in format <w:name w:val="stylename"/>
                    iPos = xStyle.IndexOf("<w:name ");

                if (iPos > -1)
                {
                    int iNameStart = xStyle.IndexOf("\"", iPos) + 1;
                    if (iNameStart > 0)
                    {
                        int iNameEnd = xStyle.IndexOf("\"", iNameStart + 1);
                        if (iNameEnd > 0)
                        {
                            //GLOG item #5713 - dcf - 10/3/11
                            //GLOG 8200 - replace &quot; token as well
                            string xName = String.RestoreXMLChars(
                                xStyle.Substring(iNameStart, iNameEnd - iNameStart), true);

                            //GLOG 8605
                            xName = LMP.Forte.MSWord.WordDoc.GetBuiltInStyleName(xName, LMP.Forte.MSWord.WordApp.ActiveDocument());
                            //Check for MacPac Numbering styles (ending in '_L#'
                            if (xStyle.IndexOf("<w:outlineLvl w:val=") > -1 && 
                                (xName.Substring(xName.Length - 3, 2) == "_L") 
                                && String.IsNumericInt32(xName.Substring(xName.Length - 1, 1)))
                            {
                                //List Template does not carry over when copying style using Organizer
                                //So MacPac Numbering styles are omitted from list
                                m_bOutlineStylesOmitted = true;
                            }
                            else
                            {
                                //Omit linked Char styles from list
                                if (!xName.EndsWith("Char") || xStyle.IndexOf("<w:link w:val=") == -1)
                                    lbStyles.Items.Add(xName);
                            }
                        }
                    }
                }
            }
        }
        private string[] GetSelectedStyles()
        {
            if (lbStyles.CheckedItems.Count == 0)
                return null;

            string[] aTemp = new string[lbStyles.CheckedItems.Count];
            int iOffset = 0;
            for (int i = lbStyles.CheckedItems.Count - 1; i >= 0; i--)
            {
                if (lbStyles.CheckedItems[i].ToString().ToUpper() == "NORMAL")
                {
                    //Copy Normal style first - this duplicates Organizer behavior when
                    //selecting multiple styles
                    aTemp[0] = lbStyles.CheckedItems[i].ToString();
                    iOffset = 1;
                }
                else
                {
                    aTemp[i + iOffset] = lbStyles.CheckedItems[i].ToString();
                }
            }
            return aTemp;
        }
        private void ProcessSelectedStyles()
        {
            string[] aStyles = GetSelectedStyles();
            if (aStyles == null)
                return;

            // Create temporary file for source XML
            string xSourcePath = Path.GetTempFileName();
            File.Move(xSourcePath, xSourcePath.Replace(".tmp", ".xml"));
            xSourcePath = xSourcePath.Replace(".tmp", ".xml");

            try
            {
                //Extract style nodes from XML
                //LMP.String.GetMinimalStyleDocumentXML(m_oSegmentDef.XML);
                //JTS 6/21/10: GetMinimalStyleDocumentXML not compatible with WordOpen format
                //so just use Segment XML for source
                string xStyleXML = m_oSegmentDef.XML; 

                // Convert XML text to byte array
                byte[] bytSrcXML = System.Text.Encoding.UTF8.GetBytes((xStyleXML).ToCharArray());
                

                try
                {
                    FileStream oSrcStream = File.Open(xSourcePath, FileMode.Create);

                    // Write bytXML to temporary file
                    oSrcStream.Write(bytSrcXML, 0, bytSrcXML.Length);
                    oSrcStream.Close();
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.XMLException(oE.Message);
                }

                if ((Int32.Parse(cmbCopyTo.SelectedValue.ToString()) == (int)mpDestinations.SelectedSegments))
                {
                    //Copy styles to selected segments
                    AdminSegmentDefs oAdminDefs = new AdminSegmentDefs();
                    UserSegmentDefs oUserDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner,
                        Session.CurrentUser.ID);
                    for (int i = 0; i < this.ftvSegments.MemberList.Count; i++)
                    {
                        //Display status label
                        ISegmentDef oSegDef = null;
                        string xID = ((ArrayList)this.ftvSegments.MemberList[i])[0].ToString();
                        if (xID.IndexOf(".") == -1 || xID.EndsWith(".0"))
                        {
                            if (xID.EndsWith(".0"))
                                xID = xID.Substring(0, xID.Length - 2);
                            AdminSegmentDef oDef = (AdminSegmentDef)oAdminDefs.ItemFromID(Int32.Parse(xID));
                            //Skip if Selected Segment is same as source segment
                            if (m_oSegmentDef.TypeID == mpObjectTypes.UserSegment ||
                                ((AdminSegmentDef)m_oSegmentDef).ID != oDef.ID)
                                oSegDef = oDef;
                        }
                        else
                        {
                            UserSegmentDef oDef = (UserSegmentDef)oUserDefs.ItemFromID(xID);
                            if (m_oSegmentDef.TypeID != mpObjectTypes.UserSegment ||
                                ((UserSegmentDef)m_oSegmentDef).ID != oDef.ID)
                                oSegDef = oDef;
                        }

                        if (oSegDef != null)
                        {
                            this.lblProgress.Text = LMP.Resources.GetLangString("Dialog_StylesManager_Progress") + oSegDef.DisplayName;
                            this.lblProgress.Visible = true;
                            this.lblProgress.Refresh();

                            // Convert XML text to byte array
                            byte[] bytXML = System.Text.Encoding.UTF8.GetBytes(
                                (oSegDef.XML).ToCharArray());

                            // Create temporary file
                            string xTargetPath = Path.GetTempFileName();
                            string xTargetPath2 = Path.GetTempFileName();
                            File.Move(xTargetPath, xTargetPath.Replace(".tmp", ".xml"));
                            xTargetPath = xTargetPath.Replace(".tmp", ".xml");
                            File.Move(xTargetPath2, xTargetPath2.Replace(".tmp", ".xml"));
                            xTargetPath2 = xTargetPath2.Replace(".tmp", ".xml");
                            try
                            {
                                FileStream oStream = File.Open(xTargetPath, FileMode.Create);

                                // Write bytXML to temporary file
                                oStream.Write(bytXML, 0, bytXML.Length);
                                oStream.Close();
                                CopyStylesToTarget(xSourcePath, xTargetPath, aStyles);
                                //GLOG 2961: OrganizerCopy can leave file locked, so create copy to use
                                File.Copy(xTargetPath, xTargetPath2, true);
                                //Read updated XML from temporary file
                                oStream = File.Open(xTargetPath2, FileMode.Open);
                                StreamReader oSR = new StreamReader(oStream);
                                oSegDef.XML = oSR.ReadToEnd();
                                oStream.Close();
                                oSR.Close();
                                //GLOG 8798: prepend <mAuthorPrefs> code if the segment contains author preferences
                                bool bContainsAuthorPrefs = oSegDef.XML.ToUpper().Contains("[AUTHORPREFERENCE_") ||
                                    oSegDef.XML.ToUpper().Contains("[LEADAUTHORPREFERENCE_") ||
                                    oSegDef.XML.ToUpper().Contains("[AUTHORPARENTPREFERENCE") ||
                                    oSegDef.XML.ToUpper().Contains("[LEADAUTHORPARENTPREFERENCE_");

                                if (bContainsAuthorPrefs)
                                    oSegDef.XML = "<mAuthorPrefs>" + oSegDef.XML;

                                if (oSegDef.TypeID == mpObjectTypes.UserSegment)
                                    oUserDefs.Save((StringIDSimpleDataItem)(UserSegmentDef)oSegDef);
                                else
                                    oAdminDefs.Save((LongIDSimpleDataItem)(AdminSegmentDef)oSegDef);

                            }
                            catch (System.Exception oE)
                            {
                                throw new LMP.Exceptions.XMLException(oE.Message);
                            }
                            finally
                            {
                                // Clean up temp files
                                try
                                {
                                    if (File.Exists(xTargetPath))
                                    {
                                        File.Delete(xTargetPath);
                                    }
                                }
                                catch (IOException)
                                {
                                    //If couldn't delete, add to list to be deleted at exit
                                    OS.AddFileToDeletionList(xTargetPath);
                                }
                                try
                                {
                                    if (File.Exists(xTargetPath2))
                                    {
                                        File.Delete(xTargetPath2);
                                    }
                                }
                                catch (IOException)
                                {
                                    //If couldn't delete, add to list to be deleted at exit
                                    OS.AddFileToDeletionList(xTargetPath2);
                                }

                            }
                        }
                    }
                }
                else
                {
                    this.lblProgress.Text = LMP.Resources.GetLangString("Dialog_StylesManager_Progress") + Session.CurrentWordApp.ActiveDocument.Name;
                    this.lblProgress.Visible = true;
                    this.lblProgress.Refresh();
                    LMP.Forte.MSWord.WordDoc.OrganizerCopyStyles(Session.CurrentWordApp.ActiveDocument, xSourcePath, aStyles);
                    //CopyStylesToTarget(xSourcePath, Session.CurrentWordApp.ActiveDocument.FullName, aStyles);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.XMLException(oE.Message);
            }
            finally
            {
                try
                {
                    //Clean up temporary file
                    if (File.Exists(xSourcePath))
                    {
                        File.Delete(xSourcePath);
                    }
                }
                catch (IOException)
                {
                    //Word 2003 may not release file handle after OrganizerCopy action
                    //File will be left in temp directory
                    OS.AddFileToDeletionList(xSourcePath);
                }

                this.lblProgress.Visible = false;
            }
        }
        private void CopyStylesToTarget(string xSourcePath, string xTargetPath, string[] aStyles)
        {
            bool bRSID = Session.CurrentWordApp.Options.StoreRSIDOnSave;
            Session.CurrentWordApp.Options.StoreRSIDOnSave = false;
            foreach (string xStyle in aStyles)
            {
                //Copy specified styles individually
                Word.WdOrganizerObject oObjectType =
                    Word.WdOrganizerObject.wdOrganizerObjectStyles;

                try
                {
                    Session.CurrentWordApp.OrganizerCopy(xSourcePath,
                        xTargetPath, xStyle, oObjectType);
                }
                catch { }
            }
            Session.CurrentWordApp.Options.StoreRSIDOnSave = bRSID;
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// Dropdown selection has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCopyTo_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!m_bSegmentsLoaded)
                {
                    ftvSegments.ExecuteFinalSetup();
                    m_bSegmentsLoaded = true;
                }
                if (cmbCopyTo.SelectedValue != null && (Int32.Parse(cmbCopyTo.SelectedValue.ToString()) == (int)mpDestinations.SelectedSegments))
                {
                    ftvSegments.Enabled = true;
                }
                else
                {
                    ftvSegments.Enabled = false;
                }
            }
            catch(System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Check all items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lbStyles.Items.Count; i++)
                lbStyles.SetItemChecked(i, true);
        }
        /// <summary>
        /// Uncheck all items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeselectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lbStyles.Items.Count; i++)
                lbStyles.SetItemChecked(i, false);
        }
        /// <summary>
        /// Ok button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbStyles.CheckedItems.Count == 0)
                {
                    MessageBox.Show("No Styles selected", LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                else if ((Int32.Parse(cmbCopyTo.SelectedValue.ToString()) ==
                    (int)mpDestinations.SelectedSegments) && ftvSegments.Value == "")
                {
                    MessageBox.Show("No Segments selected", LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                ProcessSelectedStyles();
                this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void StylesManager_Shown(object sender, EventArgs e)
        {
            try
            {
                if (m_bOutlineStylesOmitted)
                {
                    //Notify user that some styles were omitted
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_StylesManagerOutlineStylesOmitted"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

   }
}