using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using LMP.Data;
using LMP.Administration.Controls; 

namespace LMP.MacPac
{
    internal partial class PeopleManager : Form
    {
        #region ******************fields*******************
        private AttorneyLicenses m_oLicenses;
        private Jurisdictions m_oJurisdictions0 = new Jurisdictions(Jurisdictions.Levels.Zero);
        private Jurisdictions m_oJurisdictions1 = new Jurisdictions(Jurisdictions.Levels.One);
        private Jurisdictions m_oJurisdictions2 = new Jurisdictions(Jurisdictions.Levels.Two);
        private Jurisdictions m_oJurisdictions3 = new Jurisdictions(Jurisdictions.Levels.Three);
        private Jurisdictions m_oJurisdictions4 = new Jurisdictions(Jurisdictions.Levels.Four);
        private LocalPersons m_oDisplayPeople;
        private Offices m_oAllOffices;
        private BindingSource m_oPeopleSource;
        private BindingSource m_oPropSource;
        private DataRow m_oLastPeopleRow = null;
        private DataRow m_oLastPropRow = null;
        private Hashtable m_oLevelIDs = null;
        private bool m_bValidate = true;
        int m_iRowIndexOfIsAttorney = -1;
        int m_iRowIndexOfIsAuthor = -1;
        DataTable m_oBooleanComboValuesTable = null;
        DataTable m_oBooleanComboYesTable = null;
        private string m_xGrdLicensesBeforeEditCellValue;
        private bool m_bLicenseChangesMade = false;
        private bool m_bCurrentLicenseRowIsNewRecord = false;
        private bool m_bGrdLicensesIsBound = false;
        private Timer m_oSearchTimer = new Timer();
        private string m_xSearchString = "";

        #endregion
        #region ******************constructors*****************
        public PeopleManager()
        {
            InitializeComponent();
        }
        #endregion
        #region ******************properties*****************
        #endregion
        #region ******************methods********************
        /// <summary>
        /// Bind the appropriate people to grdPeople
        /// </summary>
        private void DataBindGridPeople()
        {
            //Get a persons object consisting of public people in
            //the office of the current user or private records belonging
            //to the current user -
            //GLOG item #4247
            m_oDisplayPeople = new LocalPersons(mpPeopleListTypes.AllPeople, UsageStates.OfficeActive);

            //create bindingsource object
            m_oPeopleSource = new BindingSource();

            //Bind a datatable of the people to lstPeople
            //GLOG : 7754 : JSW
            //include office name
            DataTable oDT = m_oDisplayPeople.ToDisplayFieldsDataSet(true).Tables[0];

            grdPeople.DataSource = null;

            //bind datatable to binding source
            m_oPeopleSource.DataSource = oDT;

            //bind bindingsource to grid
            grdPeople.DataSource = m_oPeopleSource;

            //Set the display properties of grdPeople
            grdPeople.Columns["ID"].Visible = false;
            grdPeople.Columns["DisplayName"].AutoSizeMode =
                DataGridViewAutoSizeColumnMode.Fill;

            //Set the current row of grdPeople to the first record
            this.grdPeople.CurrentCell = this.grdPeople.Rows[0].Cells["DisplayName"];

            RefreshGrids(m_oPeopleSource);

            //subscribe to positionchanged event - this will handle updates/refresh
            //of property grid and provide row state info through bindingsource.current
            m_oPeopleSource.PositionChanged += new EventHandler(m_oPeopleSource_PositionChanged);

        }
        /// <summary>
        /// Bind the properties and property names of the 
        /// currently selected person to grdProperties
        /// </summary>
        private void DataBindGridProperties(string xID)
        {
            try
            {
                Person oCurPerson;

                //Get the currently selected person record
                oCurPerson = m_oDisplayPeople.ItemFromID(xID);

                //display person type
                string xType = "Public";

                if (oCurPerson.IsAlias)
                    xType = "Alias";
                else if (oCurPerson.IsPrivate)
                    xType = "Private";

                lblPersonType.Text = xType;

                if (xType == "Alias" || xType == "Private")
                    this.lblPersonType.ForeColor = Color.Red;
                else
                    this.lblPersonType.ForeColor = Color.Black;
                
                object[,] aProperties = oCurPerson.ToArray();

                //transfer properties to hashtable -- this way we can 
                //control order of their presentation in the properties grid
                Hashtable oProperties = CollectionsUtil.CreateCaseInsensitiveHashtable();

                for (int i = 0; i <= aProperties.GetUpperBound(0); i++)
                {
                    //add data type name to hashtable - convert null to "String"
                    string[] aValues = new string[2];
                    aValues[1] = aProperties[i, 1] == null ? "String" : 
                        aProperties[i, 1].GetType().Name.ToString();
                    
                    //get property value - convert null to string
                    aValues[0] = aProperties[i, 1] == null ? "" : 
                        aProperties[i, 1].ToString();
                    
                    //get property data type name
                    oProperties.Add(aProperties[i, 0], aValues);
                }

                //Create a DataTable to populate with the property names 
                //and values
                DataTable oDT = new DataTable();
                oDT.Columns.Add("Property");
                oDT.Columns.Add("Value");
                oDT.Columns.Add("DataType");

                //create storage for possible hashtable keys
                //their order determines order in property grid -- some are custom,
                //so we need to test whether they actually exist, i.e, have been
                //returned by oPerson.ToArray(), before attempting to
                //add rows to the grid
                //office, shortname, admitted in, and Email have different
                //display names than property names
                string[,] xKeys = { {"Prefix", "Prefix"},
                                    {"First Name", "First Name"},
                                    {"Middle Name", "Middle Name"},
                                    {"Last Name", "Last Name"},
                                    {"Suffix", "Suffix"},
                                    {"Display Name", "Display Name"},
                                    {"Full Name", "Full Name"},
                                    {"Short Name", "ShortName"},
                                    {"Initials", "Initials"},
                                    {"Title", "Title"},
                                    {"Office", "Office ID"},
                                    {"Phone", "Phone"},
                                    {"Fax", "Fax"},
                                    {"Email", "EMail"},
                                    {"Is Author", "Is Author"},
                                    {"Is Attorney", "Is Attorney"},
                                    {"Admitted In", "AdmittedIn"},
                                    {"ID", "ID"},
                                    {"System User ID", "System User ID"},
                                    {"Owner ID", "Owner ID"},
                                    {"Linked Person ID", "Linked Person ID"},
                                    {"Office Usage State", "OfficeUsageState"},
                                    {"Default Office Record ID1", "DefaultOfficeRecordID1"}};

                //add rows to datatable only if hashtable item exists
                for (int i = 0; i < xKeys.Length / 2; i++)
                {
                    if (oProperties.ContainsKey(xKeys[i, 1]))
                    {
                        //extract value/data type name array from hashtable
                        string[] aValues = (string[])oProperties[xKeys[i, 1]];
                        //add name, value, and data type name to datatable
                        oDT.Rows.Add(xKeys[i, 0], aValues[0], aValues[1]);
                        //remove from hashtable once added
                        oProperties.Remove(xKeys[i, 1]);
                    }
                }

                //Add rows for additional custom properties set up by admin
                //they will be any items remaining in the hashtable
                ICollection oKeys = oProperties.Keys;
                foreach (string xKey in oKeys)
                {
                    //extract value/data type name array from hashtable
                    string[] aValues = (string[])oProperties[xKey];
                    //add name, value, and data type name to datatable
                    oDT.Rows.Add(xKey, aValues[0], aValues[1]);
                }
                
                oDT.AcceptChanges();

                m_oPropSource = new BindingSource();

                //unsubscribe to any existing event handler
                m_oPropSource.PositionChanged -= m_oPropSource_PositionChanged;

                //subscribe to position changed handler - this will handle updates to record
                m_oPropSource.PositionChanged += new EventHandler(m_oPropSource_PositionChanged);
                m_oPropSource.DataSource = oDT;

                oDT.RowChanged += new DataRowChangeEventHandler(oDT_RowChanged);

                //Bind the binding source to grdProperties
                this.grdProperties.DataSource = m_oPropSource;

                //Set the display properties of the gird
                this.grdProperties.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                this.grdProperties.Columns[0].FillWeight = 65;
                this.grdProperties.Columns[0].ReadOnly = true;
                this.grdProperties.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //hide the data type column
                this.grdProperties.Columns["DataType"].Visible = false;

                //If this is a public record set the 
                //properties to Read Only
                if (oCurPerson.IsPrivate || oCurPerson.IsAlias)
                    this.grdProperties.Columns[1].ReadOnly = false;
                else
                    this.grdProperties.Columns[1].ReadOnly = true;

                //hide various non-editable properties
                HideNonEditableFields();

                //create and configure office dropdown lookup
                CreateOfficeCombo();
                
                //create boolean combo lookups
                CreateBooleanCombo();

                // Set the IsAttorney IsAuthor relationship.
                EnforceIsAttorneyIsAuthorRelation();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.
                    GetLangString("Error_CouldNotDataBindGrdProperties"), oE);
            }
        }

        /// <summary>
        /// Retrieve the DataTable that will be used as the source for BooleanCombo. This
        /// table provides both the "Yes" and "No" option.
        /// </summary>
        private DataTable BooleanComboValuesTable
        {
            get
            {
                if (m_oBooleanComboValuesTable == null)
                {
                    m_oBooleanComboValuesTable = new DataTable();

                    DataColumn oValCol = m_oBooleanComboValuesTable.Columns.Add("Value");
                    DataColumn oDisplayValCol = m_oBooleanComboValuesTable.Columns.Add("DisplayText");

                    DataRow oYesRow = m_oBooleanComboValuesTable.Rows.Add();

                    oYesRow["Value"] = "True";
                    oYesRow["DisplayText"] = "Yes";

                    DataRow oNoRow = m_oBooleanComboValuesTable.Rows.Add();
                    oNoRow["Value"] = "False";
                    oNoRow["DisplayText"] = "No";
                }

                return m_oBooleanComboValuesTable;
            }
        }

        /// <summary>
        /// Retrieve the DataTable that will be used as the source for BooleanCombo. This
        /// table provides only the "Yes" option.
        /// </summary>
        private DataTable BooleanComboYesTable
        {
            get
            {
                if (m_oBooleanComboYesTable == null)
                {
                    m_oBooleanComboYesTable = new DataTable();

                    DataColumn oValCol = m_oBooleanComboYesTable.Columns.Add("Value");
                    DataColumn oDisplayValCol = m_oBooleanComboYesTable.Columns.Add("DisplayText");

                    DataRow oYesRow = m_oBooleanComboYesTable.Rows.Add();

                    oYesRow["Value"] = "True";
                    oYesRow["DisplayText"] = "Yes";
                }

                return m_oBooleanComboYesTable;
            }
        }


        /// <summary>
        /// configures boolean lookup cell for properties grid
        /// </summary>
        private void CreateBooleanCombo()
        {
            //change all boolean type cells to combo lookups
            for (int i = 0; i < grdProperties.Rows.Count; i++)
            {
                if (grdProperties.Rows[i].Cells[2].FormattedValue.ToString() == "Boolean")
                {
                    DataGridViewComboBoxCell oDGVComboBoxCell = new DataGridViewComboBoxCell();
                    this.grdProperties.Rows[i].Cells[1] = oDGVComboBoxCell;

                    //set the data source to lookup array
                    ((DataGridViewComboBoxCell)this.grdProperties.Rows[i].Cells[1]).
                        DataSource = this.BooleanComboValuesTable;
                    oDGVComboBoxCell.DisplayMember = "DisplayText";
                    oDGVComboBoxCell.ValueMember = "Value";

                    // Store the IsAttorney and IsAuthor row indices to later be used
                    // when enforcing the IsAttorney IsAuthor relation.
                    if (m_iRowIndexOfIsAuthor == -1 && grdProperties.Rows[i].Cells[0].FormattedValue.ToString() == "Is Author")
                    {
                        m_iRowIndexOfIsAuthor = i;
                    }
                    else
                    {
                        if (m_iRowIndexOfIsAttorney == -1 && grdProperties.Rows[i].Cells[0].FormattedValue.ToString() == "Is Attorney")
                        {
                            m_iRowIndexOfIsAttorney = i;
                        }
                    }
                }
            }

            EnforceIsAttorneyIsAuthorRelation();
        }

        void oDT_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            //JTS 3/21/10: Missing error handling
            try
            {
                // If the changing row is the row of the IsAttorney field.
                if (e.Row.Table.Rows.IndexOf(e.Row) == m_iRowIndexOfIsAttorney)
                {
                    EnforceIsAttorneyIsAuthorRelation();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void EnforceIsAttorneyIsAuthorRelation()
        {
            DataTable oDT = (DataTable)m_oPropSource.DataSource;

            if (oDT != null && m_iRowIndexOfIsAttorney != -1 && m_iRowIndexOfIsAuthor != -1)
            {
                DataGridViewComboBoxCell oDGVComboBoxCell = (DataGridViewComboBoxCell)this.grdProperties.Rows[m_iRowIndexOfIsAuthor].Cells[1];

                // Set IsAuthor = true whenever IsAttorney is set to true.
                if (((string)(oDT.Rows[m_iRowIndexOfIsAttorney][1])).ToUpper() == "TRUE")
                {
                    oDT.Rows[m_iRowIndexOfIsAuthor][1] = (string)(oDT.Rows[m_iRowIndexOfIsAttorney][1]);

                    // Only display the "Yes" option.
                    oDGVComboBoxCell.DataSource = this.BooleanComboYesTable;
                }
                else
                {
                    // Give the option for either "Yes" or "No".
                    oDGVComboBoxCell.DataSource = this.BooleanComboValuesTable;
                }

                oDGVComboBoxCell.DisplayMember = "DisplayText";
                oDGVComboBoxCell.ValueMember = "Value";
            }
        }
        /// <summary>
        /// hides display of various properties in properties grid
        /// </summary>
        private void HideNonEditableFields()
        {
            for (int i = 0; i < grdProperties.Rows.Count; i++)
            {
                switch (grdProperties.Rows[i].Cells[0].FormattedValue.ToString())
                {
                    case "ID":
                    case "Linked Person ID":
                    case "System User ID":
                    case "Owner ID":
                    case "Office Usage State":
                    case "Default Office Record ID1":
                        grdProperties.Rows[i].Visible = false;
                        break;
                }
            }
        }
        /// <summary>
        /// converts grdProperties Office cell to dropdown combo
        /// </summary>
        private void CreateOfficeCombo()
        {
            int iOffRow = 0;

            //find the index of the row containing the office property data
            for (int i = 0; i < grdProperties.Rows.Count; i++)
            {
                if (grdProperties.Rows[i].Cells[0]
                    .FormattedValue.ToString() == "Office")
                {
                    iOffRow = i;
                    break;
                }
            }

            //configure the cell as a dataview combobox cell
            this.grdProperties.Rows[iOffRow].Cells[1] = new DataGridViewComboBoxCell();
            DataGridViewComboBoxCell oCell = ((DataGridViewComboBoxCell)this.grdProperties.Rows[iOffRow].Cells[1]);

            //create dropdown storage
            DataTable oDT = new DataTable();
            oDT.Columns.Add("DisplayName");
            oDT.Columns.Add("ID");

            //create an array of the office names/ids to be bound to the office combo
            m_oAllOffices = new Offices();
            Array xOffices = m_oAllOffices.ToArray(0, 4);

            for (int i = 0; i < xOffices.GetLength(0); i++)
            {
                object[] oValues = (object[])xOffices.GetValue(i);
                oDT.Rows.Add(oValues[1].ToString(), oValues[0].ToString());
            }

            //Set the datasource and value/display members of lookup
            oCell.DataSource = oDT;
            oCell.DisplayMember = "DisplayName";
            oCell.ValueMember = "ID";
        }
        /// <summary>
        /// Display the licenses corresponding to the 
        /// person record that is selected in grdLicenses
        /// </summary>
        public void DataBindGridLicenses(string xID)
        {

            //Note that grdLicenses is in the process of being bound
            m_bGrdLicensesIsBound = false;

            if (System.String.IsNullOrEmpty(xID))
            {
                //User has entered the row for new records, clear grdLicenses
                this.grdLicenses.DataSource = null;
                this.grdLicenses.Rows.Clear();

                //Keep the user from entering invalid data
                this.grdLicenses.ReadOnly = true;

                return;
            }
            Person oPerson = m_oDisplayPeople.ItemFromID(xID);

            //GLOG 4380: If Linked Person, Licenses are attached to Base record
            if (oPerson.BasePerson.IsAttorney)
            {
                // Add the tabLicense tabPage since it may have been removed for non-lawyers.

                if (!tabPeople.TabPages.Contains(tabLicenses))
                {
                    this.tabPeople.TabPages.Add(tabLicenses);
                }

                //Clear any records already in the grid
                this.grdLicenses.DataSource = null;
                this.grdLicenses.Rows.Clear();

                //display license for attorneys; allow input for
                //private or alias persons
                if (oPerson.IsPrivate)
                    this.grdLicenses.ReadOnly = !oPerson.IsAttorney;
                else
                    this.grdLicenses.ReadOnly = true;

                //disable delete button for non-attorneys
                this.tbtnDeleteLicense.Enabled = !grdLicenses.ReadOnly;

                //GLOG 4380: Use BasePerson ID to get Licenses collection
                m_oLicenses = new AttorneyLicenses(oPerson.BasePerson.ID);
                AttorneyLicense oLicense = null;

                //array for grdLicenses rows
                object[] aLicenseProperties = new object[8];

                m_oLevelIDs = new Hashtable();

                //storage with properties of the current AttorneyLicense
                for (int i = 1; i <= m_oLicenses.Count; i++)
                {
                    oLicense = (AttorneyLicense)m_oLicenses.ItemFromIndex(i);
                    aLicenseProperties[0] = oLicense.Description;
                    aLicenseProperties[1] = oLicense.License;
                    aLicenseProperties[2] = (oLicense.L0 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions0.ItemFromID(oLicense.L0)).Name;
                    aLicenseProperties[3] = (oLicense.L1 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions1.ItemFromID(oLicense.L1)).Name;
                    aLicenseProperties[4] = (oLicense.L2 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions2.ItemFromID(oLicense.L2)).Name;
                    aLicenseProperties[5] = (oLicense.L3 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions3.ItemFromID(oLicense.L3)).Name;
                    aLicenseProperties[6] = (oLicense.L4 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions4.ItemFromID(oLicense.L4)).Name;
                    aLicenseProperties[7] = oLicense.ID;

                    //Add the row to the datatable 
                    this.grdLicenses.Rows.Add(aLicenseProperties);

                    //Store the IDs of the Jurisdictions in hashtable
                    //grid will display jurisdiction name
                    int[] iLevels = {   oLicense.L0,
                                        oLicense.L1,
                                        oLicense.L2,
                                        oLicense.L3,
                                        oLicense.L4};

                    string xKey = "License" + i.ToString();
                    m_oLevelIDs.Add(xKey, iLevels);
                }

                //do not allow new row / user imput for public or alias records
                grdLicenses.AllowUserToAddRows = oPerson.IsPrivate;

                //read only columns for jurisdiction
                for (int f = 0; f <= 6; f++)
                {
                    if (f < 2 && oPerson.IsPrivate)
                        this.grdLicenses.Columns[f].ReadOnly = false;
                    else
                        this.grdLicenses.Columns[f].ReadOnly = true;
                }

                //GLOG : 8532 : ceh
                //set column attributes
                grdLicenses.Columns["ID"].Visible = false;
                grdLicenses.Columns["Description"].MinimumWidth = (int)(150 * Session.ScreenScalingFactor);
                grdLicenses.Columns["Level0"].HeaderText = "Level 0";
                grdLicenses.Columns["Level0"].MinimumWidth = (int)(100 * Session.ScreenScalingFactor);
                grdLicenses.Columns["Level1"].HeaderText = "Level 1";
                grdLicenses.Columns["Level1"].MinimumWidth = (int)(100 * Session.ScreenScalingFactor);
                grdLicenses.Columns["Level2"].HeaderText = "Level 2";
                grdLicenses.Columns["Level2"].MinimumWidth = (int)(100 * Session.ScreenScalingFactor);
                grdLicenses.Columns["Level3"].HeaderText = "Level 3";
                grdLicenses.Columns["Level3"].MinimumWidth = (int)(100 * Session.ScreenScalingFactor);
                grdLicenses.Columns["Level4"].HeaderText = "Level 4";
                grdLicenses.Columns["Level4"].MinimumWidth = (int)(100 * Session.ScreenScalingFactor);

                //freeze description column
                grdLicenses.Columns["Description"].Frozen = true;
                
                //Note that grdLicenses is now bound
                m_bGrdLicensesIsBound = true;
            }
            else
            {

                //User is not an attorney, clear grdLicenses
                this.grdLicenses.DataSource = null;
                this.grdLicenses.Rows.Clear();

                //User has selected a non-attorney while grdLicenses
                //is displayed, change the selected tab in tabPersonalDetail
                //to a display relevant data
                if (this.tabPeople.SelectedIndex == 1)
                    this.tabPeople.SelectedIndex = 0;

                // Disable the License tab.
                this.tabPeople.TabPages.Remove(tabLicenses);
            }

        }
        /// <summary>
        /// Create a new person record with the display name "New Person" 
        /// and an OfficeID equal to the current user
        /// </summary>
        private void AddNewPerson()
        {
            LMP.Data.FirmApplicationSettings m_oFirmAppSettings = new FirmApplicationSettings(Session.CurrentUser.ID);

            //Create a new person.
            m_oDisplayPeople = new LocalPersons(mpPeopleListTypes.AllPeople);

            if (grdPeople.RowCount >= m_oFirmAppSettings.MaxPeople)
            {
                MessageBox.Show(LMP.Resources.GetLangString("PromptMaxPeopleLimitReached"), LMP.String.MacPacProductName);
                return;
            }

            if (grdPeople.RowCount >= m_oFirmAppSettings.PeopleListWarningThreshold)
            {
                MessageBox.Show(LMP.Resources.GetLangString("PromptPeopleListWarningThresholdReached"), LMP.String.MacPacProductName);
            }
            

            Person oNewPerson = m_oDisplayPeople.Create();


            //Set the name, officeID and ownerID of the new person record
            oNewPerson.DisplayName = "New Person";
            oNewPerson.OfficeID = Session.CurrentUser.Office.ID;
            oNewPerson.OwnerID = Session.CurrentUser.ID;
            oNewPerson.UsageState = 1;
            oNewPerson.IsAuthor = true;

            try
            {
                //Save the new person record to the DB
                m_oDisplayPeople.Save(oNewPerson);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB") , oE);
            }

            //Create an array of the display values of the new 
            //person
            object [] aDisplayProperties = new object [2];
            aDisplayProperties[0] = oNewPerson.DisplayName;
            aDisplayProperties[1] = oNewPerson.ID;

            //Add a row representing the new person to grdPeople's DataSource
            ((DataTable)m_oPeopleSource.DataSource).Rows.Add(aDisplayProperties);

            //navigate to the new row
            grdPeople.CurrentCell = grdPeople.Rows[grdPeople.RowCount - 1].Cells["DisplayName"];
            
            //bind license grid
            DataBindGridLicenses(oNewPerson.ID);

            //set focus on properties grid for new input
            grdProperties.CurrentCell = grdProperties.Rows[0].Cells["Value"];
            grdProperties.Focus();
        }
        /// <summary>
        /// Create and display in the grid a new alias record
        /// of the currently selected person
        /// </summary>
        public void AddNewAlias()
        {
            //Get the currently selected person record
            Person oCurPerson = m_oDisplayPeople.ItemFromID(this.grdPeople.
                CurrentRow.Cells["ID"].FormattedValue.ToString());

            string xMsg = "";

            if (oCurPerson.IsPrivate)
            {
                //disallow creation of alias from alias record
                xMsg = LMP.Resources.
                    GetLangString("Msg_CannotCreateAliasOfPrivatePerson");
            }
            else if (oCurPerson.IsAlias)
            {
                //disallow creation of alias from alias record
                xMsg = LMP.Resources.
                    GetLangString("Msg_CannotCreateAliasOfAliasPerson");
            }
            else
            {
                //Create a new person record for the new alias
                Person oNewAlias = m_oDisplayPeople.Create();

                //Set the properties of the new alias that 
                //distinguish it as a private alias
                oNewAlias.OwnerID = Session.CurrentUser.ID;
                char[] aStringSpliter = new char[1];
                aStringSpliter[0] = '.';
                oNewAlias.LinkedPersonID = Int32.Parse(
                    oCurPerson.ID.Split(aStringSpliter)[0]);
                oNewAlias.SystemUserID = "";

                //Set the properties of the new alias record to
                //be unassigned- assign them a value indicating this
                oNewAlias.NamePrefix = oCurPerson.NamePrefix;
                oNewAlias.DisplayName = oCurPerson.DisplayName;
                oNewAlias.FullName = oCurPerson.FullName;
                oNewAlias.FirstName = oCurPerson.FirstName;
                oNewAlias.MiddleName = oCurPerson.MiddleName;
                oNewAlias.LastName = oCurPerson.LastName;
                oNewAlias.NameSuffix = oCurPerson.NameSuffix;
                //Set the values of IsAuthor, IsAttorney, and OfficeID 
                //equal to the values of the orginal person
                oNewAlias.IsAuthor = oCurPerson.IsAuthor;
                oNewAlias.IsAttorney = oCurPerson.IsAttorney;
                oNewAlias.OfficeID = oCurPerson.OfficeID;
                oNewAlias.UsageState = 1;

                DataGridViewRowCollection oRows = this.grdProperties.Rows;
                bool bIsAttorneyUpdated = false;
                SetCustomProperties(oNewAlias, oRows, true, null, out bIsAttorneyUpdated);

                try
                {
                    //Save the alias to the DB
                    m_oDisplayPeople.Save(oNewAlias);
                }

                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
                }

                //Create an array of the new aliases display values to
                //set to grdPeople
                object[] aNewAliasDisplayValues = new object[2];

                aNewAliasDisplayValues[0] = oCurPerson.DisplayName;
                aNewAliasDisplayValues[1] = oNewAlias.ID;

                //Add a row for the new alias to the DataSet grdPeople is bound to
                ((DataTable)m_oPeopleSource.DataSource).Rows.Add(aNewAliasDisplayValues);

                //navigate to the new row
                grdPeople.CurrentCell = grdPeople.Rows[grdPeople.RowCount - 1].Cells["DisplayName"];

                //set focus on properties grid for new input
                grdProperties.CurrentCell = grdProperties.Rows[0].Cells["Value"];
                grdProperties.Focus();
            }

            //message user that there was an invalid attempt to add alias
            //from either private or alias record
            if (xMsg != "")
                MessageBox.Show(xMsg,LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        /// <summary>
        /// Delete the currently selected person record 
        /// iff the record is a private record
        /// </summary>
        private void DeletePerson()
        {
            //Get current person record.
            Person oCurPerson = m_oDisplayPeople.ItemFromID(this.
                grdPeople.CurrentRow.Cells["ID"].FormattedValue.ToString());

            //GLOG item #4247 - dcf - added condition not to allow deletion
            //if the current person is linked to the person logged in for this session
            Person oBasePerson = oCurPerson.BasePerson;

            //GLOG 4646 (JTS 3/21/10): Modified to allow deleting Alias for self
            if (oBasePerson.ID1 == Session.LoginUser.ID && (oCurPerson.ID2 == 0 || oCurPerson.ID2 == -999999999))
            {
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_CannotRemoveSelf"), LMP.String.MacPacProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            //GLOG 5614: Proxiable User ID might correspond to Default Office Record
            //GLOG 5706: Don't apply this to private people, since CanProxyFor will return true for self
            else if ((oCurPerson.ID2 == 0 || oCurPerson.ID2 == -999999999)  && Session.CurrentUser.CanProxyFor(oCurPerson.BasePerson.ID1))
            {
                MessageBox.Show(LMP.Resources.GetLangString("Prompt_CannotRemoveProxy"), LMP.String.MacPacProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string xMsg = null;

            if (oCurPerson.IsPrivate || oCurPerson.IsAlias)
            {
                //Confirm that the user intends to delete this record.
                xMsg = LMP.Resources.GetLangString("Message_DeleteEntry");
            }
            else
            {
                //Confirm that the user intends to remove this record.
                xMsg = LMP.Resources.GetLangString("Message_RemoveFirmPersonFromAuthorList");
            }

            DialogResult oChoice = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //User confirmed delete- remove either primary or secondary record
            if (oChoice == DialogResult.Yes)
            {
                //GLOG 4647 (JTS 3/21/10): If Public Author has multiple offices, deleting any individual entry will remove all
                if (!oCurPerson.IsBasePerson && (oCurPerson.ID2 == 0 || oCurPerson.ID2 == -999999999))
                    oCurPerson = oBasePerson;

                //attempt to delete the record
                try
                {
                    //GLOG 5721: If Base Person is not an Active Office, it won't
                    //be included in m_oDisplayPeople.  Create LocalPersons object
                    //containing all active records to delete from
                    LocalPersons oAllPeople = new LocalPersons();
                    //Delete the current person from the DB
                    oAllPeople.Delete(oCurPerson.ID, oCurPerson.IsBasePerson);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
                }

                int iCurRowIndex = this.grdPeople.CurrentRow.Index;
                int iCurCellIndex = this.grdPeople.CurrentCell.ColumnIndex;
                
                grdPeople.SuspendLayout();
                try
                {

                    DataBindGridPeople();
                    //Make sure Grid Redraws completely
                    grdPeople.Refresh();
                    System.Windows.Forms.Application.DoEvents();
                }
                finally
                {
                    grdPeople.ResumeLayout();
                }

                if (grdPeople.Rows.Count > 0)
                {
                    grdPeople.Rows[Math.Min(iCurRowIndex, this.grdPeople.Rows.Count - 1)]
                        .Cells[iCurCellIndex].Selected = true;

                    string xID = grdPeople.CurrentRow.Cells["ID"].FormattedValue.ToString();
                    DataBindGridProperties(xID);

                    // GLOG : 2972 : JAB
                    // Have the license grid show the corresponding info for
                    // the current person.
                    DataBindGridLicenses(xID);
                }
            }
        }
        /// <summary>
        /// Deletes the license record the user has chosen for deletion
        /// </summary>
        private void DeleteLicense()
        {
            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                //Test to make sure the user is deleting a valid license
                if (this.grdLicenses.CurrentRow.Cells["ID"].FormattedValue.ToString() == "")
                {
                    StringBuilder oSB = new StringBuilder();
                    for (int i = 0; i < 7; i++)
                        oSB.Append(grdLicenses.CurrentRow.Cells[i].EditedFormattedValue.ToString());

                    if (oSB.ToString().Length > 0)
                    {
                        grdLicenses.Rows.RemoveAt(grdLicenses.CurrentRow.Index);
                        //reset current row unless last license has been deleted
                        if (grdLicenses.CurrentRow != null)
                            grdLicenses.CurrentCell = grdLicenses.CurrentRow.Cells[0];
                        else
                        {
                            EnablePeopleTypeControls(true); 
                        }
                    }
                    return;
                }
                
                //User has confirmed the delete, delete the record
                DataGridViewRow oCurRow = this.grdLicenses.CurrentRow;

                //Get the license to be deleted
                AttorneyLicense oCurLicense = (AttorneyLicense)m_oLicenses.
                    ItemFromID(oCurRow.Cells["ID"].FormattedValue.ToString());

                //Delete the license
                m_oLicenses.Delete(oCurLicense.ID);
                
                //Remove the row for the deleted license from the grid
                this.grdLicenses.Rows.Remove(oCurRow);

                //reset global datarow variable, last record has been deleted
                if (grdLicenses.Rows.Count == 1)
                {
                    EnablePeopleTypeControls(true); 
                }
            }
        }
        /// <summary>
        /// validates certain items of property grid
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Update the property that the user is editing- 
        /// </summary>
        /// <returns></returns>
        private void UpdatePerson()
        {
            string xPersonID = this.grdPeople.CurrentRow.Cells["ID"].FormattedValue.ToString();

            if(string.IsNullOrEmpty(xPersonID))
            {
                // The person id may be empty if the current row is that of a previously deleted person.
                return;
            }
            //get current person
            Person oCurPerson = m_oDisplayPeople.ItemFromID(xPersonID);

            //get properter name from grid column 0
            DataGridViewRowCollection oRows = this.grdProperties.Rows;
            string xPropName = oRows[grdProperties.CurrentRow.Index].Cells[0].FormattedValue.ToString();

            //construct displayname, fullname, and initials,
            //update grid fields only if current people
            //record is new, ie. rowstate == added, and we have not
            //explicitly edited the display name field
            if (grdProperties.CurrentRow.Index == 5)
                m_oLastPeopleRow.AcceptChanges();

            bool bIsAttorneyUpdated = false;
            if (m_oLastPeopleRow.RowState == DataRowState.Added)
            {
                UpdateCalculatedFields();
                
                //update properties belonging to calculated fields
                //update DisplayName only if the new value is not null
                string xDisplayName = oRows[5].Cells[1].FormattedValue.ToString();
                if (xDisplayName != "")
                    oCurPerson.DisplayName = xDisplayName;

                //Update FullName
                oCurPerson.FullName = oRows[6].Cells[1].FormattedValue.ToString();

                //update initials, which is a Forte custom property
                SetCustomProperties(oCurPerson, oRows, false,
                                    "Initials", out bIsAttorneyUpdated);
            }
            else
            {
                //Update DisplayName only if the new value
                //is not null
                if (xPropName == "Display Name" && oRows[5].Cells[1].FormattedValue.ToString() != "")
                    oCurPerson.DisplayName = oRows[5].Cells[1].FormattedValue.ToString();

                //Update FullName
                if (xPropName == "Full Name")
                    oCurPerson.FullName = oRows[6].Cells[1].FormattedValue.ToString();

            }

            //set custom properties - both Forte custom properties plus
            //any additional custom properties defined by administrator
            SetCustomProperties(oCurPerson, oRows, false,
                                xPropName, out bIsAttorneyUpdated);
            
            //set values for hard coded standard Forte person properties only if changed
            //Update NamePrefix
            if (xPropName == "Prefix")
                oCurPerson.NamePrefix = oRows[0].Cells[1].FormattedValue.ToString();

            //Update FirstName
            if (xPropName == "First Name")
                oCurPerson.FirstName = oRows[1].Cells[1].FormattedValue.ToString();

            //Update MiddleName
            if (xPropName == "Middle Name")
                oCurPerson.MiddleName = oRows[2].Cells[1].FormattedValue.ToString();

            //Update LastName
            if (xPropName == "Last Name")
                oCurPerson.LastName = oRows[3].Cells[1].FormattedValue.ToString();

            //Update NameSuffix
            if (xPropName == "Suffix")
                oCurPerson.NameSuffix = oRows[4].Cells[1].FormattedValue.ToString();

            try
            {
                //Update the DB
                m_oDisplayPeople.Save(oCurPerson);

                //accept changes for global data row variable
                //this will reset row state
                m_oLastPropRow.AcceptChanges();

                //Set the display name in grdPeople if the
                //user has changed it
                if (this.grdPeople.CurrentRow.Cells["DisplayName"].
                    FormattedValue.ToString() != oCurPerson.DisplayName)
                    this.grdPeople.CurrentRow.Cells[0].Value = oCurPerson.DisplayName;

                //data bind License grid
                if(oCurPerson.IsAttorney)
                    DataBindGridLicenses(oCurPerson.ID); 
            }
            catch(System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// sets value of specified custom property
        /// of specified data type
        /// </summary>
        /// <param name="oPerson"></param>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        /// <param name="xDataType"></param>
        private void SetPersonCustomProperty(LMP.Data.Person oPerson, string xName,
                                          string xValue, string xDataType)
        {
            if (xDataType == "Boolean")
            {
                bool bValue = (xValue.ToUpper() == "TRUE");
                oPerson.SetPropertyValue(xName, bValue);
            }
            else
                oPerson.SetPropertyValue(xName, xValue);
        }

        /// <summary>
        /// refreshes the grids based on changes to the binding source
        /// </summary>
        /// <param name="oBindingSource"></param>
        private void RefreshGrids(BindingSource oBindingSource)
        {
            if (this.grdPeople.DataSource == null || this.grdPeople.CurrentRow == null)
                return;

            if (m_oLastPeopleRow == null)
                m_oLastPeopleRow = ((DataRowView)oBindingSource.Current).Row;

            if (grdPeople.CurrentRow == null)
                return;

            //commit any pending property changes
            if (m_oLastPropRow != null)
            {
                grdProperties.EndEdit();
                m_oPropSource.EndEdit();

                if (m_oLastPropRow.RowState == DataRowState.Modified)
                    UpdatePerson();

                //reset row state of previous row if necessary
                if (m_oLastPeopleRow.RowState == DataRowState.Added)
                    m_oLastPeopleRow.AcceptChanges();
            }


            //reset last row variable
            if (oBindingSource.Current != null)
                m_oLastPeopleRow = ((DataRowView)oBindingSource.Current).Row;

            //rebind properties grid
            if (oBindingSource.Position > -1)
            {
                //freeze the window
                LMP.WinAPI.LockWindowUpdate(this.Handle.ToInt32());

                //get the current person ID
                string xID = grdPeople.Rows[oBindingSource.Position].Cells["ID"].FormattedValue.ToString();

                //bind property grids
                DataBindGridProperties(xID);
                DataBindGridLicenses(xID);

                //unfreeze the window
                LMP.WinAPI.LockWindowUpdate(0);
            }

            //GLOG : 8333 : jsw
            //highlight cell for current user
            for (int i = 0; i < grdPeople.Rows.Count; i++)
            {
                string xiD = (string)grdPeople.Rows[i].Cells[1].Value;
                int iPos = xiD.IndexOf(".");
                //xiD = xiD.Substring(0, iPos);
                if (xiD == Session.CurrentUser.ID.ToString() + ".0")
                {
                    grdPeople.Rows[i].DefaultCellStyle.BackColor = Color.Yellow;
                    break;
                }
            }
        }
        /// <summary>
        /// Update the DB with the changes the user has just made
        /// to the current License record
        /// </summary>
        private void UpdateLicense()
        {
            if (m_oLicenses == null)
                m_oLicenses = new AttorneyLicenses();

            if (this.grdLicenses.CurrentRow != null)
            {

                DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

                //Get the license record that the user is editing.
                AttorneyLicense oCurLicense = (AttorneyLicense)m_oLicenses.
                    ItemFromIndex(grdLicenses.CurrentRow.Index + 1);

                //Update the properties of the current License record to match the new data
                oCurLicense.Description = oCurCells[0].EditedFormattedValue.ToString();
                oCurLicense.License = oCurCells[1].EditedFormattedValue.ToString();

                //get the current license level IDs from the hashtable
                string xKey = "License" + (grdLicenses.CurrentRow.Index + 1).ToString();
                int[] iIDs = (int[])m_oLevelIDs[xKey];

                oCurLicense.L0 = iIDs[0];
                oCurLicense.L1 = iIDs[1];
                oCurLicense.L2 = iIDs[2];
                oCurLicense.L3 = iIDs[3];
                oCurLicense.L4 = iIDs[4];

                try
                {
                    //Save the altered license record
                    m_oLicenses.Save(oCurLicense);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
                }
            }
        }
        /// <summary>
        /// Creates a new license record with the data the user has provided
        /// </summary>
        private void AddLicense()
        {
            string xCurPersonID = this.grdPeople.CurrentRow.Cells[1].FormattedValue.ToString();

            if (m_oLicenses == null)
                m_oLicenses = new AttorneyLicenses();

            //Create a new license
            AttorneyLicense oNewLicense = (AttorneyLicense)m_oLicenses.Create
                (this.grdPeople.CurrentRow.Cells[1].FormattedValue.ToString());

            //Get the current cells with data for the new license
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            //Set the description for the new license from user input
            oNewLicense.Description = oCurCells[0].EditedFormattedValue.ToString();

            //Set the license name for the new license from user input
            oNewLicense.License = oCurCells[1].EditedFormattedValue.ToString();

            //get the current license level IDs from the hashtable
            string xKey = "License" + (grdLicenses.CurrentRow.Index + 1).ToString();
            int[] iIDs = (int[])m_oLevelIDs[xKey];

            oNewLicense.L0 = iIDs[0];
            oNewLicense.L1 = iIDs[1];
            oNewLicense.L2 = iIDs[2];
            oNewLicense.L3 = iIDs[3];
            oNewLicense.L4 = iIDs[4];

            try
            {
                //Save the new license
                m_oLicenses.Save(oNewLicense);

                //Store the ID of the new license in the grid
                oCurCells[7].Value = oNewLicense.ID;

                //commit the change
                grdLicenses.EndEdit();

                //reset new record flag
                m_bCurrentLicenseRowIsNewRecord = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE); //display a message box instead and make the user handle it?
            }
        }
        /// <summary>
        /// Displays a dialog box in which the user can edit an existing
        /// license or enter data for a new one.
        /// </summary>
        private void ShowSetLicenseJurisdictionDialog()
        {
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            //Create a new Dialog
            SetLicenseJurisdictionDialog oJDialog = new SetLicenseJurisdictionDialog();

            //Get the level of the deepest jurisdiction level that is defined
            int iCurLevelDepth = 0;

            for (int i = 2; i <= 6; i++)
                if (oCurCells[i].FormattedValue.ToString() != "")
                    iCurLevelDepth = i - 2;
            
            //retrieve the jurisdiction levels for the currently selected license row
            //they have been stored in a hashtable
            string xKey = "License" + (grdLicenses.CurrentRow.Index + 1).ToString();
            int[] iLevels = (int[])m_oLevelIDs[xKey];

            if (iCurLevelDepth != 0)
                //There is a jurisdiction defined for this record
                //Set the jurisdiction that will be selected in the dialog
                oJDialog.Value = iCurLevelDepth.ToString() + "."
                    + iLevels[iCurLevelDepth].ToString();

            DialogResult oResult = oJDialog.ShowDialog();

            if (oResult == DialogResult.OK)
            {
                //User did not cancel the dialog, insert the data they entered into the grid
                this.GetJurisdictionsData(oJDialog);

                if (this.ValidateLicenseRow())
                {
                    this.EnablePeopleTypeControls(true);
                }
                else
                    //we have an uncommitted edit now, so disable navigation
                    //it will be reenabled when row is validated
                    this.EnablePeopleTypeControls(false);
            }
        }
        /// <summary>
        /// enables/
        /// </summary>
        /// <param name="p"></param>
        private void EnablePeopleTypeControls(bool bEnabled)
        {
            grdPeople.Enabled = bEnabled;
            this.btnDelete.Enabled = bEnabled;
            this.btnDelete.Enabled = bEnabled;
            this.btnNew.Enabled = bEnabled;
            this.btnImport.Enabled = bEnabled;
            this.btnNewAlias.Enabled = bEnabled;
        }
        /// <summary>
        /// Inserts the data that the user has entered in the SetLicenseJurisdictionDialog
        /// into the grid and updates the DB
        /// </summary>
        private void GetJurisdictionsData(SetLicenseJurisdictionDialog oJD)
        {
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            //get selected jurisdiction IDs
            int[] iJurisdictions = oJD.Jurisdictions;
            
            //store IDs in hashtable
            string xKey = "License" + (grdLicenses.CurrentRow.Index + 1).ToString();
            m_oLevelIDs[xKey] = iJurisdictions;

            //fill grdJurisdiction cells
            for (int i = 0; i < iJurisdictions.Length; i++)
            {
                int iInd = i + 2;
                string xValue = "";
                int iJID = iJurisdictions[i];
                switch (i)
                {
                    case 0:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions0.ItemFromID(iJID)).Name;
                        break;
                    case 1:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions1.ItemFromID(iJID)).Name;
                        break;
                    case 2:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions2.ItemFromID(iJID)).Name;
                        break;
                    case 3:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions3.ItemFromID(iJID)).Name;
                        break;
                    case 4:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions4.ItemFromID(iJID)).Name;
                        break;
                    default:
                        break;
                }

                oCurCells[iInd].Value = xValue;
            }
        }
        /// <summary>
        /// builds DisplayName, FullName, and Initials based
        /// on input, fills grid values
        /// </summary>
        private void UpdateCalculatedFields()
        {
            DataGridViewRowCollection oRows = this.grdProperties.Rows;

            //get NamePrefix
            string xPrefix = oRows[0].Cells[1].FormattedValue.ToString();
            xPrefix = xPrefix == "" ? xPrefix : xPrefix + " ";

            //get FirstName
            string xFirstName = oRows[1].Cells[1].FormattedValue.ToString();
            xFirstName = xFirstName == "" ? xFirstName : xFirstName + " ";
            string xFirstInitial = xFirstName.Substring(0, Math.Min(1, xFirstName.Length));

            //get MiddleName
            string xMiddleName = oRows[2].Cells[1].FormattedValue.ToString();
            xMiddleName = xMiddleName == "" ? xMiddleName : xMiddleName + " ";
            string xMiddleInitial = "";
            if (xMiddleName != "")
            {
                //GLOG item #6000 - dcf
                xMiddleName = xMiddleName.Trim('"');
                xMiddleInitial = xMiddleName.Substring(0, 1) + ". ";
            }

            //get LastName
            string xLastName = oRows[3].Cells[1].FormattedValue.ToString();
            string xLastInitial = xLastName.Substring(0, Math.Min(1, xLastName.Length));

            //get NameSuffix
            string xNameSuffix = oRows[4].Cells[1].FormattedValue.ToString();
            xNameSuffix = xNameSuffix == "" ? xNameSuffix : ", " + xNameSuffix;

            //build FullName
            StringBuilder oSB = new StringBuilder();
            string xFullName = oSB.AppendFormat("{0}{1}{2}{3}{4}",
                xPrefix, xFirstName, xMiddleInitial, xLastName, xNameSuffix).ToString();

            //build Initials
            oSB = new StringBuilder();
            string xInitials = oSB.AppendFormat("{0}{1}{2}",
                xFirstInitial, xMiddleInitial.Replace(". ", ""), xLastInitial).ToString();

            //build DisplayName
            oSB = new StringBuilder();
            xNameSuffix = xNameSuffix == "" ? xNameSuffix : " " + xNameSuffix;
            string xDisplayName = oSB.AppendFormat("{0}{1}, {2}",
                xLastName, xNameSuffix.Replace(", ", ""), xFirstName.Replace(" ", "")).ToString();
            
            //if user has entered nothing in the name fields, don't recalculate
            if (xInitials == "")
                return;
            
            //edit grid cells with new values
            //set display name
            oRows[5].Cells[1].Value = xDisplayName;
            oRows[6].Cells[1].Value = xFullName;
            oRows[8].Cells[1].Value = xInitials;

            //commit the edits
            grdProperties.EndEdit();
            m_oPropSource.EndEdit();



        }

        /// <summary>
        /// Select the first record in grdPeople matching
        /// the key entered by the user
        /// </summary>
        private void SelectMatchingRecord(string xSearchString)
        {

            if (xSearchString != "")
            {
                //User has entered a search string

                //Get the index of the first matching record
                int iMatchIndex = this.grdPeople.FindRecord
                    (xSearchString, this.grdPeople.Columns[0].Name, false);

                if (iMatchIndex > -1)
                {
                    //Match found

                    //Select the appropriate record
                    this.grdPeople.CurrentCell =
                        this.grdPeople.Rows[iMatchIndex].Cells[0];
                }
            }
        }

        #endregion
        #region******************private functions****************
        /// <summary>
        /// validation for people grid values
        /// </summary>
        /// <returns></returns>
        private bool ValidatePersonDisplayName()
        {
            if (grdPeople.CurrentCell.FormattedValue.ToString() == "")
            {
                //alert user
                MessageBox.Show(LMP.Resources.GetLangString("Error_EmptyValueNotAllowed"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            else
                return true;
        }
        private bool ValidatePersonRecord()
        {
            //get current person
            Person oDirtyPerson = m_oDisplayPeople.ItemFromID(this.
                   grdPeople.CurrentRow.Cells["ID"].FormattedValue.ToString());

            //Store the name of the dirty property
            string xDirtyPropertyName = grdProperties.CurrentRow.Cells[0].FormattedValue.ToString();

            //Store the value of the currently dirty propterty
            string xDirtyPropertyNewValue = grdProperties.CurrentRow.Cells[1].EditedFormattedValue.ToString();

            if (xDirtyPropertyName == "Display Name" && xDirtyPropertyNewValue == "")
            {
                //User is attemting to edit a person records display name
                //to be null - alert user that this is invalid and 
                //do not update

                MessageBox.Show(LMP.Resources.
                    GetLangString("Msg_PersonCannotHaveBlankDisplayName"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }

            else if (xDirtyPropertyName == "Is Attorney" && oDirtyPerson.IsAlias)
            {
                //User is attempting to change the Is Attorney value
                //of an alias- disallow and alert user

                grdProperties.CancelEdit();

                MessageBox.Show(LMP.Resources.
                    GetLangString("Msg_CannotChangeIsAttorneyValueOfAlias"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }

            //record is valid
            return true;
        }
        /// <summary>
        /// Tests to make sure the user is not leaving required data blank
        /// in edditing or creating a license
        /// </summary>
        /// <returns></returns>
        private bool ValidateLicenseRow()
        {
            if (grdLicenses.CurrentRow == null)
                return true;

            //Get the current row
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            //all the cells are empty - no edits made so row is valid
            if (oCurCells["Description"].EditedFormattedValue.ToString() == "" &&
                oCurCells["Number"].EditedFormattedValue.ToString() == "" &&
                oCurCells["Level0"].EditedFormattedValue.ToString() == "")
                return true;

            if (oCurCells["Description"].EditedFormattedValue.ToString() == "" ||
                oCurCells["Number"].EditedFormattedValue.ToString() == "" ||
                oCurCells["Level0"].EditedFormattedValue.ToString() == "")
            {
                //one or more required fields are blank, the row is invalid
                //Message user
                MessageBox.Show(LMP.Resources.GetLangString
                    ("Error_LicenseMustHaveDescriptionNumberAndLevel0Val"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            else
            {
                //The required fields are set, row is valid
                return true;
            }
        }
        /// <summary>
        /// sets both Forte custom props and user defined custom props
        /// sets props to undefined value string if alias, sets specified
        /// property if xPropToSet is specified
        /// </summary>
        /// <param name="oCurPerson"></param>
        /// <param name="bIsAlias"></param>
        private void SetCustomProperties(Person oCurPerson, DataGridViewRowCollection oRows, 
                        bool bIsNewAlias, string xPropToSet, out bool bIsAttorneyUpdated)
        {
            bIsAttorneyUpdated = false;
            
            //build storage for Forte custom props -- datatable will contain display name
            //which is in some cases different from the actual property name
            string[,] aTemp = { {"Short Name", "ShortName"},
                                {"Initials", "Initials"},
                                {"Title", "Title"},
                                {"Phone", "Phone"},
                                {"Fax", "Fax"},
                                {"Email", "EMail"},
                                {"Admitted In", "AdmittedIn"},
                                {"Is Attorney", "Is Attorney"},
                                {"Is Author", "Is Author"},
                                {"Office", "OfficeID"} };

            // Add the Person custom fields to the array of Forte custom props.
            // In order to avoid duplicating properties build a hash of the field
            // and only add the Person properties that are not listed as Forte
            // custom props.

            Hashtable oPropsHash = new Hashtable();

            // Populate the hash with the Forte custom props.
            for (int i = 0; i < aTemp.GetLength(0); i++)
            {
                oPropsHash.Add(aTemp[i, 1], aTemp[i, 0]);
            }

            // Add the Person properties that are not listed as Forte
            // custom props.
            foreach (string xPropName in oCurPerson.Properties.Keys)
            {
                if (!oPropsHash.ContainsKey(xPropName))
                {
                    oPropsHash.Add(xPropName, xPropName);
                }
            }

            // Create an array of the appropriate size for the properties.
            aTemp = new string[oPropsHash.Count, 2];

            int iPropIndex = 0;

            // Populate the array of properties with the complete list of Forte custom 
            // properties and Person properties.
            foreach (string xPropName in oPropsHash.Keys)
            {
                aTemp[iPropIndex, 0] = (string)oPropsHash[xPropName];
                aTemp[iPropIndex, 1] = xPropName;
                iPropIndex++;
            }
            
            string[,] aCustomProps = {{"", ""}};

            //if a property name was specified and person is alias, restrict storage
            //to only that property; if the name is the name of 
            //a admin-defined custom property, the storage array will 
            //contain one record with empty values
            if (xPropToSet == null || !oCurPerson.IsAlias)
                aCustomProps = (string[,])aTemp.Clone();

            
            for (int i = 0; i < aTemp.GetLength(0); i++)
            {
                //set all custom prop values if person is not alias
                if (!oCurPerson.IsAlias)
                {
                    aCustomProps[i, 0] = aTemp[i, 0];
                    aCustomProps[i, 1] = aTemp[i, 1];
                }
                else
                {
                    if (xPropToSet == aTemp[i, 0])
                    {
                        aCustomProps[0, 0] = aTemp[i, 0];
                        aCustomProps[0, 1] = aTemp[i, 1];
                        break;
                    }
                }
            }
     
            
            //custom properties may not exist - attempt to select items
            //from properties grid binding source, then set person props accordingly
            DataTable oDT = (DataTable)((BindingSource)grdProperties.DataSource).DataSource;
            DataRow[] oDR = null;

            int iOffset = 0;
            for (int i = 0; i <= aCustomProps.GetUpperBound(0); i++)
            {
                //select datarow from datatable using property name as shown in grid
                oDR = oDT.Select("Property = '" + aCustomProps[i, 0] + "'");

                //if row exists set property value - get name, value and type from
                //datarow
                if (oDR.Length > 0)
                {
                    //check for Is Attorney and Is Author -- these are not
                    //custom props, but have been presented in the grid below 
                    //Forte custom props, so their row number can't be known if 
                    //Forte custom items have been omitted by admin
                    string xProp = aCustomProps[i, 1];

                    string xValue = "";
                    string xType = ((DataRow)oDR.GetValue(0)).ItemArray.GetValue(2).ToString();

                    xValue = ((DataRow)oDR.GetValue(0)).ItemArray.GetValue(1).ToString();

                    if (xProp == "Is Attorney")
                    {
                        oCurPerson.IsAttorney = (xValue.ToUpper() == "TRUE");
                        bIsAttorneyUpdated = true;
                    }
                    else if (xProp == "Is Author")
                        oCurPerson.IsAuthor = (xValue.ToUpper() == "TRUE");
                    else if (xProp == "OfficeID")
                        oCurPerson.OfficeID = Int32.Parse(xValue);
                    else
                        SetPersonCustomProperty(oCurPerson, xProp, xValue, xType);
                }
                else
                {
                    //prop does not exist in the grid because macpac custom property
                    //has not been implemented by administrator - increment an offset count;
                    //if custom prop storage is empty, we're dealing with an admin custom prop
                    //so do not increment offset - we will set values for these props below.
                    if (aCustomProps[i, 0] != "")
                        iOffset++;
                }
            }

            //exit if merely setting up an alias
            if (bIsNewAlias)
                return;

            //Update any other custom properties set by admin
            //these are stored in additional rows set up when
            //the properties grid was bound
            //get field count of 7 standard mp fields, plus 
            //count of existing mpCustom fields, plus 4 hidden fields
            //minus offset of any Forte custom fields not implemented by admin
            int iCount = aTemp.GetLength(0) - iOffset + 7 + 4;

            for (int i = iCount; i < oRows.Count; i++)
            {
                string xValue = oRows[i].Cells[1].FormattedValue.ToString();
                string xProp = oRows[i].Cells[0].FormattedValue.ToString();
                string xType = oRows[i].Cells[2].FormattedValue.ToString();
                
                //set only the specified property
                if (xPropToSet == xProp)
                    SetPersonCustomProperty(oCurPerson, xProp, xValue, xType);
            }

            return;
        }
        #endregion
        #region ******************event handlers*******************
        private void PeopleManager_Load(object sender, EventArgs e)
        {
            try
            {
                this.DataBindGridPeople();

                LMP.Data.FirmApplicationSettings m_oFirmAppSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                
                if (grdPeople.Rows.Count > 0)
                {
                    //bind all the various grids
                    string xID = grdPeople.Rows[0].Cells["ID"].FormattedValue.ToString();
                    DataBindGridProperties(xID);
                    DataBindGridLicenses(xID);
                }

                // GLOG : 2973: JAB 
                // Control the display of the Import, Proxies & New Person buttons
                //glog - 3637 - CEH
                this.btnImport.Visible = !Session.AdminMode;
                this.btnProxies.Visible = (!Session.AdminMode) && (m_oFirmAppSettings.AllowProxying); 
                this.btnNew.Visible = m_oFirmAppSettings.AllowPrivatePeople;
                this.btnNewAlias.Visible = m_oFirmAppSettings.AllowPrivatePeople;

                //Arrange buttons
                if (!m_oFirmAppSettings.AllowPrivatePeople && !this.btnImport.Visible)
                {
                    this.btnDelete.Left = this.btnImport.Left;
                    this.btnProxies.Left = this.btnDelete.Left + 93;
                }
                else if (!m_oFirmAppSettings.AllowPrivatePeople)
                {
                    this.btnDelete.Left = this.btnNew.Left;
                    this.btnProxies.Left = this.btnDelete.Left + 93;
                }
                else if (Session.AdminMode)
                {
                    this.btnNew.Left = this.btnNew.Left - 93;
                    this.btnNewAlias.Left = this.btnNewAlias.Left - 93;
                    this.btnDelete.Left = this.btnDelete.Left - 93;
                }
                else
                {

                }
                m_oSearchTimer.Tick += m_oSearchTimer_Tick;
                m_oSearchTimer.Interval = 300;
                m_oSearchTimer.Stop();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void m_oSearchTimer_Tick(object sender, EventArgs e)
        {
            //Clear search string after time out
            m_xSearchString = "";
            m_oSearchTimer.Stop();
        }
        private void grdPeople_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //Update the person record that the 
                //user has just finished editing
                if (!ValidatePersonDisplayName())
                {
                    e.Cancel = true;
                    grdPeople.CancelEdit();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdPeople_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                if (e.Exception is IndexOutOfRangeException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeletePerson();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddNewPerson();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnNewAlias_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddNewAlias();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdProperties_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //If there has been an edit made save the new property value
                if (e.FormattedValue.ToString() !=
                        grdProperties.Rows[e.RowIndex].Cells[e.ColumnIndex]
                        .FormattedValue.ToString())
                {
                    if (!ValidatePersonRecord())
                        //record is not valid - cancel selection change
                        e.Cancel = true;
                    else
                    {
                        //special handling if IsAttorney flipped
                        //this will save change and permit immediate
                        //access to license grid
                        if (grdProperties.Rows[e.RowIndex].Cells[0]
                            .FormattedValue.ToString() == "Is Attorney" &&
                            e.FormattedValue.ToString() == "True")
                            UpdatePerson();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdProperties_CellEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (grdProperties.Focused)
                {
                    if (grdProperties.CurrentCellAddress.X == 0 &&
                        grdProperties.CurrentCellAddress.Y == 0)
                        return;

                    //programmatically resetting current cell causes
                    //reentrantcy threading error
                    //we can use sendkeys instead
                    if (e.ColumnIndex == 0)
                        System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdProperties_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                if (e.Exception is IndexOutOfRangeException)
                    return;
                else if (e.Exception is ArgumentException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                //GLOG : 2973 : JAB
                // Show the ImportedPeopleManager control in a form.
                ImportPeopleForm oForm = new ImportPeopleForm();
                oForm.ShowDialog(this);

                DataBindGridPeople();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handler will disable licence tab if person is not attorney
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabPeople_Selecting(object sender, TabControlCancelEventArgs e)
        {
            try
            {
                if (e.TabPageIndex == 0)
                    return;
                else if (e.TabPageIndex == 1 && grdPeople.CurrentRow == null)
                {
                    e.Cancel = true;
                    return;
                }

                string xID = grdPeople.CurrentRow.Cells["ID"].FormattedValue.ToString();
                Person oPerson = m_oDisplayPeople.ItemFromID(xID);

                e.Cancel = !oPerson.IsAttorney;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles updates to property records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oPropSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //get current BindingSource datarow object
                BindingSource oBS = (BindingSource)sender;

                if (m_oLastPropRow == null)
                    m_oLastPropRow = ((DataRowView)oBS.Current).Row;

                //update format if datarow has been modified
                if (m_oLastPropRow.RowState == DataRowState.Modified)
                {
                    this.UpdatePerson();
                }

                //reset last row variable
                if (oBS.Current != null)
                    m_oLastPropRow = ((DataRowView)oBS.Current).Row;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles data binding to property and license grids
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oPeopleSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshGrids((BindingSource)sender);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tabPeople_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tabPeople.SelectedIndex == 1)
                {
                    //update format if datarow has been modified
                    if (m_oLastPropRow.RowState == DataRowState.Modified)
                    {
                        this.UpdatePerson();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdLicenses_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex > 1 && !grdLicenses.CurrentRow.IsNewRow &&
                    !grdLicenses.ReadOnly)
                    this.ShowSetLicenseJurisdictionDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (!this.grdLicenses.CurrentRow.IsNewRow && !this.CurLicenseRowIsValid())
                {
                    //invalid row - cancel validation and warn user
                    //grdLicenses.CancelEdit();
                    e.Cancel = true;

                    MessageBox.Show(
                        LMP.Resources.GetLangString("Error_LicenseMustHaveDescriptionNumberAndLevel0Val"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// disable navigation while there is a pending edit in grdLicenses
        /// this is due to bug where e.cancel = true in the RowValidating event
        /// handler causes app to hang if people grid is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdLicenses_CurrentCellDirtyStateChanged(object sender, System.EventArgs e)
        {
            try
            {
                //GLOG #4155 - dcf
                //EnablePeopleTypeControls(false);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles deletion of license records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnDeleteLicense_Click(object sender, EventArgs e)
        {
            try
            {
                //Test to make sure the user is deleting a valid license
                if (this.grdLicenses.CurrentRow.Cells[7].FormattedValue.ToString() != "")
                {
                    this.DeleteLicense();
                    return;
                }
                StringBuilder oSB = new StringBuilder();
                for (int i = 0; i < 7; i++)
                {
                    oSB.Append(grdLicenses.CurrentRow.Cells[i].FormattedValue.ToString());
                }
                if (oSB.ToString().Length > 0)
                {
                    grdLicenses.CancelEdit();
                    //GLOG 6253: Move CurrentCell first, to ensure Jurisdiction Chooser isn't displayed when row changes
                    grdLicenses.CurrentCell = grdLicenses.CurrentRow.Cells[0];
                    grdLicenses.Rows.RemoveAt(grdLicenses.CurrentRow.Index);
                    m_bLicenseChangesMade = false;
                    m_bCurrentLicenseRowIsNewRecord = false;    //GLOG 5837: Reset New Record Flag
                    EnablePeopleTypeControls(true); //glog 6253: Re-enable navigation
                }
                EnablePeopleTypeControls(true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handles any concommitted edits
        /// at close of form -- prevents close
        /// if partial or invalid edits exist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PeopleManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (!ValidateLicenseRow())
                {
                    e.Cancel = true;
                    return;
                }

                //commit any pending property edits and save if necessary
                grdProperties.EndEdit();
                m_oPropSource.EndEdit();

                if (m_oLastPropRow.RowState == DataRowState.Modified)
                {
                    UpdateCalculatedFields();
                    UpdatePerson();
                }
                
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdLicenses_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bLicenseChangesMade)
                {
                    //User has edited the current license, update the DB

                    if (m_bCurrentLicenseRowIsNewRecord)
                    {
                        //The user is adding a new license record, update the DB
                        this.AddLicense();
                        m_bCurrentLicenseRowIsNewRecord = false;
                    }

                    else
                        this.UpdateLicense();

                    m_bLicenseChangesMade = false;

                    EnablePeopleTypeControls(true);
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                //Note that the user has added a row for a new record
                m_bCurrentLicenseRowIsNewRecord = true;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // GLOG : 2973: JAB 
        // Show the display of ProxiesManager buttons.
        private void btnProxies_Click(object sender, EventArgs e)
        {
            try
            {
                ProxiesForm oForm = new ProxiesForm();
                oForm.ShowDialog(this);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion       

        //GLOG #3937 - ceh
        private void grdPeople_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Tab:
                    case Keys.Enter:
                    case Keys.Down:
                    case Keys.Left:
                    case Keys.Up:
                    case Keys.Right:
                        break;
                    default:
                        string xKey = e.KeyCode.ToString();
                        //Ignore non-input keys
                        if (xKey.Length == 1)
                        {
                            m_oSearchTimer.Stop();
                            m_oSearchTimer.Start();
                            m_xSearchString += xKey;
                            //Select the first record matching
                            //the search string entered by the user
                            this.SelectMatchingRecord(m_xSearchString);
                            e.Handled = true;
                        }
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdLicenses_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //Store the current value of the cell being edited so it can be used after 
                //the new value is assigned - ignore if we're dealing with levels cells
                //glogal before edit value will be stored prior to selection of new item
                //in locations dialog
                if (e.ColumnIndex < 2)
                    m_xGrdLicensesBeforeEditCellValue = grdLicenses.CurrentCell.FormattedValue.ToString();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdLicenses_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (!this.grdLicenses.CurrentRow.IsNewRow && m_xGrdLicensesBeforeEditCellValue !=
                        this.grdLicenses.CurrentCell.FormattedValue.ToString())
                    //Edits made to the current license, set flag
                    m_bLicenseChangesMade = true;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Tests to make sure the user is not leaving required data blank
        /// in edditing or creating a license
        /// </summary>
        /// <returns></returns>
        private bool CurLicenseRowIsValid()
        {
            //Get the current row
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            if (oCurCells[0].EditedFormattedValue.ToString() == "" ||
                oCurCells[1].EditedFormattedValue.ToString() == "" ||
                oCurCells[2].EditedFormattedValue.ToString() == "")
            {
                //Required fields are blank, the row is invalid
                return false;
            }
            else
            {
                //The required fields are set, row is valid
                return true;
            }
        }

    }
}