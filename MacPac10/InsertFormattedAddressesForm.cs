using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;
using TSG.CI;   //GLOG : 8819 : ceh


namespace LMP.MacPac
{
    internal partial class InsertFormattedAddressesForm : Form
    {
        public enum mpAddressFormats{
            List=1,
            Table=2
        }

        #region ****************fields********************
        //introduce a data table for drop down values
        DataTable m_dtValues = null;

        //Fixed Separator values
        string[,] m_oSeparators = new string[,] {   {"Paragraph", ((char)13).ToString()},
                                                    {"Soft Return", ((char)11).ToString()},
                                                    {"Comma + Space", ", "}};
        //Fixed column values
        string[,] m_oColumns = new string[,] {   {"1", "1"},
                                                   {"2", "2"},
                                                   {"3", "3"},
                                                   {"4", "4"},
                                                   {"5", "5"},
                                                   {"6", "6"}};

        #endregion
        #region**********************constructors*******************
        public InsertFormattedAddressesForm()
        {
            InitializeComponent();
            InitializeApplicationContent();
        }
        #endregion
        #region************************events***********************
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                // Do nothing and close the dialog
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();

                string xSep = "";
                int iColCount = 0;
                bool bPhone = false;
                bool bFax = false;
                bool bEmail = false;
                mpAddressFormats oAddressFormat = mpAddressFormats.List;
                Word.Selection oSel = null;


                if (this.radList.Checked)
                {
                    xSep = (string)this.cmbSeparators.SelectedValue;
                    iColCount = 0;
                }
                else
                {
                    xSep = ((char)11).ToString();
                    iColCount = Int32.Parse(this.cmbColumns.SelectedValue.ToString());
                }

                if (iColCount != 0)
                    oAddressFormat = mpAddressFormats.Table;

                bPhone = this.chkPhone.Checked;
                bFax = this.chkFax.Checked;
                bEmail = this.chkEMail.Checked;

                //setup CI
                Data.Application.StartCIIfNecessary();

                //get position to "park" CI dialog
                System.Drawing.Point oCoords = LMP.Data.Application.User.UserSettings.CIDlgLocation;
                float x = (float)oCoords.X;
                float y = (float)oCoords.Y;

                //set CI dialog position
                ICSession oCI = Data.Application.CISession;
                oCI.FormLeft = x;
                oCI.FormTop = y;

                ICContacts oContacts = null;

                //Retrieve Contacts
                oContacts = oCI.GetContactsEx(ciRetrieveData.ciRetrieveData_All,
                    ciRetrieveData.ciRetrieveData_None, ciRetrieveData.ciRetrieveData_None,
                    ciRetrieveData.ciRetrieveData_None,
                    ICI.ciSelectionlists.ciSelectionList_To, 0, ciAlerts.ciAlert_NoAddresses,
                    "Selected Contacts", "", "", "",
                    0, 0, 0, 0);

                //save CI dialog coordinates for next use
                oCoords.X = (int)oCI.FormLeft;
                oCoords.Y = (int)oCI.FormTop;

                LMP.Data.Application.User.UserSettings.CIDlgLocation = oCoords;

                //build address string
                if ((oContacts != null) && (oContacts.Count() > 0))
                {
                    System.Text.StringBuilder oSB = new System.Text.StringBuilder();

                    for (int i = 1; i <= oContacts.Count(); i++)
                    {
                        //GLOG : 8031 : ceh
                        object o = i;
                        string xDetail = "";
                        string xTemplate = "";
                        ICContact oContact = oContacts.Item(o);

                        //GLOG 4825: Don't duplicate Company if same as FullName
                        if (oContact.Company != "" && oContact.FullName == oContact.Company)
                        {
                            xTemplate = "<COMPANY>" + xSep + "<COREADDRESS>";
                        }
                        else
                        {
                            if (oContact.FullName != "")
                            {
                                xTemplate = "<FULLNAMEWITHPREFIXANDSUFFIX>" + xSep;
                            }
                            if (oContact.Title != "")
                            {
                                xTemplate = xTemplate + "<TITLE>" + xSep;
                            }
                            if (oContact.Company != "")
                            {
                                xTemplate = xTemplate + "<COMPANY>" + xSep;
                            }
                            xTemplate = xTemplate + "<COREADDRESS>";
                        }
                        //GLOG : 8031 : ceh
                        //fill in CoreAddress token first
                        xDetail = xTemplate.Replace("<COREADDRESS>", LMP.Data.Application.CleanCoreAddress(oContact.Country, oContact.CoreAddress));
                        //fill in other details
                        xDetail = oContact.GetDetail(xDetail, true);

                        xDetail = xDetail.Replace("\r\n", xSep);

                        //get label formatting for Firm Application Settings
                        LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);

                        //add phone/fax/email if necessary
                        if (bPhone)
                            xDetail = xDetail + xSep + 
                                     oFirmSettings.AdvancedAddressbookFormatPhonePrefix + 
                                     (string)oContacts.Item(o).Phone;
                        if (bFax)
                            xDetail = xDetail + xSep + 
                                     oFirmSettings.AdvancedAddressbookFormatFaxPrefix + 
                                     (string)oContacts.Item(o).Fax;
                        if (bEmail)
                            xDetail = xDetail + xSep + 
                                     oFirmSettings.AdvancedAddressbookFormatEmailPrefix + 
                                     (string)oContacts.Item(o).EmailAddress;

                        if (oAddressFormat == mpAddressFormats.Table)
                            oSB.AppendFormat("{0}\r\n", xDetail);
                        else
                            oSB.AppendFormat("{0}\r\n\r\n", xDetail);
                    }

                    //get selection & insert
                    oSel = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection;

                    oSel.InsertAfter(oSB.ToString());

                    if (oAddressFormat == mpAddressFormats.Table)
                    //convert to table
                    {
                        //GLOG : 7096 : CEH
                        oSel.ParagraphFormat.SpaceAfter = 0;
                        oSel.ParagraphFormat.SpaceBefore = 12;
                        oSel.ParagraphFormat.LineSpacingRule = Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceSingle;

                        object iRows = 0;
                        object iCols = iColCount;
                        object xTableSep = ((char)13).ToString();
                        object missing = System.Reflection.Missing.Value;
                        //GLOG : 7689 : ceh
                        bool bResetStyles = false;

                        oSel.ConvertToTable(ref missing, ref missing, ref iCols,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing);

                        LMP.MacPac.Application.ScreenUpdating = false;
                        LMP.Forte.MSWord.WordDoc.AdjustTableMargins(oSel.Range, bResetStyles);
                        LMP.MacPac.Application.ScreenUpdating = true;
                    }

                    object oMissing = System.Reflection.Missing.Value;
                    oSel.EndOf(ref oMissing, ref oMissing);

                }

                //if (oContacts != null)
                //    System.Runtime.InteropServices.Marshal.ReleaseComObject((object)oContacts);

                oContacts = null;

                System.Windows.Forms.Application.DoEvents();

                // Set focus back to the document after inserting the address.
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

            // Close the dialog
            this.Close();
        }

        #endregion
        #region************************properties*******************
        #endregion
        #region************************methods*********************
        private void InitializeApplicationContent()
        {
            // Set the data source for form drop downs
            this.cmbSeparators.DataSource = GetDataTable(m_oSeparators);
            this.cmbSeparators.DisplayMember = "Name";
            this.cmbSeparators.ValueMember = "Value";

            this.cmbColumns.DataSource = GetDataTable(m_oColumns);
            this.cmbColumns.DisplayMember = "Name";
            this.cmbColumns.ValueMember = "Value";

            // Select defaults
            this.cmbSeparators.SelectedIndex = 1;
            this.cmbColumns.SelectedIndex = 1;

            // Localize the form.
            LocalizeForm();
        }

        // Create data source for form drop downs
        private DataTable GetDataTable(string[,] oValues)
        {
            m_dtValues = new DataTable();

            DataColumn oNameCol = new DataColumn("Name", typeof(string));
            m_dtValues.Columns.Add(oNameCol);

            DataColumn oValueCol = new DataColumn("Value", typeof(string));
            m_dtValues.Columns.Add(oValueCol);

            // Populate the table with the values.
            for (int i = 0; i < oValues.GetLength(0); i++)
            {
                this.m_dtValues.Rows.Add(oValues[i, 0], oValues[i, 1]);
            }
            return m_dtValues;
        }

        private void LocalizeForm()
        {
            this.lblFormat.Text = LMP.Resources.GetLangString("FormattedAddressFormFormat");
            this.lblInclude.Text = LMP.Resources.GetLangString("FormattedAddressFormInclude");
            this.radList.Text = LMP.Resources.GetLangString("FormattedAddressFormList");
            this.radTable.Text = LMP.Resources.GetLangString("FormattedAddressFormTable");
            this.lblColumns.Text = LMP.Resources.GetLangString("FormattedAddressFormColumns");
            this.chkEMail.Text = LMP.Resources.GetLangString("FormattedAddressFormEmail");
            this.chkFax.Text = LMP.Resources.GetLangString("FormattedAddressFormFax");
            this.chkPhone.Text = LMP.Resources.GetLangString("FormattedAddressFormPhone");
            this.btnOK.Text = LMP.Resources.GetLangString("FormattedAddressFormOK");
            this.btnCancel.Text = LMP.Resources.GetLangString("FormattedAddressFormCancel");
            this.Text = LMP.Resources.GetLangString("FormattedAddressFormInsertFormattedAddresses");
        }

        #endregion

        


    }
}            