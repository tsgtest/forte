﻿namespace LMP.MacPac
{
    partial class MultipleSegmentsManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Insertion Location", -1, 1013179772);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("In Separate Section");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Restart Page Numbering", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, false);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Keep Headers/ Footers");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueList valueList1 = new Infragistics.Win.ValueList(1013179772);
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultipleSegmentsManager));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblSelectedSegments = new System.Windows.Forms.Label();
            this.btnAddSegment = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnSavedData = new System.Windows.Forms.Button();
            this.ftvSegments = new LMP.Controls.SearchableFolderTreeView();
            this.ftvMain = new LMP.Controls.FolderTreeView(this.components);
            this.lblSavedData = new System.Windows.Forms.Label();
            this.chkSaveSegments = new System.Windows.Forms.CheckBox();
            this.chkDeautomateSegments = new System.Windows.Forms.CheckBox();
            this.grdSelSegs = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnDeleteSegment = new System.Windows.Forms.Button();
            this.btnMoveSegmentDown = new System.Windows.Forms.Button();
            this.btnMoveSegmentUp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ftvSegments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ftvMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSelSegs)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(9, 146);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(156, 25);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Sa&ve As Segment Packet";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(433, 146);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(352, 146);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblSelectedSegments
            // 
            this.lblSelectedSegments.AutoSize = true;
            this.lblSelectedSegments.BackColor = System.Drawing.Color.Transparent;
            this.lblSelectedSegments.Location = new System.Drawing.Point(9, 16);
            this.lblSelectedSegments.Name = "lblSelectedSegments";
            this.lblSelectedSegments.Size = new System.Drawing.Size(102, 13);
            this.lblSelectedSegments.TabIndex = 8;
            this.lblSelectedSegments.Text = "Selected Segments:";
            // 
            // btnAddSegment
            // 
            this.btnAddSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddSegment.AutoSize = true;
            this.btnAddSegment.Enabled = false;
            this.btnAddSegment.Location = new System.Drawing.Point(12, 221);
            this.btnAddSegment.Name = "btnAddSegment";
            this.btnAddSegment.Size = new System.Drawing.Size(156, 25);
            this.btnAddSegment.TabIndex = 2;
            this.btnAddSegment.Text = "&Add To Selected Segments";
            this.btnAddSegment.UseVisualStyleBackColor = true;
            this.btnAddSegment.Click += new System.EventHandler(this.btnAddSegment_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnSavedData);
            this.splitContainer1.Panel1.Controls.Add(this.ftvSegments);
            this.splitContainer1.Panel1.Controls.Add(this.btnAddSegment);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lblSavedData);
            this.splitContainer1.Panel2.Controls.Add(this.btnSave);
            this.splitContainer1.Panel2.Controls.Add(this.chkSaveSegments);
            this.splitContainer1.Panel2.Controls.Add(this.chkDeautomateSegments);
            this.splitContainer1.Panel2.Controls.Add(this.grdSelSegs);
            this.splitContainer1.Panel2.Controls.Add(this.btnCancel);
            this.splitContainer1.Panel2.Controls.Add(this.btnOK);
            this.splitContainer1.Panel2.Controls.Add(this.btnDeleteSegment);
            this.splitContainer1.Panel2.Controls.Add(this.btnMoveSegmentDown);
            this.splitContainer1.Panel2.Controls.Add(this.btnMoveSegmentUp);
            this.splitContainer1.Panel2.Controls.Add(this.lblSelectedSegments);
            this.splitContainer1.Size = new System.Drawing.Size(554, 445);
            this.splitContainer1.SplitterDistance = 256;
            this.splitContainer1.TabIndex = 23;
            // 
            // btnSavedData
            // 
            this.btnSavedData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSavedData.AutoSize = true;
            this.btnSavedData.Location = new System.Drawing.Point(384, 221);
            this.btnSavedData.Name = "btnSavedData";
            this.btnSavedData.Size = new System.Drawing.Size(156, 25);
            this.btnSavedData.TabIndex = 20;
            this.btnSavedData.Text = "Select &Saved Data";
            this.btnSavedData.UseVisualStyleBackColor = true;
            this.btnSavedData.Click += new System.EventHandler(this.btnSavedData_Click);
            // 
            // ftvSegments
            // 
            this.ftvSegments.AllowFavoritesMenu = false;
            this.ftvSegments.AllowSearching = true;
            this.ftvSegments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ftvSegments.AutoSize = true;
            this.ftvSegments.Controls.Add(this.ftvMain);
            this.ftvSegments.DisableExcludedTypes = false;
            this.ftvSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
            this.ftvSegments.ExcludeNonMacPacTypes = false;
            this.ftvSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvSegments.Location = new System.Drawing.Point(1, 4);
            this.ftvSegments.Name = "ftvSegments";
            this.ftvSegments.OwnerID = 0;
            this.ftvSegments.ShowCheckboxes = false;
            this.ftvSegments.Size = new System.Drawing.Size(549, 207);
            this.ftvSegments.TabIndex = 12;
            this.ftvSegments.AfterSelect += new LMP.Controls.AfterSelectEventHandler(this.ftvSegments_AfterSelect);
            this.ftvSegments.DoubleClick += new System.EventHandler(this.ftvSegments_DoubleClick);
            // 
            // ftvMain
            // 
            this.ftvMain.AdvancedFilterText = "";
            appearance1.BackColor = System.Drawing.Color.White;
            this.ftvMain.Appearance = appearance1;
            this.ftvMain.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.ftvMain.DisableExcludedTypes = false;
            this.ftvMain.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
            this.ftvMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ftvMain.ExcludeNonMacPacTypes = false;
            this.ftvMain.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvMain.FilterText = "";
            this.ftvMain.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ftvMain.HideSelection = false;
            this.ftvMain.IncludeAdminContentInSearch = true;
            this.ftvMain.IncludeUserContentInSearch = true;
            this.ftvMain.IsDirty = false;
            this.ftvMain.Location = new System.Drawing.Point(0, 0);
            this.ftvMain.Mode = LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FolderTree;
            this.ftvMain.Name = "ftvMain";
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.ftvMain.Override = _override1;
            this.ftvMain.OwnerID = 0;
            this.ftvMain.SearchContentType = LMP.Controls.FolderTreeView.mpFolderTreeSearchContentTypes.AllContent;
            this.ftvMain.SearchFolderList = "";
            this.ftvMain.SearchUserID = 0;
            this.ftvMain.SettingsKey = "MultipleSegmentsManager.ftvMain";
            this.ftvMain.ShowCheckboxes = false;
            this.ftvMain.ShowFindPaths = true;
            this.ftvMain.ShowTopUserFolderNode = false;
            this.ftvMain.Size = new System.Drawing.Size(549, 207);
            this.ftvMain.TabIndex = 2;
            this.ftvMain.TransparentBackground = false;
            // 
            // lblSavedData
            // 
            this.lblSavedData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSavedData.AutoSize = true;
            this.lblSavedData.BackColor = System.Drawing.Color.Transparent;
            this.lblSavedData.Location = new System.Drawing.Point(117, 16);
            this.lblSavedData.Name = "lblSavedData";
            this.lblSavedData.Size = new System.Drawing.Size(77, 13);
            this.lblSavedData.TabIndex = 23;
            this.lblSavedData.Text = "#Prefill Name#";
            this.lblSavedData.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // chkSaveSegments
            // 
            this.chkSaveSegments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkSaveSegments.AutoSize = true;
            this.chkSaveSegments.Location = new System.Drawing.Point(12, 161);
            this.chkSaveSegments.Name = "chkSaveSegments";
            this.chkSaveSegments.Size = new System.Drawing.Size(169, 17);
            this.chkSaveSegments.TabIndex = 18;
            this.chkSaveSegments.Text = "&Save Segments After Insertion";
            this.chkSaveSegments.UseVisualStyleBackColor = true;
            // 
            // chkDeautomateSegments
            // 
            this.chkDeautomateSegments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkDeautomateSegments.AutoSize = true;
            this.chkDeautomateSegments.Location = new System.Drawing.Point(12, 142);
            this.chkDeautomateSegments.Name = "chkDeautomateSegments";
            this.chkDeautomateSegments.Size = new System.Drawing.Size(202, 17);
            this.chkDeautomateSegments.TabIndex = 17;
            this.chkDeautomateSegments.Text = "&Deautomate Segments After Insertion";
            this.chkDeautomateSegments.UseVisualStyleBackColor = true;
            // 
            // grdSelSegs
            // 
            this.grdSelSegs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.grdSelSegs.DisplayLayout.Appearance = appearance2;
            ultraGridColumn1.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.ProportionalResize = true;
            ultraGridColumn1.Width = 111;
            ultraGridColumn2.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.Width = 97;
            ultraGridColumn3.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            ultraGridColumn3.Header.VisiblePosition = 2;
            ultraGridColumn3.Width = 82;
            ultraGridColumn4.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            ultraGridColumn4.Header.VisiblePosition = 3;
            ultraGridColumn4.Width = 85;
            ultraGridColumn5.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;
            ultraGridColumn5.Header.VisiblePosition = 4;
            ultraGridColumn5.Width = 102;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5});
            this.grdSelSegs.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.grdSelSegs.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.grdSelSegs.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.grdSelSegs.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.grdSelSegs.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grdSelSegs.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.grdSelSegs.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.grdSelSegs.DisplayLayout.GroupByBox.Hidden = true;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grdSelSegs.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.grdSelSegs.DisplayLayout.MaxColScrollRegions = 1;
            this.grdSelSegs.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grdSelSegs.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.grdSelSegs.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.grdSelSegs.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.grdSelSegs.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.grdSelSegs.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.grdSelSegs.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.grdSelSegs.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.grdSelSegs.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.grdSelSegs.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.grdSelSegs.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.grdSelSegs.DisplayLayout.Override.CellAppearance = appearance9;
            this.grdSelSegs.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.grdSelSegs.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.grdSelSegs.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.grdSelSegs.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.grdSelSegs.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.grdSelSegs.DisplayLayout.Override.RowAppearance = appearance12;
            this.grdSelSegs.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.grdSelSegs.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.grdSelSegs.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.grdSelSegs.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.grdSelSegs.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.grdSelSegs.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True;
            this.grdSelSegs.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.grdSelSegs.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            valueList1.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DisplayText;
            valueList1.DropDownListAlignment = Infragistics.Win.DropDownListAlignment.Left;
            valueList1.Key = "lstInsertionLocations";
            valueList1.SortStyle = Infragistics.Win.ValueListSortStyle.Ascending;
            valueListItem1.DataValue = "ValueListItem3";
            valueListItem2.DataValue = "ValueListItem4";
            valueListItem3.DataValue = "ValueListItem2";
            valueListItem4.DataValue = "ValueListItem0";
            valueListItem5.DataValue = "ValueListItem1";
            valueList1.ValueListItems.Add(valueListItem1);
            valueList1.ValueListItems.Add(valueListItem2);
            valueList1.ValueListItems.Add(valueListItem3);
            valueList1.ValueListItems.Add(valueListItem4);
            valueList1.ValueListItems.Add(valueListItem5);
            this.grdSelSegs.DisplayLayout.ValueLists.AddRange(new Infragistics.Win.ValueList[] {
            valueList1});
            this.grdSelSegs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdSelSegs.Location = new System.Drawing.Point(12, 33);
            this.grdSelSegs.Name = "grdSelSegs";
            this.grdSelSegs.Size = new System.Drawing.Size(496, 102);
            this.grdSelSegs.TabIndex = 12;
            this.grdSelSegs.Text = "grdSelSegs";
            this.grdSelSegs.BeforeEnterEditMode += new System.ComponentModel.CancelEventHandler(this.grdSelSegs_BeforeEnterEditMode);
            // 
            // btnDeleteSegment
            // 
            this.btnDeleteSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteSegment.AutoSize = true;
            this.btnDeleteSegment.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSegment.Image")));
            this.btnDeleteSegment.Location = new System.Drawing.Point(517, 84);
            this.btnDeleteSegment.Name = "btnDeleteSegment";
            this.btnDeleteSegment.Size = new System.Drawing.Size(24, 22);
            this.btnDeleteSegment.TabIndex = 3;
            this.btnDeleteSegment.UseVisualStyleBackColor = true;
            this.btnDeleteSegment.Click += new System.EventHandler(this.btnDeleteSegment_Click);
            // 
            // btnMoveSegmentDown
            // 
            this.btnMoveSegmentDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveSegmentDown.AutoSize = true;
            this.btnMoveSegmentDown.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveSegmentDown.Image")));
            this.btnMoveSegmentDown.Location = new System.Drawing.Point(517, 56);
            this.btnMoveSegmentDown.Name = "btnMoveSegmentDown";
            this.btnMoveSegmentDown.Size = new System.Drawing.Size(24, 22);
            this.btnMoveSegmentDown.TabIndex = 9;
            this.btnMoveSegmentDown.TabStop = false;
            this.btnMoveSegmentDown.UseVisualStyleBackColor = true;
            this.btnMoveSegmentDown.Click += new System.EventHandler(this.btnMoveSegmentDown_Click);
            // 
            // btnMoveSegmentUp
            // 
            this.btnMoveSegmentUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveSegmentUp.AutoSize = true;
            this.btnMoveSegmentUp.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveSegmentUp.Image")));
            this.btnMoveSegmentUp.Location = new System.Drawing.Point(517, 31);
            this.btnMoveSegmentUp.Name = "btnMoveSegmentUp";
            this.btnMoveSegmentUp.Size = new System.Drawing.Size(24, 22);
            this.btnMoveSegmentUp.TabIndex = 8;
            this.btnMoveSegmentUp.TabStop = false;
            this.btnMoveSegmentUp.UseVisualStyleBackColor = true;
            this.btnMoveSegmentUp.Click += new System.EventHandler(this.btnMoveSegmentUp_Click);
            // 
            // MultipleSegmentsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.Controls.Add(this.splitContainer1);
            this.Name = "MultipleSegmentsManager";
            this.Size = new System.Drawing.Size(554, 445);
            this.Load += new System.EventHandler(this.MultipleSegmentsManager_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ftvSegments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ftvMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSelSegs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnMoveSegmentDown;
        private System.Windows.Forms.Button btnMoveSegmentUp;
        private System.Windows.Forms.Button btnCancel;
        private LMP.Controls.SearchableFolderTreeView ftvSegments;
        private LMP.Controls.FolderTreeView ftvMain;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnDeleteSegment;
        private System.Windows.Forms.Label lblSelectedSegments;
        private System.Windows.Forms.Button btnAddSegment;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Infragistics.Win.UltraWinGrid.UltraGrid grdSelSegs;
        private System.Windows.Forms.CheckBox chkSaveSegments;
        private System.Windows.Forms.CheckBox chkDeautomateSegments;
        private System.Windows.Forms.Button btnSavedData;
        private System.Windows.Forms.Label lblSavedData;
    }
}
