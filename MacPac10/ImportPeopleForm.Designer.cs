﻿namespace LMP.MacPac
{
    partial class ImportPeopleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFindGroup = new System.Windows.Forms.Label();
            this.txtFindPerson = new System.Windows.Forms.TextBox();
            this.importedPeopleManager1 = new LMP.MacPac.ImportedPeopleManager();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblFindGroup);
            this.panel1.Controls.Add(this.txtFindPerson);
            this.panel1.Controls.Add(this.importedPeopleManager1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(498, 301);
            this.panel1.TabIndex = 1;
            // 
            // lblFindGroup
            // 
            this.lblFindGroup.AutoSize = true;
            this.lblFindGroup.Location = new System.Drawing.Point(12, 15);
            this.lblFindGroup.Name = "lblFindGroup";
            this.lblFindGroup.Size = new System.Drawing.Size(30, 13);
            this.lblFindGroup.TabIndex = 2;
            this.lblFindGroup.Text = "Find:";
            // 
            // txtFindPerson
            // 
            this.txtFindPerson.Location = new System.Drawing.Point(48, 12);
            this.txtFindPerson.Name = "txtFindPerson";
            this.txtFindPerson.Size = new System.Drawing.Size(435, 20);
            this.txtFindPerson.TabIndex = 1;
            this.txtFindPerson.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFindPerson_KeyUp);
            // 
            // importedPeopleManager1
            // 
            this.importedPeopleManager1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.importedPeopleManager1.AutoSize = true;
            this.importedPeopleManager1.BackColor = System.Drawing.Color.Transparent;
            this.importedPeopleManager1.Enabled = false;
            this.importedPeopleManager1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importedPeopleManager1.Location = new System.Drawing.Point(2, 34);
            this.importedPeopleManager1.Name = "importedPeopleManager1";
            this.importedPeopleManager1.Size = new System.Drawing.Size(498, 280);
            this.importedPeopleManager1.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Location = new System.Drawing.Point(408, 307);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 25);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // ImportPeopleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(498, 342);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportPeopleForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import People";
            this.Load += new System.EventHandler(this.ImportForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ImportedPeopleManager importedPeopleManager1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblFindGroup;
        private System.Windows.Forms.TextBox txtFindPerson;
    }
}