﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using LMP.Architect.Api;
using LMP.Data;
using Word = Microsoft.Office.Interop.Word;
using CollectionTableStructure = LMP.Architect.Base.CollectionTableStructure;

namespace LMP.MacPac
{
    public partial class CollectionTableForm : Form
    {
        #region *********************fields*********************
        private bool m_bSingleColumn = false;
        private Rectangle m_oDragRectangle = Rectangle.Empty;
        private DataGridViewCell m_oDragCell = null;
        private Point m_oScreenOffset;
        private Hashtable m_oItems = new Hashtable();
        private Hashtable m_oPrefills = new Hashtable();
        private Color m_oFillColor;
        private CollectionTable m_oCollectionTable = null;
        private ForteDocument m_oMPDocument = null;
        private bool m_bSelectingProgrammatically = false;
        private CollectionTableForm.Modes m_iMode = Modes.Add;
        private bool m_bGridIsDirty = false;
        private string m_xDisplayName = "";
        #endregion
        #region ****************constructors********************
        public CollectionTableForm(int iTargetObjectID, mpObjectTypes oTargetObjectType,
            mpObjectTypes oAssignedObjectType, int iDefaultAssignedObjectID,
            string xCaption, ForteDocument oMPDocument, CollectionTable oCollectionTable,
            CollectionTableForm.Modes iMode)
        {
            InitializeComponent();
            m_oMPDocument = oMPDocument;
            m_oCollectionTable = oCollectionTable;
            m_iMode = iMode;
            m_xDisplayName = LMP.Data.Application.GetObjectTypeDisplayName(
                oAssignedObjectType);
            Setup(iTargetObjectID, oTargetObjectType, oAssignedObjectType,
                iDefaultAssignedObjectID, xCaption);
        }
        #endregion
        #region *********************enumerations*********************
        public enum Modes
        {
            Add = 0,
            Layout = 1
        }
        #endregion
        #region *********************properties*********************
        #endregion
        #region *********************methods*********************
        private void Setup(int iTargetObjectID, mpObjectTypes oTargetObjectType,
            mpObjectTypes oAssignedObjectType, int iDefaultAssignedObjectID,
            string xCaption)
        {
            //populate grid
            Color m_oFillColor = SystemColors.HighlightText;
            bool bAddExtraRow = false;
            if (m_oCollectionTable != null)
            {
                //GLOG 6397 (dm) - sort segments collection to ensure that
                //ordered by position in table
                ArrayList oSegs = new ArrayList();
                for (int i = 0; i < m_oCollectionTable.Segments.Count; i++)
                {
                    Word.Range oRange = m_oCollectionTable.Segments[i].PrimaryRange;
                    int iRow = oRange.Cells[1].RowIndex - 1;
                    int iColumn = oRange.Cells[1].ColumnIndex - 1;

                    //GLOG 6839 (dm) - bookmark for right-side item may span row
                    if (((LMP.Architect.Api.CollectionTableItem)m_oCollectionTable.Segments[i]).AllowSideBySide &&
                        (iColumn == 0) && LMP.Forte.MSWord.WordDoc.CellIsEmpty(oRange.Cells[1]))
                        iColumn = 1;

                    oSegs.Add(new object[] { m_oCollectionTable.Segments[i],
                        (iRow * 10) + iColumn });
                }
                oSegs.Sort(new SegmentArraySorter());
                    
                for (int i = 0; i < oSegs.Count; i++)
                {
                    //add item to hashtable
                    CollectionTableItem oItem = (CollectionTableItem)
                        ((object[])oSegs[i])[0];
                    string xLabel = m_xDisplayName + " " + (i + 1).ToString();
                    int iIndex = (System.Int32)((object[])oSegs[i])[1];
                    int iRow = iIndex / 10;
                    //GLOG 6419 (dm) - adjust for multi-row items
                    iRow = System.Math.Min(iRow, this.dataGridView1.Rows.Count);
                    int iColumn = iIndex % 10;
                    m_oItems.Add(xLabel, oItem.ID1.ToString() +
                        LMP.StringArray.mpEndOfField + oItem.AllowSideBySide.ToString() +
                        LMP.StringArray.mpEndOfField + iRow.ToString() +
                        LMP.StringArray.mpEndOfField + iColumn.ToString() +
                        LMP.StringArray.mpEndOfField + oItem.FullTagID);

                    //add prefill to hashtable
                    m_oPrefills.Add(xLabel, new Prefill(oItem));

                    //add row if necessary
                    if (iRow > this.dataGridView1.Rows.Count - 1)
                        this.AddRow();

                    //add to grid
                    DataGridViewRow oRow = this.dataGridView1.Rows[iRow];
                    DataGridViewCell oCell = oRow.Cells[(iColumn * 2) + 1];
                    oCell.Value = xLabel;
                    oCell.ToolTipText = oItem.DisplayName; //GLOG 6359 (dm)
                    if (!oItem.AllowSideBySide)
                    {
                        ((mpDataGridViewImageCell)oRow.Cells[2]).SetLeftBorder(false);
                        SetDeleteCellImage(oRow.Cells[0], true);
                    }
                    else
                    {
                        SetDeleteCellImage(oRow.Cells[oCell.ColumnIndex - 1], true);

                        //if there are items in both columns of row, give the user
                        //an extra row for repositioning
                        if ((iColumn == 1) && (oRow.Cells[1].Value != null))
                            bAddExtraRow = true;
                    }
                }
            }

            //add extra row
            if (bAddExtraRow)
                this.AddRow();

            //set chooser properties
            this.cmbChooser.TargetObjectID = iTargetObjectID;
            this.cmbChooser.TargetObjectType = oTargetObjectType;
            this.cmbChooser.AssignedObjectType = oAssignedObjectType;
            this.cmbChooser.ExecuteFinalSetup();
            if (iDefaultAssignedObjectID > 0)
                this.cmbChooser.Value = iDefaultAssignedObjectID.ToString();

            //set form caption
            this.Text = xCaption;

            //mode
            if (m_iMode == Modes.Add)
            {
                DisplayGrid(false);
            }
            else
            {
                //layout only
                this.dataGridView1.Visible = true;
                this.lblChooser.Visible = false;
                this.cmbChooser.Visible = false;
                this.btnAdd.Visible = false;
                this.lblLayout.Visible = true;
                this.btnGrid.Visible = false;
                this.lblLayout.Top = this.dataGridView1.Top - 22;
                this.dataGridView1.Top = this.dataGridView1.Top - 60;
                this.btnUp.Top = this.btnUp.Top - 60;
                this.btnDown.Top = this.btnDown.Top - 60;
                this.btnLeft.Top = this.btnLeft.Top - 60;
                this.btnRight.Top = this.btnRight.Top - 60;
                //this.btnOK.Top = this.btnOK.Top - 75;
                //this.btnCancel.Top = this.btnCancel.Top - 75;
                this.Height = this.Height - 55;
            }
        }

        private void AddRow()
        {
            DataGridViewRow oRow = new DataGridViewRow();
            oRow.Cells.Add(new mpDataGridViewImageCell());
            oRow.Cells.Add(new mpDataGridViewCell());
            oRow.Cells.Add(new mpDataGridViewImageCell());
            oRow.Cells.Add(new mpDataGridViewCell());
            this.dataGridView1.Rows.Add(oRow);
            int iRow = this.dataGridView1.Rows.Count - 1;
            SetDeleteCellImage(this.dataGridView1.Rows[iRow].Cells[0], false);
            SetDeleteCellImage(this.dataGridView1.Rows[iRow].Cells[2], false);
        }

        private void AddItem(string xSegmentID)
        {
            int iColumn = 0;
            int iRow = -1;
            AdminSegmentDefs oSegs = new AdminSegmentDefs();
            AdminSegmentDef oDef = (AdminSegmentDef)oSegs.ItemFromID(
                System.Int32.Parse(xSegmentID));
            bool bAllowSideBySide = (LMP.Architect.Api.Segment.GetPropertyValueFromXML(
                oDef.XML, "AllowSideBySide").ToUpper() == "TRUE");
            bool bAddRow = false;

            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                if ((this.dataGridView1.Rows[i].Cells[1].Value == null) && (bAllowSideBySide ||
                    (this.dataGridView1.Rows[i].Cells[3].Value == null)))
                {
                    //add left
                    iRow = i;
                    break;
                }
                else if (bAllowSideBySide && (this.dataGridView1.Rows[i].Cells[3].Value == null) &&
                    !IsFullRow(this.dataGridView1.Rows[i].Cells[1])) //GLOG 7544 (dm)
                {
                    //add right
                    iRow = i;
                    iColumn = 1;
                    break;
                }
            }

            //add row if necessary
            if (iRow == -1)
            {
                iRow = this.dataGridView1.Rows.Count;
                this.AddRow();
            }

            //add to hashtable
            string xLabel = m_xDisplayName + " " + (m_oItems.Count + 1).ToString();
            m_oItems.Add(xLabel, xSegmentID + LMP.StringArray.mpEndOfField +
                bAllowSideBySide + LMP.StringArray.mpEndOfField + iRow.ToString() +
                LMP.StringArray.mpEndOfField + iColumn.ToString());

            //add to grid
            DataGridViewRow oRow = this.dataGridView1.Rows[iRow];
            int iCell = (iColumn * 2) + 1;
            oRow.Cells[iCell].Value = xLabel;
            oRow.Cells[iCell].ToolTipText = oDef.DisplayName; //GLOG 6359 (dm)

            //adjust borders and deletion image
            if (!bAllowSideBySide)
            {
                ((mpDataGridViewImageCell)oRow.Cells[2]).SetLeftBorder(false);
                dataGridView1.Refresh();
                SetDeleteCellImage(oRow.Cells[0], true);
            }
            else
            {
                SetDeleteCellImage(oRow.Cells[iCell - 1], true);

                //if there are items in both columns of row, give the user
                //an extra row for repositioning
                if ((oRow.Cells[1].Value != null) && (oRow.Cells[3].Value != null))
                    bAddRow = true;
            }

            //select new item
            this.dataGridView1.CurrentCell = oRow.Cells[iCell];

            //ensure at least one empty row
            if (bAddRow)
            {
                for (int i = this.dataGridView1.Rows.Count - 1; i >= 0; i--)
                {
                    if ((this.dataGridView1.Rows[i].Cells[1].Value == null) &&
                        (this.dataGridView1.Rows[i].Cells[3].Value == null))
                    {
                        bAddRow = false;
                        break;
                    }
                }
                if (bAddRow)
                    this.AddRow();
            }
        }

        private void SetDeleteCellImage(DataGridViewCell oCell, bool bShow)
        {
            mpDataGridViewImageCell oImageCell = (mpDataGridViewImageCell)oCell;
            if (bShow)
                oImageCell.Value = this.imageList1.Images[0];
            else
                oImageCell.Value = new Bitmap(1, 1);
            mpDataGridViewCell oTextCell = (mpDataGridViewCell)this.dataGridView1.Rows[
                oCell.RowIndex].Cells[oCell.ColumnIndex + 1];
            oTextCell.SetLeftBorder(bShow);
        }

        public void Finish()
        {
            TaskPane oTP = null;
            try
            {
                oTP = TaskPanes.Item(m_oMPDocument.WordDocument);
            }
            catch { }

            LMP.Architect.Api.ForteDocument.WordXMLEvents iEvents =
                ForteDocument.IgnoreWordXMLEvents; //GLOG 6415 (dm)

            try
            {
                if (!m_bGridIsDirty)
                {
                    //if in Add mode and grid isn't showing, the OK button
                    //functions as the Add button
                    if ((m_iMode == Modes.Add) && (this.btnGrid.Text.IndexOf("Show") > -1))
                    {
                        string xSegmentID = this.cmbChooser.Value;
                        if (xSegmentID != "")
                            AddItem(xSegmentID);
                        else
                            return;
                    }
                    else
                        //nothing to do
                        return;
                }

                //suspend screen updating and ui refresh
                if (oTP != null)
                {
                    oTP.DocEditor.SuspendSegmentNodeInsertion = true;
                    oTP.ScreenUpdating = false;
                }
                else
                    SetWordEcho(false);

                //delete existing items in table
                if (m_oCollectionTable != null)
                {
                    if ((this.dataGridView1.Rows[0].Cells[1].Value == null) &&
                           (this.dataGridView1.Rows[0].Cells[3].Value == null))
                        //GLOG 6363 (dm) - grid is empty - delete entire table
                        m_oMPDocument.DeleteSegment(m_oCollectionTable, false, true, true, true, true); //GLOG 8549
                    else
                    {
                        //preserve items that are still in same position
                        //GLOG 6686 (dm) - persist items only if still in original
                        //order and, if side-by-side is allowed, also in original position
                        int iLastIndex = 0;
                        int iIndex = 0;
                        bool bReinsert = false;
                        for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                        {
                            for (int j = 0; j <= 1; j++)
                            {
                                DataGridViewCell oCell = dataGridView1.Rows[i].Cells[(j * 2) + 1];
                                if (oCell.Value != null)
                                {
                                    //cell contains an item
                                    string xKey = oCell.Value.ToString();
                                    iIndex = System.Int32.Parse(xKey.Replace(m_xDisplayName + " ", ""));
                                    string xProps = m_oItems[xKey].ToString();
                                    string[] oProps = xProps.Split(LMP.StringArray.mpEndOfField);

                                    if ((oProps.Length > 4) && (iIndex > iLastIndex))
                                    {
                                        //this is an existing item and sequence is unchanged
                                        if (oProps[1] == "True")
                                        {
                                            //side-by-side is allowed
                                            int iRow = System.Int32.Parse(oProps[2].ToString());
                                            int iCol = System.Int32.Parse(oProps[3].ToString());
                                            object oValue = dataGridView1.Rows[iRow].Cells[(iCol * 2) + 1].Value;
                                            if ((oValue == null) || (oValue.ToString() != xKey))
                                            {
                                                //item has been moved
                                                bReinsert = true;
                                                break;
                                            }
                                        }

                                        //mark item as persistent
                                        m_oItems[xKey] = xProps + LMP.StringArray.mpEndOfField + "1";
                                    }
                                    else
                                    {
                                        //item is new or has been moved
                                        bReinsert = true;
                                        break;
                                    }

                                    iLastIndex = iIndex;
                                }
                            }

                            if (bReinsert)
                                break;
                        }

                        //delete existing items not marked as persistent
                        int iCount = m_oCollectionTable.Segments.Count;
                        for (int i = iCount - 1; i >= 0; i--)
                        {
                            string xKey = m_xDisplayName + " " + (i + 1).ToString();
                            string xProps = m_oItems[xKey].ToString();
                            string[] oProps = xProps.Split(LMP.StringArray.mpEndOfField);
                            if (oProps.Length < 6)
                                //delete
                                m_oMPDocument.DeleteSegment(m_oCollectionTable.Segments[i],
                                    false, true, true, true, true); //GLOG 8549
                        }
                    }
                }

                //cycle through grid, inserting specified items
                Segment oFirstNewItem = null;
                Segment oFirstItem = null;
                for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                {
                    for (int j = 0; j <= 1; j++)
                    {
                        DataGridViewCell oCell = dataGridView1.Rows[i].Cells[(j * 2) + 1];
                        if (oCell.Value != null)
                        {
                            //persistent items are marked above with a sixth property
                            Prefill oPrefill = null;
                            string xKey = oCell.Value.ToString();
                            string xProps = m_oItems[xKey].ToString();
                            string[] oProps = xProps.Split(LMP.StringArray.mpEndOfField);
                            if (oProps.Length < 6)
                            {
                                //get segment id
                                string xSegmentID = oProps[0].ToString();

                                //get prefill for existing item
                                if (m_oPrefills.Contains(xKey))
                                    oPrefill = (Prefill)m_oPrefills[xKey];

                                //insert collection table if necessary
                                //GLOG 6789 (dm) - make sure that table still exists - WordDoc.DeleteFullRowBookmark(),
                                //unlike its bounding object analogs, will delete a 2-cell row along with the collection table
                                //item in the left cell when the right cell is empty, but I'm loathe to change that given that
                                //attempted fixes to that method seem to invariably cause new issues
                                if ((m_oCollectionTable == null) || (m_oCollectionTable.PrimaryRange.Tables.Count == 0))
                                {
                                    AdminSegmentDefs oSegs = new AdminSegmentDefs();
                                    AdminSegmentDef oDef = (AdminSegmentDef)oSegs.ItemFromID(
                                        System.Int32.Parse(xSegmentID));
                                    string xWrapperID = Segment.GetWrapperID(oDef,
                                        (m_oMPDocument.FileFormat == mpFileFormats.OpenXML));
                                    Segment oWrapper = m_oMPDocument.InsertSegment(xWrapperID,
                                        null, Segment.InsertionLocations.InsertAtSelection,
                                        Segment.InsertionBehaviors.Default, null);
                                    m_oCollectionTable = (CollectionTable)oWrapper;
                                }

                                //set insertion location
                                CollectionTableStructure.ItemInsertionLocations iLocation =
                                    CollectionTableStructure.ItemInsertionLocations.Unspecified;
                                if (System.Boolean.Parse(oProps[1].ToString()))
                                {
                                    //allows side by side - current row if there's an item in other cell
                                    Word.Range oRange = m_oCollectionTable.PrimaryRange;

                                    bool bRowExists = (oRange.Tables[1].Rows.Count > i);

                                    if (j == 0)
                                    {
                                        //left cell
                                        iLocation = CollectionTableStructure.ItemInsertionLocations.LeftCell;
                                        if (bRowExists) //GLOG 6789 (dm)
                                        {
                                            iLocation = CollectionTableStructure.ItemInsertionLocations.CurrentRow |
                                                iLocation;
                                        }
                                    }
                                    else
                                    {
                                        //right cell
                                        iLocation = CollectionTableStructure.ItemInsertionLocations.RightCell;
                                        if (bRowExists) //GLOG 6789 (dm)
                                        {
                                            iLocation = CollectionTableStructure.ItemInsertionLocations.CurrentRow |
                                                iLocation;
                                        }
                                    }

                                    if ((iLocation & CollectionTableStructure.ItemInsertionLocations.CurrentRow) > 0)
                                    {
                                        //select target row
                                        oRange = oRange.Tables[1].Rows[i + 1].Range;
                                        object oMissing = System.Reflection.Missing.Value;
                                        oRange.StartOf(ref oMissing, ref oMissing);
                                        oRange.Select();
                                    }
                                }
                                m_oCollectionTable.PendingInsertionLocation = iLocation;

                                //GLOG 6385 (dm) - flag to prevent prefill values from being
                                //overwritten by segment actions
                                //GLOG 6385/6414 (dm, 7/10/14) - this flag was previously being
                                //set unconditionally for all collection table items -
                                //apply to only existing segments that are being moved -
                                //the fifth prop is the tag id
                                if (oProps.Length == 5)
                                    m_oMPDocument.ChildStructureInsertionInProgess = true;

                                //insert item
                                Segment oSegment = m_oMPDocument.InsertSegment(
                                    xSegmentID, m_oCollectionTable,
                                    Segment.InsertionLocations.InsertAtSelection,
                                    Segment.InsertionBehaviors.Default, true,
                                    oPrefill, "", false, false, null);

                                //GLOG 6385 (dm)
                                if (oProps.Length == 5) //GLOG 6385/6414 (dm, 7/10/14)
                                    m_oMPDocument.ChildStructureInsertionInProgess = false;

                                //get first item and first newly inserted item for
                                //selection in document and editor below
                                if (oFirstItem == null)
                                    oFirstItem = oSegment;
                                if ((oPrefill == null) && (oFirstNewItem == null))
                                    oFirstNewItem = oSegment;
                            }
                            else if (oFirstItem == null)
                            {
                                //first item is persistent
                                string xTagID = oProps[4].ToString();
                                oFirstItem = m_oCollectionTable.Segments[xTagID];
                            }
                        }
                    }
                }


                //refresh ui
                if (oTP != null)
                {
                    oTP.DocEditor.SuspendSegmentNodeInsertion = false;
                    oTP.DocEditor.RefreshTree();

                    //GLOG 6415 (dm)
                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                    //select appropriate item in document and editor
                    if (oFirstNewItem != null)
                    {
                        //select first newly inserted item
                        oFirstNewItem.Select(true);
                        oTP.DocEditor.SelectFirstNodeForSegment(oFirstNewItem);
                    }
                    else if (oFirstItem != null)
                    {
                        //no new items - select first item
                        oFirstItem.Select(true);
                        oTP.DocEditor.SelectFirstNodeForSegment(oFirstItem);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                ForteDocument.IgnoreWordXMLEvents = iEvents; //GLOG 6415 (dm)

                if (oTP != null)
                {
                    oTP.DocEditor.SuspendSegmentNodeInsertion = false;
                    oTP.ScreenUpdating = true;
                }
                else
                    SetWordEcho(true);
            }
        }
        private void SetWordEcho(bool bOn)
        {
            Word.Application oWord = LMP.Forte.MSWord.GlobalMethods.CurWordApp;
            if (bOn)
            {
                LMP.WinAPI.LockWindowUpdate(0);
                oWord.ScreenUpdating = true;
            }
            else
            {
                //freeze Word
                LMP.WinAPI.LockWindowUpdate(LMP.WinAPI.FindWindow("OpusApp", 0));
                oWord.ScreenUpdating = false;
            }
        }
        private DataGridViewCell GetPrimaryCell(DataGridViewCell oCell)
        {
            int iCol = oCell.ColumnIndex;
            if (oCell is DataGridViewImageCell)
                iCol++;
            DataGridViewRow oRow = dataGridView1.Rows[oCell.RowIndex];
            if ((iCol == 3) && (oRow.Cells[3].Value == null) &&
                (oRow.Cells[1].Value != null))
            {
                //may be full row
                if (IsFullRow(oRow.Cells[1]))
                    iCol = 1;
            }

            if (oRow.Cells[iCol].Value != null)
                return oRow.Cells[iCol];
            else
                return null;
        }

        private bool IsFullRow(DataGridViewCell oCell)
        {
            if (oCell.Value == null)
                return false;
            string xProps = m_oItems[oCell.Value.ToString()].ToString();
            string[] oProps = xProps.Split(LMP.StringArray.mpEndOfField);
            return (oProps[1].ToUpper() == "FALSE");
        }

        private void AdjustSelection(Hashtable hSelDirectives)
        {
            m_bSelectingProgrammatically = true;
            foreach (string xKey in hSelDirectives.Keys)
            {
                string[] oIndicies = xKey.Split(LMP.StringArray.mpEndOfValue);
                int iRow = System.Int32.Parse(oIndicies[0]);
                int iCol = System.Int32.Parse(oIndicies[1]);
                DataGridViewCell oCell = dataGridView1.Rows[iRow].Cells[iCol];
                oCell.Selected = (hSelDirectives[xKey].ToString() == "1");
            }
            m_bSelectingProgrammatically = false;
        }

        private void DisplayGrid(bool bShow)
        {
            //int iVerticalOffset = 216;
            //int iHorizontalOffset = 162;
            //if (!bShow)
            //{
            //    iVerticalOffset = -iVerticalOffset;
            //    iHorizontalOffset = -iHorizontalOffset;
            //}
            //this.Height = this.Height + iVerticalOffset;
            //this.btnOK.Top = this.btnOK.Top + iVerticalOffset;
            //this.btnCancel.Top = this.btnCancel.Top + iVerticalOffset;
            //this.btnGrid.Top = this.btnGrid.Top + iVerticalOffset;
            //this.Width = this.Width + iHorizontalOffset;
            //this.btnOK.Left = this.btnOK.Left + iHorizontalOffset;
            //this.btnCancel.Left = this.btnCancel.Left + iHorizontalOffset;
            this.btnAdd.Visible = bShow;
            this.lblLayout.Visible = bShow;
            this.dataGridView1.Visible = bShow;
            this.btnUp.Visible = bShow;
            this.btnDown.Visible = bShow;
            this.btnLeft.Visible = bShow;
            this.btnRight.Visible = bShow;

            if (bShow)
            {
                this.cmbChooser.Width = (this.btnAdd.Left  - 10) - this.cmbChooser.Left;

                //GLOG #8492 - dcf
                this.Size = new Size(this.lblFormSizeMarker2.Left, this.lblFormSizeMarker2.Top + 40);
                this.btnGrid.Text = "&Hide Layout";
                this.dataGridView1.Focus();
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[0].Cells[1];
            }
            else
            {
                this.cmbChooser.Width = (this.btnAdd.Left + this.btnAdd.Width) - this.cmbChooser.Left;

                //GLOG #8492 - dcf
                this.Size = new Size(this.lblFormSizeMarker1.Left, this.lblFormSizeMarker1.Top + 40);

                this.btnGrid.Text = "&Show Layout";
            }
        }

        private void MoveItem(DataGridViewCell oSourceCell, DataGridViewCell oDestCell)
        {
            if (oSourceCell.Value == null)
                return;

            DataGridViewRow oSourceRow = dataGridView1.Rows[oSourceCell.RowIndex];
            DataGridViewRow oDestRow = dataGridView1.Rows[oDestCell.RowIndex];
            object oSourceValue = oSourceCell.Value;
            string xSourceToolTip = oSourceCell.ToolTipText; //GLOG 6359 (dm)
            object oCompanionValue = null;
            string xCompanionToolTip = ""; //GLOG 6359 (dm)
            string xProps = m_oItems[oSourceValue].ToString();
            string[] oProps = xProps.Split(LMP.StringArray.mpEndOfField);
            bool bIsFullRow = (oProps[1].ToUpper() != "TRUE");

            //GLOG 7544 (dm) - disallow problematic moves
            if (bIsFullRow && (oDestCell.ColumnIndex == 3))
            {
                //prevent a full row item from being dragged to right cell of a row that
                //contains a side-by-side item in either cell
                if (((oDestRow.Cells[1].Value != null) && !this.IsFullRow(oDestRow.Cells[1])) ||
                    ((oDestRow.Cells[3].Value != null) && !this.IsFullRow(oDestRow.Cells[3])))
                    return;
            }
            else if ((!bIsFullRow) && ((oSourceCell.ColumnIndex == 3) ||
                (oDestCell.ColumnIndex == 3)))
            {
                //prevent a side-by-side item from being dragged to a row that contains
                //a full row item unless going from left to left
                DataGridViewCell oCell = this.GetPrimaryCell(oDestCell);
                if ((oCell != null) && this.IsFullRow(oCell))
                    return;
            }

            //if dragging side-by-side item to a row that contains a full row item,
            //the other side-by-side item in source row needs to be moved as well
            if ((!bIsFullRow) && this.IsFullRow(oDestCell))
            {
                if (oSourceCell.ColumnIndex == 1)
                {
                    oCompanionValue = oSourceRow.Cells[3].Value;
                    xCompanionToolTip = oSourceRow.Cells[3].ToolTipText; //GLOG 6359 (dm)
                }
                else
                {
                    oCompanionValue = oSourceRow.Cells[1].Value;
                    xCompanionToolTip = oSourceRow.Cells[1].ToolTipText; //GLOG 6359 (dm)
                }
            }

            //full row item should always target left cell
            if (bIsFullRow && (oDestCell.ColumnIndex == 3))
                oDestCell = oDestRow.Cells[1];

            //exit if dragging to same cell
            if (oSourceCell.Value == oDestCell.Value)
                return;

            //move items in destination cells
            if (bIsFullRow || this.IsFullRow(oDestCell))
            {
                //full row swap
                //GLOG 6359 - instead of simply swapping source and
                //destination rows, move all intervening rows up/down -
                //also added tooltip text
                if (oSourceRow.Index > oDestRow.Index)
                {
                    //moving up - move other rows down
                    for (int i = oSourceRow.Index - 1; i >= oDestRow.Index; i--)
                    {
                        dataGridView1.Rows[i + 1].Cells[1].Value = dataGridView1.Rows[i].Cells[1].Value;
                        dataGridView1.Rows[i + 1].Cells[1].ToolTipText = dataGridView1.Rows[i].Cells[1].ToolTipText;
                        dataGridView1.Rows[i + 1].Cells[3].Value = dataGridView1.Rows[i].Cells[3].Value;
                        dataGridView1.Rows[i + 1].Cells[3].ToolTipText = dataGridView1.Rows[i].Cells[3].ToolTipText;
                    }
                }
                else
                {
                    //moving down - move other rows up
                    for (int i = oSourceRow.Index; i < oDestRow.Index; i++)
                    {
                        dataGridView1.Rows[i].Cells[1].Value = dataGridView1.Rows[i + 1].Cells[1].Value;
                        dataGridView1.Rows[i].Cells[1].ToolTipText = dataGridView1.Rows[i + 1].Cells[1].ToolTipText;
                        dataGridView1.Rows[i].Cells[3].Value = dataGridView1.Rows[i + 1].Cells[3].Value;
                        dataGridView1.Rows[i].Cells[3].ToolTipText = dataGridView1.Rows[i + 1].Cells[3].ToolTipText;
                    }
                }
            }
            else
            {
                //single cell swap
                oSourceCell.Value = oDestCell.Value;
                oSourceCell.ToolTipText = oDestCell.ToolTipText; //GLOG 6359 (dm)
            }

            //move specified item
            oDestCell.Value = oSourceValue;
            oDestCell.ToolTipText = xSourceToolTip; //GLOG 6359 (dm)

            //move companion item if necessary
            if (oCompanionValue != null)
            {
                if (oSourceCell.ColumnIndex == 1)
                {
                    oDestRow.Cells[3].Value = oCompanionValue;
                    oDestRow.Cells[3].ToolTipText = xCompanionToolTip; //GLOG 6359 (dm)
                }
                else
                {
                    oDestRow.Cells[1].Value = oCompanionValue;
                    oDestRow.Cells[1].ToolTipText = xCompanionToolTip; //GLOG 6359 (dm)
                }
            }

            //GLOG 7544 (dm) - make sure that right cell of full row item
            //no longer contains side-by-side item that was there before
            if (bIsFullRow)
            {
                oDestRow.Cells[3].Value = null;
                oDestRow.Cells[3].ToolTipText = ""; //GLOG 6359 (dm)
            }

            //adjust borders and deletion image
            //GLOG 6359 - do for all impacted rows
            int iStart = System.Math.Min(oSourceRow.Index, oDestRow.Index);
            int iEnd = System.Math.Max(oSourceRow.Index, oDestRow.Index);
            for (int i = iStart; i <= iEnd; i++)
            {
                ((mpDataGridViewImageCell)dataGridView1.Rows[i].Cells[2]).SetLeftBorder(
                    !this.IsFullRow(dataGridView1.Rows[i].Cells[1]));
                dataGridView1.Refresh();
                SetDeleteCellImage(dataGridView1.Rows[i].Cells[0],
                    (dataGridView1.Rows[i].Cells[1].Value != null));
                SetDeleteCellImage(dataGridView1.Rows[i].Cells[2],
                    (dataGridView1.Rows[i].Cells[3].Value != null));
            }

            //adjust selection
            oSourceRow.Selected = false;
            oDestCell.Selected = true;

            m_bGridIsDirty = true;
            this.btnGrid.Visible = false;
        }
        #endregion
        #region *********************events*********************
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string xSegmentID = this.cmbChooser.Value;
                if (xSegmentID != "")
                {
                    AddItem(xSegmentID);
                    m_bGridIsDirty = true;
                    this.btnGrid.Visible = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // Remember the point where the mouse down occurred. The DragSize indicates
            // the size that the mouse can move before a drag event should be started.                
            Size dragSize = SystemInformation.DragSize;

            // Create a rectangle using the DragSize, with the mouse position being
            // at the center of the rectangle.
            m_oDragRectangle = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                e.Y - (dragSize.Height / 2)), dragSize);

            int iColumnIndex = 0;
            if (!m_bSingleColumn)
            {
                int iLeft1 = dataGridView1.GetColumnDisplayRectangle(1, true).X;
                int iLeft2 = dataGridView1.GetColumnDisplayRectangle(2, true).X;
                int iLeft3 = dataGridView1.GetColumnDisplayRectangle(3, true).X;
                if ((iLeft1 < e.X) && (iLeft2 > e.X))
                    iColumnIndex = 1;
                else if ((iLeft2 < e.X) && (iLeft3 > e.X))
                    iColumnIndex = 2;
                else if (iLeft3 < e.X)
                    iColumnIndex = 3;
            }

            int iRowIndex = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                Rectangle oRowRect = dataGridView1.GetRowDisplayRectangle(i, true);
                Point oRowTop = new Point(oRowRect.Left, oRowRect.Top);
                Point oRowBottom = new Point(oRowRect.Left, oRowRect.Bottom);
                if ((oRowTop.Y <= e.Y) && (oRowBottom.Y >= e.Y))
                {
                    iRowIndex = i;
                    break;
                }
            }

            //get primary cell
            DataGridViewCell oCell = dataGridView1.Rows[iRowIndex].Cells[iColumnIndex];
            DataGridViewCell oPrimaryCell = this.GetPrimaryCell(oCell);
            if (oPrimaryCell != null)
            {
                if ((iColumnIndex == 0) || ((iColumnIndex == 2) &&
                    !this.IsFullRow(oPrimaryCell)))
                {
                    //delete
                    DialogResult oChoice = MessageBox.Show(
                        LMP.Resources.GetLangString("Message_DeleteEntry"),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oChoice == DialogResult.Yes)
                    {
                        oPrimaryCell.Value = null;
                        this.SetDeleteCellImage(oCell, false);
                        m_bGridIsDirty = true;
                        this.btnGrid.Visible = false;
                    }
                    m_oDragRectangle = Rectangle.Empty;
                }
                else
                    //drag
                    m_oDragCell = oPrimaryCell;
            }
            else
                //cancel drag
                m_oDragRectangle = Rectangle.Empty;
        }

        private void dataGridView1_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {

        }

        private void dataGridView1_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (m_oDragRectangle != Rectangle.Empty &&
                    !m_oDragRectangle.Contains(e.X, e.Y))
                {
                    // The screenOffset is used to account for any desktop bands 
                    // that may be at the top or left side of the screen when 
                    // determining when to cancel the drag drop operation.
                    m_oScreenOffset = SystemInformation.WorkingArea.Location;

                    // Proceed with the drag-and-drop, passing in the data
                    DragDropEffects dropEffect = dataGridView1.DoDragDrop(
                        m_oDragCell.Value, DragDropEffects.All);
                }
            }
        }

        private void dataGridView1_MouseUp(object sender, MouseEventArgs e)
        {
            // Reset the drag rectangle when the mouse button is raised.
            m_oDragRectangle = Rectangle.Empty;
        }

        private void dataGridView1_DragDrop(object sender, DragEventArgs e)
        {
            int iColumnIndex = 0;
            if (!m_bSingleColumn)
            {
                Rectangle oColRect = dataGridView1.GetColumnDisplayRectangle(2, true);
                Point oColPoint = new Point(oColRect.X, oColRect.Y);
                if (dataGridView1.PointToScreen(oColPoint).X < e.X)
                    iColumnIndex = 1;
            }

            int iRowIndex = -1; //GLOG 6359 (dm)
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                Rectangle oRowRect = dataGridView1.GetRowDisplayRectangle(i, true);
                Point oRowTop = new Point(oRowRect.Left, oRowRect.Top);
                Point oRowBottom = new Point(oRowRect.Left, oRowRect.Bottom);
                if ((dataGridView1.PointToScreen(oRowTop).Y <= e.Y) &&
                    (dataGridView1.PointToScreen(oRowBottom).Y >= e.Y))
                {
                    iRowIndex = i;
                    break;
                }
            }

            //GLOG 6359 (dm) - don't move if target is off of grid
            if (iRowIndex == -1)
                return;

            int iCell = (iColumnIndex * 2) + 1;
            DataGridViewCell oDropCell = dataGridView1.Rows[iRowIndex].Cells[iCell];
            this.MoveItem(m_oDragCell, oDropCell);
        }

        private void dataGridView1_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            //enable/disable arrows
            bool bFound = false;
            bool bLeft = true;
            bool bRight = true;
            bool bUp = true;
            bool bDown = true;
            for (int i = 0; i < dataGridView1.SelectedCells.Count; i++)
            {
                DataGridViewCell oCell = dataGridView1.SelectedCells[i];
                if ((oCell is DataGridViewTextBoxCell) && (oCell.Value != null))
                {
                    bFound = true;
                    if (oCell.ColumnIndex == 1)
                    {
                        bLeft = false;
                        if (this.IsFullRow(oCell))
                            bRight = false;
                    }
                    else
                        bRight = false;
                    if (oCell.RowIndex == 0)
                        bUp = false;
                    if (oCell.RowIndex == dataGridView1.Rows.Count - 1)
                        bDown = false;
                }
            }
            this.btnLeft.Enabled = (bLeft && bFound);
            this.btnRight.Enabled = (bRight && bFound);
            this.btnUp.Enabled = (bUp && bFound);
            this.btnDown.Enabled = (bDown && bFound);

            if (m_bSelectingProgrammatically)
                return;

            Hashtable hSelDirectives = new Hashtable();
            for (int i = 0; i < dataGridView1.SelectedCells.Count; i++)
            {
                DataGridViewCell oCell = dataGridView1.SelectedCells[i];
                int iRow = oCell.RowIndex;
                int iCol = oCell.ColumnIndex;
                if (!hSelDirectives.ContainsKey(iRow.ToString() +
                    StringArray.mpEndOfValue + iCol.ToString()))
                {
                    DataGridViewCell oPrimaryCell = this.GetPrimaryCell(oCell);
                    if (oPrimaryCell != null)
                    {
                        if (IsFullRow(oPrimaryCell))
                        {
                            hSelDirectives.Add(iRow.ToString() +
                                StringArray.mpEndOfValue + "0", "0");
                            hSelDirectives.Add(iRow.ToString() +
                                StringArray.mpEndOfValue + "1", "1");
                            hSelDirectives.Add(iRow.ToString() +
                                StringArray.mpEndOfValue + "2", "1");
                            hSelDirectives.Add(iRow.ToString() +
                                StringArray.mpEndOfValue + "3", "1");
                        }
                        else if (oCell is DataGridViewImageCell)
                        {
                            //select text cell and deselect image cell
                            hSelDirectives.Add(iRow.ToString() +
                               StringArray.mpEndOfValue + (iCol + 1).ToString(), "1");
                            hSelDirectives.Add(iRow.ToString() +
                                StringArray.mpEndOfValue + iCol.ToString(), "0");
                        }
                    }
                    else if (oCell is DataGridViewImageCell)
                    {
                        //select empty text cell
                        hSelDirectives.Add(iRow.ToString() +
                           StringArray.mpEndOfValue + (iCol + 1).ToString(), "1");
                    }
                    else
                    {
                        //select empty image cell
                        hSelDirectives.Add(iRow.ToString() +
                           StringArray.mpEndOfValue + (iCol - 1).ToString(), "1");
                    }
                }
            }

            AdjustSelection(hSelDirectives);
        }

        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\t')
            {
                //tab past image cells
                int iRow = this.dataGridView1.CurrentCell.RowIndex;
                int iCol = this.dataGridView1.CurrentCell.ColumnIndex;
                DataGridViewCell oPrimaryCell = this.GetPrimaryCell(
                    this.dataGridView1.CurrentCell);
                if ((iCol > 0) && (oPrimaryCell != null) && this.IsFullRow(oPrimaryCell))
                {
                    //full row - move to next row
                    if (dataGridView1.Rows.Count > iRow + 1)
                    {
                        this.dataGridView1.CurrentCell =
                            this.dataGridView1.Rows[iRow + 1].Cells[1];
                    }
                    else
                    {
                        //last row - leave control
                        this.dataGridView1.CurrentCell = oPrimaryCell;
                        if (this.btnUp.Enabled)
                            this.btnUp.Focus();
                        else if (this.btnGrid.Visible)
                            this.btnGrid.Focus();
                        else
                            this.btnOK.Focus();
                    }
                    e.Handled = true;
                }
                else if ((iCol == 0) || (iCol == 2))
                {
                    //tab to next text cell
                    this.dataGridView1.CurrentCell =
                        this.dataGridView1.Rows[iRow].Cells[iCol + 1];
                    e.Handled = true;
                }
            }
        }

        private void btnGrid_Click(object sender, EventArgs e)
        {
            this.DisplayGrid((this.btnGrid.Text.IndexOf("Show") > -1));
        }

        private void btnOK_Enter(object sender, EventArgs e)
        {
            try
            {
                //GLOG 6388 (dm) - validate chooser
                if (string.IsNullOrEmpty(this.cmbChooser.Value))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_LimitedToList"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    this.cmbChooser.Focus();
                    return;
                }

                //validate grid - don't allow gaps between rows
                bool bEmptyRow = false;
                for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                {
                    if ((this.dataGridView1.Rows[i].Cells[1].Value == null) &&
                        (this.dataGridView1.Rows[i].Cells[3].Value == null))
                        bEmptyRow = true;
                    else if (bEmptyRow)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Prompt_CollectionTableHasGaps"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                        this.dataGridView1.Focus();
                        return;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewSelectedCellCollection oCells = dataGridView1.SelectedCells;
                int iCount = oCells.Count;
                int[,] aCells = new int[iCount, 2];
                for (int i = 0; i < iCount; i++)
                {
                    DataGridViewCell oCell = oCells[i];
                    if ((oCell is DataGridViewTextBoxCell) && (oCell.Value != null))
                    {
                        aCells[i, 0] = oCells[i].RowIndex;
                        aCells[i, 1] = oCells[i].ColumnIndex;
                    }
                    else
                        aCells[i, 0] = -1;
                }
                for (int i = 0; i < iCount; i++)
                {
                    if (aCells[i, 0] > -1)
                    {
                        DataGridViewCell oCell = dataGridView1.Rows[aCells[i, 0]].Cells[aCells[i, 1]];
                        this.MoveItem(oCell, dataGridView1.Rows[oCell.RowIndex].Cells[1]);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewSelectedCellCollection oCells = dataGridView1.SelectedCells;
                int iCount = oCells.Count;
                int[,] aCells = new int[iCount, 2];
                for (int i = 0; i < iCount; i++)
                {
                    DataGridViewCell oCell = oCells[i];
                    if ((oCell is DataGridViewTextBoxCell) && (oCell.Value != null))
                    {
                        aCells[i, 0] = oCells[i].RowIndex;
                        aCells[i, 1] = oCells[i].ColumnIndex;
                    }
                    else
                        aCells[i, 0] = -1;
                }
                for (int i = 0; i < iCount; i++)
                {
                    if (aCells[i, 0] > -1)
                    {
                        DataGridViewCell oCell = dataGridView1.Rows[aCells[i, 0]].Cells[aCells[i, 1]];
                        this.MoveItem(oCell, dataGridView1.Rows[oCell.RowIndex].Cells[3]);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewSelectedCellCollection oCells = dataGridView1.SelectedCells;
                int iCount = oCells.Count;
                int[,] aCells = new int[iCount, 2];
                for (int i = 0; i < iCount; i++)
                {
                    DataGridViewCell oCell = oCells[i];
                    if ((oCell is DataGridViewTextBoxCell) && (oCell.Value != null))
                    {
                        aCells[i, 0] = oCells[i].RowIndex;
                        aCells[i, 1] = oCells[i].ColumnIndex;
                    }
                    else
                        aCells[i, 0] = -1;
                }
                for (int i = 0; i < iCount; i++)
                {
                    if (aCells[i, 0] > -1)
                    {
                        DataGridViewCell oCell = dataGridView1.Rows[aCells[i, 0]].Cells[aCells[i, 1]];
                        this.MoveItem(oCell, dataGridView1.Rows[oCell.RowIndex - 1]
                            .Cells[oCell.ColumnIndex]);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewSelectedCellCollection oCells = dataGridView1.SelectedCells;
                int iCount = oCells.Count;
                int[,] aCells = new int[iCount, 2];
                for (int i = 0; i < iCount; i++)
                {
                    DataGridViewCell oCell = oCells[i];
                    if ((oCell is DataGridViewTextBoxCell) && (oCell.Value != null))
                    {
                        aCells[i, 0] = oCells[i].RowIndex;
                        aCells[i, 1] = oCells[i].ColumnIndex;
                    }
                    else
                        aCells[i, 0] = -1;
                }
                for (int i = 0; i < iCount; i++)
                {
                    if (aCells[i, 0] > -1)
                    {
                        DataGridViewCell oCell = dataGridView1.Rows[aCells[i, 0]].Cells[aCells[i, 1]];
                        this.MoveItem(oCell, dataGridView1.Rows[oCell.RowIndex + 1]
                            .Cells[oCell.ColumnIndex]);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void cmbChooser_ValueChanged(object sender, EventArgs e)
        {
            //GLOG 6388 (dm)
            this.btnOK.Enabled = !string.IsNullOrEmpty(this.cmbChooser.Value);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DataGridViewCell oPrimaryCell = this.GetPrimaryCell(
                    this.dataGridView1.CurrentCell);
                if (oPrimaryCell != null)
                {
                    //delete
                    DialogResult oChoice = MessageBox.Show(
                        LMP.Resources.GetLangString("Message_DeleteEntry"),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oChoice == DialogResult.Yes)
                    {
                        oPrimaryCell.Value = null;
                        DataGridViewCell oCell = this.dataGridView1.Rows[oPrimaryCell
                            .RowIndex].Cells[oPrimaryCell.ColumnIndex - 1];
                        this.SetDeleteCellImage(oCell, false);
                        m_bGridIsDirty = true;
                        this.btnGrid.Visible = false;
                    }
                }
                e.Handled = true;
            }
        }
        #endregion
        #region *********************helper classes*********************
        /// <summary>
        /// IComparer for ArrayList of segments and row/column index
        /// </summary>
        private class SegmentArraySorter : IComparer
        {
            public int Compare(object x, object y)
            {
                object[] aPref1 = (object[])x;
                object[] aPref2 = (object[])y;

                int iIndex1 = int.Parse(aPref1[1].ToString());
                int iIndex2 = int.Parse(aPref2[1].ToString());

                if (iIndex1 == iIndex2)
                    return 0;
                else if (iIndex1 > iIndex2)
                    return 1;
                else
                    return -1;
            }
        }
        #endregion
    }

    public class mpDataGridViewImageCell : DataGridViewImageCell
    {
        private bool m_bLeftBorder = true;
        public mpDataGridViewImageCell() { }

        public void SetLeftBorder(bool bShow)
        {
            m_bLeftBorder = bShow;
        }

        public override DataGridViewAdvancedBorderStyle AdjustCellBorderStyle(
            DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStyleInput,
            DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStylePlaceholder,
            bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded,
            bool isFirstDisplayedColumn, bool isFirstDisplayedRow)
        {
            if (m_bLeftBorder)
            {
                dataGridViewAdvancedBorderStylePlaceholder.Left =
                    DataGridViewAdvancedCellBorderStyle.Single;
            }
            else
            {
                dataGridViewAdvancedBorderStylePlaceholder.Left =
                    DataGridViewAdvancedCellBorderStyle.None;
            }

            dataGridViewAdvancedBorderStylePlaceholder.Top = DataGridViewAdvancedCellBorderStyle.Single;
            dataGridViewAdvancedBorderStylePlaceholder.Bottom = DataGridViewAdvancedCellBorderStyle.Single;

            return dataGridViewAdvancedBorderStylePlaceholder;
        }
    }

    public class mpDataGridViewCell : DataGridViewTextBoxCell
    {
        private bool m_bLeftBorder = true;
        public mpDataGridViewCell() { }

        public void SetLeftBorder(bool bShow)
        {
            m_bLeftBorder = bShow;
        }

        public override DataGridViewAdvancedBorderStyle AdjustCellBorderStyle(
            DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStyleInput,
            DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStylePlaceholder,
            bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded,
            bool isFirstDisplayedColumn, bool isFirstDisplayedRow)
        {
            if (m_bLeftBorder)
            {
                dataGridViewAdvancedBorderStylePlaceholder.Left =
                    DataGridViewAdvancedCellBorderStyle.Single;
            }
            else
            {
                dataGridViewAdvancedBorderStylePlaceholder.Left =
                    DataGridViewAdvancedCellBorderStyle.None;
            }

            dataGridViewAdvancedBorderStylePlaceholder.Top = DataGridViewAdvancedCellBorderStyle.Single;
            dataGridViewAdvancedBorderStylePlaceholder.Bottom = DataGridViewAdvancedCellBorderStyle.Single;

            return dataGridViewAdvancedBorderStylePlaceholder;
        }
    }
}
