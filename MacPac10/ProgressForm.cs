﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class ProgressForm : Form
    {
        private int iTotalNumber;
        private int iCurNumber;
        public bool Cancelled { get; protected set; }
        
        public ProgressForm(int iTotalNumber, int iStartNumber, string xStatusText, string xLabelText, bool bEnableCancelButton)
        {
            InitializeComponent();

            this.TotalNumber = iTotalNumber;
            this.CurrentNumber = iStartNumber;
            this.StatusText = xStatusText;
            this.ProgressLabelText = xLabelText;
            this.EnableCancelButton = bEnableCancelButton;
        }

        public int TotalNumber
        {
            get{return iTotalNumber;}
            set{iTotalNumber = value;}
        }
        public int CurrentNumber
        {
            get { return iCurNumber; }
            set { iCurNumber = value; }
        }
        public string StatusText
        {
            get { return this.lblStatus.Text; }
            set { this.lblStatus.Text = value; }
        }
        public string ProgressLabelText
        {
            get { return this.lblProgress.Text; }
            set { this.lblProgress.Text = value; }
        }

        public bool EnableCancelButton
        {
            get { return this.btnCancel.Enabled; }
            set { this.btnCancel.Enabled = value; }
        }

        public void UpdateProgress(int iCurrentNumber)
        {
            this.CurrentNumber = iCurrentNumber;
            this.prgPercent.Value = (int)(((Double)this.CurrentNumber / (Double)this.TotalNumber) * 100);
            this.Refresh();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Cancelled = true;
        }

    }
}
