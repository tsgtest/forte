using System;
using LMP.Controls;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    internal partial class DataReviewer : System.Windows.Forms.UserControl
    {
        #region *********************fields*********************
        private System.Windows.Forms.Panel pnlDocContents;
        private System.Windows.Forms.ImageList ilImages;
        private System.ComponentModel.IContainer components;
        private LMP.Controls.TreeView treeDocContents;
        private ToolStripMenuItem copyToolStripMenuItem;
        private ToolStripMenuItem saveAsWebPageToolStripMenuItem;
        private ToolStripMenuItem printPreviewToolStripMenuItem;
        private ToolStripMenuItem printToolStripMenuItem;
        private TimedContextMenuStrip mnuQuickHelp;
        private ToolStripMenuItem mnuQuickHelp_Copy;
        private ToolStripMenuItem mnuQuickHelp_SaveAs;
        private ToolStripMenuItem mnuQuickHelp_Print;
        private ToolStripMenuItem mnuQuickHelp_PrintPreview;
        #endregion
        #region *********************Component Designer generated code*********************
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataReviewer));
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override2 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.pnlDocContents = new System.Windows.Forms.Panel();
            this.tsEditor = new System.Windows.Forms.ToolStrip();
            this.tbtnRecreate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnSaveData = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnSaveSegment = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.pnlGuidance = new System.Windows.Forms.Panel();
            this.txtGuidance = new System.Windows.Forms.TextBox();
            this.picMenu = new System.Windows.Forms.PictureBox();
            this.tsReviewer = new System.Windows.Forms.ToolStrip();
            this.tbtnCancel = new System.Windows.Forms.ToolStripButton();
            this.tbtnFinish = new System.Windows.Forms.ToolStripButton();
            this.btnContacts = new System.Windows.Forms.Button();
            this.treeDocContents = new LMP.Controls.TreeView();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.mnuQuickHelp_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQuickHelp_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQuickHelp_PrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQuickHelp_Print = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsWebPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQuickHelp = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.pnlDocContents.SuspendLayout();
            this.tsEditor.SuspendLayout();
            this.pnlGuidance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMenu)).BeginInit();
            this.tsReviewer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeDocContents)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlDocContents
            // 
            this.pnlDocContents.BackColor = System.Drawing.Color.Transparent;
            this.pnlDocContents.Controls.Add(this.tsEditor);
            this.pnlDocContents.Controls.Add(this.pnlGuidance);
            this.pnlDocContents.Controls.Add(this.btnContacts);
            this.pnlDocContents.Controls.Add(this.treeDocContents);
            this.pnlDocContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDocContents.Location = new System.Drawing.Point(0, 0);
            this.pnlDocContents.Name = "pnlDocContents";
            this.pnlDocContents.Size = new System.Drawing.Size(328, 528);
            this.pnlDocContents.TabIndex = 19;
            this.pnlDocContents.SizeChanged += new System.EventHandler(this.pnlDocContents_SizeChanged);
            // 
            // tsEditor
            // 
            this.tsEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsEditor.AutoSize = false;
            this.tsEditor.Dock = System.Windows.Forms.DockStyle.None;
            this.tsEditor.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsEditor.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.tsEditor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnRecreate,
            this.toolStripSeparator8,
            this.tbtnSaveData,
            this.toolStripSeparator7,
            this.tbtnSaveSegment,
            this.toolStripSeparator2});
            this.tsEditor.Location = new System.Drawing.Point(0, 442);
            this.tsEditor.Name = "tsEditor";
            this.tsEditor.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsEditor.Size = new System.Drawing.Size(328, 25);
            this.tsEditor.TabIndex = 61;
            // 
            // tbtnRecreate
            // 
            this.tbtnRecreate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnRecreate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnRecreate.Image = ((System.Drawing.Image)(resources.GetObject("tbtnRecreate.Image")));
            this.tbtnRecreate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnRecreate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnRecreate.Name = "tbtnRecreate";
            this.tbtnRecreate.Size = new System.Drawing.Size(63, 22);
            this.tbtnRecreate.Text = "Recreate";
            this.tbtnRecreate.Click += new System.EventHandler(this.tbtnRecreate_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnSaveData
            // 
            this.tbtnSaveData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSaveData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnSaveData.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSaveData.Image")));
            this.tbtnSaveData.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnSaveData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSaveData.Name = "tbtnSaveData";
            this.tbtnSaveData.Size = new System.Drawing.Size(69, 22);
            this.tbtnSaveData.Text = "Save Data";
            this.tbtnSaveData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tbtnSaveData.Click += new System.EventHandler(this.tbtnSaveData_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnSaveSegment
            // 
            this.tbtnSaveSegment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSaveSegment.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnSaveSegment.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSaveSegment.Name = "tbtnSaveSegment";
            this.tbtnSaveSegment.Size = new System.Drawing.Size(93, 22);
            this.tbtnSaveSegment.Text = "Save Segment";
            this.tbtnSaveSegment.Click += new System.EventHandler(this.tbtnSaveSegment_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // pnlGuidance
            // 
            this.pnlGuidance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlGuidance.BackColor = System.Drawing.Color.White;
            this.pnlGuidance.Controls.Add(this.txtGuidance);
            this.pnlGuidance.Controls.Add(this.picMenu);
            this.pnlGuidance.Controls.Add(this.tsReviewer);
            this.pnlGuidance.Location = new System.Drawing.Point(0, 469);
            this.pnlGuidance.Name = "pnlGuidance";
            this.pnlGuidance.Size = new System.Drawing.Size(326, 59);
            this.pnlGuidance.TabIndex = 60;
            // 
            // txtGuidance
            // 
            this.txtGuidance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGuidance.BackColor = System.Drawing.Color.White;
            this.txtGuidance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGuidance.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGuidance.Location = new System.Drawing.Point(9, 3);
            this.txtGuidance.Multiline = true;
            this.txtGuidance.Name = "txtGuidance";
            this.txtGuidance.ReadOnly = true;
            this.txtGuidance.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGuidance.Size = new System.Drawing.Size(316, 54);
            this.txtGuidance.TabIndex = 61;
            this.txtGuidance.TabStop = false;
            this.txtGuidance.Text = "Please compare and update Finished Data to match manually edited document text, a" +
    "s needed.";
            // 
            // picMenu
            // 
            this.picMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picMenu.Image = ((System.Drawing.Image)(resources.GetObject("picMenu.Image")));
            this.picMenu.Location = new System.Drawing.Point(336, 12);
            this.picMenu.Name = "picMenu";
            this.picMenu.Size = new System.Drawing.Size(11, 11);
            this.picMenu.TabIndex = 49;
            this.picMenu.TabStop = false;
            this.picMenu.Visible = false;
            // 
            // tsReviewer
            // 
            this.tsReviewer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsReviewer.AutoSize = false;
            this.tsReviewer.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsReviewer.Dock = System.Windows.Forms.DockStyle.None;
            this.tsReviewer.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsReviewer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnCancel,
            this.tbtnFinish});
            this.tsReviewer.Location = new System.Drawing.Point(64, 7);
            this.tsReviewer.Name = "tsReviewer";
            this.tsReviewer.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsReviewer.Size = new System.Drawing.Size(160, 25);
            this.tsReviewer.TabIndex = 57;
            this.tsReviewer.Text = "toolStrip1";
            this.tsReviewer.Visible = false;
            // 
            // tbtnCancel
            // 
            this.tbtnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("tbtnCancel.Image")));
            this.tbtnCancel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnCancel.Name = "tbtnCancel";
            this.tbtnCancel.Padding = new System.Windows.Forms.Padding(18, 0, 0, 0);
            this.tbtnCancel.Size = new System.Drawing.Size(66, 22);
            this.tbtnCancel.Text = "&Cancel";
            this.tbtnCancel.Visible = false;
            this.tbtnCancel.Click += new System.EventHandler(this.tbtnCancel_Click);
            // 
            // tbtnFinish
            // 
            this.tbtnFinish.AutoToolTip = false;
            this.tbtnFinish.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnFinish.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnFinish.Image = ((System.Drawing.Image)(resources.GetObject("tbtnFinish.Image")));
            this.tbtnFinish.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnFinish.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnFinish.Name = "tbtnFinish";
            this.tbtnFinish.Padding = new System.Windows.Forms.Padding(0, 0, 6, 0);
            this.tbtnFinish.Size = new System.Drawing.Size(59, 22);
            this.tbtnFinish.Text = "     O&K    ";
            this.tbtnFinish.Click += new System.EventHandler(this.tbtnFinish_Click);
            // 
            // btnContacts
            // 
            this.btnContacts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnContacts.Image = ((System.Drawing.Image)(resources.GetObject("btnContacts.Image")));
            this.btnContacts.Location = new System.Drawing.Point(296, 469);
            this.btnContacts.Name = "btnContacts";
            this.btnContacts.Size = new System.Drawing.Size(25, 22);
            this.btnContacts.TabIndex = 58;
            this.btnContacts.TabStop = false;
            this.btnContacts.UseVisualStyleBackColor = true;
            this.btnContacts.Visible = false;
            // 
            // treeDocContents
            // 
            this.treeDocContents.AllowKeyboardSearch = false;
            this.treeDocContents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.BackColor2 = System.Drawing.Color.White;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.BackwardDiagonal;
            this.treeDocContents.Appearance = appearance6;
            this.treeDocContents.BorderStyle = Infragistics.Win.UIElementBorderStyle.Inset;
            this.treeDocContents.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeDocContents.ImageList = this.ilImages;
            this.treeDocContents.IsDirty = false;
            this.treeDocContents.Location = new System.Drawing.Point(0, 1);
            this.treeDocContents.Name = "treeDocContents";
            appearance7.BackColor = System.Drawing.Color.Transparent;
            appearance7.ForeColor = System.Drawing.Color.Blue;
            _override2.ActiveNodeAppearance = appearance7;
            _override2.AllowCopy = Infragistics.Win.DefaultableBoolean.False;
            _override2.AllowCut = Infragistics.Win.DefaultableBoolean.False;
            _override2.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            _override2.AllowPaste = Infragistics.Win.DefaultableBoolean.False;
            _override2.BorderStyleNode = Infragistics.Win.UIElementBorderStyle.None;
            appearance8.BackColor = System.Drawing.Color.Transparent;
            appearance8.ForeColor = System.Drawing.Color.Black;
            _override2.ExpandedNodeAppearance = appearance8;
            _override2.ItemHeight = 22;
            _override2.Multiline = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.ForeColor = System.Drawing.Color.Black;
            _override2.NodeAppearance = appearance9;
            appearance10.BackColor = System.Drawing.Color.Transparent;
            appearance10.ForeColor = System.Drawing.Color.Black;
            _override2.SelectedNodeAppearance = appearance10;
            this.treeDocContents.Override = _override2;
            this.treeDocContents.ScrollBounds = Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill;
            this.treeDocContents.Size = new System.Drawing.Size(328, 440);
            this.treeDocContents.SupportingValues = "";
            this.treeDocContents.TabIndex = 0;
            this.treeDocContents.Tag2 = null;
            this.treeDocContents.Value = null;
            this.treeDocContents.AfterActivate += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeDocContents_AfterActivate);
            this.treeDocContents.AfterCollapse += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeDocContents_AfterCollapse);
            this.treeDocContents.AfterExpand += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeDocContents_AfterExpand);
            this.treeDocContents.BeforeActivate += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeActivate);
            this.treeDocContents.BeforeCollapse += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeCollapse);
            this.treeDocContents.BeforeExpand += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeExpand);
            this.treeDocContents.Scroll += new Infragistics.Win.UltraWinTree.TreeScrollEventHandler(this.treeDocContents_Scroll);
            this.treeDocContents.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeDocContents_KeyDown);
            this.treeDocContents.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeDocContents_KeyUp);
            this.treeDocContents.Leave += new System.EventHandler(this.treeDocContents_Leave);
            this.treeDocContents.MouseClick += new System.Windows.Forms.MouseEventHandler(this.treeDocContents_MouseClick);
            this.treeDocContents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeDocContents_MouseDown);
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Transparent;
            this.ilImages.Images.SetKeyName(0, "ValidDocument");
            this.ilImages.Images.SetKeyName(1, "InvalidDocumentSegment");
            this.ilImages.Images.SetKeyName(2, "ValidComponentSegment");
            this.ilImages.Images.SetKeyName(3, "InvalidComponentSegment");
            this.ilImages.Images.SetKeyName(4, "ValidTextBlock");
            this.ilImages.Images.SetKeyName(5, "InvalidTextBlock");
            this.ilImages.Images.SetKeyName(6, "ValidVariable");
            this.ilImages.Images.SetKeyName(7, "InvalidVariable");
            this.ilImages.Images.SetKeyName(8, "HotkeyA.bmp");
            this.ilImages.Images.SetKeyName(9, "HotkeyB.bmp");
            this.ilImages.Images.SetKeyName(10, "HotkeyC.bmp");
            this.ilImages.Images.SetKeyName(11, "HotkeyD.bmp");
            this.ilImages.Images.SetKeyName(12, "HotkeyE.bmp");
            this.ilImages.Images.SetKeyName(13, "HotkeyF.bmp");
            this.ilImages.Images.SetKeyName(14, "HotkeyG.bmp");
            this.ilImages.Images.SetKeyName(15, "HotkeyH.bmp");
            this.ilImages.Images.SetKeyName(16, "HotkeyI.bmp");
            this.ilImages.Images.SetKeyName(17, "HotkeyJ.bmp");
            this.ilImages.Images.SetKeyName(18, "HotkeyK.bmp");
            this.ilImages.Images.SetKeyName(19, "HotkeyL.bmp");
            this.ilImages.Images.SetKeyName(20, "HotkeyM.bmp");
            this.ilImages.Images.SetKeyName(21, "HotkeyN.bmp");
            this.ilImages.Images.SetKeyName(22, "HotkeyO.bmp");
            this.ilImages.Images.SetKeyName(23, "HotkeyP.bmp");
            this.ilImages.Images.SetKeyName(24, "HotkeyQ.bmp");
            this.ilImages.Images.SetKeyName(25, "HotkeyR.bmp");
            this.ilImages.Images.SetKeyName(26, "HotkeyS.bmp");
            this.ilImages.Images.SetKeyName(27, "HotkeyT.bmp");
            this.ilImages.Images.SetKeyName(28, "HotkeyU.bmp");
            this.ilImages.Images.SetKeyName(29, "HotkeyV.bmp");
            this.ilImages.Images.SetKeyName(30, "HotkeyW.bmp");
            this.ilImages.Images.SetKeyName(31, "HotkeyX.bmp");
            this.ilImages.Images.SetKeyName(32, "Hotkey1.bmp");
            this.ilImages.Images.SetKeyName(33, "Hotkey2.bmp");
            this.ilImages.Images.SetKeyName(34, "Hotkey3.bmp");
            this.ilImages.Images.SetKeyName(35, "Hotkey4.bmp");
            this.ilImages.Images.SetKeyName(36, "Hotkey5.bmp");
            this.ilImages.Images.SetKeyName(37, "Hotkey6.bmp");
            this.ilImages.Images.SetKeyName(38, "Hotkey7.bmp");
            this.ilImages.Images.SetKeyName(39, "Hotkey8.bmp");
            this.ilImages.Images.SetKeyName(40, "Hotkey9.bmp");
            this.ilImages.Images.SetKeyName(41, "IncompleteVariable");
            this.ilImages.Images.SetKeyName(42, "IncompleteDocumentSegment");
            this.ilImages.Images.SetKeyName(43, "IncompleteDocumentComponent");
            this.ilImages.Images.SetKeyName(44, "IncompleteTextBlockSegment");
            // 
            // mnuQuickHelp_Copy
            // 
            this.mnuQuickHelp_Copy.Name = "mnuQuickHelp_Copy";
            this.mnuQuickHelp_Copy.Size = new System.Drawing.Size(32, 19);
            // 
            // mnuQuickHelp_SaveAs
            // 
            this.mnuQuickHelp_SaveAs.Name = "mnuQuickHelp_SaveAs";
            this.mnuQuickHelp_SaveAs.Size = new System.Drawing.Size(32, 19);
            // 
            // mnuQuickHelp_PrintPreview
            // 
            this.mnuQuickHelp_PrintPreview.Name = "mnuQuickHelp_PrintPreview";
            this.mnuQuickHelp_PrintPreview.Size = new System.Drawing.Size(32, 19);
            // 
            // mnuQuickHelp_Print
            // 
            this.mnuQuickHelp_Print.Name = "mnuQuickHelp_Print";
            this.mnuQuickHelp_Print.Size = new System.Drawing.Size(32, 19);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // saveAsWebPageToolStripMenuItem
            // 
            this.saveAsWebPageToolStripMenuItem.Name = "saveAsWebPageToolStripMenuItem";
            this.saveAsWebPageToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // mnuQuickHelp
            // 
            this.mnuQuickHelp.Name = "mnuQuickHelp";
            this.mnuQuickHelp.Size = new System.Drawing.Size(61, 4);
            this.mnuQuickHelp.TimerInterval = 500;
            this.mnuQuickHelp.UseTimer = true;
            // 
            // DataReviewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnlDocContents);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DataReviewer";
            this.Size = new System.Drawing.Size(328, 528);
            this.Load += new System.EventHandler(this.DataReviewer_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataReviewer_KeyDown);
            this.Leave += new System.EventHandler(this.DataReviewer_Leave);
            this.Resize += new System.EventHandler(this.DataReviewer_Resize);
            this.pnlDocContents.ResumeLayout(false);
            this.tsEditor.ResumeLayout(false);
            this.tsEditor.PerformLayout();
            this.pnlGuidance.ResumeLayout(false);
            this.pnlGuidance.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMenu)).EndInit();
            this.tsReviewer.ResumeLayout(false);
            this.tsReviewer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeDocContents)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private ToolStrip tsReviewer;
        private ToolStripButton tbtnFinish;
        private Button btnContacts;
        private ToolStripButton tbtnCancel;
        private Panel pnlGuidance;
        private PictureBox picMenu;
        private System.Windows.Forms.TextBox txtGuidance;
        private ToolStrip tsEditor;
        private ToolStripButton tbtnSaveData;
        private ToolStripSeparator toolStripSeparator7;
        private ToolStripButton tbtnRecreate;
        private ToolStripSeparator toolStripSeparator8;
        private ToolStripButton tbtnSaveSegment;
        private ToolStripSeparator toolStripSeparator2;


    }
}
