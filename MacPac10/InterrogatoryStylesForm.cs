﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class InterrogatoryStylesForm : Form
    {
        private bool m_bIsDirty;
        private string m_xUserStyleDefs;
        private string m_xFirmStyleDefs;
        private bool m_bNoUserStylesDefined;
        private string[] m_aUserStyleDefs = new string[6];
        private string[] m_aFirmStyleDefs = new string[6];

        public InterrogatoryStylesForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveStyles();
        }

        private void chkAllCaps_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.chkAllCaps.Checked)
                    this.chkSmallCaps.Checked = false;

                MarkDirty();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkBoldText_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                MarkDirty();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkSmallCaps_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.chkSmallCaps.Checked)
                    this.chkAllCaps.Checked = false;

                MarkDirty();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkUnderlined_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                MarkDirty();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void UpdateSaveView()
        {
            this.btnSave.Enabled = m_bIsDirty;
        }

        private void MarkDirty()
        {
            m_bIsDirty = true;
            UpdateSaveView();
        }

        private void UpdateDocumentStyle()
        {
            // JAB TODO: Implement
        }

        private void SaveStyles()
        {
            if (m_bIsDirty)
            {
                string[] aRogTypes = { "QuestionHeading", "QuestionText", "ResponseHeading", "ResponseText", "RunInDiscovery", "RunInResponse" };

                string xDef = aRogTypes[this.lstStyles.SelectedIndex] + ',' + 
                    this.lstStyles.Items[this.lstStyles.SelectedIndex].ToString() + ',' + 
                    (this.cmbLineSpacing.Text == "Double" ? "2" : "1") + ',' + this.FirstLineIndent + ',' +
                    this.SpaceAfter + ',' + this.SpaceBefore + ',' + this.Bold + ',' + this.Underline + ',' +
                    this.AllCaps + ',' + this.SmallCaps;

                string xStyles = "";

                if (m_bNoUserStylesDefined)
                {
                    //create a user set of rog style defs -
                    //define only the one that has been modified
                    for (int i = 0; i < 6; i++)
                    {
                        if (i == this.lstStyles.SelectedIndex)
                        {
                            xStyles += xDef + ";";
                        }
                        else
                        {
                            xStyles += "NOT DEFINED" + ";";
                        }
                    }

                    m_xUserStyleDefs = xStyles.TrimEnd(';');
                    m_aUserStyleDefs = m_xUserStyleDefs.Split(';');
                }
                else
                {
                    //modify the existing user set of rog style defs
                    m_aUserStyleDefs[this.lstStyles.SelectedIndex] = xDef;
                    m_xUserStyleDefs = string.Join(";", m_aUserStyleDefs);
                }

                //save the style defs
                LMP.MacPac.Session.CurrentUser.UserSettings.InterrogatoryStyles = m_xUserStyleDefs;

                this.m_bIsDirty = false;
            }
        }

        public string FirstLineIndent
        {
            get
            {
                //GLOG 6966: Make sure ToString doesn't convert decimal separator
                return spnFirstLineIndent.Value.ToString(LMP.Culture.USEnglishCulture);
            }
        }

        public string SpaceAfter
        {
            get
            {
                return spnSpaceAfter.Value.ToString();
            }
        }

        public string SpaceBefore
        {
            get
            {
                return spnSpaceBefore.Value.ToString();
            }
        }

        public string Bold
        {
            get
            {
                return chkBold.Checked.ToString();
            }
        }

        public string Underline
        {
            get
            {
                return this.chkUnderlined.Checked.ToString();
            }
        }

        public string AllCaps
        {
            get
            {
                // JAB TODO: Confirm: Should this be a string and not a bool?
                return this.chkAllCaps.Checked.ToString();
            }
        }

        public string SmallCaps
        {
            get
            {
                return this.chkSmallCaps.Checked.ToString();
            }
        }

        private void InterrogatoryStylesForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadStyles();

                //load dropdown values into line spacing combo
                this.cmbLineSpacing.Items.Add("Single");
                this.cmbLineSpacing.Items.Add("Double");

                this.lstStyles.SelectedIndex = 0;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void LoadStyles()
        {
            m_xFirmStyleDefs = (new LMP.Data.FirmApplicationSettings(LMP.MacPac.Session.CurrentUser.ID)).InterrogatoryStyleDefs;
            m_aFirmStyleDefs = m_xFirmStyleDefs.Split(';');

            m_xUserStyleDefs = LMP.MacPac.Session.CurrentUser.UserSettings.InterrogatoryStyles;
            m_bNoUserStylesDefined = m_xUserStyleDefs == m_xFirmStyleDefs;

            string[] xStyleDefs = m_xUserStyleDefs.Split(';');
            string[] aStyleNames = new string[6];

            for (int i = 0; i < 6; i++)
            {
                //get style name - use the firm names
                //since some user styles won't have any definitions -
                //i.e. the user might have modified only one
                //of the firm styles
                string xStyleDef = m_aFirmStyleDefs[i];
                string[] aStyleDef = xStyleDef.Split(',');
                aStyleNames[i] = aStyleDef[1];

                //fill the def array with user definitions-
                //if there are none, these will contain
                //the firm definitions
                m_aUserStyleDefs[i] = xStyleDefs[i];
            }

            Array.Sort(aStyleNames);

            this.lstStyles.Items.AddRange(aStyleNames);
        }

        private void DisplayStyleDefinition(int iStyleIndex)
        {
            string xStyleDef = m_aUserStyleDefs[iStyleIndex];
            string[] aStyleDef = null;

            if (xStyleDef == "NOT DEFINED")
            {
                //user style has not been defined -
                //display the firm definition
                xStyleDef = m_aFirmStyleDefs[iStyleIndex];
            }

            aStyleDef = xStyleDef.Split(',');

            this.cmbLineSpacing.SelectedIndex = int.Parse(aStyleDef[(int)LMP.Forte.MSWord.Interrogatories.mpRogStyleFormats.mpRogStyleFormats_LineSpacingIndex]) - 1;
            //GLOG 6966: Make sure values are parsed using '.' as decimal separator
            this.spnFirstLineIndent.Value = decimal.Parse(aStyleDef[(int)LMP.Forte.MSWord.Interrogatories.mpRogStyleFormats.mpRogStyleFormats_FirstLineIndentIndex], LMP.Culture.USEnglishCulture);
            this.spnSpaceAfter.Value = decimal.Parse(aStyleDef[(int)LMP.Forte.MSWord.Interrogatories.mpRogStyleFormats.mpRogStyleFormats_SpaceAfterIndex], LMP.Culture.USEnglishCulture);
            this.spnSpaceBefore.Value = decimal.Parse(aStyleDef[(int)LMP.Forte.MSWord.Interrogatories.mpRogStyleFormats.mpRogStyleFormats_SpaceBeforeIndex], LMP.Culture.USEnglishCulture);
            this.chkBold.Checked = bool.Parse(aStyleDef[(int)LMP.Forte.MSWord.Interrogatories.mpRogStyleFormats.mpRogStyleFormats_BoldIndex]);
            this.chkUnderlined.Checked = bool.Parse(aStyleDef[(int)LMP.Forte.MSWord.Interrogatories.mpRogStyleFormats.mpRogStyleFormats_UnderlineIndex]);
            this.chkAllCaps.Checked = bool.Parse(aStyleDef[(int)LMP.Forte.MSWord.Interrogatories.mpRogStyleFormats.mpRogStyleFormats_AllCapsIndex]);
            this.chkSmallCaps.Checked = bool.Parse(aStyleDef[(int)LMP.Forte.MSWord.Interrogatories.mpRogStyleFormats.mpRogStyleFormats_SmallCapsIndex]);

            this.m_bIsDirty = false;
        }

        private void lstStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DisplayStyleDefinition(this.lstStyles.SelectedIndex);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void spnFirstLineIndent_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                MarkDirty();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void cmbLineSpacing_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                MarkDirty();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void spnSpaceBefore_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                MarkDirty();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void spnSpaceAfter_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                MarkDirty();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (!m_bNoUserStylesDefined)
                {
                    string xStyles = null;

                    //cycle through style defs,
                    //removing the one specified
                    for (int i = 0; i < 6; i++)
                    {
                        if (i == this.lstStyles.SelectedIndex)
                        {
                            //remove definition
                            xStyles += "NOT DEFINED" + ";";
                        }
                        else
                        {
                            //keep the existing definition, if any
                            xStyles += m_aUserStyleDefs[i] + ";";
                        }
                    }

                    m_xUserStyleDefs = xStyles.TrimEnd(';');
                    m_aUserStyleDefs = m_xUserStyleDefs.Split(';');

                    //save the style defs
                    LMP.MacPac.Session.CurrentUser.UserSettings.InterrogatoryStyles = m_xUserStyleDefs;

                    this.m_bIsDirty = false;

                    //refresh display
                    DisplayStyleDefinition(this.lstStyles.SelectedIndex);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkAllCaps_CheckedChanged_1(object sender, EventArgs e)
        {

        }
    }
}
