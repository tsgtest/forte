namespace LMP.MacPac
{
    partial class FontForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.lstFontNames = new System.Windows.Forms.ListBox();
            this.lblFontName = new System.Windows.Forms.Label();
            this.lblFontSize = new System.Windows.Forms.Label();
            this.lstFontSize = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(266, 271);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 25);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnInsert.AutoSize = true;
            this.btnInsert.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnInsert.Location = new System.Drawing.Point(175, 271);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(81, 25);
            this.btnInsert.TabIndex = 4;
            this.btnInsert.Text = "O&K";
            this.btnInsert.UseVisualStyleBackColor = true;
            // 
            // lstFontNames
            // 
            this.lstFontNames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstFontNames.FormattingEnabled = true;
            this.lstFontNames.ItemHeight = 15;
            this.lstFontNames.Location = new System.Drawing.Point(12, 31);
            this.lstFontNames.Name = "lstFontNames";
            this.lstFontNames.Size = new System.Drawing.Size(272, 229);
            this.lstFontNames.TabIndex = 1;
            this.lstFontNames.SelectedIndexChanged += new System.EventHandler(this.lstFontNames_SelectedIndexChanged);
            this.lstFontNames.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstFontNames_KeyPress);
            // 
            // lblFontName
            // 
            this.lblFontName.AutoSize = true;
            this.lblFontName.BackColor = System.Drawing.Color.Transparent;
            this.lblFontName.ForeColor = System.Drawing.Color.Black;
            this.lblFontName.Location = new System.Drawing.Point(9, 11);
            this.lblFontName.Name = "lblFontName";
            this.lblFontName.Size = new System.Drawing.Size(38, 15);
            this.lblFontName.TabIndex = 0;
            this.lblFontName.Text = "&Name:";
            // 
            // lblFontSize
            // 
            this.lblFontSize.AutoSize = true;
            this.lblFontSize.BackColor = System.Drawing.Color.Transparent;
            this.lblFontSize.ForeColor = System.Drawing.Color.Black;
            this.lblFontSize.Location = new System.Drawing.Point(286, 11);
            this.lblFontSize.Name = "lblFontSize";
            this.lblFontSize.Size = new System.Drawing.Size(31, 15);
            this.lblFontSize.TabIndex = 2;
            this.lblFontSize.Text = "&Size:";
            // 
            // lstFontSize
            // 
            this.lstFontSize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstFontSize.FormattingEnabled = true;
            this.lstFontSize.ItemHeight = 15;
            this.lstFontSize.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.lstFontSize.Location = new System.Drawing.Point(290, 31);
            this.lstFontSize.Name = "lstFontSize";
            this.lstFontSize.Size = new System.Drawing.Size(57, 229);
            this.lstFontSize.TabIndex = 3;
            this.lstFontSize.SelectedIndexChanged += new System.EventHandler(this.lstFontSize_SelectedIndexChanged);
            // 
            // FontForm
            // 
            this.AcceptButton = this.btnInsert;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(359, 310);
            this.Controls.Add(this.lstFontSize);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.lstFontNames);
            this.Controls.Add(this.lblFontName);
            this.Controls.Add(this.lblFontSize);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FontForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Document Font";
            this.Load += new System.EventHandler(this.DocumentFont_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.ListBox lstFontNames;
        private System.Windows.Forms.Label lblFontName;
        private System.Windows.Forms.Label lblFontSize;
        private System.Windows.Forms.ListBox lstFontSize;
    }
}