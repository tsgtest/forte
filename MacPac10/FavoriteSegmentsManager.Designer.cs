namespace LMP.MacPac
{
    partial class FavoriteSegmentsManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer componentsOLD = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FavoriteSegmentsManager));
            this.lstSelectedSegments = new System.Windows.Forms.ListBox();
            this.btnAddSegment = new System.Windows.Forms.Button();
            this.btnMoveSegmentUp = new System.Windows.Forms.Button();
            this.btnMoveSegmentDown = new System.Windows.Forms.Button();
            this.btnDeleteSegment = new System.Windows.Forms.Button();
            this.dsSelectedSegments = new System.Data.DataSet();
            this.dtDetails = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.lblDefaultInsertionLocation = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ftvSegments = new LMP.Controls.SearchableFolderTreeView();
            this.lblSegments = new System.Windows.Forms.Label();
            this.defaultLocationComboBox1 = new LMP.Controls.DefaultLocationComboBox();
            this.lblSelectedSegments = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dsSelectedSegments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstSelectedSegments
            // 
            this.lstSelectedSegments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstSelectedSegments.FormattingEnabled = true;
            this.lstSelectedSegments.HorizontalScrollbar = true;
            this.lstSelectedSegments.IntegralHeight = false;
            this.lstSelectedSegments.ItemHeight = 15;
            this.lstSelectedSegments.Location = new System.Drawing.Point(10, 28);
            this.lstSelectedSegments.Name = "lstSelectedSegments";
            this.lstSelectedSegments.Size = new System.Drawing.Size(151, 321);
            this.lstSelectedSegments.TabIndex = 3;
            this.lstSelectedSegments.SelectedIndexChanged += new System.EventHandler(this.lstSelectedSegments_SelectedIndexChanged);
            this.lstSelectedSegments.DoubleClick += new System.EventHandler(this.lstSelectedSegments_DoubleClick);
            // 
            // btnAddSegment
            // 
            this.btnAddSegment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddSegment.Location = new System.Drawing.Point(72, 369);
            this.btnAddSegment.Name = "btnAddSegment";
            this.btnAddSegment.Size = new System.Drawing.Size(103, 23);
            this.btnAddSegment.TabIndex = 1;
            this.btnAddSegment.Text = "&Add Favorite";
            this.btnAddSegment.UseVisualStyleBackColor = true;
            this.btnAddSegment.Click += new System.EventHandler(this.btnAddSegment_Click);
            // 
            // btnMoveSegmentUp
            // 
            this.btnMoveSegmentUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveSegmentUp.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveSegmentUp.Image")));
            this.btnMoveSegmentUp.Location = new System.Drawing.Point(164, 27);
            this.btnMoveSegmentUp.Name = "btnMoveSegmentUp";
            this.btnMoveSegmentUp.Size = new System.Drawing.Size(24, 22);
            this.btnMoveSegmentUp.TabIndex = 8;
            this.btnMoveSegmentUp.TabStop = false;
            this.btnMoveSegmentUp.UseVisualStyleBackColor = true;
            this.btnMoveSegmentUp.Click += new System.EventHandler(this.btnMoveSegmentUp_Click);
            // 
            // btnMoveSegmentDown
            // 
            this.btnMoveSegmentDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveSegmentDown.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveSegmentDown.Image")));
            this.btnMoveSegmentDown.Location = new System.Drawing.Point(164, 53);
            this.btnMoveSegmentDown.Name = "btnMoveSegmentDown";
            this.btnMoveSegmentDown.Size = new System.Drawing.Size(24, 22);
            this.btnMoveSegmentDown.TabIndex = 9;
            this.btnMoveSegmentDown.TabStop = false;
            this.btnMoveSegmentDown.UseVisualStyleBackColor = true;
            this.btnMoveSegmentDown.Click += new System.EventHandler(this.btnMoveSegmentDown_Click);
            // 
            // btnDeleteSegment
            // 
            this.btnDeleteSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteSegment.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSegment.Image")));
            this.btnDeleteSegment.Location = new System.Drawing.Point(164, 79);
            this.btnDeleteSegment.Name = "btnDeleteSegment";
            this.btnDeleteSegment.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteSegment.TabIndex = 3;
            this.btnDeleteSegment.TabStop = false;
            this.btnDeleteSegment.UseVisualStyleBackColor = true;
            this.btnDeleteSegment.Click += new System.EventHandler(this.btnDeleteSegment_Click);
            // 
            // dsSelectedSegments
            // 
            this.dsSelectedSegments.DataSetName = "NewDataSet";
            this.dsSelectedSegments.Tables.AddRange(new System.Data.DataTable[] {
            this.dtDetails});
            // 
            // dtDetails
            // 
            this.dtDetails.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1});
            this.dtDetails.TableName = "dtDetails";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Segment Name";
            this.dataColumn1.ColumnName = "dataColumnSegmentName";
            // 
            // lblDefaultInsertionLocation
            // 
            this.lblDefaultInsertionLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDefaultInsertionLocation.AutoSize = true;
            this.lblDefaultInsertionLocation.BackColor = System.Drawing.Color.Transparent;
            this.lblDefaultInsertionLocation.Location = new System.Drawing.Point(8, 355);
            this.lblDefaultInsertionLocation.Name = "lblDefaultInsertionLocation";
            this.lblDefaultInsertionLocation.Size = new System.Drawing.Size(145, 15);
            this.lblDefaultInsertionLocation.TabIndex = 4;
            this.lblDefaultInsertionLocation.Text = "#Default Insertion Location:#";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ftvSegments);
            this.splitContainer1.Panel1.Controls.Add(this.btnAddSegment);
            this.splitContainer1.Panel1.Controls.Add(this.lblSegments);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnDeleteSegment);
            this.splitContainer1.Panel2.Controls.Add(this.btnMoveSegmentDown);
            this.splitContainer1.Panel2.Controls.Add(this.lstSelectedSegments);
            this.splitContainer1.Panel2.Controls.Add(this.btnMoveSegmentUp);
            this.splitContainer1.Panel2.Controls.Add(this.defaultLocationComboBox1);
            this.splitContainer1.Panel2.Controls.Add(this.lblDefaultInsertionLocation);
            this.splitContainer1.Panel2.Controls.Add(this.lblSelectedSegments);
            this.splitContainer1.Size = new System.Drawing.Size(454, 404);
            this.splitContainer1.SplitterDistance = 255;
            this.splitContainer1.TabIndex = 11;
            // 
            // ftvSegments
            // 
            this.ftvSegments.AllowFavoritesMenu = false;
            this.ftvSegments.AllowSearching = false;
            this.ftvSegments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ftvSegments.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ftvSegments.DisableExcludedTypes = false;
            this.ftvSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
            this.ftvSegments.ExcludeNonMacPacTypes = false;
            this.ftvSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvSegments.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ftvSegments.Location = new System.Drawing.Point(-3, -2);
            this.ftvSegments.Name = "ftvSegments";
            this.ftvSegments.OwnerID = 0;
            this.ftvSegments.ShowCheckboxes = false;
            this.ftvSegments.Size = new System.Drawing.Size(259, 353);
            this.ftvSegments.TabIndex = 0;
            this.ftvSegments.AfterSelect += new LMP.Controls.AfterSelectEventHandler(this.ftvSegments_AfterSelect);
            this.ftvSegments.DoubleClick += new System.EventHandler(this.ftvSegments_DoubleClick);
            // 
            // lblSegments
            // 
            this.lblSegments.AutoSize = true;
            this.lblSegments.BackColor = System.Drawing.Color.Transparent;
            this.lblSegments.Location = new System.Drawing.Point(3, 7);
            this.lblSegments.Name = "lblSegments";
            this.lblSegments.Size = new System.Drawing.Size(58, 15);
            this.lblSegments.TabIndex = 11;
            this.lblSegments.Text = "Segments:";
            // 
            // defaultLocationComboBox1
            // 
            this.defaultLocationComboBox1.AllowEmptyValue = false;
            this.defaultLocationComboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultLocationComboBox1.AutoSize = true;
            this.defaultLocationComboBox1.Borderless = false;
            this.defaultLocationComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.defaultLocationComboBox1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defaultLocationComboBox1.IsDirty = false;
            this.defaultLocationComboBox1.LimitToList = true;
            this.defaultLocationComboBox1.ListName = "";
            this.defaultLocationComboBox1.Location = new System.Drawing.Point(10, 371);
            this.defaultLocationComboBox1.MaxDropDownItems = 8;
            this.defaultLocationComboBox1.Name = "defaultLocationComboBox1";
            this.defaultLocationComboBox1.SelectedIndex = 0;
            this.defaultLocationComboBox1.SelectedValue = "1";
            this.defaultLocationComboBox1.SelectionLength = 24;
            this.defaultLocationComboBox1.SelectionStart = 0;
            this.defaultLocationComboBox1.Size = new System.Drawing.Size(151, 26);
            this.defaultLocationComboBox1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.defaultLocationComboBox1.SupportingValues = "";
            this.defaultLocationComboBox1.TabIndex = 5;
            this.defaultLocationComboBox1.Tag2 = null;
            this.defaultLocationComboBox1.Value = "1";
            this.defaultLocationComboBox1.ValueChanged += new LMP.Controls.ValueChangedHandler(this.defaultLocationComboBox1_ValueChanged);
            // 
            // lblSelectedSegments
            // 
            this.lblSelectedSegments.AutoSize = true;
            this.lblSelectedSegments.BackColor = System.Drawing.Color.Transparent;
            this.lblSelectedSegments.Location = new System.Drawing.Point(11, 12);
            this.lblSelectedSegments.Name = "lblSelectedSegments";
            this.lblSelectedSegments.Size = new System.Drawing.Size(101, 15);
            this.lblSelectedSegments.TabIndex = 2;
            this.lblSelectedSegments.Text = "Favorite Segments:";
            // 
            // FavoriteSegmentsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.splitContainer1);
            this.Name = "FavoriteSegmentsManager";
            this.Load += new System.EventHandler(this.FavoriteSegmentsManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsSelectedSegments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDetails)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Button btnAddSegment;
        private System.Windows.Forms.Button btnMoveSegmentUp;
        private System.Windows.Forms.Button btnMoveSegmentDown;
        private System.Windows.Forms.Button btnDeleteSegment;
        private System.Windows.Forms.ListBox lstSelectedSegments;
        private LMP.Controls.DefaultLocationComboBox defaultLocationComboBox1;
        private System.Windows.Forms.Label lblDefaultInsertionLocation;
        private LMP.Controls.SearchableFolderTreeView ftvSegments;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblSegments;
        private System.Windows.Forms.Label lblSelectedSegments;
    }
}
