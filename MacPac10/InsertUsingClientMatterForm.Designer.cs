﻿namespace LMP.MacPac
{
    partial class InsertUsingClientMatterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InsertUsingClientMatterForm));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.grpOptions = new System.Windows.Forms.GroupBox();
            this.chkRestartNumbering = new System.Windows.Forms.CheckBox();
            this.chkKeepExisting = new System.Windows.Forms.CheckBox();
            this.chkSeparateSection = new System.Windows.Forms.CheckBox();
            this.lblLocation = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbLocation = new LMP.Controls.ComboBox();
            this.cmMain = new LMP.Grail.ClientMatterLookupControl();
            this.grpOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(495, 340);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Enabled = false;
            this.btnOK.Location = new System.Drawing.Point(413, 340);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Fuchsia;
            this.ilImages.Images.SetKeyName(0, "Search");
            // 
            // grpOptions
            // 
            this.grpOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpOptions.Controls.Add(this.chkRestartNumbering);
            this.grpOptions.Controls.Add(this.chkKeepExisting);
            this.grpOptions.Controls.Add(this.chkSeparateSection);
            this.grpOptions.Location = new System.Drawing.Point(355, 55);
            this.grpOptions.Name = "grpOptions";
            this.grpOptions.Size = new System.Drawing.Size(215, 97);
            this.grpOptions.TabIndex = 10;
            this.grpOptions.TabStop = false;
            this.grpOptions.Text = "Insertion Options";
            // 
            // chkRestartNumbering
            // 
            this.chkRestartNumbering.AutoSize = true;
            this.chkRestartNumbering.Location = new System.Drawing.Point(16, 68);
            this.chkRestartNumbering.Name = "chkRestartNumbering";
            this.chkRestartNumbering.Size = new System.Drawing.Size(142, 17);
            this.chkRestartNumbering.TabIndex = 6;
            this.chkRestartNumbering.Text = "&Restart Page Numbering";
            this.chkRestartNumbering.UseVisualStyleBackColor = true;
            // 
            // chkKeepExisting
            // 
            this.chkKeepExisting.AutoSize = true;
            this.chkKeepExisting.Location = new System.Drawing.Point(16, 44);
            this.chkKeepExisting.Name = "chkKeepExisting";
            this.chkKeepExisting.Size = new System.Drawing.Size(173, 17);
            this.chkKeepExisting.TabIndex = 5;
            this.chkKeepExisting.Text = "Keep &Existing Headers/Footers";
            this.chkKeepExisting.UseVisualStyleBackColor = true;
            // 
            // chkSeparateSection
            // 
            this.chkSeparateSection.AutoSize = true;
            this.chkSeparateSection.Location = new System.Drawing.Point(16, 22);
            this.chkSeparateSection.Name = "chkSeparateSection";
            this.chkSeparateSection.Size = new System.Drawing.Size(148, 17);
            this.chkSeparateSection.TabIndex = 4;
            this.chkSeparateSection.Text = "Insert in &Separate Section";
            this.chkSeparateSection.UseVisualStyleBackColor = true;
            // 
            // lblLocation
            // 
            this.lblLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(353, 13);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(94, 13);
            this.lblLocation.TabIndex = 9;
            this.lblLocation.Text = "Insertion &Location:";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Number";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 155;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "ClientName";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Number";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "ClientName";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // cmbLocation
            // 
            this.cmbLocation.AllowEmptyValue = false;
            this.cmbLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLocation.Borderless = false;
            this.cmbLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLocation.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLocation.IsDirty = false;
            this.cmbLocation.LimitToList = true;
            this.cmbLocation.ListName = "";
            this.cmbLocation.Location = new System.Drawing.Point(355, 29);
            this.cmbLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbLocation.MaxDropDownItems = 8;
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.SelectedIndex = -1;
            this.cmbLocation.SelectedValue = null;
            this.cmbLocation.SelectionLength = 0;
            this.cmbLocation.SelectionStart = 0;
            this.cmbLocation.Size = new System.Drawing.Size(215, 23);
            this.cmbLocation.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLocation.SupportingValues = "";
            this.cmbLocation.TabIndex = 13;
            this.cmbLocation.Tag2 = null;
            this.cmbLocation.Value = "";
            // 
            // cmMain
            // 
            this.cmMain.Filter = null;
            this.cmMain.Location = new System.Drawing.Point(14, 16);
            this.cmMain.Name = "cmMain";
            this.cmMain.Size = new System.Drawing.Size(333, 341);
            this.cmMain.TabIndex = 14;
            // 
            // InsertUsingClientMatterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(583, 377);
            this.Controls.Add(this.cmMain);
            this.Controls.Add(this.cmbLocation);
            this.Controls.Add(this.grpOptions);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertUsingClientMatterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lookup Client Matter";
            this.Load += new System.EventHandler(this.InsertUsingClientMatterForm_Load);
            this.grpOptions.ResumeLayout(false);
            this.grpOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientNameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatterNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatterNameCol;
        private System.Windows.Forms.ImageList ilImages;
        private System.Windows.Forms.GroupBox grpOptions;
        private System.Windows.Forms.CheckBox chkRestartNumbering;
        private System.Windows.Forms.CheckBox chkKeepExisting;
        private System.Windows.Forms.CheckBox chkSeparateSection;
        private System.Windows.Forms.Label lblLocation;
        private LMP.Controls.ComboBox cmbLocation;
        private Grail.ClientMatterLookupControl cmMain;
    }
}