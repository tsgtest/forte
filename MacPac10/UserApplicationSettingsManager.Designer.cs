namespace LMP.MacPac
{
    partial class UserApplicationSettingsManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkDisplayTaskPaneForNonMacPacDoc = new System.Windows.Forms.CheckBox();
            this.chkDisplayTaskPaneForMacPacDoc = new System.Windows.Forms.CheckBox();
            this.chkShowHelp = new System.Windows.Forms.CheckBox();
            this.chkShowLocationTooltips = new System.Windows.Forms.CheckBox();
            this.chkTaskPaneUsesGradient = new System.Windows.Forms.CheckBox();
            this.lblTaskPaneBackgroundColor = new System.Windows.Forms.Label();
            this.colorDlgTaskPaneBackgroundColor = new System.Windows.Forms.ColorDialog();
            this.pnlTaskPaneColorChip = new System.Windows.Forms.Panel();
            this.btnPickColor = new System.Windows.Forms.Button();
            this.grpTaskPane = new System.Windows.Forms.GroupBox();
            this.chkShowTaskpaneWhenNoVariables = new System.Windows.Forms.CheckBox();
            this.chkExpandFindResults = new System.Windows.Forms.CheckBox();
            this.chkWarnOnFinish = new System.Windows.Forms.CheckBox();
            this.chkCloseTaskPaneOnFinish = new System.Windows.Forms.CheckBox();
            this.chkShowTaskPaneOnBlankNew = new System.Windows.Forms.CheckBox();
            this.chkFitToPageWidth = new System.Windows.Forms.CheckBox();
            this.grpMore = new System.Windows.Forms.GroupBox();
            this.chkUpdateZoom = new System.Windows.Forms.CheckBox();
            this.lblDefaultZoomPercentage = new System.Windows.Forms.Label();
            this.spnDefaultZoomPercentage = new System.Windows.Forms.NumericUpDown();
            this.chkActivateMacPacRibbonTabOnOpen = new System.Windows.Forms.CheckBox();
            this.chkActivateMacPacRibbonTab = new System.Windows.Forms.CheckBox();
            this.chkShowXMLTags = new System.Windows.Forms.CheckBox();
            this.chkWarnBeforeDeactivation = new System.Windows.Forms.CheckBox();
            this.grpTaskPane.SuspendLayout();
            this.grpMore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spnDefaultZoomPercentage)).BeginInit();
            this.SuspendLayout();
            // 
            // chkDisplayTaskPaneForNonMacPacDoc
            // 
            this.chkDisplayTaskPaneForNonMacPacDoc.AutoSize = true;
            this.chkDisplayTaskPaneForNonMacPacDoc.Location = new System.Drawing.Point(16, 40);
            this.chkDisplayTaskPaneForNonMacPacDoc.Margin = new System.Windows.Forms.Padding(2);
            this.chkDisplayTaskPaneForNonMacPacDoc.Name = "chkDisplayTaskPaneForNonMacPacDoc";
            this.chkDisplayTaskPaneForNonMacPacDoc.Size = new System.Drawing.Size(383, 19);
            this.chkDisplayTaskPaneForNonMacPacDoc.TabIndex = 1;
            this.chkDisplayTaskPaneForNonMacPacDoc.Text = "Sho&w task pane when a document not containing Forte content is opened.";
            this.chkDisplayTaskPaneForNonMacPacDoc.CheckedChanged += new System.EventHandler(this.chkDisplayTaskPaneForNonMacPacDoc_CheckedChanged);
            // 
            // chkDisplayTaskPaneForMacPacDoc
            // 
            this.chkDisplayTaskPaneForMacPacDoc.AutoSize = true;
            this.chkDisplayTaskPaneForMacPacDoc.Location = new System.Drawing.Point(16, 19);
            this.chkDisplayTaskPaneForMacPacDoc.Margin = new System.Windows.Forms.Padding(2);
            this.chkDisplayTaskPaneForMacPacDoc.Name = "chkDisplayTaskPaneForMacPacDoc";
            this.chkDisplayTaskPaneForMacPacDoc.Size = new System.Drawing.Size(379, 19);
            this.chkDisplayTaskPaneForMacPacDoc.TabIndex = 0;
            this.chkDisplayTaskPaneForMacPacDoc.Text = "&Show task pane when a document containing MacPac content is opened.";
            this.chkDisplayTaskPaneForMacPacDoc.CheckedChanged += new System.EventHandler(this.chkDisplayTaskPaneForMacPacDoc_CheckedChanged);
            // 
            // chkShowHelp
            // 
            this.chkShowHelp.AutoSize = true;
            this.chkShowHelp.Location = new System.Drawing.Point(16, 93);
            this.chkShowHelp.Margin = new System.Windows.Forms.Padding(2);
            this.chkShowHelp.Name = "chkShowHelp";
            this.chkShowHelp.Size = new System.Drawing.Size(113, 19);
            this.chkShowHelp.TabIndex = 3;
            this.chkShowHelp.Text = "Show &Quick Help.";
            this.chkShowHelp.CheckedChanged += new System.EventHandler(this.chkShowHelp_CheckedChanged);
            // 
            // chkShowLocationTooltips
            // 
            this.chkShowLocationTooltips.AutoSize = true;
            this.chkShowLocationTooltips.Location = new System.Drawing.Point(299, 116);
            this.chkShowLocationTooltips.Margin = new System.Windows.Forms.Padding(2);
            this.chkShowLocationTooltips.Name = "chkShowLocationTooltips";
            this.chkShowLocationTooltips.Size = new System.Drawing.Size(138, 19);
            this.chkShowLocationTooltips.TabIndex = 6;
            this.chkShowLocationTooltips.Text = "Show &Location Tooltips";
            this.chkShowLocationTooltips.UseVisualStyleBackColor = true;
            this.chkShowLocationTooltips.Visible = false;
            this.chkShowLocationTooltips.CheckedChanged += new System.EventHandler(this.chkShowLocationTooltips_CheckedChanged);
            // 
            // chkTaskPaneUsesGradient
            // 
            this.chkTaskPaneUsesGradient.AutoSize = true;
            this.chkTaskPaneUsesGradient.Location = new System.Drawing.Point(16, 142);
            this.chkTaskPaneUsesGradient.Margin = new System.Windows.Forms.Padding(2);
            this.chkTaskPaneUsesGradient.Name = "chkTaskPaneUsesGradient";
            this.chkTaskPaneUsesGradient.Size = new System.Drawing.Size(180, 19);
            this.chkTaskPaneUsesGradient.TabIndex = 6;
            this.chkTaskPaneUsesGradient.Text = "&Use gradient color in task pane.";
            this.chkTaskPaneUsesGradient.UseVisualStyleBackColor = true;
            this.chkTaskPaneUsesGradient.CheckedChanged += new System.EventHandler(this.chkTaskPaneUsesGradient_CheckedChanged);
            // 
            // lblTaskPaneBackgroundColor
            // 
            this.lblTaskPaneBackgroundColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTaskPaneBackgroundColor.AutoSize = true;
            this.lblTaskPaneBackgroundColor.Location = new System.Drawing.Point(54, 213);
            this.lblTaskPaneBackgroundColor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTaskPaneBackgroundColor.Name = "lblTaskPaneBackgroundColor";
            this.lblTaskPaneBackgroundColor.Size = new System.Drawing.Size(147, 15);
            this.lblTaskPaneBackgroundColor.TabIndex = 10;
            this.lblTaskPaneBackgroundColor.Text = "Task pane background &color";
            // 
            // colorDlgTaskPaneBackgroundColor
            // 
            this.colorDlgTaskPaneBackgroundColor.SolidColorOnly = true;
            // 
            // pnlTaskPaneColorChip
            // 
            this.pnlTaskPaneColorChip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlTaskPaneColorChip.AutoSize = true;
            this.pnlTaskPaneColorChip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTaskPaneColorChip.Location = new System.Drawing.Point(16, 212);
            this.pnlTaskPaneColorChip.Margin = new System.Windows.Forms.Padding(2);
            this.pnlTaskPaneColorChip.Name = "pnlTaskPaneColorChip";
            this.pnlTaskPaneColorChip.Size = new System.Drawing.Size(34, 16);
            this.pnlTaskPaneColorChip.TabIndex = 9;
            // 
            // btnPickColor
            // 
            this.btnPickColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPickColor.AutoSize = true;
            this.btnPickColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPickColor.Location = new System.Drawing.Point(214, 208);
            this.btnPickColor.Margin = new System.Windows.Forms.Padding(2);
            this.btnPickColor.Name = "btnPickColor";
            this.btnPickColor.Size = new System.Drawing.Size(33, 23);
            this.btnPickColor.TabIndex = 11;
            this.btnPickColor.Text = "...";
            this.btnPickColor.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPickColor.UseVisualStyleBackColor = true;
            this.btnPickColor.Click += new System.EventHandler(this.btnPickColor_Click);
            // 
            // grpTaskPane
            // 
            this.grpTaskPane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTaskPane.Controls.Add(this.chkShowTaskpaneWhenNoVariables);
            this.grpTaskPane.Controls.Add(this.chkExpandFindResults);
            this.grpTaskPane.Controls.Add(this.chkWarnOnFinish);
            this.grpTaskPane.Controls.Add(this.chkCloseTaskPaneOnFinish);
            this.grpTaskPane.Controls.Add(this.chkShowTaskPaneOnBlankNew);
            this.grpTaskPane.Controls.Add(this.chkFitToPageWidth);
            this.grpTaskPane.Controls.Add(this.chkDisplayTaskPaneForNonMacPacDoc);
            this.grpTaskPane.Controls.Add(this.btnPickColor);
            this.grpTaskPane.Controls.Add(this.chkDisplayTaskPaneForMacPacDoc);
            this.grpTaskPane.Controls.Add(this.lblTaskPaneBackgroundColor);
            this.grpTaskPane.Controls.Add(this.pnlTaskPaneColorChip);
            this.grpTaskPane.Controls.Add(this.chkTaskPaneUsesGradient);
            this.grpTaskPane.Location = new System.Drawing.Point(11, 1);
            this.grpTaskPane.Margin = new System.Windows.Forms.Padding(2);
            this.grpTaskPane.Name = "grpTaskPane";
            this.grpTaskPane.Padding = new System.Windows.Forms.Padding(2);
            this.grpTaskPane.Size = new System.Drawing.Size(478, 237);
            this.grpTaskPane.TabIndex = 0;
            this.grpTaskPane.TabStop = false;
            this.grpTaskPane.Text = "Task Pane";
            // 
            // chkShowTaskpaneWhenNoVariables
            // 
            this.chkShowTaskpaneWhenNoVariables.AutoSize = true;
            this.chkShowTaskpaneWhenNoVariables.Location = new System.Drawing.Point(16, 81);
            this.chkShowTaskpaneWhenNoVariables.Name = "chkShowTaskpaneWhenNoVariables";
            this.chkShowTaskpaneWhenNoVariables.Size = new System.Drawing.Size(347, 19);
            this.chkShowTaskpaneWhenNoVariables.TabIndex = 3;
            this.chkShowTaskpaneWhenNoVariables.Text = "Sh&ow task pane when creating a Forte document with no variables";
            this.chkShowTaskpaneWhenNoVariables.UseVisualStyleBackColor = true;
            this.chkShowTaskpaneWhenNoVariables.CheckedChanged += new System.EventHandler(this.chkShowTaskPaneWhenNoVariables_CheckedChanged);
            // 
            // chkExpandFindResults
            // 
            this.chkExpandFindResults.AutoSize = true;
            this.chkExpandFindResults.Location = new System.Drawing.Point(16, 185);
            this.chkExpandFindResults.Margin = new System.Windows.Forms.Padding(2);
            this.chkExpandFindResults.Name = "chkExpandFindResults";
            this.chkExpandFindResults.Size = new System.Drawing.Size(188, 19);
            this.chkExpandFindResults.TabIndex = 8;
            this.chkExpandFindResults.Text = "&Expand Find results automatically";
            this.chkExpandFindResults.UseVisualStyleBackColor = true;
            this.chkExpandFindResults.CheckedChanged += new System.EventHandler(this.chkExpandFindResults_CheckedChanged);
            // 
            // chkWarnOnFinish
            // 
            this.chkWarnOnFinish.AutoSize = true;
            this.chkWarnOnFinish.Location = new System.Drawing.Point(16, 163);
            this.chkWarnOnFinish.Margin = new System.Windows.Forms.Padding(2);
            this.chkWarnOnFinish.Name = "chkWarnOnFinish";
            this.chkWarnOnFinish.Size = new System.Drawing.Size(107, 19);
            this.chkWarnOnFinish.TabIndex = 7;
            this.chkWarnOnFinish.Text = "&Prompt on Finish";
            this.chkWarnOnFinish.UseVisualStyleBackColor = true;
            this.chkWarnOnFinish.CheckedChanged += new System.EventHandler(this.chkWarnOnFinish_CheckedChanged);
            // 
            // chkCloseTaskPaneOnFinish
            // 
            this.chkCloseTaskPaneOnFinish.AutoSize = true;
            this.chkCloseTaskPaneOnFinish.Location = new System.Drawing.Point(16, 102);
            this.chkCloseTaskPaneOnFinish.Margin = new System.Windows.Forms.Padding(2);
            this.chkCloseTaskPaneOnFinish.Name = "chkCloseTaskPaneOnFinish";
            this.chkCloseTaskPaneOnFinish.Size = new System.Drawing.Size(152, 19);
            this.chkCloseTaskPaneOnFinish.TabIndex = 4;
            this.chkCloseTaskPaneOnFinish.Text = "&Close task pane on Finish";
            this.chkCloseTaskPaneOnFinish.UseVisualStyleBackColor = true;
            this.chkCloseTaskPaneOnFinish.CheckedChanged += new System.EventHandler(this.chkCloseTaskPaneOnFinish_CheckedChanged);
            // 
            // chkShowTaskPaneOnBlankNew
            // 
            this.chkShowTaskPaneOnBlankNew.AutoSize = true;
            this.chkShowTaskPaneOnBlankNew.Location = new System.Drawing.Point(16, 61);
            this.chkShowTaskPaneOnBlankNew.Margin = new System.Windows.Forms.Padding(2);
            this.chkShowTaskPaneOnBlankNew.Name = "chkShowTaskPaneOnBlankNew";
            this.chkShowTaskPaneOnBlankNew.Size = new System.Drawing.Size(295, 19);
            this.chkShowTaskPaneOnBlankNew.TabIndex = 2;
            this.chkShowTaskPaneOnBlankNew.Text = "Show tas&k pane when creating a blank Forte document.";
            this.chkShowTaskPaneOnBlankNew.UseVisualStyleBackColor = true;
            this.chkShowTaskPaneOnBlankNew.CheckedChanged += new System.EventHandler(this.chkShowTaskPaneOnBlankNew_CheckedChanged);
            // 
            // chkFitToPageWidth
            // 
            this.chkFitToPageWidth.AutoSize = true;
            this.chkFitToPageWidth.Location = new System.Drawing.Point(16, 122);
            this.chkFitToPageWidth.Margin = new System.Windows.Forms.Padding(2);
            this.chkFitToPageWidth.Name = "chkFitToPageWidth";
            this.chkFitToPageWidth.Size = new System.Drawing.Size(231, 19);
            this.chkFitToPageWidth.TabIndex = 5;
            this.chkFitToPageWidth.Text = "&View page width when task pane is visible.";
            this.chkFitToPageWidth.CheckedChanged += new System.EventHandler(this.chkFitToPageWidth_CheckedChanged);
            // 
            // grpMore
            // 
            this.grpMore.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpMore.Controls.Add(this.chkUpdateZoom);
            this.grpMore.Controls.Add(this.lblDefaultZoomPercentage);
            this.grpMore.Controls.Add(this.spnDefaultZoomPercentage);
            this.grpMore.Controls.Add(this.chkActivateMacPacRibbonTabOnOpen);
            this.grpMore.Controls.Add(this.chkActivateMacPacRibbonTab);
            this.grpMore.Controls.Add(this.chkShowHelp);
            this.grpMore.Controls.Add(this.chkShowXMLTags);
            this.grpMore.Controls.Add(this.chkShowLocationTooltips);
            this.grpMore.Controls.Add(this.chkWarnBeforeDeactivation);
            this.grpMore.Location = new System.Drawing.Point(11, 245);
            this.grpMore.Margin = new System.Windows.Forms.Padding(2);
            this.grpMore.Name = "grpMore";
            this.grpMore.Padding = new System.Windows.Forms.Padding(2);
            this.grpMore.Size = new System.Drawing.Size(478, 172);
            this.grpMore.TabIndex = 0;
            this.grpMore.TabStop = false;
            this.grpMore.Text = "Additional Options";
            // 
            // chkUpdateZoom
            // 
            this.chkUpdateZoom.AutoSize = true;
            this.chkUpdateZoom.Location = new System.Drawing.Point(16, 116);
            this.chkUpdateZoom.Margin = new System.Windows.Forms.Padding(2);
            this.chkUpdateZoom.Name = "chkUpdateZoom";
            this.chkUpdateZoom.Size = new System.Drawing.Size(209, 19);
            this.chkUpdateZoom.TabIndex = 5;
            this.chkUpdateZoom.Text = "&Update Zoom on opening a document.";
            this.chkUpdateZoom.CheckedChanged += new System.EventHandler(this.chkUpdateZoom_CheckedChanged);
            // 
            // lblDefaultZoomPercentage
            // 
            this.lblDefaultZoomPercentage.AutoSize = true;
            this.lblDefaultZoomPercentage.Enabled = false;
            this.lblDefaultZoomPercentage.Location = new System.Drawing.Point(32, 138);
            this.lblDefaultZoomPercentage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDefaultZoomPercentage.Name = "lblDefaultZoomPercentage";
            this.lblDefaultZoomPercentage.Size = new System.Drawing.Size(133, 15);
            this.lblDefaultZoomPercentage.TabIndex = 7;
            this.lblDefaultZoomPercentage.Text = "Default &Zoom Percentage:";
            // 
            // spnDefaultZoomPercentage
            // 
            this.spnDefaultZoomPercentage.Enabled = false;
            this.spnDefaultZoomPercentage.Location = new System.Drawing.Point(169, 138);
            this.spnDefaultZoomPercentage.Margin = new System.Windows.Forms.Padding(2);
            this.spnDefaultZoomPercentage.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.spnDefaultZoomPercentage.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spnDefaultZoomPercentage.Name = "spnDefaultZoomPercentage";
            this.spnDefaultZoomPercentage.Size = new System.Drawing.Size(51, 22);
            this.spnDefaultZoomPercentage.TabIndex = 8;
            this.spnDefaultZoomPercentage.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spnDefaultZoomPercentage.ValueChanged += new System.EventHandler(this.spnDefaultZoomPercentage_ValueChanged);
            // 
            // chkActivateMacPacRibbonTabOnOpen
            // 
            this.chkActivateMacPacRibbonTabOnOpen.AutoSize = true;
            this.chkActivateMacPacRibbonTabOnOpen.Location = new System.Drawing.Point(16, 46);
            this.chkActivateMacPacRibbonTabOnOpen.Margin = new System.Windows.Forms.Padding(2);
            this.chkActivateMacPacRibbonTabOnOpen.Name = "chkActivateMacPacRibbonTabOnOpen";
            this.chkActivateMacPacRibbonTabOnOpen.Size = new System.Drawing.Size(333, 19);
            this.chkActivateMacPacRibbonTabOnOpen.TabIndex = 1;
            this.chkActivateMacPacRibbonTabOnOpen.Text = "Activate the &Forte ribbon tab when a Forte document is opened.";
            this.chkActivateMacPacRibbonTabOnOpen.CheckedChanged += new System.EventHandler(this.chkActivateMacPacRibbonTabOnOpen_CheckedChanged);
            // 
            // chkActivateMacPacRibbonTab
            // 
            this.chkActivateMacPacRibbonTab.AutoSize = true;
            this.chkActivateMacPacRibbonTab.Location = new System.Drawing.Point(16, 22);
            this.chkActivateMacPacRibbonTab.Margin = new System.Windows.Forms.Padding(2);
            this.chkActivateMacPacRibbonTab.Name = "chkActivateMacPacRibbonTab";
            this.chkActivateMacPacRibbonTab.Size = new System.Drawing.Size(334, 19);
            this.chkActivateMacPacRibbonTab.TabIndex = 0;
            this.chkActivateMacPacRibbonTab.Text = "&Activate the Forte ribbon tab when a Forte document is created.";
            this.chkActivateMacPacRibbonTab.CheckedChanged += new System.EventHandler(this.chkActiveMacPacRibbonTab_CheckedChanged);
            // 
            // chkShowXMLTags
            // 
            this.chkShowXMLTags.AutoSize = true;
            this.chkShowXMLTags.Location = new System.Drawing.Point(16, 70);
            this.chkShowXMLTags.Margin = new System.Windows.Forms.Padding(2);
            this.chkShowXMLTags.Name = "chkShowXMLTags";
            this.chkShowXMLTags.Size = new System.Drawing.Size(228, 19);
            this.chkShowXMLTags.TabIndex = 2;
            this.chkShowXMLTags.Text = "Show XML &Tags when editing documents.";
            this.chkShowXMLTags.CheckedChanged += new System.EventHandler(this.chkShowXMLTags_CheckedChanged);
            // 
            // chkWarnBeforeDeactivation
            // 
            this.chkWarnBeforeDeactivation.AutoSize = true;
            this.chkWarnBeforeDeactivation.Location = new System.Drawing.Point(299, 93);
            this.chkWarnBeforeDeactivation.Margin = new System.Windows.Forms.Padding(2);
            this.chkWarnBeforeDeactivation.Name = "chkWarnBeforeDeactivation";
            this.chkWarnBeforeDeactivation.Size = new System.Drawing.Size(194, 19);
            this.chkWarnBeforeDeactivation.TabIndex = 4;
            this.chkWarnBeforeDeactivation.Text = "Warn before deactivating segment.";
            this.chkWarnBeforeDeactivation.UseVisualStyleBackColor = true;
            this.chkWarnBeforeDeactivation.Visible = false;
            this.chkWarnBeforeDeactivation.CheckedChanged += new System.EventHandler(this.chkWarnBeforeDeletion_CheckedChanged);
            // 
            // UserApplicationSettingsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.grpMore);
            this.Controls.Add(this.grpTaskPane);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UserApplicationSettingsManager";
            this.Size = new System.Drawing.Size(506, 432);
            this.Load += new System.EventHandler(this.UserApplicationSettingsManager_Load);
            this.grpTaskPane.ResumeLayout(false);
            this.grpTaskPane.PerformLayout();
            this.grpMore.ResumeLayout(false);
            this.grpMore.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spnDefaultZoomPercentage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkDisplayTaskPaneForNonMacPacDoc;
        private System.Windows.Forms.CheckBox chkDisplayTaskPaneForMacPacDoc;
        private System.Windows.Forms.CheckBox chkShowHelp;
        private System.Windows.Forms.CheckBox chkShowLocationTooltips;
        private System.Windows.Forms.CheckBox chkTaskPaneUsesGradient;
        private System.Windows.Forms.Label lblTaskPaneBackgroundColor;
        private System.Windows.Forms.ColorDialog colorDlgTaskPaneBackgroundColor;
        private System.Windows.Forms.Panel pnlTaskPaneColorChip;
        private System.Windows.Forms.Button btnPickColor;
        private System.Windows.Forms.GroupBox grpTaskPane;
        private System.Windows.Forms.GroupBox grpMore;
        private System.Windows.Forms.CheckBox chkFitToPageWidth;
        private System.Windows.Forms.CheckBox chkActivateMacPacRibbonTab;
        private System.Windows.Forms.CheckBox chkShowXMLTags;
        private System.Windows.Forms.CheckBox chkActivateMacPacRibbonTabOnOpen;
        private System.Windows.Forms.CheckBox chkShowTaskPaneOnBlankNew;
        private System.Windows.Forms.CheckBox chkWarnBeforeDeactivation;
        private System.Windows.Forms.Label lblDefaultZoomPercentage;
        private System.Windows.Forms.NumericUpDown spnDefaultZoomPercentage;
        private System.Windows.Forms.CheckBox chkCloseTaskPaneOnFinish;
        private System.Windows.Forms.CheckBox chkUpdateZoom;
        private System.Windows.Forms.CheckBox chkWarnOnFinish;
        private System.Windows.Forms.CheckBox chkExpandFindResults;
        private System.Windows.Forms.CheckBox chkShowTaskpaneWhenNoVariables;
    }
}
