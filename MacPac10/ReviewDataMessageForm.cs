﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class ReviewDataMessageForm : Form
    {
        public ReviewDataMessageForm()
        {
            InitializeComponent();
        }

        private void ReviewDataMessageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.lblMessage.Text = LMP.Resources.GetLangString("Msg_ReviewData");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.chkStopPrompting.Checked)
                {
                    LMP.MacPac.Session.CurrentUser.UserSettings.ShowReviewDataMessage = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
