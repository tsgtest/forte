namespace LMP.MacPac
{
    partial class PeopleManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PeopleManager));
            this.btnNew = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.lblPeople = new System.Windows.Forms.Label();
            this.btnNewAlias = new System.Windows.Forms.Button();
            this.tabPeople = new System.Windows.Forms.TabControl();
            this.tabDetail = new System.Windows.Forms.TabPage();
            this.grdProperties = new System.Windows.Forms.DataGridView();
            this.tabLicenses = new System.Windows.Forms.TabPage();
            this.grdLicenses = new LMP.Controls.DataGridView();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tsLicense = new System.Windows.Forms.ToolStrip();
            this.tbtnDeleteLicense = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.lblPersonType = new System.Windows.Forms.Label();
            this.btnProxies = new System.Windows.Forms.Button();
            this.grdPeople = new LMP.Controls.DataGridView();
            this.tabPeople.SuspendLayout();
            this.tabDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).BeginInit();
            this.tabLicenses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLicenses)).BeginInit();
            this.tsLicense.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNew.AutoSize = true;
            this.btnNew.Location = new System.Drawing.Point(106, 457);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(86, 26);
            this.btnNew.TabIndex = 4;
            this.btnNew.Text = "&New Person...";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelete.AutoSize = true;
            this.btnDelete.Location = new System.Drawing.Point(292, 457);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 26);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "&Delete...";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.AutoSize = true;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Location = new System.Drawing.Point(542, 456);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 27);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImport.AutoSize = true;
            this.btnImport.Location = new System.Drawing.Point(3, 457);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(96, 26);
            this.btnImport.TabIndex = 3;
            this.btnImport.Text = "&Import People...";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // lblPeople
            // 
            this.lblPeople.AutoSize = true;
            this.lblPeople.Location = new System.Drawing.Point(4, 16);
            this.lblPeople.Name = "lblPeople";
            this.lblPeople.Size = new System.Drawing.Size(81, 15);
            this.lblPeople.TabIndex = 0;
            this.lblPeople.Text = "My &People List:";
            // 
            // btnNewAlias
            // 
            this.btnNewAlias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNewAlias.AutoSize = true;
            this.btnNewAlias.Location = new System.Drawing.Point(199, 457);
            this.btnNewAlias.Name = "btnNewAlias";
            this.btnNewAlias.Size = new System.Drawing.Size(85, 26);
            this.btnNewAlias.TabIndex = 5;
            this.btnNewAlias.Text = "New &Alias...";
            this.btnNewAlias.UseVisualStyleBackColor = true;
            this.btnNewAlias.Click += new System.EventHandler(this.btnNewAlias_Click);
            // 
            // tabPeople
            // 
            this.tabPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabPeople.Controls.Add(this.tabDetail);
            this.tabPeople.Controls.Add(this.tabLicenses);
            this.tabPeople.Location = new System.Drawing.Point(210, 10);
            this.tabPeople.Name = "tabPeople";
            this.tabPeople.SelectedIndex = 0;
            this.tabPeople.Size = new System.Drawing.Size(410, 438);
            this.tabPeople.TabIndex = 2;
            this.tabPeople.SelectedIndexChanged += new System.EventHandler(this.tabPeople_SelectedIndexChanged);
            this.tabPeople.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabPeople_Selecting);
            // 
            // tabDetail
            // 
            this.tabDetail.Controls.Add(this.grdProperties);
            this.tabDetail.Location = new System.Drawing.Point(4, 24);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetail.Size = new System.Drawing.Size(402, 410);
            this.tabDetail.TabIndex = 0;
            this.tabDetail.Text = "Detail";
            this.tabDetail.UseVisualStyleBackColor = true;
            // 
            // grdProperties
            // 
            this.grdProperties.AllowUserToAddRows = false;
            this.grdProperties.AllowUserToDeleteRows = false;
            this.grdProperties.AllowUserToResizeColumns = false;
            this.grdProperties.AllowUserToResizeRows = false;
            this.grdProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProperties.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grdProperties.BackgroundColor = System.Drawing.Color.White;
            this.grdProperties.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdProperties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProperties.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.DefaultCellStyle = dataGridViewCellStyle1;
            this.grdProperties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdProperties.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grdProperties.Location = new System.Drawing.Point(-4, 7);
            this.grdProperties.MultiSelect = false;
            this.grdProperties.Name = "grdProperties";
            this.grdProperties.RowHeadersVisible = false;
            this.grdProperties.RowTemplate.Height = 20;
            this.grdProperties.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdProperties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdProperties.Size = new System.Drawing.Size(406, 413);
            this.grdProperties.TabIndex = 37;
            this.grdProperties.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProperties_CellEnter);
            this.grdProperties.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdProperties_CellValidating);
            this.grdProperties.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdProperties_DataError);
            // 
            // tabLicenses
            // 
            this.tabLicenses.Controls.Add(this.grdLicenses);
            this.tabLicenses.Controls.Add(this.tsLicense);
            this.tabLicenses.Location = new System.Drawing.Point(4, 24);
            this.tabLicenses.Name = "tabLicenses";
            this.tabLicenses.Padding = new System.Windows.Forms.Padding(3);
            this.tabLicenses.Size = new System.Drawing.Size(402, 410);
            this.tabLicenses.TabIndex = 1;
            this.tabLicenses.Text = "Licenses";
            this.tabLicenses.UseVisualStyleBackColor = true;
            // 
            // grdLicenses
            // 
            this.grdLicenses.AllowUserToResizeColumns = false;
            this.grdLicenses.AllowUserToResizeRows = false;
            this.grdLicenses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdLicenses.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grdLicenses.BackgroundColor = System.Drawing.Color.White;
            this.grdLicenses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdLicenses.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grdLicenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdLicenses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Description,
            this.Number,
            this.Level0,
            this.Level1,
            this.Level2,
            this.Level3,
            this.Level4,
            this.ID});
            this.grdLicenses.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdLicenses.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdLicenses.IsDirty = false;
            this.grdLicenses.Location = new System.Drawing.Point(-1, 27);
            this.grdLicenses.MultiSelect = false;
            this.grdLicenses.Name = "grdLicenses";
            this.grdLicenses.RowHeadersVisible = false;
            this.grdLicenses.RowHeadersWidth = 25;
            this.grdLicenses.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdLicenses.Size = new System.Drawing.Size(403, 380);
            this.grdLicenses.SupportingValues = "";
            this.grdLicenses.TabIndex = 23;
            this.grdLicenses.Tag2 = null;
            this.grdLicenses.Value = null;
            this.grdLicenses.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdLicenses_CellEnter);
            this.grdLicenses.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdLicenses_CellValidated);
            this.grdLicenses.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdLicenses_CellValidating);
            this.grdLicenses.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdLicenses_CurrentCellDirtyStateChanged);
            this.grdLicenses.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdLicenses_RowValidated);
            this.grdLicenses.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdLicenses_RowValidating);
            this.grdLicenses.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.grdLicenses_UserAddedRow);
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.Description.DefaultCellStyle = dataGridViewCellStyle3;
            this.Description.FillWeight = 10.52631F;
            this.Description.HeaderText = "Description";
            this.Description.MinimumWidth = 200;
            this.Description.Name = "Description";
            this.Description.Width = 200;
            // 
            // Number
            // 
            this.Number.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.Number.DefaultCellStyle = dataGridViewCellStyle4;
            this.Number.FillWeight = 109.4737F;
            this.Number.HeaderText = "Number";
            this.Number.MinimumWidth = 100;
            this.Number.Name = "Number";
            // 
            // Level0
            // 
            this.Level0.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Level0.DefaultCellStyle = dataGridViewCellStyle5;
            this.Level0.HeaderText = "Level 0";
            this.Level0.MinimumWidth = 100;
            this.Level0.Name = "Level0";
            this.Level0.ReadOnly = true;
            // 
            // Level1
            // 
            this.Level1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Level1.HeaderText = "Level 1";
            this.Level1.MinimumWidth = 100;
            this.Level1.Name = "Level1";
            this.Level1.ReadOnly = true;
            // 
            // Level2
            // 
            this.Level2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Level2.HeaderText = "Level 2";
            this.Level2.MinimumWidth = 100;
            this.Level2.Name = "Level2";
            this.Level2.ReadOnly = true;
            // 
            // Level3
            // 
            this.Level3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Level3.HeaderText = "Level 3";
            this.Level3.MinimumWidth = 100;
            this.Level3.Name = "Level3";
            this.Level3.ReadOnly = true;
            // 
            // Level4
            // 
            this.Level4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Level4.HeaderText = "Level 4";
            this.Level4.MinimumWidth = 100;
            this.Level4.Name = "Level4";
            this.Level4.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // tsLicense
            // 
            this.tsLicense.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsLicense.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnDeleteLicense,
            this.toolStripSeparator1});
            this.tsLicense.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsLicense.Location = new System.Drawing.Point(3, 3);
            this.tsLicense.Name = "tsLicense";
            this.tsLicense.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsLicense.Size = new System.Drawing.Size(396, 25);
            this.tsLicense.TabIndex = 22;
            // 
            // tbtnDeleteLicense
            // 
            this.tbtnDeleteLicense.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteLicense.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeleteLicense.Image")));
            this.tbtnDeleteLicense.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteLicense.Name = "tbtnDeleteLicense";
            this.tbtnDeleteLicense.Size = new System.Drawing.Size(53, 22);
            this.tbtnDeleteLicense.Text = "&Delete...";
            this.tbtnDeleteLicense.Click += new System.EventHandler(this.tbtnDeleteLicense_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // lblPersonType
            // 
            this.lblPersonType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPersonType.AutoSize = true;
            this.lblPersonType.Location = new System.Drawing.Point(122, 16);
            this.lblPersonType.Name = "lblPersonType";
            this.lblPersonType.Size = new System.Drawing.Size(82, 15);
            this.lblPersonType.TabIndex = 38;
            this.lblPersonType.Text = "#Person Type#";
            this.lblPersonType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnProxies
            // 
            this.btnProxies.AutoSize = true;
            this.btnProxies.Location = new System.Drawing.Point(385, 457);
            this.btnProxies.Name = "btnProxies";
            this.btnProxies.Size = new System.Drawing.Size(85, 26);
            this.btnProxies.TabIndex = 7;
            this.btnProxies.Text = "Pro&xies...";
            this.btnProxies.UseVisualStyleBackColor = true;
            this.btnProxies.Click += new System.EventHandler(this.btnProxies_Click);
            // 
            // grdPeople
            // 
            this.grdPeople.AllowUserToAddRows = false;
            this.grdPeople.AllowUserToDeleteRows = false;
            this.grdPeople.AllowUserToResizeColumns = false;
            this.grdPeople.AllowUserToResizeRows = false;
            this.grdPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdPeople.BackgroundColor = System.Drawing.Color.White;
            this.grdPeople.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdPeople.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPeople.ColumnHeadersVisible = false;
            this.grdPeople.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdPeople.GridColor = System.Drawing.SystemColors.Menu;
            this.grdPeople.IsDirty = false;
            this.grdPeople.Location = new System.Drawing.Point(3, 35);
            this.grdPeople.MultiSelect = false;
            this.grdPeople.Name = "grdPeople";
            this.grdPeople.RowHeadersVisible = false;
            this.grdPeople.Size = new System.Drawing.Size(201, 412);
            this.grdPeople.SupportingValues = "";
            this.grdPeople.TabIndex = 1;
            this.grdPeople.Tag2 = null;
            this.grdPeople.Value = null;
            this.grdPeople.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdPeople_KeyDown);
            // 
            // PeopleManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(627, 489);
            this.Controls.Add(this.grdPeople);
            this.Controls.Add(this.btnProxies);
            this.Controls.Add(this.lblPersonType);
            this.Controls.Add(this.btnNewAlias);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.tabPeople);
            this.Controls.Add(this.lblPeople);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PeopleManager";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage People";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PeopleManager_FormClosing);
            this.Load += new System.EventHandler(this.PeopleManager_Load);
            this.tabPeople.ResumeLayout(false);
            this.tabDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).EndInit();
            this.tabLicenses.ResumeLayout(false);
            this.tabLicenses.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLicenses)).EndInit();
            this.tsLicense.ResumeLayout(false);
            this.tsLicense.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Label lblPeople;
        private System.Windows.Forms.Button btnNewAlias;
        private System.Windows.Forms.TabControl tabPeople;
        private System.Windows.Forms.TabPage tabDetail;
        private System.Windows.Forms.DataGridView grdProperties;
        private System.Windows.Forms.TabPage tabLicenses;
        private System.Windows.Forms.ToolStrip tsLicense;
        private System.Windows.Forms.ToolStripButton tbtnDeleteLicense;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private LMP.Controls.DataGridView grdLicenses;
        //private System.Windows.Forms.DataGridView grdLicenses;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level0;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.Label lblPersonType;
        private System.Windows.Forms.Button btnProxies;
        private LMP.Controls.DataGridView grdPeople;

    }
}