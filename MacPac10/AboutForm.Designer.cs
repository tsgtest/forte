﻿namespace LMP.MacPac
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.mnuContext_Copy = new System.Windows.Forms.MenuItem();
            this.oTip = new System.Windows.Forms.ToolTip(this.components);
            this.pctCopy = new System.Windows.Forms.PictureBox();
            this.lblDB = new System.Windows.Forms.Label();
            this.lblMode = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.pnlBorder = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnSessionInfo = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblProductName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pctCopy)).BeginInit();
            this.pnlBorder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContext_Copy});
            // 
            // mnuContext_Copy
            // 
            this.mnuContext_Copy.Index = 0;
            this.mnuContext_Copy.Text = "Copy";
            // 
            // pctCopy
            // 
            this.pctCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pctCopy.Image = ((System.Drawing.Image)(resources.GetObject("pctCopy.Image")));
            this.pctCopy.Location = new System.Drawing.Point(503, 15);
            this.pctCopy.Name = "pctCopy";
            this.pctCopy.Size = new System.Drawing.Size(19, 20);
            this.pctCopy.TabIndex = 45;
            this.pctCopy.TabStop = false;
            this.oTip.SetToolTip(this.pctCopy, "Copy Version Number");
            this.pctCopy.Click += new System.EventHandler(this.pctCopy_Click);
            this.pctCopy.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pctCopy_MouseDown);
            this.pctCopy.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pctCopy_MouseUp);
            // 
            // lblDB
            // 
            this.lblDB.Location = new System.Drawing.Point(373, 249);
            this.lblDB.Name = "lblDB";
            this.lblDB.Size = new System.Drawing.Size(25, 64);
            this.lblDB.TabIndex = 30;
            this.lblDB.Text = "Database";
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Location = new System.Drawing.Point(535, 248);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(34, 13);
            this.lblMode.TabIndex = 31;
            this.lblMode.Text = "Mode";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(535, 224);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(29, 13);
            this.lblUser.TabIndex = 32;
            this.lblUser.Text = "User";
            // 
            // pnlBorder
            // 
            this.pnlBorder.Controls.Add(this.pictureBox2);
            this.pnlBorder.Controls.Add(this.btnSessionInfo);
            this.pnlBorder.Controls.Add(this.pictureBox4);
            this.pnlBorder.Controls.Add(this.pictureBox1);
            this.pnlBorder.Controls.Add(this.lblCopyright);
            this.pnlBorder.Controls.Add(this.label3);
            this.pnlBorder.Controls.Add(this.pctCopy);
            this.pnlBorder.Controls.Add(this.lblVersion);
            this.pnlBorder.Controls.Add(this.btnClose);
            this.pnlBorder.Controls.Add(this.pictureBox3);
            this.pnlBorder.Controls.Add(this.lblProductName);
            this.pnlBorder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBorder.Location = new System.Drawing.Point(0, 0);
            this.pnlBorder.Name = "pnlBorder";
            this.pnlBorder.Size = new System.Drawing.Size(555, 331);
            this.pnlBorder.TabIndex = 42;
            this.pnlBorder.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::LMP.MacPac.Properties.Resources.TSGLogo;
            this.pictureBox2.Location = new System.Drawing.Point(259, 253);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(38, 28);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 53;
            this.pictureBox2.TabStop = false;
            // 
            // btnSessionInfo
            // 
            this.btnSessionInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSessionInfo.AutoSize = true;
            this.btnSessionInfo.Location = new System.Drawing.Point(454, 289);
            this.btnSessionInfo.Name = "btnSessionInfo";
            this.btnSessionInfo.Size = new System.Drawing.Size(88, 23);
            this.btnSessionInfo.TabIndex = 51;
            this.btnSessionInfo.Text = "Session Info...";
            this.btnSessionInfo.UseVisualStyleBackColor = true;
            this.btnSessionInfo.Click += new System.EventHandler(this.btnSessionInfo_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::LMP.MacPac.Properties.Resources.ForteLogoLarge;
            this.pictureBox4.Location = new System.Drawing.Point(12, 19);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(251, 236);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 50;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LMP.MacPac.Properties.Resources.TSGTagline;
            this.pictureBox1.Location = new System.Drawing.Point(7, 295);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(292, 17);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 49;
            this.pictureBox1.TabStop = false;
            // 
            // lblCopyright
            // 
            this.lblCopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.Location = new System.Drawing.Point(261, 151);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(282, 16);
            this.lblCopyright.TabIndex = 44;
            this.lblCopyright.Text = "Copyright";
            this.lblCopyright.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(261, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(282, 71);
            this.label3.TabIndex = 47;
            this.label3.Text = "Compatible with \r\nOffice 2016/365 ProPlus (32-bit and 64-bit) and \r\nOffice 2013/3" +
    "65 ProPlus and 2010 (32-bit)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVersion
            // 
            this.lblVersion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(261, 118);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(282, 27);
            this.lblVersion.TabIndex = 43;
            this.lblVersion.Text = "Version automatic";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.AutoSize = true;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = global::LMP.MacPac.Properties.Resources.CloseButton;
            this.btnClose.Location = new System.Drawing.Point(521, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(21, 22);
            this.btnClose.TabIndex = 42;
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::LMP.MacPac.Properties.Resources.TSGName;
            this.pictureBox3.Location = new System.Drawing.Point(10, 260);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(253, 43);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 52;
            this.pictureBox3.TabStop = false;
            // 
            // lblProductName
            // 
            this.lblProductName.Font = new System.Drawing.Font("Microsoft YaHei", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblProductName.Location = new System.Drawing.Point(261, 38);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(282, 73);
            this.lblProductName.TabIndex = 46;
            this.lblProductName.Text = "Forte";
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // AboutForm
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(555, 331);
            this.ControlBox = false;
            this.Controls.Add(this.pnlBorder);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblDB);
            this.Controls.Add(this.lblMode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MacPac";
            this.Load += new System.EventHandler(this.AboutForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctCopy)).EndInit();
            this.pnlBorder.ResumeLayout(false);
            this.pnlBorder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip oTip;
        private System.Windows.Forms.Label lblDB;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Panel pnlBorder;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCopyright;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.PictureBox pctCopy;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSessionInfo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;

    }
}
