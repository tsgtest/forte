using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    internal partial class BasicNameForm : Form
    {

        private enum InvalidDisplayNames
        { 
            ValidName = 0,
            NullString = 1,
            ContainsSpaces = 2,
            DuplicateName = 3,
            IllegalName = 4
        }

        public enum TargetObjectTypes
        {
            Variable = 0,
            Block = 1
        }

        LMP.Architect.Api.Segment m_oSeg = null;
        TargetObjectTypes m_iTargetObjectType = TargetObjectTypes.Variable;

        #region**********************constructors*******************
        public BasicNameForm(LMP.Architect.Api.Segment oSegment)
        {
            InitializeComponent();

            this.m_oSeg = oSegment;

            //present simplified options for user variable or block
            if (!(oSegment is LMP.Architect.Api.AdminSegment || Session.CurrentUser.IsContentDesigner))
            {
                this.lblName.Visible = false;
                this.txtName.Visible = false;
                this.Height = 143;
            }
        }
        #endregion
        #region************************events***********************
        private void VariableNameForm_Load(object sender, EventArgs e)
        {
            this.txtDisplayName.MaxLength = 50;
        }
        private void txtName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.txtDisplayName.Text.Length == 50)
                {
                    MessageBox.Show("Display Name cannot be longer than 50 characters", String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //set default name
                this.txtName.Text = String.RemoveIllegalNameChars(this.txtDisplayName.Text);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string xMsg = null;
            switch (ValidateName())
            {
                case InvalidDisplayNames.ValidName:
                    this.ItemDisplayName = this.txtDisplayName.Text;
                    this.Hide();
                    this.DialogResult = DialogResult.OK;
                    return;
                case InvalidDisplayNames.NullString:
                    xMsg = LMP.Resources.GetLangString("Msg_InvalidVariableNameNullString");
                    break;
                case InvalidDisplayNames.ContainsSpaces:
                    xMsg = LMP.Resources.GetLangString("Msg_InvalidVariableNameContainsSpaces");
                    break;
                case InvalidDisplayNames.DuplicateName:
                    xMsg = LMP.Resources.GetLangString("Msg_InvalidVariableNameDuplicateName");
                    break;
                case InvalidDisplayNames.IllegalName:
                    xMsg = LMP.Resources.GetLangString("Msg_InvalidVariableNameIllegalName");
                    break;
                default:
                    break;
            }
            
            MessageBox.Show(xMsg, LMP.String.MacPacProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            this.txtDisplayName.Focus(); 
        }
        #endregion
        #region************************properties*******************
        public string ItemDisplayName
        {
            get
            {
                return this.txtDisplayName.Text;
            }
            set
            {
                this.txtDisplayName.Text = value;
            }
        }

        public string ItemName
        {
            get
            {
                return this.txtName.Text;
            }
            set
            {
                this.txtName.Text = value;
            }
        }

        public TargetObjectTypes TargetObjectType
        {
            get { return m_iTargetObjectType; }
            set { m_iTargetObjectType = value; }
        }
        
        //public LMP.Architect.Api.Segment Segment
        //{
        //    get { return m_oSeg; }
        //    set { m_oSeg = value; }
        //}

        #endregion
        #region************************procedures*********************
        /// <summary>
        /// validates designated name by attempting to create instance
        /// of variable using Variables.ItemByName
        /// </summary>
        private InvalidDisplayNames ValidateName()
        {
            LMP.Architect.Api.Variable oVar = null;
            LMP.Architect.Api.Block oBlock = null;

            //empty text box is invalid name
            if (this.txtDisplayName.Text == "" || this.txtName.Text == "")
                return InvalidDisplayNames.NullString;  

            try
            {
                //attempt to create variable w/ proposed name
                oVar = this.m_oSeg.Variables.ItemFromName(this.txtName.Text);
                if (oVar != null)
                    //variable name already exists
                    return InvalidDisplayNames.DuplicateName;

                //attempt to create block w/ proposed name
                oBlock = this.m_oSeg.Blocks.ItemFromName(this.txtName.Text);
                if (oBlock != null)
                    //block name already exists
                    return InvalidDisplayNames.DuplicateName;
            }
            catch
            {
                return InvalidDisplayNames.ValidName;
            }
            return InvalidDisplayNames.IllegalName;
        }
        #endregion

        private void txtVariableName_EnterKeyPressed(object sender, LMP.Controls.EnterKeyPressedEventArgs e)
        {
            btnOK_Click(sender, e);
        }
    }
}