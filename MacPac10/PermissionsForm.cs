using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    public partial class PermissionsForm : Form
    {
        #region *********************fields*********************
        private mpObjectTypes m_iObjectTypeIDFilter = 0;
        private int m_iObjectIDFilter = 0;
        private int m_iParentID = 0;
        #endregion
        #region *********************Constructor*********************
        public PermissionsForm(mpObjectTypes iObjectType, int iObjectID, string xTitle,
            string xDescription, int iParentID)
        {
            m_iParentID = iParentID;
            InitializeComponent();
            m_iObjectTypeIDFilter = iObjectType;
            m_iObjectIDFilter = iObjectID;
            this.Text = xTitle;
            this.lblDescription.Text = xDescription;
        }
        #endregion
        #region *********************methods*********************

        public new DialogResult ShowDialog()
        {
            tvPermissions.SetObjectFilter(m_iObjectTypeIDFilter, m_iObjectIDFilter);
            return base.ShowDialog();
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// OK button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (tvPermissions.IsDirty)
            {
                tvPermissions.SavePermissions();
            }
        }
        #endregion

        private void PermissionsForm_Load(object sender, EventArgs e)
        {

        }
    }
}