﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LMP.Architect.Api;
using LMP.Data;

namespace LMP.MacPac
{
    public partial class InsertSectionBreakForm : Form
    {
        public enum BreakLocations
        {
            InsertionPoint = 1,
            StartOfDocument = 2,
            EndOfDocument = 3,
            BeforeBodyBlock = 4
        }
        public enum HeaderFooterOptions
        {
            LinkToPrevious = 0,
            KeepExisting = 1,
            RemoveExisting = 2,
            RemovePage1Only = 3
        }
        public enum PageNumberOptions
        {
            NoChange = 0,
            AddPage1 = 1,
            RemovePage1 = 2
        }

        private Segment m_oSegment;
        private bool m_bPleadingPaperExists = false;
        private bool m_bLoaded = false;

        DataTable m_dtValues = null;


        public InsertSectionBreakForm()
        {
            InitializeComponent();
        }
        public Segment Segment
        {
            get { return m_oSegment; }
            set { m_oSegment = value; }
        }
        public BreakLocations InsertionLocation
        {
            get
            {
                try
                {
                    return (BreakLocations)Enum.Parse(typeof(BreakLocations), cmbLocation.SelectedValue.ToString());
                }
                catch
                {
                    return BreakLocations.InsertionPoint;
                }
            }
        }
        public HeaderFooterOptions Section1Option
        {
            get 
            {
                try
                {
                    return (HeaderFooterOptions)Enum.Parse(typeof(HeaderFooterOptions), cmbHeaderFooter1.SelectedValue.ToString());
                }
                catch
                {
                    return HeaderFooterOptions.KeepExisting;
                }
            }
        }
        public HeaderFooterOptions Section2Option
        {
            get 
            {
                try
                {
                    return (HeaderFooterOptions)Enum.Parse(typeof(HeaderFooterOptions), cmbHeaderFooter2.SelectedValue.ToString());
                }
                catch
                {
                    return HeaderFooterOptions.LinkToPrevious;
                }
            }
        }
        public PageNumberOptions Section1PageNumber
        {
            get 
            {
                try
                {
                    return (PageNumberOptions)Enum.Parse(typeof(PageNumberOptions), cmbPageNumbers1.SelectedValue.ToString());
                }
                catch
                {
                    return PageNumberOptions.NoChange;
                }
            }
        }
        public PageNumberOptions Section2PageNumber
        {
            get 
            {
                try
                {
                    return (PageNumberOptions)Enum.Parse(typeof(PageNumberOptions), cmbPageNumbers2.SelectedValue.ToString());
                }
                catch
                {
                    return PageNumberOptions.NoChange;
                }
            }
        }
        public bool RestartPageNumbering
        {
            get { return chkRestartNumbering.Checked; }
        }   
        private void InsertSectionBreakForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Get User Settings
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;
                int iTest = 0;
				//GLOG : 7603 : CEH - use native combo instead of LMP.Controls.combo
				//to fix display issue
                int iSelectedValue = 0;

                string[,] xHeaderFooter1 = null;
                string[,] xPageNumbers1 = null;
                string[,] xHeaderFooter2 = null;
                string[,] xPageNumbers2 = null;
                string[,] xLocation = null;

                //GLOG : 8074 : ceh
                //GLOG : 7798 : ceh
                //GLOG : 7797 : ceh
                if (m_oSegment != null && m_oSegment.Contains(mpObjectTypes.PleadingPaper))
                {
                    m_bPleadingPaperExists = true;

                    xHeaderFooter1 = new string[,] {   {"Include Pleading Paper", ((int)HeaderFooterOptions.KeepExisting).ToString()},
                                                       {"Remove Pleading Paper", ((int)HeaderFooterOptions.RemoveExisting).ToString()}};

                    xPageNumbers1 = new string[,] {   {"Use Pleading Paper Default", ((int)PageNumberOptions.NoChange).ToString()},
                                                      {"No", ((int)PageNumberOptions.AddPage1).ToString()},
                                                      {"Yes", ((int)PageNumberOptions.RemovePage1).ToString()}};

                    xHeaderFooter2 = new string[,] {   {"Link to Previous Section", ((int)HeaderFooterOptions.LinkToPrevious).ToString()},
                                                       {"Unlink From Previous Section", ((int)HeaderFooterOptions.KeepExisting).ToString()},
                                                       {"Remove Pleading Paper", ((int)HeaderFooterOptions.RemoveExisting).ToString()}};

                    xPageNumbers2 = new string[,] {   {"Use Pleading Paper Default", ((int)PageNumberOptions.NoChange).ToString()},
                                                      {"No", ((int)PageNumberOptions.AddPage1).ToString()},
                                                      {"Yes", ((int)PageNumberOptions.RemovePage1).ToString()}};
                }
                else
                {
                    m_bPleadingPaperExists =false;

                    xHeaderFooter1 = new string[,] {   {"Keep Existing", ((int)HeaderFooterOptions.KeepExisting).ToString()},
                                                       {"Clear Contents", ((int)HeaderFooterOptions.RemoveExisting).ToString()}};

                    xPageNumbers1 = new string[,] {   {"Keep Existing", ((int)PageNumberOptions.NoChange).ToString()},
                                                      {"No", ((int)PageNumberOptions.AddPage1).ToString()},
                                                      {"Yes", ((int)PageNumberOptions.RemovePage1).ToString()}};

                    xHeaderFooter2 = new string[,] {   {"Link to Previous Section", ((int)HeaderFooterOptions.LinkToPrevious).ToString()},
                                                       {"Unlink From Previous Section", ((int)HeaderFooterOptions.KeepExisting).ToString()},
                                                       {"Clear Contents", ((int)HeaderFooterOptions.RemoveExisting).ToString()}};

                    xPageNumbers2 = new string[,] {   {"Keep Existing", ((int)PageNumberOptions.NoChange).ToString()},
                                                      {"No", ((int)PageNumberOptions.AddPage1).ToString()},
                                                      {"Yes", ((int)PageNumberOptions.RemovePage1).ToString()}};
                }

                //GLOG : 8436 : JSW
                //this.cmbHeaderFooter1.DataSource = GetDataTable(xHeaderFooter1);
                //this.cmbHeaderFooter1.DisplayMember = "Name";
                //this.cmbHeaderFooter1.ValueMember = "Value";
                this.cmbHeaderFooter1.SetList(xHeaderFooter1);

                //GLOG : 8436 : JSw
                //this.cmbPageNumbers1.DataSource = GetDataTable(xPageNumbers1);
                //this.cmbPageNumbers1.DisplayMember = "Name";
                //this.cmbPageNumbers1.ValueMember = "Value";
                this.cmbPageNumbers1.SetList(xPageNumbers1);

                //GLOG : 8436 : JSw
                //this.cmbHeaderFooter2.DataSource = GetDataTable(xHeaderFooter2);
                //this.cmbHeaderFooter2.DisplayMember = "Name";
                //this.cmbHeaderFooter2.ValueMember = "Value";
                this.cmbHeaderFooter2.SetList(xHeaderFooter2);

                //GLOG : 8436 : JSw
                //this.cmbPageNumbers2.DataSource = GetDataTable(xPageNumbers2);
                //this.cmbPageNumbers2.DisplayMember = "Name";
                //this.cmbPageNumbers2.ValueMember = "Value";
                this.cmbPageNumbers2.SetList(xPageNumbers2);

                //Load values from User Settings
                iTest = oSettings.SectionBreakHeader1Option;
                if (iTest > 0 && iTest < 3)
                    cmbHeaderFooter1.SelectedValue = iTest;
                else
                    cmbHeaderFooter1.SelectedValue = ((int)HeaderFooterOptions.KeepExisting).ToString();
                
                iTest = oSettings.SectionBreakPageNumber1Option;
                if (iTest > -1 && iTest < 3)
                    cmbPageNumbers1.SelectedValue = iTest;
                else
                    cmbPageNumbers1.SelectedValue = ((int)PageNumberOptions.RemovePage1).ToString();


                iTest = oSettings.SectionBreakHeader2Option;
                if (iTest > -1 && iTest < 3)
                    cmbHeaderFooter2.SelectedValue = iTest;
                else
                    cmbHeaderFooter2.SelectedValue = ((int)HeaderFooterOptions.KeepExisting).ToString();

                iTest = oSettings.SectionBreakPageNumber2Option;
                if (iTest > -1 && iTest < 3)
                    cmbPageNumbers2.SelectedValue = iTest;
                else
                    cmbPageNumbers2.SelectedValue = ((int)PageNumberOptions.RemovePage1).ToString();

                iTest = oSettings.SectionBreakLocation;
                if (m_oSegment != null && m_oSegment.HasBody())
                {

                    xLocation = new string[,] {   {"Insertion Point", ((int)BreakLocations.InsertionPoint).ToString()},
                                                  {"Start of Document", ((int)BreakLocations.StartOfDocument).ToString()},
                                                  {"End of Document", ((int)BreakLocations.EndOfDocument).ToString()},
                                                  {"Before Body", ((int)BreakLocations.BeforeBodyBlock).ToString()}};

                    if (iTest > 0 && iTest < 5)
                        iSelectedValue = iTest;
                    else
                        iSelectedValue = (int)BreakLocations.BeforeBodyBlock;

                }
                else
                {
                    xLocation = new string[,] {   {"Insertion Point", ((int)BreakLocations.InsertionPoint).ToString()},
                                                  {"Start of Document", ((int)BreakLocations.StartOfDocument).ToString()},
                                                  {"End of Document", ((int)BreakLocations.EndOfDocument).ToString()}};
                    if (iTest > 0 && iTest < 4)
                        iSelectedValue = iTest;
                    else
                        iSelectedValue = (int)BreakLocations.InsertionPoint;
                }
                
                //GLOG : 8436 : JSW
                //this.cmbLocation.DataSource = GetDataTable(xLocation);
                //this.cmbLocation.DisplayMember = "Name";
                //this.cmbLocation.ValueMember = "Value";
                this.cmbLocation.SetList(xLocation);

                cmbLocation.SelectedValue=iSelectedValue;

                chkRestartNumbering.Checked = oSettings.SectionBreakRestartNumbering;
                m_bLoaded = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // Create data source for form drop downs
        private DataTable GetDataTable(string[,] oValues)
        {
            m_dtValues = new DataTable();

            DataColumn oNameCol = new DataColumn("Name", typeof(string));
            m_dtValues.Columns.Add(oNameCol);

            DataColumn oValueCol = new DataColumn("Value", typeof(string));
            m_dtValues.Columns.Add(oValueCol);

            // Populate the table with the values.
            for (int i = 0; i < oValues.GetLength(0); i++)
            {
                this.m_dtValues.Rows.Add(oValues[i, 0], oValues[i, 1]);
            }
            return m_dtValues;
        }


        private void cmbLocation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!m_bLoaded || string.IsNullOrEmpty(cmbLocation.SelectedValue.ToString()))
                    return;
                switch ((BreakLocations)Int32.Parse(cmbLocation.SelectedValue.ToString()))
                {
                    case BreakLocations.InsertionPoint:
                    case BreakLocations.BeforeBodyBlock:
                        cmbPageNumbers1.SelectedValue = ((int)PageNumberOptions.RemovePage1).ToString();
                        break;
                    default:
                        cmbPageNumbers1.SelectedValue = ((int)PageNumberOptions.NoChange).ToString();
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void cmbHeaderFooter1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //GLOG : 8436 : jsw
                if (cmbHeaderFooter1.SelectedValue != null)
                {   
                    //if (m_bPleadingPaperExists)
                    //{
                    lblPageNumbers1.Enabled = cmbHeaderFooter1.SelectedValue.ToString() != ((int)HeaderFooterOptions.RemoveExisting).ToString();
                    cmbPageNumbers1.Enabled = cmbHeaderFooter1.SelectedValue.ToString() != ((int)HeaderFooterOptions.RemoveExisting).ToString();
                    // }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void cmbHeaderFooter2_ValueChanged(object sender, EventArgs e)
        {
            //GLOG : 7886 : ceh
            try
            {
                //GLOG : 8436 : jsw
                if (cmbHeaderFooter2.SelectedValue != null)
                {
                    lblPageNumbers2.Enabled = cmbHeaderFooter2.SelectedValue.ToString() != ((int)HeaderFooterOptions.RemoveExisting).ToString();
                    cmbPageNumbers2.Enabled = cmbHeaderFooter2.SelectedValue.ToString() != ((int)HeaderFooterOptions.RemoveExisting).ToString();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

            //Obsolete code
            //try
            //{
            //    if (m_bPleadingPaperExists)
            //    {
            //        lblPageNumbers2.Enabled = (cmbHeaderFooter2.Value != ((int)HeaderFooterOptions.RemoveExisting).ToString()) 
            //            && (cmbHeaderFooter2.Value != ((int)HeaderFooterOptions.LinkToPrevious).ToString());
            //        cmbPageNumbers2.Enabled = (cmbHeaderFooter2.Value != ((int)HeaderFooterOptions.RemoveExisting).ToString())
            //            && (cmbHeaderFooter2.Value != ((int)HeaderFooterOptions.LinkToPrevious).ToString());
            //    }
            //}
            //catch (System.Exception oE)
            //{
            //    LMP.Error.Show(oE);
            //}

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                //Store User Settings
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;
                oSettings.SectionBreakLocation = Int32.Parse(cmbLocation.SelectedValue.ToString());
                oSettings.SectionBreakHeader1Option = Int32.Parse(cmbHeaderFooter1.SelectedValue.ToString());
                oSettings.SectionBreakHeader2Option = Int32.Parse(cmbHeaderFooter2.SelectedValue.ToString());
                //if (m_bPleadingPaperExists)
                //{
                oSettings.SectionBreakPageNumber1Option = Int32.Parse(cmbPageNumbers1.SelectedValue.ToString());
                oSettings.SectionBreakPageNumber2Option = Int32.Parse(cmbPageNumbers2.SelectedValue.ToString());
                //}
                oSettings.SectionBreakRestartNumbering = chkRestartNumbering.Checked;
            }
            catch
            {
            }
        }
    }
}
