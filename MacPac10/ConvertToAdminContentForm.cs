using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    public partial class ConvertToAdminContentForm : Form
    {
        private bool m_bUpdateExisting = false;

        public ConvertToAdminContentForm():this(false)
        {
        }
        public ConvertToAdminContentForm(bool bUpdateExisting)
        {
            InitializeComponent();
            m_bUpdateExisting = bUpdateExisting;

            if (m_bUpdateExisting)
            {
                this.ftvLocation.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
                this.lblLocation.Text = "&Select content to update:";
                this.Text = "Update Admin Content";
            }
            else
            {
                this.ftvLocation.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders;
            }

            this.ftvLocation.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
            this.ftvLocation.ExecuteFinalSetup();
        }

        /// <summary>
        /// returns the selected folder
        /// </summary>
        public Folder SelectedFolder
        {
            get
            {
                if (this.ftvLocation.SelectedNodes[0].Tag is Folder)
                    return (Folder)this.ftvLocation.SelectedNodes[0].Tag;
                else
                    return (Folder)this.ftvLocation.SelectedNodes[0].Parent.Tag;
            }
        }

        /// <summary>
        /// returns the selected segment definition
        /// </summary>
        public AdminSegmentDef SelectedSegmentDefinition
        {
            get
            {
                if (this.ftvLocation.SelectedNodes[0].Tag is FolderMember)
                {
                    FolderMember oMember = (FolderMember)this.ftvLocation.SelectedNodes[0].Tag;
                    AdminSegmentDef oDef = (AdminSegmentDef)(new AdminSegmentDefs()).ItemFromID(oMember.ObjectID1);
                    return oDef;
                }
                else
                    return null;
            }
        }
    }
}