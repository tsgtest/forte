using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Architect.Api;
using Infragistics.Win.UltraWinTree;

namespace LMP.MacPac
{
    public partial class SegmentForm : Form
    {
        #region *********************constants**************************
        private const string mpPathSeparator = "\\\\";
        #endregion
        #region *********************fields**************************
        //TODO: settle on an ITEM_SEP - these are set to work with ComboBox.SetList(string)
        public const char ITEM_SEP = '~';
        public const char NAME_VALUE_SEP = ';';
        private string m_xSegmentID = "";
        private bool m_bMultipleSelection = false;
        //GLOG : 8380 : jsw
        private bool m_bExludeNonMacPacTypes = false;

        #endregion
        #region *********************constructors**************************
        /// <summary>
        /// Contructor
        /// </summary>
        public SegmentForm()
        {
            InitializeComponent();
            InitializeControls();
        }
        public SegmentForm(bool bMultipleSelection, string xSegmentsList, string xDlgTitle)
        {
            InitializeComponent();
            m_bMultipleSelection = bMultipleSelection;
            InitializeControls();

            if (!string.IsNullOrEmpty(xDlgTitle))
                this.Text = xDlgTitle;
        }
        #endregion
        #region *********************methods**************************
        private void InitializeControls()
        {
            if (!DesignMode)
            {
                try
                {
                    //GLOG : 7276 : JSW
                    this.ftvSegment.AllowFavoritesMenu = true;
                    ////GLOG : 8380 : jsw
                    //this.ftvSegment.ExcludeNonMacPacSegments = this.ExcludeNonMacPacSegments;
                    this.ftvSegment.OwnerID = LMP.Data.Application.User.ID;
                    this.ftvSegment.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                        LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
                    this.ftvSegment.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
                    DataTable oDT = new DataTable();
                    if (m_bMultipleSelection)
                    {
                        oDT.Columns.Add("DisplayName");
                        oDT.Columns.Add("ID");
                        oDT.PrimaryKey = new DataColumn[] { oDT.Columns["ID"] };
                        this.Text = LMP.Resources.GetLangString("Dialog_SegmentTree_Title_Multi");
                    }
                    else
                    {
                        this.Text = LMP.Resources.GetLangString("Dialog_SegmentTree_Title_Single");
                    }
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
        }
        
        /// <summary>
        /// Hide optional controls and resize
        /// </summary>
        private void SetupControl()
        {
            this.ftvSegment.ExecuteFinalSetup();

			//GLOG : 7698 : ceh
            string xDefaultFolder = "";

            try
            {
                xDefaultFolder = LMP.Data.Application.User.GetPreferenceSet(
                    mpPreferenceSetTypes.UserApp, "").GetValue("DefaultContentFolder");
            }
            catch { }

            if (!string.IsNullOrEmpty(xDefaultFolder))
            {
                SelectFolderNode(xDefaultFolder);
            }
        }
        /// <summary>
        /// Select Folder node corresponding to Key
        /// </summary>
        /// <param name="xKey"></param>
        /// <returns></returns>
        private void SelectFolderNode(string xKey)
        {
            UltraTreeNode oNode = LoadNodeIfNecessary(xKey);
            if (oNode != null)
            {
                oNode.Selected = true;
                this.ftvSegment.Tree.ActiveNode = oNode;
                this.ftvSegment.Tree.ActiveNode.Expanded = true;
            }
        }
        /// <summary>
        /// loads the specified folder into the tree if necessary
        /// </summary>
        /// <param name="xFolderPath"></param>
        private UltraTreeNode LoadNodeIfNecessary(string xNodeKey)
        {
            try
            {
                UltraTreeNode oNode = this.ftvSegment.Tree.GetNodeByKey(xNodeKey);
                if (oNode != null)
                    //node already exists in tree
                    return oNode;

                //node does not exist in tree - get node
                UltraTreeNode oNewNode = null;
                string xParentKey, xNewKey;
                string[] xSep = new string[] { mpPathSeparator };
                string[] xPath = xNodeKey.Split(xSep, StringSplitOptions.None);
                for (int i = xPath.GetLowerBound(0); i <= xPath.GetUpperBound(0); i++)
                {
                    if (oNewNode != null)
                        xParentKey = oNewNode.Key + mpPathSeparator;
                    else
                        xParentKey = "";
                    xNewKey = xParentKey + xPath[i];
                    LMP.Trace.WriteNameValuePairs("Level " + i.ToString(), xNewKey);
                    oNewNode = null;
                    oNewNode = this.ftvSegment.Tree.GetNodeByKey(xPath[i]);
                    if (oNewNode != null)
                    {
                        //GLOG item #6670 - dcf
                        if (!oNewNode.Expanded)
                            oNewNode.Expanded = true;
                    }
                    else
                    {
                        // Default path doesn't exist
                        return null;
                    }
                }
                return oNewNode;
            }
            catch
            {
                return null;
            }
        }
        #endregion
        #region *********************properties**************************
        /// <summary>
        /// Returns ID of Segment selected in Treeview, or delimited list of IDs and Names for Multiple selection
        /// </summary>
        public string Value
        {
            get
            {
                return m_xSegmentID;
            }
        }
        public LMP.Controls.FolderTreeView.mpFolderTreeViewOptions DisplayOptions
        {
            get { return this.ftvSegment.DisplayOptions; }
            set { this.ftvSegment.DisplayOptions = value; }
        }
        public UltraTreeNode SelectedNode
        {
            get { return this.ftvSegment.Tree.ActiveNode; }
        }
        //GLOG : 8380 : jsw
        /// <summary>
        /// Get/Set ExcludeNonMacPacSegments for displayed objects
        /// </summary>
        public bool ExcludeNonMacPacTypes
        {
            get { return ftvSegment.ExcludeNonMacPacTypes; }
            set
            {
                ftvSegment.ExcludeNonMacPacTypes = value;
            }
        }

        #endregion
        #region *********************event handlers**************************
        /// <summary>
        /// True if Node corresponds to an VariableSet/Prefill object
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private bool IsVariableSetNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return false;
            else if ((oNode.Tag is string && ((string)oNode.Tag).StartsWith("P")) ||
                (oNode.Tag is FolderMember && ((FolderMember)oNode.Tag).ObjectTypeID == mpFolderMemberTypes.VariableSet) ||
                (oNode.Tag is VariableSetDef))
                return true;
            else
                return false;
        }
        #endregion
        #region *********************event handlers**************************
        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            //GLOG : 8353 : ceh - set dialog result in code
            //return if no selection
            try
            {
                if (ftvSegment.MemberList.Count > 0)
                    m_xSegmentID = ((ArrayList)ftvSegment.MemberList[0])[0].ToString();
            }
            catch { }

            //GLOG 8518 - dcf
            if (m_xSegmentID == "")
            {
                this.ftvSegment.Focus();
                //GLOG : 8353 : jsw
                if (!m_bMultipleSelection)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_NoSegmentSelected"), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_xSegmentID = "";
            this.Close();
        }
        /// <summary>
        /// Set options first time form is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SegmentTreeview_Load(object sender, EventArgs e)
        {
            SetupControl();
        }

        #endregion

        private void SegmentAddEvent(object sender, EventArgs e)
        {

        }

        private void ftvSegment_DoubleClick(object sender, EventArgs e)
        {
            //GLOG 8518
            if (ftvSegment.Tree.ActiveNode.Tag is Folder || ftvSegment.Tree.ActiveNode.Tag is UserFolder 
                ||  ftvSegment.Tree.ActiveNode.Tag is string || ftvSegment.Tree.ActiveNode.Tag is int || ftvSegment.Tree.ActiveNode == null)
            {
                return;
            }

            this.btnOK_Click(sender, e);
        }

    }
}