using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class AuthorPreferencesManager : Form
    {
        private string m_xInitialSegmentID = "0";
        private bool m_bShowTree = true;
        private bool m_bDesignerMode = false;
        private Administration.Controls.AuthorPreferencesManager.Modes m_iMode = Administration.Controls.AuthorPreferencesManager.Modes.User;

        public AuthorPreferencesManager(string xSegmentID, bool bShowTree, bool bDesigner)
        {
            InitializeComponent();
            m_xInitialSegmentID = xSegmentID;
            m_bShowTree = bShowTree;
            if (bDesigner)
            {
                m_iMode = Administration.Controls.AuthorPreferencesManager.Modes.Designer;
                this.authorPreferencesManager1.Mode = Administration.Controls.AuthorPreferencesManager.Modes.Designer;
            }
            this.authorPreferencesManager1.DisplayTree = m_bShowTree;
        }
        public AuthorPreferencesManager(int iSegmentID, bool bShowTree)
        {
            InitializeComponent();
            m_xInitialSegmentID = iSegmentID.ToString();
            m_bShowTree = bShowTree;
            this.authorPreferencesManager1.DisplayTree = m_bShowTree;
        }

        private void AuthorPreferencesManager_Load(object sender, EventArgs e)
        {
            try
            {
                if (m_iMode == Administration.Controls.AuthorPreferencesManager.Modes.User)
                {
                    //GLOG #8492 - dcf - moved from constructor
                    Size oDlgSize = Session.CurrentUser.UserSettings.AuthorPreferencesDialogSize;

                    if (oDlgSize.Width != 0)
                    {
                        this.Width = oDlgSize.Width;
                        this.Height = oDlgSize.Height;
                    }
                    this.btnClose.Size = this.authorPreferencesManager1.ButtonSize;
                    this.btnClose.Top = this.authorPreferencesManager1.Top + this.authorPreferencesManager1.ButtonTop + 2;
                }

                this.CenterToScreen();

                if (m_xInitialSegmentID != "")
                    this.authorPreferencesManager1.SelectSegment(m_xInitialSegmentID);

                this.authorPreferencesManager1.Refresh();
                this.Refresh();
            }
            catch (SystemException oE)
            {
                if (oE.Message.Contains("Key not found") && m_bDesignerMode)
                {
                    this.Visible = false;
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;

                    MessageBox.Show("Segment not found in Author Preferences list.  Please Save Design to update the Segment XML.", "Segment Designer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void AuthorPreferencesManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.authorPreferencesManager1.SaveRecord();

                Session.CurrentUser.UserSettings.AuthorPreferencesDialogSize = new Size(this.Width, this.Height);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }

        private void AuthorPreferencesManager_ResizeEnd(object sender, EventArgs e)
        {
            try
            {
                this.authorPreferencesManager1.RefreshPreferencesPanel();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}