using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.MacPac
{
    public partial class ImportedPeopleManager : LMP.Controls.ManagerBase
    {
        DataTable m_oNetworkUsers = null;
        LocalPersons m_oPublicPeople = null;
        LMP.Data.LocalPersons m_oProxiableUsers = null;

        public ImportedPeopleManager()
        {
            InitializeComponent();
        }
		
        //GLOG : 5808 : jsw
        /// <summary>
        ///Select the first item in lstPeople matching
        /// the text typed into ttxtFindPerson by the user
        /// <param name="xSearchString"></param>
        /// </summary>
        public void SelectMatchingRecord(string xSearchString)
        {
            if (xSearchString != "" && lstPeople.Items.Count > 0 & lstLocalPeople.Items.Count > 0)
            {
                //User has entered a search string
                //Get the index of the first matching record for import people
                int iMatchIndex = this.lstPeople.FindString(xSearchString);

                //if not found, search local people
                if (iMatchIndex == -1)
                {
                    iMatchIndex = this.lstLocalPeople.FindString(xSearchString);
                    if (iMatchIndex > -1)
                      //Match found
                      //select item
                      this.lstLocalPeople.SelectedIndex = iMatchIndex;
                }

                if (iMatchIndex > -1)
                {
                    //Match found
                    //Select the appropriate item
                    this.lstPeople.SelectedIndex = iMatchIndex;
                }
            }
        }
        private LocalPersons LocalPeople
        {
            get
            {
                if (m_oPublicPeople == null)
                    //GLOG 4838: Don't show inactive office records
                    m_oPublicPeople = new LocalPersons(mpPeopleListTypes.AllPeople, UsageStates.OfficeActive);
                return m_oPublicPeople;
            }

            set
            {
                m_oPublicPeople = value;
            }
        }
        /// <summary>
        /// Add button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddLocalPerson_Click(object sender, EventArgs e)
        {
            try
            {
                AddNetworkUserToLocalList();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Assign selected User to the Local People list
        /// </summary>
        private void AddNetworkUserToLocalList()
        {
            if (lstPeople.SelectedIndex > -1)
            {
                //GLOG 15980: SelectedValue may not reflect correct ID if Windows is using non-Latin-1 sort order
                System.Data.DataRowView oItem = (DataRowView)this.lstPeople.SelectedItem;

                int iPersonID = Int32.Parse(oItem[1].ToString());

                if (iPersonID > 0)
                {
                    //GLOG 5446: Check all People, including inactive Offices, for ID match
                    LocalPersons oDBPersons = new LocalPersons(mpPeopleListTypes.AllPeople);
                    // If selected ID is not in local People table, copy record from Network
                    if (!oDBPersons.IsInCollection(iPersonID))
                    {
                        try
                        {
                            ///JTS 5/6/09 - Can't delete People record or all related objects will also get deleted
                            //We will just update FavoritePeople record as necessary
                            ////GLOG - 3691 - ceh
                            ////delete record from local DB if it exists
                            //try
                            //{
                            //    this.LocalPeople.DeleteRecordFromDB(iPersonID);
                            //}
                            //catch { }


                            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                            {
                                this.LocalPeople.CopyPublicUser(iPersonID, true, true);
                            }
                            else
                            {
                                this.LocalPeople.CopyNetworkUser(iPersonID, true);
                            }

                            // Set the LocalPeople to null so that it is refreshed with
                            // the newly added person.
                            this.LocalPeople = null;
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        }
                        RefreshLocalList();
                    }
                    else
                    {
                        System.Data.DataRowView drView = (DataRowView)this.lstPeople.SelectedItem;
                        string xPerson = (string)drView.Row[0];
                        MessageBox.Show(LMP.Resources.GetLangString("Prompt_PersonAlreadyInList"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private LMP.Data.LocalPersons ProxiableUsers
        {
            get
            {
                if (m_oProxiableUsers == null)
                {
                    m_oProxiableUsers = Session.LoginUser.GetProxiableUsers();
                }

                return m_oProxiableUsers;
            }
        }

        /// <summary>
        /// Populate listbox with current Local public people
        /// </summary>
        private void RefreshLocalList()
        {
            //GLOG : 7754 : JSW
            //include office name
            DataTable oPeopleDT = this.LocalPeople.ToDisplayFieldsDataSet(true).Tables[0];
            lstLocalPeople.DataSource = oPeopleDT;
            lstLocalPeople.DisplayMember = "DisplayName";
            lstLocalPeople.ValueMember = "ID";
            lstLocalPeople.Refresh();
        }
        /// <summary>
        /// Remove selected user(s) from the local list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteLocalPerson_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lstLocalPeople.SelectedItems.Count == 0)
                    return;

                ListBox.SelectedObjectCollection oCol = this.lstLocalPeople.SelectedItems;
                int iCurSelection = -1;
                
                if (oCol.Count == 1)
                    iCurSelection = this.lstLocalPeople.SelectedIndex;

                for (int i = 0; i < oCol.Count; i++)
                {
                    DataRowView oRow = (DataRowView)oCol[i];

                    string xID = oRow["ID"].ToString();
                    string[] axIDParts = xID.Split('.');

                    // The ids can have the following formats:
                    // 2144.0  ...............  for a proxy
                    // 9400.0  ...............  for the current user.
                    // 9400.9975577600 .......  for a private person
                    // 9358.0  ...............  for a favorite person
                    // Determine if the user is a proxy, the current user of a favorite person
                    // using only the part of the id left of the decimal when the part of the id
                    // right of the decimal is 0 or non-existant.

                    if(axIDParts.Length == 1 || (axIDParts.Length > 1 && axIDParts[1] == "0"))
                    {
                        // if there is no decimal separator or if the value after the decimal is 0
                        // just use the first part of the id.
                        xID = axIDParts[0];
                    }

                    if ((axIDParts.Length == 1 || (axIDParts.Length > 1 && axIDParts[1] == "0")) && 
                        Int32.Parse(xID) == Session.LoginUser.ID)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Prompt_CannotRemoveSelf"), 
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    //GLOG 5614: Proxiable user ID might correspond to Default Office Record
                    else if((axIDParts.Length == 1 || (axIDParts.Length > 1 && axIDParts[1] == "0")) &&
                        ProxiableUsers.IsInCollection(LocalPeople.ItemFromID(xID).BasePerson.ID))
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Prompt_CannotRemoveProxy"), 
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (LocalPeople.ItemFromID(xID).IsPrivate)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Prompt_CannotRemovePrivatePeople"), 
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        try
                        {
                            //GLOG 5721: If Related Office REcord is selected, remove Base and all other Office Records at the same time
                            if (this.LocalPeople.ItemFromID(xID).ID2 == ForteConstants.mpFirmRecordID && !this.LocalPeople.ItemFromID(xID).IsBasePerson)
                            {
                                //Base record may not be visible in list if not active office
                                LocalPersons oAllPeople = new LocalPersons();
                                oAllPeople.Delete(this.LocalPeople.ItemFromID(xID).BasePerson.ID, true);
                            }
                            else
                                this.LocalPeople.Delete(xID, true);
                        }
                        catch (LMP.Exceptions.StoredProcedureException)
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Error_DeleteLocalPerson") + xID,
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch (System.Exception oE)
                        {
                            LMP.Error.Show(oE);
                        }
                    }
                }
                RefreshLocalList();
                this.lstLocalPeople.ClearSelected();
                if (iCurSelection > -1)
                {
                    // if one item was selected initially, select following item
                    if (iCurSelection < lstLocalPeople.Items.Count)
                        this.lstLocalPeople.SelectedIndex = iCurSelection;
                    else
                        this.lstLocalPeople.SelectedIndex = lstLocalPeople.Items.Count - 1;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Item in Network People list has been double-clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstPeople_DoubleClick(object sender, EventArgs e)
        {
            AddNetworkUserToLocalList();
        }

        private void ImportedPeopleManager_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.DesignMode && (this.Site == null || !this.Site.DesignMode))
                {
                    try
                    {
                        //populate people from network db, if necessary
                        if (m_oNetworkUsers == null)
                            m_oNetworkUsers = LMP.Data.User.GetNetworkUserList();
                    }
                    catch (System.Exception oE)
                    {
                        this.lstPeople.Items.Add(
                            LMP.Resources.GetLangString("Msg_NoNetworkConnectionAvailable"));
                        this.Enabled = false;

                        ////GLOG 2942
                        //throw new System.Exception(LMP.Resources.GetLangString("Prompt_CouldNotRetrieveNetworkAllPeopleList"), oE);

                        //GLOG 4037
                        MessageBox.Show(LMP.Resources.GetLangString("Prompt_CouldNotRetrieveNetworkAllPeopleList"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        return;
                    }
                    this.lstPeople.DisplayMember = "DisplayName";
                    this.lstPeople.ValueMember = "ID1";
                    this.lstPeople.DataSource = m_oNetworkUsers;
                    RefreshLocalList();
                }
            }
            catch (LMP.Exceptions.ConnectionParametersException)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Error_NoNetworkDBConnectionParametersSet"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void ImportedPeopleManager_Resize(object sender, EventArgs e)
        {
            try
            {
                lstPeople.Height = this.Height - (int)(44 * LMP.OS.GetScalingFactor());
                lstLocalPeople.Height = this.Height - (int)(44 * LMP.OS.GetScalingFactor());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

    }
}
