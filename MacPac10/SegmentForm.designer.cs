namespace LMP.MacPac
{
    partial class SegmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ftvSegment = new LMP.Controls.SearchableFolderTreeView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ftvSegment
            // 
            this.ftvSegment.AllowFavoritesMenu = false;
            this.ftvSegment.AllowSearching = true;
            this.ftvSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ftvSegment.AutoSize = true;
            this.ftvSegment.DisableExcludedTypes = false;
            this.ftvSegment.DisplayOptions = ((LMP.Controls.FolderTreeView.mpFolderTreeViewOptions)((((((LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserSegments) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.StyleSheets) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SegmentPackets)));
            this.ftvSegment.ExcludeNonMacPacTypes = false;
            this.ftvSegment.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvSegment.Location = new System.Drawing.Point(5, 3);
            this.ftvSegment.Margin = new System.Windows.Forms.Padding(4);
            this.ftvSegment.Name = "ftvSegment";
            this.ftvSegment.OwnerID = 0;
            this.ftvSegment.ShowCheckboxes = false;
            this.ftvSegment.Size = new System.Drawing.Size(358, 341);
            this.ftvSegment.TabIndex = 1;
            this.ftvSegment.DoubleClick += new System.EventHandler(this.ftvSegment_DoubleClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(283, 354);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.Location = new System.Drawing.Point(202, 354);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // SegmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(368, 388);
            this.Controls.Add(this.ftvSegment);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SegmentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Segment";
            this.Load += new System.EventHandler(this.SegmentTreeview_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LMP.Controls.SearchableFolderTreeView ftvSegment;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }
}