namespace LMP.MacPac
{
    partial class ProxiesManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProxiesManager));
            this.lblPeople = new System.Windows.Forms.Label();
            this.lblProxies = new System.Windows.Forms.Label();
            this.btnDeleteProxy = new System.Windows.Forms.Button();
            this.btnAddProxy = new System.Windows.Forms.Button();
            this.lstPeople = new LMP.Controls.SortedListBox();
            this.lstCurrentProxies = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblPeople
            // 
            this.lblPeople.AutoSize = true;
            this.lblPeople.Location = new System.Drawing.Point(12, 8);
            this.lblPeople.Name = "lblPeople";
            this.lblPeople.Size = new System.Drawing.Size(40, 15);
            this.lblPeople.TabIndex = 0;
            this.lblPeople.Text = "&Users:";
            // 
            // lblProxies
            // 
            this.lblProxies.AutoSize = true;
            this.lblProxies.Location = new System.Drawing.Point(282, 8);
            this.lblProxies.Name = "lblProxies";
            this.lblProxies.Size = new System.Drawing.Size(87, 15);
            this.lblProxies.TabIndex = 4;
            this.lblProxies.Text = "Current &Proxies:";
            // 
            // btnDeleteProxy
            // 
            this.btnDeleteProxy.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteProxy.Image")));
            this.btnDeleteProxy.Location = new System.Drawing.Point(225, 83);
            this.btnDeleteProxy.Name = "btnDeleteProxy";
            this.btnDeleteProxy.Size = new System.Drawing.Size(44, 30);
            this.btnDeleteProxy.TabIndex = 3;
            this.btnDeleteProxy.UseVisualStyleBackColor = true;
            this.btnDeleteProxy.Click += new System.EventHandler(this.btnDeleteProxy_Click);
            // 
            // btnAddProxy
            // 
            this.btnAddProxy.Image = ((System.Drawing.Image)(resources.GetObject("btnAddProxy.Image")));
            this.btnAddProxy.Location = new System.Drawing.Point(225, 46);
            this.btnAddProxy.Name = "btnAddProxy";
            this.btnAddProxy.Size = new System.Drawing.Size(44, 30);
            this.btnAddProxy.TabIndex = 2;
            this.btnAddProxy.UseVisualStyleBackColor = true;
            this.btnAddProxy.Click += new System.EventHandler(this.btnAddProxy_Click);
            // 
            // lstPeople
            // 
            this.lstPeople.AllowEmptyValue = false;
            this.lstPeople.FormattingEnabled = true;
            this.lstPeople.IsDirty = false;
            this.lstPeople.ItemHeight = 15;
            this.lstPeople.ListName = "";
            this.lstPeople.Location = new System.Drawing.Point(12, 24);
            this.lstPeople.Name = "lstPeople";
            this.lstPeople.Size = new System.Drawing.Size(199, 349);
            this.lstPeople.SupportingValues = "";
            this.lstPeople.TabIndex = 1;
            this.lstPeople.Tag2 = null;
            this.lstPeople.Value = "";
            this.lstPeople.DoubleClick += new System.EventHandler(this.btnAddProxy_Click);
            // 
            // lstCurrentProxies
            // 
            this.lstCurrentProxies.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstCurrentProxies.FormattingEnabled = true;
            this.lstCurrentProxies.ItemHeight = 15;
            this.lstCurrentProxies.Location = new System.Drawing.Point(283, 24);
            this.lstCurrentProxies.Name = "lstCurrentProxies";
            this.lstCurrentProxies.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstCurrentProxies.Size = new System.Drawing.Size(199, 349);
            this.lstCurrentProxies.TabIndex = 5;
            // 
            // ProxiesManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lstPeople);
            this.Controls.Add(this.lstCurrentProxies);
            this.Controls.Add(this.btnDeleteProxy);
            this.Controls.Add(this.btnAddProxy);
            this.Controls.Add(this.lblPeople);
            this.Controls.Add(this.lblProxies);
            this.Name = "ProxiesManager";
            this.Size = new System.Drawing.Size(503, 404);
            this.Load += new System.EventHandler(this.ProxiesManager_Load);
            this.Resize += new System.EventHandler(this.ProxiesManager_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPeople;
        private System.Windows.Forms.Label lblProxies;
        private System.Windows.Forms.Button btnDeleteProxy;
        private System.Windows.Forms.Button btnAddProxy;
        private LMP.Controls.SortedListBox lstPeople;
        private System.Windows.Forms.ListBox lstCurrentProxies;
    }
}
