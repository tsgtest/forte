namespace LMP.MacPac
{
    partial class UserPreferencesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.btnClose = new System.Windows.Forms.Button();
            this.scPanels = new System.Windows.Forms.SplitContainer();
            this.treeCategories = new Infragistics.Win.UltraWinTree.UltraTree();
            ((System.ComponentModel.ISupportInitialize)(this.scPanels)).BeginInit();
            this.scPanels.Panel1.SuspendLayout();
            this.scPanels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeCategories)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Location = new System.Drawing.Point(820, 547);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 31);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // scPanels
            // 
            this.scPanels.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scPanels.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scPanels.IsSplitterFixed = true;
            this.scPanels.Location = new System.Drawing.Point(0, 0);
            this.scPanels.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.scPanels.Name = "scPanels";
            // 
            // scPanels.Panel1
            // 
            this.scPanels.Panel1.BackColor = System.Drawing.Color.White;
            this.scPanels.Panel1.Controls.Add(this.treeCategories);
            // 
            // scPanels.Panel2
            // 
            this.scPanels.Panel2.BackColor = System.Drawing.Color.White;
            this.scPanels.Size = new System.Drawing.Size(950, 528);
            this.scPanels.SplitterDistance = 182;
            this.scPanels.TabIndex = 9;
            // 
            // treeCategories
            // 
            this.treeCategories.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            appearance1.BackColor = System.Drawing.Color.WhiteSmoke;
            appearance1.BackColor2 = System.Drawing.Color.LightGray;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            this.treeCategories.Appearance = appearance1;
            this.treeCategories.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.treeCategories.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeCategories.FullRowSelect = true;
            this.treeCategories.Location = new System.Drawing.Point(0, 0);
            this.treeCategories.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.treeCategories.Name = "treeCategories";
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            appearance2.FontData.Name = "Tahoma";
            appearance2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            _override1.NodeAppearance = appearance2;
            _override1.NodeDoubleClickAction = Infragistics.Win.UltraWinTree.NodeDoubleClickAction.None;
            _override1.NodeSpacingAfter = 0;
            _override1.NodeSpacingBefore = 6;
            appearance3.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            _override1.SelectedNodeAppearance = appearance3;
            this.treeCategories.Override = _override1;
            this.treeCategories.ShowLines = false;
            this.treeCategories.ShowRootLines = false;
            this.treeCategories.Size = new System.Drawing.Size(228, 528);
            this.treeCategories.TabIndex = 5;
            this.treeCategories.UpdateMode = Infragistics.Win.UltraWinTree.UpdateMode.OnActiveNodeChange;
            this.treeCategories.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeCategories_AfterSelect);
            // 
            // UserPreferencesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(945, 597);
            this.Controls.Add(this.scPanels);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserPreferencesForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Settings";
            this.Load += new System.EventHandler(this.UserApplicationSettingsForm_Load);
            this.scPanels.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scPanels)).EndInit();
            this.scPanels.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeCategories)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.SplitContainer scPanels;
        private Infragistics.Win.UltraWinTree.UltraTree treeCategories;

    }
}