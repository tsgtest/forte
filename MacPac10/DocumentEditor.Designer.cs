using System;
using LMP.Controls;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    internal partial class DocumentEditor : System.Windows.Forms.UserControl
    {
        #region *********************fields*********************
        private System.Windows.Forms.Panel pnlDocContents;
        private System.Windows.Forms.Splitter splitContents;
        private System.Windows.Forms.Panel pnlDescription;
        private System.Windows.Forms.ImageList ilImages;
        private System.ComponentModel.IContainer components;
        private LMP.Controls.TreeView treeDocContents;
        private System.Windows.Forms.Button btnContacts;
        private PictureBox picMenu;
        private ToolStripMenuItem copyToolStripMenuItem;
        private ToolStripMenuItem saveAsWebPageToolStripMenuItem;
        private ToolStripMenuItem printPreviewToolStripMenuItem;
        private ToolStripMenuItem printToolStripMenuItem;
        private TimedContextMenuStrip mnuQuickHelp;
        private ToolStripMenuItem mnuQuickHelp_Copy;
        private ToolStripMenuItem mnuQuickHelp_SaveAs;
        private ToolStripMenuItem mnuQuickHelp_Print;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem mnuQuickHelp_PrintPreview;
        #endregion
        #region *********************Component Designer generated code*********************
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentEditor));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.pnlDocContents = new System.Windows.Forms.Panel();
            this.tsEditor = new System.Windows.Forms.ToolStrip();
            this.tbtnFinish = new System.Windows.Forms.ToolStripButton();
            this.tsSepFinish = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnSpacerLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tbtnInsertSavedData = new System.Windows.Forms.ToolStripButton();
            this.tsSepInsertSavedData = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnSaveData = new System.Windows.Forms.ToolStripButton();
            this.tsSepSaveData = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnSaveDataAs = new System.Windows.Forms.ToolStripButton();
            this.tsSepSaveDataAs = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnRecreate = new System.Windows.Forms.ToolStripButton();
            this.tsSepRecreate = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnSaveSegment = new System.Windows.Forms.ToolStripButton();
            this.tsSepSaveSegment = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnSpacerLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tbtnClose = new System.Windows.Forms.ToolStripButton();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.picMenu = new System.Windows.Forms.PictureBox();
            this.btnContacts = new System.Windows.Forms.Button();
            this.treeDocContents = new LMP.Controls.TreeView();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.splitContents = new System.Windows.Forms.Splitter();
            this.pnlDescription = new System.Windows.Forms.Panel();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.stsStatus = new System.Windows.Forms.StatusStrip();
            this.tsSelectedPath = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.wbDescription = new System.Windows.Forms.WebBrowser();
            this.mnuQuickHelp = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuQuickHelp_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuQuickHelp_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuQuickHelp_PrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQuickHelp_Print = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsWebPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlDocContents.SuspendLayout();
            this.tsEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeDocContents)).BeginInit();
            this.pnlDescription.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            this.stsStatus.SuspendLayout();
            this.mnuQuickHelp.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDocContents
            // 
            this.pnlDocContents.BackColor = System.Drawing.Color.Transparent;
            this.pnlDocContents.Controls.Add(this.tsEditor);
            this.pnlDocContents.Controls.Add(this.button3);
            this.pnlDocContents.Controls.Add(this.button2);
            this.pnlDocContents.Controls.Add(this.button1);
            this.pnlDocContents.Controls.Add(this.picMenu);
            this.pnlDocContents.Controls.Add(this.btnContacts);
            this.pnlDocContents.Controls.Add(this.treeDocContents);
            this.pnlDocContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDocContents.Location = new System.Drawing.Point(0, 0);
            this.pnlDocContents.Name = "pnlDocContents";
            this.pnlDocContents.Size = new System.Drawing.Size(324, 448);
            this.pnlDocContents.TabIndex = 19;
            this.pnlDocContents.SizeChanged += new System.EventHandler(this.pnlDocContents_SizeChanged);
            // 
            // tsEditor
            // 
            this.tsEditor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tsEditor.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsEditor.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.tsEditor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnFinish,
            this.tsSepFinish,
            this.tbtnSpacerLabel1,
            this.tbtnInsertSavedData,
            this.tsSepInsertSavedData,
            this.tbtnSaveData,
            this.tsSepSaveData,
            this.tbtnSaveDataAs,
            this.tsSepSaveDataAs,
            this.tbtnRecreate,
            this.tsSepRecreate,
            this.tbtnSaveSegment,
            this.tsSepSaveSegment,
            this.tbtnSpacerLabel2,
            this.tbtnClose});
            this.tsEditor.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsEditor.Location = new System.Drawing.Point(0, 423);
            this.tsEditor.Name = "tsEditor";
            this.tsEditor.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsEditor.Size = new System.Drawing.Size(324, 25);
            this.tsEditor.TabIndex = 57;
            // 
            // tbtnFinish
            // 
            this.tbtnFinish.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnFinish.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnFinish.Image = ((System.Drawing.Image)(resources.GetObject("tbtnFinish.Image")));
            this.tbtnFinish.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnFinish.Name = "tbtnFinish";
            this.tbtnFinish.Size = new System.Drawing.Size(43, 22);
            this.tbtnFinish.Text = "Finish";
            this.tbtnFinish.Click += new System.EventHandler(this.tbtnFinish_Click);
            // 
            // tsSepFinish
            // 
            this.tsSepFinish.Name = "tsSepFinish";
            this.tsSepFinish.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnSpacerLabel1
            // 
            this.tbtnSpacerLabel1.Name = "tbtnSpacerLabel1";
            this.tbtnSpacerLabel1.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tbtnSpacerLabel1.Size = new System.Drawing.Size(25, 22);
            this.tbtnSpacerLabel1.Text = "      ";
            // 
            // tbtnInsertSavedData
            // 
            this.tbtnInsertSavedData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnInsertSavedData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnInsertSavedData.Image = ((System.Drawing.Image)(resources.GetObject("tbtnInsertSavedData.Image")));
            this.tbtnInsertSavedData.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnInsertSavedData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnInsertSavedData.Name = "tbtnInsertSavedData";
            this.tbtnInsertSavedData.Size = new System.Drawing.Size(73, 22);
            this.tbtnInsertSavedData.Text = "Apply Data";
            this.tbtnInsertSavedData.ToolTipText = "Apply saved data values to the active segment";
            this.tbtnInsertSavedData.Click += new System.EventHandler(this.tbtnInsertSavedData_Click);
            // 
            // tsSepInsertSavedData
            // 
            this.tsSepInsertSavedData.Name = "tsSepInsertSavedData";
            this.tsSepInsertSavedData.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnSaveData
            // 
            this.tbtnSaveData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSaveData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnSaveData.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSaveData.Image")));
            this.tbtnSaveData.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnSaveData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSaveData.Name = "tbtnSaveData";
            this.tbtnSaveData.Size = new System.Drawing.Size(69, 22);
            this.tbtnSaveData.Text = "Save Data";
            this.tbtnSaveData.ToolTipText = "Save data for future use";
            this.tbtnSaveData.Click += new System.EventHandler(this.tbtnSaveData_Click);
            // 
            // tsSepSaveData
            // 
            this.tsSepSaveData.Name = "tsSepSaveData";
            this.tsSepSaveData.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnSaveDataAs
            // 
            this.tbtnSaveDataAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSaveDataAs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnSaveDataAs.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSaveDataAs.Image")));
            this.tbtnSaveDataAs.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnSaveDataAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSaveDataAs.Name = "tbtnSaveDataAs";
            this.tbtnSaveDataAs.Size = new System.Drawing.Size(86, 17);
            this.tbtnSaveDataAs.Text = "Save Data As";
            this.tbtnSaveDataAs.ToolTipText = "Save data for future reuse";
            this.tbtnSaveDataAs.Click += new System.EventHandler(this.tbtnSaveDataAs_Click);
            // 
            // tsSepSaveDataAs
            // 
            this.tsSepSaveDataAs.Name = "tsSepSaveDataAs";
            this.tsSepSaveDataAs.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnRecreate
            // 
            this.tbtnRecreate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnRecreate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnRecreate.Image = ((System.Drawing.Image)(resources.GetObject("tbtnRecreate.Image")));
            this.tbtnRecreate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tbtnRecreate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnRecreate.Name = "tbtnRecreate";
            this.tbtnRecreate.Size = new System.Drawing.Size(63, 17);
            this.tbtnRecreate.Text = "Recreate";
            this.tbtnRecreate.ToolTipText = "Recreate the active segment";
            this.tbtnRecreate.Click += new System.EventHandler(this.tbtnRecreate_Click);
            // 
            // tsSepRecreate
            // 
            this.tsSepRecreate.Name = "tsSepRecreate";
            this.tsSepRecreate.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnSaveSegment
            // 
            this.tbtnSaveSegment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSaveSegment.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnSaveSegment.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSaveSegment.Image")));
            this.tbtnSaveSegment.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSaveSegment.Name = "tbtnSaveSegment";
            this.tbtnSaveSegment.Size = new System.Drawing.Size(93, 17);
            this.tbtnSaveSegment.Text = "Save Segment";
            this.tbtnSaveSegment.ToolTipText = "Save the entire segment with its data for future use";
            this.tbtnSaveSegment.Click += new System.EventHandler(this.tbtnSaveSegment_Click);
            // 
            // tsSepSaveSegment
            // 
            this.tsSepSaveSegment.Name = "tsSepSaveSegment";
            this.tsSepSaveSegment.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnSpacerLabel2
            // 
            this.tbtnSpacerLabel2.BackColor = System.Drawing.Color.Transparent;
            this.tbtnSpacerLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSpacerLabel2.Name = "tbtnSpacerLabel2";
            this.tbtnSpacerLabel2.Size = new System.Drawing.Size(25, 15);
            this.tbtnSpacerLabel2.Text = "      ";
            // 
            // tbtnClose
            // 
            this.tbtnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbtnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tbtnClose.Image = ((System.Drawing.Image)(resources.GetObject("tbtnClose.Image")));
            this.tbtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnClose.Name = "tbtnClose";
            this.tbtnClose.Size = new System.Drawing.Size(41, 17);
            this.tbtnClose.Text = "Close";
            this.tbtnClose.Click += new System.EventHandler(this.tbtnClose_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(296, 95);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(25, 22);
            this.button3.TabIndex = 51;
            this.button3.TabStop = false;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(296, 67);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 22);
            this.button2.TabIndex = 51;
            this.button2.TabStop = false;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(296, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 22);
            this.button1.TabIndex = 50;
            this.button1.TabStop = false;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // picMenu
            // 
            this.picMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picMenu.Image = ((System.Drawing.Image)(resources.GetObject("picMenu.Image")));
            this.picMenu.Location = new System.Drawing.Point(306, 13);
            this.picMenu.Name = "picMenu";
            this.picMenu.Size = new System.Drawing.Size(11, 11);
            this.picMenu.TabIndex = 49;
            this.picMenu.TabStop = false;
            this.picMenu.Visible = false;
            this.picMenu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picMenu_MouseClick);
            // 
            // btnContacts
            // 
            this.btnContacts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnContacts.Image = ((System.Drawing.Image)(resources.GetObject("btnContacts.Image")));
            this.btnContacts.Location = new System.Drawing.Point(298, 383);
            this.btnContacts.Name = "btnContacts";
            this.btnContacts.Size = new System.Drawing.Size(25, 22);
            this.btnContacts.TabIndex = 48;
            this.btnContacts.TabStop = false;
            this.btnContacts.UseVisualStyleBackColor = true;
            this.btnContacts.Visible = false;
            this.btnContacts.Click += new System.EventHandler(this.btnContacts_Click);
            // 
            // treeDocContents
            // 
            this.treeDocContents.AllowKeyboardSearch = false;
            this.treeDocContents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.White;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.BackwardDiagonal;
            this.treeDocContents.Appearance = appearance1;
            this.treeDocContents.BorderStyle = Infragistics.Win.UIElementBorderStyle.Inset;
            this.treeDocContents.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeDocContents.ImageList = this.ilImages;
            this.treeDocContents.IsDirty = false;
            this.treeDocContents.Location = new System.Drawing.Point(1, 2);
            this.treeDocContents.Name = "treeDocContents";
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.ForeColor = System.Drawing.Color.Blue;
            _override1.ActiveNodeAppearance = appearance2;
            _override1.AllowCopy = Infragistics.Win.DefaultableBoolean.False;
            _override1.AllowCut = Infragistics.Win.DefaultableBoolean.False;
            _override1.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            _override1.AllowPaste = Infragistics.Win.DefaultableBoolean.False;
            _override1.BorderStyleNode = Infragistics.Win.UIElementBorderStyle.None;
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.ForeColor = System.Drawing.Color.Black;
            _override1.ExpandedNodeAppearance = appearance3;
            _override1.ItemHeight = 22;
            _override1.Multiline = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.ForeColor = System.Drawing.Color.Black;
            _override1.NodeAppearance = appearance4;
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.ForeColor = System.Drawing.Color.Black;
            _override1.SelectedNodeAppearance = appearance5;
            this.treeDocContents.Override = _override1;
            this.treeDocContents.ScrollBounds = Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill;
            this.treeDocContents.Size = new System.Drawing.Size(322, 421);
            this.treeDocContents.SupportingValues = "";
            this.treeDocContents.TabIndex = 0;
            this.treeDocContents.Tag2 = null;
            this.treeDocContents.Value = null;
            this.treeDocContents.AfterActivate += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeDocContents_AfterActivate);
            this.treeDocContents.AfterCollapse += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeDocContents_AfterCollapse);
            this.treeDocContents.AfterExpand += new Infragistics.Win.UltraWinTree.AfterNodeChangedEventHandler(this.treeDocContents_AfterExpand);
            this.treeDocContents.BeforeActivate += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeActivate);
            this.treeDocContents.BeforeCollapse += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeCollapse);
            this.treeDocContents.BeforeExpand += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.treeDocContents_BeforeExpand);
            this.treeDocContents.Scroll += new Infragistics.Win.UltraWinTree.TreeScrollEventHandler(this.treeDocContents_Scroll);
            this.treeDocContents.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeDocContents_KeyDown);
            this.treeDocContents.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeDocContents_KeyUp);
            this.treeDocContents.Leave += new System.EventHandler(this.treeDocContents_Leave);
            this.treeDocContents.MouseClick += new System.Windows.Forms.MouseEventHandler(this.treeDocContents_MouseClick);
            this.treeDocContents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeDocContents_MouseDown);
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Transparent;
            this.ilImages.Images.SetKeyName(0, "ValidDocument");
            this.ilImages.Images.SetKeyName(1, "InvalidDocumentSegment");
            this.ilImages.Images.SetKeyName(2, "ValidComponentSegment");
            this.ilImages.Images.SetKeyName(3, "InvalidComponentSegment");
            this.ilImages.Images.SetKeyName(4, "ValidTextBlock");
            this.ilImages.Images.SetKeyName(5, "InvalidTextBlock");
            this.ilImages.Images.SetKeyName(6, "ValidVariable");
            this.ilImages.Images.SetKeyName(7, "InvalidVariable");
            this.ilImages.Images.SetKeyName(8, "HotkeyA.bmp");
            this.ilImages.Images.SetKeyName(9, "HotkeyB.bmp");
            this.ilImages.Images.SetKeyName(10, "HotkeyC.bmp");
            this.ilImages.Images.SetKeyName(11, "HotkeyD.bmp");
            this.ilImages.Images.SetKeyName(12, "HotkeyE.bmp");
            this.ilImages.Images.SetKeyName(13, "HotkeyF.bmp");
            this.ilImages.Images.SetKeyName(14, "HotkeyG.bmp");
            this.ilImages.Images.SetKeyName(15, "HotkeyH.bmp");
            this.ilImages.Images.SetKeyName(16, "HotkeyI.bmp");
            this.ilImages.Images.SetKeyName(17, "HotkeyJ.bmp");
            this.ilImages.Images.SetKeyName(18, "HotkeyK.bmp");
            this.ilImages.Images.SetKeyName(19, "HotkeyL.bmp");
            this.ilImages.Images.SetKeyName(20, "HotkeyM.bmp");
            this.ilImages.Images.SetKeyName(21, "HotkeyN.bmp");
            this.ilImages.Images.SetKeyName(22, "HotkeyO.bmp");
            this.ilImages.Images.SetKeyName(23, "HotkeyP.bmp");
            this.ilImages.Images.SetKeyName(24, "HotkeyQ.bmp");
            this.ilImages.Images.SetKeyName(25, "HotkeyR.bmp");
            this.ilImages.Images.SetKeyName(26, "HotkeyS.bmp");
            this.ilImages.Images.SetKeyName(27, "HotkeyT.bmp");
            this.ilImages.Images.SetKeyName(28, "HotkeyU.bmp");
            this.ilImages.Images.SetKeyName(29, "HotkeyV.bmp");
            this.ilImages.Images.SetKeyName(30, "HotkeyW.bmp");
            this.ilImages.Images.SetKeyName(31, "HotkeyX.bmp");
            this.ilImages.Images.SetKeyName(32, "Hotkey1.bmp");
            this.ilImages.Images.SetKeyName(33, "Hotkey2.bmp");
            this.ilImages.Images.SetKeyName(34, "Hotkey3.bmp");
            this.ilImages.Images.SetKeyName(35, "Hotkey4.bmp");
            this.ilImages.Images.SetKeyName(36, "Hotkey5.bmp");
            this.ilImages.Images.SetKeyName(37, "Hotkey6.bmp");
            this.ilImages.Images.SetKeyName(38, "Hotkey7.bmp");
            this.ilImages.Images.SetKeyName(39, "Hotkey8.bmp");
            this.ilImages.Images.SetKeyName(40, "Hotkey9.bmp");
            this.ilImages.Images.SetKeyName(41, "IncompleteVariable");
            this.ilImages.Images.SetKeyName(42, "IncompleteDocumentSegment");
            this.ilImages.Images.SetKeyName(43, "IncompleteDocumentComponent");
            this.ilImages.Images.SetKeyName(44, "IncompleteTextBlockSegment");
            // 
            // splitContents
            // 
            this.splitContents.BackColor = System.Drawing.Color.SlateGray;
            this.splitContents.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContents.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitContents.Location = new System.Drawing.Point(0, 448);
            this.splitContents.Name = "splitContents";
            this.splitContents.Size = new System.Drawing.Size(324, 5);
            this.splitContents.TabIndex = 40;
            this.splitContents.TabStop = false;
            this.splitContents.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContents_SplitterMoved);
            // 
            // pnlDescription
            // 
            this.pnlDescription.BackColor = System.Drawing.Color.Transparent;
            this.pnlDescription.Controls.Add(this.picHelp);
            this.pnlDescription.Controls.Add(this.lblStatus);
            this.pnlDescription.Controls.Add(this.stsStatus);
            this.pnlDescription.Controls.Add(this.wbDescription);
            this.pnlDescription.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDescription.Location = new System.Drawing.Point(0, 453);
            this.pnlDescription.Name = "pnlDescription";
            this.pnlDescription.Size = new System.Drawing.Size(324, 75);
            this.pnlDescription.TabIndex = 39;
            // 
            // picHelp
            // 
            this.picHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picHelp.Image = ((System.Drawing.Image)(resources.GetObject("picHelp.Image")));
            this.picHelp.Location = new System.Drawing.Point(306, 3);
            this.picHelp.Name = "picHelp";
            this.picHelp.Size = new System.Drawing.Size(15, 15);
            this.picHelp.TabIndex = 61;
            this.picHelp.TabStop = false;
            this.picHelp.Click += new System.EventHandler(this.picHelp_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(3, 31);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(297, 14);
            this.lblStatus.TabIndex = 60;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lblStatus.Visible = false;
            // 
            // stsStatus
            // 
            this.stsStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.stsStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSelectedPath,
            this.tslblStatus});
            this.stsStatus.Location = new System.Drawing.Point(0, 0);
            this.stsStatus.Name = "stsStatus";
            this.stsStatus.Size = new System.Drawing.Size(324, 22);
            this.stsStatus.TabIndex = 59;
            this.stsStatus.Text = "statusStrip1";
            // 
            // tsSelectedPath
            // 
            this.tsSelectedPath.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsSelectedPath.Name = "tsSelectedPath";
            this.tsSelectedPath.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tsSelectedPath.Size = new System.Drawing.Size(0, 17);
            this.tsSelectedPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tslblStatus
            // 
            this.tslblStatus.Name = "tslblStatus";
            this.tslblStatus.Size = new System.Drawing.Size(10, 17);
            this.tslblStatus.Text = " ";
            // 
            // wbDescription
            // 
            this.wbDescription.AllowWebBrowserDrop = false;
            this.wbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wbDescription.CausesValidation = false;
            this.wbDescription.ContextMenuStrip = this.mnuQuickHelp;
            this.wbDescription.IsWebBrowserContextMenuEnabled = false;
            this.wbDescription.Location = new System.Drawing.Point(0, 21);
            this.wbDescription.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbDescription.Name = "wbDescription";
            this.wbDescription.ScriptErrorsSuppressed = true;
            this.wbDescription.Size = new System.Drawing.Size(324, 54);
            this.wbDescription.TabIndex = 42;
            this.wbDescription.TabStop = false;
            this.wbDescription.Url = new System.Uri("http://f", System.UriKind.Absolute);
            this.wbDescription.WebBrowserShortcutsEnabled = false;
            this.wbDescription.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.wbDescription_PreviewKeyDown);
            // 
            // mnuQuickHelp
            // 
            this.mnuQuickHelp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuQuickHelp_Copy,
            this.toolStripSeparator2,
            this.mnuQuickHelp_SaveAs,
            this.toolStripSeparator1,
            this.mnuQuickHelp_PrintPreview,
            this.mnuQuickHelp_Print});
            this.mnuQuickHelp.Name = "mnuQuickHelp";
            this.mnuQuickHelp.ShowImageMargin = false;
            this.mnuQuickHelp.Size = new System.Drawing.Size(155, 104);
            this.mnuQuickHelp.TimerInterval = 500;
            this.mnuQuickHelp.UseTimer = true;
            // 
            // mnuQuickHelp_Copy
            // 
            this.mnuQuickHelp_Copy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.mnuQuickHelp_Copy.Name = "mnuQuickHelp_Copy";
            this.mnuQuickHelp_Copy.ShortcutKeyDisplayString = "";
            this.mnuQuickHelp_Copy.ShowShortcutKeys = false;
            this.mnuQuickHelp_Copy.Size = new System.Drawing.Size(154, 22);
            this.mnuQuickHelp_Copy.Text = "&Copy";
            this.mnuQuickHelp_Copy.Click += new System.EventHandler(this.mnuQuickHelp_Copy_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(151, 6);
            // 
            // mnuQuickHelp_SaveAs
            // 
            this.mnuQuickHelp_SaveAs.Name = "mnuQuickHelp_SaveAs";
            this.mnuQuickHelp_SaveAs.Size = new System.Drawing.Size(154, 22);
            this.mnuQuickHelp_SaveAs.Text = "Save As &Web Page...";
            this.mnuQuickHelp_SaveAs.Click += new System.EventHandler(this.mnuQuickHelp_SaveAs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(151, 6);
            // 
            // mnuQuickHelp_PrintPreview
            // 
            this.mnuQuickHelp_PrintPreview.Name = "mnuQuickHelp_PrintPreview";
            this.mnuQuickHelp_PrintPreview.Size = new System.Drawing.Size(154, 22);
            this.mnuQuickHelp_PrintPreview.Text = "Print Pre&view...";
            this.mnuQuickHelp_PrintPreview.Click += new System.EventHandler(this.mnuQuickHelp_PrintPreview_Click);
            // 
            // mnuQuickHelp_Print
            // 
            this.mnuQuickHelp_Print.Name = "mnuQuickHelp_Print";
            this.mnuQuickHelp_Print.Size = new System.Drawing.Size(154, 22);
            this.mnuQuickHelp_Print.Text = "&Print...";
            this.mnuQuickHelp_Print.Click += new System.EventHandler(this.mnuQuickHelp_Print_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            // 
            // saveAsWebPageToolStripMenuItem
            // 
            this.saveAsWebPageToolStripMenuItem.Name = "saveAsWebPageToolStripMenuItem";
            this.saveAsWebPageToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.saveAsWebPageToolStripMenuItem.Text = "Save As &Web Page...";
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.printPreviewToolStripMenuItem.Text = "Print Pre&view...";
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.printToolStripMenuItem.Text = "&Print...";
            // 
            // DocumentEditor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnlDocContents);
            this.Controls.Add(this.splitContents);
            this.Controls.Add(this.pnlDescription);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DocumentEditor";
            this.Size = new System.Drawing.Size(324, 528);
            this.Load += new System.EventHandler(this.DocumentEditor_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DocumentEditor_KeyDown);
            this.Leave += new System.EventHandler(this.DocumentEditor_Leave);
            this.Resize += new System.EventHandler(this.DocumentEditor_Resize);
            this.pnlDocContents.ResumeLayout(false);
            this.pnlDocContents.PerformLayout();
            this.tsEditor.ResumeLayout(false);
            this.tsEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeDocContents)).EndInit();
            this.pnlDescription.ResumeLayout(false);
            this.pnlDescription.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            this.stsStatus.ResumeLayout(false);
            this.stsStatus.PerformLayout();
            this.mnuQuickHelp.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private Button button2;
        private Button button1;
        private Button button3;
        private ToolStrip tsEditor;
        private ToolStripButton tbtnInsertSavedData;
        private ToolStripButton tbtnFinish;
        private PictureBox picHelp;
        private Label lblStatus;
        private StatusStrip stsStatus;
        private ToolStripStatusLabel tsSelectedPath;
        private ToolStripStatusLabel tslblStatus;
        private ToolStripButton tbtnRecreate;
        private ToolStripButton tbtnSaveDataAs;
        private ToolStripSeparator tsSepInsertSavedData;
        private ToolStripSeparator tsSepRecreate;
        private ToolStripSeparator tsSepSaveData;
        private ToolStripSeparator tsSepSaveSegment;
        private ToolStripButton tbtnSaveSegment;
        private ToolStripLabel tbtnSpacerLabel2;
        private ToolStripButton tbtnSaveData;
        private ToolStripSeparator tsSepSaveDataAs;
        private ToolStripButton tbtnClose;
        private ToolStripSeparator tsSepFinish;
        private ToolStripLabel tbtnSpacerLabel1;


    }
}
