using System;
using System.Collections.Generic;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using LMP.fDMS;

namespace LMP.MacPac
{
    public class DMSIntegrator
    {
        #region *********************fields**********************
        private DMSBackend m_oDocumentManager = null;
        private bool m_bInEventHandler = false;
		#endregion
		#region *********************constructors*********************
		static DMSIntegrator(){}
		#endregion
		#region *********************properties*********************
        public DMSBackend DocumentManager
        {
            get
            {
                if (m_oDocumentManager == null)
                {
                    m_oDocumentManager = DMS.ProfileObject;
                    //Enable event handler
                    m_oDocumentManager.FileSaved += new FileSavedHandler(m_oDocumentManager_FileSaved);
                }
                return m_oDocumentManager;
            }
        }
        #endregion
		#region *********************methods*********************
        public void FileSave()
        {
            LMP.Trace.WriteInfo("FileSave");
            try
            {
                this.DocumentManager.FileSave();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public void FileSaveAs()
        {
            LMP.Trace.WriteInfo("FileSaveAs");
            try
            {
                this.DocumentManager.FileSaveAs();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public void FileSaveAll()
        {
            LMP.Trace.WriteInfo("FileSaveAll");
            try
            {
                Word.Application oWord = LMP.MacPac.Session.CurrentWordApp;
                for (int i = 1; i < Session.CurrentWordApp.Documents.Count; i++)
                {
                    object oIndex = i;
                    Word.Document oDoc = oWord.Documents.get_Item(ref oIndex);

                    if (!oDoc.Saved)
                    {
                        oDoc.Activate();

                        this.DocumentManager.FileSave();
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public void FileClose()
        {
            LMP.Trace.WriteInfo("FileClose");
            try
            {
                this.DocumentManager.FileClose();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public bool FileCloseAll()
        {
            LMP.Trace.WriteInfo("FileCloseAll");
            try
            {
                Word.Application oWord = LMP.MacPac.Session.CurrentWordApp;
                while (oWord.Documents.Count > 0)
                {
                    int iCount = oWord.Documents.Count;
                    this.DocumentManager.FileClose();
                    if (iCount == oWord.Documents.Count)
                        //Close was cancelled
                        return false;
                }
                return true;
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public void DocClose()
        {
            LMP.Trace.WriteInfo("DocClose");
            try
            {
                this.DocumentManager.DocClose();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        //JTS 3/29/16: This not implemented for any backends
        //public void FileOpen()
        //{
        //    LMP.Trace.WriteInfo("FileOpen");
        //    try
        //    {
        //        this.DocumentManager.FileOpen();
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw oE;
        //    }
        //}
        public void FileExit()
        {
            LMP.Trace.WriteInfo("FileExit");
            try
            {
                if (FileCloseAll())
                {
                    object oFalse = false;
                    object oMissing = System.Reflection.Missing.Value;
                    //GLOG 5663:  Don't want to set first parameter to False here, or Normal.dotm changes won't get saved
                    Session.CurrentWordApp.Quit(ref oMissing, ref oMissing, ref oMissing);
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public void FilePrint()
        {
            LMP.Trace.WriteInfo("FilePrint");
            try
            {
                if (LMP.Forte.MSWord.WordApp.ActiveDocPath() != "")
                {
                    LMP.MacPac.Application.InsertTrailerIfNecessary();
                    LMP.Forte.MSWord.WordApp.FilePrint();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public void FilePrintDefault()
        {
            LMP.Trace.WriteInfo("FilePrint");
            try
            {
                if (LMP.Forte.MSWord.WordApp.ActiveDocPath() != "")
                {
                    LMP.MacPac.Application.InsertTrailerIfNecessary();
                    LMP.Forte.MSWord.WordApp.FilePrintDefault();
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        public void ProfileDocument()
        {
            switch (this.DocumentManager.DMSType)
            {
                case DMSBackendType.Windows:
                    return;
                case DMSBackendType.IManageWork:
                    LMP.Forte.MSWord.WordApp.ExecuteFileSaveAsCommand();
                    break;
                default:
                    this.DocumentManager.FileSaveAs();
                    break;
            }
        }

        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// Event raise by mpDMS.dll
        /// </summary>
        void m_oDocumentManager_FileSaved()
        {
            try
            {
                if (!m_bInEventHandler)
                {
                    //Prevent recursive calls
                    m_bInEventHandler = true;

                    LMP.MacPac.Application.InsertTrailerIfNecessary();
                    //Save if necessary
                    if (!LMP.Forte.MSWord.WordApp.ActiveDocument().Saved)
                        this.DocumentManager.FileSave();
                    //GLOG 4482: Moved this out of finally block
                    //to ensure flag won't be reset by recursive call
                    m_bInEventHandler = false;
                }
            }
            catch (System.Exception oE)
            {
                m_bInEventHandler = false;
                LMP.Error.Show(oE);
            }
        }
        #endregion
    }
}
