using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;
using LMP.Architect.Api;
using Infragistics.Win;
using Infragistics.Win.UltraWinTree;
using LMP.Controls;
using System.Xml;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections.Specialized;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Linq;
using CollectionTableStructure = LMP.Architect.Base.CollectionTableStructure;

namespace LMP.MacPac
{
	/// <summary>
	/// Summary description for DocumentEditor.
	/// ********************************************************************************
	/// NOTES: 
	/// 1) in the tree, node keys have the following forms:
	/// for segment nodes, key is the tag id.  for variable nodes, key is the 
	/// parent segment tag id + "." + variable id
	/// for author nodes, key is parent segment tag id + ".Authors".
	/// 2) tree node tags contain the following object references:
	/// segment node tags contain a ref to the segment.
	/// variable node tags contain a ref to the variable.
	/// author node tags contain a ref to the segment authors.
	/// value node tags contain a ref to associated control, if there is one.
	/// ********************************************************************************
	/// </summary>
    internal partial class DocumentEditor : UserControl
    {
        #region *********************fields*********************
        private System.Windows.Forms.Control m_oCurCtl;
        private UltraTreeNode m_oCurNode;
        private TaskPane m_oTaskPane;
        private ForteDocument m_oMPDocument;
        private Word.Application m_oApp;
        private Hashtable m_oPendingControlActions = new Hashtable();
        private WebBrowser wbDescription;
        private bool m_bProgrammaticallySelectingWordTag;
        private bool m_bSelectingNode;
        private ControlHost m_oControlHost = ControlHost.EditorTree;
        private string m_xEmptyDisplayValue;
        private Hashtable m_oHotkeys = new Hashtable();
        private bool m_bInHotkeyMode;
        private bool m_bExitingEditMode = false;
        private bool m_bHotKeyPressed;
        private string m_xTransparentNodes = "|";
        private bool m_bReplaceSegmentPrimaryItem = false;
        private bool m_bReplacingDocumentSegment = false;
        private bool m_bDeletingDocumentSegment = false;
        private ContextMenuStrip m_oMenuSegment;
        private ContextMenuStrip m_oMenuVariable;
        private ContextMenuStrip m_oMenuBlock;
        private bool m_bInsertingCollectionTableStructure;
        private bool m_bAddingCollectionTableItem;
        private bool m_bEditingMasterData;

        // GLOG : 3135 : JAB
        // Introduce menu for authors.
        private TimedContextMenuStrip m_oMenuAuthors;

        // GLOG : 5398 : CEH
        // Introduce menu for authors.
        private TimedContextMenuStrip m_oMenuJurisdictions;

        private ToolStripMenuItem m_oMenuSegment_SaveData;
        private ToolStripMenuItem m_oMenuSegment_SaveContent; 
        private ToolStripMenuItem m_oMenuSegment_Refresh;
        private ToolStripMenuItem m_oMenuSegment_Delete;
        private ToolStripMenuItem m_oMenuSegment_RecreateSegment;
        private ToolStripMenuItem m_oMenuSegment_CopySegment;
        private ToolStripMenuItem m_oMenuSegment_Activate;
        private ToolStripMenuItem m_oMenuSegment_Update;
        private ToolStripMenuItem m_oMenuSegment_UpdateWithSelection;
        private ToolStripMenuItem m_oMenuSegment_RefreshStyles;
        private ToolStripMenuItem m_oMenuSegment_Help;

        // GLOG : 3135 : JAB
        // Provide a menu item for updating the authors and letterhead.
        private ToolStripMenuItem m_oMenuSegment_Letterhead_Refresh;
        private ToolStripMenuItem m_oMenuSegment_CreateEnvelopes;
        private ToolStripMenuItem m_oMenuSegment_CreateLabels;
        private ToolStripMenuItem m_oMenuSegment_TOA_Update;

        private ToolStripMenuItem m_oMenuSegment_Pleading_InsertTOA;
        private ToolStripMenuItem m_oMenuSegment_Pleading_UpdateTOA; //GLOG 7243 (dm)
        private ToolStripMenuItem m_oMenuSegment_Pleading_AlignPleadingPaper;
        private ToolStripMenuItem m_oMenuSegment_PleadingCaptions_AddItem;
        private ToolStripMenuItem m_oMenuSegment_PleadingCounsels_AddItem;
        private ToolStripMenuItem m_oMenuSegment_PleadingSignatures_AddItem;
        private ToolStripMenuItem m_oMenuSegment_PleadingSignatures_ModifyLayout;
        private ToolStripMenuItem m_oMenuSegment_LetterSignatures_AddItem;
        private ToolStripMenuItem m_oMenuSegment_AgreementSignatures_AddItem;
        private ToolStripMenuItem m_oMenuSegment_CollectionTable_AddItem;
        private ToolStripMenuItem m_oMenuSegment_Pleading_AddNonTableSignature;
        private ToolStripMenuItem m_oMenuSegment_RemoveVarAndBlockTags;
        private ToolStripMenuItem m_oMenuSegment_Pleading_InsertCaption;
        private ToolStripMenuItem m_oMenuSegment_Pleading_InsertCounsel;
        private ToolStripMenuItem m_oMenuSegment_Pleading_InsertExhibits;
        private ToolStripMenuItem m_oMenuSegment_Pleading_InsertSignature;
        private ToolStripMenuItem m_oMenuSegment_Letter_InsertSignature;
        private ToolStripMenuItem m_oMenuSegment_Agreement_InsertSignature;
        private ToolStripMenuItem m_oMenuSegment_Agreement_InsertTitlePage;
        private ToolStripMenuItem m_oMenuSegment_Agreement_InsertExhibits;
        private ToolStripMenuItem m_oMenuSegment_Pleading_InsertNonTableSignature;
        private ToolStripMenuItem m_oMenuSegment_Pleading_InsertInterrogatories;
        private ToolStripMenuItem m_oMenuSegment_Pleading_MarkCitation;

        //GLOG : 6115 : CEH
        private ToolStripMenuItem m_oMenuSegment_PleadingCounsels_MenuItem;
        private TimedContextMenuStrip mnuPleadingCounselsOptions;
        private ToolStripMenuItem m_oMenuSegment_PleadingCounsel_Add;
        private ToolStripMenuItem m_oMenuSegment_PleadingCounsel_ChangeType;
        private ToolStripMenuItem m_oMenuSegment_PleadingCounsel_Edit;
        private ToolStripMenuItem m_oMenuSegment_PleadingCounsel_Layout;

        private ToolStripMenuItem m_oMenuSegment_PleadingCaptions_MenuItem;
        private ContextMenuStrip mnuPleadingCaptionsOptions;
        private ToolStripMenuItem m_oMenuSegment_PleadingCaption_Add;
        private ToolStripMenuItem m_oMenuSegment_PleadingCaption_ChangeType;
        private ToolStripMenuItem m_oMenuSegment_PleadingCaption_Edit;
        private ToolStripMenuItem m_oMenuSegment_PleadingCaption_Layout;

        private ToolStripMenuItem m_oMenuSegment_PleadingSignatures_MenuItem;
        private ContextMenuStrip mnuPleadingSignaturesOptions;
        private ToolStripMenuItem m_oMenuSegment_PleadingSignature_Add;
        private ToolStripMenuItem m_oMenuSegment_PleadingSignature_ChangeType;
        private ToolStripMenuItem m_oMenuSegment_PleadingSignature_Edit;
        private ToolStripMenuItem m_oMenuSegment_PleadingSignature_Layout;

        //GLOG : 6160 : CEH
        private ToolStripMenuItem m_oMenuSegment_Pleading_EditNonTableSignature;

        private ToolStripMenuItem m_oMenuSegment_PleadingPaper_MenuItem;
        private ContextMenuStrip mnuPleadingPaperOptions;
        private ToolStripMenuItem m_oMenuSegment_PleadingPaper_ChangeType;
        private ToolStripMenuItem m_oMenuSegment_PleadingPaper_Edit;

        //GLOG : 6167 : CEH
        private ToolStripMenuItem m_oMenuSegment_LetterSignatures_MenuItem;
        private ContextMenuStrip mnuLetterSignaturesOptions;
        private ToolStripMenuItem m_oMenuSegment_LetterSignature_Add;
        private ToolStripMenuItem m_oMenuSegment_LetterSignature_ChangeType;
        private ToolStripMenuItem m_oMenuSegment_LetterSignature_Edit;
        private ToolStripMenuItem m_oMenuSegment_LetterSignature_Layout;

        private ToolStripMenuItem m_oMenuSegment_Letterhead_MenuItem;
        private ContextMenuStrip mnuLetterheadOptions;
        private ToolStripMenuItem m_oMenuSegment_Letterhead_ChangeType;
        private ToolStripMenuItem m_oMenuSegment_Letterhead_Edit;
        
        //GLOG : 6170 : CEH
        private ToolStripMenuItem m_oMenuSegment_AgreementSignatures_MenuItem;
        private ContextMenuStrip mnuAgreementSignaturesOptions;
        private ToolStripMenuItem m_oMenuSegment_AgreementSignature_Add;
        private ToolStripMenuItem m_oMenuSegment_AgreementSignature_ChangeType;
        private ToolStripMenuItem m_oMenuSegment_AgreementSignature_Edit;
        private ToolStripMenuItem m_oMenuSegment_AgreementSignature_Layout;

        // GLOG : 2340 : JAB
        private ToolStripMenuItem m_oMenuSegment_Pleading_QuoteStyle;

        private ToolStripMenuItem m_oMenuVariable_Help;
        private ToolStripMenuItem m_oMenuVariable_RemoveTag;
        private ToolStripMenuItem m_oMenuBlock_Help;
        private ToolStripMenuItem m_oMenuBlock_RemoveTag;

        private bool m_bHandlingTabPress = false;
        private bool m_bDisableHotKeyMode = false;
        private bool m_bIsInControlShiftEditMode = false;
        private CalloutForm m_oCallout = null;
        // Set to false to use Word Callout shape instead of Windows form
        private const bool m_bUseWindowsCallout = true;
        private System.Windows.Forms.Timer m_oTabNavigationTimer = null;
        private UltraTreeNode m_oCurTabNode = null;
        private Hashtable m_oControlsHash = null;
        private bool m_bTabTimerIsRunning = false;
        private bool m_bCIIsActive = false;
        private ValueAssignedEventHandler m_oValueAssignedHandler;
        private VariableVisitedHandler m_oVarVisitedHandler;
        private Boolean m_bRepositioningControl = false;
        private bool m_bIsResizing = false;
        private string m_xHelpText = "";
        private FocusForm oFocusForm = null;
        private const string mpTrailersNodeKey = "MacPacTrailersNode";
        private Hashtable m_oUnresolvedEmptyTags = new Hashtable();
        private bool m_bIsRefreshingSegmentNode = false;
        private string m_xSkipVarUpdateFromControl = "";
        private bool m_bSuppressTreeActivationHandlers = false;
        private bool m_bShowValueNodes = true;
        private string m_xDelayedCCEnterEvent = "";
        private string m_xDelayedCCExitEvent = "";
        private bool m_bSuspendSegmentNodeInsertion = false;
        private bool m_bExpandingNode = false; //GLOG 15860 (dm)
        private Segment m_oCurrentSegment; //GLOG 15860 (dm)

        public event LMP.Architect.Api.CurrentSegmentChangedEventHandler CurrentTopLevelSegmentSwitched;
        #endregion
        #region  *********************constants*********************
        private const int KEY_SHIFT = 16;
        private const int KEY_CONTROL = 17;
        private const int KEY_HOME = 36;
        private const int KEY_END = 35;
        private const int KEY_TAB = 9;
        private const int KEY_LEFT = 37;
        private const int KEY_RIGHT = 39;
        private const int KEY_UP = 38;
        private const int KEY_DOWN = 40;
        #endregion
        #region *********************enumerations*********************
        private enum Images
        {
            NoImage = -1,
            ValidDocumentSegment = 0,
            InvalidDocumentSegment = 1,
            ValidComponentSegment = 2,
            InvalidComponentSegment = 3,
            ValidTextBlockSegment = 4,
            InvalidTextBlockSegment =5,
            ValidVariableValue = 6,
            InvalidVariableValue = 7,
            IncompleteVariable = 41,
            IncompleteDocumentSegment = 42,
            IncompleteComponentSegment = 43,
            IncompleteTextBlockSegment = 44,
            HotkeyA = 8,
            HotkeyB = 9,
            HotkeyC = 10,
            HotkeyD = 11,
            HotkeyE = 12,
            HotkeyF = 13,
            HotkeyG = 14,
            HotkeyH = 15,
            HotkeyI = 16,
            HotkeyJ = 17,
            HotkeyK = 18,
            HotkeyL = 19,
            HotkeyM = 20,
            HotkeyN = 21,
            HotkeyO = 22,
            HotkeyP = 23,
            HotkeyQ = 24,
            HotkeyR = 25,
            HotkeyS = 26,
            HotkeyT = 27,
            HotkeyU = 28,
            HotkeyV = 29,
            HotkeyW = 30,
            HotkeyX = 31,
            Hotkey1 = 32,
            Hotkey2 = 33,
            Hotkey3 = 34,
            Hotkey4 = 35,
            Hotkey5 = 36,
            Hotkey6 = 37,
            Hotkey7 = 38,
            Hotkey8 = 39,
            Hotkey9 = 40
        }
        private enum ControlHost
        {
            EditorTree = 1,
            FloatingForm = 2
        }
        #endregion
        #region *********************constructors*********************
        public DocumentEditor()
        {
            Trace.WriteInfo("In LMP.MacPac.DocumentEditor.Ctor");

            InitializeComponent();

            //hook up to tree's TabPressed event
            this.treeDocContents.TabPressed += 
                new TabPressedHandler(oIControl_TabPressed);
            this.treeDocContents.CIRequested += new CIRequestedHandler(GetContacts);

            m_oTabNavigationTimer = new System.Windows.Forms.Timer();
            m_oTabNavigationTimer.Stop();
            m_oTabNavigationTimer.Tick += new EventHandler(m_oTabNavigationTimer_Tick);

            //create new event handlers for later subscription
            m_oValueAssignedHandler = new ValueAssignedEventHandler(OnVariableValueAssigned);
            m_oVarVisitedHandler = new VariableVisitedHandler(OnVariableVisited);

            m_oControlsHash = new Hashtable();
            //GLOG 7214
            TaskPane.SetHelpText(wbDescription, "");
            this.UpdateHelpWindowView();
        }
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// gets/sets the associated task pane
        /// </summary>
        internal TaskPane TaskPane
        {
            get { return m_oTaskPane; }
            set
            {
                m_oTaskPane = value;
                m_oMPDocument = m_oTaskPane.ForteDocument;

                //subscribe to events of the ForteDocument
                m_oMPDocument.VariableAdded += new VariableAddedHandler(
                    m_oMPDocument_VariableAdded);
                m_oMPDocument.VariableDeleted += new VariableDeletedHandler(
                    m_oMPDocument_VariableDeleted);
                m_oMPDocument.VariableDefinitionChanged += new VariableDefinitionChangedHandler(
                    m_oMPDocument_VariableDefinitionChanged);
                m_oMPDocument.BlockAdded += new BlockAddedHandler(
                    m_oMPDocument_BlockAdded);
                m_oMPDocument.BlockDeleted += new BlockDeletedHandler(
                    m_oMPDocument_BlockDeleted);
                m_oMPDocument.BlockDefinitionChanged += new BlockDefinitionChangedHandler(
                    m_oMPDocument_BlockDefinitionChanged);
                m_oMPDocument.SegmentRefreshedByAction += new SegmentRefreshedByActionHandler(
                    m_oMPDocument_SegmentRefreshedByAction);
                m_oMPDocument.SegmentAdded += new SegmentAddedHandler(
                    m_oMPDocument_SegmentAdded);
                m_oMPDocument.SegmentDeleted += new SegmentDeletedHandler(
                    m_oMPDocument_SegmentDeleted);
                m_oMPDocument.SegmentDistributedSectionsChanged +=new SegmentDistributedSectionsHandler(
                    m_oMPDocument_SegmentDistributedSectionsChanged);
                m_oMPDocument.BeforeSegmentReplacedByAction +=new BeforeSegmentReplacedByActionHandler(
                    m_oMPDocument_BeforeSegmentReplacedByAction);
                m_oMPDocument.BeforeCollectionTableStructureInserted +=
                    new BeforeCollectionTableStructureInsertedHandler(
                    m_oMPDocument_BeforeCollectionTableStructureInserted);
                m_oMPDocument.AfterCollectionTableStructureInserted +=
                    new AfterCollectionTableStructureInsertedHandler(
                    m_oMPDocument_AfterCollectionTableStructureInserted);

                //subscribe to Word App DocumentBeforeClose Event
                m_oApp = (Word.ApplicationClass)m_oMPDocument.WordDocument.Application;
                m_oApp.DocumentBeforeClose += new Word.
                    ApplicationEvents4_DocumentBeforeCloseEventHandler(m_oApp_DocumentBeforeClose);

                //GLOG 15860 (dm) - track movement in document
                m_oTaskPane.CurrentTopLevelSegmentSwitched +=
                    new CurrentSegmentChangedEventHandler(TaskPane_CurrentTopLevelSegmentSwitched);
            }
        }
        /// <summary>
        /// returns true if the scroll bars are visible, else false
        /// </summary>
        public bool ScrollBarsVisible
        {
            get
            {
                //get the top level nodes of the tree
                TreeNodesCollection oNodes = this.treeDocContents.Nodes;

                if (oNodes.Count == 0 || !oNodes[0].IsInView)
                    //first node is not in view -
                    //scroll bars must be visible
                    return true;

                //cycle through nodes, checking if last node is visible -
                //if not, scroll bars must be visible
                while (oNodes != null && oNodes.Count > 0)
                {
                    //get last node in collection
                    UltraTreeNode oLastNode = oNodes[oNodes.Count - 1];

                    if (oLastNode.Bounds.Top + oLastNode.Bounds.Height + 10 
                        >= this.treeDocContents.Height)
                        //last node is not visible -
                        //scroll bars are visible
                        return true;
                    else if (oLastNode.Expanded)
                        //get child nodes of last node
                        oNodes = oLastNode.Nodes;
                    else
                        return false;
                }

                //if we got here, all nodes are visible
                return false;
            }
        }
        /// <summary>
        /// get/sets whether we're in the process
        /// of programmatically selecting a Word tag
        /// </summary>
        internal bool ProgrammaticallySelectingWordTag
        {
            get { return this.m_bProgrammaticallySelectingWordTag; }
        }
        /// <summary>
        /// Turn off the Ctrl-key hotkey toggle to allow Ctrl in combination with other keys
        /// </summary>
        internal bool DisableHotKeyMode
        {
            get { return this.m_bDisableHotKeyMode; }
            set { this.m_bDisableHotKeyMode = value; }
        }
        /// <summary>
        /// Gets/sets mode indicating that we are using 
        /// control (shift) right/left arrow to highlight
        /// text and distinguish this from using control
        /// to enter hotkey mode.
        /// </summary>
        private bool IsInControlShiftEditMode
        {
            get
            {
                return m_bIsInControlShiftEditMode;
            }

            set
            {
                m_bIsInControlShiftEditMode = value;
            }
        }
        /// <summary>
        /// returns true if the document editor tree is refreshing
        /// </summary>
        private bool IsRefreshingSegmentNode
        {
            get { return m_bIsRefreshingSegmentNode; }
        }
        /// <summary>
        /// an mVar is added to this collection when manually cleared and acted
        /// upon when we're satisfied that the user is done editing it
        /// </summary>
        internal Hashtable UnresolvedEmptyTags
        {
            get { return m_oUnresolvedEmptyTags; }
            set { m_oUnresolvedEmptyTags = value; }
        }
        /// <summary>
        /// returns the macpac document associated with this editor
        /// </summary>
        internal ForteDocument ForteDocument
        {
            get { return m_oMPDocument; }
        }
        /// <summary>
        /// while true, new segments are not added to the tree -
        /// allows for consolidated ui refresh when inserting multiple segments
        /// </summary>
        public bool SuspendSegmentNodeInsertion
        {
            get { return m_bSuspendSegmentNodeInsertion; }
            set { m_bSuspendSegmentNodeInsertion = value; }
        }
        #endregion
        #region *********************event handlers*********************
        private void treeDocContents_Scroll(object sender, Infragistics.Win.UltraWinTree.TreeScrollEventArgs e)
        {

            try
            {
                if (this.m_oCurNode != null && !m_bRepositioningControl)
                {
                    if (this.picMenu.Visible)
                    {
                        ShowContextMenuButton(this.m_oCurNode);
                    }

                    //move control with associated node
                    this.SizeAndPositionControl();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_AfterExpand(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
        {
            try
            {
                if (!this.TaskPane.IsSetUp || this.IsRefreshingSegmentNode)
                    return;

                m_bExpandingNode = true; //GLOG 15860 (dm)
                this.SuspendLayout();

                if (e.TreeNode.Tag is Segment)
                {
                    SelectNode(e.TreeNode);

                    if (e.TreeNode.Nodes.Count > 0)
                    {
                        //scroll tree to attempt to show all child nodes
                        //if the last child node can't be seen
                        if (!e.TreeNode.Nodes[e.TreeNode.Nodes.Count - 1].IsInView)
                            e.TreeNode.BringIntoView(true);
                    }
                }
                //GLOG 5955: Select More node if expanded
                else if (e.TreeNode.Key.EndsWith(".Advanced"))
                {
                    SelectNode(e.TreeNode);
                }
                if (this.m_oCurNode != null)
                {
                    if (this.picMenu.Visible)
                    {
                        ShowContextMenuButton(this.m_oCurNode);
                    }

                    //move control with associated node
                    this.SizeAndPositionControl();
                }
            }
            catch (System.InvalidOperationException)
            {
                //ignore potential "cross-thread" exception
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
                m_bExpandingNode = false; //GLOG 15860 (dm)
            }
        }
        private void treeDocContents_BeforeCollapse(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
        {
            try
            {
                if (e.TreeNode == this.m_oCurNode)
                    return;

                if (e.TreeNode.IsAncestorOf(this.m_oCurNode))
                {
                    //exit edit mode
                    this.ExitEditMode();

                    //remove the node selection
                    this.m_oCurNode.Selected = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_BeforeExpand(object sender, Infragistics.Win.UltraWinTree.CancelableNodeEventArgs e)
        {
            try
            {
                Segment oSeg = e.TreeNode.Tag as Segment;

                if (oSeg != null && !e.TreeNode.HasNodes)
                {
                    this.RefreshSegmentNode(e.TreeNode);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_AfterCollapse(object sender, Infragistics.Win.UltraWinTree.NodeEventArgs e)
        {
            try
            {
                if (this.m_oCurNode != null)
                {                    
                    //move control with associated node
                    if (this.picMenu.Visible)
                    {
                        ShowContextMenuButton(this.m_oCurNode);
                    }

                    this.SizeAndPositionControl();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeDocContents_BeforeActivate(object sender, CancelableNodeEventArgs e)
        {
            if (m_bSuppressTreeActivationHandlers)
                return;

            try
            {
                if (m_bHandlingTabPress)
                {
                    //If Tabbing through tree, don't select node until
                    //tabbing stops - each Tab press will restart timer
                    m_oCurTabNode = e.TreeNode;
                    m_oTabNavigationTimer.Start();
                    m_bTabTimerIsRunning = true;
                }
                else
                {
                    m_oTabNavigationTimer.Stop();
                    m_bTabTimerIsRunning = false;
                    m_oCurTabNode = null;

                    if (m_oCurNode != null && m_oCurNode == e.TreeNode)
                        return;

                    this.SelectNode(e.TreeNode);
                    
                    //ensure that screen refreshes
                    this.TaskPane.ScreenUpdating = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // Update the any view related to the node that may have been modified when navigating
        // away from the document editor.
        public void UpdateNodeView()
        {
            // The help window's text is cleared when navigating away from the document editor.
            // Restore the help text.
            
            TaskPane.SetHelpText(this.wbDescription, m_xHelpText);
        }


        private void treeDocContents_Leave(object sender, EventArgs e)
        {
            try
            {
                //update value of current variable
                if ((m_oCurCtl != null) && (m_oCurNode != null) && (m_oCurNode.Tag is Variable))
                {
                    //Display of listbox for Multiline combo will trigger this event
                    if (m_oCurCtl is MultilineCombo && ((MultilineCombo)m_oCurCtl).Focused)
                        return;
                    else
                    {
                        //GLOG 3792 and 3816 (dm) - make sure Word doc still exists
                        //before proceeding - in Word 2003, this event gets called when
                        //obsolete task panes are disposed in TaskPanes.RemoveUnused()
                        string xName = null;
                        try
                        {
                            xName = m_oMPDocument.WordDocument.Name;
                        }
                        catch { }
                        if (xName != null)
                            UpdateVariableValueFromCurrentCtl();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void OnVariableValueAssigned(object sender, EventArgs e)
        {
            try
            {
                Variable oVar = sender as Variable;
                //GLOG 3627: If Variable currently has temp value assigned, don't do anything
                if (oVar.Value == "zzmpTempValue")
                    return;

                //execute ValueChanged control actions
                try
                {
                    ExecuteControlActions((Variable)sender, ControlActions.Events.ValueChanged);
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.ControlActionException(
                        LMP.Resources.GetLangString("Error_CouldNotExecuteControlAction"), oE);
                }


                //GLOG 3144: Adjust special caption border length if necessary
                //GLOG 4282: Run for any Collection Table item
                if (oVar.Segment is CollectionTableItem && !oVar.IsTagless)
                    oVar.Segment.UpdateTableBorders();

                //get associated tree node
                string xKey = "";
                if (oVar.Segment != null)
                    xKey = oVar.Segment.FullTagID + "." + oVar.ID;
                else
                    xKey = oVar.ID;

                //GLOG 2610 - added error trap
                UltraTreeNode oNode = null;
                try
                {
                    oNode = this.GetNodeFromTagID(xKey);
                }
                catch { }

                if (oNode != null)
                {

                    //set value of associated value node
                    oNode.Nodes[0].Text = this.GetVariableUIValue_API(oVar);

                    //clear previous validation image
                    oNode.LeftImages.Clear();

                    //add variable validation image
                    if (!oVar.HasValidValue)
                        oNode.LeftImages.Add(Images.InvalidVariableValue);
                    else if (oVar.MustVisit)
                        oNode.LeftImages.Add(Images.IncompleteVariable);
                    else
                        oNode.LeftImages.Add(Images.ValidVariableValue);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Raised after the MustVisit property of Variable has changed to false
        /// Update image displayed in tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnVariableVisited(object sender, VariableEventArgs e)
        {
            try
            {
                Variable oVar = sender as Variable;

                //get associated tree node
                string xKey = "";
                if (oVar.Segment != null)
                    xKey = oVar.Segment.FullTagID + "." + oVar.ID;
                else
                    xKey = oVar.ID;
                UltraTreeNode oNode = this.GetNodeFromTagID(xKey);

                //clear previous validation image
                oNode.LeftImages.Clear();

                //add variable validation image
                if (!oVar.HasValidValue)
                    oNode.LeftImages.Add(Images.InvalidVariableValue);
                else if (oVar.MustVisit)
                    oNode.LeftImages.Add(Images.IncompleteVariable);
                else
                    oNode.LeftImages.Add(Images.ValidVariableValue);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void oSeg_ValidationChange(object sender, LMP.Architect.Base.ValidationChangedEventArgs e)
        {
            try
            {
                Segment oSeg = sender as Segment;
                //GLOG 5888 (dm) - update parents' images - child's validation change
                //may not register at lower level because parent may not yet be
                //subscribed to the ValidationChanged event when it occurs.
                //GLOG 6945: Update Parent images built into function
                UpdateSegmentImage(oSeg, false, true);

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void UpdateSegmentImage(Segment oSegment)
        {
            UpdateSegmentImage(oSegment, false, false);
        }

        public void UpdateSegmentImage(Segment oSegment, bool bUpdateChildSegmentImages)
        {
            UpdateSegmentImage(oSegment, bUpdateChildSegmentImages, false);
        }
        public void UpdateSegmentImage(Segment oSegment, bool bUpdateChildSegmentImages, bool bUpdateParentSegmentImages)
        {
            DateTime t0 = DateTime.Now;

            if (bUpdateChildSegmentImages)
            {
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    UpdateSegmentImage(oSegment.Segments[i], true);
                }
            }
            //GLOG 6945
            if (bUpdateParentSegmentImages)
            {
                Segment oParent = oSegment.Parent;
                while (oParent != null)
                {
                    UpdateSegmentImage(oParent, false, false);
                    oParent = oParent.Parent;
                }
            }
            //exit if transparent
            if (this.NodeIsTransparent(oSegment.FullTagID))
                return;

            //get associated tree node
            UltraTreeNode oNode = null;
            try
            {
                oNode = this.GetNodeFromTagID(oSegment.FullTagID, false);
            }
            catch { }

            if (oNode != null)
            {
                //clear previous validation image
                oNode.LeftImages.Clear();

                bool bIsChildSegment = (oSegment.Parent != null);

                //add appropriate validation image
                oNode.LeftImages.Add(GetSegmentImage(oSegment, oSegment.IsValid, oSegment.AllRequiredValuesVisited));
            }

            LMP.Benchmarks.Print(t0, oSegment.Name);
        }
        public void UpdateSegmentImage(LMP.Architect.Oxml.XmlSegment oSegment, bool bUpdateChildSegmentImages, bool bUpdateParentSegmentImages)
        {
            DateTime t0 = DateTime.Now;

            if (bUpdateChildSegmentImages)
            {
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    UpdateSegmentImage(oSegment.Segments[i], true, false);
                }
            }
            //GLOG 6945
            if (bUpdateParentSegmentImages)
            {
                LMP.Architect.Oxml.XmlSegment oParent = oSegment.Parent;
                while (oParent != null)
                {
                    UpdateSegmentImage(oParent, false, false);
                    oParent = oParent.Parent;
                }
            }
            //exit if transparent
            if (this.NodeIsTransparent(oSegment.FullTagID))
                return;

            //get associated tree node
            UltraTreeNode oNode = null;
            try
            {
                oNode = this.GetNodeFromTagID(oSegment.FullTagID, false);
            }
            catch { }

            if (oNode != null)
            {
                //clear previous validation image
                oNode.LeftImages.Clear();

                bool bIsChildSegment = (oSegment.Parent != null);

                //add appropriate validation image
                oNode.LeftImages.Add(GetSegmentImage(oSegment, oSegment.IsValid, oSegment.AllRequiredValuesVisited));
            }

            LMP.Benchmarks.Print(t0, oSegment.Name);
        }
        public void UpdateSegmentImages()
        {
            for (int i = 0; i < this.ForteDocument.Segments.Count; i++)
            {
                Segment oSegment = this.ForteDocument.Segments[i];
                UpdateSegmentImage(oSegment, true, false);
            }
        }
        private void oSeg_UpdatedForAuthor(object sender, EventArgs e)
        {
            //TODO: this class is subscribed to the AfterAuthorUpdated event twice with
            //two different handlers - this needs to be consolidated into a single
            //subscription and handler
            try
            {
                Segment oSeg = sender as Segment;

                //exit if transparent
                if (this.NodeIsTransparent(oSeg.FullTagID))
                    return;

                //get segment node
                UltraTreeNode oSegmentNode = null;
                try
                {
                    oSegmentNode = GetNodeFromTagID(oSeg.FullTagID, false);
                }
                catch { }

                //exit if segment node does not yet exist or has not yet been populated
                if (oSegmentNode == null)
                    return;
                else if (oSegmentNode.Nodes.Count == 0)
                    return;

                //update variable value nodes
                for (int i = oSegmentNode.Nodes.Count - 1; i >= 0; i--)
                {
                    UltraTreeNode oNode = oSegmentNode.Nodes[i];
                    if (oNode.Tag is Variable)
                    {
                        //ensure that the node is visible
                        this.EnsureVisibleNode(oNode);

                        //GLOG 3416 - added oNode.Visible conditional
                        if (oNode.Visible)
                        {
                            //get variable
                            Variable oVar = (Variable)oNode.Tag;

                            //set value of associated value node
                            oNode.NextVisibleNode.Text = this.GetVariableUIValue_API(oVar);
                        }
                    }
                }

                //more node
                UltraTreeNode oMoreNode = null;
                try
                {
                    oMoreNode = GetNodeFromTagID(oSeg.FullTagID + ".Advanced");
                }
                catch { }
                if (oMoreNode != null)
                {
                    // GLOG : 3518 : JAB
                    // The more node expands within the following for loop
                    // when the EnsureVisibleNode(oNode) method is called.
                    // Retain and restore the Expanded state of the More node.
                    bool bMoreNodeExpanded = oMoreNode.Expanded;

                    for (int i = oMoreNode.Nodes.Count - 1; i >= 0; i--)
                    {
                        UltraTreeNode oNode = oMoreNode.Nodes[i];
                        if (oNode.Tag is Variable)
                        {
                            //ensure that the node is visible
                            this.EnsureVisibleNode(oNode);

                            //get variable
                            Variable oVar = (Variable)oNode.Tag;

                            //set value of associated value node
                            oNode.NextVisibleNode.Text = this.GetVariableUIValue_API(oVar);
                        }
                    }

                    // GLOG : 3518 : JAB
                    // Restore the Expanded state of the More node.
                    oMoreNode.Expanded= bMoreNodeExpanded;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Raised after all required variables have been visited
        /// Update segment image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oSeg_AfterAllVariablesVisited(object sender, EventArgs e)
        {
            try
            {
                Segment oSeg = sender as Segment;

                //exit if transparent
                if (this.NodeIsTransparent(oSeg.FullTagID))
                    return;

                //get associated tree node
                UltraTreeNode oNode = this.GetNodeFromTagID(oSeg.FullTagID);

                //clear previous validation image
                oNode.LeftImages.Clear();

                bool bIsChildSegment = (oSeg.Parent != null);

                //add appropriate validation image
                oNode.LeftImages.Add(GetSegmentImage(oSeg, oSeg.IsValid, oSeg.AllRequiredValuesVisited));
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            
        }
        private void DocumentEditor_Load(object sender, EventArgs e)
        {
            try
            {
                //GLOG #2897 - dcf
                ///focus form is displayed to prevent user
                //input from reaching the document while
                //variable actions execute
                oFocusForm = new FocusForm();

                if (!this.DesignMode)
                {
                    //Set Tooltip for floatint CI button
                    System.Windows.Forms.ToolTip oTip = new System.Windows.Forms.ToolTip();
                    oTip.SetToolTip(this.btnContacts, LMP.Resources.GetLangString("Prompt_RetrieveContacts") + " [F2]");

                    //GLOG item #5948 - dcf
                    if (this.ForteDocument.Segments.Count > 0 &&
                        this.ForteDocument.Segments[0].IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                    {
                        m_bEditingMasterData = true;
                        this.tsEditor.Visible = true;
                        UpdateButtons(this.ForteDocument);
                        this.TaskPane.ContentManagerVisible = false;

                        Segment oSegment = this.ForteDocument.Segments[0];

                        //object oOne = 1;
                        //Word.ContentControl oCC = this.ForteDocument.WordDocument.ContentControls.get_Item(ref oOne);

                        if (oSegment.Prefill != null)
                        {
                            oSegment.PrimaryRange.Text = string.Format(LMP.Resources.GetLangString("Msg_EditingMasterData"),
                                oSegment.Prefill.Name);
                        }

						//GLOG 6780: No longer Content Control bounding object for Master Data
                        //if (this.ForteDocument.FileFormat == mpFileFormats.OpenXML)
                        //{
                        //    oSegment.PrimaryContentControl.LockContentControl = true;
                        //    oSegment.PrimaryContentControl.LockContents = true;
                        //}

                        if (oSegment.Prefill != null)
                        {
                            this.ForteDocument.WordDocument.ActiveWindow.Caption = "Editing Master Data '" +
                                oSegment.Prefill.Name + "'.";
                        }
                        else
                        {
                            this.ForteDocument.WordDocument.ActiveWindow.Caption = "Master Data Form";
                        }

                        this.ForteDocument.WordDocument.Saved = true;
                    }
                    else if (LMP.MacPac.Session.DisplayFinishButton &&
                        this.TaskPane.Mode == ForteDocument.Modes.Edit)
                    {
                        this.tsEditor.Visible = true;
                        this.stsStatus.Visible = true;

                        UpdateButtons(this.ForteDocument);
                    }
                    else
                    {
                        this.tsEditor.Visible = false;
                        this.stsStatus.Visible = true;
                    }

                    this.SetTreeAppearance();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public void ResizeTreeControl()
        {
            if (LMP.MacPac.Session.DisplayFinishButton)
            {
                this.treeDocContents.Height = this.tsEditor.Top - 6;
            }
        }

        /// <summary>
        /// handles case when tab or shift-tab have been pressed-
        /// executes only when the ProcessDialogKey doesn't execute,
        /// as the key will get processed by the control, which raises
        /// the event that causes this handler to execute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void oIControl_TabPressed(object sender, TabPressedEventArgs e)
        {
            HandleTabPressed(e.ShiftPressed);
        }
        private void HandleTabPressed(bool bShift)
        {
            //Stop timer
            m_oTabNavigationTimer.Stop();

            //If tab is pressed multiple times, gradually increate timer interval to avoid
            //unnecessary creation of controls for each visited node
            if (m_bTabTimerIsRunning)
                m_oTabNavigationTimer.Interval = Math.Min(m_oTabNavigationTimer.Interval + 50, 200);
            else
                m_oTabNavigationTimer.Interval = 25;
            LMP.Trace.WriteNameValuePairs("Interval", m_oTabNavigationTimer.Interval);
            ExitHotkeyMode();

            m_bHandlingTabPress = true;
            try
            {
                if (bShift)
                    //shift-tab was pressed - select previous node
                    this.SelectPreviousNode();
                else
                    //tab was pressed - select next node
                    this.SelectNextNode();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_bHandlingTabPress = false;
            }
        }
        /// <summary>
        /// Timer is started upon tabbing into node
        /// When tabbing through tree, new node will not be selected until
        /// Node has remained active for 1 tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oTabNavigationTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                m_oTabNavigationTimer.Stop();
                m_bTabTimerIsRunning = false;
                //exit if the new selection is the current selection
                if (m_oCurTabNode == null)
                    return;

                this.SelectNode(m_oCurTabNode);

                //ensure that screen refreshes
                this.TaskPane.ScreenUpdating = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a variable is deleted by a
        /// variable action with an expanded delete scope - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_VariableDeleted(object sender, VariableEventArgs oArgs)
        {
            try
            {
                string xKey = null;
                Variable oVar = oArgs.Variable;
                if (!oVar.IsStandalone)
                    xKey = oVar.Segment.FullTagID + "." + oVar.ID;
                else
                    xKey = oVar.ID;

                this.DeleteNode(xKey);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a variable is added - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_VariableAdded(object sender, VariableEventArgs oArgs)
        {
            try
            {
                Variable oVar = oArgs.Variable;
                if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                    Variable.ControlHosts.DocumentEditor)
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        // Check for node matching Segment and Variable
                        oNode = this.GetNodeFromTagID(oVar.Segment.FullTagID + "." + oVar.ID);
                    }
                    catch { }
                    
                    if (oNode == null)
                        this.InsertNode(oVar);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when the definition of a variable
        /// is modified - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_VariableDefinitionChanged(object sender, VariableEventArgs oArgs)
        {
            try
            {
                Variable oVar = oArgs.Variable;

                //don't do if variables collection is obsolete, i.e. if the event is
                //occurring during initialization of a new variables collection
                if (((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                    Variable.ControlHosts.DocumentEditor) &&
                    (!oVar.Segment.Variables.CollectionIsObsolete))
                {
                    //delete existing node
                    string xKey = System.String.Concat(oVar.Segment.FullTagID, ".", oVar.ID);
                    this.DeleteNode(xKey);
                    //insert new node
                    UltraTreeNode oNewNode = this.InsertNode(oVar);
                    //Reset m_oCurNode if it's been replaced
                    if (m_oCurNode != null && m_oCurNode.Key == xKey)
                        m_oCurNode = oNewNode;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this event is raised by ForteDocument when a Block is deleted - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_BlockDeleted(object sender, BlockEventArgs oArgs)
        {
            try
            {
                Block oBlock = oArgs.Block;
                this.DeleteNode(oBlock.TagID);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// this event is raised by ForteDocument when a Block is added - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_BlockAdded(object sender, BlockEventArgs oArgs)
        {
            try
            {
                Block oBlock = oArgs.Block;
                if (oBlock.ShowInTree == true)
                    this.InsertNode(oBlock);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// this event is raised by ForteDocument when the definition of a Block
        /// is modified - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_BlockDefinitionChanged(object sender, BlockEventArgs oArgs)
        {
            try
            {
                Block oBlock = oArgs.Block;

                //delete existing node
                this.DeleteNode(oBlock.TagID);

                //insert new node
                if (oBlock.ShowInTree == true)
                    this.InsertNode(oBlock);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// this event is raised by ForteDocument following the execution of a variable
        /// action with an expanded delete scope - since segment has been refreshed,
        /// the old variable and block objects no longer exists - update the tags now
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_SegmentRefreshedByAction(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                Segment oSegment = oArgs.Segment;

                if (oSegment == null)
                    return;

                //GLOG 3844: If Segment is Transparent, refresh Nodes for first non-Transparent Parent
                while (oSegment.IsTransparent && oSegment.Parent != null)
                {
                    oSegment = oSegment.Parent;
                }
                UpdateTreeNodeTags(oSegment);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// this event is raised by ForteDocument before a segment is replaced
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_BeforeSegmentReplacedByAction(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                //preserve the node index of the segment getting replaced
                Segment oSegment = oArgs.Segment;
                //GLOG 3907: Only save index if Segment is not transparent
                if (!oSegment.IsTransparent)
                {
                    UltraTreeNode oNode = this.GetNodeFromTagID(oSegment.FullTagID);
                    if (oNode != null)
                    {
                        m_oMPDocument.ReplaceSegmentNodeIndex = oNode.Index;
                        //GLOG 4433 (dm) - preserve whether primary
                        m_bReplaceSegmentPrimaryItem = oSegment.IsPrimary;
                    }
                }
                else if (oSegment is CollectionTable && oSegment.Segments.Count == 1 && 
                    !oSegment.Segments[0].IsTransparent)
                {
                    //GLOG 4845: If target is a Collection Table with 1 non-transparent item,
                    //use the Child Segment's Index
                    UltraTreeNode oNode = this.GetNodeFromTagID(oSegment.Segments[0].FullTagID);
                    if (oNode != null)
                    {
                        m_oMPDocument.ReplaceSegmentNodeIndex = oNode.Index;
                        //GLOG 4433 (dm) - preserve whether primary
                        m_bReplaceSegmentPrimaryItem = oSegment.Segments[0].IsPrimary;
                    }
                }
                else if (oSegment.IsTransparent)
                {
                    //GLOG 6094: If transparent Segment is replaced by non-transparent Segment,
                    //Insert new Node before any Paper children of current parent
                    UltraTreeNode oNode = this.GetNodeFromTagID(oSegment.Parent.FullTagID);
                    if (oNode != null)
                    {
                        for (int i = oNode.Nodes.Count - 1; i >= 0; i--)
                        {
                            Segment oChild = oNode.Nodes[i].Tag as Segment;
                            if (oChild != null)
                            {
                                //GLOG 6094:  If toggling between separate page and standard Service List 
                                //Insert new Segment in same position as current Service List (which will be
                                //removed by end of all variable actions
                                if (oChild is Paper || (oChild is ServiceList && oSegment is ServiceListSeparatePage)
                                    || (oChild is ServiceListSeparatePage && oSegment is ServiceList))
                                {
                                    m_oMPDocument.ReplaceSegmentNodeIndex = oNode.Nodes[i].Index;
                                }
                            }
                            else
                                //No non-transparent Segment children
                                break;
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// this event is raised by ForteDocument before a collection table structure is inserted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_BeforeCollectionTableStructureInserted(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                //set flag
                m_bInsertingCollectionTableStructure = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// this event is raised by ForteDocument after a collection table structure is inserted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_AfterCollectionTableStructureInserted(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                //clear flag
                m_bInsertingCollectionTableStructure = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// this event is raised by ForteDocument when a Segment is deleted - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_SegmentDeleted(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                if (this.TaskPane.Mode !=  ForteDocument.Modes.Design)
                {
                    //GLOG 3507: Tags will be refreshed after Replace -
                    //Don't duplicate actions here
                    if (m_bReplacingDocumentSegment || m_bDeletingDocumentSegment)
                        return;

                    Segment oSeg = oArgs.Segment;
                    
                    //If trailer deleted, update display for all trailer nodes
                    if (oSeg.TypeID == mpObjectTypes.Trailer)
                    {
                        //JTS 6/24/11:  Don't add Trailers, since Bounding Objects will be deleted
                        //RefreshTrailerNodes(false);
                    }
                    else
                    {
                        this.DeleteNode(oSeg.FullTagID);
                        //GLOG 3907: If deleted segment is transparent, refresh parent node
                        if (oSeg.IsTransparent)
                        {
                            Segment oParentSeg = oSeg.Parent;
                            while (oParentSeg != null && oParentSeg.Parent != null)
                                oParentSeg = oParentSeg.Parent;
                            if (oParentSeg != null)
                                RefreshSegmentNode(oParentSeg.FullTagID);
                        }

                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// this event is raised by ForteDocument when a Segment is added - update ui
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_SegmentAdded(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                if (this.TaskPane.Mode != ForteDocument.Modes.Design)
                {
                    //GLOG 3507: Tags will be refreshed after Replace -
                    //Don't duplicate actions here
                    //GLOG 5887 (dm) - don't do for collection table structure
                    //3-27-12 (dm) - added m_bSuspendSegmentNodeInsertion
                    if (m_bReplacingDocumentSegment || m_bDeletingDocumentSegment ||
                        m_bSuspendSegmentNodeInsertion)
                        return;

                    Segment oSeg = oArgs.Segment;
                    m_oCurrentSegment = oSeg; //GLOG 15860 (dm)

                    //this conditional was added because trailers, unlike other segments,
                    //are included in the node store regardless of whether they belong to
                    //the current client - it should be removed if we ever add support
                    //for cross-client functionality
                    //GLOG 7103 (dm) - in toolkit mode, display only segments specified in metadata
                    //GLOG : 7998 : ceh
                    if (oSeg.BelongsToCurrentClient && ((!LMP.MacPac.MacPacImplementation.IsToolkit &&
                        !LMP.MacPac.MacPacImplementation.IsToolsAdministrator) ||
                        LMP.Data.Application.DisplayInToolkitTaskPane(oSeg.TypeID)))
                    {
                        oSeg.AfterAuthorUpdated += new AfterAuthorUpdatedHandler(oSeg_AfterAuthorUpdated);
                        oSeg.AfterDefaultAuthorSet += new AfterDefaultAuthorSetHandler(oSeg_AfterDefaultAuthorSet);
                        //If trailer added, refresh display for all trailer nodes
                        if (oSeg.TypeID == mpObjectTypes.Trailer)
                        {
                            //JTS 6/24/11:  Don't add Trailers, since Bounding Objects will be deleted
                            //RefreshTrailerNodes(false);
                        }
                        else
                        {
                            UltraTreeNode oNode = this.InsertNode(oSeg, oSeg.Parent);

                            //populate and expand top-level node
                            if (oSeg.Parent == null && oSeg.CreationStatus == Segment.Status.Finished)
                            {
                                RefreshSegmentNode(oNode);

                                //do not expand Draft Stamp or Trailer nodes
                                if (oSeg.TypeID != mpObjectTypes.DraftStamp && oSeg.TypeID != mpObjectTypes.Trailer)
                                {
                                    oNode.Expanded = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void oSeg_AfterDefaultAuthorSet(object sender, EventArgs e)
        {
            try
            {
                //execute value changed control actions for authors
                ExecuteControlActions(((Segment)sender).Authors,
                    ControlActions.Events.ValueChanged);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void oSeg_AfterJurisdictionSet(object sender, EventArgs e)
        {
            try
            {
                //execute value changed control actions for authors
                ExecuteControlActions(((Segment)sender).Authors,
                    ControlActions.Events.ValueChanged);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void oSeg_AfterAuthorUpdated(object sender, EventArgs e)
        {
            //TODO: this class is subscribed to the AfterAuthorUpdated event twice with
            //two different handlers - this needs to be consolidated into a single
            //subscription and handler
            try
            {
                //execute value changed control actions for authors
                ExecuteControlActions(((Segment)sender).Authors,
                    ControlActions.Events.ValueChanged);

                UpdateTreeNodeTags((Segment)sender);
                //GLOG 6945: Make sure Child Segment validity has been updated
                //if children were replaced by Author Action
                UpdateSegmentImage((Segment)sender, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void DocumentEditor_Leave(object sender, EventArgs e)
        {
            try
            {
                //GLOG 3389: In Word 2003, this event gets called at point TaskPane object is disposed
                if (this.TaskPane.Disposing)
                    return;
                this.ExitEditMode();
                //GLOG 4611: Clear m_oCurNode so click on same node will redisplay control
                //when returning to Editor
                this.m_oCurNode = null;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void OnControlClick(object sender, EventArgs e)
        {
            //TODO: remove this handler if we're not going 
            //to reinstate the Click control action event
            //Variable oVar = this.m_oCurNode.Tag as Variable;

            //if (oVar != null)
            //    //execute actions for click event
            //    ExecuteControlActions(ControlActions.Events.Click);
        }

        private void treeDocContents_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Don't pass keystrokes to tree if menu is visible
                if (m_oMenuSegment != null && m_oMenuSegment.Visible)
                {
                    //if (e.KeyCode == Keys.Escape)
                    //    m_oMenuSegment.Close(ToolStripDropDownCloseReason.Keyboard);
                    //else
                    //    m_oMenuSegment.HandleKeyDown(e);
                    //e.SuppressKeyPress = true;
                    //return;
                }
                else if ((e.KeyCode == Keys.Down) && e.Control && !e.Alt && !e.Shift)
                {
                    e.SuppressKeyPress = true;
                    //Ctrl+Down Arrow displays context menu
                    if (m_oCurNode != null && m_oCurNode.Tag is Segment)
                    {
                        this.CreateSegmentMenu((Segment)m_oCurNode.Tag);
                        m_oMenuSegment.Show(this.treeDocContents, m_oCurNode.UIElement.Rect.Left,
                            m_oCurNode.UIElement.Rect.Top);
                    }
                    return;
                }
                else if (m_oCurNode != null && m_oCurNode.Tag is Block && e.KeyCode == Keys.Enter)
                {
                    Block oBlock = (Block)m_oCurNode.Tag;
                    bool bSelectStart = false;
                    //GLOG 2165: Set selection to start of tag if Body text is different from Default
                    if (oBlock.IsBody)
                    {
                        if (oBlock.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                        {
                            bSelectStart = (oBlock.AssociatedWordTag.Text !=
                                Expression.Evaluate(oBlock.StartingText, oBlock.Segment, oBlock.ForteDocument));
                        }
                        else
                            bSelectStart = (oBlock.AssociatedContentControl.Range.Text !=
                                Expression.Evaluate(oBlock.StartingText, oBlock.Segment, oBlock.ForteDocument));
                    }

                    if (oBlock.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        LMP.Forte.MSWord.WordDoc.SelectTag(oBlock.AssociatedWordTag, bSelectStart, true);
                    }
                    else
                    {
                        this.SelectContentControl(oBlock.AssociatedContentControl, bSelectStart, true);
                    }

                    //Switch focus to Block in Document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                    //TODO:  this doesn't work
                    //SendKeys.SendWait("{F10}{F10}");
                }
                // GLOG : 3302 : JAB
                // Disallow the up and down arrow keys from navigating the tree nodes.
                else if ((e.KeyCode == Keys.Down || e.KeyCode == Keys.Up) 
                    && !e.Control && !e.Alt && !e.Shift)
                {
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                }
                else
                {
                    this.DisableHotKeyMode = false;
                    this.OnKeyPressed(this, e);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void treeDocContents_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (m_oMenuSegment != null && m_oMenuSegment.Visible)
                    return;
                this.OnKeyReleased(this, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        
        private void treeDocContents_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if ((e.Button & MouseButtons.Right) == MouseButtons.Right)
                {
                    ShowContextMenu(e.X, e.Y);
                }
                // GLOG : 3303 : JAB
                // Clicking on a Body node when it has focus already moves focus 
                // to the document.
                else
                {
                    if ((e.Button & MouseButtons.Left) == MouseButtons.Left &&
                        this.m_oCurNode != null &&
                        this.m_oCurNode.Tag is Block &&
                        this.m_oCurNode.IsActive &&
                        this.treeDocContents.ActiveNode != null)
                    {
                        UltraTreeNode oNode = this.m_oCurNode;
                        this.m_oCurNode = null;
                        this.SelectNode(oNode);
                        Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void DocumentEditor_Resize(object sender, EventArgs e)
        {
            try
            {
                //Don't do anything if Resize code is already running
                if (m_bIsResizing)
                    return;

                if (m_oCurCtl != null)
                {
                    this.SuspendLayout();
                    m_bIsResizing = true;
                    treeDocContents.Refresh();

                    //resize control width - setting the anchor property does not work
                    //set control width based on scroll bar visibility
                    int iControlWidth = 0;
                    if (this.ScrollBarsVisible)
                        iControlWidth = this.treeDocContents.Width -
                            m_oCurCtl.Left - 20;
                    else
                        iControlWidth = this.treeDocContents.Width -
                            m_oCurCtl.Left - 5;

                    //When TaskPane in Initializing in Word 2003, width may not yet be finalized
                    //when Initial control is selected.  Set to arbitrary width to avoid problems
                    if (iControlWidth <= 0)
                        iControlWidth = 192;

                    if (this.btnContacts.Visible)
                        iControlWidth = iControlWidth - (this.btnContacts.Width + 1);

                    m_oCurCtl.Width = iControlWidth;

                    //Reposition floating CI button
                    if (this.btnContacts.Visible)
                        this.btnContacts.Left = m_oCurCtl.Left + m_oCurCtl.Width + 1;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
                m_bIsResizing = false;
            }
        }

        void m_oMenuSegment_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Assignments oAssignments = null;
                System.Windows.Forms.Application.DoEvents();

                if (m_oCurNode.Tag is Segment)
                {
                    Segment oSeg = (Segment)m_oCurNode.Tag;
                    //THESE LINES ARE DISABLED BECAUSE THEY CAN CAUSE SPORADIC DOCUMENT LOCK-UP
                    //m_oMenuSegment.Hide();
                    //this.Refresh();
                    switch (e.ClickedItem.Name)
                    {
                        case "m_oMenuSegment_RecreateSegment":
                            this.RecreateSegment(oSeg);
                            break;
                        case "m_oMenuSegment_SaveData":
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            this.SaveData();
                            break;
                        case "m_oMenuSegment_SaveContent":
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            DocumentEditor.SaveSegmentWithData(oSeg);
                            break;
                        case "m_oMenuSegment_RefreshStyles":
                            LMP.MacPac.Application.RefreshStyles(oSeg);
                            break;
                        case "m_oMenuSegment_CopySegment":
                            LMP.MacPac.Application.CopySegmentContent(oSeg);
                            break;
                        case "m_oMenuSegment_Refresh":
                            this.RefreshSegmentNode(m_oCurNode);
                            break;
                        case "m_oMenuSegment_Delete":
                            this.DeleteSegment(oSeg);
                            break;
                        case "m_oMenuSegment_CreateEnvelopes":
                            LMP.MacPac.Application.CreateEnvelopes(oSeg);
                            break;
                        case "m_oMenuSegment_Pleading_InsertTOA":
                            LMP.MacPac.Application.InsertTOA(oSeg);
                            break;
                        case "m_oMenuSegment_Pleading_UpdateTOA": //GLOG 7243 (dm)
                            Segments oSegs = Segment.GetSegments(m_oMPDocument,
                                mpObjectTypes.TOA, 1);
                            LMP.MacPac.Application.UpdateTOA(oSegs[0]);
                            break;
                        case "m_oMenuSegment_RemoveVarAndBlockTags":
                            this.RemoveTags(oSeg, LMP.Forte.MSWord.mpSegmentTagRemovalOptions.None);
                            break;
                        case "m_oMenuSegment_CreateLabels":
                            LMP.MacPac.Application.CreateLabels(oSeg);
                            break;
                        case "m_oMenuSegment_Pleading_InsertExhibits":
                            this.TaskPane.InsertExhibits(oSeg);
                            break;
                        case "m_oMenuSegment_PleadingCaptions_AddItem":
                            //GLOG item #5675 - don't allow function to execute
                            //if there are no assigned objects of the appropriate type - dcf
                            //GLOG 5998 (dm) - this menu item is on the collection's
                            //segment menu - validation is inappropriate here because
                            //collections have no direct assignments
                            //oAssignments = new Assignments(oSeg.TypeID,
                            //    oSeg.ID1, mpObjectTypes.PleadingCaption);

                            //if (oAssignments.Count == 0)
                            //    MessageBox.Show(LMP.Resources.GetLangString("Msg_NoPleadingAssignments"),
                            //        LMP.ComponentProperties.ProductName,
                            //        MessageBoxButtons.OK,
                            //        MessageBoxIcon.Exclamation);
                            //else
                                this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingCaption, true);
                            break;
                        case "m_oMenuSegment_PleadingCounsels_AddItem":
                            //GLOG item #5675 - don't allow function to execute
                            //if there are no assigned objects of the appropriate type - dcf
                            //GLOG 5998 (dm) - this menu item is on the collection's
                            //segment menu - validation is inappropriate here because
                            //collections have no direct assignments
                            //oAssignments = new Assignments(oSeg.TypeID,
                            //    oSeg.ID1, mpObjectTypes.PleadingCounsel);

                            //if (oAssignments.Count == 0)
                            //    MessageBox.Show(LMP.Resources.GetLangString("Msg_NoPleadingAssignments"),
                            //        LMP.ComponentProperties.ProductName,
                            //        MessageBoxButtons.OK,
                            //        MessageBoxIcon.Exclamation);
                            //else
                                this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingCounsel, true);
                            break;
                        case "m_oMenuSegment_PleadingSignatures_AddItem":
                            //GLOG item #5675 - don't allow function to execute
                            //if there are no assigned objects of the appropriate type - dcf
                            //GLOG 5998 (dm) - this menu item is on the collection's
                            //segment menu - validation is inappropriate here because
                            //collections have no direct assignments
                            //oAssignments = new Assignments(oSeg.TypeID,
                            //    oSeg.ID1, mpObjectTypes.PleadingSignature);

                            //if (oAssignments.Count == 0)
                            //    MessageBox.Show(LMP.Resources.GetLangString("Msg_NoPleadingAssignments"),
                            //        LMP.ComponentProperties.ProductName,
                            //        MessageBoxButtons.OK,
                            //        MessageBoxIcon.Exclamation);
                            //else
                                this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingSignature, true);
                            break;
                        case "m_oMenuSegment_PleadingSignatures_ModifyLayout":
                            this.ModifyCollectionLayout(oSeg);
                            break;
                        case "m_oMenuSegment_LetterSignatures_AddItem":
                            this.AddCollectionTableItem(oSeg, mpObjectTypes.LetterSignature, true);
                            break;
                        case "m_oMenuSegment_AgreementSignatures_AddItem":
                            this.AddCollectionTableItem(oSeg, mpObjectTypes.AgreementSignature, true);
                            break;
                        case "m_oMenuSegment_CollectionTable_AddItem":
                            this.AddCollectionTableItem(oSeg, mpObjectTypes.CollectionTableItem, true);
                            break;
                        case "m_oMenuSegment_Pleading_AddNonTableSignature":
                            this.AddNonCollectionChildSegment(oSeg, mpObjectTypes.PleadingSignatureNonTable, true);
                            break;
                        case "m_oMenuSegment_Pleading_AlignPleadingPaper":
                            LMP.MacPac.Application.AlignTextToPleadingPaper(this.m_oMPDocument);
                            break;
                        case "m_oMenuSegment_Pleading_InsertInterrogatories":
                            //GLOG : 5724 : ceh
                            LMP.MacPac.Application.CreateInterrogatories();
                            break;
                        case "m_oMenuSegment_Pleading_MarkCitation":
                            //GLOG : 5724 : ceh
                            //GLOG : 7421 : ceh
                            try
                            {
                                Session.CurrentWordApp.Application.RunOld("MarkCitation");
                            }
                            catch (System.Exception oE)
                            {
                                if (oE.Message.StartsWith("This command is not available."))
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CannotMarkCitationInCurrentLocation"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation);
                                }
                                else
                                {
                                    throw oE;
                                }
                            }
                            break;
                        case "m_oMenuSegment_Pleading_InsertCaption":
                            //GLOG item #5675 - don't allow function to execute
                            //if there are no assigned objects of the appropriate type - dcf
                            oAssignments = new Assignments(oSeg.TypeID,
                                oSeg.ID1, mpObjectTypes.PleadingCaption);

                            if (oAssignments.Count == 0)
                                MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                    "Msg_NoPleadingAssignments"),"pleading caption"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                            else
                                this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingCaption, false);
                            break;
                        case "m_oMenuSegment_Pleading_InsertCounsel":
                            //GLOG item #5675 - don't allow function to execute
                            //if there are no assigned objects of the appropriate type - dcf
                            oAssignments = new Assignments(oSeg.TypeID,
                                oSeg.ID1, mpObjectTypes.PleadingCounsel);

                            if (oAssignments.Count == 0)
                                MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                    "Msg_NoPleadingAssignments"), "pleading counsel"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                            else
                            {
                                try
                                {
                                    this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingCounsel, false);
                                }
                                catch (System.Exception oE)
                                {
                                    if (oE.Message.StartsWith("Table content cannot be inserted inside another table."))
                                    {
                                        MessageBox.Show(LMP.Resources.GetLangString("Msg_CannotInsertTableItemInsideAnotherTable"),
                                            LMP.ComponentProperties.ProductName,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Exclamation);
                                    }
                                    else
                                    {
                                        throw oE;
                                    }
                                }
                            }
                            break;
                        case "m_oMenuSegment_Pleading_InsertSignature":
                            //GLOG item #5675 - don't allow function to execute
                            //if there are no assigned objects of the appropriate type - dcf
                            oAssignments = new Assignments(oSeg.TypeID,
                                oSeg.ID1, mpObjectTypes.PleadingSignature);

                            if (oAssignments.Count == 0)
                                MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                    "Msg_NoPleadingAssignments"), "pleading signature"),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                            else
                                this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingSignature, false);
                            break;
                        case "m_oMenuSegment_Letter_InsertSignature":
                            this.AddCollectionTableItem(oSeg, mpObjectTypes.LetterSignature, false);
                            break;
                        case "m_oMenuSegment_Agreement_InsertSignature":
                            this.AddCollectionTableItem(oSeg, mpObjectTypes.AgreementSignature, false);
                            break;
                        case "m_oMenuSegment_Pleading_InsertSignatureNonTable": //GLOG 4446
                            this.AddNonCollectionChildSegment(oSeg, mpObjectTypes.PleadingSignatureNonTable, false);
                            break;
                        case "m_oMenuSegment_Help":
                            this.ShowHelp();
                            break;
                            // GLOG : 2340 : JAB
                        case "m_oMenuSegment_Pleading_QuoteStyle":
                            LMP.MacPac.Application.SetQuoteStyle(oSeg);
                            break;
                        // GLOG : 3135 : JAB
                        // Update the letterhead.
                        case "m_oMenuSegment_Letterhead_Refresh":
                            LMP.MacPac.Application.UpdateLetterhead();
                            break;
                        case "m_oMenuSegment_TOA_Update":
                            LMP.MacPac.Application.UpdateTOA(oSeg);
                            break;
                        case "m_oMenuSegment_Agreement_InsertTitlePage":
                            this.TaskPane.InsertAgreementTitlePage(oSeg);
                            break;
                        case "m_oMenuSegment_Agreement_InsertExhibits":
                            this.TaskPane.InsertExhibits(oSeg);
                            break;
                        case "m_oMenuSegment_Activate":
                            oSeg.Activated = true;
                            if (!oSeg.Activated)
                            {
                                //inform user that segment could not be activated
                                string xMsg = LMP.Resources.GetLangString(
                                    "Prompt_SegmentCannotBeActivated");
                                MessageBox.Show(xMsg,
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            break;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        
        /// handle close attempt with invalid control value
        /// </summary>
        /// <param name="Doc"></param>
        /// <param name="Cancel"></param>
        private void m_oApp_DocumentBeforeClose(Microsoft.Office.Interop.Word.Document Doc, ref bool Cancel)
        {
            try
            {
                //GLOG 7621: Run this handler only for MasterDataForm
                if (this.ForteDocument != null && Doc != null && this.ForteDocument.WordDocument == Doc &&
                    this.ForteDocument.Segments.Count > 0 && this.ForteDocument.Segments[0].IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                {
                    ExitEditMode();

                    //GLOG 7082
                    bool bSaved = true;
                    try
                    {
                        bSaved = Doc.Saved;
                    }
                    catch
                    {
                        return;
                    }
                    if (bSaved)
                    {
                        return;
                    }

                    //GLOG 6344
                    //GLOG item #6309 - dcf
                    PromptToCloseMasterDataForm();
                    Cancel = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                //GLOG 7941: Unsubscribe to prevent multiple assignments
                if (!Cancel)
                {
                    m_oApp.DocumentBeforeClose -= m_oApp_DocumentBeforeClose;
                }
            }
        }
        private void splitContents_SplitterMoved(object sender, SplitterEventArgs e)
        {
            Session.CurrentUser.UserSettings.HelpWindowHeight = this.splitContents.SplitPosition;
            this.TaskPane.UpdateContentManagerHelpWindowView();
            this.TaskPane.UpdateDocumentDesignerHelpWindowView();
        }
        private void picHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

        public void ShowHelp()
        {
            try
            {
                Trace.WriteNameValuePairs("this.wbDescription.Url.AbsolutePath", this.wbDescription.Url.AbsolutePath);

                Help oHelp = null;
                if (this.wbDescription.Url.AbsolutePath != "blank")
                {
                    oHelp = new Help(this.wbDescription.Url.AbsoluteUri);
                }
                else
                {
                    oHelp = new Help(this.wbDescription.DocumentText);
                }
                oHelp.Text = LMP.ComponentProperties.ProductName + " Help - " + this.tslblStatus.Text;
                oHelp.FormClosed += new FormClosedEventHandler(oHelp_FormClosed);
                oHelp.Show();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }


        /// <summary>
        /// After selecting help the focus needs to 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oHelp_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.m_oCurCtl != null)
            {
                this.m_oCurCtl.Focus();
            }
        }

        /// <summary>
        /// Floating CI button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnContacts_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_oCurCtl == null)
                    return;

                GetContacts(m_oCurCtl, e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuQuickHelp_Copy_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Clipboard.SetText(this.wbDescription.Document.Body.OuterText);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuQuickHelp_SaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                this.wbDescription.ShowSaveAsDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuQuickHelp_Print_Click(object sender, EventArgs e)
        {
            try
            {
                this.wbDescription.ShowPrintDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuQuickHelp_PrintPreview_Click(object sender, EventArgs e)
        {
            try
            {
                this.wbDescription.ShowPrintPreviewDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void DocumentEditor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F3)
            {
                // Toggle to the next task pane component.
                this.TaskPane.Toggle();
            }
        }

        private void wbDescription_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F3)
            {
                this.TaskPane.Toggle();
            }
        }

        private void picMenu_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                // GLOG : 2985 : JAB
                // Set the m_oCurNode if it is null. This situation is visible when 
                // a saved document is opened and the picMenu is clicked.

                if (m_oCurNode == null)
                {
                    this.SelectNode(this.treeDocContents.ActiveNode, false);
                }

                if (m_oCurNode != null)
                {
                    ShowContextMenu(m_oCurNode.Bounds.X + 40, m_oCurNode.Bounds.Y + 10);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handles event raised by SetupDistributedSegmentSections variable action
        /// by refreshing the segment node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="oArgs"></param>
        void m_oMPDocument_SegmentDistributedSectionsChanged(object sender, SegmentEventArgs oArgs)
        {
            try
            {
                Segment oSegment = oArgs.Segment;
                //GLOG 4025: If Segment is Transparent, Refresh Parent Node
                while (oSegment.Parent != null && oSegment.IsTransparent)
                    oSegment = oSegment.Parent;
                RefreshSegmentNode(oSegment.FullTagID);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// populates the supplied list with a list of expanded node keys
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="oNodes"></param>
        private void GetExpandedNodes(List<string> oList, TreeNodesCollection oNodes)
        {
            foreach (UltraTreeNode oNode in oNodes)
            {
                if (oNode.Expanded)
                {
                    //add node key to list
                    oList.Add(oNode.Key);

                    //add expanded child nodes
                    GetExpandedNodes(oList, oNode.Nodes);
                }
            }
        }
        /// <summary>
        /// refreshes the document editor based on the current document
        /// </summary>
        public void RefreshTree()
        {
            RefreshTree(false);
        }
        public void RefreshTree(bool bExpandTopLevel)
        {
            RefreshTree(bExpandTopLevel, System.Drawing.Color.Transparent, GradientStyle.None, false);
        }

        public void RefreshTree(bool bExpandTopLevel, bool bRefreshUIOnly)
        {
            RefreshTree(bExpandTopLevel, System.Drawing.Color.Transparent, GradientStyle.None, bRefreshUIOnly);
        }

        public void RefreshTree(System.Drawing.Color oBackcolor, GradientStyle oGradient)
        {
            RefreshTree(false, oBackcolor, oGradient, false);
        }

        public void RefreshTree(bool bExpandTopLevel, System.Drawing.Color oBackcolor,
            GradientStyle oGradient, bool bRefreshUIOnly)
        {
            RefreshTree(bExpandTopLevel, oBackcolor, oGradient, bRefreshUIOnly, true);
        }

        public void RefreshTree(bool bExpandTopLevel, System.Drawing.Color oBackcolor,
            GradientStyle oGradient, bool bRefreshUIOnly, bool bRestoreNodeExpansion)
        {
            if (oBackcolor == System.Drawing.Color.Transparent)
            {
                oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            }

            if (oGradient == GradientStyle.None)
            {
                oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                    GradientStyle.BackwardDiagonal : GradientStyle.None;
            }

            bool bScreenUpdating = this.TaskPane.ScreenUpdating;
            this.TaskPane.ScreenUpdating = false;

            ExitEditMode();

            try
            {
                //get collection of expanded nodes
                List<string> oExpandedNodes = new List<string>();
                GetExpandedNodes(oExpandedNodes, this.treeDocContents.Nodes);

                if (!bRefreshUIOnly)
                {
                    //GLOG 3491 - update value of variable associated with
                    //current word selection
                    this.UpdateVariableValueFromSelection();

                    this.TaskPane.ForteDocument.Refresh(ForteDocument
                        .RefreshOptions.RefreshTagsWithGUIDIndexes);
                }

                this.RefreshTopLevelNodes(bExpandTopLevel);

                if (bRestoreNodeExpansion)
                {
                    //restore nodes to expansion state
                    foreach (string xNodeKey in oExpandedNodes)
                    {
                        UltraTreeNode oNode = this.treeDocContents.GetNodeByKey(xNodeKey);
                        if (oNode != null && !(oNode.Tag is Variable) &&
                            !(oNode.Tag is Authors) && !(oNode.Tag is Block) &&
                            !(oNode.Tag is Jurisdictions) && !oNode.Key.EndsWith(".Language"))
                            oNode.Expanded = true;
                    }
                }

                UpdateButtons(this.ForteDocument);

                LMP.MacPac.Application.SetXMLTagStateForEditing();
            }
            finally
            {
                if (bScreenUpdating)
                    this.TaskPane.ScreenUpdating = bScreenUpdating;
            }
        }
        public void ClearTree()
        {
            this.treeDocContents.Nodes.Clear();
        }
        public void RemoveTag()
        {
            //JTS 5/10/10: 10.2
            bool bContentControl = m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML;
            string xMsg;
            if (bContentControl)
                xMsg = LMP.Resources.GetLangString("Prompt_RemoveContentControl");
            else
                xMsg = LMP.Resources.GetLangString("Prompt_RemoveTag");
            DialogResult iRes = MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (iRes == DialogResult.Yes)
            {
                ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                try
                {
                    if (m_oCurNode.Tag is Variable)
                    {
                        Variable oVar = (Variable)m_oCurNode.Tag;

                        if (oVar.IsTagless)
                        {
                            if (bContentControl)
                            {
                                foreach (Word.ContentControl oCC in oVar.Segment.ContentControls)
                                {
                                    if (String.GetBoundingObjectBaseName(oCC.Tag) == "mSEG")
                                    {
                                        //clear out all variable definitions
                                        //GLOG 4441: Make sure ObjectData is decrypted if necessary
                                        string xValue =  LMP.Forte.MSWord.WordDoc.GetAttributeValue_CC(oCC, "ObjectData");
                                        xValue = Regex.Replace(xValue, "VariableDefinition=" + oVar.ID + @".*?\|", "");

                                        //write to document
                                        LMP.Forte.MSWord.WordDoc.SetAttributeValue_CC(
                                            oCC, "ObjectData", xValue, LMP.Data.Application.EncryptionPassword);
                                    }
                                }
                            }
                            else
                            {
                                foreach (Word.XMLNode oTag in oVar.Segment.WordTags)
                                {
                                    if (oTag.BaseName == "mSEG")
                                    {
                                        //clear out all variable definitions
                                        //GLOG 4441: Make sure ObjectData is decrypted if necessary
                                        string xValue = String.Decrypt(oTag.SelectSingleNode("@ObjectData", "", false).Text);
                                        xValue = Regex.Replace(xValue, "VariableDefinition=" + oVar.ID + @".*?\|", "");

                                        //write to document
                                        LMP.Forte.MSWord.WordDoc.SetAttributeValue(
                                            oTag, "ObjectData", xValue, LMP.Data.Application.EncryptionPassword);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (bContentControl)
                            {
                                foreach (Word.ContentControl oCC in oVar.AssociatedContentControls)
                                    LMP.Forte.MSWord.WordDoc.RemoveContentControls(oCC,
                                    LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, true);
                            }
                            else
                            {
                                foreach (Word.XMLNode oTag in oVar.AssociatedWordTags)
                                    LMP.Forte.MSWord.WordDoc.RemoveTags(oTag,
                                        LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, true);
                            }
                        }
                    }
                    if (m_oCurNode.Tag is Block)
                    {
                        Block oBlock = (Block)m_oCurNode.Tag;
                        if (bContentControl)
                            LMP.Forte.MSWord.WordDoc.RemoveContentControls(oBlock.AssociatedContentControl,
                                LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, true);
                        else
                            LMP.Forte.MSWord.WordDoc.RemoveTags(oBlock.AssociatedWordTag,
                                LMP.Forte.MSWord.mpSegmentTagRemovalOptions.All, true);
                    }

                    //GLOG item #4294/4354 -
                    //prevent code from seeing variable as having
                    //been edited - otherwise, variable actions
                    //may be executed when refreshing below, 
                    //causing an error - dcf
                    IControl oICtl = (IControl)this.m_oCurCtl;
                    if(oICtl != null)
                        oICtl.IsDirty = false;

                    TaskPane.Refresh(false, true, false);
                }
                finally
                {
                    ForteDocument.IgnoreWordXMLEvents = oEvents;
                }
            }
        }

        public void OnContentControlExit(Word.ContentControl oCC, ref bool Cancel)
        {
            this.OnContentControlExit(oCC, ref Cancel, false);
        }

        public void OnContentControlExit(Word.ContentControl oCC, ref bool Cancel,
            bool bAdvanceExecution)
        {
            DateTime t0 = DateTime.Now;
                
            try
            {
                //exit if event is disabled
                if ((ForteDocument.IgnoreWordXMLEvents &
                    ForteDocument.WordXMLEvents.XMLSelectionChange) > 0)
                    return;

                //handle only macpac content controls
                //GLOG 5058: Tag may be null for ContentControl inside predefined Textbox
                if (oCC.Tag == null || !oCC.Tag.ToString().StartsWith("mp"))
                    return;

                //if an exit is caused programmatically, this event will not fire
                //until the code that triggered it is done executing - in order to avoid
                //this delay, we may call this method immediately and set a flag to
                //prevent running it twice for the same content control
                //GLOG 7394 (dm) - moved this code to before the block that deals
                //with bug where event repeatedly fires, as that was preventing
                //flag from getting set
                string xFlag = '|' + oCC.ID + '|';
                if (bAdvanceExecution)
                    //set new flag
                    m_xDelayedCCExitEvent += xFlag;
                else if (m_xDelayedCCExitEvent.IndexOf(xFlag) > -1)
                {
                    //delete flag and exit
                    m_xDelayedCCExitEvent = m_xDelayedCCExitEvent.Replace(xFlag, "");
                    return;
                }

                Word.ContentControl oSelectionCC = oCC.Application
                    .Selection.Range.ParentContentControl;

                if (oSelectionCC != null && oSelectionCC.Tag == oCC.Tag)
                {
                    //this is the same content control, so don't fire -
                    //this handles a bug with Content Controls whereby the
                    //exit event repeatedly fires
                    return;
                }

                //this handler should only run if user is manually moving between nodes in the
                //document or moving from a node in the document to the node of a different
                //variable or block in the tree - exit if just tabbing between controls
                Word.Application oWord = Session.CurrentWordApp;
                if (m_bHandlingTabPress || oWord.ActiveDocument
                    .ActiveWindow.View.Type == Word.WdViewType.wdPrintPreview)
                    return;

                //GLOG 4241 (dm) - store screen updating status
                bool bScreenUpdating = oWord.ScreenUpdating;

                //Clean up the temp Callout if currently displayed
                this.HideCallout();

                ////TODO: though this might be moot with content controls
                ////correct schema violations, e.g. content in mDels and nested mVars
                //bool bRefreshRequired = false;
                //LMP.Forte.MSWord.WordDoc.CorrectSchemaViolations(OldXMLNode, NewXMLNode,
                //    ref bRefreshRequired);

                //if (bRefreshRequired)
                //{
                //    //invalid tags were deleted - refresh now
                //    m_oMPDocument.Refresh(ForteDocument.RefreshOptions
                //        .RefreshTagsWithGUIDIndexes);
                //}

                this.TaskPane.ScreenUpdating = false;

                //set the value of the variable associated
                //with the old Word tag
                //GLOG 4132 - set flag to skip update from control
                UpdateVariableValue(oCC, true);

                //dispose of any unresolved empty mVars
                try
                {
                    this.ResolveEmptyTags();
                }
                catch { }

                this.TaskPane.ScreenUpdating = bScreenUpdating;
            }
            catch (System.Exception oE)
            {
                while (oE.InnerException != null)
                    oE = oE.InnerException;
                if (oE.Message == "Object has been deleted.")
                {
                    //this error will occur if we've failed to detect the manual
                    //deletion of a tag associated with the same variable as OldXMLNode
                    //or NewXMLNode, e.g. when done by holding down the backspace key -
                    //silently refresh tree
                    this.RefreshTree();
                }
                else
                {
                    this.TaskPane.ScreenUpdating = true;
                    LMP.Error.Show(oE);
                }
            }
            finally
            {
                LMP.Benchmarks.Print(t0);
                //GLOG 5409: Reset flag in OnControlControlEnter instead - that's where ExitEditMode is called
                //GLOG 4132 - reset flag
                //m_xSkipVarUpdateFromControl = "";
            }
        }

        public void OnContentControlEnter(Word.ContentControl oCC)
        {
            this.OnContentControlEnter(oCC, false);
        }

        public void OnContentControlEnter(Word.ContentControl oCC, bool bAdvanceExecution)
        {
            try
            {
                //exit if event is disabled
                if ((ForteDocument.IgnoreWordXMLEvents &
                    ForteDocument.WordXMLEvents.XMLSelectionChange) > 0)
                    return;

                //handle only macpac content controls
                //GLOG 5058: Tag may be null for ContentControl inside predefined Textbox
                if (oCC.Tag == null || !oCC.Tag.ToString().StartsWith("mp"))
                    return;

                //if entry is caused programmatically, this event will not fire
                //until the code that triggered it is done executing - in order to avoid
                //this delay, we may call this method immediately and set a flag to
                //prevent running it twice for the same content control
                string xFlag = '|' + oCC.ID + '|';
                if (bAdvanceExecution)
                    //set new flag
                    m_xDelayedCCEnterEvent += xFlag;
                else if (m_xDelayedCCEnterEvent.IndexOf(xFlag) > -1)
                {
                    //delete flag and exit
                    m_xDelayedCCEnterEvent = m_xDelayedCCEnterEvent.Replace(xFlag, "");
                    return;
                }

                //ensure that the handler code runs only when
                //the passed content control is the control that
                //was actually entered
                Word.ContentControl oSelectionCC = oCC.Application
                    .Selection.Range.ParentContentControl;

                if (oSelectionCC != null && oSelectionCC.Tag == oCC.Tag)
                {
                    //select the variables tree node whose
                    //selection tag is the current Word tag
                    if ((!this.ProgrammaticallySelectingWordTag) &&
                        (this.treeDocContents.Nodes.Count > 0))
                    {
                        SelectNode(oCC);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                //GLOG 5409: Clear flag from previous OnContentControlExit
                m_xSkipVarUpdateFromControl = "";
            }
        }
        /// <summary>
        /// removes the non-segment tags in the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="bSkipSegmentTags"></param>
        public void RemoveTags(Segment oSegment, LMP.Forte.MSWord.mpSegmentTagRemovalOptions iRemovalType)
        {
            DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString("Prompt_RemoveVarAndBlockTags"),
                LMP.ComponentProperties.ProductName,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (iRes == DialogResult.Yes)
            {
                LMP.MacPac.Application.FinishSegment(oSegment, true, false);  //GLOG 7916 (dm)

                //LMP.MacPac.Application.RemoveBoundingObjects(oSegment, iRemovalType);

                ////8-18-11 (dm) - delete tagless variables
                //string xTagID = oSegment.FullTagID;
                //oSegment = this.ForteDocument.FindSegment(xTagID);
                //oSegment.DeleteVariables();
                //this.RefreshTree(false, true);
            }
        }
        /// <summary>
        /// Refresh the backcolor and gradient of the tree and the help view.
        /// </summary>
        /// <param name="oBackcolor"></param>
        /// <param name="oGradient"></param>
        public void RefreshAppearance(System.Drawing.Color oBackcolor, GradientStyle oGradient)
        {
            // Hide the help window or display it with the appropriate size.
            this.UpdateHelpWindowView();


            // Set the backcolor and gradient for the tree per user's prefs.
            if (oBackcolor == null)
            {
                oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            }

            if (oGradient == null)
            {
                oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                    GradientStyle.BackwardDiagonal : GradientStyle.None;
            }

            if (this.treeDocContents.Appearance.BackColor != oBackcolor)
            {
                // Set the backcolor
                this.treeDocContents.Appearance.BackColor = oBackcolor;
            }

            if (this.treeDocContents.Appearance.BackGradientStyle != oGradient)
            {
                //set the gradient according to the user's preference.
                this.treeDocContents.Appearance.BackGradientStyle = oGradient;
            }
        }
        /// <summary>
        /// this method cycles through all the m_oControlHash, and 
        /// refreshes the author lists of all controls of type LMP.Controls.AuthorSelector
        /// </summary>
        public void RefreshAuthorControls()
        {
            foreach (IControl oCtl in m_oControlsHash.Keys)
                if (oCtl is AuthorSelector)
                    ((AuthorSelector)oCtl).RefreshPeopleList(); 
        }
        public void SetTreeAppearance()
        {            
            // Set the backcolor and gradient of the tree according to the user's prefs
            this.treeDocContents.Appearance.BackColor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;

            GradientStyle oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            //set the gradient according to the user's preference.
            this.treeDocContents.Appearance.BackGradientStyle = oGradient;
        }

        public void UpdateHelpWindowView()
        {
            bool bShowHelp = Session.CurrentUser.UserSettings.ShowHelp;
            this.splitContents.Visible = bShowHelp;
            this.pnlDescription.Visible = bShowHelp;

            if (bShowHelp)
            {
                this.pnlDescription.Height = Session.CurrentUser.UserSettings.HelpWindowHeight;
            }
            else
            {
                this.pnlDescription.Height = 0;
            }

            this.splitContents.Enabled = bShowHelp;
        }
        /// <summary>
        /// expands all top level nodes in the tree
        /// </summary>
        public void ExpandTopLevelNodes()
        {
            ExpandTopLevelNodes(false);
        }

        /// <summary>
        /// expands all top level nodes in the tree
        /// </summary>
        /// <param name="bForceRefresh"></param>
        public void ExpandTopLevelNodes(bool bForceRefresh)
        {
            //cycle through top level nodes, expanding each
            foreach (UltraTreeNode oNode in this.treeDocContents.Nodes)
            {
                //12-15-11 (dm) - added bForceRefresh parameter -
                //BeforeExpand handler will refresh if there are no child nodes
                bool bHasNodes = oNode.HasNodes;

                oNode.Expanded = true;

                //12-15-11 (dm)
                if (bForceRefresh)
                {
                    Segment oSeg = oNode.Tag as Segment;
                    if ((oSeg != null) && bHasNodes)
                        this.RefreshSegmentNode(oNode);
                }
            }
        }
        
        /// <summary>
        /// collapses tree to initial state
        /// </summary>
        public void CollapseAll(UltraTreeNode oNode)
        {
            if (oNode == null)
            {
                TreeNodesCollection oNodes = this.treeDocContents.Nodes;

                //Collapse all top level nodes
                foreach (UltraTreeNode oTopNode in oNodes)
                    CollapseAll(oTopNode);

                //GLOG item #6309 - dcf
                if (oNodes.Count > 0)
                {
                    oNodes[0].Expanded = true;
                }

                //GLOG - 3338 - CEH
                if (TaskPane.NewSegment != null && TaskPane.NewSegment.ShowChooser)
                    //set focus to chooser
                    this.SelectChooserNode();
                else
                    this.SelectFirstVariableNode();
            }
            else
            {
                //Collapse node branch recursively
                for (int i = 0; i < oNode.Nodes.Count; i++)
                {
                    if (oNode.Nodes[i].HasExpansionIndicator)
                        CollapseAll(oNode.Nodes[i]);
                }
                oNode.Expanded = false;
            }
        }
        public void CollapseAll()
        {
            ExitEditMode();
            this.treeDocContents.SuspendLayout();
            foreach (UltraTreeNode oNode in this.treeDocContents.Nodes)
            {
                oNode.Expanded = false;
            }
            this.treeDocContents.ActiveNode = this.treeDocContents.Nodes[0];
            this.treeDocContents.ResumeLayout();
            this.treeDocContents.Refresh();
        }
        /// <summary>
        /// Expand all nodes
        /// </summary>
        public void ExpandAll()
        {
            UltraTreeNode oNode = m_oCurNode;
            ExitEditMode();
            this.treeDocContents.SuspendLayout();
            this.treeDocContents.ExpandAll();
            this.treeDocContents.ActiveNode = oNode;
            this.treeDocContents.ResumeLayout();
            this.treeDocContents.Refresh();
        }
        /// <summary>
        /// Selects first variable node under Selected Segment node
        /// </summary>
        /// <param name="oSegment"></param>
        internal void SelectFirstNodeForSegment(Segment oSegment)
        {
            SelectFirstNodeForSegment(oSegment, true);
        }
        /// <summary>
        /// Selects first variable node under Selected Segment node
        /// </summary>
        /// <param name="oSegment"></param>
        internal void SelectFirstNodeForSegment(Segment oSegment, bool bShowTaskPane)
        {
            try
            {
                Variable oDisplayVar = null;

                //GLOG item #6248 - dcf
                System.Windows.Forms.Application.DoEvents();
                this.TaskPane.WordTaskPane.Visible = bShowTaskPane;
                System.Windows.Forms.Application.DoEvents();

                //GLOG 3936(dm) - if segment is transparent, get closest visible ancestor
                //GLOG item #6102 - supercedes GLOG item #3936 -
                //select first node of transparent segment - dcf
                while (oSegment.IsTransparent)
                {

                    int iNumVariables = oSegment.Variables.Count;

                    for(int i=0; i<iNumVariables; i++)
                    {
                        if ((oSegment.Variables[i].DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                            Variable.ControlHosts.DocumentEditor)
                        {
                            oDisplayVar = oSegment.Variables[i];
                            break;
                        }
                    }

                    oSegment = oSegment.Parent;
                }
                
                UltraTreeNode oParentNode = this.GetNodeFromTagID(oSegment.FullTagID, true);

                //Expand node if necessary - Pleading table items are not expanded automatically when added
                if (!oParentNode.HasNodes)
                    RefreshSegmentNode(oParentNode, true);

                if (oParentNode.HasNodes)
                {
                    //GLOG item #6102 - dcf
                    if (oDisplayVar != null)
                    {
                        SelectNode(oDisplayVar.Segment.FullTagID + "." + oDisplayVar.ID);
                    }
                    else
                    {
                        SelectNode(this.GetNodeFromTagID(oSegment.FullTagID).Nodes[0], true);
                    }

                    if (this.picMenu.Visible)
                    {
                        ShowContextMenuButton(this.m_oCurNode);
                    }

                    //attempt to show all nodes of child segment -
                    //first show last node, then first node, which is active at this point
                    bool bScreenUpdating = TaskPane.ScreenUpdating;
                    
                    TaskPane.ScreenUpdating = false;
                    UltraTreeNode oLastNode = oParentNode.Nodes[oParentNode.Nodes.Count - 1];
                    UltraTreeNode oActiveNode = this.treeDocContents.ActiveNode;

                    if (!oLastNode.IsInView)
                    {
                        oLastNode.BringIntoView(false);
                    }

                    if (!oActiveNode.IsInView)
                    {
                        oActiveNode.BringIntoView(false);
                    }

                    this.SizeAndPositionControl();

                    TaskPane.ScreenUpdating = bScreenUpdating;
                }
                else if (oSegment.ShowChooser)
                {
                    //segment design doesn't specify nodes
                    m_oCurNode = null; //GLOG 6202 (dm)
                    SelectNode(this.GetNodeFromTagID(oSegment.FullTagID), true);
                    //this.RefreshTree(true, true);
                }
                else
                {
                    //Select Segment node if there are no children
                    SelectNode(oParentNode, false);
                }
            }
            catch { }
        }

        /// <summary>
        /// Determine if the first variable node is selected.
        /// </summary>
        internal bool IsFirstVariableNodeSelected
        {
            get
            {
                TreeNodesCollection oNodes = this.treeDocContents.Nodes;
                return oNodes.Count > 0 && oNodes[0].Nodes.Count > 0 &&
                    this.m_oCurNode == oNodes[0].Nodes[0] && m_oCurCtl != null && m_oCurCtl.Focused;
            }
        }

        /// <summary>
        /// Selects the first variable node in the tree
        /// </summary>
        internal void SelectFirstVariableNode()
        {
            //if (IsFirstVariableNodeSelected)
            //{
            //    // Do nothing if the first variable node is selected.
            //    return;
            //}
            ExitEditMode();

            try
            {
                //Set focus to first variable node
                if (this.treeDocContents.Nodes.Count > 0)
                {
                    if (this.treeDocContents.Nodes[0].Nodes.Count > 0)
                        SelectNode(this.treeDocContents.Nodes[0].Nodes[0], true);
                    else
                    {
                        //If only node is Segment with chooser, make sure chooser is displayed
                        m_oCurNode = null;
                        SelectNode(this.treeDocContents.Nodes[0], true);
                    }
                }

                if (m_oCurCtl != null)
                {
                    m_oCurCtl.Focus();
                    m_oCurCtl.Select();
                }
                //Make sure scrollbar is at top - additional space for control may have 
                //changed scrollbar state
                this.treeDocContents.TopNode = this.treeDocContents.Nodes[0];
            }
            catch { }
        }

        //GLOG - 3338 - CEH
        /// <summary>
        /// Selects chooser node in the tree
        /// </summary>
        internal void SelectChooserNode()
        {
            ExitEditMode();

            try
            {
                //GLOG item #6549 - dcf
                System.Windows.Forms.Application.DoEvents();
                this.TaskPane.WordTaskPane.Visible = true;
                System.Windows.Forms.Application.DoEvents();

                //Set focus to first node
                if (this.treeDocContents.Nodes.Count > 0)
                {
                    m_oCurNode = null;
                    SelectNode(this.treeDocContents.Nodes[0], true);
                }

                if (m_oCurCtl != null)
                {
                    m_oCurCtl.Focus();
                    m_oCurCtl.Select();
                }
                //Make sure scrollbar is at top - additional space for control may have 
                //changed scrollbar state
                this.treeDocContents.TopNode = this.treeDocContents.Nodes[0];
            }
            catch { }
        }

        /// <summary>
        /// Selects the last variable node in the tree
        /// </summary>
        internal void SelectLastVariableNode()
        {
            ExitEditMode();
            try
            {
                //Initialize with last node in top branch
                UltraTreeNode oLast = this.treeDocContents.Nodes[this.treeDocContents.Nodes.Count - 1];
                do
                {
                    //Check if it's a Segment node
                    if (oLast.HasExpansionIndicator)
                    {
                        oLast.Expanded = true;
                        if (oLast.Nodes.Count > 0)
                            oLast = oLast.Nodes[oLast.Nodes.Count - 1];
                        else
                            break;
                    }
                }
                while (oLast.HasExpansionIndicator);
                //Set focus to first variable node
                SelectNode(oLast, true);
            }
            catch { }
        }
        /// <summary>
        /// refreshes top level nodes in tree
        /// </summary>
        public void RefreshTopLevelNodes(bool bExpandTop)
        {
            DateTime t0 = DateTime.Now;
            bool bScreenUpdating = false;
            
            try
            {
                //get screenupdating state for later use
                bScreenUpdating = this.TaskPane.ScreenUpdating;

                this.TaskPane.ScreenUpdating = false;

                //display top level nodes - first clear tree
                this.treeDocContents.Nodes.Clear();

                //clear out current node references
                this.m_oCurNode = null;
                this.m_oCurCtl = null;
                this.m_oCurTabNode = null;

                //empty controls Hash Table
                m_oControlsHash.Clear();
                //GLOG 3589: Empty Hotkeys Hashtable
                m_oHotkeys.Clear();

                //clear transparent node list
                //GLOG 3482 - added leading pipe
                m_xTransparentNodes = "|";

                //get top level nodes
                //GLOG 7101 (dm) - in toolkit mode, limit collection to
                //segments specified in metadata as displayable in task pane
                Segments oSegments = null;
				//GLOG : 7998 : ceh
                if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                {
                    oSegments = new Segments(m_oMPDocument);
                    Segment.GetToolkitTaskPaneSegments(m_oMPDocument.Segments, ref oSegments);
                }
                else
                    oSegments = m_oMPDocument.Segments;

                //cycle through segments, adding each as a node to tree
                for (int i = 0; i < oSegments.Count; i++)
                {
                    //get node
                    Segment oSegment = oSegments[i];
                    //GLOG 3246: Trailers handled separately
                    if (oSegment.TypeID == mpObjectTypes.Trailer)
                    {
                        continue;
                    }
                    //this conditional was added because trailers, unlike other segments,
                    //are included in the node store regardless of whether they belong to
                    //the current client - it should be removed if we ever add support
                    //for cross-client functionality
                    //GLOG 7103 (dm) - in toolkit mode, display only segments specified in metadata
                    //GLOG 7101 (dm) - removed toolkit mode condition - new code above ensures that
                    //all segments supported by toolkit will be displayed as top-level, regardless of
                    //whether the parent is supported
                    if (oSegment.BelongsToCurrentClient)
                    {
                        //add node to tree - 
                        //use segment type name if component -
                        //otherwise, use segment display name
                        //GLOG : 15813 : ceh - get display name for Document components that are of type Architect
                        string xNodeText = (oSegment.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent && oSegment.TypeID != mpObjectTypes.Architect) ?
                                            LMP.Data.Application.GetObjectTypeDisplayName(oSegment.TypeID) : oSegment.DisplayName;

                        UltraTreeNode oTreeNode = this.treeDocContents.Nodes.Add(
                            oSegment.FullTagID, xNodeText);

                        //initialize new node
                        InitializeNode(oSegment, oTreeNode, true);

                        if (bExpandTop)
                        {
                            oTreeNode.Expanded = true;
                        }
                    }
                }

                //RefreshTrailerNodes(bExpandTop);
            }
            finally
            {
                this.TaskPane.ScreenUpdating = bScreenUpdating;
                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// selects the node with the specified tag id
        /// </summary>
        /// <param name="xTagID"></param>
        public void SelectNode(string xTagID)
        {
            try
            {
                if (xTagID == null || xTagID == "")
                {
                    ExitEditMode();
                    return;
                }

                //select node only if the document editor is visible -
                //won't be if we're in design mode
                if (this.Visible)
                {
                    UltraTreeNode oNode = null;
                    //get node with the specified tag id
                    try
                    {
                        //GLOG 6581: Test for existence of node first
                        //Transparent children of children won't have been
                        //added to m_xTransparentNodes if the top child has
                        //never been refreshed - this will refresh it if necessary
                        oNode = this.GetNodeFromTagID(xTagID);
                    }
                    catch { }
                    if (oNode == null)
                    {
                        //if node is transparent, select parent
                        while (this.NodeIsTransparent(xTagID))
                        {
                            int iPos = xTagID.LastIndexOf('.');
                            xTagID = xTagID.Substring(0, iPos);
                        }
                    }
                    //get node with the specified tag id
                    oNode = this.GetNodeFromTagID(xTagID);
                    this.SelectNode(oNode);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotSelectDocContentsTreeNode") + xTagID, oE);
            }
        }
        /// <summary>
        /// sets the text of the value node of a variable
        /// </summary>
        /// <param name="xVariableTreeNodeKey"></param>
        /// <param name="oVariable"></param>
        public void SetVariablesNodeValue(string xVariableTreeNodeKey, Variable oVariable)
        {
            //get node with the specified tag id
            UltraTreeNode oNode = this.GetNodeFromTagID(xVariableTreeNodeKey);

            bool bNodeIsInView = oNode.IsInView;

            //ensure that the node is visible
            this.EnsureVisibleNode(oNode);

            if(oNode.NextVisibleNode != null)
                //set value of associated value node
                oNode.NextVisibleNode.Text = this.GetVariableUIValue_API(oVariable);

            //GLOG #3820 - dcf
            if (!bNodeIsInView)
                this.EnsureHiddenNode(oNode);
        }
        /// <summary>
        /// refreshes the portion of the tree
        /// starting with the specified segment tree node
        /// </summary>
        /// <param name="xTagID"></param>
        public void RefreshSegmentNode(string xTagID)
        {
            UltraTreeNode oNode = null;

            //get segment node - if it's not found, simply skip refresh -
            //this method may be called while the segment is still initializing
            //and before it's been added to the tree
            try
            {
                oNode = GetNodeFromTagID(xTagID);
            }
            catch { }

            //refresh segment node
            if (oNode != null)
            {
                this.pnlDocContents.SuspendLayout();
                this.treeDocContents.SuspendLayout();

                try
                {
                    RefreshSegmentNode(oNode, true);
                }
                finally
                {
                    this.treeDocContents.ResumeLayout();
                    this.pnlDocContents.ResumeLayout();
                }
            }
        }
        /// <summary>
        /// deletes the nodes with the specified tag id
        /// </summary>
        /// <param name="xTagID"></param>
        public void DeleteNode(string xTagID)
        {
            UltraTreeNode oNode = null;

            try
            {
                //if this is the tag id of a transparent segment, just remove from
                //list of transparent node
                if (this.NodeIsTransparent(xTagID))
                {
                    m_xTransparentNodes = m_xTransparentNodes.Replace(xTagID + '|', "");
                    return;
                }

                //get node
                try
                {
                    oNode = this.GetNodeFromTagID(xTagID, false);
                }
                catch { }

                if (oNode != null)
                {
                    UltraTreeNode oParentNode = oNode.Parent;
                    //GLOG 2923: Segment nodes handled separately
                    if ((this.treeDocContents.Nodes.Count == 1) &&
                        (oParentNode == null) && !(oNode.Tag is Segment))
                    {
                        //this is the only node on the tree - to avoid error,
                        //use Clear() instead of Remove()
                        this.treeDocContents.Nodes.Clear();
                    }
                    else
                    {
                        //switch target to More node if this is the only node on it
                        if (oParentNode != null)
                        {
                            if ((oParentNode.Nodes.Count == 1) &&
                                (oParentNode.Key.IndexOf(".Advanced") != -1))
                                oNode = oParentNode;
                        }

                        if (oNode.Tag is Variable)
                        {
                            //remove associated control from hashtable
                            UltraTreeNode oValueNode = oNode.Nodes[0];
                            if ((oValueNode != null) && (oValueNode.Tag != null))
                            {
                                //GLOG 2645: Need to unsubscribe events, otherwise they'll be duplicated
                                //next time ValueNode tag is set in EnterVariableEditMode
                                IControl oICtl = oValueNode.Tag as IControl;
                                if (oICtl != null)
                                {
                                    oICtl.TabPressed -= oIControl_TabPressed;
                                    if (oICtl is ICIable)
                                    {
                                        ((ICIable)oICtl).CIRequested -= GetContacts;
                                        //Enable double-clicking to Get Contacts
                                        ((System.Windows.Forms.Control)oICtl).DoubleClick -= OnControlDoubleClick;
                                    }
                                }
                                m_oControlsHash.Remove(oValueNode.Tag);

                                //hide control - added 1/26/09 (dm) because active control
                                //was sometimes still displayed after the deletion of its node,
                                //leading to errors if the user then entered a value in it
                                ((System.Windows.Forms.Control)oValueNode.Tag).Visible = false;
                            }
                        }
                        else if (oNode.Tag is Segment)
                        {
                            //GLOG 2923: Remove Chooser if currently displayed
                            if ((m_oCurNode == oNode))
                            {
                                if (m_oCurCtl is Chooser)
                                {
                                    this.treeDocContents.Controls.Remove(m_oCurCtl);
                                    m_oControlsHash.Remove(m_oCurCtl);
                                    //Hide Context Menu button
                                    m_oCurCtl = null;
                                }
                                //Hide Context Menu button
                                this.picMenu.Visible = false;
                                //Clear Help text
                                //GLOG 7214
                                TaskPane.SetHelpText(wbDescription, "");
                                this.tslblStatus.Text = null;
                                this.lblStatus.Text = null;
                            }
                            if (this.treeDocContents.Nodes.Count == 1 && oParentNode == null)
                            {
                                //this is the only node on the tree - to avoid error,
                                //use Clear() instead of Remove()
                                this.treeDocContents.Nodes.Clear();
                                return;
                            }

                            UpdateButtons(this.ForteDocument);
                        }

                        //GLOG 3367 - delete hotkey
                        if ((oNode.Tag != null) && !(oNode.Tag is Segment))
                            this.DeleteHotkey(oNode);

                        //GLOG 8133: Make sure Node Activate event code does not run
                        bool bHandlersActive = m_bSuppressTreeActivationHandlers;
                        m_bSuppressTreeActivationHandlers = true;
                        try
                        {
                            //remove node
                            oNode.Remove();
                        }
                        catch (System.ArgumentOutOfRangeException)
                        {
                            //due to a bug in UltraTreeNode, this exception may be raised (for no
                            //apparent reason) in the course of removing multiple nodes -
                            //it works to simply call the Remove() method again
                            oNode.Remove();
                        }
                        //GLOG 8133
                        m_bSuppressTreeActivationHandlers = bHandlersActive;
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString(
                        "Error_CouldNotSelectDocContentsTreeNode") + xTagID, oE);
            }
        }
        /// <summary>
        /// inserts node for specified variable on node for specified segment
        /// at the location indicated by its execution index
        /// </summary>
        /// <param name="oVar"></param>
        public UltraTreeNode InsertNode(Variable oVar)
        {
            UltraTreeNode oVarNode = null;
            string xKey = null;
            if (!oVar.IsStandalone)
            {
                //get segment node
                UltraTreeNode oSegmentNode = null;
                Segment oSegment = oVar.Segment;
                string xSegmentTagID = oSegment.FullTagID;

                //GLOG 2460 (dm) - variables belonging to transparent segments
                //require different handling
                bool bTransparentSeg = oSegment.IsTransparent;

                //if segment is transparent, insert on its parent's node
                while (oSegment.IsTransparent)
                    oSegment = oSegment.Parent;

                try
                {
                    oSegmentNode = GetNodeFromTagID(oSegment.FullTagID, false);
                }
                catch { }

                //exit if segment node does not yet exist or has not yet been populated
                if (oSegmentNode == null)
                    return oVarNode;
                else if (oSegmentNode.Nodes.Count == 0)
                    return oVarNode;

                //get insertion position
                int iIndex = 0;
                if (!bTransparentSeg)
                {
                    for (int i = 0; i < oSegment.Variables.Count; i++)
                    {
                        Variable oVar2 = oSegment.Variables[i];
                        if (oVar2.Name == oVar.Name)
                            break;
                        else if (((oVar2.DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                            Variable.ControlHosts.DocumentEditor) && (oVar2.DisplayLevel ==
                            oVar.DisplayLevel))
                            iIndex++;
                    }
                }

                //insert node
                xKey = xSegmentTagID + "." + oVar.ID;
                if (oVar.DisplayLevel == Variable.Levels.Basic)
                {
                    if (!bTransparentSeg)
                    {
                        //8-18-11 (dm) - the following nodes are now only displayed
                        //when there are variables
                        //10-12-11 (dm) - condition changed to HasSnapshot
                        if (!Snapshot.Exists(oSegment))
                        {
                            //account for Jurisdiction node
                            if (oSegment.ShowCourtChooser)
                                iIndex++;

                            //account for authors node
                            if ((oSegment.IsTopLevel || (!oSegment.LinkAuthorsToParent))
                                && (oSegment.MaxAuthors > 0))
                                iIndex++;

                            //account for language node
                            if (oSegment.IsTopLevel && oSegment.SupportsMultipleLanguages)
                                iIndex++;
                        }
                    }
                    else
                    {
                        //get More node
                        UltraTreeNode oMoreNode = null;
                        try
                        {
                            oMoreNode = GetNodeFromTagID(oSegment.FullTagID + ".Advanced");
                        }
                        catch { }

                        //get first child segment node
                        UltraTreeNode oChildSegNode = null;
                        foreach (UltraTreeNode oNode in oSegmentNode.Nodes)
                        {
                            if (oNode.Tag is Segment)
                            {
                                oChildSegNode = oNode;
                                break;
                            }
                        }
                        
                        if (oMoreNode == null && oChildSegNode == null)
                        {
                            //add as last node
                            iIndex = oSegmentNode.Nodes.Count;
                        }
                        else if (oMoreNode != null)
                        {
                            //add as last node in basic section
                            iIndex = oMoreNode.Index;
                        }
                        else
                        {
                            //add as last node in basic section
                            iIndex = oChildSegNode.Index;
                        }
                    }

                    //add to main variables section
                    oVarNode = oSegmentNode.Nodes.Insert(iIndex, xKey,
                        oVar.DisplayName + ":");
                }
                else
                {
                    //get More node
                    UltraTreeNode oMoreNode = null;
                    try
                    {
                        oMoreNode = GetNodeFromTagID(oSegment.FullTagID + ".Advanced");
                    }
                    catch { }

                    //create More node if neecessary
                    if (oMoreNode == null)
                        oMoreNode = CreateMoreNode(oSegmentNode, oSegment);

                    //add variable belonging to transparent seg to end of More node
                    if (bTransparentSeg)
                        iIndex = oMoreNode.Nodes.Count;

                    //add to More node
                    oVarNode = oMoreNode.Nodes.Insert(iIndex, xKey,
                        oVar.DisplayName + ":");
                }
            }
            else
            {
                //variable is standalone - add to top level
                xKey = oVar.ID;
                oVarNode = treeDocContents.Nodes.Add(xKey, oVar.DisplayName + ":");
            }

            //initialize new node
            InitializeNode_API(oVar, oVarNode, oVar.IsStandalone);

            //5-12-11 (dm)
            if (!oVar.Segment.Activated)
                oVarNode.Enabled = false;

            return oVarNode;
        }
        /// <summary>
        /// adds node for specified block to end of node for specified segment
        /// </summary>
        /// <param name="oBlock"></param>
        public UltraTreeNode InsertNode(Block oBlock)
        {
            UltraTreeNode oBlockNode = null;

            if (!oBlock.IsStandalone)
            {
                //get segment node
                UltraTreeNode oSegmentNode = null;
                Segment oSegment = oBlock.Segment;

                //if segment is transparent, insert on its parent's node
                while (oSegment.IsTransparent)
                    oSegment = oSegment.Parent;

                string xSegmentTagID = oSegment.FullTagID;
                try
                {
                    oSegmentNode = GetNodeFromTagID(xSegmentTagID);
                }
                catch { }

                //exit if segment node does not yet exist or has not yet been populated
                if (oSegmentNode == null)
                    return oBlockNode;
                else if (oSegmentNode.Nodes.Count == 0)
                    return oBlockNode;

                //add node
                if (oBlock.DisplayLevel == Variable.Levels.Basic)
                    oBlockNode = oSegmentNode.Nodes.Add(oBlock.TagID, oBlock.DisplayName);
                else
                {
                    //get More node
                    UltraTreeNode oMoreNode = null;
                    try
                    {
                        oMoreNode = GetNodeFromTagID(oSegment.FullTagID + ".Advanced");
                    }
                    catch { }

                    //create More node if neecessary
                    if (oMoreNode == null)
                        oMoreNode = CreateMoreNode(oSegmentNode, oSegment);

                    //add to More node
                    oBlockNode = oMoreNode.Nodes.Add(oBlock.TagID, oBlock.DisplayName);
                }
            }
            else
                //block is standalone - add to top level
                oBlockNode = treeDocContents.Nodes.Add(oBlock.TagID, oBlock.DisplayName);

            //add image
            InitializeNode(oBlock, oBlockNode, oBlock.IsStandalone);

            return oBlockNode;
        }
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oParent"></param>
        public UltraTreeNode InsertNode(Segment oSegment, Segment oParent)
        {
            //add node
            UltraTreeNode oNode = null;
            UltraTreeNode oParentNode = null;
            if (oParent != null)
            {
                Segment oVisibleParent;

                //if parent is transparent, insert on grandparent's node
                oVisibleParent = oParent;
                while (oVisibleParent.IsTransparent)
                    oVisibleParent = oVisibleParent.Parent;

                //if there's already a sibling of the same type,
                //ensure that parent is not transparent
                if ((oParent.IsTransparent) && (!oSegment.IsOnlyInstanceOfType))
                {
                    oParent.IsTransparent = false;
                    m_xTransparentNodes = m_xTransparentNodes.Replace(
                        oParent.FullTagID + '|', "");
                    RefreshSegmentNode(oVisibleParent.FullTagID);
                    oParentNode = GetNodeFromTagID(oParent.FullTagID);
                    oParentNode.Expanded = true;
                    return GetNodeFromTagID(oSegment.FullTagID);
                }

                //get parent node
                try
                {
                    oParentNode = GetNodeFromTagID(oVisibleParent.FullTagID);
                }
                catch { }

                //exit if segment node does not yet exist or has not yet been populated -
                //GLOG item #5771 - added condition that parent node be expanded - dcf
                if (oParentNode == null)
                    return oNode;
                else if ((oParentNode.Nodes.Count == 0) && !oParentNode.Expanded && !(oParent is CollectionTable))
                    return oNode;

                //insert node
                if (!oSegment.IsTransparent)
                {
                    //display transparent sibling if necessary
                    if (SiblingIsTransparent(oSegment))
                    {
                        RefreshSegmentNode(oParentNode, true);
                        return GetNodeFromTagID(oSegment.FullTagID);
                    }

                    int iIndex = -1;
                    if ((m_oMPDocument.ReplaceSegmentNodeIndex > -1) &&
                        !m_bInsertingCollectionTableStructure)
                        //this is a chooser replacement - reinsert at same index
                        //GLOG 5921 (dm) - ignore index while inserting collection
                        //table structure
                        iIndex = m_oMPDocument.ReplaceSegmentNodeIndex;
                    else if (oParent.IsTransparent)
                    {
                        //group with other children of transparent parent
                        UltraTreeNode oSiblingNode = null;
                        for (int i = 0; i < oParent.Segments.Count; i++)
                        {
                            string xTagID = oParent.Segments[i].FullTagID;
                            if (xTagID != oSegment.FullTagID)
                            {
                                //sibling found - get node
                                oSiblingNode = GetNodeFromTagID(xTagID);
                                break;
                            }
                        }

                        if (oSiblingNode != null)
                        {
                            //get index of last sibling on tree
                            TreeNodesCollection oNodes = oParentNode.Nodes;
                            iIndex = oSiblingNode.Index;
                            while ((iIndex < oNodes.Count) &&
                                (oNodes[iIndex].Tag is Segment) &&
                                (((Segment)oNodes[iIndex].Tag).Parent.FullTagID ==
                                oParent.FullTagID))
                                iIndex++;
                        }
                    }

                    string xNodeText = null;

                    //GLOG : 15813 : ceh - get display name for Document components that are of type Architect
                    if (oSegment.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent && oSegment.TypeID != mpObjectTypes.Architect)
                        //document component children display their segment type
                        xNodeText = LMP.Data.Application.GetObjectTypeDisplayName(oSegment.TypeID);
                    else
                        xNodeText = oSegment.DisplayName;

                    //the following conditional was modified for GLOG 3477
                    if (iIndex > -1)
                    {
                        //insert at index
                        oNode = oParentNode.Nodes.Insert(iIndex, oSegment.FullTagID, xNodeText);
                    }
                    else
                        //insert at bottom
                        oNode = oParentNode.Nodes.Add(oSegment.FullTagID, xNodeText);

                    //preserve primacy on pleading table item replacement via chooser
                    if (m_bReplaceSegmentPrimaryItem && !oSegment.IsPrimary)
                    {
                        oSegment.IsPrimary = true;
                        if (oSegment.LinkAuthorsToParentDef ==
                            Segment.LinkAuthorsToParentOptions.FirstChildOfType)
                            oSegment.LinkAuthorsToParent = true;
                    }
                }
                else
                {
                    //GLOG 3447: Need to update Hashtable before Refreshing Node, so that
                    //control actions will be able to locate Variable nodes contained therein
                    //add tag id to list of transparent nodes
                    if (!this.NodeIsTransparent(oSegment.FullTagID))
                        m_xTransparentNodes += oSegment.FullTagID + '|';

                    //segment is transparent - insert nodes for children
                    RefreshTransparentNode(oSegment, oParentNode);

                    return oNode;
                }
            }
            else
            {
                //GLOG 3507
                int iNewIndex = m_oMPDocument.ReplaceSegmentNodeIndex;

                string xNodeText = null;

                //GLOG : 15813 : ceh - get display name for Document components that are of type Architect
                if (oSegment.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent && oSegment.TypeID != mpObjectTypes.Architect)
                {
                    xNodeText = LMP.Data.Application.GetObjectTypeDisplayName(oSegment.TypeID);
                }
                else if (oSegment.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm && oSegment.Prefill != null)
                    xNodeText = oSegment.Prefill.Name;
                else
                {
                    xNodeText = oSegment.DisplayName;
                }


                //add to top level
                if (iNewIndex > -1)
                    //GLOG 3507: Reinsert replacement segment at same position as previous item
                    oNode = treeDocContents.Nodes.Insert(iNewIndex, oSegment.FullTagID, xNodeText);
                else
                    oNode = treeDocContents.Nodes.Add(oSegment.FullTagID, xNodeText);
            }

            //add image
            InitializeNode(oSegment, oNode, (oParent == null));

            UpdateButtons(oSegment.ForteDocument);
            return oNode;
        }
        /// <summary>
        /// retags the variable and block nodes on the node for 
        /// all segments in the document associated with this editor
        /// </summary>
        public void UpdateTreeNodeTags()
        {
            DateTime t0 = DateTime.Now;
            for(int i=0; i<this.ForteDocument.Segments.Count; i++)
            {
                Segment oSegment = this.ForteDocument.Segments[i];
                UpdateTreeNodeTags(oSegment, true);
            }
            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// retags the variable and block nodes on the node for the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        public void UpdateTreeNodeTags(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;
            UpdateTreeNodeTags(oSegment, false);
            LMP.Benchmarks.Print(t0);
        }

        public void UpdateTreeNodeTags(Segment oSegment, bool bUpdateChildren)
        {
            //get segment node
            UltraTreeNode oSegmentNode = null;
            try
            {
                oSegmentNode = GetNodeFromTagID(oSegment.FullTagID, false);
            }
            catch { }

            //exit if segment node does not yet exist or has not yet been populated
            if (oSegmentNode == null)
                return;
            else if (oSegmentNode.Nodes.Count == 0)
                return;

            if (bUpdateChildren)
            {
                for(int i=0; i<oSegment.Segments.Count; i++)
                {
                    Segment oChildSegment = oSegment.Segments[i];
                    UpdateTreeNodeTags(oChildSegment, true);
                }
            }

            //update tree node tags
            this.UpdateTreeNodeTags(oSegmentNode.Nodes);

            //update tags on more node
            UltraTreeNode oMoreNode = null;
            try
            {
                oMoreNode = GetNodeFromTagID(oSegment.FullTagID + ".Advanced");
            }
            catch { }
            if (oMoreNode != null)
                this.UpdateTreeNodeTags(oMoreNode.Nodes);
        }

        /// <summary>
        /// retags the variable and block nodes on the specified nodes
        /// </summary>
        /// <param name="oSegment"></param>
        private void UpdateTreeNodeTags(TreeNodesCollection oNodes)
        {
            for (int i = oNodes.Count - 1; i >= 0; i--)
            {
                UltraTreeNode oNode = oNodes[i];
                if (oNode.Tag is Variable)
                {
                    //get variable from existing tag
                    Variable oVar = (Variable)oNode.Tag;
                    Segment oSegment = oVar.Segment;
                    string xName = oVar.Name;

                    //unsubscribe to events - we'll resubscribe below
                    oVar.ValueAssigned -= m_oValueAssignedHandler;
                    oVar.VariableVisited -= m_oVarVisitedHandler;

                    oVar = null;

                    //get new variable
                    oVar = GetDisplayedVariable(oSegment, xName);
                    if (oVar != null)
                    {
                        //update tag
                        oNode.Tag = oVar;

                        //GLOG 2996: If Nodes have been refreshed due to variable actions of oVar
                        //ValueAssigned handler won't get raised because original instance
                        //has been unsubscribed, so update node text here
                        string xUIValue = GetVariableUIValue_API(oVar);
                        if (oNode.Nodes[0].Text != xUIValue)
                            oNode.Nodes[0].Text = xUIValue;

                        //GLOG 7275: If variable action for current TaskPane variable caused Nodes to be refreshed,
                        //make sure ValueChanged control actions are executed, since ValueAssigned handler is currently unsubscribed
                        if (m_oCurNode != null && m_oCurNode.Tag is Variable && ((Variable)m_oCurNode.Tag).TagID == oVar.TagID && ((Variable)m_oCurNode.Tag).Name == oVar.Name)
                        {
                            ExecuteControlActions(oVar, ControlActions.Events.ValueChanged);
                        }

                        //subscribe to variable value assigned events
                        oVar.ValueAssigned += m_oValueAssignedHandler;
                        oVar.VariableVisited += m_oVarVisitedHandler;
                    }
                    else
                        //delete node
                        oNode.Remove();
                }
                else if (oNode.Tag is Block)
                {
                    //get block from existing tag
                    Block oBlock = (Block)oNode.Tag;
                    Segment oSegment = oBlock.Segment;
                    string xName = oBlock.Name;
                    oBlock = null;
                    //get new block
                    oBlock = GetDisplayedBlock(oSegment, xName);
                    if (oBlock != null)
                        //update tag
                        oNode.Tag = oBlock;
                    else
                        //delete node
                        oNode.Remove();
                }
                else if (oNode.Tag is Segment)
                {
                    //GLOG 5319: Update any Child Segments to ensure
                    //TaskPane variables remain linked
                    this.UpdateTreeNodeTags(oNode.Nodes);
                }
            }
        }

        /// <summary>
        /// Returns named variable to be displayed in tree
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        Variable GetDisplayedVariable(Segment oSegment, string xName)
        {
            Variable oVar = null;
            try
            {
                oVar = oSegment.Variables.ItemFromName(xName);
            }
            catch { }
            if (oVar == null)
            {
                //Also search recursively through transparent children
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    if (oSegment.Segments[i].IsTransparent)
                    {
                        oVar = GetDisplayedVariable(oSegment.Segments[i], xName);
                    }
                    if (oVar != null)
                        break;
                }
            }
            return oVar;
        }
        /// <summary>
        /// Returns named block to be displayed in tree
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        Block GetDisplayedBlock(Segment oSegment, string xName)
        {
            Block oBlock = null;
            try
            {
                oBlock = oSegment.Blocks.ItemFromName(xName);
            }
            catch { }
            if (oBlock == null)
            {
                //Also search recursively through transparent children
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    if (oSegment.Segments[i].IsTransparent)
                    {
                        oBlock = GetDisplayedBlock(oSegment.Segments[i], xName);
                    }
                    if (oBlock != null)
                        break;
                }
            }
            return oBlock;
        }
        /// <summary>
        /// executes code that must be run
        /// when the user selects a Word tag
        /// </summary>
        /// <param name="Sel"></param>
        /// <param name="OldXMLNode"></param>
        /// <param name="NewXMLNode"></param>
        /// <param name="Reason"></param>
        internal void OnXMLSelectionChange(Word.XMLNode OldXMLNode, Word.XMLNode NewXMLNode)
        {
            try
            {
                if (m_bExitingEditMode)
                    return;

                DateTime t0 = DateTime.Now;

                //this handler should only run if user is manually moving between nodes in the
                //document or moving from a node in the document to the node of a different
                //variable or block in the tree - exit if just tabbing between controls
                Word.Application oWord = Session.CurrentWordApp;
                if (m_bHandlingTabPress || oWord.ActiveDocument
                    .ActiveWindow.View.Type == Word.WdViewType.wdPrintPreview ||
                    (ForteDocument.IgnoreWordXMLEvents & ForteDocument.WordXMLEvents.XMLSelectionChange) == 
                    ForteDocument.WordXMLEvents.XMLSelectionChange)
                    return;

                //GLOG 4241 (dm) - store screen updating status
                bool bScreenUpdating = oWord.ScreenUpdating;

                //Clean up the temp Callout if currently displayed
                this.HideCallout();

                //correct schema violations, e.g. content in mDels and nested mVars
                bool bRefreshRequired = false;
                LMP.Forte.MSWord.WordDoc.CorrectSchemaViolations(OldXMLNode, NewXMLNode,
                    ref bRefreshRequired);
                if (bRefreshRequired)
                {
                    //invalid tags were deleted - refresh now
                    m_oMPDocument.Refresh(ForteDocument.RefreshOptions
                        .RefreshTagsWithGUIDIndexes);
                }

                //check if we're in the same document and we've exited an mVar or mBlock
                if ((OldXMLNode != null) && (OldXMLNode.Parent == m_oMPDocument.WordDocument) &&
                    LMP.Forte.MSWord.WordDoc.VariableOrBlockExited(OldXMLNode, NewXMLNode))
                {
                    this.TaskPane.ScreenUpdating = false;

                    //set the value of the variable associated
                    //with the old Word tag
                    //GLOG 4132 - set flag to skip update from control
                    UpdateVariableValue(OldXMLNode, true);
                }

                //dispose of any unresolved empty mVars
                try
                {
                    this.ResolveEmptyTags();
                }
                catch { }

                //select the variables tree node whose
                //selection tag is the current Word tag
                if ((!this.ProgrammaticallySelectingWordTag) &&
                    (this.treeDocContents.Nodes.Count > 0))
                {
                    //adjust selection if necessary, e.g. when cursor is at end
                    //of a non-editable mVar such as initials separator - GLOG 2786, 9/5/08
                    if (NewXMLNode != null)
                        LMP.Forte.MSWord.WordDoc.AdjustXMLNodeSelection(ref NewXMLNode);

                    SelectNode(NewXMLNode);
                    //GLOG 5555: DoEvents can cause hang if Navigation Timer is enabled
                    //System.Windows.Forms.Application.DoEvents();
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }

                this.TaskPane.ScreenUpdating = bScreenUpdating;

                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                while (oE.InnerException != null)
                    oE = oE.InnerException;
                //GLOG 6529: Ignore specific error encountered when undoing Insert Footnote/Endnote within XML Tag
                if (oE.Message.Contains("This object model command is not available"))
                {
                    return;
                }
                else if (oE.Message == "Object has been deleted.")
                {
                    //this error will occur if we've failed to detect the manual
                    //deletion of a tag associated with the same variable as OldXMLNode
                    //or NewXMLNode, e.g. when done by holding down the backspace key -
                    //silently refresh tree
                    this.RefreshTree();
                }
                else
                {
                    this.TaskPane.ScreenUpdating = true;
                    LMP.Error.Show(oE);
                }
            }
            finally
            {
                //GLOG 4132 - reset flag
                m_xSkipVarUpdateFromControl = "";
            }
        }
        /// <summary>
        /// shows the specified control, under the specified tree node
        /// </summary>
        internal void ShowTreeControlNEW(System.Windows.Forms.Control oControl, UltraTreeNode oTreeNode, bool bShowCIButton)
        {
            int iControlWidth = 0;

            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                oControl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width - 30;
            else
                iControlWidth = this.treeDocContents.Width - 10;

            if (bShowCIButton)
                iControlWidth = iControlWidth - (this.btnContacts.Width + 1);

            //force tree to redraw so that scroll bar
            //visibility will be reflected in UI
            this.treeDocContents.Refresh();

            //scroll control into view, if necessary
            if (oTreeNode.Bounds.Top + oTreeNode.Bounds.Height + oControl.Height >
                this.treeDocContents.ClientRectangle.Bottom)
            {
                //bottom of control won't be visible -
                //scroll to top of next node, if it exists
                UltraTreeNode oNode = oTreeNode.NextVisibleNode;

                //skip value nodes - value node tags are IControls
                while (oNode.Tag is IControl)
                {
                    if (oNode.NextVisibleNode != null)
                        oNode = oNode.NextVisibleNode;
                    else
                        break;
                }

                oNode.BringIntoView();
            }

            //the user selected a task pane node
            //position control to display over variable value
            oControl.SetBounds(5,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, oControl.Height);

            //Position floating CI button to right of textbox
            if (bShowCIButton)
            {
                this.btnContacts.Left = oControl.Left + oControl.Width + 1;
                this.btnContacts.Top = oControl.Top + 6;
                this.btnContacts.Visible = true;
            }
            //add control to container
            this.treeDocContents.Controls.Add(oControl);

            //ensure that control is visible - controls that
            //have been displayed in the wizard are not visible
            //at this time
            if (!oControl.Visible)
                oControl.Visible = true;

            //clear value node text for display
            oValueNode.Text = "";
        }
        internal void ShowTreeControl(System.Windows.Forms.Control oControl, UltraTreeNode oTreeNode, bool bShowCIButton)
        {
            DateTime t0 = DateTime.Now;

            int iControlWidth = 0;

            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (m_bShowValueNodes)
                oValueNode = oTreeNode.Nodes[0];
            else
                oValueNode = oTreeNode;

            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                oControl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            this.treeDocContents.Refresh();

            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
            System.Windows.Forms.Application.DoEvents();

            //GLOG #8492 - dcf
            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
            {
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - ((int)(20 * Session.ScreenScalingFactor));
                
            }
            else
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - ((int)(5 * Session.ScreenScalingFactor));

            //When TaskPane in Initializing in Word 2003, width may not yet be finalized
            //when Initial control is selected.  Set to arbitrary width to avoid problems
            if (iControlWidth <= 0)
                iControlWidth = 192;
            
            if (bShowCIButton)
                iControlWidth = iControlWidth - (this.btnContacts.Width + 1);


            //scroll control into view, if necessary
            if (oTreeNode.Bounds.Top + oTreeNode.Bounds.Height + oControl.Height > 
                this.treeDocContents.ClientRectangle.Bottom)
            {
                //bottom of control won't be visible -
                //scroll to top of next node, if it exists
                UltraTreeNode oNode = oTreeNode.NextVisibleNode;

                //skip value nodes - value node tags are IControls
                while (oNode.Tag is IControl)
                {
                    if (oNode.NextVisibleNode != null)
                        oNode = oNode.NextVisibleNode;
                    else
                        break;
                }

                oNode.BringIntoView();
            }

            //the user selected a task pane node
            //position control to display over variable value
            oControl.SetBounds(oValueNode.Bounds.Left,
                Math.Max(oTreeNode.Bounds.Top, 0) + oTreeNode.Bounds.Height,
                iControlWidth, oControl.Height);

            //Position floating CI button to right of textbox
            if (bShowCIButton)
            {
                this.btnContacts.Left = oControl.Left + oControl.Width + 1;
                this.btnContacts.Top = oControl.Top + 6;
                this.btnContacts.Visible = true;
            }

            //add control to container
            this.treeDocContents.Controls.Add(oControl);

            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();

            //ensure that control is visible - controls that
            //have been displayed in the wizard are not visible
            //at this time
            if (!oControl.Visible)
                oControl.Visible = true;

            if (m_bShowValueNodes)
            {
                //clear value node text for display
                oValueNode.Text = "";
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// shows the current control in the floating form on the document
        /// </summary>
        internal void ShowFloatingControl()
        {
            try
            {
                if (this.m_oCurCtl == null)
                    return;

                //create floating form
                ControlForm oForm = new ControlForm();

                //get current value so that we can return value on cancel
                string xCurValue = ((IControl)this.m_oCurCtl).Value;

                //add control to floating form
                oForm.Controls.Add(this.m_oCurCtl);

                this.m_oCurCtl.SetBounds(20, 20, 300, this.m_oCurCtl.Height);

                //size floating form to slightly bigger than control
                oForm.Size = new Size(this.m_oCurCtl.Width + 40, this.m_oCurCtl.Height + 80);

                //showing floating form
                oForm.StartPosition = FormStartPosition.CenterScreen;
                oForm.ShowDialog();

                oForm.Controls.Remove(this.m_oCurCtl);
                ShowTreeControl(this.m_oCurCtl, this.m_oCurNode, false);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UIException(
                    LMP.Resources.GetLangString("Error_CouldNotShowFloatingControl"), oE);
            }
        }
        /// <summary>
        /// deletes designated segment and all children
        /// </summary>
        /// <param name="oTagToDelete"></param>
        public void DeleteSegment(Segment oSeg)
        {
            try
            {
                this.TaskPane.ScreenUpdating = false;

                string xMsg = LMP.Resources.GetLangString(
                    "Prompt_DeleteSegmentAndAllContainedChildren");

                //insert segment display name into message
                xMsg = xMsg.Replace("{0}", oSeg.DisplayName);

                DialogResult oRes = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oRes == DialogResult.Yes)
                {
                    this.TaskPane.ScreenUpdating = false;

                    //GLOG item #3479 - if oSeg is the only child of a transparent segment or
                    //pleading collection, switch target to parent
                    while ((!oSeg.IsTopLevel) && (oSeg.Parent.IsTransparent ||
                        (oSeg.Parent is CollectionTable)) && (oSeg.Parent.Segments.Count == 1))
                    {
                        //delete child's node - this won't happen automatically
                        //because delete event will only be raised for parent
                        this.DeleteNode(oSeg.FullTagID);
                        oSeg = oSeg.Parent;
                    }
                    //GLOG 3507
                    m_bDeletingDocumentSegment = oSeg.IsTopLevel && 
                        oSeg.IntendedUse == mpSegmentIntendedUses.AsDocument;
                    //delete segment
                    this.m_oMPDocument.DeleteSegment(oSeg);

                    this.picMenu.Visible = false;

                    //GLOG 3507: If Top-Level Segment has been deleted,
                    //need to refresh tags in case Document Stamp content has also
                    //been deleted
                    if (m_bDeletingDocumentSegment)
                    {
                        m_oMPDocument.Refresh(ForteDocument.RefreshOptions.None);
                        this.RefreshTree(false, true);
                        m_bDeletingDocumentSegment = false;
                    }

                    if (this.treeDocContents.Nodes.Count > 0)
                    {
                        //expand first node - GLOG item #4287 - dcf
                        this.treeDocContents.Nodes[0].Expanded = true;
                    }

                    //Make sure main story is selected
                    LMP.Forte.MSWord.WordDoc.SeekMainStory(m_oMPDocument.WordDocument);
                }
            }
            catch (System.Exception oE)
            {
                //this.TaskPane.Refresh();
                this.TaskPane.ScreenUpdating = true;

                throw new LMP.Exceptions.ServerException(
                    LMP.Resources.GetLangString("Error_CouldNotDeleteSegment"), oE);
            }
            finally
            {
                m_bDeletingDocumentSegment = false;
                this.TaskPane.Refresh();
                this.TaskPane.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// deactivates all of the variables that
        /// are children of the node with the specified id
        /// </summary>
        /// <param name="xTagID"></param>
        public void SetNodeActivation(string xTagID, bool bActivate)
        {
            UltraTreeNode oParentNode = this.GetNodeFromTagID(xTagID);
            SetNodeActivation(oParentNode, bActivate);
        }
        private void SetNodeActivation(UltraTreeNode oNode, bool bActivate)
        {
            for(int i=0; i<oNode.Nodes.Count; i++)
            {
                SetNodeActivation(oNode.Nodes[i], bActivate);
            }

            //if activating, update variable value node
            if (bActivate && (oNode.Tag is Variable))
                oNode.Nodes[0].Text = this.GetVariableUIValue_API((Variable)oNode.Tag);

            if (oNode.Tag is Variable || oNode.Tag is Authors)
                oNode.Enabled = bActivate;
        }

        /// <summary>
        /// updates the value of the variable associated with the selected tag
        /// </summary>
        public void UpdateVariableValueFromSelection()
        {
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                UpdateVariableValueFromSelection_CC();
            else
                UpdateVariableValueFromSelection_XMLTag();
        }
        private void UpdateVariableValueFromSelection_XMLTag()
        {
            if (LMP.Forte.MSWord.WordApp.Version > 14)
                return;
            ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;
            //bool bScreenUpdating = this.TaskPane.ScreenUpdating;
            try
            {
                //GLOG 6515:  Don't adjust ScreenUpdating, as this can cause
                //scroll position to change when called immediately before Save As dialog
                //this.TaskPane.ScreenUpdating = false;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //dispose of any unresolved empty mVars
                try
                {
                    this.ResolveEmptyTags();
                }
                catch { }

                //update value of selected tag
                Word.XMLNode oWordTag = Session.CurrentWordApp.Selection.XMLParentNode;
                if (oWordTag != null)
                {
                    string xElement = oWordTag.BaseName;
                    if ((xElement == "mVar") || (xElement == "mSubVar") ||
                        (xElement == "mDel"))
                        this.UpdateVariableValue(oWordTag);
                }
            }
            finally
            {
                //this.TaskPane.ScreenUpdating = bScreenUpdating;
                ForteDocument.IgnoreWordXMLEvents = iEvents;
            }
        }

        /// <summary>
        /// updates the value of the variable associated with the selected tag
        /// </summary>
        private void UpdateVariableValueFromSelection_CC()
        {
            ForteDocument.WordXMLEvents iEvents = ForteDocument.IgnoreWordXMLEvents;

            //GLOG 6515:  Don't adjust ScreenUpdating, as this can cause
            //scroll position to change when called immediately before Save As dialog
            //bool bScreenUpdating = this.TaskPane.ScreenUpdating;
            try
            {
                //this.TaskPane.ScreenUpdating = false;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //dispose of any unresolved empty mVars
                try
                {
                    this.ResolveEmptyTags();
                }
                catch { }

                //update value of selected tag
                Word.ContentControl oCC = Session.CurrentWordApp.Selection.ParentContentControl;
                if (oCC != null)
                {
                    string xElement = String.GetBoundingObjectBaseName(oCC.Tag);
                    if ((xElement == "mVar") || (xElement == "mSubVar") ||
                        (xElement == "mDel"))
                        this.UpdateVariableValue(oCC);
                }
            }
            finally
            {
                //this.TaskPane.ScreenUpdating = bScreenUpdating;
                ForteDocument.IgnoreWordXMLEvents = iEvents;
            }
        }
        #endregion
        #region *********************private functions*********************
        /// <summary>
        /// shows the context menu button
        /// </summary>
        /// <param name="oNode"></param>
        //private void ShowContextMenuButton(UltraTreeNode oNode)
        //{
        //    // GLOG : 1908 : JAB
        //    // Limit the location left coordinate so that it does not
        //    // overlay the scrollbar.
        //    //positions dropdown to left of folder
        //    this.picMenu.Visible = false;

        //    this.picMenu.Left = this.Left + this.treeDocContents.Left + oNode.Bounds.X + 4;
        //    this.picMenu.Top = oNode.Bounds.Y + 33;
        //    this.picMenu.Visible = true;
        //}
        private void ShowContextMenuButton(UltraTreeNode oNode)
        {
            // GLOG : 1908 : JAB
            // Limit the location left coordinate so that it does not
            // overlay the scrollbar.
            int iMargin = IsTreeContentVerticalScrollbarVisible ? 20 : 2;
            int iMaxLeft = this.treeDocContents.Right - iMargin - this.picMenu.Width;
            this.picMenu.Left = oNode.Bounds.X + oNode.Bounds.Width + 4;

            if (picMenu.Left > iMaxLeft)
            {
                picMenu.Left = iMaxLeft;
            }

            this.picMenu.Top = oNode.Bounds.Y + 11;
            this.picMenu.Visible = true;
        }
        private string GetEmptyValueDisplay(){
            if(m_xEmptyDisplayValue == null)
                m_xEmptyDisplayValue = LMP.Resources.GetLangString("Msg_EmptyValueDisplayValue");

            return m_xEmptyDisplayValue;
        }
        private void GetContacts(object sender, EventArgs e)
        {
            try
            {
                //get contact detail for active segment
                object oTag = this.treeDocContents.ActiveNode.Tag;
                if (oTag == null || oTag is Jurisdictions)
                {
                    //Current node doesn't have a tag - use toplevel node
                    oTag = this.treeDocContents.ActiveNode.RootNode.Tag;
                }
                Segment oSeg = null;
                Variable oVar = null;

                try
                {
                    if (oTag is Segment)
                        oSeg = (Segment)oTag;
                    else if (oTag is Variable)
                    {
                        oVar = (Variable)oTag;
                        oSeg = oVar.Segment;
                    }
                    else if (oTag is Block)
                        oSeg = ((Block)oTag).Segment;
                    else if (oTag is IControl)
                    {
                        oVar = (Variable)this.treeDocContents.ActiveNode.Parent.Tag;
                        oSeg = oVar.Segment;
                    }
                }
                catch 
                {
                    //for testing 
                    Trace.WriteInfo("m_oCurNode = " + m_oCurNode.Text);
                    Trace.WriteInfo("sender = " + sender.ToString());
                    if (oTag != null)
                        Trace.WriteInfo("oTag is " + oTag.ToString());
                    if (oVar != null)
                        Trace.WriteInfo("oVar is " + oVar.Name);
                    if (oSeg != null)
                        Trace.WriteInfo("oSeg is " + oSeg.DisplayName);
                }

                //GLOG : 4557 : CEH
                //Get variable name for later use
                //string xVarName = oVar.Name;
                string xKey = this.m_oCurNode.Key;

                //GLOG 2704: If current Chooser segment is not ci-enabled, use top-level segment
                if (oSeg == null || oSeg.Variables.CIEnabledVariables.Count == 0)
                    oSeg = m_oMPDocument.Segments[0];

                if (oVar != null && oVar.RequestCIForEntireSegment)
                {
                    //Make sure variables from parent segments also included
                    while (oSeg.Parent != null)
                        oSeg = oSeg.Parent;
                }
                m_bCIIsActive = true;

                LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                    this.m_oMPDocument.WordDocument);
                oEnv.SaveState();

                //If CI was called from a control, make sure 
                //variable matches current control value
                if (sender is IControl && !(sender is LMP.Controls.TreeView) && ((IControl)sender).IsDirty)
                {
                    TaskPane.ScreenUpdating = false;
                    oVar.SetValue(((IControl)sender).Value);
                    TaskPane.ScreenUpdating = true;

                    //GLOG 2645
                    oVar.SetRuntimeControlValues(((IControl)m_oCurCtl).SupportingValues);
                }
                else
                    this.TaskPane.ScreenUpdating = true;

                oEnv.RestoreState();

                //subscribe to event that let's us know
                //when the CI dialog has been released
                CIDialogReleasedHandler oHandler = new CIDialogReleasedHandler(OnCIDialogRelease);
                oSeg.CIDialogReleased += oHandler;

                //get contacts
                if (oVar != null && oVar.CIContactType != 0 && !oVar.RequestCIForEntireSegment)
                    //Populate only selected variable
                    oSeg.GetContacts(oVar);
                else
                    //Populate all CI-enabled variables
                    oSeg.GetContacts();

                oSeg.CIDialogReleased -= oHandler;

                //If CI was called from a control, update that control's value
                if (sender is IControl  && !(sender is LMP.Controls.TreeView) && oVar != null)
                {
                    IControl oICtl = ((IControl)sender);
                    if (oVar.Value != oICtl.Value)
                    {
                        oICtl.IsDirty = true;
                        oICtl.Value = oVar.Value;
                    }
                    
                    //GLOG : 4557 : CEH
                    //Tree might have been refreshed by variable action
                    //Variable oTempVar = oSeg.Variables.ItemFromName(xVarName);
                    //this.SelectNode(GetNodeFromTagID(oTempVar.Segment.TagID + "." + oTempVar.ID), false);

                    //GLOG item #4584 - dcf
                    //fix for GLOG item #4557 caused this item
                    UltraTreeNode oSelNode = this.treeDocContents.GetNodeByKey(xKey);
                    this.SelectNode(oSelNode);
                    //GLOG 5211: Make sure focus remains in original control
                    if (m_oCurCtl != null)
                        m_oCurCtl.Focus();
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
                this.TaskPane.ScreenRefresh();
                m_bCIIsActive = false;
            }
        }
        /// <summary>
        /// shows the appropriate context menu for the specified point
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void ShowContextMenu(int x, int y)
        {
            //Make sure any current control changes are saved
            //this.ExitEditMode();
            UltraTreeNode oNode = this.treeDocContents.GetNodeFromPoint(x, y);
            if (oNode == null)
                return;

            this.SelectNode(oNode);

            if (oNode.Tag is Segment)
            {
                m_oCurNode = oNode;
                try
                {
                    this.treeDocContents.ActiveNode = oNode;
                }
                catch
                {
                    //GLOG 6829 (dm) - oNode will no longer exist if SelectNode
                    //discovered that primary bookmark was missing
                    return;
                }

                //GLOG 6829 (dm) - refresh if any other mSEG bookmarks are missing
                Segment oSegment = (Segment)oNode.Tag;
                if (oSegment.ContentIsMissing)
                {
                    //refresh
                    this.RefreshTree();

                    //use refreshed segment to ensure appropriate menu items
                    oSegment = this.ForteDocument.FindSegment(oSegment.FullTagID);

                    //exit if this segment is missing
                    if (oSegment == null)
                        return;
                }

                this.CreateSegmentMenu(oSegment);
                m_oMenuSegment.Show(this.treeDocContents, x, y);
            }
            else if (oNode.Tag is Variable)
            {
                m_oCurNode = oNode;
                this.treeDocContents.ActiveNode = oNode;
                this.CreateVariableMenu((Variable)oNode.Tag);
                // Use the node's location to position the context menu since the node position can change
                // when display a large control.
                this.m_oMenuVariable.Show(this.treeDocContents, x, oNode.Bounds.Top);
            }
            else if (oNode.Tag is Block)
            {
                m_oCurNode = oNode;
                this.treeDocContents.ActiveNode = oNode;
                this.CreateBlockMenu((Block)oNode.Tag);
                this.m_oMenuBlock.Show(this.treeDocContents, x, y);
            }
            // GLOG : 3135 : JAB
            // Show the menu for authors.
            else if (oNode.Tag is Authors)
            {
                m_oCurNode = oNode;
                this.treeDocContents.ActiveNode = oNode;
                this.CreateAuthorsMenu((Authors)oNode.Tag);
                this.m_oMenuAuthors.Show(this.treeDocContents, x, y);
            }
            // GLOG : 5398 : CEH
            // Show the menu for Jurisdictions
            else if (oNode.Tag is Jurisdictions)
            {
                this.treeDocContents.ActiveNode = oNode;
                this.CreateJurisdictionsMenu((Jurisdictions)oNode.Tag);
                this.m_oMenuJurisdictions.Show(this.treeDocContents, x, y);
            }

        }
        private void OnCIDialogRelease(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();
                this.TaskPane.ScreenUpdating = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        public static void SaveSegmentWithData(Segment oSegment)
        {
            try
            {
                //GLOG item #6250 - dcf - 4/17/13
                if (!AdminSegmentDefs.Exists(oSegment.Name) && !UserSegmentDefs.Exists(LMP.Data.Application.User.ID, oSegment.ID))
                {
                    //original segment no longer exists - alert
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Error_InvalidSegmentIDCantExecuteFeature"), oSegment.ID),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return;
                }

                UserFolders oFolders = new UserFolders(Session.CurrentUser.ID);
                // Can't create Prefill if no User Folders exist
                //GLOG : 8152 : JSW
                //let ContentPropertyForm handle folder creation messages
                //if (oFolders.Count == 0)
                //{
                //    MessageBox.Show(LMP.Resources.GetLangString("Msg_PrefillRequiresUserFolder"), 
                //        LMP.ComponentProperties.ProductName,
                //        MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}

                User oUser = Session.CurrentUser;
                int iOwnedSegments = oUser.NumberOfOwnedSegments;
                int iLimit = oUser.OwnedSegmentLimit;

                // GLOG : 3138 : JAB
                // Check if the user has exceeded the maximum number of segments allowed.
                if ((iOwnedSegments >= iLimit))
                {
                    // The user has exceeded the maximum allowed segments. Notify the user
                    // and do not create new content.
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_UserContentLimitReached"),
                        iLimit), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                // GLOG : 3138 : JAB
                // Check if the number of user content requires warning the user that
                // they are approaching the maximum user content limit.
                if (iOwnedSegments >= oUser.OwnedSegmentThreshold)
                {
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_ApproachingUserContentLimit"),
                        iOwnedSegments, iLimit), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                //5-30-11 (dm) - activate if necessary - disallow if this segment or any
                //child is non-activatable - we don't want to create content that will
                //need to be deactivated on insertion
                bool bFailed = false;
                oSegment.ActivateIfNecessary(ref bFailed);
                if (bFailed)
                {
                    string xMsg = "Prompt_FunctionNotAvailableBecauseSegmentCannotBeActivated";
                    MessageBox.Show(LMP.Resources.GetLangString(xMsg), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                ContentPropertyForm oForm = new ContentPropertyForm();

                //get mode to display dialog - either for a DocumentSegment
                //or a TextBlockSegment
                ContentPropertyForm.DialogMode oMode = ContentPropertyForm.DialogMode.SavedContent;

                // Display Segment Creator form
                //oForm.ShowForm(this.ParentForm, null, Form.MousePosition, oMode, null, oSegment);
                oForm.ShowForm(null, null, Form.MousePosition, oMode, null, oSegment);

                if (oForm.Canceled)
                {
                    //GLOG : 8000 : ceh - set focus back to the document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                    return;
                }
                else
                    System.Windows.Forms.Application.DoEvents();

                UserSegmentDef oDef = (UserSegmentDef)oForm.NewSegmentDef;

                LMP.MacPac.Application.ScreenUpdating = false;

                //GLOG 6799: Temporarily insert Bounding Objects to include as part of Segment XML
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                Word.Document oWordDoc = oSegment.ForteDocument.WordDocument;
                LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();

                //GLOG 7706 (dm) - preserve primary bookmark - it may shift slightly
                //after adding and deleting bounding objects
                int iStart = oSegment.PrimaryRange.Start;
                int iEnd = oSegment.PrimaryRange.End;

                bool bCollectionBmkDeleted = false;
                string xCollectionBmkName = "";
                if (oSegment is CollectionTableItem)
                {
                    //GLOG 6920: Remove Collection Table bookmark so it will not be saved as part of saved XML
                    if (oSegment.Parent != null && oSegment.Parent is CollectionTable)
                    {
                        xCollectionBmkName = oSegment.Parent.PrimaryBookmark.Name;
                        object oBmkName = xCollectionBmkName;
                        oSegment.ForteDocument.WordDocument.Bookmarks.get_Item(ref oBmkName).Delete();
                        bCollectionBmkDeleted = true;
                    }
                }
                if (oSegment.ForteDocument.FileFormat == mpFileFormats.Binary)
                {
                    //2-2-11 (dm) - add bookmarks before getting segment xml
                    oSegment.ForteDocument.Tags.AddBookmarks();
                    oConvert.AddTagsToBookmarkedDocument(oWordDoc, false, false,
                        true, false, true);
                }
                else
                {
                    oConvert.AddContentControlsToBookmarkedDocument(oWordDoc, false, true, false, true);
                }
                if (bCollectionBmkDeleted)
                {
                    //GLOG 6920: Reinsert bookmark to avoid error when encrypting tags
                    object oRange = oSegment.PrimaryRange.Tables[1].Range;
                    oSegment.PrimaryRange.Tables[1].Range.Bookmarks.Add(xCollectionBmkName, ref oRange);
                }

                //prevent compressed attributes from getting into database -
                //check first if attributes are currently compressed
                bool bDisableEncryption = (LMP.Registry.GetMacPac10Value("DisableEncryption").ToLower() == "miller" &&
                    LMP.Data.Application.DisableEncryptionPassword.ToLower() == "fisherman") ||
                    LMP.Data.Application.EncryptionPassword == "";

                if (!bDisableEncryption)
                {
                    //document contains compressed object data -
                    //decompress Word tag attributes
                    ForteDocument oMPDoc = oSegment.ForteDocument;

                    if (oMPDoc.FileFormat == LMP.Data.mpFileFormats.Binary)
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression(oMPDoc.Tags, false, "");
                    else
                        //10.2 (dm) - use new data structure
                        LMP.Forte.MSWord.WordDoc.SetDocumentCompression_CC(oMPDoc.Tags, false, "");

                    //prevent further compression during save
                    LMP.Forte.MSWord.Application.SuspendCompression(true);
                }
                //GLOG 6920: Remove collection bookmark again
                if (bCollectionBmkDeleted)
                {
                    object oBmkName = xCollectionBmkName;
                    oSegment.ForteDocument.WordDocument.Bookmarks.get_Item(ref oBmkName).Delete();
                }

                //GLOG 3147: Don't save body with content
                //GLOG 3340: Remove Trailer before saving content
                string xSegmentXML = GetSegmentXML(oSegment, false, true);

                //GLOG 6799: Remove Bounding objects again
                LMP.Forte.MSWord.WordDoc.DeleteSegmentBoundingObjects(oWordDoc, false, true); //GLOG 7306 (dm)
                if (bCollectionBmkDeleted)
                {
                    //GLOG 6920: Recreate collection bookmark last time
                    object oRange = oSegment.PrimaryRange.Tables[1].Range;
                    oSegment.PrimaryRange.Tables[1].Range.Bookmarks.Add(xCollectionBmkName, ref oRange);
                }

                //GLOG 7706 (dm) - ensure that primary bookmark spans original range
                Word.Range oSegRange = oSegment.PrimaryRange;
                oSegRange.SetRange(iStart, iEnd);
                object oSegRangeObject = oSegRange;
                oSegRange.Bookmarks.Add(oSegment.PrimaryBookmark.Name, ref oSegRangeObject);

                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;

                //create user segment
                UserSegmentDefs oUserSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, Session.CurrentUser.ID);
                UserSegmentDef oUserSeg = (UserSegmentDef)oForm.NewSegmentDef;

                //add RecreateRedirectID to xml - this is the ID of the
                //saved content wrapper segment - on recreate, if the
                //redirect ID is not empty, the xml of the wrapper segment
                //will be inserted if possible.
                string xSegmentID = oSegment.ID;
                if (xSegmentID.EndsWith(".0"))
                    xSegmentID = xSegmentID.Substring(0, xSegmentID.Length - 2);
                xSegmentXML = xSegmentXML.Replace("SegmentID=" + xSegmentID + "|",
                    "SegmentID=" + xSegmentID + "|RecreateRedirectID=" + oUserSeg.ID + "|");

                oUserSeg.XML = xSegmentXML;
                oUserSeg.ChildSegmentIDs = oSegment.ID;

                //GLOG item #5987 - dcf
                //oUserSeg.DefaultDoubleClickLocation = (short)Segment.InsertionLocations.InsertInNewDocument;
                //oUserSeg.DefaultDragLocation = (short)Segment.InsertionLocations.InsertInNewDocument;
                //oUserSeg.MenuInsertionOptions = (short)Segment.InsertionLocations.InsertInNewDocument;
                //oUserSeg.IntendedUse = mpSegmentIntendedUses.AsDocument;
                oUserSeg.DefaultDoubleClickLocation = oSegment.Definition.DefaultDoubleClickLocation;
                oUserSeg.DefaultDragLocation = oSegment.Definition.DefaultDragLocation;
                oUserSeg.MenuInsertionOptions = oSegment.Definition.MenuInsertionOptions;
                oUserSeg.DefaultDoubleClickBehavior = oSegment.Definition.DefaultDragBehavior;
                oUserSeg.DefaultDragBehavior = oSegment.Definition.DefaultDragBehavior;
                oUserSeg.IntendedUse = mpSegmentIntendedUses.AsDocument;
                oUserSeg.TypeID = mpObjectTypes.SavedContent;
                oUserSeg.OwnerID = Session.CurrentUser.ID;
                oUserSegs.Save((StringIDSimpleDataItem)oUserSeg);

                //GLOG - 3331 - CEH
                //GLOG 4372: Alternate method used here, since previous method did not work for subfolders
                int iFolderID1;
                int iFolderID2;
                LMP.Data.Application.SplitID(oForm.FolderID, out iFolderID1, out iFolderID2);
                FolderMembers oFolderMembers = new FolderMembers(iFolderID1, iFolderID2);

                //check FolderMembers directly for duplicate
                for (int i = 1; i <= oFolderMembers.Count; i++)
                {
                    string xMemberName = ((FolderMember)oFolderMembers[i]).Name;
                    if (xMemberName.ToUpper() == oUserSeg.DisplayName.ToUpper())
                        return;
                }

                //no duplicate, create new folder member
                int iID1;
                int iID2;
                LMP.String.SplitID(oUserSeg.ID, out iID1, out iID2);
                FolderMember oMember = (FolderMember)(oFolderMembers.Create()); //GLOG 4372
                oMember.ObjectID1 = iID1;
                oMember.ObjectID2 = iID2;
                oMember.ObjectTypeID = mpFolderMemberTypes.UserSegment;
                oFolderMembers.Save((StringIDSimpleDataItem)oMember); //GLOG 4372
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                LMP.MacPac.Application.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// updates the value of the variable 
        /// associated with the specified Word tag
        /// </summary>
        /// <param name="oWordTag"></param>
        private void UpdateVariableValue(Word.XMLNode oWordTag)
        {
            this.UpdateVariableValue(oWordTag, false);
        }

        /// <summary>
        /// updates the value of the variable 
        /// associated with the specified Word Content Control
        /// </summary>
        /// <param name="oWordTag"></param>
        private void UpdateVariableValue(Word.ContentControl oCC)
        {
            this.UpdateVariableValue(oCC, false);
        }

        /// <summary>
        /// updates the value of the variable 
        /// associated with the specified Word tag
        /// </summary>
        /// <param name="oWordTag"></param>
        private void UpdateVariableValue(Word.XMLNode oWordTag,
            bool bFlagToSkipUpdateFromControl)
        {
            Word.XMLNode oVarTag = null;
            Word.XMLNode oBlockTag = null;
            string xElement = "";

            if (oWordTag != null)
            {
                //get base name
                xElement = oWordTag.BaseName;

                if (xElement == "mVar")
                    //set target to specified tag
                    oVarTag = oWordTag;
                else if ((xElement == "mSubVar") || (xElement == "mDel"))
                {
                    //for mSubVars, we're actually interested in containing mVar -
                    //keep checking parent node until an mVar is found
                    oVarTag = oWordTag.ParentNode;
                    while ((oVarTag != null) && (oVarTag.BaseName != "mVar"))
                        oVarTag = oVarTag.ParentNode;
                    if (oVarTag == null)
                        //no containing mVar
                        return;
                }
                else if (xElement == "mBlock")
                {
                    oBlockTag = oWordTag;
                }
                else
                    //this is not a variable tag
                    return;
            }
            else
                //no tag specified
                return;

            //get containing mSEG - keep checking parent node until an mSEG is found
            //GLOG 6776: Use Segment Bookmarks
            Word.Bookmark oSegBmk = null;

            if (oVarTag != null)
                oSegBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oVarTag.Range);
            else if (oBlockTag != null) //GLOG 6850
                oSegBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oBlockTag.Range);
            else
                return;
            //get segment
            Segment oSegment = null;
            if (oSegBmk != null)
            {
                string xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oSegBmk);
                if (!System.String.IsNullOrEmpty(xSegFullTagID))
                    oSegment = this.m_oMPDocument.FindSegment(xSegFullTagID);


            }

            //JTS 6/27/11:  Trailer aren't displayed in TaskPane, so don't attempt to update anything
            //GLOG 7103 (dm) - not all segments are displayed in toolkit mode
			//GLOG : 7998 : ceh
            if (oSegment == null || oSegment.TypeID == mpObjectTypes.Trailer ||
                ((LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator) &&
                !LMP.Data.Application.DisplayInToolkitTaskPane(oSegment.TypeID)))
                return;

            if (oVarTag != null)
            {
                //get variable
                Variables oVariables = null;
                //if (oSegment != null)
                oVariables = oSegment.Variables;
                //else
                //    //standalone
                //    oVariables = m_oMPDocument.Variables;

                //get the variable whose selection tag is the currently selected node
                Variable oVar = oVariables.ItemFromAssociatedTag(oVarTag);

                if (oVar != null)
                {
                    bool bUpdate = false;
                    Word.XMLNode[] oAssociatedTags = oVar.AssociatedWordTags;

                    //get the variable to update - if the ui for the selected node
                    //is another variable's control, e.g. detail grid that populates
                    //multiple variables, it's the ui source variable that we want
                    Variable oUISourceVar = oVar.UISourceVariable;

                    //GLOG 4365 (dm) - non-detail variables with a different ui source
                    //should only be targeting the ui source variable to update the ui
                    Variable oWordSourceVar = null;
                    if (oVar.IsDetailSubComponent)
                        //6-27-11 (dm) - added new variable property based on this conditional
                        oWordSourceVar = oUISourceVar;
                    else
                        oWordSourceVar = oVar;

                    //GLOG 968 (dm) - added the second condition below to ensure
                    //proper updating of single value detail variables
                    if ((oWordSourceVar != oVar) && oVar.IsMultiValue)
                        oVarTag = null;

                    //determine whether update is necessary
                    bool bSingleValueMultiTag =
                        ((!oVar.IsMultiValue) && (oAssociatedTags.Length > 1));
                    string xValueFromSource = oWordSourceVar.GetValueFromSource(oVarTag);
                    //GLOG 4365 (dm) - modified the following conditional to ensure that the
                    //comparison is to the variable's internal value - we were previously
                    //comparing source to source when the variable was uninitialized,
                    //e.g. following a refresh
                    if (xValueFromSource != oWordSourceVar.ValueNotFromSource)
                    {
                        //ui value does not match value of exited node
                        if (bSingleValueMultiTag)
                            //GLOG 3457 (dm) - we don't want to empty all associated
                            //tags when one is manually emptied
                            bUpdate = !System.String.IsNullOrEmpty(xValueFromSource);
                        else
                            bUpdate = true;
                    }
                    else if (bSingleValueMultiTag)
                    {
                        //update if tag is single value and first non-primary tag in
                        //collection does not match ui value - this branch is necessary
                        //because manual edit of primary tag will update ui when mouse
                        //moves to the task pane, but will not execute actions to update
                        //associated tags (because this was causing Word to freeze) -
                        //GLOG 2661, 10/21/08, dm
                        string xValueFromTag2 = oWordSourceVar.GetValueFromSource(oAssociatedTags[1]);
                        bUpdate = (xValueFromSource != xValueFromTag2);
                    }

                    if (bUpdate)
                    {
                        //user has modified the value in the document
                        if (bSingleValueMultiTag)
                        {
                            //TODO: prompt user for whether they want to create a new variable
                            //or apply new value to all associated tags - if they choose the former,
                            //call method to create a new variable and exit this method
                        }

                        //GLOG 6529: Footnote or Endnote has been inserted within XML Tag
                        //GLOG 8105: Only check if in Main story
                        //GLOG 8217: Don't check range if oVarTag is null
                        if (oVarTag != null && oVarTag.Range.StoryType == Word.WdStoryType.wdMainTextStory && (oVarTag.Range.Footnotes.Count > 0 || oVarTag.Range.Endnotes.Count > 0))
                        {
                            if (MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_FootnoteWithinVariableRange"), oWordSourceVar.Name),
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                oWordSourceVar.Segment.Variables.Delete(oWordSourceVar.Name);
                                return;
                            }
                        }
                        LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                            this.m_oMPDocument.WordDocument);
                        oEnv.SaveState();

                        try
                        {
                            //force a refresh of the variable value -
                            //modified for GLOG 2661, 10/21/08, dm
                            oWordSourceVar.SetValue(xValueFromSource, false);
                            oWordSourceVar.VariableActions.Execute();

                            //refresh tree
                            if ((oUISourceVar.DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                                Variable.ControlHosts.DocumentEditor)
                            {
                                //modified for GLOG 4365 (dm)
                                string xKey = oUISourceVar.Segment.FullTagID + "." +
                                    oUISourceVar.ID;
                                this.SetVariablesNodeValue(xKey, oUISourceVar);

                                //GLOG 4132 (dm) - flag variable so as not to lose manual
                                //edits when control loses focus
                                if (bFlagToSkipUpdateFromControl)
                                    m_xSkipVarUpdateFromControl = xKey;
                            }
                        }
                        finally
                        {
                            //GLOG 5669: Don't seek Main Story, since selection may have been
                            //in header or footer
                            oEnv.RestoreState(true, false, false);
                        }
                    }
                }
            }
            else if (oBlockTag != null)
            {
                //Update UI Text for block
                Block oBlock = null;
                Blocks oBlocks = null;
                //if (oSegment != null)
                oBlocks = oSegment.Blocks;
                //else
                //    //standalone
                //    oBlocks = m_oMPDocument.Blocks;

                //get the Block whose selection tag is the currently selected node
                oBlock = oBlocks.ItemFromAssociatedTag(oBlockTag);
                if (oBlock != null && oBlock.ShowInTree)
                {

                    UltraTreeNode oBlockNode = this.GetNodeFromTagID(oBlock.TagID);
                    //get current value node
                    UltraTreeNode oCurValueNode = oBlockNode.Nodes[0];

                    if (oCurValueNode == null)
                    {
                        //something is wrong - all variable and author
                        //nodes need to have a child value node
                        throw new LMP.Exceptions.UINodeException(
                            LMP.Resources.GetLangString("Error_MissingValueNode") +
                            this.m_oCurNode.FullPath);
                    }

                    //set node text to variable value
                    string xText = LMP.Forte.MSWord.WordDoc.GetRangeStartText(
                        oBlock.AssociatedWordTag.Range, 20);

                    xText = xText.Replace("\v", "...");
                    xText = xText.Replace("\r\n", "...");
                    xText = xText.Replace("\n", "...");

                    oCurValueNode.Text = xText;

                    //force a redraw
                    this.treeDocContents.Refresh();
                }
            }
        }

        /// <summary>
        /// updates the value of the variable 
        /// associated with the specified Word tag
        /// </summary>
        /// <param name="oWordTag"></param>
        private void UpdateVariableValue(Word.ContentControl oCC,
            bool bFlagToSkipUpdateFromControl)
        {
            Word.ContentControl oVarCC = null;
            Word.ContentControl oBlockCC = null;
            string xElement = "";

            if (oCC != null)
            {
                //get base name
                xElement = String.GetBoundingObjectBaseName(oCC.Tag);

                if (xElement == "mVar")
                    //set target to specified cc
                    oVarCC = oCC;
                else if ((xElement == "mSubVar") || (xElement == "mDel"))
                {
                    //for mSubVars, we're actually interested in containing mVar -
                    //keep checking parent node until an mVar is found
                    oVarCC = oCC.ParentContentControl;
                    while ((oVarCC != null) && (String.GetBoundingObjectBaseName(oVarCC.Tag) != "mVar"))
                        oVarCC = oVarCC.ParentContentControl;
                    if (oVarCC == null)
                        //no containing mVar
                        return;
                }
                else if (xElement == "mBlock")
                {
                    oBlockCC = oCC;
                }
                else
                    //this is not a variable cc
                    return;
            }
            else
                //no cc specified
                return;

            //get containing mSEG - keep checking parent node until an mSEG is found
            //GLOG 6776: Use Segment Bookmarks
            Word.Bookmark oSegBmk = null;
            if (oVarCC != null)
                oSegBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oVarCC.Range);
            else
                oSegBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oBlockCC.Range);

            //get segment
            Segment oSegment = null;
            if (oSegBmk != null)
            {
                string xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oSegBmk);
                if (!System.String.IsNullOrEmpty(xSegFullTagID))
                    oSegment = this.m_oMPDocument.FindSegment(xSegFullTagID);
            }

            //JTS 6/27/11:  Trailer aren't displayed in TaskPane, so don't attempt to update anything
            //GLOG 7103 (dm) - not all segments are displayed in toolkit mode
            if (oSegment == null || oSegment.TypeID == mpObjectTypes.Trailer ||
                ((LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator) &&
                !LMP.Data.Application.DisplayInToolkitTaskPane(oSegment.TypeID)))
                return;

            if (oVarCC != null)
            {
                //get variable
                Variables oVariables = null;
                oVariables = oSegment.Variables;

                //get the variable whose selection cc is the currently selected node
                Variable oVar = oVariables.ItemFromAssociatedContentControl(oVarCC);

                if (oVar != null)
                {
                    bool bUpdate = false;
                    Word.ContentControl[] oAssociatedCCs = oVar.AssociatedContentControls;

                    //get the variable to update - if the ui for the selected node
                    //is another variable's control, e.g. detail grid that populates
                    //multiple variables, it's the ui source variable that we want
                    Variable oUISourceVar = oVar.UISourceVariable;

                    //GLOG 4365 (dm) - non-detail variables with a different ui source
                    //should only be targeting the ui source variable to update the ui
                    Variable oWordSourceVar = null;
                    if (((oVar.ValueSourceExpression == "[DetailValue]") ||
                        (oVar.ValueSourceExpression == "[DetailValue]" + "__" + oVar.Name)) &&
                        (oVar.ControlType != mpControlTypes.DetailGrid) &&
                        (oVar.ControlType != mpControlTypes.DetailList))
                        oWordSourceVar = oUISourceVar;
                    else
                        oWordSourceVar = oVar;

                    //GLOG 968 (dm) - added the second condition below to ensure
                    //proper updating of single value detail variables
                    if ((oWordSourceVar != oVar) && oVar.IsMultiValue)
                        oVarCC = null;

                    //determine whether update is necessary
                    bool bSingleValueMultiTag =
                        ((!oVar.IsMultiValue) && (oAssociatedCCs.Length > 1));
                    string xValueFromSource = oWordSourceVar.GetValueFromSource_CC(oVarCC);
                    //GLOG 4365 (dm) - modified the following conditional to ensure that the
                    //comparison is to the variable's internal value - we were previously
                    //comparing source to source when the variable was uninitialized,
                    //e.g. following a refresh
                    string xValueNotFromSource = oWordSourceVar.ValueNotFromSource; //GLOG 8179

                    //GLOG 5275:  If True/False value, make sure comparison is not case sensitive
                    //GLOG 8179 (also alternate fix for 7412 and 7645): Make sure variable actions are not run for unitialized variables
                    if (xValueNotFromSource != null && xValueFromSource != xValueNotFromSource && 
                           !(xValueFromSource.ToUpper() == "FALSE" && xValueNotFromSource.ToUpper() == "FALSE") &&
                           !(xValueFromSource.ToUpper() == "TRUE" && xValueNotFromSource.ToUpper() == "TRUE"))
                    {
                        //ui value does not match value of exited node
                        if (oVar.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT_"))
                        {
                            //GLOG 5054: if day of month is 2 digits, evaluated format may not
                            //match control: there's no way to know if August 20, 2010 is the 
                            //result of 'MMMM d, yyyy' or 'MMMM dd, yyyy'.  If strings resulting
                            //from ValueFromSource and ValueNotFromSource are identical,
                            //don't force another update of Variable value
                            DateTime oTest = DateTime.Now;
                            string xTest1 = "";
                            string xTest2 = "";
                            try
                            {
                                xTest1 = oTest.ToString(xValueFromSource);
                            }
                            catch { }
                            try
                            {
                                xTest2 = oTest.ToString(oVar.ValueNotFromSource);
                            }
                            catch { }
                            if (xTest1 != xTest2)
                                bUpdate = true;
                        }
                        else if (bSingleValueMultiTag)
                            //GLOG 3457 (dm) - we don't want to empty all associated
                            //ccs when one is manually emptied
                            bUpdate = !System.String.IsNullOrEmpty(xValueFromSource);
                        else
                            bUpdate = true;
                    }
                    else if (bSingleValueMultiTag)
                    {
                        //update if cc is single value and first non-primary cc in
                        //collection does not match ui value - this branch is necessary
                        //because manual edit of primary cc will update ui when mouse
                        //moves to the task pane, but will not execute actions to update
                        //associated ccs (because this was causing Word to freeze) -
                        //GLOG 2661, 10/21/08, dm
                        string xValueFromTag2 = oWordSourceVar.GetValueFromSource_CC(oAssociatedCCs[1]);
                        bUpdate = (xValueFromSource != xValueFromTag2);
                    }

                    if (bUpdate)
                    {
                        //user has modified the value in the document
                        if (bSingleValueMultiTag)
                        {
                            //TODO: prompt user for whether they want to create a new variable
                            //or apply new value to all associated ccs - if they choose the former,
                            //call method to create a new variable and exit this method
                        }

                        LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                            this.m_oMPDocument.WordDocument);
                        oEnv.SaveState();

                        try
                        {
                            //GLOG 6529: Footnote or Endnote has been inserted within content control
                            //GLOG 8105: Only check if in Main story
                            //GLOG 8217: Don't check range if oVarCC is null
                            if (oVarCC != null && oVarCC.Range.StoryType == Word.WdStoryType.wdMainTextStory && (oVarCC.Range.Footnotes.Count > 0 || oVarCC.Range.Endnotes.Count > 0))
                            {
                                if (MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_FootnoteWithinVariableRange"), oWordSourceVar.Name), 
                                    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    oWordSourceVar.Segment.Variables.Delete(oWordSourceVar.Name);
                                    return;
                                }
                            }
                            //force a refresh of the variable value -
                            //modified for GLOG 2661, 10/21/08, dm
                            oWordSourceVar.SetValue(xValueFromSource, false);
                            oWordSourceVar.VariableActions.Execute();
                            //GLOG 8574: If control is displayed, make sure value matches manual edit
                            if (oWordSourceVar.AssociatedControl != null && ((System.Windows.Forms.Control)oWordSourceVar.AssociatedControl).Visible 
                                && oWordSourceVar.AssociatedControl.Value != oWordSourceVar.Value)
                            {
                                oWordSourceVar.AssociatedControl.Value = oWordSourceVar.Value;
                                oWordSourceVar.AssociatedControl.IsDirty = false;
                                //Don't flag control to ensure additional edits directly in the control will be saved
                                bFlagToSkipUpdateFromControl = false;
                            }

                            //refresh tree
                            if ((oUISourceVar.DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                                Variable.ControlHosts.DocumentEditor)
                            {
                                //modified for GLOG 4365 (dm)
                                string xKey = oUISourceVar.Segment.FullTagID + "." +
                                    oUISourceVar.ID;
                                this.SetVariablesNodeValue(xKey, oUISourceVar);

                                //GLOG 4132 (dm) - flag variable so as not to lose manual
                                //edits when control loses focus
                                if (bFlagToSkipUpdateFromControl)
                                    m_xSkipVarUpdateFromControl = xKey;
                            }
                        }
                        finally
                        {
                            //GLOG 5669: Don't seek Main Story, since selection may have been
                            //in header or footer
                            oEnv.RestoreState(true, false, false);
                        }
                    }
                }
            }
            else if (oBlockCC != null)
            {
                //Update UI Text for block
                Block oBlock = null;
                Blocks oBlocks = null;
                oBlocks = oSegment.Blocks;

                //get the Block whose selection cc is the currently selected node
                oBlock = oBlocks.ItemFromAssociatedContentControl(oBlockCC);
                if (oBlock != null && oBlock.ShowInTree)
                {

                    UltraTreeNode oBlockNode = this.GetNodeFromTagID(oBlock.TagID);
                    //get current value node
                    UltraTreeNode oCurValueNode = oBlockNode.Nodes[0];

                    if (oCurValueNode == null)
                    {
                        //something is wrong - all variable and author
                        //nodes need to have a child value node
                        throw new LMP.Exceptions.UINodeException(
                            LMP.Resources.GetLangString("Error_MissingValueNode") +
                            this.m_oCurNode.FullPath);
                    }

                    //set node text to variable value
                    string xText = LMP.Forte.MSWord.WordDoc.GetRangeStartText(
                        oBlock.AssociatedContentControl.Range, 20);

                    xText = xText.Replace("\v", "...");
                    xText = xText.Replace("\r\n", "...");
                    xText = xText.Replace("\n", "...");

                    oCurValueNode.Text = xText;

                    //force a redraw
                    this.treeDocContents.Refresh();
                }
            }
        }

        /// <summary>
        /// updates value of currently selected variable and control if necessary -
        /// called when mouse moves from document into taskpane so as not to lose manual edits
        /// </summary>
        internal void UpdateCurrentVariableValueFromDocument()
        {
            if ((m_oCurNode != null) && (m_oCurCtl != null) && (m_oCurCtl.Visible))
            {
                if (m_oCurNode.Tag is Variable)
                {
                    Variable oVar = m_oCurNode.Tag as Variable;
                    string xValue = oVar.GetValueFromSource();
                    if (!LMP.String.CompareBooleanCaseInsensitive(oVar.Value, xValue))
                    {
                        //GLOG 2601 - just update value, don't execute actions -
                        //TODO: change was made to prevent feedback issues when there are
                        //multiple associated tags (see GLOG 2661), but not executing means
                        //that values won't remain in sync (which may or may not be what
                        //the user wants)
                        oVar.SetValue(xValue, false);
                        IControl oCtl = (IControl)m_oCurCtl;
                        oCtl.Value = oVar.Value;

                        ///GLOG 3131: remmed statement below
                        ////GLOG 2645: get control-specific runtime values
                        //oCtl.SupportingValues = oVar.RuntimeControlValues;

                        //mark as not edited unless changed by control
                        //remmed for GLOG 7624 (dm) - control should
                        //never remain dirty here
                        //if (oCtl.Value == oVar.Value)
                            oCtl.IsDirty = false;
                    }
                    //m_oCurCtl.Select(); (removed for GLOG item 2601)
                    LMP.MacPac.Application.SetXMLTagStateForEditing();
                }
            }
        }

        /// <summary>
        /// refreshes the portion of the tree
        /// starting with the specified segment tree node
        /// if necessary
        /// </summary>
        /// <param name="oNode"></param>
        public void RefreshSegmentNode(UltraTreeNode oNode)
        {
            this.RefreshSegmentNode(oNode, false);
        }
        /// <summary>
        /// refreshes the portion of the tree
        /// starting with the specified segment tree node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh">forces the node to refresh, even if the node already has children</param>
        private void RefreshSegmentNode(UltraTreeNode oSegmentNode, bool bForceRefresh)
        {

            //JTS 12/15/15:  This code has been disabled for now, since Document Editor is not usable otherwise.
            //If refreshing from the Oxml objects, there need to be Oxml versions of all event handlers as well.
#if Oxml 
            if (LMP.MacPac.Application.IsOxmlSupported() && TaskPane.NewXmlSegment!= null && this.ForteDocument.Mode != Architect.Base.ForteDocument.Modes.Design)
            {
                //segment was newly inserted by Oxml - refresh from
                //data in XmlSegment instead of from document data
                RefreshSegmentNode_Oxml(oSegmentNode, bForceRefresh, TaskPane.NewXmlSegment);
            }
            else
#endif
            {
                RefreshSegmentNode_Api(oSegmentNode, bForceRefresh);
            }
        }

        /// <summary>
        /// adds oSegment's child segments to oNode
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        private void AddChildSegmentNodes(Segment oSegment, UltraTreeNode oNode)
        {
            //cycle through child segments, adding each to tree
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                //get child segment
                LMP.Architect.Api.Segment oSeg = oSegment.Segments[i];

                //GLOG 7103 (dm) - in toolkit mode, display only segments
                //specified in metadata
                //GLOG : 7998 : ceh
                if ((LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator) &&
                    !LMP.Data.Application.DisplayInToolkitTaskPane(oSeg.TypeID))
                    continue;

                //if segment that is defined as transparent has multiple
                //children of the same type, change to non-transparent
                if (oSeg.IsTransparent)
                {
                    for (int j = 0; j < oSeg.Segments.Count; j++)
                    {
                        //GLOG 15907
                        if (oSeg.Segments[j] is Architect.Api.CollectionTableItem)
                        {
                            if (!oSeg.Segments[j].IsOnlyInstanceOfType)
                            {
                                oSeg.IsTransparent = false;
                                break;
                            }
                        }
                        else if (!oSeg.Segments[j].IsTransparent || oSeg.Segments[j].HasVisibleUIElements)
                        {
                            oSeg.IsTransparent = false;
                            break;
                        }
                    }
                }

                //add to tree
                if (!oSeg.IsTransparent)
                {
                    //get node display text -
                    //if the child is a component segment
                    //display the segment type - otherwise,
                    //display the display name
                    string xDisplayText = "";

                    //GLOG : 15813 : ceh - get display name for Document components that are of type Architect
                    if (oSeg.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent && oSeg.TypeID != mpObjectTypes.Architect)
                        xDisplayText = LMP.Data.Application.GetObjectTypeDisplayName(oSeg.TypeID);
                    else
                        xDisplayText = oSeg.DisplayName;

                    //not transparent - add child to tree
                    UltraTreeNode oTreeNode = null;

                    //primary item should be listed first among
                    //siblings of the same type
                    if (oSeg.IsPrimary && (oSeg.LinkAuthorsToParentDef ==
                        Segment.LinkAuthorsToParentOptions.FirstChildOfType))
                    {
                        //cycle through previously added siblings
                        for (int j = 0; j < i; j++)
                        {
                            Segment oSibling = oSegment.Segments[j];
                            if (oSibling.TypeID == oSeg.TypeID)
                            {
                                //put immediately above first sibling of same type
                                UltraTreeNode oSiblingNode = this.GetNodeFromTagID(
                                    oSibling.FullTagID);
                                oTreeNode = oNode.Nodes.Insert(oSiblingNode.Index,
                                    oSeg.FullTagID, xDisplayText);
                                break;
                            }
                        }
                    }

                    if (oTreeNode == null)
                        //add to bottom of parent node
                        oTreeNode = oNode.Nodes.Add(oSeg.FullTagID, xDisplayText);

                    //initialize new node
                    InitializeNode(oSeg, oTreeNode, false);

                    //remove tag id from list of transparent nodes if necessary
                    m_xTransparentNodes = m_xTransparentNodes.Replace(oSeg.FullTagID + '|', "");
                }
                else
                {
                    //GLOG 3447: Need to update Hashtable before Refreshing Node, so that
                    //control actions will be able to locate Variable nodes contained therein
                    //add tag id to list of transparent nodes
                    if (!this.NodeIsTransparent(oSeg.FullTagID))
                        m_xTransparentNodes += oSeg.FullTagID + '|';

                    //transparent - add children of child to tree
                    //GLOG 6531: Don't try to refresh Parent Node here -
                    //will go into endless loop if neither Parent nor Transparent child
                    //contains any variables or blocks.
                    RefreshTransparentNode(oSeg, oNode, false);

                }
            }
        }
        /// <summary>
        /// adds oSegment's child segments to oNode
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        private void AddChildSegmentNodes(Segment oSegment, LMP.Architect.Oxml.XmlSegment oXmlSegment, UltraTreeNode oNode)
        {
            //cycle through child segments, adding each to tree
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                //get child segment
                LMP.Architect.Api.Segment oSeg = oSegment.Segments[i];
                LMP.Architect.Oxml.XmlSegment oXmlSeg = oXmlSegment.Segments[oSeg.FullTagID];

                //GLOG 7103 (dm) - in toolkit mode, display only segments
                //specified in metadata
                //GLOG : 7998 : ceh
                if ((LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator) &&
                    !LMP.Data.Application.DisplayInToolkitTaskPane(oXmlSeg.TypeID))
                    continue;

                //if segment that is defined as transparent has multiple
                //children of the same type, change to non-transparent
                if (oXmlSeg.IsTransparent)
                {
                    for (int j = 0; j < oXmlSeg.Segments.Count; j++)
                    {
                        //GLOG 15907
                        if (oXmlSeg.Segments[j] is Architect.Oxml.XmlCollectionTableItem)
                        {
                            if (!oXmlSeg.Segments[j].IsOnlyInstanceOfType)
                            {
                                oXmlSeg.IsTransparent = false;
                                break;
                            }
                        }
                        else if (!oXmlSeg.Segments[j].IsTransparent || oXmlSeg.Segments[j].HasVisibleUIElements)
                        {
                            oXmlSeg.IsTransparent = false;
                            break;
                        }
                    }
                }

                //add to tree
                if (!oXmlSeg.IsTransparent)
                {
                    //get node display text -
                    //if the child is a component segment
                    //display the segment type - otherwise,
                    //display the display name
                    string xDisplayText = "";

                    //GLOG : 15813 : ceh - get display name for Document components that are of type Architect
                    if (oXmlSeg.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent && oXmlSeg.TypeID != mpObjectTypes.Architect)
                        xDisplayText = LMP.Data.Application.GetObjectTypeDisplayName(oXmlSeg.TypeID);
                    else
                        xDisplayText = oXmlSeg.DisplayName;

                    //not transparent - add child to tree
                    UltraTreeNode oTreeNode = null;

                    //primary item should be listed first among
                    //siblings of the same type
                    if (oXmlSeg.IsPrimary && (oXmlSeg.LinkAuthorsToParentDef ==
                        Segment.LinkAuthorsToParentOptions.FirstChildOfType))
                    {
                        //cycle through previously added siblings
                        for (int j = 0; j < i; j++)
                        {
                            LMP.Architect.Oxml.XmlSegment oSibling = oXmlSegment.Segments[j];
                            if (oSibling.TypeID == oXmlSeg.TypeID)
                            {
                                //put immediately above first sibling of same type
                                UltraTreeNode oSiblingNode = this.GetNodeFromTagID(
                                    oSibling.FullTagID);
                                oTreeNode = oNode.Nodes.Insert(oSiblingNode.Index,
                                    oSeg.FullTagID, xDisplayText);
                                break;
                            }
                        }
                    }

                    if (oTreeNode == null)
                        //add to bottom of parent node
                        oTreeNode = oNode.Nodes.Add(oXmlSeg.FullTagID, xDisplayText);

                    //initialize new node
                    InitializeNode(oXmlSeg, oSeg, oTreeNode, false);

                    //remove tag id from list of transparent nodes if necessary
                    m_xTransparentNodes = m_xTransparentNodes.Replace(oXmlSeg.FullTagID + '|', "");
                }
                else
                {
                    //GLOG 3447: Need to update Hashtable before Refreshing Node, so that
                    //control actions will be able to locate Variable nodes contained therein
                    //add tag id to list of transparent nodes
                    if (!this.NodeIsTransparent(oXmlSeg.FullTagID))
                        m_xTransparentNodes += oXmlSeg.FullTagID + '|';

                    //transparent - add children of child to tree
                    //GLOG 6531: Don't try to refresh Parent Node here -
                    //will go into endless loop if neither Parent nor Transparent child
                    //contains any variables or blocks.
                    RefreshTransparentNode(oSeg, oXmlSeg, oNode, false);

                }
            }
        }
        //GLOG 6531: Refresh Parent if necessary by default
        private void RefreshTransparentNode(Segment oSegment, UltraTreeNode oHostNode)
        {
            RefreshTransparentNode(oSegment, oHostNode, true);
        }
        /// <summary>
        /// adds members of oSegment to oNode
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oHostNode"></param>
        private void RefreshTransparentNode(Segment oSegment, UltraTreeNode oHostNode, bool bRefreshParentNodesIfNecessary) //GLOG 6531
        {
            //get segment associated with oHostNode
            Segment oHost = oHostNode.Tag as Segment;

            if (oHost == null)
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_NodeIsNotSegmentNode"));

            //get More node
            UltraTreeNode oMoreNode = null;
            try
            {
                //GLOG 6531
                oMoreNode = GetNodeFromTagID(oHost.FullTagID + ".Advanced", bRefreshParentNodesIfNecessary);
            }
            catch { }
            //GLOG 4025: Place Transparent Segment nodes before any Nodes for other Child Segments
            UltraTreeNode oChildSegNode = null;
            foreach (UltraTreeNode oNode in oHostNode.Nodes)
            {
                if (oNode.Tag is Segment)
                {
                    oChildSegNode = oNode;
                    break;
                }
            }
            //cycle through segment variables, adding each to tree
            for (int i = 0; i < oSegment.Variables.Count; i++)
            {
                //get variable
                LMP.Architect.Api.Variable oVar = oSegment.Variables[i];

                //add only variables that should be shown in the editor
                if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) !=
                    Variable.ControlHosts.DocumentEditor)
                    continue;

                string xKey = oSegment.FullTagID + "." + oVar.ID;

                //GLOG 6218 (dm) - the parent node may already have a node with
                //this key if something earlier in this method, e.g. getting
                //the More node, has triggered a complete refresh of it
                if (oHostNode.Nodes.Exists(xKey))
                    return;

                string xDisplayName = String.RestoreXMLChars(oVar.DisplayName, true); //GLOG 8463

                UltraTreeNode oTreeNode = null;

                //add to tree
                if (oVar.DisplayLevel == Variable.Levels.Basic)
                {
                    if (oMoreNode == null && oChildSegNode == null)
                    {
                        //add as last node
                        oTreeNode = oHostNode.Nodes.Add(
                            xKey, xDisplayName + ":");
                    }
                    else if (oMoreNode != null)
                    {
                        //add as last node in basic section
                        oTreeNode = oHostNode.Nodes.Insert(oMoreNode.Index,
                            xKey, xDisplayName + ":");
                    }
                    else
                    {
                        //add as last node in basic section
                        oTreeNode = oHostNode.Nodes.Insert(oChildSegNode.Index,
                            xKey, xDisplayName + ":");
                    }
                }
                else
                {
                    //GLOG 4025: if oChildSegNode exists, place More node above it
                    //add "More" node
                    if (oMoreNode == null)
                        oMoreNode = CreateMoreNode(oHostNode, oHost, oChildSegNode);

                    //add as last node in "More" category
                    oTreeNode = oMoreNode.Nodes.Add(
                        xKey, xDisplayName + ":");
                }
                //initialize new node
                InitializeNode_API(oVar, oTreeNode, false);

                //7-12-11 (dm) - disable if parent is deactivated
                if (!oHost.Activated)
                    oTreeNode.Enabled = false;
            }
            //cycle through segment variables, executing the
            //ValueChanged actions - this sets up the nodes
            //based on the initial variable values
            for (int i = 0; i < oSegment.Variables.Count; i++)
                ExecuteControlActions(oSegment.Variables[i],
                    ControlActions.Events.ValueChanged);

            //execute the value changed control 
            //actions assigned to the author
            ExecuteControlActions(oSegment.Authors,
                ControlActions.Events.ValueChanged);

            //cycle through segment blocks, adding each to tree
            for (int i = 0; i < oSegment.Blocks.Count; i++)
            {
                //get block
                LMP.Architect.Api.Block oBlock = oSegment.Blocks[i];

                //add to tree
                if (oBlock.ShowInTree)
                {
                    //add "More" node
                    if (oMoreNode == null)
                        oMoreNode = CreateMoreNode(oHostNode, oHost);

                    string xKey = oBlock.TagID;
                    UltraTreeNode oTreeNode = oMoreNode.Nodes.Add(
                        xKey, oBlock.DisplayName);

                    //initialize new node
                    InitializeNode(oBlock, oTreeNode, false);
                }
            }

            //add child segments
            AddChildSegmentNodes(oSegment, oHostNode);
        }

        /// <summary>
        /// adds members of oSegment to oNode
        /// </summary>
        /// <param name="oXmlSegment"></param>
        /// <param name="oHostNode"></param>
        private void RefreshTransparentNode(Segment oSegment, LMP.Architect.Oxml.XmlSegment oXmlSegment, UltraTreeNode oHostNode, bool bRefreshParentNodesIfNecessary) //GLOG 6531
        {
            //get segment associated with oHostNode
            Segment oHost = oHostNode.Tag as Segment;

            if (oHost == null)
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_NodeIsNotSegmentNode"));

            //get More node
            UltraTreeNode oMoreNode = null;
            try
            {
                //GLOG 6531
                oMoreNode = GetNodeFromTagID(oHost.FullTagID + ".Advanced", bRefreshParentNodesIfNecessary);
            }
            catch { }
            //GLOG 4025: Place Transparent Segment nodes before any Nodes for other Child Segments
            UltraTreeNode oChildSegNode = null;
            foreach (UltraTreeNode oNode in oHostNode.Nodes)
            {
                if (oNode.Tag is Segment)
                {
                    oChildSegNode = oNode;
                    break;
                }
            }
            //cycle through segment variables, adding each to tree
            for (int i = 0; i < oSegment.Variables.Count; i++)
            {
                //get variable
                Variable oVar = oSegment.Variables[i];
                LMP.Architect.Oxml.XmlVariable oXmlVar = oXmlSegment.Variables[oVar.ID];

                //add only variables that should be shown in the editor
                if ((oXmlVar.DisplayIn & Variable.ControlHosts.DocumentEditor) !=
                    Variable.ControlHosts.DocumentEditor)
                    continue;

                string xKey = oXmlSegment.FullTagID + "." + oXmlVar.ID;

                //GLOG 6218 (dm) - the parent node may already have a node with
                //this key if something earlier in this method, e.g. getting
                //the More node, has triggered a complete refresh of it
                if (oHostNode.Nodes.Exists(xKey))
                    return;

                string xDisplayName = String.RestoreXMLChars(oXmlVar.DisplayName, true); //GLOG 8463

                UltraTreeNode oTreeNode = null;

                //add to tree
                if (oXmlVar.DisplayLevel == Variable.Levels.Basic)
                {
                    if (oMoreNode == null && oChildSegNode == null)
                    {
                        //add as last node
                        oTreeNode = oHostNode.Nodes.Add(
                            xKey, xDisplayName + ":");
                    }
                    else if (oMoreNode != null)
                    {
                        //add as last node in basic section
                        oTreeNode = oHostNode.Nodes.Insert(oMoreNode.Index,
                            xKey, xDisplayName + ":");
                    }
                    else
                    {
                        //add as last node in basic section
                        oTreeNode = oHostNode.Nodes.Insert(oChildSegNode.Index,
                            xKey, xDisplayName + ":");
                    }
                }
                else
                {
                    //GLOG 4025: if oChildSegNode exists, place More node above it
                    //add "More" node
                    if (oMoreNode == null)
                        oMoreNode = CreateMoreNode(oHostNode, oHost, oChildSegNode);

                    //add as last node in "More" category
                    oTreeNode = oMoreNode.Nodes.Add(
                        xKey, xDisplayName + ":");
                }
                //initialize new node
                InitializeNode_Oxml(oVar, oXmlVar, oTreeNode, false);

                //7-12-11 (dm) - disable if parent is deactivated
                if (!oHost.Activated)
                    oTreeNode.Enabled = false;
            }
            //cycle through segment variables, executing the
            //ValueChanged actions - this sets up the nodes
            //based on the initial variable values
            for (int i = 0; i < oSegment.Variables.Count; i++)
                ExecuteControlActions(oSegment.Variables[i],
                    ControlActions.Events.ValueChanged);

            //execute the value changed control 
            //actions assigned to the author
            ExecuteControlActions(oSegment.Authors,
                ControlActions.Events.ValueChanged);

            //cycle through segment blocks, adding each to tree
            for (int i = 0; i < oSegment.Blocks.Count; i++)
            {
                //get block
                LMP.Architect.Api.Block oBlock = oSegment.Blocks[i];
                LMP.Architect.Oxml.XmlBlock oXmlBlock = oXmlSegment.Blocks.ItemFromName(oBlock.Name);

                //add to tree
                if (oXmlBlock.ShowInTree)
                {
                    //add "More" node
                    if (oMoreNode == null)
                        oMoreNode = CreateMoreNode(oHostNode, oHost);

                    string xKey = oBlock.TagID;
                    UltraTreeNode oTreeNode = oMoreNode.Nodes.Add(
                        xKey, oXmlBlock.DisplayName);

                    //initialize new node
                    InitializeNode(oBlock, oXmlBlock, oTreeNode, false);
                }
            }
            
            //add child segments
            AddChildSegmentNodes(oSegment, oXmlSegment, oHostNode);
        }

        /// <summary>
        /// returns true if the specified tag id represents a transparent segment node
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        private bool NodeIsTransparent(string xTagID)
        {
            //GLOG 3482 - added leading pipe
            return (m_xTransparentNodes.IndexOf('|' + xTagID + '|') > -1);
        }
        /// <summary>
        /// returns the image that should be
        /// displayed for the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private Images GetSegmentImage(Segment oSegment, bool bIsValid, bool bIsComplete)
        {
            switch (oSegment.IntendedUse)
            {
                case mpSegmentIntendedUses.AsDocument:
                    if (!bIsValid)
                        return Images.InvalidDocumentSegment;
                    else if (!bIsComplete)
                        return Images.IncompleteDocumentSegment;
                    else
                        return Images.ValidDocumentSegment;
                case mpSegmentIntendedUses.AsDocumentComponent:
                    if (!bIsValid)
                        return Images.InvalidComponentSegment;
                    else if (!bIsComplete)
                        return Images.IncompleteComponentSegment;
                    else
                        return Images.ValidComponentSegment;
                case mpSegmentIntendedUses.AsParagraphText:
                default:
                    if (!bIsValid)
                        return Images.InvalidTextBlockSegment;
                    else if (!bIsComplete)
                        return Images.IncompleteTextBlockSegment;
                    else
                        return Images.ValidTextBlockSegment;
            }
        }
        /// <summary>
        /// returns the image that should be
        /// displayed for the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private Images GetSegmentImage(LMP.Architect.Oxml.XmlSegment oSegment, bool bIsValid, bool bIsComplete)
        {
            switch (oSegment.IntendedUse)
            {
                case mpSegmentIntendedUses.AsDocument:
                    if (!bIsValid)
                        return Images.InvalidDocumentSegment;
                    else if (!bIsComplete)
                        return Images.IncompleteDocumentSegment;
                    else
                        return Images.ValidDocumentSegment;
                case mpSegmentIntendedUses.AsDocumentComponent:
                    if (!bIsValid)
                        return Images.InvalidComponentSegment;
                    else if (!bIsComplete)
                        return Images.IncompleteComponentSegment;
                    else
                        return Images.ValidComponentSegment;
                case mpSegmentIntendedUses.AsParagraphText:
                default:
                    if (!bIsValid)
                        return Images.InvalidTextBlockSegment;
                    else if (!bIsComplete)
                        return Images.IncompleteTextBlockSegment;
                    else
                        return Images.ValidTextBlockSegment;
            }
        }
        private UltraTreeNode CreateMoreNode(UltraTreeNode oSegmentNode, Segment oSegment, UltraTreeNode oNextNode)
        {
            UltraTreeNode oMoreNode = null;
            if (oNextNode != null)
            {
                //add "advanced" node
                oMoreNode = oSegmentNode.Nodes.Insert(oNextNode.Index,
                    oSegment.FullTagID + ".Advanced",
                    LMP.Resources.GetLangString("Prompt_AdvancedVariables"));
            }
            else
            {
                //add "advanced" node
                oMoreNode = oSegmentNode.Nodes.Add(
                    oSegment.FullTagID + ".Advanced",
                    LMP.Resources.GetLangString("Prompt_AdvancedVariables"));
            }
            //set node properties
            oMoreNode.LeftImages.Add(Images.ValidVariableValue);
            oMoreNode.Expanded = false;

            Override oOverride = oMoreNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Always;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.ToggleExpansion;

            return oMoreNode;
        }
        private UltraTreeNode CreateMoreNode(UltraTreeNode oSegmentNode, Segment oSegment)
        {
            return CreateMoreNode(oSegmentNode, oSegment, null);
        }
        /// <summary>
        /// returns the node with the specified tag ID, populating parent nodes if necessary
        /// </summary>
        /// <param name="xTagID"></param>
        /// <returns></returns>
        private UltraTreeNode GetNodeFromTagID(string xTagID)
        {
            return GetNodeFromTagID(xTagID, true);
        }
        /// <summary>
        /// returns the node with the specified tag ID -
        /// if bRefreshParentsIfNecessary=TRUE, then first populates parent nodes if necessary -
        /// if bRefreshParentsIfNecessary=FALSE, returns null if parent nodes have not yet been populated
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="bRefreshParentsIfNecessary"></param>
        /// <returns></returns>
        private UltraTreeNode GetNodeFromTagID(string xTagID, bool bRefreshParentsIfNecessary)
        {
            string xPartialTag = "";
            UltraTreeNode oNode = null;
            int iPos = xTagID.IndexOf(".");

            //cycle through tag id separators,
            //referring the node specified by the
            //partial tag
            while (iPos > -1)
            {
                xPartialTag = xTagID.Substring(0, iPos);

                if (!this.NodeIsTransparent(xPartialTag))
                {
                    try
                    {
                        //get the node referred to by the partial tag
                        if (oNode == null)
                        {
                            try
                            {
                                oNode = this.treeDocContents.Nodes[xPartialTag];
                            }
                            catch { }
                            //GLOG 3246: If Key not found, also check Trailers umbrella node
                            if (oNode == null)
                            {
                                //GLOG 7103 (dm) - in toolkit mode, not all partial tags will
                                //have corresponding nodes and there's no reason to check for
                                //a trailers node
								//GLOG : 7998 : ceh
                                if (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                                    return null;

                                oNode = this.treeDocContents.Nodes[mpTrailersNodeKey].Nodes[xPartialTag];
                            }
                        }
                        else
                            oNode = oNode.Nodes[xPartialTag];
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.NotInCollectionException(
                            LMP.Resources.GetLangString(
                            "Error_InvalidDocContentsTreeNode") + xPartialTag, oE);
                    }

                    if (oNode.Nodes.Count == 0)
                    {
                        if (bRefreshParentsIfNecessary)
                        {
                            //get any child nodes
                            Segment oSegment = oNode.Tag as Segment;

                            //8-18-11 (dm) - added condition that segment actually
                            //has any variable or block nodes to add - this was
                            //necessary to avoid an infinite loop
                            //10-12-11 (dm) - changed condition to HasSnapshot to
                            //match new condition in RefreshSegmentNode
                            //if ((oSegment != null) && (oSegment.HasVariables(true) ||
                            //    oSegment.HasBlocks(true)))
                            //GLOG 5856 (dm) - limited condition to advanced node, which
                            //was what was causing the infinite loop in finished segments -
                            //we do need to refresh if we're looking for a child segment here
                            if ((xTagID.IndexOf(".Advanced") == -1) ||
                                ((oSegment != null) && (!Snapshot.Exists(oSegment))))
                                this.RefreshSegmentNode(oNode);
                        }
                        else
                            //parent is not yet populated
                            return null;
                    }
                }

                //get next tag id separator
                iPos = xTagID.IndexOf(".", iPos + 1);
            }

            iPos = xTagID.LastIndexOf(".");

            if (iPos == -1)
            {
                try
                {
                    oNode = this.treeDocContents.Nodes[xTagID];
                }
                catch { }
                //GLOG 3246: If Key not found, also check Trailers umbrella node
                if (oNode == null)
                {
                    oNode = this.treeDocContents.Nodes[mpTrailersNodeKey].Nodes[xTagID];
                }
            }
            else
            {
                try
                {
                    //there are separators - get the last tag element
                    oNode = oNode.Nodes[xTagID];
                }
                catch
                {
                    //no var node with the specified key
                    //is located in the "basic" variables section -
                    //look in the advanced section
                    string xAdvancedVarsNode = oNode.Key + ".Advanced";

                    //get advanced vars node
                    oNode = oNode.Nodes[xAdvancedVarsNode];

                    //get the var in the advanced vars node
                    oNode = oNode.Nodes[xTagID];
                }
            }

            //return the node referred to last
            return oNode;
        }
        /// <summary>
        /// selects the variables tree node whose 
        /// selection tag is the specified Word Tag
        /// </summary>
        /// <param name="oWordTag"></param>
        private void SelectNode(Word.ContentControl oCC)
        {
            Word.ContentControl oTargetCC = null;
            string xElement = "";

            if (oCC != null)
            {
                //get base name - exit if base name
                //can't be retrieved - this happens
                //sometimes when pasting subvars
                try
                {
                    xElement = String.GetBoundingObjectBaseName(oCC.Tag);
                }
                catch
                {
                    return;
                }

                if (xElement == "mSubVar")
                {
                    //for mSubVars, we're actually interested in containing mVar -
                    //keep checking parent node until an mVar is found
                    oTargetCC = oCC.ParentContentControl;
                    while ((oTargetCC != null) && 
                        (String.GetBoundingObjectBaseName(oTargetCC.Tag) != "mVar"))
                        oTargetCC = oTargetCC.ParentContentControl;
                }
                else if (xElement == "mDel")
                {
                    //GLOG 3683 (3/19/09 dm) - select mDel's parent node
                    oTargetCC = oCC.ParentContentControl;

                    //6-21-11 (dm) - don't stop at mSubVar
                    while ((oTargetCC != null) &&
                        (String.GetBoundingObjectBaseName(oTargetCC.Tag) == "mSubVar"))
                        oTargetCC = oTargetCC.ParentContentControl;

                    if (oTargetCC != null)
                        xElement = String.GetBoundingObjectBaseName(oTargetCC.Tag);
                }
                else
                    //set target to specified tag
                    oTargetCC = oCC;
            }

            if (oTargetCC != null)
            {
                //get containing mSEG
                Word.ContentControl oSegCC = null;
                Word.Bookmark oSegBmk = null;

                if (xElement != "mSEG")
                {
                    //keep checking parent node until an mSEG is found
                    oSegCC = oTargetCC.ParentContentControl;
                    while ((oSegCC != null) &&
                        (String.GetBoundingObjectBaseName(oSegCC.Tag) != "mSEG"))
                        oSegCC = oSegCC.ParentContentControl;
                    if (oSegCC == null)
                    {
                        try
                        {
                            oSegBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oTargetCC.Range);
                        }
                        catch { }
                    }
                }
                else
                    oSegCC = oTargetCC;

                //get segment
                Segment oSegment = null;
                string xSegFullTagID = "";
                if (oSegCC != null) 
                {
                    xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oSegCC);
                }
                else if (oSegBmk != null)
                {
                    xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oSegBmk);
                }
                if (!System.String.IsNullOrEmpty(xSegFullTagID))
                    oSegment = this.m_oMPDocument.FindSegment(xSegFullTagID);

                //GLOG 7103 (dm) - non-toolkit segments aren't displayed to toolkit users
				//GLOG : 7998 : ceh
                if ((oSegment != null) && (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator) &&
                    !LMP.Data.Application.DisplayInToolkitTaskPane(oSegment.TypeID))
                    return;

                switch (xElement)
                {
                    case "mVar":
                    case "mDel":
                    case "mSubVar":
                        {
                            //variable
                            Variables oVariables = null;
                            if (oSegment != null)
                            {
                                //JTS 6/27/11: Trailers aren't added to TaskPane, so don't select anything
                                if (oSegment.TypeID == mpObjectTypes.Trailer)
                                    return;
                                oVariables = oSegment.Variables;
                            }
                            else
                                ////standalone
                                //oVariables = m_oMPDocument.Variables;
                                return;

                            //GLOG - 3255 - CEH
                            //get the variable whose selection tag is the currently selected node
                            Variable oVar = oVariables.ItemFromAssociatedContentControl(oTargetCC);
                            Variable oTargetVar = null;

                            if (oVar != null)
                            {
                                //get target var
                                if (oVar.UISourceVariable != oVar && ((oVar.UISourceVariable.DisplayIn &
                                                                        Variable.ControlHosts.DocumentEditor) ==
                                                                        Variable.ControlHosts.DocumentEditor))
                                {
                                    oTargetVar = oVar.UISourceVariable;
                                }
                                else
                                {
                                    oTargetVar = oVar;
                                }
                            }

                            if (oTargetVar != null && ((oTargetVar.DisplayIn &
                                Variable.ControlHosts.DocumentEditor) ==
                                Variable.ControlHosts.DocumentEditor))
                            {
                                //select the node associated with the variable
                                string xKey = "";
                                if (oTargetVar.Segment != null)
                                    xKey = oTargetVar.Segment.FullTagID + "." + oTargetVar.ID;
                                else
                                    xKey = oTargetVar.ID;

                                this.SelectNode(xKey);
                            }
                            else if ((oTargetVar != null) && (oTargetVar.Segment != null))
                                //select the segment containing the mVar
                                this.SelectNode(oTargetVar.Segment.FullTagID);
                            else
                                //select default node
                                oTargetCC = null;

                            break;
                        }
                    case "mBlock":
                        {
                            //block
                            Blocks oBlocks = null;
                            if (oSegment != null)
                            {
                                //JTS 6/27/11: Trailers aren't added to TaskPane, so don't select anything
                                if (oSegment.TypeID == mpObjectTypes.Trailer)
                                    return;
                                oBlocks = oSegment.Blocks;
                            }
                            else
                                return;
                            ////standalone
                            //oBlocks = m_oMPDocument.Blocks;

                            //get the Block whose selection tag is the currently selected node
                            Block oBlock = oBlocks.ItemFromAssociatedContentControl(oTargetCC);

                            if ((oBlock != null) && (oBlock.ShowInTree))
                                //select the Block node associated with the Block
                                this.SelectNode(oBlock.TagID);
                            else if (oSegment != null)
                                //select the segment containing the mVar
                                this.SelectNode(oSegment.FullTagID);
                            else
                                //select default node
                                oTargetCC = null;

                            break;
                        }
                    default:
                        {
                            if (oSegment != null)
                            {
                                //JTS 6/27/11: Trailers aren't added to TaskPane, so don't select anything
                                if (oSegment.TypeID == mpObjectTypes.Trailer)
                                    return;
                                //select the segment node in the variables tree
                                this.SelectNode(oSegment.FullTagID);
                            }
                            else
                                //select default node
                                oTargetCC = null;

                            break;
                        }
                }
            }

            //if there's no tree node associated with the selected element, select 
            //node for first document segment if possible
            if ((oTargetCC == null) && (m_oMPDocument.Segments.Count > 0))
            {
                this.SelectNode("");
                m_oCurNode = null;
            }
        }
        private void SelectNode(Word.XMLNode oWordTag)
        {
            Word.XMLNode oTargetTag = null;
            string xElement = "";

            if (oWordTag != null)
            {
                //get base name - exit if base name
                //can't be retrieved - this happens
                //sometimes when pasting subvars
                try
                {
                    xElement = oWordTag.BaseName;
                }
                catch
                {
                    return;
                }

                if (xElement == "mSubVar")
                {
                    //for mSubVars, we're actually interested in containing mVar -
                    //keep checking parent node until an mVar is found
                    oTargetTag = oWordTag.ParentNode;
                    while ((oTargetTag != null) && (oTargetTag.BaseName != "mVar"))
                        oTargetTag = oTargetTag.ParentNode;
                }
                else if (xElement == "mDel")
                {
                    //GLOG 3683 (3/19/09 dm) - select mDel's parent node
                    oTargetTag = oWordTag.ParentNode;

                    //6-21-11 (dm) - don't stop at mSubVar
                    while ((oTargetTag != null) && (oTargetTag.BaseName == "mSubVar"))
                        oTargetTag = oTargetTag.ParentNode;

                    if (oTargetTag != null)
                        xElement = oTargetTag.BaseName;
                }
                else
                    //set target to specified tag
                    oTargetTag = oWordTag;
            }

            if (oTargetTag != null)
            {
                //get containing mSEG
                Word.XMLNode oSegTag = null;
                Word.Bookmark oSegBmk = null;
                if (xElement != "mSEG")
                {
                    //keep checking parent node until an mSEG is found
                    oSegTag = oTargetTag.ParentNode;
                    while ((oSegTag != null) &&
                        (oSegTag.BaseName != "mSEG"))
                        oSegTag = oSegTag.ParentNode;
                    if (oSegTag == null)
                    {
                        oSegBmk = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oTargetTag.Range);
                    }
                }
                else
                    oSegTag = oTargetTag;

                //get segment
                Segment oSegment = null;
                string xSegFullTagID = "";
                if (oSegTag != null)
                {
                    xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(oSegTag);
                }
                else if (oSegBmk != null)
                {
                    xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oSegBmk);
                }
                if (!System.String.IsNullOrEmpty(xSegFullTagID))
                    oSegment = this.m_oMPDocument.FindSegment(xSegFullTagID);

                //GLOG 7103 (dm) - non-toolkit segments aren't displayed to toolkit users
				//GLOG : 7998 : ceh
                if ((oSegment != null) && (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator) &&
                    !LMP.Data.Application.DisplayInToolkitTaskPane(oSegment.TypeID))
                    return;

                switch (xElement)
                {
                    case "mVar":
                    case "mDel":
                    case "mSubVar":
                        {
                            //variable
                            Variables oVariables = null;
                            if (oSegment != null)
                            {
                                //JTS 6/27/11: Trailers aren't added to TaskPane, so don't select anything
                                if (oSegment.TypeID == mpObjectTypes.Trailer)
                                    return;
                                oVariables = oSegment.Variables;
                            }
                            else
                                ////standalone
                                //oVariables = m_oMPDocument.Variables;
                                return;

                            //GLOG - 3255 - CEH
                            //get the variable whose selection tag is the currently selected node
                            Variable oVar = oVariables.ItemFromAssociatedTag(oTargetTag);
                            Variable oTargetVar = null;

                            if (oVar != null)
                            {
                                //get target var
                                if (oVar.UISourceVariable != oVar && ((oVar.UISourceVariable.DisplayIn &
                                                                        Variable.ControlHosts.DocumentEditor) ==
                                                                        Variable.ControlHosts.DocumentEditor))
                                {
                                    oTargetVar = oVar.UISourceVariable;
                                }
                                else
                                {
                                    oTargetVar = oVar;
                                }
                            }

                            if (oTargetVar != null && ((oTargetVar.DisplayIn &
                                Variable.ControlHosts.DocumentEditor) ==
                                Variable.ControlHosts.DocumentEditor))
                            {
                                //select the node associated with the variable
                                string xKey = "";
                                if (oTargetVar.Segment != null)
                                    xKey = oTargetVar.Segment.FullTagID + "." + oTargetVar.ID;
                                else
                                    xKey = oTargetVar.ID;

                                this.SelectNode(xKey);
                            }
                            else if ((oTargetVar != null) && (oTargetVar.Segment != null))
                                //select the segment containing the mVar
                                this.SelectNode(oTargetVar.Segment.FullTagID);
                            else
                                //select default node
                                oTargetTag = null;

                            break;
                        }
                    case "mBlock":
                        {
                            //block
                            Blocks oBlocks = null;
                            if (oSegment != null)
                            {
                                //JTS 6/27/11: Trailers aren't added to TaskPane, so don't select anything
                                if (oSegment.TypeID == mpObjectTypes.Trailer)
                                    return;
                                oBlocks = oSegment.Blocks;
                            }
                            else
                                return;
                                ////standalone
                                //oBlocks = m_oMPDocument.Blocks;

                            //get the Block whose selection tag is the currently selected node
                            Block oBlock = oBlocks.ItemFromAssociatedTag(oTargetTag);

                            if ((oBlock != null) && (oBlock.ShowInTree))
                                //select the Block node associated with the Block
                                this.SelectNode(oBlock.TagID);
                            else if (oSegment != null)
                                //select the segment containing the mVar
                                this.SelectNode(oSegment.FullTagID);
                            else
                                //select default node
                                oTargetTag = null;

                            break;
                        }
                    default:
                        {
                            if (oSegment != null)
                            {
                                //JTS 6/27/11: Trailers aren't added to TaskPane, so don't select anything
                                if (oSegment.TypeID == mpObjectTypes.Trailer)
                                    return;
                                //select the segment node in the variables tree
                                this.SelectNode(oSegment.FullTagID);
                            }
                            else
                                //select default node
                                oTargetTag = null;

                            break;
                        }
                }
            }

            //if there's no tree node associated with the selected element, select 
            //node for first document segment if possible
            if ((oTargetTag == null) && (m_oMPDocument.Segments.Count > 0))
            {
                this.SelectNode("");
                m_oCurNode = null;
            }
                //this.SelectNode(m_oMPDocument.Segments[0].TagID);
        }

        /// <summary>
        /// refreshes the selected tree node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh"></param>
        private void SelectNode(UltraTreeNode oNode)
        {
            this.SelectNode(oNode, false);
        }
        /// <summary>
        /// refreshes the selected tree node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh"></param>
        private void SelectNode(UltraTreeNode oNode, bool bForceRefresh) 
        {
            DateTime t0 = DateTime.Now;

            //get screenupdating state for later use
            if (oNode == null)
            {
                ExitEditMode();
                return;
            }

            bool bScreenUpdating = this.TaskPane.ScreenUpdating;

            //get current and new segment for use below-
            //if we're switching segments, we'll 
            Segment oCurSegment = null;

            if (m_oCurNode != null)
            {
                //GLOG 6005 (dm) - accept all "as document" segments as top-level
                oCurSegment = GetTopLevelSegmentFromNode(m_oCurNode, true);
            }

            //GLOG 6005 (dm) - accept all "as document" segments as top-level
            Segment oNewSegment = GetTopLevelSegmentFromNode(oNode, true);

            if (oNode.Key.EndsWith("_Value"))
            {
                //node is a variable value node - select parent
                this.treeDocContents.ActiveNode = oNode.Parent;
                // GLOG : 3303 : JAB 
                // If this is a block node move focus to the document.
                // This occurs when the previous node was not the parent node.
                if (oNode.Parent != null && oNode.Parent.Tag is Block)
                {
                    this.tslblStatus.Text = DateTime.Now.ToString() + " : " + oNode.Text;
                    this.lblStatus.Text = this.tslblStatus.Text;
                    UltraTreeNode oBlockNode = oNode.Parent; //GLOG 5903
                    this.m_oCurNode = null;
                    this.SelectNode(oBlockNode);
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }
                return;
            }

            if (m_bSelectingNode)
                return;
            else
                m_bSelectingNode = true;

            try
            {
                //exit if we're clicking the same node or no node is specified
                if (oNode == null || oNode == this.m_oCurNode)
                    return;

                try
                {
                    //exit edit mode - if not already exited
                    UltraTreeNode oNewNode = null;
                    UltraTreeNode oParentNode = oNode.Parent;

                    if (m_oCurCtl != null || (m_oCurNode != null && m_oCurNode.Tag is Block))
                        oNewNode = this.ExitEditMode();

                    if (oNewNode != null)
                    {
                        //segment has been replaced via a chooser control and
                        //oNode may be a child of the old segment node - get the
                        //node following the new segment node
                        UltraTreeNode oNextNode = oNewNode.NextVisibleNode;
                        if (oNextNode != null)
                            //select next node
                            oNode = oNextNode;
                        else
                        {
                            //if oNextNode is null, select either top level
                            //treenode or oNewNode, which will belong to newly
                            //added segment replaced via chooser or insertsegment
                            //variable action
                            if (!(oNewNode.Tag is Segment))
                                //select first node in tree
                                oNode = this.treeDocContents.Nodes[0];
                            else
                                oNode = oNewNode;
                        }
                    }
                    else if ((oParentNode != null) && (!oParentNode.Nodes.Exists(oNode.Key)))
                    {
                        //GLOG 5312: Get text of Node we were originally trying to select
                        string xOldCaption = oNode.Text;
                        //oNode no longer exists - it may correspond to a
                        //variable that was within the delete scope of the node
                        //being exited - switch to next visible node
                        //GLOG 5312: m_oCurNode may no longer be in tree if it has been refreshed by an action
                        oNode = GetNodeFromTagID(m_oCurNode.Key).NextVisibleNode;

                        //skip over all value nodes - value nodes can't be selected
                        while (oNode != null && (oNode.Key.EndsWith("_Value") || !oNode.Enabled))
                            oNode = oNode.NextVisibleNode;

                        //12-15-10 (dm) - if no more nodes, set back to current node
                        if (oNode == null)
                            oNode = GetNodeFromTagID(m_oCurNode.Key);

                        //GLOG 5312: Try to locate node with text matching node that
                        //was originally clicked on
                        if (oNode.Text != xOldCaption)
                        {                            
                            UltraTreeNode oTestNode = oNode;
                            while (oTestNode != null && oTestNode.Text != xOldCaption)
                            {
                                oTestNode = oTestNode.NextVisibleNode;
                                if (oTestNode != null && oTestNode.Text == xOldCaption)
                                {
                                    oNode = oTestNode;
                                    break;
                                }
                            }
                        }
                    }

                    //Clean up any orphaned controls that might occur
                    //when quickly tabbing through tree
                    foreach (System.Windows.Forms.Control oCtl in this.treeDocContents.Controls)
                    {
                        if (oCtl is IControl && oCtl.Visible)
                        {
                            UltraTreeNode oValueNode = null;
                            try
                            {
                                //Get corresponding node from Hash table
                                oValueNode = (UltraTreeNode)m_oControlsHash[oCtl];
                            }
                            catch { }
                            this.treeDocContents.Controls.Remove(oCtl);
                            if (oValueNode != null)
                            {
                                //Reset Node text and spacing
                                oValueNode.Override.NodeSpacingAfter = 10;
                                if (oValueNode.Parent.Tag is Variable)
                                {
                                    oValueNode.Text = GetVariableUIValue_API((Variable)oValueNode.Parent.Tag);
                                }
                                else if (oValueNode.Parent.Tag is Authors)
                                {
                                    oValueNode.Text = GetAuthorsUIValue((Authors)oValueNode.Parent.Tag);
                                }
                                else if (oValueNode.Tag is Jurisdictions)
                                {
                                    oValueNode.Text = ((Segment)oValueNode.Parent.Parent.Tag).GetJurisdictionText();
                                }
                                else if (oValueNode.Tag is Segment)
                                {
                                    //Remove from has table - the Chooser is recreated each time
                                    m_oControlsHash.Remove(oCtl);
                                }
                                else if (oValueNode.Key.EndsWith(".Language_Value"))
                                {
                                    string xCulture = ((Segment)oValueNode.Parent.Parent.Tag).Culture.ToString();
                                    oValueNode.Text = LMP.Data.Application.GetLanguagesDisplayValue(xCulture);
                                }
                            }
                            this.treeDocContents.Refresh();
                        }
                    }
                }
                catch { }

                //GLOG 3542: If Tree or Control has been refreshed by an action
                //Original Node may have been removed and recreated, in which case
                //oNode.Control will be null - repoint to current instance in tree
                if (oNode.Control == null)
                {
                    //GLOG 6045 (dm) - if GetNodeFromTagID errs under these circumstances,
                    //it's because the key for this node points to a segment that no longer
                    //exists - in this case, refreshing makes more sense than raising an error
                    try
                    {
                        oNode = GetNodeFromTagID(oNode.Key);
                    }
                    catch
                    {
                        this.RefreshTree();
                        return;
                    }
                }

                //GLOG 3452: The following was moved out of finally block to aid debugging
                while (!oNode.Enabled)
                    oNode = oNode.NextVisibleNode;

                //hide menu button
                this.picMenu.Visible = false;

                //ensure that the primary associated word tag is still in the document -
                //we are not always able to detect manual tag deletions, e.g. when
                //done by holding down the backspace key
                if (AssociatedWordTagIsMissing(oNode))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Prompt_AssociatedWordTagMissing"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.RefreshTree();
                }
                else
                {
                    if (oNode.Tag is Segment)
                    {
                        //selected node is a segment
                        if (oNode.Nodes.Count == 0 || bForceRefresh)
                            //segment node has children - refresh
                            this.RefreshSegmentNode(oNode);

                        //expand node based on user preference
                        if (oNode.Level != 0 && !oNode.Expanded)
                            oNode.Expanded = Session.CurrentUser.FirmSettings.ExpandSelectedChildSegment;

                        //scroll into view
                        if (!oNode.IsInView && oNode.Parent != null && oNode.Parent.Expanded)
                        {
                            //Can only set TopNode if parent is expanded
                            this.treeDocContents.TopNode.Expanded = true;
                            this.treeDocContents.TopNode = this.m_oCurNode;
                        }

                        //set up node for editing
                        EnterEditMode(oNode);

                        //set node as the current node for the edit tab
                        this.m_oCurNode = oNode;

                        //GLOG #2873 - dcf - there seems to be a bug with the infragistics controls
                        //such that the parent control is null when closing a document
                        //that is not active - e.g. from the Windows Task Pane -
                        //hence, the condition below
                        //GLOG #3289 - dcf - similar to #2873
                        if (this.treeDocContents.ActiveNode != null && 
                            this.treeDocContents.ActiveNode.Control != null)
                        {
                            //select node
                            this.treeDocContents.ActiveNode = this.m_oCurNode;
                        }

                        //show the button for the context menu
                        ShowContextMenuButton(oNode);

                        //the following block was remmed for GLOG 6864 (dm)
                        ////select start of segment
                        //try
                        //{
                        //    LMP.MacPac.Application.ScreenUpdating = false;
                        //    object oMissing = System.Reflection.Missing.Value;
                        //    Word.Range oSegmentRng = ((Segment)oNode.Tag).PrimaryRange;
                        //    oSegmentRng.Select();
                        //    oSegmentRng.StartOf(ref oMissing, ref oMissing);
                        //    oSegmentRng.Select();
                        //}
                        //finally
                        //{
                        //    LMP.MacPac.Application.ScreenUpdating = bScreenUpdating;
                        //}
                    }
                    else if (oNode.Tag is Variable || oNode.Tag is Authors ||
                        oNode.Tag is Jurisdictions || oNode.Key.EndsWith(".Language"))
                    {
                        //node is a variable -
                        //set up edit mode
                        EnterEditMode(oNode);
                        

                        //set node as the current node for the edit tab
                        this.m_oCurNode = GetNodeFromTagID(oNode.Key);

                        //GLOG #2873 - dcf - there seems to be a bug with the infragistics controls
                        //such that the parent control is null when closing a document
                        //that is not active - e.g. from the Windows Task Pane -
                        //hence, the condition below -
                        //GLOG 3969 (dm) - added first condition
                        if ((this.treeDocContents.ActiveNode != null) &&
                            (this.treeDocContents.ActiveNode.Control != null))
                        {
                            //select node
                            this.treeDocContents.ActiveNode = this.m_oCurNode;
                        }

                        this.picMenu.Visible = false;
                    }
                    else if (oNode.Tag is Block)
                    {
                        //set node as the current node for the edit tab
                        //GLOG 5744 (dm) - the following line doesn't belong here
                        //because oNode isn't always getting selected in this block -
                        //it resulted in an error when subsequently attempting to select
                        //a newly inserted segment node without some intervening selection
                        //this.m_oCurNode = oNode;

                        if (!this.TaskPane.WordTagChangeOccurring)
                        {
                            //get block - assumes that we've already tested for null
                            Block oBlock = (Block)oNode.Tag;

                            //set help text
                            TaskPane.SetHelpText(this.wbDescription, oBlock.HelpText);

                            //select range associated with 
                            //variable if there is one
                            if (m_oMPDocument.FileFormat ==
                                LMP.Data.mpFileFormats.Binary)
                            {
                                //xml tag
                                Word.XMLNode oWordTag = oBlock.AssociatedWordTag;

                                if (oWordTag != null)
                                {
                                    m_bProgrammaticallySelectingWordTag = true;

                                    //select tag
                                    LMP.Forte.MSWord.WordDoc.SelectTag(oWordTag, m_bHandlingTabPress,
                                        !m_bHandlingTabPress);
                                    //GLOG 2165: If Body text is different from default text,
                                    //only select start of text
                                    if (oBlock.IsBody)
                                    {
                                        if (Session.CurrentWordApp.Selection.Text !=
                                            Expression.Evaluate(oBlock.StartingText,
                                            oBlock.Segment, oBlock.ForteDocument))
                                        {
                                            object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                                            Session.CurrentWordApp.Selection.Collapse(ref oDirection);
                                        }
                                    }
                                    m_bProgrammaticallySelectingWordTag = false;
                                }
                            }
                            else
                            {
                                //content control
                                Word.ContentControl oCC = oBlock.AssociatedContentControl;

                                if (oCC != null)
                                {
                                    m_bProgrammaticallySelectingWordTag = true;

                                    //select tag
                                    this.SelectContentControl(oCC, m_bHandlingTabPress,
                                        !m_bHandlingTabPress);
                                    //GLOG 2165: If Body text is different from default text,
                                    //only select start of text
                                    if (oBlock.IsBody)
                                    {
                                        if (Session.CurrentWordApp.Selection.Text !=
                                            Expression.Evaluate(oBlock.StartingText,
                                            oBlock.Segment, oBlock.ForteDocument))
                                        {
                                            object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                                            Session.CurrentWordApp.Selection.Collapse(ref oDirection);
                                        }
                                    }
                                    m_bProgrammaticallySelectingWordTag = false;
                                }
                            }
                        }
                        if (m_bHandlingTabPress || m_oCurTabNode != null)
                        {
                            //set node as the current node for the edit tab
                            this.m_oCurNode = oNode;

                            //bring into view, if necessary
                            EnsureVisibleNode(oNode);

                            //GLOG #2873 - dcf - there seems to be a bug with the infragistics controls
                            //such that the parent control is null when closing a document
                            //that is not active - e.g. from the Windows Task Pane -
                            //hence, the condition below
                            if (this.treeDocContents.ActiveNode.Control != null)
                            {
                                //select node
                                this.treeDocContents.ActiveNode = this.m_oCurNode;
                            }
                        }
                        else
                        {
                            //GLOG 6066:  Clear m_oCurNode, so that previously selected
                            //TaskPane control will be displayed again if reselected
                            m_oCurNode = null;
                            //Switch focus to Block in Document
                            Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                            //SendKeys.Send("{F10}{F10}");
                        }
                    }
                    else if (oNode.Key.EndsWith("_Value"))
                    {
                        //node is a variable value node - select parent
                        this.treeDocContents.ActiveNode = oNode.Parent;
                    }
                    else if (oNode.Key.EndsWith(".Advanced"))
                    {

                        //set node as the current node for the edit tab
                        this.m_oCurNode = oNode;

                        //scroll into view
                        if (!this.m_oCurNode.IsInView)
                            this.treeDocContents.TopNode = this.m_oCurNode;

                        //GLOG #2873 - dcf - there seems to be a bug with the infragistics controls
                        //such that the parent control is null when closing a document
                        //that is not active - e.g. from the Windows Task Pane -
                        //hence, the condition below
                        if (this.treeDocContents.ActiveNode.Control != null)
                        {
                            //select node
                            this.treeDocContents.ActiveNode = this.m_oCurNode;
                        }
                    }

                    //display node path as status text
                    //GLOG 3856: Avoids error if only top level node is Trailers umbrella node
                    if (this.m_oCurNode != null)
                        this.tslblStatus.Text = this.m_oCurNode.FullPath;
                    else
                        this.tslblStatus.Text = null;

                    this.lblStatus.Text = this.tslblStatus.Text;
                }

                if (oCurSegment != oNewSegment)
                {
                    //current segment has been switched -
                    //raise event if necessary
                    if (this.CurrentTopLevelSegmentSwitched != null)
                    {
                        CurrentSegmentSwitchedEventArgs oArgs = new CurrentSegmentSwitchedEventArgs(
                            oCurSegment, oNewSegment);
                        this.CurrentTopLevelSegmentSwitched(this, oArgs);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }
            finally
            {
                m_bSelectingNode = false;
                m_oCurTabNode = null;
                if (bScreenUpdating)
                    this.TaskPane.ScreenUpdating = true;

                LMP.Benchmarks.Print(t0);
            }
        }
        /// <summary>
        /// returns the top level segment containing the specified node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private Segment GetTopLevelSegmentFromNode(UltraTreeNode oNode)
        {
            return GetTopLevelSegmentFromNode(oNode, false);
        }

        /// <summary>
        /// returns the top level segment containing the specified node -
        /// if specified, treats intended use "as document" as top-level
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bIncludeIntendedAsDocument"></param>
        /// <returns></returns>
        private Segment GetTopLevelSegmentFromNode(UltraTreeNode oNode,
            bool bIncludeIntendedAsDocument)
        {
            Segment oSegment = null;
            if (oNode.Tag is LMP.Architect.Api.Segment)
                oSegment = (Segment)oNode.Tag;
            else if (oNode.Tag is LMP.Architect.Api.Variable)
                oSegment = ((Variable)oNode.Tag).Segment;
            else if (oNode.Tag is LMP.Architect.Api.Block)
                oSegment = ((Block)oNode.Tag).Segment;
            else if (oNode.Tag is LMP.Architect.Api.Authors)
                oSegment = ((Authors)oNode.Tag).Segment;
            else if (oNode.Tag is LMP.Data.Jurisdictions)
            {
                if (oNode.Parent != null)
                {
                    oSegment = (Segment)(oNode.Parent.Tag);
                }
            }
            else if (oNode.Text == "More...")
            {
                //GLOG item #6142 - dcf
                string xKey = oNode.Key.Substring(0, oNode.Key.Length - 9);
                UltraTreeNode oParent = this.treeDocContents.GetNodeByKey(xKey);

                //GLOG : 7644 : ceh
                if (oParent != null)
                    oSegment = (Segment)(oParent.Tag);
            }

            while (oSegment != null)
            {
                //GLOG 6005 (dm) - treat TOA as top-level unconditionally and other
                //"as document" segments as top-level if specified
                if (oSegment.IsTopLevel || (oSegment is LMP.Architect.Api.TOA) ||
                    (bIncludeIntendedAsDocument &&
                    (oSegment.IntendedUse == mpSegmentIntendedUses.AsDocument)))
                    return oSegment;
                else
                    oSegment = oSegment.Parent;
            }

            return oSegment;
        }

        /// <summary>
        /// ensures that the specified node is visible in the tree
        /// </summary>
        /// <param name="oTreeNode"></param>
        private void EnsureVisibleNode(UltraTreeNode oTreeNode)
        {
            UltraTreeNode oParent = oTreeNode;

            while (!oTreeNode.IsInView)
            {
                //get parent
                oParent = oParent.Parent;

                //exit if no more parents
                if (oParent == null)
                    break;

                //expand parent
                oParent.Expanded = true;
            }
        }
        /// <summary>
        /// ensures that the specified node is not visible in the tree
        /// </summary>
        /// <param name="oTreeNode"></param>
        //GLOG item #3820 - df
        private void EnsureHiddenNode(UltraTreeNode oTreeNode)
        {
            UltraTreeNode oParent = oTreeNode;

            while (oTreeNode.IsInView)
            {
                //get parent
                oParent = oParent.Parent;

                //exit if no more parents
                if (oParent == null)
                    break;

                //expand parent
                oParent.Expanded = false;
            }
        }
        /// <summary>
        /// sets up edit mode for the specified  
        /// tree node if necessary and appropriate
        /// </summary>
        /// <param name="oTreeNode"></param>
        private void EnterEditMode(UltraTreeNode oTreeNode)
        {
            if (this.m_oCurNode == oTreeNode)
                //node is the same as current node - exit
                return;

            DateTime t0 = DateTime.Now;

            //ensure that node is visible
            //before entering edit mode
            EnsureVisibleNode(oTreeNode);

            //Clear this so any scroll events generated here won't reposition control
            m_oCurNode = null;
            if (oTreeNode.Tag is Variable)
                EnterVariableEditMode(oTreeNode);
            else if (oTreeNode.Tag is Segment)
            {
                this.HideCallout();
                Segment oSeg = (Segment)oTreeNode.Tag;
                if (oSeg.ShowChooser)
                    EnterSegmentEditMode(oTreeNode);
                else if(this.m_xHelpText != oSeg.HelpText)
                {
                    //GLOG item #5773 - dcf
                    this.m_xHelpText = "";  //oSeg.HelpText;
                    //TaskPane.SetHelpText(this.wbDescription, oSeg.HelpText);
                    TaskPane.SetHelpText(this.wbDescription, "");
                }
            }
            else if (oTreeNode.Tag is Authors)
            {
                this.HideCallout();
                EnterAuthorsEditMode(oTreeNode);
            }
            else if (oTreeNode.Tag is Jurisdictions)
            {
                this.HideCallout();
                EnterJurisdictionEditMode(oTreeNode);
            }
            else if (oTreeNode.Key.EndsWith(".Language"))
            {
                this.HideCallout();
                EnterLanguageEditMode(oTreeNode);
            }
            else
            {
                //node is not a variable node nor an author node
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString(
                    "Error_NodeIsNotVariableOrAuthorNode"));
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// sets up the tree for editing the variable
        /// with the specified tree node
        /// </summary>
        private void EnterVariableEditMode(UltraTreeNode oTreeNode)
        {
            IControl oICtl = null;
            bool bShowCIButton = false;

            //get variable - assumes that we've already tested for null
            Variable oVar = (Variable)oTreeNode.Tag;

            //GLOG 3965 (dm) - ensure that control reflects current value from source
            //prior to potential editing via ui
            oVar.SetValue(oVar.GetValueFromSource(), false);

            //get value node
            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (oVar.AssociatedControl == null)
            {
                //there is no control associated with this variable -
                //create new control and store in value node tag
                oICtl = oVar.CreateAssociatedControl(this);

                //store as current control
                this.m_oCurCtl = (System.Windows.Forms.Control)oICtl;

                //handle events associated with Control Action Events -
                //reinstate these subscriptions if we reinstate the control action events
                //this.m_oCurCtl.Click += new EventHandler(OnControlClick);
                //this.m_oCurCtl.DoubleClick += new EventHandler(OnControlDoubleClick);
                oICtl.KeyPressed += new KeyEventHandler(OnKeyPressed);
                oICtl.KeyReleased += new KeyEventHandler(OnKeyReleased);

                //execute actions associated with FirstDisplay event
                ExecuteControlActions(oVar, ControlActions.Events.FirstDisplay);

                // Set the oValueNode tag to null so that we subscribe to the 
                // events of this newly created associated control below.
                oValueNode.Tag = null;
            }
            else
            {
                //get control
                oICtl = oVar.AssociatedControl;

                //refresh properties of associated control -
                //necessary because some control properties
                //have MacPac expressions as their value -
                //these need to get evaluated
                oVar.SetAssociatedControlProperties();

                //store as current control
                this.m_oCurCtl = (System.Windows.Forms.Control)oICtl;
            }

            if (oValueNode.Tag == null)
            {
                //associated control is not yet a part of the editor -
                //add associated control to tag of value node
                oValueNode.Tag = oICtl;
                m_oControlsHash.Add(oICtl, oValueNode);

                //add subscribe control to dialog events, if necessary
                oICtl.TabPressed += new TabPressedHandler(oIControl_TabPressed);

                //GLOG item #5587 - dcf - limit to one CIRequested subscription per control
                //GLOG item #5902 - dcf - conditioned on the variable specifying a contact type
                if (oICtl is ICIable && !((ICIable)oICtl).IsSubscribedToCI && oVar.CIContactType != 0)
                {
                    ((ICIable)oICtl).CIRequested += new CIRequestedHandler(GetContacts);

                    //Enable double-clicking to Get Contacts
                    ((System.Windows.Forms.Control)oICtl).DoubleClick += new EventHandler(OnControlDoubleClick);
                }
            }

            //Show floating CI button for CI-enabled textboxes -
            if (oICtl is LMP.Controls.TextBox && oVar.CIContactType > 0 && oVar.CIDetailTokenString != "")
                bShowCIButton = true;

            try
            {
                //Mark variable as having been visited
                oVar.MustVisit = false;

                //set control value
                oICtl.Value = oVar.Value;

                //mark as not edited unless changed by control
                //GLOG 7624 (dm) - it makes no sense to compare these values immediately
                //after setting them to be the same and it's causing certain variable
                //actions to run repeatedly and needlessly (because the values will always
                //differ) - for example, multiline controls replace shift-returns with line
                //feeds and InsertText puts them back - another example is the date combo,
                //which adds quotes around the text portion of the date - we could take these
                //differences into account here, but there's no conceivable reason for internal
                //control adjustments to trigger action execution
                //if (oICtl.Value == oVar.Value)
                    oICtl.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }

            //GLOG #3397 - moved this conditional block after the
            //try/catch block above so that control
            //only shows after the value has been set - 
            //show control
            if (m_oControlHost == ControlHost.EditorTree)
                ShowTreeControl(this.m_oCurCtl, oTreeNode, bShowCIButton);
            else
                ShowFloatingControl();

            //execute actions associated with GotFocus event
            ExecuteControlActions(oVar, ControlActions.Events.GotFocus);

            if (this.m_xHelpText != oVar.HelpText)
            {
                this.m_xHelpText = oVar.HelpText;
                TaskPane.SetHelpText(this.wbDescription, oVar.HelpText);
            }

            if (!this.TaskPane.WordTagChangeOccurring)
            {
                //select range associated with 
                //variable if there is one
                object[] aTags = null;
                bool bContentControls = (m_oMPDocument.FileFormat ==
                    LMP.Data.mpFileFormats.OpenXML);

                if (bContentControls)
                    aTags = new Word.ContentControl[0];
                else
                    aTags = new Word.XMLNode[0];

                //GLOG 1408: Use Navigation Tag for focus if defined
                if (oVar.IsTagless && oVar.NavigationTag != "")
                {
                    //GLOG 4419: Allow expression for NavigationTag
                    string xRefVar = GetNavigationTag(oVar);
                    Segment[] oTargetSegments = oVar.Segment.GetReferenceTargets(ref xRefVar);
                    if (oTargetSegments.Length > 0 && xRefVar != "")
                    {
                        Variable oTargetVar = null;
                        try
                        {
                            oTargetVar = oTargetSegments[0].Variables.ItemFromName(xRefVar);
                        }
                        catch { }
                        if (oTargetVar != null)
                        {
                            if (bContentControls)
                                aTags = oTargetVar.AssociatedContentControls;
                            else
                                aTags = oTargetVar.AssociatedWordTags;
                        }
                        else
                        {
                            //Look for Block with specified name - may be mBlock or mDel
                            string xBlockTagID = oTargetSegments[0].FullTagID + "." + xRefVar;
                            try
                            {
                                NodeStore oNodes = oTargetSegments[0].Nodes;
                                if (bContentControls)
                                    aTags = oNodes.GetContentControls(xBlockTagID);
                                else
                                    aTags = oNodes.GetWordTags(xBlockTagID);
                            }
                            catch { }
                        }
                    }
                }
                else
                {
                    if (bContentControls)
                        aTags = oVar.AssociatedContentControls;
                    else
                        aTags = oVar.AssociatedWordTags;
                }

                m_bProgrammaticallySelectingWordTag = true;

                if (aTags.Length > 0 && aTags[0] != null)
                {

                    //select tag range
                    if (bContentControls)
                    {
                        //GLOG 8288: If Variable is multivalue detail, collapse selection to cursor rather than selecting entire first CC.
                        //Also if selection is already positioned within a detail of this variable, don't reposition cursor
                        Variable oSelVar = null;
                        Word.ContentControl oSelCC = Session.CurrentWordApp.Selection.Range.ParentContentControl;
                        if (oSelCC != null)
                        {
                            oSelVar = oVar.Segment.Variables.ItemFromAssociatedContentControl(oSelCC);
                        }
                        if (oSelVar == null || oSelVar.TagID != oVar.TagID)
                        {
                            this.SelectContentControl((Word.ContentControl)aTags[0], aTags.Length > 1 && oVar.IsMultiValue, false);
                        }
                    }
                    else
                    {
                        //GLOG 8288: If Variable is multivalue detail, collapse selection to cursor rather than selecting entire first Tag.
                        //Also if selection is already positioned within a detail of this variable, don't reposition cursor
                        Variable oSelVar = null;
                        Word.XMLNode oSelTag = Session.CurrentWordApp.Selection.Range.XMLParentNode;
                        if (oSelTag != null)
                        {
                            oSelVar = oVar.Segment.Variables.ItemFromAssociatedTag(oSelTag);
                        }
                        if (oSelVar == null || oSelVar.TagID != oVar.TagID)
                        {
                            LMP.Forte.MSWord.WordDoc.SelectTag((Word.XMLNode)aTags[0], aTags.Length > 1 && oVar.IsMultiValue, false);
                        }
                    }
                }
                else
                {
                    //GLOG 7394 (dm) - if there's a currently selected content control, we
                    //need to run exit event handler to prevent document from getting focus
                    //when start of segment is selected
                    Word.ContentControl oSelectionCC = Session.CurrentWordApp
                        .Selection.Range.ParentContentControl;
                    if (oSelectionCC != null)
                    {
                        bool bCancel = false;
                        this.OnContentControlExit(oSelectionCC, ref bCancel, true);
                    }

                    //select beginning of segment
                    //GLOG 6797: Use PrimaryRange instead of calling SelectNode
                    Word.Range oRng = oVar.Segment.PrimaryRange;
                    object oMissing = Missing.Value;
                    oRng.StartOf(ref oMissing, ref oMissing);
                    oRng.Select();

                    //GLOG 7022: If start of Segment is in Content Control such as Body Block,
                    //run OnContentControlEnter immediately with bAdvanceExecution=true,
                    //otherwise m_bProgrammaticallySelectingWordTag will no longer be set
                    //when event handler runs normally and document will get focus.
                    if (bContentControls)
                    {
                        if (oRng.ParentContentControl != null)
                        {
                            OnContentControlEnter(oRng.ParentContentControl, true);
                        }
                    }
                }

                m_bProgrammaticallySelectingWordTag = false;

                //this setup was not triggered by the
                //user clicking on a Word tag - it was
                //triggered by the user working in the task pane -
                //give control the focus
                this.m_oCurCtl.Focus();
                this.m_oCurCtl.Select();
            }
        }

        void OnControlDoubleClick(object sender, EventArgs e)
        {
            try
            {
                GetContacts(sender, new EventArgs());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// sets the browser to the url
        /// </summary>
        private void OnKeyReleased(object sender, KeyEventArgs e)
        {
            try
            {
                // Prevent entering hotkey mode if we are releasing the 
                // control key after using it while selecting text 
                // (eg conrol-shift-right arrow).
                if (IsInControlShiftEditMode && e.KeyValue == 17)
                {
                    IsInControlShiftEditMode = false;
                    this.DisableHotKeyMode = false;
                    return;
                }

                if (this.DisableHotKeyMode)
                    return;

                //Ignore Ctrl in combination with other keys
                if (e.KeyValue == 17 && !e.Alt && !m_bHotKeyPressed)
                {
                    //control key press has ended
                    //without a hotkey pressed - 
                    //toggle hotkey mode
                    if (this.m_bInHotkeyMode)
                        ExitHotkeyMode();
                    else
                        EnterHotkeyMode();
                }
                else if (m_bHotKeyPressed)
                {
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                    GetContacts(m_oCurCtl, new EventArgs());

                    // GLOG : 3411 : JAB
                    // Return focus to the current control.
                    if (this.m_oCurCtl != null)
                    {
                        this.m_oCurCtl.Focus();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// executes actions that should occur
        /// when a key is pressed, specifically hotkeys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyPressed(object sender, KeyEventArgs e)
        {
            try
            {
                m_bHotKeyPressed = false;

                if ((m_bInHotkeyMode || (e.Modifiers == Keys.Control)) && (e.KeyCode != Keys.ControlKey))
                {
                    if (e.KeyCode == Keys.F1 && e.Control)
                    {
                        // Control F1 navigates focus to the Find textbox of the content manager.
                        this.TaskPane.ActivateFind();
                    }
                    else if (e.KeyCode == Keys.F3 && e.Control)
                    {
                        this.TaskPane.Toggle();
                    }
                    else
                    {
                        //either we're in hotkey mode, or the control key 
                        //was pressed in conjunction with another key - 
                        //execute hotkey
                        string xKey = ((char)e.KeyValue).ToString().ToUpper();
                        //Process any system-defined keystrokes normally
                        if (@"ABDEFGHIJKLMNOPQRSTUW123456789".Contains(xKey))
                        {
                            //mark that hotkey was pressed
                            m_bHotKeyPressed = true;
                            e.SuppressKeyPress = true;
                            this.ExecuteHotkey(((char)e.KeyValue).ToString());
                        }
                        else if (xKey != "")
                        {
                            //Mark as hotkey, so labels won't toggle when Ctrl is released
                            m_bHotKeyPressed = true;
                        }
                    }
                }
                else if (e.KeyCode == Keys.F5 && e.Modifiers == Keys.None)
                {
                    //GLOG 2768, 8/1/08
                    TaskPanes.RefreshAll();
                    this.RefreshTree();
                    this.Focus();
                }
                else if (e.Control && e.Shift)
                {
                    //Ctrl+Shift shortcuts defined for the control
                    //Home - move to first variable
                    //End - move to last variable
                    //Left - Collapse tree to initial state
                    //Right - Expand all nodes in tree
                    //get state of Home key
                    int iHomeKeyState = LMP.WinAPI.GetKeyState(KEY_HOME);
                    bool bHomeKeyPressed = !(iHomeKeyState == 1 || iHomeKeyState == 0);
                    //get state of End key
                    int iEndKeyState = LMP.WinAPI.GetKeyState(KEY_END);
                    bool bEndKeyPressed = !(iEndKeyState == 1 || iEndKeyState == 0);
                    //get state of Left key
                    int iLeftKeyState = LMP.WinAPI.GetKeyState(KEY_LEFT);
                    bool bLeftKeyPressed = !(iLeftKeyState == 1 || iLeftKeyState == 0);
                    //get state of Right key
                    int iRightKeyState = LMP.WinAPI.GetKeyState(KEY_RIGHT);
                    bool bRightKeyPressed = !(iRightKeyState == 1 || iRightKeyState == 0);
                    //get state of B key (select body)
                    int iBKeyState = LMP.WinAPI.GetKeyState(66);
                    bool bBKeyPressed = !(iBKeyState == 1 || iBKeyState == 0);
                    if (bHomeKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        //GLOG - 3338 - CEH
                        //GLOG 7774 (dm) - NewSegment may be null in Forte Tools
                        if ((TaskPane.NewSegment != null) && (TaskPane.NewSegment.ShowChooser))
                            //set focus to chooser
                            this.SelectChooserNode();
                        else
                            SelectFirstVariableNode();
                    }
                    else if (bEndKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        SelectLastVariableNode();
                    }
                    else if (bBKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        SelectBodyNode(m_oCurNode);
                    }
                    else
                    {
                        // The control key is being pressed while editting 
                        // (eg ctrl-shift-right arrow to select text) to 
                        // select text. Enter the IsInControlShiftEditMode 
                        // to prevent the release of the control key activating
                        // the hot key mode.
                        this.DisableHotKeyMode = true;
                        IsInControlShiftEditMode = true;
                    }
                }
                else if (e.Control && e.Alt)
                {
                    if (e.KeyCode == Keys.Up)
                    {
                        this.DisableHotKeyMode = true;
                        this.CollapseAll();
                    }
                    else if (e.KeyCode == Keys.Down)
                    {
                        this.DisableHotKeyMode = true;
                        this.ExpandAll();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void ExecuteHotkey(string xHotkey)
        {
            try
            {
                //get currently selected node for later comparison
                UltraTreeNode oStartingNode = this.m_oCurNode;

                if (this.m_oHotkeys.ContainsKey(xHotkey))
                {
                    //get node associated with the pressed key - 
                    //the hash key can be assigned either a node or
                    //an ArrayList of nodes
                    UltraTreeNode oNode = null;
                    if (this.m_oHotkeys[xHotkey] is UltraTreeNode)
                        oNode = (UltraTreeNode)this.m_oHotkeys[xHotkey];
                    else
                    {
                        //there are multiple nodes assigned to this hotkey - 
                        //get the next one, ie the one after the current node
                        ArrayList oHotkeyNodesArray = (ArrayList) this.m_oHotkeys[xHotkey];
                        //int iCurNodeIndex = this.m_oCurNode.Index;
                        UltraTreeNode oNextNearestNode = null;
                        for (int i = 0; i < oHotkeyNodesArray.Count; i++)
                        {
                            UltraTreeNode oHotkeyNode = (UltraTreeNode)oHotkeyNodesArray[i];
                            //GLOG 3589: Node may not exist anymore if Segment has been refreshed.
                            //Ignore any obsolete Nodes.
                            if (oHotkeyNode.Control != null)
                                //set node as nearest node if it comes after the current
                                //node and either there is no next nearest node or this
                                //node is nearer than the current next nearest node
                                if (NodeIsBelowNode(oHotkeyNode, m_oCurNode) && (oNextNearestNode == null ||
                                    NodeIsBelowNode(oNextNearestNode, oHotkeyNode)))
                                    oNextNearestNode = oHotkeyNode;
                        }

                        if (oNextNearestNode != null)
                            oNode = oNextNearestNode;
                        else
                        {
                            //the next nearest node is before the current node
                            for (int i = 0; i < oHotkeyNodesArray.Count; i++)
                            {
                                UltraTreeNode oHotkeyNode = (UltraTreeNode)oHotkeyNodesArray[i];

                                // GLOG : 553 : JAB
                                // The selected node must be part of the tree control in order
                                // for it to be selectable. A node that is part of the tree has
                                // Control 
                                if (oHotkeyNode.Control != null)
                                {
                                    //set node to first match in the tree
                                    if (oNextNearestNode == null ||
                                        NodeIsBelowNode(oNextNearestNode, oHotkeyNode))
                                        oNextNearestNode = oHotkeyNode;
                                }
                            }

                            oNode = oNextNearestNode;
                        }
                    }

                    if (oNode != null)
                        //select node
                        SelectNode(oNode);
                }

                if (this.m_oCurNode != oStartingNode)
                    //a new node was selected -
                    //exit hotkey mode
                    ExitHotkeyMode();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UIException(
                    LMP.Resources.GetLangString("Error_CouldNotExecuteHotkey") + 
                    xHotkey, oE);
            }
        }
        /// <summary>
        /// Returns true of oNode1 appears below oNode2 in tree
        /// </summary>
        /// <param name="oNode1"></param>
        /// <param name="oNode2"></param>
        /// <returns></returns>
        private bool NodeIsBelowNode(UltraTreeNode oNode1, UltraTreeNode oNode2)
        {
            if (oNode2.IsAncestorOf(oNode1))
                //Parent node has to be above
                return true;
            else if (oNode1.IsAncestorOf(oNode2))
                //Child node has to be below
                return false;
            else if (oNode1.Parent == oNode2.Parent)
            {
                //If both nodes are in same collection, compare Index of each node
                return (oNode1.Index > oNode2.Index);
            }
            else if (oNode1.Level == oNode2.Level)
            {
                //If nodes are at the same level, compare Position of each parent
                return NodeIsBelowNode(oNode1.Parent, oNode2.Parent);
            }
            else if (oNode2.Level > oNode1.Level)
            {
                //If oNode2 is at a lower level, compare Position of oNode1
                //and oNode2.Parent
                return NodeIsBelowNode(oNode1, oNode2.Parent);
            }
            else if (oNode2.Level < oNode1.Level)
            {
                //If oNode1 is at a lower level, compare Position of oNode2
                //and oNode1.Parent
                return NodeIsBelowNode(oNode1.Parent, oNode2);
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// puts the editor into hotkey mode
        /// </summary>
        private void EnterHotkeyMode()
        {
            DateTime t0 = DateTime.Now;

            //cycle through all nodes that have hotkeys
            IEnumerator oEnumerator = this.m_oHotkeys.Values.GetEnumerator();

            bool bNextExists = oEnumerator.MoveNext();
            while (bNextExists)
            {
                if (oEnumerator.Current is UltraTreeNode)
                {
                    //show hotkey for node - ie add hotkey image
                    UltraTreeNode oNode = (UltraTreeNode)oEnumerator.Current;

                    //get the validation image for the node
                    Images oValidationImage = (Images)oNode.LeftImages[0];

                    Images oHotkeyImage = GetHotkeyImage(oNode);

                    if(oHotkeyImage != null)
                        //get add the hotkey image to the node
                        oNode.LeftImages.Add(oHotkeyImage);
                }
                else
                {
                    //value is array list of nodes - cycle through
                    //list adding images to each
                    ArrayList oArrayList = (ArrayList) oEnumerator.Current;

                    for (int i = 0; i < oArrayList.Count; i++)
                    {
                        UltraTreeNode oNode = (UltraTreeNode)oArrayList[i];

                        //get the validation image for the node
                        Images oImage = (Images)oNode.LeftImages[0];

                        Images oHotkeyImage = GetHotkeyImage(oNode);

                        if (oHotkeyImage != null && oHotkeyImage != Images.NoImage)
                            //get add the hotkey image to the node
                            oNode.LeftImages.Add(GetHotkeyImage(oNode));
                    }
                }

                bNextExists = oEnumerator.MoveNext();
            }

            //update value of currently selected variable and control if necessary -
            //removed with GLOG item 2601 - it's not clear why this is necessary
            //when focus is already in the task pane
            //this.UpdateCurrentVariableValueFromDocument();

            LMP.Benchmarks.Print(t0);

            m_bInHotkeyMode = true;
        }
        /// <summary>
        /// takes the editor out of hotkey mode
        /// </summary>
        private void ExitHotkeyMode()
        {
            //cycle through all nodes that have hotkeys
            IEnumerator oEnumerator = this.m_oHotkeys.Values.GetEnumerator();

            bool bNextExists = oEnumerator.MoveNext();
            while (bNextExists)
            {
                if (oEnumerator.Current is UltraTreeNode)
                {
                    //remove hotkey image
                    UltraTreeNode oNode = (UltraTreeNode)oEnumerator.Current;
                    oNode.LeftImages.Remove(GetHotkeyImage(oNode));
                }
                else
                {
                    //value is array list of nodes - cycle through
                    //list adding images to each
                    ArrayList oArrayList = (ArrayList)oEnumerator.Current;

                    for (int i = 0; i < oArrayList.Count; i++)
                    {
                        UltraTreeNode oNode = (UltraTreeNode)oArrayList[i];
                        oNode.LeftImages.Remove(GetHotkeyImage(oNode));
                    }
                }
                bNextExists = oEnumerator.MoveNext();
            }

            m_bInHotkeyMode = false;
        }
        /// <summary>
        /// returns the hotkey image associated with the specified tree node
        /// </summary>
        /// <param name="oNode"></param>
        private Images GetHotkeyImage(UltraTreeNode oNode)
        {
            string xHotkey = "";
            if (oNode.Tag is LMP.Architect.Api.Authors)
                xHotkey = "U";
            else if (oNode.Tag is Jurisdictions)
                xHotkey = "J";
            else if (oNode.Key.EndsWith(".Language"))
                xHotkey = "L";
            else
            {
                if (oNode.Tag is Variable)
                    xHotkey = ((Variable)oNode.Tag).Hotkey;
                else
                    xHotkey = ((Block)oNode.Tag).Hotkey;
            }
        
            if(!System.String.IsNullOrEmpty(xHotkey))
                return (Images)Enum.Parse(typeof(Images), "Hotkey" + xHotkey);
            else
                return Images.NoImage;
        }
        /// <summary>
        /// sets up the tree for editing authors
        /// </summary>
        private void EnterAuthorsEditMode(UltraTreeNode oTreeNode)
        {
            //get authors - assumes that we've already tested for null
            Authors oAuthors = (Authors)oTreeNode.Tag;

            //get value node
            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (oValueNode.Tag == null)
            {
                //there is no control stored in the value node tag -
                //create new control and store in value node tag
                oValueNode.Tag = SetupAuthorControl(oAuthors);
                //Add to hash table
                m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            }
            else
            {
                AuthorSelector oAuthorSelector = oValueNode.Tag as AuthorSelector;
                //set jurisdiction if the bar id column is displayed
                if (oAuthorSelector != null && (oAuthorSelector.ShowColumns & mpAuthorColumns.BarID) > 0)
                {
                    Segment oLevelSegment = oAuthors.Segment;
                    //GLOG 3316: If Pleading Table item, get Court Levels from parent pleading
                    if (oLevelSegment is CollectionTableItem)
                    {
                        while (oLevelSegment.Parent != null)
                            oLevelSegment = oLevelSegment.Parent;
                    }
                    oAuthorSelector.Jurisdiction = new int[] { oLevelSegment.L0, oLevelSegment.L1, oLevelSegment.L2, oLevelSegment.L3, oLevelSegment.L4 };
                }
            }

            //get the control to show
            this.m_oCurCtl = (System.Windows.Forms.Control)oValueNode.Tag;

            //add space to display control
            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                this.m_oCurCtl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            //force tree to redraw to get a 
            //valid vertical reading for scroll bars
            this.treeDocContents.Refresh();
            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();

            int iControlWidth = 0;

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - (int)(20 * Session.ScreenScalingFactor);
            else
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - (int)(5 * Session.ScreenScalingFactor);

            //position control to display over variable value
            this.m_oCurCtl.SetBounds(oValueNode.Bounds.Left,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, this.m_oCurCtl.Height);

            //clear value node text for display
            oValueNode.Text = "";

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;

                oCtl.Value = oAuthors.XML;
                oCtl.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                //add control to container
                this.treeDocContents.Controls.Add(this.m_oCurCtl);

                if (this.m_xHelpText != oAuthors.Segment.AuthorsHelpText)
                {
                    this.m_xHelpText = oAuthors.Segment.AuthorsHelpText;
                    TaskPane.SetHelpText(this.wbDescription, oAuthors.Segment.AuthorsHelpText);
                }

                //GLOG 7394 (dm) - if there's a currently selected content control, we
                //need to run exit event handler to prevent document from getting focus
                //when start of segment is selected
                Word.ContentControl oSelectionCC = Session.CurrentWordApp
                    .Selection.Range.ParentContentControl;
                if (oSelectionCC != null)
                {
                    bool bCancel = false;
                    this.OnContentControlExit(oSelectionCC, ref bCancel, true);
                }

                //select beginning of segment
                Word.Range oRng = oAuthors.Segment.PrimaryRange;
                object oMissing = Missing.Value;
                oRng.StartOf(ref oMissing, ref oMissing);
                oRng.Select();

                //GLOG : 7147 : CEH
                //task pane control focus & selection
                //should happen after segment selection

                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }


            }
        }
        /// <summary>
        /// sets up the tree for editing authors
        /// </summary>
        private void EnterJurisdictionEditMode(UltraTreeNode oTreeNode)
        {
            Segment oSegment = (Segment)oTreeNode.Parent.Tag;

            //get value node
            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (oValueNode.Tag == null)
            {
                //there is no control stored in the value node tag -
                //create new control and store in value node tag
                oValueNode.Tag = SetupJurisdictionControl(oSegment);

                //Add to hash table
                m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            }

            //get the control to show
            this.m_oCurCtl = (System.Windows.Forms.Control)oValueNode.Tag;

            //add space to display control
            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                this.m_oCurCtl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            //force tree to redraw to get a 
            //valid vertical reading for scroll bars
            this.treeDocContents.Refresh();
            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();

            int iControlWidth = 0;

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - (int)(20 * Session.ScreenScalingFactor);
            else
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - (int)(5 * Session.ScreenScalingFactor);

            //When TaskPane in Initializing in Word 2003, width may not yet be finalized
            //when Initial control is selected.  Set to arbitrary width to avoid problems
            if (iControlWidth <= 0)
                iControlWidth = 192;

            this.treeDocContents.Refresh();
            //position control to display over variable value
            this.m_oCurCtl.SetBounds(oValueNode.Bounds.Left,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, this.m_oCurCtl.Height);

            //clear value node text for display
            oValueNode.Text = "";
            //oValueNode.Enabled = true;

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;
                oCtl.Value = oSegment.GetJurisdictionLevels();
                oCtl.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                //add control to container
                this.treeDocContents.Controls.Add(this.m_oCurCtl);

                if (oSegment != null && (this.m_xHelpText != oSegment.CourtChooserHelpText))
                {
                    this.m_xHelpText = oSegment.CourtChooserHelpText;
                    TaskPane.SetHelpText(this.wbDescription, oSegment.CourtChooserHelpText);
                }

                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }
            }
        }
        /// <summary>
        /// sets up the tree for editing the segment
        /// </summary>
        /// <param name="oTreeNode"></param>
        private void EnterSegmentEditMode(UltraTreeNode oTreeNode)
        {
            //get segment - assumes that we've already tested for null
            Segment oSeg = (Segment)oTreeNode.Tag;

            //get the control to show
            Chooser oChooser = (Chooser)new Chooser();
            //Add to hash table
            m_oControlsHash.Add(oChooser, oTreeNode);
            oChooser.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oChooser.KeyReleased += new KeyEventHandler(OnKeyReleased);

            // Subscribe to the chooser controls control-left arrow event
            // so that we can collapse the node.
            oChooser.OnCtrlLeftArrowPressed +=
                new Chooser.CtrlLeftArrowPressedHandler(OnChooserCtlLeftArrowPressed);

            // Subscribe to the chooser controls control-right arrow event
            // so that we can expand the node.
            oChooser.OnCtrlRightArrowPressed +=
                new Chooser.CtrlRightArrowPressedHandler(OnChooserCtrlRightArrowPressed);

            Segment oTarget = oSeg.Parent;

            if (oTarget != null)
            {
                //filter assignments based on the containing parent segment
                if ((oTarget is CollectionTable) && (oTarget.Parent != null))
                    oTarget = oTarget.Parent;
                oChooser.TargetObjectType = oTarget.TypeID;
                oChooser.TargetObjectID = oTarget.ID1;

                //GLOG 6264: For Add-on Segments (Exhibits, TOA) display Pleading Paper types assigned to Parent Segment,
                //or first Top-Level Litigation Segment if no Parent
                if (oTarget is LMP.Architect.Base.ILitigationAddOnSegment && oSeg is PleadingPaper)
                {
                    if (oTarget.Parent != null && oTarget.Parent.FindChildren(mpObjectTypes.PleadingPaper).GetLength(0) > 0)
                    {
                        oChooser.TargetObjectType = oTarget.Parent.TypeID;
                        oChooser.TargetObjectID = oTarget.Parent.ID1;
                    }
                    else
                    {
                        for (int s = 0; s < oSeg.ForteDocument.Segments.Count; s++)
                        {
                            Segment oLitSeg = oSeg.ForteDocument.Segments[s];
                            if (!(oLitSeg is LMP.Architect.Base.ILitigationAddOnSegment) && oLitSeg.IntendedUse == mpSegmentIntendedUses.AsDocument &&
                                oLitSeg.FindChildren(mpObjectTypes.PleadingPaper).GetLength(0) > 0)
                            {
                                oChooser.TargetObjectType = oLitSeg.TypeID;
                                oChooser.TargetObjectID = oLitSeg.ID1;
                                break;
                            }
                        }
                    }
                }
            }
            else if (oSeg is LMP.Architect.Base.IDocumentStamp)
            {
                //Trailers/Document Stamp assignments work differently
                if (Session.CurrentWordApp.Selection.StoryType != Microsoft.Office.Interop.Word.WdStoryType.wdMainTextStory)
                {
                    //GLOG 3251:  Look in main story for top level segment, as selection might
                    //currently be in the trailer itself
                    //GLOG 4215: Don't change current selection - we don't want to close footer if open
                    //GLOG 6930: Get Section Index from PrimaryRange
                    int iSection = oSeg.PrimaryRange.Sections[1].Index;

                    Word.Range oSecRange = oSeg.ForteDocument.WordDocument.Sections[iSection].Range;
                    object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                    oSecRange.Collapse(ref oDirection);
                    oTarget = oSeg.ForteDocument.GetTopLevelSegmentForRange(oSecRange);
                }
                else
                    oTarget = oSeg.ForteDocument.GetTopLevelSegmentFromSelection();
                Assignments oAssignments = null;
                if (oTarget == oSeg)
                {
                    oTarget = null;

                }
                if (oTarget != null)
                {
                    for (int i = 0; i < oTarget.Segments.Count; i++)
                    {
                        //Check for any Paper-type children with assignments first
                        if (oTarget.Segments[i] is Paper)
                        {
                            //get assigned stamps for the child segment
                            oAssignments = new Assignments(oTarget.Segments[i].TypeID,
                                oTarget.Segments[i].ID1, oSeg.TypeID);
                            break;
                        }
                    }

                    if (oAssignments == null || oAssignments.Count == 0)
                    {
                        //get assigned stamps for the segment if no assignments based on Paper
                        oAssignments = new Assignments(oTarget.TypeID,
                            oTarget.ID1, oSeg.TypeID);
                    }
                }
                const char ITEM_SEP = '~';
                const char NAME_VALUE_SEP = ';';
                string xSegmentList = "";
                AdminSegmentDefs oDefs = new AdminSegmentDefs(oSeg.TypeID);
                //Build segment list from Assignments or Firm application setting default list
                if (oAssignments == null || oAssignments.Count == 0)
                {
                    //if no segment-specific assignments, show types selected
                    //in Firm Application Settings
                    FirmApplicationSettings oSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                    if (oSeg.TypeID == mpObjectTypes.DraftStamp)
                    {
                        for (int i = 0; i < oSettings.DefaultDraftStampIDs.GetLength(0); i++)
                        {
                            int iID = oSettings.DefaultDraftStampIDs[i];
                            AdminSegmentDef oDef = null;
                            try
                            {
                                oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                            }
                            catch { }
                            if (oDef != null)
                            {
                                xSegmentList += oDef.DisplayName + NAME_VALUE_SEP + oDef.ID.ToString() + ITEM_SEP;
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < oSettings.DefaultTrailerIDs.GetLength(0); i++)
                        {
                            int iID = oSettings.DefaultTrailerIDs[i];
                            AdminSegmentDef oDef = null;
                            try
                            {
                                oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                            }
                            catch { }
                            if (oDef != null)
                            {
                                xSegmentList += oDef.DisplayName + NAME_VALUE_SEP + oDef.ID.ToString() + ITEM_SEP;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < oAssignments.Count; i++)
                    {
                        int iID = ((Assignment)oAssignments.ItemFromIndex(i)).AssignedObjectID;
                        AdminSegmentDef oDef = null;
                        try
                        {
                            oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                        }
                        catch { }
                        if (oDef != null)
                        {
                            xSegmentList += oDef.DisplayName + NAME_VALUE_SEP + oDef.ID.ToString() + ITEM_SEP;
                        }
                    }
                }
                //If this is blank, then chooser will display all available items
                if (xSegmentList != "")
                {
                    xSegmentList = xSegmentList.TrimEnd(ITEM_SEP);
                    oChooser.SegmentsList = xSegmentList;
                }
            }
            else
            {
                //segment is top level and not a document stamp-
                //get all assignments for itself.
                oChooser.TargetObjectType = oSeg.TypeID;
                oChooser.TargetObjectID = oSeg.ID1;
            }

            //set assigned object type
            oChooser.AssignedObjectType = oSeg.TypeID;
            //load list of segments
            oChooser.ExecuteFinalSetup();

            //select chooser value
            oChooser.Value = oSeg.ID1.ToString();

            //set as current control
            this.m_oCurCtl = (System.Windows.Forms.Control)oChooser;

            m_bRepositioningControl = true;
            //add space to display control
            oTreeNode.Override.NodeSpacingAfter =
                Math.Max(oTreeNode.NodeSpacingAfterResolved,
                    this.m_oCurCtl.Height);

            //force tree to redraw so that scroll bar
            //visibility will be reflected in UI
            this.treeDocContents.Refresh();
            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();
            int iControlWidth = 0;

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oTreeNode.Bounds.Left - this.treeDocContents.Indent - (int)(20 * Session.ScreenScalingFactor);
            else
                iControlWidth = this.treeDocContents.Width -
                    oTreeNode.Bounds.Left - this.treeDocContents.Indent - (int)(5 * Session.ScreenScalingFactor);

            //scroll control into view, if necessary
            if (oTreeNode.Bounds.Top + oTreeNode.Bounds.Height + this.m_oCurCtl.Height >
                this.treeDocContents.ClientRectangle.Bottom)
            {
                //bottom of control won't be visible -
                //scroll to top of next node, if it exists
                UltraTreeNode oNode = oTreeNode.NextVisibleNode;

                if (oNode != null)
                {
                    //skip value nodes - value node tags are IControls
                    while (oNode.Tag is IControl)
                        oNode = oNode.NextVisibleNode;

                    oNode.BringIntoView();
                }
                else
                {
                    oTreeNode.BringIntoView();
                }
            }

            //position control to display over variable value
            this.m_oCurCtl.SetBounds(oTreeNode.Bounds.Left + this.treeDocContents.Indent,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, this.m_oCurCtl.Height);

            //add control to container
            this.treeDocContents.Controls.Add(this.m_oCurCtl);

            //GLOG 5239 (dm) - added condition that oSeg.Definition != null
            //GLOG : 6198 : CEH - help text needs to update if empty
            if (oSeg.Definition != null)
            {
                if ((this.m_xHelpText != oSeg.Definition.HelpText) ||
                   (this.wbDescription.DocumentText == ""))
                {
                    //display segment help text here
                    this.m_xHelpText = oSeg.Definition.HelpText;
                    TaskPane.SetHelpText(this.wbDescription, oSeg.Definition.HelpText);
                }
            }

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;

                //TODO:set control value

                //mark as not edited
                oCtl.IsDirty = false;

                //hook up to event that handles chooser selection event-
                //if, indeed, the chooser is associated with the only node in the tree
                TreeNodesCollection oNodes = this.treeDocContents.Nodes;

                //GLOG 2750: Chooser changes reflected immediately, rather than waiting for tab    
                oChooser.ValueChanged += new ValueChangedHandler(OnChooserSelectionChanged);
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }
                m_bRepositioningControl = false;
            }
        }

        /// <summary>
        /// sets up the tree for editing language
        /// </summary>
        private void EnterLanguageEditMode(UltraTreeNode oTreeNode)
        {
            Segment oSegment = (Segment)oTreeNode.Parent.Tag;

            //get value node
            UltraTreeNode oValueNode = oTreeNode.Nodes[0];

            if (oValueNode.Tag == null)
            {
                //there is no control stored in the value node tag -
                //create new control and store in value node tag
                oValueNode.Tag = SetupLanguageControl(oSegment);

                //Add to hash table
                m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            }

            //get the control to show
            this.m_oCurCtl = (System.Windows.Forms.Control)oValueNode.Tag;

            //add space to display control
            oValueNode.Override.NodeSpacingAfter =
                Math.Max(oValueNode.NodeSpacingAfterResolved, +
                this.m_oCurCtl.Height - oValueNode.NodeSpacingAfterResolved - 1);

            //force tree to redraw to get a 
            //valid vertical reading for scroll bars
            this.treeDocContents.Refresh();
            //Can't use DoEvents while OnKeyPressed is still processing
            if (!m_bHotKeyPressed)
                System.Windows.Forms.Application.DoEvents();

            int iControlWidth = 0;

            //set control width based on scroll bar visibility
            if (this.ScrollBarsVisible)
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - (int)(20 - Session.ScreenScalingFactor);
            else
                iControlWidth = this.treeDocContents.Width -
                    oValueNode.Bounds.Left - (int)(5 - Session.ScreenScalingFactor);

            //When TaskPane in Initializing in Word 2003, width may not yet be finalized
            //when Initial control is selected.  Set to arbitrary width to avoid problems
            if (iControlWidth <= 0)
                iControlWidth = 192;

            this.treeDocContents.Refresh();
            //position control to display over variable value
            this.m_oCurCtl.SetBounds(oValueNode.Bounds.Left,
                oTreeNode.Bounds.Top + oTreeNode.Bounds.Height,
                iControlWidth, this.m_oCurCtl.Height);

            //clear value node text for display
            oValueNode.Text = "";
            //oValueNode.Enabled = true;

            try
            {
                IControl oCtl = this.m_oCurCtl as IControl;
                oCtl.Value = oSegment.Culture.ToString();
                oCtl.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                System.Exception oEOuter = new LMP.Exceptions.ControlValueException(
                    LMP.Resources.GetLangString("Error_CouldNotSetControlValue"), oE);

                LMP.Error.Show(oEOuter);
            }
            finally
            {
                //add control to container
                this.treeDocContents.Controls.Add(this.m_oCurCtl);

                if (oSegment != null && (this.m_xHelpText != oSegment.CourtChooserHelpText))
                {
                    this.m_xHelpText = oSegment.CourtChooserHelpText;
                    TaskPane.SetHelpText(this.wbDescription, oSegment.CourtChooserHelpText);
                }

                if (!this.TaskPane.WordTagChangeOccurring)
                {
                    //this setup was not triggered by the
                    //user clicking on a Word tag - it was
                    //triggered by the user working in the task pane -
                    //give control the focus
                    this.m_oCurCtl.Focus();
                    this.m_oCurCtl.Select();
                }
            }
        }

        void OnChooserCtrlRightArrowPressed(object sender)
        {
            // Expand the nodes in response to a control-right arrow key stroke.
            this.m_oCurNode.Expanded = true;
        }

        void OnChooserCtlLeftArrowPressed(object sender)
        {
            // Collapse the nodes in response to a control-left arrow key stroke.
            this.m_oCurNode.Expanded = false;
        }

        void OnChooserSelectionChanged(object sender, EventArgs e)
        {
            //Automatically run actions for Chooser, without requiring tabbing out
            try
            {
                System.Windows.Forms.Application.DoEvents();
                UltraTreeNode oNode = ExitEditMode();
                if (oNode != null)
                {
                    Segment oSeg = oNode.Tag as Segment;
                    if (oSeg != null)
                    {
                        if (oSeg.IntendedUse == mpSegmentIntendedUses.AsDocument)
                            //If Shell segment, select first variable
                            SelectFirstNodeForSegment(oSeg);
                        else
                        {
                            //else keep Chooser selected
                            m_oCurNode = null;
                            m_oCurCtl = null;
                            SelectNode(oNode);
                        }
                        System.Windows.Forms.Application.DoEvents();
                    }
                }
                //this.HandleTabPressed(false);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// exits edit mode -
        /// resets the current node in the tree -
        /// i.e. if the current node is a variable,
        /// sets variable value if necessary,
        /// removes associated control, and respaces node
        /// </summary>
        /// <returns>new segment node if changed by a chooser control</returns>
        internal UltraTreeNode ExitEditMode()
        {
            DateTime t0 = DateTime.Now;

            m_bExitingEditMode = true;
            string xCurNodeName = "";

            if(m_oCurNode != null)
                xCurNodeName = this.m_oCurNode.Text;

            UltraTreeNode oNewNode = null;

            //GLOG 3833
            UltraTreeNode oNode = null;

            bool bScreenUpdating = this.TaskPane.ScreenUpdating;

            //exit if there is no current tree node, or if currently tabbing through nodes
            //GLOG 5531 (dm) - this method shouldn't be running before oFocusForm is initialized
            if (this.m_oCurNode == null || this.m_oCurTabNode != null ||
                oFocusForm == null)
            {
                m_bExitingEditMode = false;
                return oNewNode;
            }

            //JTS 7/26/10: Don't continue if Word Document has already been closed
            try
            {
                string xTestPath = this.ForteDocument.WordDocument.Path;
            }
            catch
            {
                m_bExitingEditMode = false;
                return oNewNode;
            }
            try
            {
                if (!m_bHotKeyPressed)
                    ExitHotkeyMode();

                //ensure that node is visible
                //before exiting edit mode
                EnsureVisibleNode(this.m_oCurNode);

                //Clean up the temp Callout if currently displayed
                this.HideCallout();
                object oCurNodeTag = this.m_oCurNode.Tag;

                //get referred variable
                if (oCurNodeTag is Variable || oCurNodeTag is Authors ||
                    oCurNodeTag is Segment || oCurNodeTag is Jurisdictions ||
                    this.m_oCurNode.Key.EndsWith(".Language"))
                {
                    UltraTreeNode oCurValueNode = null;

                    if (!(oCurNodeTag is Segment))
                    {
                        //get current value node
                        oCurValueNode = this.m_oCurNode.Nodes[0];

                        if (oCurValueNode == null)
                        {
                            //something is wrong - all variable and author
                            //nodes need to have a child value node
                            throw new LMP.Exceptions.UINodeException(
                                LMP.Resources.GetLangString("Error_MissingValueNode") +
                                this.m_oCurNode.FullPath);
                        }

                        //restore to original space after value
                        if (m_bShowValueNodes)
                            oCurValueNode.Override.NodeSpacingAfter = 10;
                        else
                            this.m_oCurNode.Override.NodeSpacingAfter = 10;
                    }

                    //clear out help text
                    //GLOG 7214
                    TaskPane.SetHelpText(wbDescription, "");

                    //save Word environment parameters for later restoration
                    LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                        m_oMPDocument.WordDocument);

                    oEnv.SaveState();

                    //deal with current control
                    if (this.m_oCurCtl != null)
                    {
                        this.btnContacts.Visible = false;
                        IControl oCtl = (IControl)this.m_oCurCtl;

                        try
                        {
                            this.TaskPane.ScreenUpdating = false;

                            oFocusForm.Visible = true;

                            //if (this.m_oFloatingForm != null)
                            //{
                            //    //hide floating form
                            //    this.m_oFloatingForm.Hide();

                            //    //remove control from floating form
                            //    this.m_oFloatingForm.Controls.Clear();
                            //}

                            //refresh value node text -
                            if (oCurNodeTag is Variable)
                            {
                                Variable oVar = (Variable)oCurNodeTag;

                                string xBaseName = null;

                                //GLOG 6829 (dm) - if error occurred when getting associated tags/ccs,
                                //this variable was mistaken as a tagless variable by design and
                                //update criteria below were met - to prevent this, initialize to -1
                                int iBoundingObjectCount = -1;

                                //5-5-11 (dm) - I moved the try/catch to encompass
                                //all of the following code - we were previously only
                                //trapping errors due to an associated tag or cc that
                                //no longer exists, but the error may occur when getting
                                //the collection of associated tags/ccs
                                try
                                {
                                    if (oVar.ForteDocument.FileFormat == mpFileFormats.Binary)
                                    {
                                        iBoundingObjectCount = oVar.AssociatedWordTags.Length;

                                        if (iBoundingObjectCount > 0)
                                        {
                                            //check that tag hasn't been deleted
                                            xBaseName = oVar.AssociatedWordTags[0].BaseName;
                                        }
                                    }
                                    else if (oVar.ForteDocument.FileFormat == mpFileFormats.OpenXML)
                                    {
                                        iBoundingObjectCount = oVar.AssociatedContentControls.Length;

                                        if (iBoundingObjectCount > 0)
                                        {
                                            //check that content control hasn't been deleted
                                            xBaseName = oVar.AssociatedContentControls[0].Tag;
                                        }
                                    }
                                }
                                catch { }

                                if (xBaseName != null || iBoundingObjectCount == 0)
                                {
                                    //GLOG 3833: Save current value of m_oCurNode
                                    oNode = m_oCurNode;
                                    if (oCtl.IsDirty)
                                    {
                                        oCtl.IsDirty = false;

                                        //control has been edited - update variable value
                                        this.UpdateVariableValueFromCurrentCtl();
                                    }

                                    //set node text to variable value
                                    oCurValueNode.Text = this.GetVariableUIValue_API(oVar);

                                    //execute actions associated with LostFocus event
                                    ExecuteControlActions(oVar, ControlActions.Events.LostFocus);
                                    //GLOG 3833: Restore m_oCurNode in case any Enable/Visible actions have changed it
                                    m_oCurNode = oNode;
                                    //TODO: this code is problematic, as there are times when the
                                    //InsertSegment variable action executes, but doesn't replace
                                    //the existing segment - it replaces a child segment
                                    ////get new TreeNode if InsertSegment variable action has replaced
                                    ////existing segment
                                    //oNewNode = GetNodeFromInsertedSegment((Variable)oCurNodeTag);
                                    if (oVar.AssociatedControl is PaperTraySelector)
                                    {
                                        //GLOG 4752:  Reload the Paper Tray control each time so that
                                        //it will update with appropriate Tray choices when Printer is changed
                                        oVar.AssociatedControl = null;
                                    }
                                }
                            }
                            else if (oCurNodeTag is Authors)
                            {
                                Authors oAuthors = (Authors)oCurNodeTag;
                                //GLOG 3833: Save current value of m_oCurNode
                                oNode = m_oCurNode;
                                if (oCtl.IsDirty)
                                {
                                    oCtl.IsDirty = false;

                                    //Save copy of current list for FieldCode evaluation
                                    oAuthors.Segment.PreviousAuthors.SetAuthors(oAuthors);

                                    try
                                    {
                                        //control has been edited - update authors value
                                        oAuthors.XML = oCtl.Value;
                                    }
                                    catch (System.Exception oE)
                                    {
                                    }

                                    //execute actions associated with ValueChanged event
                                    ExecuteControlActions(oAuthors, ControlActions.Events.ValueChanged);
                                }

                                //set node text to authors UI value
                                oCurValueNode.Text = this.GetAuthorsUIValue(oAuthors);

                                //execute actions associated with LostFocus event
                                ExecuteControlActions(oAuthors, ControlActions.Events.LostFocus);
                                //GLOG 3833: Restore m_oCurNode in case any Enable/Visible actions have changed it
                                m_oCurNode = oNode;
                            }
                            else if (oCurNodeTag is Jurisdictions)
                            {
                                //GLOG 3833: Save current value of m_oCurNode
                                oNode = m_oCurNode;
                                Segment oASeg = (Segment)m_oCurNode.Parent.Tag;
                                if (oCtl.IsDirty)
                                {
                                    oCtl.IsDirty = false;

                                    //control has been edited - update segment levels
                                    int[] iLevels = Segment.GetJurisdictionArray(oCtl.Value);

                                    oASeg.L0 = iLevels[0];
                                    oASeg.L1 = iLevels[1];
                                    oASeg.L2 = iLevels[2];
                                    oASeg.L3 = iLevels[3];
                                    oASeg.L4 = iLevels[4];

                                    //Update Segment ObjectData
                                    oASeg.Nodes.SetItemObjectDataValue(oASeg.FullTagID, "L0", oASeg.L0.ToString());
                                    oASeg.Nodes.SetItemObjectDataValue(oASeg.FullTagID, "L1", oASeg.L1.ToString());
                                    oASeg.Nodes.SetItemObjectDataValue(oASeg.FullTagID, "L2", oASeg.L2.ToString());
                                    oASeg.Nodes.SetItemObjectDataValue(oASeg.FullTagID, "L3", oASeg.L3.ToString());
                                    oASeg.Nodes.SetItemObjectDataValue(oASeg.FullTagID, "L4", oASeg.L4.ToString());

                                    //execute actions associated with ValueChanged event
                                    ExecuteControlActions(oASeg.CourtChooserControlActions,
                                        ControlActions.Events.ValueChanged);

                                    //GLOG item #6690 - dcf - 04/16/13
                                    UpdateSegmentImage(oASeg, true);
                                }
                                
                                //set node text to Jurisdiction UI value-
                                //get only visible levels
                                JurisdictionChooser oChooser = (JurisdictionChooser)oCtl;
                                oCurValueNode.Text = GetJurisdictionTextForControl(oChooser, (Segment)oASeg);
                                oCurValueNode.Override.ItemHeight = 28;
                                //execute actions associated with LostFocus event
                                ExecuteControlActions(oASeg.CourtChooserControlActions, ControlActions.Events.LostFocus);
                                //GLOG 3833: Restore m_oCurNode in case any Enable/Visible actions have changed it
                                m_oCurNode = oNode;
                            }
                            else if (oCurNodeTag is Segment)
                            {
                                //GLOG : 7324 : JSW
                                //check if there is a control value
                                if (oCtl.IsDirty && !string.IsNullOrEmpty(oCtl.Value))
                                {
                                    oCtl.IsDirty = false;
                                    Segment oCurSeg = (Segment)oCurNodeTag;
                                    //GLOG 3507
                                    m_bReplacingDocumentSegment = oCurSeg.IsTopLevel &&
                                        oCurSeg.IntendedUse == mpSegmentIntendedUses.AsDocument;

                                    //chooser control has been edited - change segment
                                    Segment oSeg = this.ReplaceSegment((Segment)oCurNodeTag,
                                        Int32.Parse(oCtl.Value));

                                    //GLOG 2908: Insertion may have been cancelled
                                    if (oSeg != null)
                                    {
                                        if (this.m_oCurCtl != null)
                                        {
                                            //remove the chooser as the current control -
                                            //we need this to happen before oNewNode.Expanded = true
                                            //below, as node expansion will cause this block to 
                                            //be rerun a second time, resulting in an error
                                            this.treeDocContents.Controls.Remove(this.m_oCurCtl);
                                            m_oControlsHash.Remove(this.m_oCurCtl);

                                            //Can't use DoEvents while OnKeyPressed is still processing
                                            if (!m_bHotKeyPressed)
                                                System.Windows.Forms.Application.DoEvents();

                                            //clear out current control
                                            this.m_oCurCtl = null;
                                        }
                                        //GLOG 3507: If top-level document segment has been replaced using
                                        //Chooser, need to refresh tags, in case DocumentStamp content
                                        //has also been deleted
                                        if (m_bReplacingDocumentSegment)
                                        {
                                            m_oMPDocument.Refresh(ForteDocument.RefreshOptions.None);
                                            this.RefreshTree(false, true);
                                            m_bReplacingDocumentSegment = false;
                                        }
                                        else
                                            //GLOG : 8047 : ceh
											//GLOG : 8096 : ceh
                                            this.TaskPane.Refresh(false, true, false);

                                        //get new segment node
                                        oNewNode = this.GetNodeFromTagID(oSeg.FullTagID);
                                        oNewNode.Expanded = true;
                                    }
                                }

                                //restore to original space after value
                                this.m_oCurNode.Override.NodeSpacingAfter = 10;
                            }
                            else if (m_oCurNode.Key.EndsWith(".Language"))
                            {
                                //GLOG 3833: Save current value of m_oCurNode
                                oNode = m_oCurNode;
                                UltraTreeNode oSegNode = m_oCurNode.Parent;
                                Segment oSeg = (Segment)oSegNode.Tag;
                                if (oCtl.IsDirty)
                                {
                                    oCtl.IsDirty = false;

                                    //control has been edited - update culture value
                                    oSeg.Culture = System.Int32.Parse(oCtl.Value);
                                    oSeg.Nodes.SetItemObjectDataValue(oSeg.FullTagID, "Culture",
                                        oCtl.Value);

                                    //update for new language
                                    UpdateForLanguage(oSeg);

                                    //execute actions associated with ValueChanged event
                                    ExecuteControlActions(oSeg.LanguageControlActions,
                                        ControlActions.Events.ValueChanged);

                                    //GLOG item #6751 - dcf
                                    UpdateSegmentImage(oSeg, true);
                                }

                                //set node text to culture UI value
                                oCurValueNode.Text = LMP.Data.Application
                                    .GetLanguagesDisplayValue(oCtl.Value);

                                //execute actions associated with LostFocus event
                                ExecuteControlActions(oSeg.LanguageControlActions,
                                    ControlActions.Events.LostFocus);
                                //GLOG 3833: Restore m_oCurNode in case any Enable/Visible actions have changed it
                                m_oCurNode = oNode;
                            }
                        }
                        finally
                        {
                            //GLOG 7742:  Save original XML Events state
                            ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
                            try
                            {
                                //GLOG 3507
                                m_bReplacingDocumentSegment = false;
                                //delete temp page break that might have been inserted-
                                //inserted when there is only one page in the section
                                //and the variable is in the header/footer
                                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                                LMP.Forte.MSWord.WordDoc.DeleteTempPageBreak(this.m_oMPDocument.WordDocument);

                                //GLOG 6496/6497 (dm) - selection should be at the start of the section here, but
                                //the header pane may not get closed if the section is multiple pages (because
                                //there's no temp page break to delete) or when moving between the header and
                                //footer (in which case the document was sometimes winding up in draft view)
                                //10-12-12 (dm) - added WordTagChangeOccurring condition to prevent header from
                                //closing when the user is manually moving around in the header/footer
                                if ((!this.TaskPane.WordTagChangeOccurring) &&
                                    (Session.CurrentWordApp.Selection.Range.StoryType != Word.WdStoryType.wdMainTextStory))
                                {
                                    object oMissing = System.Reflection.Missing.Value;
                                    Word.Range oRange = Session.CurrentWordApp.Selection.Sections[1].Range;
                                    oRange.StartOf(ref oMissing, ref oMissing);
                                    oRange.Select();
                                }

                                //restore Word environment
                                oEnv.RestoreState();

                                //8-1-11 (dm) - moved this line to after the call to RestoreState(),
                                //as this method can trigger the content control enter/exit events
                                //GLOG 7742: Restore original state
                                ForteDocument.IgnoreWordXMLEvents = oEvents;

                                if (m_oCurCtl != null)
                                {
                                    //there is a current control - remove from form
                                    this.treeDocContents.Controls.Remove(this.m_oCurCtl);

                                    //Can't use DoEvents while OnKeyPressed is still processing
                                    if (!m_bHotKeyPressed)
                                        System.Windows.Forms.Application.DoEvents();

                                    //clear out current control
                                    this.m_oCurCtl = null;
                                }
                            }
                            catch 
                            {
                                //GLOG 7742: Restore original XML Events state if error occurs
                                ForteDocument.IgnoreWordXMLEvents = oEvents;
                            }
                            finally
                            {
                                oFocusForm.Visible = false;

                                if (bScreenUpdating)
                                {
                                    Session.CurrentWordApp.ScreenRefresh();
                                    this.TaskPane.ScreenUpdating = true;
                                }
                            }
                        }
                    }
                    else
                        //force a redraw
                        this.treeDocContents.Refresh();
                }
                else if (oCurNodeTag is Block)
                {
                    UltraTreeNode oCurValueNode = null;

                    //get current value node
                    oCurValueNode = this.m_oCurNode.Nodes[0];

                    if (oCurValueNode == null)
                    {
                        //something is wrong - all variable and author
                        //nodes need to have a child value node
                        throw new LMP.Exceptions.UINodeException(
                            LMP.Resources.GetLangString("Error_MissingValueNode") +
                            this.m_oCurNode.FullPath);
                    }

                    //set node text to variable value
                    Word.Range oRng = null;

                    //get the range of the block - ignore
                    //if there is an error, as the block may no longer exist
                    try
                    {
                        if (this.m_oMPDocument.FileFormat ==
                            LMP.Data.mpFileFormats.Binary)
                        {
                            oRng = ((Block)oCurNodeTag).AssociatedWordTag.Range;
                        }
                        else
                        {
                            oRng = ((Block)oCurNodeTag).AssociatedContentControl.Range;
                        }
                    }
                    catch { }

                    if (oRng != null)
                    {
                        string xText = LMP.Forte.MSWord.WordDoc.GetRangeStartText(oRng, 20);
                        xText = xText.Replace("\v", "...");
                        xText = xText.Replace("\r\n", "...");
                        xText = xText.Replace("\n", "...");

                        oCurValueNode.Text = xText;
                    }

                    //force a redraw
                    this.treeDocContents.Refresh();
                }
            }
            finally
            {
                m_bSuppressTreeActivationHandlers = true;
                this.treeDocContents.ActiveNode = null;
                this.treeDocContents.SelectedNodes.Clear();
                m_bSuppressTreeActivationHandlers = false;
                m_bExitingEditMode = false;
            }

            LMP.Benchmarks.Print(t0, xCurNodeName);

            //return node - this will be null unless a segment has been replaced
            //via a chooser control
            return oNewNode;
        }

        ///// <summary>
        /////gets tree node from segment inserted via designated variable's
        /////InsertSegment variable action, if any
        ///// <param name="oVar"></param>
        ///// <returns></returns>
        //private UltraTreeNode GetNodeFromInsertedSegment(Variable oVar)
        //{
        //    Word.XMLNode oNode = null;
        //    Word.Range oRng = null;
        //    VariableAction oVA = null;
        //    Object oBmk = null;
        //    string xTagID = null;
        //    UltraTreeNode oTreeNode = null;

        //    for (int i = 0; i < oVar.VariableActions.Count; i++)
        //    {
        //        if (oVar.VariableActions.ItemFromIndex(i).Type == VariableActions.Types.InsertSegment)
        //        {
        //            oVA = oVar.VariableActions.ItemFromIndex(i);
        //            oBmk = (object)oVA.Parameters.Split(StringArray.mpEndOfSubField)[0];

        //            //attempt to get range from bookmark param
        //            //this may be empty if bookmark does not exist in document
        //            //or bookmark param is empty
        //            try
        //            {
        //                oRng = m_oMPDocument.WordDocument.Bookmarks.get_Item(ref oBmk).Range;
        //            }
        //            catch {}
                    
        //            //get the tag ID of the bookmarked range top level XML node
        //            //this will be an mSEG if the action has run correctly
        //            if (oRng != null && oRng.XMLNodes.Count > 0)
        //            {
        //                oNode = oRng.XMLNodes[1];
        //                if (oNode.BaseName == "mSEG")
        //                {
        //                    xTagID = m_oMPDocument.GetAttributeValue(oNode, "TagID");
        //                    oTreeNode = GetNodeFromTagID(oVar.Segment.TagID + "." + xTagID);
        //                    return oTreeNode;
        //                }
        //            }
        //        }
        //    }
        //    //no segment has been replaced, return empty node
        //    return oTreeNode;
        //}

        /// <summary>
        /// replaces the specified segment 
        /// with a segment having the specified ID -
        /// returns the new segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="iNewSegmentID1"></param>
        /// <returns></returns>
        private Segment ReplaceSegment(Segment oSegment, int iNewSegmentID1)
        {
            bool bScreenUpdating = this.TaskPane.ScreenUpdating;
            Segment oDraftStamp = null;     //GLOG : 8178 : ceh
            LMP.Architect.Api.ForteDocument.WordXMLEvents bIgnoreWordXMLEvents = ForteDocument.IgnoreWordXMLEvents;
            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(this.m_oMPDocument.WordDocument);
            oEnv.SaveState();
            try
            {
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //turn off screen updating
                this.TaskPane.ScreenUpdating = false;

                oEnv.SetExecutionState(mpTriState.Undefined, mpTriState.Undefined, mpTriState.Undefined,
                    mpTriState.Undefined, mpTriState.True, false);

                //store node index of segment being replaced, so that we can insert the
                //new segment node at the same location in tree
                if (m_oCurNode != null)
                {
                    UltraTreeNode oSegmentNode = m_oCurNode;

                    while (!(oSegmentNode.Tag is Segment))
                    {
                        oSegmentNode = oSegmentNode.Parent;
                    }
                    
                    m_oMPDocument.ReplaceSegmentNodeIndex = oSegmentNode.Index;
                }

                //store whether existing segment is primary among siblings
                m_bReplaceSegmentPrimaryItem = oSegment.IsPrimary;

                Prefill oPrefill;
                string xBodyText = "";
                if (Segment.IsShellSegment(oSegment))
                {
                    oPrefill = this.TaskPane.TransientPrefill;
                    xBodyText = this.TaskPane.TransientBodyText;
                    this.TaskPane.TransientPrefill = null;
                    this.TaskPane.TransientBodyText = "";
                }
                else
                    oPrefill = new Prefill(oSegment);

                //flag pleading table item replacement
                Segment oParent = oSegment.Parent;
                if ((oSegment is CollectionTableItem) && (oParent != null) &&
                    (oParent is CollectionTable))
                {
                    //7-20-11 (dm) - insert as full row when existing segment doesn't
                    //allow side-by-side - if such an existing segment isn't itself
                    //full row, it's probably the result of bad conversion
                    LMP.Architect.Api.CollectionTableItem oItem =
                        (LMP.Architect.Api.CollectionTableItem)oSegment;
                    LMP.Forte.MSWord.Pleading oPleading = new LMP.Forte.MSWord.Pleading();
                    if (oSegment.TypeID == mpObjectTypes.PleadingCaption)
                    {
                        //caption is always full row
                        LMP.Architect.Api.CollectionTable oPT = (LMP.Architect.Api.CollectionTable)oParent;
                        oPT.ReplacementInProgress = true;
                    }
                    //GLOG 6789 (dm) - the following three branches are unnecessary, since this function
                    //will no longer ever encounter segment bounding objects, and first of these (which
                    //was added for 10.5) was preventing the last branch from ever being reached
                    //else if (oSegment.PrimaryContentControl == null && oSegment.PrimaryWordTag == null)
                    //{
                    //    if ((!oItem.AllowSideBySide) || oPleading.ItemIsFullRow_Bookmark(oSegment.PrimaryBookmark))
                    //    {
                    //        //insert as full row
                    //        ((CollectionTable)oParent).PendingInsertionLocation =
                    //            CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                    //    }
                    //}
                    //else if (oSegment.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary &&
                    //    ((!oItem.AllowSideBySide) || oPleading.ItemIsFullRow(oSegment.PrimaryWordTag)))
                    //{
                    //    //insert as full row
                    //    ((CollectionTable)oParent).PendingInsertionLocation =
                    //        CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                    //}
                    //else if ((!oItem.AllowSideBySide) ||
                    //    oPleading.ItemIsFullRow_CC(oSegment.PrimaryContentControl))
                    //{
                    //    //insert as full row
                    //    ((CollectionTable)oParent).PendingInsertionLocation =
                    //        CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                    //}
                    else if ((!oItem.AllowSideBySide) || oPleading.ItemIsFullRow_Bookmark(oSegment.PrimaryBookmark))
                    {
                        //insert as full row
                        ((CollectionTable)oParent).PendingInsertionLocation =
                            CollectionTableStructure.ItemInsertionLocations.CurrentRow;
                    }
                    else
                    {
                        //existing item is not full row
                        AdminSegmentDefs oDefs = new AdminSegmentDefs(oSegment.TypeID);
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iNewSegmentID1);
                        bool bAllowSideBySide = (LMP.Architect.Api.Segment.GetPropertyValueFromXML(
                            oDef.XML, "AllowSideBySide").ToUpper() == "TRUE");
                        if (bAllowSideBySide)
                        {
                            //insert in current cell
                            //oPT.PendingInsertionLocation =
                            //    CollectionTableStructure.ItemInsertionLocations.CurrentRow |
                            //    CollectionTableStructure.ItemInsertionLocations.ExistingCell;
                            Word.Range oRange = oItem.PrimaryRange;
                            if (oRange.Cells[1].ColumnIndex > 1)
                            {
                                ((CollectionTable)oParent).PendingInsertionLocation =
                                    CollectionTableStructure.ItemInsertionLocations.CurrentRow |
                                    CollectionTableStructure.ItemInsertionLocations.RightCell;
                            }
                            else
                            {
                                ((CollectionTable)oParent).PendingInsertionLocation =
                                    CollectionTableStructure.ItemInsertionLocations.CurrentRow |
                                    CollectionTableStructure.ItemInsertionLocations.LeftCell;
                            }
                        }
                        else
                        {
                            //selected item does not allow side-by-side insertion -
                            //alert user that can't be replaced
                            string xMsg = "Msg_CannotReplaceCollectionTableItem";
                            MessageBox.Show(LMP.Resources.GetLangString(xMsg),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return null;
                        }
                    }
                }

                //get location of current segment, accounting for start tag
                Word.Range oRng = null;

                //if (oSegment.IsFinished)
                //{
                    oRng = oSegment.FirstBookmark.Range;
                //}
                //else
                //{
                //    if (oSegment.ForteDocument.FileFormat == mpFileFormats.Binary)
                //    {
                //        Word.XMLNode oNode = oSegment.FirstWordTag;

                //        if (oNode != null)
                //        {
                //            oRng = oSegment.FirstWordTag.Range;
                //        }
                //        else
                //        {
                //            oRng = oSegment.FirstBookmark.Range;
                //        }
                //    }
                //    else
                //    {
                //        Word.ContentControl oCC = oSegment.FirstContentControl;

                //        if (oCC != null)
                //        {
                //            oRng = oSegment.FirstContentControl.Range;
                //        }
                //        else
                //        {
                //            oRng = oSegment.FirstBookmark.Range;
                //        }
                //    }
                //}

                //get mDels belonging to parent
                LMP.Forte.MSWord.Tags oTags = this.m_oMPDocument.Tags;
                string xmDels = "";
                if (oParent != null)
                {
                    string xTagID = oSegment.FullTagID;
                    xTagID = xTagID.Substring(xTagID.LastIndexOf('.') + 1);
                    xmDels = LMP.Forte.MSWord.WordDoc.GetmDels(oRng, ref oTags, -1, false, false, false, xTagID);
                }

                //GLOG 6789 (dm) - since oRng is now a bookmark mSEG, we should no longer be
                //moving the start as we previously did to get it before the bounding object
                //object oUnit = Word.WdUnits.wdCharacter;
                //object oCount = -1;
                //oRng.MoveStart(ref oUnit, ref oCount);

                //If mSEG is within Textbox, point to containing section range
                if (oRng.StoryType == Word.WdStoryType.wdTextFrameStory)
                    oRng = oRng.Sections.First.Range;

                object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                oRng.Collapse(ref oDirection);
                int iStart = oRng.Start;
                Segment.InsertionLocations iLocation = Segment.InsertionLocations.Default;

                //GLOG 6428 (dm) - moved this line up, since oSegment may get deleted below
                int iSection = oSegment.PrimaryRange.Sections[1].Index;

                //GLOG 15799: If immediately adjacent to following segment,
                //make sure replacement segment doesn't end up inserted
                //inside bookmark of following segment
                Word.Bookmark oTempBmk = null;
                if (!(oSegment is ISingleInstanceSegment) && !(oSegment is CollectionTableItem) && !(oSegment is Paper))
                {
                    Word.Range oBmkRng = oSegment.FirstBookmark.Range.Duplicate;
                    object oEnd = Word.WdCollapseDirection.wdCollapseEnd;
                    oBmkRng.Collapse(ref oEnd);
                    Word.Bookmark oSegBookmark = LMP.Forte.MSWord.WordDoc.GetParentSegmentBookmarkFromRange(oBmkRng);
                    if (!(bool)(oBmkRng.get_Information(Word.WdInformation.wdWithInTable)) && oSegBookmark != null && (oBmkRng.Start == oSegBookmark.Start))
                    {
                        string xSegTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_Bookmark(oSegBookmark);
                        if (xSegTagID != oParent.FullTagID)
                        {
                            oBmkRng.InsertBefore("\rmpReplaceTempText");
                            oBmkRng.SetRange(oBmkRng.End, oBmkRng.End);
                            oTempBmk = oBmkRng.Bookmarks.Add("mpReplaceTempText");
                            oSegBookmark.Start = oBmkRng.End; // oSegBookmark.Start + @"mpReplaceTempText".Length;
                        }
                    }
                }
                //delete current segment, if the segment is
                //not a single instance segment - single instance
                //segments replace themselves
#if Oxml
                if (!(oSegment is ISingleInstanceSegment) && !LMP.MacPac.Application.IsOxmlSupported())
#else
                if (!(oSegment is ISingleInstanceSegment))
#endif
                {
                    try
                        {
                            this.TaskPane.ForteDocument.DeleteSegment(oSegment, true, false, false);
                            //GLOG 6832: This was changed from 10.4.1, but it doesn't seem
                            //necessary to handle this case differently now.
                            //if (oSegment is CollectionTableItem && oParent is CollectionTable && oParent.Segments.Count == 1)
                            //{
                            //    if (oParent.PrimaryContentControl == null &&
                            //        oParent.PrimaryWordTag == null)
                            //    {
                            //        //convert bookmark to either tag or content control
                            //        Word.Bookmark oPrimaryBmk = oParent.PrimaryBookmark;
                            //        Word.Range oTmpRng = oPrimaryBmk.Range;

                            //        //insert preceding paragraph -
                            //        object oDir = Word.WdCollapseDirection.wdCollapseStart;
                            //        oTmpRng.Collapse(ref oDir);
                            //        oRng = oTmpRng.Duplicate;
                            //        bRangeSet = true;

                            //        //delete child's node - this won't happen automatically
                            //        //because delete event will only be raised for parent
                            //        this.DeleteNode(oSegment.FullTagID);

                            //        this.TaskPane.ForteDocument.DeleteSegment(oParentSegment, true, false, false);

                            //        oParentSegment = null;
                            //    }
                            //    else
                            //    {
                            //        this.TaskPane.ForteDocument.DeleteSegment(oSegment, true, false, false);
                            //    }
                            //}
                            //else
                            //{
                            //    this.TaskPane.ForteDocument.DeleteSegment(oSegment, true, false, false);
                            //}
                    }
                    catch
                    {
                        throw new LMP.Exceptions.SegmentException(
                            LMP.Resources.GetLangString("Error_CouldNotRemoveSegment") +
                            oSegment.ID);
                    }
                    iLocation = Segment.InsertionLocations.InsertAtSelection;
                }
                else
                {
                    //Replacement Segment might have a different location than current instance
                    AdminSegmentDefs oDefs = new AdminSegmentDefs(oSegment.TypeID);
                    AdminSegmentDef oDef = null;
                    try
                    {
                        oDef = (AdminSegmentDef)oDefs.ItemFromID(iNewSegmentID1);
                    }
                    catch { }

                    if (oDef != null)
                        iLocation = (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation;

                    if (iLocation == 0)
                        iLocation = Segment.InsertionLocations.Default;
                }
                //GLOG 6379/4028
                if (iLocation == Segment.InsertionLocations.InsertAtSelection
                    || iLocation == Segment.InsertionLocations.Default
                    || iLocation == Segment.InsertionLocations.InsertAtStartOfCurrentSection
                    || iLocation == Segment.InsertionLocations.InsertAtEndOfCurrentSection)
                {
                    //select insertion location
                    //GLOG 6832
                    //if (!bRangeSet)
                    //{
                        oRng.SetRange(iStart, iStart);
                    //}

                    oRng.Select();

                    //GLOG 6379/4028
                    iSection = oRng.Sections[1].Index;
                }

                try
                {
                    //GLOG 6379/4028: Get existing trailers
                    object oTrailers = null;
                    Prefill oDraftStampPrefill = null; //GLOG 8552
                    if (oSegment is Paper)
                    {
                        LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                        if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains(
                            "CONVERTTRAILERSTO9XFORMAT") || oFirmSettings.UseMacPac9xStyleTrailer)
                            oTrailers = LMP.Forte.MSWord.WordDoc.GetConverted9xTrailers(oSegment.ForteDocument.WordDocument, "zzmpTrailerItem");
                        else
                            oTrailers = LMP.Forte.MSWord.WordDoc.GetExistingTrailers(oSegment.ForteDocument.WordDocument, "MacPac Trailer"); //GLOG 8111 (dm)

                        //GLOG : 8178 : ceh - deal with Draft Stamps as well
                        if (oSegment is Letterhead)
                        {
                            //Get existing draft stamp where PrimaryRange is within the section in which Letterhead is being changed
                            Segments oSegs = oSegment.ForteDocument.Segments;

                            for (int i = 0; i < oSegs.Count; i++)
                            {
                                Segment oStampSeg = oSegs[i];
                                if (oSegs[i] is DraftStamp &&
                                    oSegs[i].PrimaryRange.Sections[1].Index == iSection)
                                {
                                    oDraftStamp = oStampSeg;
                                    //GLOG 8552: If DraftSTamp is in header, it may or may not get deleted with Letterhead, depending on LH design.
                                    //Always delete and recreate in this case
                                    if (oDraftStamp.DefaultDoubleClickLocation != Architect.Base.Segment.InsertionLocations.InsertAtSelection)
                                    {
                                        oDraftStampPrefill = new Prefill(oDraftStamp);
                                        oSegment.ForteDocument.DeleteSegment(oDraftStamp, false);
                                    }
                                    else
                                        //No need to recreate current page stamps
                                        oDraftStamp = null;
                                    break;
                                }
                            }
                        }
                    }
                    Segment oSeg = null;
#if Oxml                    
                    if (LMP.MacPac.Application.IsOxmlSupported())
                    {
                        LMP.Architect.Oxml.XmlSegment oNewSegment = TaskPane.InsertXmlSegmentInCurrentDoc(iNewSegmentID1.ToString(), oSegment, oPrefill, iLocation, Segment.InsertionBehaviors.Default, xBodyText, null, oRng);
                        if (oNewSegment != null)
                        {
                            //Get Segment object with same TagID as XmlSegment
                            oSeg = ForteDocument.GetSegment(oNewSegment.FullTagID, this.ForteDocument);
                        }
                    }
                    else
#endif
                    {
                        //insert new segment
                        oSeg = m_oMPDocument.InsertSegment(iNewSegmentID1.ToString(),
                            oParent, iLocation, Segment.InsertionBehaviors.Default,
                            true, oPrefill, xBodyText, oSegment.IntendedUse == mpSegmentIntendedUses.AsDocument,
                            oSegment.IntendedUse == mpSegmentIntendedUses.AsDocument, null);
                    }
                    if (oTempBmk != null)
                    {
                        try
                        {
                            Word.Range oBmkRng = oTempBmk.Range;
                            object oChar = Word.WdUnits.wdCharacter;
                            object oCount = -1 *  @"mpReplaceTempText".Length;
                            oBmkRng.MoveStart(ref oChar, ref oCount);
                            oBmkRng.Delete();
                            oTempBmk.Delete();
                        }
                        catch { }
                    }
                    //GLOG 5393:  Clear these flags here, so they won't affect Refresh below
                    //clear replacement flags
                    m_oMPDocument.ReplaceSegmentNodeIndex = -1;
                    m_bReplaceSegmentPrimaryItem = false;

                    //GLOG 2908: Insertion may be cancelled by InsertXML
                    if (oSeg != null)
                    {
                        //restore mDels
                        if ((xmDels != "") && (xmDels != null))
                        {
                            oTags = this.m_oMPDocument.Tags;

                            LMP.Forte.MSWord.WordDoc.RestoremDels(oSeg.PrimaryRange, xmDels, ref oTags);

                            //refresh all ancestors -
                            //mDels may be positioned a few generations down from owner
                            oParent = oSeg.Parent;
                            while (oParent != null)
                            {
                                oParent.RefreshNodes();
                                this.UpdateTreeNodeTags(oParent);
                                oParent = oParent.Parent;
                            }
                        }
                        else
                        {
                            //GLOG 2276: need to refresh tags for parent segment to ensure all point to the current variables
                            if (oSeg.Parent != null)
                                this.UpdateTreeNodeTags(oSeg.Parent);
                        }
                        
                        //GLOG 5393: Refresh Trailers after changing Paper type
                        if (oSeg is Paper)
                        {
                            bool bRefreshTrailers = false;
                            foreach (UltraTreeNode oTestNode in this.treeDocContents.Nodes)
                            {
                                if (oTestNode.Key == mpTrailersNodeKey ||
                                    (oTestNode.Tag is Segment && ((Segment)oTestNode.Tag).TypeID == mpObjectTypes.Trailer))
                                {
                                    bRefreshTrailers = true;
                                    break;
                                }
                            }
                            if (bRefreshTrailers)
                                this.RefreshTree(false);
                        }
                        //GLOG 6379/4028: Recreate Trailer after changing pleading paper or letterhead
                        if (oTrailers != null)
                        {
                            try
                            {
                                //Check if current section contains a trailer - may be null if no trailer
                                string xTrailerID = ((object[])oTrailers)[iSection - 1].ToString();
                                if (LMP.String.IsNumericInt32(xTrailerID))
                                {
                                    int iTrailerID = Int32.Parse(xTrailerID);
                                    //GLOG 6917: Check that Trailer Segment currently exists
                                    AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.Trailer);
                                    AdminSegmentDef oTrailerDef = (AdminSegmentDef)oDefs.ItemFromID(iTrailerID);
                                    //GLOG 6917: Don't need to recreate trailer if it's in body story
                                    if (oTrailerDef != null && String.ContentContainsLinkedStories(oTrailerDef.XML, true, true, true, true, true, true))
                                    {
                                        Prefill oTrailerPrefill = DocumentStampUI.LastTrailerPrefill(iTrailerID);
                                        if (oTrailerPrefill != null)
                                            LMP.MacPac.Application.RecreateTrailer(oTrailers, oTrailerPrefill, 0);
                                    }
                                }
                            }
                            catch { }
                        }

                        //GLOG : 8178 : ceh
                        if (oDraftStampPrefill != null) //GLOG 8552
                        {
                            LMP.MacPac.Application.RecreateDraftStamp(oDraftStamp, oDraftStampPrefill, 0);
                        }


                    }
                    //GLOG item #6651 - dcf - needed to update the segment image
                    //GLOG 6945: Update images for Parent and children, in case validity has changed
                    this.UpdateSegmentImage(oSeg, true, true);

                    //return the new segment
                    return oSeg;
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.SegmentException(
                        LMP.Resources.GetLangString("Error_CouldNotInsertSegment") +
                        iNewSegmentID1.ToString(), oE);
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                UpdateButtons(this.ForteDocument);
                oEnv.RestoreState();
                //GLOG 7860: Enable events last
                ForteDocument.IgnoreWordXMLEvents = bIgnoreWordXMLEvents;
                if (bScreenUpdating)
                    this.TaskPane.ScreenUpdating = true;
            }
        }
        private void UpdateButtons(ForteDocument oMPDocument)
        {
            bool bContainsSegments = this.ForteDocument.Segments.Count > 0;

            if (bContainsSegments &&
                m_oMPDocument.Segments[0].IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
            {
                this.tbtnInsertSavedData.Visible = true;
                this.tsSepInsertSavedData.Visible = true;
                this.tbtnSaveData.Visible = true;
                this.tsSepSaveData.Visible = false;
                this.tbtnSaveDataAs.Visible = false;
                this.tsSepSaveDataAs.Visible = false;
                this.tbtnRecreate.Visible = false;
                this.tsSepRecreate.Visible = false;
                this.tbtnSaveSegment.Text = "Save";
                this.tbtnSaveSegment.Visible = false;
                this.tsSepSaveSegment.Visible = false;
                this.tbtnFinish.Visible = false;
                this.tsSepFinish.Visible = false;
                this.tbtnSpacerLabel2.Visible = true;
                this.tbtnSpacerLabel1.Visible = false;
                this.tbtnClose.Visible = true;
                this.picMenu.Visible = false;
                this.toolStripSeparator1.Visible = false;
                this.toolStripSeparator2.Visible = false;
            }
            else
            {
                //GLOG 8511: Allow moving Finish button to end of toolstrip except for Labels and Envelopes
                bool bStaticSegment = false;
                if (bContainsSegments)
                {
                    if (m_oCurrentSegment is LMP.Architect.Base.IStaticCreationSegment ||
                        m_oCurrentSegment is LMP.Architect.Base.IStaticDistributedSegment ||
                        m_oCurrentSegment is NumberedLabels)
                    {
                        bStaticSegment = true;
                        tbtnFinish.Text = "Complete";
                    }
                    else
                    {
                        tbtnFinish.Text = "Finish";
                    }
                }

                ToolStripItem oButton = tbtnFinish;
                ToolStripItem oSep = tsSepFinish;
                ToolStripItem oSpacerLabel = tbtnSpacerLabel1;
                if (!bStaticSegment && Session.CurrentUser.FirmSettings.FinishButtonAtEndOfEditor && tsEditor.Items.IndexOf(tbtnFinish) == 0)
                {
                    //Remove Finish button and move to end of items
                    tsEditor.Items.RemoveByKey(tbtnSpacerLabel1.Name);
                    tsEditor.Items.RemoveByKey(tbtnFinish.Name);
                    tsEditor.Items.RemoveByKey(tsSepFinish.Name);
                    tsEditor.Items.Insert(tsEditor.Items.Count-1, oButton);
                    tsEditor.Items.Insert(tsEditor.Items.Count-1, oSep);
                }
                else if (bStaticSegment && (tsEditor.Items.IndexOf(tbtnFinish) != 0))
                {
                    //GLOG 15860 (dm) - restore to front of items when user navigates to unfinished static segment
                    tsEditor.Items.RemoveByKey(tbtnFinish.Name);
                    tsEditor.Items.RemoveByKey(tsSepFinish.Name);
                    tsEditor.Items.Insert(0, oSpacerLabel);
                    tsEditor.Items.Insert(0, oSep);
                    tsEditor.Items.Insert(0, oButton);
                }
                this.tbtnSpacerLabel2.Visible = (tsEditor.Items.IndexOf(tbtnFinish) != 0);

                //GLOG : 7432 : CEH
                bool bContainsUnfinishedContent = false;
                bool bContainsUnfinishedTopLevelContent = this.ForteDocument.ContainsUnfinishedTopLevelContent();

				//GLOG : 7998 : ceh
                bool bRibbonOnlyImplementation = (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator);

                //GLOG 15860 (dm) - distinguish bewteen static and non-static unfinished content
                bool bContainsUnfinishedStaticDistributedContent;

                if (bContainsUnfinishedTopLevelContent)
                {
                    bContainsUnfinishedContent = true;
                    bContainsUnfinishedStaticDistributedContent = this.ForteDocument.ContainsUnfinishedContent(false);
                }
                else
                {
					//GLOG : 7432 : CEH
                    bContainsUnfinishedContent = this.ForteDocument.ContainsUnfinishedContent();
                    bContainsUnfinishedStaticDistributedContent = bContainsUnfinishedContent;
                }

                this.tbtnInsertSavedData.Enabled = bContainsUnfinishedTopLevelContent;
                this.tbtnInsertSavedData.Visible = !bRibbonOnlyImplementation;
                this.tsSepInsertSavedData.Visible = bContainsUnfinishedTopLevelContent && !bRibbonOnlyImplementation;

                this.tbtnSaveData.Enabled = bContainsUnfinishedContent;
                this.tbtnSaveData.Visible = !bRibbonOnlyImplementation;
                this.tsSepSaveData.Visible = !bRibbonOnlyImplementation;

                this.tbtnSaveDataAs.Visible = false;
                this.tsSepSaveDataAs.Visible = false;
                //this.tbtnSaveDataAs.Enabled = bContainsUnfinishedTopLevelContent;

                this.tbtnRecreate.Enabled = bContainsUnfinishedTopLevelContent;
                this.tbtnRecreate.Visible = !bRibbonOnlyImplementation;
                this.tsSepRecreate.Visible = !bRibbonOnlyImplementation;

                this.tbtnSaveSegment.Enabled = bContainsUnfinishedTopLevelContent;
                this.tbtnSaveSegment.Visible = !bRibbonOnlyImplementation;
                this.tsSepSaveSegment.Visible = !bRibbonOnlyImplementation; //GLOG 8830

                this.tbtnFinish.Visible = true;
                this.tsSepFinish.Visible = true;
                this.tbtnFinish.Enabled = (bContainsUnfinishedStaticDistributedContent  ||
                    (m_oCurrentSegment is LMP.Architect.Base.IStaticDistributedSegment));  //GLOG 15860 (dm)

                this.tbtnClose.Visible = false;

                this.toolStripSeparator1.Visible = !bRibbonOnlyImplementation;
                this.toolStripSeparator2.Visible = !bRibbonOnlyImplementation;

                //GLOG item #6009 - dcf
                if (!bContainsSegments || bRibbonOnlyImplementation)
                    this.picMenu.Visible = false;
            }
        }

        /// <summary>
        /// evaluates DisplayValue field code using variable value
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string GetNavigationTag(Variable oVar)
        {
            //GLOG 4419
            if (!oVar.IsTagless || oVar.NavigationTag == null || oVar.NavigationTag == "")
                return "";

            string xVarValue = oVar.Value;
            //GLOG 2151: Mark reserved expression characters in Value so they'll
            //be treated as the literal characters in Expression.Evaluate
            xVarValue = Expression.MarkLiteralReservedCharacters(xVarValue);
            //pre-evaluate the NavigationTag field code(s), substituting MyValue for oVar.Value
            //string xFieldCode = oVar.NavigationTag.Replace("[MyValue]", xVarValue);
            //xFieldCode = xFieldCode.Replace("MyValue", xVarValue);
            string xFieldCode = LMP.Architect.Api.FieldCode.EvaluateMyValue(oVar.NavigationTag, xVarValue);

            //Evaluate entire expression
            return Expression.Evaluate(xFieldCode, oVar.Segment, oVar.ForteDocument);
        }

        /// <summary>
        /// returns the UI value for the specified authors collection
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <returns></returns>
        private string GetAuthorsUIValue(Authors oAuthors)
        {
            //get display text for value node
            Author oLeadAuthor = oAuthors.GetLeadAuthor();

            string xAuthors = "";

            if (oLeadAuthor != null)
            {
                if (oLeadAuthor.IsManualAuthor)
                    xAuthors = oLeadAuthor.FullName;
                else
                {
                    try
                    {
                        //try setting to Display Name first
                        xAuthors = oLeadAuthor.PersonObject.DisplayName;
                    }
                    catch { }
                    if (xAuthors == "")
                        xAuthors = oLeadAuthor.FullName;
                }

                if (oAuthors.Count > 1)
                    xAuthors += ", et al.";
            }
            return xAuthors;
        }
        /// <summary>
        /// creates, sets up, and returns the author selection control
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <returns></returns>
        private System.Windows.Forms.Control SetupAuthorControl(Authors oAuthors)
        {
            DateTime t0 = DateTime.Now;

            AuthorSelector oAuthorSelector = new AuthorSelector();

            IControl oIControl = (IControl)oAuthorSelector;
            oIControl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
            oIControl.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oIControl.KeyReleased += new KeyEventHandler(OnKeyReleased);

            oAuthorSelector.ShowUserPeopleManagerRequested += 
                new AuthorSelector.UpdatePeopleListEventHandler(oAuthorSelector_ShowUserPeopleManagerRequested);
            oAuthorSelector.MaxAuthors = (short)oAuthors.Segment.MaxAuthors;
            SetAuthorControlProperties(oAuthorSelector, oAuthors);

            // GLOG : 3135 : JAB
            // Subscribe to the UpdateAuthorsRequested event.
            oAuthorSelector.UpdateAuthorsRequested +=
                new AuthorSelector.UpdateAuthorsEventHandler(oAuthorSelector_UpdateAuthorsRequested);

            // GLOG : 2547 : JAB
            // Subscribe to the EditAuthorPreferencesRequested event.
            oAuthorSelector.EditAuthorPreferencesRequested += new AuthorSelector.EditAutorPreferencesEventHandler(oAuthorSelector_EditAuthorPreferencesRequested);

            //set jurisdiction if the bar id column is displayed
            if ((oAuthorSelector.ShowColumns & mpAuthorColumns.BarID) > 0)
            {
                DateTime t1 = DateTime.Now;
                Segment oLevelSegment = oAuthors.Segment;
                //GLOG 3316: If Pleading Table item, get Court Levels from parent pleading
                if (oLevelSegment is CollectionTableItem)
                {
                    while (oLevelSegment.Parent != null)
                        oLevelSegment = oLevelSegment.Parent;
                }
                oAuthorSelector.Jurisdiction = new int[] { oLevelSegment.L0, oLevelSegment.L1, oLevelSegment.L2, oLevelSegment.L3, oLevelSegment.L4 };
                LMP.Benchmarks.Print(t0, "Set Jurisdiction");
            }
            oAuthorSelector.ExecuteFinalSetup();

            LMP.Benchmarks.Print(t0);
            return oAuthorSelector;
        }

        /// <summary>
        /// GLOG : 3135 : JAB
        /// Handle the context menu selection of the Update Authors menu item.
        /// </summary>
        /// <param name="sender"></param>
        void oAuthorSelector_UpdateAuthorsRequested(object sender)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();
                LMP.MacPac.Application.UpdateAuthor();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 2547 : JAB
        /// Handle selection of the Edit Author Preferences menu item.
        /// </summary>
        /// <param name="sender"></param>
        void oAuthorSelector_EditAuthorPreferencesRequested(object sender)
        {
            try
            {
                LMP.MacPac.Application.EditAuthorPreferences(false);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// creates, sets up, and returns the jurisdiction chooser control
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <returns></returns>
        private System.Windows.Forms.Control SetupJurisdictionControl(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;

            JurisdictionChooser oChooser = new JurisdictionChooser();

            IControl oIControl = (IControl)oChooser;
            oIControl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
            oIControl.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oIControl.KeyReleased += new KeyEventHandler(OnKeyReleased);

            SetJurisdictionControlProperties(oChooser, oSegment);

            oChooser.ExecuteFinalSetup();

            LMP.Benchmarks.Print(t0);
            return oChooser;
        }
        /// <summary>
        /// creates, sets up, and returns the jurisdiction chooser control
        /// </summary>
        /// <param name="oAuthors"></param>
        /// <returns></returns>
        private System.Windows.Forms.Control SetupJurisdictionControl(LMP.Architect.Oxml.XmlSegment oSegment)
        {
            DateTime t0 = DateTime.Now;

            JurisdictionChooser oChooser = new JurisdictionChooser();

            IControl oIControl = (IControl)oChooser;
            oIControl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
            oIControl.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oIControl.KeyReleased += new KeyEventHandler(OnKeyReleased);

            SetJurisdictionControlProperties(oChooser, oSegment);

            oChooser.ExecuteFinalSetup();

            LMP.Benchmarks.Print(t0);
            return oChooser;
        }

        /// <summary>
        /// creates, sets up, and returns the languages combo box
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private System.Windows.Forms.Control SetupLanguageControl(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;

            LanguagesComboBox oLCB = new LanguagesComboBox(oSegment.SupportedLanguages,
                LMP.StringArray.mpEndOfValue);

            IControl oIControl = (IControl)oLCB;
            oIControl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
            oIControl.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oIControl.KeyReleased += new KeyEventHandler(OnKeyReleased);

            oLCB.Value = oSegment.Culture.ToString();
            oLCB.ExecuteFinalSetup();

            LMP.Benchmarks.Print(t0);
            return oLCB;
        }
        /// <summary>
        /// creates, sets up, and returns the languages combo box
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private System.Windows.Forms.Control SetupLanguageControl(LMP.Architect.Oxml.XmlSegment oSegment)
        {
            DateTime t0 = DateTime.Now;

            LanguagesComboBox oLCB = new LanguagesComboBox(oSegment.SupportedLanguages,
                LMP.StringArray.mpEndOfValue);

            IControl oIControl = (IControl)oLCB;
            oIControl.TabPressed += new TabPressedHandler(oIControl_TabPressed);
            oIControl.KeyPressed += new KeyEventHandler(OnKeyPressed);
            oIControl.KeyReleased += new KeyEventHandler(OnKeyReleased);

            oLCB.Value = oSegment.Culture.ToString();
            oLCB.ExecuteFinalSetup();

            LMP.Benchmarks.Print(t0);
            return oLCB;
        }


        /// <summary>
        /// called when the author control requests the User People Manager -
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void oAuthorSelector_ShowUserPeopleManagerRequested(System.Windows.Forms.Control oControl, ref bool bUseCallBack)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();

                //setting the handler parameter will prevent the author selector from refreshing
                //the calling instance's people list twice
                bUseCallBack = true;
                
                //show the User People Manager
                PeopleManager oForm = new PeopleManager();
                oForm.ShowDialog();

                //use the static method to refresh people list in all instances of author selector
                Application.RefreshCurrentAuthorControls();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Set Properties for author control, stored in AuthorControlProperties property of Segment
        /// </summary>
        /// <param name="oControl"></param>
        /// <param name="xProperties"></param>
        private void SetAuthorControlProperties(AuthorSelector oAuthorControl, Authors oAuthors)
        {
            DateTime t0 = DateTime.Now;

            //set up control properties-
            //get control properties from variable
            string[] aProps = null;
            string xProperties = oAuthors.Segment.AuthorControlProperties;
            System.Windows.Forms.Control oControl = (System.Windows.Forms.Control)oAuthorControl;

            if (!System.String.IsNullOrEmpty(xProperties))
                aProps = xProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);

                //evaluate control prop value, which may be a MacPac expression
                xValue = Expression.Evaluate(xValue, oAuthors.Segment, this.TaskPane.ForteDocument);

                System.Type oCtlType = oControl.GetType();
                PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType);

                oPropInfo.SetValue(oControl, oValue, null);
            }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// Set Properties for Jurisdiction control, stored in 
        /// CourtChooserControlProperties property of segment
        /// </summary>
        /// <param name="oControl"></param>
        /// <param name="xProperties"></param>
        private void SetJurisdictionControlProperties(JurisdictionChooser oChooser, LMP.Architect.Oxml.XmlSegment oSegment)
        {
            //set up control properties-
            //get control properties from Segment
            string[] aProps = null;
            string xProperties = oSegment.CourtChooserControlProperties;
            System.Windows.Forms.Control oControl = (System.Windows.Forms.Control)oChooser;

            if (!System.String.IsNullOrEmpty(xProperties))
                aProps = xProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);

                //evaluate control prop value, which may be a MacPac expression
                xValue = LMP.Architect.Oxml.XmlExpression.Evaluate(xValue, oSegment, oSegment.ForteDocument);

                System.Type oCtlType = oControl.GetType();
                PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType);

                oPropInfo.SetValue(oControl, oValue, null);
            }
        }
        /// <summary>
        /// Set Properties for Jurisdiction control, stored in 
        /// CourtChooserControlProperties property of segment
        /// </summary>
        /// <param name="oControl"></param>
        /// <param name="xProperties"></param>
        private void SetJurisdictionControlProperties(JurisdictionChooser oChooser, Segment oSegment)
        {
            //set up control properties-
            //get control properties from Segment
            string[] aProps = null;
            string xProperties = oSegment.CourtChooserControlProperties;
            System.Windows.Forms.Control oControl = (System.Windows.Forms.Control)oChooser;

            if (!System.String.IsNullOrEmpty(xProperties))
                aProps = xProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs, executing each
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);

                //evaluate control prop value, which may be a MacPac expression
                xValue = Expression.Evaluate(xValue, oSegment, this.TaskPane.ForteDocument);

                System.Type oCtlType = oControl.GetType();
                PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                System.Type oPropType = oPropInfo.PropertyType;

                object oValue = null;

                if (oPropType.IsEnum)
                    oValue = Enum.Parse(oPropType, xValue);
                else
                    oValue = Convert.ChangeType(xValue, oPropType);

                oPropInfo.SetValue(oControl, oValue, null);
            }
        }
        /// <summary>
        /// sizes and positions control at the specified node
        /// </summary>
        /// <param name="oUnderlyingNode"></param>
        private void SizeAndPositionControl()
        {
            if (this.m_oCurCtl == null || this.m_oCurNode == null)
                return;

            try
            {
                this.treeDocContents.SuspendLayout();
                this.m_oCurCtl.SuspendLayout();
                m_bRepositioningControl = true;
                int iControlWidth = 0;

                //set control width based on scroll bar visibility
                if (this.ScrollBarsVisible)
                    iControlWidth = this.treeDocContents.Width -
                        this.m_oCurNode.Bounds.Left - this.treeDocContents.Indent - 20;
                else
                    iControlWidth = this.treeDocContents.Width -
                        this.m_oCurNode.Bounds.Left - this.treeDocContents.Indent - 5;


                //When TaskPane in Initializing in Word 2003, width may not yet be finalized
                //when Initial control is selected.  Set to arbitrary width to avoid problems
                if (iControlWidth <= 0)
                    iControlWidth = 192;

                if (this.btnContacts.Visible)
                    iControlWidth = iControlWidth - (this.btnContacts.Width + 1);

                //position control to display over variable value
                this.m_oCurCtl.SetBounds(this.m_oCurNode.Bounds.Left + this.treeDocContents.Indent,
                    this.m_oCurNode.Bounds.Top + this.m_oCurNode.Bounds.Height,
                    iControlWidth, this.m_oCurCtl.Height);
                
                //Reposition floating CI button
                if (this.btnContacts.Visible)
                {
                    this.btnContacts.Left = m_oCurCtl.Left + m_oCurCtl.Width + 1;
                    this.btnContacts.Top = m_oCurCtl.Top + 5;
                }
            }
            finally
            {
                this.treeDocContents.ResumeLayout();
                this.m_oCurCtl.ResumeLayout();
                m_bRepositioningControl = false;
            }
        }
        /// <summary>
        /// sets value of variable associated with the current node equal to
        /// the value of the node's control
        /// </summary>
        private void UpdateVariableValueFromCurrentCtl()
        {
            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(m_oMPDocument.WordDocument);
            oEnv.SaveState();

            try
            {
                bool bScreenUpdating = this.TaskPane.ScreenUpdating;
                this.TaskPane.ScreenUpdating = false;

                Variable oVar = m_oCurNode.Tag as Variable;

                //GLOG 4132 (dm) - added conditional to avoid overwriting manual edits
                if (m_xSkipVarUpdateFromControl != oVar.Segment.FullTagID + "." + oVar.ID)
                {
                    //GLOG 5307: m_oCurCtl may no longer exist after SetValue if exiting
                    //the control and a Variable Action causes the Tree to be refreshed.
                    //Get SupportingValues beforehand
                    string xSuppVals = ((IControl)m_oCurCtl).SupportingValues;
                    oVar.SetValue(((IControl)m_oCurCtl).Value);
                    //GLOG 2645
                    oVar.SetRuntimeControlValues(xSuppVals);

                    //GLOG 6945: Update Parent Segment images as well
                    UpdateSegmentImage(oVar.Segment, true, true);
                }

                if (bScreenUpdating)
                    this.TaskPane.ScreenUpdating = true;
            }
            finally
            {
                //8-1-11 (dm) - disable event handling - RestoreState() can trigger
                //content control enter/exit events
                ForteDocument.WordXMLEvents oEvents = ForteDocument.IgnoreWordXMLEvents;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                oEnv.RestoreState();
                ForteDocument.IgnoreWordXMLEvents = oEvents;
            }
        }
        /// <summary>
        /// Initializes Jurisdiction node for segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        private void InitializeJurisdictionNode(Segment oSegment, UltraTreeNode oNode)
        {
            //add hotkey to collection of hotkeys for this editor
            this.AddHotkey("J", oNode);

            //set node properties
            oNode.Expanded = true;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //assign empty Jurisdiction object to Tag
            oNode.Tag = new Jurisdictions(0);

            //add authors validation image -
            //TODO: deal with Jurisdiction validation
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //create new control and store in value node tag
            System.Windows.Forms.Control oJurCtl = SetupJurisdictionControl(oSegment);

            //oValueNode.Enabled = true;
            string xValue = GetJurisdictionTextForControl((JurisdictionChooser)oJurCtl, (Segment)oSegment);

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value", xValue);
            oValueNode.Tag = oJurCtl;
            //Add to hash table
            m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            oOverride = oValueNode.Override;
            oOverride.Multiline = DefaultableBoolean.True;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
        }
        /// <summary>
        /// Initializes Jurisdiction node for segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        private void InitializeJurisdictionNode(LMP.Architect.Oxml.XmlSegment oSegment, UltraTreeNode oNode)
        {
            //add hotkey to collection of hotkeys for this editor
            this.AddHotkey("J", oNode);

            //set node properties
            oNode.Expanded = true;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //assign empty Jurisdiction object to Tag
            oNode.Tag = new Jurisdictions(0);

            //add authors validation image -
            //TODO: deal with Jurisdiction validation
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //create new control and store in value node tag
            System.Windows.Forms.Control oJurCtl = SetupJurisdictionControl(oSegment);

            //oValueNode.Enabled = true;
            string xValue = GetJurisdictionTextForControl((JurisdictionChooser)oJurCtl, oSegment);

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value", xValue);
            oValueNode.Tag = oJurCtl;
            //Add to hash table
            m_oControlsHash.Add(oValueNode.Tag, oValueNode);
            oOverride = oValueNode.Override;
            oOverride.Multiline = DefaultableBoolean.True;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
        }
        /// <summary>
        /// Set display text for Jurisdiction Chooser,
        /// based on Min and Max Display levels
        /// </summary>
        /// <param name="oCtl"></param>
        /// <param name="oSeg"></param>
        /// <returns></returns>
        private string GetJurisdictionTextForControl(JurisdictionChooser oCtl, Segment oSeg)
        {
            int iMin = (int)oCtl.FirstLevel;
            int iMax = (int)oCtl.LastLevel;

            string xDisplayValue = (oSeg.GetJurisdictionText(iMin <= 0,
                iMin <= 1 && iMax >= 1, iMin <= 2 && iMax >= 2, iMin <= 3 && iMax >= 3, iMin <= 4 && iMax >= 4));

            if (xDisplayValue == "")
                return LMP.Resources.GetLangString("Msg_NotSpecified");
            else
            {
                return xDisplayValue;
            }
        }
        /// <summary>
        /// Set display text for Jurisdiction Chooser,
        /// based on Min and Max Display levels
        /// </summary>
        /// <param name="oCtl"></param>
        /// <param name="oSeg"></param>
        /// <returns></returns>
        private string GetJurisdictionTextForControl(JurisdictionChooser oCtl, LMP.Architect.Oxml.XmlSegment oSeg)
        {
            int iMin = (int)oCtl.FirstLevel;
            int iMax = (int)oCtl.LastLevel;

            string xDisplayValue = (oSeg.GetJurisdictionText(iMin <= 0,
                iMin <= 1 && iMax >= 1, iMin <= 2 && iMax >= 2, iMin <= 3 && iMax >= 3, iMin <= 4 && iMax >= 4));

            if (xDisplayValue == "")
                return LMP.Resources.GetLangString("Msg_NotSpecified");
            else
            {
                return xDisplayValue;
            }
        }
        private void InitializeNode(Authors oAuthors, UltraTreeNode oNode)
        {
            //add hotkey to collection of hotkeys for this editor
            this.AddHotkey("U", oNode);

            //set node properties
            oNode.Expanded = m_bShowValueNodes;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //add authors to node tag
            oNode.Tag = oAuthors;

            //add authors validation image -
            //TODO: deal with Authors validation
            oNode.LeftImages.Add(Images.ValidVariableValue);

            string xAuthors = GetAuthorsUIValue(oAuthors);

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value", xAuthors);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
            //oValueNode.Enabled = true;
        }

        private void InitializeLanguageNode(Segment oSegment, UltraTreeNode oNode)
        {
            //add hotkey to collection of hotkeys for this editor
            this.AddHotkey("L", oNode);

            //set node properties
            oNode.Expanded = true;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //add validation image -
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //create new control
            System.Windows.Forms.Control oLCB = SetupLanguageControl(oSegment);

            //add node for value -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value",
                LMP.Data.Application.GetLanguagesDisplayValue(oSegment.Culture.ToString()));
            oValueNode.Tag = oLCB;

            //add to hash table
            m_oControlsHash.Add(oValueNode.Tag, oValueNode);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
        }
        private void InitializeLanguageNode(LMP.Architect.Oxml.XmlSegment oSegment, UltraTreeNode oNode)
        {
            //add hotkey to collection of hotkeys for this editor
            this.AddHotkey("L", oNode);

            //set node properties
            oNode.Expanded = true;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //add validation image -
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //create new control
            System.Windows.Forms.Control oLCB = SetupLanguageControl(oSegment);

            //add node for value -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value",
                LMP.Data.Application.GetLanguagesDisplayValue(oSegment.Culture.ToString()));
            oValueNode.Tag = oLCB;

            //add to hash table
            m_oControlsHash.Add(oValueNode.Tag, oValueNode);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
        }


        /// <summary>
        /// initializes node corresponding to oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsTopLevel"></param>
        private void InitializeNode(Segment oSegment, UltraTreeNode oNode, bool bAsTopLevel)
        {
            DateTime t0 = DateTime.Now;

            //add image
            oNode.LeftImages.Add(GetSegmentImage(oSegment, oSegment.IsValid, oSegment.AllRequiredValuesVisited));

            //set properties
            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Always;
            oOverride.NodeSpacingAfter = 10;

            //subscribe to changes in segment validation
            oSegment.ValidationChange += new LMP.Architect.Base.ValidationChangedHandler(oSeg_ValidationChange);
            //subscribe to UpdatedForAuthor event
            oSegment.AfterAuthorUpdated += new AfterAuthorUpdatedHandler(oSeg_UpdatedForAuthor);
            oSegment.AfterAllVariablesVisited += new AfterAllVariablesVisitedHandler(oSeg_AfterAllVariablesVisited);

            //subscribe to changes in segment activation state
            oSegment.ActivationStateChanged += new ActivationStateChangedHandler(oSeg_ActivationStateChanged);
            //add reference to segment
            oNode.Tag = oSegment;

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// initializes node corresponding to oSegment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsTopLevel"></param>
        private void InitializeNode(LMP.Architect.Oxml.XmlSegment oXmlSegment, Segment oSegment, UltraTreeNode oNode, bool bAsTopLevel)
        {
            DateTime t0 = DateTime.Now;

            //add image
            oNode.LeftImages.Add(GetSegmentImage(oXmlSegment, oXmlSegment.IsValid, oXmlSegment.AllRequiredValuesVisited));

            //set properties
            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Always;
            oOverride.NodeSpacingAfter = 10;

            //subscribe to changes in segment validation
            oSegment.ValidationChange += new LMP.Architect.Base.ValidationChangedHandler(oSeg_ValidationChange);
            //subscribe to UpdatedForAuthor event
            oSegment.AfterAuthorUpdated += new AfterAuthorUpdatedHandler(oSeg_UpdatedForAuthor);
            oSegment.AfterAllVariablesVisited += new AfterAllVariablesVisitedHandler(oSeg_AfterAllVariablesVisited);

            //subscribe to changes in segment activation state
            oSegment.ActivationStateChanged += new ActivationStateChangedHandler(oSeg_ActivationStateChanged);
            //add reference to segment
            oNode.Tag = oSegment;

            LMP.Benchmarks.Print(t0);
        }

        void oSeg_ActivationStateChanged(object sender, ActivationStateChangedEventArgs e)
        {
            try
            {
                this.ExitEditMode();
                SetNodeActivation(((Segment)sender).FullTagID, e.Activated);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void AddHotkey(string xHotkey, UltraTreeNode oNode)
        {
            if (m_oHotkeys.ContainsKey(xHotkey))
            {
                //hotkey already exists -
                //add as part of array -
                //first get current hotkey node
                ArrayList oArrayList = null;
                if (m_oHotkeys[xHotkey] is UltraTreeNode)
                {
                    //get the node that is currently assigned to the hotkey
                    UltraTreeNode oCurNode = (UltraTreeNode)m_oHotkeys[xHotkey];

                    //create array
                    oArrayList = new ArrayList();

                    // Only add the node if the node belongs to a tree control.
                    // This existing node's tree control may have been cleared 
                    // during a refresh of segments.
                    if (oCurNode.Control != null)
                    {
                        //add currently assigned node
                        oArrayList.Add(oCurNode);
                    }
                }
                else
                    //there are already multiple nodes assigned
                    //to this hotkey - get them
                    oArrayList = (ArrayList)m_oHotkeys[xHotkey];

                //add this node
                oArrayList.Add(oNode);

                //add array list to hash table
                m_oHotkeys[xHotkey] = oArrayList;
            }
            else
                //assign node to hotkey
                m_oHotkeys.Add(xHotkey, oNode);
        }
        /// <summary>
        /// initializes node corresponding to oBlock
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsTopLevel"></param>
        private void InitializeNode(Block oBlock,  UltraTreeNode oNode, bool bAsTopLevel)
        {
            //get hotkey, and add to hotkey hashtable
            string xHotkey = oBlock.Hotkey;

            if(xHotkey != null)
                AddHotkey(xHotkey, oNode);

            //add image
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //add reference to block
            oNode.Tag = oBlock;

            //set node properties
            oNode.Expanded = m_bShowValueNodes;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //get beginning of block text for display
            Word.Range oRange = null;
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                oRange = oBlock.AssociatedWordTag.Range;
            else
                oRange = oBlock.AssociatedContentControl.Range;

            string xBlockStartText = LMP.Forte.MSWord.WordDoc.GetRangeStartText(oRange, 20);

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value", xBlockStartText);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
            //oValueNode.Enabled = true;
        }
        /// <summary>
        /// initializes node corresponding to oBlock
        /// </summary>
        /// <param name="oBlock"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsTopLevel"></param>
        private void InitializeNode(Block oBlock, LMP.Architect.Oxml.XmlBlock oXmlBlock, UltraTreeNode oNode, bool bAsTopLevel)
        {
            //get hotkey, and add to hotkey hashtable
            string xHotkey = oXmlBlock.Hotkey;

            if(xHotkey != null)
                AddHotkey(xHotkey, oNode);

            //add image
            oNode.LeftImages.Add(Images.ValidVariableValue);

            //add reference to block
            oNode.Tag = oBlock;

            //set node properties
            oNode.Expanded = m_bShowValueNodes;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //get beginning of block text for display
            string xBlockText = oXmlBlock.AssociatedContentControl.InnerText;
            if (xBlockText.Length > 20)
            {
                xBlockText = xBlockText.Substring(0, 20);
            }

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value", xBlockText);

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
        }
        /// <summary>
        /// selects the previous node on the tree
        /// </summary>
        private void SelectPreviousNode()
        {
            ExitEditMode();

            UltraTreeNode oNode = null;
            try
            {
                if (m_oCurTabNode != null)
                    oNode = this.m_oCurTabNode.PrevVisibleNode;
                else
                    oNode = this.m_oCurNode.PrevVisibleNode;
            }
            catch { }

            //skip over all value nodes - value nodes can't be selected
            while (oNode != null && (oNode.Key.EndsWith("_Value") || !oNode.Enabled))
                oNode = oNode.PrevVisibleNode;

            if (oNode != null)
                this.treeDocContents.ActiveNode = oNode;
            else
            {
                //If there's only one node, make sure it's selected
                if (this.treeDocContents.Nodes.Count == 1 && this.treeDocContents.Nodes[0].Nodes.Count == 0)
                {
                    m_oCurNode = null;
                    this.SelectNode(treeDocContents.Nodes[0]);
                }
            }

        }
        /// <summary>
        /// selects the next node on the tree
        /// </summary>
        private void SelectNextNode()
        {
            //GLOG item #4588 - dcf -
            //fix for GLOG item #3802 was buggy
            //GLOG : 3802 : CEH
            //get current node key for later use -
            //ExitEditMode may force a reconstitution
            //of nodes, rendering the current node
            //different than the original
            string xKey;
            UltraTreeNode oCurNode = null;
            UltraTreeNode oNode = null;
            //GLOG 4670: Don't call ExitEditMode if currently tabbing without displaying controls
            if (m_oCurTabNode == null && m_oCurNode == null)
                oCurNode = this.treeDocContents.ActiveNode;
            else if (m_oCurTabNode != null)
                oCurNode = m_oCurTabNode;
            else
            {
                xKey = this.m_oCurNode.Key;
                ExitEditMode();
                oCurNode = this.treeDocContents.GetNodeByKey(xKey);
            }

            try
            {
                oNode = oCurNode.NextVisibleNode;
            }
            catch { }

            //skip over all value nodes - value nodes can't be selected
            while (oNode != null && (oNode.Key.EndsWith("_Value") || !oNode.Enabled))
                oNode = oNode.NextVisibleNode;

            if (oNode != null)
            {
                //there is a next node - select it
                this.treeDocContents.ActiveNode = oNode;
                if (m_oCurCtl != null)
                    m_oCurCtl.Focus();
            }
            else
            {
                //there is no next node - select first node
                this.treeDocContents.ActiveNode = this.treeDocContents.Nodes[0];
                //If there's only one control, make sure it's selected
                if (this.treeDocContents.Nodes.Count == 1 && this.treeDocContents.ActiveNode.Nodes.Count == 0)
                {
                    m_oCurNode = null;
                    this.SelectNode(treeDocContents.ActiveNode);
                }
            }
        }
        /// <summary>
        /// execute all actions defined for the specified event,
        /// or, if no event has been specified, all actions
        /// </summary>
        /// <param name="oEvent"></param>
        private void ExecuteControlActions(Variable oVariable, ControlActions.Events oEvent)
        {
            ExecuteControlActions(oVariable.ControlActions, oEvent);
        }
        private void ExecuteControlActions(Authors oAuthors, ControlActions.Events oEvent)
        {
            ExecuteControlActions(oAuthors.ControlActions, oEvent);
        }
        private void ExecuteControlActions(ControlActions oActions, ControlActions.Events oEvent)
        {
            //cycle through actions in collection and execute
            for (int i = 0; i < oActions.Count; i++)
            {
                ControlAction oAction = oActions[i];

                //execute only if the action has beeen specified
                //to run on the specified event, or if
                //no event has been specified
                if (oEvent == 0 || oAction.Event == oEvent)
                {
                    if (oAction.Type == ControlActions.Types.EndExecution)
                        //GLOG 7069: Only return if EndExecution type is encountered
                        return;
                    else if (!oAction.ExecutionIsSpecified())
                    {
                        //GLOG 7069: If condition not met, continue with next action
                        continue;
                    }

                    try
                    {
                        ExecuteControlAction(oAction);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.ActionException(
                            LMP.Resources.GetLangString("Error_CouldNotExecuteAction") +
                            oAction.ID.ToString(), oE);
                    }
                }
            }
        }
        /// <summary>
        /// executes the specified ControlAction
        /// </summary>
        /// <param name="oAction"></param>
        private void ExecuteControlAction(ControlAction oAction)
        {
            DateTime t0 = DateTime.Now;

            string xParameters = oAction.Parameters;

            Trace.WriteNameValuePairs("xParameters", xParameters);

            //exit if condition for insertion is not satisfied
            if (!oAction.ExecutionIsSpecified())
                return;

            Variable oVar = oAction.ParentVariable;

            if (xParameters != "" && oVar != null)
            {
                string xValue = oVar.Value;
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                //They will treated as the literal characters by the actions
                xValue = Expression.MarkLiteralReservedCharacters(xValue);

                //evaluate MyValue first
                xParameters = LMP.Architect.Api.FieldCode.EvaluateMyValue(xParameters, xValue);
            }

            try
            {
                switch (oAction.Type)
                {
                    case ControlActions.Types.ChangeLabel:
                        ExecuteControlActionChangeLabel(oAction, xParameters);
                        break;
                    case ControlActions.Types.Enable:
                        ExecuteControlActionEnable(oAction, xParameters);
                        break;
                    case ControlActions.Types.ExecuteControlActionGroup:
                        break;
                    case ControlActions.Types.RunMethod:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            oAction.Execute();
                        break;
                    case ControlActions.Types.SetControlProperty:
                        break;
                    case ControlActions.Types.SetFocus:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionSetFocus(oAction, xParameters);
                        break;
                    case ControlActions.Types.SetVariableValue:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionSetVariableValue(oAction, xParameters);
                        break;
                    case ControlActions.Types.Visible:
                        ExecuteControlActionVisible(oAction, xParameters);
                        break;
                    case ControlActions.Types.DisplayMessage:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionDisplayMessage(oAction, xParameters);
                        break;
                    case ControlActions.Types.RefreshControl:
                        //GLOG 4276: Don't run this action if just refreshing tree or Segment node
                        if (!this.IsRefreshingSegmentNode)
                            ExecuteControlActionRefreshControl(oAction, xParameters);
                        break;
                    default:
                        break;
                }
            }
            catch (System.Exception oE)
            {
                //action failed
                throw new LMP.Exceptions.ActionException(
                    LMP.Resources.GetLangString("Error_ControlActionExecutionFailed") + //GLOG 7299: Incorrect resource string name specified
                    oVar.Name, oE);
            }

            if(oAction.ParentVariable != null)
                LMP.Benchmarks.Print(t0, Convert.ToString(oAction.Type), 
                    "Value=" + oVar.Value, "Parameters=" + xParameters);
            else
                LMP.Benchmarks.Print(t0, Convert.ToString(oAction.Type),
                    "Parameters=" + xParameters);
        }
        private void ExecuteControlActionEnable(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";
            mpTriState bEnable = mpTriState.Undefined;

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    string xEnable = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);
                    
                    switch (xEnable.ToUpper())
                    {
                        case "1":
                        case "TRUE":
                            bEnable = mpTriState.True;
                            break;
                        case "0":
                        case "FALSE":
                            bEnable = mpTriState.False;
                            break;
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //get target node from variable
                Variable oVar = null;
                oVar = GetVariableFromPath(oSegment, xTargetVarPath);

                if (oVar != null && bEnable != mpTriState.Undefined)
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        // Check for node matching Segment and Variable
                        oNode = this.GetNodeFromTagID(oVar.Segment.FullTagID + "." + oVar.ID);
                    }
                    catch { }

                    if (oNode != null)
                        //node exists - set node enabled state
                        oNode.Enabled = (bEnable == mpTriState.True);
                    else
                        //node does not exist - add to the list of
                        //pending control actions - the action will
                        //be executed when the node is first 
                        //created and initialized
                        AddPendingControlAction(oVar.TagID, oAction);
                }
            }
        }
        private void ExecuteControlActionSetFocus(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //get target node from variable
                Variable oVar = null;
                oVar = GetVariableFromPath(oSegment, xTargetVarPath);

                if (oVar != null)
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        // Check for node matching Segment and Variable
                        oNode = this.GetNodeFromTagID(oVar.Segment.FullTagID + "." + oVar.ID);
                    }
                    catch { }

                    if (oNode != null)
                    {
                        //node exists - set Focus
                        SelectNode(oNode);
                    }
                    else
                    {
                        //***THIS DOESN'T SEEM TO MAKE SENSE FOR THIS ACTION TYPE
                        //node does not exist - add to the list of
                        //pending control actions - the action will
                        //be executed when the node is first 
                        //created and initialized
                        //AddPendingControlAction(oVar.TagID, oAction);
                    }
                }
            }
        }
        private void ExecuteControlActionVisible(ControlAction oAction, string xParameters)
        {
            //GLOG - 3395 - CEH
            string xTargetVarPath = "";
            mpTriState bVisible = mpTriState.Undefined;
            string xName = "";
            string[] aNames = null;

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //GLOG - 3395 - CEH
                    //Allow for more multiple variable names
                    //get parameters
                    xName = aParams[0];
                    //Can be list of Variable names
                    aNames = xName.Split(',');

                    string xVisible = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);

                    switch (xVisible.ToUpper())
                    {
                        case "1":
                        case "TRUE":
                            bVisible = mpTriState.True;
                            break;
                        case "0":
                        case "FALSE":
                            bVisible = mpTriState.False;
                            break;
                    }
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }

                Trace.WriteNameValuePairs("xName", xName);

                //Cycle through each variable
                foreach (string xVar in aNames)
                {
                    xTargetVarPath = Expression.Evaluate(xVar,
                        oSegment, m_oMPDocument);

                    //get target node from variable
                    Variable oVar = null;
                    oVar = GetVariableFromPath(oSegment, xTargetVarPath);

                    if (oVar != null && bVisible != mpTriState.Undefined)
                    {
                        UltraTreeNode oNode = null;
                        try
                        {
                            // Check for node matching Segment and Variable
                            oNode = this.GetNodeFromTagID(oVar.Segment.FullTagID + "." +  oVar.ID);
                        }
                        catch { }

                        if (oNode != null)
                            //node exists - set node visible state
                            oNode.Visible = (bVisible == mpTriState.True);
                        else
                            //node does not exist - add to the list of
                            //pending control actions - the action will
                            //be executed when the node is first 
                            //created and initialized
                            AddPendingControlAction(oVar.TagID, oAction);
                    }
                }
            }
        }
        /// <summary>
        /// GLOG 2645: Update control properties that contain MacPac expressions
        /// </summary>
        /// <param name="oAction"></param>
        /// <param name="xParameters"></param>
        private void ExecuteControlActionRefreshControl(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 1)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //get target node from variable
                Variable oVar = null;
                oVar = GetVariableFromPath(oSegment, xTargetVarPath);

                if (oVar != null)
                {
                    string xCtlValue = "";
                    if (oVar.AssociatedControl == null)
                        oVar.CreateAssociatedControl(this);
                    else
                        //GLOG 3267: Only compare value if control was previously created
                        xCtlValue = oVar.AssociatedControl.Value;

                    IControl oICtl = oVar.AssociatedControl;
                    System.Windows.Forms.Control oCtl = (System.Windows.Forms.Control)oICtl;
                    System.Type oCtlType = oCtl.GetType();

                    //get control properties from variable
                    string[] aProps = null;

                    if (oVar.ControlProperties != "")
                        aProps = oVar.ControlProperties.Split(StringArray.mpEndOfSubValue);
                    else
                        aProps = new string[0];

                    bool bChanged = false;
                    //cycle through property name/value pairs, setting those that 
                    //have a MacPac Expression as the value
                    for (int i = 0; i < aProps.Length; i++)
                    {
                        int iPos = aProps[i].IndexOf("=");
                        string xName = aProps[i].Substring(0, iPos);
                        string xValue = aProps[i].Substring(iPos + 1);

                        //evaluate control prop value, which may be a MacPac expression
                        string xEval = Expression.Evaluate(xValue, oVar.Segment, oVar.ForteDocument);
                        //Only set those properties using expressions
                        if (xValue != xEval)
                        {
                            PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                            System.Type oPropType = oPropInfo.PropertyType;

                            object oValue = null;

                            if (oPropType.IsEnum)
                                oValue = Enum.Parse(oPropType, xEval);
                            else
                                oValue = Convert.ChangeType(xEval, oPropType,
                                    LMP.Culture.USEnglishCulture);

                            oPropInfo.SetValue(oCtl, oValue, null);
                            bChanged = true;
                        }
                    }
                    //Update Variable Word Tag with latest Control Values if necessary
                    if (bChanged)
                    {
                        if (oICtl.Value != xCtlValue)
                            oVar.SetValue(oICtl.Value);
                        //Also update RuntimeControlValues - these can change without changing
                        //Control Value
                        oVar.SetRuntimeControlValues(oICtl.SupportingValues);
                    }
                }
            }
        }
        private void ExecuteControlActionChangeLabel(ControlAction oAction, string xParameters)
        {
            string xTargetVarPath = "";
            string xNewLabel = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    xNewLabel = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);
                    if (xNewLabel == "Null")
                        xNewLabel = "";

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }

                //get target node from variable
                Variable oVar = null;
                oVar = GetVariableFromPath(oSegment, xTargetVarPath);

                if (oVar != null)
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        // Check for node matching Segment and Variable
                        oNode = this.GetNodeFromTagID(oVar.Segment.FullTagID + "." + oVar.ID);
                    }
                    catch { }

                    if (oNode != null)
                        //node exists - set node enabled state
                        oNode.Text = xNewLabel;
                    else
                        //node does not exist - add to the list of
                        //pending control actions - the action will
                        //be executed when the node is first 
                        //created and initialized
                        AddPendingControlAction(oVar.TagID, oAction);
                }
            }
        }
        /// <summary>
        /// Displays message box
        /// </summary>
        /// <param name="xParameters"></param>
        protected void ExecuteControlActionDisplayMessage(ControlAction oAction, string xParameters)
        {
            string xMsg = "";
            string xCaption = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString") + xParameters);

            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //1 parameter is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") + xParameters);
            else
            {
                try
                {
                    //get parameters
                    xMsg = Expression.Evaluate(aParams[0], oSegment, m_oMPDocument);
                    xCaption = Expression.Evaluate(aParams[1], oSegment, m_oMPDocument);
                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") + xParameters, oE);
                }
            }

            Trace.WriteNameValuePairs("xMsg", xMsg, "xCaption", xCaption);

            if (xMsg != "")
            {
                if (xCaption == "")
                    xCaption = LMP.ComponentProperties.ProductName;
                MessageBox.Show(xMsg, xCaption, MessageBoxButtons.OK);
            }
        }
        /// <summary>
        /// adds the specified control action to the list of pending
        /// control actions - these actions are executed when the
        /// target node of the action is created - the action is 
        /// considered pending because the target node does not yet exist
        /// </summary>
        /// <param name="xTagID"></param>
        /// <param name="oAction"></param>
        private void AddPendingControlAction(string xTagID, ControlAction oAction)
        {
            Trace.WriteNameValuePairs("xTagID", xTagID, "oAction", oAction);

            if (this.m_oPendingControlActions.ContainsKey(xTagID)){
                //key exists - add action to collection of actions
                List<ControlAction> oActions = (List<ControlAction>)
                    this.m_oPendingControlActions[xTagID];
                oActions.Add(oAction);
            }
            else{
                //add key
                List<ControlAction> oActions = new List<ControlAction>();
                oActions.Add(oAction);
                this.m_oPendingControlActions.Add(xTagID, oActions);
            }
        }
        private void ExecuteControlActionSetVariableValue(ControlAction oAction, string xParameters)
        {
            if (oAction.ParentSegment.CreationStatus != Segment.Status.Finished)
                return;

            string xTargetVarPath = "";
            string xNewValue = "";

            Segment oSegment = oAction.ParentSegment;

            if (xParameters == "")
                //parameters are required for this action
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_InvalidParameterString"));

            //parse parameter string
            string[] aParams = xParameters.Split(LMP.StringArray.mpEndOfSubField);

            if (aParams.Length != 2)
                //2 parameters is required
                throw new LMP.Exceptions.ArgumentException(
                    LMP.Resources.GetLangString("Error_WrongNumberOfArguments") +
                    xParameters);
            else
            {
                try
                {
                    //get parameters
                    xTargetVarPath = Expression.Evaluate(aParams[0],
                        oSegment, m_oMPDocument);

                    xNewValue = Expression.Evaluate(aParams[1],
                        oSegment, m_oMPDocument);

                }
                catch (System.Exception oE)
                {
                    //one of the parameters is invalid
                    throw new LMP.Exceptions.ArgumentException(
                        LMP.Resources.GetLangString("Error_InvalidParameterValue") +
                        xParameters, oE);
                }
                Variable oVar = null;
                oVar = GetVariableFromPath(oSegment, xTargetVarPath);
                if (oVar != null)
                    oVar.SetValue(xNewValue);
            }
        }
        /// <summary>
        /// GLOG 2999: Updated to support Reference Target format
        /// Given a path in the form x.y.z.var, returns 
        /// Variable object from specified Child segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="xVariablePath"></param>
        /// <returns></returns>
        private Variable GetVariableFromPath(Segment oSegment, string xVariablePath)
        {
            Segment oTargetSegment = null;
            Variable oVar = null;
            string xTargetVar = "";

            if (xVariablePath.IndexOf(".") > 0)
            {
                // Variable belongs to a child segment - form is x.y.z.var
                // This is the old method - code left in place to support existing content
                int iLastSep = xVariablePath.LastIndexOf(".");
                xTargetVar = xVariablePath.Substring(iLastSep + 1);
                string xChildSegmentPath = xVariablePath.Substring(0, iLastSep);
                oTargetSegment = oSegment.GetChildSegmentFromPath(xChildSegmentPath);
                try
                {
                    if (oTargetSegment != null)
                    {
                        oVar = oTargetSegment.Variables.ItemFromName(xTargetVar);
                    }
                }
                catch
                {
                    // No match found
                    return null;
                }
            }
            else
            {
                //GLOG 2999: Reference target method
                xTargetVar = xVariablePath;
                Segment[] oTargets = oSegment.GetReferenceTargets(ref xTargetVar);
                if (oTargets != null)
                {
                    foreach (Segment oTarget in oTargets)
                    {
                        //get target variable
                        oVar = oTarget.GetVariable(xTargetVar);

                        //stop after finding match
                        if (oVar != null)
                        {
                            break;
                        }
                    }
                }
            }
            return oVar;
        }
        /// <summary>
        /// creates the associated menu for the specified segment
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private void CreateSegmentMenu(Segment oSegment)
        {
            //GLOG : 7206 : ceh
			//GLOG : 7998 : ceh
            bool bRibbonOnlyImplementation = (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator);

            //tooltips removed per GLOG item #4177 - df
            if (m_oMenuSegment == null)
            {
                //create new segment menu
                m_oMenuSegment = new TimedContextMenuStrip(this.components);
                //m_oMenuSegment.TimerInterval = 1000;
                m_oMenuSegment.ItemClicked += new ToolStripItemClickedEventHandler(m_oMenuSegment_ItemClicked);
                m_oMenuSegment.ShowItemToolTips = false;
            }
            else
                //clear existing menu
                m_oMenuSegment.Items.Clear();

            if (m_oMenuSegment_RecreateSegment == null)
            {
                m_oMenuSegment_RecreateSegment = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_RecreateSegment.Name = "m_oMenuSegment_RecreateSegment";
                m_oMenuSegment_RecreateSegment.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_RecreateSegment");
                m_oMenuSegment_RecreateSegment.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_RecreateSegmentTooltip");
            }

            m_oMenuSegment_RecreateSegment.Enabled = !oSegment.IsFinished;

            if (m_oMenuSegment_Update == null)
            {
                m_oMenuSegment_Update = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_Update.Name = "m_oMenuSegment_UpdateSegment";
                m_oMenuSegment_Update.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_UpdateSegment");
                m_oMenuSegment_Update.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_UpdateSegmentTooltip");
                m_oMenuSegment_Update.Visible = false;
            }

            if (m_oMenuSegment_UpdateWithSelection == null)
            {
                m_oMenuSegment_UpdateWithSelection = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_UpdateWithSelection.Name = "m_oMenuSegment_UpdateSegmentWithSelection";
                m_oMenuSegment_UpdateWithSelection.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_UpdateSegmentWithSelection");
                m_oMenuSegment_UpdateWithSelection.Visible = false;
            }

            if (m_oMenuSegment_SaveData == null)
            {
                m_oMenuSegment_SaveData = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_SaveData.Name = "m_oMenuSegment_SaveData";
                m_oMenuSegment_SaveData.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_CreatePrefill");
                m_oMenuSegment_SaveData.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_CreatePrefillTooltip");
            }

            m_oMenuSegment_SaveData.Enabled = !oSegment.IsFinished;

            if (m_oMenuSegment_SaveContent == null)
            {
                m_oMenuSegment_SaveContent = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_SaveContent.Name = "m_oMenuSegment_SaveContent";
                m_oMenuSegment_SaveContent.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_SaveContent");
                m_oMenuSegment_SaveContent.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_SaveContentTooltip");
            }

            //GLOG : 7310 :  JSW
            m_oMenuSegment_SaveContent.Visible = oSegment.IsTopLevel;
            m_oMenuSegment_SaveContent.Enabled = !oSegment.IsFinished;

            if (m_oMenuSegment_CopySegment == null)
            {
                m_oMenuSegment_CopySegment = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_CopySegment.Name = "m_oMenuSegment_CopySegment";
                m_oMenuSegment_CopySegment.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_CopySegment");
                m_oMenuSegment_CopySegment.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_CopySegmentTooltip");
            }

            if (m_oMenuSegment_Activate == null)
            {
                m_oMenuSegment_Activate = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_Activate.Name = "m_oMenuSegment_Activate";
                //m_oMenuSegment_Activate.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Activate");
                //m_oMenuSegment_Activate.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_ActivateTooltip");
            }

            m_oMenuSegment_Activate.Visible = !oSegment.Activated;

            if (m_oMenuSegment_Refresh == null)
            {
                m_oMenuSegment_Refresh = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_Refresh.Name = "m_oMenuSegment_Refresh";
                m_oMenuSegment_Refresh.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Refresh");
                m_oMenuSegment_Refresh.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_RefreshTooltip");
                m_oMenuSegment_Refresh.Visible = true;
            }
            
            if (m_oMenuSegment_RemoveVarAndBlockTags == null)
            {
                m_oMenuSegment_RemoveVarAndBlockTags = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_RemoveVarAndBlockTags.Name = "m_oMenuSegment_RemoveVarAndBlockTags";
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                {
                    m_oMenuSegment_RemoveVarAndBlockTags.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_RemoveContentControls");
                    m_oMenuSegment_RemoveVarAndBlockTags.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_RemoveContentControls");
                }
                else
                {
                    m_oMenuSegment_RemoveVarAndBlockTags.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_RemoveTags");
                    m_oMenuSegment_RemoveVarAndBlockTags.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_RemoveTags");
                }
                m_oMenuSegment_RemoveVarAndBlockTags.Visible = false; //LMP.MacPac.Session.CurrentUser.FirmSettings.AllowSegmentMemberTagRemoval;
            }

            if (m_oMenuSegment_Delete == null)
            {
                m_oMenuSegment_Delete = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_Delete.Name = "m_oMenuSegment_Delete";
                m_oMenuSegment_Delete.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_DeleteSegment");
                m_oMenuSegment_Delete.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_DeleteSegmentTooltip");
            }

            if (m_oMenuSegment_RefreshStyles == null)
            {
                m_oMenuSegment_RefreshStyles = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_RefreshStyles.Name = "m_oMenuSegment_RefreshStyles";
                m_oMenuSegment_RefreshStyles.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_RefreshStyles");
                m_oMenuSegment_RefreshStyles.ToolTipText = LMP.Resources.GetLangString("Menu_DocumentEditor_RefreshStylesTooltip");
            }

            if (m_oMenuSegment_Help == null)
            {
                m_oMenuSegment_Help = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuSegment_Help.Name = "m_oMenuSegment_Help";
                m_oMenuSegment_Help.Text = LMP.Resources.GetLangString("mnuSegment_Help");
            }

            ToolStripSeparator oSep3 = new ToolStripSeparator();
            oSep3.Visible = false; //LMP.MacPac.Session.CurrentUser.FirmSettings.AllowSegmentMemberTagRemoval;

            //add common menu items
            //GLOG : 7206 : CEH
            if (bRibbonOnlyImplementation)
                m_oMenuSegment.Items.Add(m_oMenuSegment_Refresh);
            else if (oSegment is CollectionTable)
            {
                //Don't allow Save Data, RecreateSegment, or RefreshStyles for Collections
                m_oMenuSegment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                m_oMenuSegment_Update, 
                m_oMenuSegment_UpdateWithSelection,
                m_oMenuSegment_CopySegment, 
                m_oMenuSegment_Delete, 
                new ToolStripSeparator(),
                m_oMenuSegment_Refresh});
            }
            else
            {
                m_oMenuSegment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                m_oMenuSegment_SaveData,
                m_oMenuSegment_SaveContent,
                new ToolStripSeparator(), 
                m_oMenuSegment_Activate,
                m_oMenuSegment_RecreateSegment, 
                m_oMenuSegment_Update, 
                m_oMenuSegment_UpdateWithSelection,
                m_oMenuSegment_CopySegment, 
                m_oMenuSegment_Delete, 
                new ToolStripSeparator(), 
                m_oMenuSegment_RefreshStyles,
                m_oMenuSegment_Refresh, 
                oSep3,
                m_oMenuSegment_RemoveVarAndBlockTags});
            }
            //add type-specific menu items
            AddTypeSpecificMenuItems(oSegment);

            //add type-specific menu items for transparent children
            AddMenuItemsForTransparentChildren(oSegment);

            // Add Quick Help Menu item in its own section.
            m_oMenuSegment.Items.Add(new ToolStripSeparator());
            m_oMenuSegment.Items.Add(m_oMenuSegment_Help);
        }

        private void CreateVariableMenu(Variable oVar)
        {
            if (m_oMenuVariable == null)
            {
                //create new segment menu
                m_oMenuVariable = new TimedContextMenuStrip(this.components);
                //m_oMenuVariable.TimerInterval = 1000;
                m_oMenuVariable.ItemClicked += new ToolStripItemClickedEventHandler(m_oMenuVariable_ItemClicked);
            }
            else
            {
                //clear existing menu
                m_oMenuVariable.Items.Clear();
            }

            if (this.m_oMenuVariable_Help == null)
            {
                m_oMenuVariable_Help = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuVariable_Help.Name = "m_oMenuVariable_Help";
                m_oMenuVariable_Help.Text = LMP.Resources.GetLangString("MenuVariable_Help");
            }

            if (this.m_oMenuVariable_RemoveTag == null)
            {
                m_oMenuVariable_RemoveTag = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuVariable_RemoveTag.Name = "m_oMenuVariable_RemoveTag";
                if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                {
                    m_oMenuVariable_RemoveTag.Text = LMP.Resources.GetLangString("m_oMenuVariable_RemoveContentControl");
                }
                else
                {
                    m_oMenuVariable_RemoveTag.Text = LMP.Resources.GetLangString("m_oMenuVariable_RemoveTag");
                }
                m_oMenuVariable_RemoveTag.Visible = LMP.MacPac.Session.CurrentUser.FirmSettings.AllowVariableBlockTagRemoval;
            }

            m_oMenuVariable.Items.Add(m_oMenuVariable_Help);
            m_oMenuVariable.Items.Add(m_oMenuVariable_RemoveTag);
        }

        void m_oMenuVariable_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (m_oCurNode.Tag is Variable)
                {
                    switch (e.ClickedItem.Name)
                    {
                        case "m_oMenuVariable_Help":

                            if (this.m_oCurNode.Tag is Variable)
                            {
                                //Variable oVar = (Variable)this.m_oCurNode.Tag;
                                //TaskPane.SetHelpText(this.wbDescription, oVar.HelpText);
                                this.ShowHelp();
                            }
                            break;
                        case "m_oMenuVariable_RemoveTag":
                            this.RemoveTag();
                            break;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        // GLOG : 3135 : JAB
        // Create the Authors menu.
        private void CreateAuthorsMenu(Authors oAuthors)
        {
            if (this.m_oMenuAuthors == null)
            {
                // Create a new Authors menu.
                m_oMenuAuthors = new TimedContextMenuStrip(this.components);
                m_oMenuAuthors.TimerInterval = 1000;
                m_oMenuAuthors.ItemClicked += new ToolStripItemClickedEventHandler(m_oMenuAuthors_ItemClicked);
            }
            else
            {
                // Empty the menu items collection.
                m_oMenuAuthors.Items.Clear();
            }

            // GLOG : 3135 : JAB
            // Only show the help menu item.
            if (this.m_oMenuVariable_Help == null)
            {
                m_oMenuVariable_Help = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuVariable_Help.Name = "m_oMenuVariable_Help";
                m_oMenuVariable_Help.Text = LMP.Resources.GetLangString("MenuVariable_Help");
            }

            m_oMenuAuthors.Items.Add(m_oMenuVariable_Help);
        }

        // GLOG : 5398 : CEH
        // Create the Jurisdiction menu.
        private void CreateJurisdictionsMenu(Jurisdictions oJurisdictions)
        {
            if (this.m_oMenuJurisdictions == null)
            {
                // Create a new Authors menu.
                m_oMenuJurisdictions = new TimedContextMenuStrip(this.components);
                m_oMenuJurisdictions.TimerInterval = 1000;
                m_oMenuJurisdictions.ItemClicked += new ToolStripItemClickedEventHandler(m_oMenuJurisdictions_ItemClicked);
            }
            else
            {
                // Empty the menu items collection.
                m_oMenuJurisdictions.Items.Clear();
            }

            // GLOG : 3135 : JAB
            // Only show the help menu item.
            if (this.m_oMenuVariable_Help == null)
            {
                m_oMenuVariable_Help = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuVariable_Help.Name = "m_oMenuVariable_Help";
                m_oMenuVariable_Help.Text = LMP.Resources.GetLangString("MenuVariable_Help");
            }

            m_oMenuJurisdictions.Items.Add(m_oMenuVariable_Help);
        }
        
        private void CreateBlockMenu(Block oBlock)
        {
            if (m_oMenuBlock == null)
            {
                //create new segment menu
                m_oMenuBlock = new TimedContextMenuStrip(this.components);
               // m_oMenuBlock.TimerInterval = 1000;
                m_oMenuBlock.ItemClicked +=new ToolStripItemClickedEventHandler(m_oMenuBlock_ItemClicked);
            }
            else
            {
                //clear existing menu
                m_oMenuBlock.Items.Clear();
            }

            if (this.m_oMenuBlock_RemoveTag == null)
            {
                m_oMenuBlock_RemoveTag = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuBlock_RemoveTag.Name = "m_oMenuBlock_RemoveTag";

                if (m_oMPDocument.FileFormat == mpFileFormats.OpenXML)
                {
                    m_oMenuBlock_RemoveTag.Text = LMP.Resources.GetLangString("MenuBlock_RemoveContentControl");
                }
                else
                {
                    m_oMenuBlock_RemoveTag.Text = LMP.Resources.GetLangString("MenuBlock_RemoveTag");
                }

                m_oMenuBlock_RemoveTag.Visible = LMP.MacPac.Session.CurrentUser.FirmSettings.AllowVariableBlockTagRemoval;
            }

            if (this.m_oMenuBlock_Help == null)
            {
                m_oMenuBlock_Help = new System.Windows.Forms.ToolStripMenuItem();
                m_oMenuBlock_Help.Name = "m_oMenuBlock_Help";
                m_oMenuBlock_Help.Text = LMP.Resources.GetLangString("MenuBlock_Help");
            }

            m_oMenuBlock.Items.Add(m_oMenuBlock_Help);
            m_oMenuBlock.Items.Add(m_oMenuBlock_RemoveTag);
        }

        // GLOG : 5398 : CEH
        // Handle menu item selection from the jurisdictions menu.
        void m_oMenuJurisdictions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (m_oCurNode.Tag is Jurisdictions)
                {
                    switch (e.ClickedItem.Name)
                    {
                        //case "m_oMenuAuthors_Refresh":
                        //    LMP.MacPac.Application.UpdateAuthor();
                        //    break;


                        case "m_oMenuVariable_Help":
                            this.ShowHelp();
                            break;
                    }
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }


        // GLOG : 3135 : JAB
        // Handle menu item selection from the authors menu.
        void m_oMenuAuthors_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (m_oCurNode.Tag is Authors)
                {
                    switch (e.ClickedItem.Name)
                    {
                        case "m_oMenuAuthors_Refresh":
                            LMP.MacPac.Application.UpdateAuthor();
                            break;


                        case "m_oMenuVariable_Help":
                            this.ShowHelp();
                            break;
                    }
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void m_oMenuBlock_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (m_oCurNode.Tag is Block)
                {
                    switch (e.ClickedItem.Name)
                    {
                        case "m_oMenuBlock_Help":
                            this.ShowHelp();
                            break;
                        case "m_oMenuBlock_RemoveTag":
                            this.RemoveTag();
                            break;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// adds type-specific menu items for transparent children
        /// </summary>
        /// <param name="oSegment"></param>
        private void AddMenuItemsForTransparentChildren(Segment oSegment)
        {
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Segment oChild = oSegment.Segments[i];
                //GLOG 3787: Don't display Child menus if there's more than one of this type
                Segment [] oChildren = oSegment.FindChildren(oChild.TypeID);
                if (oChild.IsTransparentDef && oChildren.Length == 1)
                {
                    AddTypeSpecificMenuItems(oChild);
                    AddMenuItemsForTransparentChildren(oChild);
                }
            }
        }

        private void AddTypeSpecificMenuItems(Segment oSegment)
        {
            //GLOG : 7206 : ceh
			//GLOG : 7998 : ceh
            bool bRibbonOnlyImplementation = (LMP.MacPac.MacPacImplementation.IsToolkit || LMP.MacPac.MacPacImplementation.IsToolsAdministrator);

            switch (oSegment.TypeID)
            {
                case mpObjectTypes.Pleading:
                    m_oMenuSegment.Items.Add(new ToolStripSeparator());

                    //GLOG 3936 (dm) - add Insert Counsel when there's no existing collection
                    if (!oSegment.Contains(mpObjectTypes.PleadingCounsels))
                    {
                        if (m_oMenuSegment_Pleading_InsertCounsel == null)
                        {
                            m_oMenuSegment_Pleading_InsertCounsel = new ToolStripMenuItem();
                            m_oMenuSegment_Pleading_InsertCounsel.Name =
                                "m_oMenuSegment_Pleading_InsertCounsel";
                            m_oMenuSegment_Pleading_InsertCounsel.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_InsertPleadingCounsel");
                            m_oMenuSegment_Pleading_InsertCounsel.Tag = 1;
                        }
                        AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_InsertCounsel);
                    }

                    //GLOG 3936 (dm) - add Insert Caption when there's no existing collection
                    if (!oSegment.Contains(mpObjectTypes.PleadingCaptions))
                    {
                        if (m_oMenuSegment_Pleading_InsertCaption == null)
                        {
                            m_oMenuSegment_Pleading_InsertCaption = new ToolStripMenuItem();
                            m_oMenuSegment_Pleading_InsertCaption.Name =
                                "m_oMenuSegment_Pleading_InsertCaption";
                            m_oMenuSegment_Pleading_InsertCaption.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_InsertPleadingCaption");
                            m_oMenuSegment_Pleading_InsertCaption.Tag = 2;
                        }
                        AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_InsertCaption);
                    }

                    //add Exhibits menu item
                    if (m_oMenuSegment_Pleading_InsertExhibits == null)
                    {
                        m_oMenuSegment_Pleading_InsertExhibits = new ToolStripMenuItem();
                        m_oMenuSegment_Pleading_InsertExhibits.Name =
                            "m_oMenuSegment_Pleading_InsertExhibits";
                        m_oMenuSegment_Pleading_InsertExhibits.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_Pleading_InsertExhibits");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_Pleading_InsertExhibits.Tag = 6;
                    }

                    AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_InsertExhibits);
                    
                    //add Insert Interrogatories
                    if (m_oMenuSegment_Pleading_InsertInterrogatories == null)
                    {
                        m_oMenuSegment_Pleading_InsertInterrogatories = new ToolStripMenuItem();
                        m_oMenuSegment_Pleading_InsertInterrogatories.Name =
                            "m_oMenuSegment_Pleading_InsertInterrogatories";
                        m_oMenuSegment_Pleading_InsertInterrogatories.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_Pleading_InsertInterrogatories");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_Pleading_InsertInterrogatories.Tag = 7;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_InsertInterrogatories);

                    //add Insert TOA menu item
                    //GLOG 7243 (dm) - if there's an existing TOA, display update option instead -
                    //users would never want multiple TOAs in a document
                    if (!m_oMPDocument.ContainsSegmentOfType(mpObjectTypes.TOA))
                    {
                        if (m_oMenuSegment_Pleading_InsertTOA == null)
                        {
                            m_oMenuSegment_Pleading_InsertTOA = new ToolStripMenuItem();
                            m_oMenuSegment_Pleading_InsertTOA.Name = "m_oMenuSegment_Pleading_InsertTOA";
                            m_oMenuSegment_Pleading_InsertTOA.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_Pleading_InsertTOA");

                            //assign a position index - the optimal position for this item in the menu
                            m_oMenuSegment_Pleading_InsertTOA.Tag = 8;
                        }

                        AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_InsertTOA);
                    }
                    else
                    {
                        //GLOG 7243 (dm)
                        if (m_oMenuSegment_Pleading_UpdateTOA == null)
                        {
                            m_oMenuSegment_Pleading_UpdateTOA = new ToolStripMenuItem();
                            m_oMenuSegment_Pleading_UpdateTOA.Name = "m_oMenuSegment_Pleading_UpdateTOA";
                            m_oMenuSegment_Pleading_UpdateTOA.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_TOA_Update");

                            //assign a position index - the optimal position for this item in the menu
                            m_oMenuSegment_Pleading_UpdateTOA.Tag = 8;
                        }

                        AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_UpdateTOA);
                    }

                    //GLOG : 5724 : CEH
                    //add Mark Citation menu item
                    if (m_oMenuSegment_Pleading_MarkCitation == null)
                    {
                        m_oMenuSegment_Pleading_MarkCitation = new ToolStripMenuItem();
                        m_oMenuSegment_Pleading_MarkCitation.Name = "m_oMenuSegment_Pleading_MarkCitation";
                        m_oMenuSegment_Pleading_MarkCitation.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_Pleading_MarkCitation");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_Pleading_MarkCitation.Tag = 9;
                    }

                    AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_MarkCitation);

                    //add Align Pleading Paper menu item
                    if (m_oMenuSegment_Pleading_AlignPleadingPaper == null)
                    {
                        m_oMenuSegment_Pleading_AlignPleadingPaper = new ToolStripMenuItem();
                        m_oMenuSegment_Pleading_AlignPleadingPaper.Name =
                            "m_oMenuSegment_Pleading_AlignPleadingPaper";
                        m_oMenuSegment_Pleading_AlignPleadingPaper.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_Pleading_AlignPleadingPaper");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_Pleading_AlignPleadingPaper.Tag = 10;
                    }

                    if (oSegment.Contains(LMP.Data.mpObjectTypes.PleadingPaper))
                    {
                        AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_AlignPleadingPaper);

                        //create context menu
                        mnuPleadingPaperOptions = new TimedContextMenuStrip(this.components);
                        mnuPleadingPaperOptions.Name = "mnuPleadingPaperOptions";
                        mnuPleadingPaperOptions.Size = new System.Drawing.Size(211, 114);
                        //mnuPleadingPaperOptions.TimerInterval = 250;
                        //mnuPleadingPaperOptions.UseTimer = true;
                        mnuPleadingPaperOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuPleadingPaperOptions_ItemClicked);

                        //add sub-menu items
                        m_oMenuSegment_PleadingPaper_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_ChangeType.Name = "m_oMenuSegment_PleadingPaper_ChangeType";
                        m_oMenuSegment_PleadingPaper_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        m_oMenuSegment_PleadingPaper_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_Edit.Name = "m_oMenuSegment_PleadingPaper_Edit";
                        m_oMenuSegment_PleadingPaper_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");
                        m_oMenuSegment_PleadingPaper_Edit.Enabled = !oSegment.IsFinished;

                        mnuPleadingPaperOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_PleadingPaper_ChangeType,
                        m_oMenuSegment_PleadingPaper_Edit});

                        //GLOG : 6115 : CEH
                        //create a new Pleading Paper menu.
                        m_oMenuSegment_PleadingPaper_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_MenuItem.DropDown = mnuPleadingPaperOptions;
                        m_oMenuSegment_PleadingPaper_MenuItem.Name = "m_oMenuSegment_PleadingPaper_MenuItem";
                        m_oMenuSegment_PleadingPaper_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_PleadingPaper_MenuItem.Text = "Paper...";
                        m_oMenuSegment_PleadingPaper_MenuItem.Tag = 4;

                        AddTypeSpecificMenuItem(m_oMenuSegment_PleadingPaper_MenuItem);
                    }

                    //GLOG 3936 (dm) - add Insert Signature when there's no existing collection
                    if (!oSegment.Contains(mpObjectTypes.PleadingSignatures))
                    {
                        //add menu item for non-table pleading signatures only if there's already
                        //one in the pleading
                        if (oSegment.FindChildren(mpObjectTypes.PleadingSignatureNonTable).Length > 0)
                        {

                            //GLOG : 6160 : CEH
                            //create context menu
                            if (m_oMenuSegment_PleadingSignatures_MenuItem == null)
                            {
                                mnuPleadingSignaturesOptions = new ContextMenuStrip(this.components);
                                mnuPleadingSignaturesOptions.Name = "mnuPleadingSignaturesOptions";
                                mnuPleadingSignaturesOptions.Size = new System.Drawing.Size(211, 114);
                                //mnuPleadingSignaturesOptions.TimerInterval = 250;
                                //mnuPleadingSignaturesOptions.UseTimer = true;
                                mnuPleadingSignaturesOptions.ItemClicked += new ToolStripItemClickedEventHandler(this.mnuPleadingSignaturesOptions_ItemClicked);

                                //add sub-menu items
                                m_oMenuSegment_Pleading_AddNonTableSignature = new ToolStripMenuItem();
                                m_oMenuSegment_Pleading_AddNonTableSignature.Name = "m_oMenuSegment_Pleading_AddNonTableSignature";
                                m_oMenuSegment_Pleading_AddNonTableSignature.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Add");

                                m_oMenuSegment_PleadingSignature_ChangeType = new ToolStripMenuItem();
                                m_oMenuSegment_PleadingSignature_ChangeType.Name = "m_oMenuSegment_PleadingSignature_ChangeType";
                                m_oMenuSegment_PleadingSignature_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                                m_oMenuSegment_Pleading_EditNonTableSignature = new ToolStripMenuItem();
                                m_oMenuSegment_Pleading_EditNonTableSignature.Name = "m_oMenuSegment_Pleading_EditNonTableSignature";
                                m_oMenuSegment_Pleading_EditNonTableSignature.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");

                                mnuPleadingSignaturesOptions.Items.AddRange(new ToolStripItem[] {
                                    m_oMenuSegment_Pleading_AddNonTableSignature,
                                    m_oMenuSegment_PleadingSignature_ChangeType,
                                    m_oMenuSegment_Pleading_EditNonTableSignature});

                                m_oMenuSegment_PleadingSignatures_MenuItem = new ToolStripMenuItem();
                                m_oMenuSegment_PleadingSignatures_MenuItem = new ToolStripMenuItem();
                                m_oMenuSegment_PleadingSignatures_MenuItem.DropDown = mnuPleadingSignaturesOptions;
                                m_oMenuSegment_PleadingSignatures_MenuItem.Name = "m_oMenuSegment_PleadingSignatures_MenuItem";
                                m_oMenuSegment_PleadingSignatures_MenuItem.Size = new System.Drawing.Size(209, 22);
                                m_oMenuSegment_PleadingSignatures_MenuItem.Text = "Signatures...";
                                m_oMenuSegment_PleadingSignatures_MenuItem.Tag = 4;

                                ////assign a position index - the optimal position for this item in the menu
                                //m_oMenuSegment_PleadingSignatures_MenuItem.Tag = 3;
                            }

                            m_oMenuSegment_Pleading_EditNonTableSignature.Enabled = !oSegment.IsFinished;
                            AddTypeSpecificMenuItem(m_oMenuSegment_PleadingSignatures_MenuItem);
                        }
                        else
                        {
                            //GLOG 4446: Check for both Table and Non-Table assignments
                            Assignments oAssignmentsTable = new Assignments(mpObjectTypes.Pleading,
                                oSegment.ID1, mpObjectTypes.PleadingSignature);
                            Assignments oAssignmentsNonTable = new Assignments(mpObjectTypes.Pleading,
                                oSegment.ID1, mpObjectTypes.PleadingSignatureNonTable);
                            if (oAssignmentsTable.Count > 0)
                            {
                                if (m_oMenuSegment_Pleading_InsertSignature == null)
                                {
                                    m_oMenuSegment_Pleading_InsertSignature = new ToolStripMenuItem();
                                    m_oMenuSegment_Pleading_InsertSignature.Name =
                                        "m_oMenuSegment_Pleading_InsertSignature";
                                    m_oMenuSegment_Pleading_InsertSignature.Tag = 3;
                                }
                                //If both type of Assignments exist, show separate menu item for each
                                if (oAssignmentsNonTable.Count == 0)
                                    m_oMenuSegment_Pleading_InsertSignature.Text =
                                        LMP.Resources.GetLangString("Menu_DocumentEditor_InsertSignature");
                                else
                                    m_oMenuSegment_Pleading_InsertSignature.Text =
                                        LMP.Resources.GetLangString("Menu_DocumentEditor_InsertTableSignature");
                                AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_InsertSignature);
                            }
                            if (oAssignmentsNonTable.Count > 0)
                            {
                                if (m_oMenuSegment_Pleading_InsertNonTableSignature == null)
                                {
                                    m_oMenuSegment_Pleading_InsertNonTableSignature = new ToolStripMenuItem();
                                    m_oMenuSegment_Pleading_InsertNonTableSignature.Name =
                                        "m_oMenuSegment_Pleading_InsertSignatureNonTable";
                                    m_oMenuSegment_Pleading_InsertNonTableSignature.Tag = 4;
                                }
                                //If both type of Assignments exist, show separate menu item for each
                                if (oAssignmentsTable.Count == 0)
                                    m_oMenuSegment_Pleading_InsertNonTableSignature.Text =
                                        LMP.Resources.GetLangString("Menu_DocumentEditor_InsertSignature");
                                else
                                    m_oMenuSegment_Pleading_InsertNonTableSignature.Text =
                                        LMP.Resources.GetLangString("Menu_DocumentEditor_InsertNonTableSignature");
                                AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_InsertNonTableSignature);
                            }
                        }
                    }

                    // GLOG : 2340 : JAB
                    // Add quote style menu item.
                    if (m_oMenuSegment_Pleading_QuoteStyle == null)
                    {
                        m_oMenuSegment_Pleading_QuoteStyle = new ToolStripMenuItem();
                        m_oMenuSegment_Pleading_QuoteStyle.Name =
                            "m_oMenuSegment_Pleading_QuoteStyle";
                        m_oMenuSegment_Pleading_QuoteStyle.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_Pleading_QuoteStyle");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_Pleading_QuoteStyle.Tag = 11;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_QuoteStyle);
                    break;
                //GLOG : 7154 : ceh
                case mpObjectTypes.LitigationBack:
                    break;
                //GLOG : 6115 : CEH
                case mpObjectTypes.PleadingCounsels:
                    if (m_oMenuSegment_PleadingCounsels_MenuItem == null)
                    {
                        //create context menu
                        mnuPleadingCounselsOptions = new TimedContextMenuStrip(this.components);
                        mnuPleadingCounselsOptions.Name = "mnuPleadingCounselsOptions";
                        mnuPleadingCounselsOptions.Size = new System.Drawing.Size(211, 114);
                        mnuPleadingCounselsOptions.TimerInterval = 250;
                        mnuPleadingCounselsOptions.UseTimer = true;
                        mnuPleadingCounselsOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuPleadingCounselsOptions_ItemClicked);

                        //add sub-menu items
                        m_oMenuSegment_PleadingCounsel_Add = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingCounsel_Add.Name = "m_oMenuSegment_PleadingCounsel_Add";
                        m_oMenuSegment_PleadingCounsel_Add.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Add");

                        //GLOG : 6115 : CEH
                        //m_oMenuSegment_PleadingCounsel_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        //m_oMenuSegment_PleadingCounsel_ChangeType.Name = "m_oMenuSegment_PleadingCounsel_ChangeType";
                        //m_oMenuSegment_PleadingCounsel_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        m_oMenuSegment_PleadingCounsel_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingCounsel_Edit.Name = "m_oMenuSegment_PleadingCounsel_Edit";
                        m_oMenuSegment_PleadingCounsel_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");

                        m_oMenuSegment_PleadingCounsel_Layout = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingCounsel_Layout.Name = "m_oMenuSegment_PleadingCounsel_Layout";
                        m_oMenuSegment_PleadingCounsel_Layout.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Layout");

                        //GLOG : 6115 : CEH
                        mnuPleadingCounselsOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_PleadingCounsel_Add,
                        m_oMenuSegment_PleadingCounsel_Edit,
                        m_oMenuSegment_PleadingCounsel_Layout});

                        //create a new Counsel menu.
                        m_oMenuSegment_PleadingCounsels_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_PleadingCounsels_MenuItem.DropDown = mnuPleadingCounselsOptions;
                        m_oMenuSegment_PleadingCounsels_MenuItem.Name = "m_oMenuSegment_PleadingCounsels_MenuItem";
                        m_oMenuSegment_PleadingCounsels_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_PleadingCounsels_MenuItem.Text = "Counsel/Co-Counsel...";
                        m_oMenuSegment_PleadingCounsels_MenuItem.Tag = 1;
                    }

                    m_oMenuSegment_PleadingCounsel_Edit.Enabled = !oSegment.IsFinished;
                    AddTypeSpecificMenuItem(m_oMenuSegment_PleadingCounsels_MenuItem);

                    break;
                case mpObjectTypes.PleadingCaptions:
                    //create context menu
                    mnuPleadingCaptionsOptions = new TimedContextMenuStrip(this.components);
                    mnuPleadingCaptionsOptions.Name = "mnuPleadingCaptionsOptions";
                    mnuPleadingCaptionsOptions.Size = new System.Drawing.Size(211, 114);
                    //mnuPleadingCaptionsOptions.TimerInterval = 250;
                    //mnuPleadingCaptionsOptions.UseTimer = true;
                    mnuPleadingCaptionsOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(
                        this.mnuPleadingCaptionsOptions_ItemClicked);

                    //add sub-menu items
                    m_oMenuSegment_PleadingCaption_Add = new System.Windows.Forms.ToolStripMenuItem();
                    m_oMenuSegment_PleadingCaption_Add.Name = "m_oMenuSegment_PleadingCaption_Add";
                    m_oMenuSegment_PleadingCaption_Add.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Add");

                    m_oMenuSegment_PleadingCaption_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                    m_oMenuSegment_PleadingCaption_ChangeType.Name = "m_oMenuSegment_PleadingCaption_ChangeType";
                    m_oMenuSegment_PleadingCaption_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                    m_oMenuSegment_PleadingCaption_Edit = new System.Windows.Forms.ToolStripMenuItem();
                    m_oMenuSegment_PleadingCaption_Edit.Name = "m_oMenuSegment_PleadingCaption_Edit";
                    m_oMenuSegment_PleadingCaption_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");
                    m_oMenuSegment_PleadingCaption_Edit.Enabled = !oSegment.IsFinished;

                    m_oMenuSegment_PleadingCaption_Layout = new System.Windows.Forms.ToolStripMenuItem();
                    m_oMenuSegment_PleadingCaption_Layout.Name = "m_oMenuSegment_PleadingCaption_Layout";
                    m_oMenuSegment_PleadingCaption_Layout.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Layout");

                    mnuPleadingCaptionsOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                    m_oMenuSegment_PleadingCaption_Add,
                    m_oMenuSegment_PleadingCaption_ChangeType,
                    m_oMenuSegment_PleadingCaption_Edit,
                    m_oMenuSegment_PleadingCaption_Layout});

                    //create a new Caption menu.
                    m_oMenuSegment_PleadingCaptions_MenuItem = new ToolStripMenuItem();
                    m_oMenuSegment_PleadingCaptions_MenuItem.DropDown = mnuPleadingCaptionsOptions;
                    m_oMenuSegment_PleadingCaptions_MenuItem.Name = "m_oMenuSegment_PleadingCaptions_MenuItem";
                    m_oMenuSegment_PleadingCaptions_MenuItem.Size = new System.Drawing.Size(209, 22);
                    m_oMenuSegment_PleadingCaptions_MenuItem.Text = "Caption...";
                    m_oMenuSegment_PleadingCaptions_MenuItem.Tag = 2;

                    AddTypeSpecificMenuItem(m_oMenuSegment_PleadingCaptions_MenuItem);

                    break;
                case mpObjectTypes.PleadingSignatures:
                    //create context menu
                    mnuPleadingSignaturesOptions = new TimedContextMenuStrip(this.components);
                    mnuPleadingSignaturesOptions.Name = "mnuPleadingSignaturesOptions";
                    mnuPleadingSignaturesOptions.Size = new System.Drawing.Size(211, 114);
                    //mnuPleadingSignaturesOptions.TimerInterval = 250;
                    //mnuPleadingSignaturesOptions.UseTimer = true;
                    mnuPleadingSignaturesOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuPleadingSignaturesOptions_ItemClicked);

                    //add sub-menu items
                    m_oMenuSegment_PleadingSignature_Add = new System.Windows.Forms.ToolStripMenuItem();
                    m_oMenuSegment_PleadingSignature_Add.Name = "m_oMenuSegment_PleadingSignature_Add";
                    m_oMenuSegment_PleadingSignature_Add.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Add");

                    m_oMenuSegment_PleadingSignature_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                    m_oMenuSegment_PleadingSignature_ChangeType.Name = "m_oMenuSegment_PleadingSignature_ChangeType";
                    m_oMenuSegment_PleadingSignature_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                    m_oMenuSegment_PleadingSignature_Edit = new System.Windows.Forms.ToolStripMenuItem();
                    m_oMenuSegment_PleadingSignature_Edit.Name = "m_oMenuSegment_PleadingSignature_Edit";
                    m_oMenuSegment_PleadingSignature_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");
                    m_oMenuSegment_PleadingSignature_Edit.Enabled = !oSegment.IsFinished;

                    m_oMenuSegment_PleadingSignature_Layout = new System.Windows.Forms.ToolStripMenuItem();
                    m_oMenuSegment_PleadingSignature_Layout.Name = "m_oMenuSegment_PleadingSignature_Layout";
                    m_oMenuSegment_PleadingSignature_Layout.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Layout");

                    mnuPleadingSignaturesOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                    m_oMenuSegment_PleadingSignature_Add,
                    m_oMenuSegment_PleadingSignature_ChangeType,
                    m_oMenuSegment_PleadingSignature_Edit,
                    m_oMenuSegment_PleadingSignature_Layout});

                    //create a new Signature menu.
                    m_oMenuSegment_PleadingSignatures_MenuItem = new ToolStripMenuItem();
                    m_oMenuSegment_PleadingSignatures_MenuItem.DropDown = mnuPleadingSignaturesOptions;
                    m_oMenuSegment_PleadingSignatures_MenuItem.Name = "m_oMenuSegment_PleadingSignatures_MenuItem";
                    m_oMenuSegment_PleadingSignatures_MenuItem.Size = new System.Drawing.Size(209, 22);
                    m_oMenuSegment_PleadingSignatures_MenuItem.Text = "Signature...";
                    m_oMenuSegment_PleadingSignatures_MenuItem.Tag = 3;

                    AddTypeSpecificMenuItem(m_oMenuSegment_PleadingSignatures_MenuItem);

                    break;
                case mpObjectTypes.Fax:
                    //GLOG : 6167 : CEH
                    m_oMenuSegment.Items.Add(new ToolStripSeparator());
                    if (m_oMenuSegment_CreateEnvelopes == null)
                    {
                        m_oMenuSegment_CreateEnvelopes = new ToolStripMenuItem();
                        m_oMenuSegment_CreateEnvelopes.Name =
                            "m_oMenuSegment_CreateEnvelopes";
                        m_oMenuSegment_CreateEnvelopes.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateEnvelopes");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateEnvelopes.Tag = 5;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateEnvelopes);

                    if (m_oMenuSegment_CreateLabels == null)
                    {
                        m_oMenuSegment_CreateLabels = new ToolStripMenuItem();
                        m_oMenuSegment_CreateLabels.Name =
                            "m_oMenuSegment_CreateLabels";
                        m_oMenuSegment_CreateLabels.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateLabels");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateLabels.Tag = 6;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateLabels);

                    if (m_oMenuSegment_Letterhead_MenuItem == null)
                    {
                        //create context menu
                        mnuLetterheadOptions = new TimedContextMenuStrip(this.components);
                        mnuLetterheadOptions.Name = "mnuLetterheadOptions";
                        mnuLetterheadOptions.Size = new System.Drawing.Size(211, 114);
                        //mnuLetterheadOptions.TimerInterval = 250;
                        //mnuLetterheadOptions.UseTimer = true;
                        mnuLetterheadOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuLetterheadOptions_ItemClicked);

                        //add Change Type menu item
                        m_oMenuSegment_Letterhead_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_ChangeType.Name = "m_oMenuSegment_Letterhead_ChangeType";
                        m_oMenuSegment_Letterhead_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        //add Edit menu item
                        m_oMenuSegment_Letterhead_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_Edit.Name = "m_oMenuSegment_Letterhead_Edit";
                        m_oMenuSegment_Letterhead_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");

                        mnuLetterheadOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_Letterhead_ChangeType,
                        m_oMenuSegment_Letterhead_Edit});

                        //create a new Counsel menu.
                        m_oMenuSegment_Letterhead_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_MenuItem.DropDown = mnuLetterheadOptions;
                        m_oMenuSegment_Letterhead_MenuItem.Name = "m_oMenuSegment_Letterhead_MenuItem";
                        m_oMenuSegment_Letterhead_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_Letterhead_MenuItem.Text = "Letterhead...";
                        m_oMenuSegment_Letterhead_MenuItem.Tag = 7;
                    }

                    m_oMenuSegment_Letterhead_Edit.Enabled = !oSegment.IsFinished;
                    AddTypeSpecificMenuItem(m_oMenuSegment_Letterhead_MenuItem);

                    break;
                case mpObjectTypes.Letter:
                    //GLOG : 6167 : CEH
                    m_oMenuSegment.Items.Add(new ToolStripSeparator());

                    if (m_oMenuSegment_CreateEnvelopes == null)
                    {
                        m_oMenuSegment_CreateEnvelopes = new ToolStripMenuItem();
                        m_oMenuSegment_CreateEnvelopes.Name =
                            "m_oMenuSegment_CreateEnvelopes";
                        m_oMenuSegment_CreateEnvelopes.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateEnvelopes");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateEnvelopes.Tag = 4;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateEnvelopes);

                    if (m_oMenuSegment_CreateLabels == null)
                    {
                        m_oMenuSegment_CreateLabels = new ToolStripMenuItem();
                        m_oMenuSegment_CreateLabels.Name =
                            "m_oMenuSegment_CreateLabels";
                        m_oMenuSegment_CreateLabels.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateLabels");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateLabels.Tag = 5;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateLabels);

                    //GLOG 3936 (dm) - add Insert Signature when there's no existing collection
                    if (!oSegment.Contains(mpObjectTypes.LetterSignatures))
                    {
                        if (m_oMenuSegment_Letter_InsertSignature == null)
                        {
                            m_oMenuSegment_Letter_InsertSignature = new ToolStripMenuItem();
                            m_oMenuSegment_Letter_InsertSignature.Name =
                                "m_oMenuSegment_Letter_InsertSignature";
                            m_oMenuSegment_Letter_InsertSignature.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_InsertSignature");
                            m_oMenuSegment_Letter_InsertSignature.Tag = 6;
                        }
                        AddTypeSpecificMenuItem(m_oMenuSegment_Letter_InsertSignature);
                    }

                    //GLOG : 6167 : CEH
                    if (m_oMenuSegment_Letterhead_MenuItem == null)
                    {
                        //create context menu
                        mnuLetterheadOptions = new TimedContextMenuStrip(this.components);
                        mnuLetterheadOptions.Name = "mnuLetterheadOptions";
                        mnuLetterheadOptions.Size = new System.Drawing.Size(211, 114);
                        //mnuLetterheadOptions.TimerInterval = 250;
                        //mnuLetterheadOptions.UseTimer = true;
                        mnuLetterheadOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuLetterheadOptions_ItemClicked);

                        //add Change Type menu item
                        m_oMenuSegment_Letterhead_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_ChangeType.Name = "m_oMenuSegment_Letterhead_ChangeType";
                        m_oMenuSegment_Letterhead_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        //add Edit menu item
                        m_oMenuSegment_Letterhead_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_Edit.Name = "m_oMenuSegment_Letterhead_Edit";
                        m_oMenuSegment_Letterhead_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");

                        mnuLetterheadOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_Letterhead_ChangeType,
                        m_oMenuSegment_Letterhead_Edit});

                        //create a new Counsel menu.
                        m_oMenuSegment_Letterhead_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_MenuItem.DropDown = mnuLetterheadOptions;
                        m_oMenuSegment_Letterhead_MenuItem.Name = "m_oMenuSegment_Letterhead_MenuItem";
                        m_oMenuSegment_Letterhead_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_Letterhead_MenuItem.Text = "Letterhead...";
                        m_oMenuSegment_Letterhead_MenuItem.Tag = 8;
                    }

                    m_oMenuSegment_Letterhead_Edit.Enabled = !oSegment.IsFinished;
                    AddTypeSpecificMenuItem(m_oMenuSegment_Letterhead_MenuItem);
                    break;
                //GLOG : 6135 : CEH
                case mpObjectTypes.Memo:
                    //GLOG : 6167 : CEH
                    m_oMenuSegment.Items.Add(new ToolStripSeparator());

                    if (m_oMenuSegment_CreateEnvelopes == null)
                    {
                        m_oMenuSegment_CreateEnvelopes = new ToolStripMenuItem();
                        m_oMenuSegment_CreateEnvelopes.Name =
                            "m_oMenuSegment_CreateEnvelopes";
                        m_oMenuSegment_CreateEnvelopes.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateEnvelopes");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateEnvelopes.Tag = 4;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateEnvelopes);

                    if (m_oMenuSegment_CreateLabels == null)
                    {
                        m_oMenuSegment_CreateLabels = new ToolStripMenuItem();
                        m_oMenuSegment_CreateLabels.Name =
                            "m_oMenuSegment_CreateLabels";
                        m_oMenuSegment_CreateLabels.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateLabels");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateLabels.Tag = 5;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateLabels);

                    //GLOG : 6167 : CEH
                    if (m_oMenuSegment_Letterhead_MenuItem == null)
                    {
                        //create context menu
                        mnuLetterheadOptions = new TimedContextMenuStrip(this.components);
                        mnuLetterheadOptions.Name = "mnuLetterheadOptions";
                        mnuLetterheadOptions.Size = new System.Drawing.Size(211, 114);
                        //mnuLetterheadOptions.TimerInterval = 250;
                        //mnuLetterheadOptions.UseTimer = true;
                        mnuLetterheadOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuLetterheadOptions_ItemClicked);

                        //add Change Type menu item
                        m_oMenuSegment_Letterhead_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_ChangeType.Name = "m_oMenuSegment_Letterhead_ChangeType";
                        m_oMenuSegment_Letterhead_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        //add Edit menu item
                        m_oMenuSegment_Letterhead_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_Edit.Name = "m_oMenuSegment_Letterhead_Edit";
                        m_oMenuSegment_Letterhead_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");

                        mnuLetterheadOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_Letterhead_ChangeType,
                        m_oMenuSegment_Letterhead_Edit});

                        //create a new Counsel menu.
                        m_oMenuSegment_Letterhead_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_MenuItem.DropDown = mnuLetterheadOptions;
                        m_oMenuSegment_Letterhead_MenuItem.Name = "m_oMenuSegment_Letterhead_MenuItem";
                        m_oMenuSegment_Letterhead_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_Letterhead_MenuItem.Text = "Letterhead...";
                        m_oMenuSegment_Letterhead_MenuItem.Tag = 8;
                    }

                    m_oMenuSegment_Letterhead_Edit.Enabled = !oSegment.IsFinished;
                    AddTypeSpecificMenuItem(m_oMenuSegment_Letterhead_MenuItem);

                    break;
                case mpObjectTypes.ServiceList:
                case mpObjectTypes.ServiceListSeparatePage: //GLOG 6094
                    m_oMenuSegment.Items.Add(new ToolStripSeparator());

                    //GLOG 3936 (dm) - add Insert Signature when there's no existing collection
                    if (!oSegment.Contains(mpObjectTypes.LetterSignatures))
                    {
                        if (m_oMenuSegment_Letter_InsertSignature == null)
                        {
                            m_oMenuSegment_Letter_InsertSignature = new ToolStripMenuItem();
                            m_oMenuSegment_Letter_InsertSignature.Name =
                                "m_oMenuSegment_Letter_InsertSignature";
                            m_oMenuSegment_Letter_InsertSignature.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_InsertSignature");
                            m_oMenuSegment_Letter_InsertSignature.Tag = 4;
                        }
                        AddTypeSpecificMenuItem(m_oMenuSegment_Letter_InsertSignature);
                    }

                    if (m_oMenuSegment_CreateEnvelopes == null)
                    {
                        m_oMenuSegment_CreateEnvelopes = new ToolStripMenuItem();
                        m_oMenuSegment_CreateEnvelopes.Name =
                            "m_oMenuSegment_CreateEnvelopes";
                        m_oMenuSegment_CreateEnvelopes.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateEnvelopes");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateEnvelopes.Tag = 5;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateEnvelopes);

                    if (m_oMenuSegment_CreateLabels == null)
                    {
                        m_oMenuSegment_CreateLabels = new ToolStripMenuItem();
                        m_oMenuSegment_CreateLabels.Name =
                            "m_oMenuSegment_CreateLabels";
                        m_oMenuSegment_CreateLabels.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateLabels");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateLabels.Tag = 6;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateLabels);

                    break;
                case mpObjectTypes.Agreement:
                    m_oMenuSegment.Items.Add(new ToolStripSeparator());
                    
                    //GLOG 3936 (dm) - add Insert Signature when there's no existing collection
                    if (!oSegment.Contains(mpObjectTypes.AgreementSignatures))
                    {
                        if (m_oMenuSegment_Agreement_InsertSignature == null)
                        {
                            m_oMenuSegment_Agreement_InsertSignature = new ToolStripMenuItem();
                            m_oMenuSegment_Agreement_InsertSignature.Name =
                                "m_oMenuSegment_Agreement_InsertSignature";
                            m_oMenuSegment_Agreement_InsertSignature.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_InsertSignature");
                            m_oMenuSegment_Agreement_InsertSignature.Tag = 1;
                        }
                        AddTypeSpecificMenuItem(m_oMenuSegment_Agreement_InsertSignature);
                    }
                    if (m_oMenuSegment_Agreement_InsertTitlePage == null)
                    {
                        m_oMenuSegment_Agreement_InsertTitlePage = new ToolStripMenuItem();
                        m_oMenuSegment_Agreement_InsertTitlePage.Name =
                            "m_oMenuSegment_Agreement_InsertTitlePage";
                        m_oMenuSegment_Agreement_InsertTitlePage.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_InsertTitlePage");
                        m_oMenuSegment_Agreement_InsertTitlePage.Tag = 3;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_Agreement_InsertTitlePage);
                    if (m_oMenuSegment_Agreement_InsertExhibits == null)
                    {
                        m_oMenuSegment_Agreement_InsertExhibits = new ToolStripMenuItem();
                        m_oMenuSegment_Agreement_InsertExhibits.Name =
                            "m_oMenuSegment_Agreement_InsertExhibits";
                        m_oMenuSegment_Agreement_InsertExhibits.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_InsertExhibits");
                        m_oMenuSegment_Agreement_InsertExhibits.Tag = 4;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_Agreement_InsertExhibits);
                    break;
                //GLOG : 7206 : CEH
                case mpObjectTypes.Labels:
                    if (!bRibbonOnlyImplementation)
                    {
                        m_oMenuSegment.Items.Add(new ToolStripSeparator());
                        if (m_oMenuSegment_CreateEnvelopes == null)
                        {
                            m_oMenuSegment_CreateEnvelopes = new ToolStripMenuItem();
                            m_oMenuSegment_CreateEnvelopes.Name =
                                "m_oMenuSegment_CreateEnvelopes";
                            m_oMenuSegment_CreateEnvelopes.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_CreateEnvelopes");

                            //assign a position index - the optimal position for this item in the menu
                            m_oMenuSegment_CreateEnvelopes.Tag = 5;
                        }
                        AddTypeSpecificMenuItem(m_oMenuSegment_CreateEnvelopes);
                    }
                    break;
                //GLOG : 7206 : CEH
                case mpObjectTypes.Envelopes:
                    if (!bRibbonOnlyImplementation)
                    {
                        m_oMenuSegment.Items.Add(new ToolStripSeparator());
                        if (m_oMenuSegment_CreateLabels == null)
                        {
                            m_oMenuSegment_CreateLabels = new ToolStripMenuItem();
                            m_oMenuSegment_CreateLabels.Name =
                                "m_oMenuSegment_CreateLabels";
                            m_oMenuSegment_CreateLabels.Text =
                                LMP.Resources.GetLangString("Menu_DocumentEditor_CreateLabels");

                            //assign a position index - the optimal position for this item in the menu
                            m_oMenuSegment_CreateLabels.Tag = 5;
                        }
                        AddTypeSpecificMenuItem(m_oMenuSegment_CreateLabels);
                    }
                    break;
                case mpObjectTypes.LetterSignatures:
                    //GLOG : 6167 : CEH
                    if (m_oMenuSegment_LetterSignatures_MenuItem == null)
                    {
                        //create context menu
                        mnuLetterSignaturesOptions = new TimedContextMenuStrip(this.components);
                        mnuLetterSignaturesOptions.Name = "mnuLetterSignaturesOptions";
                        mnuLetterSignaturesOptions.Size = new System.Drawing.Size(211, 114);
                        //mnuLetterSignaturesOptions.TimerInterval = 250;
                        //mnuLetterSignaturesOptions.UseTimer = true;
                        mnuLetterSignaturesOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuLetterSignaturesOptions_ItemClicked);

                        //add sub-menu items
                        m_oMenuSegment_LetterSignature_Add = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_LetterSignature_Add.Name = "m_oMenuSegment_LetterSignature_Add";
                        m_oMenuSegment_LetterSignature_Add.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Add");

                        m_oMenuSegment_LetterSignature_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_LetterSignature_ChangeType.Name = "m_oMenuSegment_LetterSignature_ChangeType";
                        m_oMenuSegment_LetterSignature_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        m_oMenuSegment_LetterSignature_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_LetterSignature_Edit.Name = "m_oMenuSegment_LetterSignature_Edit";
                        m_oMenuSegment_LetterSignature_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");

                        m_oMenuSegment_LetterSignature_Layout = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_LetterSignature_Layout.Name = "m_oMenuSegment_LetterSignature_Layout";
                        m_oMenuSegment_LetterSignature_Layout.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Layout");

                        mnuLetterSignaturesOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_LetterSignature_Add,
                        m_oMenuSegment_LetterSignature_ChangeType,
                        m_oMenuSegment_LetterSignature_Edit,
                        m_oMenuSegment_LetterSignature_Layout});

                        //create a new Counsel menu.
                        m_oMenuSegment_LetterSignatures_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_LetterSignatures_MenuItem.DropDown = mnuLetterSignaturesOptions;
                        m_oMenuSegment_LetterSignatures_MenuItem.Name = "m_oMenuSegment_LetterSignatures_MenuItem";
                        m_oMenuSegment_LetterSignatures_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_LetterSignatures_MenuItem.Text = "Signature...";
                        m_oMenuSegment_LetterSignatures_MenuItem.Tag = 7;
                    }

                    m_oMenuSegment_LetterSignature_Edit.Enabled = !oSegment.IsFinished;
                    AddTypeSpecificMenuItem(m_oMenuSegment_LetterSignatures_MenuItem);
                    break;
                case mpObjectTypes.AgreementSignatures:
                    //GLOG : 6170 : CEH
                    if (m_oMenuSegment_AgreementSignatures_MenuItem == null)
                    {
                        //create context menu
                        mnuAgreementSignaturesOptions = new TimedContextMenuStrip(this.components);
                        mnuAgreementSignaturesOptions.Name = "mnuAgreementSignaturesOptions";
                        mnuAgreementSignaturesOptions.Size = new System.Drawing.Size(211, 114);
                        //mnuAgreementSignaturesOptions.TimerInterval = 250;
                        //mnuAgreementSignaturesOptions.UseTimer = true;
                        mnuAgreementSignaturesOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuAgreementSignaturesOptions_ItemClicked);

                        //add sub-menu items
                        m_oMenuSegment_AgreementSignature_Add = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_AgreementSignature_Add.Name = "m_oMenuSegment_AgreementSignature_Add";
                        m_oMenuSegment_AgreementSignature_Add.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Add");

                        m_oMenuSegment_AgreementSignature_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_AgreementSignature_ChangeType.Name = "m_oMenuSegment_AgreementSignature_ChangeType";
                        m_oMenuSegment_AgreementSignature_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        m_oMenuSegment_AgreementSignature_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_AgreementSignature_Edit.Name = "m_oMenuSegment_AgreementSignature_Edit";
                        m_oMenuSegment_AgreementSignature_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");

                        m_oMenuSegment_AgreementSignature_Layout = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_AgreementSignature_Layout.Name = "m_oMenuSegment_AgreementSignature_Layout";
                        m_oMenuSegment_AgreementSignature_Layout.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Layout");

                        mnuAgreementSignaturesOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_AgreementSignature_Add,
                        m_oMenuSegment_AgreementSignature_ChangeType,
                        m_oMenuSegment_AgreementSignature_Edit,
                        m_oMenuSegment_AgreementSignature_Layout});

                        //create a new Counsel menu.
                        m_oMenuSegment_AgreementSignatures_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_AgreementSignatures_MenuItem.DropDown = mnuAgreementSignaturesOptions;
                        m_oMenuSegment_AgreementSignatures_MenuItem.Name = "m_oMenuSegment_AgreementSignatures_MenuItem";
                        m_oMenuSegment_AgreementSignatures_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_AgreementSignatures_MenuItem.Text = "Signature...";
                        m_oMenuSegment_AgreementSignatures_MenuItem.Tag = 2;
                    }

                    m_oMenuSegment_AgreementSignature_Edit.Enabled = !oSegment.IsFinished;
                    AddTypeSpecificMenuItem(m_oMenuSegment_AgreementSignatures_MenuItem);
                    break;
                case mpObjectTypes.CollectionTable:
                    if (m_oMenuSegment_CollectionTable_AddItem == null)
                    {
                        m_oMenuSegment_CollectionTable_AddItem = new ToolStripMenuItem();
                        m_oMenuSegment_CollectionTable_AddItem.Name =
                            "m_oMenuSegment_CollectionTable_AddItem";
                        m_oMenuSegment_CollectionTable_AddItem.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_AddCollectionTableItem");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CollectionTable_AddItem.Tag = 5;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CollectionTable_AddItem);
                    break;

                // GLOG : 3135 : JAB
                // Add a menu item for Letterhead refresh.
                case mpObjectTypes.Letterhead:
                    if (m_oMenuSegment_Letterhead_Refresh == null)
                    {
                        m_oMenuSegment_Letterhead_Refresh = new ToolStripMenuItem();
                        m_oMenuSegment_Letterhead_Refresh.Name =
                            "m_oMenuSegment_Letterhead_Refresh";
                        m_oMenuSegment_Letterhead_Refresh.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_Letterhead_Refresh");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_Letterhead_Refresh.Tag = 8;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_Letterhead_Refresh);
                    break;
                case mpObjectTypes.TOA:
                    if (m_oMenuSegment_TOA_Update == null)
                    {
                        m_oMenuSegment_TOA_Update = new ToolStripMenuItem();
                        m_oMenuSegment_TOA_Update.Name =
                            "m_oMenuSegment_TOA_Update";
                        m_oMenuSegment_TOA_Update.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_TOA_Update");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_TOA_Update.Tag = 5;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_TOA_Update);
                    break;
                case mpObjectTypes.Service:
                    //GLOG : 6171 : CEH
                    m_oMenuSegment.Items.Add(new ToolStripSeparator());

                    if (m_oMenuSegment_CreateEnvelopes == null)
                    {
                        m_oMenuSegment_CreateEnvelopes = new ToolStripMenuItem();
                        m_oMenuSegment_CreateEnvelopes.Name =
                            "m_oMenuSegment_CreateEnvelopes";
                        m_oMenuSegment_CreateEnvelopes.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateEnvelopes");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateEnvelopes.Tag = 1;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateEnvelopes);

                    if (m_oMenuSegment_CreateLabels == null)
                    {
                        m_oMenuSegment_CreateLabels = new ToolStripMenuItem();
                        m_oMenuSegment_CreateLabels.Name =
                            "m_oMenuSegment_CreateLabels";
                        m_oMenuSegment_CreateLabels.Text =
                            LMP.Resources.GetLangString("Menu_DocumentEditor_CreateLabels");

                        //assign a position index - the optimal position for this item in the menu
                        m_oMenuSegment_CreateLabels.Tag = 2;
                    }
                    AddTypeSpecificMenuItem(m_oMenuSegment_CreateLabels);

                    ////GLOG : 6135 : CEH
                    ////add Align Pleading Paper menu item
                    //if (m_oMenuSegment_Pleading_AlignPleadingPaper == null)
                    //{
                    //    m_oMenuSegment_Pleading_AlignPleadingPaper = new ToolStripMenuItem();
                    //    m_oMenuSegment_Pleading_AlignPleadingPaper.Name =
                    //        "m_oMenuSegment_Pleading_AlignPleadingPaper";
                    //    m_oMenuSegment_Pleading_AlignPleadingPaper.Text =
                    //        LMP.Resources.GetLangString("Menu_DocumentEditor_Pleading_AlignPleadingPaper");

                    //    //assign a position index - the optimal position for this item in the menu
                    //    m_oMenuSegment_Pleading_AlignPleadingPaper.Tag = 7;
                    //}

                    if (oSegment.Contains(LMP.Data.mpObjectTypes.PleadingPaper))
                    {
                        //AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_AlignPleadingPaper);

                        //create context menu
                        mnuPleadingPaperOptions = new TimedContextMenuStrip(this.components);
                        mnuPleadingPaperOptions.Name = "mnuPleadingPaperOptions";
                        mnuPleadingPaperOptions.Size = new System.Drawing.Size(211, 114);
                        //mnuPleadingPaperOptions.TimerInterval = 250;
                        //mnuPleadingPaperOptions.UseTimer = true;
                        mnuPleadingPaperOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuPleadingPaperOptions_ItemClicked);

                        //add sub-menu items
                        m_oMenuSegment_PleadingPaper_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_ChangeType.Name = "m_oMenuSegment_PleadingPaper_ChangeType";
                        m_oMenuSegment_PleadingPaper_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        m_oMenuSegment_PleadingPaper_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_Edit.Name = "m_oMenuSegment_PleadingPaper_Edit";
                        m_oMenuSegment_PleadingPaper_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");
                        m_oMenuSegment_PleadingPaper_Edit.Enabled = !oSegment.IsFinished;

                        mnuPleadingPaperOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_PleadingPaper_ChangeType,
                        m_oMenuSegment_PleadingPaper_Edit});

                        //GLOG : 6115 : CEH
                        //create a new Pleading Paper menu.
                        m_oMenuSegment_PleadingPaper_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_MenuItem.DropDown = mnuPleadingPaperOptions;
                        m_oMenuSegment_PleadingPaper_MenuItem.Name = "m_oMenuSegment_PleadingPaper_MenuItem";
                        m_oMenuSegment_PleadingPaper_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_PleadingPaper_MenuItem.Text = "Paper...";
                        m_oMenuSegment_PleadingPaper_MenuItem.Tag = 6;

                        AddTypeSpecificMenuItem(m_oMenuSegment_PleadingPaper_MenuItem);
                    }

                    break;
                case mpObjectTypes.Verification:
                    m_oMenuSegment.Items.Add(new ToolStripSeparator());

                    ////GLOG : 6135 : CEH
                    ////add Align Pleading Paper menu item
                    //if (m_oMenuSegment_Pleading_AlignPleadingPaper == null)
                    //{
                    //    m_oMenuSegment_Pleading_AlignPleadingPaper = new ToolStripMenuItem();
                    //    m_oMenuSegment_Pleading_AlignPleadingPaper.Name =
                    //        "m_oMenuSegment_Pleading_AlignPleadingPaper";
                    //    m_oMenuSegment_Pleading_AlignPleadingPaper.Text =
                    //        LMP.Resources.GetLangString("Menu_DocumentEditor_Pleading_AlignPleadingPaper");

                    //    //assign a position index - the optimal position for this item in the menu
                    //    m_oMenuSegment_Pleading_AlignPleadingPaper.Tag = 7;
                    //}

                    if (oSegment.Contains(LMP.Data.mpObjectTypes.PleadingPaper))
                    {
                        //AddTypeSpecificMenuItem(m_oMenuSegment_Pleading_AlignPleadingPaper);

                        //create context menu
                        mnuPleadingPaperOptions = new TimedContextMenuStrip(this.components);
                        mnuPleadingPaperOptions.Name = "mnuPleadingPaperOptions";
                        mnuPleadingPaperOptions.Size = new System.Drawing.Size(211, 114);
                        //mnuPleadingPaperOptions.TimerInterval = 250;
                        //mnuPleadingPaperOptions.UseTimer = true;
                        mnuPleadingPaperOptions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnuPleadingPaperOptions_ItemClicked);

                        //add sub-menu items
                        m_oMenuSegment_PleadingPaper_ChangeType = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_ChangeType.Name = "m_oMenuSegment_PleadingPaper_ChangeType";
                        m_oMenuSegment_PleadingPaper_ChangeType.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_ChangeType");

                        m_oMenuSegment_PleadingPaper_Edit = new System.Windows.Forms.ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_Edit.Name = "m_oMenuSegment_PleadingPaper_Edit";
                        m_oMenuSegment_PleadingPaper_Edit.Text = LMP.Resources.GetLangString("Menu_DocumentEditor_Edit");
                        m_oMenuSegment_PleadingPaper_Edit.Enabled = !oSegment.IsFinished;

                        mnuPleadingPaperOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        m_oMenuSegment_PleadingPaper_ChangeType,
                        m_oMenuSegment_PleadingPaper_Edit});

                        //GLOG : 6115 : CEH
                        //create a new Pleading Paper menu.
                        m_oMenuSegment_PleadingPaper_MenuItem = new ToolStripMenuItem();
                        m_oMenuSegment_PleadingPaper_MenuItem.DropDown = mnuPleadingPaperOptions;
                        m_oMenuSegment_PleadingPaper_MenuItem.Name = "m_oMenuSegment_PleadingPaper_MenuItem";
                        m_oMenuSegment_PleadingPaper_MenuItem.Size = new System.Drawing.Size(209, 22);
                        m_oMenuSegment_PleadingPaper_MenuItem.Text = "Paper...";
                        m_oMenuSegment_PleadingPaper_MenuItem.Tag = 6;

                        AddTypeSpecificMenuItem(m_oMenuSegment_PleadingPaper_MenuItem);
                    }

                    break;
                default:
                    break;
            }
        }

        //GLOG 2343:
        /// <summary>
        /// Adds the specified segment-specific menu item
        /// to the segment menu - each segment specific
        /// menu item has it's relative index position specified
        /// in the .Tag property - this is assigned in
        /// AddTypeSpecificMenuItems
        /// </summary>
        /// <param name="oMenuItem"></param>
        private void AddTypeSpecificMenuItem(ToolStripMenuItem oMenuItem)
        {
            int iRelativeIndex = (int)oMenuItem.Tag;
            bool bReachedSegmentSpecificSection = false;
            int iIndex = 0;
            //cycle through all menu items on the segment menu -
            //when we get to the segment-specific section, 
            //we compare relative indexes, and insert the 
            //menu item above the first item having a greater index.
            foreach (ToolStripItem oItem in m_oMenuSegment.Items)
            {
                if (oItem is ToolStripSeparator)
                {
                    //skip menu separators
                    iIndex++;
                    continue;
                }
                if (oItem == m_oMenuSegment_RemoveVarAndBlockTags)
                {
                    //we're at the end of the non-specific section-
                    iIndex++;
                    bReachedSegmentSpecificSection = true;
                    continue;
                }
                else if (bReachedSegmentSpecificSection)
                {
                    if (oMenuItem.Tag != null)
                    {
                        if ((int)oMenuItem.Tag > (int)oItem.Tag)
                        {
                            //item index is still greater than
                            //the relative index of the current
                            //menu item
                            iIndex++;
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    iIndex++;
                    continue;
                }
            }

            //add item to menu
            m_oMenuSegment.Items.Insert(iIndex, oMenuItem);
        }

        /// <summary>
        /// recreates the specified segment -
        /// prompts the user for location options
        /// and body text options
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        public void RecreateSegment(Segment oSegment)
        {
            try
            {
                //GLOG item #6703 - dcf - 4/17/13
                if (!AdminSegmentDefs.Exists(oSegment.Name) && !UserSegmentDefs.Exists(LMP.Data.Application.User.ID, oSegment.ID))
                {
                    //original segment no longer exists - alert
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Error_InvalidSegmentIDCantExecuteFeature"), oSegment.ID),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return;
                }

                //GLOG : 6771 : CEH
                //if there is a selection, save it
                Word.Range oRange = null;
                if (Session.CurrentWordApp.Selection.Start !=
                    Session.CurrentWordApp.Selection.End)
                    oRange = Session.CurrentWordApp.Selection.Range;

                if (!this.TaskPane.FinishChildrenIfNecessary(oSegment))
                    return;

                if (oRange != null)
                    oRange.Select();

                //GLOG 5822 (dm) - update oSegment to reflect changes, e.g. the
                //removal of text segments
                oSegment = oSegment.ForteDocument.FindSegment(oSegment.FullTagID);

                RecreateOptionsForm.Locations iLocation = 0;
                RecreateOptionsForm.TextOptions iTextOption = 0;

                RecreateOptionsForm.BodyTypes iBodyType = RecreateOptionsForm.BodyTypes.None;

                for (int i = 0; i < oSegment.Blocks.Count; i++)
                {
                    if (oSegment.Blocks[i].IsBody)
                    {       
                        iBodyType = RecreateOptionsForm.BodyTypes.Standard;
                        break;
                    }
                }

                Variable oMsgVar = null;
                if (iBodyType == RecreateOptionsForm.BodyTypes.None)
                {
                    //segment has no body - check for message variable
                    try
                    {
                        oMsgVar = oSegment.Variables.ItemFromName("Message");
                    }
                    catch { }

                    if (oMsgVar != null)
                        iBodyType = RecreateOptionsForm.BodyTypes.Message;
                }

                using (RecreateOptionsForm oForm = new RecreateOptionsForm(
                    RecreateOptionsForm.FormType.RecreateMacPac10Segment))
                {
                    // Set the HasBody property so that the appropriate options are shown.
                    oForm.BodyType = iBodyType;

                    Word.Selection oSel = LMP.MacPac.Session.CurrentWordApp.Selection;

                    //GLOG item #4505 - dcf
                    // If no text is selected, disable the option to use selected text.
                    //oForm.UseSelectedTextOption = (oSel.End - oSel.Start) > 0;
                    oForm.UseSelectedTextOption = true;

                    //prompt for recreation options
                    DialogResult iChoice = oForm.ShowDialog(this);

                    if (iChoice == DialogResult.Cancel)
                    {
                        //GLOG : 8000 : ceh - set focus back to the document
                        Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                        return;
                    }

                    iLocation = oForm.InsertionLocation;
                    iTextOption = oForm.TextOption;
                }

                // GLOG : 2246 : JAB
                // Prompt the prompt the user to verify that they want to replace existing.
                if (iLocation == RecreateOptionsForm.Locations.ReplaceExisting)
                {
                    if (MessageBox.Show(LMP.Resources.GetLangString("Prompt_ReplaceDocumentContent"),
                                        LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }
                }

                //GLOG #5925 CEH
                //turn off track changes if necessary
                bool bTrackChanges = oSegment.ForteDocument.WordDocument.TrackRevisions;
                if (bTrackChanges)
                    oSegment.ForteDocument.WordDocument.TrackRevisions = false;

                //GLOG : 8183 : ceh
                object oTrailers = null;
                int iStartSection = 0;
                int iEndSection = 0;

                //GLOG 6588: If Replacing existing document, remove Trailers first
                if (iLocation == RecreateOptionsForm.Locations.ReplaceExisting)
                {
                    Word.Document oDoc = oSegment.ForteDocument.WordDocument;
                    string xTrailerStyle= "MacPac Trailer";
                    bool b9xTrailers = false;
                    //Get Start and End sections of Segment
                    iStartSection = oSegment.PrimaryRange.Sections.First.Index;
                    iEndSection = oSegment.PrimaryRange.Sections.Last.Index;
                    ArrayList aSections = new ArrayList();
                    for (int i = iStartSection; i <= iEndSection; i++)
                    {
                        aSections.Add(i);
                    }
                    LMP.Data.FirmApplicationSettings oFirmSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                    if (LMP.Data.Application.GetMetadata("CustomCode").ToUpper().Contains(
                        "CONVERTTRAILERSTO9XFORMAT") || oFirmSettings.UseMacPac9xStyleTrailer)
                    {
                        xTrailerStyle = "zzmpTrailerItem";
                        b9xTrailers = true;
                        oTrailers = LMP.Forte.MSWord.WordDoc.GetConverted9xTrailers(oDoc, xTrailerStyle);
                    }
                    else
                    {
                        xTrailerStyle = "MacPac Trailer";
                        b9xTrailers = false;
                        oTrailers = LMP.Forte.MSWord.WordDoc.GetExistingTrailers(oDoc, xTrailerStyle);
                    }
                    if (oTrailers != null)
                    {
                        object oZero = 0;
                        object[] aExistingTrailers = (object[])oTrailers;
                        //Remove any Existing Trailer from each Section 
                        if (aExistingTrailers != null)
                        {
                            for (int i = 0; i < aSections.Count; i++)
                            {
                                int iSection = (int)aSections[i];
                                if (aExistingTrailers[iSection-1] != null && aExistingTrailers[iSection-1] != oZero &&
                                       aExistingTrailers[iSection-1].ToString() != "Linked")
                                {
                                    object oSections = new int[] { iSection };
                                    if (b9xTrailers)
                                        LMP.Forte.MSWord.WordDoc.Remove8xTrailers(oDoc, false, oSections);
                                    else
                                        LMP.Forte.MSWord.WordDoc.RemoveOrphanedTrailers(oDoc, xTrailerStyle, oSections);
                                    
                                    //if Trailer has been deleted, clear LastTrailer variables so that Trailer prompt will appear again when saving'
                                    //GLOG : 8183 : ceh
                                    //need trailer variable for reinsertion
                                    //try
                                    //{
                                    //    DocumentStampUI.ClearLastTrailerVariable(Int32.Parse(aExistingTrailers[iSection - 1].ToString()));
                                    //}
                                    //catch { }
                                    //DocumentStampUI.ClearLastTrailerVariable();
                                    //DocumentStampUI.ClearNoTrailerVariable();
                                }
                            }
                        }
                    }
                    //Also delete any Draft Stamps or tagged Trailer Segments in same section
                    for (int i = oSegment.ForteDocument.Segments.Count - 1; i >= 0; i--)
                    {
                        Segment oStamp = m_oMPDocument.Segments[i];
                        if (oStamp != oSegment && (oStamp.TypeID == mpObjectTypes.DraftStamp || oStamp.TypeID == mpObjectTypes.Trailer))
                        {
                            //JTS 6/13/13: Use PrimaryRange instead of WordTags or ContentControls collections
                            if (!aSections.Contains(oStamp.PrimaryRange.Sections[1].Index))
                                continue;
                            oSegment.ForteDocument.DeleteSegment(oStamp, false);
                        }
                    }
                }
                Prefill oPrefill = null;
                //snapshot does not exist - create 
                //prefill from document to use as prefill
                oPrefill = oSegment.CreatePrefill();

                string xID = oSegment.ID;
                string xBodyXML = "";

                if (iTextOption != RecreateOptionsForm.TextOptions.NoText)
                {
                    //get xml of text specified for inclusion
                    if (oSegment.ForteDocument.FileFormat ==
                        LMP.Data.mpFileFormats.Binary)
                    {
                        Word.XMLNode oBodyTag = null;
                        if (iTextOption == RecreateOptionsForm.TextOptions.IncludeEntireBody)
                        {
                            //entire body - get body block
                            for (int i = 0; i < oSegment.Blocks.Count; i++)
                            {
                                if (oSegment.Blocks[i].IsBody)
                                {
                                    oBodyTag = oSegment.Blocks[i].AssociatedWordTag;
                                    break;
                                }
                            }
                        }

                        //GLOG 2668: Prevent loop if GetRecreateSegmentBodyXML has to insert temp paragraph
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        //get xml
                        xBodyXML = LMP.Forte.MSWord.WordDoc.GetRecreateSegmentBodyXML(oBodyTag);
                    }
                    else if (oSegment.ForteDocument.FileFormat ==
                        LMP.Data.mpFileFormats.OpenXML)
                    {
                        Word.ContentControl oBodyCC = null;
                        if (iTextOption == RecreateOptionsForm.TextOptions.IncludeEntireBody)
                        {
                            //entire body - get body block
                            for (int i = 0; i < oSegment.Blocks.Count; i++)
                            {
                                if (oSegment.Blocks[i].IsBody)
                                {
                                    oBodyCC = oSegment.Blocks[i].AssociatedContentControl;
                                    break;
                                }
                            }
                        }

                        //GLOG 2668: Prevent loop if GetRecreateSegmentBodyXML has to insert temp paragraph
                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;
                        //get xml
                        xBodyXML = LMP.Forte.MSWord.WordDoc.GetRecreateSegmentBodyXML_CC(oBodyCC, LMP.MacPac.Application.IsOxmlSupported());
                    }

                    ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                }

                if (iBodyType == RecreateOptionsForm.BodyTypes.Message)
                {
                    string xOriginalMessage = oMsgVar.Value;
                    string xNewMessage = "";
                    if (iTextOption == RecreateOptionsForm.TextOptions.IncludeSelectedText)
                    {
                        Word.Selection oSel = LMP.Forte.MSWord.GlobalMethods.CurWordApp.Selection;
                        xNewMessage = oSel.Text;
                    }
                    else
                    {
                        if (iTextOption == RecreateOptionsForm.TextOptions.IncludeEntireBody)
                        {
                            xNewMessage = xOriginalMessage;
                        }
                    }

                    oMsgVar.SetValue(xNewMessage);
                    oPrefill = oSegment.CreatePrefill();

                    if (iLocation == RecreateOptionsForm.Locations.NewDocument)
                    {
                        // If we're creating this in a new document restore the segment. Otherwise
                        // the segment is intended to be replaced and the modification made above
                        // should remain.
                        oMsgVar.SetValue(xOriginalMessage);
                    }
                }

                if (iLocation == RecreateOptionsForm.Locations.NewDocument)
                {
                    //we need to insert in a new document - that document's 
                    //ForteDocument object will need to insert the segment, as 
                    //the current doc's ForteDocument points to the current doc - 
                    //specify a pending action that the new content manager instance 
                    //will process

                    //use RecreateRedirectID if there is one -
                    if (oSegment.RecreateRedirectID != "")
                        xID = oSegment.RecreateRedirectID;

#if Oxml
                    if (LMP.MacPac.Application.IsOxmlSupported())
                    {
                        //GLOG 8425: Use Oxml for recreate in new document
                        //GLOG 7143 (dm) - don't allow in Word 2013 if save format is .doc
                        if ((LMP.Forte.MSWord.WordApp.Version > 14) && (Session.CurrentWordApp.DefaultSaveFormat.ToUpper() == "DOC"))
                        {
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_NewDocAutomationNotSupportedInWord2013"),
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        TaskPane.InsertSegmentInNewDoc(xID, oPrefill, xBodyXML, oSegment.ChildCollectionTableStructure);
                    }
                    else
#endif
                    {
                        TaskPane.PendingAction = new PendingAction(
                            PendingAction.Types.Insert, xID, oPrefill, xBodyXML,
                            null, false, oSegment.ChildCollectionTableStructure);

                        ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                        //create a new document
                        Word.Document oNewDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                            LMP.Data.Application.BaseTemplate, true, true);

                        //activate document - this prevents the task pane from
                        //going blank if right-clicked on immediately after creation
                        //of document
                        oNewDoc.Activate();

                        //Allow TaskPane Setup to complete
                        System.Windows.Forms.Application.DoEvents();

                        //GLOG 7845 (dm) - this block of code is no longer required
                        //because we're now reverting in ProcessPendingAction() when necessary
                        ////GLOG 6967 (dm) - if segment has pleading paper, revert to
                        ////Word 2010 compatibility mode and suppress extra space at top
                        //if (LMP.Forte.MSWord.WordDoc.GetDocumentCompatibility(oNewDoc) > 14)
                        //{
                        //    if (oSegment.Contains(mpObjectTypes.PleadingPaper))
                        //    {
                        //        LMP.fWordObjects.cWord14.SetCompatibilityMode(oNewDoc, 14);
                        //        oNewDoc.Compatibility[Word.WdCompatibility.wdSuppressTopSpacing] = true;
                        //    }
                        //}

                        if (!TaskPane.SkipScreenUpdates)
                        {
                            TaskPane oTP = TaskPanes.Item(oNewDoc);

                            if (oTP != null)
                            {
                                //GLOG item #6250 - dcf - 4/12/13
                                if (TaskPane.NewSegment == null)
                                {
                                    //no new segment was inserted - 
                                    //close document and exit
                                    object oFalse = false;
                                    object oMissing = System.Reflection.Missing.Value;
                                    oNewDoc.Close(ref oFalse, ref oMissing, ref oMissing);
                                    return;
                                }

                                //GLOG - 3338 - CEH
                                if (TaskPane.NewSegment.ShowChooser)
                                    //set focus to chooser
                                    oTP.DocEditor.SelectChooserNode();
                                else
                                {
                                    if (TaskPane.NewSegment.Variables.Count > 0)
                                        //Set focus to first variable control in tree
                                        oTP.DocEditor.SelectFirstVariableNode();
                                    else
                                        oTP.DocEditor.SelectFirstNodeForSegment(TaskPane.NewSegment);
                                }
                                System.Windows.Forms.Application.DoEvents();
                            }
                        }
                        LMP.MacPac.Application.ForceProfileIfNecessary();
                    }

                }
                else
                {
                    UltraTreeNode oNode = GetNodeFromTagID(oSegment.FullTagID);

                    //replace existing segment
                    UpdateSegment(oSegment, oNode, xBodyXML);

                    //GLOG : 8183 : ceh
                    //Recreate Trailer upon Replace Existing
                    if (oTrailers != null)
                    {
                        try
                        {
                            if (this.treeDocContents.ActiveNode.Tag is Segment)
                            {
                                //Get ActiveNode segment
                                oSegment = (Segment)this.treeDocContents.ActiveNode.Tag;

                                //Get Start and End sections of Segment
                                iStartSection = oSegment.PrimaryRange.Sections.First.Index;
                                iEndSection = oSegment.PrimaryRange.Sections.Last.Index;
                                ArrayList aSections = new ArrayList();
                                for (int i = iStartSection; i <= iEndSection; i++)
                                {
                                    aSections.Add(i);
                                }

                                for (int i = 0; i < aSections.Count; i++)
                                {
                                    int iSection = (int)aSections[i];

                                    //Check if current section contains a trailer - may be null if no trailer
                                    string xTrailerID = ((object[])oTrailers)[iSection - 1].ToString();
                                    if (LMP.String.IsNumericInt32(xTrailerID))
                                    {
                                        //GLOG 6917: Check that Trailer Segment currently exists
                                        int iTrailerID = Int32.Parse(xTrailerID);
                                        AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.Trailer);
                                        AdminSegmentDef oTrailerDef = (AdminSegmentDef)oDefs.ItemFromID(iTrailerID);
                                        //GLOG 6917: Don't need to recreate trailer if it's in body story
                                        if (oTrailerDef != null && String.ContentContainsLinkedStories(oTrailerDef.XML, true, true, true, true, true, true))
                                        {
                                            Prefill oTrailerPrefill = DocumentStampUI.LastTrailerPrefill(iTrailerID);
                                            if (oTrailerPrefill != null)
                                                LMP.MacPac.Application.RecreateTrailer(oTrailers, oTrailerPrefill, 0);
                                        }
                                    }
                                }
                            }
                        }
                        catch { }
                    }

                    //GLOG item #5912 - dcf
                    //GLOG 8477: ForteDocument.Refresh has already been run by UpdateSegment, just update UI
                    this.RefreshTree(false, true);

                    UpdateButtons(this.ForteDocument);
                }

                //GLOG #5925 CEH
                if (bTrackChanges)
                    oSegment.ForteDocument.WordDocument.TrackRevisions = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.TaskPane.DataReviewerVisible = this.TaskPane.ForteDocument.ContainsFinishedSegments;
                this.TaskPane.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// updates the specified segment to the latest definition
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oNode"></param>
        /// <param name="xBodyXML"></param>
        private void UpdateSegment(Segment oSegment, UltraTreeNode oNode, string xBodyXML){
            try
            {
                //Save Index of selected node
                //Keys will have changed after refresh,
                //So we can't use that to restore
                List<int> oNodeLevels = new List<int>(oNode.Level+1);
                UltraTreeNode oTempNode = oNode;
                //Fill List with node index for each level, starting at bottom
                for (int iLevel = oNode.Level; iLevel >= 0; iLevel--)
                {
                    oNodeLevels.Add(oTempNode.Index);
                    //GLOG 4433 (dm)
                    //oTempNode = oNode.Parent;
                    oTempNode = oTempNode.Parent;
                }

                LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                    LMP.MacPac.Session.CurrentWordApp.ActiveDocument);
                oEnv.SaveState();
                this.TaskPane.ScreenUpdating = false;
                Prefill oPrefill = oSegment.CreatePrefill();

                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.All;

                //GLOG item #5823 - dcf - 11/28/11
                bool bHandlersActive = m_bSuppressTreeActivationHandlers;
                m_bSuppressTreeActivationHandlers = true;

                //if RecreateRedirectID is not empty, the segment
                //to be updated is saved content - redirect as specified
                //so that we're using the saved content - this will
                //preserve user changes to the structure of the segment,
                //e.g. additional pleading signatures.
                if (oSegment.RecreateRedirectID != "")
                {
                    string xID = oSegment.RecreateRedirectID;
                    this.m_oMPDocument.UpdateSegment(xID, oSegment.Parent, oPrefill, xBodyXML);
                }
#if Oxml
                else if (LMP.MacPac.Application.IsOxmlSupported())
                {
                    //GLOG 3118 - raise event so that editor knows to preserve
                    //the index of the segment node getting replaced
                    //m_oMPDocument_BeforeSegmentReplacedByAction(this.m_oMPDocument, new SegmentEventArgs(oSegment));
                    CollectionTableStructure oStructure = oSegment.ChildCollectionTableStructure;
                    object oMissing = System.Reflection.Missing.Value;
                    Word.Range oRng = oSegment.PrimaryRange;
                    oRng.StartOf(ref oMissing, ref oMissing);
                    object oNewSegment = TaskPane.InsertXmlSegmentInCurrentDoc(oSegment.ID, oSegment, oPrefill, Architect.Base.Segment.InsertionLocations.Default, 
                        Architect.Base.Segment.InsertionBehaviors.Default, xBodyXML, oStructure, oRng);
                    //GLOG 8701: Refresh TaskPane
                    this.m_oMPDocument.Refresh(ForteDocument.RefreshOptions.None);
                }
#endif
                else
                {
                    //GLOG 4439 (dm) - we need to use a different method to avoid
                    //inadvertantly updating all sibling instances of the same segment
                    string xID = oSegment.ID;
                    Segment.Update(oSegment, oPrefill, xBodyXML);
                    this.m_oMPDocument.Refresh(ForteDocument.RefreshOptions.RefreshTagsWithGUIDIndexes);
                }

                //GLOG 2674: Make sure View is restored and Main story is active
                oEnv.RestoreState(false, true, false);

                //GLOG item #5823 - dcf - 11/28/11
                m_bSuppressTreeActivationHandlers = bHandlersActive;

                this.ExpandTopLevelNodes();

                //restore originally selected node
                oTempNode = null;
                for (int i = oNodeLevels.Count - 1; i >= 0; i--)
                {
                    if (oTempNode == null)
                        oTempNode = this.treeDocContents.Nodes[oNodeLevels[i]];
                    else
                        oTempNode = oTempNode.Nodes[oNodeLevels[i]];
                    oTempNode.Expanded = true;
                }
                this.SelectNode(oTempNode);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                //GLOG 5794 (dm)
                UpdateButtons(this.ForteDocument);

                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
                LMP.MacPac.Application.SetXMLTagStateForEditing();

                this.Focus();

                this.TaskPane.ScreenUpdating = true;
            }
        }
        /// <summary>
        /// Create a Prefill based on current segment
        /// </summary>
        private void SaveData()
        {
            if (m_oCurNode.Tag is Segment)
            {
                SaveData((Segment)m_oCurNode.Tag);
            }
        }
        
        /// <summary>
        /// Create a Prefill based on the specified segment
        /// </summary>
        internal bool SaveData(Segment oSegment)
        {
            if (oSegment != null)
            {
                UserFolders oFolders = new UserFolders(Session.CurrentUser.ID);
                // Can't create Prefill if no User Folders exist
                //GLOG : 8152 : JSW
                //let ContentPropertyForm handle folder creation messages
                //if (oFolders.Count == 0)
                //{
                //    MessageBox.Show(LMP.Resources.GetLangString("Msg_PrefillRequiresUserFolder"),
                //        LMP.ComponentProperties.ProductName,
                //        MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return false;
                //}

                User oUser = Session.CurrentUser;
                VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.User, oUser.ID);
                int iSavedDataItems = oDefs.Count;
                int iLimit = oUser.SavedDataLimit;

                // GLOG : 5358 : JSW
                // Check if the user has exceeded the maximum number of segments allowed.
                if ((iSavedDataItems >= iLimit))
                {
                    // The user has exceeded the maximum allowed segments. Notify the user
                    // and do not create new content.
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_SavedDataLimitReached"),
                        iLimit), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }

                // GLOG : 5358 : JSW
                // Check if the number of saved data items requires warning the user that
                // they are approaching the maximum user content limit.
                if (iSavedDataItems >= oUser.SavedDataThreshold)
                {
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_ApproachingSavedDataLimit"),
                        iSavedDataItems, iLimit), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                } 
                
                return FinishSaveData(oSegment);
            }
            else
                return false;
        }

        internal bool FinishSaveData(Segment oSegment)
        {
            //GLOG item #5873 - dcf - need to finish unfinished children
            if (!this.TaskPane.FinishChildrenIfNecessary(oSegment))
                return false;

            //GLOG 5822 (dm) - update oSegment to reflect changes, e.g. the
            //removal of text segments
            oSegment = oSegment.ForteDocument.FindSegment(oSegment.FullTagID);

            ContentPropertyForm oForm = new ContentPropertyForm();
            DialogResult oRes = oForm.ShowForm(this.ParentForm, this.m_oCurNode, Form.MousePosition,
                ContentPropertyForm.DialogMode.CreatePrefill, null, oSegment);
            System.Windows.Forms.Application.DoEvents();

            return oRes == DialogResult.OK;

            //GLOG 3064: refresh content manager tree in all
            //task panes to reflect new saved data
            TaskPanes.RefreshAll(true, false, false);
        }

        /// <summary>
        /// returns the xml of the specified segment
        /// </summary>
        /// <param name="oSeg"></param>
        /// <returns></returns>
        public static string GetSegmentXML(Segment oSeg)
        {
            return GetSegmentXML(oSeg, true, true);
        }
        /// <summary>
        /// returns the xml of the specified segment, optionally omitting Body Block
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="bIncludeBody"></param>
        /// <returns></returns>
        public static string GetSegmentXML(Segment oSeg, bool bIncludeBody, bool bRemoveStamps)
        {
            //GLOG 3227, 3274, 3042: Modified to remove Trailers and Draft Stamps from content
            //if specified.  Also, headers and footers should be included even if selected Segment
            //is not the only top-level Segment in the document
            string xSegmentXML = "";
            bool bCCInBody = false;
            //GLOG 6866
            string xID = oSeg.ID;
            if (xID.EndsWith(".0"))
                xID = xID.Replace(".0", "");
            Word.Range oRange = oSeg.PrimaryRange; //GLOG 6793
            //if (oSeg.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            //{
            //    oRange = oSeg.WordTags[0].Range;
            //}
            //else
            //{
            //    oRange = oSeg.ContentControls[0].Range;
            //}
            //GLOG 8347: Ensure XML does not contain rsid attributes
            bool bStore = Session.CurrentWordApp.Options.StoreRSIDOnSave;
            Session.CurrentWordApp.Options.StoreRSIDOnSave = false;
            try
            {
                //GLOG 6866: Only use full Document XML if PrimaryRange is in Main Body
                if (oRange.StoryType == Microsoft.Office.Interop.Word.WdStoryType.wdMainTextStory && oRange.Text.Length >= oRange.Document.Content.Text.Length - 3)
                {
                    //segment is entire document
                    if (oSeg.ForteDocument.FileFormat == mpFileFormats.Binary)
                    {
                        xSegmentXML = oRange.Document.Content.get_XML(false);
                        string xNSPrefix = String.GetNamespacePrefix(xSegmentXML, ForteConstants.MacPacNamespace);
                        if (xNSPrefix != "" && bRemoveStamps)
                        {
                            //Remove Trailers/Draft Stamps from headers/footers
                            xSegmentXML = String.RemoveDocumentStampXML(xSegmentXML, xNSPrefix, true, true);
                        }
                    }
                    else
                    {
                        //GLOG 4975 (dm)
                        xSegmentXML = oRange.Document.Content.WordOpenXML;
                    }
                }
                else if (oSeg is Paper)
                {
                    //GLOG 6866: For Paper, get Section XML and remove main body
                    if (oSeg.ForteDocument.FileFormat == mpFileFormats.Binary)
                    {
                        xSegmentXML = oRange.Sections[1].Range.get_XML(false);
                        string xNSPrefix = String.GetNamespacePrefix(xSegmentXML, ForteConstants.MacPacNamespace);
                        if (xNSPrefix != "" && bRemoveStamps)
                        {
                            //Remove Trailers/Draft Stamps from headers/footers
                            xSegmentXML = String.RemoveDocumentStampXML(xSegmentXML, xNSPrefix, true, true);
                        }
                    }
                    else
                    {
                        //GLOG 4975 (dm)
                        xSegmentXML = oRange.Sections[1].Range.WordOpenXML;
                    }
                    //Replace body story with empty paragraph
                    int iBodyStart = xSegmentXML.IndexOf("<w:body>");
                    if (iBodyStart > -1)
                    {
                        int iBodyEnd = xSegmentXML.IndexOf("<w:sectPr", iBodyStart);
                        if (iBodyEnd > -1)
                        {
                            if (oSeg.ForteDocument.FileFormat == mpFileFormats.Binary)
                            {
                                xSegmentXML = xSegmentXML.Substring(0, iBodyStart) + "<w:body><wx:sect><w:p/>" +
                                    xSegmentXML.Substring(iBodyEnd);
                            }
                            else
                            {
                                xSegmentXML = xSegmentXML.Substring(0, iBodyStart) + "<w:body><w:p/>" +
                                    xSegmentXML.Substring(iBodyEnd);
                            }
                        }
                    }
                    //Get List of Child Segment IDs to be included
                    ArrayList aIncludedIDs = new ArrayList();
                    aIncludedIDs.Add(xID);
                    foreach (string xChildID in oSeg.ChildSegmentIDs(true).Split('|'))
                    {
                        string xNewID = xChildID;
                        if (xNewID.EndsWith(".0"))
                            xNewID = xNewID.Replace(".0", "");
                        aIncludedIDs.Add(xNewID);
                    }
                    //Look for mSEG tags or ContentControls that don't belong to this Segment or its Children
                    if (oSeg.ForteDocument.FileFormat == mpFileFormats.Binary)
                    {
                        string xNSPrefix = String.GetNamespacePrefix(xSegmentXML, ForteConstants.MacPacNamespace);
                        if (xNSPrefix != "")
                        {
                            string xmSEGStart = "<" + xNSPrefix + ":mSEG ";
                            string xmSEGEnd = "</" + xNSPrefix + ":mSEG>";
                            int iPos = xSegmentXML.IndexOf(xmSEGStart);
                            while (iPos > -1)
                            {
                                //mSEG found - determine whether this a target tag
                                int iPos2 = xSegmentXML.IndexOf('>', iPos);
                                string xStartTag = xSegmentXML.Substring(iPos, iPos2 - iPos);
                                //Remove if TagID is not for Segment or Child Segment
                                bool bInclude = false;
                                foreach (string xTagID in aIncludedIDs)
                                {

                                if (xStartTag.Contains("SegmentID=" + xTagID + "|"))
                                {
                                    bInclude = true;
                                    break;
                                }
                            }
                            if (!bInclude)
                            {
                                //Opening mSEG tag for different Segment
                                int iStart = iPos;
                                int iEnd = xSegmentXML.IndexOf(xmSEGEnd, iStart);
                                iStart = xSegmentXML.IndexOf(xmSEGStart, iStart + 1, iEnd - iStart);
                                while (iStart > -1 && iStart < iEnd)
                                {
                                    //There's an intervening mSEG start tag before found end Tag
                                    //Search again until all contained mSEG opening tags have been closed
                                    iEnd = xSegmentXML.IndexOf(xmSEGEnd, iEnd + xmSEGEnd.Length);
                                    iStart = xSegmentXML.IndexOf(xmSEGStart, iStart + 1, iEnd - iStart);
                                }
                                xSegmentXML = xSegmentXML.Substring(0, iPos) + "<w:p/>" + xSegmentXML.Substring(iEnd + xmSEGEnd.Length);
                                iPos2 = iPos;
                            }
                            iPos = xSegmentXML.IndexOf(xmSEGStart, iPos2 + 1);
                        }
                    }
                }
                else
                {
                    bool bFound = false;
                    int iCCEnd = 0;
                    do
                    {
                        //Search for Content Controls containing mSEGs not belonging to this Segment and remove from XML
                        bFound = false;
                        int iCCStart = xSegmentXML.IndexOf("<w:sdt>", iCCEnd);
                        if (iCCStart > -1)
                        {
                            bFound = true;
                            iCCEnd = xSegmentXML.IndexOf("</w:sdtPr>", iCCStart);
                            if (iCCEnd > -1)
                            {
                                int iTagStart = xSegmentXML.IndexOf("<w:tag w:val=");
                                if (iTagStart > -1)
                                {
                                    bFound = true;
                                    int iTagEnd = xSegmentXML.IndexOf("/>", iTagStart);
                                    if (iTagEnd > -1)
                                    {
                                        string xTagXML = xSegmentXML.Substring(iTagStart, iTagEnd - iTagStart);
                                        if (xTagXML.Contains("\"mps"))
                                        {
                                            bool bInclude = false;
                                            foreach (string xTagID in aIncludedIDs)
                                            {
                                                if (xTagXML.Contains(xTagID.Replace("-", "m").Replace(".", "d") + "0000"))
                                                {
                                                    bInclude = true;
                                                    break;
                                                }
                                            }
                                            if (!bInclude)
                                            {
                                                iCCEnd = xSegmentXML.IndexOf("</w:sdt>", iCCStart);
                                                int iChildSeg = iCCStart + @"<w:sdt>".Length;
                                                iChildSeg = xSegmentXML.IndexOf("<w:sdt>", iChildSeg, iCCEnd - iChildSeg);
                                                while (iChildSeg > -1)
                                                {
                                                    iCCEnd = xSegmentXML.IndexOf("</w:sdt>", iCCEnd + @"</w:sdt>".Length);
                                                    iChildSeg = xSegmentXML.IndexOf("<w:sdt>", iChildSeg + @"<w:sdt>".Length, iCCEnd - iChildSeg);
                                                }
                                                if (iCCEnd > -1)
                                                {
                                                    xSegmentXML = xSegmentXML.Substring(0, iCCStart) + xSegmentXML.Substring(iCCEnd + @"</w:sdt>".Length);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } while (bFound);
                }
            }
            else if (oSeg.IntendedUse == mpSegmentIntendedUses.AsDocument)
            {
                //Start with XML for entire Document and extract relevant portions
                //to ensure headers/footers are included
                //GLOG 6275: Use TagID, since Segment may not be top-level, even if Document type
                //(e.g., TOA inserted at cursor)
                string xTagID = oSeg.TagID; // .FullTagID;
                if (oSeg.ForteDocument.FileFormat == mpFileFormats.Binary)
                {
                    xSegmentXML = oRange.Document.Content.get_XML(false);
                    string xNSPrefix = String.GetNamespacePrefix(xSegmentXML, ForteConstants.MacPacNamespace);
                    if (xNSPrefix != "")
                    {
                        string xmSEGStart = "<" + xNSPrefix + ":mSEG ";
                        string xmSEGEnd = "</" + xNSPrefix + ":mSEG>";
                        string xSegBody = "";
                        int iPos = xSegmentXML.IndexOf(xmSEGStart);
                        while (iPos > -1)
                        {
                            //mSEG found - determine whether this a target tag
                            int iPos2 = xSegmentXML.IndexOf('>', iPos);
                            string xStartTag = xSegmentXML.Substring(iPos, iPos2 - iPos);
                            if (xStartTag.Contains(" TagID=" + "\"" + xTagID + "\""))
                            {
                                //Found correct opening tag - now locate corresponding closing tag
                                int iStart = iPos;
                                int iEnd = xSegmentXML.IndexOf(xmSEGEnd, iStart);
                                iStart = xSegmentXML.IndexOf(xmSEGStart, iStart + 1, iEnd - iStart);
                                while (iStart > -1 && iStart < iEnd)
                                {
                                    //There's an intervening mSEG start tag before found end Tag
                                    //Search again until all contained mSEG opening tags have been closed
                                    iEnd = xSegmentXML.IndexOf(xmSEGEnd, iEnd + xmSEGEnd.Length);
                                    iStart = xSegmentXML.IndexOf(xmSEGStart, iStart + 1, iEnd - iStart);
                                }
                                xSegBody = xSegmentXML.Substring(iPos, (iEnd + xmSEGEnd.Length) - iPos);

                                //GLOG 5595 (dm) - this branch was creating segments that erred on
                                //insertion in Word 2010 because we weren't including the mSEG bookmark
                                iPos2 = xStartTag.IndexOf(" Reserved=\"mps");
                                if (iPos2 > -1)
                                {
                                    iPos2 += 11;
                                    int iPos3 = xStartTag.IndexOf("\"", iPos2);
                                    string xBkmk = "_" + xStartTag.Substring(iPos2, iPos3 - iPos2);
                                    iPos2 = xSegmentXML.LastIndexOf(" w:name=\"" + xBkmk, iPos);
                                    if (iPos2 > -1)
                                    {
                                        int iBmkStart = xSegmentXML.LastIndexOf("<aml:annotation ", iPos2);
                                        if (iBmkStart > -1)
                                        {
                                            //bookmark starts before mSEG - prepend start tag
                                            iPos2 = xSegmentXML.IndexOf(">", iBmkStart) + 1;
                                            string xBmkStart = xSegmentXML.Substring(iBmkStart, iPos2 - iBmkStart);
                                            xSegBody = xBmkStart + xSegBody;

                                            //find bookmark end
                                            iPos2 = xSegmentXML.IndexOf(" aml:id=\"", iBmkStart);
                                            iPos3 = xSegmentXML.IndexOf("\"", iPos2 + 9);
                                            string xBkmkID = xSegmentXML.Substring(iPos2, iPos3 - iPos2);
                                            iPos2 = xSegmentXML.IndexOf(xBkmkID, iPos);
                                            int iBmkEnd = xSegmentXML.IndexOf(">", iPos2) + 1;

                                            if (iBmkEnd > iEnd)
                                            {
                                                //bookmark ends after mSEG - append closing tag
                                                iPos2 = xSegmentXML.LastIndexOf("<aml:annotation ", iBmkEnd);
                                                string xBmkEnd = xSegmentXML.Substring(iPos2, iBmkEnd - iPos2);
                                                xSegBody = xSegBody + xBmkEnd;
                                            }
                                        }
                                    }
                                }

                                //Look for w:sectPr tag following closing mSEG
                                int iSectStart = xSegmentXML.IndexOf("<w:sectPr", iEnd + xmSEGEnd.Length);
                                if (iSectStart > -1)
                                {
                                    int iSectEnd = xSegmentXML.IndexOf("</w:sectPr>", iSectStart);
                                    //If intervening wx:sect is found, the w:sectPr node is not for this section
                                    if (!xSegmentXML.Substring(iSectStart, iSectEnd - iSectStart).Contains("<wx:sect>"))
                                    {
                                        string xLastSectionXML = xSegmentXML.Substring(iSectStart, (iSectEnd + @"</w:sectPr>".Length) - iSectStart);
                                        xSegBody = xSegBody + xLastSectionXML;
                                    }
                                }
                                break;
                            }
                            else
                                iPos = xSegmentXML.IndexOf(xmSEGStart, iPos2 + 1);
                        }
                        if (xSegBody != "")
                        {
                            //Remove Trailers/Draft Stamps from headers/footers
                            if (bRemoveStamps)
                                xSegBody = String.RemoveDocumentStampXML(xSegBody, xNSPrefix, true, true);
                            int iBodyStart = xSegmentXML.IndexOf("<w:body>");
                            if (iBodyStart > -1)
                            {
                                int iBodyEnd = xSegmentXML.IndexOf("</w:body>", iBodyStart);
                                if (iBodyEnd > -1)
                                {
                                    xSegmentXML = xSegmentXML.Substring(0, iBodyStart) + "<w:body>" + xSegBody +
                                        "</w:body>" + xSegmentXML.Substring(iBodyEnd + @"</w:body>".Length);
                                }
                            }
                        }
                    }
                }
                else
                {
                    //GLOG 4975 (dm) - open xml
                    xSegmentXML = oRange.Document.Content.WordOpenXML;

                    //get xml for target mSEG
                    string xTag = "";
                    string xSegBody = "";
                    XmlDocument oSegmentXML = new XmlDocument();
                    oSegmentXML.LoadXml(xSegmentXML);
                    XmlNamespaceManager oNSMan = new XmlNamespaceManager(oSegmentXML.NameTable);
                    oNSMan.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);
                    XmlNodeList oTagNodes = oSegmentXML.SelectNodes(
                        "//w:sdtPr/w:tag[starts-with(@w:val, 'mps')]", oNSMan);
                    for (int i = 0; i < oTagNodes.Count; i++)
                    {
                        xTag = oTagNodes[i].Attributes["w:val"].Value;
                        XmlNode oDocVarNode = oSegmentXML.SelectSingleNode(
                            "//w:docVar[@w:name='" + "mpo" + xTag.Substring(3, 8) + "']", oNSMan);
                        string xValue = oDocVarNode.Attributes["w:val"].Value;
                        if (xValue.Substring(0, xValue.IndexOf("��")) == xTagID)
                        {
                            XmlNode oSegNode = oTagNodes[i].ParentNode.ParentNode;
                            //remove unnecessary namespace reference
                            xSegBody = oSegNode.OuterXml.Replace("<w:sdt xmlns:w=\"" +
                                ForteConstants.WordOpenXMLNamespace + "\">", "<w:sdt>");
                            break;
                        }
                    }

                    //get xml for last section of target mSEG
                    int iSectStart = xSegmentXML.IndexOf("<w:tag w:val=\"" + xTag);
                    int iSectionCount = LMP.String.CountChrs(xSegBody, "<w:sectPr");
                    for (int i = 0; i <= iSectionCount; i++)
                        iSectStart = xSegmentXML.IndexOf("<w:sectPr", iSectStart);
                    if (iSectStart > -1)
                    {
                        int iSectEnd = xSegmentXML.IndexOf("</w:sectPr>", iSectStart);
                        string xLastSectionXML = xSegmentXML.Substring(iSectStart, (iSectEnd + @"</w:sectPr>".Length) - iSectStart);
                        xSegBody = xSegBody + xLastSectionXML;
                    }

                    //replace body node with the xml for just the targeted segment
                    int iBodyStart = xSegmentXML.IndexOf("<w:body>");
                    if (iBodyStart > -1)
                    {
                        int iBodyEnd = xSegmentXML.IndexOf("</w:body>", iBodyStart);
                        if (iBodyEnd > -1)
                        {
                            xSegmentXML = xSegmentXML.Substring(0, iBodyStart) + "<w:body>" + xSegBody +
                                "</w:body>" + xSegmentXML.Substring(iBodyEnd + @"</w:body>".Length);
                        }
                    }

                }
            }
            else if (oSeg.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //GLOG 6274: Make sure containing Word Tag is included in XML
                oRange.SetRange(oRange.Start - 1, oRange.End + 1);
                //Get just xml for containing Word Tag
                xSegmentXML = oRange.get_XML(false);
                //GLOG 5274: Segment bookmark may not be included if mSEG immediately follows Section break - insert directly into XML
                //GLOG 6855: Get bookmark name
                string xReserved = oSeg.Bookmarks[0].Name;

                if (xReserved != "" && !xSegmentXML.Contains("w:name=\"" + xReserved + "\""))
                {
                    string xBookmarkStartXML = "<aml:annotation aml:id=\"{0}\" w:type=\"Word.Bookmark.Start\" w:name=\"{1}\" w:displacedBySDT=\"next\"/>";
                    string xBookmarkEndXML = "<aml:annotation aml:id=\"{0}\" w:type=\"Word.Bookmark.End\" w:displacedBySDT=\"prev\"/>";
                    int iBmkID = 0;
                    while (xSegmentXML.Contains("aml:id=\"" + iBmkID.ToString() + "\""))
                    {
                        //Find first unused Bookmark ID
                        iBmkID++;
                    }
                    string xBmkName = "_" + xReserved;
                    xBookmarkStartXML = string.Format(xBookmarkStartXML, iBmkID, xBmkName);
                    xBookmarkEndXML = string.Format(xBookmarkEndXML, iBmkID);
                    string xNSPrefix = String.GetNamespacePrefix(xSegmentXML, ForteConstants.MacPacNamespace);
                    int iPos = xSegmentXML.IndexOf("<" + xNSPrefix + ":mSEG");
                    if (iPos > -1)
                    {
                        //Insert Bookmark.Start before mSEG
                        xSegmentXML = xSegmentXML.Insert(iPos, xBookmarkStartXML);
                    }
                    iPos = xSegmentXML.LastIndexOf("</" + xNSPrefix + ":mSEG>");
                    if (iPos > -1)
                    {
                        //Insert Bookmark.End after mSEG
                        xSegmentXML = xSegmentXML.Insert(iPos + ("</" + xNSPrefix + ":mSEG>").Length, xBookmarkEndXML);
                    }
                }
            }
            else
            {
                //GLOG 6274: Make sure containing ContentControl is included in XML
                //GLOG 6855: Don't expand if range is in table
                if (oRange.Characters.First.Tables.Count == 0 && oRange.Characters.Last.Tables.Count == 0)
                    oRange.SetRange(oRange.Start - 1, oRange.End + 1);
                //Get just xml for containing content control
                xSegmentXML = oRange.WordOpenXML;
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                //GLOG 8347: Restore original rsid state
                Session.CurrentWordApp.Options.StoreRSIDOnSave = bStore;
            }
            xSegmentXML = UpdateSegmentTagIDInXML(xSegmentXML, oSeg, true);

            //GLOG 6866: Remove AlternateContent nodes before inserting new Bookmark XML, since it may be inside a textbos
            //9-30-11 (dm)- remove <mc:AlternateContent> nodes for textboxes containing
            //graphics as we already do in DocumentDesigner.SaveSegmentDefinition() -
            //these nodes cause portions of graphic to be cut off
            if (LMP.String.IsWordOpenXML(xSegmentXML))
            {
                int iStart = xSegmentXML.IndexOf("<mc:AlternateContent");
                while (iStart > -1)
                {
                    int iEnd = xSegmentXML.IndexOf("</mc:AlternateContent>", iStart);
                    if (iEnd > -1)
                    {
                        string xPictXML = xSegmentXML.Substring(iStart, iEnd - iStart);
                        int iPictStart = xPictXML.IndexOf("<w:pict>");
                        if (iPictStart > -1)
                        {
                            int iPictEnd = xPictXML.IndexOf("</w:pict>");
                            if (iPictEnd > -1)
                            {
                                xPictXML = xPictXML.Substring(iPictStart, (iPictEnd + @"</w:pict>".Length) - iPictStart);
                                xSegmentXML = xSegmentXML.Substring(0, iStart) + xPictXML + xSegmentXML.Substring(iEnd + @"</mc:AlternateContent>".Length);
                            }
                        }
                    }
                    iStart = xSegmentXML.IndexOf("<mc:AlternateContent>", iStart + 1);
                }
                //GLOG 5739: Remove Trailer/Draft Stamp textboxes after AlternateContent nodes have been removed
                if (bRemoveStamps)
                    xSegmentXML = String.RemoveDocumentStampXML_CC(xSegmentXML, true, true);
            }


            // Make sure mpNewSegment bookmark exists for Segment XML
            if (oSeg.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary && xSegmentXML.IndexOf(Segment.NEW_SEGMENT_BMK_XML) == -1) //GLOG 4913
            {
                int iPos = xSegmentXML.IndexOf("<" + LMP.String.GetNamespacePrefix(
                    xSegmentXML, ForteConstants.MacPacNamespace) + ":mSEG");

                if (iPos > -1)
                {
                    iPos = xSegmentXML.IndexOf(">", iPos + 1);
                    if (iPos > -1)
                        xSegmentXML = xSegmentXML.Insert(iPos + 1, Segment.NEW_SEGMENT_BMK_XML);
                }
                else
                {
                    //GLOG 6799: If no mSEG tag, insert mpNewSegment bookmark before first existing bookmark
                    iPos = xSegmentXML.IndexOf("<aml:annotation aml:id=");
                    if (iPos > -1)
                        xSegmentXML = xSegmentXML.Insert(iPos, Segment.NEW_SEGMENT_BMK_XML);

                }
            }
            else if (oSeg.ForteDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML && xSegmentXML.IndexOf(Segment.NEW_SEGMENT_BMK_OPENXML) == -1)
            {
                //GLOG 4913: Need to add New Segment Bookmark to XML for first Content Control
                int iPos = xSegmentXML.IndexOf("w:tag w:val=\"mps");
                if (iPos > -1)
                {
                    iPos = xSegmentXML.IndexOf("<w:sdtContent>", iPos);
                    if (iPos > -1)
                        xSegmentXML = xSegmentXML.Insert(iPos + 14, Segment.NEW_SEGMENT_BMK_OPENXML);
                }
                else
                {
                    //GLOG 6799: If no mSEG Content Control, insert mpNewSegment bookmark before first existing bookmark
                    iPos = xSegmentXML.IndexOf("<w:bookmarkStart ");
                    if (iPos > -1)
                        xSegmentXML = xSegmentXML.Insert(iPos, Segment.NEW_SEGMENT_BMK_OPENXML);
                }
            }
            if (!bIncludeBody)
            {
                //GLOG 3147: Replace body with default text
                for (int i = 0; i < oSeg.Blocks.Count; i++)
                {
                    Block oBlock = oSeg.Blocks[i];
                    if (oBlock.IsBody)
                    {
                        string xBlock = oBlock.Name;
                        //Locate start of mBlock
                        int iTagStart = -1;
                        if (oSeg.ForteDocument.FileFormat == mpFileFormats.Binary)
                        {
                            iTagStart = xSegmentXML.IndexOf("<" + LMP.String.GetNamespacePrefix(
                                xSegmentXML, ForteConstants.MacPacNamespace) + ":mBlock TagID=\"" + xBlock + "\"");
                        }
                        else
                        {
                            //GLOG 4975 (dm) - open xml
                            iTagStart = xSegmentXML.IndexOf("<w:tag w:val=\"mpb");
                            while (iTagStart > -1)
                            {
                                //mBlock found - determine whether this the right one
                                string xDocVarID = xSegmentXML.Substring(iTagStart + 17, 8);
                                int iPos2 = xSegmentXML.IndexOf(
                                    "<w:docVar w:name=\"mpo" + xDocVarID);
                                iPos2 = xSegmentXML.IndexOf("w:val=", iPos2) + 7;
                                int iPos3 = xSegmentXML.IndexOf("��", iPos2);
                                if (xSegmentXML.Substring(iPos2, iPos3 - iPos2) == xBlock)
                                    break;
                                else
                                    iTagStart = xSegmentXML.IndexOf("<w:tag w:val=\"mpb",   
                                        iTagStart + 1);
                            }
                        }
                        if (iTagStart > -1)
                        {
                            int iTagEnd = -1;
                            if (oSeg.ForteDocument.FileFormat == mpFileFormats.Binary)
                            {
                                iTagEnd = xSegmentXML.IndexOf("</" + LMP.String.GetNamespacePrefix(
                                    xSegmentXML, ForteConstants.MacPacNamespace) + ":mBlock>", iTagStart); //GLOG 6275 - make sure closing tag follows start tag
                            }
                            else
                            {
                                iTagStart = xSegmentXML.IndexOf("<w:sdtContent", iTagStart);
                                //GLOG 7133: If Body is empty, unpaired tag may exist
                                //Just replace with default text
                                if (xSegmentXML.IndexOf("<w:sdtContent/>", iTagStart) == iTagStart)
                                {
                                    //Replace with default text
                                    //GLOG 2165: Use StartingText defined in Block
                                    iTagEnd = iTagStart + @"<w:sdtContent/>".Length;
                                    xSegmentXML = xSegmentXML.Substring(0, iTagStart) + "<w:sdtContent><w:r><w:t>" + oBlock.StartingText + "</w:t></w:r></w:sdtContent>" +
                                        xSegmentXML.Substring(iTagEnd);
                                    return xSegmentXML;
                                }
                                else
                                {
                                    iTagStart = iTagStart + @"<w:sdtContent>".Length;
                                    //GLOG 4975 (dm) - open xml
                                    iTagEnd = xSegmentXML.IndexOf("</w:sdtContent>", iTagStart);
                                    //GLOG 6345:  Block may contain other content controls, so skip any matched pairs of w:sdtContent tags
                                    //Keep searching until number of opening and closing w:sdtContent tags within substring ist the same
                                    while (iTagEnd > iTagStart && LMP.String.CountChrs(xSegmentXML.Substring(iTagStart + 1, iTagEnd - (iTagStart + 1)), "<w:sdtContent") >
                                        LMP.String.CountChrs(xSegmentXML.Substring(iTagStart + 1, iTagEnd - (iTagStart + 1)), "</w:sdtContent"))
                                    {
                                        iTagEnd = xSegmentXML.IndexOf("</w:sdtContent>", iTagEnd + 1);
                                        bCCInBody = true;
                                    }
                                    //GLOG 15757: Need to ensure XML defining table cell remains in place when 
                                    //Content Control is sdtCell type.
                                    string xContent = xSegmentXML.Substring(iTagStart, iTagEnd - iTagStart);
                                    if (xContent.IndexOf("<w:tc>") > -1 && xContent.IndexOf("<w:tr>") == -1)
                                    {
                                        int iTCStart = xContent.IndexOf("</w:tcPr>");
                                        if (iTCStart == -1)
                                        {
                                            iTCStart = xContent.IndexOf("<w:tc>") + 6;
                                        }
                                        else
                                        {
                                            iTCStart = iTCStart + 9;
                                        }
                                        int iTCEnd = xContent.IndexOf("</w:tc>", iTCStart);
                                        iTagEnd = iTagStart + iTCEnd;
                                        iTagStart = iTagStart + iTCStart;
                                        int iParaStart = xSegmentXML.IndexOf("</w:pPr>", iTagStart, iTagEnd - iTagStart);
                                        if (iParaStart > -1)
                                        {
                                            iTagStart = iParaStart + 8;
                                            xSegmentXML = xSegmentXML.Substring(0, iTagStart) + "<w:r><w:t>" + oBlock.StartingText + "</w:t></w:r></w:p>" +
                                                xSegmentXML.Substring(iTagEnd);
                                        }
                                        else
                                        {
                                            xSegmentXML = xSegmentXML.Substring(0, iTagStart) + "<w:p><w:r><w:t>" + oBlock.StartingText + "</w:t></w:r></w:p>" +
                                                xSegmentXML.Substring(iTagEnd);
                                        }
                                        return xSegmentXML;
                                    }
                                }
                            }
                            int iTextStart = -1;
                            string xExtraTags = "";
                            string xStartTags = "";

                            //GLOG 6345: Look for <w:r> instead of <w:t> -
                            //certain structures, such as TA codes, may be outside of <w:t> tags
                            iTextStart = xSegmentXML.IndexOf("<w:r>", iTagStart, iTagEnd - iTagStart);
                            if (iTextStart > -1)
                            {
                                //GLOG 7133: Make sure <w:r> tag found doesn't belong to a child segment
                                if (xSegmentXML.IndexOf("<w:sdt>", iTagStart, iTextStart - iTagStart) == -1 && 
                                    xSegmentXML.IndexOf("<ns0:m", iTagStart + 1, iTextStart - (iTagStart + 1)) == -1)
                                {
                                    //Look for Text run within Block
                                    iTextStart = iTextStart + @"<w:r>".Length;
                                    //GLOG 7133:  Changes made to deal with table at the start or end of Body block
                                    int iParaStart = xSegmentXML.IndexOf("<w:p", iTagStart);
                                    if (iParaStart > -1 && iParaStart < iTextStart)
                                    {
                                        int iBookmarkStart = xSegmentXML.IndexOf("<w:bookmarkStart", iTagStart);
                                        if (iBookmarkStart > -1 && iBookmarkStart < iParaStart)
                                            iParaStart = iBookmarkStart;
                                        xStartTags = xSegmentXML.Substring(iParaStart, iTextStart - iParaStart);
                                    }
                                }
                                else
                                {
                                    //GLOG 7133: There may be no <w:r> tag if first paragraph is empty
                                    iTextStart = xSegmentXML.IndexOf("<w:p", iTagStart, iTagEnd - iTagStart);
                                    if (iTextStart > -1)
                                    {
                                        int iParaStart = iTextStart;
                                        iTextStart = xSegmentXML.IndexOf("</w:p>", iTextStart);
                                        int iBookmarkStart = xSegmentXML.IndexOf("<w:bookmarkStart", iTagStart);
                                        if (iBookmarkStart > -1 && iBookmarkStart < iParaStart)
                                            iParaStart = iBookmarkStart;
                                        xStartTags = xSegmentXML.Substring(iParaStart, iTextStart - iParaStart) + "<w:r>";
                                    }
                                }
                            }
                            //Look for last Text closing tag within Block
                            int iTextEnd = xSegmentXML.LastIndexOf("</w:r>", iTagEnd);
                            if (iTextEnd > iTextStart)
                            {
                                //GLOG 6630: Strip extra blank Paragraphs from end of XML
                                xExtraTags = xSegmentXML.Substring(iTextEnd, iTagEnd - iTextEnd);
                                int iPara = xExtraTags.IndexOf("<w:p");
                                if (iPara > -1)
                                {
                                    int iLastPara = xExtraTags.LastIndexOf("</w:p");
                                    if (iLastPara > -1)
                                    {
                                        iLastPara = xExtraTags.IndexOf(">", iLastPara);
                                        if (iLastPara > -1)
                                        {
                                            xExtraTags = xExtraTags.Substring(0, iPara) + xExtraTags.Substring(iLastPara + 1);
                                        }
                                    }
                                }
                                //GLOG 6954 (dm) - limit GLOG 7133 changes to open xml
                                if (oSeg.ForteDocument.FileFormat == mpFileFormats.OpenXML)
                                {
                                    //GLOG 7133: Clear out leftover closing tags from child segment in a table
                                    Regex oRegex = new Regex("</w:sdtContent|</w:sdt>|</w:tbl>|</w:tr>|<w:tc.*?>|</w:tc.*?>");
                                    MatchCollection oMatches = oRegex.Matches(xExtraTags);
                                    foreach (Match oMatch in oMatches)
                                    {
                                        xExtraTags = xExtraTags.Replace(oMatch.Value, "");
                                    }
                                    //Replace with default text
                                    //GLOG 2165: Use StartingText defined in Block
                                    xSegmentXML = xSegmentXML.Substring(0, iTagStart) + xStartTags + "<w:t>" + oBlock.StartingText + "</w:t>" +
                                        xExtraTags + xSegmentXML.Substring(iTagEnd);
                                }
                                else
                                {
                                    //Replace with default text
                                    //GLOG 2165: Use StartingText defined in Block
                                    xSegmentXML = xSegmentXML.Substring(0, iTextStart) + "<w:t>" + oBlock.StartingText + "</w:t>" +
                                        xExtraTags + xSegmentXML.Substring(iTagEnd);
                                }
                            }
                        }
                        
                        break;
                    }

                }
            }

            return xSegmentXML;
        }

        /// <summary>
        /// Replace GUID TagIDs in XML with integer IDs
        /// to ensure no conflicts when pasting into document
        /// </summary>
        /// <param name="xXML"></param>
        /// <param name="oSeg"></param>
        /// <param name="bincludeChildren"></param>
        /// <returns></returns>
        private static string UpdateSegmentTagIDInXML(string xXML, Segment oSeg, bool bincludeChildren)
        {
            return UpdateSegmentTagIDInXML(xXML, oSeg, bincludeChildren, 1);
        }
        private static string UpdateSegmentTagIDInXML(string xXML, Segment oSeg, bool bIncludeChildren, int iIndex)
        {
            //Get Just last part of TagID
            string xTagID = oSeg.FullTagID;
            int iSep = xTagID.LastIndexOf(".");
            iSep = iSep + 1;
            xTagID = xTagID.Substring(iSep);
            int iGUIDStart = xTagID.LastIndexOf("{");
            if (iGUIDStart == -1)
                return xXML;
            //Replace GUID with Integer
            string xNewTagID = xTagID.Substring(0, iGUIDStart) + iIndex.ToString();
            xXML = xXML.Replace(xTagID, xNewTagID);
            if (bIncludeChildren)
            {
                for (int i = 0; i < oSeg.Segments.Count; i++)
                {
                    xXML = UpdateSegmentTagIDInXML(xXML, oSeg.Segments[i], true, i+1);  //GLOG 7935
                }
            }
            return xXML;
        }
        /// <summary>
        /// adds a pleading table item of the specified type
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oType"></param>
        /// <param name="bToExistingCollection"></param>
        public void AddCollectionTableItem(Segment oSegment, mpObjectTypes oType,
            bool bToExistingCollection)
        {
            //GLOG 3936 (dm) - added bToExistingCollection argument

            DateTime t0 = DateTime.Now;
            bool bShowValueNodes = false;

            try
            {
                //get collection type and chooser dialog caption
                mpObjectTypes oCollectionType = mpObjectTypes.CollectionTable;
                Assignments oAssignments = null;
                string xCaption = "";
                bool bHasAssignments = false; //GLOG 5998

                switch (oType)
                {
                    case mpObjectTypes.PleadingCaption:
                        //GLOG item #5675 - don't allow function to execute
                        //if there are no assigned objects of the appropriate type - dcf
                        //GLOG 5998 (dm) - collections have no direct assignments
                        if (oSegment.TypeID == mpObjectTypes.PleadingCaptions)
                            bHasAssignments = true;
                        else
                        {
                            oAssignments = new Assignments(oSegment.TypeID,
                                oSegment.ID1, mpObjectTypes.PleadingCaption);
                            bHasAssignments = (oAssignments.Count > 0);
                        }

                        if (!bHasAssignments)
                        {
                            MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_NoPleadingAssignments"), "pleading caption"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        else
                        {
                            oCollectionType = mpObjectTypes.PleadingCaptions;
                            if (bToExistingCollection)
                                xCaption = LMP.Resources.GetLangString(
                                    "Menu_DocumentEditor_AddPleadingCaption");
                            else
                                xCaption = LMP.Resources.GetLangString(
                                    "Menu_DocumentEditor_InsertPleadingCaption");
                        }
                        break;
                    case mpObjectTypes.PleadingCounsel:
                        //GLOG item #5675 - don't allow function to execute
                        //if there are no assigned objects of the appropriate type - dcf
                        //GLOG 5998 (dm) - collections have no direct assignments
                        if (oSegment.TypeID == mpObjectTypes.PleadingCounsels)
                            bHasAssignments = true;
                        else
                        {
                            oAssignments = new Assignments(oSegment.TypeID,
                                oSegment.ID1, mpObjectTypes.PleadingCounsel);
                            bHasAssignments = (oAssignments.Count > 0);
                        }

                        if (!bHasAssignments)
                        {
                            MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_NoPleadingAssignments"), "pleading counsel"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        else
                        {
                            oCollectionType = mpObjectTypes.PleadingCounsels;
                            if (bToExistingCollection)
                                xCaption = LMP.Resources.GetLangString(
                                    "Menu_DocumentEditor_AddPleadingCounsel");
                            else
                                xCaption = LMP.Resources.GetLangString(
                                    "Menu_DocumentEditor_InsertPleadingCounsel");
                        }
                        break;
                    case mpObjectTypes.PleadingSignature:
                        //GLOG item #5675 - don't allow function to execute
                        //if there are no assigned objects of the appropriate type - dcf
                        //GLOG 5998 (dm) - collections have no direct assignments
                        if (oSegment.TypeID == mpObjectTypes.PleadingSignatures)
                            bHasAssignments = true;
                        else
                        {
                            oAssignments = new Assignments(oSegment.TypeID,
                                oSegment.ID1, mpObjectTypes.PleadingSignature);
                            bHasAssignments = (oAssignments.Count > 0);
                        }

                        if (!bHasAssignments)
                        {
                            MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_NoPleadingAssignments"), "pleading signature"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        else
                        {
                            oCollectionType = mpObjectTypes.PleadingSignatures;
                            if (bToExistingCollection)
                                xCaption = LMP.Resources.GetLangString(
                                    "Menu_DocumentEditor_AddPleadingSignature");
                            else
                                xCaption = LMP.Resources.GetLangString(
                                    "Menu_DocumentEditor_InsertSignature");
                        }
                        break;
                    case mpObjectTypes.CollectionTableItem:
                        oCollectionType = mpObjectTypes.CollectionTable;
                        xCaption = LMP.Resources.GetLangString(
                            "Menu_DocumentEditor_AddCollectionTableItem");
                        break;
                    case mpObjectTypes.LetterSignature:
                        oCollectionType = mpObjectTypes.LetterSignatures;
                        if (bToExistingCollection)
                            xCaption = LMP.Resources.GetLangString(
                                "Menu_DocumentEditor_AddSignature");
                        else
                            xCaption = LMP.Resources.GetLangString(
                                "Menu_DocumentEditor_InsertSignature");
                        break;
                    case mpObjectTypes.AgreementSignature:
                        oCollectionType = mpObjectTypes.AgreementSignatures;
                        if (bToExistingCollection)
                            xCaption = LMP.Resources.GetLangString(
                                "Menu_DocumentEditor_AddSignature");
                        else
                            xCaption = LMP.Resources.GetLangString(
                                "Menu_DocumentEditor_InsertSignature");
                        break;
                    default:
                        break;
                }

                //strip ellipsis from caption
                xCaption = xCaption.Replace("...", "");

                //get collection segment
                Segment oParent = null;
                if ((oSegment.TypeID == oCollectionType) || !bToExistingCollection)
                    //collection is oSegment
                    oParent = oSegment;
                else
                {
                    //get transparent child
                    for (int i = 0; i < oSegment.Segments.Count; i++)
                    {
                        Segment oChild = oSegment.Segments[i];
                        if (oChild.TypeID == oCollectionType)
                        {
                            oParent = oChild;
                            break;
                        }
                    }
                }

                //pleading table items are assigned directly to pleadings
                Segment oTarget = oParent;
                if ((oTarget is CollectionTable) && (oTarget.Parent != null))
                    oTarget = oTarget.Parent;

                //get collection tag
                Word.XMLNode oCollectionTag = null;
                Word.ContentControl oCollectionCC = null;

                if (this.m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    oCollectionTag = oParent.PrimaryWordTag;
                else
                    oCollectionCC = oParent.PrimaryContentControl;

                //display chooser
                if (!bToExistingCollection)
                {
                    //display form without location options
                    string xSegmentID = "";
                    using (SegmentChooserForm oForm = new SegmentChooserForm(oTarget.ID1,
                        oTarget.TypeID, oType, 0, xCaption))
                    {
                        oForm.ShowDialog();
                        System.Windows.Forms.Application.DoEvents();

                        if (!oForm.Cancelled)
                        {
                            this.TaskPane.ScreenUpdating = false;
                            xSegmentID = oForm.Value.ToString();
                            if (bToExistingCollection)
                            {
                                if (oCollectionTag != null)
                                    oCollectionTag.Range.Select();
                                else
                                    oCollectionCC.Range.Select();
                            }
                        }
                        else
                        {
                            //GLOG : 8000 : ceh - set focus back to the document
                            Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                        }
                    }

                    //insert
                    if (xSegmentID != "")
                    {
                        //additional handling is required when there's no existing table
                        Segment oNewSegment = this.TaskPane.InsertSegmentInCurrentDoc(xSegmentID,
                            null, "", Segment.InsertionLocations.InsertAtSelection,
                            Segment.InsertionBehaviors.Default);

                        if (oNewSegment.HasDisplayVariables(true))
                        {
                            this.SelectFirstNodeForSegment(oNewSegment);
                        }

                        //GLOG item #6231 - dcf
                        TaskPane oTP = LMP.MacPac.TaskPanes.Item(oNewSegment.ForteDocument.WordDocument);
                        if (oTP.WordTaskPane.Visible && oTP.DataReviewerVisible)
                        {
                            oTP.DataReviewer.RefreshTree(true);
                        }

                        if (oTP != null)
                        {
                            LMP.MacPac.Application.MacPac10Ribbon.Invalidate();
                            UpdateButtons(this.ForteDocument);
                            //GLOG 6300 (dm) - the following line was preventing
                            //screen updating from getting turned back on below
                            //System.Windows.Forms.Application.DoEvents();
                        }
                    }
                }
                else
                {
                    CollectionTableForm oForm = null;
                    oForm = new CollectionTableForm(oTarget.ID1,
                        oTarget.TypeID, oType, 0, xCaption, m_oMPDocument,
                        (CollectionTable)oParent, CollectionTableForm.Modes.Add);
                    DialogResult lRet = oForm.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();
                    if (lRet == DialogResult.OK)
                    {
                        //GLOG 6396 (dm) - make sure that there are no pending
                        //variable updates before deleting collection table
                        this.ExitEditMode();

                        oForm.Finish();
                    }
                    else
                    {
                        //GLOG : 8000 : ceh - set focus back to the document
                        Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                    }
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
                LMP.Benchmarks.Print(t0);
            }
        }

        /// <summary>
        /// shows layout form for specified collection table
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oType"></param>
        public void EditCollectionTableLayout(Segment oSegment, mpObjectTypes oType)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                //get collection type and chooser dialog caption
                mpObjectTypes oCollectionType = mpObjectTypes.CollectionTable;
                Assignments oAssignments = null;
                string xCaption = "";
                bool bHasAssignments = false; //GLOG 5998

                switch (oType)
                {
                    case mpObjectTypes.PleadingCaption:
                        //GLOG item #5675 - don't allow function to execute
                        //if there are no assigned objects of the appropriate type - dcf
                        //GLOG 5998 (dm) - collections have no direct assignments
                        if (oSegment.TypeID == mpObjectTypes.PleadingCaptions)
                            bHasAssignments = true;
                        else
                        {
                            oAssignments = new Assignments(oSegment.TypeID,
                                oSegment.ID1, mpObjectTypes.PleadingCaption);
                            bHasAssignments = (oAssignments.Count > 0);
                        }

                        if (!bHasAssignments)
                        {
                            MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_NoPleadingAssignments"), "pleading caption"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        else
                        {
                            oCollectionType = mpObjectTypes.PleadingCaptions;
                            xCaption = LMP.Resources.GetLangString(
                                "Dialog_CollectionTableLayout_Captions");
                        }
                        break;
                    case mpObjectTypes.PleadingCounsel:
                        //GLOG item #5675 - don't allow function to execute
                        //if there are no assigned objects of the appropriate type - dcf
                        //GLOG 5998 (dm) - collections have no direct assignments
                        if (oSegment.TypeID == mpObjectTypes.PleadingCounsels)
                            bHasAssignments = true;
                        else
                        {
                            oAssignments = new Assignments(oSegment.TypeID,
                                oSegment.ID1, mpObjectTypes.PleadingCounsel);
                            bHasAssignments = (oAssignments.Count > 0);
                        }

                        if (!bHasAssignments)
                        {
                            MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_NoPleadingAssignments"), "pleading counsel"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        else
                        {
                            oCollectionType = mpObjectTypes.PleadingCounsels;
                            xCaption = LMP.Resources.GetLangString(
                                "Dialog_CollectionTableLayout_Counsels");
                        }
                        break;
                    case mpObjectTypes.PleadingSignature:
                        //GLOG item #5675 - don't allow function to execute
                        //if there are no assigned objects of the appropriate type - dcf
                        //GLOG 5998 (dm) - collections have no direct assignments
                        if (oSegment.TypeID == mpObjectTypes.PleadingSignatures)
                            bHasAssignments = true;
                        else
                        {
                            oAssignments = new Assignments(oSegment.TypeID,
                                oSegment.ID1, mpObjectTypes.PleadingSignature);
                            bHasAssignments = (oAssignments.Count > 0);
                        }

                        if (!bHasAssignments)
                        {
                            MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_NoPleadingAssignments"), "pleading signature"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        else
                        {
                            oCollectionType = mpObjectTypes.PleadingSignatures;
                            xCaption = LMP.Resources.GetLangString(
                                "Dialog_CollectionTableLayout_Signatures");
                        }
                        break;
                    case mpObjectTypes.CollectionTableItem:
                        oCollectionType = mpObjectTypes.CollectionTable;
                        xCaption = LMP.Resources.GetLangString(
                            "Dialog_CollectionTableLayout_CollectionTable");
                        break;
                    case mpObjectTypes.LetterSignature:
                        oCollectionType = mpObjectTypes.LetterSignatures;
                        xCaption = LMP.Resources.GetLangString(
                            "Dialog_CollectionTableLayout_Signatures");
                        break;
                    case mpObjectTypes.AgreementSignature:
                        oCollectionType = mpObjectTypes.AgreementSignatures;
                        xCaption = LMP.Resources.GetLangString(
                            "Dialog_CollectionTableLayout_Signatures");
                        break;
                    default:
                        break;
                }

                //strip ellipsis from caption
                xCaption = xCaption.Replace("...", "");

                //get collection segment
                Segment oParent = null;
                if (oSegment.TypeID == oCollectionType)
                    //collection is oSegment
                    oParent = oSegment;
                else
                {
                    //get transparent child
                    for (int i = 0; i < oSegment.Segments.Count; i++)
                    {
                        Segment oChild = oSegment.Segments[i];
                        if (oChild.TypeID == oCollectionType)
                        {
                            oParent = oChild;
                            break;
                        }
                    }
                }

                //pleading table items are assigned directly to pleadings
                Segment oTarget = oParent;
                if ((oTarget is CollectionTable) && (oTarget.Parent != null))
                    oTarget = oTarget.Parent;

                //get collection tag
                Word.XMLNode oCollectionTag = null;
                Word.ContentControl oCollectionCC = null;

                if (this.m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    oCollectionTag = oParent.PrimaryWordTag;
                else
                    oCollectionCC = oParent.PrimaryContentControl;

                //display form in layout mode
                CollectionTableForm oForm = null;
                oForm = new CollectionTableForm(oTarget.ID1,
                    oTarget.TypeID, oType, 0, xCaption, m_oMPDocument,
                    (CollectionTable)oParent, CollectionTableForm.Modes.Layout);
                DialogResult lRet = oForm.ShowDialog();
                System.Windows.Forms.Application.DoEvents();
                if (lRet == DialogResult.OK)
                {
                    //GLOG 6396 (dm) - make sure that there are no pending
                    //variable updates before deleting collection table
                    this.ExitEditMode();

                    oForm.Finish();
                }
                else
                {
                    //GLOG : 8000 : ceh - set focus back to the document
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
                LMP.Benchmarks.Print(t0);
            }
        }

        /// <summary>
        /// adds a pleading table item of the specified type
        /// </summary>
        /// <param name="oSegment"></param>
        /// <param name="oType"></param>
        /// <param name="bToExistingCollection"></param>
        public void AddNonCollectionTableSignature(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                //get collection type and chooser dialog caption
                Assignments oAssignments = null;
                string xCaption = "Add Pleading Signature";
                bool bHasAssignments = false; //GLOG 5998

                if (oSegment.TypeID == mpObjectTypes.PleadingSignatures)
                    bHasAssignments = true;
                else
                {
                    oAssignments = new Assignments(oSegment.TypeID,
                        oSegment.ID1, mpObjectTypes.PleadingSignatureNonTable);
                    bHasAssignments = (oAssignments.Count > 0);
                }

                if (!bHasAssignments)
                {
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                        "Msg_NoPleadingAssignments"), "pleading signature"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);

                    return;
                }

                //display chooser
                string xSegmentID = "";

                //display form without location options
                using (SegmentChooserForm oForm = new SegmentChooserForm(oSegment.ID1,
                    oSegment.TypeID, mpObjectTypes.PleadingSignatureNonTable, 0, xCaption))
                {
                    oForm.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();

                    if (!oForm.Cancelled)
                    {
                        this.TaskPane.ScreenUpdating = false;
                        xSegmentID = oForm.Value.ToString();

                        //select end of last existing signature
                        Segment[] aSignatures = oSegment.FindChildren(mpObjectTypes.PleadingSignatureNonTable);

                        //get range of last signature
                        Word.Range oSegRng = aSignatures[aSignatures.Length - 1].PrimaryRange;

                        //selec the end of the signature
                        object oDirection = Word.WdCollapseDirection.wdCollapseEnd;
                        oSegRng.Collapse(ref oDirection);

                        object oUnit = Word.WdUnits.wdCharacter;
                        object oCount = 1;

                        oSegRng.Move(ref oUnit, ref oCount);

                        //insert a paragraph
                        oSegRng.InsertParagraphAfter();

                        oSegRng.Collapse(ref oDirection);
                        oSegRng.Select();
                    }
                }

                //insert
                if (xSegmentID != "")
                {
                    Segment oNewSegment = null;

                    //additional handling is required when there's no existing table
                    oNewSegment = this.TaskPane.InsertSegmentInCurrentDoc(xSegmentID,
                        null, "", Segment.InsertionLocations.InsertAtSelection,
                        Segment.InsertionBehaviors.Default);

                    if (oNewSegment.HasDisplayVariables(true))
                    {
                        this.SelectFirstNodeForSegment(oNewSegment);
                    }
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
                LMP.Benchmarks.Print(t0);
            }
        }

        internal void EditCollectionTableItem(Segment oParent, mpObjectTypes iType)
        {
            try
            {
                Segment[] aChildren = null;

                string xTypeDisplayName = LMP.Data.Application.GetObjectTypeDisplayName(iType);

                //check if segment of specified type is selected
                Segment oItem = oParent.ForteDocument.GetSegmentFromSelection(iType);

                //count items of specified type
                aChildren = oParent.FindChildren(iType);

                if (oItem == null)
                {
                    //if only one item, edit this one
                    if (aChildren.Length == 0)
                    {
                        MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                            "Msg_CouldNotEditChildNoChildOfTypeFound"), xTypeDisplayName),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                    }
                    else if (aChildren.Length == 1)
                    {
                        oItem = aChildren[0];

                        if (iType == mpObjectTypes.PleadingCaption)
                        {
                            DialogResult iRes = MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_CantChangePrimaryCaption"), xTypeDisplayName.ToLower()),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        else if (iType == mpObjectTypes.PleadingCounsel)
                        {
                            DialogResult iRes = MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_CantChangePrimaryCounsel"), xTypeDisplayName.ToLower()),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                    }
                    else if (aChildren.Length > 1)
                    {
                        switch (iType)
                        {
                            case mpObjectTypes.PleadingCaption:
                                {
                                    DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString(
                                        "Msg_MultipleCaptionsFound"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Exclamation);

                                    if (iRes == DialogResult.Yes)
                                        oItem = aChildren[1];
                                    else
                                        return;

                                    break;
                                }
                            case mpObjectTypes.PleadingCounsel:
                                {
                                    DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString(
                                        "Msg_MultipleCounselBlocksFound"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Exclamation);

                                    if (iRes == DialogResult.Yes)
                                        oItem = aChildren[1];
                                    else
                                        return;

                                    break;
                                }
                            case mpObjectTypes.PleadingSignature:
                            case mpObjectTypes.LetterSignature:
                            case mpObjectTypes.AgreementSignature:
                                {
                                    DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString(
                                        "Msg_MultipleSignaturesFound"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Exclamation);

                                    if (iRes == DialogResult.Yes)
                                        oItem = aChildren[0];
                                    else
                                        return;

                                    break;
                                }
                            default:
                                {
                                    DialogResult iRes = MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                        "Msg_MultipleChildrenOfTypeFound"), xTypeDisplayName.ToLower()),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Exclamation);

                                    if (iRes == DialogResult.Yes)
                                        oItem = aChildren[0];
                                    else
                                        return;

                                    break;
                                }
                        }
                    }
                }

                if (oItem != null)
                {
                    if (iType == mpObjectTypes.PleadingCaption && oItem == aChildren[0])
                    {
                        if (aChildren.Length == 1)
                        {
                            DialogResult iRes = MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_CantChangePrimaryCaption"), xTypeDisplayName.ToLower()),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        {
                            DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString(
                            "Msg_EditFirstCrossaction"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Exclamation);

                            if (iRes == DialogResult.Yes)
                                oItem = aChildren[1];
                            else
                                return;
                        }
                    }
                    else if (iType == mpObjectTypes.PleadingCounsel && oItem == aChildren[0])
                    {
                        if (aChildren.Length == 1)
                        {
                            DialogResult iRes = MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                                "Msg_CantChangePrimaryCounsel"), xTypeDisplayName.ToLower()),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                        else
                        {
                            DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString(
                                "Msg_EditFirstCoCounsel"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Exclamation);

                            if (iRes == DialogResult.Yes)
                                oItem = aChildren[1];
                            else
                                return;
                        }
                    }

                    string xSegmentID = null;

                    //GLOG : 6211 : CEH
                    //pleading table items are assigned directly to pleadings
                    Segment oTarget = oParent;
                    if ((oTarget is CollectionTable) && (oTarget.Parent != null))
                        oTarget = oTarget.Parent;

                    //display chooser form
                    using (SegmentChooserForm oForm = new SegmentChooserForm(oParent.ID1,
                        oParent.TypeID, iType, oItem.ID1, string.Format(
                        LMP.Resources.GetLangString("Dlg_ChangeType"), xTypeDisplayName)))
                    {
                        oForm.ShowDialog();
                        System.Windows.Forms.Application.DoEvents();

                        if (!oForm.Cancelled)
                        {
                            this.TaskPane.ScreenUpdating = false;
                            xSegmentID = oForm.Value.ToString();

                            //GLOG item #6124 - dcf
                            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                                oTarget.ForteDocument.WordDocument);
                            oEnv.SaveState();

                            this.SelectFirstNodeForSegment(oItem, this.TaskPane.WordTaskPane.Visible);
                            //Remove any visible control
                            this.ExitEditMode();
                            Segment oNewSegment = this.ReplaceSegment(oItem, int.Parse(xSegmentID));

                            oEnv.RestoreState(true, true, false);

                            //GLOG item #6231 - dcf
                            TaskPane oTP = LMP.MacPac.TaskPanes.Item(oNewSegment.ForteDocument.WordDocument);
                            if (oTP.WordTaskPane.Visible && oTP.DataReviewerVisible)
                            {
                                oTP.DataReviewer.RefreshTree(true);
                            }
                        }
                    }
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
				//GLOG : 8047 : ceh
				//GLOG : 8096 : ceh
                this.TaskPane.Refresh(false, true, false);
            }
        }

        internal void ChangeNonTableSignatureType(Segment oParent)
        {
            try
            {
                string xTypeDisplayName = "Pleading Signature";

                //check if segment of specified type is selected
                Segment oItem = oParent.ForteDocument.GetSegmentFromSelection(mpObjectTypes.PleadingSignatureNonTable);

                //count items of specified type
                Segment[] aChildren = oParent.FindChildren(mpObjectTypes.PleadingSignatureNonTable);

                if (oItem == null)
                {
                    //if only one item, edit this one, else prompt appropriately
                    if (aChildren.Length == 0)
                    {
                        MessageBox.Show(string.Format(LMP.Resources.GetLangString(
                            "Msg_CouldNotEditChildNoChildOfTypeFound"), xTypeDisplayName),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                    }
                    else if (aChildren.Length == 1)
                    {
                        oItem = aChildren[0];
                    }
                    else if (aChildren.Length > 1)
                    {
                        DialogResult iRes = MessageBox.Show(LMP.Resources.GetLangString(
                            "Msg_MultipleSignaturesFound"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Exclamation);

                        if (iRes == DialogResult.Yes)
                            oItem = aChildren[0];
                        else
                            return;
                    }
                }

                if (oItem != null)
                {
                    string xSegmentID = null;

                    //display chooser form
                    using (SegmentChooserForm oForm = new SegmentChooserForm(oParent.ID1,
                        oParent.TypeID, mpObjectTypes.PleadingSignatureNonTable, oItem.ID1, string.Format(
                        LMP.Resources.GetLangString("Dlg_ChangeType"), xTypeDisplayName)))
                    {
                        oForm.ShowDialog();
                        System.Windows.Forms.Application.DoEvents();

                        if (!oForm.Cancelled)
                        {
                            this.TaskPane.ScreenUpdating = false;
                            xSegmentID = oForm.Value.ToString();

                            //GLOG item #6124 - dcf
                            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                                oParent.ForteDocument.WordDocument);
                            oEnv.SaveState();

                            this.SelectFirstNodeForSegment(oItem, this.TaskPane.WordTaskPane.Visible);

                            Segment oNewSegment = this.ReplaceSegment(oItem, int.Parse(xSegmentID));

                            oEnv.RestoreState(true, true, false);
                        }
                    }
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
            }
        }
        private void ModifyCollectionLayout(Segment oSeg)
        {
            Segment[,] aLayout = ((CollectionTable)oSeg).GetLayout();
            DataTable oDT = new DataTable();

            for (int i = 0; i < aLayout.Length; i++)
            {
                if (i + 1 < aLayout.Length)
                {
                    //add entire row
                }
            }
        }

        /// <summary>
        /// displays chooser for specified parent and type and inserts selected
        /// segment immediately below last child of type
        /// </summary>
        /// <param name="oParent"></param>
        /// <param name="oType"></param>
        /// <param name="bAfterLastExisting"></param>
        private void AddNonCollectionChildSegment(Segment oParent, mpObjectTypes oType,
            bool bAfterLastExisting)
        {
            //GLOG 3936 (dm) - added bAfterLastExisting argument
            DateTime t0 = DateTime.Now;

            try
            {
                //get chooser dialog caption
                string xCaption = "";
                switch (oType)
                {
                    case mpObjectTypes.PleadingSignatureNonTable:
                        if (bAfterLastExisting)
                            xCaption = LMP.Resources.GetLangString(
                                "Menu_DocumentEditor_AddPleadingSignature");
                        else
                            xCaption = LMP.Resources.GetLangString(
                                "Menu_DocumentEditor_InsertSignature");
                        break;
                    default:
                        //no support for other types
                        return;
                }

                //strip ellipsis from caption
                xCaption = xCaption.Replace("...", "");

                //display chooser
                string xSegmentID = "";
                using (SegmentChooserForm oForm = new SegmentChooserForm(oParent.ID1,
                    oParent.TypeID, oType, 0, xCaption))
                {
                    oForm.ShowDialog();
                    System.Windows.Forms.Application.DoEvents();

                    if (!oForm.Cancelled)
                        xSegmentID = oForm.Value.ToString();
                }

                //insert
                if (xSegmentID != "")
                {
                    Segment oNewSegment = null;
                    this.TaskPane.ScreenUpdating = false;

                    if (bAfterLastExisting)
                    {
                        //get last existing sibling of type
                        Segment oTarget = null;
                        for (int i = 0; i < oParent.Segments.Count; i++)
                        {
                            Segment oSibling = oParent.Segments[i];
                            if (oSibling.TypeID == oType)
                                oTarget = oSibling;
                        }

                        //insert below target - add normal paragraph between
                        Word.Range oRange = oTarget.PrimaryRange;

                        object oMissing = System.Reflection.Missing.Value;
                        oRange.EndOf(ref oMissing, ref oMissing);
                        oRange.Move(ref oMissing, ref oMissing);
                        oRange.InsertParagraphAfter();
                        object oNormalStyleID = Word.WdBuiltinStyle.wdStyleNormal;
                        Word.Style oNormalStyle = m_oMPDocument.WordDocument.Styles.get_Item(
                            ref oNormalStyleID);
                        object oNormal = (object)oNormalStyle;
                        oRange.set_Style(ref oNormal);
                        oRange.EndOf(ref oMissing, ref oMissing);
                        oRange.Select();

                        oNewSegment = m_oMPDocument.InsertSegment(xSegmentID,
                            oParent, Segment.InsertionLocations.InsertAtSelection,
                            Segment.InsertionBehaviors.Default, false, null);

                        //this is the only way to reorder the collection to reflect
                        //the new segment's position in the document
                        this.RefreshTree();
                    }
                    else
                    {
                        //additional handling is required when there's no target
                        oNewSegment = this.TaskPane.InsertSegmentInCurrentDoc(xSegmentID,
                            null, "", Segment.InsertionLocations.InsertAtSelection,
                            Segment.InsertionBehaviors.Default);
                    }

                    this.SelectFirstNodeForSegment(oNewSegment);
                }
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
                LMP.Benchmarks.Print(t0);
            }
        }

        /// <summary>
        /// returns true if the node for a same-type sibling
        /// of the specified segment is transparent
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private bool SiblingIsTransparent(Segment oSegment)
        {
            if (!oSegment.IsTopLevel)
            {
                Segments oSiblings = oSegment.Parent.Segments;
                for (int i = 0; i < oSiblings.Count; i++)
                {
                    Segment oSibling = oSiblings[i];
                    if ((oSibling.TypeID == oSegment.TypeID) &&
                        (oSibling.FullTagID != oSegment.FullTagID) &&
                        this.NodeIsTransparent(oSibling.FullTagID))
                        return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Display callout above and to left of selected range
        /// </summary>
        /// <param name="xText"></param>
        /// <param name="oRng"></param>
        private void ShowCallout(string xText, Word.Range oRng)
        {
            DateTime t0 = DateTime.Now;

            //show location tooltip popup if specified
            if (!Session.CurrentUser.UserSettings.ShowLocationTooltips)
                return;

            try
            {
                if (m_bUseWindowsCallout)
                {
                    int iPixelsLeft = 0;
                    int iPixelsTop = 0;
                    int iPixelsWidth = 0;
                    int iPixelsHeight = 0;
                    if (m_oCallout == null)
                    {
                        m_oCallout = new CalloutForm();
                    }
                    m_oCallout.Visible = false;
                    m_oCallout.Text = xText;

                    //Get screen coordinates of Word Range
                    oRng.Document.ActiveWindow.GetPoint(out iPixelsLeft, out iPixelsTop,
                        out iPixelsWidth, out iPixelsHeight, oRng);

                    //get call out position offset based on space before of range and font size
                    object oTrue = true;
                    int iPixelsTopOffset = System.Convert.ToInt32(
                        oRng.Application.PointsToPixels(oRng.Paragraphs.First.SpaceBefore, ref oTrue) + 2);

                    m_oCallout.Left = iPixelsLeft - m_oCallout.Width;
                    m_oCallout.Top = iPixelsTop - m_oCallout.Height;
                    m_oCallout.Top = iPixelsTop - m_oCallout.Height + iPixelsTopOffset;
                    m_oCallout.Show(this);
                    m_oCallout.Refresh();
                }
                else
                {
                    LMP.Forte.MSWord.WordDoc.DisplayCallout(oRng, xText);
                }
            }
            catch{ }

            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// Hide Callout if displayed
        /// </summary>
        internal void HideCallout()
        {
            if (m_bUseWindowsCallout)
            {
                if (m_oCallout != null)
                    m_oCallout.Hide();
            }
            else
                this.m_oMPDocument.ClearCallout();
        }
        /// <summary>
        /// Hides callout form if mouse has moved off of TaskPane or Word application is no longer active
        /// </summary>
        internal void ShowHideCalloutIfNecessary()
        {
            if (m_oCallout == null)
                return;

            //Leave callout on screen while CI Dialog is displayed
            if (m_bCIIsActive)
                return;

            Point oPt = Cursor.Position;
            if (!this.ClientRectangle.Contains(this.PointToClient(oPt)) && m_oCallout != null && m_oCallout.Visible)
                HideCallout();
            else if (this.ClientRectangle.Contains(this.PointToClient(oPt)) && m_oCallout != null && !m_oCallout.Visible)
            {
                //Redisplay callout if mouse moved off and back over tree
                if (m_oCurNode != null && m_oCurNode.Tag is Variable)
                {
                    Variable oVar = m_oCurNode.Tag as Variable;
                    if (oVar != null)
                    {
                        Word.Range oRng = null;
                        try
                        {
                            if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                            {
                                oRng = oVar.AssociatedWordTags[0].Range;
                            }
                            else
                            {
                                oRng = oVar.AssociatedContentControls[0].Range;
                            }
                        }
                        catch { }
                        if (oRng != null)
                            this.ShowCallout(oVar.DisplayName,
                                oRng);
                    }
                }
            }
        }
        /// <summary>
        /// selects the first body tag in the first segment
        /// after the specified node
        /// </summary>
        /// <param name="oStartingNode"></param>
        private void SelectBodyNode(UltraTreeNode oStartingNode)
        {
            Segment oSeg = null;
            UltraTreeNode oSegmentNode = null;

            if (oStartingNode != null)
            {
                oSegmentNode = oStartingNode;
                //GLOG 2648 - if current segment doesn't have body block, also check parent
                while (!(oSegmentNode.Tag is Segment) || !((Segment)oSegmentNode.Tag).HasBody())
                {
                    if (oSegmentNode.Level > 0)
                        oSegmentNode = oSegmentNode.Parent;
                    else
                        break;
                }
                if (oSegmentNode.Tag is Segment && ((Segment)oSegmentNode.Tag).HasBody())
                    oSeg = (Segment)oSegmentNode.Tag;
            }
            //If body not found in current branch, find first top-level segment
            //containing a body block
            if (oSeg == null)
            {
                foreach (UltraTreeNode oSegNode in this.treeDocContents.Nodes)
                {
                    if (oSegNode.Tag is Segment && ((Segment)oSegNode.Tag).HasBody())
                    {
                        oSeg = (Segment)oSegNode.Tag;
                        break;
                    }
                }
            }
            if (oSeg != null)
            {
                for (int i = 0; i < oSeg.Blocks.Count; i++)
                {
                    Block oBlock = oSeg.Blocks[i];
                    if (oBlock.IsBody)
                    {
                        //GLOG item #7111 - dcf
                        //select body node if one is shown
                        if (oBlock.ShowInTree)
                        {
                            SelectNode(this.GetNodeFromTagID(oBlock.TagID));
                        }
                        else
                        {
                            //GLOG 7111: Select Body Block if not displayed in TaskPane
                            if (m_oCurNode != null)
                                this.ExitEditMode();

                            //select range associated with 
                            //variable if there is one
                            m_bProgrammaticallySelectingWordTag = true;
                            try
                            {
                                if (m_oMPDocument.FileFormat ==
                                    LMP.Data.mpFileFormats.Binary)
                                {
                                    //xml tag
                                    Word.XMLNode oWordTag = oBlock.AssociatedWordTag;
                                    if (oWordTag != null)
                                    {
                                        //select tag
                                        LMP.Forte.MSWord.WordDoc.SelectTag(oWordTag, false, true);
                                        //GLOG 2165: If Body text is different from default text,
                                        //only select start of text
                                        if (oBlock.IsBody)
                                        {
                                            if (Session.CurrentWordApp.Selection.Text !=
                                                Expression.Evaluate(oBlock.StartingText,
                                                oBlock.Segment, oBlock.ForteDocument))
                                            {
                                                object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                                                Session.CurrentWordApp.Selection.Collapse(ref oDirection);
                                            }
                                        }
                                        m_bProgrammaticallySelectingWordTag = false;
                                    }
                                }
                                else
                                {
                                    //content control
                                    Word.ContentControl oCC = oBlock.AssociatedContentControl;

                                    if (oCC != null)
                                    {
                                        //select tag
                                        this.SelectContentControl(oCC, false, true);
                                        //GLOG 2165: If Body text is different from default text,
                                        //only select start of text
                                        if (oBlock.IsBody)
                                        {
                                            if (Session.CurrentWordApp.Selection.Text !=
                                                Expression.Evaluate(oBlock.StartingText,
                                                oBlock.Segment, oBlock.ForteDocument))
                                            {
                                                object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                                                Session.CurrentWordApp.Selection.Collapse(ref oDirection);
                                            }
                                        }
                                    }
                                }

                            }
                            finally
                            {
                                m_bProgrammaticallySelectingWordTag = false;
                            }
                            //GLOG 6066:  Clear m_oCurNode, so that previously selected
                            //TaskPane control will be displayed again if reselected
                            m_oCurNode = null;
                            //Switch focus to Block in Document
                            Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                        }
                        break;
                    }
                }
            }
            else
            {
                // Glog 2671 : Select the message part of the content since there is no body portion to this segment.
                if (oSegmentNode != null)
                {
                    for (int i = 0; i < oSegmentNode.Nodes.Count; i++)
                    {
                        if (oSegmentNode.Nodes[i].Tag is LMP.Architect.Api.AdminVariable)
                        {
                            if (((LMP.Architect.Api.AdminVariable)oSegmentNode.Nodes[i].Tag).Name == "Message")
                            {
                                SelectNode(oSegmentNode.Nodes[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// deletes the hotkey for oNode
        /// </summary>
        /// <param name="oNode"></param>
        private void DeleteHotkey(UltraTreeNode oNode)
        {
            //get hotkey for node
            string xHotkey = "";
            if (oNode.Tag is LMP.Architect.Api.Authors)
                xHotkey = "U";
            else if (oNode.Tag is Jurisdictions)
                xHotkey = "J";
            else if (oNode.Key.EndsWith(".Language"))
                xHotkey = "L";
            else
            {
                if (oNode.Tag is Variable)
                    xHotkey = ((Variable)oNode.Tag).Hotkey;
                else
                    xHotkey = ((Block)oNode.Tag).Hotkey;
            }

            //delete hotkey
            if (xHotkey != "")
            {
                if (this.m_oHotkeys[xHotkey] is UltraTreeNode)
                    //hash key is assigned a node - delete key
                    m_oHotkeys.Remove(xHotkey);
                else
                {
                    //hash key is assigned an array list
                    ArrayList oHotkeyNodesArray = (ArrayList)this.m_oHotkeys[xHotkey];
                    if (oHotkeyNodesArray.Count == 1)
                        //only one item on list - delete key
                        m_oHotkeys.Remove(xHotkey);
                    else
                    {
                        //find item to delete
                        for (int i = 0; i < oHotkeyNodesArray.Count; i++)
                        {
                            UltraTreeNode oHotkeyNode = (UltraTreeNode)oHotkeyNodesArray[i];
                            if (oHotkeyNode == oNode)
                                oHotkeyNodesArray.Remove(oHotkeyNode);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// when the user manually clears out an inline mVar, we don't want to do
        /// anything until we're satisfied that they're done with the edit, as they
        /// may be planning to type a new value into the tag - this method handles
        /// all pending emptied tags, by either deleting the tag or updating the
        /// variable value
        /// </summary>
        private void ResolveEmptyTags()
        {
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.OpenXML)
                ResolveEmptyTags_CC();
            else
                ResolveEmptyTags_XMLTag();
        }
        private void ResolveEmptyTags_XMLTag()
        {
            //cycle through unresolved tags
            IEnumerator oEnumerator = this.m_oUnresolvedEmptyTags.Values.GetEnumerator();

            bool bNextExists = oEnumerator.MoveNext();
            while (bNextExists)
            {
                Word.XMLNode oTag = (Word.XMLNode)oEnumerator.Current;

                //get containing mSEG - keep checking parent node until an mSEG is found
                Word.XMLNode oSegTag = null;
                bool bTagExists = true;
                try
                {
                    oSegTag = oTag.ParentNode;
                }
                catch
                {
                    //the referenced tag is no longer in the document - this may
                    //be because its scope has been deleted in the interim or because
                    //this method failed to complete on a previous attempt
                    bTagExists = false;
                }

                if (bTagExists)
                {
                    while ((oSegTag != null) && (oSegTag.BaseName != "mSEG"))
                        oSegTag = oSegTag.ParentNode;

                    //get segment
                    Segment oSegment = null;
                    if (oSegTag != null)
                    {
                        string xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID(oSegTag);
                        if (!System.String.IsNullOrEmpty(xSegFullTagID))
                            oSegment = this.m_oMPDocument.FindSegment(xSegFullTagID);
                    }

                    if (oSegment == null)
                        return;

                    //get variable
                    Variables oVariables = oSegment.Variables;

                    //get the variable whose selection tag is the currently selected node
                    Variable oVar = oVariables.ItemFromAssociatedTag(oTag);

                    if (oVar != null)
                    {
                        //we delete the tag if it's still empty
                        string xValue = oVar.GetValueFromSource(oTag);
                        if (System.String.IsNullOrEmpty(xValue))
                        {
                            //delete tag
                            if (oVar.AssociatedWordTags.Length == 1)
                                //no othere associated tags - delete variable
                                oVariables.Delete(oVar);
                            else
                            {
                                //delete Tag
                                m_oMPDocument.Tags.Delete(oTag);

                                //delete mSubVars
                                Word.XMLNodes oSubVars = oTag.SelectNodes("x:mSubVar",
                                    "xmlns:x='urn-legalmacpac-data/10'", true);
                                for (int i = oSubVars.Count; i > 0; i--)
                                    oSubVars[i].Delete();

                                //delete tag
                                oTag.Delete();

                                if (oSegment != null)
                                    //refresh segment node store
                                    oSegment.RefreshNodes();
                                else
                                    //refresh standalone node store
                                    m_oMPDocument.RefreshOrphanNodes();
                            }
                        }
                        else
                            //update variable value
                            this.UpdateVariableValue(oTag);
                    }
                }
                bNextExists = oEnumerator.MoveNext();
            }

            m_oUnresolvedEmptyTags.Clear();
        }
        private void ResolveEmptyTags_CC()
        {
            //cycle through unresolved tags
            IEnumerator oEnumerator = this.m_oUnresolvedEmptyTags.Values.GetEnumerator();

            bool bNextExists = oEnumerator.MoveNext();
            while (bNextExists)
            {
                Word.ContentControl oCC = (Word.ContentControl)oEnumerator.Current;

                //get containing mSEG - keep checking parent node until an mSEG is found
                Word.ContentControl oSegCC = null;
                bool bTagExists = true;
                try
                {
                    oSegCC = oCC.ParentContentControl;
                }
                catch
                {
                    //the referenced tag is no longer in the document - this may
                    //be because its scope has been deleted in the interim or because
                    //this method failed to complete on a previous attempt
                    bTagExists = false;
                }

                if (bTagExists)
                {
                    while ((oSegCC != null) && (String.GetBoundingObjectBaseName(oSegCC.Tag) != "mSEG"))
                        oSegCC = oSegCC.ParentContentControl;

                    //get segment
                    Segment oSegment = null;
                    if (oSegCC != null)
                    {
                        string xSegFullTagID = LMP.Forte.MSWord.WordDoc.GetFullTagID_CC(oSegCC);
                        if (!System.String.IsNullOrEmpty(xSegFullTagID))
                            oSegment = this.m_oMPDocument.FindSegment(xSegFullTagID);
                    }

                    if (oSegment == null)
                        return;

                    //get variable
                    Variables oVariables = null;

                    oVariables = oSegment.Variables;

                    //get the variable whose selection tag is the currently selected node
                    Variable oVar = oVariables.ItemFromAssociatedContentControl(oCC);

                    if (oVar != null)
                    {
                        //we delete the tag if it's still empty
                        string xValue = oVar.GetValueFromSource_CC(oCC);
                        if (System.String.IsNullOrEmpty(xValue))
                        {
                            //delete CC
                            if (oVar.AssociatedContentControls.Length == 1)
                                //no othere associated tags - delete variable
                                oVariables.Delete(oVar);
                            else
                            {
                                //delete CC
                                m_oMPDocument.Tags.Delete_CC(oCC);

                                //delete mSubVars
                                foreach (Word.ContentControl oChildCC in oCC.Range.ContentControls)
                                {
                                    if (String.GetBoundingObjectBaseName(oChildCC.Tag) == "mSubVar")
                                    {
                                        oChildCC.Delete(false);
                                    }
                                }

                                //delete cc
                                oCC.Delete(false);

                                if (oSegment != null)
                                    //refresh segment node store
                                    oSegment.RefreshNodes();
                                else
                                    //refresh standalone node store
                                    m_oMPDocument.RefreshOrphanNodes();
                            }
                        }
                        else
                            //update variable value
                            this.UpdateVariableValue(oCC);
                    }
                }
                bNextExists = oEnumerator.MoveNext();
            }

            m_oUnresolvedEmptyTags.Clear();
        }
        #region *********************OxmlMethods*********************
#if Oxml
        private void RefreshSegmentNode_Oxml(UltraTreeNode oSegmentNode, bool bForceRefresh, LMP.Architect.Oxml.XmlSegment oXmlSegment)
        {
            m_bIsRefreshingSegmentNode = true;

            try
            {
                DateTime t0 = DateTime.Now;
                UltraTreeNode oMoreNode = null;

                Segment oSegment = null;

                //exit if node is the same
                if (oSegmentNode == this.m_oCurNode && !bForceRefresh)
                    return;

                //get referred segment
                oSegment = oSegmentNode.Tag as Segment;

                if (oSegment == null)
                    throw new LMP.Exceptions.UINodeException(
                        LMP.Resources.GetLangString("Error_NodeIsNotSegmentNode"));

                if (oSegment.TagID != oXmlSegment.TagID)
                {
                    //If node doesn't correspond to last inserted Oxml Segment, use API method
                    RefreshSegmentNode_Api(oSegmentNode, bForceRefresh);
                    return;
                }
                //clear any existing child nodes
                oSegmentNode.Nodes.Clear();
                //clear controls hash table
                m_oControlsHash.Clear();

                ////stream temp file Oxml into WpDocument -
                ////this is identical to the Oxml of newly inserted segment -
                ////we'll populate doc editor using the WpDocument
                //byte[] aBytes = File.ReadAllBytes(xTempFileName);
                //MemoryStream oMS = new MemoryStream();
                //oMS.Write(aBytes, 0, aBytes.Length);
                //WordprocessingDocument oWPDoc = WordprocessingDocument.Open(oMS, true);



                //8-18-11 (dm) - add author, court, and language nodes only if
                //these have variables to target
                //10-12-11 (dm) - condition changed to HasSnapshot - the
                //previous condition didn't account for the reintroduction
                //of variables when a child is added or replaced, resulting
                //in the inappropriate reappearance of these nodes - once
                //a segment has been finished, the language and court must be
                //fixed, and the author subject to change only where
                //children have been configured with their own control
                if (!Snapshot.Exists(oSegment))
                {
                    //Display Jurisdiction control for appropriate types
                    if (oXmlSegment.IsTopLevel && oXmlSegment.ShowCourtChooser)
                    {
                        string xKey = oXmlSegment.FullTagID + ".Jurisdiction";

                        //get appropriate display name for node
                        string xDisplayName = oXmlSegment.CourtChooserUILabel;
                        if (string.IsNullOrEmpty(xDisplayName))
                            xDisplayName = LMP.Resources.GetLangString("Lbl_Court") + ":";

                        //add Jurisdiction node
                        UltraTreeNode oJurisdictionNode = oSegmentNode.Nodes.Add(xKey, xDisplayName);


                        //initialize new node
                        InitializeJurisdictionNode(oXmlSegment, oJurisdictionNode);

                        if (!oSegment.Activated)
                            oJurisdictionNode.Enabled = false;
                    }

                    if ((oXmlSegment.IsTopLevel || (!oXmlSegment.LinkAuthorsToParent))
                        && (oXmlSegment.MaxAuthors > 0))
                    {
                        //get authors node key
                        string xKey = oXmlSegment.FullTagID + ".Authors";

                        //get appropriate display name for node
                        string xDisplayName = "";
                        //GLOG 6653
                        if (oXmlSegment.AuthorsNodeUILabel == "Author" || oXmlSegment.AuthorsNodeUILabel == "")
                        {
                            if (oXmlSegment.MaxAuthors == 1)
                                xDisplayName = LMP.Resources.GetLangString("Msg_Author");
                            else
                                xDisplayName = LMP.Resources.GetLangString("Msg_Authors");
                        }
                        else if (oXmlSegment.AuthorsNodeUILabel == "Attorney")

                            if (oXmlSegment.MaxAuthors == 1)
                                xDisplayName = LMP.Resources.GetLangString("Msg_Attorney");
                            else
                                xDisplayName = LMP.Resources.GetLangString("Msg_Attorneys");
                        else
                        {
                            xDisplayName = oXmlSegment.AuthorsNodeUILabel;
                        }

                        //add authors node
                        UltraTreeNode oAuthorsNode = oSegmentNode.Nodes.Add(xKey, xDisplayName);

                        //initialize new node
                        InitializeNode(oSegment.Authors, oAuthorsNode);

                        if (!oSegment.Activated)
                            oAuthorsNode.Enabled = false;
                    }

                    //add language node
                    if (oXmlSegment.IsTopLevel && oXmlSegment.SupportsMultipleLanguages)
                    {
                        string xKey = oSegment.FullTagID + ".Language";
                        string xDisplayName = LMP.Resources.GetLangString("Lbl_Language") + ":";
                        UltraTreeNode oLanguageNode = oSegmentNode.Nodes.Add(xKey, xDisplayName);
                        InitializeLanguageNode(oXmlSegment, oLanguageNode);

                        if (!oSegment.Activated)
                            oLanguageNode.Enabled = false;
                    }
                }

                //cycle through segment variables, adding each to tree
                for (int i = 0; i < oSegment.Variables.Count; i++)
                {
                    //get variable
                    Variable oVar = oSegment.Variables[i];
                    LMP.Architect.Oxml.XmlVariable oXmlVar = oXmlSegment.Variables[oVar.ID];
                    //GLOG 8816
                    oVar.SetValue(oXmlVar.Value, false, false, true);
                    //add only variables that should be shown in the editor
                    if ((oXmlVar.DisplayIn & Variable.ControlHosts.DocumentEditor) !=
                        Variable.ControlHosts.DocumentEditor)
                        continue;

                    string xKey = oXmlSegment.FullTagID + "." + oXmlVar.ID;

                    string xDisplayName = String.RestoreXMLChars(oXmlVar.DisplayName, true); //GLOG 8463
                    
                    UltraTreeNode oTreeNode = null;

                    //add to tree
                    if (oXmlVar.DisplayLevel == Variable.Levels.Basic)
                        if (oMoreNode == null)
                            //add as last node
                            oTreeNode = oSegmentNode.Nodes.Add(
                                xKey, xDisplayName + ":");
                        else
                            //add as last node in basic section
                            oTreeNode = oSegmentNode.Nodes.Insert(oMoreNode.Index,
                                xKey, xDisplayName + ":");
                    else
                    {
                        //add "More" node
                        if (oMoreNode == null)
                            oMoreNode = CreateMoreNode(oSegmentNode, oSegment);

                        //add as last node in "More" category
                        oTreeNode = oMoreNode.Nodes.Add(
                            xKey, xDisplayName + ":");
                    }
                    //initialize new node
                    InitializeNode_Oxml(oVar, oXmlVar, oTreeNode, false);

                    if (!oSegment.Activated)
                        oTreeNode.Enabled = false;
                }

                //TODO: rewrite for Oxml
                if (oSegment.Activated)
                {
                    //cycle through segment variables, executing the
                    //ValueChanged actions - this sets up the nodes
                    //based on the initial variable values
                    for (int i = 0; i < oSegment.Variables.Count; i++)
                        ExecuteControlActions(oSegment.Variables[i],
                            ControlActions.Events.ValueChanged);

                    //execute the value changed control 
                    //actions assigned to the author
                    ExecuteControlActions(oSegment.Authors,
                        ControlActions.Events.ValueChanged);
                }

                //cycle through segment blocks, adding each to tree
                for (int i = 0; i < oSegment.Blocks.Count; i++)
                {
                    
                    //get block
                    Block oBlock = oSegment.Blocks[i];
                    LMP.Architect.Oxml.XmlBlock oXmlBlock = oXmlSegment.Blocks.ItemFromName(oBlock.Name);

                    UltraTreeNode oTreeNode;
                    //add to tree

                    if (oXmlBlock.ShowInTree)
                    {
                        string xKey = oXmlBlock.TagID;

                        if (oBlock.DisplayLevel == Variable.Levels.Basic)
                        {
                            // This segment is configured to be in the basic section.

                            if (oMoreNode == null)
                                //add as last node
                                oTreeNode = oSegmentNode.Nodes.Add(
                                    xKey, oXmlBlock.DisplayName + ":");
                            else
                                //add as last node in basic section
                                oTreeNode = oSegmentNode.Nodes.Insert(oMoreNode.Index,
                                    xKey, oXmlBlock.DisplayName + ":");
                        }
                        else
                        {
                            // This segment is configured to be in the advanced section.
                            //add "More" node
                            if (oMoreNode == null)
                                oMoreNode = CreateMoreNode(oSegmentNode, oSegment);

                            oTreeNode = oMoreNode.Nodes.Add(xKey, oXmlBlock.DisplayName);
                        }

                        //initialize new node
                        InitializeNode(oBlock, oXmlBlock, oTreeNode, false);
                    }
                }

                //add child segments to tree
                //GLOG 7101 (dm) - don't do in toolkit mode - all supported
                //segments are displayed at top-level
                //GLOG : 7998 : ceh
                if (!LMP.MacPac.MacPacImplementation.IsToolkit && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                    AddChildSegmentNodes(oSegment, oXmlSegment, oSegmentNode);

                LMP.Benchmarks.Print(t0);
            }
            finally
            {
                m_bIsRefreshingSegmentNode = false;
            }
        }
        /// <summary>
        /// returns the value to be displayed 
        /// in the tree for the specified variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string GetVariableUIValue_Oxml(LMP.Architect.Oxml.XmlVariable oVar)
        {
            DateTime t0 = DateTime.Now;

            string m_xEmptyDisplayValue = GetEmptyValueDisplay();
            string xValue = null;

            //check if variable.DisplayValue contains a field code, date format
            //or boolean value, then process accordingly

            if (System.String.IsNullOrEmpty(oVar.DisplayValue))
            {
                xValue = oVar.Value;
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                xValue = LMP.Architect.Oxml.XmlExpression.MarkLiteralReservedCharacters(xValue);
                xValue = LMP.Architect.Oxml.XmlExpression.Evaluate(xValue, oVar.Segment, oVar.Segment.ForteDocument);

                if (oVar.ControlType == mpControlTypes.Checkbox)
                {
                    if (!oVar.IsMultiValue)
                    {
                        //Display appropriate True/False text based on control properties-
                        BooleanComboBox oBC = (BooleanComboBox)oVar.AssociatedControl;
                        if (oBC == null)
                        {
                            DateTime t1 = DateTime.Now;
                            oBC = (BooleanComboBox)oVar.CreateAssociatedControl(this);
                            LMP.Benchmarks.Print(t1, "Create Combobox");
                            //Since we're creating these in advance, make sure 
                            //event handlers are also assigned
                            oBC.KeyPressed += new KeyEventHandler(OnKeyPressed);
                            oBC.KeyReleased += new KeyEventHandler(OnKeyReleased);

                        }
                        if (xValue.ToUpper() == "TRUE")
                            xValue = oBC.TrueString;
                        else if (xValue.ToUpper() == "FALSE")
                            xValue = oBC.FalseString;
                    }
                    else
                    {
                        //AssociatedControl is a MultiValueControl configured in BooleanComboBox mode
                        //Display appropriate True/False text based on control properties-
                        MultiValueControl oMVC = (MultiValueControl)oVar.AssociatedControl;
                        if (oMVC == null)
                        {
                            oMVC = (MultiValueControl)oVar.CreateAssociatedControl(this);
                            //Since we're creating these in advance, make sure 
                            //event handlers are also assigned
                            oMVC.KeyPressed += new KeyEventHandler(OnKeyPressed);
                            oMVC.KeyReleased += new KeyEventHandler(OnKeyReleased);

                        }
                        xValue = xValue.Replace("true", oMVC.TrueString);
                        xValue = xValue.Replace("false", oMVC.FalseString);
                    }
                }
                else if (oVar.ControlType == mpControlTypes.PaperTraySelector)
                {
                    //GLOG 4752: Access Printer bin information via Base wrapper functions
                    if (!string.IsNullOrEmpty(oVar.Value))
                        xValue = LMP.Forte.MSWord.Application.GetPrinterBinName(short.Parse(oVar.Value));
                }
                if (oVar.IsMultiValue)
                {
                    //Replace delimiter between multiple values;
                    xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
                }
            }
            else
            {
                //evaluate the display value expression using variable value
                try
                {
                    xValue = EvaluateDisplayValue_Oxml(oVar);

                    //evaluate the display value expression using variable value
                    try
                    {
                        //GLOG 3318: reflect use of field code in display value
                        if (oVar.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT") &&
                            oVar.AssociatedContentControls[0].Descendants<FieldChar>().Any())
                            xValue = xValue + " (field code)";
                    }
                    catch { }
                    //TODO: Oxml rewrite
                    ////GLOG 3318: reflect use of field code in display value
                    //if (oVar.ValueSourceExpression.ToUpper().StartsWith("[DATEFORMAT") &&
                    //    oVar.AssociatedContentControls[0].Range.Fields.Count > 0)
                    //    xValue = xValue + " (field code)";
                }
                catch { }
            }

            if (xValue == null || xValue == "")
                xValue = m_xEmptyDisplayValue;
            else
            {
                //replace non-printing chars with ellipses
                xValue = xValue.Replace("\v", "...");
                xValue = xValue.Replace("\t", "...");
                xValue = xValue.Replace("\r\n", "...");
                xValue = xValue.Replace("\r", "...");
                xValue = xValue.Replace("\n", "...");
                xValue = (oVar.Value == "") ? m_xEmptyDisplayValue : xValue;
            }

            //get available value node text width
            //TODO: we may want to enhance this method
            //to determine how much text to enter into
            //the node, given how much width is available
            if (xValue.Length > 35)
                //trim string and append ellipse if necessary
                xValue = xValue.Substring(0, 35) +
                    (xValue.EndsWith("...") ? "" : "...");

            LMP.Benchmarks.Print(t0, oVar.DisplayName);

            return xValue;
        }
        /// <summary>
        /// evaluates DisplayValue field code using variable value
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string EvaluateDisplayValue_Oxml(LMP.Architect.Oxml.XmlVariable oVar)
        {
            DateTime t0 = DateTime.Now;

            if (oVar.DisplayValue == null || oVar.DisplayValue == "")
                return "";

            string xVarValue = oVar.Value;
            //GLOG 2151: Mark reserved expression characters in Value so they'll
            //be treated as the literal characters in Expression.Evaluate
            xVarValue = Expression.MarkLiteralReservedCharacters(xVarValue);
            //pre-evaluate the DisplayValue field code(s), substituting MyValue for oVar.Value
            //string xFieldCode = oVar.DisplayValue.Replace("[MyValue]", xVarValue);
            string xFieldCode = LMP.Architect.Api.FieldCode.EvaluateMyValue(oVar.DisplayValue, xVarValue);

            if (xFieldCode.Contains("[MyTagValue]"))
            {
                try
                {
                    SdtElement oCC = oVar.AssociatedContentControls[0];
                    string xTagValue = oCC.InnerText;
                    //If CC contains a FieldCode, get the field result from the Text element
                    if (oCC.Descendants<FieldChar>().Any())
                    {
                        OpenXmlElement oText = oCC.Descendants<Text>().FirstOrDefault();
                        if (oText != null)
                        {
                            xTagValue = oText.InnerText;
                        }
                    }

                    //TODO: rework Oxml
                    ////GLOG 7394 (dm) - we need to take revisions into account here,
                    ////as Range.Text might include deletions
                    //string xTagValue = LMP.Forte.MSWord.WordDoc.GetTextWithRevisions(oCC.Range);
                    ////string xTagValue = oCC.Range.Text;

                    ////GLOG 3318: Get text using original input case if necessary
                    //if (!string.IsNullOrEmpty(xTagValue) && xTagValue.ToUpper() == xTagValue)
                    //    xTagValue = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCC.Range);
                    xTagValue = Expression.MarkLiteralReservedCharacters(xTagValue);

                    xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                }
                catch
                {
                    xFieldCode = xFieldCode.Replace("[MyTagValue]", "");
                }
            }

            //xFieldCode = xFieldCode.Replace("MyValue", xVarValue);
            xFieldCode = LMP.Architect.Api.FieldCode.EvaluateMyValue(xFieldCode, xVarValue);

            //handle listlookup field code
            if (oVar.DisplayValue.StartsWith("[ListLookup"))
                return LMP.Architect.Oxml.XmlExpression.Evaluate(xFieldCode, oVar.Segment, oVar.Segment.ForteDocument);

            //check for ^AND operator in compound DisplayValue strings 
            //or "][" indicating multiple field codes to be evaluated -
            //reline control value, which contains nodes for Standard and Special
            //is an example where DisplayValue may contain two field codes

            //***REPLACING THESE CHARACTERS DOESN'T SEEM TO BE NECESSARY, AND DOES NOT WORK CORRECTLY WITH
            //***MORE COMPLEX EXPRESSIONS - LET EXPRESSION.EVALUATE DEAL WITH THIS IN THE NORMAL FASHION
            //xFieldCode = xFieldCode.Replace("^AND", LMP.StringArray.mpEndOfElement.ToString() + "[");
            //xFieldCode = xFieldCode.Replace("][", "]" + LMP.StringArray.mpEndOfElement.ToString() + "[");

            //create array of field codes for further pre-evaluation
            string[] xFieldCodes = xFieldCode.Split(LMP.StringArray.mpEndOfElement);
            StringBuilder oSB = new StringBuilder();

            //build an array list of values returned by Expression.Evaluate
            ArrayList oArrayList = new ArrayList();
            string xTemp = null;
            string[] aTemp = null;

            //for each configured field code, evaluate and return its string value
            //in the case of CI field codes, the string will be delimited by "; "
            //one member for each recipient
            for (int i = 0; i <= xFieldCodes.GetUpperBound(0); i++)
            {
                xTemp = xFieldCodes[i];
                xTemp = xTemp.Replace("; ", ";mp10Tempmp10 ");
                if (xTemp == "[TagValue]" || xTemp == "[MyTagValue]")
                {
                    xTemp = oVar.AssociatedContentControls[0].InnerText;
                }
                else
                {
                    xTemp = LMP.Architect.Oxml.XmlExpression.Evaluate(
                        xTemp, oVar.Segment, oVar.Segment.ForteDocument);
                }

                if (xTemp == null)
                    xTemp = "";

                //split value and add to arraylist
                xTemp = xTemp.Replace(";mp10Tempmp10 ", ";");
                xTemp = xTemp.Replace("mp10Tempmp10 ", " ");
                xTemp = xTemp.Replace("; ", "|");
                aTemp = xTemp.Split('|');
                oArrayList.Add(aTemp);
            }

            //go through the array list of field codes and match field code return values
            //In most cases aTemp will have only a single member.  For CI type field codes,
            //aTemp will have as many members as returned contacts.
            for (int i = 0; i < oArrayList.Count; i++)
            {
                string[] aItems = (string[])oArrayList[i];

                for (int j = 0; j < aItems.Length; j++)
                {
                    //delimit each record w/ semicolon, add
                    //commas between record fields, i.e, name, address; name2, address;
                    string xFormat = (j == aItems.Length - 1) ? "{0}; " : "{0}, ";

                    oSB.AppendFormat(xFormat, aItems[j]);
                }
            }

            string xValue = oSB.ToString();
            if (oVar.IsMultiValue)
            {
                //Replace value separator for display
                xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
            }
            //eliminate double delimiters caused by empty values
            xValue = xValue.Replace(";;", ";");
            xValue = xValue.Replace("; ;", ";");
            xValue = xValue.Replace(",,", ",");
            xValue = xValue.Replace(", ,", ",");

            xValue = xValue.TrimEnd();
            xValue = xValue.TrimEnd(';');
            xValue = xValue.TrimStart(';');
            xValue = xValue.TrimStart(',');
            xValue = xValue.TrimStart();

            LMP.Benchmarks.Print(t0, oVar.Name);

            return xValue;
        }
        /// <summary>
        /// initializes node corresponding to oVar
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsToplevel"></param>
        private void InitializeNode_Oxml(Variable oVar, LMP.Architect.Oxml.XmlVariable oXmlVar, UltraTreeNode oNode, bool bAsToplevel)
        {
            DateTime t0 = DateTime.Now;

            //get hotkey, and add to hotkey hashtable
            string xHotkey = oXmlVar.Hotkey;

            if (xHotkey != null)
                AddHotkey(xHotkey, oNode);

            //add variable validation image
            if (!oXmlVar.HasValidValue)
                oNode.LeftImages.Add(Images.InvalidVariableValue);
            else if (oXmlVar.MustVisit)
                oNode.LeftImages.Add(Images.IncompleteVariable);
            else
                oNode.LeftImages.Add(Images.ValidVariableValue);

            //GLOG 8508: Make sure event handlers aren't subscribed more than once
            oVar.ValueAssigned -= m_oValueAssignedHandler;
            oVar.VariableVisited -= m_oVarVisitedHandler;
            //GLOG 8488
            //subscribe to variable value assigned events
            //so that we can change the "validated" symbol when necessary
            oVar.ValueAssigned += m_oValueAssignedHandler;
            oVar.VariableVisited += m_oVarVisitedHandler;

            //add reference to variable
            oNode.Tag = oVar;

            //set node properties
            oNode.Expanded = m_bShowValueNodes;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //set any pending node properties
            if (this.m_oPendingControlActions.ContainsKey(oXmlVar.TagID))
            {
                List<ControlAction> oActions = (List<ControlAction>)
                    this.m_oPendingControlActions[oXmlVar.TagID];

                for (int i = 0; i < oActions.Count; i++)
                    ExecuteControlAction(oActions[i]);
            }

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value",
                this.GetVariableUIValue_Oxml(oXmlVar));

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
            //oValueNode.Enabled = true;

            LMP.Benchmarks.Print(t0);
        }
#endif
        #endregion
        #region *********************ApiMethods*********************
        private void RefreshSegmentNode_Api(UltraTreeNode oSegmentNode, bool bForceRefresh)
        {
            m_bIsRefreshingSegmentNode = true;

            try
            {
                DateTime t0 = DateTime.Now;
                UltraTreeNode oMoreNode = null;

                //exit if node is the same
                if (oSegmentNode == this.m_oCurNode && !bForceRefresh)
                    return;

                //get referred segment
                Segment oSegment = oSegmentNode.Tag as Segment;
                if (oSegment == null)
                    throw new LMP.Exceptions.UINodeException(
                        LMP.Resources.GetLangString("Error_NodeIsNotSegmentNode"));

                //clear any existing child nodes
                oSegmentNode.Nodes.Clear();
                //clear controls hash table
                m_oControlsHash.Clear();

                //8-18-11 (dm) - add author, court, and language nodes only if
                //these have variables to target
                //10-12-11 (dm) - condition changed to HasSnapshot - the
                //previous condition didn't account for the reintroduction
                //of variables when a child is added or replaced, resulting
                //in the inappropriate reappearance of these nodes - once
                //a segment has been finished, the language and court must be
                //fixed, and the author subject to change only where
                //children have been configured with their own control
                if (!Snapshot.Exists(oSegment))
                {
                    //Display Jurisdiction control for appropriate types
                    if (oSegment.IsTopLevel && oSegment.ShowCourtChooser)
                    {
                        string xKey = oSegment.FullTagID + ".Jurisdiction";

                        //get appropriate display name for node
                        string xDisplayName = oSegment.CourtChooserUILabel;
                        if (string.IsNullOrEmpty(xDisplayName))
                            xDisplayName = LMP.Resources.GetLangString("Lbl_Court") + ":";

                        //add Jurisdiction node
                        UltraTreeNode oJurisdictionNode = oSegmentNode.Nodes.Add(xKey, xDisplayName);


                        //initialize new node
                        InitializeJurisdictionNode(oSegment, oJurisdictionNode);

                        if (!oSegment.Activated)
                            oJurisdictionNode.Enabled = false;
                    }

                    if ((oSegment.IsTopLevel || (!oSegment.LinkAuthorsToParent))
                        && (oSegment.MaxAuthors > 0))
                    {
                        //get authors node key
                        string xKey = oSegment.FullTagID + ".Authors";

                        //get appropriate display name for node
                        string xDisplayName = "";
                        //GLOG 6653
                        if (oSegment.AuthorsNodeUILabel == "Author" || oSegment.AuthorsNodeUILabel == "")
                        {
                                if (oSegment.MaxAuthors == 1)
                                    xDisplayName = LMP.Resources.GetLangString("Msg_Author");
                                else
                                    xDisplayName = LMP.Resources.GetLangString("Msg_Authors");
                        }
                        else if (oSegment.AuthorsNodeUILabel == "Attorney")

                                if (oSegment.MaxAuthors == 1)
                                    xDisplayName = LMP.Resources.GetLangString("Msg_Attorney");
                                else
                                    xDisplayName = LMP.Resources.GetLangString("Msg_Attorneys");
                        else
                        {
                                xDisplayName = oSegment.AuthorsNodeUILabel;
                        }

                        //add authors node
                        UltraTreeNode oAuthorsNode = oSegmentNode.Nodes.Add(xKey, xDisplayName);

                        //initialize new node
                        InitializeNode(oSegment.Authors, oAuthorsNode);

                        if (!oSegment.Activated)
                            oAuthorsNode.Enabled = false;
                    }

                    //add language node
                    if (oSegment.IsTopLevel && oSegment.SupportsMultipleLanguages)
                    {
                        string xKey = oSegment.FullTagID + ".Language";
                        string xDisplayName = LMP.Resources.GetLangString("Lbl_Language") + ":";
                        UltraTreeNode oLanguageNode = oSegmentNode.Nodes.Add(xKey, xDisplayName);
                        InitializeLanguageNode(oSegment, oLanguageNode);

                        if (!oSegment.Activated)
                            oLanguageNode.Enabled = false;
                    }
                }

                //cycle through segment variables, adding each to tree
                for (int i = 0; i < oSegment.Variables.Count; i++)
                {
                    //get variable
                    LMP.Architect.Api.Variable oVar = oSegment.Variables[i];

                    //add only variables that should be shown in the editor
                    if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) !=
                        Variable.ControlHosts.DocumentEditor)
                        continue;

                    string xKey = oSegment.FullTagID + "." + oVar.ID;

                    string xDisplayName = String.RestoreXMLChars(oVar.DisplayName, true); //GLOG 8463

                    UltraTreeNode oTreeNode = null;

                    //add to tree
                    if (oVar.DisplayLevel == Variable.Levels.Basic)
                        if (oMoreNode == null)
                            //add as last node
                            oTreeNode = oSegmentNode.Nodes.Add(
                                xKey, xDisplayName + ":");
                        else
                            //add as last node in basic section
                            oTreeNode = oSegmentNode.Nodes.Insert(oMoreNode.Index,
                                xKey, xDisplayName + ":");
                    else
                    {
                        //add "More" node
                        if (oMoreNode == null)
                            oMoreNode = CreateMoreNode(oSegmentNode, oSegment);

                        //add as last node in "More" category
                        oTreeNode = oMoreNode.Nodes.Add(
                            xKey, xDisplayName + ":");
                    }
                    //initialize new node
                    InitializeNode_API(oVar, oTreeNode, false);

                    if (!oSegment.Activated)
                        oTreeNode.Enabled = false;
                }

                if (oSegment.Activated)
                {
                    //cycle through segment variables, executing the
                    //ValueChanged actions - this sets up the nodes
                    //based on the initial variable values
                    for (int i = 0; i < oSegment.Variables.Count; i++)
                        ExecuteControlActions(oSegment.Variables[i],
                            ControlActions.Events.ValueChanged);

                    //execute the value changed control 
                    //actions assigned to the author
                    ExecuteControlActions(oSegment.Authors,
                        ControlActions.Events.ValueChanged);
                }

                //Don't want to run this here, because it will undo
                //any adjustments to the default Court Title saved in a prefill
                ////execute value changed control actions
                ////assigned to jurisdiction control
                //if (oSegment.IsTopLevel && oSegment.ShowCourtChooser)
                //{
                //    ExecuteControlActions(oSegment
                //        .CourtChooserControlActions, ControlActions.Events.ValueChanged);
                //}

                //cycle through segment blocks, adding each to tree
                for (int i = 0; i < oSegment.Blocks.Count; i++)
                {
                    //get block
                    LMP.Architect.Api.Block oBlock = oSegment.Blocks[i];

                    UltraTreeNode oTreeNode;
                    //add to tree

                    if (oBlock.ShowInTree)
                    {
                        string xKey = oBlock.TagID;

                        if (oBlock.DisplayLevel == Variable.Levels.Basic)
                        {
                            // This segment is configured to be in the basic section.

                            if (oMoreNode == null)
                                //add as last node
                                oTreeNode = oSegmentNode.Nodes.Add(
                                    xKey, oBlock.DisplayName + ":");
                            else
                                //add as last node in basic section
                                oTreeNode = oSegmentNode.Nodes.Insert(oMoreNode.Index,
                                    xKey, oBlock.DisplayName + ":");
                        }
                        else
                        {
                            // This segment is configured to be in the advanced section.
                            //add "More" node
                            if (oMoreNode == null)
                                oMoreNode = CreateMoreNode(oSegmentNode, oSegment);

                            oTreeNode = oMoreNode.Nodes.Add(xKey, oBlock.DisplayName);
                        }

                        //initialize new node
                        InitializeNode(oBlock, oTreeNode, false);
                    }
                }
                //GLOG 8437: Clear this so it won't affect order of child segments
                m_bReplaceSegmentPrimaryItem = false;
                //add child segments to tree
                //GLOG 7101 (dm) - don't do in toolkit mode - all supported
                //segments are displayed at top-level
                //GLOG : 7998 : ceh
                if (!LMP.MacPac.MacPacImplementation.IsToolkit && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
                    AddChildSegmentNodes(oSegment, oSegmentNode);

                LMP.Benchmarks.Print(t0);
            }
            finally
            {
                m_bIsRefreshingSegmentNode = false;
            }
        }
        /// <summary>
        /// returns the value to be displayed 
        /// in the tree for the specified variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string GetVariableUIValue_API(Variable oVar)
        {
            DateTime t0 = DateTime.Now;

            string m_xEmptyDisplayValue = GetEmptyValueDisplay();
            string xValue = null;

            //check if variable.DisplayValue contains a field code, date format
            //or boolean value, then process accordingly

            if (System.String.IsNullOrEmpty(oVar.DisplayValue))
            {
                xValue = oVar.Value;
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                xValue = Expression.MarkLiteralReservedCharacters(xValue);
                xValue = Expression.Evaluate(xValue, oVar.Segment, m_oMPDocument);

                if (oVar.ControlType == mpControlTypes.Checkbox)
                {
                    if (!oVar.IsMultiValue)
                    {
                        //Display appropriate True/False text based on control properties-
                        BooleanComboBox oBC = (BooleanComboBox)oVar.AssociatedControl;
                        if (oBC == null)
                        {
                            DateTime t1 = DateTime.Now;
                            oBC = (BooleanComboBox)oVar.CreateAssociatedControl(this);
                            LMP.Benchmarks.Print(t1, "Create Combobox");
                            //Since we're creating these in advance, make sure 
                            //event handlers are also assigned
                            oBC.KeyPressed += new KeyEventHandler(OnKeyPressed);
                            oBC.KeyReleased += new KeyEventHandler(OnKeyReleased);

                        }
                        if (xValue.ToUpper() == "TRUE")
                            xValue = oBC.TrueString;
                        else if (xValue.ToUpper() == "FALSE")
                            xValue = oBC.FalseString;
                    }
                    else
                    {
                        //AssociatedControl is a MultiValueControl configured in BooleanComboBox mode
                        //Display appropriate True/False text based on control properties-
                        MultiValueControl oMVC = (MultiValueControl)oVar.AssociatedControl;
                        if (oMVC == null)
                        {
                            oMVC = (MultiValueControl)oVar.CreateAssociatedControl(this);
                            //Since we're creating these in advance, make sure 
                            //event handlers are also assigned
                            oMVC.KeyPressed += new KeyEventHandler(OnKeyPressed);
                            oMVC.KeyReleased += new KeyEventHandler(OnKeyReleased);

                        }
                        xValue = xValue.Replace("true", oMVC.TrueString);
                        xValue = xValue.Replace("false", oMVC.FalseString);
                    }
                }
                else if (oVar.ControlType == mpControlTypes.PaperTraySelector)
                {
                    //GLOG 4752: Access Printer bin information via Base wrapper functions
                    if (!string.IsNullOrEmpty(oVar.Value))
                        xValue = LMP.Forte.MSWord.Application.GetPrinterBinName(short.Parse(oVar.Value));
                }
                if (oVar.IsMultiValue)
                {
                    //Replace delimiter between multiple values;
                    xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
                }
            }
            else
            {
                //evaluate the display value expression using variable value
                try
                {
                    xValue = EvaluateDisplayValue_API(oVar);

                    if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        //GLOG 3318: reflect use of field code in display value
                        if (oVar.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT") &&
                            oVar.AssociatedWordTags[0].Range.Fields.Count > 0)
                            xValue = xValue + " (field code)";
                    }
                    else
                    {
                        //GLOG 3318: reflect use of field code in display value
                        if (oVar.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT") &&
                            oVar.AssociatedContentControls[0].Range.Fields.Count > 0)
                            xValue = xValue + " (field code)";
                    }
                }
                catch { }
            }

            if (xValue == null || xValue == "")
                xValue = m_xEmptyDisplayValue;
            else
            {
                //replace non-printing chars with ellipses
                xValue = xValue.Replace("\v", "...");
                xValue = xValue.Replace("\t", "...");
                xValue = xValue.Replace("\r\n", "...");
                xValue = xValue.Replace("\r", "...");
                xValue = xValue.Replace("\n", "...");
                xValue = (oVar.Value == "") ? m_xEmptyDisplayValue : xValue;
            }

            //get available value node text width
            //TODO: we may want to enhance this method
            //to determine how much text to enter into
            //the node, given how much width is available
            if (xValue.Length > 35)
                //trim string and append ellipse if necessary
                xValue = xValue.Substring(0, 35) +
                    (xValue.EndsWith("...") ? "" : "...");

            LMP.Benchmarks.Print(t0, oVar.DisplayName);

            return xValue;
        }
        /// <summary>
        /// evaluates DisplayValue field code using variable value
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string EvaluateDisplayValue_API(Variable oVar)
        {
            DateTime t0 = DateTime.Now;

            if (oVar.DisplayValue == null || oVar.DisplayValue == "")
                return "";

            string xVarValue = oVar.Value;
            //GLOG 2151: Mark reserved expression characters in Value so they'll
            //be treated as the literal characters in Expression.Evaluate
            xVarValue = Expression.MarkLiteralReservedCharacters(xVarValue);
            //pre-evaluate the DisplayValue field code(s), substituting MyValue for oVar.Value
            //string xFieldCode = oVar.DisplayValue.Replace("[MyValue]", xVarValue);
            string xFieldCode = LMP.Architect.Api.FieldCode.EvaluateMyValue(oVar.DisplayValue, xVarValue);

            if (xFieldCode.Contains("[MyTagValue]"))
            {
                try
                {
                    if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {
                        Word.XMLNode oWordNode = oVar.AssociatedWordTags[0];

                        //GLOG 7394 (dm) - we need to take revisions into account here,
                        //as Range.Text might include deletions
                        //GLOG 8407 - Avoid call to GetTextWithRevisions during segment creation
                        string xTagValue = "";
                        if (oVar.Segment.CreationStatus == Architect.Base.Segment.Status.Finished)
                        	xTagValue = LMP.Forte.MSWord.WordDoc.GetTextWithRevisions(oWordNode.Range);
                        else
                            xTagValue = oWordNode.Text;

                        //GLOG 3318: Get text using original input case if necessary
                        if (!string.IsNullOrEmpty(xTagValue) && xTagValue.ToUpper() == xTagValue)
                            //GLOG 8407: Only check revisions if Segment is fully created
                            xTagValue = LMP.Forte.MSWord.WordDoc.GetInputValueIfAllCapped(oWordNode, oVar.Segment.CreationStatus == Architect.Base.Segment.Status.Finished);
                        xTagValue = Expression.MarkLiteralReservedCharacters(xTagValue);

                        //GLOG 4476 (dm) - if field code is displayed, get result
                        if ((!string.IsNullOrEmpty(xTagValue)) &&
                            xTagValue.Contains(((char)21).ToString()) &&
                            (oWordNode.Range.Fields.Count > 0))
                            xTagValue = oWordNode.Range.Fields[1].Result.Text;

                        xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                    }
                    else
                    {
                        Word.ContentControl oCC = oVar.AssociatedContentControls[0];

                        //GLOG 7394 (dm) - we need to take revisions into account here,
                        //as Range.Text might include deletions
                        //GLOG 8407 - Avoid call to GetTextWithRevisions during segment creation
                        string xTagValue = "";
                        if (oVar.Segment.CreationStatus == Architect.Base.Segment.Status.Finished)
                        	xTagValue = LMP.Forte.MSWord.WordDoc.GetTextWithRevisions(oCC.Range);
                        else
                            xTagValue = oCC.Range.Text;

                        //GLOG 3318: Get text using original input case if necessary
                        if (!string.IsNullOrEmpty(xTagValue) && xTagValue.ToUpper() == xTagValue)
                            //GLOG 8407: Only check revisions if Segment is fully created
                            xTagValue = LMP.Forte.MSWord.WordDoc.GetRangeTextIfAllCapped(oCC.Range, oVar.Segment.CreationStatus == Architect.Base.Segment.Status.Finished);
                        xTagValue = Expression.MarkLiteralReservedCharacters(xTagValue);

                        //GLOG 4476 (dm) - if field code is displayed, get result
                        if ((!string.IsNullOrEmpty(xTagValue)) &&
                            xTagValue.Contains(((char)21).ToString()) &&
                            (oCC.Range.Fields.Count > 0))
                            xTagValue = oCC.Range.Fields[1].Result.Text;

                        xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                    }
                }
                catch
                {
                    xFieldCode = xFieldCode.Replace("[MyTagValue]", "");
                }
            }

            //xFieldCode = xFieldCode.Replace("MyValue", xVarValue);
            xFieldCode = LMP.Architect.Api.FieldCode.EvaluateMyValue(xFieldCode, xVarValue);

            //handle listlookup field code
            if (oVar.DisplayValue.StartsWith("[ListLookup"))
                return Expression.Evaluate(xFieldCode, oVar.Segment, m_oMPDocument);

            //check for ^AND operator in compound DisplayValue strings 
            //or "][" indicating multiple field codes to be evaluated -
            //reline control value, which contains nodes for Standard and Special
            //is an example where DisplayValue may contain two field codes

            //***REPLACING THESE CHARACTERS DOESN'T SEEM TO BE NECESSARY, AND DOES NOT WORK CORRECTLY WITH
            //***MORE COMPLEX EXPRESSIONS - LET EXPRESSION.EVALUATE DEAL WITH THIS IN THE NORMAL FASHION
            //xFieldCode = xFieldCode.Replace("^AND", LMP.StringArray.mpEndOfElement.ToString() + "[");
            //xFieldCode = xFieldCode.Replace("][", "]" + LMP.StringArray.mpEndOfElement.ToString() + "[");

            //create array of field codes for further pre-evaluation
            string[] xFieldCodes = xFieldCode.Split(LMP.StringArray.mpEndOfElement);
            StringBuilder oSB = new StringBuilder();

            //build an array list of values returned by Expression.Evaluate
            ArrayList oArrayList = new ArrayList();
            string xTemp = null;
            string[] aTemp = null;

            //for each configured field code, evaluate and return its string value
            //in the case of CI field codes, the string will be delimited by "; "
            //one member for each recipient
            for (int i = 0; i <= xFieldCodes.GetUpperBound(0); i++)
            {
                xTemp = xFieldCodes[i];
                xTemp = xTemp.Replace("; ", ";mp10Tempmp10 ");
                if (xTemp == "[TagValue]" || xTemp == "[MyTagValue]")
                {
                    if (oVar.ForteDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
                    {

                        xTemp = oVar.AssociatedWordTags[0].Text;
                    }
                    else
                    {
                        xTemp = oVar.AssociatedContentControls[0].Range.Text;
                    }
                }
                else
                {
                    xTemp = Expression.Evaluate(xTemp, oVar.Segment, m_oMPDocument);
                }

                if (xTemp == null)
                    xTemp = "";

                //split value and add to arraylist
                xTemp = xTemp.Replace(";mp10Tempmp10 ", ";");
                xTemp = xTemp.Replace("mp10Tempmp10 ", " ");
                xTemp = xTemp.Replace("; ", "|");
                aTemp = xTemp.Split('|');
                oArrayList.Add(aTemp);
            }

            //go through the array list of field codes and match field code return values
            //In most cases aTemp will have only a single member.  For CI type field codes,
            //aTemp will have as many members as returned contacts.
            for (int i = 0; i < oArrayList.Count; i++)
            {
                string[] aItems = (string[])oArrayList[i];

                for (int j = 0; j < aItems.Length; j++)
                {
                    //delimit each record w/ semicolon, add
                    //commas between record fields, i.e, name, address; name2, address;
                    string xFormat = (j == aItems.Length - 1) ? "{0}; " : "{0}, ";

                    oSB.AppendFormat(xFormat, aItems[j]);
                }
            }

            string xValue = oSB.ToString();
            if (oVar.IsMultiValue)
            {
                //Replace value separator for display
                xValue = xValue.Replace(StringArray.mpEndOfValue.ToString(), "; ");
            }
            //eliminate double delimiters caused by empty values
            xValue = xValue.Replace(";;", ";");
            xValue = xValue.Replace("; ;", ";");
            xValue = xValue.Replace(",,", ",");
            xValue = xValue.Replace(", ,", ",");

            xValue = xValue.TrimEnd();
            xValue = xValue.TrimEnd(';');
            xValue = xValue.TrimStart(';');
            xValue = xValue.TrimStart(',');
            xValue = xValue.TrimStart();

            LMP.Benchmarks.Print(t0, oVar.Name);

            return xValue;
        }
        /// <summary>
        /// initializes node corresponding to oVar
        /// </summary>
        /// <param name="oVar"></param>
        /// <param name="oNode"></param>
        /// <param name="bAsToplevel"></param>
        private void InitializeNode_API(Variable oVar, UltraTreeNode oNode, bool bAsToplevel)
        {
            DateTime t0 = DateTime.Now;

            //get hotkey, and add to hotkey hashtable
            string xHotkey = oVar.Hotkey;

            if (xHotkey != null)
                AddHotkey(xHotkey, oNode);

            //add variable validation image
            if (!oVar.HasValidValue)
                oNode.LeftImages.Add(Images.InvalidVariableValue);
            else if (oVar.MustVisit)
                oNode.LeftImages.Add(Images.IncompleteVariable);
            else
                oNode.LeftImages.Add(Images.ValidVariableValue);

            //GLOG 8508: Make sure event handlers aren't subscribed more than once
            oVar.ValueAssigned -= m_oValueAssignedHandler;
            oVar.VariableVisited -= m_oVarVisitedHandler;
            //subscribe to variable value assigned events
            //so that we can change the "validated" symbol when necessary
            oVar.ValueAssigned += m_oValueAssignedHandler;
            oVar.VariableVisited += m_oVarVisitedHandler;

            //add reference to variable
            oNode.Tag = oVar;

            //set node properties
            oNode.Expanded = m_bShowValueNodes;

            Override oOverride = oNode.Override;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingBefore = 1;
            oOverride.NodeSpacingAfter = 1;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;

            //set any pending node properties
            if (this.m_oPendingControlActions.ContainsKey(oVar.TagID))
            {
                List<ControlAction> oActions = (List<ControlAction>)
                    this.m_oPendingControlActions[oVar.TagID];

                for (int i = 0; i < oActions.Count; i++)
                    ExecuteControlAction(oActions[i]);
            }

            //add node for variable value underneath variable node -
            UltraTreeNode oValueNode = oNode.Nodes.Add(oNode.Key + "_Value",
                this.GetVariableUIValue_API(oVar));

            oOverride = oValueNode.Override;
            oOverride.NodeAppearance.ForeColor = System.Drawing.Color.Gray;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;
            oOverride.NodeSpacingAfter = 10;
            oOverride.NodeDoubleClickAction = NodeDoubleClickAction.None;
            //oValueNode.Enabled = true;

            LMP.Benchmarks.Print(t0);
        }
        #endregion
        #endregion
        #region *********************protected methods*********************
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// processes the tab and shift-tab key combinations -
        /// selects next or previous node -
        /// unfortunately, there is an IBF bug that prevents
        /// this method from getting executed when appropriate -
        /// in cases when this method does not execute, the
        /// tab pressed event handler will execute
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            try
            {
                //get state of tab key - calling the API KeyState function
                //is the only reliable way to get this info
                int iTabKeyState = LMP.WinAPI.GetKeyState(KEY_TAB);
                bool bTabPressed = !(iTabKeyState == 1 || iTabKeyState == 0);
                //get state of ctrl key
                int iCtrlKeyState = LMP.WinAPI.GetKeyState(KEY_CONTROL);
                bool bCtrlPressed = !(iCtrlKeyState == 1 || iCtrlKeyState == 0);
                //get state of shift key
                int iShiftKeyState = LMP.WinAPI.GetKeyState(KEY_SHIFT);
                bool bShiftPressed = !(iShiftKeyState == 1 || iShiftKeyState == 0);

                if (bTabPressed)
                {
                    keyData = Keys.None;
                    HandleTabPressed(bShiftPressed);
                    return true;
                }
                else if (bCtrlPressed && bShiftPressed)
                {
                    //Ctrl+Shift shortcuts defined for the control
                    //Home - move to first variable
                    //End - move to last variable
                    //Left - Collapse tree to initial state
                    //Right - Expand all nodes in tree
                    //get state of Home key
                    int iHomeKeyState = LMP.WinAPI.GetKeyState(KEY_HOME);
                    bool bHomeKeyPressed = !(iHomeKeyState == 1 || iHomeKeyState == 0);
                    //get state of End key
                    int iEndKeyState = LMP.WinAPI.GetKeyState(KEY_END);
                    bool bEndKeyPressed = !(iEndKeyState == 1 || iEndKeyState == 0);
                    //get state of Left key
                    int iLeftKeyState = LMP.WinAPI.GetKeyState(KEY_LEFT);
                    bool bLeftKeyPressed = !(iLeftKeyState == 1 || iLeftKeyState == 0);
                    //get state of Right key
                    int iRightKeyState = LMP.WinAPI.GetKeyState(KEY_RIGHT);
                    bool bRightKeyPressed = !(iRightKeyState == 1 || iRightKeyState == 0);
                    //get state of B key (select body)
                    int iBKeyState = LMP.WinAPI.GetKeyState(66);
                    bool bBKeyPressed = !(iBKeyState == 1 || iBKeyState == 0);
                    if (bHomeKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        //GLOG 7774 (dm) - NewSegment may be null in Forte Tools
                        if ((TaskPane.NewSegment != null) && (TaskPane.NewSegment.ShowChooser))
                            //set focus to chooser
                            this.SelectChooserNode();
                        else
                            SelectFirstVariableNode();
                        return true;
                    }
                    else if (bEndKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        SelectLastVariableNode();
                        return true;
                    }
                    else if (bLeftKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        this.CollapseAll();
                        return true;
                    }
                    else if (bRightKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        this.ExpandAll();
                        return true;
                    }
                    else if (bBKeyPressed)
                    {
                        this.DisableHotKeyMode = true;
                        SelectBodyNode(m_oCurNode);
                        return true;
                    }
                }
                if (keyData == Keys.F5)
                {
                    //this.TaskPane.Refresh() - relaced with lines below (GLOG 2768, 8/1/08);
                    TaskPanes.RefreshAll();
                    this.RefreshTree();
                    this.Focus();
                    return true;
                }
                else
                {
                    this.DisableHotKeyMode = false;
                    return false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            return false;
        }
        #endregion
        #region *********************static methods*********************
        /// <summary>
        /// creates an instance of each control -
        /// called on a separate thread to increase
        /// performance of initial control display by
        /// forcing code to cache
        /// </summary>
        private static void InitializeControls()
        {
            LMP.Controls.TextBox oTB = new LMP.Controls.TextBox();
            LMP.Controls.MultilineTextBox oMLT = new MultilineTextBox();
            LMP.Controls.ComboBox oCombo = new LMP.Controls.ComboBox();
            //LMP.Controls.MultilineCombo oMLC = new MultilineCombo(this.TaskPane);
            LMP.Controls.Reline oReline = new Reline();
            LMP.Controls.Detail oRecipDetail = new Detail();
            LMP.Controls.DetailList oRecipList = new DetailList();
            LMP.Controls.AuthorSelector oAuthorSelector = new AuthorSelector();
            LMP.Controls.Chooser oChooser = new Chooser();
            LMP.Controls.JurisdictionChooser oJurisChooser = new JurisdictionChooser();
            LMP.Controls.NameValuePairGrid oNameValue = new NameValuePairGrid();
            LMP.Controls.LanguagesComboBox oLanguage = new LanguagesComboBox();
            LMP.Controls.PaperTraySelector oPaperTraySelector = new PaperTraySelector();
            //LMP.Controls.ClientMatterSelector oCMSelector = new ClientMatterSelector();

            oTB.Dispose();
            oMLT.Dispose();
            oCombo.Dispose();
            //oMLC.Dispose();
            oReline.Dispose();
            oRecipDetail.Dispose();
            oRecipList.Dispose();
            oAuthorSelector.Dispose();
            oChooser.Dispose();
            oJurisChooser.Dispose();
            oNameValue.Dispose();
            oLanguage.Dispose();
            oPaperTraySelector.Dispose();
            //oCMSelector.Dispose();
        }
        #endregion

        /// <summary>
        /// GLOG : 1908 : JAB
        /// Move the picMenu control to the appropriate location
        /// after resizing the tree control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_Resize(object sender, EventArgs e)
        {
            try
            {
                DisplayContextMenuButton();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 1908 : JAB
        /// Conditionally show the context menu button.
        /// </summary>
        private void DisplayContextMenuButton()
        {
            if (this.m_oCurNode != null)
            {
                //move control with associated node
                if (this.picMenu.Visible)
                {
                    ShowContextMenuButton(this.m_oCurNode);
                }

                this.SizeAndPositionControl();
            }
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Determine if the treeContent control's vertical scrollbar is visible.
        /// This is used in determining the location of the content mgr menu 
        /// picture control. 
        /// </summary>
        private bool IsTreeContentVerticalScrollbarVisible
        {
            get
            {
                UltraTreeNode oLastExposedNode = GetLastExposedNode(this.treeDocContents);
                return !(this.treeDocContents.Nodes[0].IsInView && oLastExposedNode.IsInView);
            }
        }

        /// <summary>
        /// True if a context menu is displayed
        /// </summary>
        /// <returns></returns>
        internal bool MenuIsVisible()
        {
            //GLOG 3721
            return ((mnuQuickHelp != null && mnuQuickHelp.Visible) || 
                (m_oMenuAuthors != null && m_oMenuAuthors.Visible) ||
                (m_oMenuSegment != null &&  m_oMenuSegment.Visible) || 
                (m_oMenuVariable != null && m_oMenuVariable.Visible) || 
                (m_oMenuBlock != null && m_oMenuBlock.Visible));
        }
        /// <summary>
        /// GLOG : 2918 : JAB
        /// Get the last node that is exposed in the treeContent control. This
        /// is needed to determine if the treeContent's vertical scrollbar is 
        /// visible.
        /// </summary>
        /// <param name="treeContent"></param>
        /// <returns></returns>
        private UltraTreeNode GetLastExposedNode(UltraTree treeContent)
        {
            UltraTreeNode oLastExposedNode = null;

            if (treeContent.Nodes.Count > 0)
            {
                oLastExposedNode = treeContent.Nodes[treeContent.Nodes.Count - 1];

                while (oLastExposedNode.HasNodes && oLastExposedNode.Expanded)
                {
                    oLastExposedNode = oLastExposedNode.Nodes[oLastExposedNode.Nodes.Count - 1];
                }
            }

            return oLastExposedNode;
        }

        /// <summary>
        /// GLOG : 2985 : JAB
        /// Position the document editor's menu picture control appropriately if the treeContent
        /// container panel has been resized.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlDocContents_SizeChanged(object sender, EventArgs e)
        {
            if (this.TaskPane == null || !this.TaskPane.IsSetUp)
                return;

            if (this.treeDocContents.SelectedNodes.Count > 0)
            {
                if (this.treeDocContents.SelectedNodes[0].Tag is Segment)
                {
                    this.ShowContextMenuButton(this.treeDocContents.SelectedNodes[0]);
                }
            }
            else
            {
                if (this.treeDocContents.ActiveNode != null)
                {
                    if (this.treeDocContents.ActiveNode.Tag is Segment)
                    {
                        this.ShowContextMenuButton(this.treeDocContents.ActiveNode);
                    }
                }
            }
        }

        /// <summary>
        /// returns true if the Word tag associated with oNode is
        /// no longer in the document
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private bool AssociatedWordTagIsMissing(UltraTreeNode oNode)
        {
            if (m_oMPDocument.FileFormat == LMP.Data.mpFileFormats.Binary)
            {
                //check for xml tag
                Word.XMLNode oWordTag = null;
                Word.Bookmark oBmk = null;

                if (oNode.Tag is Segment)
                {
                    Segment oSegment = (Segment)oNode.Tag;
                    try
                    {
                        oWordTag = oSegment.FirstWordTag;

                        //GLOG 6829 (dm)
                        if (oWordTag == null)
                            oBmk = oSegment.FirstBookmark;
                    }
                    catch 
                    {
                        oBmk = oSegment.FirstBookmark;
                    }
                }
                else if (oNode.Tag is Variable)
                {
                    Variable oVar = (Variable)oNode.Tag;
                    Word.XMLNode[] aTags = oVar.AssociatedWordTags;
                    if (aTags.Length > 0)
                    {
                        //validate all associated tags
                        Object oNodesArray = aTags;
                        return LMP.Forte.MSWord.WordDoc.TagIsMissing(ref oNodesArray);
                    }
                    else
                    {
                        try
                        {
                            oWordTag = oVar.Segment.FirstWordTag;
                        }
                        catch
                        {
                            oBmk = oVar.Segment.FirstBookmark;
                        }
                    }
                }
                else if (oNode.Tag is Block)
                {
                    Block oBlock = (Block)oNode.Tag;
                    oWordTag = oBlock.AssociatedWordTag;
                }
                else if (oNode.Tag is Authors)
                {
                    Authors oAuthors = (Authors)oNode.Tag;
                    try
                    {
                        oWordTag = oAuthors.Segment.FirstWordTag;
                    }
                    catch
                    {
                        oBmk = oAuthors.Segment.FirstBookmark;
                    }
                }

                if (oWordTag != null)
                {
                    //validate tag
                    try
                    {
                        string xBaseName = oWordTag.BaseName;
                    }
                    catch
                    {
                        return true;
                    }
                }
                else if (oBmk != null)
                {
                    //GLOG 6829 (dm) - the bookmark may no longer exist
                    try
                    {
                        return String.GetBoundingObjectBaseName(oBmk.Name.Substring(1)) != "mSEG";
                    }
                    catch
                    {
                        return true;
                    }
                }
            }
            else
            {
                //check for content control
                Word.ContentControl oCC = null;
                Word.Bookmark oBmk = null;

                if (oNode.Tag is Segment)
                {
                    Segment oSegment = (Segment)oNode.Tag;
                    try
                    {
                        oCC = oSegment.FirstContentControl;

                        //GLOG 6829 (dm)
                        if (oCC == null)
                            oBmk = oSegment.FirstBookmark;
                    }
                    catch
                    {
                        oBmk = oSegment.FirstBookmark;
                    }
                }
                else if (oNode.Tag is Variable)
                {
                    Variable oVar = (Variable)oNode.Tag;
                    Word.ContentControl[] aTags = oVar.AssociatedContentControls;
                    if (aTags.Length > 0)
                    {
                        //validate all associated tags
                        Object oNodesArray = aTags;
                        return LMP.Forte.MSWord.WordDoc.ContentControlIsMissing(
                            ref oNodesArray);
                    }
                    else
                    {
                        try
                        {
                            oCC = oVar.Segment.FirstContentControl;
                        }
                        catch 
                        {
                            oBmk = oVar.Segment.FirstBookmark;
                        }
                    }
                }
                else if (oNode.Tag is Block)
                {
                    Block oBlock = (Block)oNode.Tag;
                    oCC = oBlock.AssociatedContentControl;
                }
                else if (oNode.Tag is Authors)
                {
                    Authors oAuthors = (Authors)oNode.Tag;
                    try
                    {
                        oCC = oAuthors.Segment.FirstContentControl;
                    }
                    catch
                    {
                        oBmk = oAuthors.Segment.FirstBookmark;
                    }
                }

                if (oCC != null)
                {
                    //validate content control
                    try
                    {
                        string xTag = oCC.ID;
                    }
                    catch
                    {
                        return true;
                    }
                }
                else if (oBmk != null)
                {
                    //GLOG 6829 (dm) - the bookmark may no longer exist
                    try
                    {
                        return String.GetBoundingObjectBaseName(oBmk.Name.Substring(1)) != "mSEG";
                    }
                    catch
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// refreshes all language dependent variable controls belonging
        /// to the specified segment or its children
        /// </summary>
        /// <param name="oSegmentNode"></param>
        /// <param name="iCulture"></param>
        private void UpdateForLanguage(Segment oSegment)
        {
            DateTime t0 = DateTime.Now;

            for (int i = 0; i < oSegment.Variables.Count; i++)
            {
                Variable oVar = oSegment.Variables[i];

                //GLOG 3528: Code to check for language dependence moved to property
                if (oVar.IsLanguageDependent)
                {
                    string xExp = oVar.DefaultValue;
                    if (Expression.ContainsPreferenceCode(xExp) ||
                        (xExp.IndexOf("[SegmentLanguage") > -1))
                    {
                        //default value contains a language or pref code -
                        //reset variable value
                        string xDefault = Expression.Evaluate(xExp, oSegment,
                            m_oMPDocument);
                        if (xDefault != "null")
                            oVar.SetValue(xDefault, false);
                    }

                    //execute actions
                    oVar.VariableActions.Execute();

                    //update ui
                    if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) ==
                        Variable.ControlHosts.DocumentEditor)
                    {
                        //get associated tree node
                        string xKey = oSegment.FullTagID + "." + oVar.ID;
                        UltraTreeNode oNode = null;
                        try
                        {
                            oNode = this.GetNodeFromTagID(xKey);
                        }
                        catch { }

                        if (oNode != null)
                        {
                            //set value of associated value node
                            oNode.Nodes[0].Text = this.GetVariableUIValue_API(oVar);

                            //clear previous validation image
                            oNode.LeftImages.Clear();

                            //add variable validation image
                            if (!oVar.HasValidValue)
                                oNode.LeftImages.Add(Images.InvalidVariableValue);
                            else if (oVar.MustVisit)
                                oNode.LeftImages.Add(Images.IncompleteVariable);
                            else
                                oNode.LeftImages.Add(Images.ValidVariableValue);
                        }
                    }
                }
            }

            //execute segment action
            oSegment.Actions.Execute(Segment.Events.AfterLanguageUpdated);

            //update tags
            UpdateTreeNodeTags(oSegment);

            //update child segments
            int iCulture = oSegment.Culture;
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                Segment oChild = oSegment.Segments[i];
                //GLOG #4219 - dcf - set culture for all collection
                //tables regardless of supported languages
                if ((oChild.Culture != iCulture) && 
                    (oChild.LanguageIsSupported(iCulture) || oChild is CollectionTable))
                {
                    oChild.Culture = iCulture;
                    oChild.Nodes.SetItemObjectDataValue(oChild.FullTagID, "Culture",
                        iCulture.ToString());
                    UpdateForLanguage(oChild);
                }
                UpdateTreeNodeTags(oChild);
            }

            LMP.Benchmarks.Print(t0);
        }

        /// <summary>
        /// GLOG : 3303 : JAB
        /// Return the focus to the document when a Block node is activated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_AfterActivate(object sender, NodeEventArgs e)
        {
            if (m_bSuppressTreeActivationHandlers)
                return;

            try
            {
                //GLOG 2165: Don't want to select body when tabbing through tree, only when
                //Clicking on Body node
                if (!m_bHandlingTabPress && this.treeDocContents.ActiveNode != null &&
                    (this.treeDocContents.ActiveNode.Tag is Block ||
                    (this.treeDocContents.ActiveNode.Parent != null &&
                    this.treeDocContents.ActiveNode.Parent.Tag is Block)))
                {
                    //GLOG - 3622 - CEH
                    //get block from ActiveNode.tag
                    Block oBlock = (Block)this.treeDocContents.ActiveNode.Tag;
                    if (oBlock != null && oBlock.IsBody)
                    {
                        //GLOG 5903: Clear current node
                        m_oCurNode = null;
                        //GLOG 2165: If body text is different from default, only select start
                        if (Session.CurrentWordApp.Selection.Text != Expression.Evaluate(oBlock.StartingText, oBlock.Segment, oBlock.ForteDocument))
                        {
                            object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                            Session.CurrentWordApp.Selection.Collapse(ref oDirection);
                        }
                        Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 3303 : JAB
        /// Move focus to the document when a block node is selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeDocContents_MouseClick(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                UltraTreeNode oNode = this.treeDocContents.GetNodeFromPoint(e.X, e.Y);

                if (oNode != null &&
                    (oNode.Tag is Block ||
                    (oNode.Parent != null &&
                    oNode.Parent.Tag is Block)))
                {
                    Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();
                }
            }
        }
        private void RefreshTrailerNodes(bool bExpandTop)
        {
            //GLOG 3246: Add Trailer nodes to tree, combining under umbrella node if more than one
            UltraTreeNode oParentNode = null;
            try
            {
                oParentNode = this.treeDocContents.Nodes[mpTrailersNodeKey];
            }
            catch { }
            //Clear existing Trailers umbrella node
            if (oParentNode != null)
            {
                //Clear current node if it's contained in trailer nodes
                if (m_oCurNode != null && oParentNode.IsAncestorOf(m_oCurNode))
                    m_oCurNode = null;
                oParentNode.Remove();
            }
            oParentNode = null;
            //Clear any existing top level Trailer Nodes
            foreach (UltraTreeNode oNode in this.treeDocContents.Nodes)
            {
                if (oNode.Tag is Segment && ((Segment)oNode.Tag).TypeID == mpObjectTypes.Trailer)
                {
                    //Clear current node if it's contained in trailer nodes
                    if (m_oCurNode != null && oNode.IsAncestorOf(m_oCurNode))
                        m_oCurNode = null;
                    this.DeleteNode(oNode.Key);
                }
            }
            ArrayList oTrailers = new ArrayList();
            //Collect all Trailer Segments
            for (int i = 0; i < m_oMPDocument.Segments.Count; i++)
            {
                Segment oSegment = m_oMPDocument.Segments[i];
                if (oSegment.TypeID == mpObjectTypes.Trailer)
                {
                    //JTS 6/13/13: Use PrimaryRange instead of WordTags or ContentControls collections
                    try
                    {
                        oTrailers.Add(new object[] { oSegment, oSegment.PrimaryRange.Sections[1].Index });
                    }
                    catch { }
                }
            }
            //Sort items by Section index to display in document order, rather than order added
            oTrailers.Sort(new TrailerArraySorter());
            //If more than one Trailer, create umbrella node to contain them
            if (oTrailers.Count > 1)
            {
                oParentNode = this.treeDocContents.Nodes.Add(mpTrailersNodeKey, "Trailers");
            }
            for (int i = 0; i < oTrailers.Count; i++)
            {
                Segment oTrailer = (Segment)((object[])oTrailers[i])[0];
                string xSection = ((object[])oTrailers[i])[1].ToString();
                UltraTreeNode oTreeNode = null;
                //Append Section number to display text
                string xNodeText = LMP.Data.Application.GetObjectTypeDisplayName(oTrailer.TypeID) +
                    " [Section " + xSection + "]";
                if (oParentNode != null)
                {
                    oTreeNode = oParentNode.Nodes.Add(oTrailer.FullTagID, xNodeText);
                }
                else
                {
                    oTreeNode = this.treeDocContents.Nodes.Add(oTrailer.FullTagID, xNodeText);
                    oTreeNode.Visible = true;
                }

                //initialize new node
                InitializeNode(oTrailer, oTreeNode, true);
                //populate and expand top-level node
                if (oTrailer.Parent == null && oTrailer.CreationStatus == Segment.Status.Finished)
                {
                    RefreshSegmentNode(oTreeNode, true);
                }
                if (bExpandTop && oTreeNode.Level == 0)
                    oTreeNode.Expanded = true;
            }
        }

        /// <summary>
        /// selects specified content control and forces immediate execution
        /// of exit and enter events
        /// </summary>
        /// <param name="oCC"></param>
        /// <param name="bSelectStart"></param>
        /// <param name="bActivate"></param>
        private void SelectContentControl(Word.ContentControl oCC, bool bSelectStart,
            bool bActivate)
        {
            //get currently selected content control
            Word.ContentControl oSelectionCC = oCC.Application
                .Selection.Range.ParentContentControl;

            //run exit event handler
            if (oSelectionCC != null)
            {
                bool bCancel = false;
                this.OnContentControlExit(oSelectionCC, ref bCancel, true);
            }

            //GLOG 7984: Disable SelectionChange events since we run handler directly afterward
            ForteDocument.WordXMLEvents iIgnore = ForteDocument.IgnoreWordXMLEvents;
            ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.XMLSelectionChange;
            try
            {
            //select content control
            LMP.Forte.MSWord.WordDoc.SelectContentControl(oCC, bSelectStart, bActivate);
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
            finally
            {
                ForteDocument.IgnoreWordXMLEvents = iIgnore;
            }

            //run enter event handler
            this.OnContentControlEnter(oCC, true);
        }
        #region *********************helper classes*********************
        /// <summary>
        /// IComparer for ArrayList of Segment and Section Index items
        /// </summary>
        private class TrailerArraySorter : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                object[] aPref1 = (object[])x;
                object[] aPref2 = (object[])y;

                int iIndex1 = int.Parse(aPref1[1].ToString());
                int iIndex2 = int.Parse(aPref2[1].ToString());

                if (iIndex1 == iIndex2)
                    return 0;
                else if (iIndex1 > iIndex2)
                    return 1;
                else
                    return -1;
            }

            #endregion
        }

        private class CreationFilter : IUIElementCreationFilter
        {
            private static IToolTipItem m_oToolTip = new ToolTipGetter();

            void IUIElementCreationFilter.AfterCreateChildElements(UIElement parent)
            {
                if (parent is ImageUIElement)
                {
                    //set the ToolTipItem of this element.
                    parent.ToolTipItem = m_oToolTip;
                }
            }

            bool IUIElementCreationFilter.BeforeCreateChildElements(UIElement parent)
            {
                return false;
            }

            private class ToolTipGetter : IToolTipItem
            {
                Infragistics.Win.ToolTipInfo IToolTipItem.GetToolTipInfo(System.Drawing.Point mousePosition,
                    Infragistics.Win.UIElement element, Infragistics.Win.UIElement previousToolTipElement,
                    Infragistics.Win.ToolTipInfo toolTipInfoDefault)
                {
                    if (element is ImageUIElement)
                    {
                        // get the Node associated with this image. 
                        UltraTreeNode node = element.GetContext(typeof(UltraTreeNode)) as UltraTreeNode;

                        if (node == null)
                            return toolTipInfoDefault;

                        // Change the tool tip text.				
                        toolTipInfoDefault.ToolTipText = "This is an image. It's text is: " + node.Text;

                        // Position the tooltip over the node
                        toolTipInfoDefault.Location = node.Control.PointToScreen(element.Rect.Location);
                    }

                    return toolTipInfoDefault;
                }
            }
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //TestForm oForm = new TestForm();
                //oForm.ShowDialog();
                return;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GetTopLevelSegmentFromNode(this.treeDocContents.ActiveNode).Activated = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Segment o = GetTopLevelSegmentFromNode(this.treeDocContents.ActiveNode);
            //o.ReviewData("DeliveryPhrases");
        }

        private void tbtnFinish_Click(object sender, EventArgs e)
        {
            try
            {
                //8-18-11 (dm) - exit if no segments
                if (this.ForteDocument.Segments.Count == 0)
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                    this.RefreshTree();

                DateTime t0 = DateTime.Now;

                this.TaskPane.ScreenUpdating = false;

                ExitEditMode();

                Segment oSeg = this.TaskPane.ForteDocument.Segments[0];

                //GLOG : 8041 : ceh
                bool bLabelEnvelopeIsOnlySegment = (oSeg is LMP.Architect.Base.IStaticDistributedSegment && this.ForteDocument.Segments.Count < 2);

                //GLOG 15860 (dm) - different behavior for labels or envelopes
                bool bFinishDocument = !(m_oCurrentSegment is LMP.Architect.Base.IStaticDistributedSegment);
                bool bContainsStaticSegment = bLabelEnvelopeIsOnlySegment || !bFinishDocument;
                if (!bContainsStaticSegment)
                {
                    for (int i = 0; i < this.TaskPane.ForteDocument.Segments.Count; i++)
                    {
                        if (this.TaskPane.ForteDocument.Segments[i] is LMP.Architect.Base.IStaticDistributedSegment)
                        {
                            bContainsStaticSegment = true;
                            break;
                        }
                    }
                }

                if (m_oCurrentSegment is LMP.Architect.Base.IStaticDistributedSegment)
                {
                    //GLOG 15860 (dm) - complete only labels/envelopes
                    LMP.MacPac.Application.FinishSegment(m_oCurrentSegment,false,false);
                }
                else
                {
                    for (int i = 0; i < oSeg.Blocks.Count; i++)
                    {
                        Block oBlock = oSeg.Blocks[i];
                        if (oBlock.IsBody)
                        {
                            //GLOG 6605:  Access range directly, rather than through SelectNode,
                            //in case Block is not displayed in the TaskPane
                            if (oSeg.ForteDocument.FileFormat == mpFileFormats.Binary)
                            {
                                oBlock.AssociatedWordTag.Range.Select();
                            }
                            else
                            {
                                oBlock.AssociatedContentControl.Range.Select();
                            }
                            if (Session.CurrentWordApp.Selection.Text !=
                                Expression.Evaluate(oBlock.StartingText,
                                oBlock.Segment, oBlock.ForteDocument))
                            {
                                object oDirection = Word.WdCollapseDirection.wdCollapseStart;
                                Session.CurrentWordApp.Selection.Collapse(ref oDirection);
                            }
                            //SelectNode(this.GetNodeFromTagID(oBlock.TagID));
                            break;
                        }
                    }
                    //finish document - no need to refresh during Finish
                    //as task pane has been updating the ForteDocument
                    ////GLOG 7874:  Make sure Taskpane is not redisplayed for Envelopes and Labels
                    //LMP.MacPac.Application.DisableTaskpaneEvents = true;
                    //GLOG 6811: Need Refresh to update Tags collection after deleting Variable tags
                    if (!LMP.MacPac.Application.FinishDocument(this.ForteDocument, false))
                        return;
                }

                //GLOG 7874: Check first before accessing TaskPane
                //label/envelope may have been output to new document
                //and closed original document
                string xDocName = null;

                try
                {
                    xDocName = this.ForteDocument.WordDocument.Name;
                }
                catch { }

                //GLOG : 6049 : JSW
                //GLOG 15860 (dm) - don't close if not all segments will be finished/completed
                if ((LMP.MacPac.Session.CurrentUser.UserSettings.CloseTaskPaneOnFinish && !bContainsStaticSegment) ||
                    bLabelEnvelopeIsOnlySegment) //GLOG 7874: Always close TaskPane if Envelope/Label is only Segment
                {
                    //GLOG 7874: After finishing Envelopes or Labels, current TaskPane may not be the original object
                    TaskPane oCurTP = null;
                    if (xDocName != null)
                    {
                        oCurTP = this.TaskPane;
                    }
                    else
                    {
                        oCurTP = TaskPanes.Item(Session.CurrentWordApp.ActiveDocument);
                    }
                    //Taskpane may not exist in Tools mode
                    if (oCurTP != null)
                    {
                        //GLOG 7874:  Set default tab to Content Manager for Envelopes and Labels
                        if (bLabelEnvelopeIsOnlySegment)
                            oCurTP.SelectTab(MacPac.TaskPane.Tabs.Content);
                        oCurTP.WordTaskPane.Visible = false;
                        //Don't automatically show TaskPane on Document Change
                        oCurTP.AutoClosed = false;
                    }
                }

                if (xDocName != null)
                {
                    this.TaskPane.ScreenUpdating = false;
                    //GLOG 7874: Don't display Data Reviewer for Envelopes/Labels
                    //GLOG 15860 (dm): display only when finishing
                    if (bFinishDocument)
                        this.TaskPane.DataReviewerVisible = true;

                    LMP.MacPac.Application.DisableTaskpaneEvents = true; //Prevent DocumentChange running at this point
                    this.TaskPane.SuspendLayout();
                    this.TaskPane.Refresh(false, true, false); //GLOG 6811:  Refresh TaskPane
                    //this.RefreshTree(false, true);
                    //GLOG 5821 (dm) - expand top level nodes - this fixes errors
                    //resulting from not being aware of which nodes are transparent
                    //and is also consistent with the way the editor will appear
                    //after closing and reopening the document
                    this.CollapseAll(null);
                    this.ExpandTopLevelNodes();
                    this.TaskPane.ScreenUpdating = true;
                    this.TaskPane.ResumeLayout();
                    LMP.MacPac.Application.DisableTaskpaneEvents = false;
                    //GLOG 15906
                    SendKeys.Send("%");
                    SendKeys.Send("%");
                }
                //GLOG : 8041 : ceh - invalidate ribbon to allow for hiding of Label/Envelope ribbon tab
                if (bLabelEnvelopeIsOnlySegment)
                    LMP.MacPac.Application.MacPac10Ribbon.Invalidate();

                //GLOG : 6369 : CEH
                Session.CurrentWordApp.ActiveDocument.ActiveWindow.SetFocus();

                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
                //LMP.MacPac.Application.DisableTaskpaneEvents = false;
            }
        }

        private void tbtnHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

        private void tbtnSaveDataAs_Click(object sender, EventArgs e)
        {
            try
            {
                bool bRet = false;

                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                    this.RefreshTree();

                ExitEditMode();

                Segment oSeg = this.TaskPane.GetTargetSegment();

                if (oSeg != null)
                {
                    //create Saved Data from segment
                    bRet = this.SaveData(oSeg);
                }

                object oFalse = false;
                object oMissing = System.Reflection.Missing.Value;

                if (bRet)
                {
                    if (oSeg.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                        this.ForteDocument.WordDocument.Saved = true;

                    //this.ForteDocument.WordDocument.Close(ref oFalse, ref oMissing, ref oMissing);

                    TaskPanes.RefreshAll(true, false, false);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnInsertSavedData_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                    this.RefreshTree();

                //GLOG 5790: Apply changes for current control
                ExitEditMode();

                Segment oSeg = this.TaskPane.GetTargetSegment();

                if (oSeg == null)
                    return;

                //prompt for saved data
                PrefillChooser oForm = new PrefillChooser(LMP.Resources.GetLangString("Dialog_ApplyPrefill"));

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    string xVSetID = oForm.VariableSetID;
                    VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, Session.CurrentUser.ID);
                    VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(xVSetID);
                    //GLOG 3475
                    Prefill oPrefill = new Prefill(oDef.ContentString, oDef.Name, oDef.SegmentID);
                    System.Windows.Forms.Application.DoEvents();
                    this.TaskPane.ScreenUpdating = false;

                    //get current node for future reselection
                    UltraTreeNode oNode = m_oCurNode;
                    //GLOG 5790: Clear m_oCurNode so that Control will be positioned properly when reselected
                    m_oCurNode = null;

                    //GLOG 6313
                    if (oNode != null && oNode.Parent != null)
                        SelectNode(oNode.Parent);

                    //GLOG item #5953 - dcf
                    int iShowTags = this.ForteDocument.WordDocument.ActiveWindow.View.ShowXMLMarkup;

                    oSeg.ApplyPrefill(oPrefill);

                    //GLOG : 6987 : CEH
                    //Insert Data now initializes authors, so we need to insure people list is accurate.
                    Application.RefreshCurrentAuthorControls();

                    //update author display name if necessary
                    if (oNode == null || !(oNode.Tag is Authors))
                    {
                        if ((oSeg.IsTopLevel || (!oSeg.LinkAuthorsToParent))
                           && (oSeg.MaxAuthors > 0))
                        {
                            UltraTreeNode oAuthorNode = null;
                            try
                            {
                                oAuthorNode = this.GetNodeFromTagID(oSeg.FullTagID + ".Authors");
                            }
                            catch { }

                            if (oAuthorNode != null)
                            {
                                //get authors
                                Authors oAuthors = (Authors)oAuthorNode.Tag;
                                //set author display name
                                oAuthorNode.Nodes[0].Text = this.GetAuthorsUIValue(oAuthors);
                            }
                        }
                    }
                    //GLOG 8084
                    UpdateLanguageDisplayValue(oSeg);

                    this.ForteDocument.WordDocument.ActiveWindow.View.ShowXMLMarkup = iShowTags;
                    //GLOG 6993:  Select top node first to ensure first nodes are visible
                    SelectNode(treeDocContents.Nodes[0]);

                    //GLOG 5790
                    if (oNode != null)
                        //GLOG 6993: Nodes will have been refreshed, so select by Key
                        SelectNode(oNode.Key);
                    else
                        //GLOG 6313: If no current Node at start, select top-level node for segment
                        SelectFirstNodeForSegment(oSeg);
                    //EnterEditMode(oNode);

                    this.TaskPane.ScreenRefresh();
                }

            }
            catch (System.Exception oE)
            {
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
            }
        }
        internal void UpdateLanguageDisplayValue(Segment oSeg)
        {
            //GLOG 8084: Update Language displayed in Tree to match current Segment language
            try
            {
                if (oSeg.IsTopLevel && oSeg.SupportsMultipleLanguages)
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        oNode = this.treeDocContents.Nodes[oSeg.FullTagID].Nodes[oSeg.FullTagID + ".Language"].Nodes[oSeg.FullTagID + ".Language_Value"];
                    }
                    catch { }
                    if (oNode != null)
                    {
                        string xCulture = oSeg.Culture.ToString();
                        oNode.Text = LMP.Data.Application.GetLanguagesDisplayValue(xCulture);
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }
        private void tbtnRecreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                    this.RefreshTree();

                ExitEditMode();

                Segment oSeg = this.TaskPane.GetTargetSegment();

                if (oSeg != null)
                {
                    //recreate segment
                    this.RecreateSegment(oSeg);
                }

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6115 : CEH
        void mnuPleadingCounselsOptions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Segment oSeg = (Segment)m_oCurNode.Tag;
                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSeg.ForteDocument.WordDocument);
                switch (e.ClickedItem.Name)
                {
                    case "m_oMenuSegment_PleadingCounsel_Add":
                        this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingCounsel, true);
                        break;
                    case "m_oMenuSegment_PleadingCounsel_ChangeType":
                        if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                            return;

                        oTP.ChangePleadingCounselType(oSeg);
                        break;
                    case "m_oMenuSegment_PleadingCounsel_Edit":
                        oTP.SelectChild(
                            mpObjectTypes.PleadingCounsel, oSeg);
                        break;
                    case "m_oMenuSegment_PleadingCounsel_Layout":
                        this.EditCollectionTableLayout(oSeg, mpObjectTypes.PleadingCounsel);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void mnuPleadingCaptionsOptions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Segment oSeg = (Segment)m_oCurNode.Tag;
                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSeg.ForteDocument.WordDocument);
                switch (e.ClickedItem.Name)
                {
                    case "m_oMenuSegment_PleadingCaption_Add":
                        this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingCaption, true);
                        break;
                    case "m_oMenuSegment_PleadingCaption_ChangeType":
                        if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                            return;

                        oTP.ChangePleadingCaptionType(oSeg);
                        break;
                    case "m_oMenuSegment_PleadingCaption_Edit":
                        oTP.SelectChild(
                            mpObjectTypes.PleadingCaption, oSeg);
                        break;
                    case "m_oMenuSegment_PleadingCaption_Layout":
                        this.EditCollectionTableLayout(oSeg, mpObjectTypes.PleadingCaption);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6160 : CEH
        void mnuPleadingSignaturesOptions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Segment oSeg = (Segment)m_oCurNode.Tag;
                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSeg.ForteDocument.WordDocument);
                switch (e.ClickedItem.Name)
                {
                    case "m_oMenuSegment_PleadingSignature_Add":
                        this.AddCollectionTableItem(oSeg, mpObjectTypes.PleadingSignature, true);
                        break;
                    case "m_oMenuSegment_Pleading_AddNonTableSignature":
                        this.AddNonCollectionChildSegment(oSeg, mpObjectTypes.PleadingSignatureNonTable, true);
                        break;
                    case "m_oMenuSegment_PleadingSignature_ChangeType":
                        if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                            return;

                        oTP.ChangePleadingSignatureType(oSeg);
                        break;
                    case "m_oMenuSegment_PleadingSignature_Edit":
                        oTP.SelectChild(
                            mpObjectTypes.PleadingSignature, oSeg);
                        break;
                    case "m_oMenuSegment_Pleading_EditNonTableSignature":
                        oTP.SelectChild(
                            mpObjectTypes.PleadingSignatureNonTable, oSeg);
                        break;
                    case "m_oMenuSegment_PleadingSignature_Layout":
                        this.EditCollectionTableLayout(oSeg, mpObjectTypes.PleadingSignature);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void mnuPleadingPaperOptions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Segment oSeg = (Segment)m_oCurNode.Tag;
                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSeg.ForteDocument.WordDocument);
                switch (e.ClickedItem.Name)
                {
                    case "m_oMenuSegment_PleadingPaper_ChangeType":
                        bool bChanged = LMP.MacPac.Application.ChangePleadingPaper(oSeg);

                        if (bChanged)
                        {
                            oTP.SelectChild(mpObjectTypes.PleadingPaper, oSeg);
                        }
                        break;
                    case "m_oMenuSegment_PleadingPaper_Edit":
                        oTP.SelectChild(mpObjectTypes.PleadingPaper, oSeg);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnSaveSegment_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                    this.RefreshTree();

                ExitEditMode();

                Segment oSeg = this.TaskPane.GetTargetSegment();

                if (oSeg != null)
                {
                    LMP.MacPac.Application.SaveSegmentWithData(oSeg);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        //GLOG : 6167 : CEH
        void mnuLetterSignaturesOptions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Segment oSeg = (Segment)m_oCurNode.Tag;
                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSeg.ForteDocument.WordDocument);
                switch (e.ClickedItem.Name)
                {
                    case "m_oMenuSegment_LetterSignature_Add":
                        this.AddCollectionTableItem(oSeg, mpObjectTypes.LetterSignature, true);
                        break;
                    case "m_oMenuSegment_LetterSignature_ChangeType":
                        if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                            return;

                        oTP.ChangeLetterSignatureType(oSeg);
                        break;
                    case "m_oMenuSegment_LetterSignature_Edit":
                        oTP.SelectChild(
                            mpObjectTypes.LetterSignature, oSeg);
                        break;
                    case "m_oMenuSegment_LetterSignature_Layout":
                        this.EditCollectionTableLayout(oSeg, mpObjectTypes.LetterSignature);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6167 : CEH
        void mnuLetterheadOptions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Segment oSeg = (Segment)m_oCurNode.Tag;
                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSeg.ForteDocument.WordDocument);
                switch (e.ClickedItem.Name)
                {
                    case "m_oMenuSegment_Letterhead_ChangeType":
                        if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                            return;

                        oTP.ChangeLetterheadType(oSeg);
                        break;
                    case "m_oMenuSegment_Letterhead_Edit":
                        oTP.SelectChild(
                            mpObjectTypes.Letterhead, oSeg);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6170 : CEH
        void mnuAgreementSignaturesOptions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                Segment oSeg = (Segment)m_oCurNode.Tag;
                LMP.MacPac.TaskPane oTP = LMP.MacPac.TaskPanes.Item(oSeg.ForteDocument.WordDocument);
                switch (e.ClickedItem.Name)
                {
                    case "m_oMenuSegment_AgreementSignature_Add":
                        this.AddCollectionTableItem(oSeg, mpObjectTypes.AgreementSignature, true);
                        break;
                    case "m_oMenuSegment_AgreementSignature_ChangeType":
                        if (!oTP.ForteDocument.MacPacFunctionalityIsAvailable)
                            return;

                        oTP.ChangeAgreementSignatureType(oSeg);
                        break;
                    case "m_oMenuSegment_AgreementSignature_Edit":
                        oTP.SelectChild(
                            mpObjectTypes.AgreementSignature, oSeg);
                        break;
                    case "m_oMenuSegment_AgreementSignature_Layout":
                        this.EditCollectionTableLayout(oSeg, mpObjectTypes.AgreementSignature);
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnSaveData_Click(object sender, EventArgs e)
        {
            try
            {
                bool bRet = false;

                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                //GLOG 6829 (dm) - refresh if any content is missing
                if (this.ForteDocument.Segments.ContentIsMissing)
                    this.RefreshTree();

                ExitEditMode();

                Segment oSeg = this.TaskPane.GetTargetSegment();

                //GLOG : 7223 : CEH
                if (oSeg != null)
                {
                    if (oSeg.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm &&
                        oSeg.Prefill != null)
                    {
                        //we're editing existing master data
                        SaveMasterData(oSeg);
                        bRet = true;
                    }
                    else
                    {
                        //create Saved Data from segment
                        bRet = this.SaveData(oSeg);
                    }
                }

                if (bRet)
                {
                    if (oSeg.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                        this.ForteDocument.WordDocument.Saved = true;

                    TaskPanes.RefreshAll(true, false, false);
                }

                //object oFalse = false;
                //object oMissing = System.Reflection.Missing.Value;

                //if(bRet)
                //    this.ForteDocument.WordDocument.Close(ref oFalse, ref oMissing, ref oMissing);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void SaveMasterData(Segment oSegment)
        {
            ExitEditMode();

            VariableSetDefs oDefs = new VariableSetDefs(
                mpVariableSetsFilterFields.User, LMP.Data.Application.User.ID);
            VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(
                oSegment.Prefill.VariableSetID);
            Prefill oPrefill = new Prefill(oSegment);
            oDef.ContentString = oPrefill.ToString();

            oDefs.Save(oDef);
        }

        private void tbtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                object oMissing = System.Reflection.Missing.Value;
                object oFalse = false;

                ExitEditMode();

                if (!this.ForteDocument.WordDocument.Saved)
                {
                    if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                        return;

                    Segment oSeg = this.TaskPane.GetTargetSegment();

                    if (oSeg.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm &&
                        !this.ForteDocument.WordDocument.Saved)
                        {
                        try
                        {
                            this.ForteDocument.WordDocument.Close(ref oMissing, ref oMissing, ref oMissing);
                        }
                        catch { }
                    }
                }
                else
                {
                    try
                    {
                        this.ForteDocument.WordDocument.Close(ref oFalse, ref oMissing, ref oMissing);
                    }
                    catch { }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void PromptToCloseMasterDataForm()
        {
            object oFalse = false;
            object oTrue = true;
            object oMissing = System.Reflection.Missing.Value;

            DialogResult iRet = MessageBox.Show(LMP.Resources.GetLangString("Prompt_SaveMasterData"),
                LMP.ComponentProperties.ProductName,
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);

            if (iRet == DialogResult.Yes)
            {
                //SaveMasterData(this.ForteDocument.Segments[0]);
                bool bRet = false;
                Segment oSeg = this.TaskPane.GetTargetSegment();

                if (oSeg.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm &&
                    oSeg.Prefill != null)
                {
                    SaveMasterData(oSeg);
                    bRet = true;
                }
                else
                {
                    if (oSeg != null)
                    {
                        //create Saved Data from segment
                        bRet = this.SaveData(oSeg);
                    }
                }

                if (bRet)
                {
                    if (oSeg.IntendedUse == mpSegmentIntendedUses.AsMasterDataForm)
                        this.ForteDocument.WordDocument.Saved = true;

                    TaskPanes.RefreshAll(true, false, false);
                }

            }

            if (iRet != DialogResult.Cancel)
            {
                try
                {
                    this.ForteDocument.WordDocument.Close(ref oFalse, ref oMissing, ref oMissing);
                }
                catch { }
            }
        }

        void TaskPane_CurrentTopLevelSegmentSwitched(object sender, CurrentSegmentSwitchedEventArgs args)
        {
            //GLOG 15860 (dm)
            if (args.NewSegment.TagID != m_oCurrentSegment.TagID)
            {
                m_oCurrentSegment = args.NewSegment;
                if (!m_bExpandingNode)
                    UpdateButtons(m_oMPDocument);
            }
        }

    }
}
