namespace LMP.MacPac
{
    partial class AuthorPreferencesManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.authorPreferencesManager1 = new LMP.Administration.Controls.AuthorPreferencesManager();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.AutoSize = true;
            this.btnClose.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(505, 479);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 25);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // authorPreferencesManager1
            // 
            this.authorPreferencesManager1.AutoSize = true;
            this.authorPreferencesManager1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.authorPreferencesManager1.BackColor = System.Drawing.Color.White;
            this.authorPreferencesManager1.DisplayTree = false;
            this.authorPreferencesManager1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.authorPreferencesManager1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authorPreferencesManager1.IsValid = true;
            this.authorPreferencesManager1.Location = new System.Drawing.Point(0, 0);
            this.authorPreferencesManager1.Mode = LMP.Administration.Controls.AuthorPreferencesManager.Modes.User;
            this.authorPreferencesManager1.Name = "authorPreferencesManager1";
            this.authorPreferencesManager1.Size = new System.Drawing.Size(592, 532);
            this.authorPreferencesManager1.TabIndex = 0;
            // 
            // AuthorPreferencesManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(592, 532);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.authorPreferencesManager1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthorPreferencesManager";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Author Preferences";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AuthorPreferencesManager_FormClosing);
            this.Load += new System.EventHandler(this.AuthorPreferencesManager_Load);
            this.ResizeEnd += new System.EventHandler(this.AuthorPreferencesManager_ResizeEnd);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LMP.Administration.Controls.AuthorPreferencesManager authorPreferencesManager1;
        private System.Windows.Forms.Button btnClose;






    }
}