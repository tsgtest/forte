using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class LicenseNumberForm : Form
    {
        #region *********************fields*********************
        private bool m_bCancel = true;
        private string m_xKey;
        private string m_xMessage = LMP.Resources.GetLangString("Dialog_ControlPropertyEnterLicenseKey");
        #endregion
        #region ****************constructors********************
        public LicenseNumberForm()
        {
            InitializeComponent();
            this.lblMessage.Text = this.Message;
        }
        #endregion
        #region *********************properties*********************
        public bool Cancelled
        {
            get { return m_bCancel; }
        }
        public string Key
        {
            get { return m_xKey; }
        }
        public string Message
        {
            get { return m_xMessage; }
            set { m_xMessage = value; }
         
        }
        #endregion
        #region *********************events*********************
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                m_bCancel = true;
                m_xKey = null;
                this.Close();
                
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.Close();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                m_bCancel = false;
                m_xKey = this.txtLicenseKey.Text;
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
         #endregion

        private void txtLicenseKey_TextChanged(object sender, EventArgs e)
        {
            if (this.txtLicenseKey.Text == "")
            {
                this.btnOK.Enabled = false;
            }
            else
            {
                this.btnOK.Enabled = true;
            }
        }

    }
}