using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class RecreateOptionsForm : Form
    {
        private BodyTypes m_iBodyType = BodyTypes.Standard;
        private FormType m_oType;

        public enum FormType
        {
            RecreateMacPac10Segment = 1,
            RecreateLegacyDocument = 2
        }
        public enum Locations
        {
            NewDocument = 1,
            ReplaceExisting = 2
        }
        public enum TextOptions
        {
            NoText = 1,
            IncludeSelectedText = 2,
            IncludeEntireBody = 3
        }
        public enum BodyTypes
        {
            None = 0,
            Standard = 1,
            Message = 2
        }

        public RecreateOptionsForm(FormType oType)
        {
            InitializeComponent();
            this.m_oType = oType;

            if (this.m_oType == FormType.RecreateLegacyDocument)
            {
                this.optEntireBody.Enabled = false;
                //this.optNoText.Top = this.optEntireBody.Top;
                this.Text = LMP.Resources.GetLangString("Dlg_RecreateLegacyDocument");
                this.optNoText.Checked = true;
                this.optSelectedText.Enabled = false;
            }
        }

        /// <summary>
        /// returns the selected insertion location
        /// </summary>
        public Locations InsertionLocation
        {
            get
            {
                if (this.optNewDocument.Checked)
                    return Locations.NewDocument;
                else
                    return Locations.ReplaceExisting;
            }
        }

        /// <summary>
        /// returns the text option selected
        /// </summary>
        public TextOptions TextOption
        {
            get
            {
                if (this.optNoText.Checked)
                    return TextOptions.NoText;
                else if (this.optSelectedText.Checked)
                    return TextOptions.IncludeSelectedText;
                else
                    return TextOptions.IncludeEntireBody;
            }
        }

        /// <summary>
        /// Enable the "Use selected text" option. If no text is selected prior to 
        /// choosing the Recreate button/menu item, the "Use selected text" option 
        /// can be disabled using this property.
        /// </summary>
        public bool UseSelectedTextOption
        {
            get
            {
                return this.optSelectedText.Enabled;
            }

            set
            {
                this.optSelectedText.Visible = true;
                this.optSelectedText.Enabled = value;

                if (value)
                {
                    this.optSelectedText.Checked = true;
                    this.optSelectedText.Enabled = true;

                    //// GLOG : 3045 : JAB
                    //// Move the optNoText radio button up when text is selected.
                    //if (this.m_oType == FormType.RecreateLegacyDocument)
                    //{
                    //    this.optNoText.Location = this.optEntireBody.Location;
                    //}
                    //else
                    //{
                    //    this.optNoText.Top = 80;
                    //}
                }
                else
                {
                    //this.optNoText.Top = this.optSelectedText.Top;
                    this.optSelectedText.Enabled = false;

                    if (this.optSelectedText.Checked)
                    {
                        this.optNoText.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Get/Set the property which determine if the options captions are for
        /// is for a message or a body.
        /// </summary>
        public BodyTypes BodyType
        {
            get
            {
                return this.m_iBodyType;
            }

            set
            {
                this.m_iBodyType = value;
            }
        }

        private void RecreateOptionsForm_Load(object sender, EventArgs e)
        {
            if (this.BodyType == BodyTypes.None)
            {
                if (Session.CurrentWordApp.Selection.Start ==
                    Session.CurrentWordApp.Selection.End)
                {
                    //segment doesn't have any body or message - 
                    //hide body inclusion options groupbox and resize
                    //this.groupBox1.Visible = false;
                    //this.Width = 206;
                    //this.Height = 190;
                    //this.grpLocation.Width = 176;
                    //this.grpLocation.Height = 94;
                    //this.btnOK.Top = 120;
                    //this.btnCancel.Top = 120;
                    this.optEntireBody.Enabled = false;
                    this.optSelectedText.Enabled = false;
                    this.optNoText.Checked = true;
                    this.optNoText.Enabled = false;
                }
                else
                {
                    //there is a selection
                    this.optSelectedText.Enabled = true;
                    this.optSelectedText.Checked = true;
                    this.optEntireBody.Enabled = false;
                }
            }
            if (this.BodyType == BodyTypes.Message)
            {
                this.optEntireBody.Text = LMP.Resources.GetLangString("RecreateOptionsForm_EntireMessage");
                this.optNoText.Text = LMP.Resources.GetLangString("RecreateOptionsForm_NoMessage");
            }

            //GLOG item #4505 - dcf
            if (Session.CurrentWordApp.Selection.Start == 
                Session.CurrentWordApp.Selection.End)
            {
                this.optSelectedText.Enabled = false;
                this.optNoText.Checked = true;
            }
        }
    }
}