﻿namespace LMP.MacPac
{
    partial class CustomInterrogatoryHeadingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtCustomResponse = new System.Windows.Forms.TextBox();
            this.txtCustomQuestion = new System.Windows.Forms.TextBox();
            this.lblCustomResponse = new System.Windows.Forms.Label();
            this.lblCustomQuestion = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(232, 98);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(73, 25);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(159, 98);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(67, 25);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // txtCustomResponse
            // 
            this.txtCustomResponse.Location = new System.Drawing.Point(79, 57);
            this.txtCustomResponse.Name = "txtCustomResponse";
            this.txtCustomResponse.Size = new System.Drawing.Size(221, 20);
            this.txtCustomResponse.TabIndex = 3;
            this.txtCustomResponse.TextChanged += new System.EventHandler(this.txtCustomResponse_TextChanged);
            // 
            // txtCustomQuestion
            // 
            this.txtCustomQuestion.Location = new System.Drawing.Point(79, 22);
            this.txtCustomQuestion.Name = "txtCustomQuestion";
            this.txtCustomQuestion.Size = new System.Drawing.Size(221, 20);
            this.txtCustomQuestion.TabIndex = 1;
            this.txtCustomQuestion.TextChanged += new System.EventHandler(this.txtCustomQuestion_TextChanged);
            // 
            // lblCustomResponse
            // 
            this.lblCustomResponse.AutoSize = true;
            this.lblCustomResponse.Location = new System.Drawing.Point(8, 60);
            this.lblCustomResponse.Name = "lblCustomResponse";
            this.lblCustomResponse.Size = new System.Drawing.Size(58, 13);
            this.lblCustomResponse.TabIndex = 2;
            this.lblCustomResponse.Text = "&Response:";
            // 
            // lblCustomQuestion
            // 
            this.lblCustomQuestion.AutoSize = true;
            this.lblCustomQuestion.Location = new System.Drawing.Point(8, 25);
            this.lblCustomQuestion.Name = "lblCustomQuestion";
            this.lblCustomQuestion.Size = new System.Drawing.Size(52, 13);
            this.lblCustomQuestion.TabIndex = 0;
            this.lblCustomQuestion.Text = "&Question:";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.AutoSize = true;
            this.btnClear.Location = new System.Drawing.Point(11, 98);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(67, 25);
            this.btnClear.TabIndex = 6;
            this.btnClear.TabStop = false;
            this.btnClear.Text = "&Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // CustomInterrogatoryHeadingsForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(317, 133);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.txtCustomResponse);
            this.Controls.Add(this.txtCustomQuestion);
            this.Controls.Add(this.lblCustomResponse);
            this.Controls.Add(this.lblCustomQuestion);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomInterrogatoryHeadingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Custom Question/Response";
            this.Activated += new System.EventHandler(this.CustomInterrogatoryHeadingsForm_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox txtCustomResponse;
        private System.Windows.Forms.TextBox txtCustomQuestion;
        private System.Windows.Forms.Label lblCustomResponse;
        private System.Windows.Forms.Label lblCustomQuestion;
        private System.Windows.Forms.Button btnClear;
    }
}