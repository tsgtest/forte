namespace LMP.MacPac
{
    partial class DocumentStampUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentStampUI));
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.gbOptions = new System.Windows.Forms.GroupBox();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.lblTypes = new System.Windows.Forms.Label();
            this.btnOptions = new System.Windows.Forms.Button();
            this.lstTypes = new LMP.Controls.ListBox();
            this.gbApplyTo = new System.Windows.Forms.GroupBox();
            this.txtMultiple = new System.Windows.Forms.TextBox();
            this.rbMultiple = new System.Windows.Forms.RadioButton();
            this.rbSection = new System.Windows.Forms.RadioButton();
            this.rbDocument = new System.Windows.Forms.RadioButton();
            this.gbOptions.SuspendLayout();
            this.gbApplyTo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(407, 324);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(81, 25);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            this.btnOK.Enter += new System.EventHandler(this.btnOK_Enter);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(494, 324);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 25);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemove.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnRemove.Location = new System.Drawing.Point(12, 358);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(105, 29);
            this.btnRemove.TabIndex = 6;
            this.btnRemove.Text = "&Remove Existing";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Visible = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // gbOptions
            // 
            this.gbOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbOptions.AutoSize = true;
            this.gbOptions.Controls.Add(this.pnlControls);
            this.gbOptions.Location = new System.Drawing.Point(348, 13);
            this.gbOptions.Name = "gbOptions";
            this.gbOptions.Size = new System.Drawing.Size(230, 305);
            this.gbOptions.TabIndex = 2;
            this.gbOptions.TabStop = false;
            this.gbOptions.Text = "Options";
            // 
            // pnlControls
            // 
            this.pnlControls.AutoScroll = true;
            this.pnlControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlControls.Location = new System.Drawing.Point(3, 18);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(224, 284);
            this.pnlControls.TabIndex = 12;
            // 
            // lblTypes
            // 
            this.lblTypes.AutoSize = true;
            this.lblTypes.Location = new System.Drawing.Point(9, 13);
            this.lblTypes.Name = "lblTypes";
            this.lblTypes.Size = new System.Drawing.Size(87, 15);
            this.lblTypes.TabIndex = 0;
            this.lblTypes.Text = "&Available Types:";
            // 
            // btnOptions
            // 
            this.btnOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOptions.AutoSize = true;
            this.btnOptions.Location = new System.Drawing.Point(7, 324);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(81, 25);
            this.btnOptions.TabIndex = 7;
            this.btnOptions.Text = "< Options >";
            this.btnOptions.UseVisualStyleBackColor = true;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // lstTypes
            // 
            this.lstTypes.AllowEmptyValue = false;
            this.lstTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTypes.FormattingEnabled = true;
            this.lstTypes.HorizontalScrollbar = true;
            this.lstTypes.IntegralHeight = false;
            this.lstTypes.IsDirty = false;
            this.lstTypes.ItemHeight = 15;
            this.lstTypes.ListName = "";
            this.lstTypes.Location = new System.Drawing.Point(7, 30);
            this.lstTypes.Name = "lstTypes";
            this.lstTypes.Size = new System.Drawing.Size(335, 154);
            this.lstTypes.SupportingValues = "";
            this.lstTypes.TabIndex = 1;
            this.lstTypes.Tag2 = null;
            this.lstTypes.Value = "";
            this.lstTypes.SelectedIndexChanged += new System.EventHandler(this.lstTypes_SelectedIndexChanged);
            // 
            // gbApplyTo
            // 
            this.gbApplyTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbApplyTo.Controls.Add(this.txtMultiple);
            this.gbApplyTo.Controls.Add(this.rbMultiple);
            this.gbApplyTo.Controls.Add(this.rbSection);
            this.gbApplyTo.Controls.Add(this.rbDocument);
            this.gbApplyTo.Location = new System.Drawing.Point(7, 190);
            this.gbApplyTo.Name = "gbApplyTo";
            this.gbApplyTo.Size = new System.Drawing.Size(335, 127);
            this.gbApplyTo.TabIndex = 8;
            this.gbApplyTo.TabStop = false;
            this.gbApplyTo.Text = "Apply To";
            // 
            // txtMultiple
            // 
            this.txtMultiple.Enabled = false;
            this.txtMultiple.Location = new System.Drawing.Point(32, 90);
            this.txtMultiple.Name = "txtMultiple";
            this.txtMultiple.Size = new System.Drawing.Size(172, 22);
            this.txtMultiple.TabIndex = 10;
            this.txtMultiple.WordWrap = false;
            // 
            // rbMultiple
            // 
            this.rbMultiple.AutoSize = true;
            this.rbMultiple.Location = new System.Drawing.Point(14, 67);
            this.rbMultiple.Name = "rbMultiple";
            this.rbMultiple.Size = new System.Drawing.Size(181, 19);
            this.rbMultiple.TabIndex = 5;
            this.rbMultiple.Text = "&Multiple Sections (e.g., 1, 3-5, 7)";
            this.rbMultiple.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbMultiple.UseVisualStyleBackColor = true;
            this.rbMultiple.Click += new System.EventHandler(this.rbMultiple_CheckedChanged);
            // 
            // rbSection
            // 
            this.rbSection.AutoSize = true;
            this.rbSection.Location = new System.Drawing.Point(14, 44);
            this.rbSection.Name = "rbSection";
            this.rbSection.Size = new System.Drawing.Size(101, 19);
            this.rbSection.TabIndex = 4;
            this.rbSection.Text = "Current &Section";
            this.rbSection.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbSection.UseVisualStyleBackColor = true;
            // 
            // rbDocument
            // 
            this.rbDocument.AutoSize = true;
            this.rbDocument.Checked = true;
            this.rbDocument.Location = new System.Drawing.Point(14, 21);
            this.rbDocument.Name = "rbDocument";
            this.rbDocument.Size = new System.Drawing.Size(105, 19);
            this.rbDocument.TabIndex = 4;
            this.rbDocument.TabStop = true;
            this.rbDocument.Text = "Entire &Document";
            this.rbDocument.UseVisualStyleBackColor = true;
            // 
            // DocumentStampUI
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(588, 358);
            this.Controls.Add(this.gbApplyTo);
            this.Controls.Add(this.btnOptions);
            this.Controls.Add(this.lstTypes);
            this.Controls.Add(this.gbOptions);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblTypes);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DocumentStampUI";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert/Remove Draft Stamp/Watermark";
            this.Load += new System.EventHandler(this.DocumentStampUI_Load);
            this.gbOptions.ResumeLayout(false);
            this.gbApplyTo.ResumeLayout(false);
            this.gbApplyTo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.GroupBox gbOptions;
        private System.Windows.Forms.Label lblTypes;
        private LMP.Controls.ListBox lstTypes;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.Button btnOptions;
        private System.Windows.Forms.GroupBox gbApplyTo;
        private System.Windows.Forms.RadioButton rbMultiple;
        private System.Windows.Forms.RadioButton rbSection;
        private System.Windows.Forms.RadioButton rbDocument;
        private System.Windows.Forms.TextBox txtMultiple;
    }
}