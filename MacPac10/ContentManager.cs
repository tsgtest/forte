using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Infragistics.Win;
using Infragistics.Win.UltraWinTree;
using LMP.Architect.Api;
using LMP.Controls;
using LMP.Data;
using LMP.Grail;
using Microsoft.Office.Interop.Word;

namespace LMP.MacPac
{
    /// <summary>
    /// Summary description for ContentManager.
    /// ********************************************************************************
    /// NOTES: 
    /// 1) in the tree, node keys have the following forms:
    ///    - For UserFolder or UserFolder, key is [ParentKey] + '\\' + [FolderID]
    ///    - For FolderMember, key is 'U' + [ParentFolderKey] + '\\' + [MemberID] for UserSegment,
    ///      'A' + [ParentFolderKey] + '\\' + [MemberID] for AdminSegment
    /// 2) tree node tags contain the following object references:
    ///    - Folder Member node tags contain a ref to the FolderMem ber
    ///    - User Folder nodes contain a ref to the UserFolder
    ///    - Admin Folder nodes contain a ref to the Folder
    /// ********************************************************************************
    /// </summary>
    public partial class ContentManager : System.Windows.Forms.UserControl
    {
        #region *********************fields*********************
        private static string m_xDefaultFolder;
        private TaskPane m_oTaskPane;
        private ForteDocument m_oMPDocument;
        private UltraTreeNode m_oCurNode = null;
        private UltraTreeNode m_oCurFindNode = null;
        private UserFolders m_oUserFolders;
        private Folders m_oAdminFolders;
        private FolderMenuItems m_iFolderMenuSelection;
        private SegmentMenuItems m_iSegmentMenuSelection;
        private NewSegmentItems m_iNewSegmentSelection;
        private const string mpPathSeparator = "\\\\";
        private UltraTreeNode m_oNodeToPaste;
        private System.Drawing.Rectangle m_oRectForDrag = System.Drawing.Rectangle.Empty;
        private UltraTreeNode m_oDraggedNode = null;
        private System.Drawing.Point m_oLastPointOnScreen;
        private bool m_bIgnoreCollapse = false;
        private ArrayList m_aFindResults = null;
        private bool m_bSetup = false;
        private bool m_bMenuShownAlready = false;
        private bool m_bNewSegmentDesigner = false; //GLOG 8376

        private static string m_xBlankSegmentXML = null;
        private static string m_xBlankDocFormatSegmentXML = null;

        // required xml for mpNewSegment bookmark
        private const string m_xBookmarkTag = "<aml:annotation aml:id=\"9999\" w:type=\"Word.Bookmark.Start\"" +
            " w:name=\"mpNewSegment\" w:displacedBySDT=\"prev\"/><aml:annotation aml:id=\"9999\"" +
            " w:type=\"Word.Bookmark.End\" w:displacedBySDT=\"prev\"/>";

        System.Drawing.Point m_oMouseLocation;
        private int m_iNeighboringNodeOffset = 0;
        private static Cursor m_oBetweenCursor = null;
        private const double LOWER_NODE_REGION_COEFFICIENT = 0.30;
        private const double MIDDLE_NODE_REGION_COEFFICIENT = 0.40;
        private const double UPPER_NODE_REGION_COEFFICIENT = 0.30;

        // GLOG : 3292 : JAB
        // Introduce a FirmApplicationSettings object to access
        // the My, Public, and Shared folders help text.
        private FirmApplicationSettings m_oFirmAppSettings;

        // GLOG : 3292 : JAB
        // Introduce fields for the My, Public, and Shared folders help text.
        private string m_xSharedFoldersHelpText;
        private string m_xMyFoldersHelpText;
        private string m_xPublicFoldersHelpText;

        private bool m_bAdvancedSearchRunning = false; //GLOG 8037

        #endregion
        #region *********************constructors*********************
        static ContentManager()
        {
        }

        public ContentManager()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************enumerations*********************
        private enum FolderMoveDirections
        {
            //These match the UltraWinTree.NodePosition constants
            MoveUp = 2,
            MoveDown = 3
        }
        private enum Images
        {
            FolderClosed = 0,
            FolderOpen = 1,
            DocumentSegment = 2,
            SearchResults = 3,
            Prefill = 4,
            ParagraphTextSegment = 5,
            ComponentSegment = 6,
            StyleSheetSegment = 7,
            OwnerFolder = 8,
            LeftArrow = 9,
            SegmentPacket = 10,
            DataPacket = 11,
            WordDocument = 12,
            ExternalContent = 13,
            XLDocument = 14,
            UserDocument = 15,
            UserParagraphText = 16,
            // GLOG : 3041 : JAB
            // Add image index for user styles sheet and user answer file segments.
            UserStyleSheetSegment = 17,
            UserComponentSegment = 18,
            SentenceTextSegment = 19,
            UserSentenceText = 20,
            UserSegmentPacket = 21,
            MasterDataForm = 22
        }
        private enum FolderMenuItems
        {
            None = 0,
            Default = 1,
            SelectDefault = 2,
            RemoveDefault = 3,
            CreateNewFolder = 4,
            CreateNewSegment = 5,
            Paste = 6,
            Delete = 7,
            Properties = 8,
            Refresh = 9,
            ApplyStyles = 10,
            SaveStyles = 11,
            MoveUp = 12,
            MoveDown = 13,
            SetPermissions = 14,
            AddDocument = 15,
            ImportSegment = 16,
            CollapseAllSubfolders = 17
        }
        private enum SegmentMenuItems
        {
            None = 0,
            NewDocument = 1,
            Insert = 2,
            Selection = 3,
            StartOfDocument = 4,
            EndOfDocument = 5,
            StartOfCurrentSection = 6,
            StartOfAllSections = 7,
            EndOfCurrentSection = 8,
            EndOfAllSections = 9,
            SeparateSection = 10,
            KeepHeaders = 11,
            RestartPageNumbering = 12,
            Copy = 13,
            Delete = 14,
            Properties = 15,
            Design = 16,
            CopyStyles = 17,
            Clone = 18,
            InsertSavedPrefill = 19,
            SelectionAsBody = 20,
            Share = 21,
            ApplyStyles = 22,
            StyleSummary = 23,
            InsertTransientPrefill = 24,
            AttachTo = 25, 
            ManageAuthorPreferences = 26,
            CreateExternalContent = 27,
            ConvertToAdminContent = 28,
            UpdateAdminContent = 29,
            SegmentHelp = 30, 
            InsertUsingSelection = 31,
            EditAnswerFile = 32,
            SaveAsUserSegment = 33,
            Export = 34,
            ShowAssignments = 35,
            InsertMultipleusingSavedPrefill = 36,
            AddToFavorites = 37,
            CreateSegmentPacket = 38,
            EditSavedData = 39,
            InsertUsingClientMatterLookup = 40,
            //GLOG : 7276 : jsw
            RemoveFromFavorites = 41,
            //GLOG : 7975 : jsw
            CreateMasterData = 42,
	        //GLOG : 8248 : ceh
            SelectionAsPlainText = 43
        }
        private enum NewSegmentItems
        {
            None = 0,
            SegmentBlank = 1,
            SegmentFromSelection = 2,
            SegmentFromDocument = 3,
            StyleSheetFromDocument = 4,
            AnswerFile = 5,
            SegmentPacket = 6,
            MasterDataForm = 7
        }

        private enum NodeDropLocation
        {
            Above,
            Inside,
            Below
        }
        #endregion
        #region *********************properties*********************
        internal TaskPane TaskPane
        {
            get { return m_oTaskPane; }
            set
            {
                m_oTaskPane = value;
                m_oMPDocument = m_oTaskPane.ForteDocument;
            }
        }
        /// <summary>
        /// accesses current active folder from other task panes
        /// </summary>
        public IFolder ActiveFolder
        {
            get
            {
                if (m_oCurNode.Tag is IFolder)
                    return (IFolder)m_oCurNode.Tag;
                else
                    return (IFolder)m_oCurNode.Parent.Tag;
            }
        }
        /// <summary>
        /// Used to access the internal UserFolders collection
        /// </summary>
        private UserFolders UserFolders
        {
            get
            {
                if (m_oUserFolders == null)
                    m_oUserFolders = new UserFolders();
                return m_oUserFolders;
            }
        }
        /// <summary>
        /// Used to access the internal Folders collection
        /// </summary>
        private Folders AdminFolders
        {
            get
            {
                if (m_oAdminFolders == null)
                    m_oAdminFolders = new Folders();
                return m_oAdminFolders;
            }
        }
        /// <summary>
        /// returns the My Folders folder
        /// </summary>
        public UserFolder PersonalRootFolder
        {
            get
            {
                return (UserFolder)this.treeContent.Nodes[0].Tag;
            }
        }
        /// <summary>
        /// returns the My Folders folder
        /// </summary>
        public Folder PublicRootFolder
        {
            get
            {
                return (Folder)this.treeContent.Nodes[1].Tag;
            }
        }
        /// <summary>
        /// accesses current node
        /// </summary>
        public UltraTreeNode ActiveNode
        {
            get { return m_oCurNode; }
        }
        /// <summary>
        /// returns the cursor used to show drag drop placement
        /// of items in the content manager
        /// </summary>
        private static Cursor BetweenCursor
        {
            get
            {
                if(m_oBetweenCursor == null)
                    m_oBetweenCursor = new Cursor(LMP.Data.Application.AppDirectory + @"\Between.cur");

                return m_oBetweenCursor;
            }
        }
        #endregion
        #region *********************event handlers*********************

        private void ContentManager_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Control && e.KeyCode == Keys.F1)
                {
                    //Ctl+F1 selects the find text box
                    this.txtFindContent.Focus();
                }
                else if (e.Control && e.KeyCode == Keys.F3)
                {
                    this.TaskPane.Toggle();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Tree Node is about to be collapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_BeforeCollapse(object sender, CancelableNodeEventArgs e)
        {

            LMP.Trace.WriteValues("treeContent_BeforeCollapse");
            try
            {
                // Don't automatically collapse node if it was just 
                // expanded programatically
                if (m_bIgnoreCollapse)
                    e.Cancel = true;
            }
            catch { }
            finally
            {
                m_bIgnoreCollapse = false;
            }

        }
        /// <summary>
        /// Sets text in WebBrowser Description of currently selected Folder or Segment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_AfterActivate(object sender, NodeEventArgs e)
        {
            try
            {
                LMP.Trace.WriteValues("treeContent_AfterActivate");

                try
                {
                    m_oCurNode = e.TreeNode;

                    if (m_oCurNode != null)
                    {
                        //Populate reference if necessary
                        ContentTreeHelper.LoadNodeReference(m_oCurNode);

                        // GLOG : 2918 : JAB
                        // Display the content mgr's context menu picture control accordingly.
                        DisplayContextMenuButton(e.TreeNode);
                    }

                    SetBrowserContents(m_oCurNode);
                }

                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                    m_bIgnoreCollapse = false;
                }
            }
            catch(System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Populates nodes under the current node if necessary
        /// before expanding the node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_BeforeExpand(object sender, CancelableNodeEventArgs e)
        {
            try
            {
                LMP.Trace.WriteValues("treeContent_BeforeExpand");
                try
                {
                    // Expand node if it's an Admin or User folder
                    if (e.TreeNode.Tag is Folder || e.TreeNode.Tag is int || e.TreeNode.Tag is Person) 
                        RefreshAdminFolderNode(e.TreeNode);
                    else if (e.TreeNode.Tag is UserFolder || e.TreeNode.Tag is string)
                        RefreshUserFolderNode(e.TreeNode);
                    else e.Cancel = true;
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Loads reference and sets Browser text for currently selected Find Results Node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_AfterActivate(object sender, NodeEventArgs e)
        {
            LMP.Trace.WriteValues("treeResults_AfterActivate");
            try
            {
                if (e.TreeNode.Key == "0") return;

                e.TreeNode.Nodes.Clear();

                ExpandNodeShowSegmentPath(e.TreeNode);

                m_oCurFindNode = e.TreeNode;
                ContentTreeHelper.LoadNodeReference(m_oCurFindNode);
                SetBrowserContents(m_oCurFindNode);

                // GLOG : 3061 : JAB
                // Display the context menu button.
                this.DisplayContextMenuButton(e.TreeNode);
            }

            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// User has double-clicked on a tree node
        /// If node references a FolderMember, insert Segment
        /// using DefaultDoubleClick options
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            LMP.Trace.WriteValues("treeContent_MouseDoubleClick");
            try
            {
                UltraTreeNode oNode = this.treeContent.GetNodeFromPoint(e.X, e.Y);
                if (oNode == null) return;
                m_oCurNode = oNode;
                PerformDoubleClickActionForNode(m_oCurNode);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// User has double-clicked on a tree node
        /// If node references a FolderMember, insert Segment
        /// using DefaultDoubleClick options
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            LMP.Trace.WriteValues("treeResults_MouseDoubleCIick");
            try
            {
                UltraTreeNode oNode = this.treeResults.GetNodeFromPoint(e.X, e.Y);

                if (oNode == null)
                    return;

                if (IsSegmentPathNode(oNode))
                {
                    //GLOG 6197: Double-clicking Path Node selects Segment in specified Folder
                    string xSegKey = oNode.Parent.Key;
                    //clear search
                    this.chkFindContent.Checked = false;
                    this.txtFindContent.Text = "";
                    UltraTreeNode oFolderNode = SelectFolderNodeFromPath(oNode.Text);
                    if (oFolderNode != null)
                    {
                        //GLOG 8037: Search results may be from Shared folder, in which case
                        //Tag will be a UserSegmentDef or VariableSetDef object
                        UltraTreeNode oSegNode = null;
                        for (int i = 0; i < oFolderNode.Nodes.Count; i++)
                        {
                            if (oFolderNode.Nodes[i].Tag is UserSegmentDef)
                            {
                                UserSegmentDef oUdef = oFolderNode.Nodes[i].Tag as UserSegmentDef;
                                if (oUdef.ID == xSegKey.Substring(1))
                                {
                                    oSegNode = oFolderNode.Nodes[i];
                                    break;
                                }
                            }
                            else if (oFolderNode.Nodes[i].Tag is VariableSetDef)
                            {
                                VariableSetDef oVdef = oFolderNode.Nodes[i].Tag as VariableSetDef;
                                if (oVdef.ID == xSegKey.Substring(1))
                                {
                                    oSegNode = oFolderNode.Nodes[i];
                                    break;
                                }
                            }
                            else if (oFolderNode.Nodes[i].Tag is FolderMember)
                            {
                                FolderMember oMember = oFolderNode.Nodes[i].Tag as FolderMember;
                                if (oMember != null)
                                {
                                    string xMemberID = oMember.ObjectID1.ToString();
                                    if (xSegKey.Contains("."))
                                    {
                                        xMemberID = xMemberID + "." + oMember.ObjectID2.ToString();
                                    }
                                    if (xMemberID == xSegKey.Substring(1))
                                    {
                                        oSegNode = oFolderNode.Nodes[i];
                                        break;
                                    }
                                }
                            }
                        }
                        if (oSegNode != null)
                        {
                            oSegNode.Selected = true;
                            treeContent.ActiveNode = oSegNode;
                        }
                    }
                }
                else
                {
                    if (oNode == null || oNode.Key == "0") return;
                    m_oCurFindNode = oNode;
                    PerformDoubleClickActionForNode(m_oCurFindNode);
                    //clear search
                    this.chkFindContent.Checked = false;
                    this.txtFindContent.Text = "";
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Determine if the node clicked in the FindResults tree is a segment path or a segment.
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private bool IsSegmentPathNode(UltraTreeNode oNode)
        {
            // There are three types of node that can be selected: the very top node 
            // (aka the search results node), the segment path node and the segment node. 
            // This method identifies a segment path node by process of elimination, by testing
            // that it is neither the search result node nor the segment node.
            // The search results node has a null parent node.
            // The segment nodes has the search results node as its parent. The search result
            // node can be identified by its text.
            return oNode.Parent != null && oNode.Parent.Text != LMP.Resources.GetLangString("Prompt_SearchResults");
        }

        /// <summary>
        /// User has clicked on the UltraTreeView control
        /// Used to display context menu on right-click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_MouseDown(object sender, MouseEventArgs e)
        {
            LMP.Trace.WriteValues("treeResults_MouseDown");
            try
            {
                DismissActiveMenu();
                m_bIgnoreCollapse = false;
                m_oDraggedNode = null;
                m_oLastPointOnScreen = new System.Drawing.Point(e.X, e.Y);
                m_oRectForDrag = System.Drawing.Rectangle.Empty;
                UltraTreeNode oNode = this.treeResults.GetNodeFromPoint(e.X, e.Y);
                if (oNode == null)
                    return;
                else if (oNode.Key == "0")
                {
                    // Don't select top-level node
                    if (oNode.HasNodes)
                        oNode = oNode.Nodes[0];
                    else
                        return;
                }
                if ((e.Button & MouseButtons.Right) == MouseButtons.Right)
                {
                    this.treeResults.ActiveNode = oNode;
                    oNode.Selected = true;
                    m_oCurFindNode = oNode;
                    DisplayContextMenu(m_oCurFindNode, true, e.X, e.Y);
                }
                else if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                {
                    oNode.Selected = true;
                    this.treeResults.ActiveNode = oNode;
                    Size dragSize = SystemInformation.DragSize;
                    m_oRectForDrag = new System.Drawing.Rectangle(new System.Drawing.Point(e.X
                        - dragSize.Width / 2, e.Y
                        - dragSize.Height / 2), dragSize);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }        /// <summary>
        /// User has clicked on the UltraTreeView control
        /// Used to display context menu on right-click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_MouseDown(object sender, MouseEventArgs e)
        {
            LMP.Trace.WriteValues("treeContent_MouseDown");
            try
            {
                DismissActiveMenu();
                m_bIgnoreCollapse = false;
                m_oDraggedNode = null;
                m_oLastPointOnScreen = new System.Drawing.Point(e.X, e.Y);
                m_oRectForDrag = System.Drawing.Rectangle.Empty;
                UltraTreeNode oNode = this.treeContent.GetNodeFromPoint(e.X, e.Y);
                if (oNode == null)
                {
                    return;
                }

                if ((e.Button & MouseButtons.Right) == MouseButtons.Right)
                {
                    this.treeContent.ActiveNode = oNode;
                    oNode.Selected = true;
                    m_oCurNode = oNode;
                    DisplayContextMenu(m_oCurNode, true, e.X, e.Y);
                }
                else if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
                {
                    oNode.Selected = true;
                    this.treeContent.ActiveNode = oNode;
                    Size dragSize = SystemInformation.DragSize;
                    m_oRectForDrag = new System.Drawing.Rectangle(new System.Drawing.Point(e.X
                        - dragSize.Width / 2, e.Y
                        - dragSize.Height / 2), dragSize);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Display the content mgr menu picture control in the appropriate location.
        /// </summary>
        /// <param name="oNode"></param>
        //private void DisplayContextMenuButton(UltraTreeNode oNode)
        //{
        //    if (this.TaskPane.IsSetUp)
        //    {
        //        //positions dropdown to left of folder
        //        this.picMenuDropdown.Visible = false;
                 
        //        this.picMenuDropdown.Left = this.Left + this.treeContent.Left + oNode.Bounds.X + 4;
        //        this.picMenuDropdown.Top = oNode.Bounds.Y + 33;
        //        this.picMenuDropdown.Visible = true;
        //    }
        //}
        private void DisplayContextMenuButton(UltraTreeNode oNode)
        {
            this.picMenuDropdown.Visible = false;

            if (oNode == null || !oNode.IsInView)
                return;

            // GLOG : 3061 : JAB
            // If the find tree is visible and the node is a path node,
            // hide the drop down button.
            if (this.treeResults.Visible && this.IsSegmentPathNode(oNode))
            {
                return;
            }
            int iMargin;

            // GLOG : 3061 : JAB
            // Get the appropriate margin size based on which tree
            // is visible.
            if (treeContent.Visible)
            {
                iMargin = IsTreeContentVerticalScrollbarVisible ? 17 : 2;
            }
            else
            {
                iMargin = IsTreeResultsVerticalScrollbarVisible ? 17 : 2;
            }

            int iMaxLeft = this.treeContent.Right - iMargin - this.picMenuDropdown.Width;

            //there is a bug in the treecontrol such that oNode.Bounds.X returns the
            //wrong value when the tree is in the process of loading.  when this is the
            //case, all nodes will return an X value less than what it should be.
            //test to see if the first node X value < 0 - if so, the shift has occurred -
            //offset by the amount of the shift
            int iOffset = 0;

            int iFirstNodeX = 0;

            // GLOG : 3061 : JAB
            // Handle displaying the context menu button in the results tree.
            if (this.treeContent.Visible)
            {
                iFirstNodeX = this.treeContent.Nodes[0].Bounds.Left;
            }
            else if(this.treeResults.Visible)
            {
                iFirstNodeX = this.treeResults.Nodes[0].Bounds.Left;
            }

            if (iFirstNodeX < 0)
                iOffset = iFirstNodeX;

            this.picMenuDropdown.Left = oNode.Bounds.X - iOffset + oNode.Bounds.Width + 4;

            if (picMenuDropdown.Left > iMaxLeft)
            {
                picMenuDropdown.Left = iMaxLeft;
            }
            // GLOG : 3061 : JAB
            // Apply the appropriate offset depending on the tree that is visible.
            if (oNode.Control == treeContent)
            {
                iOffset = 0;
            }
            else
            {
                iOffset = 8;
            }

            this.picMenuDropdown.Top = GetNodeYPos(oNode) + this.treeContent.Top + iOffset;
            this.picMenuDropdown.Visible = true;
        }

        /// <summary>
        /// returns the Y position of the specified node
        /// in the tree -  needed because oNode.Bounds.Y
        /// returns the wrong value when tree.ItemHeight = -1
        /// </summary>
        /// <param name="oSelNode"></param>
        /// <returns></returns>
        private int GetNodeYPos(UltraTreeNode oSelNode)
        {
            int iY = 0;
            UltraTreeNode oPrevNode = oSelNode.PrevVisibleNode;

            while (oPrevNode != null && oPrevNode.IsInView)
            {
                iY += oPrevNode.ItemHeightResolved + this.treeContent.Margin.Bottom - 1;
                oPrevNode = oPrevNode.PrevVisibleNode;
            }

            return iY;
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Determine if the treeContent control's vertical scrollbar is visible.
        /// This is used in determining the location of the content mgr menu 
        /// picture control. 
        /// </summary>
        private bool IsTreeContentVerticalScrollbarVisible
        {
            get
            {
                if (treeContent.Visible)
                {
                    UltraTreeNode oLastExposedNode = GetLastExposedNode(treeContent);
                    return !(treeContent.Nodes[0].IsInView && oLastExposedNode.IsInView);
                }
                else
                    return false;
            }
        }


        /// <summary>
        /// GLOG : 3061 : JAB
        /// Determine if the treeResults control's vertical scrollbar is visible.
        /// This is used in determining the location of the content mgr menu 
        /// picture control. 
        /// </summary>
        private bool IsTreeResultsVerticalScrollbarVisible
        {
            get
            {
                if (treeResults.Visible)
                {
                    UltraTreeNode oLastExposedNode = GetLastExposedNode(treeResults);
                    return !(treeResults.Nodes[0].IsInView && oLastExposedNode.IsInView);
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Get the last node that is exposed in the treeContent control. This
        /// is needed to determine if the treeContent's vertical scrollbar is 
        /// visible.
        /// </summary>
        /// <param name="treeContent"></param>
        /// <returns></returns>
        private UltraTreeNode GetLastExposedNode(UltraTree treeContent)
        {
            UltraTreeNode oLastExposedNode = null;

            if(treeContent.Nodes.Count > 0)
            {
                oLastExposedNode = treeContent.Nodes[treeContent.Nodes.Count - 1];
                
                while(oLastExposedNode.HasNodes && oLastExposedNode.Expanded)
                {
                    oLastExposedNode = oLastExposedNode.Nodes[oLastExposedNode.Nodes.Count - 1];
                }
            }

            return oLastExposedNode;
        }

        /// <summary>
        /// Node is switching to Edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_BeforeLabelEdit(object sender, CancelableNodeEventArgs e)
        {
            try
            {
                m_oCurNode = e.TreeNode;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }


        }
        /// <summary>
        /// Get selected item and close menu if item clicked is not one of the checkbox options 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegment_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
 
            try
            {
                switch (e.ClickedItem.Name)
                {
                    case "mnuSegment_CreateNewDocument":
                        m_iSegmentMenuSelection = SegmentMenuItems.NewDocument;
                        break;
                    case "mnuSegment_Insert":
                        m_iSegmentMenuSelection = SegmentMenuItems.Insert;
                        break;
                    case "mnuSegment_Share":
                        m_iSegmentMenuSelection = SegmentMenuItems.Share;
                        break;
                    case "mnuSegment_InsertAtSelection":
                        m_iSegmentMenuSelection = SegmentMenuItems.Selection;
                        break;
		    //GLOG : 8248 : ceh
                    case "mnuSegment_InsertAtSelectionAsPlainText":
                        m_iSegmentMenuSelection = SegmentMenuItems.SelectionAsPlainText;
                        break;
                    case "mnuSegment_InsertStartOfDocument":
                        m_iSegmentMenuSelection = SegmentMenuItems.StartOfDocument;
                        break;
                    case "mnuSegment_InsertEndOfDocument":
                        m_iSegmentMenuSelection = SegmentMenuItems.EndOfDocument;
                        break;
                    case "mnuSegment_InsertStartOfAllSections":
                        m_iSegmentMenuSelection = SegmentMenuItems.StartOfAllSections;
                        break;
                    case "mnuSegment_InsertStartOfThisSection":
                        m_iSegmentMenuSelection = SegmentMenuItems.StartOfCurrentSection;
                        break;
                    case "mnuSegment_InsertEndOfAllSections":
                        m_iSegmentMenuSelection = SegmentMenuItems.EndOfAllSections;
                        break;
                    case "mnuSegment_InsertEndOfThisSection":
                        m_iSegmentMenuSelection = SegmentMenuItems.EndOfCurrentSection;
                        break;
                    case "mnuSegment_SeparateSection":
                        m_iSegmentMenuSelection = SegmentMenuItems.SeparateSection;
                        break;
                    case "mnuSegment_KeepHeadersFooters":
                        m_iSegmentMenuSelection = SegmentMenuItems.KeepHeaders;
                        break;
                    case "mnuSegment_RestartPageNumbering":
                        m_iSegmentMenuSelection = SegmentMenuItems.RestartPageNumbering;
                        break;
                     //GLOG : 7975 : jsw
                    case "mnuSegment_CreateMasterData":
                        m_iSegmentMenuSelection = SegmentMenuItems.CreateMasterData;
                        break;
                   	case "mnuSegment_Copy":
                        m_iSegmentMenuSelection = SegmentMenuItems.Copy;
                        break;
                    case "mnuSegment_AddToFavorites":
                        m_iSegmentMenuSelection = SegmentMenuItems.AddToFavorites;
                        break;
                    //GLOG : 7276 : jsw
                    case "mnuSegment_RemoveFromFavorites":
                        m_iSegmentMenuSelection = SegmentMenuItems.RemoveFromFavorites;
                        break;
                    case "mnuSegment_Clone":
                        m_iSegmentMenuSelection = SegmentMenuItems.Clone;
                        break;
                    //GLOG : 3870 : CEH
                    case "mnuSegment_ShowAssignments":
                        m_iSegmentMenuSelection = SegmentMenuItems.ShowAssignments;
                        break;
                    case "mnuSegment_ConvertToAdminContent":
                        m_iSegmentMenuSelection = SegmentMenuItems.ConvertToAdminContent;
                        break;
                    case "mnuSegment_UpdateAdminContent":
                        m_iSegmentMenuSelection = SegmentMenuItems.UpdateAdminContent;
                        break;
                    case "mnuSegment_Properties":
                        m_iSegmentMenuSelection = SegmentMenuItems.Properties;
                        break;
                    case "mnuSegment_Delete":
                        m_iSegmentMenuSelection = SegmentMenuItems.Delete;
                        break;
                    case "mnuSegment_Design":
                        m_iSegmentMenuSelection = SegmentMenuItems.Design;
                        break;
                    case "mnuSegment_EditAnswerFile":
                        //GLOG 2966
                        m_iSegmentMenuSelection = SegmentMenuItems.EditAnswerFile;
                        break;
                    case "mnuSegment_CreateSegmentPack":
                        m_iSegmentMenuSelection = SegmentMenuItems.CreateSegmentPacket;
                        break;
                    case "mnuSegment_ApplyStyles":
                        m_iSegmentMenuSelection = SegmentMenuItems.ApplyStyles;
                        break;
                    case "mnuSegment_ManageAuthorPreferences":
                        m_iSegmentMenuSelection = SegmentMenuItems.ManageAuthorPreferences;
                        break;
                    case "mnuSegment_CopyStyles":
                        m_iSegmentMenuSelection = SegmentMenuItems.CopyStyles;
                        break;
                    case "mnuSegment_StyleSummary":
                        m_iSegmentMenuSelection = SegmentMenuItems.StyleSummary;
                        break;
                    case "mnuSegment_InsertUsingSavedPrefill":
                        m_iSegmentMenuSelection = SegmentMenuItems.InsertSavedPrefill;
                        break;
                    case "mnuSegment_InsertUsingClientMatterLookup":
                        m_iSegmentMenuSelection = SegmentMenuItems.InsertUsingClientMatterLookup;
                        break;
                    case "mnuSegment_InsertMultipleusingSavedData":
                        m_iSegmentMenuSelection = SegmentMenuItems.InsertMultipleusingSavedPrefill;
                        break;
                    case "mnuSegment_EditSavedData":
                        m_iSegmentMenuSelection = SegmentMenuItems.EditSavedData;
                        break;
                    case "mnuSegment_InsertUsingTransientPrefill":
                        m_iSegmentMenuSelection = SegmentMenuItems.InsertTransientPrefill;
                        break;
                    case "mnuSegment_InsertUsingSelection":
                        m_iSegmentMenuSelection = SegmentMenuItems.InsertUsingSelection;
                        break;
                    case "mnuSegment_SelectionAsBody":
                        m_iSegmentMenuSelection = SegmentMenuItems.SelectionAsBody;
                        break;
                    case "mnuSegment_AttachTo":
                        m_iSegmentMenuSelection = SegmentMenuItems.AttachTo;
                        break;
                    case "mnuSegment_CreateExternalContent":
                        m_iSegmentMenuSelection = SegmentMenuItems.CreateExternalContent;
                        break;
                    case "mnuSegment_Help":
                        m_iSegmentMenuSelection = SegmentMenuItems.SegmentHelp;
                        break;
                    case "mnuSegment_SaveAsUserSegment":
                        m_iSegmentMenuSelection = SegmentMenuItems.SaveAsUserSegment;
                        break;
                    case "mnuSegment_Export":
                        m_iSegmentMenuSelection = SegmentMenuItems.Export;
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Determine item that was clicked after menu closes, and run appropriate code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegment_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            try
            {
                if (this.chkFindContent.Checked)
                {
                    this.treeResults.Refresh();
                    this.treeResults.Focus();
                }
                else
                {
                    this.treeContent.Refresh();
                    this.treeContent.Focus();
                }

                if ((e.CloseReason & ToolStripDropDownCloseReason.ItemClicked) == ToolStripDropDownCloseReason.ItemClicked)
                {
                    UltraTreeNode oNode = null;
                    if (this.treeContent.Visible)
                        oNode = m_oCurNode;
                    else
                        // Find results are displayed
                        oNode = m_oCurFindNode;

                    bool bInsertSelectionAsBody = mnuSegment_SelectionAsBody.Checked;

                    Segment.InsertionBehaviors iInsertBehavior = 0;
                    //GLOG - 3590 - CEH
                    if (mnuSegment_SeparateSection.Checked)
                    {
                        //Get segment def
                        //GLOG - 4050 - CEH
                        ISegmentDef oDef = ContentTreeHelper.GetSegmentDefFromNode(oNode);

                        if ((oDef.DefaultMenuInsertionBehavior &
                             (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection)
                              == (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection)
                        {
                            iInsertBehavior = iInsertBehavior | Segment.InsertionBehaviors.InsertInSeparateNextPageSection;
                        }
                        else if ((oDef.DefaultMenuInsertionBehavior &
                             (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection)
                              == (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection)
                        {
                            iInsertBehavior = iInsertBehavior | Segment.InsertionBehaviors.InsertInSeparateContinuousSection;
                        }
                        else
                        {
                            //GLOG 6275:  Make sure context menu selection is honored
                            iInsertBehavior = iInsertBehavior | Segment.InsertionBehaviors.InsertInSeparateNextPageSection;
                        }
                    }
                    if (mnuSegment_KeepHeadersFooters.Checked)
                        iInsertBehavior = iInsertBehavior | Segment.InsertionBehaviors.KeepExistingHeadersFooters;
                    if (mnuSegment_RestartPageNumbering.Checked)
                        iInsertBehavior = iInsertBehavior | Segment.InsertionBehaviors.RestartPageNumbering;

                    switch (m_iSegmentMenuSelection)
                    {
                        case SegmentMenuItems.NewDocument:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertInNewDocument, iInsertBehavior, null, bInsertSelectionAsBody);
                            break;
                        case SegmentMenuItems.Insert:
                            FolderMember oFolderMem = oNode.Tag as FolderMember;

                            if (oFolderMem != null && oFolderMem.ObjectTypeID == mpFolderMemberTypes.UserSegmentPacket)
                            {
                                UserSegmentDefs oDefs = new UserSegmentDefs();
                                UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(oFolderMem.ObjectID1 + "." + oFolderMem.ObjectID2);
                                InsertMultiple(oDef.XML, null);
                            }
                            else
                            {
                                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oFolderMem.ObjectID1);

                                //GLOG : 8022 : ceh
                                if ((oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile) && (oDef.TypeID != mpObjectTypes.Architect))
                                {
                                    InsertMultiple(oDef.XML, null);
                                }
                                else
                                {
                                    InsertSegmentFromNode(oNode, Segment.InsertionLocations.Default, Segment.InsertionBehaviors.Default, null, bInsertSelectionAsBody);
                                }
                            }
                            break;
                        case SegmentMenuItems.EditSavedData:
                            EditSavedData(oNode);
                            break;
                        case SegmentMenuItems.Selection:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtSelection,
                                iInsertBehavior, null, bInsertSelectionAsBody);
                            break;
			//GLOG : 8248 : ceh
                        case SegmentMenuItems.SelectionAsPlainText:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtSelectionAsPlainText,
                                iInsertBehavior, null, bInsertSelectionAsBody);
                            break;
                        case SegmentMenuItems.StartOfDocument:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtStartOfDocument,
                                iInsertBehavior, null, bInsertSelectionAsBody);
                            break;
                        case SegmentMenuItems.EndOfDocument:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtEndOfDocument,
                                iInsertBehavior, null, bInsertSelectionAsBody);
                            break;
                        case SegmentMenuItems.StartOfAllSections:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtStartOfAllSections,
                                0, null, bInsertSelectionAsBody);
                            break;
                        case SegmentMenuItems.StartOfCurrentSection:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtStartOfCurrentSection,
                                iInsertBehavior, null, bInsertSelectionAsBody);
                            break;
                        case SegmentMenuItems.EndOfAllSections:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtEndOfAllSections,
                                0, null, bInsertSelectionAsBody);
                            break;
                        case SegmentMenuItems.EndOfCurrentSection:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtEndOfCurrentSection,
                                iInsertBehavior, null, bInsertSelectionAsBody);
                            break;
                        //GLOG : 7975 : jsw
                        case SegmentMenuItems.CreateMasterData:
                            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertInNewDocument, 0, null, bInsertSelectionAsBody);
                            break;
                        case SegmentMenuItems.Copy:
                            CopySegmentNode(oNode, true);
                            break;
                        case SegmentMenuItems.AddToFavorites:
                            AddToFavorites(oNode);
                            break;
                        //GLOG : 7276 : jsw
                        case SegmentMenuItems.RemoveFromFavorites:
                            RemoveFromFavorites(oNode);
                            break;
                        case SegmentMenuItems.Clone:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            CloneSegment(oNode);
                            break;
                        case SegmentMenuItems.UpdateAdminContent:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            ConvertToAdminSegment(oNode, true);
                            break;
                        case SegmentMenuItems.ConvertToAdminContent:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            ConvertToAdminSegment(oNode, false);
                            break;
                        case SegmentMenuItems.Properties:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            SetPropertiesForNode(oNode);
                            break;
                        case SegmentMenuItems.Delete:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            DeleteNode(oNode);
                            break;
                        case SegmentMenuItems.Design:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            DesignSegmentFromNode(oNode);
                            break;
                        case SegmentMenuItems.ApplyStyles:
                            ApplyStylesFromNode(oNode, false);
                            break;
                        case SegmentMenuItems.CopyStyles:
                            ApplyStylesFromNode(oNode, true);
                            break;
                        case SegmentMenuItems.AttachTo:
                            Transform(oNode);
                            break;
                        case SegmentMenuItems.InsertSavedPrefill:
                            oFolderMem = oNode.Tag as FolderMember;

                            if (oFolderMem != null && oFolderMem.ObjectTypeID == mpFolderMemberTypes.UserSegmentPacket)
                            {
                                //folder member is a segment packet -
                                //get user segment def
                                UserSegmentDefs oDefs = new UserSegmentDefs();
                                UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(oFolderMem.ObjectID1 + "." + oFolderMem.ObjectID2);

                                //prompt for saved data
                                PrefillChooser oForm = new PrefillChooser();
                                DialogResult iRes = oForm.ShowDialog();

                                if (iRes == DialogResult.OK)
                                {
                                    //get prefill from selection
                                    string xVSetID = oForm.VariableSetID;

                                    VariableSetDefs oSetDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                                    VariableSetDef oSetDef = (VariableSetDef)oSetDefs.ItemFromID(xVSetID);

                                    Prefill oPrefill = new Prefill(oSetDef.ContentString, oSetDef.Name, oSetDef.SegmentID);

                                    //insert multiple segments using selected prefill
									//GLOG 7316
                                    LMP.MacPac.Application.InsertMultipleSegments(oDef.XML, oPrefill);
                                }
                            }
                            else if (oFolderMem != null && oFolderMem.ObjectTypeID == mpFolderMemberTypes.AdminSegment)
                            {
                                //test to see if segment is a segment packet
                                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oFolderMem.ObjectID1);

                                if (oDef.TypeID == mpObjectTypes.SegmentPacket)
                                {
                                    //prompt for saved data
                                    PrefillChooser oForm = new PrefillChooser();
                                    DialogResult iRes = oForm.ShowDialog();

                                    if (iRes == DialogResult.OK)
                                    {
                                        //get prefill from selection
                                        string xVSetID = oForm.VariableSetID;

                                        VariableSetDefs oSetDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                                        VariableSetDef oSetDef = (VariableSetDef)oSetDefs.ItemFromID(xVSetID);

                                        Prefill oPrefill = new Prefill(oSetDef.ContentString, oSetDef.Name, oSetDef.SegmentID);

                                        //insert multiple segments using selected prefill
										//GLOG 7316
                                        LMP.MacPac.Application.InsertMultipleSegments(oDef.XML, oPrefill);
                                    }
                                }
                                else
                                {
                                    InsertSavedPrefillFromNode(oNode);
                                }
                            }
                            else
                            {
                                InsertSavedPrefillFromNode(oNode);
                            }
                            break;
                        case SegmentMenuItems.InsertUsingClientMatterLookup:
                            {
                                InsertUsingClientMatterNumber(oNode);
                            }
                            break;
                        case SegmentMenuItems.InsertMultipleusingSavedPrefill:
                            InsertMultiple(null, oNode);
                            break;
                        case SegmentMenuItems.InsertUsingSelection:
                            InsertUsingSelection(oNode);
                            break;
                        case SegmentMenuItems.InsertTransientPrefill:
                            oFolderMem = oNode.Tag as FolderMember;

                            if (oFolderMem != null && oFolderMem.ObjectTypeID == mpFolderMemberTypes.UserSegmentPacket)
                            {
                                //folder member is a segment packet -
                                //get user segment def
                                UserSegmentDefs oDefs = new UserSegmentDefs();
                                UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(oFolderMem.ObjectID1 + "." + oFolderMem.ObjectID2);

                                Segment oSourceSeg = this.TaskPane.ForteDocument
                                    .GetTopLevelSegmentFromSelection();

                                //GLOG 7976 jsw 1/14/15
                                //if active document is not a segment, 
                                //function should proceed wihtout any data 
                                //remmming out the error message. 
                                //if (oSourceSeg == null)
                                //{
                                //    MessageBox.Show(LMP.Resources.GetLangString("Msg_PlaceInsertionInASegment"),
                                //        LMP.ComponentProperties.ProductName,
                                //        MessageBoxButtons.OK,
                                //        MessageBoxIcon.Exclamation);
                                //    return;
                                //}

                                if (oSourceSeg == null)
                                {
                                    InsertMultiple(oDef.XML);
                                }
                                else
                                {
                                    Prefill oPrefill = new Prefill(oSourceSeg);

                                    //insert multiple segments using selected prefill
									//GLOG 7316
                                    LMP.MacPac.Application.InsertMultipleSegments(oDef.XML, oPrefill);
                                }
                            }
                            else
                            {
                                InsertTransientPrefillFromNode(oNode);
                            }
                            break;
                        case SegmentMenuItems.StyleSummary:
                            CreateStyleSummary(oNode);
                            break;
                        case SegmentMenuItems.Share:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            ShareContent(oNode);
                            break;
                        case SegmentMenuItems.ManageAuthorPreferences:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            ManageAuthorPreferences(oNode);
                            break;
                        case SegmentMenuItems.CreateExternalContent:
                            CreateExternalContent(oNode);
                            break;
                        case SegmentMenuItems.SegmentHelp:
                            ShowHelp();
                            break;
                        case SegmentMenuItems.EditAnswerFile:
                            //GLOG 2966
                            EditAnswerFileData(oNode);
                            break;
                        case SegmentMenuItems.SaveAsUserSegment:
                            {
                                FolderMember oMember = (FolderMember)oNode.Tag;
                                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oMember.ObjectID1);
                                SaveAsUserSegment(oDef);
                                break;
                            }
                        case SegmentMenuItems.Export:
                            {
                                //GLOG
                                if (oNode.Tag is FolderMember)
                                {
                                    FolderMember oMember = (FolderMember)oNode.Tag;
                                    if (oMember.ObjectTypeID == mpFolderMemberTypes.UserSegment)
                                    {
                                        UserSegmentDefs oDefs = new UserSegmentDefs(
                                            mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);
                                        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(
                                            oMember.ObjectID1 + "." + oMember.ObjectID2);
                                        oDef.Export();
                                    }
                                    else if (oMember.ObjectTypeID == mpFolderMemberTypes.AdminSegment)
                                    {
                                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oMember.ObjectID1);
                                        oDef.Export();
                                    }
                                }
                                else if (oNode.Tag is AdminSegmentDef)
                                {
                                    ((AdminSegmentDef)oNode.Tag).Export();
                                }
                                else if (oNode.Tag is UserSegmentDef)
                                {
                                    ((UserSegmentDef)oNode.Tag).Export();
                                }
                                break;
                            }
                        //GLOG : 3870 : CEH
                        case SegmentMenuItems.ShowAssignments:
                            {
                                string xAssignments = "";
                                AdminSegmentDef oAssignedDef = null;

                                FolderMember oMember = (FolderMember)oNode.Tag;
                                
                                int iID = oMember.ObjectID1;

                                //get admin segment def for current node
                                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);

                                //get assignments for current node
                                Assignments oExistingAssignments = new Assignments(oDef.TypeID, iID, 0);

                                if (oExistingAssignments.Count > 0)
                                {
                                    //cycle throught assignments & build display name string
                                    for (int i = oExistingAssignments.Count - 1; i >= 0; i--)
                                    {
                                        Assignment oA = (Assignment)oExistingAssignments.ItemFromIndex(i);
                                        oAssignedDef = (AdminSegmentDef)oDefs.ItemFromID(oA.AssignedObjectID);
                                        xAssignments += LMP.Data.Application.GetObjectTypeDisplayName(oAssignedDef.TypeID) 
                                                                + " - " + oAssignedDef.DisplayName + "\n";
                                    }

                                    MessageBox.Show(xAssignments,oDef.DisplayName + " Assignments",
                                                    MessageBoxButtons.OK, 
                                                    MessageBoxIcon.Information);
                                }
                                else
                                    //show no assignments message
                                    MessageBox.Show(LMP.Resources.GetLangString("Msg_SegmentHasNoAssignments"), 
                                                                                oDef.DisplayName + " Assignments",
                                                                                MessageBoxButtons.OK, 
                                                                                MessageBoxIcon.Information);
                                break;
                            }
                        case SegmentMenuItems.CreateSegmentPacket:
                            CreateSegmentPacket(m_oCurNode);
                            break;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_iSegmentMenuSelection = SegmentMenuItems.None;
            }
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                //this.RefreshTree(); - replaced with line below (GLOG 2768, 8/1/08)
                TaskPanes.RefreshAll();
                return true;
            }
            else if (keyData == Keys.Escape && MenuIsVisible())
            {
                DismissActiveMenu(ToolStripDropDownCloseReason.Keyboard);
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }
        /// <summary>
        /// Disable/Enable 'Keep Headers' and 'Restart Page Numbering' menu items
        /// based on checked state of 'Separate Section' item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegment_SeparateSection_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                mnuSegment_KeepHeadersFooters.Enabled = mnuSegment_SeparateSection.Checked;
                mnuSegment_RestartPageNumbering.Enabled = mnuSegment_SeparateSection.Checked;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Get clicked menu item so correct code can be run after menu is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFolder_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                switch (e.ClickedItem.Name)
                {
                    case "mnuFolder_CreateNewFolder":
                        m_iFolderMenuSelection = FolderMenuItems.CreateNewFolder;
                        break;
                    case "mnuFolder_CollapseAllSubfolders":
                        m_iFolderMenuSelection = FolderMenuItems.CollapseAllSubfolders;
                        break;
                    case "mnuFolder_Properties":
                        m_iFolderMenuSelection = FolderMenuItems.Properties;
                        break;
                    case "mnuFolder_Delete":
                        m_iFolderMenuSelection = FolderMenuItems.Delete;
                        break;
                    case "mnuFolder_Refresh":
                        m_iFolderMenuSelection = FolderMenuItems.Refresh;
                        break;
                    case "mnuFolder_Paste":
                        m_iFolderMenuSelection = FolderMenuItems.Paste;
                        break;
                    case "mnuFolder_Default":
                        m_iFolderMenuSelection = FolderMenuItems.Default;
                        break;
                    case "mnuFolder_SelectDefault":
                        m_iFolderMenuSelection = FolderMenuItems.SelectDefault;
                        break;
                    case "mnuFolder_RemoveDefault":
                        m_iFolderMenuSelection = FolderMenuItems.RemoveDefault;
                        break;
                    case "mnuFolder_CreateNewSegment":
                        m_iFolderMenuSelection = FolderMenuItems.CreateNewSegment;
                        break;
                    case "mnuFolder_CreateNewDesigner": //GLOG 8376
                        m_iFolderMenuSelection = FolderMenuItems.CreateNewSegment;
                        break;
                    case "mnuFolder_ApplyStyles":
                        m_iFolderMenuSelection = FolderMenuItems.ApplyStyles;
                        break;
                    case "mnuFolder_SaveStyles":
                        m_iFolderMenuSelection = FolderMenuItems.SaveStyles;
                        break;
                    case "mnuFolder_MoveUp":
                        m_iFolderMenuSelection = FolderMenuItems.MoveUp;
                        break;
                    case "mnuFolder_MoveDown":
                        m_iFolderMenuSelection = FolderMenuItems.MoveDown;
                        break;
                    case "mnuFolder_SetPermissions":
                        m_iFolderMenuSelection = FolderMenuItems.SetPermissions;
                        break;
                    case "mnuFolder_ImportSegment":
                        m_iFolderMenuSelection = FolderMenuItems.ImportSegment;
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Stop menu from closing if checkbox item has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFolder_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
        }
        /// <summary>
        /// Stop menu from closing if checkbox item has been clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegment_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == ToolStripDropDownCloseReason.ItemClicked && (m_iSegmentMenuSelection == SegmentMenuItems.SeparateSection ||
                    m_iSegmentMenuSelection == SegmentMenuItems.KeepHeaders ||
                    m_iSegmentMenuSelection == SegmentMenuItems.RestartPageNumbering ||
                    m_iSegmentMenuSelection == SegmentMenuItems.SelectionAsBody))       //GLOG : 8248
                    e.Cancel = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Set right-click menu options for Nodes 
        /// whose Tag property references a FolderMember Object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSegment_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                bool bIsOwnerOfSegment = false;
                bool bIsOwnerOfParent = false;
                bool bIsStyleSheet = false;
                bool bIsAnswerFile = false;
                bool bIsSharedFolderMember = false;
                bool bIsPrefill = false;
                bool bIsOwnerFolderMember = false;
                mpFolderTypes iFolderType = 0;
                int iAdminSegmentID = 0;
                bool bIsIdlessVariableSegment = false;
                bool bIsSavedContent = false;

                if (this.treeContent.Visible)
                {
                    
                    if (!(m_oCurNode.Tag is FolderMember || ContentTreeHelper.IsOwnerFolderMemberNode(m_oCurNode) || 
                        ContentTreeHelper.IsSharedFolderMemberNode(m_oCurNode)))
                    {
                        e.Cancel = true;
                        return;
                    }

                    if (!(ContentTreeHelper.IsOwnerFolderMemberNode(m_oCurNode) || 
                        ContentTreeHelper.IsSharedFolderMemberNode(m_oCurNode)))
                    {
                        FolderMember oMember = (FolderMember)m_oCurNode.Tag;
                        if (ContentTreeHelper.IsVariableSetNode(m_oCurNode))
                        {
                            VariableSetDef oVarSetDef = GetVariableSetFromNode(m_oCurNode);

                            if (oVarSetDef == null || oVarSetDef.SegmentID != "")
                            {
                                if (!CheckVariableSetSegment(m_oCurNode))
                                {
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }
                        else if (ContentTreeHelper.GetSegmentDefFromNode(m_oCurNode) == null)
                        {
                            //Underlying Segment object has been deleted
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_Object_Deleted_From_DB"), 
                                LMP.ComponentProperties.ProductName,
                                   MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            DeleteSegmentNode(m_oCurNode, false);
                            return;
                        }
                        iFolderType = oMember.FolderType;
                        if (oMember.ObjectID2 == 0)
                            iAdminSegmentID = oMember.ObjectID1;
                    }
                    else if (ContentTreeHelper.IsSharedFolderMemberNode(m_oCurNode))
                        bIsSharedFolderMember = true;
                    else if (ContentTreeHelper.IsOwnerFolderMemberNode(m_oCurNode))
                        bIsOwnerFolderMember = true;

                    bIsOwnerOfSegment = ContentTreeHelper.UserIsOwnerOf(m_oCurNode);
                    bIsOwnerOfParent = ContentTreeHelper.UserIsOwnerOf(m_oCurNode.Parent);
                    bIsStyleSheet = ContentTreeHelper.IsStyleSheetNode(m_oCurNode);
                    bIsAnswerFile = ContentTreeHelper.IsAnswerFileNode(m_oCurNode);

                    if (ContentTreeHelper.IsOwnerFolderMemberNode(m_oCurNode))
                    {
                        // Selected Node is an Admin Segment
                        if (m_oCurNode.Tag is AdminSegmentDef)
                        {
                            iAdminSegmentID = ((AdminSegmentDef)m_oCurFindNode.Tag).ID;
                            iFolderType = mpFolderTypes.Admin;
                        }
                        else if (m_oCurNode.Tag is VariableSetDef)
                        {
                            if (!CheckVariableSetSegment(m_oCurNode))
                            {
                                e.Cancel = true;
                                return;
                            }

                            string xPrefillSegmentID = ((VariableSetDef)m_oCurNode.Tag).SegmentID;
                            if (!xPrefillSegmentID.Contains("."))
                                iAdminSegmentID = Int32.Parse(xPrefillSegmentID);
                            else if (xPrefillSegmentID.EndsWith(".0"))
                                iAdminSegmentID = Int32.Parse(xPrefillSegmentID.Substring(0, xPrefillSegmentID.IndexOf(".")));
                            else
                                iAdminSegmentID = 0;
                            iFolderType = mpFolderTypes.User;
                            bIsPrefill = true;
                        }
                        else
                        {
                            iFolderType = mpFolderTypes.User;
                        }
                    }
                    else if (ContentTreeHelper.IsVariableSetNode(m_oCurNode))
                    {
                        // Allow variable segment defs that have no segment id's.
                        VariableSetDef oVarSetDef = GetVariableSetFromNode(m_oCurNode);

                        if (oVarSetDef == null || oVarSetDef.SegmentID != "")
                        {
                            if (!CheckVariableSetSegment(m_oCurNode))
                            {
                                e.Cancel = true;
                                return;
                            }
                        }

                        // If this is a variable set with no segment id get the segment
                        // that the variable set should be associated with.

                        string xPrefillSegmentID = "";

                        if (oVarSetDef != null && oVarSetDef.SegmentID == "")
                        {
                            bIsIdlessVariableSegment = true;
                        }
                        else
                        {
                            // For Prefill, display same menu options as related Admin Segment
                            xPrefillSegmentID = GetVariableSetFromNode(m_oCurNode).SegmentID;
                        }

                        // Only get the admin segment ID if this Variable Segment has an associated segment id.
                        if (!bIsIdlessVariableSegment)
                        {
                            if (!xPrefillSegmentID.Contains("."))
                                iAdminSegmentID = Int32.Parse(xPrefillSegmentID);
                            else if (xPrefillSegmentID.EndsWith(".0"))
                                iAdminSegmentID = Int32.Parse(xPrefillSegmentID.Substring(0, xPrefillSegmentID.IndexOf(".")));
                            else
                                iAdminSegmentID = 0;
                        }
                        iFolderType = mpFolderTypes.User;
                        bIsPrefill = true;
                    }
                }
                else
                {
                    // Find Results tree is displayed
                    if (m_oCurFindNode == null)
                    {
                        e.Cancel = true;
                        return;
                    }
                    // Selected Node is an Admin Segment
                    if (m_oCurFindNode.Tag is AdminSegmentDef)
                    {
                        iAdminSegmentID = ((AdminSegmentDef)m_oCurFindNode.Tag).ID;
                        iFolderType = mpFolderTypes.Admin;
                    }
                    else if (m_oCurFindNode.Tag is VariableSetDef)
                    {
                        string xPrefillSegmentID = ((VariableSetDef)m_oCurFindNode.Tag).SegmentID;
                        if (!xPrefillSegmentID.Contains("."))
                            iAdminSegmentID = Int32.Parse(xPrefillSegmentID);
                        else if (xPrefillSegmentID.EndsWith(".0"))
                            iAdminSegmentID = Int32.Parse(xPrefillSegmentID.Substring(0, xPrefillSegmentID.IndexOf(".")));
                        else
                            iAdminSegmentID = 0;
                        iFolderType = mpFolderTypes.User;
                        bIsPrefill = true;
                    }
                    else
                    {
                        iFolderType = mpFolderTypes.User;
                        bIsOwnerOfSegment = ((UserSegmentDef)m_oCurFindNode.Tag).OwnerID == Session.CurrentUser.ID;
                        bIsStyleSheet = ((UserSegmentDef)m_oCurFindNode.Tag).IntendedUse == mpSegmentIntendedUses.AsStyleSheet;
                    }
                }

                if (bIsIdlessVariableSegment)
                {
                    SetIdlessVariableSegmentMenuOptions();
                }
                else
                {
                    // Admin Segment type
                    if ((iFolderType == mpFolderTypes.Admin) || (iAdminSegmentID != 0))
                    {
                        SetAdminSegmentMenuOptions(iAdminSegmentID, iFolderType, bIsPrefill, bIsStyleSheet,
                            bIsOwnerFolderMember, bIsSharedFolderMember, bIsAnswerFile, bIsSavedContent);
                    }
                    else
                    {
                        ISegmentDef oDef = null;
                        if (treeContent.Visible)
                        {
                            oDef = ContentTreeHelper.GetSegmentDefFromNode(m_oCurNode);
                        }
                        else
                        {
                            oDef = ContentTreeHelper.GetSegmentDefFromNode(m_oCurFindNode);
                        }

                        if (oDef.TypeID == mpObjectTypes.SavedContent)
                        {
                            // GLOG : 3040 : ceh
                            bIsSavedContent = true;
                            string xSegmentID = oDef.ChildSegmentIDs;
                            if (!xSegmentID.Contains(".") || xSegmentID.EndsWith(".0"))
                            {
                                //GLOG 6966: Make sure string can be parsed as a Double even if
                                //the Decimal separator is something other than '.' in Regional Settings
                                if (xSegmentID.EndsWith(".0"))
                                {
                                    xSegmentID = xSegmentID.Replace(".0", "");
                                }
                                double dSegmentID = double.Parse(xSegmentID);
                                int iSegmentID = (int)dSegmentID;

                                //contained segment is admin segment
                                SetAdminSegmentMenuOptions(iSegmentID, iFolderType, bIsPrefill,
                                    bIsStyleSheet, bIsOwnerFolderMember, bIsSharedFolderMember, bIsAnswerFile, bIsSavedContent);
                            }
                            else
                                SetUserSegmentMenuOptions(bIsOwnerOfSegment, bIsOwnerOfParent,
                                    bIsPrefill, bIsStyleSheet, bIsOwnerFolderMember, bIsSharedFolderMember, bIsSavedContent);

                            //disable segment editing for saved content
                            this.mnuSegment_Design.Visible = false;
                            this.mnuSegment_Sep5.Visible = false;
                        }
                        else
                            SetUserSegmentMenuOptions(bIsOwnerOfSegment, bIsOwnerOfParent, bIsPrefill,
                                bIsStyleSheet, bIsOwnerFolderMember, bIsSharedFolderMember, bIsSavedContent); //GLOG 8376
                    }
                }
            
                mnuSegment_Copy.Text = string.Format(LMP.Resources.GetLangString("Menu_Content_Segment_Copy"), m_oCurNode.Text);

                // Reset tracking variable
                m_iSegmentMenuSelection = SegmentMenuItems.None;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Set right-click menu options for Nodes 
        /// whose Tag property references a Folder or UserFolder Object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFolder_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                if (!(m_oCurNode.Tag is Folder || m_oCurNode.Tag is UserFolder ||
                    ContentTreeHelper.IsTopLevelNode(m_oCurNode) || ContentTreeHelper.IsOwnerFolderNode(m_oCurNode)))
                {
                    e.Cancel = true;
                    return;
                }

                if (!string.IsNullOrEmpty(m_xDefaultFolder))
                {
                    mnuFolder_SelectDefault.Enabled = true;
                    mnuFolder_RemoveDefault.Enabled = true;
                }
                else
                {
                    mnuFolder_SelectDefault.Enabled = false;
                    mnuFolder_RemoveDefault.Enabled = false;
                }


                if (ContentTreeHelper.IsAdminFolderNode(m_oCurNode))
                {
                    //GLOG 8376: Never displayed for Admin Folder
                    mnuFolder_CreateNewDesigner.Visible = false;
                    if (ContentTreeHelper.IsPublicFolderNode(m_oCurNode) || ContentTreeHelper.IsOwnerFolderNode(m_oCurNode) || ContentTreeHelper.IsSharedFolderRootNode(m_oCurNode))
                    {
                        //Node is Public Folders Node
                        //JTS 12/31/08: Only display Create New Folder for Shared Folders in Admin Mode
                        mnuFolder_CreateNewFolder.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuFolder_CollapseAllSubfolders.Visible = true;
                        mnuFolder_AddFile.Visible = false;
                        mnuFolder_Sep2.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuFolder_SaveStyles.Visible = false;
                        mnuFolder_ApplyStyles.Visible = false;
                        mnuFolder_Sep1.Visible = false;
                        mnuFolder_Sep3.Visible = false;
                        mnuFolder_Delete.Visible = false;
                        mnuFolder_Properties.Visible = false;
                        mnuFolder_Sep4.Visible = false;
                        mnuFolder_CreateNewSegment.Visible = false;
                        mnuFolder_Sep5.Visible = true;
                        mnuFolder_Paste.Visible = false;
                        mnuFolder_MoveUp.Visible = false;
                        mnuFolder_MoveDown.Visible = false;
                        mnuFolder_SetPermissions.Visible = false;
                        mnuFolder_Sep6.Visible = false;
                        mnuFolder_ImportSegment.Visible = false;
                    }
                    else
                    {
                        if (!FolderExists((Folder)m_oCurNode.Tag))
                        {
                            // Folder might have been deleted in a different document window
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_Object_Deleted_From_DB"), LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            m_oCurNode.Remove();
                            //GLOG 7214
                            TaskPane.SetHelpText(wbDescription, "");
                            e.Cancel = true;
                            return;
                        }
                        mnuFolder_CreateNewFolder.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        //GLOG 6090: Don't expose for Shared Folders
						//GLOG : 7998 : ceh
                        mnuFolder_AddFile.Visible = Session.AdminMode 
                                                    && !ContentTreeHelper.IsSharedFolderNode(m_oCurNode) 
                                                    && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
                        mnuFolder_SaveStyles.Visible = false;
                        mnuFolder_CollapseAllSubfolders.Visible = ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuFolder_ApplyStyles.Visible = false;
                        //GLOG 5826: Don't expose for Content Designers
                        //GLOG 6090: Don't expose for Shared Folders
                        mnuFolder_ImportSegment.Visible = Session.AdminMode && !ContentTreeHelper.IsSharedFolderNode(m_oCurNode);
                        mnuFolder_Sep1.Visible = ((((((Folder)m_oCurNode.Tag).XML != "") || Session.AdminMode) && 
                            !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode) && !ContentTreeHelper.IsSharedFolderNode(m_oCurNode) && Session.AdminMode));  //GLOG 5826: Don't expose for Content Designers
                        mnuFolder_Sep2.Visible = Session.AdminMode;
                        mnuFolder_Sep3.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuFolder_Delete.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuFolder_Properties.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        //GLOG : 7998 : ceh
						mnuFolder_SetPermissions.Visible = Session.AdminMode 
                                                           && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode) 
                                                           && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
                        mnuFolder_Sep4.Visible = false;
                        mnuFolder_CreateNewSegment.Enabled = this.TaskPane.Mode != ForteDocument.Modes.Design && 
                            !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode) && !ContentTreeHelper.IsSharedFolderNode(m_oCurNode);
                        mnuFolder_CreateNewSegment.Visible = (Session.AdminMode) && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode) && 
                            !ContentTreeHelper.IsSharedFolderNode(m_oCurNode);
                        mnuFolder_Sep5.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        if (Session.AdminMode && m_oNodeToPaste != null && m_oNodeToPaste.Parent != m_oCurNode && 
                            ContentTreeHelper.IsAdminSegmentNode(m_oNodeToPaste) && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode) && !ContentTreeHelper.IsSharedFolderNode(m_oCurNode))
                        {
                            mnuFolder_Sep3.Visible = true;
                            mnuFolder_Paste.Visible = true;
                            mnuFolder_Paste.Text = string.Format(
                                LMP.Resources.GetLangString("Menu_Content_Segment_Paste"), m_oNodeToPaste.Text);
                        }
                        else
                        {
                            mnuFolder_Sep3.Visible = false;
                            mnuFolder_Paste.Visible = false;
                        }

                        mnuFolder_MoveDown.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuFolder_MoveDown.Enabled = (m_oCurNode.GetSibling(NodePosition.Next) != null) && 
                            (ContentTreeHelper.IsAdminFolderNode(m_oCurNode.GetSibling(NodePosition.Next)) && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode));
                        mnuFolder_MoveUp.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuFolder_MoveUp.Enabled = (m_oCurNode.GetSibling(NodePosition.Previous) != null) && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuFolder_Sep6.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        //mnuNewSegment_AnswerFile.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode);
                        mnuNewSegment_MasterDataForm.Visible = Session.AdminMode &&
							 !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode) && //GLOG 6795: Moved out of Development Mode
							 !LMP.MacPac.MacPacImplementation.IsToolsAdministrator; //GLOG : 7998 : ceh
                        mnuNewSegment_SegmentPacket.Visible = Session.AdminMode && !ContentTreeHelper.IsOwnerFolderNode(m_oCurNode); //GLOG 7127
                    }
                }
                else
                {
                    if (m_oCurNode.Tag is UserFolder && !UserFolderExists((UserFolder)m_oCurNode.Tag))
                    {
                        // Folder might have been deleted in a different document window
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_Object_Deleted_From_DB"), LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        m_oCurNode.Remove();
                        //GLOG 7214
                        TaskPane.SetHelpText(wbDescription, "");
                        e.Cancel = true;
                        return;
                    }
                    mnuFolder_CollapseAllSubfolders.Visible = ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    //mnuNewSegment_AnswerFile.Visible = false;
                    mnuNewSegment_MasterDataForm.Visible = false;
                    mnuNewSegment_SegmentPacket.Visible = true;
                    mnuFolder_CreateNewFolder.Visible = true;
                    mnuFolder_AddFile.Visible = false;
                    mnuFolder_ApplyStyles.Visible = false;
                    mnuFolder_SaveStyles.Visible = false;
                    mnuFolder_Sep1.Visible = (!ContentTreeHelper.IsMyFolderNode(m_oCurNode))
                        && ((((UserFolder)m_oCurNode.Tag).XML != "") || ContentTreeHelper.UserIsOwnerOf(m_oCurNode));
                    mnuFolder_SaveStyles.Visible = false; // DISABLED BY JAB per glog item875: ((!ContentTreeHelper.IsMyFolderNode(m_oCurNode)) && ContentTreeHelper.UserIsOwnerOf(m_oCurNode));
                    mnuFolder_Sep2.Visible = false;  // DISABLED BY JAB per glog item875: true;
                    mnuFolder_Sep3.Visible = !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    mnuFolder_Delete.Visible = !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    mnuFolder_Properties.Visible = !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    mnuFolder_SetPermissions.Visible = false;
                    mnuFolder_Sep4.Visible = !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    mnuFolder_CreateNewSegment.Enabled = (this.TaskPane.Mode !=  ForteDocument.Modes.Design);
                    mnuFolder_CreateNewSegment.Visible = !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    //GLOG 8376
                    mnuFolder_CreateNewDesigner.Visible = !Session.AdminMode && Session.CurrentUser.IsContentDesigner && !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    if (!ContentTreeHelper.IsMyFolderNode(m_oCurNode) && m_oNodeToPaste != null && m_oNodeToPaste.Parent != m_oCurNode)
                    {
                        mnuFolder_Sep3.Visible = true;
                        mnuFolder_Paste.Visible = true;
                        mnuFolder_Paste.Text = string.Format(LMP.Resources.GetLangString("Menu_Content_Segment_Paste"), m_oNodeToPaste.Text);
                    }
                    else
                    {
                        mnuFolder_Sep3.Visible = false;
                        mnuFolder_Paste.Visible = false;
                    }
                    mnuFolder_ImportSegment.Visible =  !ContentTreeHelper.IsMyFolderNode(m_oCurNode) && (Session.AdminMode || Session.CurrentUser.IsContentDesigner); //GLOG 8376

                    mnuFolder_Sep5.Visible = !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    mnuFolder_MoveDown.Visible = !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    mnuFolder_MoveDown.Enabled = !ContentTreeHelper.IsMyFolderNode(m_oCurNode) && (m_oCurNode.GetSibling(NodePosition.Next) != null) &&
                        (ContentTreeHelper.IsUserFolderNode(m_oCurNode.GetSibling(NodePosition.Next)));
                    mnuFolder_MoveUp.Visible = !ContentTreeHelper.IsMyFolderNode(m_oCurNode);
                    mnuFolder_MoveUp.Enabled = !ContentTreeHelper.IsMyFolderNode(m_oCurNode) && (m_oCurNode.GetSibling(NodePosition.Previous) != null);
                    mnuFolder_Sep6.Visible = true;
                }
                //reset tracking variable
                m_iFolderMenuSelection = FolderMenuItems.None;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// After Folder context menu has closed, get any action to be performed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFolder_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            try
            {
                if (this.chkFindContent.Checked)
                {
                    this.treeResults.Refresh();
                    this.treeResults.Focus();
                }
                else
                {
                    this.treeContent.Refresh();
                    this.treeContent.Focus();
                }

                if (e.CloseReason == ToolStripDropDownCloseReason.ItemClicked)
                {
                    switch (m_iFolderMenuSelection)
                    {
                        case FolderMenuItems.CreateNewFolder:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            CreateFolderNode(m_oCurNode);
                            break;
                        case FolderMenuItems.Properties:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            SetPropertiesForNode(m_oCurNode);
                            break;
                        case FolderMenuItems.Delete:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            DeleteFolderNode(m_oCurNode);
                            break;
                        case FolderMenuItems.Refresh:
                            RefreshFolderNode(m_oCurNode);
                            break;
                        case FolderMenuItems.RemoveDefault:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            SetDefaultFolder("");
                            break;
                        case FolderMenuItems.SelectDefault:
                            LoadDefaultFolder();
                            break;
                        case FolderMenuItems.Paste:
                            PasteSegmentNode(m_oCurNode);
                            break;
                        case FolderMenuItems.Default:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            SetDefaultFolder(m_oCurNode.Key);
                            break;
                        case FolderMenuItems.CollapseAllSubfolders:
                            CollapseAllSubfolders(m_oCurNode);
                            break;
                        case FolderMenuItems.ApplyStyles:
                            ApplyStylesFromNode(m_oCurNode, false);
                            break;
                        case FolderMenuItems.SaveStyles:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            SaveDocStylesToFolder(m_oCurNode);
                            break;
                        case FolderMenuItems.MoveUp:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            ReorderFolderNodes(m_oCurNode, FolderMoveDirections.MoveUp);
                            break;
                        case FolderMenuItems.MoveDown:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            ReorderFolderNodes(m_oCurNode, FolderMoveDirections.MoveDown);
                            break;
                        case FolderMenuItems.SetPermissions:
                            if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            SetFolderPermissions(m_oCurNode);
                            break;
                        case FolderMenuItems.ImportSegment:
                            if (MacPac.Application.IsUserSyncInProgress(true))
                                return;
                            ImportDataFileDetail(m_oCurNode);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_iFolderMenuSelection = FolderMenuItems.None;
            }
        }

        /// <summary>
        /// Clear find results if text is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFindContent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.chkFindContent.Checked == true)
                {
                    this.chkFindContent.Checked = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Handle special purpose keystrokes in TreeView control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Control && (e.KeyCode == Keys.F1))
                {
                    //Ctl+F selects the find text box
                    this.txtFindContent.Focus();
                }
                else if (e.Control && e.KeyCode == Keys.F3)
                {
                    this.TaskPane.Toggle();
                }
                else if (e.Control && e.KeyCode == Keys.F5)
                {
                    this.ShowSegmentID(this.treeContent.SelectedNodes[0]);
                }

                //Key presses still get passed to tree, even if ContextMenuStrip is displayed
                if (mnuSegment.Visible)
                {
                    mnuSegment.HandleKeyDown(e);
                    e.SuppressKeyPress = true;
                    return;
                }
                else if (mnuFolder.Visible)
                {
                    mnuFolder.HandleKeyDown(e);
                    e.SuppressKeyPress = true;
                    return;
                }
            
                switch (e.KeyCode)
                {
                    case Keys.Enter:
                        PerformDoubleClickActionForNode(m_oCurNode);
                        break;
                    case Keys.Delete:
                        if (m_oCurNode == null || m_oCurNode.IsEditing)
                            return;
                        else if (m_oCurNode.Tag is Folder || m_oCurNode.Tag is UserFolder)
                            DeleteFolderNode(m_oCurNode);
                        else if (m_oCurNode.Tag is FolderMember)
                            DeleteSegmentNode(m_oCurNode, true);
                        break;
                    case Keys.V:
                        if (e.Control && (m_oNodeToPaste != null))
                            PasteSegmentNode(m_oCurNode);
                        break;
                    case Keys.C:
                        if (e.Control && m_oCurNode.Tag is FolderMember)
                            CopySegmentNode(m_oCurNode, true);
                        break;
                    case Keys.Down:
                        if (!e.Alt && !e.Shift && e.Control)
                        {
                            //Ctrl+Down displays context menu
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            DisplayContextMenu(m_oCurNode, false);
                        }
                            break;
                    case Keys.End:
                        if (e.Control && e.Shift)
                        {
                            //Move to last visible node
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            UltraTreeNode oNode = this.treeContent.Nodes[this.treeContent.Nodes.Count - 1];
                            while (oNode.Nodes.Count > 0)
                            {
                                if (oNode.Expanded)
                                    oNode = oNode.Nodes[oNode.Nodes.Count - 1];
                                else
                                    break;
                            }
                            this.treeContent.ActiveNode = oNode;
                            oNode.Selected = true;
                        }
                        break;
                    case Keys.Home:
                        //Move to top of tree
                        if (e.Control && e.Shift)
                        {
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            this.treeContent.ActiveNode = this.treeContent.Nodes[0];
                            this.treeContent.Nodes[0].Selected = true;
                        }
                        break;
                    case Keys.Left:
                        //Collapse to toplevel nodes
                        if (e.Control && e.Shift)
                        {
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            this.treeContent.ActiveNode = this.treeContent.Nodes[0];
                            this.treeContent.Nodes[0].Selected = true;
                            m_bIgnoreCollapse = false;
                            foreach (UltraTreeNode oNode in this.treeContent.Nodes)
                                oNode.Expanded = false;
                        }
                        break;
                    case Keys.Right:
                        //Expand previously expanded nodes
                        if (e.Control)
                        {
                            if (e.Shift)
                            {
                                e.SuppressKeyPress = true;
                                e.Handled = true;
                                this.treeContent.ExpandAll(ExpandAllType.OnlyNodesWithChildren);
                            }
                            else
                            {
                                // Any time control right arrow is pressed expand the selected node.
                                if (this.treeContent.SelectedNodes.Count > 0)
                                {
                                    this.treeContent.SelectedNodes[0].Expanded = true;
                                }
                            }
                        }
                        // GLOG : 4095 : CEH
                        //Expand previously non-expanded nodes and select first in sub-tree if present
                        else
                        {
                            if (this.treeContent.SelectedNodes.Count > 0)
                            {
                                this.treeContent.SelectedNodes[0].Expanded = true;
                            }
                        }
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        /// <summary>
        /// Handle special purpose keystrokes in TreeView control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Key presses still get passed to tree, even if ContextMenuStrip is displayed
                if (mnuSegment.Visible)
                {
                    mnuSegment.HandleKeyDown(e);
                    e.SuppressKeyPress = true;
                    return;
                }
                else if (mnuFolder.Visible)
                {
                    mnuFolder.HandleKeyDown(e);
                    e.SuppressKeyPress = true;
                    return;
                }

                switch (e.KeyCode)
                {
                    case Keys.Enter:
                        if (m_oCurFindNode.Tag == null)
                        {
                            //GLOG #2770 - jb 7/23/08
                            if (m_oCurFindNode.Parent != null && m_oCurFindNode.Parent.Parent != null && m_oCurFindNode.Parent.Parent.Parent == null)
                            {
                                /*
                                 * When the selected node is the node detailing the path to the content,
                                 * apply the double click behavior its parents node. The tree
                                 * looks like this:
                                 * 
                                 * Search Results
                                 *        |
                                 *         -> Found Content
                                 *                |
                                 *                -> Path for found content.
                                 * */

                                PerformDoubleClickActionForNode(m_oCurFindNode.Parent);
                            }
                        }
                        else
                        {
                            PerformDoubleClickActionForNode(m_oCurFindNode);
                        }
                        break;
                    case Keys.Down:
                        //Ctrl+Down displays context menu
                        if (!e.Alt && !e.Shift && e.Control)
                        {
                            e.SuppressKeyPress = true;
                            e.Handled = true;
                            DisplayContextMenu(m_oCurFindNode, false);
                        }
                        break;
                    case Keys.F3:
                        if (e.Control)
                        {
                            this.TaskPane.Toggle();
                        }
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// Dragged item has been dropped on a node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                //The Point that the mouse cursor is on, in Tree coords. 
                //This event passes X and Y in form coords. 
                System.Drawing.Point PointInTree;
                //Get the position of the mouse in the tree, as opposed
                //to form coords
                PointInTree = this.treeContent.PointToClient(new System.Drawing.Point(e.X, e.Y));
                UltraTreeNode oDropNode = GetNearestNodeFromPoint(ref PointInTree);

                if (oDropNode != null)
                {
                    switch (GetNodeDropLocation(PointInTree, oDropNode))
                    {
                        case NodeDropLocation.Inside:
                            ChangeParentNode(oDropNode, m_oDraggedNode, e.Effect, e.Data);
                            break;

                        case NodeDropLocation.Below:
                            MoveFolderNode(oDropNode, m_oDraggedNode, FolderMoveDirections.MoveDown);
                            break;

                        case NodeDropLocation.Above:
                            MoveFolderNode(oDropNode, m_oDraggedNode, FolderMoveDirections.MoveUp);
                            break;
                    }
                }
                //else
                //{
                //    InsertSegmentFromNodeDrag(m_oDraggedNode);
                //}

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                // End dragging
                m_oDraggedNode = null;
            }

        }

        private NodeDropLocation GetNodeDropLocation(System.Drawing.Point oPointInTree, UltraTreeNode oNode)
        {
            if (ContentTreeHelper.IsTopLevelNode(oNode))
            {
                return NodeDropLocation.Inside;
            }

            if (IsUpperNodeRegion(oPointInTree, oNode) && !IsLowerNeighbor(oNode))
            {
                return NodeDropLocation.Above;
            }

            if (IsLowerNodeRegion(oPointInTree, oNode) && !IsUpperNeighbor(oNode))
            {
                return NodeDropLocation.Below;
            }

            return NodeDropLocation.Inside; 
        }

        private bool IsLowerNeighbor(UltraTreeNode oNode)
        {
            UltraTreeNode oNeighborNode = this.m_oDraggedNode.GetSibling(NodePosition.Next);
            return (oNeighborNode == oNode);
        }

        private bool IsUpperNeighbor(UltraTreeNode oNode)
        {
            UltraTreeNode oNeighborNode = this.m_oDraggedNode.GetSibling(NodePosition.Previous);
            return (oNeighborNode == oNode);
        }

        private bool IsLowerNodeRegion(System.Drawing.Point oPointInTree, UltraTreeNode oNode)
        {
            int iLowerRegionHeight = (int)(oNode.Bounds.Height * LOWER_NODE_REGION_COEFFICIENT);
            int iLowerRegionBoundary = oNode.Bounds.Bottom - iLowerRegionHeight;
            return oPointInTree.Y <= oNode.Bounds.Bottom &&
                    oPointInTree.Y >= iLowerRegionBoundary;
        }

        private bool IsUpperNodeRegion(System.Drawing.Point oPointInTree, UltraTreeNode oNode)
        {
            int iUpperRegionHeight = (int)(oNode.Bounds.Height * UPPER_NODE_REGION_COEFFICIENT);
            int iUpperRegionBoundary = oNode.Bounds.Top + iUpperRegionHeight;
            return oPointInTree.Y >= oNode.Bounds.Top &&
                oPointInTree.Y <= iUpperRegionBoundary;
        }

        private bool IsMiddleNodeRegion(System.Drawing.Point oPointInTree, UltraTreeNode oNode)
        {
            return  oPointInTree.Y >= oNode.Bounds.Top &&
                    oPointInTree.Y <= oNode.Bounds.Bottom && 
                    !IsUpperNodeRegion(oPointInTree, oNode) && 
                    !IsLowerNodeRegion(oPointInTree, oNode);
        }

        private void treeContent_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            //Get the position of the mouse in the tree, as opposed
            //to form coords
            System.Drawing.Point oPointInTree = this.m_oMouseLocation;

            //Get the node the mouse is over.
            UltraTreeNode oNode = GetNearestNodeFromPoint(ref oPointInTree);


            if (m_oDraggedNode != oNode)
            {
                if (this.m_oDraggedNode == null || oNode == null || (ContentTreeHelper.IsTopLevelNode(oNode)))
                {
                    e.UseDefaultCursors = true;
                    Cursor.Current = Cursors.Default;
                }
                else
                {
                    if (e.Effect == DragDropEffects.Move)
                    {
                        if (IsUpperNodeRegion(oPointInTree, oNode) && !IsLowerNeighbor(oNode))
                        {
                            e.UseDefaultCursors = false;
                            Cursor.Current = ContentManager.BetweenCursor;
                            return;
                        }

                        if (IsLowerNodeRegion(oPointInTree, oNode) && !IsUpperNeighbor(oNode))
                        {
                            e.UseDefaultCursors = false;
                            Cursor.Current = ContentManager.BetweenCursor;
                            return;
                        }

                        e.UseDefaultCursors = true;
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
        }

        /// <summary>
        /// Dragged node has been dropped
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_DragDrop(object sender, DragEventArgs e)
        {
            //try
            //{
            //    //The Point that the mouse cursor is on, in Tree coords. 
            //    //This event passes X and Y in form coords. 
            //    System.Drawing.Point PointInTree;
            //    //Get the position of the mouse in the tree, as opposed
            //    //to form coords
            //    PointInTree = this.treeResults.PointToClient(new Point(e.X, e.Y));
            //    UltraTreeNode oDropNode = this.treeResults.GetNodeFromPoint(PointInTree);
            //    if (oDropNode == null)
            //    {
            //        InsertSegmentFromNodeDrag(m_oDraggedNode);
            //    }

            //}
            //catch (System.Exception oE)
            //{
            //    LMP.Error.Show(oE);
            //}
        }

        /// <summary>
        /// Update mouse pointer to reflect available actions when
        /// dragging over individual nodes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                //The Point that the mouse cursor is on, in Tree coords. 
                //This event passes X and Y in form coords. 
                System.Drawing.Point PointInTree;

                //Get the position of the mouse in the tree, as opposed
                //to form coords
                PointInTree = this.treeContent.PointToClient(new System.Drawing.Point(e.X, e.Y));

                m_oMouseLocation = PointInTree;

                //Get the node the mouse is over.
                UltraTreeNode oNode = GetNearestNodeFromPoint(ref PointInTree);

                if (m_oDraggedNode == null)
                {
                    // Selected Text has been dragged to the tree
                    if (e.Data.GetDataPresent("HTML Format"))
                    {
                        if (!ContentTreeHelper.IsSharedContentNode(oNode) &&
                            ((ContentTreeHelper.IsUserFolderNode(oNode) && !ContentTreeHelper.IsMyFolderNode(oNode)) ||
                            (ContentTreeHelper.IsAdminFolderNode(oNode) && Session.AdminMode) ||
                            (ContentTreeHelper.IsUserSegmentNode(oNode) && ContentTreeHelper.UserIsOwnerOf(oNode.Parent)) ||
                            (ContentTreeHelper.IsAdminSegmentNode(oNode) && Session.AdminMode)))
                            e.Effect = DragDropEffects.Link;
                        else
                            e.Effect = DragDropEffects.None;
                    }
                }
                else
                {
                    if (ContentTreeHelper.IsSharedContentNode(oNode) || ContentTreeHelper.IsSharedFolderNode(oNode) || ContentTreeHelper.IsOwnerFolderNode(oNode))
                    {
                        //Can't drag into a Shared or Owner folder
                        e.Effect = DragDropEffects.None;
                    }
                    else if ((ContentTreeHelper.IsAdminFolderNode(oNode) || ContentTreeHelper.IsAdminSegmentNode(oNode)) &&
                    ((ContentTreeHelper.IsAdminFolderNode(m_oDraggedNode) || ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode))) && Session.AdminMode)
                    {
                        if (ContentTreeHelper.IsAdminFolderNode(m_oDraggedNode) && (m_oDraggedNode == oNode))
                        {
                            // Don't allow dropping folder on itself
                            e.Effect = DragDropEffects.None;
                        }
                        else if (ContentTreeHelper.IsPublicFolderNode(oNode) && !ContentTreeHelper.IsAdminFolderNode(m_oDraggedNode))
                        {
                            // Only allow dragging Folders to root level
                            e.Effect = DragDropEffects.None;
                        }
                        else if (ContentTreeHelper.IsAdminFolderNode(m_oDraggedNode) && ((ContentTreeHelper.IsAdminFolderNode(oNode) || ContentTreeHelper.IsAdminSegmentNode(oNode)))
                            && m_oDraggedNode.IsAncestorOf(oNode))
                        {
                            // Don't allow moving folder to child folder
                            e.Effect = DragDropEffects.None;
                        }
                        else if ((ContentTreeHelper.IsAdminFolderNode(m_oDraggedNode) || ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode)) &&
                            (m_oDraggedNode.Parent == oNode))
                        {
                            //if (((e.KeyState & 8) == 8) && ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode))
                            //    e.Effect = DragDropEffects.Copy;
                            //else
                            //Don't allow dropping in the same folder
                            //but do allow placing the folder above or below the containing folder.
                            if ((IsUpperNodeRegion(PointInTree, oNode) || IsLowerNodeRegion(PointInTree, oNode)) && !ContentTreeHelper.IsTopLevelNode(oNode))
                            {
                                if (((e.KeyState & 8) == 8) && ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode))
                                    e.Effect = DragDropEffects.Copy;
                                else
                                    e.Effect = DragDropEffects.Move;
                            }
                            else
                            {
                                e.Effect = DragDropEffects.None;
                            }

                        }
                        else if ((ContentTreeHelper.IsAdminFolderNode(m_oDraggedNode) || ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode)) &&
                            ContentTreeHelper.IsAdminSegmentNode(oNode) && m_oDraggedNode.Parent == oNode.Parent)
                        {
                            //if (((e.KeyState & 8) == 8) && ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode))
                            //    e.Effect = DragDropEffects.Copy;
                            //else
                            //Don't allow dropping in the same folder
                            e.Effect = DragDropEffects.None;
                        }
                        else
                            if (((e.KeyState & 8) == 8) && ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode))
                                e.Effect = DragDropEffects.Copy;
                            else
                                e.Effect = DragDropEffects.Move;
                    }
                    else if ((ContentTreeHelper.IsUserFolderNode(oNode) || ContentTreeHelper.IsUserSegmentNode(oNode)) &&
                    (ContentTreeHelper.IsUserFolderNode(m_oDraggedNode) || ContentTreeHelper.IsUserSegmentNode(m_oDraggedNode) 
                        || ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode) || ContentTreeHelper.IsVariableSetNode(m_oDraggedNode)))
                    {
                        if (ContentTreeHelper.IsUserFolderNode(m_oDraggedNode) && (m_oDraggedNode == oNode))
                            // Don't allow dropping folder on itself
                            e.Effect = DragDropEffects.None;
                        else if (ContentTreeHelper.IsMyFolderNode(oNode) && !ContentTreeHelper.IsUserFolderNode(m_oDraggedNode))
                            // Only allow dragging UserFolders to root level
                            e.Effect = DragDropEffects.None;
                        else if (ContentTreeHelper.IsUserFolderNode(m_oDraggedNode) && (ContentTreeHelper.IsUserFolderNode(oNode) || ContentTreeHelper.IsUserSegmentNode(oNode) || ContentTreeHelper.IsVariableSetNode(oNode)) &&
                            m_oDraggedNode.IsAncestorOf(oNode))
                        {
                            // Don't allow moving folder to child folder
                            e.Effect = DragDropEffects.None;
                        }
                        else if ((ContentTreeHelper.IsUserFolderNode(m_oDraggedNode) || ContentTreeHelper.IsUserSegmentNode(m_oDraggedNode) || ContentTreeHelper.IsVariableSetNode(m_oDraggedNode)) &&
                            m_oDraggedNode.Parent == oNode)
                        {
                            //if (((e.KeyState & 8) == 8) && ContentTreeHelper.IsUserSegmentNode(m_oDraggedNode))
                            //    e.Effect = DragDropEffects.Copy;
                            //else
                            //Don't allow dropping in the same folder 
                            //but do allow placing the folder above or below the containing folder.
                            if ((IsUpperNodeRegion(PointInTree, oNode) || IsLowerNodeRegion(PointInTree, oNode)) && !ContentTreeHelper.IsTopLevelNode(oNode))
                            {
                                // Copy if Ctrl Key is pressed
                                if (((e.KeyState & 8) == 8) && ContentTreeHelper.IsUserSegmentNode(m_oDraggedNode))
                                    e.Effect = DragDropEffects.Copy;
                                else
                                    e.Effect = DragDropEffects.Move;
                            }
                            else
                            {
                                e.Effect = DragDropEffects.None;
                            }
                        }
                        else if ((ContentTreeHelper.IsUserFolderNode(m_oDraggedNode) || ContentTreeHelper.IsUserSegmentNode(m_oDraggedNode) || ContentTreeHelper.IsVariableSetNode(m_oDraggedNode)) &&
                            (ContentTreeHelper.IsUserSegmentNode(oNode) || ContentTreeHelper.IsVariableSetNode(oNode)) && m_oDraggedNode.Parent == oNode.Parent)
                        {
                            //if (((e.KeyState & 8) == 8) && ContentTreeHelper.IsUserSegmentNode(m_oDraggedNode))
                            //    e.Effect = DragDropEffects.Copy;
                            //else
                                //Don't allow dropping in the same folder
                                e.Effect = DragDropEffects.None;
                        }
                        else
                            if (ContentTreeHelper.IsAdminSegmentNode(m_oDraggedNode) || ContentTreeHelper.IsSharedContentNode(m_oDraggedNode))
                                // Admin Segments and Shared Content can only be copied,
                                // not moved to a User Folder
                                e.Effect = DragDropEffects.Copy;
                            else
                            {
                                // Copy if Ctrl Key is pressed
                                if (((e.KeyState & 8) == 8) && ContentTreeHelper.IsUserSegmentNode(m_oDraggedNode))
                                    e.Effect = DragDropEffects.Copy;
                                else
                                    e.Effect = DragDropEffects.Move;
                            }
                    }
                    else
                    {
                        //anything else not allowed
                        e.Effect = DragDropEffects.None;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private UltraTreeNode GetNearestNodeFromPoint(ref System.Drawing.Point oPointInTree)
        {
            UltraTreeNode oNode = this.treeContent.GetNodeFromPoint(oPointInTree);

            if (oNode == null)
            {
                // Check for a node just above this point.
                oPointInTree.Y -= NeighboringNodeOffset;
                oNode = this.treeContent.GetNodeFromPoint(oPointInTree);

                if (IsUpperNeighbor(oNode))
                {
                    // Return null if the nearest node to the point just above the
                    // current mouse location is the node just above the dragged
                    // node.
                    return null;
                }

                if (ContentTreeHelper.IsTopLevelNode(oNode))
                {
                    // If the nearest node is a top level folder node, return 
                    // null since we should only be able to insert folders into these 
                    // folders and not be able to make this folder as a peer of a top
                    // folder node. 
                    //
                    // Also return null if this is a neighboring folder since if the mouse location is 
                    // not directly over the folder node it would imply that we want to move 
                    // the dragged folder next to this folder when it is already a neighbor
                    // of this node.

                    return null;
                }

                if (oNode == null)
                {
                    // Check for a node just below this point. Since we've 
                    // decremented the Y value, we'll need to add the offset
                    // twice.

                    oPointInTree.Y += (2 * NeighboringNodeOffset);
                    oNode = this.treeContent.GetNodeFromPoint(oPointInTree);

                    if (IsLowerNeighbor(oNode))
                    {
                        // Return null if the nearest node to the point just below the
                        // current mouse location is the node just below the dragged
                        // node.
                        return null;
                    }

                    if (ContentTreeHelper.IsTopLevelNode(oNode))
                    {
                        // If the nearest node is a top level folder node, return 
                        // null since we should only be able to insert folders into these 
                        // folders and not be able to make this folder as a peer of a top
                        // folder node. 
                        //
                        // Also return null if this is a neighboring folder since if the mouse location is 
                        // not directly over the folder node it would imply that we want to move 
                        // the dragged folder next to this folder when it is already a neighbor
                        // of this node.

                        return null;
                    }
                }
            }

            return oNode;
        }

        private int NeighboringNodeOffset
        {
            get
            {
                if (m_iNeighboringNodeOffset == 0)
                {
                    // Calculate the m_iNeighboringNodeOffset based on a
                    // percentage of the height of the folder. This will hopefully
                    // account for situations where the folder size varies.

                    if (this.treeContent.Nodes.Count > 0)
                    {
                        this.m_iNeighboringNodeOffset = (int)(this.treeContent.Nodes[0].Bounds.Height * 0.20);
                    }
                }

                return this.m_iNeighboringNodeOffset;
            }
        }
        /// <summary>
        /// Update mouse pointer to reflect available actions when
        /// dragging over individual nodes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                e.Effect = DragDropEffects.None;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Check if drag action has been cancelled by Esc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            try
            {
                //Did the user press escape? 
                if (e.EscapePressed)
                {
                    //User pressed escape
                    //Cancel the Drag
                    e.Action = DragAction.Cancel;
                    m_oDraggedNode = null;
                    m_oRectForDrag = System.Drawing.Rectangle.Empty;
                }
                // Keep track of where on screen the mouse is positioned
                m_oLastPointOnScreen.X = Control.MousePosition.X;
                m_oLastPointOnScreen.Y = Control.MousePosition.Y;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Check if drag action has been cancelled by Esc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            try
            {
                //Did the user press escape? 
                if (e.EscapePressed)
                {
                    //User pressed escape
                    //Cancel the Drag
                    e.Action = DragAction.Cancel;
                    m_oDraggedNode = null;
                    m_oRectForDrag = System.Drawing.Rectangle.Empty;
                }
                // Keep track of where on screen the mouse is positioned
                m_oLastPointOnScreen.X = Control.MousePosition.X;
                m_oLastPointOnScreen.Y = Control.MousePosition.Y;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Mouse has been moved over UltraWinTree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                //Mouse has moved beyond minimum boundaries required to start drag
                if (m_oRectForDrag != System.Drawing.Rectangle.Empty && !m_oRectForDrag.Contains(e.X, e.Y))
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        //oNode = this.treeContent.SelectedNodes[0];
                        oNode = m_oCurNode;
                    }
                    catch
                    {
                        //There may not be a selected node 
                    }
                    finally
                    {
                        //Start a DragDrop operation for Segments, owned UserFolder items 
                        //or AdminFolder items if in AdminMode
                        if ((ContentTreeHelper.IsUserFolderNode(oNode) && ContentTreeHelper.UserIsOwnerOf(oNode))
                            || ContentTreeHelper.IsUserSegmentNode(oNode)
                            || ContentTreeHelper.IsVariableSetNode(oNode)
                            || ContentTreeHelper.IsAdminSegmentNode(oNode)
                            || (ContentTreeHelper.IsAdminFolderNode(oNode) && Session.AdminMode))
                        {
                            m_oDraggedNode = oNode;

                            if (ContentTreeHelper.IsAdminFolderNode(m_oDraggedNode) || ContentTreeHelper.IsUserFolderNode(m_oDraggedNode))
                                // Set data to Node object for folder, so it can't be dropped
                                // over Word Document
                                this.treeContent.DoDragDrop(m_oDraggedNode,
                                    DragDropEffects.Move | DragDropEffects.Copy);
                            else
                                // Set data to empty string for segments
                                // This way Word will automatically recognize
                                // and allow dropping onto a document
                                this.treeContent.DoDragDrop("",
                                    DragDropEffects.Move | DragDropEffects.Copy);

                            if (m_oDraggedNode != null)
                            {
                                try
                                {
                                    // If coordinates are to the left of control, node is being
                                    // dropped on the Word document screen
                                    m_oLastPointOnScreen.X = Control.MousePosition.X;
                                    m_oLastPointOnScreen.Y = Control.MousePosition.Y;
                                    if (!this.TaskPane.RectangleToScreen(this.TaskPane.ClientRectangle).Contains(m_oLastPointOnScreen))
                                    {
                                        // GLOG : 2962 : JAB
                                        // Prevent the user from inserting external content into a design document and from
                                        // inserting into a document.
                                        if (IsExternalContentNode(oNode))
                                        {
                                            if (this.TaskPane.Mode == ForteDocument.Modes.Design)
                                            {
                                                MessageBox.Show(LMP.Resources.GetLangString("Msg_CannotInsertExternalContentDesign"),
                                                                LMP.ComponentProperties.ProductName,
                                                                MessageBoxButtons.OK,
                                                                MessageBoxIcon.Error);
                                            }
                                            else
                                            {
                                                MessageBox.Show(LMP.Resources.GetLangString("Msg_CannotInsertExternalContent"),
                                                                LMP.ComponentProperties.ProductName,
                                                                MessageBoxButtons.OK,
                                                                MessageBoxIcon.Error);
                                            }

                                        }
                                        else
                                        {
                                            // Only segments can be dropped
                                            if (ContentTreeHelper.IsAdminSegmentNode(oNode) || ContentTreeHelper.IsUserSegmentNode(oNode))
                                                InsertSegmentFromNodeDrag(oNode);
                                        }
                                    }
                                }
                                catch (System.Exception oE)
                                {
                                    LMP.Error.Show(oE);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Mouse has been moved over UltraWinTree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                //Mouse has moved beyond minimum boundaries required to start drag
                if (m_oRectForDrag != System.Drawing.Rectangle.Empty && !m_oRectForDrag.Contains(e.X, e.Y))
                {
                    UltraTreeNode oNode = null;
                    try
                    {
                        oNode = m_oCurFindNode;
                    }
                    catch
                    {
                        //There may not be a selected node 
                    }
                    finally
                    {
                        //Start a DragDrop operation for Segments, owned UserFolder items 
                        //or AdminFolder items if in AdminMode
                        if (oNode != null)
                        {
                            m_oDraggedNode = oNode;

                            // Set data to empty string for segments
                            // This way Word will automatically recognize
                            // and allow dropping onto a document
                            this.treeResults.DoDragDrop("",
                                DragDropEffects.Move | DragDropEffects.Copy);

                            try
                            {
                                // If coordinates are to the left of control, node is being
                                // dropped on the Word document screen
                                // Point p = new Point(Control.MousePosition.X, Control.MousePosition.Y);
                                m_oLastPointOnScreen.X = Control.MousePosition.X;
                                m_oLastPointOnScreen.Y = Control.MousePosition.Y;
                                if (!this.TaskPane.RectangleToScreen(this.TaskPane.ClientRectangle).Contains(m_oLastPointOnScreen))
                                {
                                    InsertSegmentFromNodeDrag(oNode);
                                }
                            }
                            catch (System.Exception oE)
                            {
                                LMP.Error.Show(oE);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Mouse button has been released
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_MouseUp(object sender, MouseEventArgs e)
        {
            //reset Drag boundaries
            m_oRectForDrag = System.Drawing.Rectangle.Empty;
        }

        /// <summary>
        /// Mouse button has been released
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_TreeMouseUp(object sender, MouseEventArgs e)
        {
            //reset Drag boundaries
            m_oRectForDrag = System.Drawing.Rectangle.Empty;
        }
        /// <summary>
        /// Toggle between Find Results and Content trees
        /// Check if there are matching results before display find results tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkFindContent_CheckedChanged(object sender, EventArgs e) //GLOG 8037 - renamed to match control name
        {
            try
            {
                if (m_bAdvancedSearchRunning == true)
                    return;

                if (this.txtFindContent.Text == "")
                {
                    chkFindContent.Checked = false;
                }
                else if (chkFindContent.Checked)
                {
                    m_aFindResults = Data.Application.GetSegmentFindResults(this.txtFindContent.Text,
                        Session.CurrentUser.ID, mpSegmentFindMode.MatchSubString, 0);
                    if (m_aFindResults == null || m_aFindResults.Count == 0)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_No_Matching_Segments"), 
                            LMP.ComponentProperties.ProductName,
                           MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // The find string is appended a carriage return line feed. 
                        //Remove it to fix situation where subseqent
                        // search contains this find string.
                        string xCRLF = "\r\n";

                        if (this.txtFindContent.Text.EndsWith(xCRLF))
                        {
                            this.txtFindContent.Text = this.txtFindContent.Text.Remove(
                                this.txtFindContent.Text.IndexOf(xCRLF));
                        }

                        chkFindContent.Checked = false;
                        txtFindContent.Focus();
                    }
                    else
                        ShowFindResults();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

            finally
            {
                if (chkFindContent.Checked)
                {
                    this.tipMain.SetToolTip(chkFindContent, "Clear find results"); //GLOG 8037
                    this.treeResults.Visible = true;
                    this.treeContent.Visible = false;
                    //GLOG 7214
                    TaskPane.SetHelpText(wbDescription, "");
                    this.chkFindContent.BackColor = Color.SlateGray;

                    // GLOG : 3061 : JAB
                    // Hide the picMenuDropdown image when displaying the
                    // Find results.
                    this.picMenuDropdown.Visible = false;
                }
                else
                {
                    this.tipMain.SetToolTip(chkFindContent, "Find matching content"); //GLOG 8037
                    this.treeContent.Visible = true;
                    this.treeResults.Visible = false;
                    if (!m_bAdvancedSearchRunning) //GLOG 8162
                        m_aFindResults = null;
                    SetBrowserContents(this.treeContent.ActiveNode);
                    this.chkFindContent.BackColor = Color.Transparent;
                    // GLOG : 3061 : JAB
                    // Restore the picMenuDropdown image's visibility when 
                    // exitting Find results.
                    this.picMenuDropdown.Visible = true;

                    if (this.treeContent.SelectedNodes.Count > 0)
                        this.DisplayContextMenuButton(this.treeContent.SelectedNodes[0]);
                }
            }
        }
        private void wbDescription_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.F3)
            {
                this.TaskPane.Toggle();
            }
        }
        /// <summary>
        /// Perform appropriate action, depending on
        /// which new Segment item was selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuNewSegmentOptions_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            try
            {
                if (!Session.AdminMode && MacPac.Application.IsUserSyncInProgress(true))
                    return;

                if ((e.CloseReason & ToolStripDropDownCloseReason.ItemClicked) == ToolStripDropDownCloseReason.ItemClicked)
                {
                    if (m_iNewSegmentSelection == NewSegmentItems.SegmentPacket)
                    {
                        CreateSegmentPacket(m_oCurNode);
                    }
                    else if (m_iNewSegmentSelection != NewSegmentItems.None)
                    {
                        CreateNewSegment(m_oCurNode, m_iNewSegmentSelection, m_bNewSegmentDesigner); //GLOG 8376
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_iNewSegmentSelection = NewSegmentItems.None;
                m_bNewSegmentDesigner = false; //GLOG 8376
            }
        }
        /// <summary>
        /// Display appropriate New Segment options
        /// based on whether there is an Active Document window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuNewSegmentOptions_Opening(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.TaskPane.ForteDocument == null)
                {
                    mnuNewSegment_Document.Visible = false;
                    mnuNewSegment_Selection.Visible = false;
                    mnuNewSegment_StyleSheet.Visible = false;
                }
                else
                {
                    mnuNewSegment_Document.Visible = true;
					//GLOG : 7998 : ceh
                    mnuNewSegment_StyleSheet.Visible = !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
                    //only display this option if text is selected
                    mnuNewSegment_Selection.Visible = (Session.CurrentWordApp.Selection.Type > Word.WdSelectionType.wdSelectionIP);
                    mnuNewSegment_SegmentPacket.Visible = !m_bNewSegmentDesigner; //GLOG 8376
                }
                m_iNewSegmentSelection = NewSegmentItems.None;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Test that new folder name is valid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_ValidateLabelEdit(object sender, ValidateLabelEditEventArgs e)
        {
            try
            {
                UltraTreeNode oNode = e.Node;
                if (e.LabelEditText == "")
                {
                    //GLOG 8887: Don't allow empty name
                    e.Cancel = true;
                }
                else if (oNode.Tag is Folder)
                {
                    // Folder name has been changed
                    if (e.LabelEditText != ((Folder)oNode.Tag).Name)
                    {
                        ((Folder)oNode.Tag).Name = e.LabelEditText;
                        this.AdminFolders.Save((LongIDSimpleDataItem)oNode.Tag);
                    }
                }
                else if (oNode.Tag is UserFolder)
                {
                    // Folder name has been changed
                    if (e.LabelEditText != ((UserFolder)oNode.Tag).Name)
                    {
                        ((UserFolder)oNode.Tag).Name = e.LabelEditText;
                        this.UserFolders.Save((StringIDSimpleDataItem)oNode.Tag);
                    }
                }
                else if (oNode.Tag is FolderMember)
                {
                    // Edit not allowed for FolderMembers
                    e.Cancel = true;
                }
            }
            catch (LMP.Exceptions.StoredProcedureException)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Error_InvalidFolderName"), LMP.ComponentProperties.ProductName,
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
                e.StayInEditMode = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }


        }
        /// <summary>
        /// Capture menu item clicked to run appropriate action after closing menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuNewSegmentOptions_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                switch (e.ClickedItem.Name)
                {
                    case "mnuNewSegment_Blank":
                        m_iNewSegmentSelection = NewSegmentItems.SegmentBlank;
                        break;
                    case "mnuNewSegment_Selection":
                        m_iNewSegmentSelection = NewSegmentItems.SegmentFromSelection;
                        break;
                    case "mnuNewSegment_Document":
                        m_iNewSegmentSelection = NewSegmentItems.SegmentFromDocument;
                        break;
                    case "mnuNewSegment_StyleSheet":
                        m_iNewSegmentSelection = NewSegmentItems.StyleSheetFromDocument;
                        break;
                    //case "mnuNewSegment_AnswerFile":
                    //    m_iNewSegmentSelection = NewSegmentItems.AnswerFile;
                    //    break;
                    case "mnuNewSegment_MasterDataForm":
                        m_iNewSegmentSelection = NewSegmentItems.MasterDataForm;
                        break;
                    case "mnuNewSegment_SegmentPacket":
                        m_iNewSegmentSelection = NewSegmentItems.SegmentPacket;
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// When Enter is pressed in Find Textbox, toggle Find checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFindContent_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Return)
                {
                    e.SuppressKeyPress = true;
                    if (txtFindContent.Text != "")
                    {
                        chkFindContent.Checked = !chkFindContent.Checked;
                    }
                    else if (chkFindContent.Checked)
                        chkFindContent.Checked = false;
                }
                else if (e.Control && e.KeyCode == Keys.F3)
                {
                    this.TaskPane.Toggle();
                    e.Handled = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Check to disallow selecting top-level node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_BeforeSelect(object sender, BeforeSelectEventArgs e)
        {
            try
            {
                UltraTreeNode oNewNode = (UltraTreeNode)e.NewSelections.GetItem(0);
                if (oNewNode != null && oNewNode.Key == "0")
                    e.Cancel = true;
            }
            catch { e.Cancel = true; }
        }
        /// <summary>
        /// Check to disallow activating top-level node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeResults_BeforeActivate(object sender, CancelableNodeEventArgs e)
        {
            try
            {
                if (e.TreeNode.Key == "0")
                    e.Cancel = true;
            }
            catch { e.Cancel = true; }
        }
        private void treeContent_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                if (e.NewSelections.Count > 0)
                    this.lblStatus.Text = e.NewSelections[0].FullPath;
                else
                    this.lblStatus.Text = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeResults_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                //if (e.NewSelections.Count > 0)
                //    this.lblStatus.Text = " " + e.NewSelections[0].FullPath;
                //else
                    this.lblStatus.Text = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void picHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

        public void ShowHelp()
        {
            try
            {
                string xHelp = "";
                                
                //GLOG - 3655 - CEH
                //treeContent node might not be visible
                if ((this.treeContent.SelectedNodes.Count > 0) && (this.treeContent.SelectedNodes[0].Control.Visible))
                {
                    xHelp = this.GetBrowserContents(this.treeContent.SelectedNodes[0]);
                }
                //treeResult node might be visible but not selected
                else if ((this.treeResults.SelectedNodes.Count != 0) && (this.treeResults.SelectedNodes[0].Control != null))
                {
                    if (this.treeResults.SelectedNodes[0].Control.Visible)
                    {
                        xHelp = this.GetBrowserContents(this.treeResults.SelectedNodes[0]);
                    }
                }
                //only show help if necessary
                if (xHelp == "")
                    return;
                else
                {
                    Help oHelp = new Help(xHelp);
                    oHelp.Text = LMP.ComponentProperties.ProductName + " Help" + (this.lblStatus.Text != "" ? " - " + this.lblStatus.Text : "");
                    oHelp.Show();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void wbDescription_SizeChanged(object sender, EventArgs e)
        {
            Session.CurrentUser.UserSettings.HelpWindowHeight = this.wbDescription.Height;
        }
        private void splitContent_SplitterMoved(object sender, SplitterEventArgs e)
        {
            Session.CurrentUser.UserSettings.HelpWindowHeight = this.pnlContentDescription.Height;
            this.TaskPane.UpdateDocumentDesignerHelpWindowView();
            this.TaskPane.UpdateDocumentEditorHelpWindowView();
            this.TaskPane.UpdateDataReviewerHelpWindowView();
        }
        /// <summary>
        /// clears the find textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSearch_Clear_Click(object sender, EventArgs e)
        {
            try
            {
                this.txtFindContent.Text = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuQuickHelp_Copy_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Clipboard.SetText(this.wbDescription.Document.Body.OuterText);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuQuickHelp_SaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                this.wbDescription.ShowSaveAsDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuQuickHelp_Print_Click(object sender, EventArgs e)
        {
            try
            {
                this.wbDescription.ShowPrintDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuQuickHelp_PrintPreview_Click(object sender, EventArgs e)
        {
            try
            {
                this.wbDescription.ShowPrintPreviewDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************methods*********************
        //sets up the task pane if it has
        //not already been set up
        public void SetupIfNecessary()
        {
            if (!m_bSetup)
                Setup();
        }

        /// <summary>
        /// Setup options for Content Manager pane
        /// </summary>
        public void Setup()
        {
            DateTime t0 = DateTime.Now;

            SetupMenus();
            //Set TreeNode default settings
            Override oOverride = this.treeContent.Override;
            oOverride.NodeAppearance.Image = Images.FolderClosed;
            oOverride.ExpandedNodeAppearance.Image = Images.FolderOpen;
            oOverride.LabelEditAppearance.BackColor = Color.Blue;
            oOverride.LabelEditAppearance.ForeColor = Color.White;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.CheckOnExpand;
            oOverride.LabelEditAppearance.BackColor = Color.Red;
            oOverride.LabelEditAppearance.ForeColor = Color.White;
            oOverride.NodeSpacingAfter = 2;

            //// Set default to False - editing will be enabled where appropriate when nodes are added
            oOverride.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            
            //Set display options for Find Results Tree
            //this.treeResults.Override.NodeAppearance.Image = Images.DocumentSegment;
            oOverride.NodeSpacingAfter = 2;
            oOverride.ShowExpansionIndicator = ShowExpansionIndicator.Never;


            RefreshTopLevelNodes();

            //get default folder if not already gotten
            if (string.IsNullOrEmpty(m_xDefaultFolder))
            {
                try
                {
                    m_xDefaultFolder = Session.CurrentUser.GetPreferenceSet(
                        mpPreferenceSetTypes.UserApp, "").GetValue("DefaultContentFolder");
                    Trace.WriteNameValuePairs("DefaultContentFolder", m_xDefaultFolder);
                }
                catch { }
            }

            // Set the height of the help window according to this user's app settings.
            this.UpdateHelpWindowView();

            // Set the backcolor and gradient for the tree per user's prefs.
            SetTreeAppearance();
            
            //load default folder
            LoadDefaultFolder();

            m_bSetup = true;
            LMP.Benchmarks.Print(t0);

        }
        public void UpdateHelpWindowView()
        {
            bool bShowHelp = Session.CurrentUser.UserSettings.ShowHelp;
            this.splitContent.Visible = bShowHelp;
            this.pnlContentDescription.Visible = bShowHelp;

            if (bShowHelp)
            {
                this.pnlContentDescription.Height = Session.CurrentUser.UserSettings.HelpWindowHeight;
            }
            else
            {
                this.pnlContentDescription.Height = 0;
            }

            this.splitContent.Enabled = bShowHelp;
        }
        /// <summary>
        /// Ensure that User Application default folder node has been populated
        /// </summary>
        private void LoadDefaultFolder()
        {
            if (!string.IsNullOrEmpty(m_xDefaultFolder))
            {
                try
                {
                    UltraTreeNode oNewNode = LoadNodeIfNecessary(m_xDefaultFolder);

                    if(oNewNode != null)
                        SelectDefaultFolder();
                }
                catch
                {
                    return;
                }
            }
            else
            {
                //select public folders not
                const string PUBLIC_FOLDERS_NODE_KEY = "0";
                UltraTreeNode oNode = this.treeContent
                    .GetNodeByKey(PUBLIC_FOLDERS_NODE_KEY);
                oNode.Selected = true;
                this.treeContent.ActiveNode = oNode;
                this.treeContent.ActiveNode.Expanded = true;
            }
        }
        /// <summary>
        /// loads the specified folder into the tree if necessary
        /// </summary>
        /// <param name="xFolderPath"></param>
        private UltraTreeNode LoadNodeIfNecessary(string xNodeKey)
        {
            try
            {
                UltraTreeNode oNode = this.treeContent.GetNodeByKey(xNodeKey);
                if (oNode != null)
                    //node already exists in tree
                    return oNode;

                //node does not exist in tree - get node
                UltraTreeNode oNewNode = null;
                string xParentKey, xNewKey;
                string[] xSep = new string[] { mpPathSeparator };
                string[] xPath = xNodeKey.Split(xSep, StringSplitOptions.None);
                for (int i = xPath.GetLowerBound(0); i <= xPath.GetUpperBound(0); i++)
                {
                    if (oNewNode != null)
                        xParentKey = oNewNode.Key + mpPathSeparator;
                    else
                        xParentKey = "";
                    xNewKey = xParentKey + xPath[i];
                    LMP.Trace.WriteNameValuePairs("Level " + i.ToString(), xNewKey);
                    oNewNode = null;
                    oNewNode = this.treeContent.GetNodeByKey(xNewKey);
                    if (oNewNode != null)
                    {
                        RefreshFolderNode(oNewNode);
                    }
                    else
                    {
                        // Default path doesn't exist
                        return null;
                    }
                }
                return oNewNode;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// loads the specified folder into the tree if necessary
        /// </summary>
        /// <param name="xFolderPath"></param>
        private UltraTreeNode LoadNodePathIfNecessary(string xNodePath) //GLOG 6197
        {
            try
            {
                xNodePath = xNodePath.TrimEnd('/');
                string[] aPaths = xNodePath.Split('/');

                UltraTreeNode oNode = null;
                for (int i = 0; i < aPaths.Length; i++)
                {
                    string xPath = aPaths[i];
                    TreeNodesCollection oNodes = null;

                    if (oNode == null && i == 0)
                    {
                        //Top level of path will either be My Folders or Public Folders
                        if (xPath == Session.CurrentUser.FirmSettings.MyFoldersLabel)
                            oNode = treeContent.Nodes[0];
                        else if (xPath == Session.CurrentUser.FirmSettings.SharedFoldersLabel) //GLOG 8037
                            oNode = treeContent.Nodes[2];
                        else
                            oNode = treeContent.Nodes[1];
                    }
                    if (ContentTreeHelper.IsUserFolderNode(oNode))
                        RefreshUserFolderNode(oNode);
                    else if (ContentTreeHelper.IsAdminFolderNode(oNode))
                        RefreshAdminFolderNode(oNode);
                    else
                        return null;
                    oNodes = oNode.Nodes;

                    for (int p = 0; p < oNodes.Count; p++)
                    {
                        //GLOG 8037: Path may be Shared or Owner Folder
                        if ((ContentTreeHelper.IsAdminFolderNode(oNodes[p]) || ContentTreeHelper.IsUserFolderNode(oNodes[p]) 
                            || ContentTreeHelper.IsSharedContentNode(oNodes[p]) || ContentTreeHelper.IsOwnerFolderNode(oNodes[p]) ) && oNodes[p].Text == xPath)
                        {
                            oNode = oNodes[p];
                            break;
                        }
                        else    if (!ContentTreeHelper.IsAdminFolderNode(oNodes[p]) && !ContentTreeHelper.IsUserFolderNode(oNodes[p]))
                        {
                            //End of Folder children reached without a match
                            return null;
                        }
                    }
                }
                return oNode;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Select the folder set as User Application default
        /// </summary>
        private void SelectDefaultFolder()
        {
            if (!string.IsNullOrEmpty(m_xDefaultFolder))
                SelectFolderNode(m_xDefaultFolder);
        }
        /// <summary>
        /// Select Folder node corresponding to Key
        /// </summary>
        /// <param name="xKey"></param>
        /// <returns></returns>
        private UltraTreeNode SelectFolderNode(string xKey)
        {
            UltraTreeNode oNode = LoadNodeIfNecessary(xKey);
            if (oNode != null)
            {
                oNode.Selected = true;
                this.treeContent.ActiveNode = oNode;
                this.treeContent.ActiveNode.Expanded = true;
                return oNode;
            }
            else
                return null;
        }
        /// <summary>
        /// Select Folder node corresponding to Tree Path
        /// </summary>
        /// <param name="xPath"></param>
        /// <returns></returns>
        private UltraTreeNode SelectFolderNodeFromPath(string xPath) //GLOG 6197
        {
            UltraTreeNode oNode = LoadNodePathIfNecessary(xPath);
            if (oNode != null)
            {
                oNode.Selected = true;
                this.treeContent.ActiveNode = oNode;
                this.treeContent.ActiveNode.Expanded = true;
                return oNode;
            }
            else
                return null;
        }
        /// <summary>
        /// Set User Application Preference for default folder
        /// </summary>
        /// <param name="xKey"></param>
        private void SetDefaultFolder(string xKey)
        {
            KeySet oPrefs = new KeySet(mpPreferenceSetTypes.UserApp, "", Session.CurrentUser.ID.ToString());
            oPrefs.SetValue("DefaultContentFolder", xKey);
            oPrefs.Save();
            try
            {
                if (!string.IsNullOrEmpty(m_xDefaultFolder))
                    this.treeContent.GetNodeByKey(m_xDefaultFolder).Override.NodeAppearance.Reset();
            }
            catch { }

            m_xDefaultFolder = xKey;
        }
        /// <summary>
        /// refreshes top level nodes in tree
        /// </summary>
        private void RefreshTopLevelNodes()
        {
            DateTime t0 = DateTime.Now;
            this.treeContent.SuspendLayout();

            //display top level nodes - first clear tree
            this.treeContent.Nodes.Clear();

            LMP.Data.Person oUser = Session.CurrentUser.PersonObject;
            FirmApplicationSettings oSettings = Session.CurrentUser.FirmSettings;

            string xMyFoldersLabel = oSettings.MyFoldersLabel;
            string xPublicFoldersLabel = oSettings.PublicFoldersLabel;
            string xSharedFoldersLabel = oSettings.SharedFoldersLabel;

			//GLOG : 7998 : ceh
            UltraTreeNode oTreeNode = null;

            //Add top node My Folders if allowed
            if (!LMP.MacPac.MacPacImplementation.IsToolsAdministrator )
            {
                //Add top node of My Folders
                oTreeNode = this.treeContent.Nodes.Add(
                    "0.0", xMyFoldersLabel +
                    "  -  " + oUser.FirstName + " " + oUser.LastName);

                oTreeNode.Tag = (object)"0.0";
            }

            //Add top node Public Folders
            oTreeNode = this.treeContent.Nodes.Add("0",
                xPublicFoldersLabel);

            oTreeNode.Tag = (object)0;

            //Add top node Shared Folders if allowed
            //GLOG : 3637 : ceh
			//GLOG : 7998 : ceh
            if (oSettings.AllowSharedSegments && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator )
            {
                oTreeNode = this.treeContent.Nodes.Add("-2",
                    xSharedFoldersLabel);

                oTreeNode.Tag = (object)-2;
            }

            this.ResumeLayout();
            LMP.Benchmarks.Print(t0);
        }
        /// <summary>
        /// Populates Admin folder node with child folders and segments
        /// </summary>
        /// <param name="oNode"></param>
        private void RefreshAdminFolderNode(UltraTreeNode oNode)
        {
            RefreshAdminFolderNode(oNode, false);
        }
        /// <summary>
        /// Populates Admin folder node with child folders and segments
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh"></param>
        private void RefreshAdminFolderNode(UltraTreeNode oNode, bool bForceRefresh)
        {
            DateTime t0 = DateTime.Now;

            try
            {
                this.treeContent.SuspendLayout();

                //node has already been populated
                if (oNode.HasNodes && !bForceRefresh)
                    return;

                //clear out existing nodes before refreshing
                if (oNode.HasNodes)
                    oNode.Nodes.Clear();

                //get object reference if necessary
                ContentTreeHelper.LoadNodeReference(oNode);

                int iParentID = 0;
                Folder oParent = null;
                //If Tag is not a Folder, then it's the top level node
                if (oNode.Tag is Folder)
                {
                    oParent = (Folder)oNode.Tag;
                    iParentID = oParent.ID;
                }
                else if (oNode.Tag is Person)
                {
                    int iID2 = 0;
                    String.SplitID(((Person)oNode.Tag).ID, out iParentID, out iID2);
                }
                else
                    //node is top level - there are two types
                    //of admin top level nodes - 
                    //public folders and shared folders
                    iParentID = (int)oNode.Tag;

                ArrayList oDisplay = null;
                //Load Folders and FolderMembers - Owner Folder doesn't have either
                if (!ContentTreeHelper.IsOwnerFolderNode(oNode))
                {
                    if (Session.AdminMode)
                        //Show all folders
                        this.AdminFolders.SetFilter(0, iParentID);
                    else
                        // Filter folders on current user ID and parent node folder ID
                        this.AdminFolders.SetFilter(Session.CurrentUser.ID, iParentID);
                    // Get two column array of IDs and Names for child folders
                    oDisplay = this.AdminFolders.ToDisplayArray();

                    int iCount = oDisplay.Count;
                    LMP.Trace.WriteNameValuePairs("Count of Folders", iCount.ToString());

                    //cycle child folder nodes, adding each to tree
                    for (int i = 0; i < iCount; i++)
                    {
                        string xName = ((Object[])oDisplay[i])[0].ToString();
                        int iID = (int)(((Object[])oDisplay[i])[1]);

                        //add node to tree
                        //key is 'A<Parent.ID>|<Folder.ID>'
                        UltraTreeNode oTreeNode = oNode.Nodes.Add(
                            oNode.Key + mpPathSeparator + iID.ToString(), xName);

                        oTreeNode.Tag = (object)iID;

                        // Allow inline editing if Admin Mode
                        if (Session.AdminMode)
                            oTreeNode.Override.LabelEdit = Infragistics.Win.DefaultableBoolean.True;
                    }
                    //get contained folder members (e.g. segments) - 
                    //top level folder won't have FolderMembers
                    if (!ContentTreeHelper.IsPublicFolderNode(oNode) && 
                        !ContentTreeHelper.IsSharedFolderRootNode(oNode) && 
                        !ContentTreeHelper.IsSharedFolderNode(oNode))
                    {
                        //Get 3-column array of IDs and Names for folder 
                        //members based on ID of current Node (stored in Tag)
                        FolderMembers oMembers = oParent.GetMembers();
                        oDisplay = oMembers.ToDisplayArray();
                        iCount = oDisplay.Count;
                        LMP.Trace.WriteNameValuePairs("Count of Members", iCount);
                        for (int i = 0; i < iCount; i++)
                        {
                            object[] aVals = (Object[])oDisplay[i];
                            string xName = aVals[0].ToString();
                            string xID = aVals[1].ToString();
                            LMP.Data.mpSegmentIntendedUses iIntendedUse =
                                (LMP.Data.mpSegmentIntendedUses)(int.Parse(aVals[2].ToString()));

                            //add node to tree
                            //key is 'S<Parent.ID>|<Member.ID>
                            UltraTreeNode oTreeNode = oNode.Nodes.Add(
                                "A" + oNode.Key + mpPathSeparator + xID, xName);

                            //set properties -
                            oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                            oTreeNode.Tag = (object)(string.Concat("A", xID));
                            oTreeNode.Override.NodeAppearance.Image = GetImage(iIntendedUse, mpFolderMemberTypes.AdminSegment, oTreeNode);
                        }
                    }
                    else if (ContentTreeHelper.IsSharedFolderRootNode(oNode))
                    {
                        //Get list of Owner Folders to display under Shared Folders node
                        oDisplay = Data.Application.GetOwnerFoldersForDisplay(Session.CurrentUser.ID);
                        iCount = oDisplay.Count;
                        LMP.Trace.WriteNameValuePairs("Count of Owner Folders", iCount.ToString());
                        for (int i = 0; i < iCount; i++)
                        {
                            string xName = ((Object[])oDisplay[i])[0].ToString();
                            int iID = (int)(((Object[])oDisplay[i])[1]);

                            LocalPersons oPersons = new LocalPersons(mpPeopleListTypes.AllPeopleInDatabase);
                            Person oOwner = null;
                            try
                            {
                                oOwner = oPersons.ItemFromID(iID);
                            }
                            catch { }
                            if (oOwner != null)
                            {
                                //add node to tree
                                //key is 'O<Parent.ID>|<Folder.ID>'
                                UltraTreeNode oTreeNode = oNode.Nodes.Add(
                                    "O" + oNode.Key + mpPathSeparator + iID.ToString(), xName);

                                //Make Owner object the Node tag
                                oTreeNode.Tag = oOwner;

                                // Label is read-only
                                oTreeNode.Override.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
                                oTreeNode.Override.NodeAppearance.Image = Images.FolderClosed;
                                oTreeNode.Override.ExpandedNodeAppearance.Image = Images.FolderOpen;
                            }
                        }
                    }
                }
                //Owner Folders and Shared Folders contain User objects
                if (ContentTreeHelper.IsOwnerFolderNode(oNode) || ContentTreeHelper.IsSharedFolderNode(oNode))
                {
                    if (ContentTreeHelper.IsOwnerFolderNode(oNode))
                        //Display objects shared by the current Owner
                        oDisplay = Data.Application.GetOwnerFoldersMembersForDisplay(Session.CurrentUser.ID, iParentID);
                    else
                        //Display objects in Shared Folder
                        oDisplay = LMP.Data.SharedFolder.GetSharedFolderMembersForDisplay(iParentID);

                    //Get 2-column array of IDs and Names for folder 
                    //members based on ID of current Node (stored in Tag)
                    int iCount = oDisplay.Count;
                    LMP.Trace.WriteNameValuePairs("Count of Members", iCount);
                    for (int i = 0; i < iCount; i++)
                    {
                        object[] aVals = (Object[])oDisplay[i];
                        string xName = aVals[0].ToString();
                        string xID = aVals[1].ToString();
                        int iObjectType = int.Parse(aVals[2].ToString());
                        LMP.Data.mpSegmentIntendedUses iIntendedUse =
                            (LMP.Data.mpSegmentIntendedUses)(int.Parse(aVals[3].ToString()));
                        mpFolderMemberTypes shMemberType = 0;
                        if (iObjectType == 104)
                            shMemberType = mpFolderMemberTypes.VariableSet;
                        else if (iObjectType == 99)
                            shMemberType = mpFolderMemberTypes.UserSegment;
                        else
                            shMemberType = mpFolderMemberTypes.AdminSegment;

                        //add node to tree
                        //key is 'O<Parent.ID>|<Member.ID>
                        UltraTreeNode oTreeNode = oNode.Nodes.Add(string.Concat(
                            "S", oNode.Key, mpPathSeparator, xID), xName);

                        switch (shMemberType)
                        {
                            case mpFolderMemberTypes.VariableSet:
                                // Member is a VariableSet (Prefill)
                                oTreeNode.Tag = (object)string.Concat("P", xID);
                                break;
                            default:
                                // Member is a User Segment
                                oTreeNode.Tag = (object)string.Concat("U", xID);
                                //set properties -
                                break;
                        }
                        oTreeNode.Override.NodeAppearance.Image = GetImage(iIntendedUse, shMemberType, oTreeNode);
                        oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                    }
                }
                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }
            finally
            {
                this.treeContent.ResumeLayout();
            }
        }

        private void ShowSegmentID(UltraTreeNode oNode)
        {
            if (oNode != null && oNode.Tag != null)
            {
                string[] aNodeDetail = oNode.Tag.ToString().Split(';');
                if (aNodeDetail.Length > 6)
                {
                    if (aNodeDetail[4] == "UserSegment" || aNodeDetail[4] == "AdminSegment")
                    {
                        MessageBox.Show("Segment ID = " + aNodeDetail[5] + (aNodeDetail[6] == "0" ? "" : "." + aNodeDetail[6]),
                            "Segment ID", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        /// <summary>
        /// returns an image for the specified intended use with special consideration for external content
        /// </summary>
        /// <param name="iIntendedUse"></param>
        /// <returns></returns>
        private Images GetImage(mpSegmentIntendedUses iIntendedUse, mpFolderMemberTypes iMemberType, UltraTreeNode oNode)
        {
            bool bIsDesignerSegment = false;
            string xFullFileName = null;

            ContentTreeHelper.LoadNodeReference(oNode);

            if (oNode.Tag is FolderMember)
            {
                FolderMember oMember = (FolderMember)oNode.Tag;

                if (oMember != null)
                {
                    if (oMember.ObjectTypeID == mpFolderMemberTypes.AdminSegment)
                    {
                        int iID = oMember.ObjectID1;

                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);

                        if (oDef.TypeID == mpObjectTypes.NonMacPacContent)
                        {
                            // The file name of the external content is stored as a non-xml within the def's XML property.
                            xFullFileName = oDef.XML;
                        }
                    }
                    else if (ContentTreeHelper.IsDesignerSegmentNode(oNode)) //GLOG 8376
                    {
                        bIsDesignerSegment = true;
                    }
                }
            }
            else
            {
                if (oNode.Tag is AdminSegmentDef)
                {
                    AdminSegmentDef oDef = (AdminSegmentDef)oNode.Tag;

                    if (oDef.TypeID == mpObjectTypes.NonMacPacContent)
                    {
                        // The file name of the external content is stored as a non-xml within the def's XML property.
                        xFullFileName = oDef.XML;
                    }
                }
            }

            if (xFullFileName != null)
            {
                // Obtain the file extension to determine the icon type.
                string xFileExtension = Path.GetExtension(xFullFileName);

                if (xFileExtension.StartsWith(".xls") || xFileExtension.StartsWith(".xlt"))
                {
                    // Excel file extensions get an XL image.
                    return Images.XLDocument;
                }
                //GLOG : 8065 : jsw
                if (xFileExtension.ToLower().StartsWith(".doc") || xFileExtension.ToLower().StartsWith(".dot"))
                {
                    // Word files extensions get a Word image.
                    return Images.WordDocument;
                }

                // All other types of external content files get an external content image.
                return Images.ExternalContent;
            }

            // This is not external content. Determine the image based on the intended use.
            return GetImage(iIntendedUse, iMemberType, bIsDesignerSegment); //GLOG 8376
        }

        /// <summary>
        /// returns an image for the specified intended use
        /// </summary>
        /// <param name="iIntendedUse"></param>
        /// <returns></returns>
        private Images GetImage(mpSegmentIntendedUses iIntendedUse, mpFolderMemberTypes iMemberType, bool bIsDesignerSegment) //GLOG 8376
        {

            //get icon based on intended use
            if (iMemberType == mpFolderMemberTypes.VariableSet)
            {
                //VariableSet may reference Data Interview or Standalone Segment
                if (iIntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                    // GLOG : 3041 : JAB
                    // User answer files are VariableSets with intended use AsAnswerFile.
                    // Use the icon for user answer file segment.
                    return Images.UserSegmentPacket;
                else
                    return Images.Prefill;
            }
            else
            {
                switch (iIntendedUse)
                {
                    //GLOG 8376: Show Admin content icons for Designer Segments
                    case mpSegmentIntendedUses.AsDocument:
                        return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserDocument : Images.DocumentSegment;
                    case mpSegmentIntendedUses.AsDocumentComponent:
                        return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserComponentSegment : Images.ComponentSegment;
                    case mpSegmentIntendedUses.AsParagraphText:
                        return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserParagraphText : Images.ParagraphTextSegment;
                    case mpSegmentIntendedUses.AsSentenceText:
                        return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserSentenceText : Images.SentenceTextSegment;
                    case mpSegmentIntendedUses.AsStyleSheet:
                        // GLOG : 3041 : JAB
                        // Return image index for user styles sheet segments.
                        return iMemberType == mpFolderMemberTypes.UserSegment && !bIsDesignerSegment ? Images.UserStyleSheetSegment : Images.StyleSheetSegment;
                    case mpSegmentIntendedUses.AsAnswerFile:
                        // GLOG : 3041 : JAB
                        // Return image index for answer file segments.
                        switch (iMemberType)
                        {
                            case mpFolderMemberTypes.UserSegment:
                                return Images.UserSegmentPacket;
                            case mpFolderMemberTypes.UserSegmentPacket:
                                return Images.UserSegmentPacket;
                            default: 
                                return Images.SegmentPacket;
                        }
                    case mpSegmentIntendedUses.AsMasterDataForm:
                        return Images.MasterDataForm;
                    default:
                        return Images.DocumentSegment;
                }
            }
        }
        /// <summary>
        /// Populates User Folder node with child folders and segments
        /// </summary>
        /// <param name="oNode"></param>
        private void RefreshUserFolderNode(UltraTreeNode oNode)
        {
            RefreshUserFolderNode(oNode, false);
        }
        /// <summary>
        /// Populates User Folder node with child folders and segments
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bForceRefresh"></param>
        private void RefreshUserFolderNode(UltraTreeNode oNode, bool bForceRefresh)
        {
            try
            {
                this.treeContent.SuspendLayout();
                //node has already been populated
                if (oNode.HasNodes == true && !bForceRefresh)
                    return;
                // clear nodes before refreshing
                if (oNode.HasNodes)
                    oNode.Nodes.Clear();
                // Filter folders on current user ID and parent node folder ID
                DateTime t0 = DateTime.Now;
                ContentTreeHelper.LoadNodeReference(oNode);
                string xID1, xID2, xParentID;
                if (ContentTreeHelper.IsMyFolderNode(oNode))
                {
                    xParentID = "0.0";
                    xID1 = "0";
                    xID2 = "0";
                }
                else if (oNode.Tag is UserFolder)
                {
                    xParentID = ((UserFolder)oNode.Tag).ID.ToString();
                    LMP.String.SplitString(xParentID, out xID1, out xID2, ".");
                }
                else
                    return;

                this.UserFolders.SetFilter(Session.CurrentUser.ID,
                    Int32.Parse(xID1), Int32.Parse(xID2));
                ArrayList oDisplay = this.UserFolders.ToDisplayArray();
                int iCount = oDisplay.Count;
                LMP.Trace.WriteNameValuePairs("Count of Folders", iCount);
                //cycle child folders nodes, adding each to tree
                for (int i = 0; i < iCount; i++)
                {
                    string xName = ((Object[])oDisplay[i])[0].ToString();
                    string xID = ((Object[])oDisplay[i])[1].ToString();
                    //add node to tree
                    //key is 'U<Parent.ID>|<Folder.ID>'
                    UltraTreeNode oTreeNode = oNode.Nodes.Add(string.Concat(oNode.Key, mpPathSeparator, xID), xName);
                    oTreeNode.Override.LabelEdit = Infragistics.Win.DefaultableBoolean.True;
                    oTreeNode.Tag = (object)xID;
                }
                // Get folder members based on ID of current Node (stored in Tag)
                FolderMembers oMembers = new LMP.Data.FolderMembers(Int32.Parse(xID1), Int32.Parse(xID2));
                oDisplay = oMembers.ToDisplayArray();
                iCount = oDisplay.Count;
                LMP.Trace.WriteNameValuePairs("Count of Members", iCount);
                for (int i = 0; i < iCount; i++)
                {
                    object[] aVals = (object[])oDisplay[i];
                    string xName = aVals[0].ToString();
                    string xID = aVals[1].ToString();

                    mpSegmentIntendedUses iIntendedUse = 0;
                    if (aVals[3].ToString() != "0")
                        iIntendedUse = (LMP.Data.mpSegmentIntendedUses)(int.Parse(aVals[3].ToString()));
                    mpFolderMemberTypes shMemberType = (mpFolderMemberTypes)((int)aVals[2]);

                    //add node to tree
                    //key is 'U<Parent.ID>|<Member.ID>'
                    UltraTreeNode oTreeNode = oNode.Nodes.Add(
                        "U" + oNode.Key + mpPathSeparator + xID, xName);

                    //set properties
                    oTreeNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                    //store ID in Tag
                    //Append 'U', 'A' or 'P' to start of string to differentiate User, Admin and Prefills

                    switch (shMemberType)
                    {
                        case mpFolderMemberTypes.AdminSegment:
                            // Member is an Admin Segment
                            oTreeNode.Tag = (object)string.Concat("A", xID);
                            break;
                        case mpFolderMemberTypes.VariableSet:
                            // Member is a VariableSet (Prefill)
                            oTreeNode.Tag = (object)string.Concat("P", xID);
                            ISegmentDef oSeg = ContentTreeHelper.GetSegmentDefFromNode(oTreeNode);
                            if (oSeg != null)
                                iIntendedUse = oSeg.IntendedUse;
                            break;
                        default:
                            // Member is a User Segment
                            oTreeNode.Tag = (object)string.Concat("U", xID);
                            break;
                    }
                    oTreeNode.Override.NodeAppearance.Image = GetImage(iIntendedUse, shMemberType, oTreeNode);
                }
                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }
            finally
            {
                this.treeContent.ResumeLayout();
            }
        }

        //private ISegmentDef RelatedSegmentDef(UltraTreeNode oNode)
        //{
        //    try
        //    {
        //        ContentTreeHelper.LoadNodeReference(oNode);
        //        if (oNode.Tag is FolderMember)
        //        {
        //            FolderMember oMember = (FolderMember)oNode.Tag;
        //            if (oMember.ObjectID2 == 0)
        //            {
        //                AdminSegmentDefs oSegs = new AdminSegmentDefs();
        //                AdminSegmentDef oSeg = (AdminSegmentDef)oSegs.ItemFromID(oMember.ObjectID1);
        //                return (ISegmentDef)oSeg;
        //            }
        //            else
        //            {
        //                UserSegmentDefs oSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, 0);
        //                UserSegmentDef oSeg = (UserSegmentDef)oSegs.ItemFromID(oMember.ObjectID1.ToString()
        //                    + "." + oMember.ObjectID2.ToString());
        //                return (ISegmentDef)oSeg;
        //            }
        //        }
        //        else
        //        {
        //            return (ISegmentDef)oNode.Tag;
        //        }
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}
        /// <summary>
        /// Return iSegmentDef object corresponding to FolderMember Node
        /// </summary>
        /// <param name="oNode"></param>
        /// <returns></returns>
        private VariableSetDef GetVariableSetFromNode(UltraTreeNode oNode)
        {
            try
            {
                ContentTreeHelper.LoadNodeReference(oNode);
                if (oNode.Tag is FolderMember)
                {
                    FolderMember oMember = (FolderMember)oNode.Tag;
                    if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
                    {
                        VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                        VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(oMember.ObjectID1.ToString()
                            + "." + oMember.ObjectID2.ToString());
                        return oDef;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return (VariableSetDef)oNode.Tag;
                }
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// If segment associated with saved data no longer exists
        /// prompts to allow assigning a different Segment or to delete
        /// the VariableSet definition
        /// </summary>
        /// <param name="oNode"></param>
        private bool CheckVariableSetSegment(UltraTreeNode oNode) //GLOG 6702
        {
            return CheckVariableSetSegment(oNode, false);
        }
        private bool CheckVariableSetSegment(UltraTreeNode oNode, bool bInserting) //GLOG 6702
        {
            VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
            VariableSetDef oDef = GetVariableSetFromNode(oNode);
            if (oDef != null)
            {
                if (ContentTreeHelper.GetSegmentDefFromNode(oNode) == null)
                {
                    if (ContentTreeHelper.UserIsOwnerOf(oNode))
                    {
                        //GLOG 6702: Different prompt if inserting saved data
                        string xMsg = "";
                        if (bInserting)
                            xMsg = LMP.Resources.GetLangString("Prompt_InsertSavedDataSegmentMissing");
                        else
                            xMsg = LMP.Resources.GetLangString("Prompt_SavedDataSegmentMissing");

                        //Underlying Segment object associated with saved data has been deleted
                        DialogResult iResult = MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, 
                                MessageBoxIcon.Question);

                        if (iResult == DialogResult.Yes)
                        {
                            //Update Segment ID assigned to VariableSet in DB
                            SegmentTreeview oForm = new SegmentTreeview();
                            oForm.Size = new Size(650, 400); //Set initial size
                            oForm.ShowDialog();
                            if (oForm.Value != "")
                            {
                                string xNewID = oForm.Value;
                                oDef.SegmentID = xNewID;
                                oDefs.Save(oDef);
                                ISegmentDef oSegDef = ContentTreeHelper.GetSegmentDefFromNode(m_oCurNode);

                                //Reset node icon 
                                oNode.Override.NodeAppearance.Image = GetImage(
                                    oSegDef.IntendedUse, mpFolderMemberTypes.VariableSet, false); //GLOG 8376
                                //GLOG 6702:  Continue inserting after assigning new segment
                                return bInserting;
                            }
                            return false;
                        }
                    }
                    DialogResult iDelete = MessageBox.Show(LMP.Resources.GetLangString("Prompt_DeleteSavedData"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (iDelete == DialogResult.Yes)
                        DeleteSegmentNode(oNode, false);
                    return false;
                }
                else
                    //return true if both VariableSet ID and Associated Segment ID are valid
                    return true;
            }
            else
            {
                //Underlying Variable Set object has been deleted
                MessageBox.Show(LMP.Resources.GetLangString("Msg_Object_Deleted_From_DB"),
                        LMP.ComponentProperties.ProductName,
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                DeleteSegmentNode(m_oCurNode, false);
                return false;
            }
        }
        /// <summary>
        /// Check if Folder object still exists in DB
        /// </summary>
        /// <param name="oFolder"></param>
        /// <returns></returns>
        private bool FolderExists(Folder oFolder)
        {
            this.AdminFolders.SetFilter(0, oFolder.ParentFolderID);
            Folder oTestFolder = null;
            try
            {
                oTestFolder = (Folder)this.AdminFolders.ItemFromID(oFolder.ID);
            }
            catch { }
            if (oTestFolder == null)
                return false;
            else
                return true;
        }
        /// <summary>
        /// Check if UserFolder object still exists in DB
        /// </summary>
        /// <param name="oFolder"></param>
        /// <returns></returns>
        private bool UserFolderExists(UserFolder oFolder)
        {
            this.UserFolders.SetFilter(Session.CurrentUser.ID, oFolder.ParentFolderID1, oFolder.ParentFolderID2);
            UserFolder oTestFolder = null;
            try
            {
                oTestFolder = (UserFolder)this.UserFolders.ItemFromID(oFolder.ID);
            }
            catch { }
            if (oTestFolder == null)
                return false;
            else
                return true;
        }
        /// <summary>
        /// displays the Author Preferences Manager
        /// </summary>
        /// <param name="oNode"></param>
        private void ManageAuthorPreferences(UltraTreeNode oNode)
        {
            int iID = 0;
            if (oNode.Tag is FolderMember)
            {
                iID = ((FolderMember)oNode.Tag).ObjectID1;
            }

            if (iID != 0)
            {
                AuthorPreferencesManager oForm = new AuthorPreferencesManager(iID, false);

                //add node text to title of form
                oForm.Text += " - " + oNode.Text;

                oForm.ShowDialog();
            }

        }

        /// <summary>
        /// shares the content represented by the specified node
        /// </summary>
        /// <param name="oNode"></param>
        private void ShareContent(UltraTreeNode oNode)
        {
            //use appropriate constructor params - this is
            //just for testing
            ContentTreeHelper.LoadNodeReference(oNode);
            mpObjectTypes iObjectType = 0;
            int iObjectID1 = 0;
            int iObjectID2 = 0;
            if (ContentTreeHelper.IsUserSegmentNode(oNode))
            {
                iObjectType = mpObjectTypes.UserSegment;
                LMP.String.SplitID(((UserSegmentDef)ContentTreeHelper.GetSegmentDefFromNode(oNode)).ID, out iObjectID1, out iObjectID2);
            }
            else if (ContentTreeHelper.IsVariableSetNode(oNode))
            {
                iObjectType = mpObjectTypes.VariableSet;
                LMP.String.SplitID(GetVariableSetFromNode(oNode).ID, out iObjectID1, out iObjectID2);
            }
            SharingForm oForm = new SharingForm(iObjectType, iObjectID1, iObjectID2);
            DialogResult iRes = oForm.ShowDialog();
            if (iRes == DialogResult.OK)
            {
                //Refresh Shared Folder nodes
                UltraTreeNode oSharedNode = this.treeContent.Nodes[2];
                if (oSharedNode.HasNodes)
                    RefreshAdminFolderNode(oSharedNode, true);
            }
        }
        /// <summary>
        /// Copy style definitions from the selected folder or segment to current document
        /// TODO: We might need to use the same method to copy styles when using InsertXML
        /// to create a new document.  Otherwise only styles that are in use will be copied.
        /// </summary>
        /// <param name="oNode"></param>
        private void ApplyStylesFromNode(UltraTreeNode oNode, bool bShowDialog)
        {
            ContentTreeHelper.LoadNodeReference(oNode);
            ISegmentDef oSegmentDef = null;
            string xBaseXML = "";
            DateTime t0 = DateTime.Now;
            try
            {
                // Get XML from Folder or Segment
                // Folder XML will be fragment consisting only of <w:style> nodes
                // Segment will contain complete document XML
                // NOTE:  Menu items dealing with Folder styles have been hidden
                //        So code dealing with Folders is not currently used
                if (oNode.Tag is UserFolder)
                {
                    xBaseXML = ((UserFolder)oNode.Tag).XML;
                    if (xBaseXML != "")
                    {
                        xBaseXML = "<wordDocument>" + xBaseXML + "</wordDocument>";
                    }
                }
                else if (oNode.Tag is Folder)
                {
                    xBaseXML = ((Folder)oNode.Tag).XML;
                    xBaseXML = "<wordDocument>" + xBaseXML + "</wordDocument>";
                }
                else if (ContentTreeHelper.IsAdminSegmentNode(oNode))
                {
                    if (oNode.Tag is AdminSegmentDef)
                    {
                        oSegmentDef = (ISegmentDef)oNode.Tag;
                    }
                    else
                    {
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(((FolderMember)oNode.Tag).ObjectID1);
                        oSegmentDef = oDef;
                    }
                }
                else if (ContentTreeHelper.IsUserSegmentNode(oNode))
                {
                    if (oNode.Tag is UserSegmentDef)
                    {
                        // Node is UserSegment in Find Results
                        oSegmentDef = (ISegmentDef)oNode.Tag;
                    }
                    else
                    {
                        UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User,
                            Session.CurrentUser.ID);
                        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(((FolderMember)oNode.Tag).ObjectID1.ToString() + "."
                            + ((FolderMember)oNode.Tag).ObjectID2.ToString());
                        oSegmentDef = oDef;
                    }
                }
                else
                    return;
            }
            catch
            {
            }
            if (oSegmentDef == null)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_NoStyleDefinitions"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //show copy styles form
            if (bShowDialog)
            {
                StylesManager oForm = new StylesManager(oSegmentDef);
                DialogResult iRes = oForm.ShowDialog();
            }
            else
                this.m_oMPDocument.UpdateStylesFromXML(oSegmentDef.XML);
        }
        /// <summary>
        /// Copy styles definitions from current document to XML property of folder or userfolder
        /// </summary>
        /// <param name="oNode"></param>
        private void SaveDocStylesToFolder(UltraTreeNode oNode)
        {
            string xFolderXML = "";
            string xStyleXML = "";
            ContentTreeHelper.LoadNodeReference(oNode);
            if (ContentTreeHelper.IsAdminFolderNode(oNode) && Session.AdminMode)
            {
                xFolderXML = ((Folder)oNode.Tag).XML;
            }
            else if (!ContentTreeHelper.IsMyFolderNode(oNode) && ContentTreeHelper.IsUserFolderNode(oNode) && ContentTreeHelper.UserIsOwnerOf(oNode))
            {
                xFolderXML = ((UserFolder)oNode.Tag).XML;
            }
            else
                // not supported for this node
                return;
            if (xFolderXML != "")
            {
                // Folder already has XML defined in DB.  Prompt to overwrite.
                DialogResult iAns = MessageBox.Show(LMP.Resources.GetLangString("Msg_OverwriteFolderStyles"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (iAns != DialogResult.Yes)
                    return;
            }
            // Extract <w:style> nodes from full document XML
            xStyleXML = String.ExtractStyleXML(Session.CurrentWordApp.ActiveDocument.Content.get_XML(false));

            try
            {
                // Set XML property of Folder and Save
                if (oNode.Tag is Folder)
                {
                    Folder oFolder = (Folder)oNode.Tag;
                    oFolder.XML = xStyleXML;
                    this.AdminFolders.Save((LongIDSimpleDataItem)oFolder);
                }
                else if (oNode.Tag is UserFolder)
                {
                    UserFolder oFolder = (UserFolder)oNode.Tag;
                    oFolder.XML = xStyleXML;
                    this.UserFolders.Save((StringIDSimpleDataItem)oFolder);
                }
                else
                    return;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(oE.Message);
            }
        }
        /// <summary>
        /// Displays dialog to change name and description properties for Folder or FolderMember
        /// </summary>
        /// <param name="oNode"></param>
        private void SetPropertiesForNode(UltraTreeNode oNode)
        {
            ContentTreeHelper.LoadNodeReference(oNode);

            //GLOG : 6072 : CEH
            if (IsExternalContentNode(oNode))
            {
                //prompt to edit properties of an external file
                ExternalContentForm oForm = new ExternalContentForm();
                oForm.ShowForm(ExternalContentForm.DialogMode.EditProperties, oNode);
                if (oForm.DialogResult == DialogResult.OK)
                {
                    EditFileNode(oNode, oForm.FullFileName,
                        oForm.DisplayName, oForm.Help);
                }
            }
            else
            {
                // Don't allow renaming Admin folders or segments
                // unless in Admin mode
                ContentPropertyForm oForm = new ContentPropertyForm();
                oForm.ShowForm(this.ParentForm, oNode, Form.MousePosition, ContentPropertyForm.DialogMode.EditProperties);
                if (!oForm.Canceled)
                {
                    SetBrowserContents(oNode);
                }
            }
        }
        /// <summary>
        /// Set Contents of Description panel based on type of Node selected
        /// </summary>
        /// <param name="oNode"></param>
        private void SetBrowserContents(UltraTreeNode oNode)
        {
            if (!oNode.Control.Visible)
                return;

            string xDescription = "";

            if (oNode != null)
            {
                xDescription = this.GetBrowserContents(oNode);
                
                if (xDescription == null)
                {
                    xDescription = "";
                }
            }

            TaskPane.SetHelpText(this.wbDescription, xDescription);
        }

        private string GetBrowserContents(UltraTreeNode oNode)
        {
            // GLOG : 3292 : JAB
            // Instantiate the FirmApplicationSettings object and initialize
            // the My, Public, and Shared folders help text.
            if (m_oFirmAppSettings == null)
            {
                m_oFirmAppSettings = new FirmApplicationSettings(Session.CurrentUser.ID);
                
                m_xSharedFoldersHelpText = m_oFirmAppSettings.SharedFoldersHelpText;
                m_xMyFoldersHelpText = m_oFirmAppSettings.MyFoldersHelpText;
                m_xPublicFoldersHelpText = m_oFirmAppSettings.PublicFoldersHelpText;
            }

            if (!oNode.Control.Visible)
                return null;
            string xDescription = "";
            if (oNode == null)
                xDescription = "";
            else if (oNode.Tag is FolderMember)
                if (((FolderMember)oNode.Tag).ObjectTypeID == mpFolderMemberTypes.VariableSet)
                    xDescription = GetVariableSetFromNode(oNode).Description;
                else
                    xDescription = ContentTreeHelper.GetSegmentDefFromNode(oNode).HelpText;
            else if (oNode.Tag is Folder)
                xDescription = ((Folder)oNode.Tag).Description;
            else if (oNode.Tag is UserFolder)
                xDescription = ((UserFolder)oNode.Tag).Description;
            else if (oNode.Tag is AdminSegmentDef)
                xDescription = ((AdminSegmentDef)oNode.Tag).HelpText;
            else if (oNode.Tag is UserSegmentDef)
                xDescription = ((UserSegmentDef)oNode.Tag).HelpText;
            else if (oNode.Tag is VariableSetDef)
                xDescription = ((VariableSetDef)oNode.Tag).Description;
            else if (ContentTreeHelper.IsOwnerFolderNode(m_oCurNode))
                xDescription = LMP.Resources.GetLangString("Prompt_OwnerSharedItems") + ((Person)m_oCurNode.Tag).FullName;
            // GLOG : 3292 : JAB
            // Use the FirmApplicationSettings help text specified.
            else if (ContentTreeHelper.IsSharedFolderRootNode(oNode))
                xDescription = m_xSharedFoldersHelpText;
            else if (ContentTreeHelper.IsPublicFolderNode(oNode))
                xDescription = m_xPublicFoldersHelpText;
            else if (ContentTreeHelper.IsMyFolderNode(oNode))
                xDescription = m_xMyFoldersHelpText;
            //xDescription = String.RestoreMPReservedChars(xDescription);
            return (xDescription);
        }
        /// <summary>
        /// Inserts Segment with the options specified in DefaultDoubleClickBehavior
        /// </summary>
        /// <param name="oNode"></param>
        private LMP.Architect.Base.Segment InsertSegmentFromNodeDoubleClick(UltraTreeNode oNode)
        {
            try
            {
                LMP.Architect.Base.Segment oSegment = null;
                if (ContentTreeHelper.IsVariableSetNode(oNode))
                {
                    // Allow variable segment defs that have no segment id's.
                    VariableSetDef oVarSetDef = GetVariableSetFromNode(oNode);

                    if (oVarSetDef == null || oVarSetDef.SegmentID != "")
                    {
                        if (!CheckVariableSetSegment(oNode, true))
                        {
                            return null;
                        }
                    }

                    string xSegmentID = GetVariableSetFromNode(oNode).SegmentID;

                    if (xSegmentID == "")
                    {
                        // Variable sets that don't have associated segment id's require that
                        // the user select content to apply the variable set to.
                        SegmentTreeview oForm = new SegmentTreeview();
                        oForm.Size = new Size(650, 400); //Set initial size
                        oForm.ShowDialog();

                        if (oForm.Value == "")
                        {
                            // Nothing was selected. Just return.
                            return null;
                        }
                        else
                        {
                            // Use the segment id for the content selected.
                            xSegmentID = oForm.Value;
                        }
                    }

                    //Prefill can use either Admin or User Segment
                    if (!xSegmentID.Contains(".") || xSegmentID.EndsWith(".0"))
                    {
                        //Admin Segment
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        int iID1 = 0;
                        int iID2 = 0;
                        LMP.String.SplitID(xSegmentID, out iID1, out iID2);
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);
                        LMP.Trace.WriteNameValuePairs("DefaultDoubleClickBehavior", oDef.DefaultDoubleClickBehavior);

                        oSegment = InsertSegmentFromNode(oNode, (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation,
                            (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior, null, false, xSegmentID);
                    }
                    else
                    {
                        //User Segment
                        UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);
                        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xSegmentID);

                        if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocument)
                        {
                            oSegment = InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertInNewDocument, 0, null, false, xSegmentID);
                        }
                        else if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent ||
                            oDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText ||
                            oDef.IntendedUse == mpSegmentIntendedUses.AsSentenceText)
                        {
                            oSegment = InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtSelection, 0, null, false, xSegmentID);
                        }
                    }
                }
                else
                {
                    ISegmentDef oSegDef = ContentTreeHelper.GetSegmentDefFromNode(oNode);

                    if (oSegDef == null)
                    {
                        // Underlying Segment has been deleted
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_Object_Deleted_From_DB"), LMP.ComponentProperties.ProductName,
                               MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DeleteSegmentNode(oNode, false);
                        return null;
                    }
                    else if (ContentTreeHelper.IsStyleSheetNode(oNode))
                    {
                        ApplyStylesFromNode(oNode, false);
                        return null;
                    }
                    else if (ContentTreeHelper.IsAdminSegmentNode(oNode))
                    {
                        if (oNode.Tag is FolderMember)
                        {
                            AdminSegmentDefs oDefs = new AdminSegmentDefs();
                            int iID = ((FolderMember)oNode.Tag).ObjectID1;
                            AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                            LMP.Trace.WriteNameValuePairs("DefaultDoubleClickBehavior", oDef.DefaultDoubleClickBehavior);

                            if ((oDef.TypeID == mpObjectTypes.Envelopes || oDef.TypeID == mpObjectTypes.Labels)
                                && (Session.CurrentWordApp.Selection.Type > Word.WdSelectionType.wdSelectionIP))
                                //GLOG 2988: Default to Insert using Selection if there is selected text
                                oSegment = InsertUsingSelection(oNode);
                            else if (oDef.TypeID == mpObjectTypes.SegmentPacket)
                            {
                                InsertMultiple(oDef.XML, null);
                            }
                            else
                                oSegment = InsertSegmentFromNode(oNode, (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation,
                                    (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior);
                        }
                        else
                        {
                            AdminSegmentDef oDef = (AdminSegmentDef)oNode.Tag;
                            LMP.Trace.WriteNameValuePairs("DefaultDoubleClickBehavior", oDef.DefaultDoubleClickBehavior);

                            if ((oDef.TypeID == mpObjectTypes.Envelopes || oDef.TypeID == mpObjectTypes.Labels)
                                && (Session.CurrentWordApp.Selection.Type > Word.WdSelectionType.wdSelectionIP))
                                //GLOG 2988: Default to Insert using Selection if there is selected text
                                oSegment = InsertUsingSelection(oNode);
                            else
                                oSegment = InsertSegmentFromNode(oNode, (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation,
                                (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior);
                        }
                    }
                    else if (ContentTreeHelper.IsAnswerFileNode(oNode))
                    {
                        oSegment = InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertInNewDocument,
                            Segment.InsertionBehaviors.Default);
                    }
                    else
                    {
                        //is user segment node - 
                        //insert based on intended use

                        UserSegmentDef oDef = null;

                        if (oNode.Tag is UserSegmentDef)
                        {
                            oDef = (UserSegmentDef)oNode.Tag;
                        }
                        else
                        {
                            FolderMember oMember = (FolderMember)oNode.Tag;
                            string xID = oMember.ObjectID1.ToString() + "." + oMember.ObjectID2.ToString();
                            UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);
                            try
                            {
                                oDef = (UserSegmentDef)oDefs.ItemFromID(xID);
                            }
                            catch (LMP.Exceptions.NotInCollectionException)
                            {
                                //the segment no longer exists, or permission
                                //has been revoked - alert and delete from tree
                                MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_SegmentNotAccessible"), oMember.Name),
                                    LMP.ComponentProperties.ProductName,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                                DeleteSegmentNode(oNode, false);

                                return null;
                            }
                        }

                        //GLOG item #5987 - dcf
                        if (oDef.TypeID == mpObjectTypes.SavedContent)
                        {
                            oSegment = InsertSegmentFromNode(oNode, (Segment.InsertionLocations)(int)oDef.DefaultDoubleClickLocation, 
                                (Segment.InsertionBehaviors)(int)oDef.DefaultDoubleClickBehavior);
                        }
                        else if (oDef.TypeID == mpObjectTypes.SegmentPacket)
                        {
                            InsertMultiple(oDef.XML, null);
                        }
                        else if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocument)
                        {
                            oSegment = InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertInNewDocument, 0);
                        }
                        else if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent ||
                            oDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText ||
                            oDef.IntendedUse == mpSegmentIntendedUses.AsSentenceText)
                        {
                            oSegment = InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtSelection, 0);
                        }
                    }
                }
                return oSegment;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(LMP.Resources.GetLangString("Error_Segment_Insertion") + " " + oNode.Text, oE);
            }
        }
        /// <summary>
        /// Inserts Segment with the options specified in DefaultDragBehavior
        /// </summary>
        /// <param name="oNode"></param>
        private void InsertSegmentFromNodeDrag(UltraTreeNode oNode)
        {
            try
            {
                if (ContentTreeHelper.IsVariableSetNode(oNode))
                {
                     if (!CheckVariableSetSegment(oNode, true))
                        return;

                    string xSegmentID = GetVariableSetFromNode(oNode).SegmentID;
                    //Prefill can use either Admin or User Segment
                    if (!xSegmentID.Contains(".") || xSegmentID.EndsWith(".0"))
                    {
                        //Admin Segment
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        int iID1 = 0;
                        int iID2 = 0;
                        LMP.String.SplitID(xSegmentID, out iID1, out iID2);
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);
                        LMP.Trace.WriteNameValuePairs("DefaultDragBehavior", oDef.DefaultDragBehavior);
                        InsertSegmentFromNode(oNode, (Segment.InsertionLocations)oDef.DefaultDragLocation,
                            (Segment.InsertionBehaviors)oDef.DefaultDragBehavior);
                    }
                    else
                    {
                        //User Segment
                        InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtSelection, 0);
                    }
                }
                else if (ContentTreeHelper.GetSegmentDefFromNode(oNode) == null)
                {
                    // Underlying Segment has been deleted
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_Object_Deleted_From_DB"), LMP.ComponentProperties.ProductName,
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DeleteSegmentNode(oNode, false);
                    return;
                }
                else if (ContentTreeHelper.IsStyleSheetNode(oNode))
                {
                    ApplyStylesFromNode(oNode, false);
                }
                else if (ContentTreeHelper.IsAnswerFileNode(oNode))
                {
                    InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertInNewDocument,
                        Segment.InsertionBehaviors.Default);
                }
                else if (ContentTreeHelper.IsAdminSegmentNode(oNode))
                {
                    if (oNode.Tag is FolderMember)
                    {
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        int iID = ((FolderMember)oNode.Tag).ObjectID1;
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);
                        LMP.Trace.WriteNameValuePairs("DefaultDragLocation", oDef.DefaultDragLocation, "DefaultDragBehavior", oDef.DefaultDragBehavior);
                        InsertSegmentFromNode(oNode, (Segment.InsertionLocations)oDef.DefaultDragLocation,
                            (Segment.InsertionBehaviors)oDef.DefaultDragBehavior);
                    }
                    else
                    {
                        AdminSegmentDef oDef = (AdminSegmentDef)oNode.Tag;
                        LMP.Trace.WriteNameValuePairs("DefaultDragLocation", oDef.DefaultDragLocation, "DefaultDragBehavior", oDef.DefaultDragBehavior);
                        InsertSegmentFromNode(oNode, (Segment.InsertionLocations)oDef.DefaultDragLocation,
                            (Segment.InsertionBehaviors)oDef.DefaultDragBehavior);
                    }

                }
                else
                {
                    InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertAtSelection, 0);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Insertion") + " " + oNode.Text, oE);
            }

        }
        private void CreateStyleSummary(UltraTreeNode oNode)
        {
            this.TaskPane.ScreenUpdating = false;
            ContentTreeHelper.LoadNodeReference(oNode);
            if (!ContentTreeHelper.IsStyleSheetNode(oNode))
            {
                return;
            }
            string xID = "";
            string xXML = "";
            if (oNode.Tag is FolderMember)
            {
                FolderMember oMember = (FolderMember)oNode.Tag;
                if (oMember.FolderType == mpFolderTypes.Admin)
                {
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oMember.ObjectID1);
                    xID = oDef.ID.ToString();
                    xXML = oDef.XML;
                }
                else
                {
                    xID = oMember.ObjectID1.ToString() + "." + oMember.ObjectID2.ToString();
                    UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);
                    UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xID);
                    xXML = oDef.XML;
                }
            }
            else if (oNode.Tag is AdminSegmentDef)
            {
                xID = ((AdminSegmentDef)oNode.Tag).ID.ToString();
                xXML = ((AdminSegmentDef)oNode.Tag).XML;
            }
            else if (oNode.Tag is UserSegmentDef)
            {
                xID = ((UserSegmentDef)oNode.Tag).ID;
                xXML = ((UserSegmentDef)oNode.Tag).XML;
            }
            else
                return;

            if (xXML == "")
                return;

            try
            {
                //we need to insert in a new document - that document's 
                //ForteDocument object will need to insert the segment, as 
                //the current doc's ForteDocument points to the current doc - 
                //specify a pending action that the new content manager instance 
                //will process 
                string xOutputFile = System.IO.Path.GetTempFileName();
                System.IO.StreamWriter oWriter = new System.IO.StreamWriter(
                    xOutputFile, true, Encoding.UTF8);
                //Write segment XML to temp file
                //JTS 6/21/10: GetMinimalStyleDocumentXML not compatible with WordOpen format
                //so just use Segment XML for source
                //xXML = String.GetMinimalStyleDocumentXML(xXML);
                oWriter.Write(xXML);
                oWriter.Close();
                //create a new document based on temp file
                Word.Document oNewDoc = LMP.Forte.MSWord.WordApp.CreateDocument(xOutputFile, true, false);
                oNewDoc.ActiveWindow.View.Type = Microsoft.Office.Interop.Word.WdViewType.wdNormalView;
                oNewDoc.ActiveWindow.StyleAreaWidth = 100;
                oNewDoc.ActiveWindow.Caption = oNode.Text;
                oNewDoc.Activate();
                ForteDocument oMPDoc = new ForteDocument(oNewDoc);
                oMPDoc.InsertStyleSummary(true, string.Format(
                    LMP.Resources.GetLangString("Msg_StyleSummaryDescription"),
                    oNode.Text));
                oNewDoc.Saved = true;
                try
                {
                    //Clean up temp file
                    File.Delete(xOutputFile);
                }
                catch { }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Insertion") +
                    " " + oNode.Text, oE);
            }
            finally
            {
                this.TaskPane.ScreenUpdating = true;
            }
        }

        private void Transform(UltraTreeNode oNode)
        {
            //prompt to confirm execution
            DialogResult iChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Prompt_ReplaceDocumentContent"),
                LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (iChoice == DialogResult.No)
                return;

            LMP.MacPac.Application.Transform(oNode, this.TaskPane);
        }

        /// <summary>
        /// inserts the segment corresponding to the
        /// specified node using the current seletion -
        /// currently available only to labels and envelopes
        /// </summary>
        /// <param name="oNode"></param>
        private LMP.Architect.Base.Segment InsertUsingSelection(UltraTreeNode oNode)
        {
            ISegmentDef oSeg = ContentTreeHelper.GetSegmentDefFromNode(oNode);
            if (oSeg != null)
            {
                PrefillChooser oForm = new PrefillChooser(oSeg,
                    PrefillChooser.PrefillSources.Selection);

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    this.TaskPane.ScreenRefresh();

                    //get prefill using analyzer
                    Word.Selection oSel = Session.CurrentWordApp.Selection;

                    DocAnalyzer.DocType oDocType = new LMP.DocAnalyzer.DocType("RecipientList", oSel);
                    if (oSel.Text == null)
                        return null;
                    string xRecipients = oDocType.AnalyzeDocument(
                        LMP.DocAnalyzer.DocType.OutputFormats.DelimitedString, false, false);

                    if (xRecipients == null)
                        return null;

                    //GLOG 3475: Get related Segment ID for Prefill
                    string xSegID = "";

                    if (oSeg.TypeID == mpObjectTypes.UserSegment)
                        xSegID = ((UserSegmentDef)oSeg).ID;
                    // GLOG : 6934 : CEH
                    else if (oSeg.TypeID == mpObjectTypes.SavedContent)
                        xSegID = "";
                    else
                        xSegID = ((AdminSegmentDef)oSeg).ID.ToString();

                    Prefill oPrefill = new Prefill(xRecipients, "Recipients", xSegID);

                    //Continue with Segment insertion, passing Prefill object
                    return InsertSegmentFromNode(oNode, (Segment.InsertionLocations)oForm.InsertionLocation,
                        (Segment.InsertionBehaviors)oForm.InsertionBehavior, oPrefill);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        private void InsertUsingClientMatterNumber(UltraTreeNode oNode)
        {
            //GLOG 8469: Convert Tag object to SegmentDef - can be FolderMember or SegmentDef
            ISegmentDef oSegDef = ContentTreeHelper.GetSegmentDefFromNode(oNode);
            Segment.InsertionLocations oDefLocation = 0;
            Segment.InsertionBehaviors oDefBehavior = 0;
            LMP.Data.mpObjectTypes iTypeID = 0;
            string xXML = null;
            string xSegmentID = "";
            string xClientMatterNumber = "";

            InsertUsingClientMatterForm oLookupForm = null;

            //get default insertion location and behavior
            
            if (oSegDef is AdminSegmentDef)
            {
                iTypeID = oSegDef.TypeID;
                xXML = oSegDef.XML;
                xSegmentID = ((AdminSegmentDef)oSegDef).ID.ToString();
                oLookupForm = new InsertUsingClientMatterForm(null, (AdminSegmentDef)oSegDef);
            }
            else if (oSegDef.TypeID == mpObjectTypes.SegmentPacket)
            {
                iTypeID = oSegDef.TypeID;
                xXML = oSegDef.XML;
                oLookupForm = new InsertUsingClientMatterForm(null, (UserSegmentDef)oSegDef);
            }
            else

            {
                oLookupForm = new InsertUsingClientMatterForm(null);
            }

            //get GrailStore object
            GrailBackend oBackend = LMP.Grail.GrailStore.Backend;

            //connect to GrailBackend - we do this here
            //to avoid the ClientMatter lookup from having to
            //connect to the store again
            oBackend.Connect(null);

            string xPrefill = null;

            if (oLookupForm.ShowDialog() == DialogResult.OK)
            {
                xClientMatterNumber = oLookupForm.Matter.Number;
                xPrefill = oBackend.GetData(xClientMatterNumber);

                //modify jurisdiction key names for prefill that contains
                //keys of the form SegmentID_LX - this is the case for 
                //the TSG grail demo database
                if (xSegmentID != "")
                {
                    xPrefill = xPrefill.Replace(xSegmentID + "_L0", "L0")
                        .Replace(xSegmentID + "_L1", "L1")
                        .Replace(xSegmentID + "_L2", "L2")
                        .Replace(xSegmentID + "_L3", "L3")
                        .Replace(xSegmentID + "_L4", "L4");
                }
                oDefLocation = oLookupForm.InsertionLocation;
                oDefBehavior = oLookupForm.InsertBehaviors;
            }
            else
            {
                return;
            }

            oBackend.Disconnect();

            Prefill oPrefill = new Prefill(xPrefill, "SegmentPrefill", xSegmentID);

            if (oSegDef is UserSegmentDef && iTypeID == mpObjectTypes.SegmentPacket)
            {
				//GLOG 7316
                ArrayList oNewDocSegments = LMP.MacPac.Application.InsertMultipleSegments(xXML, oPrefill);
                Segment[] aNewDocSegments = (Segment[])oNewDocSegments.ToArray(typeof(Segment));
            }
            else if (oSegDef is AdminSegmentDef)
            {
                //test to see if segment is a segment packet
                if (iTypeID == mpObjectTypes.SegmentPacket)
                {
                    bool bDeautomate = false;
                    bool bSave = false;

                    //parse out save/deautomate flags -
                    //this functionality was added for Pierce
                    if (xXML.Substring(1, 1) == "|" && xXML.Substring(3, 1) == "|")
                    {
                        bDeautomate = xXML.Substring(0, 1) == "1";
                        bSave = xXML.Substring(2, 1) == "1";
                        xXML = xXML.Substring(4);
                    }

                    //insert multiple segments using selected prefill
					//GLOG 7316
                    ArrayList oNewDocSegments = LMP.MacPac.Application.InsertMultipleSegments(xXML, oPrefill);
                    Segment[] aNewDocSegments = (Segment[])oNewDocSegments.ToArray(typeof(Segment));

                    foreach (Segment oSegment in aNewDocSegments)
                    {
                        if (bDeautomate)
                        {
                            LMP.MacPac.Application.RemoveAutomation(oSegment.ForteDocument.WordDocument, false);
                        }

                        if (bSave)
                        {
                            oBackend.SaveFile(oSegment.ForteDocument.WordDocument,
                                xClientMatterNumber, oSegment.DisplayName, oSegment.TypeID.ToString());
                        }
                    }
                }
                else
                {
                    //admin segments
                    InsertSegmentFromNode(oNode, oDefLocation,
                        oDefBehavior, oPrefill, false, "", true); //GLOG 7128
                }
            }
            else
            {
                //user segments
                InsertSegmentFromNode(oNode,
                    Segment.InsertionLocations.Default,
                    Segment.InsertionBehaviors.Default, oPrefill);
            }

            LMP.MacPac.Application.ScreenUpdating = true;
        }
        /// <summary>
        /// inserts multiple segments - prompts for segments to insert
        /// </summary>
        private void InsertMultiple()
        {
            InsertMultiple(null, null);
        }

        /// <summary>
        /// inserts the specified multiple segments
        /// </summary>
        /// <param name="xSegmentInsertionInfo"></param>
        private void InsertMultiple(string xSegmentInsertionInfo)
        {
            InsertMultiple(xSegmentInsertionInfo, null);
        }
        /// <summary>
        /// inserts multiple segments using the saved 
        /// prefill referenced by the specified node
        /// </summary>
        /// <param name="oNode"></param>
        private void InsertMultiple(string xSegmentInsertionInfo, UltraTreeNode oPrefillNode)
        {
            Prefill oPrefill = null;

            if (oPrefillNode != null)
            {
                VariableSetDef oVarDef = GetVariableSetFromNode(oPrefillNode);

                oPrefill = new Prefill(oVarDef.ContentString, oVarDef.Name, oVarDef.SegmentID);
            }
            //GLOG 7316: Moved to 
            LMP.MacPac.Application.InsertMultipleSegments(xSegmentInsertionInfo, oPrefill, null);
        }
        private void EditSavedData(UltraTreeNode oNode)
        {
            InsertSegmentFromNode(oNode, Segment.InsertionLocations.InsertInNewDocument, Segment.InsertionBehaviors.Default);
        }
        /// <summary>
        /// creates a segment packet
        /// </summary>
        /// <param name="oNode"></param>
        private void CreateSegmentPacket(UltraTreeNode oNode)
        {
            //TaskPane oTP = null;
            //Segment oSegment = null;
            string xName = null;
            string xDisplayName = null;
            string xDescription = null;

            //GLOG : 8164 : ceh
            MultipleSegmentsForm oForm = new MultipleSegmentsForm("Create Segment Packet", "Sa&ve",
                null, false, null, ContentTreeHelper.IsAdminFolderNode(oNode));

            if (oForm.ShowDialog() == DialogResult.OK)
            {
                xDescription = LMP.Resources.GetLangString("Msg_SegmentPacketDescription") + oForm.SegmentList;

                ContentPropertyForm oSegForm = new ContentPropertyForm();
                //GLOG : 15816 : JSW
                //change bExecuteOnFinish parameter from false to true.
                if (oSegForm.ShowForm(null, oNode, new System.Drawing.Point(),
                    ContentPropertyForm.DialogMode.NewSegmentPacket, null,
                    null, true, xDescription) == DialogResult.OK)
                {
                    xName = oSegForm.ContentName;
                    xDisplayName = oSegForm.ContentDisplayName;
                    xDescription = oSegForm.ContentDescription;

                    if (ContentTreeHelper.IsAdminFolderNode(oNode))
                    {
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.Create(mpObjectTypes.SegmentPacket);

                        oDef.TypeID = mpObjectTypes.SegmentPacket;
                        oDef.XML = oForm.Value;
                        oDef.Name = xName;
                        oDef.DisplayName = xDisplayName;
                        oDef.HelpText = xDescription;
                        oDef.IntendedUse = mpSegmentIntendedUses.AsAnswerFile;
                        oDef.MenuInsertionOptions = 0;
                        oDef.DefaultMenuInsertionBehavior = 0;
                        oDef.DefaultDoubleClickBehavior = 0;
                        oDef.DefaultDoubleClickLocation = (int)Segment.InsertionLocations.Default;
                        oDef.DefaultDragBehavior = 0;
                        oDef.DefaultDragLocation = 0;
                        oDefs.Save(oDef);

                        //create folder assignment for new segment
                        Folder oFolder = (Folder)oNode.Tag;
                        FolderMember oMember = (FolderMember)(oFolder.GetMembers().Create());
                        oMember.ObjectID1 = oDef.ID;
                        oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);

                        RefreshAdminFolderNode(oNode, true);

                        UltraTreeNode oNewNode = this.treeContent.GetNodeByKey(
                            string.Concat("A", oNode.Key, mpPathSeparator, oMember.ID));
                        this.treeContent.ActiveNode = oNewNode;

                        oNewNode.Selected = true;
                    }
                    else
                    {
                        UserSegmentDefs oDefs = new UserSegmentDefs();
                        UserSegmentDef oDef = (UserSegmentDef)oDefs.Create();

                        oDef.TypeID = mpObjectTypes.SegmentPacket;
                        oDef.XML = oForm.Value;
                        oDef.Name = xName;
                        oDef.DisplayName = xDisplayName;
                        oDef.HelpText = xDescription;
                        oDef.OwnerID = LMP.MacPac.Session.CurrentUser.ID;
                        oDef.IntendedUse = mpSegmentIntendedUses.AsAnswerFile;
                        oDef.MenuInsertionOptions = 0;
                        oDef.DefaultMenuInsertionBehavior = 0;
                        oDef.DefaultDoubleClickBehavior = 0;
                        oDef.DefaultDoubleClickLocation = (int)Segment.InsertionLocations.Default;
                        oDef.DefaultDragBehavior = 0;
                        oDef.DefaultDragLocation = 0;
                        oDefs.Save(oDef);

                        //create user folder member
                        string xID = ((LMP.Data.UserFolder)oNode.Tag).ID;
                        int iID1, iID2;

                        LMP.Data.Application.SplitID(xID, out iID1, out iID2);

                        FolderMembers oMembers = new FolderMembers(iID1, iID2);
                        FolderMember oMember = (FolderMember)oMembers.Create();

                        xID = oDef.ID;
                        LMP.Data.Application.SplitID(xID, out iID1, out iID2);
                        oMember.ObjectID1 = iID1;
                        oMember.ObjectID2 = iID2;
                        oMember.ObjectTypeID = mpFolderMemberTypes.UserSegmentPacket;
                        oMembers.Save(oMember);
                    }
                    this.RefreshFolderNode(m_oCurNode);
                }
            }
        }

        /// <summary>
        /// inserts multiple segments using the saved 
        /// prefill referenced by the specified node
        /// </summary>
        /// <param name="oNode"></param>
        private void InsertMultipleusingSavedPrefillOLD(UltraTreeNode oNode)
        {
            SegmentTreeview oForm = new SegmentTreeview(true, "");

            if (oForm.ShowDialog() == DialogResult.OK)
            {
                string xIDs = oForm.Value;

                VariableSetDef oVarDef = GetVariableSetFromNode(oNode);
                //GLOG 3475
                Prefill oPrefill = new Prefill(oVarDef.ContentString, oVarDef.Name, oVarDef.SegmentID);

                string[] aDetails = xIDs.Split('~');

                foreach (string xDetail in aDetails)
                {
                    string[] aIDs = xDetail.Split(';');
                    string xID = aIDs[1];
                    //GLOG 6966: Make sure string can be parsed as a Double even if
                    //the Decimal separator is something other than '.' in Regional Settings
                    if (xID.EndsWith(".0"))
                    {
                        xID = xID.Replace(".0", "");
                    }

                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID((int)double.Parse(xID));

                    InsertSegment(oDef, Segment.InsertionLocations.InsertInNewDocument, Segment.InsertionBehaviors.Default, oPrefill, false);
                }
            }
        }

        /// <summary>
        /// inserts the segment corresponding to the 
        /// specified node using a saved prefill
        /// </summary>
        /// <param name="oNode"></param>
        private void InsertSavedPrefillFromNode(UltraTreeNode oNode)
        {
            if (ContentTreeHelper.IsAdminSegmentNode(oNode) || ContentTreeHelper.IsUserSegmentNode(oNode))
            {
                ISegmentDef oSeg = ContentTreeHelper.GetSegmentDefFromNode(oNode);
                if (oSeg != null)
                {
                    PrefillChooser oForm = new PrefillChooser(oSeg,
                        PrefillChooser.PrefillSources.SavedPrefill);

                    if (oForm.ShowDialog() == DialogResult.OK)
                    {
                        string xVSetID = oForm.VariableSetID;
                        //GLOG 5676: Don't limit to this User's saved data, 
                        //since shortcuts to shared items may also be used
                        VariableSetDefs oDefs = new VariableSetDefs(mpVariableSetsFilterFields.Owner, 0);
                        VariableSetDef oDef = (VariableSetDef)oDefs.ItemFromID(xVSetID);
                        //GLOG 3475
                        Prefill oPrefill = new Prefill(oDef.ContentString, oDef.Name, oDef.SegmentID);

                        Segment.InsertionLocations iLocation = (Segment.InsertionLocations)oForm.InsertionLocation;
                        Segment.InsertionBehaviors iOptions = (Segment.InsertionBehaviors)oForm.InsertionBehavior;

                        this.TaskPane.ScreenRefresh();

                        //Continue with Segment insertion, passing Prefill object
                        InsertSegmentFromNode(oNode, iLocation, iOptions, oPrefill);
                    }
                }
            }
        }
        /// <summary>
        /// inserts the segment corresponding to the specified node
        /// using a prefill created from the selected top level segment
        /// </summary>
        /// <param name="oNode"></param>
        private void InsertTransientPrefillFromNode(UltraTreeNode oNode)
        {
            if (ContentTreeHelper.IsAdminSegmentNode(oNode) || ContentTreeHelper.IsUserSegmentNode(oNode))
            {
                if (ContentTreeHelper.IsSegmentPacket(oNode))
                {
                    //create prefill
                    Prefill oPrefill = null;

                    Segment oSourceSeg = this.TaskPane.ForteDocument
                        .GetTopLevelSegmentFromSelection();

                    if (oSourceSeg != null)
                        oPrefill = new Prefill(oSourceSeg);

                    this.TaskPane.ScreenRefresh();

                    ISegmentDef oDef = ContentTreeHelper.GetSegmentDefFromNode(oNode);
                    
                    //insert segment, passing Prefill object
					//GLOG 7316
                    LMP.MacPac.Application.InsertMultipleSegments(oDef.XML, oPrefill);                    
                }
                else
                {
                    ISegmentDef oSeg = ContentTreeHelper.GetSegmentDefFromNode(oNode);
                    if (oSeg != null)
                    {
                        //get selected segment - 
                        Segment oSourceSeg = this.TaskPane.ForteDocument
                            .GetTopLevelSegmentFromSelection();

                        PrefillChooser oForm = new PrefillChooser(oSeg,
                            PrefillChooser.PrefillSources.SelectedSegment);

                        if (oForm.ShowDialog() == DialogResult.OK)
                        {
                            System.Windows.Forms.Application.DoEvents();

                            Segment.InsertionLocations iLocation = (Segment.InsertionLocations)oForm.InsertionLocation;
                            Segment.InsertionBehaviors iOptions = (Segment.InsertionBehaviors)oForm.InsertionBehavior;

                            //create prefill
                            Prefill oPrefill = null;

                            if (oSourceSeg != null)
                                oPrefill = new Prefill(oSourceSeg);

                            this.TaskPane.ScreenRefresh();

                            //insert segment, passing Prefill object
                            InsertSegmentFromNode(oNode, iLocation, iOptions, oPrefill, true);

                            this.TaskPane.DataReviewerVisible = this.TaskPane.ForteDocument.ContainsFinishedSegments;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handle Double-click or Enter press on node
        /// </summary>
        /// <param name="oNode"></param>
        private void PerformDoubleClickActionForNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return;

            ContentTreeHelper.LoadNodeReference(oNode);

            if (IsExternalContentNode(oNode))
            {
                CreateExternalContent(oNode);
            }
            else if (ContentTreeHelper.IsMasterData(oNode))
            {
                InsertMultiple(null, oNode);
            }
            else
            {
                if (ContentTreeHelper.IsUserSegmentNode(oNode) || ContentTreeHelper.IsAdminSegmentNode(oNode) || ContentTreeHelper.IsVariableSetNode(oNode))
                    InsertSegmentFromNodeDoubleClick(oNode);
            }
        }

        /// <summary>
        /// inserts the segment with the specified id with
        /// the specified options
        /// </summary>
        /// <param name="oVariableSetDef"></param>
        public void InsertSegment(VariableSetDef oVariableSetDef, bool bInsertSelectionAsBody)
        {
            //GLOG 3475
            Prefill oPrefill = new Prefill(oVariableSetDef.ContentString, oVariableSetDef.Name, oVariableSetDef.SegmentID);
            string xID = oVariableSetDef.SegmentID;

            //Prefill can use either Admin or User Segment
            if (!xID.Contains(".") || xID.EndsWith(".0"))
            {
                //Admin Segment
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                int iID1 = 0;
                int iID2 = 0;
                LMP.String.SplitID(xID, out iID1, out iID2);
                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);

                InsertSegment(oDef, (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation, 
                    (Segment.InsertionBehaviors)oDef.DefaultDoubleClickBehavior, 
                    oPrefill, bInsertSelectionAsBody);
            }
            else
            {
                //user segment
                UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User,
                    LMP.MacPac.Session.CurrentUser.ID);

                UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xID);

                if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocument)
                {
                    InsertSegment(oDef, Segment.InsertionLocations.InsertInNewDocument,
                        Segment.InsertionBehaviors.Default, oPrefill, bInsertSelectionAsBody);
                }
                else if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent ||
                    oDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText ||
                    oDef.IntendedUse == mpSegmentIntendedUses.AsSentenceText)
                {
                    InsertSegment(oDef, Segment.InsertionLocations.InsertAtSelection,
                        Segment.InsertionBehaviors.Default, oPrefill, bInsertSelectionAsBody);
                }
            }
        }
        public void InsertSegment(AdminSegmentDef oDef, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iOptions, Prefill oPrefill, bool bInsertSelectionAsBody)
        {
            LMP.Trace.WriteNameValuePairs("oDef.ID", oDef.ID);
            string xID = oDef.ID.ToString();

            try
            {
                string xSelectedText = "";
                if (bInsertSelectionAsBody)
                    xSelectedText = this.m_oMPDocument.WordDocument.Application.Selection.Text;

                if (oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                {
                    InsertAnswerFile(xID, oPrefill, xSelectedText, false);
                }
                else
                {
                    if (iLocation == Segment.InsertionLocations.InsertInNewDocument)
                        TaskPane.InsertSegmentInNewDoc(xID, oPrefill, xSelectedText);
                    else
                        this.TaskPane.InsertSegmentInCurrentDoc(xID, oPrefill, xSelectedText,
                            iLocation, iOptions, oDef);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Insertion") +
                    " " + xID, oE);
            }
        }
        public void InsertSegment(UserSegmentDef oDef, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iOptions, Prefill oPrefill, bool bInsertSelectionAsBody)
        {
            LMP.Trace.WriteNameValuePairs("oDef.ID", oDef.ID);
            string xID = oDef.ID;

            try
            {
                // GLOG : 2475 : JAB
                // Apply the style to this document.
                if (oDef.IntendedUse == mpSegmentIntendedUses.AsStyleSheet)
                {
                    this.m_oMPDocument.UpdateStylesFromXML(oDef.XML);
                }
                else
                {
                    string xSelectedText = "";
                    if (bInsertSelectionAsBody)
                        xSelectedText = this.m_oMPDocument.WordDocument.Application.Selection.Text;

                    //segment is a user segment - make location based on intended use
                    if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocument)
                        TaskPane.InsertSegmentInNewDoc(xID, oPrefill, xSelectedText);
                    else
                        this.TaskPane.InsertSegmentInCurrentDoc(xID, oPrefill, xSelectedText,
                            iLocation, iOptions, oDef);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Insertion") +
                    " " + xID, oE);
            }
        }
        /// <summary> 
        /// Inserts Segment with the options specified 
        /// </summary> 
        /// <param name="oNode"></param> 
        /// <param name="iInsertType"></param> 
        /// <param name="bSeparateSection"></param> 
        /// <param name="bKeepHeadersFooters"></param> 
        /// <param name="bRestartPageNumbering"></param> 
        private LMP.Architect.Base.Segment InsertSegmentFromNode(UltraTreeNode oNode, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iOptions)
        {
            return InsertSegmentFromNode(oNode, iLocation, iOptions, null, false);
        }
        private LMP.Architect.Base.Segment InsertSegmentFromNode(UltraTreeNode oNode, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iOptions, Prefill oPrefill)
        {
            return InsertSegmentFromNode(oNode, iLocation, iOptions, oPrefill, false);
        }

        private LMP.Architect.Base.Segment InsertSegmentFromNode(UltraTreeNode oNode, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iOptions, Prefill oPrefill, bool bInsertSelectionAsBody)
        {
            if (ContentTreeHelper.IsVariableSetNode(oNode) && GetVariableSetFromNode(oNode).SegmentID == "")
            {
                return InsertSegmentFromNodeDoubleClick(oNode);
            }
            else
            {
                return InsertSegmentFromNode(oNode, iLocation, iOptions, oPrefill, bInsertSelectionAsBody, "", false);
            }
        }
        private LMP.Architect.Base.Segment InsertSegmentFromNode(UltraTreeNode oNode, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iOptions, Prefill oPrefill, bool bInsertSelectionAsBody, string xSegmentIDOverride) //GLOG 7128
        {
            return InsertSegmentFromNode(oNode, iLocation, iOptions, oPrefill, bInsertSelectionAsBody, xSegmentIDOverride, false);
        }

        private LMP.Architect.Base.Segment InsertSegmentFromNode(UltraTreeNode oNode, Segment.InsertionLocations iLocation,
            Segment.InsertionBehaviors iOptions, Prefill oPrefill, bool bInsertSelectionAsBody, string xSegmentIDOverride, bool bClientMatterInsertion) //GLOG 7128
        {
            LMP.Trace.WriteNameValuePairs("iLocation", iLocation, "iOptions", iOptions);
            string xID = ContentTreeHelper.GetSegmentIDFromNode(oNode);
            bool bIsVariableSetWithoutSegmentID = false;
            if (iLocation != Architect.Base.Segment.InsertionLocations.InsertInNewDocument &&
                LMP.Architect.Api.Environment.DocumentIsProtected(this.m_oMPDocument.WordDocument, true))
            {
                return null;
            }
            //LMP.Forte.MSWord.Undo oUndo = new LMP.Forte.MSWord.Undo();
            ////oUndo.MarkStart("Dan");

            if (string.IsNullOrEmpty(xID))
            {
                // Only VariableSet's can have no segment id.
                if (!ContentTreeHelper.IsVariableSetNode(oNode))
                {
                    return null;
                }
                else
                {
                    bIsVariableSetWithoutSegmentID = true;
                }
            }

            bool bUseAnswerFile = false;
            if (ContentTreeHelper.IsVariableSetNode(oNode))
            {
                VariableSetDef oVarSet = GetVariableSetFromNode(oNode);
                //GLOG 3475
                oPrefill = new Prefill(oVarSet.ContentString, oVarSet.Name, oVarSet.SegmentID, oVarSet.ID);

                if (bIsVariableSetWithoutSegmentID && !string.IsNullOrEmpty(xSegmentIDOverride))
                {
                    if (!xSegmentIDOverride.Contains(".") || xSegmentIDOverride.EndsWith(".0"))
                    {
                        //Admin Segment
                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        int iID1 = 0;
                        int iID2 = 0;
                        LMP.String.SplitID(xSegmentIDOverride, out iID1, out iID2);
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iID1);

                        if (oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                        {
                            bUseAnswerFile = true;
                        }
                    }
                    else
                    {
                        //User Segment
                        UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);
                        UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xSegmentIDOverride);

                        if (oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                        {
                            bUseAnswerFile = true;
                        }
                    }

                    xID = xSegmentIDOverride;
                }
                else
                {
                    if (ContentTreeHelper.GetSegmentDefFromNode(oNode).IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
                        bUseAnswerFile = true;
                }
            }
            else if (ContentTreeHelper.IsAnswerFileNode(oNode))
            {
                bUseAnswerFile = true;
            }

            if (bUseAnswerFile && this.TaskPane.Mode == ForteDocument.Modes.Design)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_CantInsertAnswerFileInDesignMode"),
                    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }

            //get segment def
            ISegmentDef oSegDef = ContentTreeHelper.GetSegmentDefFromNode(oNode);

            if (ContentTreeHelper.IsAdminSegmentNode(oNode) && this.TaskPane.Mode != ForteDocument.Modes.Design)
            {
                //Don't allow inserting collection out of design mode
                //TODO: this branch is a temporary measure until 
                //Collections functionality is finalized
                mpObjectTypes iTypeID = oSegDef.TypeID;
                if (iTypeID == mpObjectTypes.PleadingCaptions || 
                    iTypeID == mpObjectTypes.PleadingCounsels ||
                    iTypeID == mpObjectTypes.PleadingSignatures ||
                    iTypeID == mpObjectTypes.CollectionTable ||
                    iTypeID == mpObjectTypes.LetterSignatures ||
                    iTypeID == mpObjectTypes.AgreementSignatures)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_SegmentMustBeInsertedInDesignMode"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;
                }
                else if (iTypeID == mpObjectTypes.Letterhead ||
                    iTypeID == mpObjectTypes.PleadingPaper)
                {
                    if (this.m_oMPDocument.WordDocument.Application.Selection.Sections[1].Index > 1 &&
                        (iLocation == Segment.InsertionLocations.InsertAtSelection || 
                        iLocation == Segment.InsertionLocations.Default ||
                        iLocation == Segment.InsertionLocations.InsertAtEndOfCurrentSection || 
                        iLocation == Segment.InsertionLocations.InsertAtStartOfCurrentSection))
                    {
                        //GLOG 3133: If Paper is being inserted in single section,
                        //make sure headers and footers are not linked
                        LMP.Forte.MSWord.WordDoc.UnlinkHeadersFooters(this.m_oMPDocument.WordDocument.Application.Selection.Sections[1]);
                    }
                }

                //GLOG 3159: Only prompt if Saved/Document Data has Court Levels defined
                //GLOG 7128: Don't prompt if inserting from Client/Matter lookup
                if (!bClientMatterInsertion && (oPrefill != null && (oSegDef.L0 + oSegDef.L1 + oSegDef.L2 + oSegDef.L3 + oSegDef.L4 > 0) &&
                        (oPrefill["L0"] + oPrefill["L1"] + oPrefill["L2"] + oPrefill["L3"] + oPrefill["L4"] != "")))
                {
                    //GLOG 2914: Prompt before overwriting Court levels from Prefill
                    try
                    {
                        if ((oPrefill["L0"] != oSegDef.L0.ToString() && oSegDef.L0 > 0) ||
                            (oPrefill["L1"] != oSegDef.L1.ToString() && oSegDef.L1 > 0) ||
                            (oPrefill["L2"] != oSegDef.L2.ToString() && oSegDef.L2 > 0) ||
                            (oPrefill["L3"] != oSegDef.L3.ToString() && oSegDef.L3 > 0) ||
                            (oPrefill["L4"] != oSegDef.L4.ToString() && oSegDef.L4 > 0))
                        {
                            string xTargetCourt = LMP.Data.Application.GetCourtText(oSegDef.L0.ToString(), oSegDef.L1.ToString(),
                                oSegDef.L2.ToString(), oSegDef.L3.ToString(), oSegDef.L4.ToString());
                            string xSavedCourt = LMP.Data.Application.GetCourtText(oPrefill["L0"], oPrefill["L1"], oPrefill["L2"],
                                oPrefill["L3"], oPrefill["L4"]);
							//GLOG : 3159 : CEH
                            DialogResult iRet = MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_UseCourtLevelsFromSavedData"), 
                                xTargetCourt, xSavedCourt, LMP.Resources.GetLangString("Prompt_SavedData")),  LMP.ComponentProperties.ProductName, 
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                            if (iRet != DialogResult.Yes)
                            {
                                //Remove Court Level values from Prefill so they won't be applied
                                oPrefill.Delete("L0");
                                oPrefill.Delete("L1");
                                oPrefill.Delete("L2");
                                oPrefill.Delete("L3");
                                oPrefill.Delete("L4");
                                oPrefill.Delete("CourtTitle");
                            }
                        }
                    }
                    catch { }
                }
            }
            try
            {
                string xSelectedText = "";

                if (bInsertSelectionAsBody)
                {
                    xSelectedText = this.m_oMPDocument.WordDocument.Application.Selection.Text;
                    if (xSelectedText == null)
                        //GLOG 4979 (dm) - this is the case when the cursor is
                        //immediately before a block content control
                        xSelectedText = "";
                    else
                    {
                        //GLOG 3250: Trim End-of-Cell characters from string
                        if (xSelectedText.EndsWith("\r\a"))
                            xSelectedText = xSelectedText.Substring(0, xSelectedText.Length - 2);
                        if (xSelectedText.Length < 2)
                            xSelectedText = "";
                    }
                }

                if (bUseAnswerFile)
                {
                    InsertAnswerFile(xID, oPrefill, xSelectedText, false);
                    return null;
                }
                else if (iLocation == Segment.InsertionLocations.InsertInNewDocument)
                {
                    //GLOG 7143 (dm) - don't allow in Word 2013 if save format is .doc
                    if ((LMP.Forte.MSWord.WordApp.Version > 14) && (Session.CurrentWordApp.DefaultSaveFormat.ToUpper() == "DOC"))
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_NewDocAutomationNotSupportedInWord2013"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return null;
                    }

                    return TaskPane.InsertSegmentInNewDoc(xID, oPrefill, xSelectedText);
                }
                else
                {
                    return this.TaskPane.InsertSegmentInCurrentDoc(xID, oPrefill, xSelectedText,
                         iLocation, iOptions, oSegDef);
                }
            }
            catch (LMP.Exceptions.SegmentInsertionException oE)
            {
                MessageBox.Show(oE.Message, LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Insertion") +
                    " " + oNode.Text, oE);
            }

            return null;
        }
        /// <summary>
        /// Display Data Interview Dialog to edit existing VariableSet values
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="oVarSet"></param>
        internal void InsertAnswerFile(string xSegmentID, VariableSetDef oVarSet)
        {
            InsertAnswerFile(xSegmentID, null, null, false, oVarSet);
        }
        internal void InsertAnswerFile(string xSegmentID, Prefill oPrefill, string xSelectedText, bool bPreviewOnly)
        {
            InsertAnswerFile(xSegmentID, oPrefill, xSelectedText, bPreviewOnly, null);
        }
        /// <summary>
        /// Insert Answer File segments, displaying dialog to gather values if necessary
        /// </summary>
        /// <param name="xSegmentID"></param>
        /// <param name="oPrefill"></param>
        /// <param name="xSelectedText"></param>
        /// <param name="bPreviewOnly"></param>
        /// <param name="oVarSet"></param>
        private void InsertAnswerFile(string xSegmentID, Prefill oPrefill, string xSelectedText, bool bPreviewOnly, VariableSetDef oVarSet)
        {
            if (oPrefill == null)
            {
                ////we need to insert in a new document - that document's 
                ////ForteDocument object will need to insert the segment, as 
                ////the current doc's ForteDocument points to the current doc
                //create a new document
                TaskPane.ScreenUpdating = false;
                Word.Document oNewDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                    LMP.Data.Application.BaseTemplate, false, false);
                System.Windows.Forms.Application.DoEvents();
                oNewDoc.Activate();
                ForteDocument oMPDoc = new ForteDocument(oNewDoc);

                //insert the specified segment into the document
                Segment oSegment = oMPDoc.InsertSegment(xSegmentID, null,
                    Segment.InsertionLocations.InsertInNewDocument,
                    Segment.InsertionBehaviors.Default, true, null,
                    "", false, false, "", false, false, null);
                TaskPane.ScreenUpdating = true;
                //Prompt for prefill values
                SegmentDialog oDlg = new SegmentDialog((AdminSegment)oSegment, oVarSet);
                DialogResult iRet = oDlg.ShowDialog();
                if (iRet == DialogResult.OK && !bPreviewOnly)
                {
                    oPrefill = oDlg.NewPrefill();
                }
                // We're done with document opened to display variables
                object oFalse = (object)false;
                object oMissing = (object)System.Reflection.Missing.Value;
                oNewDoc.Close(ref oFalse, ref oMissing, ref oMissing);
            }
            if (oPrefill != null)
            {
                this.InsertAnswerFileSegments(xSegmentID, oPrefill, xSelectedText);
            }
        }
        private void InsertAnswerFileSegments(string xID, Prefill oPrefill, string xSelectedText)
        {
            DateTime t0 = DateTime.Now;
            ForteDocument oMPDocument = this.TaskPane.ForteDocument;
            TaskPane oTP = this.TaskPane;
            Segment oSegment = null;
            Word.Document oFirstDoc = null;

            bool bScreenUpdating = this.TaskPane.ScreenUpdating;

            LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api
                .Environment(this.m_oMPDocument.WordDocument);
            oEnv.SaveState();
            oEnv.SetExecutionState(mpTriState.True, mpTriState.Undefined,
                mpTriState.Undefined, mpTriState.Undefined, mpTriState.True, true);

            try
            {
                //GLOG 6966: Make sure string can be parsed as a Double even if
                //the Decimal separator is something other than '.' in Regional Settings
                if (xID.EndsWith(".0"))
                    xID = xID.Replace(".0", "");
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID((int)(Double.Parse(xID)));
                string xSegList = oDef.ChildSegmentIDs;
                AnswerFileSegments oSegs = new AnswerFileSegments(xSegList);
                ProgressList oProgress = new ProgressList();
                List<string> aNames = new List<string>();
                List<string> aSegmentIDs = new List<string>();
                for (int i = 0; i < oSegs.Count; i++)
                {
                    AdminSegmentDef oSegDef = null;
                    //GLOG 6966: Make sure string can be parsed as a Double even if
                    //the Decimal separator is something other than '.' in Regional Settings
                    string xSegID = oSegs.Item(i).SegmentID;
                    if (xSegID.EndsWith(".0"))
                        xSegID = xSegID.Replace(".0", "");
                    try
                    {
                        oSegDef = (AdminSegmentDef)oDefs.ItemFromID(
                            (int)Double.Parse(xSegID));
                    }
                    catch { }
                    if (oSegDef != null)
                    {
                        aNames.Add(oSegDef.DisplayName);
                        aSegmentIDs.Add(oSegDef.ID.ToString() + ".0");
                    }
                }
                oProgress.Setup(oPrefill.Name,
                    LMP.Resources.GetLangString("Dialog_InsertingDataPacketSegments")
                        .Replace("{0}", oPrefill.Name), null, aNames, 3, this.TaskPane, false);

                //Make sure no other processes can reenable screenupdating
                TaskPane.SkipScreenUpdates = true;
                oTP.ScreenUpdating = false;
                bool bReusedDocument = false;
                try
                {
                    for (int i = 0; i < oSegs.Count; i++)
                    {
                        //Skip any assigned segments that no longer exist
                        if (aSegmentIDs.Contains(oSegs.Item(i).SegmentID))
                        {
                            oProgress.UpdateProgress();
                            AnswerFileSegment oSeg = oSegs.Item(i);
                            if (oSeg.InsertionLocation == Segment.InsertionLocations.InsertInNewDocument)
                            {
                                if (bReusedDocument)
                                {
                                    //Last segment was inserted in existing doc
                                    //Refresh tree only after all segments have been inserted
                                    oTP.SelectTab(TaskPane.Tabs.Edit);
                                    System.Windows.Forms.Application.DoEvents();
                                    oTP.DocEditor.RefreshTree(true);
                                }
                                //create a new document
                                oSegment = (LMP.Architect.Api.Segment) TaskPane.InsertSegmentInNewDoc(oSeg.SegmentID, oPrefill,
                                    xSelectedText);
                                System.Windows.Forms.Application.DoEvents();
                                oProgress.UpdateProgress();
                                oTP = TaskPanes.Item(Session.CurrentWordApp.ActiveDocument);
                                oMPDocument = oTP.ForteDocument;
                                bReusedDocument = false;
                            }
                            else
                            {
                                try
                                {
                                    oSegment = oTP.InsertSegmentInCurrentDoc(oSeg.SegmentID, oPrefill,
                                        xSelectedText, oSeg.InsertionLocation,
                                        oSeg.InsertionBehavior);
                                    oProgress.UpdateProgress();
                                    //Let other ContentManager finish its work
                                    System.Windows.Forms.Application.DoEvents();
                                    bReusedDocument = true;
                                }
                                finally
                                {
                                    LMP.MacPac.Application.SetXMLTagStateForEditing();
                                }
                            }
                            if (oFirstDoc == null)
                                oFirstDoc = oMPDocument.WordDocument;
                            oProgress.UpdateProgress(ProgressList.StatusTypes.Succeeded);
                        }
                    }
                    if (bReusedDocument)
                    {
                        //If more than one segment inserted in one document, refresh the tree
                        oTP.SelectTab(TaskPane.Tabs.Edit);
                        System.Windows.Forms.Application.DoEvents();
                        oTP.DocEditor.RefreshTree(true);
                    }
                }
                catch
                {
                    oProgress.UpdateProgress(ProgressList.StatusTypes.Failed);
                }
                finally
                {
                    oProgress.Hide();
                }
            }
            finally
            {
                oEnv.RestoreState();
                TaskPane.SkipScreenUpdates = false;
                if (bScreenUpdating)
                {
                    oTP.ScreenUpdating = true;
                    oTP.ScreenUpdating = false;
                    oTP.ScreenUpdating = true;
                }
                //Display first Answer File document
                if (oFirstDoc != null)
                    oFirstDoc.Activate();
                LMP.MacPac.Application.SetXMLTagStateForEditing();

                LMP.Benchmarks.Print(t0);
            }
        }

        /// <summary>
        /// Setup blank document and set PendingAction to InsertForDesign
        /// </summary>
        /// <param name="oNode"></param>
        private void DesignSegmentFromNode(UltraTreeNode oNode)
        {
            DesignSegmentFromNode(oNode, true);
        }
        /// <summary>
        /// Set PendingAction to InsertForDesign, optionally creating new blank document
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bCreateDoc"></param>
        private void DesignSegmentFromNode(UltraTreeNode oNode, bool bCreateDoc)
        {
            try
            {
                string xID = "";
                bool bIsSegmentPacket = false;

                ContentTreeHelper.LoadNodeReference(oNode);
                if (oNode.Tag is FolderMember)
                {
                    FolderMember oMember = (FolderMember)oNode.Tag;
                    if (oMember.FolderType == mpFolderTypes.Admin)
                    {
                        xID = oMember.ObjectID1.ToString();

                        AdminSegmentDefs oDefs = new AdminSegmentDefs();
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(int.Parse(xID));

                        bIsSegmentPacket = oDef.TypeID == mpObjectTypes.SegmentPacket;
                    }
                    else
                    {
                        xID = oMember.ObjectID1.ToString() + "." + oMember.ObjectID2.ToString();
                        bIsSegmentPacket = oMember.ObjectTypeID == mpFolderMemberTypes.UserSegmentPacket;
                    }
                }
                else if (oNode.Tag is AdminSegmentDef)
                {
                    xID = ((AdminSegmentDef)oNode.Tag).ID.ToString();
                }
                else if (oNode.Tag is UserSegmentDef)
                {
                    xID = ((UserSegmentDef)oNode.Tag).ID;
                }
                else
                    return;

                //GLOG 6966: Make sure string can be parsed as a Double even if
                //the Decimal separator is something other than '.' in Regional Settings
                if (xID.EndsWith(".0"))
                    xID = xID.Replace(".0", "");

                //GLOG - 3540 - CEH
                try
                {
                    //get all the current task panes
                    List<LMP.MacPac.TaskPane> oTP = LMP.MacPac.Application.CurrentTaskPanes();

                    //for each task pane, check to see if segment is already opened in design
                    for (int i = 0; i < oTP.Count; i++)
                        if ((oTP[i].Mode == ForteDocument.Modes.Design) && 
                            (oTP[i].ForteDocument.Segments.Count > 0))
                        {
                            //GLOG 6966: Make sure string can be parsed as a Double even if
                            //the Decimal separator is something other than '.' in Regional Settings
                            if (xID.EndsWith(".0"))
                                xID = xID.Replace(".0", "");
                            string xSegmentID = oTP[i].ForteDocument.Segments[0].ID;
                            if (xSegmentID.EndsWith(".0"))
                                xSegmentID = xSegmentID.Replace(".0", "");

                            //GLOG 6966: Don't use Double.Parse here, as UserSegment ID 
                            //won't be parseable if decimal separator in Regional settings is not '.'
                            if (xSegmentID == xID)
	                        {
                                //activate the open window
                                oTP[i].ForteDocument.WordDocument.Activate();
                                return;
	                        }
                        }
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }

                if (bIsSegmentPacket)
                {
                    EditSegmentPacket(xID);
                }
                else
                {
                    TaskPane.PendingAction = new PendingAction(
                        PendingAction.Types.InsertForDesign, xID, null);

#if Oxml
                    if (LMP.MacPac.Application.IsOxmlSupported())
                    {
                        TaskPane.InsertSegmentInNewDoc(xID, null, null, null, true);
                    }
                    else
#endif
                    {

                        if (bCreateDoc)
                        {
                            //GLOG 4971 (dm) - use dedicated designer template if it exists
                            string xTemplate = "";
	                        //GLOG 8445:  Name has changed
	                        string xDesignerTemplate = Data.Application.TemplatesDirectory +
	                            @"\ForteNormalDesigner.dotx";
                            if (File.Exists(xDesignerTemplate))
                                xTemplate = xDesignerTemplate;
                            else
                                xTemplate = LMP.Data.Application.BaseTemplate;

                            //create a new blank document
                            Word.Document oNewDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                                xTemplate, true, true);
                            System.Windows.Forms.Application.DoEvents();
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Design") + " " + oNode.Text, oE);
            }
        }
        private void EditSegmentPacket(string xSegmentID)
        {
            if (!xSegmentID.Contains(".") || xSegmentID.Contains(".0"))
            {
                //this is an admin segment
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(int.Parse(xSegmentID));

                MultipleSegmentsForm oForm = new MultipleSegmentsForm("Edit Segment Packet", "Sa&ve", oDef.XML, false, null, true);

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    oDef.XML = oForm.Value;
                    oDefs.Save(oDef);
                }
            }
            else
            {
                //this is a user segment
                UserSegmentDefs oDefs = new UserSegmentDefs();
                UserSegmentDef oDef = (UserSegmentDef)oDefs.ItemFromID(xSegmentID);

                MultipleSegmentsForm oForm = new MultipleSegmentsForm("Edit Segment Packet", "Sa&ve", oDef.XML, false, null, false);

                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    oDef.XML = oForm.Value;
                    oDefs.Save(oDef);
                }
            }
        }
        /// <summary>
        /// Creates new AdminSegment or UserSegment of the selected type
        /// (blank, based on selection, or based on entire document)
        /// and adds as a FolderMember to the selected Node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="iType"></param>
        private void CreateNewSegment(UltraTreeNode oNode, NewSegmentItems iType)
        {
            CreateNewSegment(oNode, iType, false); //GLOG 8376
        }
        //GLOG 8376
        private void CreateNewSegment(UltraTreeNode oNode, NewSegmentItems iType, bool bAsDesigner)
        {
            try
            {
                FolderMember oMember = null;
                UltraTreeNode oNewNode = null;
                Segment oCurSeg = null;
                string xRelatedSegmentIDs = "";


                //GLOG 8118: Only count Document Segments toward total
                int iNumSegments = 0;

                for (int i = 0; i < this.TaskPane.ForteDocument.Segments.Count; i++)
                {
                    if (this.TaskPane.ForteDocument.Segments[i].IntendedUse == mpSegmentIntendedUses.AsDocument)
                        iNumSegments++;
                }

                //GLOG 8118
                if (iType == NewSegmentItems.SegmentFromDocument && iNumSegments > 1) // && ContentTreeHelper.IsAdminFolderNode(oNode))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_NewSegmentFromMultipleSegmentsNotAllowed"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if ((iType == NewSegmentItems.SegmentFromDocument && iNumSegments == 1) ||
                       iType == NewSegmentItems.SegmentFromSelection)
                {
                    if (iType == NewSegmentItems.SegmentFromSelection)
                    {
                        //GLOG 8118
                        iNumSegments = 0;
                        string xTagsInSelection = null;
                        Word.Selection oSel = Session.CurrentWordApp.Selection;
                        if (this.TaskPane.ForteDocument.FileFormat ==
                            LMP.Data.mpFileFormats.Binary)
                        {
                            //xml tags
                            Word.XMLNode oFirstNode = null;
                            try
                            {
                                oFirstNode = oSel.XMLNodes[1];
                            }
                            catch { }
                            // Test if selection start and end correspond to existing mSeg tag
                            if (oFirstNode != null && oFirstNode.BaseName == "mSEG" &&
                                oFirstNode.Range.Start == oSel.Range.Start + 1 &&
                                oFirstNode.Range.End >= oSel.Range.End - 2)
                            {
                                xTagsInSelection = LMP.Forte.MSWord.WordDoc.GetTagsInSelection("mSEG");
                            }
                        }
                        else
                        {
                            //content controls
                            Word.ContentControl oFirstCC = null;
                            object iIndex = 1;
                            try
                            {
                                oFirstCC = oSel.ContentControls.get_Item(ref iIndex);
                            }
                            catch { }
                            // Test if selection start and end correspond to existing mSeg tag
                            if (oFirstCC != null &&
                                LMP.String.GetBoundingObjectBaseName(oFirstCC.Tag) == "mSEG" &&
                                oFirstCC.Range.Start == oSel.Range.Start + 1 &&
                                oFirstCC.Range.End >= oSel.Range.End - 2)
                            {
                                xTagsInSelection = LMP.Forte.MSWord.WordDoc.GetContentControlsInSelection("mSEG");
                            }
                        }

                        //conditional added 6/16/10 (dm) to fix error
                        //when only the start of an existing segment is selected
                        if (xTagsInSelection != null)
                        {
                            //existing segment is selected - use its definition
                            string[] xTags = xTagsInSelection.Split('|');
                            string[] xFields = xTags[1].Split('�');
                            string xFirstTagInScope = xFields[0];
                            oCurSeg = this.TaskPane.ForteDocument.FindSegment(xFirstTagInScope);
                        }
                    }
                    else
                    {
                        oCurSeg = this.TaskPane.ForteDocument.Segments[0];
                    }

                    if (iNumSegments == 1)
                    {
                        // If selection contains exactly one existing segment, prompt to create a copy
                        // Yes will create new segment as copy, No will create new segment containing the existing segment
                        // If Creating a UserSegment from an AdminSegment, new segment must contain the original segment
                        string xMsg = "";
                        MessageBoxButtons iButtons;
                        MessageBoxIcon iIcon;
                        DialogResult iRet = DialogResult.OK;

                        //Can't copy segment def unless source and target are both User or Admin Segments
                        if (ContentTreeHelper.IsUserFolderNode(oNode) && this.TaskPane.ForteDocument.Segments[0].TypeID != mpObjectTypes.UserSegment)
                        {
                            //iButtons = MessageBoxButtons.OKCancel;
                            //xMsg = LMP.Resources.GetLangString("Msg_CreateUserSegmentContainingSegment");
                            //iIcon = MessageBoxIcon.Information;
                        }
                        else
                        {
                            iButtons = MessageBoxButtons.YesNoCancel;
                            xMsg = LMP.Resources.GetLangString("Msg_CreateCopyOfExistingSegment");
                            iIcon = MessageBoxIcon.Question;
                            iRet = MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                                iButtons, iIcon);
                            if (iRet == DialogResult.Cancel)
                                return;
                        }
                        if (iRet != DialogResult.Yes)
                        {
                            // Don't copy existing Segment definition
                            if (oCurSeg is AdminSegment && oCurSeg.HasDefinition)
                            {
                                //New segment will contain existing segment
                                //Copy ChildSegmentIDs
                                xRelatedSegmentIDs = ((AdminSegmentDef)(oCurSeg.Definition)).ChildSegmentIDs;
                                //Include main segment ID also
                                xRelatedSegmentIDs = oCurSeg.ID + (xRelatedSegmentIDs != "" ? "|" : "") + xRelatedSegmentIDs;
                            }
                            oCurSeg = null;
                        }
                    }
                }
                ContentTreeHelper.LoadNodeReference(oNode);
                ContentPropertyForm oForm = new ContentPropertyForm();

                //get mode to display dialog - either for a DocumentSegment
                //or a TextBlockSegment
                ContentPropertyForm.DialogMode oMode = 0;
                //GLOG : 8305 : ceh
                //GLOG 8329
                bool bTextIsSelected = false;

                switch (iType)
                {
                    case NewSegmentItems.SegmentBlank:
                        //GLOG 8376: always default to Document segment
                        oMode = ContentPropertyForm.DialogMode.NewSegment;
                        break;
                    case NewSegmentItems.SegmentFromDocument:
                        oMode = ContentPropertyForm.DialogMode.NewSegment;
                        //GLOG 8329
                        bTextIsSelected = (Session.CurrentWordApp.Selection.Type > Word.WdSelectionType.wdSelectionIP);
                        break;
                    case NewSegmentItems.SegmentFromSelection:
                        //GLOG : 8305 : ceh - cannot rely on DialogMode.NewParagraphTextSegment
                        //because NewSegmentItems.SegmentBlank also sets oMode to the same type (see above case)

                        //GLOG : 15887 : ceh
                        if (Session.CurrentWordApp.Selection.Type == WdSelectionType.wdSelectionInlineShape || 
                            Session.CurrentWordApp.Selection.Type == WdSelectionType.wdSelectionShape)
	                    {
                            //user only selected a Shape, prompt for paragraph selection
                            MessageBox.Show(LMP.Resources.GetLangString("Prompt_IncludeParagraphInSelectionForNewSegment"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }

                        bTextIsSelected = true;

                        //GLOG 2543 (dm) - if only part of paragraph is selected,
                        //set intended use to as "as sentence text"
                        if (Session.CurrentWordApp.Selection.Text.Contains("\r"))
                            oMode = ContentPropertyForm.DialogMode.NewParagraphTextSegment;
                        else
                            oMode = ContentPropertyForm.DialogMode.NewSentenceTextSegment;
                        break;
                    case NewSegmentItems.StyleSheetFromDocument:
                        oMode = ContentPropertyForm.DialogMode.NewStyleSheet;
                        //GLOG 8329
                        bTextIsSelected = (Session.CurrentWordApp.Selection.Type > Word.WdSelectionType.wdSelectionIP);
                        break;
                    case NewSegmentItems.AnswerFile:
                        oMode = ContentPropertyForm.DialogMode.NewAnswerFile;
                        break;
                    case NewSegmentItems.MasterDataForm:
                        oMode = ContentPropertyForm.DialogMode.NewMasterDataForm;
                        break;
                    case NewSegmentItems.SegmentPacket:
                        oMode = ContentPropertyForm.DialogMode.PromptForSegmentInfoOnly;
                        break;
                }

                // GLOG : 3138 : JAB
                // Check the limits and threshold for user content.
                //GLOG 8376: Don't apply limits to Segment Designer content
                if (ContentTreeHelper.IsUserFolderNode(oNode) && !bAsDesigner)
                {
                    User oUser = Session.CurrentUser;
                    int iOwnedSegments = oUser.NumberOfOwnedSegments;
                    int iLimit = oUser.OwnedSegmentLimit;

                    // GLOG : 3138 : JAB
                    // Check if the user has exceeded the maximum number of segments allowed.
                    if ((iOwnedSegments >= iLimit))
                    {
                        // The user has exceeded the maximum allowed segments. Notify the user
                        // and do not create new content.
                        MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_UserContentLimitReached"), 
                            iLimit), LMP.ComponentProperties.ProductName, 
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    // GLOG : 3138 : JAB
                    // Check if the number of user content requires warning the user that
                    // they are approaching the maximum user content limit.
                    if (iOwnedSegments >= oUser.OwnedSegmentThreshold)
                    {
                        MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_ApproachingUserContentLimit"), 
                            iOwnedSegments, iLimit), LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }

                //GLOG : 8305 : ceh - pass SegmentFromSelection argument
                // Display Segment Creator form
                oForm.ShowForm(this.ParentForm, oNode, Form.MousePosition, oMode, null, null, true, "", bTextIsSelected, bAsDesigner); //GLOG 8376

                if (!oForm.Canceled)
                {
                    LMP.Architect.Api.Environment oEnv = new LMP.Architect.Api.Environment(
                        Session.CurrentWordApp.ActiveDocument);

                    oEnv.SaveState();

                    // Ensure XML tags are displayed
                    oEnv.SetExecutionState(mpTriState.Undefined,
                        mpTriState.Undefined, mpTriState.Undefined,
                           mpTriState.Undefined, mpTriState.True, false);

                    //2-2-11 (dm) - add bookmarks before getting segment xml
                    if (this.TaskPane.ForteDocument.FileFormat == mpFileFormats.Binary)
                        this.TaskPane.ForteDocument.Tags.AddBookmarks();

                    //GLOG 7653 - add segment bounding objects and decrypt before getting xml -
                    //everything will be restored automatically when we refresh at the end of this method
                    Word.Document oDoc = Session.CurrentWordApp.ActiveDocument;
                    LMP.Forte.MSWord.Conversion oConvert = new LMP.Forte.MSWord.Conversion();
                    if (iNumSegments == 1)
                    {
                        if (this.TaskPane.ForteDocument.FileFormat == mpFileFormats.Binary)
                        {
                            oConvert.AddTagsToBookmarkedDocument(oDoc, false, true, false, true);
                            LMP.Forte.MSWord.WordDoc.SetDocumentCompression(this.TaskPane.ForteDocument.Tags, false, "");
                        }
                        else
                        {
                            oConvert.AddContentControlsToBookmarkedDocument(oDoc, false, true, false, true);
                            LMP.Forte.MSWord.WordDoc.SetDocumentCompression_CC(this.TaskPane.ForteDocument.Tags, false, "");
                        }
                    }
                        
                    if (ContentTreeHelper.IsAdminFolderNode(oNode))
                    {
                        Folder oFolder = (Folder)oNode.Tag;
                        AdminSegmentDef oAdminSeg = (AdminSegmentDef)oForm.NewSegmentDef;
                        AdminSegmentDefs oAdminSegs = new AdminSegmentDefs(oAdminSeg.TypeID);
                        if (oCurSeg != null)
                        {

                            AdminSegmentDef oCurDef = null;
                            if (oCurSeg.HasDefinition && oCurSeg.TypeID != mpObjectTypes.UserSegment)
                                oCurDef = (AdminSegmentDef)oCurSeg.Definition;
                            
                            oCurSeg.Select();

                            //If selected segment is not UserSegment, copy the entire 
                            //thing, including enclosing tags.  Otherwise just use the contents
                            Word.Selection oSel = Session.CurrentWordApp.Selection;
                            if (oCurSeg.TypeID != mpObjectTypes.UserSegment)
                            {
                                Object oOne = (object)1;
                                Object oMinusOne = (object)-1;

                                // Expand selection to include mSeg tags
                                oSel.MoveStart(ref oOne, ref oMinusOne);
                                oSel.MoveEnd(ref oOne, ref oOne);
                            }
                            string xXML = "";
                            int iSelLength = 0;
                            try
                            {
                                iSelLength = oSel.Text.Length;
                            }
                            catch 
                            { 
                                // An error will occur if Selection is zero-length
                            }

                            if (oCurSeg.TypeID == mpObjectTypes.UserSegment)
                            {
                                //If selection contains a User Segment, get XML from contents, without the mSEG tags
                                oAdminSeg.XML = GetWordXML(NewSegmentItems.SegmentFromSelection, oAdminSeg.ID.ToString(), oAdminSeg.TypeID,
                                        oAdminSeg.DisplayName, oAdminSeg.Name, oAdminSeg.HelpText, oAdminSeg.IntendedUse,
                                        oAdminSeg.DefaultDoubleClickLocation, oAdminSeg.IntendedUse < mpSegmentIntendedUses.AsParagraphText ? 1 : 0); //GLOG 8376
                            }
                            else
                            {
                                int iDocLength = oDoc.Content.Text.Length;

                                //GLOG 8347: Ensure XML does not contain rsids
                                bool bStore = Session.CurrentWordApp.Options.StoreRSIDOnSave;
                                Session.CurrentWordApp.Options.StoreRSIDOnSave = false;
                                try
                                {
                                    //if existing segment encloses entire document get document XML, else Selection xml
                                    if (this.TaskPane.ForteDocument.FileFormat == mpFileFormats.Binary)
                                    {
                                        //xml tags
                                        if (iDocLength == 1 || iSelLength >= iDocLength - 1)
                                            xXML = oDoc.Content.get_XML(false);
                                        else
                                            xXML = oSel.get_XML(false);

                                    // Make sure mpNewSegment bookmark exists for Segment XML
                                    if (xXML.IndexOf(m_xBookmarkTag) == -1)
                                    {
                                        int iPos = xXML.IndexOf("<" + LMP.String.GetNamespacePrefix(xXML,
                                            ForteConstants.MacPacNamespace) + ":mSEG");

                                        if (iPos > -1)
                                        {
                                            iPos = xXML.IndexOf(">", iPos + 1);
                                            if (iPos > -1)
                                                xXML = xXML.Insert(iPos + 1, m_xBookmarkTag);
                                        }
                                    }
                                }
                                else
                                {
                                    //content controls
                                    if (iDocLength == 1 || iSelLength >= iDocLength - 1)
                                        xXML = oDoc.Content.WordOpenXML;
                                    else
                                        xXML = oSel.WordOpenXML;

                                    // Make sure mpNewSegment bookmark exists for Segment XML
                                    if (xXML.IndexOf(Segment.NEW_SEGMENT_BMK_OPENXML) == -1)
                                    {
                                        int iPos = xXML.IndexOf("w:tag w:val=\"mps");
                                        if (iPos > -1)
                                        {
                                            iPos = xXML.IndexOf("<w:sdtContent>", iPos);
                                            if (iPos > -1)
                                                xXML = xXML.Insert(iPos + 14, Segment.NEW_SEGMENT_BMK_OPENXML);
                                        }
                                        }
                                    }
                                }
                                catch (System.Exception oE)
                                {
                                    throw oE;
                                }
                                finally
                                {
                                    //GLOG 8347: restore original rside state
                                    Session.CurrentWordApp.Options.StoreRSIDOnSave = bStore;
                                }
                                oAdminSeg.XML = String.ReplaceSegmentXML(xXML, oCurSeg.ID, oAdminSeg.ID.ToString(),
                                    oCurSeg.DisplayName, oAdminSeg.DisplayName, oCurSeg.FullTagID, oAdminSeg.Name,
                                    oCurSeg.TranslationID.ToString());
                            }
                            if (oCurDef != null)
                            {
                                //Copy Segment Definition options
                                AdminSegmentDef oOrigDef = (AdminSegmentDef)oCurDef;
                                oAdminSeg.TypeID = oOrigDef.TypeID;
                                oAdminSeg.L0 = oOrigDef.L0;
                                oAdminSeg.L1 = oOrigDef.L1;
                                oAdminSeg.L2 = oOrigDef.L2;
                                oAdminSeg.L3 = oOrigDef.L3;
                                oAdminSeg.L4 = oOrigDef.L4;
                                oAdminSeg.HelpText = oOrigDef.HelpText;
                                oAdminSeg.ChildSegmentIDs = oOrigDef.ChildSegmentIDs;
                                oAdminSeg.IntendedUse = oOrigDef.IntendedUse;
                                oAdminSeg.DefaultDoubleClickBehavior = oOrigDef.DefaultDoubleClickBehavior;
                                oAdminSeg.DefaultDoubleClickLocation = ((AdminSegmentDef)oCurSeg.Definition).DefaultDoubleClickLocation;
                                oAdminSeg.DefaultDragBehavior = ((AdminSegmentDef)oCurSeg.Definition).DefaultDragBehavior;
                                oAdminSeg.DefaultDragLocation = ((AdminSegmentDef)oCurSeg.Definition).DefaultDragLocation;
                                oAdminSeg.DefaultMenuInsertionBehavior = ((AdminSegmentDef)oCurSeg.Definition).DefaultMenuInsertionBehavior;
                                oAdminSeg.MenuInsertionOptions = ((AdminSegmentDef)oCurSeg.Definition).MenuInsertionOptions;
                                // Copy existing assignments and KeySets from original segment
                                LMP.Data.Application.CopyFirmKeySetsForObjectID(oCurDef.ID, oAdminSeg.ID);
                                LMP.Data.Application.CopyAssignmentsForObject(oCurDef.TypeID, oCurDef.ID, oAdminSeg.ID);
                            }
                            else
                            {
                                oAdminSeg.MenuInsertionOptions = (short)(Segment.InsertionLocations.InsertInNewDocument) +
                                    (short)(Segment.InsertionLocations.InsertAtSelection);
                            }

                        }
                        else
                        {
                            mpObjectTypes iSegType = oAdminSeg.TypeID;

                            if (iType == NewSegmentItems.MasterDataForm)
                                iSegType = mpObjectTypes.MasterData;

                            oAdminSeg.XML = GetWordXML(iType, oAdminSeg.ID.ToString(), iSegType,
                                oAdminSeg.DisplayName, oAdminSeg.Name, oAdminSeg.HelpText, oAdminSeg.IntendedUse,
                                oAdminSeg.DefaultDoubleClickLocation, oAdminSeg.IntendedUse < mpSegmentIntendedUses.AsParagraphText ? 1 : 0); //GLOG 8376
                            if (iType != NewSegmentItems.StyleSheetFromDocument)
                            {
                                if (xRelatedSegmentIDs != "")
                                    oAdminSeg.ChildSegmentIDs = xRelatedSegmentIDs;
                                oAdminSeg.MenuInsertionOptions = (short)(Segment.InsertionLocations.InsertInNewDocument) +
                                    (short)(Segment.InsertionLocations.InsertAtSelection);
                            }
                        }

                        oAdminSegs.Save((LongIDSimpleDataItem)oAdminSeg);

                        oMember = (FolderMember)(oFolder.GetMembers().Create());
                        oMember.ObjectID1 = oAdminSeg.ID;
                        oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);

                        RefreshAdminFolderNode(oNode, true);

                        oNewNode = this.treeContent.GetNodeByKey(
                            string.Concat("A", oNode.Key, mpPathSeparator, oMember.ID));
                        this.treeContent.ActiveNode = oNewNode;

                        oNewNode.Selected = true;
                    }
                    else if (ContentTreeHelper.IsUserFolderNode(oNode))
                    {
                        UserFolder oFolder = (UserFolder)oNode.Tag;
                        UserSegmentDefs oUserSegs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, Session.CurrentUser.ID);
                        UserSegmentDef oUserSeg = (UserSegmentDef)oForm.NewSegmentDef;
                        if (oCurSeg != null)
                        {
                            // If creating from Document that already contains a segment, replace references
                            // to old segment in tags
                            UserSegmentDef oCurDef = null;
                            if (oCurSeg.HasDefinition)
                                oCurDef = (UserSegmentDef)oCurSeg.Definition;

                            oCurSeg.Select();
                            Object oOne = (object)1;
                            Object oMinusOne = (object)-1;

                            // Expand selection to include mSeg tags
                            Word.Selection oSel = Session.CurrentWordApp.Selection;
                            oSel.MoveStart(ref oOne, ref oMinusOne);
                            oSel.MoveEnd(ref oOne, ref oOne);
                            string xXML = "";
                            int iSelLength = 0;
                            try
                            {
                                iSelLength = oSel.Text.Length;
                            }
                            catch
                            {
                                // An error will occur if Selection is zero-length
                            }

                            int iDocLength = oDoc.Content.Text.Length;

                            //GLOG 8347: Ensure XML does not contain rsids
                            bool bStore = Session.CurrentWordApp.Options.StoreRSIDOnSave;
                            Session.CurrentWordApp.Options.StoreRSIDOnSave = false;
                            try
                            {
                                // If existing segment encloses entire document get document XML, else Selection xml
                                if (this.TaskPane.ForteDocument.FileFormat == mpFileFormats.Binary)
                                {
                                    //xml tags
                                    if ((iDocLength == 1 || iSelLength >= iDocLength - 1) && oCurSeg == null)
                                        xXML = oDoc.Content.get_XML(false);
                                    else
                                        xXML = oSel.get_XML(false);

                                // Make sure mpNewSegment bookmark exists for Segment XML
                                if (xXML.IndexOf(m_xBookmarkTag) == -1)
                                {
                                    int iPos = xXML.IndexOf("<" + LMP.String.GetNamespacePrefix(xXML,
                                        ForteConstants.MacPacNamespace) + ":mSEG");

                                    if (iPos > -1)
                                    {
                                        iPos = xXML.IndexOf(">", iPos + 1);
                                        if (iPos > -1)
                                            xXML = xXML.Insert(iPos + 1, m_xBookmarkTag);
                                    }
                                }
                            }
                            else
                            {
                                //content controls
                                if ((iDocLength == 1 || iSelLength >= iDocLength - 1) && oCurSeg == null)
                                    xXML = oDoc.Content.WordOpenXML;
                                else
                                    xXML = oSel.WordOpenXML;

                                // Make sure mpNewSegment bookmark exists for Segment XML
                                if (xXML.IndexOf(Segment.NEW_SEGMENT_BMK_OPENXML) == -1)
                                {
                                    int iPos = xXML.IndexOf("w:tag w:val=\"mps");
                                    if (iPos > -1)
                                    {
                                        iPos = xXML.IndexOf("<w:sdtContent>", iPos);
                                        if (iPos > -1)
                                            xXML = xXML.Insert(iPos + 14, Segment.NEW_SEGMENT_BMK_OPENXML);
                                    }
                                }
                                }
                            }
                            catch (System.Exception oE)
                            {
                                throw oE;
                            }
                            finally
                            {
                                //GLOG 8347: restore original rside state
                                Session.CurrentWordApp.Options.StoreRSIDOnSave = bStore;
                            }

                            if (oCurDef != null)
                            {
                                //We're copying the definition of an existing UserSegment
                                //Replace references to the old ID and Name in XML
                                oUserSeg.XML = String.ReplaceSegmentXML(xXML, oCurDef.ID, oUserSeg.ID,
                                    oCurDef.DisplayName, oUserSeg.DisplayName, oCurDef.Name, oUserSeg.Name, "0");
                                //Copy Segment Definition options
                                UserSegmentDef oOrigDef = (UserSegmentDef)oCurDef;
                                oCurDef.HelpText = oOrigDef.HelpText;
                            }
                        }
                        else
                        {
                            NewSegmentItems iUseType = iType;
                            if (iType == NewSegmentItems.SegmentFromDocument && this.TaskPane.ForteDocument.Segments.Count > 0)
                            {
                                iUseType = NewSegmentItems.SegmentFromSelection;
                                this.TaskPane.ForteDocument.WordDocument.Content.Select();
                            }
                            oUserSeg.XML = GetWordXML(iUseType, oUserSeg.ID, oUserSeg.TypeID,
                                oUserSeg.DisplayName, oUserSeg.Name, oUserSeg.HelpText,
                                oUserSeg.IntendedUse, oUserSeg.DefaultDoubleClickLocation, (oUserSeg.IntendedUse < mpSegmentIntendedUses.AsParagraphText) && bAsDesigner ? 1 : 0); //GLOG 8376
                            //GLOG 8376
                            if (bAsDesigner)
                            {
                                oUserSeg.MenuInsertionOptions = (short)(Segment.InsertionLocations.InsertInNewDocument) +
                                    (short)(Segment.InsertionLocations.InsertAtSelection);
                            }
                        }
                        oUserSegs.Save((StringIDSimpleDataItem)oUserSeg);
                        int iID1;
                        int iID2;
                        LMP.String.SplitID(oUserSeg.ID, out iID1, out iID2);
                        oMember = (FolderMember)(oFolder.GetMembers().Create());
                        oMember.ObjectID1 = iID1;
                        oMember.ObjectID2 = iID2;
                        oMember.ObjectTypeID = mpFolderMemberTypes.UserSegment;
                        oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);
                        RefreshUserFolderNode(oNode, true);
                        oNewNode = this.treeContent.GetNodeByKey(string.Concat("U", oNode.Key, mpPathSeparator, oMember.ID));
                        this.treeContent.ActiveNode = oNewNode;
                        oNewNode.Selected = true;
                    }
                    // Restore original environment
                    oEnv.RestoreState(true, false, false);

                    //go to design mode - prompt in some circumstances
                    switch(iType)
                    {
                        case NewSegmentItems.SegmentFromSelection:
                        case NewSegmentItems.SegmentFromDocument:
                            if (Session.AdminMode)
                            {
                                // If creating segment from selection, ask before switching to design
                                DialogResult iAns = MessageBox.Show(LMP.Resources.GetLangString("Msg_NewSegment_PromptForDesign"),
                                   LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (iAns == DialogResult.No)
                                    return;
                                else
                                    DesignSegmentFromNode(oNewNode, true);
                            }
                            break;
                        case NewSegmentItems.StyleSheetFromDocument:
                            break;
                        default:
                            DesignSegmentFromNode(oNewNode, true);
                            break;
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SegmentException(
                    LMP.Resources.GetLangString("Error_Segment_Create"), oE);
            }
            finally
            {
                TaskPane.ScreenUpdating = true;
                m_iNewSegmentSelection = NewSegmentItems.None;
            }
        }

        public void SaveAsUserSegment(AdminSegmentDef oAdminDef)
        {
            SaveAsUserSegmentForm oForm = new SaveAsUserSegmentForm();

            if (oForm.ShowDialog() == DialogResult.OK)
            {
                int iID1;
                int iID2;
                UserSegmentDef oUserSeg = oAdminDef.SaveAsUserSegmentDef(
                    oForm.ItemName, oForm.ItemDisplayName);

                LMP.String.SplitID(oUserSeg.ID, out iID1, out iID2);

                UserFolders oFolders = new UserFolders();
                UserFolder oFolder = (UserFolder)oFolders.ItemFromID(oForm.SelectedFolderID);
                FolderMember oMember = (FolderMember)(oFolder.GetMembers().Create());
                oMember.ObjectID1 = iID1;
                oMember.ObjectID2 = iID2;
                oMember.ObjectTypeID = mpFolderMemberTypes.UserSegment;
                oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);

                UltraTreeNode oNode = this.SelectFolderNode(oForm.SelectedFolderNodeKey);

                UltraTreeNode oNewNode = null;

                if (oNode != null)
                {
                    //node is showing, or has been previously shown -
                    //refresh
                    RefreshUserFolderNode(oNode, true);
                    oNewNode = this.treeContent.GetNodeByKey(
                        string.Concat("U", oNode.Key, mpPathSeparator, oMember.ID));
                    this.treeContent.ActiveNode = oNewNode;
                    oNewNode.Selected = true;
                }

                // If creating segment from selection, ask before switching to design
                DialogResult iAns = MessageBox.Show(LMP.Resources.GetLangString("Msg_NewSegment_PromptForDesign"),
                   LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (iAns == DialogResult.No)
                    return;
                else
                    DesignSegmentFromNode(oNewNode, true);
            }
        }

        ///// <summary>
        ///// GLOG : 3138 : JAB
        ///// Determine if this mode reflects content which is user content.
        ///// </summary>
        ///// <param name="oMode"></param>
        ///// <returns></returns>
        //private bool IsUserContent(ContentPropertyForm.DialogMode oMode)
        //{
        //    return (oMode == LMP.MacPac.ContentPropertyForm.DialogMode.NewSegment || 
        //            oMode == LMP.MacPac.ContentPropertyForm.DialogMode.NewStyleSheet || 
        //            oMode == LMP.MacPac.ContentPropertyForm.DialogMode.NewParagraphTextSegment);
        //}

        /// <summary>
        /// refreshes all currently expanded nodes in Content Manager
        /// </summary>
        public void RefreshTree()
        {
            this.RefreshTree(Session.CurrentUser.UserSettings.TaskPaneBackgroundColor,
                Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None);
       }

        public void RefreshTree(Color oBackcolor, GradientStyle oGradient)
        {
            m_oUserFolders = null;
            m_oAdminFolders = null;
            string xSelectedKey= null;

            //get currently selected node
            if(this.treeContent.SelectedNodes.Count > 0)
                xSelectedKey = this.treeContent.SelectedNodes[0].Key;

            //get collection of expanded nodes
            List<string> oExpandedNodes = new List<string>();
            GetExpandedNodes(oExpandedNodes, this.treeContent.Nodes);

            this.RefreshTopLevelNodes();

            //restore nodes to expansion state
            foreach (string xNodeKey in oExpandedNodes)
            {
                UltraTreeNode oNode = this.treeContent.GetNodeByKey(xNodeKey);
                if (oNode != null && (ContentTreeHelper.IsAdminFolderNode(oNode) || ContentTreeHelper.IsUserFolderNode(oNode)))
                    oNode.Expanded = true;
            }

            if (xSelectedKey != null)
            {
                //there was a selected node - reselect it
                UltraTreeNode oSelNode = this.treeContent.GetNodeByKey(xSelectedKey);
                if (oSelNode != null)
                {
                    try
                    {
                        this.treeContent.ActiveNode = oSelNode;
                        oSelNode.Selected = true;
                        SetBrowserContents(oSelNode);
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Refresh the backcolor and gradient of the tree and the help view.
        /// </summary>
        /// <param name="oBackcolor"></param>
        /// <param name="oGradient"></param>
        public void RefreshAppearance(Color oBackcolor, GradientStyle oGradient)
        {
            // Hide the help window or display it with the appropriate size.
            this.UpdateHelpWindowView();

            // Set the backcolor and gradient for the tree per user's prefs.
            if (oBackcolor == null)
            {
                oBackcolor = Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;
            }

            if (oGradient == null)
            {
                oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                    GradientStyle.BackwardDiagonal : GradientStyle.None;
            }

            if (this.treeContent.Appearance.BackColor != oBackcolor)
            {
                // Set the backcolor
                this.treeContent.Appearance.BackColor = oBackcolor;
            }

            if (this.treeContent.Appearance.BackGradientStyle != oGradient)
            {
                //set the gradient according to the user's preference.
                this.treeContent.Appearance.BackGradientStyle = oGradient;
            }
        }

        private void SetTreeAppearance()
        {
            // Set the backcolor and gradient of the tree according to the user's prefs
            this.treeContent.Appearance.BackColor = 
                Session.CurrentUser.UserSettings.TaskPaneBackgroundColor;

            GradientStyle oGradient = Session.CurrentUser.UserSettings.UseGradientInTaskPane ?
                GradientStyle.BackwardDiagonal : GradientStyle.None;

            //set the gradient according to the user's preference.
            this.treeContent.Appearance.BackGradientStyle = oGradient;
        }

        private void SetFolderPermissions(UltraTreeNode oNode)
        {
            ContentTreeHelper.LoadNodeReference(oNode);
            if (!ContentTreeHelper.IsAdminFolderNode(oNode))
                return;

            Folder oFolder = (Folder)oNode.Tag;
            UltraTreeNode oParentNode = oNode.Parent;
            int iParentID = 0;

            if (oParentNode != null)
            {
                if (oNode.Parent.Tag is int)
                {
                    iParentID = (int)oNode.Parent.Tag;
                }
                else
                {
                    Folder oParentFolder = (Folder)oNode.Parent.Tag;
                    iParentID = oParentFolder.ID;
                }
            }

            PermissionsForm oForm = new PermissionsForm(mpObjectTypes.ContentFolder, oFolder.ID,
                LMP.Resources.GetLangString("Dialog_FolderPermissions_Title"),
                LMP.Resources.GetLangString("Dialog_FolderPermissions_Description"), iParentID);

            oForm.ShowDialog();
        }

        private void ImportDataFileDetail(UltraTreeNode oFolderNode)
        {
            string xExistingAdminSegDefs = "";
            string xNonExistingAdminSegDefs = "";
            string xNonExistingUserSegDefs = "";
            string xExistingUserSegDefs = "";
            string xConvertedIDs = "";
            bool bTargetIsUserFolder = false;
            bool bReplaceExisting = false;
            bool bReplacePreferences = false;
            bool bFirstPrefSegment = true;
            const string EXPORT_REG_VALUE_NAME = "SegmentExportPath"; //GLOG 6647
            ArrayList aExistingSegments = new ArrayList();
            //GLOG 6757
            ArrayList aHandledSegments = new ArrayList();

            //get .mps file
            OpenFileDialog oDlg = new OpenFileDialog();
            oDlg.Title = LMP.Resources.GetLangString("Lbl_ImportSegment");
            //GLOG 6647: Get last selected path from registry
            string xPath = LMP.Registry.GetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, EXPORT_REG_VALUE_NAME);
            //If invalid or not set, default to Writable Data location
            if (string.IsNullOrEmpty(xPath) || !Directory.Exists(xPath))
                xPath = LMP.Data.Application.WritableDBDirectory; //JTS 3/28/13
            oDlg.InitialDirectory = xPath;
            oDlg.Filter = LMP.ComponentProperties.ProductName + " Data File (*.mpd)|*.mpd";
            oDlg.DefaultExt = ".mpd";
            if (oDlg.ShowDialog() == DialogResult.OK)
            {
                //GLOG 6647: Save selected path as default
                LMP.Registry.SetCurrentUserValue(LMP.Registry.MacPac10RegistryRoot, EXPORT_REG_VALUE_NAME, Path.GetDirectoryName(oDlg.FileName));
                //get data from file
                string xItems = File.ReadAllText(oDlg.FileName);

                //split into items to import
                string[] aItems = xItems.Split(new string[]{
                    LMP.Data.ForteConstants.mpDataFileEntitySep},
                    StringSplitOptions.RemoveEmptyEntries);
                //GLOG 5826: Allow importing UserSegments to User Folder
                bTargetIsUserFolder = oFolderNode.Tag is UserFolder;
                //alert if there are any
                foreach (string xItem in aItems)
                {
                    //get type of data in file - parse first field
                    int iPos = xItem.IndexOf(LMP.Data.ForteConstants.mpDataFileFieldSep);
                    string xType = xItem.Substring(0, iPos);
                    string xItemMod = xItem.Substring(iPos +
                        LMP.Data.ForteConstants.mpDataFileFieldSep.Length);

                    if (xType == "UserSegmentDef")
                    {
                        //get segment def object
                        UserSegmentDef oUserDef = UserSegmentDef.FromString(xItemMod);

                        if (bTargetIsUserFolder)
                        {
                            //GLOG 5826: Check if UserSegment with same name already exists
                            UserSegmentDefs oUserDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, Session.CurrentUser.ID);
                            UserSegmentDef oExistingUserDef = null;
                            for (int u = 1; u < oUserDefs.Count; u++)
                            {
                                UserSegmentDef oCurDef = (UserSegmentDef)oUserDefs.ItemFromIndex(u);
                                if (oCurDef.Name.ToUpper() == oUserDef.Name.ToUpper())
                                {
                                    oExistingUserDef = oCurDef;
                                    break;
                                }
                            }
                            if (oExistingUserDef != null)
                            {
                                if (oExistingUserDef.XML != oUserDef.XML)
                                {
                                    //GLOG 6757: Don't import the same segment definition more than once
                                    if (!aExistingSegments.Contains(oUserDef.Name))
                                    {
                                        //definition has changed, we'll be importing this segment
                                        xExistingUserSegDefs += oUserDef.Name + "\r\n";
                                        aExistingSegments.Add(oUserDef.Name);
                                    }
                                }
                            }
                            else
                            {
                                xNonExistingUserSegDefs += oUserDef.Name + "\r\n";
                            }
                        }
                        else
                        {
                            if (AdminSegmentDefs.Exists(oUserDef.Name))
                            {
                                //GLOG 6757: Don't import the same segment definition more than once
                                if (!aExistingSegments.Contains(oUserDef.Name))
                                {
                                    xExistingAdminSegDefs += oUserDef.Name + "\r\n";
                                    aExistingSegments.Add(oUserDef.Name);
                                }
                            }
                            else
                            {
                                xNonExistingAdminSegDefs += oUserDef.Name + "\r\n";
                            }
                        }
                    }
                    else if (xType == "AdminSegmentDef")
                    {
                        //get segment def object
                        AdminSegmentDef oAdminDef = AdminSegmentDef.FromString(xItemMod);
                        //Don't import Collection table types
                        if (oAdminDef.TypeID == mpObjectTypes.AgreementSignatures || oAdminDef.TypeID == mpObjectTypes.LetterSignatures ||
                            oAdminDef.TypeID == mpObjectTypes.CollectionTable || oAdminDef.TypeID == mpObjectTypes.PleadingCaptions ||
                            oAdminDef.TypeID == mpObjectTypes.PleadingCounsels || oAdminDef.TypeID == mpObjectTypes.PleadingSignatures)
                            continue;

                        if (AdminSegmentDefs.Exists(oAdminDef.Name))
                        {
                            AdminSegmentDefs oDefs = new AdminSegmentDefs();
                            int iID = AdminSegmentDefs.GetIDFromName(oAdminDef.Name);
                            AdminSegmentDef oExistingDef = (AdminSegmentDef)oDefs.ItemFromID(iID);

                            if (oExistingDef.XML != oAdminDef.XML)
                            {
                                //GLOG 6757: Don't import the same segment definition more than once
                                if (!aExistingSegments.Contains(oAdminDef.Name))
                                {
                                    //definition has changed, we'll be importing this segment
                                    xExistingAdminSegDefs += oAdminDef.Name + "\r\n";
                                    aExistingSegments.Add(oAdminDef.Name);
                                }
                            }

                        }
                        else
                        {
                            xNonExistingAdminSegDefs += oAdminDef.Name + "\r\n";
                        }
                    }
                }

                if ((bTargetIsUserFolder && xExistingUserSegDefs != "") || (!bTargetIsUserFolder && xExistingAdminSegDefs != ""))
                {
                    //admin segments with the same name exist - 
                    //prompt to replace
                    string xType = bTargetIsUserFolder ? "user" : "admin";
                    string xExistingDefs = bTargetIsUserFolder ? xExistingUserSegDefs : xExistingAdminSegDefs;
                    string xMsg = string.Format(LMP.Resources.GetLangString("Error_CantImportSegNameAlreadyExists"), xType, xExistingDefs);
                    ImportDataFileMessageBox oMsgBox = new ImportDataFileMessageBox();
                    oMsgBox.MessageText = xMsg;
                    DialogResult iRes = oMsgBox.ShowDialog();

                    switch (iRes)
                    {
                        case DialogResult.Yes:
                            bReplaceExisting = true;
                            break;
                        case DialogResult.No:
                            bReplaceExisting = false;
                            break;
                        default:
                            return;
                            break;
                    }
                }
                else if ((bTargetIsUserFolder && (xExistingUserSegDefs == "") && (xNonExistingUserSegDefs == "")) || 
                    (!bTargetIsUserFolder && xExistingAdminSegDefs == "" & xNonExistingAdminSegDefs == ""))
                {
                    string xMsg = LMP.Resources.GetLangString("Msg_ImportDataFileNothingToDo");
                    DialogResult iRes = MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                bool bSegmentImported = false;
                for (int i = 0; i < aItems.Length; i++)
                {
                    string xItem = aItems[i];

                    //get type of data in file - parse first field
                    int iPos2 = xItem.IndexOf(LMP.Data.ForteConstants.mpDataFileFieldSep);
                    string xType = xItem.Substring(0, iPos2);
                    string xItemMod = xItem.Substring(iPos2 + LMP.Data.ForteConstants.mpDataFileFieldSep.Length);

                    switch (xType)
                    {
                        case "AdminSegmentDef":
                            {
                                //GLOG item #5883 - dcf
                                if (oFolderNode.Tag is UserFolder)
                                {
                                    MessageBox.Show(LMP.Resources.GetLangString("Error_CantImportAdminSegmentIntoUserFolder"),
                                        LMP.ComponentProperties.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation);
                                    return;
                                }

                                //get segment def object
                                AdminSegmentDef oAdminDef = AdminSegmentDef.FromString(xItemMod);
                                if (oAdminDef.TypeID == mpObjectTypes.AgreementSignatures || oAdminDef.TypeID == mpObjectTypes.LetterSignatures ||
                                    oAdminDef.TypeID == mpObjectTypes.CollectionTable || oAdminDef.TypeID == mpObjectTypes.PleadingCaptions ||
                                    oAdminDef.TypeID == mpObjectTypes.PleadingCounsels || oAdminDef.TypeID == mpObjectTypes.PleadingSignatures)
                                    continue;
                                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                                string xOrigName = oAdminDef.Name;
                                //GLOG 6757: Skip if already handled
                                if (aHandledSegments.Contains(oAdminDef.ID))
                                {
                                    continue;
                                }
                                else
                                {
                                    aHandledSegments.Add(oAdminDef.ID);
                                }
                                //GLOG 5826: Need to assign new name to imported Segment
                                if (AdminSegmentDefs.Exists(oAdminDef.Name) && !bReplaceExisting && aExistingSegments.Contains(oAdminDef.Name))
                                {
                                    string xNewName = "";
                                    string xNewDisplayName = "";
                                    ContentPropertyForm oForm = new ContentPropertyForm();
                                    oForm.Text = "Enter New Name for " + oAdminDef.Name;
                                    DialogResult iRet = oForm.ShowForm(this.ParentForm, oFolderNode, new System.Drawing.Point(), ContentPropertyForm.DialogMode.RenameImportedSegment);
                                    if (iRet == DialogResult.OK)
                                    {
                                        xNewName = oForm.ContentName;
                                        xNewDisplayName = oForm.ContentDisplayName;
                                    }
                                    else
                                    {
                                        return;
                                    }
                                    oAdminDef.XML = LMP.String.ReplaceSegmentXML(oAdminDef.XML, oAdminDef.ID.ToString(), oAdminDef.ID.ToString(), oAdminDef.DisplayName,
                                        xNewDisplayName, oAdminDef.Name, xNewName, oAdminDef.TranslationID.ToString());
                                    oAdminDef.Name = xNewName;
                                    oAdminDef.DisplayName = xNewDisplayName;
                                    xItemMod = oAdminDef.ToString(LMP.Data.ForteConstants.mpDataFileFieldSep);
                                }
                                if (!AdminSegmentDefs.Exists(oAdminDef.Name))
                                {
                                    //GLOG 5238: Reuse same ID if available
                                    AdminSegmentDef oMatchID = null;
                                    try
                                    {
                                        oMatchID = (AdminSegmentDef)oDefs.ItemFromID(oAdminDef.ID);
                                    }
                                    catch 
                                    { 
                                    }
                                    if (oMatchID == null)
                                    {
                                        //Need to Insert directly bypassing Data Class, since new ID is being specified
                                        //Set all except XML and ChildSegmentIDs, which will be modified as necessary and set below
                                        string xSQL = @"INSERT INTO Segments ( ID, DisplayName, Name, TranslationID, TypeID, " +
                                            @"L0, L1, L2, L3, L4, MenuInsertionOptions, DefaultMenuInsertionBehavior, DefaultDragLocation, " +
                                            @"DefaultDragBehavior, DefaultDoubleClickLocation, DefaultDoubleClickBehavior, HelpText, " +
                                            @"IntendedUse, LastEditTime ) VALUES(@ID, @DisplayName, @Name, @TranslationID, @TypeID, " +
                                            @"@L0, @L1, @L2, @L3, @L4, @MenuInsertionOptions, @DefaultMenuInsertionBehavior, @DefaultDragLocation, @DefaultDragBehavior, " +
                                            @"@DefaultDoubleClickLocation, @DefaultDoubleClickBehavior, @HelpText, @IntendedUse, @LastEditTime);";

                                        object[] oParams = new object[] {oAdminDef.ID, oAdminDef.DisplayName, oAdminDef.Name, oAdminDef.TranslationID, oAdminDef.TypeID,
                                                oAdminDef.L0, oAdminDef.L1, oAdminDef.L2, oAdminDef.L3, oAdminDef.L4, 
                                                oAdminDef.MenuInsertionOptions, oAdminDef.DefaultMenuInsertionBehavior, oAdminDef.DefaultDragLocation,
                                                oAdminDef.DefaultDragBehavior,oAdminDef.DefaultDoubleClickLocation, oAdminDef.DefaultDoubleClickBehavior,
                                                oAdminDef.HelpText, oAdminDef.IntendedUse, LMP.Data.Application.GetCurrentEditTime()};

                                        System.Data.OleDb.OleDbCommand oCmd = new System.Data.OleDb.OleDbCommand();

                                        using (oCmd)
                                        {
                                            try
                                            {
                                                //set up command
                                                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                                                oCmd.CommandText = xSQL;
                                                oCmd.CommandType = CommandType.Text;

                                                //add parameters to command
                                                foreach (object oParam in oParams)
                                                {
                                                    System.Data.OleDb.OleDbParameter oOleDbParam = new System.Data.OleDb.OleDbParameter();
                                                    oOleDbParam.Value = (oParam == null) ? System.DBNull.Value : oParam;
                                                    oCmd.Parameters.Add(oOleDbParam);
                                                }
                                                //execute command
                                                int iRecsAffected = oCmd.ExecuteNonQuery();
                                            }

                                            catch (System.Exception e)
                                            {
                                                throw new LMP.Exceptions.SQLStatementException(
                                                    Resources.GetLangString("Error_SQLStatementFailed") +
                                                    xSQL, e);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        AdminSegmentDef oNewDef = (AdminSegmentDef)oDefs.Create(oAdminDef.TypeID);
                                        oNewDef.Name = oAdminDef.Name;
                                        oNewDef.DisplayName = oAdminDef.DisplayName;
                                        oNewDef.IsDirty = true;
                                        oDefs.Save((LongIDSimpleDataItem)oNewDef);
                                    }
                                }
                                int iID = AdminSegmentDefs.GetIDFromName(oAdminDef.Name);
                                AdminSegmentDef oExistingDef = (AdminSegmentDef)oDefs.ItemFromID(iID);

                                if (oExistingDef.XML != oAdminDef.XML)
                                {
                                    if (oAdminDef.ID != oExistingDef.ID)
                                    {
                                        //add ids to converted ID string so that future
                                        //references to the old ID can be replaced with the new ID
                                        xConvertedIDs += oAdminDef.ID.ToString() + "=" + oExistingDef.ID.ToString() + ",";
                                        //Change ID to existing record
                                        oAdminDef.SetID(oExistingDef.ID);
                                    }

                                    //JTS 4/29/13:  Check for IDs to convert even if current Segment doesn't have new ID
                                    //Child Segments may have been replaced previously
                                    if (xConvertedIDs != "")
                                    {
                                        //replace old ID with new ID for all Content Imported to this point
                                        string[] aConvertedIDs = xConvertedIDs.Split(new char[] { ',' },
                                            StringSplitOptions.RemoveEmptyEntries);

                                        foreach (string xConvertedID in aConvertedIDs)
                                        {
                                            int iPos = xConvertedID.IndexOf("=");
                                            string xOldID = xConvertedID.Substring(0, iPos);
                                            string xNewID = xConvertedID.Substring(iPos + 1);
                                            //Replace Segment ID references to child or current Segment in XML
                                            //GLOG 5238: zzTEMP marker is used to avoid replacing IDs that have already been replaced 
                                            //on a previous pass when there is overlap between the old and new IDs
                                            //GLOG 5308: Segment ID can also appear as part of ContentControl tag, preceded by '0's padded to 19 chars
                                            oAdminDef.XML = oAdminDef.XML.Replace(xOldID.PadLeft(19, '0') + "0", string.Concat("zzmpCCTemp", xNewID).PadLeft(29, '0') + "0");
                                            oAdminDef.XML = oAdminDef.XML.Replace("SegmentID=" + xOldID + "|", "SegmentID=" + xNewID + "|");
                                            //Replace occurrences of Old ID in ChildSegmentIDs with New ID
                                            oAdminDef.ChildSegmentIDs = oAdminDef.ChildSegmentIDs.Replace("|" + xOldID + ".0", "|zzTEMP" + xNewID + ".0");
                                            oAdminDef.ChildSegmentIDs = oAdminDef.ChildSegmentIDs.Replace(xOldID + ".0" + "|zzTEMP", xNewID + ".0" + "|");
                                            if (oAdminDef.ChildSegmentIDs == xOldID + ".0")
                                                oAdminDef.ChildSegmentIDs = "zzTEMP" + xNewID + ".0";
                                        }
                                        oAdminDef.XML = oAdminDef.XML.Replace("SegmentID=zzTEMP", "SegmentID=");
                                        //GLOG 5308: Remove temp markers placed in ContentControl tags
                                        oAdminDef.XML = oAdminDef.XML.Replace("zzmpCCTemp", "");
                                        oAdminDef.ChildSegmentIDs = oAdminDef.ChildSegmentIDs.Replace("zzTEMP", "");
                                    }
                                    xItemMod = oAdminDef.ToString(LMP.Data.ForteConstants.mpDataFileFieldSep);
                                    //definition has changed, import
                                    //JTS 7/13/10: Assign to Selected Folder if Segment of same name didn't already exist
                                    ImportAdminSegmentDef(xItemMod, oFolderNode, xNonExistingAdminSegDefs.Contains(oAdminDef.Name + "\r\n") || 
                                        (!bReplaceExisting && aExistingSegments.Contains(xOrigName)));
                                    bSegmentImported = true;
                                }
                                else
                                    bSegmentImported = false;
                                break;
                            }
                        case "UserSegmentDef":
                            {
                                if (xConvertedIDs != "")
                                {
                                    //replace old ID with new ID
                                    string[] aConvertedIDs = xConvertedIDs.Split(new char[] { ',' },
                                        StringSplitOptions.RemoveEmptyEntries);

                                    foreach (string xConvertedID in aConvertedIDs)
                                    {
                                        int iPos = xConvertedID.IndexOf("=");
                                        string xOldID = xConvertedID.Substring(0, iPos);
                                        string xNewID = xConvertedID.Substring(iPos + 1);

                                        //GLOG 5238: zzTEMP marker is used to avoid replacing IDs that have already been replaced 
                                        //on a previous pass when there is overlap between the old and new IDs
                                        //GLOG 5308: Segment ID can also appear as part of ContentControl tag, preceded by '0's padded to 19 chars
                                        //With User Segment, decimal separator will be replaced with 'd' in the tag
                                        if (bTargetIsUserFolder) //GLOG 5826: Import Designer Segment into User Folder
                                        {
                                            xItemMod = xItemMod.Replace(xOldID.Replace('.', 'd').PadLeft(19, '0') + "0", string.Concat("zzmpCCTemp", xNewID.Replace('.', 'd')).PadLeft(29, '0') + "0");
                                        }
                                        else
                                        {
                                            xItemMod = xItemMod.Replace(xOldID.Replace('.', 'd').PadLeft(19, '0') + "0", string.Concat("zzmpCCTemp", xNewID).PadLeft(29, '0') + "0");
                                            xItemMod = xItemMod.Replace("ObjectTypeID=99", "ObjectTypeID=4");
                                        }

                                        xItemMod = xItemMod.Replace("SegmentID=" + xOldID + "|", "SegmentID=zzTEMP" + xNewID + "|");
                                        //xItemMod = xItemMod.Replace(xOldID, xNewID + ".0");
                                    }
                                    xItem = xItemMod.Replace("SegmentID=zzTEMP", "SegmentID=");
                                    //GLOG 5308: Remove temp markers placed in ContentControl tags
                                    xItem = xItem.Replace("zzmpCCTemp", "");
                                }

                                //get segment def object
                                UserSegmentDefs oUserDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, Session.CurrentUser.ID);
                                UserSegmentDef oUserDef = UserSegmentDef.FromString(xItemMod);
                                string xOrigName = oUserDef.Name;
                                string xUserDefID = oUserDef.ID;
                                //GLOG 6757: Skip if already handled
                                if (aHandledSegments.Contains(xUserDefID))
                                {
                                    continue;
                                }
                                else
                                {
                                    aHandledSegments.Add(xUserDefID);
                                }

                                if (oFolderNode.Tag is Folder)
                                {
                                    //GLOG 5826: Need to assign new name to imported Segment
                                    if (!bReplaceExisting && aExistingSegments.Contains(oUserDef.Name))
                                    {
                                        string xNewName = "";
                                        string xNewDisplayName = "";
                                        ContentPropertyForm oForm = new ContentPropertyForm();
                                        DialogResult iRet = oForm.ShowForm(this.ParentForm, oFolderNode, new System.Drawing.Point(), ContentPropertyForm.DialogMode.RenameImportedSegment);
                                        if (iRet == DialogResult.OK)
                                        {
                                            xNewName = oForm.ContentName;
                                            xNewDisplayName = oForm.ContentDisplayName;
                                        }
                                        else
                                        {
                                            return;
                                        }

                                        oUserDef.XML = LMP.String.ReplaceSegmentXML(oUserDef.XML, oUserDef.ID, oUserDef.ID, oUserDef.DisplayName,
                                            xNewDisplayName, oUserDef.Name, xNewName, "");
                                        oUserDef.Name = xNewName;
                                        oUserDef.DisplayName = xNewDisplayName;
                                        xItemMod = oUserDef.ToString(LMP.Data.ForteConstants.mpDataFileFieldSep);
                                    }
                                    int iAdminID = ImportUserSegmentDefAsAdminDef(xItemMod, oFolderNode, true);
                                    bSegmentImported = iAdminID != 0;
                                    if (bSegmentImported)
                                    {
                                        //add ids to converted ID string so that future
                                        //references to the old ID can be replaced with the new ID
                                        xConvertedIDs += xUserDefID + "=" + iAdminID.ToString() + ",";
                                    }
                                }
                                else if (oFolderNode.Tag is UserFolder)
                                {
                                    //Generate new UserSegmentDef if Existing item is not replaced, or if no UserSegment with same name exists
                                    if ((!bReplaceExisting && aExistingSegments.Contains(oUserDef.Name)) || (xNonExistingUserSegDefs.Contains(oUserDef.Name + "\r\n")))
                                    {
                                        string xNewName = oUserDef.Name;
                                        string xNewDisplayName = oUserDef.DisplayName;
                                        string xNewHelpText = oUserDef.HelpText;
                                        //If current name is already used, prompt for new name
                                        if (!xNonExistingUserSegDefs.Contains(oUserDef.Name + "\r\n"))
                                        {
                                            ContentPropertyForm oForm = new ContentPropertyForm();
                                            DialogResult iRet = oForm.ShowForm(this.ParentForm, oFolderNode, new System.Drawing.Point(), ContentPropertyForm.DialogMode.RenameImportedSegment);
                                            if (iRet == DialogResult.OK)
                                            {
                                                xNewName = oForm.ContentName;
                                                xNewDisplayName = oForm.ContentDisplayName;
                                                xNewHelpText = oForm.ContentDescription;
                                            }
                                            else
                                            {
                                                return;
                                            }
                                        }
                                        xItemMod = oUserDef.ToString(LMP.Data.ForteConstants.mpDataFileFieldSep);
                                        //Create UserSegmentDef with new ID
                                        UserSegmentDef oNewDef = (UserSegmentDef)oUserDefs.Create();
                                        StringIDSimpleDataItem oItem = null;
                                        int iTries = 0;
                                        //Retry if duplicate ID was generated
                                        do
                                        {
                                            oItem = null;
                                            oNewDef = (UserSegmentDef)oUserDefs.Create();
                                            try
                                            {
                                                oItem = oUserDefs.ItemFromID(oNewDef.ID);
                                            }
                                            catch { }
                                            iTries++;
                                        } while (iTries < 10 && oItem != null);
                                        oNewDef.Name = xNewName;
                                        oNewDef.DisplayName = xNewDisplayName;
                                        oNewDef.OwnerID = Session.CurrentUser.ID;
                                        oNewDef.ChildSegmentIDs = oUserDef.ChildSegmentIDs;
                                        oNewDef.DefaultDoubleClickBehavior = oUserDef.DefaultDoubleClickBehavior;
                                        oNewDef.DefaultDoubleClickLocation = oUserDef.DefaultDoubleClickLocation;
                                        oNewDef.DefaultDragBehavior = oUserDef.DefaultDragLocation;
                                        oNewDef.DefaultMenuInsertionBehavior = oUserDef.DefaultMenuInsertionBehavior;
                                        oNewDef.HelpText = xNewHelpText;
                                        oNewDef.IntendedUse = oUserDef.IntendedUse;
                                        oNewDef.L0 = oUserDef.L0;
                                        oNewDef.L1 = oUserDef.L1;
                                        oNewDef.L2 = oUserDef.L2;
                                        oNewDef.L3 = oUserDef.L3;
                                        oNewDef.L4 = oUserDef.L4;
                                        oNewDef.MenuInsertionOptions = oNewDef.MenuInsertionOptions;
                                        oNewDef.TypeID = oUserDef.TypeID;
                                        oNewDef.IsDirty = true;
                                        //Save to generate new ID
                                        oUserDefs.Save((StringIDSimpleDataItem)oNewDef);
                                        string xNewID = oNewDef.ID;
                                        oNewDef.XML = LMP.String.ReplaceSegmentXML(oUserDef.XML, oUserDef.ID, xNewID, oUserDef.DisplayName,
                                            xNewDisplayName, oUserDef.Name, xNewName, "");
                                        xItemMod = oNewDef.ToString(LMP.Data.ForteConstants.mpDataFileFieldSep);
                                    }
                                    string xNewUserDefID = ImportUserSegmentDefAsUserDef(xItemMod, oFolderNode, bReplaceExisting && aExistingSegments.Contains(xOrigName));
                                    bSegmentImported = (xNewUserDefID != "") && (xNewUserDefID != xUserDefID);
                                    if (bSegmentImported)
                                    {
                                        xConvertedIDs += xUserDefID + "=" + xNewUserDefID + ",";
                                    }
                                }

                                break;
                            }
                        case "Assignments":
                            {
                                if (bSegmentImported)
                                {
                                    if (xConvertedIDs != "")
                                    {
                                        //replace old ID with new ID
                                        string[] aConvertedIDs = xConvertedIDs.Split(new char[] { ',' },
                                            StringSplitOptions.RemoveEmptyEntries);

                                        foreach (string xConvertedID in aConvertedIDs)
                                        {
                                            int iPos = xConvertedID.IndexOf("=");
                                            string xOldID = xConvertedID.Substring(0, iPos);
                                            string xNewID = xConvertedID.Substring(iPos + 1);

                                            //GLOG 5238: zzTEMP marker is used to avoid replacing IDs that have already been replaced 
                                            //on a previous pass when there is overlap between the old and new IDs
                                            xItemMod = xItemMod.Replace("|" + xOldID, "|zzTEMP" + xNewID);
                                        }
                                        xItemMod = xItemMod.Replace("zzTEMP", "");
                                    }
                                    //segment was imported - import assignments
                                    ImportAssignments(xItemMod);
                                }
                                break;
                            }
                        case "Preferences":
                            {
                                if (bSegmentImported)
                                {
                                    if (xConvertedIDs != "")
                                    {
                                        //replace old ID with new ID
                                        string[] aConvertedIDs = xConvertedIDs.Split(new char[] { ',' },
                                            StringSplitOptions.RemoveEmptyEntries);

                                        foreach (string xConvertedID in aConvertedIDs)
                                        {
                                            int iPos = xConvertedID.IndexOf("=");
                                            string xOldID = xConvertedID.Substring(0, iPos);
                                            string xNewID = xConvertedID.Substring(iPos + 1);

                                            //GLOG 5238: zzTEMP marker is used to avoid replacing IDs that have already been replaced 
                                            //on a previous pass when there is overlap between the old and new IDs
                                            xItemMod = xItemMod.Replace(xOldID + ForteConstants.mpDataFileFieldSep, "zzTEMP" + xNewID + ForteConstants.mpDataFileFieldSep);
                                        }
                                        xItemMod = xItemMod.Replace("zzTEMP", "");
                                    }
                                    if (bReplacePreferences || bFirstPrefSegment)
                                    {
                                        //Prompt only once to import Preferences
                                        ImportAuthorPreferences(xItemMod, ref bReplacePreferences, bFirstPrefSegment);
                                    }
                                    bFirstPrefSegment = false;
                                }
                                break;
                            }
                    }
                }
                if (oFolderNode.Tag is UserFolder)
                    RefreshUserFolderNode(oFolderNode, true);
                else
                    RefreshAdminFolderNode(oFolderNode, true);
            }
        }

        private void ImportAuthorPreferences(string xKeysets, ref bool bReplace, bool bPrompt)
        {
            //split into items to import
            string[] aParts = xKeysets.Split(new string[]{
                    LMP.Data.ForteConstants.mpDataFileFieldSep}, StringSplitOptions.RemoveEmptyEntries);
            string xSegmentID = aParts[0];
            string[] aRecords = aParts[1].Split(new char[] { StringArray.mpEndOfSubField }, StringSplitOptions.RemoveEmptyEntries);

            if (bPrompt)
            {
                string xSQL = "SELECT OwnerID1 FROM Keysets WHERE ScopeID1=" + xSegmentID
                                    + " AND OwnerID2=" + ForteConstants.mpFirmRecordID.ToString() + " AND [Type]=" + ((int)mpKeySetTypes.AuthorPref).ToString();
                object oRet = LMP.Data.SimpleDataCollection.GetScalar(xSQL);
                if (oRet != null)
                {
                    DialogResult iDR = MessageBox.Show("Overwrite existing Author Preferences?", "Import Segment", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (iDR != DialogResult.Yes)
                    {
                        bReplace = false;
                        return;
                    }
                    else
                        bReplace = true;
                }
            }
            foreach (string xRecord in aRecords)
            {
                string[] aKeyset = xRecord.Split(new char[] { StringArray.mpEndOfSubValue });
                int iOwnerID = int.Parse(aKeyset[0]);
                string xValue = aKeyset[1];
                int iCulture = int.Parse(aKeyset[2]);
                KeySet oNewPref = null;
                try
                {
                    oNewPref = KeySet.GetPreferenceSet(mpPreferenceSetTypes.Author, xSegmentID, iOwnerID, 0, iCulture);
                }
                catch { }
                if (oNewPref == null)
                {
                    KeySet.AddAuthorPrefRecord(xSegmentID, iOwnerID.ToString(), iCulture, new ArrayList());
                    oNewPref = KeySet.GetPreferenceSet(mpPreferenceSetTypes.Author, xSegmentID, iOwnerID, 0, iCulture);
                }
                else if (!bReplace)
                {
                    return;
                }
                string[] aFields = xValue.Split(new char[] {StringArray.mpEndOfField}, StringSplitOptions.RemoveEmptyEntries);
                foreach (string xField in aFields)
                {
                    string[] aVals = xField.Split(new char[] {StringArray.mpEndOfValue});
                    //GLOG 15985: Skip any entries that don't have both field and value
                    if (aVals.GetUpperBound(0) == 1)
                        oNewPref.SetValue(aVals[0], aVals[1]);
                }
                oNewPref.Save();
            }
        }
        private void ImportAssignments(string xAssignments)
        {
            if (xAssignments == "")
                return;

            string[] aAssignments = xAssignments.Split('|');
            mpObjectTypes iTargetObjectTypeID = (mpObjectTypes) int.Parse(aAssignments[0]);

            //delete existing assignments
            int iTargetObjectID = int.Parse(aAssignments[1]);

            Assignments oExistingAssignments = new Assignments(iTargetObjectTypeID, iTargetObjectID, 0);
            for (int i = oExistingAssignments.Count - 1; i>=0; i--)
            {
                Assignment oA = (Assignment)oExistingAssignments.ItemFromIndex(i);
                Assignments.Delete(oA.TargetObjectType, oA.TargetObjectID,
                    oA.AssignedObjectType, oA.AssignedObjectID);
            }

            //add new assignments
            for (int i = 0; i < aAssignments.Length; i += 4)
            {
                int iObjectType = int.Parse(aAssignments[i + 2]);
                int iObjectID = int.Parse(aAssignments[i + 3]);
                if (iObjectType != 100 && iObjectType != 101)
                {
                    //GLOG 5238: Only add Assignment if Segment matching AssignedObjectType and AssignedObjectID exists
                    AdminSegmentDefs oDefs = new AdminSegmentDefs((mpObjectTypes)iObjectType);
                    AdminSegmentDef oDef = null;
                    try
                    {
                        oDef = (AdminSegmentDef)oDefs.ItemFromID(iObjectID);
                    }
                    catch 
                    {
                        continue;
                    }

                }
                Assignments.Add((mpObjectTypes)int.Parse(aAssignments[i]), 
                    int.Parse(aAssignments[i + 1]),
                    (mpObjectTypes)int.Parse(aAssignments[i + 2]), 
                    int.Parse(aAssignments[i + 3]));
            }
        }

        private void ImportAdminSegmentDef(string xSegmentDef, UltraTreeNode oFolderNode, bool bAssignToFolder)
        {
            AdminSegmentDefs oDefs = new AdminSegmentDefs();

            //get segment def object
            AdminSegmentDef oDef = AdminSegmentDef.FromString(xSegmentDef);

            //get ID of segment from file
            int iID = oDef.ID;

            //check that children of this segment already exist in the db
            string xChildIDs = oDef.ChildSegmentIDs;
            string[] aChildIDs = xChildIDs.Split('|');

            if (xChildIDs != "")
            {
                foreach (string xID in aChildIDs)
                {
                    string xChildID = xID;
                    //GLOG 6966: Make sure string can be parsed as a Double even if
                    //the Decimal separator is something other than '.' in Regional Settings
                    if (xChildID.EndsWith(".0"))
                    {
                        xChildID = xChildID.Replace(".0", "");
                    }
                    //ensure that segment exists in db
                    int iChildID = 0;
                    try
                    {
                        iChildID = (int)double.Parse(xChildID);
                    }
                    catch { }
                    bool bChildIDExists = false;
                    if (iChildID != 0)
                    {
                        try
                        {
                            if (oDefs.ItemFromID(iChildID) != null)
                                bChildIDExists = true;

                        }
                        catch
                        {
                            bChildIDExists = false;
                        }
                    }

                    if (iChildID == 0 || !bChildIDExists)
                    {
                        //TODO: needs a CC analog
                        //child segment does not exist 
                        //or is a user segment - check to see
                        //if a segment with the same name as the child
                        //exists as an admin segment - if so, modify the IDs-
                        //get name of segment with this ID
                        string xXML = oDef.XML;
                        XmlDocument oXML = new XmlDocument();
                        oXML.LoadXml(xXML);
                        string xNamespacePrefix = String.GetNamespacePrefix(
                            xXML, ForteConstants.MacPacNamespace);
                        XmlNamespaceManager oNamespaceManager = new XmlNamespaceManager(
                            oXML.NameTable);
                        oNamespaceManager.AddNamespace(xNamespacePrefix, ForteConstants.MacPacNamespace);
                        string xAdminID = xID;
                        if (xAdminID.EndsWith(".0"))
                            xAdminID = xAdminID.Substring(0, xAdminID.Length - 2);
                        string xXPath = @"//" + xNamespacePrefix +
                            @":mSEG[contains(@ObjectData,'SegmentID=" + xAdminID + "|')]";
                        XmlNode oNode = oXML.SelectSingleNode(xXPath, oNamespaceManager);
                        string xChildName = "";
                        if (oNode != null)
                        {
                            string xObjectData = oNode.Attributes["ObjectData"].InnerText;
                            int iPos = xObjectData.IndexOf("|Name=");
                            int iPos2 = xObjectData.IndexOf('|', iPos + 1);
                            xChildName = xObjectData.Substring(
                                iPos + "|Name=".Length, iPos2 - (iPos + "|Name=".Length));
                            if (AdminSegmentDefs.Exists(xChildName))
                            {
                                //JTS 7/13/10: Child Segment exists but with a different ID -
                                //Update XML to reflect correct value
                                int iNewID = AdminSegmentDefs.GetIDFromName(xChildName);
                                //GLOG 5308: Segment ID can also appear as part of ContentControl tag, preceded by '0's padded to 19 chars
                                oDef.XML = oDef.XML.Replace(xAdminID.PadLeft(19, '0') + "0", string.Concat("zzmpCCTemp", iNewID.ToString()).PadLeft(29, '0') + "0");
                                oDef.XML = oDef.XML.Replace("SegmentID=" + xAdminID + "|", "SegmentID=zzTEMP" + iNewID.ToString() + "|");
                                //Also update ChildSegmentIDs value - ID may be preceded or followed by Pipe character, or could be a single ID 
                                oDef.ChildSegmentIDs = oDef.ChildSegmentIDs.Replace("|" + xAdminID + ".0", "|zzTEMP" + iNewID.ToString() + ".0");
                                oDef.ChildSegmentIDs = oDef.ChildSegmentIDs.Replace(xAdminID + ".0|", "zzTEMP" + iNewID.ToString() + ".0|");
                                if (oDef.ChildSegmentIDs == xAdminID + ".0")
                                    oDef.ChildSegmentIDs = iNewID.ToString() + ".0";
                            }
                        }
                        //check admin db for existence of a segment with that name
                        if (oNode != null && !AdminSegmentDefs.Exists(xChildName))
                        {
                            MessageBox.Show(
                                LMP.Resources.GetLangString("Msg_CouldNotImportSegmentChildSegmentDoesNotExist" + xChildName),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);

                            return;
                        }
                    }
                }
                oDef.XML = oDef.XML.Replace("SegmentID=zzTEMP", "SegmentID=");
                //GLOG 5308: Remove temp markers placed in ContentControl tags
                oDef.XML = oDef.XML.Replace("zzmpCCTemp", "");
                oDef.ChildSegmentIDs = oDef.ChildSegmentIDs.Replace("zzTEMP", "");
            }

            //prepend <mAuthorPrefs> code if the segment contains author preferences-
            //this tag allows us to display in the Author Preferences Manager only
            //those segments that contain author prefs
            bool bContainsAuthorPrefs = oDef.XML.ToUpper().Contains("[AUTHORPREFERENCE_") ||
                oDef.XML.ToUpper().Contains("[LEADAUTHORPREFERENCE_") ||
                oDef.XML.ToUpper().Contains("[AUTHORPARENTPREFERENCE") ||
                oDef.XML.ToUpper().Contains("[LEADAUTHORPARENTPREFERENCE_");

            if (bContainsAuthorPrefs)
                oDef.XML = "<mAuthorPrefs>" + oDef.XML;

            //import segment record
            oDefs.Save(oDef);
            if (bAssignToFolder)
            {
                Folder oFolder = (Folder)oFolderNode.Tag;
                FolderMember oMember = (FolderMember)(oFolder.GetMembers().Create());
                oMember.ObjectID1 = oDef.ID;
                oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);
            }
        }

        /// <summary>
        /// imports the specified user segment data as an admin segment-
        /// adds segment to the specified folder - returns the ID of
        /// the new AdminSegmentDef that is created
        /// </summary>
        /// <param name="xSegmentDef"></param>
        /// <param name="oFolderNode"></param>
        private int ImportUserSegmentDefAsAdminDef(string xSegmentDef, UltraTreeNode oFolderNode, bool bReplaceExisting)
        {
            int iExistingDef = 0;

            //get segment def object
            UserSegmentDef oUserDef = UserSegmentDef.FromString(xSegmentDef);

            if (bReplaceExisting)
            {
                //replace existing admin segment def, if it exists-
                //set ID of admin segment
                iExistingDef = AdminSegmentDefs.GetIDFromName(oUserDef.Name);
            }

            //don't allow save if a segment designer is attempting
            //to add a new segment to something other than a private folder
            if (!(oFolderNode.Tag is UserFolder) && iExistingDef == 0 &&
                !Session.AdminMode)
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_CantAddUserSegmentToAdminFolder"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return 0;
            }

            AdminSegmentDefs oDefs = new AdminSegmentDefs();
            AdminSegmentDef oAdminDef = oUserDef.ConvertToAdminSegmentDef(iExistingDef, false);

            //import segment record
            oDefs.Save(oAdminDef);

            //change ID of segment in XML to match new admin segment ID
            //JTS 11/26/10: Include SegmentID= as part of replacement string, to avoid accidental matching of ID string in other contexts
            oAdminDef.XML = oAdminDef.XML.Replace("SegmentID=" + oUserDef.ID, "SegmentID=" + oAdminDef.ID.ToString()); 
            //GLOG 5308: Segment ID can also appear as part of ContentControl tag, with decimal point replaced with 'd'
            oAdminDef.XML = oAdminDef.XML.Replace(oUserDef.ID.Replace('.', 'd').PadLeft(19, '0') + "0", oAdminDef.ID.ToString().PadLeft(19, '0') + "0");
            //change typeid from usersegment to architect, if necessary
            oAdminDef.XML = oAdminDef.XML.Replace("ObjectTypeID=99", "ObjectTypeID=4");

            //prepend <mAuthorPrefs> code if the segment contains author preferences-
            //this tag allows us to display in the Author Preferences Manager only
            //those segments that contain author prefs
            bool bContainsAuthorPrefs = oAdminDef.XML.ToUpper().Contains("[AUTHORPREFERENCE_") ||
                oAdminDef.XML.ToUpper().Contains("[LEADAUTHORPREFERENCE_") ||
                oAdminDef.XML.ToUpper().Contains("[AUTHORPARENTPREFERENCE") ||
                oAdminDef.XML.ToUpper().Contains("[LEADAUTHORPARENTPREFERENCE_");

            if (bContainsAuthorPrefs)
                oAdminDef.XML = "<mAuthorPrefs>" + oAdminDef.XML;

            oDefs.Save(oAdminDef);

            //add to selected folder
            ContentTreeHelper.LoadNodeReference(oFolderNode);
            if (!ContentTreeHelper.IsAdminFolderNode(oFolderNode))
                return 0;

            if (iExistingDef == 0)
            {
                //create and select tree node
                Folder oFolder = (Folder)oFolderNode.Tag;

                FolderMember oMember = (FolderMember)(oFolder.GetMembers().Create());
                oMember.ObjectID1 = oAdminDef.ID;
                oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);

                RefreshAdminFolderNode(oFolderNode, true);

                UltraTreeNode oNewNode = this.treeContent.GetNodeByKey(
                    string.Concat("A", oFolderNode.Key, mpPathSeparator, oMember.ID));
                this.treeContent.ActiveNode = oNewNode;

                oNewNode.Selected = true;
            }

            return oAdminDef.ID;
        }

        /// <summary>
        /// imports the specified user segment data as an admin segment-
        /// adds segment to the specified folder - returns the ID of
        /// the new AdminSegmentDef that is created
        /// </summary>
        /// <param name="xSegmentDef"></param>
        /// <param name="oFolderNode"></param>
        private string ImportUserSegmentDefAsUserDef(string xSegmentDef, UltraTreeNode oFolderNode, bool bReplaceExisting)
        {
            string xExistingDef = "";

            //get segment def object
            UserSegmentDef oUserDef = UserSegmentDef.FromString(xSegmentDef);

            UserSegmentDefs oDefs = new UserSegmentDefs(mpUserSegmentsFilterFields.Owner, Session.CurrentUser.ID) ;

            if (bReplaceExisting)
            {
                xExistingDef = oUserDef.ID;
            }

            //don't allow save if a segment designer is attempting
            //to add a new segment to something other than a private folder
            if (!(oFolderNode.Tag is UserFolder))
            {
                MessageBox.Show(LMP.Resources.GetLangString("Msg_CantAddUserSegmentToAdminFolder"),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return "";
            }

            //import segment record
            oDefs.Save(oUserDef);

            if (xExistingDef != "" && oUserDef.ID != xExistingDef)
            {
                //change ID of segment in XML to match new admin segment ID
                //JTS 11/26/10: Include SegmentID= as part of replacement string, to avoid accidental matching of ID string in other contexts
                oUserDef.XML = oUserDef.XML.Replace("SegmentID=" + xExistingDef, "SegmentID=" + oUserDef.ID);
                //GLOG 5308: Segment ID can also appear as part of ContentControl tag, with decimal point replaced with 'd'
                oUserDef.XML = oUserDef.XML.Replace(xExistingDef.Replace('.', 'd').PadLeft(19, '0') + "0", oUserDef.ID.ToString().PadLeft(19, '0') + "0");
                oDefs.Save(oUserDef);
            }

            //add to selected folder
            ContentTreeHelper.LoadNodeReference(oFolderNode);

            if (xExistingDef == "")
            {
                //create and select tree node
                UserFolder oFolder = (UserFolder)oFolderNode.Tag;
                int ID1 = 0;
                int ID2 = 0;
                Data.Application.SplitID(oUserDef.ID, out ID1, out ID2);
                FolderMember oMember = (FolderMember)(oFolder.GetMembers().Create());
                oMember.ObjectID1 = ID1;
                oMember.ObjectID2 = ID2;
                oMember.ObjectTypeID = mpFolderMemberTypes.UserSegment;
                oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);

                RefreshUserFolderNode(oFolderNode, true);

                UltraTreeNode oNewNode = this.treeContent.GetNodeByKey(
                    string.Concat("U", oFolderNode.Key, mpPathSeparator, oMember.ID));
                this.treeContent.ActiveNode = oNewNode;

                oNewNode.Selected = true;
            }

            return oUserDef.ID;
        }

        private void ChangeParentNode(UltraTreeNode oNewParentNode, UltraTreeNode oMovingNode, DragDropEffects oEffect, IDataObject oData)
        {
            if (oNewParentNode != null)
            {
                ContentTreeHelper.LoadNodeReference(oNewParentNode);

                if (ContentTreeHelper.IsUserSegmentNode(oNewParentNode) || ContentTreeHelper.IsAdminSegmentNode(oNewParentNode) || ContentTreeHelper.IsVariableSetNode(oNewParentNode))
                {
                    // Node is a segment, so move to parent folder
                    oNewParentNode = oNewParentNode.Parent;
                }
                
                //check that expected data is present
                if (oMovingNode == null)
                {
                    if (((oEffect & DragDropEffects.Link) == DragDropEffects.Link) &&
                    oData.GetDataPresent("Rich Text Format"))
                    {
                        CreateNewSegment(oNewParentNode, NewSegmentItems.SegmentFromSelection);
                    }
                }
                else if (ContentTreeHelper.IsUserFolderNode(oNewParentNode))
                {
                    //Duplicate names are not prevented by table structure
                    //So check FolderMembers directly for duplicates
                    //GLOG : 3893 : CEH
                    if (ContentTreeHelper.IsUserSegmentNode(oMovingNode))
                    {
                        UserFolder oUserFolder = (UserFolder)oNewParentNode.Tag;
                        FolderMembers oUserMembers = oUserFolder.GetMembers();
                        FolderMember oMember = (FolderMember)oMovingNode.Tag;

                        for (int i = 1; i <= oUserMembers.Count; i++)
                        {
                            string xMemberName = ((FolderMember)oUserMembers[i]).Name;
                            if (xMemberName.ToUpper() == oMember.Name.ToUpper())
                            {
                                DialogResult iChoice = MessageBox.Show(
                                    LMP.Resources.GetLangString("ContentPropertyForm_OverwriteExistingSegment"), 
                                    LMP.ComponentProperties.ProductName, 
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (iChoice == DialogResult.No)
                                    return;
                                // Delete the existing node so that it may be replaced.
                                oUserMembers.Delete(((FolderMember)oUserMembers[i]).ID);
                            }
                        }
                    }

                    int iID1 = 0;
                    int iID2 = 0;

                    if (oNewParentNode.Tag is UserFolder)
                        LMP.String.SplitID(((UserFolder)oNewParentNode.Tag).ID, out iID1, out iID2);

                    if (ContentTreeHelper.IsUserSegmentNode(oMovingNode) || ContentTreeHelper.IsAdminSegmentNode(oMovingNode) || ContentTreeHelper.IsVariableSetNode(oMovingNode))
                    {
                        if (oEffect == DragDropEffects.Copy)
                        {
                            // Copy if Ctrl key is down, or if move is not allowed
                            CopySegmentNode(oMovingNode, false);
                            PasteSegmentNode(oNewParentNode);
                        }
                        else
                        {
                            // Otherwise, move segment
                            FolderMember oMember = (FolderMember)oMovingNode.Tag;
                            FolderMembers oMembers = new FolderMembers(0, 0);
                            oMember.FolderID1 = iID1;
                            oMember.FolderID2 = iID2;
                            oMembers.Save((StringIDSimpleDataItem)oMember);
                            // Delete original Node
                            oMovingNode.Remove();
                        }
                    }
                    else
                    {
                        UserFolder oFolder = (UserFolder)oMovingNode.Tag;
                        oFolder.ParentFolderID1 = iID1;
                        oFolder.ParentFolderID2 = iID2;
                        this.UserFolders.Save((StringIDSimpleDataItem)oFolder);
                        // Delete original Node
                        UltraTreeNode oOldParentNode = oMovingNode.Parent; //GLOG 8134
                        oMovingNode.Remove();
                        ReindexFolderNodes(oOldParentNode); //GLOG 8134
                    }

                    RefreshUserFolderNode(oNewParentNode, true);
                    ReindexFolderNodes(oNewParentNode); //GLOG 8134
                    oNewParentNode.Expanded = true;
                }
                else if (ContentTreeHelper.IsAdminFolderNode(oNewParentNode) && Session.AdminMode)
                {
                    if (ContentTreeHelper.IsAdminSegmentNode(oMovingNode))
                    {
                        // Make Copy, since Admin Folder Member Folder ID can't be updated
                        CopySegmentNode(oMovingNode, false);
                        PasteSegmentNode(oNewParentNode);
                        // Copy if Ctrl key is down, or if move is not allowed
                        if (oEffect == DragDropEffects.Move)
                            DeleteSegmentNode(oMovingNode, false);
                    }
                    else
                    {
                        Folder oFolder = (Folder)oMovingNode.Tag;
                        
                        if (oNewParentNode.Tag is Folder)
                            oFolder.ParentFolderID = ((Folder)oNewParentNode.Tag).ID;
                        else
                            oFolder.ParentFolderID = 0;
                    
                        this.AdminFolders.Save((LongIDSimpleDataItem)oFolder);
                        UltraTreeNode oOldParentNode = oMovingNode.Parent; //GLOG 8134
                        oMovingNode.Remove();
                        ReindexFolderNodes(oOldParentNode); //GLOG 8134
                        RefreshAdminFolderNode(oNewParentNode, true);
                        ReindexFolderNodes(oNewParentNode); //GLOG 8134
                    }

                    oNewParentNode.Expanded = true;
                }
            }
        }

        /// <summary>
        /// Set the index value of specified Node after or before another Node, depending on direction
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="iDirection"></param>
        private void MoveFolderNode(UltraTreeNode oFixedNode, UltraTreeNode oMovingNode, FolderMoveDirections iDirection)
        {
            if (ContentTreeHelper.IsUserFolderNode(oMovingNode) || ContentTreeHelper.IsAdminFolderNode(oMovingNode))
            {
                ContentTreeHelper.LoadNodeReference(oMovingNode);

                if (ContentTreeHelper.IsTopLevelNode(oMovingNode))
                    return;

                //Already at top or bottom of order
                if (oFixedNode == null)
                    return;

                ContentTreeHelper.LoadNodeReference(oFixedNode);

                if (ContentTreeHelper.IsAdminFolderNode(oFixedNode))
                {
                    try
                    {
                        if (oMovingNode.Tag is Folder)
                        {
                            Folder oMovingFolder = (Folder)oMovingNode.Tag;
                            Folder oFixedFolder = (Folder)oFixedNode.Tag;
                            UltraTreeNode oOldParentNode = null; //GLOG 8134
                            if (oMovingFolder.ParentFolderID != oFixedFolder.ParentFolderID)
                            {
                                //GLOG 8134: Reindexing of folders will be handled at end
                                oOldParentNode = oMovingNode.Parent;
                                //UltraTreeNode oMovingNodeSiblingNode = oMovingNode.GetSibling(NodePosition.Next);

                                //while (oMovingNodeSiblingNode != null)
                                //{
                                //    if (oMovingNodeSiblingNode.Tag is Folder)
                                //    {
                                //        ContentTreeHelper.LoadNodeReference(oMovingNodeSiblingNode);
                                //        ((Folder)oMovingNodeSiblingNode.Tag).Index--;
                                //        this.AdminFolders.Save(((Folder)oMovingNodeSiblingNode.Tag));
                                //    }

                                //    oMovingNodeSiblingNode = oMovingNodeSiblingNode.GetSibling(NodePosition.Next);
                                //}

                                // Make the moving folder a sibling of the fixed folder by 
                                // assigning it the fixed folder's parent folder's id's.
                                oMovingFolder.ParentFolderID = oFixedFolder.ParentFolderID;
								//GLOG 8134
                                // Save the moving folder
                                this.AdminFolders.Save(oMovingFolder);
                            }

                            //GLOG 8134:  Reindexing of folders will be handled at the end
                            //UltraTreeNode oSiblingNode = null;

                            //switch (iDirection)
                            //{
                            //    case FolderMoveDirections.MoveDown:
                            //        // Set the moving folder's index to be the subsequent index after the fixed folder's index.
                            //        oMovingFolder.Index = oFixedFolder.Index + 1;
                            //        oSiblingNode = oFixedNode.GetSibling(NodePosition.Next);
                            //        break;

                            //    case FolderMoveDirections.MoveUp:
                            //        // Set the moving folder's index to be the index 
                            //        // of the fixed folder's index.
                            //        oMovingFolder.Index = oFixedFolder.Index;
                            //        oSiblingNode = oFixedNode;
                            //        break;
                            //}

                            //int iPreviousFolderIndex = oMovingFolder.Index;


                            //// Adjust the index of all the folders that follow the fixed node such that
                            //// they are after the newly inserted moving folder's node.
                            //while (oSiblingNode != null)
                            //{
                            //    if (oSiblingNode != oMovingNode)
                            //    {
                            //        ContentTreeHelper.LoadNodeReference(oSiblingNode);

                            //        if (oSiblingNode.Tag is Folder)
                            //        {
                            //            ((Folder)(oSiblingNode.Tag)).Index = ++iPreviousFolderIndex;
                            //            this.AdminFolders.Save(((Folder)(oSiblingNode.Tag)));
                            //        }
                            //    }

                            //    oSiblingNode = oSiblingNode.GetSibling(NodePosition.Next);
                            //}

                            //Now move actual node
                            oMovingNode.Reposition(oFixedNode, (NodePosition)iDirection);
                            //GLOG 8134: Reindex source and target folder collections
                            ReindexFolderNodes(oFixedNode.Parent);
                            if (oOldParentNode != null)
                            {
                                ReindexFolderNodes(oOldParentNode);
                            }
                        }
                    }
                    catch (System.Exception oE)
                    {
                        LMP.Error.Show(oE);
                    }
                }
                else if (ContentTreeHelper.IsUserFolderNode(oFixedNode))
                {
                    try
                    {
                        if (oMovingNode.Tag is UserFolder)
                        {
                            UserFolder oMovingFolder = (UserFolder)oMovingNode.Tag;
                            UserFolder oFixedFolder = (UserFolder)oFixedNode.Tag;
                            //GLOG 8134
                            UltraTreeNode oOldParentNode = null;
                            if (oMovingFolder.ParentFolderID1 != oFixedFolder.ParentFolderID1 ||
                                oMovingFolder.ParentFolderID2 != oFixedFolder.ParentFolderID2)
                            {
                                //GLOG 8134: Reindexing will be handled below
                                oOldParentNode = oMovingNode.Parent;
                                //// Since we are removing the moving folder from another parent 
                                //// folder, adjust the indexes of its sibling folders.
                                //UltraTreeNode oMovingNodeSiblingNode = oMovingNode.GetSibling(NodePosition.Next);

                                //while (oMovingNodeSiblingNode != null)
                                //{
                                //    if (oMovingNodeSiblingNode.Tag is UserFolder)
                                //    {
                                //        ContentTreeHelper.LoadNodeReference(oMovingNodeSiblingNode);
                                //        ((UserFolder)oMovingNodeSiblingNode.Tag).Index--;
                                //        this.UserFolders.Save(((UserFolder)oMovingNodeSiblingNode.Tag));
                                //    }
                                //    oMovingNodeSiblingNode = oMovingNodeSiblingNode.GetSibling(NodePosition.Next);
                                //}

                                // Make the moving folder a sibling of the fixed folder by 
                                // assigning it the fixed folder's parent folder's id's.
                                oMovingFolder.ParentFolderID1 = oFixedFolder.ParentFolderID1;
                                oMovingFolder.ParentFolderID2 = oFixedFolder.ParentFolderID2;
                                //GLOG 8134
                                // Save the moving folder
                                this.UserFolders.Save(oMovingFolder);
                            }
                            //GLOG 8134:  Reindexing of folders will be handled at the end
                            //UltraTreeNode oSiblingNode = null;

                            //switch (iDirection)
                            //{
                            //    case FolderMoveDirections.MoveDown:
                            //        // Set the moving folder's index to be the subsequent index after the fixed folder's index.
                            //        oMovingFolder.Index = oFixedFolder.Index + 1;
                            //        oSiblingNode = oFixedNode.GetSibling(NodePosition.Next);
                            //        break;

                            //    case FolderMoveDirections.MoveUp:
                            //        // Set the moving folder's index to be the index 
                            //        // of the fixed folder's index.
                            //        oMovingFolder.Index = oFixedFolder.Index;
                            //        oSiblingNode = oFixedNode;
                            //        break;
                            //}

                            //int iPreviousFolderIndex = oMovingFolder.Index;


                            //// Adjust the index of all the folders that follow the fixed node such that
                            //// they are after the newly inserted moving folder's node.
                            //while (oSiblingNode != null)
                            //{
                            //    if (oSiblingNode.Tag is UserFolder)
                            //    {
                            //        if (oSiblingNode != oMovingNode)
                            //        {
                            //            ContentTreeHelper.LoadNodeReference(oSiblingNode);
                            //            ((UserFolder)(oSiblingNode.Tag)).Index = ++iPreviousFolderIndex;
                            //            this.UserFolders.Save(((UserFolder)(oSiblingNode.Tag)));
                            //        }
                            //    }
                            //    else
                            //        //GLOG 8134: end of User Folder nodes
                            //        break;

                            //    oSiblingNode = oSiblingNode.GetSibling(NodePosition.Next);
                            //}

                            //Now move actual node
                            oMovingNode.Reposition(oFixedNode, (NodePosition)iDirection);
                            //GLOG 8134: Reindex source and target folder collections
                            ReindexFolderNodes(oFixedNode.Parent);
                            if (oOldParentNode != null)
                            {
                                ReindexFolderNodes(oOldParentNode);
                            }
                        }
                    }
                    catch (System.Exception oE)
                    {
                        LMP.Error.Show(oE);
                    }
                }
            }
        }

        /// <summary>
        /// Switch index value of selected Node and next or previous Node, depending on direction
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="iDirection"></param>
        private void ReorderFolderNodes(UltraTreeNode oNode, FolderMoveDirections iDirection)
        {
            ContentTreeHelper.LoadNodeReference(oNode);
            if (ContentTreeHelper.IsTopLevelNode(oNode))
                return;
            UltraTreeNode oOtherNode = null;

            if (iDirection == FolderMoveDirections.MoveDown)
                oOtherNode = oNode.GetSibling(NodePosition.Next);
            else if (iDirection == FolderMoveDirections.MoveUp)
                oOtherNode = oNode.GetSibling(NodePosition.Previous);

            //Already at top or bottom of order
            if (oOtherNode == null)
                return;
            ContentTreeHelper.LoadNodeReference(oOtherNode);

            if (ContentTreeHelper.IsAdminFolderNode(oOtherNode))
            {
                try
                {
                    //GLOG 8134:  Starting indices may not be sequential
                    //Instead of setting individually, reindex entire collection
                    ////Swap Index with other node
                    //Folder oCurrentFolder = (Folder)oNode.Tag;
                    //Folder oOtherFolder = (Folder)oOtherNode.Tag;
                    //int iSaveIndex = oCurrentFolder.Index;
                    //oCurrentFolder.Index = oOtherFolder.Index;
                    //oOtherFolder.Index = iSaveIndex;
                    ////Save both objects
                    //this.AdminFolders.Save(oCurrentFolder);
                    //this.AdminFolders.Save(oOtherFolder);
                    //Now move actual node
                    oNode.Reposition(oOtherNode, (NodePosition)iDirection);
                    //GLOG 8134
                    ReindexFolderNodes(oOtherNode.Parent);
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
            else if (ContentTreeHelper.IsUserFolderNode(oOtherNode))
            {
                try
                {
                    //GLOG 8134:  Starting indices may not be sequential
                    //Instead of setting individually, reindex entire collection
                    ////Swap Index with other node
                    //UserFolder oCurrentFolder = (UserFolder)oNode.Tag;
                    //UserFolder oOtherFolder = (UserFolder)oOtherNode.Tag;
                    //int iSaveIndex = oCurrentFolder.Index;
                    //oCurrentFolder.Index = oOtherFolder.Index;
                    //oOtherFolder.Index = iSaveIndex;
                    ////Save both objects
                    //this.UserFolders.Save(oCurrentFolder);
                    //this.UserFolders.Save(oOtherFolder);
                    //Now move actual node
                    oNode.Reposition(oOtherNode, (NodePosition)iDirection);
                    //GLOG 8134
                    ReindexFolderNodes(oOtherNode.Parent);
                }
                catch (System.Exception oE)
                {
                    LMP.Error.Show(oE);
                }
            }
        }
        /// <summary>
        /// populates the supplied list with a list of expanded node keys
        /// </summary>
        /// <param name="oList"></param>
        /// <param name="oNodes"></param>
        private void GetExpandedNodes(List<string> oList, TreeNodesCollection oNodes)
        {
            foreach (UltraTreeNode oNode in oNodes)
            {
                if (oNode.Expanded)
                {
                    //add node key to list
                    oList.Add(oNode.Key);

                    //add expanded child nodes
                    GetExpandedNodes(oList, oNode.Nodes);
                }
            }
        }

        /// <summary>
        /// Get XML string to initialize xml field for Segment
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xID"></param>
        /// <param name="iTypeID"></param>
        /// <param name="xDisplay"></param>
        /// <returns></returns>
        private string GetWordXML(NewSegmentItems iType, string xID, mpObjectTypes iTypeID, 
            string xDisplayName, string xName, string xHelpText, mpSegmentIntendedUses iIntendedUse,
            short shDefaultDoubleClickLocation, int iMaxAuthors) //GLOG 8376
        {
            string xXML = "";
            string xObjectData = "";
            string xTagID = "";

            Trace.WriteNameValuePairs("iType", iType, "xID", xID, 
                "iTypeID", iTypeID, "xDisplayName", xDisplayName, "xName", xName);

            //GLOG 8347: Ensure XML does not contain rsids
            bool bStore = Session.CurrentWordApp.Options.StoreRSIDOnSave;
            Session.CurrentWordApp.Options.StoreRSIDOnSave = false;
            try
            {
                // Set TagID
                xTagID = xName + "_1";

                //get mSEG object data 
                System.Text.StringBuilder oSB = new StringBuilder();
                //GLOG 15856: Make sure HelpText is not null
                if (xHelpText == null)
                    xHelpText = "";
                //GLOG : 15819 : ceh - ensure HelpText formatting is preserved
                //GLOG 5419:  Make sure reserved XML characters are replaced by entity references in ObjectData
                oSB.AppendFormat(Segment.ADMIN_SEG_OBJECT_DATA_TEMPLATE,
                    xID, ((short)iTypeID).ToString(), xName, String.ReplaceXMLChars(xDisplayName, true), xHelpText.Replace("\r\n", "_x000d__x000a_"), (byte)iIntendedUse, 
                    iIntendedUse == mpSegmentIntendedUses.AsAnswerFile, String.ReplaceXMLChars(xDisplayName, true),  //GLOG 8463: Make sure Segment Name doesn't include quotes
                    shDefaultDoubleClickLocation.ToString(), iMaxAuthors); //GLOG 8376

                xObjectData = oSB.ToString();

                if (iTypeID == mpObjectTypes.UserSegment)
                    xObjectData = xObjectData.Replace("MaxAuthors=|", "MaxAuthors=0|");

                //get get default file format as we use this xml to create new
                //ForteDocument oMPDoc = new ForteDocument(LMP.Forte.MSWord.WordApp.ActiveDocument());
                //bool bDocFileFormat = oMPDoc.FileFormat == mpFileFormats.Binary;
                //GLOG 7008: Need to check default format for new document, not current document format
                bool bDocFileFormat = Session.CurrentWordApp.DefaultSaveFormat.ToUpper() == "DOC";

                switch (iType)
                {
                    case NewSegmentItems.SegmentBlank:
                    case NewSegmentItems.AnswerFile:
                        xXML = GetBlankSegmentXML();
                        xXML = Segment.GetFormattedNewSegmentXML(xXML,
                            xTagID, xObjectData, m_xBookmarkTag, false);
                        return xXML;
                    case NewSegmentItems.MasterDataForm:
                        xObjectData = xObjectData.Replace("MaxAuthors=|", "MaxAuthors=0|");
                        xXML = GetBlankSegmentXML();
                        xXML = Segment.GetFormattedNewSegmentXML(xXML,
                            xTagID, xObjectData, m_xBookmarkTag, false);
                        return xXML;
                    case NewSegmentItems.SegmentFromSelection:
                        if (bDocFileFormat)
                            xXML = Session.CurrentWordApp.Selection.get_XML(false);
                        else
                            xXML = Session.CurrentWordApp.Selection.WordOpenXML;
                        return Segment.GetFormattedNewSegmentXML(xXML, 
                            xTagID, xObjectData, m_xBookmarkTag, false);
                    case NewSegmentItems.SegmentFromDocument:
                        if (bDocFileFormat)
                            xXML = Session.CurrentWordApp.ActiveDocument.Content.get_XML(false);
                        else
                            xXML = Session.CurrentWordApp.ActiveDocument.Content.WordOpenXML;
                        return Segment.GetFormattedNewSegmentXML(xXML, 
                            xTagID, xObjectData, m_xBookmarkTag, true);
                    case NewSegmentItems.StyleSheetFromDocument:
                        bool bScreenUpdating = TaskPane.ScreenUpdating;
                        TaskPane.ScreenUpdating = false;

                        //Get Blank Doc XML and replace Style Definitions from current doc
                        string xDocXML = "";
                        //GLOG 5072: Source XML needs to match format of new Blank document created below,
                        //since format of style attributes is different between DOC and DOCX formats
                        if (bDocFileFormat)
                            xDocXML = Session.CurrentWordApp.ActiveDocument.Content.get_XML(false);
                        else
                            xDocXML = Session.CurrentWordApp.ActiveDocument.Content.WordOpenXML;

                        TaskPane.ScreenUpdating = true;
                        string xStyleXML = String.ExtractStyleXML(xDocXML);
                        //GLOG 5072: Extracted XML already includes <w:styles> node
                        if (xStyleXML == "")
                        {
                            xStyleXML = @"<w:styles></w:styles>";
                        }
                        string xListXML = "";
                        if (bDocFileFormat)
                        {
                            if (xStyleXML.Contains("<w:ilfo"))
                            {
                                xListXML = String.ExtractListXML(xDocXML);
                            }
                        }
                        else
                        {
                            if (xStyleXML.Contains("<w:numId "))
                            {
                                xListXML = LMP.String.ExtractListXML(xDocXML);
                            }
                        }
                        xXML = GetBlankSegmentXML();
                        //xXML = GetBlankDocFormatSegmentXML();

                        int iPkgStart = 0;

                        if(!bDocFileFormat)
                            iPkgStart = xXML.IndexOf("<pkg:part pkg:name=\"/word/styles.xml\"");

                        int iStart = xXML.IndexOf("<w:styles", iPkgStart);
                        int iEnd = xXML.IndexOf("</w:styles>", iStart) + @"</w:styles>".Length;
                        string xStartingXML = xXML.Substring(0, iStart);
                        string xEndingXML = xXML.Substring(iEnd);
                        string xNewXML = "";
                        if (bDocFileFormat)
                            xNewXML = xStartingXML + xListXML + xStyleXML + xEndingXML;
                        else
                        {
                            //Replace existing Numbering section if it exists, or add new part
                            xNewXML = xStartingXML + xStyleXML + xEndingXML;

                            if (!string.IsNullOrEmpty(xListXML))
                            {
                                int iNumPos1 = xNewXML.IndexOf("<w:numbering");
                                if (iNumPos1 > -1)
                                {
                                    int iNumPos2 = xNewXML.IndexOf("</w:numbering>", iNumPos1);
                                    if (iNumPos2 > -1)
                                    {
                                        xNewXML = xNewXML.Substring(0, iNumPos1) + xListXML + xNewXML.Substring(iNumPos2 + @"</w:numbering>".Length);
                                    }
                                }
                                else
                                {
                                    iNumPos1 = xNewXML.LastIndexOf("</pkg:package>");
                                    xNewXML = xNewXML.Substring(0, iNumPos1) +
                                        "<pkg:part pkg:name=\"/word/numbering.xml\" pkg:contentType=\"application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml\"><pkg:xmlData>"
                                        + xListXML + "</pkg:xmlData></pkg:part>" + xNewXML.Substring(iNumPos1);

                                    //GLOG 6242 (dm) - add numbering relationship
                                    xNewXML = LMP.String.AddNumberingRelationship(xNewXML);
                                }
                            }
                        }

                        return Segment.GetFormattedNewSegmentXML(xNewXML, xTagID, xObjectData, m_xBookmarkTag, false);
                    default:
                        return "";
                }
            }
            catch
            {
                return "";
            }
            finally
            {
                //GLOG 8347: Restore original rsid state
                Session.CurrentWordApp.Options.StoreRSIDOnSave = bStore;
                ForteDocument.IgnoreWordXMLEvents = ForteDocument.WordXMLEvents.None;
            }
        }

        private string GetBlankSegmentXML()
        {
            try
            {
                //GLOG 5072: Re-evalute if Default Save format has been changed during session
                //GLOG 5500: Make this test case-insensitive
                if (m_xBlankSegmentXML == null ||
                    (LMP.String.IsWordOpenXML(m_xBlankSegmentXML) && LMP.MacPac.Session.CurrentWordApp.DefaultSaveFormat.ToUpper() == "DOC") ||
                    (!LMP.String.IsWordOpenXML(m_xBlankSegmentXML) && LMP.MacPac.Session.CurrentWordApp.DefaultSaveFormat.ToUpper() != "DOC"))
                {
                    //create a new blank document to get base XML
                    Word.Document oDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                        LMP.Data.Application.BaseTemplate, false, false);

                    //JTS: Check format of created document, not one associated with TaskPane document
                    if (oDoc.SaveFormat == 0)
                        //get xml
                        m_xBlankSegmentXML = oDoc.Content.get_XML(false);
                    else
                        //get word open xml
                        m_xBlankSegmentXML = oDoc.Content.WordOpenXML;

                    object oMissing = System.Reflection.Missing.Value;
                    object oFalse = false;
                    //GLOG 6564: Set Saved=true to avoid prompt with DMS
                    oDoc.Saved = true;
                    oDoc.Close(ref oFalse, ref oMissing, ref oMissing);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordDocException(
                    LMP.Resources.GetLangString("Error_CouldNotGetBlankSegmentXML"), oE);
            }
            return m_xBlankSegmentXML;
        }

        /// <summary>
        /// returns the segment xml for a blank segment in doc format
        /// </summary>
        /// <returns></returns>
        private string GetBlankDocFormatSegmentXML()
        {
            try
            {
                if (m_xBlankDocFormatSegmentXML == null)
                {
                    //create a new blank document to get base XML
                    Word.Document oDoc = LMP.Forte.MSWord.WordApp.CreateDocument(
                        LMP.Data.Application.BaseTemplate, false, false);

                    m_xBlankDocFormatSegmentXML = oDoc.Content.get_XML(false);

                    object oMissing = System.Reflection.Missing.Value;
                    object oFalse = false;
                    //GLOG 6564: Set Saved=true to avoid prompt with DMS
                    oDoc.Saved = true;
                    oDoc.Close(ref oFalse, ref oMissing, ref oMissing);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.WordDocException(
                    LMP.Resources.GetLangString("Error_CouldNotGetBlankSegmentXML"), oE);
            }
            return m_xBlankDocFormatSegmentXML;
        }

        /// <summary>
        /// Set menu text using defined resource strings
        /// </summary>
        private void SetupMenus()
        {
            // Set Text of Segment Menu
            mnuSegment_CreateNewDocument.Text = LMP.Resources.GetLangString("Menu_Content_Segment_NewDocument");
            mnuSegment_Insert.Text = LMP.Resources.GetLangString("Menu_Content_Segment_Insert");
            mnuSegment_InsertAtSelection.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertAtSelection");
            //GLOG : 8248 : ceh
            mnuSegment_InsertAtSelectionAsPlainText.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertAtSelectionTargetFormatting");
            mnuSegment_InsertEndOfDocument.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertEndOfDocument");
            mnuSegment_InsertStartOfDocument.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertStartOfDocument");
            mnuSegment_InsertStartOfThisSection.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertStartOfThisSection");
            mnuSegment_InsertStartOfAllSections.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertStartOfAllSections");
            mnuSegment_InsertEndOfThisSection.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertEndOfThisSection");
            mnuSegment_InsertEndOfAllSections.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertEndOfAllSections");
            mnuSegment_AttachTo.Text = LMP.Resources.GetLangString("Menu_Content_Segment_AttachTo");
            mnuSegment_InsertUsingSavedPrefill.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertUsingSavedPrefill");
            mnuSegment_InsertUsingClientMatterLookup.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertUsingClientMatterLookup");
            mnuSegment_InsertMultipleusingSavedData.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertMultipleUsingSavedPrefill");
            mnuSegment_EditSavedData.Text = LMP.Resources.GetLangString("Menu_Content_Segment_EditSavedData");
            mnuSegment_InsertUsingTransientPrefill.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertUsingTransientPrefill");
            mnuSegment_Share.Text = LMP.Resources.GetLangString("Menu_Content_Segment_Share");
            mnuSegment_SelectionAsBody.Text = LMP.Resources.GetLangString("Menu_Content_Segment_SelectionAsBody");
            mnuSegment_SeparateSection.Text = LMP.Resources.GetLangString("Menu_Content_Segment_SeparateSection");
            mnuSegment_KeepHeadersFooters.Text = LMP.Resources.GetLangString("Menu_Content_Segment_KeepHeadersFooters");
            mnuSegment_RestartPageNumbering.Text = LMP.Resources.GetLangString("Menu_Content_Segment_RestartPageNumbering");
            mnuSegment_ApplyStyles.Text = LMP.Resources.GetLangString("Menu_Content_Segment_ApplyStyles");
            mnuSegment_ManageAuthorPreferences.Text = LMP.Resources.GetLangString("Menu_Content_Segment_ManageAuthorPreference");
            mnuSegment_CopyStyles.Text = LMP.Resources.GetLangString("Menu_Content_Segment_CopyStyles");
            mnuSegment_StyleSummary.Text = LMP.Resources.GetLangString("Menu_Content_Segment_StyleSummary");
            mnuSegment_Copy.Text = LMP.Resources.GetLangString("Menu_Content_Segment_Copy");
            //GLOG : 7975 : jsw
            mnuSegment_CreateMasterData.Text = LMP.Resources.GetLangString("Menu_Content_Segment_CreateMasterData");
            mnuSegment_Clone.Text = LMP.Resources.GetLangString("Menu_Content_Segment_Clone");
            mnuSegment_AddToFavorites.Text = LMP.Resources.GetLangString("Menu_Content_Segment_AddToFavorites");
            //GLOG : 7276 : jsw
            mnuSegment_RemoveFromFavorites.Text = LMP.Resources.GetLangString("Menu_Content_Segment_RemoveFromFavorites");
            mnuSegment_ShowAssignments.Text = LMP.Resources.GetLangString("Menu_Content_Segment_ShowAssignments");
            mnuSegment_ConvertToAdminContent.Text = LMP.Resources.GetLangString("Menu_Content_Segment_ConvertToAdminContent");
            mnuSegment_UpdateAdminContent.Text = LMP.Resources.GetLangString("Menu_Content_Segment_UpdateAdminContent");
            mnuSegment_Delete.Text = LMP.Resources.GetLangString("Menu_Content_Segment_Delete");
            mnuSegment_Properties.Text = LMP.Resources.GetLangString("Menu_Content_Segment_Properties");
            mnuSegment_Design.Text = LMP.Resources.GetLangString("Menu_Content_Segment_Design");
            mnuSegment_SaveAsUserSegment.Text = LMP.Resources.GetLangString("Menu_Content_Save_As_User_Segment");
            mnuSegment_Export.Text = LMP.Resources.GetLangString("Menu_Content_Export");
            mnuSegment_EditAnswerFile.Text = LMP.Resources.GetLangString("Menu_Content_Segment_EditAnswerFile");
            mnuSegment_CreateExternalContent.Text = LMP.Resources.GetLangString("mnuSegment_CreateNonMPFile");
            mnuSegment_Help.Text = LMP.Resources.GetLangString("mnuSegment_Help");
            mnuSegment_InsertUsingSelection.Text = LMP.Resources.GetLangString("mnuSegment_InsertUsingSelection");
            // Set Text of Folder Menu
            mnuFolder_CreateNewFolder.Text = LMP.Resources.GetLangString("Menu_Content_Folder_CreateNew");
            mnuFolder_CollapseAllSubfolders.Text = LMP.Resources.GetLangString("Menu_Content_Folder_CollapseAllSubfolders");
            mnuFolder_Properties.Text = LMP.Resources.GetLangString("Menu_Content_Folder_Properties");
            mnuFolder_Delete.Text = LMP.Resources.GetLangString("Menu_Content_Folder_Delete");
            mnuFolder_Refresh.Text = LMP.Resources.GetLangString("Menu_Content_Folder_Refresh");
            mnuFolder_Default.Text = LMP.Resources.GetLangString("Menu_Content_Folder_Default");
            mnuFolder_SelectDefault.Text = LMP.Resources.GetLangString("Menu_Content_Folder_SelectDefault");
            mnuFolder_RemoveDefault.Text = LMP.Resources.GetLangString("Menu_Content_Folder_RemoveDefault");
            mnuFolder_Paste.Text = LMP.Resources.GetLangString("Menu_Content_Segment_Paste");
            mnuFolder_CreateNewSegment.Text = LMP.Resources.GetLangString("Menu_Content_Folder_CreateNewSegment");
            mnuFolder_ApplyStyles.Text = LMP.Resources.GetLangString("Menu_Content_Folder_ApplyStyles");
            mnuFolder_SaveStyles.Text = LMP.Resources.GetLangString("Menu_Content_Folder_SaveStyles");
            mnuFolder_MoveDown.Text = LMP.Resources.GetLangString("Menu_Content_Folder_MoveDown");
            mnuFolder_MoveUp.Text = LMP.Resources.GetLangString("Menu_Content_Folder_MoveUp");
            mnuFolder_SetPermissions.Text = LMP.Resources.GetLangString("Menu_Content_Folder_SetPermissions");
            mnuFolder_ImportSegment.Text = LMP.Resources.GetLangString("Menu_Content_Folder_ImportSegment");
            mnuFolder_AddFile.Text = LMP.Resources.GetLangString("mnuFolder_AddFile");
            mnuFolder_Help.Text = LMP.Resources.GetLangString("mnuFolder_Help");

            // Set Text of New Segment Submenu
            mnuNewSegment_Blank.Text = LMP.Resources.GetLangString("Menu_Content_NewSegment_Blank");
            mnuNewSegment_Document.Text = LMP.Resources.GetLangString("Menu_Content_NewSegment_Document");
            mnuNewSegment_Selection.Text = LMP.Resources.GetLangString("Menu_Content_NewSegment_Selection");
            mnuNewSegment_StyleSheet.Text = LMP.Resources.GetLangString("Menu_Content_NewSegment_StyleSheet");
            //mnuNewSegment_AnswerFile.Text = LMP.Resources.GetLangString("Menu_Content_NewSegment_AnswerFile");
            mnuNewSegment_MasterDataForm.Text = LMP.Resources.GetLangString("Menu_Content_NewSegment_MasterDataForm");
            mnuNewSegment_SegmentPacket.Text = LMP.Resources.GetLangString("Menu_Content_NewSegment_SegmentPacket");

            //GLOG 8376: Set Text of New Desinger Submenu
            mnuFolder_CreateNewDesigner.Text = LMP.Resources.GetLangString("Menu_Content_Folder_CreateNewDesigner");
        }
        /// <summary>
        /// Delete selected node, along with underlying
        /// FolderMember record
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="bPrompt"></param>
        private void DeleteSegmentNode(UltraTreeNode oNode, bool bPrompt)
        {
            try
            {
                // FolderMembers can only be deleted if User is owner of the Folder
                if (!(ContentTreeHelper.UserIsOwnerOf(oNode.Parent) || (ContentTreeHelper.IsAdminFolderNode(oNode.Parent) && Session.AdminMode))
                    || ContentTreeHelper.IsSharedContentNode(oNode))
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_FolderMemberDeleteNotPermitted"), 
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    FolderMember oMember = (FolderMember)oNode.Tag;
                    string xSegmentID = string.Concat(oMember.ObjectID1.ToString(), ".",
                        oMember.ObjectID2.ToString());
                    if (this.TaskPane.Mode == ForteDocument.Modes.Design && this.TaskPane.ForteDocument.Segments[0].ID == xSegmentID)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_DesignModeDeleteNotPermitted"), 
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    // GLOG : 2797 : JAB
                    // Prompt the users with options related to deleting this items if it is shared.
                    //GLOG item #5845 - prompt only if the user is the owner of the shared segment - dcf
                    //GLOG : 8004 : JSW 
                    //Changed options to OK and Cancel
                    if (bPrompt && IsSharedSegment(oNode) && ContentTreeHelper.UserIsOwnerOf(oNode))
                    {
                        DialogResult iChoice = MessageBox.Show(
                            LMP.Resources.GetLangString("Msg_ConfirmSharedSegmentDeletion"), 
                            LMP.ComponentProperties.ProductName, 
                            MessageBoxButtons.OKCancel,
                            MessageBoxIcon.Warning);

                        switch (iChoice)
                        {
                            //GLOG : 8004 : JSW 
                            //case DialogResult.Yes:
                            //    // Allow the user to review the share properties. Do not delete the segment.
                            //    ShareContent(oNode);
                            //    return;
                            //    break;

                            case DialogResult.OK:
                                // The user does not want to modify share properties. Proceed with the delete.
                                //bPrompt = false;
                                break;

                            case DialogResult.Cancel:
                                // The user has elected to not delete the item. Just return.
                                return;
                                break;
                        }
                    }

                    //Disallow deleting if if segment is used by other segments
                    //and this is the only shortcut for the segment
                    if (ContentTreeHelper.IsAdminFolderNode(oNode.Parent)
                        && Data.Application.CountAdminSegmentFolderAssignments(oMember.ObjectID1) < 2)
                    {
                        ArrayList aRelated = LMP.Data.Application.GetContainingSegmentIDs(xSegmentID);
                        if (aRelated != null && aRelated.Count > 0)
                        {
                            string xDisplayNames = "";
                            for (int i = 0; i < aRelated.Count; i++)
                            {
                                //GLOG 4434: Due to change to the underlying query, ArrayList values
                                //are now strings, so using (int) to cast is no longer valid
                                string xRelatedID = ((object[])aRelated[i])[0].ToString();
                                if (xRelatedID.EndsWith(".0"))
                                    xRelatedID = xRelatedID.Substring(0, xRelatedID.Length - 2);
                                else if (xRelatedID.Contains("."))
                                    //GLOG 4434: Skip any UserSegment IDs
                                    continue;
                                xDisplayNames = xDisplayNames + Data.Application.GetSegmentNameFromID(Int32.Parse(xRelatedID));
                                if (i < aRelated.Count - 1)
                                    xDisplayNames = xDisplayNames + ", ";
                            }
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_DeleteInUseSegmentNotPermitted") + "\r\r" + xDisplayNames, 
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (bPrompt)
                    {
                        DialogResult oChoice = MessageBox.Show(LMP.Resources.GetLangString("Msg_ConfirmSegmentDeletion"),
                            LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (oChoice == DialogResult.No)
                            return;
                    }
                    UltraTreeNode oNewNode = null;
                    // Determine which node will be selected after deletion
                    // 1) next sibling, if it exists
                    // 2) previous sibling, if that exists
                    // 3) otherwise select Parent node
                    if (oNode.HasSibling(NodePosition.Next))
                        oNewNode = oNode.GetSibling(NodePosition.Next);
                    else if (oNode.HasSibling(NodePosition.Previous))
                        oNewNode = oNode.GetSibling(NodePosition.Previous);
                    else
                        oNewNode = oNode.Parent;

                    DateTime t0 = DateTime.Now;
                    FolderMembers oMembers;
                    // Initialize appropriate collection type for Delete proc
                    if (oMember.FolderType == mpFolderTypes.Admin)
                    {
                        oMembers = new LMP.Data.FolderMembers(((Folder)oNode.Parent.Tag).ID);
                        oMembers.Delete(oMembers.FolderID1Filter + "." + oMember.ObjectID1);
                    }
                    else
                    {
                        oMembers = new LMP.Data.FolderMembers(0, 0);
                        oMembers.Delete(oMember.ID);
                    }
                    m_oCurNode.Remove();
                    m_oCurNode = null;

                    // clear browser text
                    //GLOG 7214
                    TaskPane.SetHelpText(wbDescription, "");
                    
                    //hide menu button
                    this.picMenuDropdown.Visible = false;

                    LMP.Benchmarks.Print(t0);
                    // select new node after deletion
                    if (oNewNode != null && oNewNode.IsInView && bPrompt)
                    {
                        oNewNode.Selected = true;
                        this.treeContent.ActiveNode = oNewNode;
                        this.treeContent.Focus();
                    }

                    TaskPanes.RefreshAll(true, false, false);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(LMP.Resources.GetLangString(
                    "Error_FolderMember_Delete") + " " + oNode.Text, oE);
            }
        }

        // GLOG : 2797 : JAB
        // Determine if a segment is shared.
        bool IsSharedSegment(UltraTreeNode oNode)
        {
            ContentTreeHelper.LoadNodeReference(oNode);
            mpObjectTypes iObjectType = 0;

            if (ContentTreeHelper.IsUserSegmentNode(oNode))
            {
                iObjectType = mpObjectTypes.UserSegment;
            }
            else if (ContentTreeHelper.IsVariableSetNode(oNode))
            {
                iObjectType = mpObjectTypes.VariableSet;
            }

            //Get a list of user that have permission to this segment.
            FolderMember oMember = (FolderMember)oNode.Tag;
            ArrayList oUsers = UserPermissions.GetPermittedUsers(iObjectType, oMember.ObjectID1, oMember.ObjectID2);

            if (oUsers.Count > 0)
            {
                // We do have users that share this segment, therefore this is a shared segment.
                return true;
            }

            // No users share the segment, therefore check if it shared within folders.
            // Determine if the segment is shared within folders by checking if it has a shared folder id.
            int iSharedFolderID = SharedFolder.GetSharedFolderID(oMember.ObjectID1, oMember.ObjectID2, iObjectType);
            return (iSharedFolderID > 0);
        }

        /// <summary>
        /// Delete the selected node, along with underlying 
        /// Folder or UserFolder record and assignments
        /// </summary>
        /// <param name="oNode"></param>
        private void DeleteFolderNode(UltraTreeNode oNode)
        {
            try
            {
                UltraTreeNode oNewNode = null;
                UltraTreeNode oParentNode = oNode.Parent; //GLOG 8134
                // Determine which node will be selected after deletion
                // 1) next sibling, if it exists
                // 2) previous sibling, if that exists
                // 3) otherwise select Parent node
                if (oNode.HasSibling(NodePosition.Next))
                    oNewNode = oNode.GetSibling(NodePosition.Next);
                else if (oNode.HasSibling(NodePosition.Previous))
                    oNewNode = oNode.GetSibling(NodePosition.Previous);
                else
                    oNewNode = oNode.Parent;

                // Only owned UserFolders or Admin Folders in Admin Mode can be deleted
                if (oNode.Tag is UserFolder && ContentTreeHelper.UserIsOwnerOf(oNode))
                {
                    DialogResult oChoice = MessageBox.Show(LMP.Resources.GetLangString
                        ("Msg_ConfirmFolderDeletion"), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oChoice == DialogResult.Yes)
                    {
                        DateTime t0 = DateTime.Now;
                        this.UserFolders.Delete(((UserFolder)oNode.Tag).ID);
                        oNode.Remove();
                        LMP.Benchmarks.Print(t0);
                        if (oNewNode != null)
                        {
                            oNewNode.Selected = true;
                            this.treeContent.ActiveNode = oNewNode;
                        }
                        ReindexFolderNodes(oParentNode); //GLOG 8134
                    }
                }
                else if (oNode.Tag is Folder && Session.AdminMode)
                {
                    DialogResult oChoice = MessageBox.Show(LMP.Resources.GetLangString
                        ("Msg_ConfirmFolderDeletion"), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (oChoice == DialogResult.Yes)
                    {
                        DateTime t0 = DateTime.Now;
                        m_oAdminFolders.Delete(((Folder)oNode.Tag).ID);
                        oNode.Remove();
                        LMP.Benchmarks.Print(t0);
                        if (oNewNode != null)
                        {
                            oNewNode.Selected = true;
                            this.treeContent.ActiveNode = oNewNode;
                        }
                        ReindexFolderNodes(oParentNode); //GLOG 8134
                    }
                }
                else
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_FolderDeleteNotPermitted"), LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_Folder_Delete") + " " + oNode.Text, oE);
            }
        }
        /// <summary>
        /// Reload children of selected node
        /// </summary>
        /// <param name="oNode"></param>
        private void RefreshFolderNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return;
            try
            {
                ContentTreeHelper.LoadNodeReference(oNode);
                if (oNode.Tag is UserFolder || ContentTreeHelper.IsMyFolderNode(oNode))
                    RefreshUserFolderNode(oNode, true);
                else if (ContentTreeHelper.IsAdminFolderNode(oNode))
                    RefreshAdminFolderNode(oNode, true);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(LMP.Resources.GetLangString("Error_CouldNotRefreshSelectedNode"), oE);
            }

        }
        /// <summary>
        /// Collapse all subfolders
        /// </summary>
        /// <param name="oNode"></param>
        
        // GLOG : 3715 : CEH
        private void CollapseAllSubfolders(UltraTreeNode oNode)
        {
            if (oNode == null)
                return;
            try
            {
                ContentTreeHelper.LoadNodeReference(oNode);
                m_bIgnoreCollapse = false;
                foreach (UltraTreeNode oSubNode in oNode.Nodes)
                    oSubNode.Expanded = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(LMP.Resources.GetLangString("Error_CouldNotCollapseNodes"), oE);
            }

        }
        /// <summary>
        /// Delete selected node and underlying object
        /// </summary>
        /// <param name="oNode"></param>
        private void DeleteNode(UltraTreeNode oNode)
        {
            if (oNode == null)
                return;
            try
            {
                ContentTreeHelper.LoadNodeReference(oNode);
                // only allow deleting UserFolders and UserFolderMembers if not in Admin mode
                if (oNode.Tag is UserFolder || (oNode.Tag is Folder && Session.AdminMode))
                    DeleteFolderNode(oNode);
                else if (oNode.Tag is FolderMember && 
                    (((FolderMember)oNode.Tag).FolderType== mpFolderTypes.User || Session.AdminMode))
                    DeleteSegmentNode(oNode, true);
                this.treeContent.Focus();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException("", oE);
            }

        }
        /// <summary>
        /// Create new folder object and corresponding Tree node
        /// </summary>
        /// <param name="oParentNode"></param>
        private void CreateFolderNode(UltraTreeNode oParentNode)
        {
            try
            {
                if (oParentNode == null)
                    return;
                ContentTreeHelper.LoadNodeReference(oParentNode);
                string xNewKey = "";
                ContentPropertyForm oForm = new ContentPropertyForm();
                // Display Segment Creator form
                oForm.ShowForm(this.ParentForm, oParentNode, Form.MousePosition, ContentPropertyForm.DialogMode.NewFolder);
                if (!oForm.Canceled)
                {
                    if (oForm.NewUserFolder != null)
                    {
                        // New Folder is UserFolder
                        if (!oParentNode.Expanded)
                        {
                            //Refresh Folder - newly-create Folder will be included
                            RefreshUserFolderNode(oParentNode, true);
                            xNewKey = oParentNode.Key + mpPathSeparator + oForm.NewUserFolder.ID;
                        }
                        else
                        {
                            UserFolder oNewFolder = oForm.NewUserFolder;
                            xNewKey = oParentNode.Key + mpPathSeparator + oForm.NewUserFolder.ID;
                            int iPos = 0;
                            foreach (UltraTreeNode oNode in oParentNode.Nodes)
                            {
                                // Add to end of existing child Folder nodes
                                if (!oNode.Key.StartsWith("U"))
                                {
                                    ////if (oNode.Text.CompareTo(oNewFolder.Name) < 0)
                                    iPos = oNode.Index + 1;
                                }
                                else
                                    //if key Starts with "U" it's a segment node, so don't check for sort
                                    break;
                            }
                            this.treeContent.SuspendLayout();
                            UltraTreeNode oNewNode = oParentNode.Nodes.Insert(iPos, xNewKey, oNewFolder.Name);
                            oNewNode.Tag = oNewFolder;
                            oNewNode.Override.LabelEdit = Infragistics.Win.DefaultableBoolean.True;
                            ReindexFolderNodes(oParentNode);
                        }
                        m_oCurNode = SelectFolderNode(xNewKey);
                    }
                    else if (oForm.NewAdminFolder != null)
                    {
                        if (!oParentNode.Expanded)
                        {
                            //Refresh Folder - newly-create Folder will be included
                            RefreshAdminFolderNode(oParentNode, true);
                            xNewKey = oParentNode.Key + mpPathSeparator + oForm.NewAdminFolder.ID.ToString();
                        }
                        else
                        {
                            Folder oNewFolder = oForm.NewAdminFolder;
                            xNewKey = oParentNode.Key + mpPathSeparator + oNewFolder.ID.ToString();
                            int iPos = 0;
                            foreach (UltraTreeNode oNode in oParentNode.Nodes)
                            {
                                // Add to end of existing child Folder nodes
                                if (!oNode.Key.StartsWith("A") && !oNode.Key.StartsWith("O"))
                                {
                                    //if (oNode.Text.CompareTo(oNewFolder.Name) < 0)
                                    iPos = oNode.Index + 1;
                                }
                                else
                                    //if key Starts with "A" it's a segment node, so don't check for sort
                                    break;
                            }
                            this.treeContent.SuspendLayout();
                            UltraTreeNode oNewNode = oParentNode.Nodes.Insert(iPos, xNewKey, oNewFolder.Name);
                            oNewNode.Tag = oNewFolder;
                            if (Session.AdminMode)
                            {
                                oNewNode.Override.LabelEdit = Infragistics.Win.DefaultableBoolean.True;
                                ReindexFolderNodes(oParentNode); //GLOG 8134
                            }
                        }
                        m_oCurNode = SelectFolderNode(xNewKey);
                        this.treeContent.ResumeLayout();
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_Folder_Create"), oE);
            }
        }
        /// <summary>
        /// Reindexes Folder collection after adding, deleting or moving items
        /// </summary>
        /// <param name="oParent"></param>
        private void ReindexFolderNodes(UltraTreeNode oParent)
        {
            //GLOG 8134
            if (oParent.Tag is UserFolder || oParent.Tag is string)
            {
                UserFolder oParentFolder = oParent.Tag as UserFolder;
                int iId1 = 0;
                int iId2 = 0;
                //If tag is not a User Folder, Parent is the My Folders node
                if (oParentFolder != null)
                {
                    String.SplitID(oParentFolder.ID, out iId1, out iId2);
                }
                UserFolders oFolders = new UserFolders(Session.CurrentUser.ID, iId1, iId2);
                int iNext = 1;
                foreach (UltraTreeNode oChild in oParent.Nodes)
                {
                    if (oChild.Tag is UserFolder || oChild.Tag is string)
                    {
                        UserFolder oFolder = oChild.Tag as UserFolder;
                        if (oFolder == null)
                        {
                            oFolder = (UserFolder)oFolders.ItemFromID(oChild.Tag.ToString());
                        }
                        if (oFolder.Index != iNext)
                        {
                            oFolder.Index = iNext;
                            oFolders.Save((StringIDSimpleDataItem)oFolder);
                        }
                        iNext++;
                    }
                    else
                        //end of folder nodes
                        break;
                }
            }
            else if (oParent.Tag is Folder || oParent.Tag is int)
            {
                Folder oParentFolder = oParent.Tag as Folder;
                int iId;
                if (oParentFolder == null)
                {
                    iId = (int)oParent.Tag;
                }
                else
                    iId = oParentFolder.ID;

                Folders oFolders = new Folders(Session.CurrentUser.ID, iId);
                int iNext = 1;
                foreach (UltraTreeNode oChild in oParent.Nodes)
                {
                    if (oChild.Tag is Folder || oChild.Tag is int)
                    {
                        Folder oFolder = oChild.Tag as Folder;
                        if (oFolder == null)
                        {
                            oFolder = (Folder)oFolders.ItemFromID((int)oChild.Tag);
                        }
                        if (oFolder.Index != iNext)
                        {
                            oFolder.Index = iNext;
                            oFolders.Save((LongIDSimpleDataItem)oFolder);
                        }
                        iNext++;
                    }
                    else
                        //end of folder nodes
                        break;
                }
            }

        }
        /// <summary>
        /// Check for any active context menu and close if necessary
        /// </summary>
        private void DismissActiveMenu()
        {
            DismissActiveMenu(ToolStripDropDownCloseReason.AppFocusChange);
        }
        /// <summary>
        /// Close active menu in response to Escape key or Focus change
        /// </summary>
        private void DismissActiveMenu(ToolStripDropDownCloseReason oReason)
        {
            if (mnuSegment.Visible)
                mnuSegment.Close(oReason);
            else if (mnuFolder.Visible)
                mnuFolder.Close(oReason);
            else if (mnuSearch.Visible)
                mnuSearch.Close(oReason);
            else if (mnuQuickHelp.Visible)
                mnuQuickHelp.Close(oReason);
        }

        /// <summary>
        /// Creates a new segment that is a duplicate of the selected segment
        /// </summary>
        /// <param name="oNode"></param>
        private void CloneSegment(UltraTreeNode oNode)
        {
            if (!(ContentTreeHelper.IsAdminSegmentNode(oNode) && (Session.AdminMode || Session.CurrentUser.IsContentDesigner))  && !ContentTreeHelper.IsUserSegmentNode(oNode))
                return;
            ContentTreeHelper.LoadNodeReference(oNode);
            UltraTreeNode oNewNode = null;
            ContentPropertyForm oForm = new ContentPropertyForm();
            // Show Content Properties form
            ContentPropertyForm.DialogMode iMode = ContentPropertyForm.DialogMode.ClonedSegment;
            if (ContentTreeHelper.IsStyleSheetNode(oNode))
                iMode = ContentPropertyForm.DialogMode.ClonedStyleSheet;
            oForm.ShowForm(this.ParentForm, oNode, Form.MousePosition, iMode);
            if (!oForm.Canceled)
            {
                this.treeContent.SuspendLayout();
                if (ContentTreeHelper.IsAdminSegmentNode(oNode))
                {
                    AdminSegmentDef oNew = (AdminSegmentDef)oForm.NewSegmentDef;
                    Folder oFolder = ((Folder)oNode.Parent.Tag);
                    // Create corresponding FolderMember
                    FolderMember oMember = (FolderMember)oFolder.GetMembers().Create();
                    oMember.ObjectID1 = oNew.ID;
                    oFolder.GetMembers().Save(oMember);
                    int iPos = 0;
                    foreach (UltraTreeNode oSiblingNode in oNode.Parent.Nodes)
                    {
                        // Find appropriate sort order within existing child FolderMember nodes
                        if (oSiblingNode.Key.StartsWith("A"))
                        {
                            if (oSiblingNode.Text.CompareTo(oNew.DisplayName) < 0)
                                iPos = oSiblingNode.Index + 1;
                        }
                        else
                        {
                            // Place after any folders
                            iPos = oSiblingNode.Index + 1;
                        }
                    }

                    oNewNode = oNode.Parent.Nodes.Insert(iPos, "A" + oNode.Parent.Key + mpPathSeparator + oMember.ID, oNew.DisplayName);
                    //set UI properties
                    oNewNode.Override.NodeAppearance.Image = oNode.Override.NodeAppearance.Image;
                    oNewNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                    oNewNode.Tag = (object)string.Concat("A", oMember.ID);
                }
                else
                {
                    UserSegmentDef oNew = (UserSegmentDef)oForm.NewSegmentDef;
                    UserFolder oFolder = ((UserFolder)oNode.Parent.Tag);
                    // Create corresponding FolderMember
                    FolderMember oMember = (FolderMember)oFolder.GetMembers().Create();
                    int iID1;
                    int iID2;
                    String.SplitID(oNew.ID, out iID1, out iID2);
                    oMember.ObjectID1 = iID1;
                    oMember.ObjectID2 = iID2;
                    oMember.ObjectTypeID = mpFolderMemberTypes.UserSegment;
                    oFolder.GetMembers().Save(oMember);
                    int iPos = 0;
                    foreach (UltraTreeNode oSiblingNode in oNode.Parent.Nodes)
                    {
                        // Find appropriate sort order within existing child FolderMember nodes
                        if (oSiblingNode.Key.StartsWith("U"))
                        {
                            if (oSiblingNode.Text.CompareTo(oNew.DisplayName) < 0)
                                iPos = oSiblingNode.Index + 1;
                        }
                        else
                        {
                            // Place after any folders
                            iPos = oSiblingNode.Index + 1;
                        }
                    }
                    oNewNode = oNode.Parent.Nodes.Insert(iPos, "U" + oNode.Parent.Key + mpPathSeparator + oMember.ID, oNew.DisplayName);
                    //set UI properties
                    oNewNode.Override.NodeAppearance.Image = oNode.Override.NodeAppearance.Image;
                    oNewNode.Override.ShowExpansionIndicator = ShowExpansionIndicator.Never;
                    oNewNode.Tag = (object)string.Concat("U", oMember.ID);
                }
                oNewNode.Selected = true;
                this.treeContent.ActiveNode = oNewNode;
                this.treeContent.ResumeLayout();
            }
        }
        /// <summary>
        /// creates an Admin segment from the content
        /// of the specified segment
        /// </summary>
        /// <param name="oNode"></param>
        private void ConvertToAdminSegment(UltraTreeNode oNode, bool bUpdateExisting)
        {
            if (!(ContentTreeHelper.IsUserSegmentNode(oNode) && Session.AdminMode))
                return;

            ContentTreeHelper.LoadNodeReference(oNode);

            ConvertToAdminContentForm oForm = new ConvertToAdminContentForm(bUpdateExisting);

            // Show form
            if (oForm.ShowDialog() == DialogResult.OK)
            {
                //get folder
                Folder oFolder = oForm.SelectedFolder;

                //get source segment def
                UserSegmentDef oSourceDef = (UserSegmentDef)oNode.Tag;

                //get target def
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oDef = null;

                if (bUpdateExisting)
                    oDef = oForm.SelectedSegmentDefinition;
                else
                    oDef = (AdminSegmentDef)oDefs.Create(mpObjectTypes.Architect);

                //populate target with data from source
                oDef.ChildSegmentIDs = oSourceDef.ChildSegmentIDs;
                oDef.DefaultDoubleClickBehavior = oSourceDef.DefaultDoubleClickBehavior;
                oDef.DefaultDoubleClickLocation = oSourceDef.DefaultDoubleClickLocation;
                oDef.DefaultDragBehavior = oSourceDef.DefaultDragBehavior;
                oDef.DefaultDragLocation = oSourceDef.DefaultDragLocation;
                oDef.DefaultMenuInsertionBehavior = oSourceDef.DefaultMenuInsertionBehavior;
                oDef.DisplayName = oSourceDef.DisplayName;
                oDef.HelpText = oSourceDef.HelpText;
                oDef.IntendedUse = oSourceDef.IntendedUse;
                oDef.L0 = oSourceDef.L0;
                oDef.L1 = oSourceDef.L1;
                oDef.L2 = oSourceDef.L2;
                oDef.L3 = oSourceDef.L3;
                oDef.L4 = oSourceDef.L4;
                oDef.MenuInsertionOptions = oSourceDef.MenuInsertionOptions;
                oDef.Name = oSourceDef.Name;
                oDef.XML = oSourceDef.XML;

                if (oDef.ID == 0)
                {
                    try
                    {
                        //this is a new definition -
                        //save now to get ID
                        oDefs.Save(oDef);
                    }
                    catch (LMP.Exceptions.StoredProcedureException oE)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_SegmentNameAlreadyExists") + oSourceDef.Name, 
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                        return;
                    }
                }

                //modify xml to reflect new admin ID and
                //Architect segment type
                string xXML = oSourceDef.XML;
                xXML = xXML.Replace("SegmentID=" + oSourceDef.ID, "SegmentID=" + oDef.ID.ToString());
                xXML = xXML.Replace("ObjectTypeID=99", "ObjectTypeID=4");

                //save segment with modified xml
                oDef.XML = xXML;
                oDefs.Save(oDef);

                if (!bUpdateExisting)
                {
                    //get the ID of the def
                    int iID = oDef.ID;

                    //add to specified folder
                    FolderMember oMember = (FolderMember)oFolder.GetMembers().Create();
                    oMember.ObjectID1 = oDef.ID;
                    oFolder.GetMembers().Save(oMember);

                    //refresh content manager tree
                    TaskPane.Refresh(true, false, false);
                }
            }
        }
        /// <summary>
        /// Set reference object to point to Node being copied (using menu or Ctrl+C)
        /// </summary>
        /// <param name="oNode"></param>
        private void CopySegmentNode(UltraTreeNode oNode, bool bPrompt)
        {
            m_oNodeToPaste = oNode;
            if (bPrompt)
            {
                ConditionalPromptDialog oDlg = new ConditionalPromptDialog();
                oDlg.ShowDialog(LMP.Resources.GetLangString("Prompt_SelectSegmentPasteDestination"),
                    LMP.Resources.GetLangString("Prompt_DoNotPromptAgain"), "SkipSegmentCopyPrompt");
            }

            // GLOG : 5451 : CEH
            if (this.treeResults.Visible)
            {
                this.treeResults.Visible=false;
                this.treeContent.Visible = true;
            }
        }

        //GLOG : 6030 : CEH
        //GLOG : 6221 : CEH
        /// <summary>
        /// Adds Segment to Favorites
        /// </summary>
        /// <param name="oNode"></param>
        private void AddToFavorites(UltraTreeNode oNode)
        {
            try
            {
                if (FavoriteSegments.Count < FavoriteSegments.FAVORITE_SEGMENT_MAX_FAVORITES)
                {
                    //GLOG 6295 (dm) - saved data was supplying segment id and name
                    //instead of its own - use following three variables throughout this
                    //method rather than assuming segment def
                    string xSegName;
                    string xID;
                    string xDisplayName;
					//GLOG : 6303 : jsw
					//add intended use icon to favorites 
                    mpSegmentIntendedUses tIntendedUse;

                    if (ContentTreeHelper.IsVariableSetNode(oNode))
                    {
                        VariableSetDef oDef = ContentTreeHelper.GetVariableSetFromNode(oNode);
                        xSegName = oDef.Name;
                        xID = oDef.ID;
                        xDisplayName = oDef.Name;
                        tIntendedUse = mpSegmentIntendedUses.AsDocument;
                    }
                    else
                    {
                        ISegmentDef oDef = ContentTreeHelper.GetSegmentDefFromNode(oNode);
                        xSegName = oDef.Name;
                        xID = ContentTreeHelper.GetSegmentIDFromNode(oNode);
                        xDisplayName = oDef.DisplayName;
                        tIntendedUse = oDef.IntendedUse;
                    }

                    // GLOG : 7276 : jsw
                    // check if already in the list
                    if (IsFavorite(xDisplayName))
                        return;

                    // favorite not in list, ok to add
                    // get segment type
                    mpFolderMemberTypes iType = 0;

                    if (ContentTreeHelper.IsUserSegmentNode(oNode))
                    {
                        iType = mpFolderMemberTypes.UserSegment;
                    }
                    else if (ContentTreeHelper.IsVariableSetNode(oNode))
                    {
                        iType = mpFolderMemberTypes.VariableSet;
                    }
                    else
                    {
                        iType = mpFolderMemberTypes.AdminSegment;
                    }

                    // Add segment to Favorites
                    FavoriteSegments.Add(xSegName, xID, iType, xDisplayName, tIntendedUse);

                    // Save Favorites list
                    FavoriteSegments.Save();

                    // Invalidate the ribbon to update the favorite segments selection.
                    LMP.MacPac.Application.MacPac10Ribbon.Invalidate();
                }
                else
                {
                    MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_FavoritesLimitReached"),
                        FavoriteSegments.FAVORITE_SEGMENT_MAX_FAVORITES),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        // GLOG : 7276 : jsw
        /// <summary>
        /// Checks if Segment is Favorite
        /// </summary>
        /// <param name="xDisplayName"></param>
        private bool IsFavorite(string xDisplayName)
        {
            try
            {

                // check if already in the list
                for (int i = 0; i < FavoriteSegments.Count; i++)
                {
                    string xName = FavoriteSegments.SegmentDisplayName(i);

                    if (xName == xDisplayName)
                        return true;
                }

            }
            catch { }
  
            //not in Favorites list
            return false;
        }

        //GLOG : 7276 : jsw
        /// <summary>
        /// Removes Segment from Favorites
        /// </summary>
        /// <param name="oNode"></param>
        private void RemoveFromFavorites(UltraTreeNode oNode)
        {
            try
            {
                    string xSegName;

                    if (ContentTreeHelper.IsVariableSetNode(oNode))
                    {
                        VariableSetDef oDef = ContentTreeHelper.GetVariableSetFromNode(oNode);
                        xSegName = oDef.Name;
                    }
                    else
                    {
                        ISegmentDef oDef = ContentTreeHelper.GetSegmentDefFromNode(oNode);
                        //get segment display name
                        xSegName = oDef.DisplayName;
                    }

                    // Remove Segment from Favorites
                    FavoriteSegments.Remove(xSegName);

                    // Save Favorites list
                    FavoriteSegments.Save();

                    // Invalidate the ribbon to update the favorite segments selection.
                    LMP.MacPac.Application.MacPac10Ribbon.Invalidate();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }


        /// <summary>
        /// Paste copy of UserSegment into a new folder (using menu or Ctrl+V)
        /// </summary>
        /// <param name="oTarget"></param>
        private void PasteSegmentNode(UltraTreeNode oTarget)
        {
            if (m_oNodeToPaste == null)
                return;
            try
            {
                if (ContentTreeHelper.IsUserFolderNode(oTarget) && ContentTreeHelper.IsSharedContentNode(m_oNodeToPaste))
                {
                    //Clone Segment dragged from a Shared Folder
                    FolderMembers oMembers = ((UserFolder)oTarget.Tag).GetMembers();
                    int iID1 = 0;
                    int iID2 = 0;
                    mpFolderMemberTypes iType = 0;
                    if (ContentTreeHelper.IsUserSegmentNode(m_oNodeToPaste))
                    {
                        UserSegmentDef oSource = (UserSegmentDef)m_oNodeToPaste.Tag;
                        string xID = oSource.ID;
                        LMP.String.SplitID(xID, out iID1, out iID2);
                        iType  = mpFolderMemberTypes.UserSegment;
                    }
                    else if (ContentTreeHelper.IsVariableSetNode(m_oNodeToPaste))
                    {
                        VariableSetDef oSource = (VariableSetDef)m_oNodeToPaste.Tag;
                        string xID = oSource.ID;
                        LMP.String.SplitID(xID, out iID1, out iID2);
                        iType = mpFolderMemberTypes.VariableSet;
                    }

                    if (ShortCutExists(oTarget, iID1, iID2))
                    {
                        NotifyShortCutExists();
                    }
                    else
                    {
                        FolderMember oCopy = (FolderMember)oMembers.Create();
                        oCopy.ObjectID1 = iID1;
                        oCopy.ObjectID2 = iID2;
                        oCopy.ObjectTypeID = iType;
                        oMembers.Save((StringIDSimpleDataItem)oCopy);
                        RefreshUserFolderNode(oTarget, true);
                    }
                }
                else if (ContentTreeHelper.IsUserFolderNode(oTarget) &&
                     (ContentTreeHelper.IsUserSegmentNode(m_oNodeToPaste) || ContentTreeHelper.IsAdminSegmentNode(m_oNodeToPaste)) || ContentTreeHelper.IsVariableSetNode(m_oNodeToPaste))
                {
                    FolderMembers oMembers = ((UserFolder)oTarget.Tag).GetMembers();
                    int iID1 = 0;
                    int iID2 = 0;
                    mpFolderMemberTypes iType = 0;
                    if (m_oNodeToPaste.Tag is UserSegmentDef)
                    {
                        UserSegmentDef oDef = (UserSegmentDef)m_oNodeToPaste.Tag;
                        LMP.String.SplitID(oDef.ID, out iID1, out iID2);
                        iType = mpFolderMemberTypes.UserSegment;
                    }
                    else if (m_oNodeToPaste.Tag is VariableSetDef)
                    {
                        VariableSetDef oVDef = (VariableSetDef)m_oNodeToPaste.Tag;
                        LMP.String.SplitID(oVDef.ID, out iID1, out iID2);
                        iType = mpFolderMemberTypes.VariableSet;
                    }
                    else
                    {
                        // GLOG : 5451 : CEH
                        if (m_oNodeToPaste.Tag is AdminSegmentDef)
                        {
                            AdminSegmentDef oDef = (AdminSegmentDef)m_oNodeToPaste.Tag;
                            iID1 = oDef.ID;
                            iID2 = 0;
                            iType = mpFolderMemberTypes.AdminSegment;
                        }
                        else
	                    {
                            FolderMember oSource = (FolderMember)m_oNodeToPaste.Tag;
                            iID1 = oSource.ObjectID1;
                            iID2 = oSource.ObjectID2;
                            iType = oSource.ObjectTypeID;
	                    }
                    }

                    if (ShortCutExists(oTarget, iID1, iID2))
                    {
                        NotifyShortCutExists();
                    }
                    else
                    {
                        FolderMember oCopy = (FolderMember)oMembers.Create();
                        oCopy.ObjectID1 = iID1;
                        oCopy.ObjectID2 = iID2;
                        oCopy.ObjectTypeID = iType;
                        oMembers.Save((StringIDSimpleDataItem)oCopy);
                        RefreshUserFolderNode(oTarget, true);
                    }
                }
                else if (Session.AdminMode && ContentTreeHelper.IsAdminFolderNode(oTarget)
                     && ContentTreeHelper.IsAdminSegmentNode(m_oNodeToPaste) && (m_oNodeToPaste.Parent != oTarget))
                {
                    FolderMembers oMembers = ((Folder)oTarget.Tag).GetMembers();
                    FolderMember oSource = (FolderMember)m_oNodeToPaste.Tag;

                    if (ShortCutExists(oTarget, oSource.ObjectID1, oSource.ObjectID2))
                    {
                        NotifyShortCutExists();
                    }
                    else
                    {
                        FolderMember oCopy = (FolderMember)oMembers.Create();
                        oCopy.ObjectID1 = oSource.ObjectID1;
                        oCopy.ObjectID2 = oSource.ObjectID2;
                        try
                        {
                            oMembers.Save((StringIDSimpleDataItem)oCopy);
                            RefreshAdminFolderNode(oTarget, true);
                        }
                        catch (LMP.Exceptions.StoredProcedureException)
                        {
                            // Can't have two copies of the same segment in an Admin Folder
                            MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateAdminSegmentName"),
                                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(LMP.Resources.GetLangString("Error_Node_Paste"), oE);
            }
            finally
            {
                // Clear copied object
                m_oNodeToPaste = null;
            }
        }

        private void NotifyShortCutExists()
        {
            MessageBox.Show(LMP.Resources.GetLangString("MessageBoxText_ShortCutExists"), 
                LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /// <summary>
        /// returns true if shortcut exists
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="iID1"></param>
        /// <param name="iID2"></param>
        /// <returns></returns>
        private bool ShortCutExists(UltraTreeNode oNode, int iID1, int iID2)
        {
            string xFilterString;
            FolderMembers oMembers;

            //get folder members and specify filter string
            if (oNode.Tag is Folder)
            {
                oMembers = ((Folder)oNode.Tag).GetMembers();
                xFilterString = "SegmentID = '" + iID1 + "'";
            }
            else
            {
                oMembers = ((UserFolder)oNode.Tag).GetMembers();
                xFilterString = "TargetObjectID1 = '" + iID1 + 
                    "' AND TargetObjectID2 = '" + iID2 + "'";
            }

            DataSet ds = oMembers.ToDataSet();
            DataRow[] oRows = ds.Tables[0].Select(xFilterString);
            return (oRows.Length > 0);
        }

        /// <summary>
        /// Display contents of m_aFindResults ArrayList in tree
        /// </summary>
        private void ShowFindResults()
        {
            if (m_aFindResults == null)
                return;
            treeResults.SuspendLayout();
            if (treeResults.Nodes.Count == 0)
            {
                //Add top-level Search Results node
                UltraTreeNode oTopNode = this.treeResults.Nodes.Add(
                    "0", LMP.Resources.GetLangString("Prompt_SearchResults"));
                oTopNode.Override.ExpandedNodeAppearance.Image = Images.SearchResults;
                oTopNode.Expanded = true;
                oTopNode.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                oTopNode.Override.NodeSpacingBefore = 5;
                oTopNode.Override.SelectionType = SelectType.None;
                oTopNode.Override.NodeDoubleClickAction = NodeDoubleClickAction.None;
            }
            else
            {
                treeResults.Nodes[0].Nodes.Clear();
            }
            for (int i = 0; i < m_aFindResults.Count; i++)
            {
                // Use DisplayName for Node Text, ID for Key and Tag
                object[] aFindResult = (object[])m_aFindResults[i];
                string xName = aFindResult[0].ToString();
                string xID = @aFindResult[1].ToString();
                mpFolderMemberTypes shMemberType = (mpFolderMemberTypes)(int.Parse(aFindResult[2].ToString()));
                mpSegmentIntendedUses iUse = (mpSegmentIntendedUses)(int.Parse(aFindResult[3].ToString()));  //GLOG 8037

                //add node to tree
                UltraTreeNode oTreeNode = null;
                switch (shMemberType)
                {
                    case mpFolderMemberTypes.UserSegment:
                        //UserSegment
                        oTreeNode = treeResults.Nodes[0].Nodes.Add("U" + xID, xName);
                        oTreeNode.Tag = (object)string.Concat("U", xID);
                        oTreeNode.Override.NodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.UserSegment, oTreeNode); //GLOG 8376
                        oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.UserSegment, oTreeNode); //GLOG 8376
                        break;
                    case mpFolderMemberTypes.VariableSet:
                        //VariableSet/Prefill
                        oTreeNode = treeResults.Nodes[0].Nodes.Add("P" + xID, xName);
                        oTreeNode.Tag = (object)string.Concat("P", xID);
                        oTreeNode.Override.NodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.VariableSet, false); //GLOG 8376
                        oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.VariableSet, false); //GLOG 8376
                        break;
                    default:
                        //AdminSegment
                        oTreeNode = treeResults.Nodes[0].Nodes.Add("A" + xID, xName);
                        oTreeNode.Tag = (object)string.Concat("A", xID);
                        oTreeNode.Override.NodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.AdminSegment, oTreeNode);
                        oTreeNode.Override.ExpandedNodeAppearance.Image = this.GetImage(iUse, mpFolderMemberTypes.AdminSegment, oTreeNode);
                        break;
                }
                oTreeNode.Override.CellClickAction = CellClickAction.Default;
                //GLOG : 8308 : JSW
                if (Session.CurrentUser.UserSettings.ExpandFindResults)
                    ExpandNodeShowSegmentPath(oTreeNode);
            }
            treeResults.ResumeLayout();
        }

        private void ExpandNodeShowSegmentPath(UltraTreeNode oTreeNode)
        {
            if (oTreeNode != null)
            {
                if (ContentTreeHelper.IsAdminSegmentNode(oTreeNode))
                {
                    AddAdminFolderMemberPaths(oTreeNode);
                }

                if (ContentTreeHelper.IsUserSegmentNode(oTreeNode))
                {
                    AddUserFolderMemberPaths(oTreeNode);
                }

                if (ContentTreeHelper.IsVariableSetNode(oTreeNode)) 
                {
                    AddPrefillFolderMemberPaths(oTreeNode);
                }
            }
        }

        private void CollapseNodeHideSegmentPath(UltraTreeNode oTreeNode)
        {
            if (oTreeNode != null)
            {
                oTreeNode.Nodes.Clear();
            }
        }

        private void AddUserFolderMemberPaths(UltraTreeNode oTreeNode)
        {
            string xMemberId = "";

            if (oTreeNode.Tag is string)
            {
                xMemberId = (string)oTreeNode.Tag;
            }

            if (oTreeNode.Tag is LMP.Data.UserSegmentDef)
            {
                UserSegmentDef oUserSegmentDef = (UserSegmentDef)oTreeNode.Tag;
                xMemberId = "U" + oUserSegmentDef.ID;
            }

            if (oTreeNode.Tag is LMP.Data.AdminSegmentDef)
            {
                AdminSegmentDef oAdminSegmentDef = (AdminSegmentDef)oTreeNode.Tag;
                xMemberId = "A" + oAdminSegmentDef.ID.ToString();
            }

            string[] axSegmentPaths = UserFolder.GetSegmentFullPaths(xMemberId, LMP.MacPac.Session.CurrentUser.ID);

            foreach (string xSegmentPath in axSegmentPaths)
            {
                UltraTreeNode oSegmentPathNode = new UltraTreeNode();
                oSegmentPathNode.Key = xMemberId + ".Path." + xSegmentPath;
                oSegmentPathNode.Text = xSegmentPath;
                oSegmentPathNode.Override.NodeAppearance.Image = Images.LeftArrow;
                oSegmentPathNode.Override.ExpandedNodeAppearance.Image = Images.LeftArrow;
                oTreeNode.Nodes.Add(oSegmentPathNode);                
                oTreeNode.Expanded = true;
            }
            AddSharedFolderPaths(oTreeNode); //GLOG 8037: Also show results from Shared Folders
        }
		//GLOG 8037
        private void AddSharedFolderPaths(UltraTreeNode oTreeNode)
        {
            string xMemberId = "";
            if (oTreeNode.Tag is string)
            {
                xMemberId = (string)oTreeNode.Tag;
            }
            else if (oTreeNode.Tag is LMP.Data.UserSegmentDef)
            {
                UserSegmentDef oUserSegmentDef = (UserSegmentDef)oTreeNode.Tag;
                xMemberId = "U" + oUserSegmentDef.ID;
            }
            else if (oTreeNode.Tag is LMP.Data.VariableSetDef)
            {
                VariableSetDef oVariableSetDef = (VariableSetDef)oTreeNode.Tag;
                xMemberId = "P" + oVariableSetDef.ID;
            }
            string xSharedPath = SharedFolder.GetSharedContentFullPath(xMemberId, LMP.MacPac.Session.CurrentUser.ID);

            if (xSharedPath != "")
            {
                UltraTreeNode oSegmentPathNode = new UltraTreeNode();
                oSegmentPathNode.Key = xMemberId + ".Path." + xSharedPath;
                oSegmentPathNode.Text = xSharedPath;
                oSegmentPathNode.Override.NodeAppearance.Image = Images.LeftArrow;
                oSegmentPathNode.Override.ExpandedNodeAppearance.Image = Images.LeftArrow;
                oTreeNode.Nodes.Add(oSegmentPathNode);                
                oTreeNode.Expanded = true;
            }
        }
        private void AddAdminFolderMemberPaths(UltraTreeNode oTreeNode)
        {
            string xMemberId;

            if (oTreeNode.Tag is LMP.Data.AdminSegmentDef)
            {
                AdminSegmentDef oAdminSegmentDef = (AdminSegmentDef)oTreeNode.Tag;
                xMemberId = "A" + oAdminSegmentDef.ID;
            }
            else
            {
                xMemberId = (string)oTreeNode.Tag;
            }

            string[] axSegmentPaths = Folder.GetSegmentPaths(xMemberId);

            foreach (string xSegmentPath in axSegmentPaths)
            {
                UltraTreeNode oSegmentPathNode = new UltraTreeNode();
                oSegmentPathNode.Key = xMemberId + ".Path." + xSegmentPath;
                oSegmentPathNode.Text = xSegmentPath;
                oSegmentPathNode.Override.NodeAppearance.Image = Images.LeftArrow;
                oSegmentPathNode.Override.ExpandedNodeAppearance.Image = Images.LeftArrow;
                oTreeNode.Nodes.Add(oSegmentPathNode);
                
                oTreeNode.Expanded = true;
            }

            // Admin Segments can be located in both Admin Folders and User Folders.
            // Add the User folder path for this admin segment.
            AddUserFolderMemberPaths(oTreeNode);
        }

        private void AddPrefillFolderMemberPaths(UltraTreeNode oTreeNode)
        {
            string xMemberId;

            if (oTreeNode.Tag is LMP.Data.VariableSetDef)
            {
                VariableSetDef oVariableSetDef = (VariableSetDef)oTreeNode.Tag;
                xMemberId = "P" + oVariableSetDef.ID;
            }
            else
            {
                xMemberId = (string)oTreeNode.Tag;
            }

            string[] axSegmentPaths = UserFolder.GetSegmentFullPaths(xMemberId, LMP.MacPac.Session.CurrentUser.ID);

            foreach (string xSegmentPath in axSegmentPaths)
            {
                UltraTreeNode oSegmentPathNode = new UltraTreeNode();
                oSegmentPathNode.Key = xMemberId + ".Path." + xSegmentPath;
                oSegmentPathNode.Text = xSegmentPath;
                oSegmentPathNode.Override.NodeAppearance.Image = Images.LeftArrow;
                oSegmentPathNode.Override.ExpandedNodeAppearance.Image = Images.LeftArrow;
                oTreeNode.Nodes.Add(oSegmentPathNode);                
                oTreeNode.Expanded = true;
            }
            AddSharedFolderPaths(oTreeNode); //GLOG 8037: Also show results from Shared Folders
        }

        /// <summary>
        /// Set the context menu for the Variable Segments that have no associated segment id.
        /// </summary>
        private void SetIdlessVariableSegmentMenuOptions()
        {
            this.mnuSegment_CreateNewDocument.Visible = false;
            this.mnuSegment_CreateExternalContent.Visible = false;
            this.mnuSegment_Insert.Visible = true;
            this.mnuSegment_InsertAtSelection.Visible = false;
            this.mnuSegment_InsertStartOfDocument.Visible = false;
            this.mnuSegment_InsertEndOfDocument.Visible = false;
            this.mnuSegment_InsertStartOfThisSection.Visible = false;
            this.mnuSegment_InsertEndOfThisSection.Visible = false;
            this.mnuSegment_InsertStartOfAllSections.Visible = false;
            this.mnuSegment_InsertEndOfAllSections.Visible = false;
            this.mnuSegment_Sep1.Visible = true;
            this.mnuSegment_SelectionAsBody.Visible = false;
            this.mnuSegment_SeparateSection.Visible = false;
            this.mnuSegment_InsertAtSelectionAsPlainText.Visible = false;
            this.mnuSegment_RestartPageNumbering.Visible = false;
            this.mnuSegment_KeepHeadersFooters.Visible = false;
            this.mnuSegment_Sep2.Visible = false;
            this.mnuSegment_AttachTo.Visible = false;
            this.mnuSegment_InsertUsingSavedPrefill.Visible = false;
            this.mnuSegment_InsertUsingClientMatterLookup.Visible = false;
            this.mnuSegment_InsertUsingTransientPrefill.Visible = false;
            this.mnuSegment_Sep3.Visible = false;
            this.mnuSegment_ManageAuthorPreferences.Visible = false;
            this.mnuSegment_ApplyStyles.Visible = false;
            this.mnuSegment_CopyStyles.Visible = false;
            this.mnuSegment_StyleSummary.Visible = false;
            //GLOG : 7276 : jsw
            this.mnuSegment_CreateMasterData.Visible = false;
            this.mnuSegment_Sep4.Visible = false;
            this.mnuSegment_Clone.Visible = false;
            this.mnuSegment_ShowAssignments.Visible = false;
            this.mnuSegment_ConvertToAdminContent.Visible = false;
            this.mnuSegment_UpdateAdminContent.Visible = false;
            this.mnuSegment_Copy.Visible = true;
            this.mnuSegment_AddToFavorites.Visible = true;
            //GLOG : 7975 : jsw
            this.mnuSegment_RemoveFromFavorites.Visible = false;
            this.mnuSegment_Delete.Visible = true;
            this.mnuSegment_Share.Visible = true;
            this.mnuSegment_Sep5.Visible = false;
            this.mnuSegment_Design.Visible = false;
            this.mnuSegment_EditAnswerFile.Visible = false;
            this.mnuSegment_Properties.Visible = true;
            this.mnuSegment_HelpSep.Visible = true;
            this.mnuSegment_Help.Visible = true;

            mnuSegment_Insert.ForeColor = Color.Blue;
        }

        /// <summary>
        /// Display Segment menu items appropriate to the current context
        /// </summary>
        /// <param name="iID"></param>
        /// <param name="iFolderType"></param>
        private void SetAdminSegmentMenuOptions(int iID, mpFolderTypes iFolderType)
        {
            SetAdminSegmentMenuOptions(iID, iFolderType, false, false, false ,false, false, false);
        }
        private void SetAdminSegmentMenuOptions(int iID, mpFolderTypes iFolderType, bool bPrefillOnly, 
             bool bStyleSheetOnly, bool bOwnerFolder, bool bSharedFolder, bool bAnswerFileOnly, bool bIsSavedContent)
        {
            //glog - 3637 - CEH
            FirmApplicationSettings oSettings = Session.CurrentUser.FirmSettings;
            AdminSegmentDefs oDefs = new AdminSegmentDefs();
            AdminSegmentDef oDef = null;
            oDef = (AdminSegmentDef)oDefs.ItemFromID(iID);

            bool bIsExternalContent = (oDef.TypeID == mpObjectTypes.NonMacPacContent);
            bool bIsMasterData = oDef.TypeID == mpObjectTypes.MasterData;
            bool bIsLabelOrEnvelope = (oDef.TypeID == mpObjectTypes.Envelopes || oDef.TypeID == mpObjectTypes.Labels);
            Word.WdSelectionType iSelectionType = m_oMPDocument.WordDocument.Application.Selection.Type;
            bool bSelectionEnabled = (iSelectionType == Word.WdSelectionType.wdSelectionNormal) && !bAnswerFileOnly;

            //GLOG item #4242 - dcf
            string xTypeExclusions = "," + LMP.Data.Application.GetMetadata("TypeExclusions") + ",";
            bool bIsExcludedType = xTypeExclusions.Contains("," + ((int)oDef.TypeID).ToString() + ",");

            //GLOG 2988: Indicate Use Selection as default for Labels and Envelopes
            bool bDefaultUseSelection = (bSelectionEnabled && bIsLabelOrEnvelope);

            // Allow deleting Admin Segment if it's a copy in a User Folder
            mnuSegment_Delete.Visible = ((iFolderType == mpFolderTypes.User) || Session.AdminMode) && 
                this.treeContent.Visible && !bOwnerFolder && !bSharedFolder;
            // GLOG : 2928 : JAB
            // Display "Properties..." menu item for user content.
            //GLOG : 6072 : CEH 
            //display "Properties..." menu item for external content
			//GLOG : 7998 : ceh
            mnuSegment_Properties.Visible = ((bPrefillOnly && iFolderType == mpFolderTypes.User) || bIsSavedContent || bIsExternalContent) &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            //GLOG 2966: Allow editing Answer File data
			//GLOG : 7998 : ceh
            mnuSegment_EditAnswerFile.Visible = (bPrefillOnly && iFolderType == mpFolderTypes.User && oDef.IntendedUse == mpSegmentIntendedUses.AsAnswerFile 
                                                && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator);

            mnuSegment_Design.Visible = (Session.AdminMode && !bPrefillOnly && !bIsExternalContent) ||
                (!Session.AdminMode && Session.CurrentUser.IsContentDesigner && !bPrefillOnly && !bIsExternalContent);  //GLOG 6402

            //GLOG 6780: Visible for Master Data also
            mnuSegment_SaveAsUserSegment.Visible = !Session.AdminMode && !bAnswerFileOnly && 
                !bIsExternalContent && !bIsSavedContent && !bPrefillOnly && Session.CurrentUser.IsContentDesigner &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator; //GLOG 6402 & GLOG : 7998 : ceh

            //GLOG 6780: Visible for Master Data also
            mnuSegment_Export.Visible = !bAnswerFileOnly && !bIsExternalContent && !bPrefillOnly && !bIsSavedContent && //GLOG 6402
                 (Session.CurrentUser.IsContentDesigner || Session.AdminMode);

            //GLOG - 8248 - ceh (JTS 4/8/16: Disabled for now)
            //GLOG 8343 (dm) - reenabled
            mnuSegment_InsertAtSelectionAsPlainText.Visible = oDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText;
            if (oDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText)
                mnuSegment_InsertAtSelection.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertAtSelectionSourceFormatting");
            else
                mnuSegment_InsertAtSelection.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertAtSelection");

            //GLOG : 3870 : CEH
            //mnuSegment_ShowAssignments.Visible = (iFolderType == mpFolderTypes.Admin) &&
            //    !bAnswerFileOnly && !bIsExternalContent && !bIsMasterData &&
            //    (Session.CurrentUser.IsContentDesigner || Session.AdminMode);
            mnuSegment_ShowAssignments.Visible = false;
            // GLOG : 7975 : JSW
            mnuSegment_CreateMasterData.Visible = ((this.treeResults.Visible || this.treeContent.Visible) && (bIsMasterData));
            // GLOG : 5451 : CEH
			//GLOG : 7998 : ceh
            mnuSegment_Copy.Visible = (this.treeResults.Visible || this.treeContent.Visible) && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_Clone.Visible = (iFolderType == mpFolderTypes.Admin) && (Session.AdminMode) &&
                this.treeContent.Visible && !bPrefillOnly && !bOwnerFolder && !bSharedFolder && !bIsExternalContent;
            mnuSegment_Clone.Enabled = !bIsExcludedType;
            mnuSegment_ConvertToAdminContent.Visible = false;
            mnuSegment_UpdateAdminContent.Visible = false;
            mnuSegment_ApplyStyles.Visible = bStyleSheetOnly && !bIsExternalContent && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;  //GLOG : 7998 : ceh
            mnuSegment_CopyStyles.Visible = !bPrefillOnly && !bAnswerFileOnly && !bIsExternalContent && !bIsMasterData;
            mnuSegment_StyleSummary.Visible = bStyleSheetOnly && !bIsExternalContent && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;  //GLOG : 7998 : ceh

            mnuSegment_Sep3.Visible = !bStyleSheetOnly && !bAnswerFileOnly && !bIsExternalContent && !bIsMasterData;
            mnuSegment_Sep5.Visible = Session.AdminMode && this.treeContent.Visible && !bPrefillOnly && !bIsExternalContent;

            //GLOG : 6241 : CEH
            mnuSegment_Sep4.Visible = !bPrefillOnly && !bIsExternalContent && !bIsMasterData & !bAnswerFileOnly;
            mnuSegment_CreateExternalContent.Visible = bIsExternalContent && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;  //GLOG : 7998 : ceh

            //bold the default double-click/enter key down action
            Segment.InsertionLocations oDefaultLoc = (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation;
            if (bAnswerFileOnly)
                oDefaultLoc = Segment.InsertionLocations.Default;

            mnuSegment_Insert.ForeColor = (oDefaultLoc == 
                Segment.InsertionLocations.Default && !bDefaultUseSelection) ? Color.Blue : Color.Black;
            mnuSegment_InsertEndOfAllSections.ForeColor = (oDefaultLoc == 
                Segment.InsertionLocations.InsertAtEndOfAllSections && !bDefaultUseSelection) ? Color.Blue : Color.Black;
            mnuSegment_InsertEndOfThisSection.ForeColor = (oDefaultLoc == 
                Segment.InsertionLocations.InsertAtEndOfCurrentSection && !bDefaultUseSelection) ? Color.Blue : Color.Black;
            mnuSegment_InsertEndOfDocument.ForeColor = (oDefaultLoc ==
                Segment.InsertionLocations.InsertAtEndOfDocument && !bDefaultUseSelection) ? Color.Blue : Color.Black;
            mnuSegment_InsertAtSelection.ForeColor = (oDefaultLoc ==
                Segment.InsertionLocations.InsertAtSelection && !bDefaultUseSelection) ? Color.Blue : Color.Black;
            mnuSegment_InsertStartOfAllSections.ForeColor = (oDefaultLoc ==
                Segment.InsertionLocations.InsertAtStartOfAllSections && !bDefaultUseSelection) ? Color.Blue : Color.Black;
            mnuSegment_InsertStartOfThisSection.ForeColor = (oDefaultLoc == 
                Segment.InsertionLocations.InsertAtStartOfCurrentSection && !bDefaultUseSelection) ? Color.Blue : Color.Black;
            mnuSegment_InsertStartOfDocument.ForeColor = (oDefaultLoc ==
                Segment.InsertionLocations.InsertAtStartOfDocument && !bDefaultUseSelection) ? Color.Blue : Color.Black;
            mnuSegment_CreateNewDocument.ForeColor = (oDefaultLoc ==
                Segment.InsertionLocations.InsertInNewDocument && !bDefaultUseSelection) ? Color.Blue : Color.Black;

            // Only display the Default option if no other options are enabled
            mnuSegment_Insert.Visible = (((oDef.MenuInsertionOptions == 0 || oDef.MenuInsertionOptions 
                == (short)Segment.InsertionLocations.Default) && !bStyleSheetOnly) || bAnswerFileOnly) && !bIsExternalContent && !bIsMasterData &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_CreateNewDocument.Visible = (((oDef.MenuInsertionOptions &
                (short)Segment.InsertionLocations.InsertInNewDocument)
                == (short)Segment.InsertionLocations.InsertInNewDocument) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData) && !bIsExternalContent &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertAtSelection.Visible = (((oDef.MenuInsertionOptions &
                (short)Segment.InsertionLocations.InsertAtSelection)
                == (short)Segment.InsertionLocations.InsertAtSelection) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData) && !bIsExternalContent &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertStartOfDocument.Visible = (((oDef.MenuInsertionOptions &
                (short)Segment.InsertionLocations.InsertAtStartOfDocument)
                == (short)Segment.InsertionLocations.InsertAtStartOfDocument) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData) && !bIsExternalContent &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertEndOfDocument.Visible = (((oDef.MenuInsertionOptions &
                (short)Segment.InsertionLocations.InsertAtEndOfDocument)
                == (short)Segment.InsertionLocations.InsertAtEndOfDocument) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData) && !bIsExternalContent &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertStartOfAllSections.Visible = (((oDef.MenuInsertionOptions &
                (short)Segment.InsertionLocations.InsertAtStartOfAllSections)
                == (short)Segment.InsertionLocations.InsertAtStartOfAllSections) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData) && !bIsExternalContent &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertEndOfAllSections.Visible = (((oDef.MenuInsertionOptions &
                (short)Segment.InsertionLocations.InsertAtEndOfAllSections)
                == (short)Segment.InsertionLocations.InsertAtEndOfAllSections) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData) && !bIsExternalContent &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertStartOfThisSection.Visible = (((oDef.MenuInsertionOptions &
                (short)Segment.InsertionLocations.InsertAtStartOfCurrentSection)
                == (short)Segment.InsertionLocations.InsertAtStartOfCurrentSection) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData) && !bIsExternalContent &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertEndOfThisSection.Visible = (((oDef.MenuInsertionOptions &
                (short)Segment.InsertionLocations.InsertAtEndOfCurrentSection)
                == (short)Segment.InsertionLocations.InsertAtEndOfCurrentSection) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData) && !bIsExternalContent &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_Sep2.Visible = !bStyleSheetOnly && !bIsExternalContent && !bIsMasterData &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_AttachTo.Visible = !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData && !bIsExternalContent &&
                oDef.TypeID != mpObjectTypes.Trailer && oDef.TypeID != mpObjectTypes.DraftStamp && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertUsingSavedPrefill.Visible = !bPrefillOnly && !bStyleSheetOnly && !bIsMasterData && !bIsExternalContent && !bIsSavedContent &&
                 oDef.TypeID != mpObjectTypes.Trailer && oDef.TypeID != mpObjectTypes.DraftStamp &&
                 !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertUsingClientMatterLookup.Visible = !bPrefillOnly && !bStyleSheetOnly && !bIsMasterData && !bIsExternalContent && !bIsSavedContent &&
                 oDef.TypeID != mpObjectTypes.Trailer && oDef.TypeID != mpObjectTypes.DraftStamp && LMP.Grail.GrailStore.IsConfigured &&
                 !LMP.MacPac.MacPacImplementation.IsToolsAdministrator; //GLOG 7127
            mnuSegment_InsertMultipleusingSavedData.Visible = bPrefillOnly && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_EditSavedData.Visible = bIsMasterData && bPrefillOnly && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertUsingTransientPrefill.Visible = !bPrefillOnly && !bStyleSheetOnly && !bIsMasterData && !bIsExternalContent && !bIsSavedContent &&
                 oDef.TypeID != mpObjectTypes.Trailer && oDef.TypeID != mpObjectTypes.DraftStamp && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_Sep3.Visible = oDef.TypeID != mpObjectTypes.Trailer && oDef.TypeID != mpObjectTypes.DraftStamp && oDef.TypeID != mpObjectTypes.MasterData &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            //glog - 3637 - CEH
            mnuSegment_Share.Visible = (bPrefillOnly || bIsSavedContent) && !bOwnerFolder && !bSharedFolder && !bIsExternalContent && oSettings.AllowSharedSegments &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_ManageAuthorPreferences.Visible = (!bPrefillOnly && !bAnswerFileOnly && !bIsMasterData && oDef.IntendedUse != mpSegmentIntendedUses.AsStyleSheet &&
                oDef.ContainsAuthorPreferences) && !bIsExternalContent && !bIsSavedContent && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_InsertUsingSelection.Visible = (oDef.TypeID == mpObjectTypes.Envelopes || oDef.TypeID == mpObjectTypes.Labels) && !bPrefillOnly &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator; //GLOG 6635
            //GLOG : 7783 : JSW //use Node.Text property instead of oDef.DisplayName, which does not have full name for Saved Data.
			//GLOG : 6662 : JSW
            mnuSegment_AddToFavorites.Visible = !IsFavorite(m_oCurNode.Text) && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            //GLOG : 7276 : JSW
            mnuSegment_RemoveFromFavorites.Visible = IsFavorite(m_oCurNode.Text) && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;

            iSelectionType = m_oMPDocument.WordDocument.Application.Selection.Type;

            bSelectionEnabled = (iSelectionType == Word.WdSelectionType.wdSelectionNormal) && !bAnswerFileOnly && !bIsMasterData;
            bool bOptionsAvailable = bSelectionEnabled;

            if (bSelectionEnabled && !bIsExternalContent && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
            {
                bSelectionEnabled = String.XMLContainsBodyBlock(oDef.XML);
                mnuSegment_SelectionAsBody.Visible = (bSelectionEnabled && !bStyleSheetOnly);
            }
            else
            {
                mnuSegment_SelectionAsBody.Visible = false;
            }
            // Not validating that these menu items are appropriate for the
            // current segment's Insertion options.  It is assumed that Admin UI
            // will enforce validity.
            //GLOG - 3590 - CEH
            if (((oDef.DefaultMenuInsertionBehavior &
                (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection)
                == (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection || 
                ((oDef.DefaultMenuInsertionBehavior &
                (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection)
                == (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection) && !bStyleSheetOnly && !bAnswerFileOnly && !bIsMasterData && !bIsExternalContent) &&
                !LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
            {
                mnuSegment_SeparateSection.Visible = true;
                mnuSegment_SeparateSection.Checked = oDef.IntendedUse == mpSegmentIntendedUses.AsDocument;
                mnuSegment_Sep1.Visible = true;
                bOptionsAvailable = true;
                mnuSegment_SeparateSection.Enabled = !bIsLabelOrEnvelope; //GLOG 7938 (dm)
            }
            else
                mnuSegment_SeparateSection.Visible = false;

            if ((oDef.DefaultMenuInsertionBehavior &
                (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters)
                == (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters && !bStyleSheetOnly &&
                    !bAnswerFileOnly && !bIsMasterData && !bIsExternalContent && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
            {
                mnuSegment_KeepHeadersFooters.Visible = true;
                mnuSegment_KeepHeadersFooters.Checked = oDef.IntendedUse != mpSegmentIntendedUses.AsDocument;
                mnuSegment_Sep1.Visible = true;
                mnuSegment_KeepHeadersFooters.Enabled = mnuSegment_SeparateSection.Checked;
                bOptionsAvailable = true;
            }
            else
                mnuSegment_KeepHeadersFooters.Visible = false;

			//GLOG : 7998 : ceh
            if ((oDef.DefaultMenuInsertionBehavior &
                (short)Segment.InsertionBehaviors.RestartPageNumbering)
                == (short)Segment.InsertionBehaviors.RestartPageNumbering && !bStyleSheetOnly && !bAnswerFileOnly && !bIsExternalContent && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator)
            {
                mnuSegment_RestartPageNumbering.Visible = true;
                mnuSegment_RestartPageNumbering.Checked = oDef.IntendedUse == mpSegmentIntendedUses.AsDocument;
                mnuSegment_Sep1.Visible = true;
                mnuSegment_RestartPageNumbering.Enabled = mnuSegment_SeparateSection.Checked;
                bOptionsAvailable = true;
            }
            else
                mnuSegment_RestartPageNumbering.Visible = false;

            // Hide separator if none of the following options nor Selected Text option is visible
            mnuSegment_Sep1.Visible = ((bPrefillOnly || (!bStyleSheetOnly &&
                !bAnswerFileOnly && !bIsMasterData && !bIsExternalContent)) && bOptionsAvailable) || bIsMasterData;
        }
        /// <summary>
        /// Set UserSegment Menu options appropriate to the current context
        /// </summary>
        /// <param name="bOwnsSegment"></param>
        /// <param name="bOwnsParent"></param>
        private void SetUserSegmentMenuOptions(bool bOwnsSegment, bool bOwnsParent, 
            bool bPrefillOnly, bool bStyleSheetOnly, bool bOwnerFolder, bool bSharedFolder, bool bIsSavedContent)
        {
            UserSegmentDefs oDefs = new UserSegmentDefs(
                mpUserSegmentsFilterFields.User, Session.CurrentUser.ID);
            UserSegmentDef oDef = null;

            if (this.treeResults.Visible)
            {
                oDef = (UserSegmentDef)m_oCurFindNode.Tag;
            }
            else if (this.m_oCurNode.Tag is FolderMember)
            {
                FolderMember oMember = (FolderMember)this.m_oCurNode.Tag;

                if (oMember.ObjectTypeID == mpFolderMemberTypes.VariableSet)
                {
                    //folder member is a variable set -
                    //get the definition of the target user segment
                    string xID = oMember.ObjectID1.ToString() + "." + oMember.ObjectID2.ToString();
                    VariableSetDefs oVarSetDefs = new VariableSetDefs(mpVariableSetsFilterFields.User,
                        LMP.Data.Application.User.ID);
                    VariableSetDef oVarSetDef = (VariableSetDef)oVarSetDefs.ItemFromID(xID);
                    oDef = (UserSegmentDef)oDefs.ItemFromID(oVarSetDef.SegmentID);
                }
                else
                {
                    string xID = oMember.ObjectID1.ToString() + "." + oMember.ObjectID2.ToString();
                    oDef = (UserSegmentDef)oDefs.ItemFromID(xID);
                }
            }
            else
            {
                oDef = (UserSegmentDef)m_oCurNode.Tag;
            }

            bool bIsUserPacket = oDef.TypeID == mpObjectTypes.SegmentPacket;
            bool bIsMasterData = oDef.TypeID == mpObjectTypes.MasterData;
            bool bIsDesignerContent = ContentTreeHelper.IsDesignerSegmentNode(this.m_oCurNode); //GLOG 8376

            //glog - 3637 - CEH
            FirmApplicationSettings oSettings = Session.CurrentUser.FirmSettings;
                        
            // User Segment type
            mnuSegment_Insert.Visible = bIsUserPacket;
            mnuSegment_InsertMultipleusingSavedData.Visible = bPrefillOnly;
            mnuSegment_EditSavedData.Visible = bPrefillOnly && bIsMasterData;
            mnuSegment_CreateExternalContent.Visible = false;
            mnuSegment_InsertAtSelection.Visible = !bStyleSheetOnly && !bIsUserPacket;
            mnuSegment_CreateNewDocument.Visible = !bStyleSheetOnly && !bIsUserPacket;
            mnuSegment_InsertEndOfDocument.Visible = false;
            mnuSegment_InsertStartOfDocument.Visible = false;
            mnuSegment_InsertStartOfThisSection.Visible = false;
            mnuSegment_InsertStartOfAllSections.Visible = false;
            mnuSegment_InsertEndOfAllSections.Visible = false;
            mnuSegment_InsertEndOfThisSection.Visible = false;
            mnuSegment_Sep1.Visible = !bPrefillOnly;
            mnuSegment_InsertUsingSavedPrefill.Visible = !bPrefillOnly && !bStyleSheetOnly && !bIsSavedContent;
            mnuSegment_InsertUsingClientMatterLookup.Visible = !bPrefillOnly && !bStyleSheetOnly && 
                !bIsSavedContent && LMP.Grail.GrailStore.IsConfigured;

            //GLOG - 8248 - ceh (JTS 4/8/16: Disabled for now)
            //GLOG 8343 (dm) - reenabled
            mnuSegment_InsertAtSelectionAsPlainText.Visible = oDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText;
            if (oDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText)
                mnuSegment_InsertAtSelection.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertAtSelectionSourceFormatting");
            else
                mnuSegment_InsertAtSelection.Text = LMP.Resources.GetLangString("Menu_Content_Segment_InsertAtSelection");

            mnuSegment_AttachTo.Visible = !bStyleSheetOnly && !bIsUserPacket;
            mnuSegment_Sep2.Visible = !bStyleSheetOnly;
            mnuSegment_SeparateSection.Visible = false;
            mnuSegment_SelectionAsBody.Visible = false;
            mnuSegment_KeepHeadersFooters.Visible = false;
            mnuSegment_RestartPageNumbering.Visible = false;
            mnuSegment_Sep3.Visible = false;
            mnuSegment_Sep4.Visible = bPrefillOnly || (!bIsUserPacket && !bStyleSheetOnly && !bIsSavedContent);
            mnuSegment_Delete.Visible = bOwnsParent && this.treeContent.Visible && !bOwnerFolder && !bSharedFolder;
            // GLOG : 7976 : JSW
            mnuSegment_CreateMasterData.Visible = bIsMasterData;
            //GLOG : 5451 : ceh
            mnuSegment_Copy.Visible = this.treeResults.Visible || this.treeContent.Visible;
            //GLOG : 3637 : ceh
			//GLOG : 7998 : ceh
            mnuSegment_Share.Visible = bOwnsSegment && !bOwnerFolder && !bSharedFolder && oSettings.AllowSharedSegments && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator ;
            mnuSegment_Clone.Visible = false;
            mnuSegment_ConvertToAdminContent.Visible = this.treeContent.Visible && 
                !bPrefillOnly && (bSharedFolder || bOwnerFolder) && !bIsUserPacket && LMP.MacPac.Session.AdminMode;
            mnuSegment_UpdateAdminContent.Visible = this.treeContent.Visible &&
                !bPrefillOnly && (bSharedFolder || bOwnerFolder) && !bIsUserPacket && LMP.MacPac.Session.AdminMode;
            mnuSegment_Sep5.Visible = bOwnsSegment && !bPrefillOnly;
            mnuSegment_Properties.Visible = true;//bPrefillOnly; // bOwnsParent;
            mnuSegment_Design.Visible = bOwnsSegment && !bPrefillOnly;
            mnuSegment_ApplyStyles.Visible = bStyleSheetOnly;
            mnuSegment_CopyStyles.Visible = !bPrefillOnly && !bIsUserPacket;
            mnuSegment_StyleSummary.Visible = bStyleSheetOnly;
            mnuSegment_ManageAuthorPreferences.Visible = false;
            mnuSegment_InsertUsingTransientPrefill.Visible = !bPrefillOnly && !bStyleSheetOnly && !bIsSavedContent;
            mnuSegment_InsertUsingSelection.Visible = false;
            mnuSegment_EditAnswerFile.Visible = false;
            //GLOG : 3870 : CEH
            mnuSegment_ShowAssignments.Visible = false;
            mnuSegment_SaveAsUserSegment.Visible = false;
            mnuSegment_Export.Visible = bOwnsSegment && !bOwnerFolder && !bSharedFolder && !bIsUserPacket && !bPrefillOnly &&
                ((Session.CurrentUser.IsContentDesigner && bIsDesignerContent) || Session.AdminMode); //GLOG 8376
            //GLOG : 7975 : JSW
            mnuSegment_CreateMasterData.Visible = bIsMasterData;
            //GLOG : 7767 : JSW 
            mnuSegment_AddToFavorites.Visible = !IsFavorite(m_oCurNode.Text) && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;
            mnuSegment_RemoveFromFavorites.Visible = IsFavorite(m_oCurNode.Text) && !LMP.MacPac.MacPacImplementation.IsToolsAdministrator;

            if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocument)
            {
                mnuSegment_InsertAtSelection.ForeColor = Color.Black;
                mnuSegment_CreateNewDocument.ForeColor = Color.Blue;
            }
            else if (oDef.IntendedUse == mpSegmentIntendedUses.AsDocumentComponent ||
                oDef.IntendedUse == mpSegmentIntendedUses.AsParagraphText ||
                oDef.IntendedUse == mpSegmentIntendedUses.AsSentenceText)
            {
                mnuSegment_InsertAtSelection.ForeColor = Color.Blue;
                mnuSegment_CreateNewDocument.ForeColor = Color.Black;
            }
            //GLOG 8376: Display Location and Behavior options for Designer Segments
            if (bIsDesignerContent)
            {
                bool bIsLabelOrEnvelope = (oDef.TypeID == mpObjectTypes.Envelopes || oDef.TypeID == mpObjectTypes.Labels);
                Word.WdSelectionType iSelectionType = m_oMPDocument.WordDocument.Application.Selection.Type;
                bool bSelectionEnabled = (iSelectionType == Word.WdSelectionType.wdSelectionNormal);

                //GLOG 2988: Indicate Use Selection as default for Labels and Envelopes
                bool bDefaultUseSelection = (bSelectionEnabled && bIsLabelOrEnvelope);
                //bold the default double-click/enter key down action
                Segment.InsertionLocations oDefaultLoc = (Segment.InsertionLocations)oDef.DefaultDoubleClickLocation;

                mnuSegment_Insert.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.Default && !bDefaultUseSelection) ? Color.Blue : Color.Black;
                mnuSegment_InsertEndOfAllSections.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.InsertAtEndOfAllSections && !bDefaultUseSelection) ? Color.Blue : Color.Black;
                mnuSegment_InsertEndOfThisSection.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.InsertAtEndOfCurrentSection && !bDefaultUseSelection) ? Color.Blue : Color.Black;
                mnuSegment_InsertEndOfDocument.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.InsertAtEndOfDocument && !bDefaultUseSelection) ? Color.Blue : Color.Black;
                mnuSegment_InsertAtSelection.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.InsertAtSelection && !bDefaultUseSelection) ? Color.Blue : Color.Black;
                mnuSegment_InsertStartOfAllSections.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.InsertAtStartOfAllSections && !bDefaultUseSelection) ? Color.Blue : Color.Black;
                mnuSegment_InsertStartOfThisSection.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.InsertAtStartOfCurrentSection && !bDefaultUseSelection) ? Color.Blue : Color.Black;
                mnuSegment_InsertStartOfDocument.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.InsertAtStartOfDocument && !bDefaultUseSelection) ? Color.Blue : Color.Black;
                mnuSegment_CreateNewDocument.ForeColor = (oDefaultLoc ==
                    Segment.InsertionLocations.InsertInNewDocument && !bDefaultUseSelection) ? Color.Blue : Color.Black;

                // Only display the Default option if no other options are enabled
                mnuSegment_Insert.Visible = ((oDef.MenuInsertionOptions == 0 || oDef.MenuInsertionOptions
                    == (short)Segment.InsertionLocations.Default) && !bStyleSheetOnly);
                mnuSegment_CreateNewDocument.Visible = (((oDef.MenuInsertionOptions &
                    (short)Segment.InsertionLocations.InsertInNewDocument)
                    == (short)Segment.InsertionLocations.InsertInNewDocument) && !bStyleSheetOnly);
                mnuSegment_InsertAtSelection.Visible = (((oDef.MenuInsertionOptions &
                    (short)Segment.InsertionLocations.InsertAtSelection)
                    == (short)Segment.InsertionLocations.InsertAtSelection) && !bStyleSheetOnly);
                mnuSegment_InsertStartOfDocument.Visible = (((oDef.MenuInsertionOptions &
                    (short)Segment.InsertionLocations.InsertAtStartOfDocument)
                    == (short)Segment.InsertionLocations.InsertAtStartOfDocument) && !bStyleSheetOnly);
                mnuSegment_InsertEndOfDocument.Visible = (((oDef.MenuInsertionOptions &
                    (short)Segment.InsertionLocations.InsertAtEndOfDocument)
                    == (short)Segment.InsertionLocations.InsertAtEndOfDocument) && !bStyleSheetOnly);
                mnuSegment_InsertStartOfAllSections.Visible = (((oDef.MenuInsertionOptions &
                    (short)Segment.InsertionLocations.InsertAtStartOfAllSections)
                    == (short)Segment.InsertionLocations.InsertAtStartOfAllSections) && !bStyleSheetOnly);
                mnuSegment_InsertEndOfAllSections.Visible = (((oDef.MenuInsertionOptions &
                    (short)Segment.InsertionLocations.InsertAtEndOfAllSections)
                    == (short)Segment.InsertionLocations.InsertAtEndOfAllSections) && !bStyleSheetOnly);
                mnuSegment_InsertStartOfThisSection.Visible = (((oDef.MenuInsertionOptions &
                    (short)Segment.InsertionLocations.InsertAtStartOfCurrentSection)
                    == (short)Segment.InsertionLocations.InsertAtStartOfCurrentSection) && !bStyleSheetOnly);
                mnuSegment_InsertEndOfThisSection.Visible = (((oDef.MenuInsertionOptions &
                    (short)Segment.InsertionLocations.InsertAtEndOfCurrentSection)
                    == (short)Segment.InsertionLocations.InsertAtEndOfCurrentSection) && !bStyleSheetOnly);

                bool bOptionsAvailable = bSelectionEnabled;

                if (bSelectionEnabled)
                {
                    bSelectionEnabled = String.XMLContainsBodyBlock(oDef.XML);
                    mnuSegment_SelectionAsBody.Visible = (bSelectionEnabled && !bStyleSheetOnly);
                }
                else
                {
                    mnuSegment_SelectionAsBody.Visible = false;
                }
                mnuSegment_InsertUsingSelection.Visible = (oDef.TypeID == mpObjectTypes.Envelopes || oDef.TypeID == mpObjectTypes.Labels);

                // Not validating that these menu items are appropriate for the
                // current segment's Insertion options.  It is assumed that Admin UI
                // will enforce validity.
                if (((oDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection)
                    == (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection ||
                    ((oDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection)
                    == (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection) && !bStyleSheetOnly))
                {
                    mnuSegment_SeparateSection.Visible = true;
                    mnuSegment_SeparateSection.Checked = oDef.IntendedUse == mpSegmentIntendedUses.AsDocument;
                    mnuSegment_Sep1.Visible = true;
                    bOptionsAvailable = true;
                    mnuSegment_SeparateSection.Enabled = !bIsLabelOrEnvelope;
                }
                else
                    mnuSegment_SeparateSection.Visible = false;

                if ((oDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters)
                    == (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters && !bStyleSheetOnly)
                {
                    mnuSegment_KeepHeadersFooters.Visible = true;
                    mnuSegment_KeepHeadersFooters.Checked = oDef.IntendedUse != mpSegmentIntendedUses.AsDocument;
                    mnuSegment_Sep1.Visible = true;
                    mnuSegment_KeepHeadersFooters.Enabled = mnuSegment_SeparateSection.Checked;
                    bOptionsAvailable = true;
                }
                else
                    mnuSegment_KeepHeadersFooters.Visible = false;

                if ((oDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.RestartPageNumbering)
                    == (short)Segment.InsertionBehaviors.RestartPageNumbering && !bStyleSheetOnly)
                {
                    mnuSegment_RestartPageNumbering.Visible = true;
                    mnuSegment_RestartPageNumbering.Checked = oDef.IntendedUse == mpSegmentIntendedUses.AsDocument;
                    mnuSegment_Sep1.Visible = true;
                    mnuSegment_RestartPageNumbering.Enabled = mnuSegment_SeparateSection.Checked;
                    bOptionsAvailable = true;
                }
                else
                    mnuSegment_RestartPageNumbering.Visible = false;
                // Hide separator if none of the following options nor Selected Text option is visible
                mnuSegment_Sep1.Visible = bStyleSheetOnly || bOptionsAvailable;
                //GLOG : 8506 : jsw
                mnuSegment_Sep3.Visible = !bStyleSheetOnly && !bIsUserPacket;
                mnuSegment_Sep2.Visible = !bStyleSheetOnly;
                mnuSegment_Sep4.Visible = true;
            }
        }
        private void DisplayContextMenu(UltraTreeNode oNode, bool bUseTimer)
        {
            int iX = oNode.UIElement.Rect.Left;
            int iY = oNode.UIElement.Rect.Top;
            DisplayContextMenu(oNode, bUseTimer, iX, iY);
        }
        private void DisplayContextMenu(UltraTreeNode oNode, bool bUseTimer, int iX, int iY)
        {
            if (oNode == null)
                return;

            ContentTreeHelper.LoadNodeReference(oNode);

            if (oNode.Tag is FolderMember || oNode.Tag is UserSegmentDef || oNode.Tag is AdminSegmentDef || oNode.Tag is VariableSetDef)
            {
                mnuSegment.UseTimer = bUseTimer;

                if (!m_bMenuShownAlready)
                {
                    //show menu off-screen - we do this
                    //because the first time the menu is
                    //displayed, it is incorrectly positioned -
                    //this appears to be a bug with the .Show method
                    mnuSegment.Show(-1000,-1000);
                    m_bMenuShownAlready = true;
                }

                mnuSegment.Show(oNode.Control, new System.Drawing.Point(iX, iY));
            }
            else if (ContentTreeHelper.IsUserFolderNode(oNode) || ContentTreeHelper.IsAdminFolderNode(oNode))
            {
                //mnuFolder.Capture = true;
                //mnuFolder.BringToFront();
                mnuFolder.UseTimer = bUseTimer;
                mnuFolder.Show(oNode.Control, new System.Drawing.Point(iX, iY));
                mnuFolder.Items[0].Select();
            }
        }
        private void mnuFolder_AddFile_Click(object sender, EventArgs e)
        {
			//GLOG : 6072 : CEH
            //prompt to select an external file
            ExternalContentForm oForm = new ExternalContentForm();
            oForm.ShowForm(ExternalContentForm.DialogMode.NewExternalContent);
            if ( oForm.DialogResult == DialogResult.OK)
            {
                //add selected external content
                UltraTreeNode oNode = this.treeContent.SelectedNodes[0];

                if (oNode != null)
                {
                    if (oNode.Tag is Folder)
                    {
                        Folder oFolder = (Folder)oNode.Tag;
                        AddFileNode(oFolder, oForm.FullFileName,
                            oForm.DisplayName, oForm.Help);
                    }
                }
            }
        }
        /// <summary>
        /// adds the specified external content file
        /// to the specified folder
        /// </summary>
        /// <param name="oFolder"></param>
        /// <param name="xFullFileName"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="xHelpText"></param>
        private void AddFileNode(Folder oFolder, string xFullFileName, string xDisplayName, string xHelpText)
        {
            AdminSegmentDefs oDefs = new AdminSegmentDefs(); 
            AdminSegmentDef oDef = (AdminSegmentDef)oDefs.Create(mpObjectTypes.NonMacPacContent);
            
            oDef.TypeID = LMP.Data.mpObjectTypes.NonMacPacContent;
		    oDef.Name  = xDisplayName;
		    oDef.DisplayName  = xDisplayName;
		    oDef.XML = xFullFileName;
		    oDef.HelpText = xHelpText;
		    oDef.IntendedUse = mpSegmentIntendedUses.AsDocument;
            oDef.ChildSegmentIDs = "";

            oDefs.Save((LongIDSimpleDataItem)oDef);

            FolderMember oMember = (FolderMember)(oFolder.GetMembers().Create());
            oMember.ObjectID1 = oDef.ID;
            oFolder.GetMembers().Save((StringIDSimpleDataItem)oMember);

            RefreshFolderNode(m_oCurNode);
        }

        //GLOG : 6072 : CEH
        /// <summary>
        /// Edits the specified external content file
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="xFullFileName"></param>
        /// <param name="xDisplayName"></param>
        /// <param name="xHelpText"></param>
        private void EditFileNode(UltraTreeNode oNode, string xFullFileName, string xDisplayName, string xHelpText)
        {
            FolderMember oMember = (FolderMember)oNode.Tag;

            AdminSegmentDefs oDefs = new AdminSegmentDefs(mpObjectTypes.NonMacPacContent);
            AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(oMember.ObjectID1);

            oDef.Name = xDisplayName;
            oDef.DisplayName = xDisplayName;
            oDef.XML = xFullFileName;
            oDef.HelpText = xHelpText;

            oDefs.Save((LongIDSimpleDataItem)oDef);
            m_oCurNode = oNode.Parent;
            RefreshFolderNode(m_oCurNode);
        }

        private bool IsExternalContentNode(UltraTreeNode oNode)
        {
            if (oNode.Tag is FolderMember)// AdminSegmentDef)
            {
                FolderMember oMember = (FolderMember)(oNode.Tag);

                if (oMember.ObjectTypeID == mpFolderMemberTypes.AdminSegment)
                {
                    int iAdminSegmentID = oMember.ObjectID1;
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(iAdminSegmentID);
                    return (oDef.TypeID == mpObjectTypes.NonMacPacContent);
                }
            }
            else if (oNode.Tag is AdminSegmentDef)
            {
                return (((AdminSegmentDef)oNode.Tag).TypeID == mpObjectTypes.NonMacPacContent);
            }
            return false;
        }
        /// <summary>
        /// True if a context menu is displayed
        /// </summary>
        /// <returns></returns>
        internal bool MenuIsVisible()
        {
            return (mnuSegment.Visible || mnuFolder.Visible || 
                     mnuSearch.Visible || mnuQuickHelp.Visible);
        }
        private AdminSegmentDef GetAdminSegDefFromNode(UltraTreeNode oNode)
        {
            if (oNode.Tag is FolderMember)
            {
                FolderMember oMember = (FolderMember)(oNode.Tag);

                if (oMember.ObjectTypeID == mpFolderMemberTypes.AdminSegment)
                {
                    int iAdminSegmentID = oMember.ObjectID1;
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    return (AdminSegmentDef)oDefs.ItemFromID(iAdminSegmentID);
                }
            }
            else if (oNode.Tag is AdminSegmentDef)
            {
                return (AdminSegmentDef)oNode.Tag;
            }

            return null;
        }
        /// <summary>
        /// creates a new document based on the external content
        /// referenced by the specified node
        /// </summary>
        /// <param name="oNode"></param>
        private void CreateExternalContent(UltraTreeNode oNode)
        {
            AdminSegmentDef oDef = GetAdminSegDefFromNode(oNode);

            if (oDef != null)
            {
                TaskPane.CreateExternalContent(oDef.ID);
            }
        }
        /// <summary>
        /// Displays Data Interview dialog to edit previously saved Answer File
        /// </summary>
        /// <param name="oNode"></param>
        private void EditAnswerFileData(UltraTreeNode oNode)
        {
            //GLOG 2966: Display Data Interview dialog to edit existing saved data values
            if (ContentTreeHelper.IsVariableSetNode(oNode) && ContentTreeHelper.GetSegmentDefFromNode(oNode).IntendedUse == mpSegmentIntendedUses.AsAnswerFile)
            {
                VariableSetDef oDef = GetVariableSetFromNode(oNode);
                if (oDef != null)
                    InsertAnswerFile(oDef.SegmentID, oDef);
            }
        }
        /// <summary>
        /// Move focus to the Find text box.
        /// </summary>
        public void ActivateFind()
        {
            this.txtFindContent.Select();
        }
#endregion

        private void mnuFolder_Help_Click(object sender, EventArgs e)
        {
            this.ShowHelp();
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Display the content mgr's context menu when the picture control 
        /// is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picMenu_Click(object sender, EventArgs e)
        {
            // GLOG : 3061 : JAB
            // Handle clicking of the Menu button within the Find results tree.
            if (this.treeResults.Visible)
            {
                if (this.treeResults.SelectedNodes.Count > 0)
                {
                    this.DisplayContextMenu(this.treeResults.SelectedNodes[0], true);
                }
            }
            else
            {
                if (this.treeContent.SelectedNodes.Count > 0)
                {
                    this.DisplayContextMenu(this.treeContent.SelectedNodes[0], true);
                }
            }
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Position the content mgr menu picture control appropriately if the treeContent
        /// control has been resized.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlContentTree_Resize(object sender, EventArgs e)
        {
            // GLOG : 3061 : JAB
            // Display the context menu button accordingly depending on
            // whether the find results tree or the content tree is visible.
            if (treeContent.Visible)
            {
                if (this.treeContent.SelectedNodes.Count > 0)
                {
                    DisplayContextMenuButton(this.treeContent.SelectedNodes[0]);
                }
            }
            else
            {
                if (this.treeResults.SelectedNodes.Count > 0 && !IsSegmentPathNode(this.treeResults.SelectedNodes[0]))
                {
                    DisplayContextMenuButton(this.treeResults.SelectedNodes[0]);
                }
            }
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Hide the content mgr menu picture control if the node is not
        /// visible and display it scrolling is complete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_Scroll(object sender, TreeScrollEventArgs e)
        {
            if (this.treeContent.SelectedNodes.Count > 0)
            {
                if (!this.treeContent.SelectedNodes[0].IsInView)
                {
                    this.picMenuDropdown.Visible = false;
                }
                else
                {
                    if (MouseButtons == MouseButtons.None)
                    {
                        DisplayContextMenuButton(this.treeContent.SelectedNodes[0]);
                    }
                    else
                    {
                        this.picMenuDropdown.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// GLOG : 2918 : JAB
        /// Make the Content Mgr Menu Picture control visible once the mouse is released.
        /// This picture control was made not visible to handle cases where the user is
        /// scrolling. If the picture control were visible it would be located outside
        /// of the treeContent control during scrolling.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeContent_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.treeContent.SelectedNodes.Count > 0)
            {
                if (!this.treeContent.SelectedNodes[0].IsInView)
                {
                    this.picMenuDropdown.Visible = false;
                }
                //else
                //{
                //    DisplayContextMenuButton(this.treeContent.SelectedNodes[0]);
                //}
            }          
        }

        // GLOG : 3061 : JAB
        // Display the Menu buttons appropriately in the treeResults as 
        // is scrolls.
        private void treeResults_Scroll(object sender, TreeScrollEventArgs e)
        {
            if (this.treeResults.SelectedNodes.Count > 0)
            {
                if (!this.treeResults.SelectedNodes[0].IsInView)
                {
                    this.picMenuDropdown.Visible = false;
                }
                else
                {
                    if (MouseButtons == MouseButtons.None)
                    {
                        DisplayContextMenuButton(this.treeResults.SelectedNodes[0]);
                    }
                    else
                    {
                        this.picMenuDropdown.Visible = false;
                    }
                }
            }
        }

        private void btnFindContentAdvanced_Click(object sender, EventArgs e)
        {
            //GLOG 8037: Advanced Find
            try
            {
                ContentSearchForm oForm = new ContentSearchForm();
                if (this.txtFindContent.Text != "")
                    oForm.SearchText = this.txtFindContent.Text;
                if (oForm.ShowDialog() == DialogResult.OK)
                {
                    if (oForm.SearchText == "" && oForm.SearchFolder == ContentSearchForm.mpSearchLocations.AllFolders &&
                        oForm.OwnerID == 0 && oForm.ContentType == ContentSearchForm.mpSearchContentType.AllContent)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_NoSearchCriteriaSelected"),
                            LMP.ComponentProperties.ProductName,
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    string xSearchText = oForm.SearchText;
                    if (xSearchText == "")
                        xSearchText = " "; //Set Search Text to non-empty value to use other search criteria
                    int iOwnerID = oForm.OwnerID;
                    mpSegmentFindTypes iTypes = 0;
                    mpSegmentFindFields iFields = 0;
                    string xFolderIDList = "";
                    bool bIncludeAdminContent = true;
                    bool bIncludeUserContent = true;
                    if (oForm.SearchFolder != ContentSearchForm.mpSearchLocations.AllFolders)
                    {
                        UltraTreeNode oNode = this.treeContent.ActiveNode;
                        if (oNode != null)
                        {
                            ContentTreeHelper.LoadNodeReference(oNode);
                            //If My Folders or Public node is selected, include all user or admin content
                            if (ContentTreeHelper.IsPublicFolderNode(oNode))
                            {
                                bIncludeUserContent = false;
                            }
                            else if (ContentTreeHelper.IsMyFolderNode(oNode))
                            {
                                //Search all User Folders
                                xFolderIDList = this.GetSubFolderIDs(oNode);
                            }
                            else if (oForm.SearchFolder == ContentSearchForm.mpSearchLocations.SelectedFolder)
                            {
                                if (ContentTreeHelper.IsUserFolderNode(oNode))
                                {
                                    xFolderIDList = ((UserFolder)oNode.Tag).ID;
                                    bIncludeAdminContent = false;
                                }
                                else if (ContentTreeHelper.IsAdminFolderNode(oNode))
                                {
                                    xFolderIDList = ((Folder)oNode.Tag).ID.ToString();
                                    bIncludeUserContent = false;
                                }
                                else if (oNode.Tag is FolderMember || oNode.Tag is UserSegmentDef || oNode.Tag is AdminSegmentDef || oNode.Tag is VariableSetDef)
                                {
                                    oNode = oNode.Parent;
                                    if (oNode.Tag is Folder)
                                    {
                                        xFolderIDList = ((Folder)oNode.Tag).ID.ToString();
                                        bIncludeUserContent = false;
                                    }
                                    else if (oNode.Tag is UserFolder)
                                    {
                                        xFolderIDList = ((UserFolder)oNode.Tag).ID;
                                        bIncludeAdminContent = false;
                                    }
                                }
                            }
                            else if (oForm.SearchFolder == ContentSearchForm.mpSearchLocations.SelectedFolderAndSubfolders)
                            {
                                xFolderIDList = this.GetSubFolderIDs(oNode);
                                if (xFolderIDList.Contains("."))
                                    bIncludeAdminContent = false;
                                else
                                    bIncludeUserContent = false;
                            }
                        }
                    }
                    switch (oForm.ContentType)
                    {
                        case ContentSearchForm.mpSearchContentType.Segments:
                            if (iOwnerID > 0)
                            {
                                //Only User Content will have an OwnerID
                                iTypes = mpSegmentFindTypes.UserSegments;
                            }
                            else
                            {
                                if (bIncludeAdminContent)
                                    iTypes = iTypes | mpSegmentFindTypes.AdminSegments;
                                if (bIncludeUserContent)
                                    iTypes = iTypes | mpSegmentFindTypes.UserSegments;
                            }
                            break;
                        case ContentSearchForm.mpSearchContentType.StyleSheets:
                            if (iOwnerID > 0)
                            {
                                //Only User Content will have an OwnerID
                                iTypes = mpSegmentFindTypes.UserStyleSheets;
                            }
                            else
                            {
                                if (bIncludeAdminContent)
                                    iTypes = iTypes | mpSegmentFindTypes.StyleSheets;
                                if (bIncludeUserContent)
                                    iTypes = iTypes | mpSegmentFindTypes.UserStyleSheets;
                            }
                            break;
                        case ContentSearchForm.mpSearchContentType.SavedData:
                            iTypes = mpSegmentFindTypes.Prefills;
                            break;
                        case ContentSearchForm.mpSearchContentType.SegmentPackets:
                            if (iOwnerID > 0)
                            {
                                //Only User Content will have an OwnerID
                                iTypes = mpSegmentFindTypes.UserSegmentPackets;
                            }
                            else
                            {
                                if (bIncludeAdminContent)
                                    iTypes = iTypes | mpSegmentFindTypes.SegmentPackets;
                                if (bIncludeUserContent)
                                    iTypes = iTypes | mpSegmentFindTypes.UserSegmentPackets;
                            }

                            break;
                        case ContentSearchForm.mpSearchContentType.MasterData:
                            //Only User Content will have an OwnerID
                            iTypes = mpSegmentFindTypes.MasterData;

                            break;
                        case ContentSearchForm.mpSearchContentType.AllContent:
                            if (iOwnerID > 0)
                            {
                                //Only User Content with have an OwnerID
                                iTypes = mpSegmentFindTypes.UserSegments | mpSegmentFindTypes.UserStyleSheets | mpSegmentFindTypes.UserSegmentPackets | mpSegmentFindTypes.Prefills;
                            }
                            else
                            {
                                if (bIncludeUserContent && !bIncludeAdminContent)
                                    iTypes = mpSegmentFindTypes.UserSegments | mpSegmentFindTypes.UserStyleSheets | mpSegmentFindTypes.UserSegmentPackets | mpSegmentFindTypes.Prefills;
                                if (bIncludeAdminContent && !bIncludeUserContent)
                                    iTypes = mpSegmentFindTypes.AdminSegments | mpSegmentFindTypes.StyleSheets | mpSegmentFindTypes.MasterData | mpSegmentFindTypes.SegmentPackets;
                            }
                            break;
                    }
                    iFields = oForm.SearchField; //GLOG 8162
                    //MessageBox.Show(string.Format("Text: {0}\r\nField: {1}\r\nType: {2}\r\nWhere: {3}\r\nOwner:{4}", oForm.SearchText, oForm.SearchField,
                    //    oForm.ContentType, oForm.SearchFolder, iOwnerID));
                    m_bAdvancedSearchRunning = true;
                    m_aFindResults = Data.Application.GetSegmentFindResults(xSearchText, Session.CurrentUser.ID, mpSegmentFindMode.MatchSubString, iTypes, iFields, iOwnerID, xFolderIDList);
                    if (m_aFindResults == null || m_aFindResults.Count == 0)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Msg_No_Matching_Segments"),
                            LMP.ComponentProperties.ProductName,
                           MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // The find string is appended a carriage return line feed. 
                        //Remove it to fix situation where subseqent
                        // search contains this find string.
                        string xCRLF = "\r\n";

                        if (this.txtFindContent.Text.EndsWith(xCRLF))
                        {
                            this.txtFindContent.Text = this.txtFindContent.Text.Remove(
                                this.txtFindContent.Text.IndexOf(xCRLF));
                        }

                        txtFindContent.Focus();
                        this.chkFindContent.Checked = false;
                    }
                    else
                    {
                        this.txtFindContent.Text = oForm.SearchText;
                        this.chkFindContent.Checked = true;
                        ShowFindResults();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_bAdvancedSearchRunning = false;
            }

        }
        private string GetSubFolderIDs(UltraTreeNode oNode)
        {
            string xFolderList = "";
            if (ContentTreeHelper.IsMyFolderNode(oNode))
            {
                UserFolders oChildren = new UserFolders(Session.CurrentUser.ID);

                for (int i = 1; i <= oChildren.Count; i++)
                {
                    UserFolder oChild = (UserFolder)oChildren.ItemFromIndex(i);
                    GetUserSubFolderList(oChild, ref xFolderList);
                }
            }
            else
            {
                if (oNode.Tag is FolderMember || oNode.Tag is UserSegmentDef || oNode.Tag is AdminSegmentDef || oNode.Tag is VariableSetDef)
                {
                    oNode = oNode.Parent;
                }
                if (ContentTreeHelper.IsUserFolderNode(oNode))
                {
                    UserFolder oParent = (UserFolder)oNode.Tag;
                    GetUserSubFolderList(oParent, ref xFolderList);
                }
                else if (ContentTreeHelper.IsAdminFolderNode(oNode))
                {
                    Folder oParent = (Folder)oNode.Tag;
                    GetAdminSubFolderList(oParent, ref xFolderList);
                }
            }
            xFolderList = xFolderList.TrimEnd(',');
            return xFolderList;
        }
        private void GetUserSubFolderList(UserFolder oFolder, ref string xFolderList)
        {
            xFolderList = xFolderList + oFolder.ID + ",";
            string xParentID = oFolder.ID;
            int iID1 = 0;
            int iID2 = 0;
            LMP.Data.Application.SplitID(xParentID, out iID1, out iID2);
            UserFolders oFolders = new UserFolders(Session.CurrentUser.ID, iID1, iID2);
            for (int i = 1; i <= oFolders.Count; i++)
            {
                GetUserSubFolderList((UserFolder)oFolders.ItemFromIndex(i), ref xFolderList);
            }
        }
        private void GetAdminSubFolderList(Folder oFolder, ref string xFolderList)
        {
            xFolderList = xFolderList + oFolder.ID.ToString() + ",";
            Folders oFolders = new Folders(0, oFolder.ID);
            for (int i = 1; i <= oFolders.Count; i++)
            {
                GetAdminSubFolderList((Folder)oFolders.ItemFromIndex(i), ref xFolderList);
            }
        }

        private void mnuFolder_CreateNewSegment_DropDownOpening(object sender, EventArgs e)
        {
            m_bNewSegmentDesigner = false; //GLOG 8376
        }

        private void mnuFolder_CreateNewDesigner_DropDownOpening(object sender, EventArgs e)
        {
            m_bNewSegmentDesigner = true; //GLOG 8376
        }
    }
}
