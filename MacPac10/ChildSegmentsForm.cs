using System;
using System.Collections.Generic;
using System.Collections; 
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Architect.Api;
using Infragistics.Win.UltraWinTree;

namespace LMP.MacPac
{
    internal partial class ChildSegmentsForm : Form
    {
        #region ***********************fields**********************
        private Segment m_oSegment;
        ArrayList m_oSelectedSegments = new ArrayList();
        #endregion
        #region ***********************constructors**********************
        public ChildSegmentsForm()
        {
            InitializeComponent();
        }

        #endregion
        #region ***********************properties**********************
        public Segment ParentSegment
        {
            get { return m_oSegment; }
            set
            { 
                m_oSegment = value;

                //refresh tree - there is no parent node for top level so pass null argument
                RefreshTree(value, null);

                //refresh form caption with display name of segment
                this.Text = "Save Child Segments of " + m_oSegment.DisplayName; 
            }
        }
        public ArrayList SelectedSegments
        {
            get { return m_oSelectedSegments; }
        }
        #endregion
        #region ***********************methods**********************
        /// <summary>
        /// refresh tree with child segments collection
        /// </summary>
        private void RefreshTree(Segment oSegment, UltraTreeNode oParentNode)
        {
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                UltraTreeNode oNode;
                if (!(oSegment.Segments[i] is CollectionTable))
                {
                    //insert node - top level will pass null pointer for oParentNode
                    oNode = this.InsertNode(oSegment.Segments[i], oParentNode);

                    oNode.Expanded = true;


                    //check and disable node if ID = 0
                    //this is a node created by right click menu in Designer
                    //and not yet saved
                    if (oSegment.Segments[i].ID1 == 0)
                    {
                        oNode.CheckedState = CheckState.Checked;
                        oNode.Enabled = false;
                        try
                        {
                            //we must check, disable and expand all levels above
                            //new node if its segment has not yet been saved
                            UltraTreeNode oPNode = oNode.Parent;
                            while (oPNode != null)
                            {
                                oNode.ExpandAll();
                                oPNode.Enabled = false;
                                oPNode.CheckedState = CheckState.Checked;
                                oPNode = oPNode.Parent;
                            }
                        }
                        catch { }
                    }
                }
                else
                    //display pleading table items directly on pleading node
                    oNode = oParentNode;

                //call recursively to add children
                RefreshTree(oSegment.Segments[i], oNode);
            }             
        }
        /// <summary>
        /// configures node appearance
        /// </summary>
        /// <param name="oNode"></param>
        private void ConfigureNode(UltraTreeNode oNode)
        {
        }
        #endregion
        #region ***********************private functions**********************
        /// <summary>
        /// add child segment nodes to tree
        /// </summary>
        /// <param name="oSegment"></param>
        private UltraTreeNode InsertNode(Segment oSegment, UltraTreeNode oParentNode)
        {
            string xKey = oSegment.FullTagID;
            string xText = oSegment.DisplayName;
            UltraTreeNode oRetNode = null;
            
            //add nodes
            if (oParentNode == null)
                //top level children, add to tree
                oRetNode = this.treeChildren.Nodes.Add(xKey, xText);
            else
                //add children to parent node
                oRetNode = oParentNode.Nodes.Add(xKey, xText);

            //add segment to tag for later retrieval
            oRetNode.Tag = oSegment;
            //configure node properties
            this.ConfigureNode(oRetNode);
            return oRetNode;
        }
        #endregion
        #region ***********************events**********************
        //form is sizeable, so resize tree
        private void ChildSegmentsForm_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                this.treeChildren.Width = this.Width - 29;
                this.treeChildren.Height = this.Height - 92;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// create array of child segments selected in tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btnOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                GetSelectedSegments();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// overloaded method
        /// add tag property of designated child nodes to ChildSegmentsArray
        /// </summary>
        /// <returns></returns>
        private void GetSelectedSegments(UltraTreeNode oParentNode)
        {
            for (int i = 0; i < oParentNode.Nodes.Count; i++)
            {
                //add to segment to array if node is checked
                if (oParentNode.Nodes[i].CheckedState == CheckState.Checked)
                    this.m_oSelectedSegments.Add((Segment)oParentNode.Nodes[i].Tag);
                
                //call recursively to add children of children
                if (oParentNode.Nodes[i].HasNodes)
                    GetSelectedSegments(oParentNode.Nodes[i]); 
            }
        }

        /// <summary>
        /// add tag property of top level nodes to ChildSegmentsArray
        /// </summary>
        /// <returns></returns>
        private void GetSelectedSegments()
        {
            for (int i = 0; i < treeChildren.Nodes.Count; i++)
            {
                //add to segment to array if node is checked
                if (treeChildren.Nodes[i].CheckedState == CheckState.Checked)
                    this.m_oSelectedSegments.Add((Segment)treeChildren.Nodes[i].Tag);
                
                //collect the child segments
                if (treeChildren.Nodes[i].HasNodes)
                    GetSelectedSegments(treeChildren.Nodes[i]);
            }
        }
        /// <summary>
        /// handler to check/uncheck all children of sender node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraTree1_AfterCheck(object sender, NodeEventArgs e)
        {
            try
            {
                //check or uncheck all children of checked node
                foreach (UltraTreeNode oNode in e.TreeNode.Nodes)
                {
                    if (oNode.Enabled)
                        oNode.CheckedState = e.TreeNode.CheckedState;
                    if (e.TreeNode.CheckedState == CheckState.Checked)
                        e.TreeNode.Expanded = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void treeChildren_BeforeCollapse(object sender, CancelableNodeEventArgs e)
        {
            e.Cancel = true;
        }
        #endregion
    }
}