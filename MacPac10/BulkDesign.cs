﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class BulkDesign : Form
    {
        public BulkDesign()
        {
            InitializeComponent();
        }
        public void SetupControl()
        {
            this.lblProgress.Visible = false;

                this.ftvSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                    LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;

            this.ftvSegments.OwnerID = Session.CurrentUser.ID;
            this.ftvSegments.ShowCheckboxes = true;
            this.ftvSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
            this.lblSegments.Text = LMP.Resources.GetLangString("Dialog_StylesManager_CopyTo");
            this.btnOK.Text = LMP.Resources.GetLangString("Lbl_OK");
            this.btnCancel.Text = LMP.Resources.GetLangString("Lbl_Cancel");
        }
    }
}
