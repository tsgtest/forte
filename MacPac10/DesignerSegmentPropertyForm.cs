using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using LMP.Architect.Api;
using LMP.Data;
using LMP.Controls;


namespace LMP.MacPac
{
    internal partial class DesignerSegmentPropertyForm : Form
    {
        #region *********************fields*********************
        private string m_xName = null;
        private string m_xDisplayName = null;
        private Segment m_oCurrentSeg;
        #endregion
        #region *****************enumerations*******************
        private enum InvalidDisplayNames
        {
            ValidName = 0,
            NullString = 1,
            DuplicateName = 2,
            IllegalName = 3
        }
        #endregion
        #region ****************constructors********************
        public DesignerSegmentPropertyForm(Segment oParentSeg)
        {
            this.m_oCurrentSeg = oParentSeg;
            InitializeComponent();
        }
        #endregion
        #region *****************properties*********************
        public string SegmentName
        {
            get { return m_xName; }
            set { m_xName = value; }
        }
        public string SegmentDisplayName
        {
            get { return m_xDisplayName; }
            set { m_xDisplayName = value; }
        }
        #endregion
        #region **********************methods*******************
        /// <summary>
        /// Validate proposed new user or admin segment name
        /// </summary>
        /// <returns></returns>
        private bool SetNames()
        {
            string xMsg = null;
            bool bValidName = false;

            string xDisplayName = this.txtName.Text;

            //name is display name without the spaces and apostrophes
            string xName = String.RemoveIllegalNameChars(xDisplayName);
            
            // Validate new Admin Segment name
            switch (ValidateName(xName))
            {
                case InvalidDisplayNames.ValidName:
                    bValidName = true;
                    this.SegmentName = xName;
                    this.SegmentDisplayName = xDisplayName;
                    break;
                case InvalidDisplayNames.NullString:
                    xMsg = LMP.Resources.GetLangString("Msg_Segment_Name_Required");
                    break;
                case InvalidDisplayNames.DuplicateName:
                    xMsg = LMP.Resources.GetLangString("Msg_SegmentNameExists");
                    break;
                case InvalidDisplayNames.IllegalName:
                    xMsg = LMP.Resources.GetLangString("Msg_SegmentNameIllegal");
                    break;
                default:
                    break;
            }

            //message user if invalid
            if (!bValidName)
            {
                MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtName.Focus(); 
            }
            return bValidName;
        }
        /// <summary>
        /// validates designated name by attempting to create instance
        /// of variable using Variables.ItemByName
        /// </summary>
        private InvalidDisplayNames ValidateName(string xName)
        {
            //empty text box is invalid name
            if (xName == "")
                return InvalidDisplayNames.NullString;

            //TODO: check for illegal characters - although text box 
            //currently deletes them automatically

            //see if any other defs with xName exists in db
            if (m_oCurrentSeg is AdminSegment && AdminSegment.Exists(xName))
                return InvalidDisplayNames.DuplicateName;    

            //get top level segement
            Segment oTopLevelSeg = GetTopLevelSegment(m_oCurrentSeg);

            //if no segment def of xName exists in db, check unsaved segs
            return ValidateName(oTopLevelSeg, xName);
        }
        /// <summary>
        /// overloaded method - called recursively to validate 
        /// proposed name against def name of all existing child
        /// segments
        /// </summary>
        /// <param name="oSeg"></param>
        /// <param name="xName"></param>
        /// <returns></returns>
        private InvalidDisplayNames ValidateName(Segment oSeg, string xName)
        {
            //validate top level segment name 
            if (oSeg.Definition.Name.ToUpper() == xName.ToUpper())
                return InvalidDisplayNames.DuplicateName; 
            
            //validate child names recursively
            InvalidDisplayNames iValidity = InvalidDisplayNames.ValidName;
            
            for (int i = 0; i < oSeg.Segments.Count; i++)
            {
                if (xName.ToUpper() == oSeg.Segments[i].Definition.Name.ToUpper())
                    return InvalidDisplayNames.DuplicateName;
                else if (oSeg.Segments[i].Segments.Count > 0)
                    iValidity = ValidateName(oSeg.Segments[i], xName);
            }
            return iValidity;
        }
        /// <summary>
        /// returns top level segment of document 
        /// </summary>
        /// <param name="oSegment"></param>
        /// <returns></returns>
        private Segment GetTopLevelSegment(Segment oSegment)
        {
            Segment oReturnSegment = oSegment;

            if (oSegment.Parent != null)
                oReturnSegment = GetTopLevelSegment((Segment)oSegment.Parent);

            return oReturnSegment;
        }
        #endregion
        #region ***********************events***********************
        /// <summary>
        /// Sets button text - international support
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DesignerSegmentPropertyForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.lblName.Text = LMP.Resources
                    .GetLangString("Dialog_NewSegment_Name");
                this.btnCancel.Text = LMP.Resources
                    .GetLangString("Dialog_NewSegment_Cancel");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //OK button pressed - validate proposed name
                if (!SetNames())
                    return;
                else
                    this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}