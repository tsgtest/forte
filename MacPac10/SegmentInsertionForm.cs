using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Architect.Api;

namespace LMP.MacPac
{
    public partial class SegmentInsertionForm : Form
    {
        private Segment.InsertionLocations m_shInsertionLocation = 0;
        private Segment.InsertionBehaviors m_shInsertionBehavior = 0;
        private AdminSegmentDefs m_oDefs;
        private AdminSegmentDef m_oCurDef = null;
        private bool m_bFirstSegmentShown = false;
        private short m_shLastLocation = 0;
            
        public SegmentInsertionForm(LMP.Data.mpObjectTypes iSegmentType, bool bShowUseDocumentData)
        {
            InitializeComponent();

            this.lstSegments.Height = this.btnOK.Top - this.lstSegments.Top;

            //setup the segment
            m_oDefs = new AdminSegmentDefs(iSegmentType, LMP.MacPac.Session.CurrentUser.ID.ToString(), 0, 0, 0, 0, 0, true);

            //GLOG : 7559 : CEH
            string xIDs = "";

            ArrayList oSegList = m_oDefs.ToArrayList(1, 0);
            //GLOG 7951
            ArrayList oItemList = new ArrayList();
            if (oSegList.Count > 0)
            {
                foreach (object[] a in oSegList)
                {
                    //GLOG : 7559 : CEH
                    //Skip if already added
                    if (xIDs.IndexOf("|" + a[1].ToString() + "|") == -1) //GLOG 7951
                    {
                        //GLOG 7951: Create separate ArrayList of unique values
                        oItemList.Add(new object[] {a[1], a[0]});
                        //this.lstSegments.Items.Add(a[0]);
                        //avoid duplicates
                        //GLOG 7951: Include delimiters in case one ID is a substring of another previously added
                        xIDs += "|"+ a[1].ToString() + "|";
                    }
                }

                //GLOG 7951
                this.lstSegments.SetList(oItemList);
                this.lstSegments.SelectedIndex = 0;

                //GLOG : 7606 : CEH
                int iSegmentID = 0;

                //GLOG item #6471 - dcf
                if (iSegmentType == mpObjectTypes.Labels)
                {
                    //try User Settings first
                    UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

                    iSegmentID = oSettings.LabelType;

                    if (iSegmentID == 0)
                        //get default firm label
                        iSegmentID = Session.CurrentUser.FirmSettings.DefaultLabelSegment;

                    if (iSegmentID != 0)
                    {
                        //GLOG 7951
                        try
                        {
                            this.lstSegments.SelectedValue = iSegmentID.ToString();
                        }
                        catch
                        {
                        }
                        ////cycle through label segments to find
                        ////the index of the default label
                        //int i = 0;
                        //foreach (object[] a in oSegList)
                        //{
                        //    if ((int)a[0] == iSegmentID)
                        //    {
                        //        this.lstSegments.SelectedIndex = i++;
                        //        break;
                        //    }

                        //    i++;
                        //}
                    }
                }
                else if (iSegmentType == mpObjectTypes.Envelopes)
                {
                    //try User Settings first
                    UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

                    iSegmentID = oSettings.EnvelopeType;

                    if (iSegmentID == 0)
                        //get default firm label
                        iSegmentID = Session.CurrentUser.FirmSettings.DefaultEnvelopeSegment;

                    if (iSegmentID != 0)
                    {
                        //GLOG 7951
                        try
                        {
                            this.lstSegments.SelectedValue = iSegmentID.ToString();
                        }
                        catch
                        {
                        }
                        ////cycle through label segments to find
                        ////the index of the default label
                        //int i = 0;
                        //foreach (object[] a in oSegList)
                        //{
                        //    if ((int)a[1] == iSegmentID)
                        //    {
                        //        this.lstSegments.SelectedIndex = i++;
                        //        break;
                        //    }

                        //    i++;
                        //}
                    }
                }
            }

            switch (iSegmentType)
            {
                case mpObjectTypes.MasterData:
                    {
                        string xTitle = LMP.Resources.GetLangString("Dialog_CreateContent");
                        xTitle = xTitle.Replace("{0}", LMP.Data.Application.GetObjectTypeDisplayName(iSegmentType));
                        this.Text = xTitle;
                        this.lblSegments.Text = LMP.Resources.GetLangString("Label_Content") + ":";
                        this.chkUseDocumentData.Visible = false;;
                        this.lblLocation.Visible = false;
                        this.grpOptions.Visible = false;
                        this.cmbLocation.Visible = false;

                        this.Width = 347;
                        this.Height = 330;

                        this.lstSegments.Width = 315;

                        this.lstSegments.DoubleClick += new EventHandler(btnOK_Click);
                    }
                    break;
                default:
                    {
                        string xTitle = LMP.Resources.GetLangString("Dialog_CreateContent");
                        xTitle = xTitle.Replace("{0}", LMP.Data.Application.GetObjectTypeDisplayName(iSegmentType));
                        this.Text = xTitle;
                        this.lblSegments.Text = LMP.Resources.GetLangString("Label_Content") + ":";
                        this.chkUseDocumentData.Visible = bShowUseDocumentData;
                        if (oSegList.Count == 1)
                        {
                            //GLOG #8492
                            this.lstSegments.Visible = false;
                            this.lblSegments.Visible = false;

                            //GLOG : 8532 : ceh
                            //resize dlg - visible controls will move
                            //because anchor has been set to top + right
                            this.Width = this.lblFormSizeMarker1.Left;
                            //this.grpOptions.Left = 5;
                            //this.cmbLocation.Left = 5;
                            //this.lblLocation.Left = 5;
                        }
                    }
                    break;
            }

            //GLOG item #7847 - dcf
            Application.AdjustDialogWidthForWord15(this);
        }
        /// <summary>
        /// Returns Insertion location selected in Combo
        /// </summary>
        public Segment.InsertionLocations InsertionLocation
        {
            get { return m_shInsertionLocation; }
        }
        /// <summary>
        /// Returns Insertion Behavior based on checkbox values
        /// </summary>
        public Segment.InsertionBehaviors InsertionBehavior
        {
            get { return m_shInsertionBehavior; }
        }
        /// <summary>
        /// returns the ID of the selected segment
        /// </summary>
        public AdminSegmentDef SegmentDef
        {
            //GLOG 7951: Get Segment ID from Value
            get { return (AdminSegmentDef)m_oDefs.ItemFromID(Int32.Parse(this.lstSegments.Value)); }
            //get { return (AdminSegmentDef)m_oDefs[this.lstSegments.SelectedIndex + 1]; }
        }
        /// <summary>
        /// OK button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;
                Segment.InsertionBehaviors iBehaviors = 0;
                //GLOG - 3590 - CEH
                if (chkSeparateSection.Checked)
                    if ((this.SegmentDef.DefaultMenuInsertionBehavior &
                         (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection)
                          == (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection)
                    {
                        iBehaviors = iBehaviors | Segment.InsertionBehaviors.InsertInSeparateNextPageSection;
                    }
                    else if ((this.SegmentDef.DefaultMenuInsertionBehavior &
                         (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection)
                          == (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection)
                    {
                        iBehaviors = iBehaviors | Segment.InsertionBehaviors.InsertInSeparateContinuousSection;
                    }
                if (chkKeepExisting.Checked)
                    iBehaviors  = iBehaviors | Segment.InsertionBehaviors.KeepExistingHeadersFooters;
                if (chkRestartNumbering.Checked)
                    iBehaviors = iBehaviors | Segment.InsertionBehaviors.RestartPageNumbering;
                m_shInsertionBehavior = iBehaviors;
                m_shInsertionLocation = (Segment.InsertionLocations)Enum.Parse(typeof(Segment.InsertionLocations), cmbLocation.SelectedValue.ToString());

                //GLOG : 7606 : CEH
                //Save User Settings for Labels/Envelopes
                UserApplicationSettings oSettings = Session.CurrentUser.UserSettings;

                if (m_oCurDef.TypeID == mpObjectTypes.Labels)
                {
                    oSettings.LabelType = this.m_oCurDef.ID;
                }
                else if (m_oCurDef.TypeID == mpObjectTypes.Envelopes)
                {
                    oSettings.EnvelopeType = this.m_oCurDef.ID;
                }
            }
            catch { }
        }
        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// Setup Display options for current Segment
        /// </summary>
        private void LoadSegmentOptions()
        {
            //GLOG 7951: Use Value instead of SelectedIndex
            m_oCurDef = (AdminSegmentDef)(m_oDefs.ItemFromID(Int32.Parse(this.lstSegments.Value)));
            //m_oCurDef = (AdminSegmentDef)(m_oDefs[this.lstSegments.SelectedIndex + 1]);

            //GLOG 3983: Save currently selected location so it can be preserved when Segment changes
            short shCurLocation = 0;
            if (this.cmbLocation.SelectedValue != null)
                shCurLocation = short.Parse(this.cmbLocation.SelectedValue.ToString());
            ArrayList oListItems = new ArrayList();
            object[,] aDefaultLocations = null;
            object[,] aBehaviors = null;

            //GLOG 7884 (dm) - the following conditional was limiting envelopes to only "new document"
            //in Forte Tools, while allowing all location options in labels - it appears to have been
            //left over inadvertently from an early stage of merge functionality development
            //if ((false && m_oCurDef.TypeID == mpObjectTypes.Labels || m_oCurDef.TypeID == mpObjectTypes.Envelopes) &&
            //    LMP.MacPac.MacPacImplementation.IsToolkit) //GLOG 7127
            //{
            //    aDefaultLocations = new object[,]{
            //    {"New Document",128}};
            //}
            //else
            //{
                //get array of insertion locations
                aDefaultLocations = new object[,]{
                {"Start of Current Section",1}, 
                {"End of Current Section",2}, 
                {"Start of All Sections",4}, 
                {"End of All Sections",8}, 
                {"Insertion Point",16}, 
                {"Start of Document",32}, 
                {"End of Document",64},
                {"New Document",128},
                {"Default Location", 256}};
            //}

            aBehaviors = new object[,]{
            {"Insert In Separate Section",1}, 
            {"Insert In Separate Continuous Section",8}, 
            {"Keep Existing Headers Footers",2}, 
            {"Restart Page Numbering",4}};

            for (int i = aDefaultLocations.GetUpperBound(0); i >= 0; i--)
            {
                if ((m_oCurDef.MenuInsertionOptions &
                    (short)(int)aDefaultLocations[i, 1]) == (short)(int)aDefaultLocations[i, 1])
                {
                    oListItems.Add(new object[] { aDefaultLocations[i, 1], aDefaultLocations[i, 0] });
                }
            }

            if (oListItems.Count == 0)
            {
                cmbLocation.SetList(aDefaultLocations);
            }
            else
            {
                //GLOG 5723: Add options for Below TOC and Above TOA if appropriate
                if (m_oCurDef.TypeID == mpObjectTypes.TOA || m_oCurDef.TypeID == mpObjectTypes.PleadingExhibit)
                {
                    if (m_oCurDef.TypeID != mpObjectTypes.TOA)
                    {
                        if (LMP.Forte.MSWord.WordDoc.TOAExists(LMP.Forte.MSWord.WordApp.ActiveDocument()))
                            //GLOG : 7542 : CEH
                            oListItems.Add(new object[] { "1024", "Above Table of Authorities" });
                            //oListItems.Add(new object[] { "1024", "Above TOA Section" });
                    }
                    if (LMP.Forte.MSWord.WordDoc.TOCExists(LMP.Forte.MSWord.WordApp.ActiveDocument()))
                    {
                        //GLOG : 7542 : CEH
                        oListItems.Add(new object[] { "512", "Below Table of Contents" });
                        //oListItems.Add(new object[] { "512", "Below TOC Section" });
                        if (m_oCurDef.TypeID == mpObjectTypes.TOA)
                            //Use as default location for TOA
                            m_oCurDef.DefaultDoubleClickLocation = 512;
                    }

                }
                cmbLocation.SetList(oListItems);
            }

            //GLOG 3983: Retain previously selected location after initial display
            if (!m_bFirstSegmentShown)
            {
                // Select default double-click item in list
                cmbLocation.SelectedValue = m_oCurDef.DefaultDoubleClickLocation;
                m_bFirstSegmentShown = true;
            }
            else
            {
                //Reloading List changed cmbLocation value, so restore saved Last location
                m_shLastLocation = shCurLocation;
                cmbLocation.SelectedValue = shCurLocation;
            }
            if (cmbLocation.SelectedIndex == -1)
                cmbLocation.SelectedIndex = 0;
        }
        private void EnableBehaviorOptions()
        {
            if (this.cmbLocation.SelectedValue == null)
                return;

            //GLOG 8215: Enable options to start
            this.grpOptions.Enabled = true;
            this.chkSeparateSection.Enabled = true;
            this.chkRestartNumbering.Enabled = true;
            this.chkKeepExisting.Enabled = true;
            if (short.Parse(this.cmbLocation.SelectedValue.ToString()) != 128)
            {
                this.chkSeparateSection.Checked = (m_oCurDef.IntendedUse == mpSegmentIntendedUses.AsDocument);
                this.chkKeepExisting.Checked = (m_oCurDef.IntendedUse != mpSegmentIntendedUses.AsDocument);
                //GLOG 8776
                if (m_oCurDef.TypeID == mpObjectTypes.TOA || m_oCurDef.TypeID == mpObjectTypes.PleadingExhibit)
                {
                    this.chkRestartNumbering.Checked = (m_oCurDef.DefaultMenuInsertionBehavior & (short)Segment.InsertionBehaviors.RestartPageNumbering) 
                        == (short)Segment.InsertionBehaviors.RestartPageNumbering;
                }
                else
                {
                    this.chkRestartNumbering.Checked = (m_oCurDef.IntendedUse == mpSegmentIntendedUses.AsDocument);
                }
            }
            else
            {
                //GLOG 3983: These items disabled for 'New Document' location
                this.chkSeparateSection.Enabled = false;
                this.chkRestartNumbering.Enabled = false;
                this.chkKeepExisting.Enabled = false;
            }

            if (m_oCurDef.TypeID == mpObjectTypes.AgreementTitlePage)
            {
                this.chkSeparateSection.Enabled = false;
                this.chkKeepExisting.Enabled = false;
                this.chkRestartNumbering.Enabled = true;
            }
            if (m_oCurDef.TypeID == mpObjectTypes.PleadingExhibit ||
                m_oCurDef.TypeID == mpObjectTypes.AgreementExhibit)
            {
                //GLOG item #6720 - dcf - 04/22/13
                this.chkSeparateSection.Checked = true; //GLOG 6355
                this.chkKeepExisting.Checked = false;
                //GLOG 8776
                if (m_oCurDef.TypeID == mpObjectTypes.Agreement)
                {
                    this.chkRestartNumbering.Checked = true;
                }
            }
            else if (m_oCurDef.TypeID == mpObjectTypes.TOA)
            {
                this.chkKeepExisting.Enabled = false;
                this.chkSeparateSection.Checked = true; //GLOG 6355
                this.chkRestartNumbering.Enabled = true;
            }
            else if (m_oCurDef.TypeID == mpObjectTypes.Labels ||
                m_oCurDef.TypeID == mpObjectTypes.Envelopes)
            {
                //GLOG 7938 (dm)
                this.chkSeparateSection.Checked = true;
                this.chkSeparateSection.Enabled = false;
            }
            else
            {
                //GLOG 3983: Enable options appropriate for current definition
                this.chkSeparateSection.Enabled = (((m_oCurDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection) ==
                    (short)Segment.InsertionBehaviors.InsertInSeparateNextPageSection) ||
                    ((m_oCurDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection) ==
                    (short)Segment.InsertionBehaviors.InsertInSeparateContinuousSection));

                //GLOG 3983: Set Separate Section checkbox only if changing from New Document location
                //Otherwise, maintain previously selected value
                if (this.chkSeparateSection.Enabled && m_shLastLocation == 128)
                {
                    this.chkSeparateSection.Checked = true;
                }

                this.chkRestartNumbering.Enabled = ((m_oCurDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.RestartPageNumbering) ==
                    (short)Segment.InsertionBehaviors.RestartPageNumbering);

                this.chkKeepExisting.Enabled = ((m_oCurDef.DefaultMenuInsertionBehavior &
                    (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters) ==
                    (short)Segment.InsertionBehaviors.KeepExistingHeadersFooters);
            }
        }

        private void cmbLocation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                EnableBehaviorOptions();
                //GLOG 3983: Save current location
                if (cmbLocation.SelectedValue != null)
                   m_shLastLocation = short.Parse(cmbLocation.SelectedValue.ToString());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstSegments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadSegmentOptions();

                //if (this.lstSegments.Items.Count == 1)
                //{
                //    EnableBehaviorOptions();
                //    //GLOG 3983: Save current location
                //    if (cmbLocation.SelectedValue != null)
                //        m_shLastLocation = short.Parse(cmbLocation.SelectedValue.ToString());
                //}
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}