namespace LMP.MacPac
{
    partial class ContentPropertyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            this.lblDisplayName = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtDisplayName = new LMP.Controls.TextBox();
            this.mltDescription = new LMP.Controls.MultilineTextBox();
            this.lblLocation = new System.Windows.Forms.Label();
            this.ftvLocation = new LMP.Controls.FolderTreeView(this.components);
            this.lblFolderDescription = new System.Windows.Forms.Label();
            this.txtName = new LMP.Controls.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.mnu_Folder = new LMP.Controls.TimedContextMenuStrip(this.components);
            this.mnuFolder_CreateNewFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddHelpText = new System.Windows.Forms.Button();
            this.lblFormSizeMarker1 = new System.Windows.Forms.Label();
            this.lblFormSizeMarker2 = new System.Windows.Forms.Label();
            this.lblFormSizeMarker3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ftvLocation)).BeginInit();
            this.mnu_Folder.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDisplayName
            // 
            this.lblDisplayName.AutoSize = true;
            this.lblDisplayName.Location = new System.Drawing.Point(12, 15);
            this.lblDisplayName.Name = "lblDisplayName";
            this.lblDisplayName.Size = new System.Drawing.Size(77, 15);
            this.lblDisplayName.TabIndex = 0;
            this.lblDisplayName.Text = "&Display Name:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(11, 58);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(65, 15);
            this.lblDescription.TabIndex = 3;
            this.lblDescription.Text = "Des&cription:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(283, 415);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 25);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.AutoSize = true;
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(195, 415);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 25);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtDisplayName
            // 
            this.txtDisplayName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDisplayName.IsDirty = true;
            this.txtDisplayName.Location = new System.Drawing.Point(13, 30);
            this.txtDisplayName.MaxLength = 255;
            this.txtDisplayName.Name = "txtDisplayName";
            this.txtDisplayName.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtDisplayName.Size = new System.Drawing.Size(352, 22);
            this.txtDisplayName.SupportingValues = "";
            this.txtDisplayName.TabIndex = 1;
            this.txtDisplayName.Tag2 = null;
            this.txtDisplayName.Value = "";
            this.txtDisplayName.EnterKeyPressed += new LMP.Controls.EnterKeyPressedHandler(this.txtName_EnterKeyPressed);
            this.txtDisplayName.TextChanged += new System.EventHandler(this.txtDisplayName_TextChanged);
            this.txtDisplayName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDisplayName_KeyPress);
            // 
            // mltDescription
            // 
            this.mltDescription.AcceptsReturn = true;
            this.mltDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mltDescription.IsDirty = true;
            this.mltDescription.Location = new System.Drawing.Point(13, 74);
            this.mltDescription.Multiline = true;
            this.mltDescription.Name = "mltDescription";
            this.mltDescription.NumberOfLines = 2;
            this.mltDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mltDescription.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.mltDescription.Size = new System.Drawing.Size(352, 108);
            this.mltDescription.SupportingValues = "";
            this.mltDescription.TabIndex = 4;
            this.mltDescription.Tag2 = null;
            this.mltDescription.Value = "";
            this.mltDescription.TextChanged += new System.EventHandler(this.mltDescription_TextChanged);
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(10, 195);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(74, 15);
            this.lblLocation.TabIndex = 5;
            this.lblLocation.Text = "Select &Folder:";
            // 
            // ftvLocation
            // 
            this.ftvLocation.AdvancedFilterText = "";
            this.ftvLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.White;
            this.ftvLocation.Appearance = appearance1;
            this.ftvLocation.BorderStyle = Infragistics.Win.UIElementBorderStyle.InsetSoft;
            this.ftvLocation.DisableExcludedTypes = false;
            this.ftvLocation.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders;
            this.ftvLocation.ExcludeNonMacPacTypes = false;
            this.ftvLocation.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvLocation.FilterText = "";
            this.ftvLocation.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ftvLocation.HideSelection = false;
            this.ftvLocation.IncludeAdminContentInSearch = true;
            this.ftvLocation.IncludeUserContentInSearch = true;
            this.ftvLocation.IsDirty = false;
            this.ftvLocation.Location = new System.Drawing.Point(13, 210);
            this.ftvLocation.Mode = LMP.Controls.FolderTreeView.mpFolderTreeViewModes.FolderTree;
            this.ftvLocation.Name = "ftvLocation";
            _override1.HotTracking = Infragistics.Win.DefaultableBoolean.False;
            _override1.LabelEdit = Infragistics.Win.DefaultableBoolean.False;
            _override1.ShowExpansionIndicator = Infragistics.Win.UltraWinTree.ShowExpansionIndicator.CheckOnExpand;
            this.ftvLocation.Override = _override1;
            this.ftvLocation.OwnerID = 0;
            this.ftvLocation.SearchContentType = LMP.Controls.FolderTreeView.mpFolderTreeSearchContentTypes.AllContent;
            this.ftvLocation.SearchFolderList = "";
            this.ftvLocation.SearchUserID = 0;
            this.ftvLocation.ShowCheckboxes = false;
            this.ftvLocation.ShowFindPaths = true;
            this.ftvLocation.ShowTopUserFolderNode = false;
            this.ftvLocation.Size = new System.Drawing.Size(352, 168);
            this.ftvLocation.TabIndex = 6;
            this.ftvLocation.TransparentBackground = false;
            this.ftvLocation.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.ftvLocation_AfterSelect);
            this.ftvLocation.DoubleClick += new System.EventHandler(this.ftvLocation_DoubleClick);
            this.ftvLocation.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ftvLocation_MouseDown);
            // 
            // lblFolderDescription
            // 
            this.lblFolderDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFolderDescription.Location = new System.Drawing.Point(16, 384);
            this.lblFolderDescription.Name = "lblFolderDescription";
            this.lblFolderDescription.Size = new System.Drawing.Size(347, 29);
            this.lblFolderDescription.TabIndex = 7;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.IsDirty = true;
            this.txtName.Location = new System.Drawing.Point(13, 74);
            this.txtName.MaxLength = 255;
            this.txtName.Name = "txtName";
            this.txtName.SelectionMode = LMP.Controls.TextBox.SelectionTypes.SelectAll;
            this.txtName.Size = new System.Drawing.Size(352, 22);
            this.txtName.SupportingValues = "";
            this.txtName.TabIndex = 4;
            this.txtName.Tag2 = null;
            this.txtName.Value = "";
            this.txtName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtName_KeyPress);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(13, 58);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 15);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "&Name:";
            // 
            // mnu_Folder
            // 
            this.mnu_Folder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFolder_CreateNewFolder});
            this.mnu_Folder.Name = "mnu_Folder";
            this.mnu_Folder.Size = new System.Drawing.Size(181, 26);
            this.mnu_Folder.TimerInterval = 500;
            this.mnu_Folder.UseTimer = true;
            // 
            // mnuFolder_CreateNewFolder
            // 
            this.mnuFolder_CreateNewFolder.Name = "mnuFolder_CreateNewFolder";
            this.mnuFolder_CreateNewFolder.Size = new System.Drawing.Size(180, 22);
            this.mnuFolder_CreateNewFolder.Text = "Create New Folder...";
            this.mnuFolder_CreateNewFolder.Click += new System.EventHandler(this.mnuFolder_CreateNewFolder_Click);
            // 
            // btnAddHelpText
            // 
            this.btnAddHelpText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddHelpText.AutoSize = true;
            this.btnAddHelpText.Location = new System.Drawing.Point(14, 415);
            this.btnAddHelpText.Name = "btnAddHelpText";
            this.btnAddHelpText.Size = new System.Drawing.Size(172, 25);
            this.btnAddHelpText.TabIndex = 8;
            this.btnAddHelpText.Text = "&Add Selected Text as Help Text";
            this.btnAddHelpText.UseVisualStyleBackColor = true;
            this.btnAddHelpText.Click += new System.EventHandler(this.btnAddHelpText_Click);
            // 
            // lblFormSizeMarker1
            // 
            this.lblFormSizeMarker1.AutoSize = true;
            this.lblFormSizeMarker1.Location = new System.Drawing.Point(-2, 155);
            this.lblFormSizeMarker1.Name = "lblFormSizeMarker1";
            this.lblFormSizeMarker1.Size = new System.Drawing.Size(23, 15);
            this.lblFormSizeMarker1.TabIndex = 11;
            this.lblFormSizeMarker1.Text = "----";
            this.lblFormSizeMarker1.Visible = false;
            // 
            // lblFormSizeMarker2
            // 
            this.lblFormSizeMarker2.AutoSize = true;
            this.lblFormSizeMarker2.Location = new System.Drawing.Point(-2, 270);
            this.lblFormSizeMarker2.Name = "lblFormSizeMarker2";
            this.lblFormSizeMarker2.Size = new System.Drawing.Size(23, 15);
            this.lblFormSizeMarker2.TabIndex = 12;
            this.lblFormSizeMarker2.Text = "----";
            this.lblFormSizeMarker2.Visible = false;
            // 
            // lblFormSizeMarker3
            // 
            this.lblFormSizeMarker3.AutoSize = true;
            this.lblFormSizeMarker3.Location = new System.Drawing.Point(-2, 449);
            this.lblFormSizeMarker3.Name = "lblFormSizeMarker3";
            this.lblFormSizeMarker3.Size = new System.Drawing.Size(23, 15);
            this.lblFormSizeMarker3.TabIndex = 14;
            this.lblFormSizeMarker3.Text = "----";
            this.lblFormSizeMarker3.Visible = false;
            // 
            // ContentPropertyForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(381, 452);
            this.Controls.Add(this.lblFormSizeMarker3);
            this.Controls.Add(this.lblFormSizeMarker2);
            this.Controls.Add(this.lblFormSizeMarker1);
            this.Controls.Add(this.btnAddHelpText);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblFolderDescription);
            this.Controls.Add(this.ftvLocation);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.mltDescription);
            this.Controls.Add(this.txtDisplayName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblDisplayName);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContentPropertyForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Properties";
            ((System.ComponentModel.ISupportInitialize)(this.ftvLocation)).EndInit();
            this.mnu_Folder.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDisplayName;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private LMP.Controls.TextBox txtDisplayName;
        private LMP.Controls.MultilineTextBox mltDescription;
        private System.Windows.Forms.Label lblLocation;
        private LMP.Controls.FolderTreeView ftvLocation;
        private System.Windows.Forms.Label lblFolderDescription;
        private LMP.Controls.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private Controls.TimedContextMenuStrip mnu_Folder;
        private System.Windows.Forms.ToolStripMenuItem mnuFolder_CreateNewFolder;
        private System.Windows.Forms.Button btnAddHelpText;
        private System.Windows.Forms.Label lblFormSizeMarker1;
        private System.Windows.Forms.Label lblFormSizeMarker2;
        private System.Windows.Forms.Label lblFormSizeMarker3;
    }
}