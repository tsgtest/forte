namespace LMP.MacPac
{
    partial class SegmentInsertionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSegments = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.grpOptions = new System.Windows.Forms.GroupBox();
            this.chkRestartNumbering = new System.Windows.Forms.CheckBox();
            this.chkKeepExisting = new System.Windows.Forms.CheckBox();
            this.chkSeparateSection = new System.Windows.Forms.CheckBox();
            this.chkUseDocumentData = new System.Windows.Forms.CheckBox();
            this.lstSegments = new LMP.Controls.ListBox();
            this.cmbLocation = new LMP.Controls.ComboBox();
            this.lblFormSizeMarker1 = new System.Windows.Forms.Label();
            this.grpOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.AutoSize = true;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(299, 215);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(380, 215);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblSegments
            // 
            this.lblSegments.AutoSize = true;
            this.lblSegments.BackColor = System.Drawing.Color.Transparent;
            this.lblSegments.Location = new System.Drawing.Point(12, 10);
            this.lblSegments.Name = "lblSegments";
            this.lblSegments.Size = new System.Drawing.Size(41, 15);
            this.lblSegments.TabIndex = 0;
            this.lblSegments.Text = "&Types:";
            // 
            // lblLocation
            // 
            this.lblLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(240, 10);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(96, 15);
            this.lblLocation.TabIndex = 2;
            this.lblLocation.Text = "Insertion &Location:";
            // 
            // grpOptions
            // 
            this.grpOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpOptions.AutoSize = true;
            this.grpOptions.Controls.Add(this.chkRestartNumbering);
            this.grpOptions.Controls.Add(this.chkKeepExisting);
            this.grpOptions.Controls.Add(this.chkSeparateSection);
            this.grpOptions.Location = new System.Drawing.Point(240, 61);
            this.grpOptions.Name = "grpOptions";
            this.grpOptions.Size = new System.Drawing.Size(215, 108);
            this.grpOptions.TabIndex = 4;
            this.grpOptions.TabStop = false;
            this.grpOptions.Text = "Insertion Options";
            // 
            // chkRestartNumbering
            // 
            this.chkRestartNumbering.AutoSize = true;
            this.chkRestartNumbering.Location = new System.Drawing.Point(16, 68);
            this.chkRestartNumbering.Name = "chkRestartNumbering";
            this.chkRestartNumbering.Size = new System.Drawing.Size(145, 19);
            this.chkRestartNumbering.TabIndex = 6;
            this.chkRestartNumbering.Text = "&Restart Page Numbering";
            this.chkRestartNumbering.UseVisualStyleBackColor = true;
            // 
            // chkKeepExisting
            // 
            this.chkKeepExisting.AutoSize = true;
            this.chkKeepExisting.Location = new System.Drawing.Point(16, 44);
            this.chkKeepExisting.Name = "chkKeepExisting";
            this.chkKeepExisting.Size = new System.Drawing.Size(178, 19);
            this.chkKeepExisting.TabIndex = 5;
            this.chkKeepExisting.Text = "Keep &Existing Headers/Footers";
            this.chkKeepExisting.UseVisualStyleBackColor = true;
            // 
            // chkSeparateSection
            // 
            this.chkSeparateSection.AutoSize = true;
            this.chkSeparateSection.Location = new System.Drawing.Point(16, 22);
            this.chkSeparateSection.Name = "chkSeparateSection";
            this.chkSeparateSection.Size = new System.Drawing.Size(151, 19);
            this.chkSeparateSection.TabIndex = 4;
            this.chkSeparateSection.Text = "Insert in &Separate Section";
            this.chkSeparateSection.UseVisualStyleBackColor = true;
            // 
            // chkUseDocumentData
            // 
            this.chkUseDocumentData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkUseDocumentData.AutoSize = true;
            this.chkUseDocumentData.Location = new System.Drawing.Point(258, 173);
            this.chkUseDocumentData.Name = "chkUseDocumentData";
            this.chkUseDocumentData.Size = new System.Drawing.Size(161, 19);
            this.chkUseDocumentData.TabIndex = 7;
            this.chkUseDocumentData.Text = "Insert using &Document Data";
            this.chkUseDocumentData.UseVisualStyleBackColor = true;
            // 
            // lstSegments
            // 
            this.lstSegments.AllowEmptyValue = false;
            this.lstSegments.FormattingEnabled = true;
            this.lstSegments.IsDirty = false;
            this.lstSegments.ItemHeight = 15;
            this.lstSegments.ListName = "";
            this.lstSegments.Location = new System.Drawing.Point(12, 26);
            this.lstSegments.Name = "lstSegments";
            this.lstSegments.Size = new System.Drawing.Size(212, 214);
            this.lstSegments.SupportingValues = "";
            this.lstSegments.TabIndex = 1;
            this.lstSegments.Tag2 = null;
            this.lstSegments.Value = "";
            this.lstSegments.SelectedIndexChanged += new System.EventHandler(this.lstSegments_SelectedIndexChanged);
            // 
            // cmbLocation
            // 
            this.cmbLocation.AllowEmptyValue = false;
            this.cmbLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLocation.AutoSize = true;
            this.cmbLocation.Borderless = false;
            this.cmbLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLocation.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLocation.IsDirty = false;
            this.cmbLocation.LimitToList = true;
            this.cmbLocation.ListName = "";
            this.cmbLocation.Location = new System.Drawing.Point(240, 26);
            this.cmbLocation.MaxDropDownItems = 8;
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.SelectedIndex = -1;
            this.cmbLocation.SelectedValue = null;
            this.cmbLocation.SelectionLength = 0;
            this.cmbLocation.SelectionStart = 0;
            this.cmbLocation.Size = new System.Drawing.Size(215, 26);
            this.cmbLocation.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLocation.SupportingValues = "";
            this.cmbLocation.TabIndex = 3;
            this.cmbLocation.Tag2 = null;
            this.cmbLocation.Value = "";
            this.cmbLocation.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLocation_ValueChanged);
            // 
            // lblFormSizeMarker1
            // 
            this.lblFormSizeMarker1.AutoSize = true;
            this.lblFormSizeMarker1.Location = new System.Drawing.Point(247, 225);
            this.lblFormSizeMarker1.Name = "lblFormSizeMarker1";
            this.lblFormSizeMarker1.Size = new System.Drawing.Size(23, 15);
            this.lblFormSizeMarker1.TabIndex = 13;
            this.lblFormSizeMarker1.Text = "----";
            this.lblFormSizeMarker1.Visible = false;
            // 
            // SegmentInsertionForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(469, 252);
            this.Controls.Add(this.lblFormSizeMarker1);
            this.Controls.Add(this.chkUseDocumentData);
            this.Controls.Add(this.lstSegments);
            this.Controls.Add(this.cmbLocation);
            this.Controls.Add(this.grpOptions);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.lblSegments);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SegmentInsertionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert Segment";
            this.grpOptions.ResumeLayout(false);
            this.grpOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblSegments;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.GroupBox grpOptions;
        private System.Windows.Forms.CheckBox chkRestartNumbering;
        private System.Windows.Forms.CheckBox chkKeepExisting;
        private System.Windows.Forms.CheckBox chkSeparateSection;
        private LMP.Controls.ComboBox cmbLocation;
        private LMP.Controls.ListBox lstSegments;
        private System.Windows.Forms.CheckBox chkUseDocumentData;
        private System.Windows.Forms.Label lblFormSizeMarker1;
    }
}