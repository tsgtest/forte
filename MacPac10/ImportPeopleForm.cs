﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.MacPac
{
    public partial class ImportPeopleForm : Form
    {
        public ImportPeopleForm()
        {
            InitializeComponent();
        }

        private void ImportForm_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.importedPeopleManager1.Enabled = true;
                this.importedPeopleManager1.Select();
            }
        }

        //GLOG : 5808 : jsw
        private void txtFindPerson_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                this.importedPeopleManager1.SelectMatchingRecord(this.txtFindPerson.Text);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
