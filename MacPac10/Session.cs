using System;
using System.IO;
using System.Collections;
using System.Security.Principal;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using LMP.Data;

namespace LMP.MacPac
{
	/// <summary>
	/// LMP.MacPac.Session class -
	/// defines a MacPac session -
	/// user, word app, mode, and culture -
	/// contains static members
	/// created by Daniel Fisherman - 08/22/05
	/// </summary>
	public static class Session
	{
		#region *********************constructors*********************
		#endregion
        #region *********************fields*********************
		private static bool m_bAdminMode = false;
		private static bool m_bInitialized = false;
        private static bool m_bInitializationAttempted = false;
		private static bool m_bBelongsToPrimaryUser = false;
        private static bool m_bLogonFailedMsgShown = false;
        private static bool m_bPostInjunctionWord = false;
        private static int m_iFailedInitializationAtttempts = 0;
        private static string m_xDisplayFinishButton = "NA";
        private static float m_fScalingFactor = 0;

        //GLOG : 6112 : CEH
        private static string m_xDisplayAdminRibbon = "NA";

        //login user is the user that logged in to the session
        private static User m_oLoginUser;
        #endregion
        #region *********************properties*********************
        /// <summary>
        /// returns true iff the session has been
        /// successfully initialized
        /// </summary>
        public static bool IsInitialized
		{
			get{return m_bInitialized;}
		}
        public static bool InitializationAttempted
        {
            get { return m_bInitializationAttempted; }
        }
        public static bool InitializationFailed
        {
            get { return !m_bInitialized && m_bInitializationAttempted; }
        }
        /// <summary>
        /// returns the Word application associated with this session
        /// </summary>
		public static Word.Application CurrentWordApp
		{
			get{return LMP.Architect.Api.Application.CurrentWordApp;}
		}
        /// <summary>
        /// returns the major version number of the Word application
        /// associated with this session
        /// </summary>
        public static int CurrentWordVersion
        {
            get { return LMP.Architect.Api.Application.CurrentWordVersion; }
        }
        /// <summary>
        /// returns the current user of this session
        /// </summary>
		public static User CurrentUser
		{
			get{return LMP.Data.Application.User;}
		}
        /// <summary>
        /// returns the user who logged into the session
        /// </summary>
		public static User LoginUser
		{
            get { return m_oLoginUser; }
		}
        /// <summary>
        /// gets/sets whether or not the current session is in Admin mode
        /// </summary>
		public static bool AdminMode
		{
			get{return m_bAdminMode;}
			set{m_bAdminMode = value;}
		}
        /// <summary>
        /// returns true iff the session belongs to
        /// the primary user of this install of the product
        /// </summary>
		public static bool BelongsToPrimaryUser
		{
			get{return m_bBelongsToPrimaryUser;}
		}
        /// <summary>
        /// gets/sets the time of the LastNormalStylesUpdate 
        /// for the current user
        /// </summary>
        public static DateTime LastNormalStylesUpdate
        {
            get
            {
                string xTime = LMP.Data.Application.GetMetadata(
                    "LastNormal" + Session.CurrentWordVersion.ToString()
                    + "StylesUpdate" + LMP.Data.Application.User.ID);

                if (string.IsNullOrEmpty(xTime))
                    return new DateTime(2001, 1, 1);
                else
                {
                    return DateTime.Parse(xTime, LMP.Culture.USEnglishCulture);
                }
            }
            set
            {
                LMP.Data.Application.SetMetadata("LastNormal" + Session.CurrentWordVersion.ToString()
                    + "StylesUpdate" + LMP.Data.Application.User.ID, 
                    DateTime.Now.ToUniversalTime().ToString(LMP.Culture.USEnglishCulture));
            }
        }
        public static bool DisplayFinishButton
        {
            get
            {
                if (m_xDisplayFinishButton == "NA")
                {
                    try
                    {
                        m_xDisplayFinishButton = LMP.Registry.GetMacPac10Value("DisplayFinishButtonInDocumentEditor");
                    }
                    catch { }
                }

                return m_xDisplayFinishButton == "1";
            }
        }
        //GLOG : 6112 : CEH
        public static bool DisplayAdminRibbon
        {
            get
            {
                if (m_xDisplayAdminRibbon == "NA")
                {
                    try
                    {
                        m_xDisplayAdminRibbon = LMP.Registry.GetMacPac10Value("DisplayAdministrationRibbon");
                    }
                    catch { }
                }

                return m_xDisplayAdminRibbon == "1";
            }
        }

        /// <summary>
        /// returns the screen scaling factor for the session
        /// </summary>
        public static float ScreenScalingFactor{
            get{
                if(m_fScalingFactor == 0)
                    m_fScalingFactor = LMP.OS.GetScalingFactor();

                return m_fScalingFactor;
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// initializes a MacPac session if necessary -
        /// i.e. if not already initialized and if a
        /// previous initialization hasn't been attempted
        /// </summary>
        /// <param name="oCurWordApp"></param>
        /// <returns></returns>
        public static bool StartIfNecessary(Word.Application oCurWordApp)
        {
            bool bRet = Session.IsInitialized;

            if (!Session.IsInitialized && !Session.InitializationAttempted)
            {
                bRet = Session.Start(oCurWordApp);
            }

            return bRet;
        }
        public static bool HostedInPostInjunctionWord
        {
            get { return m_bPostInjunctionWord; }
            private set { m_bPostInjunctionWord = value; }
        }

        /// <summary>
        /// initializes a MacPac session
        /// </summary>
        /// <param name="oCurWordApp"></param>
        private static bool Start(Word.Application oCurWordApp)
        {
            try
            {
                DateTime t0 = DateTime.Now;
                LMP.Data.Application.WriteDBLog("Session.Start"); //GLOG 7546
                //Do not delete this assignment - it sets a flag in mpCOM
                //that improves performance when determining the version of
                //a Word version upgrade - dcf - 02/10/11
                Session.HostedInPostInjunctionWord = LMP.Forte.MSWord.WordApp.IsPostInjunctionWordVersion();

                //install synched db if one exists
                //GLOG 4738: If ForteSync.mdb cannot be installed, 
                //display message and continue
                try
                {
                    LMP.Data.Application.InstallUpdatedDBIfNecessary();
                }
                catch { }

                LMP.Data.Application.RenameUserDBIfSpecified();

                //get current system user name
                string xSystemUserID = Environment.UserName;

                bool bAdminMode = false;

                //get required location of local admin file
                string xAdminDB = LMP.Data.Application.WritableDBDirectory + @"\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13

                Trace.WriteNameValuePairs("xAdminDB", xAdminDB, 
                    "xSystemUserID", xSystemUserID);

                //another session of Forte may have already been
                //started - in the case of Word 2003, clicking a menu
                //item starts a session - get value that specifies
                //whether that session is in admin mode or not
                string xExternalLogonAsAdmin = 
                    LMP.Registry.GetCurrentUserValue(
                        @"Software\The Sackett Group\Deca", "AdminMode");

                if (!string.IsNullOrEmpty(xExternalLogonAsAdmin))
                {
                    //previous session was started - get whether
                    //in admin mode or not
                    bAdminMode = xExternalLogonAsAdmin == "1";
                }
                else
                {
				    //GLOG : 7998 : ceh
                    //GLOG 8221 (dm) - restore vetting of admin in Enterprise
                    //GLOG 8281: Allow Forte Local login
                    //GLOG 8289: Default to allow Admin access if Admin Group is empty only for Full Network version
                    //GLOG 8321: Need to test for Full Local first, since Person.HasAdminAccess() can cause error
                    //first time Forte is initialized with new ForteLocal.mdb
                    if (LMP.Data.Application.IsAdminDB(xAdminDB) &&
                        ((LMP.MacPac.MacPacImplementation.IsFullLocal && PublicPersons.HasAdminAccess(xSystemUserID, false))  ||
                        (LMP.MacPac.MacPacImplementation.IsToolkit) || 
                        ((LMP.MacPac.MacPacImplementation.IsFullWithNetworkSupport && Person.HasAdminAccess(xSystemUserID, true)))))
                    {
                        if (File.Exists(LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName)) //JTS 3/28/13
                        {
                            //admin db exists and current system user is an admin- 
                            //prompt for user or admin mode
                            //TODO: spice up this dialog box
                            //GLOG : 8318 : jsw
                            //if the license key is expired
                            //don't prompt for admin/user, 
                            //just login as admin.
                            if (!MacPac.Application.LicenseIsValid)
                            {
                                bAdminMode = true;
                            }
                            else
                            {
                                AdminLogonForm oLoginForm = new AdminLogonForm();
                                oLoginForm.LoginChoice = AdminLogonForm.LoginChoices.Administrator;
                                oLoginForm.ShowDialog();
                                oLoginForm.Hide();

                                System.Windows.Forms.Application.DoEvents();

                                //set admin mode based on user choice
                                bAdminMode = oLoginForm.LoginChoice ==
                                    AdminLogonForm.LoginChoices.Administrator;

                                oLoginForm.Close();
                            }
                        }
                        else
                        {
                            bAdminMode = true;
                        }
                    }

                    //mark in registry whether this session
                    //is in admin mode or not
                    LMP.Registry.SetCurrentUserValue(
                        @"Software\The Sackett Group\Deca", "AdminMode",
                        bAdminMode ? "1" : "");
                }

                //GLOG #2879 - deal gracefully with lack of guest support - DF
                try
                {
                    Initialize(xSystemUserID, oCurWordApp, bAdminMode);
                }
                catch (LMP.Exceptions.InvalidUserException oE)
                {
                    //GLOG item #5346 - dcf
                    string xSuppressAlert = Registry.GetMacPac10Value("SuppressUserLoginFailureAlert");

                    if (xSuppressAlert != "1" && !m_bLogonFailedMsgShown)
                    {
                        MessageBox.Show(oE.Message.Replace("[REPLACE]", xSystemUserID),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);

                        m_bLogonFailedMsgShown = true;
                    }
                    return false;
                }
                catch (System.Exception oE)
                {
                    if (!m_bLogonFailedMsgShown)
                    {
                        MessageBox.Show(LMP.Resources.GetLangString("Error_CouldNotInitializeMacPac"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);

                        m_bLogonFailedMsgShown = true;
                    }
                    throw oE;
                }

                ////alert if current user is a guest
                //if (Session.CurrentUser.ID == ForteConstants.mpGuestPersonID)
                //{
                //    GuestDetailForm oForm = new GuestDetailForm();
                //    oForm.ShowDialog();

                //    //TODO: store guest detail
                //}

                //10-14-11 (dm) - moved line up for greater scope
                FirmApplicationSettings oSettings = Session.CurrentUser.FirmSettings;

                if (LMP.Data.Application.LastSyncFailed)
                {
                    //reset last sync failed key
                    LMP.Registry.SetCurrentUserValue(
                        LMP.Data.ForteConstants.mpSyncRegKey, "LastSyncFailed", "0");

                    //get mail server parameters from app settings
                    //JTS 12/17/09: Don't attempt to send message unless mail settings are configured
                    if (oSettings.MailHost != "" && (oSettings.MailUseNetworkCredentialsToLogin ||
                            (oSettings.MailUserID != "" && oSettings.MailPassword != "")))
                    {
                        MailServerParameters oParams;
                        oParams.UseNetworkCredentialsToLogin = oSettings.MailUseNetworkCredentialsToLogin;
                        oParams.Host = oSettings.MailHost;
                        oParams.UserID = oSettings.MailUserID;
                        oParams.Password = oSettings.MailPassword;
                        oParams.UseSsl = oSettings.MailUseSsl;
                        oParams.Port = oSettings.MailPort;

                        try
                        {
                            //send message
                            LMP.OS.SendMessage(Session.CurrentUser.PersonObject.GetPropertyValue("EMail").ToString(),
                                LMP.ComponentProperties.ProductName + " User Synchronization Failed.",
                                LMP.ComponentProperties.ProductName + " user synchronization failed for " + Environment.UserName +
                                ".  You can check that user's SyncErrors.log in the " + LMP.ComponentProperties.ProductName + " data directory for more information.",
                                oParams, null, oSettings.MailAdminEMail);
                        }
                        catch
                        {
                            //Don't stop initialization if there's a problem sending Admin e-mail
                        }
                    }
                }

                if (bAdminMode)
                    //update the admin db structure if necessary
                    LMP.Data.Application.UpdateLocalDBStructureIfNecessary(true);
                else
                    //update the user db structure if necessary
                    LMP.Data.Application.UpdateLocalDBStructureIfNecessary(false);

                //GLOG 8220: Match Local People table to Public People table and sync public records
                if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                {
                    LocalPersons oPersons = new LocalPersons();
                    oPersons.SyncPublicPeople(LMP.Data.LocalConnection.ConnectionObject);
                }

                //GLOG 8076, 8107
                LMP.Data.Application.PopulateHelpTextTable(bAdminMode);

                //Clean up Local DB
                LMP.Data.Application.DeleteOrphanedUserFolderMembers();

                //pass client's tag prefix id and encryption password to mpCOM
                string xTagPrefixID = LMP.Data.Application.CurrentClientTagPrefixID;
                string xEncryptionPassword = LMP.Data.Application.EncryptionPassword;
                LMP.Forte.MSWord.Application oCOM = new LMP.Forte.MSWord.Application();
                LMP.Forte.MSWord.Application.SetClientMetadataValues(xTagPrefixID, xEncryptionPassword);

                //determine whether to disable encryption for this session -
                //two keys are required - one from the registry, the other from
                //the metadata table
                string xRegPassword = LMP.Registry.GetMacPac10Value("DisableEncryption");

                LMP.Forte.MSWord.Application.DisableEncryption(xRegPassword.ToLower() == "miller" &&
                    LMP.Data.Application.DisableEncryptionPassword.ToLower() == "fisherman");

                //10-14-11 - set Base64 encoding
                LMP.Forte.MSWord.Application.SetBase64Encoding(oSettings.UseBase64Encoding);

                //update styles in Normal.dot if necessary and allowed
                if (!bAdminMode && Session.CurrentUser.FirmSettings.AllowNormalStyleUpdates && 
                    (Session.LastNormalStylesUpdate < LMP.Data.Application.GetFirmNormalLastEditTime()))
                {
                    //update styles in local normal.dot for current user
                    LMP.MacPac.Application.ResetNormalStyles();
                }
                //GLOG 6360: Create separate metadata value for each user
                if (!bAdminMode && LMP.Data.Application.GetMetadata("DefaultUserFoldersCreated" + Session.CurrentUser.ID.ToString()) != "True")
                {
                    //create default user folders if none already exist
                    UserFolders oFolders = new UserFolders(Session.CurrentUser.ID);

                    //creation has not yet been attempted - we try once, so that if
                    //users deleted the defaults, no second attempt would be made

                    // GLOG : 5117 : CEH
                    if (!LMP.MacPac.MacPacImplementation.IsToolkit && oFolders.Count == 0)
                    {
                        string xDefaultUserFolders = "";
                        string xDefaultDescriptions = "";

                        if (Session.CurrentUser.FirmSettings.DefaultUserFolders.Length > 0)
                        {
                            //load Firm Settings' default user folders
                            string xTemp = Session.CurrentUser.FirmSettings.DefaultUserFolders;
                            string[] axTemp = xTemp.Split(';');
                            
                            for (int i = 0; i < axTemp.Length; i = i + 2)
                            {
                                xDefaultUserFolders = xDefaultUserFolders + axTemp[i] + ";";
                                xDefaultDescriptions = xDefaultDescriptions + axTemp[i + 1] + ";";
                            }

                            xDefaultUserFolders = xDefaultUserFolders.TrimEnd(new char[] { ';' });
                            xDefaultDescriptions = xDefaultDescriptions.TrimEnd(new char[] { ';' });
                        }
                        else
                        {
                            //load default user folders
                            xDefaultUserFolders = LMP.Resources.GetLangString("Default_UserFolder_Names");
                            xDefaultDescriptions = LMP.Resources.GetLangString("Default_UserFolder_Descriptions");
                        }

                        string[] axUserFolders = xDefaultUserFolders.Split(';');
                        string[] axDescriptions = xDefaultDescriptions.Split(';');

                        for (int i = 0; i < axUserFolders.Length; i++)
                        {

                            UserFolder oFolder;
                            //GLOG 4586: Make sure a unique User Item ID has been assigned before
                            //saving the new UserFolder object.  There could be a conflict here
                            //if time since creation of last item is small enough
                            bool bDuplicateID = false;
                            do
                            {
                                oFolder = (UserFolder)oFolders.Create(Session.CurrentUser.ID);
                                try
                                {
                                    bDuplicateID = (oFolders.ItemFromID(oFolder.ID) != null);
                                }
                                catch
                                {
                                    bDuplicateID = false;
                                }
                            } while (bDuplicateID);
                            oFolder.Name = axUserFolders[i];
                            oFolder.Description = axDescriptions[i];
                            oFolders.Save(oFolder);
                        }
                    }
                    //GLOG 6360
                    LMP.Data.Application.SetMetadata("DefaultUserFoldersCreated" + Session.CurrentUser.ID.ToString(), "True");
                }

                LMP.Benchmarks.Print(t0);

                return true;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SessionException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeSession"), oE);
            }
            finally
            {
                //GLOG 6655: Prevent repeated attempts in this session
                m_bInitializationAttempted = true;
            }
        }
        public static void Quit()
        {
            LMP.Data.Application.QuitCI();
            LMP.Data.LocalConnection.Close();
            //GLOG : 6112 : CEH
            //force login after Quit
            m_bInitialized = false;
            m_bInitializationAttempted = false;
            //GLOG : 6112 : CEH
            m_xDisplayFinishButton = "NA";
            m_xDisplayAdminRibbon = "NA";
            LMP.Data.Application.CurrentClientTagPrefixID = null;
            LMP.Data.Application.EncryptionPassword = null;

        }
        /// <summary>
        /// sets the proxy for the session
        /// </summary>
        /// <param name="iPersonID"></param>
        public static void SetProxy(int iPersonID)
        {
            LMP.Data.Application.SetCurrentUser(iPersonID);
        }
        #endregion
        #region *********************private functions*********************
        private static void Initialize(string xSystemUserID, Word.Application oCurWordApp, bool bAdminMode)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                if (!bAdminMode && !LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                {
                    string xSyncDB = LMP.Data.Application.WritableDBDirectory + @"\ForteSync.mdb"; //JTS 3/28/13

                    //alert user if a sync is currently occurring
                    if (LMP.Registry.GetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing") == "1")
                    {
                        //GLOG 5919: If Sync Process is not currently running, just reset Executing key and continue
                        //Any updated ForteSync.mdb will already have been installed by Session.Start
                        int iSession = System.Diagnostics.Process.GetCurrentProcess().SessionId;
                        System.Diagnostics.Process[] aProcesses = System.Diagnostics
                            .Process.GetProcessesByName("fDesktopSyncProcess");
                        
                        bool bIsRunning = false;
                        //Check if Desktop Process is currently running
                        for (int i = 0; i < aProcesses.Length; i++)
                        {
                            if (aProcesses[i].SessionId == iSession)
                            {
                                //Process is already running in this session
                                bIsRunning = true;
                                break;
                            }
                        }

                        if (bIsRunning)
                            MessageBox.Show(LMP.Resources.GetLangString("Msg_SyncIsExecuting"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                        else
                        {
                            //Reset inappropriate Executing value
                            LMP.Registry.SetCurrentUserValue(ForteConstants.mpSyncRegKey, "Executing", "0");
                        }
                    }
                    else if (File.Exists(xSyncDB))
                    {
                        //sync is complete - set sync db as Forte.mdb
                        string xForteDB = LMP.Data.Application.WritableDBDirectory + @"\" + LMP.Data.Application.UserDBName; //JTS 3/28/13
                        LMP.Data.Application.WriteDBLog("Session.Initialize: Synch DB exists", xSyncDB); //GLOG 7546
                        LMP.Data.Application.WriteDBLog("Session.Initialize: Before replacement", xForteDB); //GLOG 7546
                        File.Delete(xForteDB);
                        File.Move(xSyncDB, xForteDB);
                        LMP.Data.Application.WriteDBLog("Session.Initialize: After replacement", xForteDB); //GLOG 7546
                    }
                }

                Session.AdminMode = bAdminMode;

                //GLOG 7898 (dm) - set corresponding property in fBase - an inelegant solution to the
                //MacPacImplementation class needing to remain at the lowest point in the hierarchy
                LMP.MacPac.MacPacImplementation.IsAdminMode = bAdminMode;

                bool bRet = LMP.Data.Application.Login(xSystemUserID, bAdminMode);

                if (bRet)
                {
                    //set the login user
                    m_oLoginUser = Session.CurrentUser;

                    //login was successful - set up session
                    //TODO: softcode culture to pull from user application settings
                    LMP.Resources.SetCulture(1033);

                    if (oCurWordApp != null)
                    {
                        LMP.Architect.Api.Application.CurrentWordApp = oCurWordApp;

                        //GLOG 3723: This command is not available in Reading Layout, so
                        //an error will occur if MacPac is initializing after opening a document
                        //from an Outlook attachment.  Leaving this option on if set doesn't
                        //appear to have adverse effects, but no-one recalls why it was being
                        //disabled in the first place.
                        ////disable auto smart tag labelling
                        //oCurWordApp.Options.LabelSmartTags = false;
                    }

                    //mark if current user is primary user
                    m_bBelongsToPrimaryUser =
                        (Session.CurrentUser.ID.ToString() == Application.PrimaryUser);

                    if (!bAdminMode)
                    {
                        Error.CurrentUser = Session.CurrentUser.PersonObject.GetPropertyValue("Email").ToString();
                        Error.MailServerParameters = LMP.Data.Application.GetMailServerParameters();
                        Error.AdminEMailAddress = Session.CurrentUser.FirmSettings.MailAdminEMail;
                    }
                    //GLOG 8107: PopulateHelpTextTable call moved to Start
                    m_bInitialized = true;
                }
                Trace.WriteNameValuePairs("bAdminMode", bAdminMode,
                    "m_bBelongsToPrimaryUser", m_bBelongsToPrimaryUser);

                LMP.Benchmarks.Print(t0);
            }
            catch (LMP.Exceptions.InvalidUserException oE)
            {
                throw oE;
            }
            catch (System.Exception oE)
            {
                //GLOG item #5638 - dcf - 08/12/11
                //try again, if this is the first failed initialization
                if (m_iFailedInitializationAtttempts == 0)
                {
                    m_iFailedInitializationAtttempts++;

                    //GLOG 5857: Release Forte.mdb
                    Data.Application.Logout();

                    //GLOG : 8883 : jsw
                    if (!Registry.Is64Bit())
                    {
                        //repair dbs
                        LMP.Data.Application.RepairUserDBs();
                    }

                    //attempt to initialize again
                    Session.Initialize(xSystemUserID, oCurWordApp, bAdminMode);

                    if (m_bInitialized)
                    {
                        //if we got here, the second initialization was successful -
                        //clear failed initialization attempts
                        m_iFailedInitializationAtttempts = 0;

                        return;
                    }
                }

                throw new LMP.Exceptions.SessionException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeSession"), oE);
            }
            finally
            {
                m_bInitializationAttempted = true;
            }
        }
        private static void Initialize(int iUserID, Word.Application oCurWordApp, bool bAdminMode)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                Session.AdminMode = bAdminMode;

                bool bRet = LMP.Data.Application.Login(iUserID, bAdminMode);

                if (bRet)
                {
                    //login was successful - set up session
                    //TODO: softcode this to pull from user application settings
                    LMP.Resources.SetCulture(1033);

                    if (oCurWordApp != null)
                        LMP.Architect.Api.Application.CurrentWordApp = oCurWordApp;

                    //mark if current user is primary user
                    m_bBelongsToPrimaryUser = 
                        (Session.CurrentUser.ID.ToString() == Application.PrimaryUser);

                    m_bInitialized = true;
                }

                LMP.Benchmarks.Print(t0);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SessionException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeSession"), oE);
            }
        }
        #endregion
    }
}
