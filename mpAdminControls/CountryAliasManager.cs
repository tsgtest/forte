using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class CountryAliasManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ********************fields*********************
        DataTable m_oCountriesDT;
        OleDbDataAdapter m_oCountriesAdapter;
        private bool m_bLoadingCountries = true;
        private LMP.Data.FirmApplicationSettings m_oFirmSettings;
        #endregion
        #region ********************enumerations******************
        #endregion
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region ********************constructors******************
        public CountryAliasManager()
        {
            InitializeComponent();
        }
        #endregion
        #region ********************methods*********************
        /// <summary>
        /// Display Office Countries.
        /// </summary>
        private void BindCountriesGrid()
        {
            try
            {
                m_bLoadingCountries = true;

                //GLOG : 8031 : ceh
                string xCmd = "SELECT DISTINCT Addresses.Country as Name FROM Addresses" +
                                " INNER JOIN Offices ON Addresses.ID = Offices.AddressID" + 
                                " WHERE (((Offices.UsageState)=1)) AND (Addresses.Country <> '')" +
                                " ORDER BY Addresses.Country;";

                //get a DataTable with the Country 
                //column of the Addresses table
                OleDbCommand oCmd = new OleDbCommand();
                oCmd.Connection = LocalConnection.ConnectionObject;
                
                oCmd.CommandText = xCmd;

                m_oCountriesAdapter = new OleDbDataAdapter();
                m_oCountriesAdapter.SelectCommand = oCmd;
                OleDbCommandBuilder oCB = new OleDbCommandBuilder(m_oCountriesAdapter);

                m_oCountriesDT = new DataTable();
                m_oCountriesDT.RowChanged += new DataRowChangeEventHandler(m_oCountriesDT_RowChanged);
                m_oCountriesDT.TableNewRow += new DataTableNewRowEventHandler(m_oCountriesDT_TableNewRow);
                m_oCountriesAdapter.Fill(m_oCountriesDT);

                //Set this DataTable as the DataSource of grdCountries.
                grdCountries.DataSource = m_oCountriesDT;

                ////GLOG : 8031 : ceh
                //oCmd.CommandText = xCmd;

                //Set the display properties of the columns in the grid.
                DataGridViewColumnCollection oCols = grdCountries.Columns;
                oCols["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotBindDataToGrid"), oE);
            }
            finally
            {
                m_bLoadingCountries = false;
            }
        }

        void m_oCountriesDT_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
        }

        void m_oCountriesDT_RowChanged(object sender, DataRowChangeEventArgs e)
        {
        }

        private void UpdateRowLastEditTime(DataRow oRow)
        {
            //m_bUpdatingLastEditTime = true;
            //oRow["LastEditTime"] = LMP.Data.Application.GetCurrentEditTime();
            //m_bUpdatingLastEditTime = false;
        }

        /// <summary>
        /// Display the state records corresponding to the selected 
        /// country record.
        /// </summary>
        private void BindAliasesGrid()
        {
            try
            {

                string xCurrentCountryID = this.grdCountries.
                    CurrentRow.Cells["Name"].FormattedValue.ToString();

                string xAliases = m_oFirmSettings.CountryAliases;

                DataTable oDT = null;

                //get data table for Aliases
                oDT = new DataTable();
                oDT.Columns.Add("ID");
                oDT.Columns.Add("Name");

                string[] aAliases = xAliases.Split('�');
                for (int i = 0; i < aAliases.Length; i = i + 2)
                {
                    //GLOG : 8031 : ceh
                    if ((aAliases[i] == xCurrentCountryID) && (xCurrentCountryID != ""))
                    {
                        oDT.Rows.Add(aAliases[i], aAliases[i + 1]);
                    }
                }

                //initialize binding source
                BindingSource oBS = new BindingSource();
                oBS.DataSource = oDT;

                //bind dataset to value set grid
                this.grdAliases.DataSource = oBS;
                this.grdAliases.Columns[0].Visible = false;
                this.grdAliases.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotBindDataToControl"), oE);
            }

            finally
            {
            }
        }


        /// <summary>
        /// saves current set of Aliases to the database
        /// </summary>
        private void SaveAliases()
        {
            try
            {
                if (!this.grdAliases.IsDirty)
                    return;

                //get selected country
                string xCurrentCountryID = this.grdCountries.
                    CurrentRow.Cells["Name"].FormattedValue.ToString();

                string xExistingAliases = null;
                string xNewAliases = null;

                //get saved keyset
                xExistingAliases = m_oFirmSettings.CountryAliases;

                if (!string.IsNullOrEmpty(xExistingAliases))
                {
                    //remove existing aliases for selected country
                    string[] aAliases = xExistingAliases.Split('�');

                    for (int i = 0; i < aAliases.Length; i = i + 2)
                    {
                        if (aAliases[i] != xCurrentCountryID)
                            xNewAliases += aAliases[i] + "�" + aAliases[i + 1] + "�";
                    }
                }

                //add aliases for selected country
                foreach (DataRow oDR in ((DataTable)((BindingSource)this.grdAliases.DataSource).DataSource).Rows)
                {
                    if (oDR[1].ToString().TrimEnd() != "")
                    {
                        xNewAliases += xCurrentCountryID + "�" + oDR[1].ToString().TrimEnd() + "�";
                    }
                }

                if (xNewAliases == null)
                    xNewAliases = "";

                //save to firm app keyset record
                m_oFirmSettings.CountryAliases = xNewAliases.TrimEnd('�');

                //get updated interrogatory string
                this.grdAliases.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }

        /// <summary>
        /// Delete the selected alias
        /// </summary>
        private void DeleteAlias()
        {
            try
            {
                //do nothing if user in new row and no edits made
                if (grdAliases.CurrentRow == null || grdAliases.CurrentRow.IsNewRow)
                    return;

                if (!grdAliases.CurrentRow.Selected)
                    grdAliases.CurrentRow.Selected = true;

                //warn user they are about to delete a ValueSet item
                DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (oChoice == DialogResult.Yes)
                {
                    //remove row
                    this.grdAliases.Rows.RemoveAt(this.grdAliases.CurrentRow.Index);
                    this.grdAliases.IsDirty = true;
                    ((DataTable)((BindingSource)this.grdAliases.DataSource).DataSource).AcceptChanges();

                    SaveAliases();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// saves all data for the control - called
        /// when hosting form is closed
        /// </summary>
        public override void SaveCurrentRecord()
        {
            base.SaveCurrentRecord();
        }
        #endregion
        #region ********************event handlers*********************
        private void CountryAliasManager_Load(object sender, EventArgs e)
        {
            try
            {
                //load firm settings
                m_oFirmSettings = new FirmApplicationSettings(ForteConstants.mpFirmRecordID);
                
                //load firm office countries
                this.BindCountriesGrid();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCountries_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!m_bLoadingCountries && this.grdCountries.ContainsFocus &&
                    this.grdCountries.CurrentRow != null && !this.grdCountries.CurrentRow.IsNewRow)
                    SetGridRowHighlight(grdCountries.CurrentRow);

                //Display relevant states in grdStates
                this.BindAliasesGrid();

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Tests for the delete key and focus-changing Alt+hotkey
        /// combos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdCountries_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Assign focus to relevant grid if an 
                //Alt+hotkey combo is pressed
                if (e.Alt && e.KeyCode == Keys.A)
                {
                    if (this.grdAliases.Focus())
                        //Focus change succeeded- Highlight default selected cell
                        this.grdAliases.CurrentCell.Selected = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdAliases_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.grdAliases.ContainsFocus)
                {
                    //Unhighlight current cells in other grids
                    //if user has selected this grid.

                    if (this.grdCountries.CurrentCell != null)
                    {
                        this.grdCountries.CurrentCell.Selected = false;
                        SetGridRowHighlight(this.grdCountries.CurrentRow);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Tests for the delete key and Alt+hotkey combos that 
        /// change grid focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdAliases_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Test if the key pressed was the delete key
                //and call the DeleteAlias method if it was.
                if (e.KeyCode == Keys.Delete)
                {
                    this.DeleteAlias();
                }
                //Assign focus to relevant grid if an 
                //Alt+hotkey combo is pressed
                else if (e.Alt && e.KeyCode == Keys.C)
                {
                    if (this.grdCountries.Focus())
                        //Focus change succeeded- Highlight default selected cell
                        this.grdCountries.CurrentCell.Selected = true;
                }

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// calls routine to delete selected record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnAliasesDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdAliases.CurrentRow != null)
                    DeleteAlias(); 
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdAliases_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
             //SetGridRowHighlight(grdAliases.Rows[e.RowIndex]);
        }

        private void grdAliases_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            //ClearGridRowHighlight(grdAliases.Rows[e.RowIndex]);
        }
        private void SetGridRowHighlight(DataGridViewRow oRow)
        {
            try
            {
                if (oRow != null)
                    oRow.DefaultCellStyle.BackColor = Color.LightGray;
            }
            catch { }
        }
        private void ClearGridRowHighlight(DataGridViewRow oRow)
        {
            try
            {
                if (oRow != null && oRow.DefaultCellStyle.BackColor != oRow.DataGridView.DefaultCellStyle.BackColor)
                {
                    oRow.DefaultCellStyle.BackColor = oRow.DataGridView.DefaultCellStyle.BackColor;
                }
            }
            catch { }
        }

        private void grdCountries_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
           ClearGridRowHighlight(grdCountries.Rows[e.RowIndex]);
        }

        private void grdCountries_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            //SetGridRowHighlight(grdCountries.Rows[e.RowIndex]);
        }
        private void ClearGridHighlight(DataGridView oGrid)
        {
            try
            {
                DateTime t0 = DateTime.Now;
                foreach (DataGridViewRow oRow in oGrid.Rows)
                {
                    ClearGridRowHighlight(oRow);
                }
                LMP.Benchmarks.Print(t0, oGrid.Name);
            }
            catch { }

        }

        private void CountryAliasManager_Enter(object sender, EventArgs e)
        {
            //Set Highlight for initial Country row
            if (grdCountries.Rows.Count > 0)
                SetGridRowHighlight(grdCountries.Rows[0]);
        }

        private void grdCountries_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }
        }

        private void grdAliases_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }
        }

        private void grdAliases_ValueChanged(object sender, EventArgs e)
        {
            this.grdAliases.IsDirty = true;
        }

        private void grdAliases_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                BindingSource oBS = (BindingSource)this.grdAliases.DataSource;
                DataTable oDT = (DataTable)oBS.DataSource;
                //GLOG : 8668 : CEH
                DataRow[] oRows = oDT.Select("Name = '" + e.FormattedValue.ToString().Replace("'", "''") + "'");
                if (oRows.Length == 0)
                {
                    this.IsValid = true;
                }
                else
                {
                    this.IsValid = false;
                    if (oRows.Length == 1)
                    {
                        // The row that was found could be the row that is being editted. In such a
                        // case the entry is valid. 
                        string xExistingRow = oRows[0]["Name"].ToString();
                        string xEdittedRow = this.grdAliases.Rows[e.RowIndex].Cells["Name"].Value.ToString();
                        this.IsValid = (xExistingRow == xEdittedRow);
                    }
                    else
                    {
                        // More than 1 row has been found. There should be a maximum of 2 items: 
                        this.IsValid = false;
                    }
                }

                if (!this.IsValid)
                {
                    //generate validation message
                    MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateAliasName"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.grdAliases.CurrentCell = this.grdAliases.Rows[e.RowIndex].Cells[e.ColumnIndex];
                }

                //cancel navigation if cell data not valid
                e.Cancel = !this.IsValid;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdAliases_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                SaveAliases();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }


        #region ********************internal procedures******************
        #endregion
        #endregion
    }
}

