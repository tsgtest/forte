using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.OleDb;

namespace LMP.Administration.Controls
{
    public partial class LegacyDocumentsManager : AdminManagerBase
    {
        private OleDbDataAdapter m_oMappingsAdapter = null;
        private const string LEGACY_MAPPINGS_SETID = "-2";
        DataTable m_oMappingsDataTable;

        public LegacyDocumentsManager()
        {
            InitializeComponent();
            treeSegments.DisplayOptions = LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders |
                LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments;
            treeSegments.ExecuteFinalSetup();
        }

        private void LegacyDocumentsManager_Load(object sender, EventArgs e)
        {
            try
            {
                base.BeforeControlUnloaded += new BeforeControlUnLoadedHandler(LegacyDocumentsManager_BeforeControlUnloaded);
                base.AfterControlLoaded += new AfterControlLoadedHandler(LegacyDocumentsManager_AfterControlLoaded);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void LegacyDocumentsManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {
                m_oMappingsDataTable = new DataTable();

                // Get the list of legacy doc types.
                List<LMP.DocAnalyzer.DocType> oDocTypes = LMP.DocAnalyzer.Definitions.GetDocTypesList();

                this.MappingsAdapter.Fill(m_oMappingsDataTable);

                // Create a hash of the Legacy document type so that we can determine which legacy
                // documents types are missing in the value sets table and which are referred to 
                // which are no longer in the list of legacy doc types.
                Hashtable oLegacyDocTypesHash = new Hashtable();

                for (int i = 0; i < oDocTypes.Count; i++)
                {
                    oLegacyDocTypesHash.Add(oDocTypes[i].Name, oDocTypes[i].Name);
                }

                // Insert missing legacy doc types.
                for (int i = 0; i < oDocTypes.Count; i++)
                {
                    DataRow[] aoMatchingRows = null;

                    try
                    {
                        aoMatchingRows = m_oMappingsDataTable.Select("Value1 = '" + oDocTypes[i].Name + "'");
                    }
                    catch { }

                    if (aoMatchingRows == null || aoMatchingRows.Length == 0)
                    {
                        DataRow oNewRow = m_oMappingsDataTable.Rows.Add();
                        oNewRow["SetID"] = LEGACY_MAPPINGS_SETID;
                        oNewRow["Value1"] = oDocTypes[i].Name;
                        oNewRow["Value2"] = "";
                    }
                }

                // Create a list of the indexes of rows that are obsolete.
                ArrayList aliObsoleteRowIndexes = new ArrayList();

                for (int i = 0; i < m_oMappingsDataTable.Rows.Count; i++)
                {
                    if (!oLegacyDocTypesHash.ContainsKey((string)m_oMappingsDataTable.Rows[i][1]))
                    {
                        aliObsoleteRowIndexes.Add(i);
                    }
                }

                // Remove the obselete legacy doc types.

                foreach (int i in aliObsoleteRowIndexes)
                {
                    m_oMappingsDataTable.Rows[i].Delete();
                }

                this.grdMappings.Columns.Clear();
                this.grdMappings.DataSource = m_oMappingsDataTable;

                // Save any modifications that were made to the table.
                SaveMappings();

                // Hide the SetID field.
                this.grdMappings.Columns[0].Visible = false;

                // Setup the Legacy Document Type column.
                this.grdMappings.Columns[1].Width = 145;
                this.grdMappings.Columns[1].ReadOnly = true;
                this.grdMappings.Columns[1].HeaderText = "Legacy Document Type";

                // Setup the Segment column.
                this.grdMappings.Columns[2].Width = 195;
                this.grdMappings.Columns[2].ReadOnly = true;
                this.grdMappings.Columns[2].HeaderText = "MacPac 10 Segment Type";

                // Hide the TranslationID field.
                this.grdMappings.Columns[3].Visible = false;

                // Hide the Last Update Time field.
                this.grdMappings.Columns[4].Visible = false;

                // Prevent the user from deleting rows.
                this.grdMappings.AllowUserToDeleteRows = false;

                // Prevent changes to the Value1 field.
                this.m_oMappingsDataTable.Columns["Value1"].ReadOnly = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void LegacyDocumentsManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                // When switching to another control, save the mappings.
                SaveMappings();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private OleDbDataAdapter MappingsAdapter
        {        
            get
            {
                if(m_oMappingsAdapter == null)
                {
                    // Setup the adapter with all the appropriate sql for each operation.
                    OleDbConnection oConnection = LMP.Data.LocalConnection.ConnectionObject;
                    m_oMappingsAdapter = new OleDbDataAdapter(
                        "Select * from ValueSets where SetID = " + LEGACY_MAPPINGS_SETID , oConnection);
                    
                    m_oMappingsAdapter.UpdateCommand = new OleDbCommand(  
                        "UPDATE ValueSets SET Value2 = ? WHERE Value1 = ? AND SetID = " + 
                        LEGACY_MAPPINGS_SETID, oConnection);
                    
                    m_oMappingsAdapter.UpdateCommand.Parameters.Add("@Value2", OleDbType.VarChar,255, "Value2");
                    m_oMappingsAdapter.UpdateCommand.Parameters.Add("@Value1", OleDbType.VarChar, 255, "Value1");

                    m_oMappingsAdapter.InsertCommand = new OleDbCommand(
                        "Insert INTO ValueSets(SetID, Value1, Value2) Values(" + LEGACY_MAPPINGS_SETID + 
                            ", ?, ?)" , oConnection);
                    m_oMappingsAdapter.InsertCommand.Parameters.Add("@Value1", OleDbType.VarChar,255, "Value1");
                    m_oMappingsAdapter.InsertCommand.Parameters.Add("@Value2", OleDbType.VarChar, 255, "Value2");

                    m_oMappingsAdapter.DeleteCommand = new OleDbCommand(
                        "Delete FROM ValueSets where SetID = " + LEGACY_MAPPINGS_SETID + 
                            " AND Value1 = ?" , oConnection);
                    m_oMappingsAdapter.InsertCommand.Parameters.Add("@Value1", OleDbType.VarChar,255, "Value1");
                }

                return m_oMappingsAdapter;
            }
        }

        private void SaveMappings()
        {        
            // Allow the adapter's update method to determine what operations 
            // need to be performed to synchronize the data set with the db.
            this.MappingsAdapter.Update(m_oMappingsDataTable);
        }
        
        private void treeSegments_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                LMP.Data.FolderMember oMember = treeSegments
                    .SelectedNodes[0].Tag as LMP.Data.FolderMember;
                int iRow = this.grdMappings.CurrentCell.RowIndex;

                // If a folder member is selected, assign the 
                //segment to the corresponding legacy doc type.
                if (oMember != null && iRow >= 0 && m_oMappingsDataTable != null)
                {
                    m_oMappingsDataTable.Rows[iRow]["Value2"] = oMember.Name;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
