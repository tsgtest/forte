namespace LMP.Administration.Controls
{
    partial class CouriersManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CouriersManager));
            this.grdCouriers = new LMP.Controls.DataGridView();
            this.scInternalLists = new System.Windows.Forms.SplitContainer();
            this.cbxOffices = new LMP.Controls.ComboBox();
            this.lblOffice = new System.Windows.Forms.Label();
            this.grdProperties = new System.Windows.Forms.DataGridView();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tsep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNew = new System.Windows.Forms.ToolStripButton();
            this.tsep2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.tsep3 = new System.Windows.Forms.ToolStripSeparator();
            this.tlblFindCourier = new System.Windows.Forms.ToolStripLabel();
            this.ttxtFindCourier = new System.Windows.Forms.ToolStripTextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdCouriers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scInternalLists)).BeginInit();
            this.scInternalLists.Panel1.SuspendLayout();
            this.scInternalLists.Panel2.SuspendLayout();
            this.scInternalLists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).BeginInit();
            this.tsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdCouriers
            // 
            this.grdCouriers.AllowUserToAddRows = false;
            this.grdCouriers.AllowUserToDeleteRows = false;
            this.grdCouriers.AllowUserToResizeColumns = false;
            this.grdCouriers.AllowUserToResizeRows = false;
            this.grdCouriers.BackgroundColor = System.Drawing.Color.White;
            this.grdCouriers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdCouriers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdCouriers.ColumnHeadersHeight = 21;
            this.grdCouriers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdCouriers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCouriers.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.grdCouriers.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdCouriers.IsDirty = false;
            this.grdCouriers.Location = new System.Drawing.Point(0, 0);
            this.grdCouriers.MultiSelect = false;
            this.grdCouriers.Name = "grdCouriers";
            this.grdCouriers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdCouriers.RowHeadersVisible = false;
            this.grdCouriers.RowHeadersWidth = 25;
            this.grdCouriers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdCouriers.RowTemplate.Height = 20;
            this.grdCouriers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdCouriers.Size = new System.Drawing.Size(171, 465);
            this.grdCouriers.SupportingValues = "";
            this.grdCouriers.TabIndex = 1;
            this.grdCouriers.Tag2 = null;
            this.grdCouriers.Value = null;
            this.grdCouriers.TabPressed += new LMP.Controls.TabPressedHandler(this.grdCouriers_TabPressed);
            this.grdCouriers.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCouriers_CellEnter);
            this.grdCouriers.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCouriers_CellValidated);
            this.grdCouriers.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdCouriers_CellValidating);
            this.grdCouriers.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdCouriers_ColumnAdded);
            this.grdCouriers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdCouriers_KeyDown);
            // 
            // scInternalLists
            // 
            this.scInternalLists.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scInternalLists.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scInternalLists.Location = new System.Drawing.Point(0, 28);
            this.scInternalLists.Name = "scInternalLists";
            // 
            // scInternalLists.Panel1
            // 
            this.scInternalLists.Panel1.Controls.Add(this.grdCouriers);
            // 
            // scInternalLists.Panel2
            // 
            this.scInternalLists.Panel2.Controls.Add(this.cbxOffices);
            this.scInternalLists.Panel2.Controls.Add(this.lblOffice);
            this.scInternalLists.Panel2.Controls.Add(this.grdProperties);
            this.scInternalLists.Size = new System.Drawing.Size(528, 469);
            this.scInternalLists.SplitterDistance = 175;
            this.scInternalLists.TabIndex = 27;
            this.scInternalLists.TabStop = false;
            // 
            // cbxOffices
            // 
            this.cbxOffices.AllowEmptyValue = false;
            this.cbxOffices.Borderless = false;
            this.cbxOffices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cbxOffices.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxOffices.IsDirty = false;
            this.cbxOffices.LimitToList = true;
            this.cbxOffices.ListName = "";
            this.cbxOffices.Location = new System.Drawing.Point(49, 6);
            this.cbxOffices.MaxDropDownItems = 8;
            this.cbxOffices.Name = "cbxOffices";
            this.cbxOffices.SelectedIndex = -1;
            this.cbxOffices.SelectedValue = null;
            this.cbxOffices.SelectionLength = 0;
            this.cbxOffices.SelectionStart = 0;
            this.cbxOffices.Size = new System.Drawing.Size(288, 23);
            this.cbxOffices.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cbxOffices.SupportingValues = "";
            this.cbxOffices.TabIndex = 5;
            this.cbxOffices.Tag2 = null;
            this.cbxOffices.Value = "";
            // 
            // lblOffice
            // 
            this.lblOffice.AutoSize = true;
            this.lblOffice.Location = new System.Drawing.Point(3, 9);
            this.lblOffice.Name = "lblOffice";
            this.lblOffice.Size = new System.Drawing.Size(39, 15);
            this.lblOffice.TabIndex = 2;
            this.lblOffice.Text = "&Office:";
            // 
            // grdProperties
            // 
            this.grdProperties.AllowUserToAddRows = false;
            this.grdProperties.AllowUserToDeleteRows = false;
            this.grdProperties.AllowUserToResizeColumns = false;
            this.grdProperties.AllowUserToResizeRows = false;
            this.grdProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProperties.BackgroundColor = System.Drawing.Color.White;
            this.grdProperties.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdProperties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProperties.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdProperties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdProperties.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grdProperties.Location = new System.Drawing.Point(3, 35);
            this.grdProperties.MultiSelect = false;
            this.grdProperties.Name = "grdProperties";
            this.grdProperties.RowHeadersVisible = false;
            this.grdProperties.RowTemplate.Height = 20;
            this.grdProperties.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdProperties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdProperties.Size = new System.Drawing.Size(334, 427);
            this.grdProperties.TabIndex = 4;
            this.grdProperties.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProperties_CellEnter);
            this.grdProperties.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProperties_CellValidated);
            this.grdProperties.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdProperties_CellValidating);
            // 
            // tsMain
            // 
            this.tsMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsMain.AutoSize = false;
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsep1,
            this.tbtnNew,
            this.tsep2,
            this.tbtnDelete,
            this.tsep3,
            this.tlblFindCourier,
            this.ttxtFindCourier});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(78, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMain.Size = new System.Drawing.Size(450, 29);
            this.tsMain.TabIndex = 0;
            // 
            // tsep1
            // 
            this.tsep1.Name = "tsep1";
            this.tsep1.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnNew
            // 
            this.tbtnNew.AutoSize = false;
            this.tbtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNew.Image")));
            this.tbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNew.Name = "tbtnNew";
            this.tbtnNew.Size = new System.Drawing.Size(53, 22);
            this.tbtnNew.Text = "&New...";
            this.tbtnNew.Click += new System.EventHandler(this.tbtnNew_Click);
            // 
            // tsep2
            // 
            this.tsep2.Name = "tsep2";
            this.tsep2.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(53, 26);
            this.tbtnDelete.Text = "&Delete...";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // tsep3
            // 
            this.tsep3.Name = "tsep3";
            this.tsep3.Size = new System.Drawing.Size(6, 29);
            // 
            // tlblFindCourier
            // 
            this.tlblFindCourier.AutoSize = false;
            this.tlblFindCourier.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tlblFindCourier.Image = ((System.Drawing.Image)(resources.GetObject("tlblFindCourier.Image")));
            this.tlblFindCourier.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlblFindCourier.Name = "tlblFindCourier";
            this.tlblFindCourier.Size = new System.Drawing.Size(50, 22);
            this.tlblFindCourier.Text = "&Find:";
            // 
            // ttxtFindCourier
            // 
            this.ttxtFindCourier.Name = "ttxtFindCourier";
            this.ttxtFindCourier.Size = new System.Drawing.Size(250, 29);
            this.ttxtFindCourier.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ttxtFindCourier_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Courier";
            // 
            // CouriersManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.scInternalLists);
            this.Controls.Add(this.tsMain);
            this.Name = "CouriersManager";
            this.Size = new System.Drawing.Size(528, 497);
            this.Load += new System.EventHandler(this.CouriersManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCouriers)).EndInit();
            this.scInternalLists.Panel1.ResumeLayout(false);
            this.scInternalLists.Panel2.ResumeLayout(false);
            this.scInternalLists.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scInternalLists)).EndInit();
            this.scInternalLists.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).EndInit();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LMP.Controls.DataGridView grdCouriers;
        private System.Windows.Forms.SplitContainer scInternalLists;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripSeparator tsep1;
        private System.Windows.Forms.ToolStripLabel tlblFindCourier;
        private System.Windows.Forms.ToolStripTextBox ttxtFindCourier;
        private System.Windows.Forms.ToolStripSeparator tsep2;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ToolStripSeparator tsep3;
        private System.Windows.Forms.DataGridView grdProperties;
        private System.Windows.Forms.Label lblOffice;
        private System.Windows.Forms.ToolStripButton tbtnNew;
        private System.Windows.Forms.Label label1;
        private LMP.Controls.ComboBox cbxOffices;


    }
}
