﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class TemplateTrailersForm : Form
    {
        Hashtable m_oTrailerIDs = new Hashtable();
        Hashtable m_oTrailerIndices = new Hashtable();
        LMP.Data.AdminSegmentDefs m_oTrailerDefs = new LMP.Data.AdminSegmentDefs(LMP.Data.mpObjectTypes.Trailer);
        List<FirmApplicationSettings.TemplateTrailerOptions> m_oTemplateOptions = new List<FirmApplicationSettings.TemplateTrailerOptions>();
        bool m_bIsDirty = false;

        public TemplateTrailersForm(List<FirmApplicationSettings.TemplateTrailerOptions> oTemplateOptions)
        {
            m_oTemplateOptions = oTemplateOptions;
            InitializeComponent();
        }

        private void TemplateTrailersForm_Load(object sender, EventArgs e)
        {
            Array oArray = m_oTrailerDefs.ToArray(1, 0);
            grpTrailerDialog.Enabled = false;
            foreach (System.Array a in oArray)
            {
                //add segment display name to the list
                int iIndex = this.lstTrailers.Items.Add(a.GetValue(0).ToString());

                //store in field hashtables for ID mapping
                m_oTrailerIDs.Add(a.GetValue(1), iIndex);
                m_oTrailerIndices.Add(iIndex, a.GetValue(1));
            }
            for (int i = 0; i < m_oTemplateOptions.Count; i++)
            {
                lstTemplates.Items.Add(m_oTemplateOptions[i].TemplateName);
            }
            if (m_oTemplateOptions.Count > 0)
                lstTemplates.SelectedIndex = 0;
        }
        public List<FirmApplicationSettings.TemplateTrailerOptions> TemplateTrailerOptions
        {
            get
            {
                return m_oTemplateOptions;
            }
        }
        /// <summary>
        /// loads the appropriate list of default trailers into the dropdown
        /// </summary>
        private void LoadDefaultTrailers(FirmApplicationSettings.TemplateTrailerOptions oOptions)
        {
            bool bDefaultIsInList = false;

            //get current default trailer
            int iDef = oOptions.DefaultTrailerID;
            //load Default Trailer combobox with NoTrailerIntegration choice
            ArrayList oTrailers = new ArrayList();
            oTrailers.Insert(0, new object[] { "-999999", LMP.Resources.GetLangString("Prompt_NoTrailerIntegration") });

            //Clear all checked items
            for (int i = 0; i < lstTrailers.Items.Count; i++)
            {
                this.lstTrailers.SetItemChecked(i, false);
            }
            if (!string.IsNullOrEmpty(oOptions.TrailerIDs))
            {
                string[] aCheckedItems = oOptions.TrailerIDs.Split(',');
                //select the default trailers
                foreach (string s in aCheckedItems)
                {
                    int i = Int32.Parse(s);
                    //GLOG 7237: Skip any Trailers that have been deleted
                    if (m_oTrailerIDs.Contains(i))
                    {
                        //get the index of the trailer in the hashtable
                        int iIndex = (int)m_oTrailerIDs[i];
                        this.lstTrailers.SetItemChecked(iIndex, true);
                    }
                }
            }
            int j = 0;

            Array oArray = m_oTrailerDefs.ToArray(1, 0);
            foreach (System.Array a in oArray)
            {
                //get id of trailer type
                int iIndex = (int)a.GetValue(1);

                //get selected trailer IDs
                int[] aIDs = new int[this.lstTrailers.CheckedIndices.Count];
                for (int i = 0; i < this.lstTrailers.CheckedIndices.Count; i++)
                {
                    aIDs[i] = (int)m_oTrailerIndices[this.lstTrailers.CheckedIndices[i]];
                }

                //cycle through default trailers and add to list
                foreach (int i in aIDs)
                {
                    //check for match
                    if (iIndex == i)
                    {
                        j = j + 1;
                        oTrailers.Insert(j, new object[] { a.GetValue(1), a.GetValue(0).ToString() });
                        if (iDef == i)
                            bDefaultIsInList = true;
                        break;
                    }
                }
            }

            //Load Default trailer combobox with selected defaults
            this.cmbTrailerDef.SetList(oTrailers);

            //Load Default trailer
            try
            {
                if (bDefaultIsInList)
                    this.cmbTrailerDef.SelectedValue = iDef;
                else
                    this.cmbTrailerDef.SelectedIndex = 0;
            }
            catch { }

        }

        private void lstTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int iIndex = lstTemplates.SelectedIndex;
                if (iIndex < 0)
                {
                    btnDelete.Enabled = false;
                    btnEdit.Enabled = false;
                    grpTrailerDialog.Enabled = false;
                }
                else
                {
                    btnDelete.Enabled = true;
                    btnEdit.Enabled = true;
                    grpTrailerDialog.Enabled = true;
                    LoadDefaultTrailers(m_oTemplateOptions[iIndex]);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstTrailers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int iIndex = lstTrailers.SelectedIndex;

                int iTemplateIndex = lstTemplates.SelectedIndex;
                string xChecked = "";
                foreach (int i in lstTrailers.CheckedIndices)
                {
                    xChecked += m_oTrailerIndices[i] + ",";
                }
                xChecked = xChecked.TrimEnd(',');
                FirmApplicationSettings.TemplateTrailerOptions oOptions = m_oTemplateOptions[iTemplateIndex];
                oOptions.TrailerIDs = xChecked;
                m_oTemplateOptions[iTemplateIndex] = oOptions;
                LoadDefaultTrailers(oOptions);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                TemplateNameForm oForm = new TemplateNameForm();
                if (oForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string xText = oForm.NamePattern;
                    FirmApplicationSettings.TemplateTrailerOptions oOptions = new FirmApplicationSettings.TemplateTrailerOptions();
                    oOptions.TemplateName = xText;
                    m_oTemplateOptions.Add(oOptions);
                    int iIndex = lstTemplates.Items.Add(oOptions.TemplateName);
                    lstTemplates.SelectedIndex = iIndex;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void cmbTrailerDef_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int iTemplateIndex = lstTemplates.SelectedIndex;
                if (iTemplateIndex > -1)
                {
                    FirmApplicationSettings.TemplateTrailerOptions oOptions = m_oTemplateOptions[iTemplateIndex];

                    if (cmbTrailerDef.SelectedValue != null)
                        oOptions.DefaultTrailerID = Int32.Parse(cmbTrailerDef.SelectedValue.ToString());
                    else
                        oOptions.DefaultTrailerID = 0;
                    m_oTemplateOptions[iTemplateIndex] = oOptions;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int iSel = lstTemplates.SelectedIndex;
                if (iSel < 0)
                    return;
                string xText = lstTemplates.Text;
                TemplateNameForm oForm = new TemplateNameForm();
                oForm.NamePattern = xText;
                if (oForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    xText = oForm.NamePattern;
                    FirmApplicationSettings.TemplateTrailerOptions oOptions = m_oTemplateOptions[iSel];
                    oOptions.TemplateName = xText;
                    m_oTemplateOptions[iSel] = oOptions;
                    lstTemplates.Items.RemoveAt(iSel);
                    lstTemplates.Items.Insert(iSel, xText);
                    lstTemplates.SelectedIndex = iSel;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int iSel = lstTemplates.SelectedIndex;
                if (iSel < 0)
                    return;
                lstTemplates.Items.RemoveAt(iSel);
                m_oTemplateOptions.RemoveAt(iSel);
                lstTemplates.SelectedIndex = Math.Min(iSel, lstTemplates.Items.Count - 1);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
