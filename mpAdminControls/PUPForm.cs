using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.Administration
{
    public partial class PUPForm : Form
    {
        public PUPForm()
        {
            InitializeComponent();
            this.txtIISServer.Text = LMP.Registry.GetLocalMachineValue(
                LMP.Data.ForteConstants.mpSyncRegKey, "ServerIP");
            this.txtMacPacApplicationName.Text = "ForteSyncServer";

        }

        public string SourceServer
        {
            get { return this.txtSourceServer.Text; }
            set { this.txtSourceServer.Text = value; }
        }

        public string SourceDatabase
        {
            get { return this.txtSourceDatabase.Text; }
            set { this.txtSourceDatabase.Text = value; }
        }

        public bool UpdateLicenses
        {
            get { return this.chkUpdateLicenses.Checked; }
            set { this.chkUpdateLicenses.Checked = value; }
        }

        public string TargetServer
        {
            get { return this.txtTargetServer.Text; }
            set { this.txtTargetServer.Text = value; }
        }

        public string TargetDatabase
        {
            get { return this.txtTargetDatabase.Text; }
            set { this.txtTargetDatabase.Text = value; }
        }

        public bool PublishAfterUpdate
        {
            get { return this.chkPublishImmediately.Checked; }
            set { this.chkPublishImmediately.Checked = value; }
        }

        public string IISServer
        {
            get { return this.txtIISServer.Text; }
            set { this.txtIISServer.Text = value; }
        }

        public string ServerApplicationName
        {
            get { return this.txtMacPacApplicationName.Text; }
            set { this.txtMacPacApplicationName.Text = value; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                //if (this.chkPublishImmediately.Checked)
                //{
                //    if (this.txtSourceServer.Text == "")
                //    {
                //        MessageBox.Show("Server is required information",
                //            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                //            MessageBoxIcon.Exclamation);

                //        this.txtSourceServer.Focus();
                //        return;
                //    }
                //    else if (this.txtSourceDatabase.Text == "")
                //    {
                //        MessageBox.Show("Database is required information",
                //            LMP.ComponentProperties.ProductName, MessageBoxButtons.OK,
                //            MessageBoxIcon.Exclamation);

                //        this.txtSourceDatabase.Focus();
                //        return;
                //    }
                //}

                this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkPublishImmediately_CheckedChanged(object sender, EventArgs e)
        {
            this.grpTarget.Enabled = this.chkPublishImmediately.Checked;
            this.grpIISServer.Enabled = this.chkPublishImmediately.Checked;
        }

        private void PUPForm_Load(object sender, EventArgs e)
        {
            if (LMP.MacPac.MacPacImplementation.IsFullLocal)
            {
                this.chkPublishImmediately.Visible = false;
                this.chkPublishImmediately.Checked = false;
                this.grpTarget.Visible = false;
                this.grpIISServer.Visible = false;
                this.Height = this.grpTarget.Top +  100;
                this.txtIISServer.Text = "";
                this.txtSourceDatabase.Text = "";
                this.txtSourceServer.Text = "";
                this.txtMacPacApplicationName.Text = "";
                this.txtTargetDatabase.Text = "";
                this.txtTargetServer.Text = "";
            }
        }
    }
}