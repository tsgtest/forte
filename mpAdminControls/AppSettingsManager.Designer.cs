namespace LMP.Administration.Controls
{
    partial class AppSettingsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppSettingsManager));
            this.colorDlgBackColor = new System.Windows.Forms.ColorDialog();
            this.fbdDistributableFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.ttButtons = new System.Windows.Forms.ToolTip(this.components);
            this.btnResetAllEntitiesToFirmDefaults = new System.Windows.Forms.Button();
            this.btnResetTabForAllEntities = new System.Windows.Forms.Button();
            this.btnResetToFirmDefaults = new System.Windows.Forms.Button();
            this.btnResetTab = new System.Windows.Forms.Button();
            this.btnResetUserForAllEntities = new System.Windows.Forms.Button();
            this.btnResetUserToFirmDefaults = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabFirmSettings = new System.Windows.Forms.TabPage();
            this.gbResetAll = new System.Windows.Forms.GroupBox();
            this.gbResetEntity = new System.Windows.Forms.GroupBox();
            this.tabFirmAppSettings = new System.Windows.Forms.TabControl();
            this.tabDisplay = new System.Windows.Forms.TabPage();
            this.chkFinishButtonAtEnd = new System.Windows.Forms.CheckBox();
            this.grpHelpFont = new System.Windows.Forms.GroupBox();
            this.numUpDnHelpFontSize = new LMP.Controls.Spinner();
            this.txtHelpFontName = new System.Windows.Forms.TextBox();
            this.lblHelpFontSize = new System.Windows.Forms.Label();
            this.lblHelpFontName = new System.Windows.Forms.Label();
            this.chkExpandChildSegments = new System.Windows.Forms.CheckBox();
            this.grpContentManager = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSharedFolderLabel = new System.Windows.Forms.TextBox();
            this.lblSharedFoldersLabel = new System.Windows.Forms.Label();
            this.btnSharedFoldersHelpText = new System.Windows.Forms.Button();
            this.txtSharedFoldersHelpText = new System.Windows.Forms.TextBox();
            this.lblSharedFoldersHelpText = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMyFoldersLabel = new System.Windows.Forms.TextBox();
            this.lblMyFoldersLabel = new System.Windows.Forms.Label();
            this.btnMyFoldersHelpText = new System.Windows.Forms.Button();
            this.txtMyFoldersHelpText = new System.Windows.Forms.TextBox();
            this.lblMyFoldersHelpText = new System.Windows.Forms.Label();
            this.lblPublicFolders = new System.Windows.Forms.Label();
            this.txtPublicFoldersLabel = new System.Windows.Forms.TextBox();
            this.btnPublicFoldersHelpText = new System.Windows.Forms.Button();
            this.lblPublicFoldersHelpText = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPublicFoldersHelpText = new System.Windows.Forms.TextBox();
            this.grpPeopleList = new System.Windows.Forms.GroupBox();
            this.numUpDnSavedDataThreshold = new System.Windows.Forms.NumericUpDown();
            this.numUpDnSavedDataLimit = new System.Windows.Forms.NumericUpDown();
            this.lblSavedDataThreshold = new System.Windows.Forms.Label();
            this.numUpDnUserContentThreshold = new System.Windows.Forms.NumericUpDown();
            this.numUpDnUserContentLimit = new System.Windows.Forms.NumericUpDown();
            this.lblUserContentLimit = new System.Windows.Forms.Label();
            this.lblUserContentThreshold = new System.Windows.Forms.Label();
            this.numUpDnMaxPeople = new System.Windows.Forms.NumericUpDown();
            this.lblMaxPeople = new System.Windows.Forms.Label();
            this.numUpDnPeopleListWarningThreshold = new System.Windows.Forms.NumericUpDown();
            this.lblPeopleListWarningThreshold = new System.Windows.Forms.Label();
            this.tabTrailers = new System.Windows.Forms.TabPage();
            this.chkPromptForLegacyTrailer = new System.Windows.Forms.CheckBox();
            this.chkUse9xStyleTrailer = new System.Windows.Forms.CheckBox();
            this.cmbDefaultTrailerBehavior = new System.Windows.Forms.ComboBox();
            this.gbFrontEndProfiling = new System.Windows.Forms.GroupBox();
            this.chkCloseOnCancel = new System.Windows.Forms.CheckBox();
            this.chkUseFrontEndProfiling = new System.Windows.Forms.CheckBox();
            this.lblDefaultTrailerBehavior = new System.Windows.Forms.Label();
            this.chkSkipTrailerForTempFiles = new System.Windows.Forms.CheckBox();
            this.grpTrailerDialog = new System.Windows.Forms.GroupBox();
            this.btnTemplateDefaults = new System.Windows.Forms.Button();
            this.cmbTrailerDef = new LMP.Controls.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTrailers = new System.Windows.Forms.Label();
            this.lstTrailers = new System.Windows.Forms.CheckedListBox();
            this.chkAllowUsersToSetTrailerBehavior = new System.Windows.Forms.CheckBox();
            this.btnDMSMessage = new System.Windows.Forms.Button();
            this.gbIntegration = new System.Windows.Forms.GroupBox();
            this.chkFileExit = new System.Windows.Forms.CheckBox();
            this.chkFilePrintDefault = new System.Windows.Forms.CheckBox();
            this.chkFilePrint = new System.Windows.Forms.CheckBox();
            this.chkFileCloseAll = new System.Windows.Forms.CheckBox();
            this.chkFileClose = new System.Windows.Forms.CheckBox();
            this.chkFileSaveAll = new System.Windows.Forms.CheckBox();
            this.chkFileSaveAs = new System.Windows.Forms.CheckBox();
            this.chkFileSave = new System.Windows.Forms.CheckBox();
            this.chkFileOpen = new System.Windows.Forms.CheckBox();
            this.lblDMS = new System.Windows.Forms.Label();
            this.cmbDocManager = new LMP.Controls.ComboBox();
            this.tabDraftStamps = new System.Windows.Forms.TabPage();
            this.chkSetRemoveAllStampsAsFirstInList = new System.Windows.Forms.CheckBox();
            this.lblDraftStamps = new System.Windows.Forms.Label();
            this.lstDraftStamps = new System.Windows.Forms.CheckedListBox();
            this.tabSync = new System.Windows.Forms.TabPage();
            this.grpDistributableCopy = new System.Windows.Forms.GroupBox();
            this.btnDistributableFolder = new System.Windows.Forms.Button();
            this.txtDistributableFolder = new System.Windows.Forms.TextBox();
            this.lblDistributableFolder = new System.Windows.Forms.Label();
            this.grpSynSchedule = new System.Windows.Forms.GroupBox();
            this.chkLogUserSync = new System.Windows.Forms.CheckBox();
            this.cmbDay = new System.Windows.Forms.ComboBox();
            this.lblDay = new System.Windows.Forms.Label();
            this.cmbSyncCompletePrompt = new LMP.Controls.ComboBox();
            this.lblSyncCompletePrompt = new System.Windows.Forms.Label();
            this.cmbSyncPrompt = new LMP.Controls.ComboBox();
            this.lblSyncPrompt = new System.Windows.Forms.Label();
            this.udFreqInterval = new System.Windows.Forms.NumericUpDown();
            this.lblCurSynScheduleDesc = new System.Windows.Forms.Label();
            this.dtTime = new System.Windows.Forms.DateTimePicker();
            this.lblTime = new System.Windows.Forms.Label();
            this.cmbFreqUnit = new System.Windows.Forms.ComboBox();
            this.lblFreqInterval = new System.Windows.Forms.Label();
            this.lblFreqUnit = new System.Windows.Forms.Label();
            this.grpConnectionParameters = new System.Windows.Forms.GroupBox();
            this.txtServerAppName = new System.Windows.Forms.TextBox();
            this.lblServerAppName = new System.Windows.Forms.Label();
            this.txtIISServerIP = new System.Windows.Forms.TextBox();
            this.lblIISServerIP = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.tabFeatures = new System.Windows.Forms.TabPage();
            this.grpAlerts = new System.Windows.Forms.GroupBox();
            this.chkPromptAboutUnconvertedTablesInDesign = new System.Windows.Forms.CheckBox();
            this.chkAlertWhenNonMP10DocumentOpened = new System.Windows.Forms.CheckBox();
            this.chkAlertTagsRemoved = new System.Windows.Forms.CheckBox();
            this.grpGeneralFeatures = new System.Windows.Forms.GroupBox();
            this.chkClearUndoStack = new System.Windows.Forms.CheckBox();
            this.chkShowSegmentSpecificRibbons = new System.Windows.Forms.CheckBox();
            this.chkAllowSavedDataCreationOnDeautomation = new System.Windows.Forms.CheckBox();
            this.chkAllowPrivatePeople = new System.Windows.Forms.CheckBox();
            this.chkAllowProxying = new System.Windows.Forms.CheckBox();
            this.chkAllowNormalStyleUpdates = new System.Windows.Forms.CheckBox();
            this.chkAllowLegacyDocs = new System.Windows.Forms.CheckBox();
            this.chkAllowSharedSegments = new System.Windows.Forms.CheckBox();
            this.grpAutomation = new System.Windows.Forms.GroupBox();
            this.chkFinishOnOpen = new System.Windows.Forms.CheckBox();
            this.chkRemoveTextSegmentTags = new System.Windows.Forms.CheckBox();
            this.chkAllowDocumentMemberTagRemoval = new System.Windows.Forms.CheckBox();
            this.chkAllowDocumentTagRemoval = new System.Windows.Forms.CheckBox();
            this.chkAllowSegmentMemberTagRemoval = new System.Windows.Forms.CheckBox();
            this.chkAllowVarBlockTagRemoval = new System.Windows.Forms.CheckBox();
            this.tabMailSetup = new System.Windows.Forms.TabPage();
            this.chkAllowErrorEmails = new System.Windows.Forms.CheckBox();
            this.grpMailLogin = new System.Windows.Forms.GroupBox();
            this.txtMailPort = new System.Windows.Forms.TextBox();
            this.lblMailPort = new System.Windows.Forms.Label();
            this.chkUseNetworkCredentials = new System.Windows.Forms.CheckBox();
            this.chkUseSSL = new System.Windows.Forms.CheckBox();
            this.txtMailPWD = new System.Windows.Forms.TextBox();
            this.lblMailUserID = new System.Windows.Forms.Label();
            this.lblMailPWD = new System.Windows.Forms.Label();
            this.txtMailUserID = new System.Windows.Forms.TextBox();
            this.txtMailHost = new System.Windows.Forms.TextBox();
            this.txtAdminEMail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAdminEMail = new System.Windows.Forms.Label();
            this.tabSecurity = new System.Windows.Forms.TabPage();
            this.chkBase64Encoding = new System.Windows.Forms.CheckBox();
            this.tsProtectionPasswords = new System.Windows.Forms.ToolStrip();
            this.tlblPassword = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnPasswordNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnPasswordDelete = new System.Windows.Forms.ToolStripButton();
            this.grdPasswords = new LMP.Controls.DataGridView();
            this.tabOther = new System.Windows.Forms.TabPage();
            this.txtAdvancedAddressBookEmailPrefix = new System.Windows.Forms.TextBox();
            this.txtAdvancedAddressBookFaxPrefix = new System.Windows.Forms.TextBox();
            this.lblAdvancedAddressBookEmailPrefix = new System.Windows.Forms.Label();
            this.lblAdvancedAddressBookFaxPrefix = new System.Windows.Forms.Label();
            this.lblAdvancedAddressBookPhonePrefix = new System.Windows.Forms.Label();
            this.txtAdvancedAddressBookPhonePrefix = new System.Windows.Forms.TextBox();
            this.lblAdvancedAddressBookFormatPrefixes = new System.Windows.Forms.Label();
            this.cmbDefaultEnvelopeSegment = new System.Windows.Forms.ComboBox();
            this.lblDefaultEnvelopeSegment = new System.Windows.Forms.Label();
            this.cmbDefaultLabelSegment = new System.Windows.Forms.ComboBox();
            this.lblDefaultLabelSegment = new System.Windows.Forms.Label();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.txtProtectionPassword = new System.Windows.Forms.TextBox();
            this.lblProtectionPassword = new System.Windows.Forms.Label();
            this.txtBlankWordDocTemplate = new System.Windows.Forms.TextBox();
            this.lblBlankWordDocTemplate = new System.Windows.Forms.Label();
            this.grpCheckboxConfig = new System.Windows.Forms.GroupBox();
            this.txtUncheckedCheckboxChar = new System.Windows.Forms.TextBox();
            this.lblUncheckedCheckboxChar = new System.Windows.Forms.Label();
            this.txtCheckedCheckboxChar = new System.Windows.Forms.TextBox();
            this.lblCheckedCheckboxChar = new System.Windows.Forms.Label();
            this.lblCheckboxFont = new System.Windows.Forms.Label();
            this.cmbCheckboxFont = new LMP.Controls.FontList();
            this.grdTypes = new LMP.Controls.DataGridView();
            this.tabDefaultUserSettings = new System.Windows.Forms.TabPage();
            this.grpAdditionalOptions = new System.Windows.Forms.GroupBox();
            this.lblDefaultZoomPercentage = new System.Windows.Forms.Label();
            this.spnDefaultZoomPercentage = new System.Windows.Forms.NumericUpDown();
            this.chkUpdateZoom = new System.Windows.Forms.CheckBox();
            this.chkWarnBeforeDeactivation = new System.Windows.Forms.CheckBox();
            this.chkActivateMacPacRibbonTabOnOpen = new System.Windows.Forms.CheckBox();
            this.chkActivateMacPacRibbonTab = new System.Windows.Forms.CheckBox();
            this.chkShowXMLTags = new System.Windows.Forms.CheckBox();
            this.chkShowQuickHelp = new System.Windows.Forms.CheckBox();
            this.grpTaskPane = new System.Windows.Forms.GroupBox();
            this.chkShowTaskpaneWhenNoVariables = new System.Windows.Forms.CheckBox();
            this.chkExpandFindResults = new System.Windows.Forms.CheckBox();
            this.chkWarnOnFinish = new System.Windows.Forms.CheckBox();
            this.chkCloseTaskPaneOnFinish = new System.Windows.Forms.CheckBox();
            this.chkShowTaskPaneOnBlankNew = new System.Windows.Forms.CheckBox();
            this.lblTaskPaneBackgroundColor = new System.Windows.Forms.Label();
            this.chkDisplayTaskPaneForMPDoc = new System.Windows.Forms.CheckBox();
            this.chkDisplayTaskPaneForNonMPDoc = new System.Windows.Forms.CheckBox();
            this.chkViewPgWidth = new System.Windows.Forms.CheckBox();
            this.pnlTaskPaneColorChip = new System.Windows.Forms.Panel();
            this.chkUseGradient = new System.Windows.Forms.CheckBox();
            this.btnPickColor = new System.Windows.Forms.Button();
            this.rdGroups = new System.Windows.Forms.RadioButton();
            this.rdOffices = new System.Windows.Forms.RadioButton();
            this.lblOwnerCategory = new System.Windows.Forms.Label();
            this.cmbEntity = new System.Windows.Forms.ComboBox();
            this.lblEntity = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabFirmSettings.SuspendLayout();
            this.gbResetAll.SuspendLayout();
            this.gbResetEntity.SuspendLayout();
            this.tabFirmAppSettings.SuspendLayout();
            this.tabDisplay.SuspendLayout();
            this.grpHelpFont.SuspendLayout();
            this.grpContentManager.SuspendLayout();
            this.grpPeopleList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnSavedDataThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnSavedDataLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnUserContentThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnUserContentLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnMaxPeople)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnPeopleListWarningThreshold)).BeginInit();
            this.tabTrailers.SuspendLayout();
            this.gbFrontEndProfiling.SuspendLayout();
            this.grpTrailerDialog.SuspendLayout();
            this.gbIntegration.SuspendLayout();
            this.tabDraftStamps.SuspendLayout();
            this.tabSync.SuspendLayout();
            this.grpDistributableCopy.SuspendLayout();
            this.grpSynSchedule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udFreqInterval)).BeginInit();
            this.grpConnectionParameters.SuspendLayout();
            this.tabFeatures.SuspendLayout();
            this.grpAlerts.SuspendLayout();
            this.grpGeneralFeatures.SuspendLayout();
            this.grpAutomation.SuspendLayout();
            this.tabMailSetup.SuspendLayout();
            this.grpMailLogin.SuspendLayout();
            this.tabSecurity.SuspendLayout();
            this.tsProtectionPasswords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPasswords)).BeginInit();
            this.tabOther.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.grpCheckboxConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTypes)).BeginInit();
            this.tabDefaultUserSettings.SuspendLayout();
            this.grpAdditionalOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spnDefaultZoomPercentage)).BeginInit();
            this.grpTaskPane.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnResetAllEntitiesToFirmDefaults
            // 
            this.btnResetAllEntitiesToFirmDefaults.Location = new System.Drawing.Point(210, 18);
            this.btnResetAllEntitiesToFirmDefaults.Name = "btnResetAllEntitiesToFirmDefaults";
            this.btnResetAllEntitiesToFirmDefaults.Size = new System.Drawing.Size(148, 23);
            this.btnResetAllEntitiesToFirmDefaults.TabIndex = 1;
            this.btnResetAllEntitiesToFirmDefaults.Text = "All Settings";
            this.ttButtons.SetToolTip(this.btnResetAllEntitiesToFirmDefaults, "All Application Settings will be reset to Firm default values for all offices and" +
        " groups");
            this.btnResetAllEntitiesToFirmDefaults.UseVisualStyleBackColor = true;
            this.btnResetAllEntitiesToFirmDefaults.Click += new System.EventHandler(this.btnResetAllEntitiesToFirmDefaults_Click);
            // 
            // btnResetTabForAllEntities
            // 
            this.btnResetTabForAllEntities.Location = new System.Drawing.Point(9, 18);
            this.btnResetTabForAllEntities.Name = "btnResetTabForAllEntities";
            this.btnResetTabForAllEntities.Size = new System.Drawing.Size(148, 23);
            this.btnResetTabForAllEntities.TabIndex = 0;
            this.btnResetTabForAllEntities.Text = "Settings on this Tab Only";
            this.ttButtons.SetToolTip(this.btnResetTabForAllEntities, "Application Settings that appear on this tab will be reset to Firm defaults for a" +
        "ll offices and groups");
            this.btnResetTabForAllEntities.UseVisualStyleBackColor = true;
            this.btnResetTabForAllEntities.Click += new System.EventHandler(this.btnResetTabForAllEntities_Click);
            // 
            // btnResetToFirmDefaults
            // 
            this.btnResetToFirmDefaults.Location = new System.Drawing.Point(210, 18);
            this.btnResetToFirmDefaults.Name = "btnResetToFirmDefaults";
            this.btnResetToFirmDefaults.Size = new System.Drawing.Size(148, 23);
            this.btnResetToFirmDefaults.TabIndex = 27;
            this.btnResetToFirmDefaults.Text = "All Settings";
            this.ttButtons.SetToolTip(this.btnResetToFirmDefaults, "All Application Settings for the selected office or group will be reset to Firm d" +
        "efaults");
            this.btnResetToFirmDefaults.UseVisualStyleBackColor = true;
            this.btnResetToFirmDefaults.Click += new System.EventHandler(this.btnResetToFirmDefaults_Click);
            // 
            // btnResetTab
            // 
            this.btnResetTab.Location = new System.Drawing.Point(9, 18);
            this.btnResetTab.Name = "btnResetTab";
            this.btnResetTab.Size = new System.Drawing.Size(148, 23);
            this.btnResetTab.TabIndex = 26;
            this.btnResetTab.Text = "Settings on this Tab Only";
            this.ttButtons.SetToolTip(this.btnResetTab, "Application Settings that appear on this tab will be reset to Firm defaults for t" +
        "he selected office or group");
            this.btnResetTab.UseVisualStyleBackColor = true;
            this.btnResetTab.Click += new System.EventHandler(this.btnResetTab_Click);
            // 
            // btnResetUserForAllEntities
            // 
            this.btnResetUserForAllEntities.Location = new System.Drawing.Point(12, 440);
            this.btnResetUserForAllEntities.Name = "btnResetUserForAllEntities";
            this.btnResetUserForAllEntities.Size = new System.Drawing.Size(270, 26);
            this.btnResetUserForAllEntities.TabIndex = 237;
            this.btnResetUserForAllEntities.Text = "Reset All Offices and Groups to Firm Defaults";
            this.ttButtons.SetToolTip(this.btnResetUserForAllEntities, "User Settings will be reset to Firm defaults for all offices and groups");
            this.btnResetUserForAllEntities.UseVisualStyleBackColor = true;
            this.btnResetUserForAllEntities.Click += new System.EventHandler(this.btnResetUserForAllEntities_Click);
            // 
            // btnResetUserToFirmDefaults
            // 
            this.btnResetUserToFirmDefaults.Location = new System.Drawing.Point(12, 441);
            this.btnResetUserToFirmDefaults.Name = "btnResetUserToFirmDefaults";
            this.btnResetUserToFirmDefaults.Size = new System.Drawing.Size(270, 26);
            this.btnResetUserToFirmDefaults.TabIndex = 238;
            this.btnResetUserToFirmDefaults.Text = "Reset Settings for this Office To Firm Defaults";
            this.ttButtons.SetToolTip(this.btnResetUserToFirmDefaults, "User Settings will be reset to Firm defaults for the selected office or group");
            this.btnResetUserToFirmDefaults.UseVisualStyleBackColor = true;
            this.btnResetUserToFirmDefaults.Click += new System.EventHandler(this.btnResetUserToFirmDefaults_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabFirmSettings);
            this.tabControl1.Controls.Add(this.tabDefaultUserSettings);
            this.tabControl1.Location = new System.Drawing.Point(9, 61);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(636, 508);
            this.tabControl1.TabIndex = 7;
            // 
            // tabFirmSettings
            // 
            this.tabFirmSettings.Controls.Add(this.gbResetAll);
            this.tabFirmSettings.Controls.Add(this.gbResetEntity);
            this.tabFirmSettings.Controls.Add(this.tabFirmAppSettings);
            this.tabFirmSettings.Location = new System.Drawing.Point(4, 24);
            this.tabFirmSettings.Name = "tabFirmSettings";
            this.tabFirmSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabFirmSettings.Size = new System.Drawing.Size(628, 480);
            this.tabFirmSettings.TabIndex = 0;
            this.tabFirmSettings.Text = "Firm Settings";
            this.tabFirmSettings.UseVisualStyleBackColor = true;
            // 
            // gbResetAll
            // 
            this.gbResetAll.Controls.Add(this.btnResetAllEntitiesToFirmDefaults);
            this.gbResetAll.Controls.Add(this.btnResetTabForAllEntities);
            this.gbResetAll.Location = new System.Drawing.Point(20, 419);
            this.gbResetAll.Name = "gbResetAll";
            this.gbResetAll.Size = new System.Drawing.Size(370, 50);
            this.gbResetAll.TabIndex = 1;
            this.gbResetAll.TabStop = false;
            this.gbResetAll.Text = "Reset All Offices and Groups to Firm Defaults";
            // 
            // gbResetEntity
            // 
            this.gbResetEntity.Controls.Add(this.btnResetToFirmDefaults);
            this.gbResetEntity.Controls.Add(this.btnResetTab);
            this.gbResetEntity.Location = new System.Drawing.Point(20, 419);
            this.gbResetEntity.Name = "gbResetEntity";
            this.gbResetEntity.Size = new System.Drawing.Size(370, 50);
            this.gbResetEntity.TabIndex = 28;
            this.gbResetEntity.TabStop = false;
            this.gbResetEntity.Text = "Reset Office to Firm Defaults";
            // 
            // tabFirmAppSettings
            // 
            this.tabFirmAppSettings.Controls.Add(this.tabDisplay);
            this.tabFirmAppSettings.Controls.Add(this.tabTrailers);
            this.tabFirmAppSettings.Controls.Add(this.tabDraftStamps);
            this.tabFirmAppSettings.Controls.Add(this.tabSync);
            this.tabFirmAppSettings.Controls.Add(this.tabFeatures);
            this.tabFirmAppSettings.Controls.Add(this.tabMailSetup);
            this.tabFirmAppSettings.Controls.Add(this.tabSecurity);
            this.tabFirmAppSettings.Controls.Add(this.tabOther);
            this.tabFirmAppSettings.Location = new System.Drawing.Point(5, 6);
            this.tabFirmAppSettings.Name = "tabFirmAppSettings";
            this.tabFirmAppSettings.SelectedIndex = 0;
            this.tabFirmAppSettings.Size = new System.Drawing.Size(616, 407);
            this.tabFirmAppSettings.TabIndex = 0;
            // 
            // tabDisplay
            // 
            this.tabDisplay.Controls.Add(this.chkFinishButtonAtEnd);
            this.tabDisplay.Controls.Add(this.grpHelpFont);
            this.tabDisplay.Controls.Add(this.chkExpandChildSegments);
            this.tabDisplay.Controls.Add(this.grpContentManager);
            this.tabDisplay.Controls.Add(this.grpPeopleList);
            this.tabDisplay.Location = new System.Drawing.Point(4, 24);
            this.tabDisplay.Name = "tabDisplay";
            this.tabDisplay.Padding = new System.Windows.Forms.Padding(3);
            this.tabDisplay.Size = new System.Drawing.Size(608, 379);
            this.tabDisplay.TabIndex = 0;
            this.tabDisplay.Text = "Display";
            this.tabDisplay.UseVisualStyleBackColor = true;
            // 
            // chkFinishButtonAtEnd
            // 
            this.chkFinishButtonAtEnd.Location = new System.Drawing.Point(359, 293);
            this.chkFinishButtonAtEnd.Name = "chkFinishButtonAtEnd";
            this.chkFinishButtonAtEnd.Size = new System.Drawing.Size(231, 47);
            this.chkFinishButtonAtEnd.TabIndex = 4;
            this.chkFinishButtonAtEnd.Text = "&Move Finish Button to end of \r\nlist in editor taskpane";
            this.chkFinishButtonAtEnd.UseVisualStyleBackColor = true;
            // 
            // grpHelpFont
            // 
            this.grpHelpFont.Controls.Add(this.numUpDnHelpFontSize);
            this.grpHelpFont.Controls.Add(this.txtHelpFontName);
            this.grpHelpFont.Controls.Add(this.lblHelpFontSize);
            this.grpHelpFont.Controls.Add(this.lblHelpFontName);
            this.grpHelpFont.Location = new System.Drawing.Point(352, 134);
            this.grpHelpFont.Name = "grpHelpFont";
            this.grpHelpFont.Size = new System.Drawing.Size(250, 96);
            this.grpHelpFont.TabIndex = 2;
            this.grpHelpFont.TabStop = false;
            this.grpHelpFont.Text = "Help Font";
            // 
            // numUpDnHelpFontSize
            // 
            this.numUpDnHelpFontSize.AppendSymbol = true;
            this.numUpDnHelpFontSize.BackColor = System.Drawing.Color.Transparent;
            this.numUpDnHelpFontSize.DecimalPlaces = 0;
            this.numUpDnHelpFontSize.DisplayUnit = LMP.Data.mpMeasurementUnits.Points;
            this.numUpDnHelpFontSize.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUpDnHelpFontSize.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numUpDnHelpFontSize.IsDirty = true;
            this.numUpDnHelpFontSize.Location = new System.Drawing.Point(111, 55);
            this.numUpDnHelpFontSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUpDnHelpFontSize.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numUpDnHelpFontSize.Name = "numUpDnHelpFontSize";
            this.numUpDnHelpFontSize.Size = new System.Drawing.Size(115, 24);
            this.numUpDnHelpFontSize.SupportingValues = "";
            this.numUpDnHelpFontSize.TabIndex = 3;
            this.numUpDnHelpFontSize.Tag2 = null;
            this.numUpDnHelpFontSize.Value = "0";
            // 
            // txtHelpFontName
            // 
            this.txtHelpFontName.Location = new System.Drawing.Point(111, 23);
            this.txtHelpFontName.Name = "txtHelpFontName";
            this.txtHelpFontName.Size = new System.Drawing.Size(115, 22);
            this.txtHelpFontName.TabIndex = 1;
            // 
            // lblHelpFontSize
            // 
            this.lblHelpFontSize.AutoSize = true;
            this.lblHelpFontSize.Location = new System.Drawing.Point(11, 58);
            this.lblHelpFontSize.Name = "lblHelpFontSize";
            this.lblHelpFontSize.Size = new System.Drawing.Size(78, 15);
            this.lblHelpFontSize.TabIndex = 2;
            this.lblHelpFontSize.Text = "Help Font &Size";
            // 
            // lblHelpFontName
            // 
            this.lblHelpFontName.AutoSize = true;
            this.lblHelpFontName.Location = new System.Drawing.Point(11, 26);
            this.lblHelpFontName.Name = "lblHelpFontName";
            this.lblHelpFontName.Size = new System.Drawing.Size(85, 15);
            this.lblHelpFontName.TabIndex = 0;
            this.lblHelpFontName.Text = "Help Font &Name";
            // 
            // chkExpandChildSegments
            // 
            this.chkExpandChildSegments.Location = new System.Drawing.Point(359, 240);
            this.chkExpandChildSegments.Name = "chkExpandChildSegments";
            this.chkExpandChildSegments.Size = new System.Drawing.Size(231, 47);
            this.chkExpandChildSegments.TabIndex = 3;
            this.chkExpandChildSegments.Text = "&Expand child segment nodes in the document editor when selected";
            this.chkExpandChildSegments.UseVisualStyleBackColor = true;
            // 
            // grpContentManager
            // 
            this.grpContentManager.Controls.Add(this.label4);
            this.grpContentManager.Controls.Add(this.txtSharedFolderLabel);
            this.grpContentManager.Controls.Add(this.lblSharedFoldersLabel);
            this.grpContentManager.Controls.Add(this.btnSharedFoldersHelpText);
            this.grpContentManager.Controls.Add(this.txtSharedFoldersHelpText);
            this.grpContentManager.Controls.Add(this.lblSharedFoldersHelpText);
            this.grpContentManager.Controls.Add(this.label3);
            this.grpContentManager.Controls.Add(this.txtMyFoldersLabel);
            this.grpContentManager.Controls.Add(this.lblMyFoldersLabel);
            this.grpContentManager.Controls.Add(this.btnMyFoldersHelpText);
            this.grpContentManager.Controls.Add(this.txtMyFoldersHelpText);
            this.grpContentManager.Controls.Add(this.lblMyFoldersHelpText);
            this.grpContentManager.Controls.Add(this.lblPublicFolders);
            this.grpContentManager.Controls.Add(this.txtPublicFoldersLabel);
            this.grpContentManager.Controls.Add(this.btnPublicFoldersHelpText);
            this.grpContentManager.Controls.Add(this.lblPublicFoldersHelpText);
            this.grpContentManager.Controls.Add(this.label2);
            this.grpContentManager.Controls.Add(this.txtPublicFoldersHelpText);
            this.grpContentManager.Location = new System.Drawing.Point(11, 6);
            this.grpContentManager.Name = "grpContentManager";
            this.grpContentManager.Size = new System.Drawing.Size(330, 316);
            this.grpContentManager.TabIndex = 0;
            this.grpContentManager.TabStop = false;
            this.grpContentManager.Text = "Content Manager";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "Shared Folders";
            // 
            // txtSharedFolderLabel
            // 
            this.txtSharedFolderLabel.Location = new System.Drawing.Point(78, 234);
            this.txtSharedFolderLabel.Name = "txtSharedFolderLabel";
            this.txtSharedFolderLabel.Size = new System.Drawing.Size(240, 22);
            this.txtSharedFolderLabel.TabIndex = 14;
            // 
            // lblSharedFoldersLabel
            // 
            this.lblSharedFoldersLabel.AutoSize = true;
            this.lblSharedFoldersLabel.Location = new System.Drawing.Point(15, 237);
            this.lblSharedFoldersLabel.Name = "lblSharedFoldersLabel";
            this.lblSharedFoldersLabel.Size = new System.Drawing.Size(36, 15);
            this.lblSharedFoldersLabel.TabIndex = 13;
            this.lblSharedFoldersLabel.Text = "&Label:";
            // 
            // btnSharedFoldersHelpText
            // 
            this.btnSharedFoldersHelpText.Location = new System.Drawing.Point(293, 264);
            this.btnSharedFoldersHelpText.Name = "btnSharedFoldersHelpText";
            this.btnSharedFoldersHelpText.Size = new System.Drawing.Size(25, 23);
            this.btnSharedFoldersHelpText.TabIndex = 17;
            this.btnSharedFoldersHelpText.Text = "...";
            this.btnSharedFoldersHelpText.UseVisualStyleBackColor = true;
            this.btnSharedFoldersHelpText.Visible = false;
            this.btnSharedFoldersHelpText.Click += new System.EventHandler(this.btnSharedFoldersHelpText_Click);
            // 
            // txtSharedFoldersHelpText
            // 
            this.txtSharedFoldersHelpText.Location = new System.Drawing.Point(78, 265);
            this.txtSharedFoldersHelpText.Multiline = true;
            this.txtSharedFoldersHelpText.Name = "txtSharedFoldersHelpText";
            this.txtSharedFoldersHelpText.Size = new System.Drawing.Size(240, 39);
            this.txtSharedFoldersHelpText.TabIndex = 16;
            // 
            // lblSharedFoldersHelpText
            // 
            this.lblSharedFoldersHelpText.AutoSize = true;
            this.lblSharedFoldersHelpText.Location = new System.Drawing.Point(15, 266);
            this.lblSharedFoldersHelpText.Name = "lblSharedFoldersHelpText";
            this.lblSharedFoldersHelpText.Size = new System.Drawing.Size(57, 15);
            this.lblSharedFoldersHelpText.TabIndex = 15;
            this.lblSharedFoldersHelpText.Text = "&Help Text:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "My Folders";
            // 
            // txtMyFoldersLabel
            // 
            this.txtMyFoldersLabel.Location = new System.Drawing.Point(78, 136);
            this.txtMyFoldersLabel.Name = "txtMyFoldersLabel";
            this.txtMyFoldersLabel.Size = new System.Drawing.Size(240, 22);
            this.txtMyFoldersLabel.TabIndex = 8;
            // 
            // lblMyFoldersLabel
            // 
            this.lblMyFoldersLabel.AutoSize = true;
            this.lblMyFoldersLabel.Location = new System.Drawing.Point(15, 139);
            this.lblMyFoldersLabel.Name = "lblMyFoldersLabel";
            this.lblMyFoldersLabel.Size = new System.Drawing.Size(36, 15);
            this.lblMyFoldersLabel.TabIndex = 7;
            this.lblMyFoldersLabel.Text = "&Label:";
            // 
            // btnMyFoldersHelpText
            // 
            this.btnMyFoldersHelpText.Location = new System.Drawing.Point(293, 166);
            this.btnMyFoldersHelpText.Name = "btnMyFoldersHelpText";
            this.btnMyFoldersHelpText.Size = new System.Drawing.Size(25, 23);
            this.btnMyFoldersHelpText.TabIndex = 11;
            this.btnMyFoldersHelpText.Text = "...";
            this.btnMyFoldersHelpText.UseVisualStyleBackColor = true;
            this.btnMyFoldersHelpText.Visible = false;
            this.btnMyFoldersHelpText.Click += new System.EventHandler(this.btnMyFoldersHelpText_Click);
            // 
            // txtMyFoldersHelpText
            // 
            this.txtMyFoldersHelpText.Location = new System.Drawing.Point(78, 166);
            this.txtMyFoldersHelpText.Multiline = true;
            this.txtMyFoldersHelpText.Name = "txtMyFoldersHelpText";
            this.txtMyFoldersHelpText.Size = new System.Drawing.Size(240, 39);
            this.txtMyFoldersHelpText.TabIndex = 10;
            // 
            // lblMyFoldersHelpText
            // 
            this.lblMyFoldersHelpText.AutoSize = true;
            this.lblMyFoldersHelpText.Location = new System.Drawing.Point(15, 166);
            this.lblMyFoldersHelpText.Name = "lblMyFoldersHelpText";
            this.lblMyFoldersHelpText.Size = new System.Drawing.Size(57, 15);
            this.lblMyFoldersHelpText.TabIndex = 9;
            this.lblMyFoldersHelpText.Text = "&Help Text:";
            // 
            // lblPublicFolders
            // 
            this.lblPublicFolders.AutoSize = true;
            this.lblPublicFolders.Location = new System.Drawing.Point(15, 41);
            this.lblPublicFolders.Name = "lblPublicFolders";
            this.lblPublicFolders.Size = new System.Drawing.Size(36, 15);
            this.lblPublicFolders.TabIndex = 1;
            this.lblPublicFolders.Text = "&Label:";
            // 
            // txtPublicFoldersLabel
            // 
            this.txtPublicFoldersLabel.Location = new System.Drawing.Point(78, 38);
            this.txtPublicFoldersLabel.Name = "txtPublicFoldersLabel";
            this.txtPublicFoldersLabel.Size = new System.Drawing.Size(240, 22);
            this.txtPublicFoldersLabel.TabIndex = 2;
            // 
            // btnPublicFoldersHelpText
            // 
            this.btnPublicFoldersHelpText.Location = new System.Drawing.Point(293, 68);
            this.btnPublicFoldersHelpText.Name = "btnPublicFoldersHelpText";
            this.btnPublicFoldersHelpText.Size = new System.Drawing.Size(25, 23);
            this.btnPublicFoldersHelpText.TabIndex = 5;
            this.btnPublicFoldersHelpText.Text = "...";
            this.btnPublicFoldersHelpText.UseVisualStyleBackColor = true;
            this.btnPublicFoldersHelpText.Visible = false;
            this.btnPublicFoldersHelpText.Click += new System.EventHandler(this.btnPublicFoldersHelpText_Click);
            // 
            // lblPublicFoldersHelpText
            // 
            this.lblPublicFoldersHelpText.AutoSize = true;
            this.lblPublicFoldersHelpText.Location = new System.Drawing.Point(15, 72);
            this.lblPublicFoldersHelpText.Name = "lblPublicFoldersHelpText";
            this.lblPublicFoldersHelpText.Size = new System.Drawing.Size(57, 15);
            this.lblPublicFoldersHelpText.TabIndex = 3;
            this.lblPublicFoldersHelpText.Text = "&Help Text:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Public Folders";
            // 
            // txtPublicFoldersHelpText
            // 
            this.txtPublicFoldersHelpText.Location = new System.Drawing.Point(78, 69);
            this.txtPublicFoldersHelpText.Multiline = true;
            this.txtPublicFoldersHelpText.Name = "txtPublicFoldersHelpText";
            this.txtPublicFoldersHelpText.Size = new System.Drawing.Size(240, 39);
            this.txtPublicFoldersHelpText.TabIndex = 4;
            // 
            // grpPeopleList
            // 
            this.grpPeopleList.Controls.Add(this.numUpDnSavedDataThreshold);
            this.grpPeopleList.Controls.Add(this.numUpDnSavedDataLimit);
            this.grpPeopleList.Controls.Add(this.lblSavedDataThreshold);
            this.grpPeopleList.Controls.Add(this.numUpDnUserContentThreshold);
            this.grpPeopleList.Controls.Add(this.numUpDnUserContentLimit);
            this.grpPeopleList.Controls.Add(this.lblUserContentLimit);
            this.grpPeopleList.Controls.Add(this.lblUserContentThreshold);
            this.grpPeopleList.Controls.Add(this.numUpDnMaxPeople);
            this.grpPeopleList.Controls.Add(this.lblMaxPeople);
            this.grpPeopleList.Controls.Add(this.numUpDnPeopleListWarningThreshold);
            this.grpPeopleList.Controls.Add(this.lblPeopleListWarningThreshold);
            this.grpPeopleList.Location = new System.Drawing.Point(352, 7);
            this.grpPeopleList.Name = "grpPeopleList";
            this.grpPeopleList.Size = new System.Drawing.Size(251, 116);
            this.grpPeopleList.TabIndex = 1;
            this.grpPeopleList.TabStop = false;
            this.grpPeopleList.Text = "User Limits";
            // 
            // numUpDnSavedDataThreshold
            // 
            this.numUpDnSavedDataThreshold.Location = new System.Drawing.Point(105, 78);
            this.numUpDnSavedDataThreshold.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDnSavedDataThreshold.Name = "numUpDnSavedDataThreshold";
            this.numUpDnSavedDataThreshold.Size = new System.Drawing.Size(59, 22);
            this.numUpDnSavedDataThreshold.TabIndex = 9;
            this.numUpDnSavedDataThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUpDnSavedDataThreshold.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numUpDnSavedDataLimit
            // 
            this.numUpDnSavedDataLimit.Location = new System.Drawing.Point(170, 78);
            this.numUpDnSavedDataLimit.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDnSavedDataLimit.Name = "numUpDnSavedDataLimit";
            this.numUpDnSavedDataLimit.Size = new System.Drawing.Size(58, 22);
            this.numUpDnSavedDataLimit.TabIndex = 10;
            this.numUpDnSavedDataLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUpDnSavedDataLimit.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // lblSavedDataThreshold
            // 
            this.lblSavedDataThreshold.AutoSize = true;
            this.lblSavedDataThreshold.BackColor = System.Drawing.Color.Transparent;
            this.lblSavedDataThreshold.Location = new System.Drawing.Point(16, 81);
            this.lblSavedDataThreshold.Name = "lblSavedDataThreshold";
            this.lblSavedDataThreshold.Size = new System.Drawing.Size(64, 15);
            this.lblSavedDataThreshold.TabIndex = 8;
            this.lblSavedDataThreshold.Text = "&Saved Data";
            // 
            // numUpDnUserContentThreshold
            // 
            this.numUpDnUserContentThreshold.Location = new System.Drawing.Point(104, 53);
            this.numUpDnUserContentThreshold.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDnUserContentThreshold.Name = "numUpDnUserContentThreshold";
            this.numUpDnUserContentThreshold.Size = new System.Drawing.Size(59, 22);
            this.numUpDnUserContentThreshold.TabIndex = 6;
            this.numUpDnUserContentThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUpDnUserContentThreshold.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numUpDnUserContentLimit
            // 
            this.numUpDnUserContentLimit.Location = new System.Drawing.Point(169, 53);
            this.numUpDnUserContentLimit.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDnUserContentLimit.Name = "numUpDnUserContentLimit";
            this.numUpDnUserContentLimit.Size = new System.Drawing.Size(58, 22);
            this.numUpDnUserContentLimit.TabIndex = 7;
            this.numUpDnUserContentLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUpDnUserContentLimit.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // lblUserContentLimit
            // 
            this.lblUserContentLimit.AutoSize = true;
            this.lblUserContentLimit.Location = new System.Drawing.Point(16, 31);
            this.lblUserContentLimit.Name = "lblUserContentLimit";
            this.lblUserContentLimit.Size = new System.Drawing.Size(62, 15);
            this.lblUserContentLimit.TabIndex = 2;
            this.lblUserContentLimit.Text = "&Author List:";
            // 
            // lblUserContentThreshold
            // 
            this.lblUserContentThreshold.AutoSize = true;
            this.lblUserContentThreshold.Location = new System.Drawing.Point(15, 55);
            this.lblUserContentThreshold.Name = "lblUserContentThreshold";
            this.lblUserContentThreshold.Size = new System.Drawing.Size(75, 15);
            this.lblUserContentThreshold.TabIndex = 5;
            this.lblUserContentThreshold.Text = "&User Content:";
            // 
            // numUpDnMaxPeople
            // 
            this.numUpDnMaxPeople.Location = new System.Drawing.Point(169, 29);
            this.numUpDnMaxPeople.Name = "numUpDnMaxPeople";
            this.numUpDnMaxPeople.Size = new System.Drawing.Size(58, 22);
            this.numUpDnMaxPeople.TabIndex = 4;
            this.numUpDnMaxPeople.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUpDnMaxPeople.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numUpDnMaxPeople.ValueChanged += new System.EventHandler(this.numUpDnMaxPeople_ValueChanged);
            // 
            // lblMaxPeople
            // 
            this.lblMaxPeople.AutoSize = true;
            this.lblMaxPeople.Location = new System.Drawing.Point(167, 12);
            this.lblMaxPeople.Name = "lblMaxPeople";
            this.lblMaxPeople.Size = new System.Drawing.Size(71, 15);
            this.lblMaxPeople.TabIndex = 1;
            this.lblMaxPeople.Text = "Max Allowed:";
            // 
            // numUpDnPeopleListWarningThreshold
            // 
            this.numUpDnPeopleListWarningThreshold.Location = new System.Drawing.Point(104, 29);
            this.numUpDnPeopleListWarningThreshold.Maximum = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.numUpDnPeopleListWarningThreshold.Name = "numUpDnPeopleListWarningThreshold";
            this.numUpDnPeopleListWarningThreshold.Size = new System.Drawing.Size(59, 22);
            this.numUpDnPeopleListWarningThreshold.TabIndex = 3;
            this.numUpDnPeopleListWarningThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUpDnPeopleListWarningThreshold.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblPeopleListWarningThreshold
            // 
            this.lblPeopleListWarningThreshold.AutoSize = true;
            this.lblPeopleListWarningThreshold.Location = new System.Drawing.Point(104, 12);
            this.lblPeopleListWarningThreshold.Name = "lblPeopleListWarningThreshold";
            this.lblPeopleListWarningThreshold.Size = new System.Drawing.Size(49, 15);
            this.lblPeopleListWarningThreshold.TabIndex = 0;
            this.lblPeopleListWarningThreshold.Text = "Warn At:";
            // 
            // tabTrailers
            // 
            this.tabTrailers.Controls.Add(this.chkPromptForLegacyTrailer);
            this.tabTrailers.Controls.Add(this.chkUse9xStyleTrailer);
            this.tabTrailers.Controls.Add(this.cmbDefaultTrailerBehavior);
            this.tabTrailers.Controls.Add(this.gbFrontEndProfiling);
            this.tabTrailers.Controls.Add(this.lblDefaultTrailerBehavior);
            this.tabTrailers.Controls.Add(this.chkSkipTrailerForTempFiles);
            this.tabTrailers.Controls.Add(this.grpTrailerDialog);
            this.tabTrailers.Controls.Add(this.chkAllowUsersToSetTrailerBehavior);
            this.tabTrailers.Controls.Add(this.btnDMSMessage);
            this.tabTrailers.Controls.Add(this.gbIntegration);
            this.tabTrailers.Controls.Add(this.lblDMS);
            this.tabTrailers.Controls.Add(this.cmbDocManager);
            this.tabTrailers.Location = new System.Drawing.Point(4, 22);
            this.tabTrailers.Name = "tabTrailers";
            this.tabTrailers.Size = new System.Drawing.Size(608, 381);
            this.tabTrailers.TabIndex = 2;
            this.tabTrailers.Text = "DMS/Trailers";
            this.tabTrailers.UseVisualStyleBackColor = true;
            // 
            // chkPromptForLegacyTrailer
            // 
            this.chkPromptForLegacyTrailer.AutoSize = true;
            this.chkPromptForLegacyTrailer.Location = new System.Drawing.Point(323, 331);
            this.chkPromptForLegacyTrailer.Name = "chkPromptForLegacyTrailer";
            this.chkPromptForLegacyTrailer.Size = new System.Drawing.Size(225, 19);
            this.chkPromptForLegacyTrailer.TabIndex = 20;
            this.chkPromptForLegacyTrailer.Text = "Prompt Before Removing Legacy Trailers";
            this.chkPromptForLegacyTrailer.UseVisualStyleBackColor = true;
            // 
            // chkUse9xStyleTrailer
            // 
            this.chkUse9xStyleTrailer.AutoSize = true;
            this.chkUse9xStyleTrailer.Location = new System.Drawing.Point(323, 307);
            this.chkUse9xStyleTrailer.Name = "chkUse9xStyleTrailer";
            this.chkUse9xStyleTrailer.Size = new System.Drawing.Size(159, 19);
            this.chkUse9xStyleTrailer.TabIndex = 19;
            this.chkUse9xStyleTrailer.Text = "Use MacPac &9 Style Trailer";
            this.chkUse9xStyleTrailer.UseVisualStyleBackColor = true;
            // 
            // cmbDefaultTrailerBehavior
            // 
            this.cmbDefaultTrailerBehavior.FormattingEnabled = true;
            this.cmbDefaultTrailerBehavior.Items.AddRange(new object[] {
            "Manual Insertion",
            "Do Not Display Trailer Dialog",
            "Display Dialog"});
            this.cmbDefaultTrailerBehavior.Location = new System.Drawing.Point(148, 227);
            this.cmbDefaultTrailerBehavior.Name = "cmbDefaultTrailerBehavior";
            this.cmbDefaultTrailerBehavior.Size = new System.Drawing.Size(133, 23);
            this.cmbDefaultTrailerBehavior.TabIndex = 5;
            this.cmbDefaultTrailerBehavior.Text = "Prompt";
            // 
            // gbFrontEndProfiling
            // 
            this.gbFrontEndProfiling.Controls.Add(this.chkCloseOnCancel);
            this.gbFrontEndProfiling.Controls.Add(this.chkUseFrontEndProfiling);
            this.gbFrontEndProfiling.Location = new System.Drawing.Point(17, 248);
            this.gbFrontEndProfiling.Name = "gbFrontEndProfiling";
            this.gbFrontEndProfiling.Size = new System.Drawing.Size(265, 56);
            this.gbFrontEndProfiling.TabIndex = 6;
            this.gbFrontEndProfiling.TabStop = false;
            // 
            // chkCloseOnCancel
            // 
            this.chkCloseOnCancel.AutoSize = true;
            this.chkCloseOnCancel.Location = new System.Drawing.Point(28, 34);
            this.chkCloseOnCancel.Name = "chkCloseOnCancel";
            this.chkCloseOnCancel.Size = new System.Drawing.Size(223, 19);
            this.chkCloseOnCancel.TabIndex = 15;
            this.chkCloseOnCancel.Text = "Close Ne&w Document if Profile Cancelled";
            this.chkCloseOnCancel.UseVisualStyleBackColor = true;
            // 
            // chkUseFrontEndProfiling
            // 
            this.chkUseFrontEndProfiling.AutoSize = true;
            this.chkUseFrontEndProfiling.Location = new System.Drawing.Point(9, 12);
            this.chkUseFrontEndProfiling.Name = "chkUseFrontEndProfiling";
            this.chkUseFrontEndProfiling.Size = new System.Drawing.Size(139, 19);
            this.chkUseFrontEndProfiling.TabIndex = 14;
            this.chkUseFrontEndProfiling.Text = "&Use Front-End Profiling";
            this.chkUseFrontEndProfiling.UseVisualStyleBackColor = true;
            this.chkUseFrontEndProfiling.CheckedChanged += new System.EventHandler(this.chkUseFrontEndProfiling_CheckedChanged);
            // 
            // lblDefaultTrailerBehavior
            // 
            this.lblDefaultTrailerBehavior.AutoSize = true;
            this.lblDefaultTrailerBehavior.Location = new System.Drawing.Point(22, 230);
            this.lblDefaultTrailerBehavior.Name = "lblDefaultTrailerBehavior";
            this.lblDefaultTrailerBehavior.Size = new System.Drawing.Size(124, 15);
            this.lblDefaultTrailerBehavior.TabIndex = 4;
            this.lblDefaultTrailerBehavior.Text = "De&fault Trailer Behavior:";
            // 
            // chkSkipTrailerForTempFiles
            // 
            this.chkSkipTrailerForTempFiles.AutoSize = true;
            this.chkSkipTrailerForTempFiles.Location = new System.Drawing.Point(26, 331);
            this.chkSkipTrailerForTempFiles.Name = "chkSkipTrailerForTempFiles";
            this.chkSkipTrailerForTempFiles.Size = new System.Drawing.Size(245, 19);
            this.chkSkipTrailerForTempFiles.TabIndex = 8;
            this.chkSkipTrailerForTempFiles.Text = "&No Integration for Documents in Temp folders";
            this.chkSkipTrailerForTempFiles.UseVisualStyleBackColor = true;
            // 
            // grpTrailerDialog
            // 
            this.grpTrailerDialog.Controls.Add(this.btnTemplateDefaults);
            this.grpTrailerDialog.Controls.Add(this.cmbTrailerDef);
            this.grpTrailerDialog.Controls.Add(this.label1);
            this.grpTrailerDialog.Controls.Add(this.lblTrailers);
            this.grpTrailerDialog.Controls.Add(this.lstTrailers);
            this.grpTrailerDialog.Location = new System.Drawing.Point(312, 16);
            this.grpTrailerDialog.Name = "grpTrailerDialog";
            this.grpTrailerDialog.Size = new System.Drawing.Size(285, 286);
            this.grpTrailerDialog.TabIndex = 9;
            this.grpTrailerDialog.TabStop = false;
            this.grpTrailerDialog.Text = "Trailer Dialog";
            // 
            // btnTemplateDefaults
            // 
            this.btnTemplateDefaults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTemplateDefaults.AutoSize = true;
            this.btnTemplateDefaults.Location = new System.Drawing.Point(11, 249);
            this.btnTemplateDefaults.Name = "btnTemplateDefaults";
            this.btnTemplateDefaults.Size = new System.Drawing.Size(263, 25);
            this.btnTemplateDefaults.TabIndex = 19;
            this.btnTemplateDefaults.Text = "Template Assignments...";
            this.btnTemplateDefaults.UseVisualStyleBackColor = true;
            this.btnTemplateDefaults.Click += new System.EventHandler(this.btnTemplateDefaults_Click);
            // 
            // cmbTrailerDef
            // 
            this.cmbTrailerDef.AllowEmptyValue = true;
            this.cmbTrailerDef.Borderless = false;
            this.cmbTrailerDef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbTrailerDef.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTrailerDef.IsDirty = false;
            this.cmbTrailerDef.LimitToList = true;
            this.cmbTrailerDef.ListName = "";
            this.cmbTrailerDef.Location = new System.Drawing.Point(10, 203);
            this.cmbTrailerDef.MaxDropDownItems = 8;
            this.cmbTrailerDef.Name = "cmbTrailerDef";
            this.cmbTrailerDef.SelectedIndex = -1;
            this.cmbTrailerDef.SelectedValue = null;
            this.cmbTrailerDef.SelectionLength = 0;
            this.cmbTrailerDef.SelectionStart = 0;
            this.cmbTrailerDef.Size = new System.Drawing.Size(264, 23);
            this.cmbTrailerDef.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbTrailerDef.SupportingValues = "";
            this.cmbTrailerDef.TabIndex = 18;
            this.cmbTrailerDef.Tag2 = null;
            this.cmbTrailerDef.Value = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 17;
            this.label1.Text = "Default Trailer:";
            // 
            // lblTrailers
            // 
            this.lblTrailers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTrailers.AutoSize = true;
            this.lblTrailers.Location = new System.Drawing.Point(7, 19);
            this.lblTrailers.Name = "lblTrailers";
            this.lblTrailers.Size = new System.Drawing.Size(98, 15);
            this.lblTrailers.TabIndex = 15;
            this.lblTrailers.Text = "Default Trailer List:";
            // 
            // lstTrailers
            // 
            this.lstTrailers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTrailers.CheckOnClick = true;
            this.lstTrailers.FormattingEnabled = true;
            this.lstTrailers.Location = new System.Drawing.Point(10, 36);
            this.lstTrailers.Name = "lstTrailers";
            this.lstTrailers.Size = new System.Drawing.Size(264, 140);
            this.lstTrailers.TabIndex = 16;
            this.lstTrailers.Leave += new System.EventHandler(this.lstTrailers_Leave);
            // 
            // chkAllowUsersToSetTrailerBehavior
            // 
            this.chkAllowUsersToSetTrailerBehavior.AutoSize = true;
            this.chkAllowUsersToSetTrailerBehavior.Location = new System.Drawing.Point(26, 307);
            this.chkAllowUsersToSetTrailerBehavior.Name = "chkAllowUsersToSetTrailerBehavior";
            this.chkAllowUsersToSetTrailerBehavior.Size = new System.Drawing.Size(195, 19);
            this.chkAllowUsersToSetTrailerBehavior.TabIndex = 7;
            this.chkAllowUsersToSetTrailerBehavior.Text = "Allow Users to Set &Trailer Behavior";
            this.chkAllowUsersToSetTrailerBehavior.UseVisualStyleBackColor = true;
            // 
            // btnDMSMessage
            // 
            this.btnDMSMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDMSMessage.Location = new System.Drawing.Point(258, 33);
            this.btnDMSMessage.Name = "btnDMSMessage";
            this.btnDMSMessage.Size = new System.Drawing.Size(25, 23);
            this.btnDMSMessage.TabIndex = 2;
            this.btnDMSMessage.Text = "...";
            this.btnDMSMessage.UseVisualStyleBackColor = true;
            this.btnDMSMessage.Click += new System.EventHandler(this.btnDMSMessage_Click);
            // 
            // gbIntegration
            // 
            this.gbIntegration.Controls.Add(this.chkFileExit);
            this.gbIntegration.Controls.Add(this.chkFilePrintDefault);
            this.gbIntegration.Controls.Add(this.chkFilePrint);
            this.gbIntegration.Controls.Add(this.chkFileCloseAll);
            this.gbIntegration.Controls.Add(this.chkFileClose);
            this.gbIntegration.Controls.Add(this.chkFileSaveAll);
            this.gbIntegration.Controls.Add(this.chkFileSaveAs);
            this.gbIntegration.Controls.Add(this.chkFileSave);
            this.gbIntegration.Controls.Add(this.chkFileOpen);
            this.gbIntegration.Location = new System.Drawing.Point(16, 65);
            this.gbIntegration.Name = "gbIntegration";
            this.gbIntegration.Size = new System.Drawing.Size(267, 157);
            this.gbIntegration.TabIndex = 3;
            this.gbIntegration.TabStop = false;
            this.gbIntegration.Text = "Trailer Integration Options:";
            // 
            // chkFileExit
            // 
            this.chkFileExit.AutoSize = true;
            this.chkFileExit.Location = new System.Drawing.Point(134, 130);
            this.chkFileExit.Name = "chkFileExit";
            this.chkFileExit.Size = new System.Drawing.Size(64, 19);
            this.chkFileExit.TabIndex = 11;
            this.chkFileExit.Text = "File E&xit";
            this.chkFileExit.UseVisualStyleBackColor = true;
            this.chkFileExit.CheckedChanged += new System.EventHandler(this.chkFileExit_CheckedChanged);
            // 
            // chkFilePrintDefault
            // 
            this.chkFilePrintDefault.AutoSize = true;
            this.chkFilePrintDefault.Location = new System.Drawing.Point(134, 104);
            this.chkFilePrintDefault.Name = "chkFilePrintDefault";
            this.chkFilePrintDefault.Size = new System.Drawing.Size(105, 19);
            this.chkFilePrintDefault.TabIndex = 10;
            this.chkFilePrintDefault.Text = "File Print &Default";
            this.chkFilePrintDefault.UseVisualStyleBackColor = true;
            this.chkFilePrintDefault.CheckedChanged += new System.EventHandler(this.chkFilePrintDefault_CheckedChanged);
            // 
            // chkFilePrint
            // 
            this.chkFilePrint.AutoSize = true;
            this.chkFilePrint.Location = new System.Drawing.Point(134, 77);
            this.chkFilePrint.Name = "chkFilePrint";
            this.chkFilePrint.Size = new System.Drawing.Size(68, 19);
            this.chkFilePrint.TabIndex = 9;
            this.chkFilePrint.Text = "File &Print";
            this.chkFilePrint.UseVisualStyleBackColor = true;
            this.chkFilePrint.CheckedChanged += new System.EventHandler(this.chkFilePrint_CheckedChanged);
            // 
            // chkFileCloseAll
            // 
            this.chkFileCloseAll.AutoSize = true;
            this.chkFileCloseAll.Location = new System.Drawing.Point(134, 50);
            this.chkFileCloseAll.Name = "chkFileCloseAll";
            this.chkFileCloseAll.Size = new System.Drawing.Size(88, 19);
            this.chkFileCloseAll.TabIndex = 8;
            this.chkFileCloseAll.Text = "File Close A&ll";
            this.chkFileCloseAll.UseVisualStyleBackColor = true;
            this.chkFileCloseAll.CheckedChanged += new System.EventHandler(this.chkFileCloseAll_CheckedChanged);
            // 
            // chkFileClose
            // 
            this.chkFileClose.AutoSize = true;
            this.chkFileClose.Location = new System.Drawing.Point(134, 24);
            this.chkFileClose.Name = "chkFileClose";
            this.chkFileClose.Size = new System.Drawing.Size(74, 19);
            this.chkFileClose.TabIndex = 7;
            this.chkFileClose.Text = "File &Close";
            this.chkFileClose.UseVisualStyleBackColor = true;
            this.chkFileClose.CheckedChanged += new System.EventHandler(this.chkFileClose_CheckedChanged);
            // 
            // chkFileSaveAll
            // 
            this.chkFileSaveAll.AutoSize = true;
            this.chkFileSaveAll.Location = new System.Drawing.Point(11, 104);
            this.chkFileSaveAll.Name = "chkFileSaveAll";
            this.chkFileSaveAll.Size = new System.Drawing.Size(85, 19);
            this.chkFileSaveAll.TabIndex = 6;
            this.chkFileSaveAll.Text = "File Sa&ve All";
            this.chkFileSaveAll.UseVisualStyleBackColor = true;
            this.chkFileSaveAll.CheckedChanged += new System.EventHandler(this.chkFileSaveAll_CheckedChanged);
            // 
            // chkFileSaveAs
            // 
            this.chkFileSaveAs.AutoSize = true;
            this.chkFileSaveAs.Location = new System.Drawing.Point(11, 77);
            this.chkFileSaveAs.Name = "chkFileSaveAs";
            this.chkFileSaveAs.Size = new System.Drawing.Size(87, 19);
            this.chkFileSaveAs.TabIndex = 5;
            this.chkFileSaveAs.Text = "File Save &As";
            this.chkFileSaveAs.UseVisualStyleBackColor = true;
            this.chkFileSaveAs.CheckedChanged += new System.EventHandler(this.chkFileSaveAs_CheckedChanged);
            // 
            // chkFileSave
            // 
            this.chkFileSave.AutoSize = true;
            this.chkFileSave.Location = new System.Drawing.Point(11, 50);
            this.chkFileSave.Name = "chkFileSave";
            this.chkFileSave.Size = new System.Drawing.Size(76, 19);
            this.chkFileSave.TabIndex = 4;
            this.chkFileSave.Text = "First &Save";
            this.chkFileSave.UseVisualStyleBackColor = true;
            this.chkFileSave.CheckedChanged += new System.EventHandler(this.chkFileSave_CheckedChanged);
            // 
            // chkFileOpen
            // 
            this.chkFileOpen.AutoSize = true;
            this.chkFileOpen.Location = new System.Drawing.Point(11, 24);
            this.chkFileOpen.Name = "chkFileOpen";
            this.chkFileOpen.Size = new System.Drawing.Size(53, 19);
            this.chkFileOpen.TabIndex = 3;
            this.chkFileOpen.Text = "&Open";
            this.chkFileOpen.UseVisualStyleBackColor = true;
            this.chkFileOpen.CheckedChanged += new System.EventHandler(this.chkFileOpen_CheckedChanged);
            // 
            // lblDMS
            // 
            this.lblDMS.AutoSize = true;
            this.lblDMS.Location = new System.Drawing.Point(13, 16);
            this.lblDMS.Name = "lblDMS";
            this.lblDMS.Size = new System.Drawing.Size(138, 15);
            this.lblDMS.TabIndex = 0;
            this.lblDMS.Text = "Select Document &Manager:";
            // 
            // cmbDocManager
            // 
            this.cmbDocManager.AllowEmptyValue = false;
            this.cmbDocManager.Borderless = false;
            this.cmbDocManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbDocManager.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDocManager.IsDirty = false;
            this.cmbDocManager.LimitToList = true;
            this.cmbDocManager.ListName = "";
            this.cmbDocManager.Location = new System.Drawing.Point(17, 33);
            this.cmbDocManager.MaxDropDownItems = 8;
            this.cmbDocManager.Name = "cmbDocManager";
            this.cmbDocManager.SelectedIndex = -1;
            this.cmbDocManager.SelectedValue = null;
            this.cmbDocManager.SelectionLength = 0;
            this.cmbDocManager.SelectionStart = 0;
            this.cmbDocManager.Size = new System.Drawing.Size(240, 23);
            this.cmbDocManager.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbDocManager.SupportingValues = "";
            this.cmbDocManager.TabIndex = 1;
            this.cmbDocManager.Tag2 = null;
            this.cmbDocManager.Value = "";
            this.cmbDocManager.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbDocManager_ValueChanged);
            // 
            // tabDraftStamps
            // 
            this.tabDraftStamps.Controls.Add(this.chkSetRemoveAllStampsAsFirstInList);
            this.tabDraftStamps.Controls.Add(this.lblDraftStamps);
            this.tabDraftStamps.Controls.Add(this.lstDraftStamps);
            this.tabDraftStamps.Location = new System.Drawing.Point(4, 22);
            this.tabDraftStamps.Name = "tabDraftStamps";
            this.tabDraftStamps.Padding = new System.Windows.Forms.Padding(3);
            this.tabDraftStamps.Size = new System.Drawing.Size(608, 381);
            this.tabDraftStamps.TabIndex = 3;
            this.tabDraftStamps.Text = "Draft Stamps";
            this.tabDraftStamps.UseVisualStyleBackColor = true;
            // 
            // chkSetRemoveAllStampsAsFirstInList
            // 
            this.chkSetRemoveAllStampsAsFirstInList.AutoSize = true;
            this.chkSetRemoveAllStampsAsFirstInList.Location = new System.Drawing.Point(312, 35);
            this.chkSetRemoveAllStampsAsFirstInList.Name = "chkSetRemoveAllStampsAsFirstInList";
            this.chkSetRemoveAllStampsAsFirstInList.Size = new System.Drawing.Size(166, 19);
            this.chkSetRemoveAllStampsAsFirstInList.TabIndex = 3;
            this.chkSetRemoveAllStampsAsFirstInList.Text = "&Set \'Remove All\' as first in list";
            this.chkSetRemoveAllStampsAsFirstInList.UseVisualStyleBackColor = true;
            // 
            // lblDraftStamps
            // 
            this.lblDraftStamps.AutoSize = true;
            this.lblDraftStamps.Location = new System.Drawing.Point(13, 12);
            this.lblDraftStamps.Name = "lblDraftStamps";
            this.lblDraftStamps.Size = new System.Drawing.Size(110, 15);
            this.lblDraftStamps.TabIndex = 3;
            this.lblDraftStamps.Text = "Default Draft Stamps:";
            // 
            // lstDraftStamps
            // 
            this.lstDraftStamps.CheckOnClick = true;
            this.lstDraftStamps.FormattingEnabled = true;
            this.lstDraftStamps.Location = new System.Drawing.Point(16, 30);
            this.lstDraftStamps.Name = "lstDraftStamps";
            this.lstDraftStamps.Size = new System.Drawing.Size(267, 225);
            this.lstDraftStamps.TabIndex = 2;
            // 
            // tabSync
            // 
            this.tabSync.Controls.Add(this.grpDistributableCopy);
            this.tabSync.Controls.Add(this.grpSynSchedule);
            this.tabSync.Controls.Add(this.grpConnectionParameters);
            this.tabSync.Location = new System.Drawing.Point(4, 22);
            this.tabSync.Name = "tabSync";
            this.tabSync.Padding = new System.Windows.Forms.Padding(3);
            this.tabSync.Size = new System.Drawing.Size(608, 381);
            this.tabSync.TabIndex = 1;
            this.tabSync.Text = "Network Database";
            this.tabSync.UseVisualStyleBackColor = true;
            // 
            // grpDistributableCopy
            // 
            this.grpDistributableCopy.Controls.Add(this.btnDistributableFolder);
            this.grpDistributableCopy.Controls.Add(this.txtDistributableFolder);
            this.grpDistributableCopy.Controls.Add(this.lblDistributableFolder);
            this.grpDistributableCopy.Location = new System.Drawing.Point(20, 290);
            this.grpDistributableCopy.Margin = new System.Windows.Forms.Padding(2);
            this.grpDistributableCopy.Name = "grpDistributableCopy";
            this.grpDistributableCopy.Size = new System.Drawing.Size(576, 67);
            this.grpDistributableCopy.TabIndex = 10;
            this.grpDistributableCopy.TabStop = false;
            this.grpDistributableCopy.Text = "Distributable Database";
            // 
            // btnDistributableFolder
            // 
            this.btnDistributableFolder.Location = new System.Drawing.Point(477, 35);
            this.btnDistributableFolder.Name = "btnDistributableFolder";
            this.btnDistributableFolder.Size = new System.Drawing.Size(75, 23);
            this.btnDistributableFolder.TabIndex = 2;
            this.btnDistributableFolder.Text = "&Browse...";
            this.btnDistributableFolder.UseVisualStyleBackColor = true;
            this.btnDistributableFolder.Click += new System.EventHandler(this.btnDistributableFolder_Click);
            // 
            // txtDistributableFolder
            // 
            this.txtDistributableFolder.Location = new System.Drawing.Point(21, 35);
            this.txtDistributableFolder.Name = "txtDistributableFolder";
            this.txtDistributableFolder.Size = new System.Drawing.Size(444, 22);
            this.txtDistributableFolder.TabIndex = 1;
            // 
            // lblDistributableFolder
            // 
            this.lblDistributableFolder.AutoSize = true;
            this.lblDistributableFolder.Location = new System.Drawing.Point(18, 17);
            this.lblDistributableFolder.Name = "lblDistributableFolder";
            this.lblDistributableFolder.Size = new System.Drawing.Size(261, 15);
            this.lblDistributableFolder.TabIndex = 0;
            this.lblDistributableFolder.Text = "&Location of Network Copy of Distributable Database:";
            // 
            // grpSynSchedule
            // 
            this.grpSynSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSynSchedule.Controls.Add(this.chkLogUserSync);
            this.grpSynSchedule.Controls.Add(this.cmbDay);
            this.grpSynSchedule.Controls.Add(this.lblDay);
            this.grpSynSchedule.Controls.Add(this.cmbSyncCompletePrompt);
            this.grpSynSchedule.Controls.Add(this.lblSyncCompletePrompt);
            this.grpSynSchedule.Controls.Add(this.cmbSyncPrompt);
            this.grpSynSchedule.Controls.Add(this.lblSyncPrompt);
            this.grpSynSchedule.Controls.Add(this.udFreqInterval);
            this.grpSynSchedule.Controls.Add(this.lblCurSynScheduleDesc);
            this.grpSynSchedule.Controls.Add(this.dtTime);
            this.grpSynSchedule.Controls.Add(this.lblTime);
            this.grpSynSchedule.Controls.Add(this.cmbFreqUnit);
            this.grpSynSchedule.Controls.Add(this.lblFreqInterval);
            this.grpSynSchedule.Controls.Add(this.lblFreqUnit);
            this.grpSynSchedule.Location = new System.Drawing.Point(20, 136);
            this.grpSynSchedule.Margin = new System.Windows.Forms.Padding(2);
            this.grpSynSchedule.Name = "grpSynSchedule";
            this.grpSynSchedule.Size = new System.Drawing.Size(577, 152);
            this.grpSynSchedule.TabIndex = 9;
            this.grpSynSchedule.TabStop = false;
            this.grpSynSchedule.Text = "Synchronization Schedule";
            // 
            // chkLogUserSync
            // 
            this.chkLogUserSync.AutoSize = true;
            this.chkLogUserSync.Location = new System.Drawing.Point(21, 127);
            this.chkLogUserSync.Name = "chkLogUserSync";
            this.chkLogUserSync.Size = new System.Drawing.Size(291, 19);
            this.chkLogUserSync.TabIndex = 13;
            this.chkLogUserSync.Text = "Log successful User Syncs in IIS Server Event &Viewer";
            this.chkLogUserSync.UseVisualStyleBackColor = true;
            // 
            // cmbDay
            // 
            this.cmbDay.AutoCompleteCustomSource.AddRange(new string[] {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"});
            this.cmbDay.FormattingEnabled = true;
            this.cmbDay.Items.AddRange(new object[] {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"});
            this.cmbDay.Location = new System.Drawing.Point(364, 20);
            this.cmbDay.Name = "cmbDay";
            this.cmbDay.Size = new System.Drawing.Size(69, 23);
            this.cmbDay.TabIndex = 5;
            this.cmbDay.SelectedIndexChanged += new System.EventHandler(this.cmbDay_SelectedIndexChanged);
            // 
            // lblDay
            // 
            this.lblDay.AutoSize = true;
            this.lblDay.Location = new System.Drawing.Point(337, 25);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(30, 15);
            this.lblDay.TabIndex = 4;
            this.lblDay.Text = "&Day:";
            // 
            // cmbSyncCompletePrompt
            // 
            this.cmbSyncCompletePrompt.AllowEmptyValue = false;
            this.cmbSyncCompletePrompt.Borderless = false;
            this.cmbSyncCompletePrompt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbSyncCompletePrompt.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSyncCompletePrompt.IsDirty = false;
            this.cmbSyncCompletePrompt.LimitToList = true;
            this.cmbSyncCompletePrompt.ListName = "";
            this.cmbSyncCompletePrompt.Location = new System.Drawing.Point(277, 96);
            this.cmbSyncCompletePrompt.MaxDropDownItems = 8;
            this.cmbSyncCompletePrompt.Name = "cmbSyncCompletePrompt";
            this.cmbSyncCompletePrompt.SelectedIndex = -1;
            this.cmbSyncCompletePrompt.SelectedValue = null;
            this.cmbSyncCompletePrompt.SelectionLength = 0;
            this.cmbSyncCompletePrompt.SelectionStart = 0;
            this.cmbSyncCompletePrompt.Size = new System.Drawing.Size(287, 23);
            this.cmbSyncCompletePrompt.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbSyncCompletePrompt.SupportingValues = "";
            this.cmbSyncCompletePrompt.TabIndex = 12;
            this.cmbSyncCompletePrompt.Tag2 = null;
            this.cmbSyncCompletePrompt.Value = "";
            // 
            // lblSyncCompletePrompt
            // 
            this.lblSyncCompletePrompt.AutoSize = true;
            this.lblSyncCompletePrompt.Location = new System.Drawing.Point(18, 100);
            this.lblSyncCompletePrompt.Name = "lblSyncCompletePrompt";
            this.lblSyncCompletePrompt.Size = new System.Drawing.Size(254, 15);
            this.lblSyncCompletePrompt.TabIndex = 11;
            this.lblSyncCompletePrompt.Text = "If Word is active when Scheduled Synch &completes";
            this.lblSyncCompletePrompt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cmbSyncPrompt
            // 
            this.cmbSyncPrompt.AllowEmptyValue = false;
            this.cmbSyncPrompt.Borderless = false;
            this.cmbSyncPrompt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbSyncPrompt.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSyncPrompt.IsDirty = false;
            this.cmbSyncPrompt.LimitToList = true;
            this.cmbSyncPrompt.ListName = "";
            this.cmbSyncPrompt.Location = new System.Drawing.Point(234, 67);
            this.cmbSyncPrompt.MaxDropDownItems = 8;
            this.cmbSyncPrompt.Name = "cmbSyncPrompt";
            this.cmbSyncPrompt.SelectedIndex = -1;
            this.cmbSyncPrompt.SelectedValue = null;
            this.cmbSyncPrompt.SelectionLength = 0;
            this.cmbSyncPrompt.SelectionStart = 0;
            this.cmbSyncPrompt.Size = new System.Drawing.Size(330, 23);
            this.cmbSyncPrompt.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbSyncPrompt.SupportingValues = "";
            this.cmbSyncPrompt.TabIndex = 10;
            this.cmbSyncPrompt.Tag2 = null;
            this.cmbSyncPrompt.Value = "";
            // 
            // lblSyncPrompt
            // 
            this.lblSyncPrompt.AutoSize = true;
            this.lblSyncPrompt.Location = new System.Drawing.Point(18, 71);
            this.lblSyncPrompt.Name = "lblSyncPrompt";
            this.lblSyncPrompt.Size = new System.Drawing.Size(212, 15);
            this.lblSyncPrompt.TabIndex = 9;
            this.lblSyncPrompt.Text = "If &Word is active when Synch is scheduled";
            this.lblSyncPrompt.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // udFreqInterval
            // 
            this.udFreqInterval.Location = new System.Drawing.Point(279, 20);
            this.udFreqInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udFreqInterval.Name = "udFreqInterval";
            this.udFreqInterval.Size = new System.Drawing.Size(52, 22);
            this.udFreqInterval.TabIndex = 3;
            this.udFreqInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udFreqInterval.ValueChanged += new System.EventHandler(this.udFreqInterval_ValueChanged);
            // 
            // lblCurSynScheduleDesc
            // 
            this.lblCurSynScheduleDesc.AutoSize = true;
            this.lblCurSynScheduleDesc.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblCurSynScheduleDesc.Location = new System.Drawing.Point(18, 47);
            this.lblCurSynScheduleDesc.Name = "lblCurSynScheduleDesc";
            this.lblCurSynScheduleDesc.Size = new System.Drawing.Size(31, 15);
            this.lblCurSynScheduleDesc.TabIndex = 8;
            this.lblCurSynScheduleDesc.Text = "####";
            // 
            // dtTime
            // 
            this.dtTime.CustomFormat = "hh:mm:ss am/pm";
            this.dtTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtTime.Location = new System.Drawing.Point(469, 20);
            this.dtTime.Name = "dtTime";
            this.dtTime.ShowUpDown = true;
            this.dtTime.Size = new System.Drawing.Size(95, 22);
            this.dtTime.TabIndex = 7;
            this.dtTime.ValueChanged += new System.EventHandler(this.dtTime_ValueChanged);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(439, 25);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(33, 15);
            this.lblTime.TabIndex = 6;
            this.lblTime.Text = "&Time:";
            // 
            // cmbFreqUnit
            // 
            this.cmbFreqUnit.FormattingEnabled = true;
            this.cmbFreqUnit.Items.AddRange(new object[] {
            "Minute",
            "Hour",
            "Day",
            "Week"});
            this.cmbFreqUnit.Location = new System.Drawing.Point(100, 20);
            this.cmbFreqUnit.Name = "cmbFreqUnit";
            this.cmbFreqUnit.Size = new System.Drawing.Size(69, 23);
            this.cmbFreqUnit.TabIndex = 1;
            this.cmbFreqUnit.SelectedIndexChanged += new System.EventHandler(this.cmbFreqUnit_SelectedIndexChanged);
            // 
            // lblFreqInterval
            // 
            this.lblFreqInterval.AutoSize = true;
            this.lblFreqInterval.Location = new System.Drawing.Point(179, 24);
            this.lblFreqInterval.Name = "lblFreqInterval";
            this.lblFreqInterval.Size = new System.Drawing.Size(102, 15);
            this.lblFreqInterval.TabIndex = 2;
            this.lblFreqInterval.Text = "&Frequency Interval:";
            // 
            // lblFreqUnit
            // 
            this.lblFreqUnit.AutoSize = true;
            this.lblFreqUnit.Location = new System.Drawing.Point(18, 25);
            this.lblFreqUnit.Name = "lblFreqUnit";
            this.lblFreqUnit.Size = new System.Drawing.Size(85, 15);
            this.lblFreqUnit.TabIndex = 0;
            this.lblFreqUnit.Text = "Frequency &Unit:";
            // 
            // grpConnectionParameters
            // 
            this.grpConnectionParameters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpConnectionParameters.Controls.Add(this.txtServerAppName);
            this.grpConnectionParameters.Controls.Add(this.lblServerAppName);
            this.grpConnectionParameters.Controls.Add(this.txtIISServerIP);
            this.grpConnectionParameters.Controls.Add(this.lblIISServerIP);
            this.grpConnectionParameters.Controls.Add(this.txtDatabase);
            this.grpConnectionParameters.Controls.Add(this.lblDatabase);
            this.grpConnectionParameters.Controls.Add(this.txtServer);
            this.grpConnectionParameters.Controls.Add(this.lblServer);
            this.grpConnectionParameters.Location = new System.Drawing.Point(20, 5);
            this.grpConnectionParameters.Margin = new System.Windows.Forms.Padding(2);
            this.grpConnectionParameters.Name = "grpConnectionParameters";
            this.grpConnectionParameters.Size = new System.Drawing.Size(577, 129);
            this.grpConnectionParameters.TabIndex = 8;
            this.grpConnectionParameters.TabStop = false;
            this.grpConnectionParameters.Text = "Connection Parameters";
            // 
            // txtServerAppName
            // 
            this.txtServerAppName.Location = new System.Drawing.Point(170, 101);
            this.txtServerAppName.Name = "txtServerAppName";
            this.txtServerAppName.Size = new System.Drawing.Size(224, 22);
            this.txtServerAppName.TabIndex = 7;
            // 
            // lblServerAppName
            // 
            this.lblServerAppName.AutoSize = true;
            this.lblServerAppName.Location = new System.Drawing.Point(18, 104);
            this.lblServerAppName.Name = "lblServerAppName";
            this.lblServerAppName.Size = new System.Drawing.Size(145, 15);
            this.lblServerAppName.TabIndex = 6;
            this.lblServerAppName.Text = "IIS Server &Application Name:";
            // 
            // txtIISServerIP
            // 
            this.txtIISServerIP.Location = new System.Drawing.Point(170, 73);
            this.txtIISServerIP.Name = "txtIISServerIP";
            this.txtIISServerIP.Size = new System.Drawing.Size(224, 22);
            this.txtIISServerIP.TabIndex = 5;
            // 
            // lblIISServerIP
            // 
            this.lblIISServerIP.AutoSize = true;
            this.lblIISServerIP.Location = new System.Drawing.Point(18, 77);
            this.lblIISServerIP.Name = "lblIISServerIP";
            this.lblIISServerIP.Size = new System.Drawing.Size(59, 15);
            this.lblIISServerIP.TabIndex = 4;
            this.lblIISServerIP.Text = "&IIS Server:";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(170, 45);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(224, 22);
            this.txtDatabase.TabIndex = 3;
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Location = new System.Drawing.Point(18, 49);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(88, 15);
            this.lblDatabase.TabIndex = 2;
            this.lblDatabase.Text = "&Database Name:";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(170, 17);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(224, 22);
            this.txtServer.TabIndex = 1;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(18, 19);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(93, 15);
            this.lblServer.TabIndex = 0;
            this.lblServer.Text = "Database &Server:";
            // 
            // tabFeatures
            // 
            this.tabFeatures.Controls.Add(this.grpAlerts);
            this.tabFeatures.Controls.Add(this.grpGeneralFeatures);
            this.tabFeatures.Controls.Add(this.grpAutomation);
            this.tabFeatures.Location = new System.Drawing.Point(4, 22);
            this.tabFeatures.Name = "tabFeatures";
            this.tabFeatures.Size = new System.Drawing.Size(608, 381);
            this.tabFeatures.TabIndex = 5;
            this.tabFeatures.Text = "Features/Alerts";
            this.tabFeatures.UseVisualStyleBackColor = true;
            // 
            // grpAlerts
            // 
            this.grpAlerts.Controls.Add(this.chkPromptAboutUnconvertedTablesInDesign);
            this.grpAlerts.Controls.Add(this.chkAlertWhenNonMP10DocumentOpened);
            this.grpAlerts.Controls.Add(this.chkAlertTagsRemoved);
            this.grpAlerts.Location = new System.Drawing.Point(16, 136);
            this.grpAlerts.Name = "grpAlerts";
            this.grpAlerts.Size = new System.Drawing.Size(574, 80);
            this.grpAlerts.TabIndex = 1;
            this.grpAlerts.TabStop = false;
            this.grpAlerts.Text = "Alerts";
            // 
            // chkPromptAboutUnconvertedTablesInDesign
            // 
            this.chkPromptAboutUnconvertedTablesInDesign.AutoSize = true;
            this.chkPromptAboutUnconvertedTablesInDesign.Location = new System.Drawing.Point(17, 54);
            this.chkPromptAboutUnconvertedTablesInDesign.Name = "chkPromptAboutUnconvertedTablesInDesign";
            this.chkPromptAboutUnconvertedTablesInDesign.Size = new System.Drawing.Size(241, 19);
            this.chkPromptAboutUnconvertedTablesInDesign.TabIndex = 1;
            this.chkPromptAboutUnconvertedTablesInDesign.Text = "Prompt About Unconverted &Tables in Design";
            this.chkPromptAboutUnconvertedTablesInDesign.UseVisualStyleBackColor = true;
            // 
            // chkAlertWhenNonMP10DocumentOpened
            // 
            this.chkAlertWhenNonMP10DocumentOpened.AutoSize = true;
            this.chkAlertWhenNonMP10DocumentOpened.Location = new System.Drawing.Point(305, 26);
            this.chkAlertWhenNonMP10DocumentOpened.Name = "chkAlertWhenNonMP10DocumentOpened";
            this.chkAlertWhenNonMP10DocumentOpened.Size = new System.Drawing.Size(232, 19);
            this.chkAlertWhenNonMP10DocumentOpened.TabIndex = 2;
            this.chkAlertWhenNonMP10DocumentOpened.Text = "Alert &When Opening non-Forte Documents";
            this.chkAlertWhenNonMP10DocumentOpened.UseVisualStyleBackColor = true;
            // 
            // chkAlertTagsRemoved
            // 
            this.chkAlertTagsRemoved.AutoSize = true;
            this.chkAlertTagsRemoved.Location = new System.Drawing.Point(17, 26);
            this.chkAlertTagsRemoved.Name = "chkAlertTagsRemoved";
            this.chkAlertTagsRemoved.Size = new System.Drawing.Size(262, 19);
            this.chkAlertTagsRemoved.TabIndex = 0;
            this.chkAlertTagsRemoved.Text = "Alert When &Unusable Tags Have Been Removed";
            this.chkAlertTagsRemoved.UseVisualStyleBackColor = true;
            // 
            // grpGeneralFeatures
            // 
            this.grpGeneralFeatures.Controls.Add(this.chkClearUndoStack);
            this.grpGeneralFeatures.Controls.Add(this.chkShowSegmentSpecificRibbons);
            this.grpGeneralFeatures.Controls.Add(this.chkAllowSavedDataCreationOnDeautomation);
            this.grpGeneralFeatures.Controls.Add(this.chkAllowPrivatePeople);
            this.grpGeneralFeatures.Controls.Add(this.chkAllowProxying);
            this.grpGeneralFeatures.Controls.Add(this.chkAllowNormalStyleUpdates);
            this.grpGeneralFeatures.Controls.Add(this.chkAllowLegacyDocs);
            this.grpGeneralFeatures.Controls.Add(this.chkAllowSharedSegments);
            this.grpGeneralFeatures.Location = new System.Drawing.Point(16, 7);
            this.grpGeneralFeatures.Name = "grpGeneralFeatures";
            this.grpGeneralFeatures.Size = new System.Drawing.Size(573, 127);
            this.grpGeneralFeatures.TabIndex = 0;
            this.grpGeneralFeatures.TabStop = false;
            this.grpGeneralFeatures.Text = "General";
            // 
            // chkClearUndoStack
            // 
            this.chkClearUndoStack.AutoSize = true;
            this.chkClearUndoStack.Location = new System.Drawing.Point(305, 73);
            this.chkClearUndoStack.Name = "chkClearUndoStack";
            this.chkClearUndoStack.Size = new System.Drawing.Size(218, 19);
            this.chkClearUndoStack.TabIndex = 6;
            this.chkClearUndoStack.Text = "Clear Undo Stac&k After Forte Functions";
            this.chkClearUndoStack.UseVisualStyleBackColor = true;
            // 
            // chkShowSegmentSpecificRibbons
            // 
            this.chkShowSegmentSpecificRibbons.Location = new System.Drawing.Point(305, 95);
            this.chkShowSegmentSpecificRibbons.Name = "chkShowSegmentSpecificRibbons";
            this.chkShowSegmentSpecificRibbons.Size = new System.Drawing.Size(231, 25);
            this.chkShowSegmentSpecificRibbons.TabIndex = 7;
            this.chkShowSegmentSpecificRibbons.Text = "S&how Segment Specific Ribbons";
            this.chkShowSegmentSpecificRibbons.UseVisualStyleBackColor = true;
            // 
            // chkAllowSavedDataCreationOnDeautomation
            // 
            this.chkAllowSavedDataCreationOnDeautomation.AutoSize = true;
            this.chkAllowSavedDataCreationOnDeautomation.Location = new System.Drawing.Point(305, 48);
            this.chkAllowSavedDataCreationOnDeautomation.Name = "chkAllowSavedDataCreationOnDeautomation";
            this.chkAllowSavedDataCreationOnDeautomation.Size = new System.Drawing.Size(239, 19);
            this.chkAllowSavedDataCreationOnDeautomation.TabIndex = 5;
            this.chkAllowSavedDataCreationOnDeautomation.Text = "Allow Saved Data &Creation on Deautomation";
            this.chkAllowSavedDataCreationOnDeautomation.UseVisualStyleBackColor = true;
            // 
            // chkAllowPrivatePeople
            // 
            this.chkAllowPrivatePeople.AutoSize = true;
            this.chkAllowPrivatePeople.Location = new System.Drawing.Point(17, 48);
            this.chkAllowPrivatePeople.Name = "chkAllowPrivatePeople";
            this.chkAllowPrivatePeople.Size = new System.Drawing.Size(124, 19);
            this.chkAllowPrivatePeople.TabIndex = 1;
            this.chkAllowPrivatePeople.Text = "Allow Pri&vate People";
            this.chkAllowPrivatePeople.UseVisualStyleBackColor = true;
            // 
            // chkAllowProxying
            // 
            this.chkAllowProxying.AutoSize = true;
            this.chkAllowProxying.Location = new System.Drawing.Point(17, 73);
            this.chkAllowProxying.Name = "chkAllowProxying";
            this.chkAllowProxying.Size = new System.Drawing.Size(97, 19);
            this.chkAllowProxying.TabIndex = 2;
            this.chkAllowProxying.Text = "Allow Pro&xying";
            this.chkAllowProxying.UseVisualStyleBackColor = true;
            // 
            // chkAllowNormalStyleUpdates
            // 
            this.chkAllowNormalStyleUpdates.AutoSize = true;
            this.chkAllowNormalStyleUpdates.Location = new System.Drawing.Point(305, 23);
            this.chkAllowNormalStyleUpdates.Name = "chkAllowNormalStyleUpdates";
            this.chkAllowNormalStyleUpdates.Size = new System.Drawing.Size(206, 19);
            this.chkAllowNormalStyleUpdates.TabIndex = 4;
            this.chkAllowNormalStyleUpdates.Text = "Allow &Normal Template Style Updates";
            this.chkAllowNormalStyleUpdates.UseVisualStyleBackColor = true;
            // 
            // chkAllowLegacyDocs
            // 
            this.chkAllowLegacyDocs.AutoSize = true;
            this.chkAllowLegacyDocs.Location = new System.Drawing.Point(17, 98);
            this.chkAllowLegacyDocs.Name = "chkAllowLegacyDocs";
            this.chkAllowLegacyDocs.Size = new System.Drawing.Size(206, 19);
            this.chkAllowLegacyDocs.TabIndex = 3;
            this.chkAllowLegacyDocs.Text = "Allow &Legacy Document Functionality";
            this.chkAllowLegacyDocs.UseVisualStyleBackColor = true;
            // 
            // chkAllowSharedSegments
            // 
            this.chkAllowSharedSegments.AutoSize = true;
            this.chkAllowSharedSegments.Location = new System.Drawing.Point(17, 23);
            this.chkAllowSharedSegments.Name = "chkAllowSharedSegments";
            this.chkAllowSharedSegments.Size = new System.Drawing.Size(140, 19);
            this.chkAllowSharedSegments.TabIndex = 0;
            this.chkAllowSharedSegments.Text = "Allow S&hared Segments";
            this.chkAllowSharedSegments.UseVisualStyleBackColor = true;
            // 
            // grpAutomation
            // 
            this.grpAutomation.Controls.Add(this.chkFinishOnOpen);
            this.grpAutomation.Controls.Add(this.chkRemoveTextSegmentTags);
            this.grpAutomation.Controls.Add(this.chkAllowDocumentMemberTagRemoval);
            this.grpAutomation.Controls.Add(this.chkAllowDocumentTagRemoval);
            this.grpAutomation.Controls.Add(this.chkAllowSegmentMemberTagRemoval);
            this.grpAutomation.Controls.Add(this.chkAllowVarBlockTagRemoval);
            this.grpAutomation.Location = new System.Drawing.Point(16, 220);
            this.grpAutomation.Name = "grpAutomation";
            this.grpAutomation.Size = new System.Drawing.Size(573, 130);
            this.grpAutomation.TabIndex = 2;
            this.grpAutomation.TabStop = false;
            this.grpAutomation.Text = "Automation";
            // 
            // chkFinishOnOpen
            // 
            this.chkFinishOnOpen.AutoSize = true;
            this.chkFinishOnOpen.Location = new System.Drawing.Point(327, 51);
            this.chkFinishOnOpen.Name = "chkFinishOnOpen";
            this.chkFinishOnOpen.Size = new System.Drawing.Size(172, 19);
            this.chkFinishOnOpen.TabIndex = 12;
            this.chkFinishOnOpen.Text = "Finish Documents Upon &Open";
            this.chkFinishOnOpen.UseVisualStyleBackColor = true;
            // 
            // chkRemoveTextSegmentTags
            // 
            this.chkRemoveTextSegmentTags.AutoSize = true;
            this.chkRemoveTextSegmentTags.Location = new System.Drawing.Point(327, 24);
            this.chkRemoveTextSegmentTags.Name = "chkRemoveTextSegmentTags";
            this.chkRemoveTextSegmentTags.Size = new System.Drawing.Size(214, 19);
            this.chkRemoveTextSegmentTags.TabIndex = 11;
            this.chkRemoveTextSegmentTags.Text = "Remove Text Segment Tags On &Finish";
            this.chkRemoveTextSegmentTags.UseVisualStyleBackColor = true;
            // 
            // chkAllowDocumentMemberTagRemoval
            // 
            this.chkAllowDocumentMemberTagRemoval.AutoSize = true;
            this.chkAllowDocumentMemberTagRemoval.Location = new System.Drawing.Point(17, 51);
            this.chkAllowDocumentMemberTagRemoval.Name = "chkAllowDocumentMemberTagRemoval";
            this.chkAllowDocumentMemberTagRemoval.Size = new System.Drawing.Size(292, 19);
            this.chkAllowDocumentMemberTagRemoval.TabIndex = 8;
            this.chkAllowDocumentMemberTagRemoval.Text = "Allow Removal of Variable and Block Tags in &Document";
            this.chkAllowDocumentMemberTagRemoval.UseVisualStyleBackColor = true;
            // 
            // chkAllowDocumentTagRemoval
            // 
            this.chkAllowDocumentTagRemoval.AutoSize = true;
            this.chkAllowDocumentTagRemoval.Location = new System.Drawing.Point(17, 24);
            this.chkAllowDocumentTagRemoval.Name = "chkAllowDocumentTagRemoval";
            this.chkAllowDocumentTagRemoval.Size = new System.Drawing.Size(276, 19);
            this.chkAllowDocumentTagRemoval.TabIndex = 7;
            this.chkAllowDocumentTagRemoval.Text = "Allow Removal of Schema and &All Tags in Document";
            this.chkAllowDocumentTagRemoval.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAllowDocumentTagRemoval.UseVisualStyleBackColor = true;
            // 
            // chkAllowSegmentMemberTagRemoval
            // 
            this.chkAllowSegmentMemberTagRemoval.AutoSize = true;
            this.chkAllowSegmentMemberTagRemoval.Location = new System.Drawing.Point(17, 78);
            this.chkAllowSegmentMemberTagRemoval.Name = "chkAllowSegmentMemberTagRemoval";
            this.chkAllowSegmentMemberTagRemoval.Size = new System.Drawing.Size(286, 19);
            this.chkAllowSegmentMemberTagRemoval.TabIndex = 9;
            this.chkAllowSegmentMemberTagRemoval.Text = "Allow Removal of Variable And Block Tags in &Segment";
            this.chkAllowSegmentMemberTagRemoval.UseVisualStyleBackColor = true;
            // 
            // chkAllowVarBlockTagRemoval
            // 
            this.chkAllowVarBlockTagRemoval.AutoSize = true;
            this.chkAllowVarBlockTagRemoval.Location = new System.Drawing.Point(17, 105);
            this.chkAllowVarBlockTagRemoval.Name = "chkAllowVarBlockTagRemoval";
            this.chkAllowVarBlockTagRemoval.Size = new System.Drawing.Size(184, 19);
            this.chkAllowVarBlockTagRemoval.TabIndex = 10;
            this.chkAllowVarBlockTagRemoval.Text = "Allow Removal of &Individual Tags";
            this.chkAllowVarBlockTagRemoval.UseVisualStyleBackColor = true;
            // 
            // tabMailSetup
            // 
            this.tabMailSetup.Controls.Add(this.chkAllowErrorEmails);
            this.tabMailSetup.Controls.Add(this.grpMailLogin);
            this.tabMailSetup.Controls.Add(this.txtMailHost);
            this.tabMailSetup.Controls.Add(this.txtAdminEMail);
            this.tabMailSetup.Controls.Add(this.label6);
            this.tabMailSetup.Controls.Add(this.lblAdminEMail);
            this.tabMailSetup.Location = new System.Drawing.Point(4, 22);
            this.tabMailSetup.Name = "tabMailSetup";
            this.tabMailSetup.Size = new System.Drawing.Size(608, 381);
            this.tabMailSetup.TabIndex = 6;
            this.tabMailSetup.Text = "Mail Setup";
            this.tabMailSetup.UseVisualStyleBackColor = true;
            // 
            // chkAllowErrorEmails
            // 
            this.chkAllowErrorEmails.AutoSize = true;
            this.chkAllowErrorEmails.Location = new System.Drawing.Point(51, 310);
            this.chkAllowErrorEmails.Name = "chkAllowErrorEmails";
            this.chkAllowErrorEmails.Size = new System.Drawing.Size(231, 19);
            this.chkAllowErrorEmails.TabIndex = 5;
            this.chkAllowErrorEmails.Text = "Allow users to email errors to administrator";
            this.chkAllowErrorEmails.UseVisualStyleBackColor = true;
            // 
            // grpMailLogin
            // 
            this.grpMailLogin.Controls.Add(this.txtMailPort);
            this.grpMailLogin.Controls.Add(this.lblMailPort);
            this.grpMailLogin.Controls.Add(this.chkUseNetworkCredentials);
            this.grpMailLogin.Controls.Add(this.chkUseSSL);
            this.grpMailLogin.Controls.Add(this.txtMailPWD);
            this.grpMailLogin.Controls.Add(this.lblMailUserID);
            this.grpMailLogin.Controls.Add(this.lblMailPWD);
            this.grpMailLogin.Controls.Add(this.txtMailUserID);
            this.grpMailLogin.Location = new System.Drawing.Point(25, 120);
            this.grpMailLogin.Name = "grpMailLogin";
            this.grpMailLogin.Size = new System.Drawing.Size(423, 167);
            this.grpMailLogin.TabIndex = 4;
            this.grpMailLogin.TabStop = false;
            this.grpMailLogin.Text = "Server Login";
            // 
            // txtMailPort
            // 
            this.txtMailPort.Location = new System.Drawing.Point(190, 130);
            this.txtMailPort.Name = "txtMailPort";
            this.txtMailPort.Size = new System.Drawing.Size(201, 22);
            this.txtMailPort.TabIndex = 7;
            // 
            // lblMailPort
            // 
            this.lblMailPort.AutoSize = true;
            this.lblMailPort.Location = new System.Drawing.Point(127, 133);
            this.lblMailPort.Name = "lblMailPort";
            this.lblMailPort.Size = new System.Drawing.Size(30, 15);
            this.lblMailPort.TabIndex = 6;
            this.lblMailPort.Text = "&Port:";
            // 
            // chkUseNetworkCredentials
            // 
            this.chkUseNetworkCredentials.AutoSize = true;
            this.chkUseNetworkCredentials.BackColor = System.Drawing.Color.Transparent;
            this.chkUseNetworkCredentials.Location = new System.Drawing.Point(26, 29);
            this.chkUseNetworkCredentials.Name = "chkUseNetworkCredentials";
            this.chkUseNetworkCredentials.Size = new System.Drawing.Size(267, 19);
            this.chkUseNetworkCredentials.TabIndex = 0;
            this.chkUseNetworkCredentials.Text = "&Use Network Credentials To Login To Mail Server";
            this.chkUseNetworkCredentials.UseVisualStyleBackColor = false;
            this.chkUseNetworkCredentials.CheckedChanged += new System.EventHandler(this.chkUseNetworkCredentials_CheckedChanged);
            // 
            // chkUseSSL
            // 
            this.chkUseSSL.AutoSize = true;
            this.chkUseSSL.Location = new System.Drawing.Point(26, 133);
            this.chkUseSSL.Name = "chkUseSSL";
            this.chkUseSSL.Size = new System.Drawing.Size(69, 19);
            this.chkUseSSL.TabIndex = 5;
            this.chkUseSSL.Text = "Use &SSL";
            this.chkUseSSL.UseVisualStyleBackColor = true;
            // 
            // txtMailPWD
            // 
            this.txtMailPWD.Location = new System.Drawing.Point(190, 93);
            this.txtMailPWD.Name = "txtMailPWD";
            this.txtMailPWD.PasswordChar = '*';
            this.txtMailPWD.Size = new System.Drawing.Size(201, 22);
            this.txtMailPWD.TabIndex = 4;
            // 
            // lblMailUserID
            // 
            this.lblMailUserID.AutoSize = true;
            this.lblMailUserID.Location = new System.Drawing.Point(23, 64);
            this.lblMailUserID.Name = "lblMailUserID";
            this.lblMailUserID.Size = new System.Drawing.Size(129, 15);
            this.lblMailUserID.TabIndex = 1;
            this.lblMailUserID.Text = "Sender Account &User ID:";
            // 
            // lblMailPWD
            // 
            this.lblMailPWD.AutoSize = true;
            this.lblMailPWD.Location = new System.Drawing.Point(23, 96);
            this.lblMailPWD.Name = "lblMailPWD";
            this.lblMailPWD.Size = new System.Drawing.Size(140, 15);
            this.lblMailPWD.TabIndex = 3;
            this.lblMailPWD.Text = "Sender Account &Password:";
            // 
            // txtMailUserID
            // 
            this.txtMailUserID.Location = new System.Drawing.Point(190, 61);
            this.txtMailUserID.Name = "txtMailUserID";
            this.txtMailUserID.Size = new System.Drawing.Size(201, 22);
            this.txtMailUserID.TabIndex = 2;
            // 
            // txtMailHost
            // 
            this.txtMailHost.Location = new System.Drawing.Point(215, 74);
            this.txtMailHost.Name = "txtMailHost";
            this.txtMailHost.Size = new System.Drawing.Size(201, 22);
            this.txtMailHost.TabIndex = 3;
            // 
            // txtAdminEMail
            // 
            this.txtAdminEMail.Location = new System.Drawing.Point(215, 40);
            this.txtAdminEMail.Name = "txtAdminEMail";
            this.txtAdminEMail.Size = new System.Drawing.Size(201, 22);
            this.txtAdminEMail.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "Mail &Host:";
            // 
            // lblAdminEMail
            // 
            this.lblAdminEMail.AutoSize = true;
            this.lblAdminEMail.Location = new System.Drawing.Point(32, 43);
            this.lblAdminEMail.Name = "lblAdminEMail";
            this.lblAdminEMail.Size = new System.Drawing.Size(146, 15);
            this.lblAdminEMail.TabIndex = 0;
            this.lblAdminEMail.Text = "Administrator &EMail Address:";
            // 
            // tabSecurity
            // 
            this.tabSecurity.Controls.Add(this.chkBase64Encoding);
            this.tabSecurity.Controls.Add(this.tsProtectionPasswords);
            this.tabSecurity.Controls.Add(this.grdPasswords);
            this.tabSecurity.Location = new System.Drawing.Point(4, 22);
            this.tabSecurity.Name = "tabSecurity";
            this.tabSecurity.Padding = new System.Windows.Forms.Padding(3);
            this.tabSecurity.Size = new System.Drawing.Size(608, 381);
            this.tabSecurity.TabIndex = 7;
            this.tabSecurity.Text = "Security";
            this.tabSecurity.UseVisualStyleBackColor = true;
            // 
            // chkBase64Encoding
            // 
            this.chkBase64Encoding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkBase64Encoding.AutoSize = true;
            this.chkBase64Encoding.Location = new System.Drawing.Point(29, 323);
            this.chkBase64Encoding.Name = "chkBase64Encoding";
            this.chkBase64Encoding.Size = new System.Drawing.Size(134, 19);
            this.chkBase64Encoding.TabIndex = 0;
            this.chkBase64Encoding.Text = "Use &Base64 Encoding";
            this.chkBase64Encoding.UseVisualStyleBackColor = true;
            // 
            // tsProtectionPasswords
            // 
            this.tsProtectionPasswords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsProtectionPasswords.AutoSize = false;
            this.tsProtectionPasswords.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsProtectionPasswords.Dock = System.Windows.Forms.DockStyle.None;
            this.tsProtectionPasswords.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsProtectionPasswords.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblPassword,
            this.toolStripSeparator1,
            this.tbtnPasswordNew,
            this.toolStripSeparator3,
            this.tbtnPasswordDelete});
            this.tsProtectionPasswords.Location = new System.Drawing.Point(19, 18);
            this.tsProtectionPasswords.Name = "tsProtectionPasswords";
            this.tsProtectionPasswords.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsProtectionPasswords.Size = new System.Drawing.Size(571, 27);
            this.tsProtectionPasswords.TabIndex = 21;
            // 
            // tlblPassword
            // 
            this.tlblPassword.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblPassword.Name = "tlblPassword";
            this.tlblPassword.Size = new System.Drawing.Size(187, 24);
            this.tlblPassword.Text = " Firm Document Protection Passwords";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // tbtnPasswordNew
            // 
            this.tbtnPasswordNew.AutoSize = false;
            this.tbtnPasswordNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnPasswordNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnPasswordNew.Image")));
            this.tbtnPasswordNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnPasswordNew.Name = "tbtnPasswordNew";
            this.tbtnPasswordNew.Size = new System.Drawing.Size(48, 22);
            this.tbtnPasswordNew.Text = "&New...";
            this.tbtnPasswordNew.Click += new System.EventHandler(this.tbtnPasswordNew_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // tbtnPasswordDelete
            // 
            this.tbtnPasswordDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnPasswordDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnPasswordDelete.Image")));
            this.tbtnPasswordDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnPasswordDelete.Name = "tbtnPasswordDelete";
            this.tbtnPasswordDelete.Size = new System.Drawing.Size(53, 24);
            this.tbtnPasswordDelete.Text = "&Delete...";
            this.tbtnPasswordDelete.Click += new System.EventHandler(this.tbtnPasswordDelete_Click);
            // 
            // grdPasswords
            // 
            this.grdPasswords.AllowUserToAddRows = false;
            this.grdPasswords.AllowUserToDeleteRows = false;
            this.grdPasswords.AllowUserToResizeRows = false;
            this.grdPasswords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdPasswords.BackgroundColor = System.Drawing.Color.White;
            this.grdPasswords.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdPasswords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPasswords.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdPasswords.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdPasswords.IsDirty = false;
            this.grdPasswords.Location = new System.Drawing.Point(19, 43);
            this.grdPasswords.MultiSelect = false;
            this.grdPasswords.Name = "grdPasswords";
            this.grdPasswords.RowHeadersVisible = false;
            this.grdPasswords.RowHeadersWidth = 25;
            this.grdPasswords.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdPasswords.Size = new System.Drawing.Size(571, 266);
            this.grdPasswords.SupportingValues = "";
            this.grdPasswords.TabIndex = 20;
            this.grdPasswords.Tag2 = null;
            this.grdPasswords.Value = null;
            // 
            // tabOther
            // 
            this.tabOther.Controls.Add(this.txtAdvancedAddressBookEmailPrefix);
            this.tabOther.Controls.Add(this.txtAdvancedAddressBookFaxPrefix);
            this.tabOther.Controls.Add(this.lblAdvancedAddressBookEmailPrefix);
            this.tabOther.Controls.Add(this.lblAdvancedAddressBookFaxPrefix);
            this.tabOther.Controls.Add(this.lblAdvancedAddressBookPhonePrefix);
            this.tabOther.Controls.Add(this.txtAdvancedAddressBookPhonePrefix);
            this.tabOther.Controls.Add(this.lblAdvancedAddressBookFormatPrefixes);
            this.tabOther.Controls.Add(this.cmbDefaultEnvelopeSegment);
            this.tabOther.Controls.Add(this.lblDefaultEnvelopeSegment);
            this.tabOther.Controls.Add(this.cmbDefaultLabelSegment);
            this.tabOther.Controls.Add(this.lblDefaultLabelSegment);
            this.tabOther.Controls.Add(this.tsMain);
            this.tabOther.Controls.Add(this.txtProtectionPassword);
            this.tabOther.Controls.Add(this.lblProtectionPassword);
            this.tabOther.Controls.Add(this.txtBlankWordDocTemplate);
            this.tabOther.Controls.Add(this.lblBlankWordDocTemplate);
            this.tabOther.Controls.Add(this.grpCheckboxConfig);
            this.tabOther.Controls.Add(this.cmbCheckboxFont);
            this.tabOther.Controls.Add(this.grdTypes);
            this.tabOther.Location = new System.Drawing.Point(4, 22);
            this.tabOther.Name = "tabOther";
            this.tabOther.Padding = new System.Windows.Forms.Padding(3);
            this.tabOther.Size = new System.Drawing.Size(608, 381);
            this.tabOther.TabIndex = 4;
            this.tabOther.Text = "Other";
            this.tabOther.UseVisualStyleBackColor = true;
            // 
            // txtAdvancedAddressBookEmailPrefix
            // 
            this.txtAdvancedAddressBookEmailPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtAdvancedAddressBookEmailPrefix.Location = new System.Drawing.Point(488, 346);
            this.txtAdvancedAddressBookEmailPrefix.Name = "txtAdvancedAddressBookEmailPrefix";
            this.txtAdvancedAddressBookEmailPrefix.Size = new System.Drawing.Size(102, 22);
            this.txtAdvancedAddressBookEmailPrefix.TabIndex = 16;
            // 
            // txtAdvancedAddressBookFaxPrefix
            // 
            this.txtAdvancedAddressBookFaxPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtAdvancedAddressBookFaxPrefix.Location = new System.Drawing.Point(371, 346);
            this.txtAdvancedAddressBookFaxPrefix.Name = "txtAdvancedAddressBookFaxPrefix";
            this.txtAdvancedAddressBookFaxPrefix.Size = new System.Drawing.Size(67, 22);
            this.txtAdvancedAddressBookFaxPrefix.TabIndex = 14;
            // 
            // lblAdvancedAddressBookEmailPrefix
            // 
            this.lblAdvancedAddressBookEmailPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAdvancedAddressBookEmailPrefix.AutoSize = true;
            this.lblAdvancedAddressBookEmailPrefix.Location = new System.Drawing.Point(447, 349);
            this.lblAdvancedAddressBookEmailPrefix.Name = "lblAdvancedAddressBookEmailPrefix";
            this.lblAdvancedAddressBookEmailPrefix.Size = new System.Drawing.Size(35, 15);
            this.lblAdvancedAddressBookEmailPrefix.TabIndex = 15;
            this.lblAdvancedAddressBookEmailPrefix.Text = "&Email:";
            // 
            // lblAdvancedAddressBookFaxPrefix
            // 
            this.lblAdvancedAddressBookFaxPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAdvancedAddressBookFaxPrefix.AutoSize = true;
            this.lblAdvancedAddressBookFaxPrefix.Location = new System.Drawing.Point(336, 349);
            this.lblAdvancedAddressBookFaxPrefix.Name = "lblAdvancedAddressBookFaxPrefix";
            this.lblAdvancedAddressBookFaxPrefix.Size = new System.Drawing.Size(29, 15);
            this.lblAdvancedAddressBookFaxPrefix.TabIndex = 13;
            this.lblAdvancedAddressBookFaxPrefix.Text = "Fa&x:";
            // 
            // lblAdvancedAddressBookPhonePrefix
            // 
            this.lblAdvancedAddressBookPhonePrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAdvancedAddressBookPhonePrefix.AutoSize = true;
            this.lblAdvancedAddressBookPhonePrefix.Location = new System.Drawing.Point(210, 349);
            this.lblAdvancedAddressBookPhonePrefix.Name = "lblAdvancedAddressBookPhonePrefix";
            this.lblAdvancedAddressBookPhonePrefix.Size = new System.Drawing.Size(41, 15);
            this.lblAdvancedAddressBookPhonePrefix.TabIndex = 11;
            this.lblAdvancedAddressBookPhonePrefix.Text = "&Phone:";
            // 
            // txtAdvancedAddressBookPhonePrefix
            // 
            this.txtAdvancedAddressBookPhonePrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtAdvancedAddressBookPhonePrefix.Location = new System.Drawing.Point(257, 346);
            this.txtAdvancedAddressBookPhonePrefix.Name = "txtAdvancedAddressBookPhonePrefix";
            this.txtAdvancedAddressBookPhonePrefix.Size = new System.Drawing.Size(68, 22);
            this.txtAdvancedAddressBookPhonePrefix.TabIndex = 12;
            // 
            // lblAdvancedAddressBookFormatPrefixes
            // 
            this.lblAdvancedAddressBookFormatPrefixes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAdvancedAddressBookFormatPrefixes.AutoSize = true;
            this.lblAdvancedAddressBookFormatPrefixes.Location = new System.Drawing.Point(27, 349);
            this.lblAdvancedAddressBookFormatPrefixes.Name = "lblAdvancedAddressBookFormatPrefixes";
            this.lblAdvancedAddressBookFormatPrefixes.Size = new System.Drawing.Size(174, 15);
            this.lblAdvancedAddressBookFormatPrefixes.TabIndex = 10;
            this.lblAdvancedAddressBookFormatPrefixes.Text = "Advanced Address Book Prefixes:";
            // 
            // cmbDefaultEnvelopeSegment
            // 
            this.cmbDefaultEnvelopeSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbDefaultEnvelopeSegment.FormattingEnabled = true;
            this.cmbDefaultEnvelopeSegment.Location = new System.Drawing.Point(198, 278);
            this.cmbDefaultEnvelopeSegment.Name = "cmbDefaultEnvelopeSegment";
            this.cmbDefaultEnvelopeSegment.Size = new System.Drawing.Size(263, 23);
            this.cmbDefaultEnvelopeSegment.TabIndex = 7;
            // 
            // lblDefaultEnvelopeSegment
            // 
            this.lblDefaultEnvelopeSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDefaultEnvelopeSegment.AutoSize = true;
            this.lblDefaultEnvelopeSegment.Location = new System.Drawing.Point(27, 281);
            this.lblDefaultEnvelopeSegment.Name = "lblDefaultEnvelopeSegment";
            this.lblDefaultEnvelopeSegment.Size = new System.Drawing.Size(137, 15);
            this.lblDefaultEnvelopeSegment.TabIndex = 6;
            this.lblDefaultEnvelopeSegment.Text = "Default &Envelope Segment:";
            // 
            // cmbDefaultLabelSegment
            // 
            this.cmbDefaultLabelSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbDefaultLabelSegment.FormattingEnabled = true;
            this.cmbDefaultLabelSegment.Location = new System.Drawing.Point(198, 243);
            this.cmbDefaultLabelSegment.Name = "cmbDefaultLabelSegment";
            this.cmbDefaultLabelSegment.Size = new System.Drawing.Size(263, 23);
            this.cmbDefaultLabelSegment.TabIndex = 5;
            // 
            // lblDefaultLabelSegment
            // 
            this.lblDefaultLabelSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDefaultLabelSegment.AutoSize = true;
            this.lblDefaultLabelSegment.Location = new System.Drawing.Point(27, 246);
            this.lblDefaultLabelSegment.Name = "lblDefaultLabelSegment";
            this.lblDefaultLabelSegment.Size = new System.Drawing.Size(118, 15);
            this.lblDefaultLabelSegment.TabIndex = 4;
            this.lblDefaultLabelSegment.Text = "Default &Label Segment:";
            // 
            // tsMain
            // 
            this.tsMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsMain.AutoSize = false;
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator4,
            this.tbtnNew,
            this.toolStripSeparator2,
            this.tbtnDelete});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(18, 13);
            this.tsMain.Name = "tsMain";
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMain.Size = new System.Drawing.Size(572, 28);
            this.tsMain.TabIndex = 0;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(108, 25);
            this.toolStripLabel1.Text = " Default User Folders";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 28);
            // 
            // tbtnNew
            // 
            this.tbtnNew.AutoSize = false;
            this.tbtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNew.Image")));
            this.tbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNew.Name = "tbtnNew";
            this.tbtnNew.Size = new System.Drawing.Size(48, 22);
            this.tbtnNew.Text = "&New...";
            this.tbtnNew.Click += new System.EventHandler(this.tbtnNew_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(54, 25);
            this.tbtnDelete.Text = "&Delete...";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // txtProtectionPassword
            // 
            this.txtProtectionPassword.Location = new System.Drawing.Point(562, 118);
            this.txtProtectionPassword.Name = "txtProtectionPassword";
            this.txtProtectionPassword.PasswordChar = '*';
            this.txtProtectionPassword.Size = new System.Drawing.Size(243, 22);
            this.txtProtectionPassword.TabIndex = 3;
            this.txtProtectionPassword.Visible = false;
            // 
            // lblProtectionPassword
            // 
            this.lblProtectionPassword.AutoSize = true;
            this.lblProtectionPassword.Location = new System.Drawing.Point(559, 100);
            this.lblProtectionPassword.Name = "lblProtectionPassword";
            this.lblProtectionPassword.Size = new System.Drawing.Size(187, 15);
            this.lblProtectionPassword.TabIndex = 2;
            this.lblProtectionPassword.Text = "Firm Document Protection &Password:";
            this.lblProtectionPassword.Visible = false;
            // 
            // txtBlankWordDocTemplate
            // 
            this.txtBlankWordDocTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtBlankWordDocTemplate.Location = new System.Drawing.Point(198, 313);
            this.txtBlankWordDocTemplate.Name = "txtBlankWordDocTemplate";
            this.txtBlankWordDocTemplate.Size = new System.Drawing.Size(263, 22);
            this.txtBlankWordDocTemplate.TabIndex = 9;
            // 
            // lblBlankWordDocTemplate
            // 
            this.lblBlankWordDocTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBlankWordDocTemplate.AutoSize = true;
            this.lblBlankWordDocTemplate.Location = new System.Drawing.Point(27, 316);
            this.lblBlankWordDocTemplate.Name = "lblBlankWordDocTemplate";
            this.lblBlankWordDocTemplate.Size = new System.Drawing.Size(165, 15);
            this.lblBlankWordDocTemplate.TabIndex = 8;
            this.lblBlankWordDocTemplate.Text = "Blank &Word Document Template:";
            // 
            // grpCheckboxConfig
            // 
            this.grpCheckboxConfig.Controls.Add(this.txtUncheckedCheckboxChar);
            this.grpCheckboxConfig.Controls.Add(this.lblUncheckedCheckboxChar);
            this.grpCheckboxConfig.Controls.Add(this.txtCheckedCheckboxChar);
            this.grpCheckboxConfig.Controls.Add(this.lblCheckedCheckboxChar);
            this.grpCheckboxConfig.Controls.Add(this.lblCheckboxFont);
            this.grpCheckboxConfig.Location = new System.Drawing.Point(596, 329);
            this.grpCheckboxConfig.Name = "grpCheckboxConfig";
            this.grpCheckboxConfig.Size = new System.Drawing.Size(141, 109);
            this.grpCheckboxConfig.TabIndex = 3;
            this.grpCheckboxConfig.TabStop = false;
            this.grpCheckboxConfig.Text = "Document Checkbox Configuration";
            this.grpCheckboxConfig.Visible = false;
            // 
            // txtUncheckedCheckboxChar
            // 
            this.txtUncheckedCheckboxChar.Location = new System.Drawing.Point(215, 89);
            this.txtUncheckedCheckboxChar.Name = "txtUncheckedCheckboxChar";
            this.txtUncheckedCheckboxChar.Size = new System.Drawing.Size(188, 22);
            this.txtUncheckedCheckboxChar.TabIndex = 7;
            // 
            // lblUncheckedCheckboxChar
            // 
            this.lblUncheckedCheckboxChar.AutoSize = true;
            this.lblUncheckedCheckboxChar.Location = new System.Drawing.Point(9, 92);
            this.lblUncheckedCheckboxChar.Name = "lblUncheckedCheckboxChar";
            this.lblUncheckedCheckboxChar.Size = new System.Drawing.Size(202, 15);
            this.lblUncheckedCheckboxChar.TabIndex = 6;
            this.lblUncheckedCheckboxChar.Text = "&Unchecked Checkbox Character ASCII:";
            // 
            // txtCheckedCheckboxChar
            // 
            this.txtCheckedCheckboxChar.Location = new System.Drawing.Point(215, 61);
            this.txtCheckedCheckboxChar.Name = "txtCheckedCheckboxChar";
            this.txtCheckedCheckboxChar.Size = new System.Drawing.Size(188, 22);
            this.txtCheckedCheckboxChar.TabIndex = 5;
            // 
            // lblCheckedCheckboxChar
            // 
            this.lblCheckedCheckboxChar.AutoSize = true;
            this.lblCheckedCheckboxChar.Location = new System.Drawing.Point(9, 64);
            this.lblCheckedCheckboxChar.Name = "lblCheckedCheckboxChar";
            this.lblCheckedCheckboxChar.Size = new System.Drawing.Size(190, 15);
            this.lblCheckedCheckboxChar.TabIndex = 4;
            this.lblCheckedCheckboxChar.Text = "Checked &Checkbox Character ASCII:";
            // 
            // lblCheckboxFont
            // 
            this.lblCheckboxFont.AutoSize = true;
            this.lblCheckboxFont.Location = new System.Drawing.Point(9, 36);
            this.lblCheckboxFont.Name = "lblCheckboxFont";
            this.lblCheckboxFont.Size = new System.Drawing.Size(85, 15);
            this.lblCheckboxFont.TabIndex = 2;
            this.lblCheckboxFont.Text = "Checkbox &Font:";
            // 
            // cmbCheckboxFont
            // 
            this.cmbCheckboxFont.AllowEmptyValue = false;
            this.cmbCheckboxFont.Borderless = false;
            this.cmbCheckboxFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbCheckboxFont.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCheckboxFont.IsDirty = false;
            this.cmbCheckboxFont.LimitToList = true;
            this.cmbCheckboxFont.ListName = "";
            this.cmbCheckboxFont.Location = new System.Drawing.Point(809, 310);
            this.cmbCheckboxFont.MaxDropDownItems = 8;
            this.cmbCheckboxFont.Name = "cmbCheckboxFont";
            this.cmbCheckboxFont.OnNotInList = 0;
            this.cmbCheckboxFont.SelectedIndex = -1;
            this.cmbCheckboxFont.SelectedValue = null;
            this.cmbCheckboxFont.SelectionLength = 0;
            this.cmbCheckboxFont.SelectionStart = 0;
            this.cmbCheckboxFont.Size = new System.Drawing.Size(188, 23);
            this.cmbCheckboxFont.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbCheckboxFont.SupportingValues = "";
            this.cmbCheckboxFont.TabIndex = 3;
            this.cmbCheckboxFont.Tag2 = null;
            this.cmbCheckboxFont.Value = "";
            // 
            // grdTypes
            // 
            this.grdTypes.AllowUserToAddRows = false;
            this.grdTypes.AllowUserToDeleteRows = false;
            this.grdTypes.AllowUserToResizeRows = false;
            this.grdTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdTypes.BackgroundColor = System.Drawing.Color.White;
            this.grdTypes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdTypes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdTypes.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdTypes.IsDirty = false;
            this.grdTypes.Location = new System.Drawing.Point(18, 38);
            this.grdTypes.MultiSelect = false;
            this.grdTypes.Name = "grdTypes";
            this.grdTypes.RowHeadersVisible = false;
            this.grdTypes.RowHeadersWidth = 25;
            this.grdTypes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdTypes.Size = new System.Drawing.Size(572, 189);
            this.grdTypes.SupportingValues = "";
            this.grdTypes.TabIndex = 1;
            this.grdTypes.Tag2 = null;
            this.grdTypes.Value = null;
            // 
            // tabDefaultUserSettings
            // 
            this.tabDefaultUserSettings.Controls.Add(this.btnResetUserForAllEntities);
            this.tabDefaultUserSettings.Controls.Add(this.btnResetUserToFirmDefaults);
            this.tabDefaultUserSettings.Controls.Add(this.grpAdditionalOptions);
            this.tabDefaultUserSettings.Controls.Add(this.grpTaskPane);
            this.tabDefaultUserSettings.Location = new System.Drawing.Point(4, 24);
            this.tabDefaultUserSettings.Name = "tabDefaultUserSettings";
            this.tabDefaultUserSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabDefaultUserSettings.Size = new System.Drawing.Size(628, 480);
            this.tabDefaultUserSettings.TabIndex = 1;
            this.tabDefaultUserSettings.Text = "Default User Settings";
            this.tabDefaultUserSettings.UseVisualStyleBackColor = true;
            // 
            // grpAdditionalOptions
            // 
            this.grpAdditionalOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpAdditionalOptions.Controls.Add(this.lblDefaultZoomPercentage);
            this.grpAdditionalOptions.Controls.Add(this.spnDefaultZoomPercentage);
            this.grpAdditionalOptions.Controls.Add(this.chkUpdateZoom);
            this.grpAdditionalOptions.Controls.Add(this.chkWarnBeforeDeactivation);
            this.grpAdditionalOptions.Controls.Add(this.chkActivateMacPacRibbonTabOnOpen);
            this.grpAdditionalOptions.Controls.Add(this.chkActivateMacPacRibbonTab);
            this.grpAdditionalOptions.Controls.Add(this.chkShowXMLTags);
            this.grpAdditionalOptions.Controls.Add(this.chkShowQuickHelp);
            this.grpAdditionalOptions.Location = new System.Drawing.Point(12, 262);
            this.grpAdditionalOptions.Name = "grpAdditionalOptions";
            this.grpAdditionalOptions.Size = new System.Drawing.Size(594, 166);
            this.grpAdditionalOptions.TabIndex = 1;
            this.grpAdditionalOptions.TabStop = false;
            this.grpAdditionalOptions.Text = "Additional Options";
            // 
            // lblDefaultZoomPercentage
            // 
            this.lblDefaultZoomPercentage.AutoSize = true;
            this.lblDefaultZoomPercentage.Location = new System.Drawing.Point(26, 141);
            this.lblDefaultZoomPercentage.Name = "lblDefaultZoomPercentage";
            this.lblDefaultZoomPercentage.Size = new System.Drawing.Size(133, 15);
            this.lblDefaultZoomPercentage.TabIndex = 232;
            this.lblDefaultZoomPercentage.Text = "Default &Zoom Percentage:";
            // 
            // spnDefaultZoomPercentage
            // 
            this.spnDefaultZoomPercentage.Enabled = false;
            this.spnDefaultZoomPercentage.Location = new System.Drawing.Point(165, 139);
            this.spnDefaultZoomPercentage.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.spnDefaultZoomPercentage.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spnDefaultZoomPercentage.Name = "spnDefaultZoomPercentage";
            this.spnDefaultZoomPercentage.Size = new System.Drawing.Size(69, 22);
            this.spnDefaultZoomPercentage.TabIndex = 236;
            this.spnDefaultZoomPercentage.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // chkUpdateZoom
            // 
            this.chkUpdateZoom.AutoSize = true;
            this.chkUpdateZoom.Location = new System.Drawing.Point(10, 115);
            this.chkUpdateZoom.Name = "chkUpdateZoom";
            this.chkUpdateZoom.Size = new System.Drawing.Size(209, 19);
            this.chkUpdateZoom.TabIndex = 235;
            this.chkUpdateZoom.Text = "&Update Zoom on opening a document.";
            this.chkUpdateZoom.CheckedChanged += new System.EventHandler(this.chkUpdateZoom_CheckedChanged);
            // 
            // chkWarnBeforeDeactivation
            // 
            this.chkWarnBeforeDeactivation.AutoSize = true;
            this.chkWarnBeforeDeactivation.Location = new System.Drawing.Point(10, 113);
            this.chkWarnBeforeDeactivation.Name = "chkWarnBeforeDeactivation";
            this.chkWarnBeforeDeactivation.Size = new System.Drawing.Size(194, 19);
            this.chkWarnBeforeDeactivation.TabIndex = 230;
            this.chkWarnBeforeDeactivation.Text = "Warn before deactivating segment.";
            this.chkWarnBeforeDeactivation.UseVisualStyleBackColor = true;
            this.chkWarnBeforeDeactivation.Visible = false;
            // 
            // chkActivateMacPacRibbonTabOnOpen
            // 
            this.chkActivateMacPacRibbonTabOnOpen.AutoSize = true;
            this.chkActivateMacPacRibbonTabOnOpen.Location = new System.Drawing.Point(10, 44);
            this.chkActivateMacPacRibbonTabOnOpen.Name = "chkActivateMacPacRibbonTabOnOpen";
            this.chkActivateMacPacRibbonTabOnOpen.Size = new System.Drawing.Size(333, 19);
            this.chkActivateMacPacRibbonTabOnOpen.TabIndex = 232;
            this.chkActivateMacPacRibbonTabOnOpen.Text = "Activate the &Forte ribbon tab when a Forte document is opened.";
            // 
            // chkActivateMacPacRibbonTab
            // 
            this.chkActivateMacPacRibbonTab.AutoSize = true;
            this.chkActivateMacPacRibbonTab.Location = new System.Drawing.Point(10, 21);
            this.chkActivateMacPacRibbonTab.Name = "chkActivateMacPacRibbonTab";
            this.chkActivateMacPacRibbonTab.Size = new System.Drawing.Size(334, 19);
            this.chkActivateMacPacRibbonTab.TabIndex = 231;
            this.chkActivateMacPacRibbonTab.Text = "&Activate the Forte ribbon tab when a Forte document is created.";
            // 
            // chkShowXMLTags
            // 
            this.chkShowXMLTags.AutoSize = true;
            this.chkShowXMLTags.Location = new System.Drawing.Point(10, 69);
            this.chkShowXMLTags.Name = "chkShowXMLTags";
            this.chkShowXMLTags.Size = new System.Drawing.Size(225, 19);
            this.chkShowXMLTags.TabIndex = 233;
            this.chkShowXMLTags.Text = "Show XML &Tags when editing documents";
            this.chkShowXMLTags.UseVisualStyleBackColor = true;
            // 
            // chkShowQuickHelp
            // 
            this.chkShowQuickHelp.AutoSize = true;
            this.chkShowQuickHelp.Location = new System.Drawing.Point(10, 93);
            this.chkShowQuickHelp.Name = "chkShowQuickHelp";
            this.chkShowQuickHelp.Size = new System.Drawing.Size(110, 19);
            this.chkShowQuickHelp.TabIndex = 234;
            this.chkShowQuickHelp.Text = "Show Quick &Help";
            this.chkShowQuickHelp.UseVisualStyleBackColor = true;
            // 
            // grpTaskPane
            // 
            this.grpTaskPane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTaskPane.Controls.Add(this.chkShowTaskpaneWhenNoVariables);
            this.grpTaskPane.Controls.Add(this.chkExpandFindResults);
            this.grpTaskPane.Controls.Add(this.chkWarnOnFinish);
            this.grpTaskPane.Controls.Add(this.chkCloseTaskPaneOnFinish);
            this.grpTaskPane.Controls.Add(this.chkShowTaskPaneOnBlankNew);
            this.grpTaskPane.Controls.Add(this.lblTaskPaneBackgroundColor);
            this.grpTaskPane.Controls.Add(this.chkDisplayTaskPaneForMPDoc);
            this.grpTaskPane.Controls.Add(this.chkDisplayTaskPaneForNonMPDoc);
            this.grpTaskPane.Controls.Add(this.chkViewPgWidth);
            this.grpTaskPane.Controls.Add(this.pnlTaskPaneColorChip);
            this.grpTaskPane.Controls.Add(this.chkUseGradient);
            this.grpTaskPane.Controls.Add(this.btnPickColor);
            this.grpTaskPane.Location = new System.Drawing.Point(12, 13);
            this.grpTaskPane.Name = "grpTaskPane";
            this.grpTaskPane.Size = new System.Drawing.Size(594, 243);
            this.grpTaskPane.TabIndex = 0;
            this.grpTaskPane.TabStop = false;
            this.grpTaskPane.Text = "Task Pane";
            // 
            // chkShowTaskpaneWhenNoVariables
            // 
            this.chkShowTaskpaneWhenNoVariables.AutoSize = true;
            this.chkShowTaskpaneWhenNoVariables.Location = new System.Drawing.Point(13, 87);
            this.chkShowTaskpaneWhenNoVariables.Name = "chkShowTaskpaneWhenNoVariables";
            this.chkShowTaskpaneWhenNoVariables.Size = new System.Drawing.Size(347, 19);
            this.chkShowTaskpaneWhenNoVariables.TabIndex = 3;
            this.chkShowTaskpaneWhenNoVariables.Text = "Sh&ow task pane when creating a Forte document with no variables";
            this.chkShowTaskpaneWhenNoVariables.UseVisualStyleBackColor = true;
            // 
            // chkExpandFindResults
            // 
            this.chkExpandFindResults.AutoSize = true;
            this.chkExpandFindResults.Location = new System.Drawing.Point(13, 194);
            this.chkExpandFindResults.Margin = new System.Windows.Forms.Padding(2);
            this.chkExpandFindResults.Name = "chkExpandFindResults";
            this.chkExpandFindResults.Size = new System.Drawing.Size(188, 19);
            this.chkExpandFindResults.TabIndex = 9;
            this.chkExpandFindResults.Text = "&Expand Find results automatically";
            this.chkExpandFindResults.UseVisualStyleBackColor = true;
            // 
            // chkWarnOnFinish
            // 
            this.chkWarnOnFinish.AutoSize = true;
            this.chkWarnOnFinish.Location = new System.Drawing.Point(13, 173);
            this.chkWarnOnFinish.Margin = new System.Windows.Forms.Padding(2);
            this.chkWarnOnFinish.Name = "chkWarnOnFinish";
            this.chkWarnOnFinish.Size = new System.Drawing.Size(107, 19);
            this.chkWarnOnFinish.TabIndex = 8;
            this.chkWarnOnFinish.Text = "&Prompt on Finish";
            this.chkWarnOnFinish.UseVisualStyleBackColor = true;
            // 
            // chkCloseTaskPaneOnFinish
            // 
            this.chkCloseTaskPaneOnFinish.AutoSize = true;
            this.chkCloseTaskPaneOnFinish.Location = new System.Drawing.Point(13, 152);
            this.chkCloseTaskPaneOnFinish.Margin = new System.Windows.Forms.Padding(2);
            this.chkCloseTaskPaneOnFinish.Name = "chkCloseTaskPaneOnFinish";
            this.chkCloseTaskPaneOnFinish.Size = new System.Drawing.Size(152, 19);
            this.chkCloseTaskPaneOnFinish.TabIndex = 6;
            this.chkCloseTaskPaneOnFinish.Text = "&Close task pane on Finish";
            this.chkCloseTaskPaneOnFinish.UseVisualStyleBackColor = true;
            // 
            // chkShowTaskPaneOnBlankNew
            // 
            this.chkShowTaskPaneOnBlankNew.AutoSize = true;
            this.chkShowTaskPaneOnBlankNew.Location = new System.Drawing.Point(13, 64);
            this.chkShowTaskPaneOnBlankNew.Name = "chkShowTaskPaneOnBlankNew";
            this.chkShowTaskPaneOnBlankNew.Size = new System.Drawing.Size(295, 19);
            this.chkShowTaskPaneOnBlankNew.TabIndex = 2;
            this.chkShowTaskPaneOnBlankNew.Text = "Show tas&k pane when creating a blank Forte document.";
            this.chkShowTaskPaneOnBlankNew.UseVisualStyleBackColor = true;
            // 
            // lblTaskPaneBackgroundColor
            // 
            this.lblTaskPaneBackgroundColor.AutoSize = true;
            this.lblTaskPaneBackgroundColor.Location = new System.Drawing.Point(59, 219);
            this.lblTaskPaneBackgroundColor.Name = "lblTaskPaneBackgroundColor";
            this.lblTaskPaneBackgroundColor.Size = new System.Drawing.Size(147, 15);
            this.lblTaskPaneBackgroundColor.TabIndex = 11;
            this.lblTaskPaneBackgroundColor.Text = "Task pane background &color";
            // 
            // chkDisplayTaskPaneForMPDoc
            // 
            this.chkDisplayTaskPaneForMPDoc.AutoSize = true;
            this.chkDisplayTaskPaneForMPDoc.Location = new System.Drawing.Point(13, 22);
            this.chkDisplayTaskPaneForMPDoc.Name = "chkDisplayTaskPaneForMPDoc";
            this.chkDisplayTaskPaneForMPDoc.Size = new System.Drawing.Size(365, 19);
            this.chkDisplayTaskPaneForMPDoc.TabIndex = 0;
            this.chkDisplayTaskPaneForMPDoc.Text = "&Show task pane when a document containing Forte content is opened.";
            this.chkDisplayTaskPaneForMPDoc.UseVisualStyleBackColor = true;
            // 
            // chkDisplayTaskPaneForNonMPDoc
            // 
            this.chkDisplayTaskPaneForNonMPDoc.AutoSize = true;
            this.chkDisplayTaskPaneForNonMPDoc.Location = new System.Drawing.Point(13, 43);
            this.chkDisplayTaskPaneForNonMPDoc.Name = "chkDisplayTaskPaneForNonMPDoc";
            this.chkDisplayTaskPaneForNonMPDoc.Size = new System.Drawing.Size(383, 19);
            this.chkDisplayTaskPaneForNonMPDoc.TabIndex = 1;
            this.chkDisplayTaskPaneForNonMPDoc.Text = "Sho&w task pane when a document not containing Forte content is opened.";
            this.chkDisplayTaskPaneForNonMPDoc.UseVisualStyleBackColor = true;
            // 
            // chkViewPgWidth
            // 
            this.chkViewPgWidth.AutoSize = true;
            this.chkViewPgWidth.Location = new System.Drawing.Point(13, 110);
            this.chkViewPgWidth.Name = "chkViewPgWidth";
            this.chkViewPgWidth.Size = new System.Drawing.Size(231, 19);
            this.chkViewPgWidth.TabIndex = 4;
            this.chkViewPgWidth.Text = "&View page width when task pane is visible.";
            this.chkViewPgWidth.UseVisualStyleBackColor = true;
            // 
            // pnlTaskPaneColorChip
            // 
            this.pnlTaskPaneColorChip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTaskPaneColorChip.Location = new System.Drawing.Point(13, 216);
            this.pnlTaskPaneColorChip.Name = "pnlTaskPaneColorChip";
            this.pnlTaskPaneColorChip.Size = new System.Drawing.Size(45, 19);
            this.pnlTaskPaneColorChip.TabIndex = 10;
            // 
            // chkUseGradient
            // 
            this.chkUseGradient.AutoSize = true;
            this.chkUseGradient.Location = new System.Drawing.Point(13, 131);
            this.chkUseGradient.Name = "chkUseGradient";
            this.chkUseGradient.Size = new System.Drawing.Size(153, 19);
            this.chkUseGradient.TabIndex = 5;
            this.chkUseGradient.Text = "&Use gradient in task pane.";
            this.chkUseGradient.UseVisualStyleBackColor = true;
            // 
            // btnPickColor
            // 
            this.btnPickColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPickColor.Location = new System.Drawing.Point(206, 215);
            this.btnPickColor.Name = "btnPickColor";
            this.btnPickColor.Size = new System.Drawing.Size(27, 23);
            this.btnPickColor.TabIndex = 0;
            this.btnPickColor.TabStop = false;
            this.btnPickColor.Text = "...";
            this.btnPickColor.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPickColor.UseVisualStyleBackColor = true;
            this.btnPickColor.Click += new System.EventHandler(this.btnPickColor_Click);
            // 
            // rdGroups
            // 
            this.rdGroups.AutoSize = true;
            this.rdGroups.Location = new System.Drawing.Point(176, 21);
            this.rdGroups.Name = "rdGroups";
            this.rdGroups.Size = new System.Drawing.Size(62, 19);
            this.rdGroups.TabIndex = 2;
            this.rdGroups.Text = "&Groups";
            this.rdGroups.UseVisualStyleBackColor = true;
            // 
            // rdOffices
            // 
            this.rdOffices.AutoSize = true;
            this.rdOffices.Location = new System.Drawing.Point(104, 21);
            this.rdOffices.Name = "rdOffices";
            this.rdOffices.Size = new System.Drawing.Size(60, 19);
            this.rdOffices.TabIndex = 1;
            this.rdOffices.Text = "&Offices";
            this.rdOffices.UseVisualStyleBackColor = true;
            this.rdOffices.CheckedChanged += new System.EventHandler(this.rdOffices_CheckedChanged);
            // 
            // lblOwnerCategory
            // 
            this.lblOwnerCategory.AutoSize = true;
            this.lblOwnerCategory.Location = new System.Drawing.Point(16, 23);
            this.lblOwnerCategory.Name = "lblOwnerCategory";
            this.lblOwnerCategory.Size = new System.Drawing.Size(75, 15);
            this.lblOwnerCategory.TabIndex = 0;
            this.lblOwnerCategory.Text = "Show Entities:";
            // 
            // cmbEntity
            // 
            this.cmbEntity.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbEntity.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEntity.Location = new System.Drawing.Point(324, 20);
            this.cmbEntity.Name = "cmbEntity";
            this.cmbEntity.Size = new System.Drawing.Size(310, 23);
            this.cmbEntity.TabIndex = 4;
            this.cmbEntity.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.cmbEntity_DrawItem);
            this.cmbEntity.SelectedIndexChanged += new System.EventHandler(this.cmbEntity_SelectedIndexChanged);
            // 
            // lblEntity
            // 
            this.lblEntity.AutoSize = true;
            this.lblEntity.Location = new System.Drawing.Point(285, 23);
            this.lblEntity.Name = "lblEntity";
            this.lblEntity.Size = new System.Drawing.Size(37, 15);
            this.lblEntity.TabIndex = 3;
            this.lblEntity.Text = "&Entity:";
            // 
            // AppSettingsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.rdGroups);
            this.Controls.Add(this.rdOffices);
            this.Controls.Add(this.lblOwnerCategory);
            this.Controls.Add(this.cmbEntity);
            this.Controls.Add(this.lblEntity);
            this.MinimumSize = new System.Drawing.Size(654, 534);
            this.Name = "AppSettingsManager";
            this.Size = new System.Drawing.Size(654, 578);
            this.Load += new System.EventHandler(this.AppSettingsManager_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabFirmSettings.ResumeLayout(false);
            this.gbResetAll.ResumeLayout(false);
            this.gbResetEntity.ResumeLayout(false);
            this.tabFirmAppSettings.ResumeLayout(false);
            this.tabDisplay.ResumeLayout(false);
            this.grpHelpFont.ResumeLayout(false);
            this.grpHelpFont.PerformLayout();
            this.grpContentManager.ResumeLayout(false);
            this.grpContentManager.PerformLayout();
            this.grpPeopleList.ResumeLayout(false);
            this.grpPeopleList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnSavedDataThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnSavedDataLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnUserContentThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnUserContentLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnMaxPeople)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDnPeopleListWarningThreshold)).EndInit();
            this.tabTrailers.ResumeLayout(false);
            this.tabTrailers.PerformLayout();
            this.gbFrontEndProfiling.ResumeLayout(false);
            this.gbFrontEndProfiling.PerformLayout();
            this.grpTrailerDialog.ResumeLayout(false);
            this.grpTrailerDialog.PerformLayout();
            this.gbIntegration.ResumeLayout(false);
            this.gbIntegration.PerformLayout();
            this.tabDraftStamps.ResumeLayout(false);
            this.tabDraftStamps.PerformLayout();
            this.tabSync.ResumeLayout(false);
            this.grpDistributableCopy.ResumeLayout(false);
            this.grpDistributableCopy.PerformLayout();
            this.grpSynSchedule.ResumeLayout(false);
            this.grpSynSchedule.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udFreqInterval)).EndInit();
            this.grpConnectionParameters.ResumeLayout(false);
            this.grpConnectionParameters.PerformLayout();
            this.tabFeatures.ResumeLayout(false);
            this.grpAlerts.ResumeLayout(false);
            this.grpAlerts.PerformLayout();
            this.grpGeneralFeatures.ResumeLayout(false);
            this.grpGeneralFeatures.PerformLayout();
            this.grpAutomation.ResumeLayout(false);
            this.grpAutomation.PerformLayout();
            this.tabMailSetup.ResumeLayout(false);
            this.tabMailSetup.PerformLayout();
            this.grpMailLogin.ResumeLayout(false);
            this.grpMailLogin.PerformLayout();
            this.tabSecurity.ResumeLayout(false);
            this.tabSecurity.PerformLayout();
            this.tsProtectionPasswords.ResumeLayout(false);
            this.tsProtectionPasswords.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPasswords)).EndInit();
            this.tabOther.ResumeLayout(false);
            this.tabOther.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.grpCheckboxConfig.ResumeLayout(false);
            this.grpCheckboxConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTypes)).EndInit();
            this.tabDefaultUserSettings.ResumeLayout(false);
            this.grpAdditionalOptions.ResumeLayout(false);
            this.grpAdditionalOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spnDefaultZoomPercentage)).EndInit();
            this.grpTaskPane.ResumeLayout(false);
            this.grpTaskPane.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkDisplayTaskPaneForNonMPDoc;
        private System.Windows.Forms.CheckBox chkShowXMLTags;
        private System.Windows.Forms.Label lblEntity;
        private System.Windows.Forms.ComboBox cmbEntity;
        private System.Windows.Forms.RadioButton rdGroups;
        private System.Windows.Forms.RadioButton rdOffices;
        private System.Windows.Forms.Label lblOwnerCategory;
        private System.Windows.Forms.CheckBox chkDisplayTaskPaneForMPDoc;
        private System.Windows.Forms.CheckBox chkViewPgWidth;
        private System.Windows.Forms.CheckBox chkUseGradient;
        private System.Windows.Forms.Button btnPickColor;
        private System.Windows.Forms.Label lblTaskPaneBackgroundColor;
        private System.Windows.Forms.Panel pnlTaskPaneColorChip;
        private System.Windows.Forms.CheckBox chkShowQuickHelp;
        private System.Windows.Forms.GroupBox grpTaskPane;
        private System.Windows.Forms.GroupBox grpAdditionalOptions;
        private System.Windows.Forms.ColorDialog colorDlgBackColor;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabFirmSettings;
        private System.Windows.Forms.TabPage tabDefaultUserSettings;
        private System.Windows.Forms.Button btnResetUserToFirmDefaults;
        private System.Windows.Forms.CheckBox chkActivateMacPacRibbonTab;
        private System.Windows.Forms.CheckBox chkActivateMacPacRibbonTabOnOpen;
        private System.Windows.Forms.FolderBrowserDialog fbdDistributableFolder;
        private System.Windows.Forms.CheckBox chkShowTaskPaneOnBlankNew;
        private System.Windows.Forms.CheckBox chkWarnBeforeDeactivation;
        private System.Windows.Forms.TabControl tabFirmAppSettings;
        private System.Windows.Forms.TabPage tabDisplay;
        private System.Windows.Forms.GroupBox grpHelpFont;
        private LMP.Controls.Spinner numUpDnHelpFontSize;
        private System.Windows.Forms.TextBox txtHelpFontName;
        private System.Windows.Forms.Label lblHelpFontSize;
        private System.Windows.Forms.Label lblHelpFontName;
        private System.Windows.Forms.CheckBox chkExpandChildSegments;
        private System.Windows.Forms.GroupBox grpContentManager;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSharedFolderLabel;
        private System.Windows.Forms.Label lblSharedFoldersLabel;
        private System.Windows.Forms.Button btnSharedFoldersHelpText;
        private System.Windows.Forms.TextBox txtSharedFoldersHelpText;
        private System.Windows.Forms.Label lblSharedFoldersHelpText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMyFoldersLabel;
        private System.Windows.Forms.Label lblMyFoldersLabel;
        private System.Windows.Forms.Button btnMyFoldersHelpText;
        private System.Windows.Forms.TextBox txtMyFoldersHelpText;
        private System.Windows.Forms.Label lblMyFoldersHelpText;
        private System.Windows.Forms.Label lblPublicFolders;
        private System.Windows.Forms.TextBox txtPublicFoldersLabel;
        private System.Windows.Forms.Button btnPublicFoldersHelpText;
        private System.Windows.Forms.Label lblPublicFoldersHelpText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPublicFoldersHelpText;
        private System.Windows.Forms.GroupBox grpPeopleList;
        private System.Windows.Forms.NumericUpDown numUpDnUserContentThreshold;
        private System.Windows.Forms.NumericUpDown numUpDnUserContentLimit;
        private System.Windows.Forms.Label lblUserContentLimit;
        private System.Windows.Forms.Label lblUserContentThreshold;
        private System.Windows.Forms.NumericUpDown numUpDnMaxPeople;
        private System.Windows.Forms.Label lblMaxPeople;
        private System.Windows.Forms.NumericUpDown numUpDnPeopleListWarningThreshold;
        private System.Windows.Forms.Label lblPeopleListWarningThreshold;
        private System.Windows.Forms.TabPage tabTrailers;
        private System.Windows.Forms.ComboBox cmbDefaultTrailerBehavior;
        private System.Windows.Forms.GroupBox gbFrontEndProfiling;
        private System.Windows.Forms.CheckBox chkCloseOnCancel;
        private System.Windows.Forms.CheckBox chkUseFrontEndProfiling;
        private System.Windows.Forms.Label lblDefaultTrailerBehavior;
        private System.Windows.Forms.CheckBox chkSkipTrailerForTempFiles;
        private System.Windows.Forms.GroupBox grpTrailerDialog;
        private LMP.Controls.ComboBox cmbTrailerDef;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTrailers;
        private System.Windows.Forms.CheckedListBox lstTrailers;
        private System.Windows.Forms.CheckBox chkAllowUsersToSetTrailerBehavior;
        private System.Windows.Forms.Button btnDMSMessage;
        private System.Windows.Forms.GroupBox gbIntegration;
        private System.Windows.Forms.CheckBox chkFileExit;
        private System.Windows.Forms.CheckBox chkFilePrintDefault;
        private System.Windows.Forms.CheckBox chkFilePrint;
        private System.Windows.Forms.CheckBox chkFileCloseAll;
        private System.Windows.Forms.CheckBox chkFileClose;
        private System.Windows.Forms.CheckBox chkFileSaveAll;
        private System.Windows.Forms.CheckBox chkFileSaveAs;
        private System.Windows.Forms.CheckBox chkFileSave;
        private System.Windows.Forms.CheckBox chkFileOpen;
        private System.Windows.Forms.Label lblDMS;
        private LMP.Controls.ComboBox cmbDocManager;
        private System.Windows.Forms.TabPage tabDraftStamps;
        private System.Windows.Forms.Label lblDraftStamps;
        private System.Windows.Forms.CheckedListBox lstDraftStamps;
        private System.Windows.Forms.TabPage tabSync;
        private System.Windows.Forms.GroupBox grpDistributableCopy;
        private System.Windows.Forms.Button btnDistributableFolder;
        private System.Windows.Forms.TextBox txtDistributableFolder;
        private System.Windows.Forms.Label lblDistributableFolder;
        private System.Windows.Forms.GroupBox grpSynSchedule;
        private System.Windows.Forms.CheckBox chkLogUserSync;
        private System.Windows.Forms.ComboBox cmbDay;
        private System.Windows.Forms.Label lblDay;
        private LMP.Controls.ComboBox cmbSyncCompletePrompt;
        private System.Windows.Forms.Label lblSyncCompletePrompt;
        private LMP.Controls.ComboBox cmbSyncPrompt;
        private System.Windows.Forms.Label lblSyncPrompt;
        private System.Windows.Forms.NumericUpDown udFreqInterval;
        private System.Windows.Forms.Label lblCurSynScheduleDesc;
        private System.Windows.Forms.DateTimePicker dtTime;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.ComboBox cmbFreqUnit;
        private System.Windows.Forms.Label lblFreqInterval;
        private System.Windows.Forms.Label lblFreqUnit;
        private System.Windows.Forms.GroupBox grpConnectionParameters;
        private System.Windows.Forms.TextBox txtServerAppName;
        private System.Windows.Forms.Label lblServerAppName;
        private System.Windows.Forms.TextBox txtIISServerIP;
        private System.Windows.Forms.Label lblIISServerIP;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TabPage tabFeatures;
        private System.Windows.Forms.GroupBox grpAlerts;
        private System.Windows.Forms.CheckBox chkAlertWhenNonMP10DocumentOpened;
        private System.Windows.Forms.CheckBox chkAlertTagsRemoved;
        private System.Windows.Forms.GroupBox grpGeneralFeatures;
        private System.Windows.Forms.CheckBox chkClearUndoStack;
        private System.Windows.Forms.CheckBox chkShowSegmentSpecificRibbons;
        private System.Windows.Forms.CheckBox chkAllowSavedDataCreationOnDeautomation;
        private System.Windows.Forms.CheckBox chkAllowPrivatePeople;
        private System.Windows.Forms.CheckBox chkAllowProxying;
        private System.Windows.Forms.CheckBox chkAllowNormalStyleUpdates;
        private System.Windows.Forms.CheckBox chkAllowLegacyDocs;
        private System.Windows.Forms.CheckBox chkAllowSharedSegments;
        private System.Windows.Forms.GroupBox grpAutomation;
        private System.Windows.Forms.CheckBox chkRemoveTextSegmentTags;
        private System.Windows.Forms.CheckBox chkAllowDocumentMemberTagRemoval;
        private System.Windows.Forms.CheckBox chkAllowDocumentTagRemoval;
        private System.Windows.Forms.CheckBox chkAllowSegmentMemberTagRemoval;
        private System.Windows.Forms.CheckBox chkAllowVarBlockTagRemoval;
        private System.Windows.Forms.TabPage tabMailSetup;
        private System.Windows.Forms.GroupBox grpMailLogin;
        private System.Windows.Forms.CheckBox chkUseNetworkCredentials;
        private System.Windows.Forms.CheckBox chkUseSSL;
        private System.Windows.Forms.TextBox txtMailPWD;
        private System.Windows.Forms.Label lblMailUserID;
        private System.Windows.Forms.Label lblMailPWD;
        private System.Windows.Forms.TextBox txtMailUserID;
        private System.Windows.Forms.TextBox txtMailHost;
        private System.Windows.Forms.TextBox txtAdminEMail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblAdminEMail;
        private System.Windows.Forms.TabPage tabOther;
        private System.Windows.Forms.GroupBox grpCheckboxConfig;
        private LMP.Controls.FontList cmbCheckboxFont;
        private System.Windows.Forms.TextBox txtUncheckedCheckboxChar;
        private System.Windows.Forms.Label lblUncheckedCheckboxChar;
        private System.Windows.Forms.TextBox txtCheckedCheckboxChar;
        private System.Windows.Forms.Label lblCheckedCheckboxChar;
        private System.Windows.Forms.Label lblCheckboxFont;
        private System.Windows.Forms.CheckBox chkCloseTaskPaneOnFinish;
        private System.Windows.Forms.CheckBox chkUse9xStyleTrailer;
        private System.Windows.Forms.CheckBox chkFinishOnOpen;
        private System.Windows.Forms.CheckBox chkPromptForLegacyTrailer;
        private System.Windows.Forms.CheckBox chkAllowErrorEmails;
        private System.Windows.Forms.NumericUpDown numUpDnSavedDataThreshold;
        private System.Windows.Forms.NumericUpDown numUpDnSavedDataLimit;
        private System.Windows.Forms.Label lblSavedDataThreshold;
        private System.Windows.Forms.TextBox txtProtectionPassword;
        private System.Windows.Forms.Label lblProtectionPassword;
        private System.Windows.Forms.TextBox txtBlankWordDocTemplate;
        private System.Windows.Forms.Label lblBlankWordDocTemplate;
        private System.Windows.Forms.TabPage tabSecurity;
        private System.Windows.Forms.CheckBox chkBase64Encoding;
        private LMP.Controls.DataGridView grdPasswords;
        private System.Windows.Forms.ToolStrip tsProtectionPasswords;
        private System.Windows.Forms.ToolStripLabel tlblPassword;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tbtnPasswordNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbtnPasswordDelete;
        private LMP.Controls.DataGridView grdTypes;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tbtnNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Label lblDefaultLabelSegment;
        private System.Windows.Forms.ComboBox cmbDefaultLabelSegment;
        private System.Windows.Forms.ComboBox cmbDefaultEnvelopeSegment;
        private System.Windows.Forms.Label lblDefaultEnvelopeSegment;
        private System.Windows.Forms.CheckBox chkUpdateZoom;
        private System.Windows.Forms.Label lblDefaultZoomPercentage;
        private System.Windows.Forms.NumericUpDown spnDefaultZoomPercentage;
        private System.Windows.Forms.Label lblAdvancedAddressBookPhonePrefix;
        private System.Windows.Forms.TextBox txtAdvancedAddressBookPhonePrefix;
        private System.Windows.Forms.Label lblAdvancedAddressBookFormatPrefixes;
        private System.Windows.Forms.Label lblAdvancedAddressBookEmailPrefix;
        private System.Windows.Forms.Label lblAdvancedAddressBookFaxPrefix;
        private System.Windows.Forms.TextBox txtAdvancedAddressBookEmailPrefix;
        private System.Windows.Forms.TextBox txtAdvancedAddressBookFaxPrefix;
        private System.Windows.Forms.CheckBox chkWarnOnFinish;
        private System.Windows.Forms.TextBox txtMailPort;
        private System.Windows.Forms.Label lblMailPort;
        private System.Windows.Forms.CheckBox chkPromptAboutUnconvertedTablesInDesign;
        private System.Windows.Forms.CheckBox chkSetRemoveAllStampsAsFirstInList;
        private System.Windows.Forms.ToolTip ttButtons;
        private System.Windows.Forms.Button btnResetUserForAllEntities;
        private System.Windows.Forms.GroupBox gbResetEntity;
        private System.Windows.Forms.Button btnResetToFirmDefaults;
        private System.Windows.Forms.Button btnResetTab;
        private System.Windows.Forms.GroupBox gbResetAll;
        private System.Windows.Forms.Button btnResetAllEntitiesToFirmDefaults;
        private System.Windows.Forms.Button btnResetTabForAllEntities;
        private System.Windows.Forms.CheckBox chkExpandFindResults;
        private System.Windows.Forms.CheckBox chkFinishButtonAtEnd;
        private System.Windows.Forms.Button btnTemplateDefaults;
        private System.Windows.Forms.CheckBox chkShowTaskpaneWhenNoVariables;
    }
}
