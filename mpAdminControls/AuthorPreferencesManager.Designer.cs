namespace LMP.Administration.Controls
{
    partial class AuthorPreferencesManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthorPreferencesManager));
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDeleteExternalListItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolTipPrefLabel = new System.Windows.Forms.ToolTip(this.components);
            this.tsExternalLists = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.scAuthorPrefs = new System.Windows.Forms.SplitContainer();
            this.ftvSegments = new LMP.Controls.SearchableFolderTreeView();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.cmbLanguage = new LMP.Controls.ComboBox();
            this.cmbOwner = new LMP.Controls.ComboBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.rdPeople = new System.Windows.Forms.RadioButton();
            this.lblOwner = new System.Windows.Forms.Label();
            this.gbxPreferences = new System.Windows.Forms.GroupBox();
            this.pnlPreferences = new System.Windows.Forms.Panel();
            this.pnlRelated = new System.Windows.Forms.Panel();
            this.lblPrefSep = new System.Windows.Forms.Label();
            this.cmbRelated = new LMP.Controls.ComboBox();
            this.lblRelated = new System.Windows.Forms.Label();
            this.lblNoPreferences = new System.Windows.Forms.Label();
            this.rdGroups = new System.Windows.Forms.RadioButton();
            this.rdOffices = new System.Windows.Forms.RadioButton();
            this.lblOwnerCategory = new System.Windows.Forms.Label();
            this.tsExternalLists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAuthorPrefs)).BeginInit();
            this.scAuthorPrefs.Panel1.SuspendLayout();
            this.scAuthorPrefs.Panel2.SuspendLayout();
            this.scAuthorPrefs.SuspendLayout();
            this.gbxPreferences.SuspendLayout();
            this.pnlRelated.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Fuchsia;
            this.ilImages.Images.SetKeyName(0, "");
            this.ilImages.Images.SetKeyName(1, "");
            this.ilImages.Images.SetKeyName(2, "");
            this.ilImages.Images.SetKeyName(3, "");
            this.ilImages.Images.SetKeyName(4, "SavedData.bmpTransparent.bmp");
            this.ilImages.Images.SetKeyName(5, "ParagraphTextSegment");
            this.ilImages.Images.SetKeyName(6, "");
            this.ilImages.Images.SetKeyName(7, "");
            this.ilImages.Images.SetKeyName(8, "");
            this.ilImages.Images.SetKeyName(9, "bullet.bmpTransparent.bmp");
            this.ilImages.Images.SetKeyName(10, "DataInterview");
            this.ilImages.Images.SetKeyName(11, "DataPacket");
            this.ilImages.Images.SetKeyName(12, "SentenceTextSegment");
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.AutoSize = false;
            this.toolStripLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(100, 22);
            this.toolStripLabel1.Text = "External Lists";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDeleteExternalListItem
            // 
            this.btnDeleteExternalListItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDeleteExternalListItem.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteExternalListItem.Image")));
            this.btnDeleteExternalListItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteExternalListItem.Name = "btnDeleteExternalListItem";
            this.btnDeleteExternalListItem.Size = new System.Drawing.Size(54, 22);
            this.btnDeleteExternalListItem.Text = "&Delete...";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsExternalLists
            // 
            this.tsExternalLists.AutoSize = false;
            this.tsExternalLists.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsExternalLists.GripMargin = new System.Windows.Forms.Padding(0);
            this.tsExternalLists.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsExternalLists.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2});
            this.tsExternalLists.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsExternalLists.Location = new System.Drawing.Point(0, 0);
            this.tsExternalLists.Name = "tsExternalLists";
            this.tsExternalLists.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsExternalLists.Size = new System.Drawing.Size(736, 25);
            this.tsExternalLists.Stretch = true;
            this.tsExternalLists.TabIndex = 24;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(117, 22);
            this.toolStripLabel2.Text = "Author Preferences";
            // 
            // scAuthorPrefs
            // 
            this.scAuthorPrefs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scAuthorPrefs.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.scAuthorPrefs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scAuthorPrefs.Location = new System.Drawing.Point(0, 27);
            this.scAuthorPrefs.Name = "scAuthorPrefs";
            // 
            // scAuthorPrefs.Panel1
            // 
            this.scAuthorPrefs.Panel1.BackColor = System.Drawing.Color.White;
            this.scAuthorPrefs.Panel1.Controls.Add(this.ftvSegments);
            // 
            // scAuthorPrefs.Panel2
            // 
            this.scAuthorPrefs.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scAuthorPrefs.Panel2.Controls.Add(this.lblLanguage);
            this.scAuthorPrefs.Panel2.Controls.Add(this.cmbLanguage);
            this.scAuthorPrefs.Panel2.Controls.Add(this.cmbOwner);
            this.scAuthorPrefs.Panel2.Controls.Add(this.btnReset);
            this.scAuthorPrefs.Panel2.Controls.Add(this.btnSave);
            this.scAuthorPrefs.Panel2.Controls.Add(this.rdPeople);
            this.scAuthorPrefs.Panel2.Controls.Add(this.lblOwner);
            this.scAuthorPrefs.Panel2.Controls.Add(this.gbxPreferences);
            this.scAuthorPrefs.Panel2.Controls.Add(this.rdGroups);
            this.scAuthorPrefs.Panel2.Controls.Add(this.rdOffices);
            this.scAuthorPrefs.Panel2.Controls.Add(this.lblOwnerCategory);
            this.scAuthorPrefs.Size = new System.Drawing.Size(733, 452);
            this.scAuthorPrefs.SplitterDistance = 236;
            this.scAuthorPrefs.SplitterWidth = 2;
            this.scAuthorPrefs.TabIndex = 0;
            this.scAuthorPrefs.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.scAuthorPrefs_SplitterMoved);
            // 
            // ftvSegments
            // 
            this.ftvSegments.AllowFavoritesMenu = false;
            this.ftvSegments.AllowSearching = true;
            this.ftvSegments.AutoSize = true;
            this.ftvSegments.DisableExcludedTypes = false;
            this.ftvSegments.DisplayOptions = ((LMP.Controls.FolderTreeView.mpFolderTreeViewOptions)((((((LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserFolders | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.AdminFolders) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.Segments) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.UserSegments) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.StyleSheets) 
            | LMP.Controls.FolderTreeView.mpFolderTreeViewOptions.SegmentPackets)));
            this.ftvSegments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ftvSegments.ExcludeNonMacPacTypes = false;
            this.ftvSegments.Expansion = LMP.Controls.FolderTreeView.mpFolderTreeViewExpansion.ExpandAll;
            this.ftvSegments.Location = new System.Drawing.Point(0, 0);
            this.ftvSegments.Name = "ftvSegments";
            this.ftvSegments.OwnerID = 0;
            this.ftvSegments.ShowCheckboxes = false;
            this.ftvSegments.Size = new System.Drawing.Size(232, 448);
            this.ftvSegments.TabIndex = 0;
            this.ftvSegments.AfterSelect += new LMP.Controls.AfterSelectEventHandler(this.ftvSegments_AfterSelect);
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Enabled = false;
            this.lblLanguage.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLanguage.Location = new System.Drawing.Point(15, 98);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(58, 15);
            this.lblLanguage.TabIndex = 6;
            this.lblLanguage.Text = "&Language:";
            // 
            // cmbLanguage
            // 
            this.cmbLanguage.AllowEmptyValue = false;
            this.cmbLanguage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLanguage.AutoSize = true;
            this.cmbLanguage.Borderless = false;
            this.cmbLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbLanguage.Enabled = false;
            this.cmbLanguage.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLanguage.IsDirty = false;
            this.cmbLanguage.LimitToList = true;
            this.cmbLanguage.ListName = "";
            this.cmbLanguage.Location = new System.Drawing.Point(116, 96);
            this.cmbLanguage.MaxDropDownItems = 8;
            this.cmbLanguage.Name = "cmbLanguage";
            this.cmbLanguage.SelectedIndex = -1;
            this.cmbLanguage.SelectedValue = null;
            this.cmbLanguage.SelectionLength = 0;
            this.cmbLanguage.SelectionStart = 0;
            this.cmbLanguage.Size = new System.Drawing.Size(357, 24);
            this.cmbLanguage.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbLanguage.SupportingValues = "";
            this.cmbLanguage.TabIndex = 7;
            this.cmbLanguage.Tag2 = null;
            this.cmbLanguage.Value = "";
            this.cmbLanguage.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbLanguage_ValueChanged);
            // 
            // cmbOwner
            // 
            this.cmbOwner.AllowEmptyValue = false;
            this.cmbOwner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbOwner.AutoSize = true;
            this.cmbOwner.Borderless = false;
            this.cmbOwner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbOwner.Enabled = false;
            this.cmbOwner.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOwner.IsDirty = false;
            this.cmbOwner.LimitToList = true;
            this.cmbOwner.ListName = "";
            this.cmbOwner.Location = new System.Drawing.Point(116, 55);
            this.cmbOwner.MaxDropDownItems = 8;
            this.cmbOwner.Name = "cmbOwner";
            this.cmbOwner.SelectedIndex = -1;
            this.cmbOwner.SelectedValue = null;
            this.cmbOwner.SelectionLength = 0;
            this.cmbOwner.SelectionStart = 0;
            this.cmbOwner.Size = new System.Drawing.Size(357, 24);
            this.cmbOwner.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbOwner.SupportingValues = "";
            this.cmbOwner.TabIndex = 5;
            this.cmbOwner.Tag2 = null;
            this.cmbOwner.Value = "";
            this.cmbOwner.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbOwner_ValueChanged);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReset.AutoSize = true;
            this.btnReset.Enabled = false;
            this.btnReset.Location = new System.Drawing.Point(21, 400);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(73, 25);
            this.btnReset.TabIndex = 9;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.AutoSize = true;
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(102, 400);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(73, 25);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Sa&ve";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rdPeople
            // 
            this.rdPeople.AutoSize = true;
            this.rdPeople.Enabled = false;
            this.rdPeople.Location = new System.Drawing.Point(249, 21);
            this.rdPeople.Name = "rdPeople";
            this.rdPeople.Size = new System.Drawing.Size(58, 19);
            this.rdPeople.TabIndex = 3;
            this.rdPeople.Text = "&People";
            this.rdPeople.UseVisualStyleBackColor = true;
            this.rdPeople.Visible = false;
            this.rdPeople.CheckedChanged += new System.EventHandler(this.rdPeople_CheckedChanged);
            // 
            // lblOwner
            // 
            this.lblOwner.AutoSize = true;
            this.lblOwner.Enabled = false;
            this.lblOwner.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOwner.Location = new System.Drawing.Point(15, 58);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(45, 15);
            this.lblOwner.TabIndex = 4;
            this.lblOwner.Text = "&Entities:";
            // 
            // gbxPreferences
            // 
            this.gbxPreferences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxPreferences.AutoSize = true;
            this.gbxPreferences.Controls.Add(this.pnlPreferences);
            this.gbxPreferences.Controls.Add(this.pnlRelated);
            this.gbxPreferences.Controls.Add(this.lblNoPreferences);
            this.gbxPreferences.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbxPreferences.Location = new System.Drawing.Point(18, 138);
            this.gbxPreferences.Name = "gbxPreferences";
            this.gbxPreferences.Size = new System.Drawing.Size(457, 247);
            this.gbxPreferences.TabIndex = 8;
            this.gbxPreferences.TabStop = false;
            this.gbxPreferences.Text = "Preferences";
            // 
            // pnlPreferences
            // 
            this.pnlPreferences.AutoScroll = true;
            this.pnlPreferences.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPreferences.Location = new System.Drawing.Point(3, 68);
            this.pnlPreferences.Name = "pnlPreferences";
            this.pnlPreferences.Size = new System.Drawing.Size(451, 176);
            this.pnlPreferences.TabIndex = 10;
            // 
            // pnlRelated
            // 
            this.pnlRelated.Controls.Add(this.lblPrefSep);
            this.pnlRelated.Controls.Add(this.cmbRelated);
            this.pnlRelated.Controls.Add(this.lblRelated);
            this.pnlRelated.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRelated.Location = new System.Drawing.Point(3, 18);
            this.pnlRelated.Name = "pnlRelated";
            this.pnlRelated.Size = new System.Drawing.Size(451, 50);
            this.pnlRelated.TabIndex = 9;
            this.pnlRelated.VisibleChanged += new System.EventHandler(this.pnlRelated_VisibleChanged);
            // 
            // lblPrefSep
            // 
            this.lblPrefSep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrefSep.BackColor = System.Drawing.Color.Gainsboro;
            this.lblPrefSep.Location = new System.Drawing.Point(-1, 41);
            this.lblPrefSep.Name = "lblPrefSep";
            this.lblPrefSep.Size = new System.Drawing.Size(453, 2);
            this.lblPrefSep.TabIndex = 6;
            // 
            // cmbRelated
            // 
            this.cmbRelated.AllowEmptyValue = false;
            this.cmbRelated.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbRelated.AutoSize = true;
            this.cmbRelated.Borderless = false;
            this.cmbRelated.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbRelated.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRelated.IsDirty = false;
            this.cmbRelated.LimitToList = true;
            this.cmbRelated.ListName = "";
            this.cmbRelated.Location = new System.Drawing.Point(133, 5);
            this.cmbRelated.MaxDropDownItems = 8;
            this.cmbRelated.Name = "cmbRelated";
            this.cmbRelated.SelectedIndex = -1;
            this.cmbRelated.SelectedValue = null;
            this.cmbRelated.SelectionLength = 0;
            this.cmbRelated.SelectionStart = 0;
            this.cmbRelated.Size = new System.Drawing.Size(309, 24);
            this.cmbRelated.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbRelated.SupportingValues = "";
            this.cmbRelated.TabIndex = 5;
            this.cmbRelated.Tag2 = null;
            this.cmbRelated.Value = "";
            this.cmbRelated.DropDown += new LMP.Controls.DropDownHandler(this.cmbRelated_DropDown);
            this.cmbRelated.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbRelated_ValueChanged);
            // 
            // lblRelated
            // 
            this.lblRelated.AutoSize = true;
            this.lblRelated.Location = new System.Drawing.Point(4, 7);
            this.lblRelated.Name = "lblRelated";
            this.lblRelated.Size = new System.Drawing.Size(98, 15);
            this.lblRelated.TabIndex = 4;
            this.lblRelated.Text = "Related Segments:";
            // 
            // lblNoPreferences
            // 
            this.lblNoPreferences.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoPreferences.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoPreferences.Location = new System.Drawing.Point(3, 18);
            this.lblNoPreferences.Name = "lblNoPreferences";
            this.lblNoPreferences.Size = new System.Drawing.Size(451, 226);
            this.lblNoPreferences.TabIndex = 8;
            this.lblNoPreferences.Text = "There are no author preferences for the selected content.";
            this.lblNoPreferences.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNoPreferences.Visible = false;
            // 
            // rdGroups
            // 
            this.rdGroups.AutoSize = true;
            this.rdGroups.Enabled = false;
            this.rdGroups.Location = new System.Drawing.Point(176, 21);
            this.rdGroups.Name = "rdGroups";
            this.rdGroups.Size = new System.Drawing.Size(62, 19);
            this.rdGroups.TabIndex = 2;
            this.rdGroups.Text = "&Groups";
            this.rdGroups.UseVisualStyleBackColor = true;
            this.rdGroups.CheckedChanged += new System.EventHandler(this.rdGroups_CheckedChanged);
            // 
            // rdOffices
            // 
            this.rdOffices.AutoSize = true;
            this.rdOffices.Checked = true;
            this.rdOffices.Enabled = false;
            this.rdOffices.Location = new System.Drawing.Point(108, 21);
            this.rdOffices.Name = "rdOffices";
            this.rdOffices.Size = new System.Drawing.Size(60, 19);
            this.rdOffices.TabIndex = 1;
            this.rdOffices.TabStop = true;
            this.rdOffices.Text = "&Offices";
            this.rdOffices.UseVisualStyleBackColor = true;
            this.rdOffices.CheckedChanged += new System.EventHandler(this.rdOffices_CheckedChanged);
            // 
            // lblOwnerCategory
            // 
            this.lblOwnerCategory.AutoSize = true;
            this.lblOwnerCategory.Enabled = false;
            this.lblOwnerCategory.Location = new System.Drawing.Point(13, 23);
            this.lblOwnerCategory.Name = "lblOwnerCategory";
            this.lblOwnerCategory.Size = new System.Drawing.Size(75, 15);
            this.lblOwnerCategory.TabIndex = 0;
            this.lblOwnerCategory.Text = "Show Entities:";
            // 
            // AuthorPreferencesManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tsExternalLists);
            this.Controls.Add(this.scAuthorPrefs);
            this.Name = "AuthorPreferencesManager";
            this.Size = new System.Drawing.Size(736, 482);
            this.BeforeControlUnloaded += new LMP.Administration.Controls.BeforeControlUnLoadedHandler(this.AuthorPreferencesManager_BeforeControlUnloaded);
            this.Load += new System.EventHandler(this.AuthorPreferencesManager_Load);
            this.Leave += new System.EventHandler(this.AuthorPreferencesManager_Leave);
            this.tsExternalLists.ResumeLayout(false);
            this.tsExternalLists.PerformLayout();
            this.scAuthorPrefs.Panel1.ResumeLayout(false);
            this.scAuthorPrefs.Panel1.PerformLayout();
            this.scAuthorPrefs.Panel2.ResumeLayout(false);
            this.scAuthorPrefs.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAuthorPrefs)).EndInit();
            this.scAuthorPrefs.ResumeLayout(false);
            this.gbxPreferences.ResumeLayout(false);
            this.pnlRelated.ResumeLayout(false);
            this.pnlRelated.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer scAuthorPrefs;
        private System.Windows.Forms.Label lblOwnerCategory;
        private System.Windows.Forms.ImageList ilImages;
        private System.Windows.Forms.RadioButton rdOffices;
        private System.Windows.Forms.RadioButton rdGroups;
        private System.Windows.Forms.GroupBox gbxPreferences;
        private System.Windows.Forms.Label lblNoPreferences;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btnDeleteExternalListItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStrip tsExternalLists;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.RadioButton rdPeople;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolTip toolTipPrefLabel;
        private LMP.Controls.ComboBox cmbOwner;
        private LMP.Controls.ComboBox cmbLanguage;
        private System.Windows.Forms.Label lblLanguage;
        private LMP.Controls.SearchableFolderTreeView ftvSegments;
        private System.Windows.Forms.Panel pnlPreferences;
        private System.Windows.Forms.Panel pnlRelated;
        private LMP.Controls.ComboBox cmbRelated;
        private System.Windows.Forms.Label lblRelated;
        private System.Windows.Forms.Label lblPrefSep;
    }
}
