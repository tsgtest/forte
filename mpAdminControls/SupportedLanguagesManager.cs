using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class SupportedLanguagesManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ********************fields*********************
        DataTable m_oLanguagesDT;
        #endregion
        #region ********************enumerations******************
        #endregion
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region ********************constructors******************
        public SupportedLanguagesManager()
        {
            InitializeComponent();
        }
        #endregion
        #region ********************methods*********************
        /// <summary>
        /// Display all of the supported languages in the metadata table
        /// </summary>
        private void BindLanguagesGrid()
        {
            try
            {
                m_oLanguagesDT = LMP.Data.Application.SupportedLanguages;

                grdSupportedLanguages.DataSource = m_oLanguagesDT;

                //Set the display properties of the columns in the grid.
                DataGridViewColumnCollection oCols = grdSupportedLanguages.Columns;
                oCols["Display Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oCols["LCID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotBindDataToGrid"), oE);
            }

        }

        /// <summary>
        /// saves current languages to the database
        /// </summary>
        private void SaveLanguages()
        {
            m_oLanguagesDT.AcceptChanges();
            LMP.Data.Application.SupportedLanguages = m_oLanguagesDT;
        }
        /// <summary>
        /// deletes the selected record from the database
        /// </summary>
        private void DeleteLanguage()
        {
            if (grdSupportedLanguages.CurrentRow == null)
                return;

            //test to make sure this is not a new row
            if (!this.grdSupportedLanguages.CurrentRow.IsNewRow)
            {
                //prompt user for delete
                DialogResult oChoice = MessageBox.Show(
                        LMP.Resources.GetLangString("Message_DeleteEntry"),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oChoice == DialogResult.Yes)
                {
                    //delete record from grid
                    int iCurIndex = this.grdSupportedLanguages.CurrentRow.Index;
                    grdSupportedLanguages.Rows.Remove(grdSupportedLanguages.CurrentRow);
                    SaveLanguages();

                    //ensure selection - suspend binding so cell & row validation events won't fire
                    if (iCurIndex > 0)
                    {
                        grdSupportedLanguages.BindingContext[m_oLanguagesDT].SuspendBinding();
                        grdSupportedLanguages.CurrentCell =
                            grdSupportedLanguages.Rows[iCurIndex - 1].Cells[0];
                        grdSupportedLanguages.BindingContext[m_oLanguagesDT].ResumeBinding();
                    }
                }
            }
        }
        /// <summary>
        /// saves all data for the control - called
        /// when hosting form is closed
        /// </summary>
        public override void SaveCurrentRecord()
        {
            base.SaveCurrentRecord();
        }
        #endregion
        #region ********************event handlers*********************
        private void LocationsManager_Load(object sender, EventArgs e)
        {
            try
            {
                this.BindLanguagesGrid();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Tests for the delete key and focus-changing Alt+hotkey
        /// combos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdSupportedLanguages_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Test if the key pressed was the delete key
                //and call the DeleteLanguage method if it was.
                if (e.KeyCode == Keys.Delete)
                {
                    this.DeleteLanguage();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void grdSupportedLanguages_RowValidating(object sender, System.Windows.Forms.DataGridViewCellCancelEventArgs e)
        {
            //get current row
            DataGridViewRow oRow = ((DataGridView)sender).Rows[e.RowIndex];
            if (oRow.Cells[0].Value == null)
                return;
            bool bIsLastRow = (e.RowIndex == grdSupportedLanguages.Rows.Count - 1);
            string xDisplayName = oRow.Cells[0].Value.ToString();
            string xLCID = oRow.Cells[1].Value.ToString();

            if ((xDisplayName == "") && ((!bIsLastRow) || (xLCID != "")))
            {
                //display name is empty
                string xMsg = LMP.Resources.GetLangString("Error_FieldCantBeEmpty") +
                    "Display Name";
                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
                return;
            }

            if (xLCID == "")
            {
                if ((!bIsLastRow) || (xDisplayName != ""))
                {
                    //lcid is empty
                    string xMsg = LMP.Resources.GetLangString("Error_FieldCantBeEmpty") +
                        "LCID";
                    MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                    return;
                }
            }
            else if (!LMP.String.IsNumericInt32(xLCID))
            {
                //lcid is non-numeric
                string xMsg = LMP.Resources.GetLangString(
                    "Error_SupportedLanguagesManager_LCIDFormat");
                MessageBox.Show(xMsg, LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
                return;
            }
        }

        private void grdSupportedLanguages_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow oRow = ((DataGridView)sender).Rows[e.RowIndex];
                if (oRow.Cells[0].Value != null)
                {
                    if (this.m_oLanguagesDT.GetChanges() != null)
                    {
                        SaveLanguages();
                        this.m_oLanguagesDT.AcceptChanges();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// calls routine to delete selected record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdSupportedLanguages.CurrentRow != null)
                    DeleteLanguage();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void grdSupportedLanguages_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }


        }
    }
}

