namespace LMP.Administration.Controls
{
    partial class AddressFormatsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressFormatsManager));
            this.grdAddressFormats = new LMP.Controls.DataGridView();
            this.scInternalLists = new System.Windows.Forms.SplitContainer();
            this.grdProperties = new System.Windows.Forms.DataGridView();
            this.tlblAddressFormats = new System.Windows.Forms.ToolStripLabel();
            this.tsep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.tsep3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tbtnNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.grdAddressFormats)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scInternalLists)).BeginInit();
            this.scInternalLists.Panel1.SuspendLayout();
            this.scInternalLists.Panel2.SuspendLayout();
            this.scInternalLists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).BeginInit();
            this.tsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdAddressFormats
            // 
            this.grdAddressFormats.AllowUserToAddRows = false;
            this.grdAddressFormats.AllowUserToDeleteRows = false;
            this.grdAddressFormats.AllowUserToResizeColumns = false;
            this.grdAddressFormats.AllowUserToResizeRows = false;
            this.grdAddressFormats.BackgroundColor = System.Drawing.Color.White;
            this.grdAddressFormats.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdAddressFormats.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdAddressFormats.ColumnHeadersHeight = 21;
            this.grdAddressFormats.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdAddressFormats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAddressFormats.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.grdAddressFormats.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdAddressFormats.IsDirty = false;
            this.grdAddressFormats.Location = new System.Drawing.Point(0, 0);
            this.grdAddressFormats.MultiSelect = false;
            this.grdAddressFormats.Name = "grdAddressFormats";
            this.grdAddressFormats.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdAddressFormats.RowHeadersVisible = false;
            this.grdAddressFormats.RowHeadersWidth = 25;
            this.grdAddressFormats.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdAddressFormats.RowTemplate.Height = 20;
            this.grdAddressFormats.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdAddressFormats.Size = new System.Drawing.Size(196, 497);
            this.grdAddressFormats.SupportingValues = "";
            this.grdAddressFormats.TabIndex = 3;
            this.grdAddressFormats.Tag2 = null;
            this.grdAddressFormats.Value = null;
            this.grdAddressFormats.TabPressed += new LMP.Controls.TabPressedHandler(this.grdAddressFormats_TabPressed);
            this.grdAddressFormats.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdAddressFormats_CellValidating);
            this.grdAddressFormats.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdAddressFormats_ColumnAdded);
            this.grdAddressFormats.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdAddressFormats_KeyDown);
            // 
            // scInternalLists
            // 
            this.scInternalLists.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scInternalLists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scInternalLists.Location = new System.Drawing.Point(0, 25);
            this.scInternalLists.Name = "scInternalLists";
            // 
            // scInternalLists.Panel1
            // 
            this.scInternalLists.Panel1.Controls.Add(this.grdAddressFormats);
            // 
            // scInternalLists.Panel2
            // 
            this.scInternalLists.Panel2.Controls.Add(this.grdProperties);
            this.scInternalLists.Size = new System.Drawing.Size(603, 501);
            this.scInternalLists.SplitterDistance = 200;
            this.scInternalLists.TabIndex = 29;
            // 
            // grdProperties
            // 
            this.grdProperties.AllowUserToAddRows = false;
            this.grdProperties.AllowUserToDeleteRows = false;
            this.grdProperties.AllowUserToResizeColumns = false;
            this.grdProperties.AllowUserToResizeRows = false;
            this.grdProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProperties.BackgroundColor = System.Drawing.Color.White;
            this.grdProperties.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdProperties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProperties.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdProperties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdProperties.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grdProperties.Location = new System.Drawing.Point(3, 3);
            this.grdProperties.MultiSelect = false;
            this.grdProperties.Name = "grdProperties";
            this.grdProperties.RowHeadersVisible = false;
            this.grdProperties.RowTemplate.Height = 20;
            this.grdProperties.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdProperties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdProperties.Size = new System.Drawing.Size(391, 490);
            this.grdProperties.TabIndex = 38;
            this.grdProperties.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProperties_CellEndEdit);
            this.grdProperties.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProperties_CellEnter);
            // 
            // tlblAddressFormats
            // 
            this.tlblAddressFormats.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblAddressFormats.Name = "tlblAddressFormats";
            this.tlblAddressFormats.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.tlblAddressFormats.Size = new System.Drawing.Size(109, 22);
            this.tlblAddressFormats.Text = "Address Formats";
            this.tlblAddressFormats.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsep1
            // 
            this.tsep1.Name = "tsep1";
            this.tsep1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(53, 22);
            this.tbtnDelete.Text = "&Delete...";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // tsep3
            // 
            this.tsep3.Name = "tsep3";
            this.tsep3.Size = new System.Drawing.Size(6, 25);
            // 
            // tsMain
            // 
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblAddressFormats,
            this.tsep1,
            this.tbtnNew,
            this.toolStripSeparator1,
            this.tbtnDelete,
            this.tsep3});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMain.Size = new System.Drawing.Size(603, 25);
            this.tsMain.TabIndex = 28;
            // 
            // tbtnNew
            // 
            this.tbtnNew.AutoSize = false;
            this.tbtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNew.Image")));
            this.tbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNew.Name = "tbtnNew";
            this.tbtnNew.Size = new System.Drawing.Size(53, 22);
            this.tbtnNew.Text = "&New...";
            this.tbtnNew.Click += new System.EventHandler(this.tbtnNew_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // AddressFormatsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 15F);
            this.Controls.Add(this.scInternalLists);
            this.Controls.Add(this.tsMain);
            this.Name = "AddressFormatsManager";
            this.Size = new System.Drawing.Size(603, 526);
            this.Load += new System.EventHandler(this.AddressFormatsManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdAddressFormats)).EndInit();
            this.scInternalLists.Panel1.ResumeLayout(false);
            this.scInternalLists.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scInternalLists)).EndInit();
            this.scInternalLists.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).EndInit();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private LMP.Controls.DataGridView grdAddressFormats;
        private System.Windows.Forms.SplitContainer scInternalLists;
        private System.Windows.Forms.DataGridView grdProperties;
        private System.Windows.Forms.ToolStripLabel tlblAddressFormats;
        private System.Windows.Forms.ToolStripSeparator tsep1;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ToolStripSeparator tsep3;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton tbtnNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}
