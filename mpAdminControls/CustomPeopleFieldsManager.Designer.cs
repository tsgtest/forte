namespace LMP.Administration.Controls
{
    partial class CustomPeopleFieldsManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tsCustomFields = new System.Windows.Forms.ToolStrip();
            this.tlblCategory = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNewCustomFields = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDeleteCustomFields = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grdCustomFields = new System.Windows.Forms.DataGridView();
            this.tsCustomFields.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCustomFields)).BeginInit();
            this.SuspendLayout();
            // 
            // tsCustomFields
            // 
            this.tsCustomFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsCustomFields.AutoSize = false;
            this.tsCustomFields.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsCustomFields.Dock = System.Windows.Forms.DockStyle.None;
            this.tsCustomFields.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsCustomFields.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblCategory,
            this.toolStripSeparator3,
            this.tbtnNewCustomFields,
            this.toolStripSeparator4,
            this.tbtnDeleteCustomFields,
            this.toolStripSeparator1});
            this.tsCustomFields.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsCustomFields.Location = new System.Drawing.Point(2, 0);
            this.tsCustomFields.Name = "tsCustomFields";
            this.tsCustomFields.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsCustomFields.Size = new System.Drawing.Size(574, 29);
            this.tsCustomFields.TabIndex = 3;
            // 
            // tlblCategory
            // 
            this.tlblCategory.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblCategory.Name = "tlblCategory";
            this.tlblCategory.Size = new System.Drawing.Size(126, 26);
            this.tlblCategory.Text = "Custom People Fields";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnNewCustomFields
            // 
            this.tbtnNewCustomFields.AutoSize = false;
            this.tbtnNewCustomFields.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNewCustomFields.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNewCustomFields.Name = "tbtnNewCustomFields";
            this.tbtnNewCustomFields.Size = new System.Drawing.Size(75, 22);
            this.tbtnNewCustomFields.Text = "&New...";
            this.tbtnNewCustomFields.Click += new System.EventHandler(this.tbtnNewCustomFields_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnDeleteCustomFields
            // 
            this.tbtnDeleteCustomFields.AutoSize = false;
            this.tbtnDeleteCustomFields.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteCustomFields.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteCustomFields.Name = "tbtnDeleteCustomFields";
            this.tbtnDeleteCustomFields.Size = new System.Drawing.Size(75, 22);
            this.tbtnDeleteCustomFields.Text = "D&elete...";
            this.tbtnDeleteCustomFields.Click += new System.EventHandler(this.tbtnDeleteCustomFields_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            this.toolStripSeparator1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.grdCustomFields);
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 582);
            this.panel1.TabIndex = 27;
            // 
            // grdCustomFields
            // 
            this.grdCustomFields.AllowUserToAddRows = false;
            this.grdCustomFields.AllowUserToDeleteRows = false;
            this.grdCustomFields.AllowUserToResizeRows = false;
            this.grdCustomFields.BackgroundColor = System.Drawing.Color.White;
            this.grdCustomFields.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCustomFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCustomFields.ColumnHeadersVisible = false;
            this.grdCustomFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCustomFields.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdCustomFields.Location = new System.Drawing.Point(0, 0);
            this.grdCustomFields.MultiSelect = false;
            this.grdCustomFields.Name = "grdCustomFields";
            this.grdCustomFields.RowHeadersVisible = false;
            this.grdCustomFields.RowHeadersWidth = 25;
            this.grdCustomFields.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdCustomFields.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdCustomFields.Size = new System.Drawing.Size(574, 578);
            this.grdCustomFields.TabIndex = 1;
            this.grdCustomFields.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdCustomFields_CellBeginEdit);
            this.grdCustomFields.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdCustomFields_CellValidating);
            this.grdCustomFields.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdCustomFields_DataError);
            // 
            // CustomPeopleFieldsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tsCustomFields);
            this.Name = "CustomPeopleFieldsManager";
            this.Size = new System.Drawing.Size(579, 612);
            this.Load += new System.EventHandler(this.CustomPeopleFieldsManager_Load);
            this.tsCustomFields.ResumeLayout(false);
            this.tsCustomFields.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCustomFields)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsCustomFields;
        private System.Windows.Forms.ToolStripButton tbtnNewCustomFields;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tbtnDeleteCustomFields;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView grdCustomFields;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel tlblCategory;
    }
}
