namespace LMP.Administration.Controls
{
    partial class ListsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListsManager));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.scLists = new System.Windows.Forms.SplitContainer();
            this.scEnumLists = new System.Windows.Forms.SplitContainer();
            this.grdEnumLists = new LMP.Controls.DataGridView();
            this.ListName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SortOrder = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SetID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdListItems = new System.Windows.Forms.DataGridView();
            this.tsEnumLists = new System.Windows.Forms.ToolStrip();
            this.tlblEnumLists = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNewEnumList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDeleteEnumList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDeleteEnumListItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnTranslate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsQueriedLists = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNewQueriedList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDeleteQueriedList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.grdQueriedLists = new System.Windows.Forms.DataGridView();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConnectionString = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SQL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.scLists)).BeginInit();
            this.scLists.Panel1.SuspendLayout();
            this.scLists.Panel2.SuspendLayout();
            this.scLists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scEnumLists)).BeginInit();
            this.scEnumLists.Panel1.SuspendLayout();
            this.scEnumLists.Panel2.SuspendLayout();
            this.scEnumLists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEnumLists)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdListItems)).BeginInit();
            this.tsEnumLists.SuspendLayout();
            this.tsQueriedLists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdQueriedLists)).BeginInit();
            this.SuspendLayout();
            // 
            // scLists
            // 
            this.scLists.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.scLists.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scLists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scLists.Location = new System.Drawing.Point(0, 0);
            this.scLists.Name = "scLists";
            this.scLists.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scLists.Panel1
            // 
            this.scLists.Panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scLists.Panel1.Controls.Add(this.scEnumLists);
            this.scLists.Panel1.Controls.Add(this.tsEnumLists);
            // 
            // scLists.Panel2
            // 
            this.scLists.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scLists.Panel2.Controls.Add(this.tsQueriedLists);
            this.scLists.Panel2.Controls.Add(this.grdQueriedLists);
            this.scLists.Size = new System.Drawing.Size(737, 565);
            this.scLists.SplitterDistance = 431;
            this.scLists.SplitterWidth = 5;
            this.scLists.TabIndex = 0;
            // 
            // scEnumLists
            // 
            this.scEnumLists.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scEnumLists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scEnumLists.Location = new System.Drawing.Point(0, 25);
            this.scEnumLists.Name = "scEnumLists";
            // 
            // scEnumLists.Panel1
            // 
            this.scEnumLists.Panel1.Controls.Add(this.grdEnumLists);
            // 
            // scEnumLists.Panel2
            // 
            this.scEnumLists.Panel2.Controls.Add(this.grdListItems);
            this.scEnumLists.Size = new System.Drawing.Size(737, 406);
            this.scEnumLists.SplitterDistance = 245;
            this.scEnumLists.SplitterWidth = 1;
            this.scEnumLists.TabIndex = 25;
            // 
            // grdEnumLists
            // 
            this.grdEnumLists.AllowUserToAddRows = false;
            this.grdEnumLists.AllowUserToDeleteRows = false;
            this.grdEnumLists.AllowUserToResizeColumns = false;
            this.grdEnumLists.AllowUserToResizeRows = false;
            this.grdEnumLists.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grdEnumLists.BackgroundColor = System.Drawing.Color.White;
            this.grdEnumLists.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdEnumLists.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdEnumLists.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEnumLists.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ListName,
            this.SortOrder,
            this.ID,
            this.SetID});
            this.grdEnumLists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdEnumLists.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.grdEnumLists.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdEnumLists.IsDirty = false;
            this.grdEnumLists.Location = new System.Drawing.Point(0, 0);
            this.grdEnumLists.MultiSelect = false;
            this.grdEnumLists.Name = "grdEnumLists";
            this.grdEnumLists.RowHeadersVisible = false;
            this.grdEnumLists.RowHeadersWidth = 25;
            this.grdEnumLists.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdEnumLists.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdEnumLists.Size = new System.Drawing.Size(241, 402);
            this.grdEnumLists.SupportingValues = "";
            this.grdEnumLists.TabIndex = 3;
            this.grdEnumLists.Tag2 = null;
            this.grdEnumLists.Value = null;
            this.grdEnumLists.TabPressed += new LMP.Controls.TabPressedHandler(this.grdEnumLists_TabPressed);
            this.grdEnumLists.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdEnumLists_CurrentCellDirtyStateChanged);
            this.grdEnumLists.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdEnumLists_DataError);
            this.grdEnumLists.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdEnumLists_RowValidating);
            this.grdEnumLists.SelectionChanged += new System.EventHandler(this.grdEnumLists_SelectionChanged);
            this.grdEnumLists.Leave += new System.EventHandler(this.grdEnumLists_Leave);
            // 
            // ListName
            // 
            this.ListName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ListName.HeaderText = "Name";
            this.ListName.Name = "ListName";
            this.ListName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ListName.Visible = false;
            // 
            // SortOrder
            // 
            this.SortOrder.HeaderText = "Sort Order";
            this.SortOrder.Name = "SortOrder";
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // SetID
            // 
            this.SetID.HeaderText = "Set ID";
            this.SetID.Name = "SetID";
            this.SetID.Visible = false;
            // 
            // grdListItems
            // 
            this.grdListItems.AllowUserToResizeColumns = false;
            this.grdListItems.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdListItems.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.grdListItems.BackgroundColor = System.Drawing.Color.White;
            this.grdListItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdListItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grdListItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdListItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdListItems.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdListItems.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdListItems.Location = new System.Drawing.Point(0, 0);
            this.grdListItems.MultiSelect = false;
            this.grdListItems.Name = "grdListItems";
            this.grdListItems.RowHeadersVisible = false;
            this.grdListItems.RowHeadersWidth = 25;
            this.grdListItems.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdListItems.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdListItems.Size = new System.Drawing.Size(487, 402);
            this.grdListItems.TabIndex = 4;
            this.grdListItems.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdListItems_ColumnHeaderMouseClick);
            this.grdListItems.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdListItems_CurrentCellDirtyStateChanged);
            this.grdListItems.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdListItems_RowEnter);
            this.grdListItems.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdListItems_RowValidating);
            // 
            // tsEnumLists
            // 
            this.tsEnumLists.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsEnumLists.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsEnumLists.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblEnumLists,
            this.toolStripSeparator3,
            this.tbtnNewEnumList,
            this.toolStripSeparator7,
            this.tbtnDeleteEnumList,
            this.toolStripSeparator2,
            this.tbtnDeleteEnumListItem,
            this.toolStripSeparator6,
            this.tbtnTranslate,
            this.toolStripSeparator5});
            this.tsEnumLists.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsEnumLists.Location = new System.Drawing.Point(0, 0);
            this.tsEnumLists.Name = "tsEnumLists";
            this.tsEnumLists.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsEnumLists.Size = new System.Drawing.Size(737, 25);
            this.tsEnumLists.TabIndex = 24;
            // 
            // tlblEnumLists
            // 
            this.tlblEnumLists.AutoSize = false;
            this.tlblEnumLists.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblEnumLists.Name = "tlblEnumLists";
            this.tlblEnumLists.Size = new System.Drawing.Size(100, 22);
            this.tlblEnumLists.Text = "Enumerated Lists";
            this.tlblEnumLists.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnNewEnumList
            // 
            this.tbtnNewEnumList.AutoSize = false;
            this.tbtnNewEnumList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNewEnumList.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNewEnumList.Image")));
            this.tbtnNewEnumList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNewEnumList.Name = "tbtnNewEnumList";
            this.tbtnNewEnumList.Size = new System.Drawing.Size(101, 22);
            this.tbtnNewEnumList.Text = "&New List...";
            this.tbtnNewEnumList.Click += new System.EventHandler(this.tbtnNewEnumList_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnDeleteEnumList
            // 
            this.tbtnDeleteEnumList.AutoSize = false;
            this.tbtnDeleteEnumList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteEnumList.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeleteEnumList.Image")));
            this.tbtnDeleteEnumList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteEnumList.Name = "tbtnDeleteEnumList";
            this.tbtnDeleteEnumList.Size = new System.Drawing.Size(101, 22);
            this.tbtnDeleteEnumList.Text = "&Delete List...";
            this.tbtnDeleteEnumList.Click += new System.EventHandler(this.tbtnDeleteEnumList_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnDeleteEnumListItem
            // 
            this.tbtnDeleteEnumListItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteEnumListItem.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeleteEnumListItem.Image")));
            this.tbtnDeleteEnumListItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteEnumListItem.Name = "tbtnDeleteEnumListItem";
            this.tbtnDeleteEnumListItem.Size = new System.Drawing.Size(101, 22);
            this.tbtnDeleteEnumListItem.Text = "Delete &List Item...";
            this.tbtnDeleteEnumListItem.Click += new System.EventHandler(this.tbtnDeleteEnumListItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnTranslate
            // 
            this.tbtnTranslate.AutoSize = false;
            this.tbtnTranslate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnTranslate.Image = ((System.Drawing.Image)(resources.GetObject("tbtnTranslate.Image")));
            this.tbtnTranslate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnTranslate.Name = "tbtnTranslate";
            this.tbtnTranslate.Size = new System.Drawing.Size(101, 22);
            this.tbtnTranslate.Text = "&Translate...";
            this.tbtnTranslate.Click += new System.EventHandler(this.tbtnTranslate_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // tsQueriedLists
            // 
            this.tsQueriedLists.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsQueriedLists.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsQueriedLists.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator8,
            this.tbtnNewQueriedList,
            this.toolStripSeparator4,
            this.tbtnDeleteQueriedList,
            this.toolStripSeparator1});
            this.tsQueriedLists.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsQueriedLists.Location = new System.Drawing.Point(0, 0);
            this.tsQueriedLists.Name = "tsQueriedLists";
            this.tsQueriedLists.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsQueriedLists.Size = new System.Drawing.Size(733, 25);
            this.tsQueriedLists.TabIndex = 25;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.AutoSize = false;
            this.toolStripLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(100, 22);
            this.toolStripLabel1.Text = "Queried Lists";
            this.toolStripLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnNewQueriedList
            // 
            this.tbtnNewQueriedList.AutoSize = false;
            this.tbtnNewQueriedList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNewQueriedList.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNewQueriedList.Image")));
            this.tbtnNewQueriedList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNewQueriedList.Name = "tbtnNewQueriedList";
            this.tbtnNewQueriedList.Size = new System.Drawing.Size(53, 22);
            this.tbtnNewQueriedList.Text = "&New...";
            this.tbtnNewQueriedList.Click += new System.EventHandler(this.tbtnNewQueriedList_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnDeleteQueriedList
            // 
            this.tbtnDeleteQueriedList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteQueriedList.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeleteQueriedList.Image")));
            this.tbtnDeleteQueriedList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteQueriedList.Name = "tbtnDeleteQueriedList";
            this.tbtnDeleteQueriedList.Size = new System.Drawing.Size(53, 22);
            this.tbtnDeleteQueriedList.Text = "&Delete...";
            this.tbtnDeleteQueriedList.Click += new System.EventHandler(this.tbtnDeleteQueriedList_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // grdQueriedLists
            // 
            this.grdQueriedLists.AllowUserToAddRows = false;
            this.grdQueriedLists.AllowUserToResizeRows = false;
            this.grdQueriedLists.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdQueriedLists.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grdQueriedLists.BackgroundColor = System.Drawing.Color.White;
            this.grdQueriedLists.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdQueriedLists.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdQueriedLists.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdQueriedLists.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GroupName,
            this.ConnectionString,
            this.SQL});
            this.grdQueriedLists.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdQueriedLists.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdQueriedLists.Location = new System.Drawing.Point(0, 28);
            this.grdQueriedLists.MultiSelect = false;
            this.grdQueriedLists.Name = "grdQueriedLists";
            this.grdQueriedLists.RowHeadersVisible = false;
            this.grdQueriedLists.RowHeadersWidth = 25;
            this.grdQueriedLists.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdQueriedLists.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdQueriedLists.Size = new System.Drawing.Size(734, 87);
            this.grdQueriedLists.TabIndex = 1;
            this.grdQueriedLists.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdQueriedLists_ColumnAdded);
            this.grdQueriedLists.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdQueriedLists_CurrentCellDirtyStateChanged);
            this.grdQueriedLists.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdQueriedLists_DataError);
            this.grdQueriedLists.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdQueriedLists_RowValidating);
            this.grdQueriedLists.Leave += new System.EventHandler(this.grdQueriedLists_Leave);
            // 
            // GroupName
            // 
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.GroupName.DefaultCellStyle = dataGridViewCellStyle4;
            this.GroupName.HeaderText = "Name";
            this.GroupName.Name = "GroupName";
            this.GroupName.Width = 140;
            // 
            // ConnectionString
            // 
            this.ConnectionString.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.ConnectionString.DefaultCellStyle = dataGridViewCellStyle5;
            this.ConnectionString.FillWeight = 200F;
            this.ConnectionString.HeaderText = "Connection String";
            this.ConnectionString.Name = "ConnectionString";
            // 
            // SQL
            // 
            this.SQL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.SQL.DefaultCellStyle = dataGridViewCellStyle6;
            this.SQL.FillWeight = 200F;
            this.SQL.HeaderText = "SQL Statement";
            this.SQL.Name = "SQL";
            // 
            // ListsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.scLists);
            this.Name = "ListsManager";
            this.Size = new System.Drawing.Size(737, 565);
            this.scLists.Panel1.ResumeLayout(false);
            this.scLists.Panel1.PerformLayout();
            this.scLists.Panel2.ResumeLayout(false);
            this.scLists.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scLists)).EndInit();
            this.scLists.ResumeLayout(false);
            this.scEnumLists.Panel1.ResumeLayout(false);
            this.scEnumLists.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scEnumLists)).EndInit();
            this.scEnumLists.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdEnumLists)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdListItems)).EndInit();
            this.tsEnumLists.ResumeLayout(false);
            this.tsEnumLists.PerformLayout();
            this.tsQueriedLists.ResumeLayout(false);
            this.tsQueriedLists.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdQueriedLists)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.SplitContainer scLists;
        private System.Windows.Forms.DataGridView grdQueriedLists;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConnectionString;
        private System.Windows.Forms.DataGridViewTextBoxColumn SQL;
        private System.Windows.Forms.SplitContainer scEnumLists;
        private LMP.Controls.DataGridView grdEnumLists;
        //private System.Windows.Forms.DataGridView grdEnumLists;
        private System.Windows.Forms.DataGridViewTextBoxColumn ListName;
        private System.Windows.Forms.DataGridViewComboBoxColumn SortOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SetID;
        private System.Windows.Forms.DataGridView grdListItems;
        private System.Windows.Forms.ToolStrip tsEnumLists;
        private System.Windows.Forms.ToolStripLabel tlblEnumLists;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbtnNewEnumList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton tbtnDeleteEnumList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tbtnDeleteEnumListItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tbtnTranslate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStrip tsQueriedLists;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tbtnDeleteQueriedList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton tbtnNewQueriedList;
    }
}
