using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using AD = System.DirectoryServices;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class GroupsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region *********************fields*********************
        private PeopleGroups m_oGroups;
        private BindingSource m_oGroupSource;
        private DataRow m_oLastGroupRow = null;
        private const string DEF_GROUPNAME = "New Group";
        private Timer m_oSearchTimer = new Timer();
        private string m_xSearchString = "";
        #endregion
        #region *********************constructors*********************
        public GroupsManager()
        {
            //subscribe to base events
            base.AfterControlLoaded += new AfterControlLoadedHandler(AdminManager_AfterControlLoaded);
            base.BeforeControlUnloaded += new BeforeControlUnLoadedHandler(AdminManager_BeforeControlUnloaded);

            InitializeComponent();
        }
        #endregion
        #region *********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region *********************methods*********************

        void grdGroups_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                BindingSource oBS = (BindingSource)this.grdGroups.DataSource;
                DataTable oDT = (DataTable)oBS.DataSource;
                string xNameColValue = grdGroups.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
                DataRow[] oRows = oDT.Select("Name = '" + xNameColValue + "'");
                this.IsValid = (oRows.Length == 0);

                if (!this.IsValid)
                {
                    //generate validation message
                    MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateNameValueNotAllowed"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.grdGroups.CurrentCell = this.grdGroups.Rows[e.RowIndex].Cells[1];
                }
            }
        }


        /// <summary>
        /// Get and bind the datasource for grdGroups.
        /// </summary>
        private void DataBindGridGroups(bool bSortByID)
        {
            try
            {
                //assign a new PeopleGroups object to field
                this.m_oGroups = new PeopleGroups();

                //create a binding source
                m_oGroupSource = new BindingSource();
                
                //Get a DataSet from the new PeopleGroups object
                //representative of the PeopleGroups table of the 
                //database.
                DataSet oPeopleGroupsDataSet = m_oGroups.ToDataSet();
                DataTable oDT = oPeopleGroupsDataSet.Tables[0];
                
                //Bind the first (and only) table of the DataSet
                //to the binding source.
                m_oGroupSource.DataSource = oDT;

                //subscribe to datarow position changed event
                m_oGroupSource.PositionChanged += new EventHandler(m_oGroupSource_PositionChanged); 
                
                //bind the grid to the binding source
                grdGroups.DataSource = m_oGroupSource;

                //Set the display properties of the columns in 
                //grdGroups.
                DataGridViewColumnCollection oCols = this.grdGroups.Columns;

                oCols["ID"].Visible = false;
                oCols["TranslationID"].Visible = false;
                oCols["CIGroup"].Visible = false;
                oCols["LastEditTime"].Visible = false;
                oCols["Visible"].DisplayIndex = 0;
                oCols["Visible"].Width = 0;
                oCols["Visible"].Visible = false;
                oCols["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oCols["Name"].SortMode = DataGridViewColumnSortMode.NotSortable;
                oCols["Name"].DisplayIndex = 1;
                oCols["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oCols["Description"].SortMode = DataGridViewColumnSortMode.NotSortable;
                oCols["Description"].DisplayIndex = 2;

                //Set the default visible value of the new group 
                //row to true.
                oCols["Visible"].DefaultCellStyle.NullValue = true;

                if (bSortByID)
                    grdGroups.Sort(grdGroups.Columns["ID"], ListSortDirection.Descending);

                //GLOG 4495: DataGridView.FindRecord only works with sorted data,
                //so locate built-in group records using alternate method
                SetReadOnlyRows();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.
                            GetLangString("Error_CouldNotDataBindPeopleGroups"),oE);
            }
        }
        /// <summary>
        /// handles updates to Groups collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oGroupSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //get current BindingSource datarow object
                BindingSource oBS = (BindingSource)sender;

                //binding source has no rows - return;
                if (oBS.Count == 0)
                {
                    m_oLastGroupRow = null;
                    return;
                }

                if (m_oLastGroupRow == null)
                    m_oLastGroupRow = ((DataRowView)oBS.Current).Row;

                if (grdGroups.CurrentRow == null)
                    return;

                //update group properties if necessary
                if (m_oLastGroupRow.RowState == DataRowState.Modified)
                {
                    this.UpdateGroups(false);
                }
                else if (m_oLastGroupRow.RowState == DataRowState.Added)
                {
                    this.UpdateGroups(true); 
                }

                //reset last row variable
                if (oBS.Current != null)
                    m_oLastGroupRow = ((DataRowView)oBS.Current).Row;
                
                 //rebind properties grid
                if (oBS.Position > -1 && m_oGroups.Count > 0)
                {
                    //bind the group members grid
                    DataBindGridMembers(GetCurrentGroupID());
                }
           }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        ///saves the current row - returns true if successful, else false
        /// </summary>
        private void UpdateGroups(bool bAddNew)
        {
            //don't save new group if entry has not validated
            if (!this.IsValid)
                return;
            
            PeopleGroup oGroup;

            //get current row
            DataGridViewRow oRow = grdGroups.CurrentRow;

            if (bAddNew)
            {
                //this is a new row - create a new group
                oGroup = (PeopleGroup)m_oGroups.Create();

                //assign a new ID to the new group
                int iID = m_oGroups.GetLastID() - 1;

                oGroup.ID = iID;
                oGroup.Name = GetNewGroupName(iID);
                oGroup.IsVisible = true;
            }
            else
            {
                //get existing group
                oGroup = (PeopleGroup)m_oGroups.ItemFromID((int)oRow.Cells["ID"].Value);
                 //set group properties
                oGroup.Description = (string)oRow.Cells["Description"].EditedFormattedValue;
                oGroup.Name = (string)oRow.Cells["Name"].EditedFormattedValue;
                oGroup.IsVisible = (bool)oRow.Cells["Visible"].EditedFormattedValue;
            }
            try
            {
                //save group
                m_oGroups.Save(oGroup);

                if (bAddNew)
                {
                    DataBindGridGroups(true);
                    grdGroups.CurrentCell = grdGroups.Rows[grdGroups.Rows.Count - 1].Cells["Name"];
                }
                //accept changes - this will reset row state for edits
                if (m_oLastGroupRow != null)
                    m_oLastGroupRow.AcceptChanges();       
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// Delete a PeopleGroup record from the database corresponding to 
        /// the record selected by the user.
        /// </summary>
        private void DeleteGroupMembers()
        {
            //do not attempt to add if no group is selected
            if (grdGroups.CurrentRow == null || grdGroups.CurrentRow.IsNewRow)
                return;
            
            int iID = GetCurrentGroupID();

            //do nothing if groups grid is empty
            if (iID == -1)
            {
                //Remove the row from the group DataGridView.
                if (grdGroups.Rows.Count > 1)
                    this.grdGroups.Rows.RemoveAt(this.grdGroups.CurrentRow.Index);
                
                //reset validity state
                this.IsValid = true;

                return;
            }
            else if (iID == LMP.Data.PeopleGroups.ADMIN_GROUP || 
                iID == LMP.Data.PeopleGroups.USER_DESIGNER_GROUP)
            {
                //prevent deletion of admin and power user groups
                MessageBox.Show(LMP.Resources.GetLangString("Error_CantDeleteBuiltInGroup"),
                    LMP.String.MacPacProductName, MessageBoxButtons.OK, 
                    MessageBoxIcon.Exclamation);

                return;
            }

            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Message_DeleteEntry"),
                LMP.String.MacPacProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                //This outer try/catch is because of the error in PeopleGroup.Delete.
                //it should be removed when this is fixed.
                try
                {
                    //Delete the PeopleGroup record corresponding
                    //to the current ID from the database.
                    try
                    { m_oGroups.Delete(iID); }
                    //TODO: an error is raised here because spObjectAssignmentsDeleteByObjectIDs
                    //is not executing properly - please fix
                    catch(System.Exception oE)
                    {
                        throw new LMP.Exceptions.DataException(LMP.Resources.
                                GetLangString("Error_CouldNotSavePeopleGroup"), oE);
                    }
                }
                catch { }
                //Remove the row from the group DataGridView.
                this.grdGroups.Rows.RemoveAt(this.grdGroups.CurrentRow.Index);

                //rebind members grid
                DataBindGridMembers(GetCurrentGroupID());

                //select new name cell
                if (grdGroups.CurrentRow != null)
                    grdGroups.CurrentRow.Cells["Name"].Selected = true;
                
                //reset validity state
                this.IsValid = true;
            }
        }
        /// <summary>
        /// Display members of the seleted group in grdMembers.
        /// </summary>
        private void DataBindGridMembers(int iCurrentPeopleGroupID)
        {
            try
            {
                if (iCurrentPeopleGroupID == -1)
                {
                    //User entered the row for new records-
                    //clear the data in grdProperties
                    if (grdMembers.DataSource != null)
                        ((DataTable)grdMembers.DataSource).Clear();
                    return;
                }
                
                PeopleGroup oCurrentPeopleGroup =
                        (PeopleGroup)m_oGroups.ItemFromID(iCurrentPeopleGroupID);

                //Get a DataSet from the current PeopleGroup and 
                //bind it to the curren DataGridView binding source.
                DataSet oGroupMembersDS = oCurrentPeopleGroup.ToMemberDataSet();
                grdMembers.DataSource = oGroupMembersDS.Tables[0];

                //Set the display properties of the columns in the
                //DataGridView
                DataGridViewColumnCollection oCols  = this.grdMembers.Columns;
                oCols["ID"].Visible = false;
                oCols["DisplayName"].HeaderText = "Name";
                oCols["DisplayName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oCols["DisplayName"].SortMode = DataGridViewColumnSortMode.NotSortable;

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.
                        GetLangString("Error_CouldNotDisplayGroupMembers"), oE);
            }
        }
        /// <summary>
        /// Delete selected person from selected group.
        /// </summary>
        private void DeletePersonFromGroup()
        {
            //do not attempt to add if no group is selected
            if (grdGroups.CurrentRow == null)
                return;
            
            //exit if no selection exists in members 
            if (grdMembers.CurrentRow == null)
                return;

            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                DataGridViewRow oCurrentMemberRow = this.grdMembers.CurrentRow;

                //Get the current PeopleGroup.
                int iCurrentPeopleGroupID;
                iCurrentPeopleGroupID = (int)this.grdGroups.CurrentRow.Cells["ID"].Value;
                PeopleGroup oCurrentPeopleGroup =
                        (PeopleGroup)m_oGroups.ItemFromID(iCurrentPeopleGroupID);


                //Delete the group member by ID.
                oCurrentPeopleGroup.DeleteMember((int)oCurrentMemberRow.Cells["ID"].Value);

                //Remove the row corresponding to the deleted
                //member from the Members DataGridView.
                this.grdMembers.Rows.RemoveAt(oCurrentMemberRow.Index);

                if (grdMembers.CurrentRow != null)
                    grdMembers.CurrentRow.Selected = true; 
            }
        }
        /// <summary>
        /// Add selected people to selected gorup.
        /// </summary>
        /// <param name="xPeopleIDsToAdd">A comma delimited string of Person IDs.</param>
        private void AddPeopleToGroup(string xPeopleIDsToAdd)
        {
            //Test if the array parameter is null
            if (xPeopleIDsToAdd != null)
            {
                //If the array is not null, add the relevant people
                //to the currently selected PeopleGroup.
                int iCurrentPeopleGroupID = (int)this.grdGroups.CurrentRow.Cells["ID"].Value;
                PeopleGroup oCurrentPeopleGroup =
                        (PeopleGroup)m_oGroups.ItemFromID(iCurrentPeopleGroupID);
                try
                {
                    //Add relevant people using a comma-delimited string
                    //of IDs.
                    oCurrentPeopleGroup.AddMembers(xPeopleIDsToAdd);
                }
                catch
                {
                    //Warn the user if they are trying to add members to a group
                    //whos current data in the grid is invalid- this will only happen
                    //if the user has not already validated the row.
                    MessageBox.Show(
                            LMP.Resources.GetLangString("Error_PersonIsAlreadyGroupMember"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                //Refresh the displayed list of group members to
                //reflect additions.
                this.DataBindGridMembers(GetCurrentGroupID()); 
            }

        }
        #endregion
        #region *********************private members*********************
        #endregion
        #region *********************private functions*******************
        private bool ValidateGroupName(string xName, bool bSuppressMessage)
        {
            string xMessage = "";
            bool bReturn = true;

            //check if name field is null
            if (System.String.IsNullOrEmpty(xName))
            {
                //message user - name field cannot be empty
                xMessage = "Error_NameFieldCannotBeNull";
                bReturn = false;
            }
            
            //check for duplicate group name
            DataTable oDT = (DataTable)m_oGroupSource.DataSource; 
            DataRow[] oRows = oDT.Select("Name =" + "'" + xName + "'");

            if (oRows.Length > 1)
            {
                //message user - duplicate names not allowed
                xMessage = "Error_DuplicateNameValueNotAllowed";
                bReturn = false;
            }

            if (!System.String.IsNullOrEmpty(xMessage) && !bSuppressMessage)
            {
                //generate validation message
                MessageBox.Show(LMP.Resources.GetLangString(xMessage),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return bReturn;
        }
        /// <summary>
        /// finds the row matching the find text
        /// </summary>
        private void FindMatchingRow(string xSearchText, LMP.Controls.DataGridView oGrid, string xColumnName)
        {
            //note that iMatchIndex == -1 means that no match was found.
            int iMatchIndex = 0;

            if (xSearchText != "")
            {
                //User has entered a string to search for

                //Get the index of the first instance of the 
                //search string the user entered.
                iMatchIndex = oGrid.FindRecord(xSearchText, xColumnName, false);

                if (iMatchIndex > -1)
                    //Match found- select appropriate record.
                    oGrid.CurrentCell = oGrid.Rows[iMatchIndex].Cells[xColumnName];
            }
        }
        /// <summary>
        /// returns ID field of current grdGroups row
        /// </summary>
        /// <returns></returns>
        private int GetCurrentGroupID()
        {
            int iRet = -1;

            if (grdGroups.CurrentRow == null)
                return iRet;

            //binding source position indicates the actual selected record
            int iInd = m_oGroupSource.Position;

            string xID = grdGroups.Rows[iInd].Cells["ID"].FormattedValue.ToString();
            iRet = xID == "" ? iRet : Int32.Parse(xID);

            return iRet;
        }
        /// <summary>
        /// generates new List name based on constant plus ID
        /// </summary>
        /// <returns></returns>
        private string GetNewGroupName(int iNewID)
        {
            if (grdGroups.Rows.Count == 0)
                return DEF_GROUPNAME;

            //search for existing default name
            DataTable oDT = (DataTable)m_oGroupSource.DataSource;
            DataRow[] oDR = oDT.Select("Name = '" + DEF_GROUPNAME + "'");

            if (oDR.GetLength(0) == 0)
                return DEF_GROUPNAME;
            else
            {
                return DEF_GROUPNAME + "_" + ((iNewID + 2000000) * -1).ToString();
            }
        }
        private void SetReadOnlyRows()
        {
            bool bFoundAdmin = false;
            bool bFoundDesigner = false;
            //do not allow edits of built in groups
            foreach (DataGridViewRow oRow in grdGroups.Rows)
            {
                string xID = oRow.Cells["ID"].Value.ToString();
                if (xID == LMP.Data.PeopleGroups.ADMIN_GROUP.ToString())
                {
                    oRow.ReadOnly = true;
                    bFoundAdmin = true;
                }
                else if (xID == LMP.Data.PeopleGroups.USER_DESIGNER_GROUP.ToString())
                {
                    oRow.ReadOnly = true;
                    bFoundDesigner = true;
                }
                if (bFoundDesigner && bFoundAdmin)
                    return;
            }
        }
        #endregion
        #region *********************event handlers*********************
        private void GroupsManager_Load(object sender, EventArgs e)
        {
            try
            {
                this.DataBindGridGroups(false);
                m_oSearchTimer.Interval = 300;
                m_oSearchTimer.Tick += m_oSearchTimer_Tick;
                m_oSearchTimer.Stop();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        void m_oSearchTimer_Tick(object sender, EventArgs e)
        {
            //Clear search string after time out
            m_xSearchString = "";
            m_oSearchTimer.Stop();
        }
        /// <summary>
        /// bind various grids after control has been loaded and docked on main
        /// form - this prevents refresh bug where record is displayed but grid
        /// display attribute settings are ignored when form is first loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {
                if (this.grdGroups.Rows.Count > 0)
                {
                    //bind the group members grid
                    DataBindGridMembers(GetCurrentGroupID());

                    //GLOG 4495: DataGridView.FindRecord only works with sorted data,
                    //so locate built-in group records using alternate method
                    SetReadOnlyRows();
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires if control is about to be unloaded by selecting another
        /// manager - calls SaveCurrentRecord to commit and save any 
        /// pending edits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// ensures name field is unique and non-null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdGroups_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (grdGroups.Columns[e.ColumnIndex].Name == "Name")
                {
                    BindingSource oBS = (BindingSource)this.grdGroups.DataSource;
                    DataTable oDT = (DataTable)oBS.DataSource;
                    DataRow[] oRows = oDT.Select("Name = '" + e.FormattedValue.ToString() + "'");

                    if (oRows.Length == 0)
                    {
                        this.IsValid = true;
                    }
                    else
                    {
                        if (oRows.Length == 1)
                        {
                            // The row that was found could be the row that is being editted. In such a
                            // case the entry is valid. 
                            int iExistingRowID = (int)oRows[0]["ID"];
                            int iEdittedRowID = (int)this.grdGroups.Rows[e.RowIndex].Cells["ID"].Value;
                            this.IsValid = (iExistingRowID == iEdittedRowID);
                        }
                        else
                        {
                            // More than 1 row has been found. There should be a maximum of 2 items: 
                            this.IsValid = false;
                        }
                    }

                    if (!this.IsValid)
                    {
                        //generate validation message
                        MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateNameValueNotAllowed"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.grdGroups.CurrentCell = this.grdGroups.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    }

                    //cancel navigation if cell data not valid
                    e.Cancel = !this.IsValid;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handle tab input when focus is on checkbox column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdGroups_TabPressed(object sender, LMP.Controls.TabPressedEventArgs e)
        {
            if (grdGroups.CurrentCell.OwningColumn.Name == "Visible")
            {
                if (e.ShiftPressed)
                {
                    SendKeys.Send("{UP}{RIGHT}^{RIGHT}");
                    grdGroups.CurrentCell = grdGroups.CurrentRow.Cells["Description"];
                    grdGroups.CurrentCell.Selected = true;
                }
                else
                    SendKeys.Send("{RIGHT}");
            }
            //GLOG : 8241 : JSW
            //tabbing moves from cell to cell or from row to row for exising records
            else
            {
                int iRowIndex;
                iRowIndex = this.grdGroups.CurrentRow.Index;
                try
                {
                    if (grdGroups.CurrentCell.OwningColumn.Name == "Name")
                    {
                        //move to next cell
                        this.grdGroups.CurrentCell = this.grdGroups.Rows[iRowIndex].Cells[2];
                    }
                    else
                    {
                        if (grdGroups.CurrentCell.OwningColumn.Name == "Description")
                        {
                            if ((grdGroups.Rows.Count - 1) > iRowIndex)
                                this.grdGroups.CurrentCell = this.grdGroups.Rows[iRowIndex + 1].Cells[1];
                            else
                                this.grdGroups.CurrentCell = this.grdGroups.Rows[0].Cells[1];
                        }
                    }
                }
                catch { }           
            }
        }
        /// <summary>
        /// Delete a record.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                //don't allow delete of read only rows -- these belong to built in records
                if(this.grdGroups.CurrentRow != null)
                    this.DeleteGroupMembers();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Initialize and display an AddPeopleToGroupForm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnAddPeople_Click(object sender, EventArgs e)
        {
            try
            {
                //message user if no group name exists or is selected
                if (grdGroups.CurrentRow == null || grdGroups.CurrentRow.IsNewRow)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_GroupNameNotSelected"),
                        LMP.String.MacPacProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation); 
                    return;
                }
                
                //save current record if modified or added
                if (m_oLastGroupRow != null && 
                    (m_oLastGroupRow.RowState == DataRowState.Added ||
                    m_oLastGroupRow.RowState == DataRowState.Detached))
                    SaveCurrentRecord();

                //exit if validation has failed 
                if (!this.IsValid)
                    return;

                if (!this.grdGroups.CurrentRow.IsNewRow)
                {
                    AddGroupMembersForm oAddForm = new AddGroupMembersForm();

                    DialogResult oChoice = oAddForm.ShowDialog();

                    if (oChoice == DialogResult.OK)
                        this.AddPeopleToGroup(oAddForm.PeopleIDsToAdd);
                }
                else
                {
                    MessageBox.Show(
                            LMP.Resources.GetLangString("Error_CannotAddEntryWhenNewRowSelected"), 
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Delete selected person from selected group.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnDeletePeople_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeletePersonFromGroup();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// adds new groups
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                //assign global variable if necessary - this will ensure
                //proper save if grid contains only one row when new record added
                if (m_oLastGroupRow == null && m_oGroupSource.Current != null)
                    m_oLastGroupRow = ((DataRowView)m_oGroupSource.Current).Row;

                SaveCurrentRecord(true);
                UpdateGroups(true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnTranslate_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Under Construction",
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnRefreshPeople_Click(object sender, EventArgs e)
        {           
            try
            {
                MessageBox.Show("Under Construction",
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdGroups_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception is IndexOutOfRangeException)
                return;
            else
                e.ThrowException = true;

        }
        private void ttxtFindGroup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                this.FindMatchingRow(this.ttxtFindGroup.Text, this.grdGroups, "Name");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void ttxtFindPeople_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                this.FindMatchingRow(this.ttxtFindPeople.Text, this.grdMembers, "DisplayName");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdGroups_Leave(object sender, System.EventArgs e)
        {
            try
            {
                if (grdGroups.CurrentRow == null)
                    return;
                
                grdGroups.EndEdit();
                m_oGroupSource.EndEdit();
                this.IsValid = this.ValidateGroupName(grdGroups.CurrentRow.Cells["Name"].FormattedValue.ToString(), true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
       
        /// <summary>
        /// overloaded method
        /// </summary>
        public override void SaveCurrentRecord()
        {
           this.SaveCurrentRecord(true);
        }

        /// <summary>
        /// handles uncommitted changes on close of form
        /// also called if user changes manager
        /// </summary>
        public override void SaveCurrentRecord(bool bValidateName)
        {

            if (m_oLastGroupRow == null && m_oGroupSource.Current != null)
                m_oLastGroupRow = ((DataRowView)m_oGroupSource.Current).Row; 
            
            //user has already deleted last last person record - nothing to save
            if (m_oLastGroupRow != null)
            {
                //commit any edits to grid and binding source
                //this will flip row state to modified if edits have
                //been made
                grdGroups.EndEdit();
                m_oGroupSource.EndEdit();

                if (m_oLastGroupRow.RowState == DataRowState.Unchanged ||
                    m_oLastGroupRow.RowState == DataRowState.Deleted)
                    return;

                if (grdGroups.RowCount == 0)
                {
                    //there are no records in the grid, either because the table was
                    //empty to begin with or because user has deleted last record.
                    return;
                }
                
                //make sure we have valid data for properties -- which 
                //requires at least a display name -
                if (bValidateName && !ValidateGroupName
                    (grdGroups.CurrentRow.Cells["Name"].EditedFormattedValue.ToString(), false))
                {
                    this.IsValid = false;
                    return;
                }
                    
                if (m_oLastGroupRow.RowState == DataRowState.Modified)
                {
                    //update db record
                    UpdateGroups(false);
                }
                else if (m_oLastGroupRow.RowState == DataRowState.Added)
                {
                    //add new group to db
                    UpdateGroups(true);
                }
                this.IsValid = true;
            }
        }
        private void tbtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                System.DirectoryServices.DirectoryEntry entry =
                   new System.DirectoryServices.DirectoryEntry("LDAP://DC=lmp,DC=com");
                System.DirectoryServices.DirectorySearcher mySearcher = new
                   System.DirectoryServices.DirectorySearcher(entry);
                //mySearcher.Filter = ("(anr= John)");
                foreach (System.DirectoryServices.SearchResult result in
                   mySearcher.FindAll())
                {
                    MessageBox.Show(result.GetDirectoryEntry().Path);
                }
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message);
            }
        }
        //GLOG : 8241 : JSW
        //tabbing moves from cell to cell or from row to row for exising records
        private void grdMembers_TabPressed(object sender, LMP.Controls.TabPressedEventArgs e)
        {
            int iRowIndex;

            try
            {
                iRowIndex = this.grdMembers.CurrentRow.Index;
                if ((grdMembers.Rows.Count - 1) > iRowIndex)
                    this.grdMembers.CurrentCell = this.grdMembers.Rows[iRowIndex + 1].Cells[1];
                else
                    this.grdMembers.CurrentCell = this.grdMembers.Rows[0].Cells[1];
            }
            catch { }
        }
        
        #endregion

        private void grdMembers_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Tab:
                    case Keys.Enter:
                    case Keys.Down:
                    case Keys.Left:
                    case Keys.Up:
                    case Keys.Right:
                        break;
                    default:
                        string xKey = e.KeyCode.ToString();
                        //Ignore non-input keys
                        if (xKey.Length == 1)
                        {
                            m_oSearchTimer.Stop();
                            m_oSearchTimer.Start();
                            m_xSearchString += xKey;
                            //Select the first record matching
                            //the search string entered by the user
                            this.SelectMatchingRecord(m_xSearchString);
                            e.Handled = true;
                        }
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Select the first record in grdPeople matching
        /// the text typed into ttxtFindPerson by the user
        /// </summary>
        private void SelectMatchingRecord(string xSearchString)
        {
            // GLOG : 6027 : CEH
            if (xSearchString != "")
            {
                //User has entered a search string

                //Get the index of the first matching record
                //search by Display Name
                int iMatchIndex = this.grdMembers.FindRecord
                    (xSearchString, this.grdMembers.Columns[1].Name, false);

                if (iMatchIndex > -1)
                {
                    //Match found

                    //Select the appropriate record
                    this.grdMembers.CurrentCell =
                        this.grdMembers.Rows[iMatchIndex].Cells[1];
                }
            }
        }

    }
}

