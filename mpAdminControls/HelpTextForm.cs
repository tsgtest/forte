﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LMP.Administration.Controls
{
    public partial class HelpTextForm : Form
    {
        public HelpTextForm()
        {
            InitializeComponent();
        }

        public HelpTextForm(string xInitialHelpText, string xFormCaption):this()
        {
            this.txtHelpText.Text = xInitialHelpText;
            this.Text = xFormCaption;
        }

        public string HelpText
        {
            get
            {
                return this.txtHelpText.Text;
            }

            set
            {
                this.txtHelpText.Text = value;
            }
        }
    }
}
