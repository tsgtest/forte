namespace LMP.Administration.Controls
{
    partial class LocationsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LocationsManager));
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.grdCountries = new System.Windows.Forms.DataGridView();
            this.tsCountries = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnCountriesDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.lblCountries = new System.Windows.Forms.Label();
            this.scSub = new System.Windows.Forms.SplitContainer();
            this.grdStates = new System.Windows.Forms.DataGridView();
            this.tsStates = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnStatesDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.lblStates = new System.Windows.Forms.Label();
            this.grdCounties = new System.Windows.Forms.DataGridView();
            this.TempName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tsCounties = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnCountiesDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lblCounties = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel1.SuspendLayout();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCountries)).BeginInit();
            this.tsCountries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scSub)).BeginInit();
            this.scSub.Panel1.SuspendLayout();
            this.scSub.Panel2.SuspendLayout();
            this.scSub.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdStates)).BeginInit();
            this.tsStates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCounties)).BeginInit();
            this.tsCounties.SuspendLayout();
            this.SuspendLayout();
            // 
            // scMain
            // 
            this.scMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scMain.Location = new System.Drawing.Point(-2, -2);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            this.scMain.Panel1.Controls.Add(this.grdCountries);
            this.scMain.Panel1.Controls.Add(this.tsCountries);
            this.scMain.Panel1.Controls.Add(this.lblCountries);
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.Controls.Add(this.scSub);
            this.scMain.Size = new System.Drawing.Size(697, 540);
            this.scMain.SplitterDistance = 314;
            this.scMain.TabIndex = 0;
            // 
            // grdCountries
            // 
            this.grdCountries.AllowUserToResizeRows = false;
            this.grdCountries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdCountries.BackgroundColor = System.Drawing.Color.White;
            this.grdCountries.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdCountries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCountries.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdCountries.Location = new System.Drawing.Point(3, 30);
            this.grdCountries.MultiSelect = false;
            this.grdCountries.Name = "grdCountries";
            this.grdCountries.RowHeadersVisible = false;
            this.grdCountries.RowHeadersWidth = 25;
            this.grdCountries.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdCountries.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdCountries.Size = new System.Drawing.Size(310, 503);
            this.grdCountries.TabIndex = 0;
            this.grdCountries.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCountries_CellEnter);
            this.grdCountries.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdCountries_CellValidating);
            this.grdCountries.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdCountries_ColumnAdded);
            this.grdCountries.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCountries_RowEnter);
            this.grdCountries.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCountries_RowLeave);
            this.grdCountries.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCountries_RowValidated);
            this.grdCountries.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdCountries_KeyDown);
            // 
            // tsCountries
            // 
            this.tsCountries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsCountries.AutoSize = false;
            this.tsCountries.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsCountries.Dock = System.Windows.Forms.DockStyle.None;
            this.tsCountries.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsCountries.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator5,
            this.tbtnCountriesDelete,
            this.toolStripSeparator6});
            this.tsCountries.Location = new System.Drawing.Point(74, -3);
            this.tsCountries.Name = "tsCountries";
            this.tsCountries.Size = new System.Drawing.Size(241, 37);
            this.tsCountries.TabIndex = 25;
            this.tsCountries.Text = "toolStrip1";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 37);
            // 
            // tbtnCountriesDelete
            // 
            this.tbtnCountriesDelete.AutoSize = false;
            this.tbtnCountriesDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnCountriesDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnCountriesDelete.Image")));
            this.tbtnCountriesDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnCountriesDelete.Name = "tbtnCountriesDelete";
            this.tbtnCountriesDelete.Size = new System.Drawing.Size(53, 22);
            this.tbtnCountriesDelete.Text = "&Delete...";
            this.tbtnCountriesDelete.ToolTipText = "Delete selected country";
            this.tbtnCountriesDelete.Click += new System.EventHandler(this.tbtnCountriesDelete_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 37);
            // 
            // lblCountries
            // 
            this.lblCountries.AutoSize = true;
            this.lblCountries.BackColor = System.Drawing.Color.Transparent;
            this.lblCountries.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountries.Location = new System.Drawing.Point(6, 7);
            this.lblCountries.Name = "lblCountries";
            this.lblCountries.Size = new System.Drawing.Size(61, 13);
            this.lblCountries.TabIndex = 20;
            this.lblCountries.Text = "&Countries";
            // 
            // scSub
            // 
            this.scSub.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scSub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scSub.Location = new System.Drawing.Point(-3, 0);
            this.scSub.Name = "scSub";
            this.scSub.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scSub.Panel1
            // 
            this.scSub.Panel1.Controls.Add(this.grdStates);
            this.scSub.Panel1.Controls.Add(this.tsStates);
            this.scSub.Panel1.Controls.Add(this.lblStates);
            // 
            // scSub.Panel2
            // 
            this.scSub.Panel2.Controls.Add(this.grdCounties);
            this.scSub.Panel2.Controls.Add(this.tsCounties);
            this.scSub.Panel2.Controls.Add(this.lblCounties);
            this.scSub.Size = new System.Drawing.Size(375, 536);
            this.scSub.SplitterDistance = 247;
            this.scSub.SplitterWidth = 5;
            this.scSub.TabIndex = 0;
            // 
            // grdStates
            // 
            this.grdStates.AllowUserToResizeRows = false;
            this.grdStates.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdStates.BackgroundColor = System.Drawing.Color.White;
            this.grdStates.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdStates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdStates.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdStates.Location = new System.Drawing.Point(0, 31);
            this.grdStates.MultiSelect = false;
            this.grdStates.Name = "grdStates";
            this.grdStates.RowHeadersVisible = false;
            this.grdStates.RowHeadersWidth = 25;
            this.grdStates.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdStates.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdStates.Size = new System.Drawing.Size(371, 211);
            this.grdStates.TabIndex = 1;
            this.grdStates.VirtualMode = true;
            this.grdStates.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdStates_CellEnter);
            this.grdStates.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdStates_CellValidating);
            this.grdStates.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdStates_ColumnAdded);
            this.grdStates.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdStates_RowEnter);
            this.grdStates.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdStates_RowLeave);
            this.grdStates.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdStates_RowValidated);
            this.grdStates.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdStates_KeyDown);
            // 
            // tsStates
            // 
            this.tsStates.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsStates.AutoSize = false;
            this.tsStates.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsStates.Dock = System.Windows.Forms.DockStyle.None;
            this.tsStates.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsStates.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.tbtnStatesDelete,
            this.toolStripSeparator4});
            this.tsStates.Location = new System.Drawing.Point(111, -3);
            this.tsStates.Name = "tsStates";
            this.tsStates.Size = new System.Drawing.Size(262, 37);
            this.tsStates.TabIndex = 24;
            this.tsStates.Text = "toolStrip1";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 37);
            // 
            // tbtnStatesDelete
            // 
            this.tbtnStatesDelete.AutoSize = false;
            this.tbtnStatesDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnStatesDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnStatesDelete.Image")));
            this.tbtnStatesDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnStatesDelete.Name = "tbtnStatesDelete";
            this.tbtnStatesDelete.Size = new System.Drawing.Size(53, 22);
            this.tbtnStatesDelete.Text = "&Delete...";
            this.tbtnStatesDelete.ToolTipText = "Delete selected state/province";
            this.tbtnStatesDelete.Click += new System.EventHandler(this.tbtnStatesDelete_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 37);
            // 
            // lblStates
            // 
            this.lblStates.AutoSize = true;
            this.lblStates.BackColor = System.Drawing.Color.Transparent;
            this.lblStates.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStates.Location = new System.Drawing.Point(4, 7);
            this.lblStates.Name = "lblStates";
            this.lblStates.Size = new System.Drawing.Size(105, 13);
            this.lblStates.TabIndex = 21;
            this.lblStates.Text = "&States/Provinces";
            // 
            // grdCounties
            // 
            this.grdCounties.AllowUserToResizeRows = false;
            this.grdCounties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdCounties.BackgroundColor = System.Drawing.Color.White;
            this.grdCounties.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCounties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCounties.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TempName});
            this.grdCounties.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdCounties.Location = new System.Drawing.Point(0, 31);
            this.grdCounties.MultiSelect = false;
            this.grdCounties.Name = "grdCounties";
            this.grdCounties.RowHeadersVisible = false;
            this.grdCounties.RowHeadersWidth = 25;
            this.grdCounties.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdCounties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdCounties.Size = new System.Drawing.Size(371, 248);
            this.grdCounties.TabIndex = 2;
            this.grdCounties.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCounties_CellEnter);
            this.grdCounties.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdCounties_CellValidating);
            this.grdCounties.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdCounties_ColumnAdded);
            this.grdCounties.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCounties_RowEnter);
            this.grdCounties.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCounties_RowLeave);
            this.grdCounties.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCounties_RowValidated);
            this.grdCounties.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdCounties_KeyDown);
            // 
            // TempName
            // 
            this.TempName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TempName.HeaderText = "Name";
            this.TempName.MaxInputLength = 100;
            this.TempName.Name = "TempName";
            // 
            // tsCounties
            // 
            this.tsCounties.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsCounties.AutoSize = false;
            this.tsCounties.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsCounties.Dock = System.Windows.Forms.DockStyle.None;
            this.tsCounties.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsCounties.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.tbtnCountiesDelete,
            this.toolStripSeparator2});
            this.tsCounties.Location = new System.Drawing.Point(80, -3);
            this.tsCounties.Name = "tsCounties";
            this.tsCounties.Size = new System.Drawing.Size(293, 37);
            this.tsCounties.TabIndex = 23;
            this.tsCounties.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 37);
            // 
            // tbtnCountiesDelete
            // 
            this.tbtnCountiesDelete.AutoSize = false;
            this.tbtnCountiesDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnCountiesDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnCountiesDelete.Image")));
            this.tbtnCountiesDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnCountiesDelete.Name = "tbtnCountiesDelete";
            this.tbtnCountiesDelete.Size = new System.Drawing.Size(53, 22);
            this.tbtnCountiesDelete.Text = "&Delete...";
            this.tbtnCountiesDelete.ToolTipText = "Delete selected county";
            this.tbtnCountiesDelete.Click += new System.EventHandler(this.tbtnCountiesDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 37);
            // 
            // lblCounties
            // 
            this.lblCounties.AutoSize = true;
            this.lblCounties.BackColor = System.Drawing.Color.Transparent;
            this.lblCounties.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCounties.Location = new System.Drawing.Point(5, 8);
            this.lblCounties.Name = "lblCounties";
            this.lblCounties.Size = new System.Drawing.Size(56, 13);
            this.lblCounties.TabIndex = 22;
            this.lblCounties.Text = "Co&unties";
            // 
            // LocationsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.scMain);
            this.Name = "LocationsManager";
            this.Size = new System.Drawing.Size(692, 537);
            this.Load += new System.EventHandler(this.LocationsManager_Load);
            this.Enter += new System.EventHandler(this.LocationsManager_Enter);
            this.scMain.Panel1.ResumeLayout(false);
            this.scMain.Panel1.PerformLayout();
            this.scMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCountries)).EndInit();
            this.tsCountries.ResumeLayout(false);
            this.tsCountries.PerformLayout();
            this.scSub.Panel1.ResumeLayout(false);
            this.scSub.Panel1.PerformLayout();
            this.scSub.Panel2.ResumeLayout(false);
            this.scSub.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scSub)).EndInit();
            this.scSub.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdStates)).EndInit();
            this.tsStates.ResumeLayout(false);
            this.tsStates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCounties)).EndInit();
            this.tsCounties.ResumeLayout(false);
            this.tsCounties.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.SplitContainer scMain;
        private System.Windows.Forms.DataGridView grdCountries;
        private System.Windows.Forms.SplitContainer scSub;
        private System.Windows.Forms.Label lblCountries;
        private System.Windows.Forms.Label lblStates;
        private System.Windows.Forms.DataGridView grdStates;
        private System.Windows.Forms.Label lblCounties;
        private System.Windows.Forms.DataGridView grdCounties;
        private System.Windows.Forms.DataGridViewTextBoxColumn TempName;
        private System.Windows.Forms.ToolStrip tsCounties;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tbtnCountiesDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStrip tsCountries;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tbtnCountriesDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStrip tsStates;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbtnStatesDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    }
}
