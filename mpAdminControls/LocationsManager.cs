using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class LocationsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ********************fields*********************
        DataTable m_oCountriesDT;
        DataTable m_oStatesDT;
        DataTable m_oCountiesDT;
        OleDbDataAdapter m_oCountriesAdapter;
        OleDbDataAdapter m_oStatesAdapter;
        OleDbDataAdapter m_oCountiesAdapter;
        private bool m_bLoadingStates = true;
        private bool m_bLoadingCounties = true;
        private bool m_bLoadingCountries = true;
        bool m_bUpdatingLastEditTime = false;
        #endregion
        #region ********************enumerations******************
        #endregion
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region ********************constructors******************
        public LocationsManager()
        {
            InitializeComponent();
        }
        #endregion
        #region ********************methods*********************
        /// <summary>
        /// Display all of the country records in the DB.
        /// </summary>
        private void BindCountriesGrid()
        {
            try
            {
                m_bLoadingCountries = true;
                //get a DataTable with the ID, Name, and ISONum 
                //columns of the Countries table
                OleDbCommand oCmd = new OleDbCommand();
                oCmd.Connection = LocalConnection.ConnectionObject;
                oCmd.CommandText = "SELECT ID, Name, ISONum, LastEditTime FROM Countries ORDER BY Name;";

                m_oCountriesAdapter = new OleDbDataAdapter();
                m_oCountriesAdapter.SelectCommand = oCmd;
                OleDbCommandBuilder oCB = new OleDbCommandBuilder(m_oCountriesAdapter);

                m_oCountriesDT = new DataTable();
                m_oCountriesDT.RowChanged += new DataRowChangeEventHandler(m_oCountriesDT_RowChanged);
                m_oCountriesDT.TableNewRow += new DataTableNewRowEventHandler(m_oCountriesDT_TableNewRow);
                m_oCountriesAdapter.Fill(m_oCountriesDT);

                //Set this DataTable as the DataSource of grdCountries.
                grdCountries.DataSource = m_oCountriesDT;

                oCmd.CommandText = "SELECT ID, Name, ISONum, LastEditTime FROM Countries ORDER BY Name;"; //GLOG 6654

                //Set the display properties of the columns in the grid.
                DataGridViewColumnCollection oCols = grdCountries.Columns;
                oCols["ID"].Visible = false;
                oCols["ISONum"].HeaderText = "ISO Abbr";
                ((DataGridViewTextBoxColumn)oCols["ISONum"]).MaxInputLength = 3;
                oCols["ISONum"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oCols["ISONum"].FillWeight = 55F;
                oCols["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oCols["LastEditTime"].Visible = false;

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotBindDataToGrid"), oE);
            }
            finally
            {
                m_bLoadingCountries = false;
            }
        }

        void m_oCountriesDT_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            UpdateRowLastEditTime(e.Row);
        }

        void m_oCountriesDT_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Change && !m_bUpdatingLastEditTime)
            {
                UpdateRowLastEditTime(e.Row);
            }
        }

        private void UpdateRowLastEditTime(DataRow oRow)
        {
            m_bUpdatingLastEditTime = true;
            oRow["LastEditTime"] = LMP.Data.Application.GetCurrentEditTime();
            m_bUpdatingLastEditTime = false;
        }

        /// <summary>
        /// Display the state records corresponding to the selected 
        /// country record.
        /// </summary>
        private void BindStatesGrid()
        {
            try
            {
                //save current data source
                SaveStates();
                m_bLoadingStates = true;

                string xCurrentCountryID = this.grdCountries.
                    CurrentRow.Cells["ID"].FormattedValue.ToString();
                //Test if this is the row for new records.
                if (xCurrentCountryID != "")
                {
                    //get a DataTable with the ID, Name, CountryID and  
                    //Abbreviation columns of the States table 
                    //where the CountryID = ID of currently selected
                    //country.
                    OleDbCommand oCmd = new OleDbCommand();
                    oCmd.Connection = LocalConnection.ConnectionObject;
                    oCmd.CommandText = "SELECT ID, Name, CountryID, Abbreviation, LastEditTime FROM States WHERE CountryID=" +
                        xCurrentCountryID + " ORDER BY Name";

                    m_oStatesAdapter = new OleDbDataAdapter();
                    m_oStatesAdapter.SelectCommand = oCmd;
                    OleDbCommandBuilder oCB = new OleDbCommandBuilder(m_oStatesAdapter);

                    m_oStatesDT = new DataTable();
                    m_oStatesDT.RowChanged += new DataRowChangeEventHandler(m_oStatesDT_RowChanged);
                    m_oStatesDT.TableNewRow += new DataTableNewRowEventHandler(m_oStatesDT_TableNewRow);
                    m_oStatesAdapter.Fill(m_oStatesDT);

                    //If there are no states associated with this country
                    //clear grdCounties, as this would not happen otherwise.
                    if ((m_oStatesDT.Rows.Count == 0) &&
                            (this.grdCounties.DataSource != null))
                    {
                        DataTable oDTCounties = (DataTable)this.grdCounties.DataSource;
                        oDTCounties.Rows.Clear();
                        this.grdCounties.DataSource = oDTCounties;
                    }

                    //Set the DataTable as the DataSource of grdStates.
                    grdStates.DataSource = m_oStatesDT;

                    //reset adapter command text - this will ensure newly added states
                    //are not reordered until grid is rebound
                    oCmd.CommandText = "SELECT ID, Name, CountryID, Abbreviation, LastEditTime FROM States WHERE CountryID=" +
                        xCurrentCountryID + " ORDER BY Name;"; //GLOG 6654: We do want to reorder once saved to avoid confusion

                    //Set the display properties of the columns in the grid.
                    DataGridViewColumnCollection oCols = grdStates.Columns;
                    oCols["ID"].Visible = false;
                    oCols["CountryID"].Visible = false;
                    oCols["Abbreviation"].HeaderText = "Abbr";
                    oCols["Abbreviation"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    oCols["Abbreviation"].FillWeight = 50F;
                    oCols["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    oCols["LastEditTime"].Visible = false;

                    //set country id for each new state to the ID of the current country
                    m_oStatesDT.Columns["CountryID"].DefaultValue = xCurrentCountryID;

                }
                else
                {
                    //If the just-entered row in grdCountries is the row for new
                    //records then clear the displayed states and counties.
                    DataTable oDT = (DataTable)this.grdStates.DataSource;
                    oDT.Rows.Clear();
                    this.grdStates.DataSource = oDT;

                    //Clear displayed Counties if a DataSource has been set for grCounties
                    if (this.grdCounties.DataSource != null)
                    {
                        DataTable oCountyDT = (DataTable)this.grdCounties.DataSource;
                        oCountyDT.Rows.Clear();
                        this.grdCounties.DataSource = oCountyDT;
                    }
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotBindDataToGrid"), oE);
            }
            finally
            {
                m_bLoadingStates = false;
            }
        }

        void m_oStatesDT_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            this.UpdateRowLastEditTime(e.Row);
        }

        void m_oStatesDT_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Change && !m_bUpdatingLastEditTime)
            {
                this.UpdateRowLastEditTime(e.Row);
            }
        }
        /// <summary>
        /// Display the county records corresponding to the currently 
        /// selected state record.
        /// </summary>
        private void BindCountiesGrid()
        {
            try
            {
                SaveCounties();
                m_bLoadingCounties = true;
                string xCurrentStateID = this.grdStates.CurrentRow.
                    Cells["ID"].FormattedValue.ToString();

                //Test if this is the row for new records.
                if (xCurrentStateID != "")
                {

                    //If this is the first time grdCounties is being 
                    //databound, remove the pre-added name row.
                    if (this.grdCounties.Columns[0].Name == "TempName")
                    {
                        //Remove the pre-added row that was only for display
                        //purposes.
                        this.grdCounties.Columns.Remove(this.grdCounties.Columns["TempName"]);
                    }

                    //get a DataSet with the ID, StateID, and Name   
                    //columns of the Counties table 
                    //where the StateID = ID of currently selected state.
                    OleDbCommand oCmd = new OleDbCommand();
                    oCmd.Connection = LocalConnection.ConnectionObject;
                    oCmd.CommandText = "SELECT ID, StateID, Name, LastEditTime FROM Counties WHERE StateID=" +
                        xCurrentStateID + " ORDER BY Name";

                    m_oCountiesAdapter = new OleDbDataAdapter();
                    m_oCountiesAdapter.SelectCommand = oCmd;
                    OleDbCommandBuilder oCB = new OleDbCommandBuilder(m_oCountiesAdapter);

                    m_oCountiesDT = new DataTable();
                    m_oCountiesDT.RowChanged += new DataRowChangeEventHandler(m_oCountiesDT_RowChanged);
                    m_oCountiesDT.TableNewRow += new DataTableNewRowEventHandler(m_oCountiesDT_TableNewRow);
                    m_oCountiesAdapter.Fill(m_oCountiesDT);


                    //Set the DataTable as the DataSource of grdCounties.
                    grdCounties.DataSource = m_oCountiesDT;

                    //reset oCmd.CommandText, omitting ORDER BY clause -- this will ensure
                    //newly added or items are not reordered until the grid is rebound
                    oCmd.CommandText = "SELECT ID, StateID, Name, LastEditTime FROM Counties WHERE StateID=" +
                        xCurrentStateID + " ORDER BY Name"; //GLOG 6654: We do want to reorder once saved to avoid confusion

                    //Set the display properties of the columns in the grid.
                    DataGridViewColumnCollection oCols = grdCounties.Columns;
                    oCols["ID"].Visible = false;
                    oCols["StateID"].Visible = false;
                    oCols["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    oCols["LastEditTime"].Visible = false;

                    //set state id for each new county to the ID of the current state
                    m_oCountiesDT.Columns["StateID"].DefaultValue = xCurrentStateID;
                }
                else if (this.grdCounties.DataSource != null)
                {
                    //If there is data bound to grdCounties and the newly
                    //selected row in grdStates is the row for new records
                    //then clear the displayed counties.
                    DataTable oDT = (DataTable)this.grdCounties.DataSource;
                    oDT.Rows.Clear();
                    this.grdCounties.DataSource = oDT;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_CouldNotBindDataToGrid"), oE);
            }
            finally
            {
                m_bLoadingCounties = false;
            }
        }

        void m_oCountiesDT_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            this.UpdateRowLastEditTime(e.Row);
        }

        void m_oCountiesDT_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Change && !m_bUpdatingLastEditTime)
            {
                this.UpdateRowLastEditTime(e.Row);
            }
        }
        /// <summary>
        /// saves current set of counties to the database
        /// </summary>
        private void SaveCounties()
        {
            if (m_oCountiesDT != null && m_oCountiesDT.GetChanges() != null &&
                m_oCountiesAdapter != null)
            {
                ClearGridHighlight(grdCounties);
                m_bLoadingCounties = true;
                try
                {
                    //update the db
                    m_oCountiesAdapter.Update(m_oCountiesDT);

                    int iCurRow = 0;

                    if (grdCounties.CurrentRow != null)
                        iCurRow = grdCounties.CurrentRow.Index;

                    //refresh data table -- this will supply new ids if records have added
                    grdCounties.BindingContext[m_oCountiesDT].SuspendBinding();

                    m_oCountiesDT.Rows.Clear();
                    m_oCountiesAdapter.Fill(m_oCountiesDT);

                    //reset current row
                    grdCounties.CurrentCell = grdCounties.Rows[iCurRow].Cells[2];

                    //rebind to the grid
                    grdCounties.BindingContext[m_oCountiesDT].ResumeBinding();
                }
                finally
                {
                    m_bLoadingCounties = false;
                }
            }
        }
        /// <summary>
        /// saves current set of states to the database
        /// </summary>
        private void SaveStates()
        {
            if (m_oStatesDT != null && m_oStatesDT.GetChanges() != null &&
                m_oStatesAdapter != null)
            {
                m_bLoadingStates = true;
                ClearGridHighlight(grdStates);
                try
                {
                    //update the db
                    m_oStatesAdapter.Update(m_oStatesDT);

                    int iCurRow = 0;

                    if (grdStates.CurrentRow != null)
                        iCurRow = grdStates.CurrentRow.Index;

                    //refresh data table -- this will supply new ids if records have added
                    grdStates.BindingContext[m_oStatesDT].SuspendBinding();

                    m_oStatesDT.Rows.Clear();
                    m_oStatesAdapter.Fill(m_oStatesDT);

                    //reset current row
                    grdStates.CurrentCell = grdStates.Rows[iCurRow].Cells[1];

                    //rebind to the grid
                    grdStates.BindingContext[m_oStatesDT].ResumeBinding();
                }
                finally
                {
                    m_bLoadingStates = false;
                }
            }
        }
        /// <summary>
        /// saves current set of states to the database
        /// </summary>
        private void SaveCountries()
        {
            if (m_oCountriesDT != null && m_oCountriesDT.GetChanges() != null &&
                m_oCountriesAdapter != null)
            {
                m_bLoadingCountries = true;
                ClearGridHighlight(grdCountries);
                try
                {
                    //update the db
                    m_oCountriesAdapter.Update(m_oCountriesDT);

                    int iCurRow = grdCountries.CurrentRow.Index;

                    //refresh data table -- this will supply new ids if records have added
                    grdCountries.BindingContext[m_oCountriesDT].SuspendBinding();

                    m_oCountriesDT.Rows.Clear();
                    m_oCountriesAdapter.Fill(m_oCountriesDT);

                    //reset current row
                    grdCountries.CurrentCell = grdCountries.Rows[iCurRow].Cells[1];

                    //rebind to the grid
                    grdCountries.BindingContext[m_oCountriesDT].ResumeBinding();
                }
                finally
                {
                    m_bLoadingCountries = false;
                }

            }
        }
        /// <summary>
        /// Delete the selected county record from the DB.
        /// </summary>
        private void DeleteCounty()
        {
            //test to make sure this is not a new row
            if (!this.grdCounties.CurrentRow.IsNewRow)
            {
                //prompt user for delete
                DialogResult oChoice = MessageBox.Show(
                        LMP.Resources.GetLangString("Message_DeleteEntry"),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oChoice == DialogResult.Yes)                
                {
                    // Log the deletion.
                    LMP.Data.SimpleDataCollection.LogDeletions(mpObjectTypes.Counties, (int)this.grdCounties.CurrentRow.Cells["ID"].Value);

                    int iCurIndex = this.grdCounties.CurrentRow.Index;
                    //delete the record from the grid
                    grdCounties.Rows.Remove(grdCounties.CurrentRow);
                    
                    SaveCounties();
                    
                    //rebind if last record has been deleted
                    if (grdCounties.Rows.Count == 1)
                        BindCountiesGrid();
                   
                    //ensure selection - suspend binding so cell & row validation events won't fire
                    if (grdCounties.Rows.Count > 1)
                    {
                        grdCounties.BindingContext[m_oCountiesDT].SuspendBinding();
                        this.grdCounties.CurrentCell = this.grdCounties.Rows[Math.Max(iCurIndex - 1, 0)].Cells[2]; //Avoid error if first item was deleted
                        grdCounties.BindingContext[m_oCountiesDT].ResumeBinding();
                    }
                }
            }
        }
        /// <summary>
        /// Delete the selected county record from the DB.
        /// </summary>
        private void DeleteCountry()
        {
            if (grdCountries.CurrentRow == null)
                return;
            
            //test to make sure this is not a new row
            if (!this.grdCountries.CurrentRow.IsNewRow)
            {
                //prompt user for delete
                DialogResult oChoice = MessageBox.Show(
                        LMP.Resources.GetLangString("Message_DeleteEntry"),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oChoice == DialogResult.Yes)
                {
                    // Log the deletion.
                    LMP.Data.SimpleDataCollection.LogDeletions(mpObjectTypes.Countries, (int)this.grdCountries.CurrentRow.Cells["ID"].Value);

                    int iCurIndex = this.grdCountries.CurrentRow.Index;

                    //delete record from grid
                    grdCountries.Rows.Remove(grdCountries.CurrentRow);
                    SaveCountries();
                    
                    //rebind if last item deleted to ensure DataTable refreshed for
                    //new entries
                    if (grdCountries.Rows.Count == 1)
                        BindCountriesGrid();
                    
                    //ensure selection - suspend binding so cell & row validation events won't fire
                    if (grdCountries.Rows.Count > 0)
                    {
                        grdCountries.BindingContext[m_oCountriesDT].SuspendBinding();
                        this.grdCountries.CurrentCell = this.grdCountries.Rows[Math.Max(iCurIndex -1, 0)].Cells[1]; //Avoid error if first item was deleted
                        grdCountries.BindingContext[m_oCountriesDT].ResumeBinding();
                    }
                }
            }
        }
        /// <summary>
        /// Delete the selected county record from the DB.
        /// </summary>
        private void DeleteState()
        {
            //test to make sure this is not a new row
            if (!this.grdStates.CurrentRow.IsNewRow)
            {
                //prompt user for delete
                DialogResult oChoice = MessageBox.Show(
                        LMP.Resources.GetLangString("Message_DeleteEntry"),
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (oChoice == DialogResult.Yes)
                {
                    // Log the deletion.
                    LMP.Data.SimpleDataCollection.LogDeletions(mpObjectTypes.States, (int)this.grdStates.CurrentRow.Cells["ID"].Value); 
                    
                    int iCurIndex = this.grdStates.CurrentRow.Index;
                    
                    //delete record from grid
                    grdStates.Rows.Remove(grdStates.CurrentRow);

                    SaveStates();

                    //rebind if last record has been deleted
                    if (grdStates.Rows.Count == 1)
                        BindStatesGrid();
                    
                    //ensure selection - suspend binding so cell & row validation events won't fire
                    if (grdStates.Rows.Count > 1)
                    {
                        grdStates.BindingContext[m_oStatesDT].SuspendBinding();
                        this.grdStates.CurrentCell = this.grdStates.Rows[Math.Max(iCurIndex - 1, 0)].Cells[1]; //Avoid error if first item was deleted
                        grdStates.BindingContext[m_oStatesDT].ResumeBinding();
                    }
                }
            }
        }
        /// <summary>
        /// saves all data for the control - called
        /// when hosting form is closed
        /// </summary>
        public override void SaveCurrentRecord()
        {
            base.SaveCurrentRecord();
        }
        #endregion
        #region ********************event handlers*********************
        private void LocationsManager_Load(object sender, EventArgs e)
        {
            try
            {
                this.BindCountriesGrid();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCountries_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!m_bLoadingCountries && this.grdCountries.ContainsFocus &&
                    this.grdCountries.CurrentRow != null && !this.grdCountries.CurrentRow.IsNewRow)
                    SetGridRowHighlight(grdCountries.CurrentRow);

                //Display relevant states in grdStates
                this.BindStatesGrid();

                //if (this.grdCountries.ContainsFocus)
                //{
                    //Unhighlight current cells in other grids
                    //if user has selected this grid.

                    if (this.grdCounties.CurrentCell != null)
                    {
                        this.grdCounties.CurrentCell.Selected = false;
                        SetGridRowHighlight(this.grdCounties.CurrentRow);
                    }

                    if (this.grdStates.CurrentCell != null)
                    {
                        this.grdStates.CurrentCell.Selected = false;
                        SetGridRowHighlight(this.grdStates.CurrentRow);
                    }
                //}
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Tests for the delete key and focus-changing Alt+hotkey
        /// combos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdCountries_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Test if the key pressed was the delete key
                //and call the DeleteCountry method if it was.
                if (e.KeyCode == Keys.Delete)
                {
                    this.DeleteCountry();
                }
                //Assign focus to relevant grid if an 
                //Alt+hotkey combo is pressed
                else if (e.Alt && e.KeyCode == Keys.S)
                {
                    if (this.grdStates.Focus())
                        //Focus change succeeded- Highlight default selected cell
                        this.grdStates.CurrentCell.Selected = true;
                }

                else if (e.Alt && e.KeyCode == Keys.U)
                {
                    if (this.grdCounties.Focus())
                        //Focus change succeeded- Highlight default selected cell
                        this.grdCounties.CurrentCell.Selected = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        void grdCountries_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //get new values  - name value is required
                string xNameValue = ((DataGridView)sender).Rows[e.RowIndex].Cells[1].EditedFormattedValue.ToString();
                string xNewValue = e.FormattedValue.ToString();

                if (System.String.IsNullOrEmpty(xNameValue))
                {
                    //cancel edit if attempting to leave ISO row if name empty
                    //or if entire row is new
                    if (this.grdCountries.Rows[e.RowIndex].IsNewRow || e.ColumnIndex == 2)
                    {
                        this.grdCountries.CancelEdit();
                        ClearGridRowHighlight(grdCountries.CurrentRow);
                    }
                    else
                    {
                        e.Cancel = true;

                        //set validation flag to prevent user from exiting via Admin form tree
                        this.IsValid = false;

                        //alert user
                        MessageBox.Show(LMP.Resources.GetLangString("Error_EmptyNameValueNotAllowed"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    return;
                }

                string xOldValue = this.grdCountries[e.ColumnIndex, e.RowIndex].Value.ToString();

                if (xOldValue != xNewValue)
                {
                    //value has changed - test for existing entry in data source  - either
                    //duplicate name or duplicate abbreviation
                    DataRow[] oDRArray = this.m_oCountriesDT.Select("Name='" + xNewValue + "'");

                    if (oDRArray.Length > 0)
                    {
                        //item already exists - cancel edit
                        e.Cancel = true;
                        this.grdCountries.CancelEdit();

                        //set validation flag to prevent user from exiting via Admin form tree
                        this.IsValid = false;

                        //alert user
                        MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateCountryEntry"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);

                        return;
                    }
                }
                if (this.grdCountries.ContainsFocus) 
                    this.IsValid = true;
            }
            catch (InvalidOperationException)
            {
                //do nothing - edit was canceled on new row proposed record with otherwise empty collection
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdStates_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!m_bLoadingStates && this.grdStates.ContainsFocus &&
                    this.grdStates.CurrentRow != null && !this.grdStates.CurrentRow.IsNewRow)
                    SetGridRowHighlight(grdStates.CurrentRow);
                //Display relevant counties in grdCounties
                this.BindCountiesGrid();
                if (this.grdStates.ContainsFocus)
                {
                    //Unhighlight current cells in other grids
                    //if user has selected this grid.

                    if (this.grdCounties.CurrentCell != null)
                    {
                        this.grdCounties.CurrentCell.Selected = false;
                        SetGridRowHighlight(this.grdCounties.CurrentRow);
                    }

                    if (this.grdCountries.CurrentCell != null)
                    {
                        this.grdCountries.CurrentCell.Selected = false;
                        SetGridRowHighlight(this.grdCountries.CurrentRow);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        void grdStates_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //get new values  - name value is required
                string xNameValue = ((DataGridView)sender).Rows[e.RowIndex].Cells[1].EditedFormattedValue.ToString();
                string xNewValue = e.FormattedValue.ToString();

                if (System.String.IsNullOrEmpty(xNameValue))
                {
                    if (this.grdStates.Rows[e.RowIndex].IsNewRow || e.ColumnIndex == 3)
                    {
                        this.grdStates.CancelEdit();
                        ClearGridRowHighlight(grdStates.CurrentRow);
                    }
                    else
                    {
                        //set validation flag to prevent user from exiting via Admin form tree
                        this.IsValid = false;

                        e.Cancel = true;

                        //alert user
                        MessageBox.Show(LMP.Resources.GetLangString("Error_EmptyNameValueNotAllowed"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    return;
                }

                string xOldValue = this.grdStates[e.ColumnIndex, e.RowIndex].Value.ToString();

                if (xOldValue != xNewValue)
                {
                    //value has changed - test for existing entry in data source  - either
                    //duplicate name or duplicate abbreviation
                    DataRow[] oDRArray = this.m_oStatesDT.Select("Name='" + xNewValue + "'");

                    if (oDRArray.Length > 0)
                    {
                        //item already exists - cancel edit
                        e.Cancel = true;
                        this.grdStates.CancelEdit();
                        
                        //set validation flag to prevent user from exiting via Admin form tree
                        this.IsValid = false;

                        //alert user
                        MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateStateName"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (this.grdStates.ContainsFocus)
                    this.IsValid = true;
            }
            catch (InvalidOperationException)
            {
                //do nothing - edit was canceled on new row proposed record with otherwise empty collection
            }

            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }        
        }
        /// <summary>
        /// Tests for the delete key and Alt+hotkey combos that 
        /// change grid focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdStates_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Test if the key pressed was the delete key
                //and call the DeleteState method if it was.
                if (e.KeyCode == Keys.Delete)
                {
                    this.DeleteState();
                }
                //Assign focus to relevant grid if an 
                //Alt+hotkey combo is pressed
                else if (e.Alt && e.KeyCode == Keys.C)
                {
                    if (this.grdCountries.Focus())
                        //Focus change succeeded- Highlight default selected cell
                        this.grdCountries.CurrentCell.Selected = true;
                }

                else if (e.Alt && e.KeyCode == Keys.U)
                {
                    if (this.grdCounties.Focus())
                        //Focus change succeeded- Highlight default selected cell
                        this.grdCounties.CurrentCell.Selected = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void grdCounties_CellEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                this.grdCounties.ReadOnly = grdStates.CurrentRow == null;
                if (!m_bLoadingCounties && this.grdCounties.ContainsFocus &&
                    this.grdCounties.CurrentRow != null && !this.grdCounties.CurrentRow.IsNewRow)
                    SetGridRowHighlight(grdCounties.CurrentRow);
                if (this.grdCounties.ContainsFocus)
                {
                    //Unhighlight current cells in other grids
                    //if user has selected this grid.

                    if (this.grdCountries.CurrentCell != null)
                    {
                        this.grdCountries.CurrentCell.Selected = false;
                        SetGridRowHighlight(this.grdCountries.CurrentRow);
                    }

                    if (this.grdStates.CurrentCell != null)
                    {
                        this.grdStates.CurrentCell.Selected = false;
                        SetGridRowHighlight(this.grdStates.CurrentRow);
                    }
                }

            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCounties_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //get new value
                string xNewValue = e.FormattedValue.ToString();

                if (System.String.IsNullOrEmpty(xNewValue))
                {
                    if (this.grdCounties.Rows[e.RowIndex].IsNewRow)
                    {
                        this.grdCounties.CancelEdit();
                        ClearGridRowHighlight(grdCounties.CurrentRow);
                    }
                    else
                    {
                        e.Cancel = true;

                        //set validation flag to prevent user from exiting via Admin form tree
                        this.IsValid = false;

                        //alert user
                        MessageBox.Show(LMP.Resources.GetLangString("Error_EmptyNameValueNotAllowed"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } 
                    return;
                }

                string xOldValue = this.grdCounties[e.ColumnIndex, e.RowIndex].Value.ToString();

                if (xOldValue != xNewValue)
                {
                    //value has changed - test for existing entry in data source
                    DataRow[] oDRArray = this.m_oCountiesDT.Select("Name='" + xNewValue + "'");

                    if (oDRArray.Length > 0)
                    {
                        //item already exists - cancel edit
                        e.Cancel = true;
                        grdCounties.CancelEdit();
                        
                        //set validation flag to prevent user from exiting via Admin form tree
                        this.IsValid = false;
                        
                        //alert user
                        MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateCountyName"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);

                        return;
                    }
                }
                if (this.grdCounties.ContainsFocus) 
                    this.IsValid = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCounties_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Test if the key pressed was the delete key
                //and call the DeleteCounty method if it was.
                if (e.KeyCode == Keys.Delete)
                {
                    this.DeleteCounty();
                }
                //Assign focus to relevant grid if an 
                //Alt+hotkey combo is pressed
                else if (e.Alt && e.KeyCode == Keys.C)
                {
                    if (this.grdCountries.Focus())
                        //Focus change succeeded- Highlight default selected cell
                        this.grdCountries.CurrentCell.Selected = true;
                }

                else if (e.Alt && e.KeyCode == Keys.S)
                {
                    if (this.grdStates.Focus())
                        //Focus change succeeded- Highlight default selected cell
                        this.grdStates.CurrentCell.Selected = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdStates_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.m_oStatesDT.GetChanges() != null)
                    SaveStates();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCounties_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.m_oCountiesDT.GetChanges() != null)
                    SaveCounties();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCountries_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.m_oCountriesDT.GetChanges() != null)
                    SaveCountries();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// calls routine to delete selected record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnCountriesDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdCountries.CurrentRow != null)
                    DeleteCountry();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// calls routine to delete selected record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnStatesDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdStates.CurrentRow != null)
                    DeleteState(); 
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// calls routine to delete selected record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnCountiesDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdCounties.CurrentRow != null)
                    DeleteCounty();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdStates_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
                //SetGridRowHighlight(grdStates.Rows[e.RowIndex]);
        }

        private void grdStates_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            ClearGridRowHighlight(grdStates.Rows[e.RowIndex]);
        }
        private void SetGridRowHighlight(DataGridViewRow oRow)
        {
            try
            {
                if (oRow != null)
                    oRow.DefaultCellStyle.BackColor = Color.LightGray;
            }
            catch { }
        }
        private void ClearGridRowHighlight(DataGridViewRow oRow)
        {
            try
            {
                if (oRow != null && oRow.DefaultCellStyle.BackColor != oRow.DataGridView.DefaultCellStyle.BackColor)
                {
                    oRow.DefaultCellStyle.BackColor = oRow.DataGridView.DefaultCellStyle.BackColor;
                }
            }
            catch { }
        }

        private void grdCounties_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (!m_bLoadingCounties && !m_bLoadingForm)
            //    SetGridRowHighlight(grdCounties.Rows[e.RowIndex]);
        }

        private void grdCounties_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            ClearGridRowHighlight(grdCounties.Rows[e.RowIndex]);
        }

        private void grdCountries_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
           ClearGridRowHighlight(grdCountries.Rows[e.RowIndex]);
        }

        private void grdCountries_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
           //SetGridRowHighlight(grdCountries.Rows[e.RowIndex]);
        }
        private void ClearGridHighlight(DataGridView oGrid)
        {
            try
            {
                DateTime t0 = DateTime.Now;
                foreach (DataGridViewRow oRow in oGrid.Rows)
                {
                    ClearGridRowHighlight(oRow);
                }
                LMP.Benchmarks.Print(t0, oGrid.Name);
            }
            catch { }

        }

        private void LocationsManager_Enter(object sender, EventArgs e)
        {
            //Set Highlight for initial Country row
            if (grdCountries.Rows.Count > 0)
                SetGridRowHighlight(grdCountries.Rows[0]);
        }

        private void grdCountries_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }
        }

        private void grdStates_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }

        }

        private void grdCounties_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }

        }
        #region ********************internal procedures******************
        #endregion
        #endregion
    }
}

