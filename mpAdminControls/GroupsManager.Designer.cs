namespace LMP.Administration.Controls
{
    partial class GroupsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GroupsManager));
            this.scGroups = new System.Windows.Forms.SplitContainer();
            this.grdGroups = new LMP.Controls.DataGridView();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tlblGroups = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnImport = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnTranslate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tlblFindGroup = new System.Windows.Forms.ToolStripLabel();
            this.ttxtFindGroup = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.lblGroups = new System.Windows.Forms.Label();
            this.grdMembers = new LMP.Controls.DataGridView();
            this.tsMembers = new System.Windows.Forms.ToolStrip();
            this.tlblMembers = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnRefreshPeople = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnAddPeople = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDeletePeople = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tlblFindPeople = new System.Windows.Forms.ToolStripLabel();
            this.ttxtFindPeople = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.lblMembers = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scGroups)).BeginInit();
            this.scGroups.Panel1.SuspendLayout();
            this.scGroups.Panel2.SuspendLayout();
            this.scGroups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdGroups)).BeginInit();
            this.tsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMembers)).BeginInit();
            this.tsMembers.SuspendLayout();
            this.SuspendLayout();
            // 
            // scGroups
            // 
            this.scGroups.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.scGroups.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scGroups.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.scGroups.Location = new System.Drawing.Point(0, 0);
            this.scGroups.Name = "scGroups";
            this.scGroups.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scGroups.Panel1
            // 
            this.scGroups.Panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scGroups.Panel1.Controls.Add(this.grdGroups);
            this.scGroups.Panel1.Controls.Add(this.tsMain);
            this.scGroups.Panel1.Controls.Add(this.lblGroups);
            // 
            // scGroups.Panel2
            // 
            this.scGroups.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scGroups.Panel2.Controls.Add(this.grdMembers);
            this.scGroups.Panel2.Controls.Add(this.tsMembers);
            this.scGroups.Panel2.Controls.Add(this.lblMembers);
            this.scGroups.Size = new System.Drawing.Size(635, 614);
            this.scGroups.SplitterDistance = 293;
            this.scGroups.SplitterWidth = 2;
            this.scGroups.TabIndex = 0;
            this.scGroups.TabStop = false;
            // 
            // grdGroups
            // 
            this.grdGroups.AllowUserToAddRows = false;
            this.grdGroups.AllowUserToDeleteRows = false;
            this.grdGroups.AllowUserToResizeRows = false;
            this.grdGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdGroups.BackgroundColor = System.Drawing.Color.White;
            this.grdGroups.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGroups.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdGroups.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdGroups.IsDirty = false;
            this.grdGroups.Location = new System.Drawing.Point(0, 30);
            this.grdGroups.MultiSelect = false;
            this.grdGroups.Name = "grdGroups";
            this.grdGroups.RowHeadersVisible = false;
            this.grdGroups.RowHeadersWidth = 25;
            this.grdGroups.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdGroups.Size = new System.Drawing.Size(631, 259);
            this.grdGroups.SupportingValues = "";
            this.grdGroups.TabIndex = 17;
            this.grdGroups.Tag2 = null;
            this.grdGroups.Value = null;
            this.grdGroups.TabPressed += new LMP.Controls.TabPressedHandler(this.grdGroups_TabPressed);
            this.grdGroups.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdGroups_CellValidating);
            this.grdGroups.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdGroups_DataError);
            this.grdGroups.Leave += new System.EventHandler(this.grdGroups_Leave);
            // 
            // tsMain
            // 
            this.tsMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsMain.AutoSize = false;
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblGroups,
            this.toolStripSeparator8,
            this.tbtnNew,
            this.toolStripSeparator2,
            this.tbtnDelete,
            this.toolStripSeparator11,
            this.tbtnImport,
            this.toolStripSeparator3,
            this.tbtnTranslate,
            this.toolStripSeparator4,
            this.tlblFindGroup,
            this.ttxtFindGroup,
            this.toolStripSeparator10});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(2, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMain.Size = new System.Drawing.Size(632, 31);
            this.tsMain.TabIndex = 18;
            // 
            // tlblGroups
            // 
            this.tlblGroups.AutoSize = false;
            this.tlblGroups.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblGroups.Name = "tlblGroups";
            this.tlblGroups.Size = new System.Drawing.Size(75, 22);
            this.tlblGroups.Text = "Groups";
            this.tlblGroups.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnNew
            // 
            this.tbtnNew.AutoSize = false;
            this.tbtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNew.Image")));
            this.tbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNew.Name = "tbtnNew";
            this.tbtnNew.Size = new System.Drawing.Size(68, 22);
            this.tbtnNew.Text = "&New...";
            this.tbtnNew.Click += new System.EventHandler(this.tbtnNew_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(53, 28);
            this.tbtnDelete.Text = "&Delete...";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnImport
            // 
            this.tbtnImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnImport.Image = ((System.Drawing.Image)(resources.GetObject("tbtnImport.Image")));
            this.tbtnImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnImport.Name = "tbtnImport";
            this.tbtnImport.Size = new System.Drawing.Size(56, 28);
            this.tbtnImport.Text = "&Import...";
            this.tbtnImport.Click += new System.EventHandler(this.tbtnImport_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnTranslate
            // 
            this.tbtnTranslate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnTranslate.Image = ((System.Drawing.Image)(resources.GetObject("tbtnTranslate.Image")));
            this.tbtnTranslate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnTranslate.Name = "tbtnTranslate";
            this.tbtnTranslate.Size = new System.Drawing.Size(67, 28);
            this.tbtnTranslate.Text = "&Translate...";
            this.tbtnTranslate.Click += new System.EventHandler(this.tbtnTranslate_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // tlblFindGroup
            // 
            this.tlblFindGroup.AutoSize = false;
            this.tlblFindGroup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tlblFindGroup.Image = ((System.Drawing.Image)(resources.GetObject("tlblFindGroup.Image")));
            this.tlblFindGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlblFindGroup.Name = "tlblFindGroup";
            this.tlblFindGroup.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tlblFindGroup.Size = new System.Drawing.Size(50, 22);
            this.tlblFindGroup.Text = "&Find:";
            // 
            // ttxtFindGroup
            // 
            this.ttxtFindGroup.Name = "ttxtFindGroup";
            this.ttxtFindGroup.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.ttxtFindGroup.Size = new System.Drawing.Size(210, 31);
            this.ttxtFindGroup.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ttxtFindGroup_KeyUp);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 31);
            // 
            // lblGroups
            // 
            this.lblGroups.AutoSize = true;
            this.lblGroups.BackColor = System.Drawing.Color.Transparent;
            this.lblGroups.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroups.Location = new System.Drawing.Point(3, 2);
            this.lblGroups.Name = "lblGroups";
            this.lblGroups.Size = new System.Drawing.Size(53, 16);
            this.lblGroups.TabIndex = 19;
            this.lblGroups.Text = "Groups";
            // 
            // grdMembers
            // 
            this.grdMembers.AllowUserToAddRows = false;
            this.grdMembers.AllowUserToResizeRows = false;
            this.grdMembers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdMembers.BackgroundColor = System.Drawing.Color.White;
            this.grdMembers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdMembers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMembers.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdMembers.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdMembers.IsDirty = false;
            this.grdMembers.Location = new System.Drawing.Point(0, 30);
            this.grdMembers.MultiSelect = false;
            this.grdMembers.Name = "grdMembers";
            this.grdMembers.ReadOnly = true;
            this.grdMembers.RowHeadersVisible = false;
            this.grdMembers.RowHeadersWidth = 25;
            this.grdMembers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdMembers.Size = new System.Drawing.Size(631, 281);
            this.grdMembers.SupportingValues = "";
            this.grdMembers.TabIndex = 21;
            this.grdMembers.Tag2 = null;
            this.grdMembers.Value = null;
            this.grdMembers.TabPressed += new LMP.Controls.TabPressedHandler(this.grdMembers_TabPressed);
            this.grdMembers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdMembers_KeyDown);
            // 
            // tsMembers
            // 
            this.tsMembers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsMembers.AutoSize = false;
            this.tsMembers.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMembers.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMembers.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMembers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblMembers,
            this.toolStripSeparator9,
            this.tbtnRefreshPeople,
            this.toolStripSeparator1,
            this.tbtnAddPeople,
            this.toolStripSeparator6,
            this.tbtnDeletePeople,
            this.toolStripSeparator5,
            this.tlblFindPeople,
            this.ttxtFindPeople,
            this.toolStripSeparator7});
            this.tsMembers.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMembers.Location = new System.Drawing.Point(2, 0);
            this.tsMembers.Name = "tsMembers";
            this.tsMembers.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMembers.Size = new System.Drawing.Size(634, 31);
            this.tsMembers.TabIndex = 22;
            // 
            // tlblMembers
            // 
            this.tlblMembers.AutoSize = false;
            this.tlblMembers.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblMembers.Name = "tlblMembers";
            this.tlblMembers.Size = new System.Drawing.Size(75, 22);
            this.tlblMembers.Text = "Members";
            this.tlblMembers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnRefreshPeople
            // 
            this.tbtnRefreshPeople.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnRefreshPeople.Image = ((System.Drawing.Image)(resources.GetObject("tbtnRefreshPeople.Image")));
            this.tbtnRefreshPeople.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnRefreshPeople.Name = "tbtnRefreshPeople";
            this.tbtnRefreshPeople.Size = new System.Drawing.Size(59, 28);
            this.tbtnRefreshPeople.Text = "&Refresh...";
            this.tbtnRefreshPeople.Click += new System.EventHandler(this.tbtnRefreshPeople_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnAddPeople
            // 
            this.tbtnAddPeople.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnAddPeople.Image = ((System.Drawing.Image)(resources.GetObject("tbtnAddPeople.Image")));
            this.tbtnAddPeople.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAddPeople.Name = "tbtnAddPeople";
            this.tbtnAddPeople.Size = new System.Drawing.Size(42, 28);
            this.tbtnAddPeople.Text = "&Add...";
            this.tbtnAddPeople.Click += new System.EventHandler(this.tbtnAddPeople_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnDeletePeople
            // 
            this.tbtnDeletePeople.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeletePeople.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeletePeople.Image")));
            this.tbtnDeletePeople.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeletePeople.Name = "tbtnDeletePeople";
            this.tbtnDeletePeople.Size = new System.Drawing.Size(53, 28);
            this.tbtnDeletePeople.Text = "&Delete...";
            this.tbtnDeletePeople.Click += new System.EventHandler(this.tbtnDeletePeople_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 31);
            // 
            // tlblFindPeople
            // 
            this.tlblFindPeople.AutoSize = false;
            this.tlblFindPeople.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tlblFindPeople.Image = ((System.Drawing.Image)(resources.GetObject("tlblFindPeople.Image")));
            this.tlblFindPeople.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlblFindPeople.Name = "tlblFindPeople";
            this.tlblFindPeople.Size = new System.Drawing.Size(50, 22);
            this.tlblFindPeople.Text = "&Find:";
            // 
            // ttxtFindPeople
            // 
            this.ttxtFindPeople.Name = "ttxtFindPeople";
            this.ttxtFindPeople.Size = new System.Drawing.Size(250, 31);
            this.ttxtFindPeople.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ttxtFindPeople_KeyUp);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 31);
            // 
            // lblMembers
            // 
            this.lblMembers.AutoSize = true;
            this.lblMembers.BackColor = System.Drawing.Color.Transparent;
            this.lblMembers.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMembers.Location = new System.Drawing.Point(3, 7);
            this.lblMembers.Name = "lblMembers";
            this.lblMembers.Size = new System.Drawing.Size(66, 16);
            this.lblMembers.TabIndex = 20;
            this.lblMembers.Text = "Members";
            // 
            // GroupsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.scGroups);
            this.Name = "GroupsManager";
            this.Size = new System.Drawing.Size(635, 614);
            this.Load += new System.EventHandler(this.GroupsManager_Load);
            this.scGroups.Panel1.ResumeLayout(false);
            this.scGroups.Panel1.PerformLayout();
            this.scGroups.Panel2.ResumeLayout(false);
            this.scGroups.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scGroups)).EndInit();
            this.scGroups.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdGroups)).EndInit();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMembers)).EndInit();
            this.tsMembers.ResumeLayout(false);
            this.tsMembers.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer scGroups;
        private System.Windows.Forms.Label lblGroups;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbtnTranslate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private LMP.Controls.DataGridView grdGroups;
        private System.Windows.Forms.Label lblMembers;
        private LMP.Controls.DataGridView grdMembers;
        private System.Windows.Forms.ToolStrip tsMembers;
        private System.Windows.Forms.ToolStripButton tbtnRefreshPeople;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tbtnDeletePeople;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton tbtnAddPeople;
        private System.Windows.Forms.ToolStripLabel tlblFindGroup;
        private System.Windows.Forms.ToolStripTextBox ttxtFindGroup;
        private System.Windows.Forms.ToolStripLabel tlblFindPeople;
        private System.Windows.Forms.ToolStripTextBox ttxtFindPeople;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel tlblGroups;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripLabel tlblMembers;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton tbtnNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton tbtnImport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;

    }
}
