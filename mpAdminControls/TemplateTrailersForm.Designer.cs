﻿namespace LMP.Administration.Controls
{
    partial class TemplateTrailersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpTrailerDialog = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTrailers = new System.Windows.Forms.Label();
            this.lstTrailers = new System.Windows.Forms.CheckedListBox();
            this.lblTemplateName = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbTrailerDef = new LMP.Controls.ComboBox();
            this.lstTemplates = new LMP.Controls.ListBox();
            this.grpTrailerDialog.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(192, 317);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(84, 27);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(101, 317);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(84, 27);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Edit...";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(10, 317);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(84, 27);
            this.btnAdd.TabIndex = 11;
            this.btnAdd.Text = "Add New...";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpTrailerDialog
            // 
            this.grpTrailerDialog.Controls.Add(this.cmbTrailerDef);
            this.grpTrailerDialog.Controls.Add(this.label1);
            this.grpTrailerDialog.Controls.Add(this.lblTrailers);
            this.grpTrailerDialog.Controls.Add(this.lstTrailers);
            this.grpTrailerDialog.Location = new System.Drawing.Point(288, 23);
            this.grpTrailerDialog.Name = "grpTrailerDialog";
            this.grpTrailerDialog.Size = new System.Drawing.Size(281, 282);
            this.grpTrailerDialog.TabIndex = 10;
            this.grpTrailerDialog.TabStop = false;
            this.grpTrailerDialog.Text = "Trailer Dialog";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 214);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Default Trailer:";
            // 
            // lblTrailers
            // 
            this.lblTrailers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTrailers.AutoSize = true;
            this.lblTrailers.Location = new System.Drawing.Point(7, 19);
            this.lblTrailers.Name = "lblTrailers";
            this.lblTrailers.Size = new System.Drawing.Size(95, 13);
            this.lblTrailers.TabIndex = 15;
            this.lblTrailers.Text = "Default Trailer List:";
            // 
            // lstTrailers
            // 
            this.lstTrailers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTrailers.CheckOnClick = true;
            this.lstTrailers.FormattingEnabled = true;
            this.lstTrailers.Location = new System.Drawing.Point(10, 35);
            this.lstTrailers.Name = "lstTrailers";
            this.lstTrailers.Size = new System.Drawing.Size(261, 154);
            this.lstTrailers.TabIndex = 16;
            this.lstTrailers.SelectedIndexChanged += new System.EventHandler(this.lstTrailers_SelectedIndexChanged);
            // 
            // lblTemplateName
            // 
            this.lblTemplateName.AutoSize = true;
            this.lblTemplateName.Location = new System.Drawing.Point(10, 12);
            this.lblTemplateName.Name = "lblTemplateName";
            this.lblTemplateName.Size = new System.Drawing.Size(124, 13);
            this.lblTemplateName.TabIndex = 0;
            this.lblTemplateName.Text = "Template Name/Pattern:";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(395, 317);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 27);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(485, 317);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(84, 27);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // cmbTrailerDef
            // 
            this.cmbTrailerDef.AllowEmptyValue = true;
            this.cmbTrailerDef.Borderless = false;
            this.cmbTrailerDef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbTrailerDef.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTrailerDef.IsDirty = false;
            this.cmbTrailerDef.LimitToList = true;
            this.cmbTrailerDef.ListName = "";
            this.cmbTrailerDef.Location = new System.Drawing.Point(10, 230);
            this.cmbTrailerDef.MaxDropDownItems = 8;
            this.cmbTrailerDef.Name = "cmbTrailerDef";
            this.cmbTrailerDef.SelectedIndex = -1;
            this.cmbTrailerDef.SelectedValue = null;
            this.cmbTrailerDef.SelectionLength = 0;
            this.cmbTrailerDef.SelectionStart = 0;
            this.cmbTrailerDef.Size = new System.Drawing.Size(261, 23);
            this.cmbTrailerDef.SortOrder = System.Windows.Forms.SortOrder.None;
            this.cmbTrailerDef.SupportingValues = "";
            this.cmbTrailerDef.TabIndex = 18;
            this.cmbTrailerDef.Tag2 = null;
            this.cmbTrailerDef.Value = "";
            this.cmbTrailerDef.ValueChanged += new LMP.Controls.ValueChangedHandler(this.cmbTrailerDef_ValueChanged);
            // 
            // lstTemplates
            // 
            this.lstTemplates.AllowEmptyValue = false;
            this.lstTemplates.FormattingEnabled = true;
            this.lstTemplates.IsDirty = false;
            this.lstTemplates.ListName = "";
            this.lstTemplates.Location = new System.Drawing.Point(11, 28);
            this.lstTemplates.Name = "lstTemplates";
            this.lstTemplates.Size = new System.Drawing.Size(264, 277);
            this.lstTemplates.SupportingValues = "";
            this.lstTemplates.TabIndex = 1;
            this.lstTemplates.Tag2 = null;
            this.lstTemplates.Value = "";
            this.lstTemplates.SelectedIndexChanged += new System.EventHandler(this.lstTemplates_SelectedIndexChanged);
            // 
            // TemplateTrailersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(579, 360);
            this.ControlBox = false;
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpTrailerDialog);
            this.Controls.Add(this.lstTemplates);
            this.Controls.Add(this.lblTemplateName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "TemplateTrailersForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Template Trailer Assignments";
            this.Load += new System.EventHandler(this.TemplateTrailersForm_Load);
            this.grpTrailerDialog.ResumeLayout(false);
            this.grpTrailerDialog.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblTemplateName;
        private LMP.Controls.ListBox lstTemplates;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpTrailerDialog;
        private LMP.Controls.ComboBox cmbTrailerDef;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTrailers;
        private System.Windows.Forms.CheckedListBox lstTrailers;
        private System.Windows.Forms.Button btnDelete;
    }
}