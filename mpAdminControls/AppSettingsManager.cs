using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using LMP.Data;
using FirmAppSettings = LMP.Data.FirmApplicationSettings;

namespace LMP.Administration.Controls
{
    public partial class AppSettingsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ********************fields******************
        ArrayList m_oOffices = null;
        ArrayList m_oGroups = null;
        ArrayList m_oEntities = null;

        LMP.Data.FirmApplicationSettings m_oFirmAppSettings;
        LMP.Data.UserApplicationSettings m_oUserAppSettings;

        int m_iEntityID = 0;

        Hashtable m_oTrailerIDs = new Hashtable();
        Hashtable m_oDraftStampIDs = new Hashtable();
        Hashtable m_oTrailerIndices = new Hashtable();
        Hashtable m_oDraftStampIndices = new Hashtable();

        string m_xPublicFoldersLabel;
        string m_xMyFoldersLabel;
        string m_xSharedFoldersLabel;
        string m_xNetworkDBServer;
        string m_xNetworkDBName;
        string m_xIISServerIPAddress;
        string m_xServerAppName;
        string m_xFreqUnit;
        string m_xFreqInterval;
        string m_xDayOfWeek;
        string m_xTime;
        bool m_bLogUserSync; //GLOG 5864
        bool m_bExpandChildSegment;
        bool m_bFinishButtonAtEnd; //GLOG 8511
        bool m_bShowSegmentSpecificRibbons;
        int m_iMaxPeople;
        int m_iPeopleListThreshold;
        string m_xCheckboxFont = "";
        string m_xCheckedCheckboxChar = "";
        string m_xUncheckedCheckboxChar = "";

        bool m_bDisplayTaskPaneForMPDoc;
        bool m_bDisplayTaskPaneForNonMPDoc;
        bool m_bDisplayTaskPaneForNewBlankDoc;
        bool m_bDisplayTaskPaneWhenNoVariables; //GLOG 15933
        bool m_bShowXMLTags;
        bool m_bFitToPageWidth;
        bool m_bUpdateZoom;
        int m_iDefaultPageZoomPercentage;

        bool m_bUseGradient;
        bool m_bCloseTaskPaneOnFinish;
        bool m_bWarnOnFinish;
        bool m_bActivateMacPacRibbonTab;
        bool m_bActivateMacPacRibbonTabOnOpen;
        Color m_oTaskPaneBackgroundColor;
        bool m_bShowQuickHelp;
        bool m_bExpandFindResults;


        // GLOG : 3138 : JAB
        // Fields for UserContentWarningTheshold and UserContentLimit.
        int m_iUserContentLimit;
        int m_iUserContentThreshold;

        // GLOG : 5358 : JSW
        // Fields for SavedDataWarningTheshold and SavedDataLimit.
        int m_iSavedDataLimit;
        int m_iSavedDataThreshold;

        // GLOG : 3315 : JAB
        // Fields for Help font size and name.
        string m_xHelpFontName;

        // This is a string because the value property in the spinner 
        // control that represents this field returns a string. using
        // a string simplifies the check for a change in the font size
        // value.
        string m_xHelpFontSize;

        // GLOG : 3292 : JAB
        // Introduce fields for the My/Public/Shared folders help text.
        string m_xMyFoldersHelpText;
        string m_xPublicFoldersHelpText;
        string m_xSharedFoldersHelpText;

        //JTS: 03/18/10
        string m_xDistributableFolder;

        //5-13-11 (dm)
        bool m_bWarnBeforeDeactivation;

        #endregion
        #region ********************constructors******************
        public AppSettingsManager()
        {
            InitializeComponent();
            this.cmbCheckboxFont.ExecuteFinalSetup();
        }
        #endregion
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        private int SelectedEntityID
        {
            get
            {
                int iIndex = this.cmbEntity.SelectedIndex;

                if (iIndex >= 0 && iIndex < m_oEntities.Count)
                {
                    object[] aoEntityDetails = (object[])m_oEntities[iIndex];
                    return int.Parse(aoEntityDetails[0].ToString());
                }
                return 0;
            }
        }

        #endregion
        #region ********************methods******************
        /// <summary>
        /// loads the appropriate list of entities into the dropdown
        /// </summary>
        private void LoadEntitiesList()
        {
            //refresh list if necessary
            if (rdOffices.Checked && m_oOffices == null)
            {
                //get offices
                m_oEntities = new LMP.Data.Offices().ToArrayList(0, 4);
                // Add firm record at top
                string[] aFirmRecord = new string[] { LMP.Data.ForteConstants.mpFirmRecordID.ToString(), "Firm Default" };
                m_oEntities.Insert(0, aFirmRecord);
            }
            else
            {
                if (rdGroups.Checked && m_oGroups == null)
                {
                    //get groups
                    m_oEntities = new LMP.Data.PeopleGroups().ToArrayList(0, 1);

                    // Add firm record at top
                    string[] aFirmRecord = new string[] { LMP.Data.ForteConstants.mpFirmRecordID.ToString(), "Firm Default" };
                    m_oEntities.Insert(0, aFirmRecord);
                }
            }

            if (rdOffices.Checked)
            {
                //update offices list if necessary
                if (m_oOffices == null)
                    m_oOffices = m_oEntities;
                //load offices into control
                m_oEntities = m_oOffices;
                FillEntitiesList(m_oEntities);
            }
            else if (rdGroups.Checked)
            {
                //update groups list if necessary
                if (m_oGroups == null)
                    m_oGroups = m_oEntities;

                //load groups into control
                m_oEntities = m_oGroups;
                FillEntitiesList(m_oEntities);
            }

            //initialize control
            this.cmbEntity.SelectedIndex = 0;
        }
        private void FillEntitiesList(ArrayList aListItems)
        {
            try
            {
                DataTable oDT = new DataTable();

                //add columns to data table
                for (int i = 0; i <= ((object[])aListItems[0]).GetUpperBound(0); i++)
                    oDT.Columns.Add("Column" + i.ToString());

                if (oDT.Columns.Count == 1)
                {
                    //set column as both display and value
                    this.cmbEntity.DisplayMember = oDT.Columns[0].Caption;
                    this.cmbEntity.ValueMember = oDT.Columns[0].Caption;
                }
                else if (oDT.Columns.Count == 2)
                {
                    //set first column for display, second column for value
                    this.cmbEntity.DisplayMember = oDT.Columns[1].Caption;
                    this.cmbEntity.ValueMember = oDT.Columns[0].Caption;
                }
                else
                {
                    //invalid list
                    throw new LMP.Exceptions.ListException(
                        LMP.Resources.GetLangString("Error_InvalidComboboxListArray"));
                }

                //cycle through list item array, adding rows to data table
                for (int i = 0; i < aListItems.Count; i++)
                {
                    DataRow oRow = oDT.Rows.Add();
                    object[] aTemp = (object[])aListItems[i];
                    for (int j = 0; j <= aTemp.GetUpperBound(0); j++)
                        oRow[j] = aTemp[j].ToString();
                }
                //bind list to combobox
                this.cmbEntity.DataSource = oDT;
                this.cmbEntity.Invalidate();
            }
            catch (System.Exception oE)
            {
                throw oE;
            }
        }

        private void cmbEntity_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                if (e.Index < 0)
                {
                    return;
                }
                DataRowView oItem = (DataRowView)cmbEntity.Items[e.Index];
                if (oItem != null)
                {
                    string xCurID = oItem[0].ToString();
                    string xCurName = oItem[1].ToString();
                    KeySet oSettings;
                    if (tabControl1.SelectedTab == this.tabDefaultUserSettings)
                        oSettings = new KeySet(mpPreferenceSetTypes.UserApp, "", xCurID);
                    else
                        oSettings = KeySet.GetFirmAppKeys(xCurID);

                    Font font = cmbEntity.Font;
                    Brush backgroundColor;
                    Brush textColor;

                    if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                    {
                        backgroundColor = SystemBrushes.Highlight;
                        textColor = SystemBrushes.HighlightText;
                    }
                    else
                    {
                        backgroundColor = SystemBrushes.Window;
                        textColor = SystemBrushes.WindowText;
                    }
                    if (xCurID != LMP.Data.ForteConstants.mpFirmRecordID.ToString() && !oSettings.IsDefault)
                    {
                        font = new Font(font, FontStyle.Bold);
                    }
                    e.Graphics.FillRectangle(backgroundColor, e.Bounds);
                    e.Graphics.DrawString(xCurName, font, textColor, e.Bounds);

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG - 3619 - ceh
        /// <summary>
        /// loads the appropriate list of default trailers into the dropdown
        /// </summary>
        private void LoadDefaultTrailers(FirmAppSettings oSettings) //GLOG 6731
        {
            bool bDefaultIsInList = false;

            //get current default trailer
            int iDef = oSettings.DefaultTrailerID; //GLOG 6731

            //get segments of specified type
            LMP.Data.AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs(LMP.Data.mpObjectTypes.Trailer);

            //load Default Trailer combobox with NoTrailerIntegration choice
            ArrayList oTrailers = new ArrayList();
            oTrailers.Insert(0, new object[] { "-999999", LMP.Resources.GetLangString("Prompt_NoTrailerIntegration") });

            int j = 0;

            Array oArray = oDefs.ToArray(1, 0);
            foreach (System.Array a in oArray)
            {
                //get id of trailer type
                int iIndex = (int)a.GetValue(1);

                //get selected trailer IDs
                int[] aIDs = new int[this.lstTrailers.CheckedIndices.Count];
                for (int i = 0; i < this.lstTrailers.CheckedIndices.Count; i++)
                {
                    aIDs[i] = (int)m_oTrailerIndices[this.lstTrailers.CheckedIndices[i]];
                }

                //cycle through default trailers and add to list
                foreach (int i in aIDs)
                {
                    //check for match
                    if (iIndex == i)
                    {
                        j = j + 1;
                        oTrailers.Insert(j, new object[] { a.GetValue(1), a.GetValue(0).ToString() });
                        if (iDef == i)
                            bDefaultIsInList = true;
                        break;
                    }
                }
            }

            //Load Default trailer combobox with selected defaults
            this.cmbTrailerDef.SetList(oTrailers);

            //Load Default trailer
            try
            {
                if (bDefaultIsInList)
                    this.cmbTrailerDef.SelectedValue = iDef;
                else
                    this.cmbTrailerDef.SelectedIndex = 0;
            }
            catch { }

        }
        /// <summary>
        /// Populate Display tab with values from specified keyset
        /// </summary>
        /// <param name="oSettings"></param>
        private void LoadDisplaySettings(FirmApplicationSettings oSettings)
        {
            //GLOG : 6421 : CEH
            m_xPublicFoldersLabel = this.txtPublicFoldersLabel.Text = oSettings.PublicFoldersLabel;
            m_xMyFoldersLabel = this.txtMyFoldersLabel.Text = oSettings.MyFoldersLabel;
            m_xSharedFoldersLabel = this.txtSharedFolderLabel.Text = oSettings.SharedFoldersLabel;
            m_bExpandChildSegment = this.chkExpandChildSegments.Checked = oSettings.ExpandSelectedChildSegment;
            m_bFinishButtonAtEnd = this.chkFinishButtonAtEnd.Checked = oSettings.FinishButtonAtEndOfEditor; //GLOG 8511

            // Set the firm application setting for PeopleListWarningThreshold and MaxPeople.
            // GLOG : 7337 : JSW
            //Set maximum value first so threshold does not exceed maximum
            numUpDnMaxPeople.Value = m_iMaxPeople = oSettings.MaxPeople;
            numUpDnPeopleListWarningThreshold.Value = m_iPeopleListThreshold = oSettings.PeopleListWarningThreshold;

            // GLOG : 3138 : JAB
            // Set the firm application settings for UserContentWarningTheshold and UserContentLimit.
            this.numUpDnUserContentLimit.Value = m_iUserContentLimit = oSettings.UserContentLimit;
            this.numUpDnUserContentThreshold.Value = m_iUserContentThreshold = oSettings.UserContentWarningThreshold;

            // GLOG : 5358 : JSW
            // Set the firm application setting for SavedDataWarningThreshold and SavedDataLimit.
            numUpDnSavedDataLimit.Value = m_iSavedDataLimit = oSettings.SavedDataLimit;
            numUpDnSavedDataThreshold.Value = m_iSavedDataThreshold = oSettings.SavedDataWarningThreshold;

            // GLOG : 3292 : JAB
            // Initialize the My/Public/Shared folders help text.
            txtMyFoldersHelpText.Text = m_xMyFoldersHelpText = oSettings.MyFoldersHelpText;
            txtPublicFoldersHelpText.Text = m_xPublicFoldersHelpText = oSettings.PublicFoldersHelpText;
            txtSharedFoldersHelpText.Text = m_xSharedFoldersHelpText = oSettings.SharedFoldersHelpText;
            // GLOG : 3315 : JAB
            // Set the font name for the firm.
            this.txtHelpFontName.Text = this.m_xHelpFontName = oSettings.HelpFontName;

            // Set the font size for the firm.
            try
            {
                this.numUpDnHelpFontSize.Value = this.m_xHelpFontSize = oSettings.HelpFontSize.ToString();
            }
            catch
            {
                this.numUpDnHelpFontSize.Value = this.m_xHelpFontSize = "12";
            }



        }
        /// <summary>
        /// Populate DMS/Trailers tab with values from specified keyset
        /// </summary>
        /// <param name="oSettings"></param>
        private void LoadDMSTrailerSettings(FirmApplicationSettings oSettings)
        {
            //clear the default trailer ids checkbox
            for (int i = 0; i < this.lstTrailers.Items.Count; i++)
            {
                this.lstTrailers.SetItemChecked(i, false);
            }

            //select the default trailers
            foreach (int i in oSettings.DefaultTrailerIDs)
            {
                //GLOG 7237: Skip any Trailers that have been deleted
                if (m_oTrailerIDs.Contains(i))
                {
                    //get the index of the trailer in the hashtable
                    int iIndex = (int)m_oTrailerIDs[i];
                    this.lstTrailers.SetItemChecked(iIndex, true);
                }
            }

            //GLOG - 3619 - ceh
            LoadDefaultTrailers(oSettings);

            this.cmbDefaultTrailerBehavior.SelectedIndex = (int)oSettings.DefaultTrailerBehavior - 1;

            //GLOG 4283
            this.chkSkipTrailerForTempFiles.Checked = oSettings.SkipTrailerForTempFiles;
            this.chkAllowUsersToSetTrailerBehavior.Checked = oSettings.AllowUsersToSetTrailerBehavior;
            this.chkUse9xStyleTrailer.Checked = oSettings.UseMacPac9xStyleTrailer;
            this.chkPromptForLegacyTrailer.Checked = oSettings.PromptBeforeRemovingLegacyTrailer; //GLOG 6192 (dm)
            FirmAppSettings.mpTrailerIntegrationOptions iOptions = oSettings.TrailerIntegrationOptions;
            this.chkFileOpen.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FileOpen) > 0);
            this.chkFileSave.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FileSave) > 0);
            this.chkFileSaveAs.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FileSaveAs) > 0);
            this.chkFileSaveAll.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FileSaveAll) > 0);
            this.chkFileClose.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FileClose) > 0);
            this.chkFileCloseAll.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FileCloseAll) > 0);
            this.chkFilePrint.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FilePrint) > 0);
            this.chkFilePrintDefault.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FilePrintDefault) > 0);
            this.chkFileExit.Checked = ((iOptions & FirmAppSettings.mpTrailerIntegrationOptions.FileExit) > 0);

            this.cmbDocManager.SelectedValue = oSettings.DMSType;
            this.chkUseFrontEndProfiling.Checked = oSettings.DMSFrontEndProfiling;
            //GLOG 5316
            this.chkCloseOnCancel.Checked = oSettings.DMSFrontEndProfiling && oSettings.DMSCloseOnFEPCancel;
            this.chkCloseOnCancel.Enabled = this.chkUseFrontEndProfiling.Checked;


        }
        /// <summary>
        /// Populate Draft Stamp tab with values from specified keyset
        /// </summary>
        /// <param name="oSettings"></param>
        private void LoadDraftStampSettings(FirmApplicationSettings oSettings)
        {
            //GLOG : 8170 : ceh
            this.chkSetRemoveAllStampsAsFirstInList.Checked = oSettings.SetRemoveAllStampsAsFirstInList;
            //clear the default draft stamp ids checkbox
            for (int i = 0; i < this.lstDraftStamps.Items.Count; i++)
            {
                this.lstDraftStamps.SetItemChecked(i, false);
            }

            //select the default trailers
            foreach (int i in oSettings.DefaultDraftStampIDs)
            {
                //GLOG 7237: Skip any Stamps that have been deleted
                if (m_oDraftStampIDs.Contains(i))
                {
                    //get the index of the trailer in the hashtable
                    int iIndex = (int)m_oDraftStampIDs[i];
                    this.lstDraftStamps.SetItemChecked(iIndex, true);
                }
            }
        }
        /// <summary>
        /// Populate Network tab with values from specified keyset
        /// </summary>
        /// <param name="oSettings"></param>
        private void LoadNetworkSettings(FirmApplicationSettings oSettings)
        {
            m_xNetworkDBServer = this.txtServer.Text = oSettings.NetworkDatabaseServer;
            m_xNetworkDBName = this.txtDatabase.Text = oSettings.NetworkDatabaseName;
            m_xIISServerIPAddress = this.txtIISServerIP.Text = oSettings.IISServerIPAddress;
            m_xServerAppName = this.txtServerAppName.Text = oSettings.ServerApplicationName;
            this.udFreqInterval.Value = (decimal)oSettings.SyncFrequencyInterval;
            m_xFreqInterval = this.udFreqInterval.Value.ToString();
            m_xFreqUnit = this.cmbFreqUnit.Text = oSettings.SyncFrequencyUnit.ToString();
            m_xDayOfWeek = this.cmbDay.Text = oSettings.SyncFrequencyDayOfWeek.ToString();
            m_bLogUserSync = this.chkLogUserSync.Checked = oSettings.LogUserSyncSuccess; //GLOG 5864
            this.dtTime.Value = oSettings.SyncTime;
            //GLOG 4488
            this.cmbSyncPrompt.SelectedValue = (int)oSettings.ScheduledSyncBehavior;
            //GLOG 5392
            if (oSettings.SyncPromptOnCompletion)
                this.cmbSyncCompletePrompt.SelectedValue = 0;
            else
                this.cmbSyncCompletePrompt.SelectedValue = 1;

            m_xTime = this.dtTime.Value.ToString();
            //JTS: 03/18/10
            m_xDistributableFolder = this.txtDistributableFolder.Text = oSettings.NetworkDistributableFolder;
        }
        /// <summary>
        /// Populate Features tab with values from specified keyset
        /// </summary>
        /// <param name="oSettings"></param>
        private void LoadFeaturesSettings(FirmApplicationSettings oSettings)
        {
            //glog - 3637 - CEH
            this.chkAllowSharedSegments.Checked = oSettings.AllowSharedSegments;
            this.chkAllowPrivatePeople.Checked = oSettings.AllowPrivatePeople;
            this.chkAllowProxying.Checked = oSettings.AllowProxying;

            //this.chkAllowPauseResume.Checked = oSettings.AllowPauseResume;
            this.chkAllowDocumentTagRemoval.Checked = oSettings.AllowDocumentTagRemoval;
            this.chkAllowSegmentMemberTagRemoval.Checked = oSettings.AllowSegmentMemberTagRemoval;
            this.chkAllowDocumentMemberTagRemoval.Checked = oSettings.AllowDocumentMemberTagRemoval;
            this.chkAllowVarBlockTagRemoval.Checked = oSettings.AllowVariableBlockTagRemoval;
            this.chkRemoveTextSegmentTags.Checked = oSettings.DeleteTextSegmentTagsOnFinish;
            this.chkFinishOnOpen.Checked = oSettings.FinishDocumentsUponOpen; //GLOG 6131 (dm)

            this.chkAllowNormalStyleUpdates.Checked = oSettings.AllowNormalStyleUpdates;

            this.chkClearUndoStack.Checked = oSettings.ClearUndoStack;

            //GLOG #4291
            this.chkAllowLegacyDocs.Checked = oSettings.AllowLegacyDocumentFunctionality;
            this.chkAlertTagsRemoved.Checked = oSettings.AlertUnusableTagsRemoved;
            this.chkAllowSavedDataCreationOnDeautomation.Checked = oSettings.AllowSavedDataCreationOnDeautomation;
            this.chkAlertWhenNonMP10DocumentOpened.Checked = oSettings.AlertWhenNonForteDocumentOpened;
            this.chkPromptAboutUnconvertedTablesInDesign.Checked = oSettings.PromptAboutUnconvertedTablesInDesign; //GLOG 7873


        }
        /// <summary>
        /// Populate Mail Setup tab with values from specified keyset
        /// </summary>
        /// <param name="oSettings"></param>
        private void LoadMailSettings(FirmApplicationSettings oSettings)
        {
            this.txtAdminEMail.Text = oSettings.MailAdminEMail;
            this.txtMailHost.Text = oSettings.MailHost;
            this.chkUseNetworkCredentials.Checked = oSettings.MailUseNetworkCredentialsToLogin;
            this.txtMailUserID.Text = oSettings.MailUserID;
            this.txtMailPWD.Text = oSettings.MailPassword;
            this.chkUseSSL.Checked = oSettings.MailUseSsl;
            this.txtMailPort.Text = oSettings.MailPort.ToString();
            this.chkAllowErrorEmails.Checked = oSettings.OfferMailForErrors;
        }
        /// <summary>
        /// Populate Security tab with values from specified keyset
        /// </summary>
        /// <param name="oSettings"></param>
        private void LoadSecuritySettings(FirmApplicationSettings oSettings)
        {
            this.chkBase64Encoding.Checked = oSettings.UseBase64Encoding;
            //GLOG : 6421 : CEH
            //load Firm Document Protection Passwords
            string xDocumentProtectionPasswords = oSettings.ProtectedDocumentsPassword;

            string[] axDocumentProtectionPasswords = xDocumentProtectionPasswords.Split(LMP.StringArray.mpEndOfSubField);

            DataTable oDTPass = new DataTable("DocumentProtectionPasswords");
            oDTPass.Columns.Add("Password");
            oDTPass.Columns.Add("Description");

            if (xDocumentProtectionPasswords != "")
            {
                for (int i = 0; i < axDocumentProtectionPasswords.Length; i = i + 2)
                {
                    oDTPass.Rows.Add(axDocumentProtectionPasswords[i], axDocumentProtectionPasswords[i + 1]);
                }
            }

            BindingSource oBSPass = new BindingSource();
            oBSPass.DataSource = oDTPass;
            this.grdPasswords.DataSource = oBSPass;

            this.grdPasswords.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.tbtnPasswordDelete.Enabled = this.grdPasswords.Rows.Count > 0;

        }
        /// <summary>
        /// Populate Other tab with values from specified keyset
        /// </summary>
        /// <param name="oSettings"></param>
        private void LoadOtherSettings(FirmApplicationSettings oSettings)
        {
            m_bShowSegmentSpecificRibbons = this.chkShowSegmentSpecificRibbons.Checked = oSettings.ShowSegmentSpecificRibbons;
            m_xCheckboxFont = this.cmbCheckboxFont.Text = oSettings.CheckboxFont;
            m_xCheckedCheckboxChar = this.txtCheckedCheckboxChar.Text = oSettings.CheckedCheckboxASCII;
            m_xUncheckedCheckboxChar = this.txtUncheckedCheckboxChar.Text = oSettings.UnCheckedCheckboxASCII;
            //GLOG item #6471
            if (LMP.Data.Application.User.FirmSettings.DefaultLabelSegment != 0)
            {
                this.cmbDefaultLabelSegment.SelectedValue =
                    LMP.Data.Application.User.FirmSettings.DefaultLabelSegment.ToString();

                //GLOG 7500: If previous default ID no longer exists, default to first item in list
                if (this.cmbDefaultLabelSegment.SelectedIndex == -1)
                    this.cmbDefaultLabelSegment.SelectedIndex = 0;
            }
            else
            {
                this.cmbDefaultLabelSegment.SelectedIndex = 0;
            }

            if (LMP.Data.Application.User.FirmSettings.DefaultEnvelopeSegment != 0)
            {
                this.cmbDefaultEnvelopeSegment.SelectedValue =
                    LMP.Data.Application.User.FirmSettings.DefaultEnvelopeSegment.ToString();
                //GLOG 7500: If previous default ID no longer exists, default to first item in list
                if (this.cmbDefaultEnvelopeSegment.SelectedIndex == -1)
                    this.cmbDefaultEnvelopeSegment.SelectedIndex = 0;
            }
            else
            {
                this.cmbDefaultEnvelopeSegment.SelectedIndex = 0;
            }

            this.txtBlankWordDocTemplate.Text = oSettings.BlankWordDocTemplate;
            this.txtAdvancedAddressBookPhonePrefix.Text = oSettings.AdvancedAddressbookFormatPhonePrefix;
            this.txtAdvancedAddressBookFaxPrefix.Text = oSettings.AdvancedAddressbookFormatFaxPrefix;
            this.txtAdvancedAddressBookEmailPrefix.Text = oSettings.AdvancedAddressbookFormatEmailPrefix;

            //load default user folders
            string xDefaultUserFolders = oSettings.DefaultUserFolders;
            string[] axDefaultUserFolders = xDefaultUserFolders.Split(';');

            DataTable oDT = new DataTable("DefaultUserFolders");
            oDT.Columns.Add("Name");
            oDT.Columns.Add("Description");

            if (xDefaultUserFolders != "")
            {
                for (int i = 0; i < axDefaultUserFolders.Length; i = i + 2)
                {
                    oDT.Rows.Add(axDefaultUserFolders[i], axDefaultUserFolders[i + 1]);
                }
            }

            BindingSource oBS = new BindingSource();
            oBS.DataSource = oDT;
            this.grdTypes.DataSource = oBS;

            this.grdTypes.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.tbtnDelete.Enabled = this.grdTypes.Rows.Count > 0;

        }
        private void LoadFirmAppSettings()
        {
            this.SuspendLayout();
            try
            {
                if (this.m_oFirmAppSettings != null)
                {
                    //GLOG 6731: Load Settings by tab
                    LoadDisplaySettings(this.m_oFirmAppSettings);
                    LoadDMSTrailerSettings(this.m_oFirmAppSettings);
                    LoadDraftStampSettings(this.m_oFirmAppSettings);
                    LoadNetworkSettings(this.m_oFirmAppSettings);
                    LoadFeaturesSettings(this.m_oFirmAppSettings);
                    LoadMailSettings(this.m_oFirmAppSettings);
                    LoadSecuritySettings(this.m_oFirmAppSettings);
                    LoadOtherSettings(this.m_oFirmAppSettings);
                    SetFirmEntityTypeLabel();
                }

                //GLOG 6731
                if (m_iEntityID == LMP.Data.ForteConstants.mpFirmRecordID)
                {

                    this.gbResetAll.Visible = true;
                    this.gbResetAll.BringToFront();
                    this.gbResetEntity.Visible = false;
                }
                else
                {
                    string xEntityType = "Office";
                    if (rdGroups.Checked)
                        xEntityType = "Group";
                    this.gbResetEntity.Text = "Reset " + xEntityType + " to Firm Defaults";
                    this.gbResetEntity.Visible = !this.m_oFirmAppSettings.IsDefault;
                    this.gbResetEntity.BringToFront();
                    this.gbResetAll.Visible = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.ResumeLayout();
            }
        }
        private void LoadUserAppSettings()
        {
            if (this.m_oUserAppSettings != null)
            {
                m_bDisplayTaskPaneForMPDoc = this.chkDisplayTaskPaneForMPDoc.Checked = this.m_oUserAppSettings.ShowTaskPaneWhenOpeningMPDoc;
                m_bDisplayTaskPaneForNonMPDoc = this.chkDisplayTaskPaneForNonMPDoc.Checked = this.m_oUserAppSettings.ShowTaskPaneWhenOpeningNonMPDoc;
                m_bDisplayTaskPaneForNewBlankDoc = this.chkShowTaskPaneOnBlankNew.Checked = this.m_oUserAppSettings.ShowTaskPaneWhenCreatingBlankMPDocument;
                m_bDisplayTaskPaneWhenNoVariables = this.chkShowTaskpaneWhenNoVariables.Checked = this.m_oUserAppSettings.ShowTaskPaneWhenNoVariables; //GLOG 15933

                m_bShowXMLTags = this.chkShowXMLTags.Checked = this.m_oUserAppSettings.ShowXMLTags;

                m_bFitToPageWidth = this.chkViewPgWidth.Checked = this.m_oUserAppSettings.FitToPageWidth;
                m_bUseGradient = this.chkUseGradient.Checked = this.m_oUserAppSettings.UseGradientInTaskPane;
                m_bCloseTaskPaneOnFinish = this.chkCloseTaskPaneOnFinish.Checked = this.m_oUserAppSettings.CloseTaskPaneOnFinish;
                m_bWarnOnFinish = this.chkWarnOnFinish.Checked = this.m_oUserAppSettings.WarnOnFinish;
                //GLOG : 8308 : JSW
                m_bExpandFindResults = this.chkExpandFindResults.Checked = this.m_oUserAppSettings.ExpandFindResults;
                m_bActivateMacPacRibbonTab = this.chkActivateMacPacRibbonTab.Checked = this.m_oUserAppSettings.ActivateMacPacRibbonTab;
                m_bActivateMacPacRibbonTabOnOpen = this.chkActivateMacPacRibbonTabOnOpen.Checked = this.m_oUserAppSettings.ActivateMacPacRibbonTabOnOpen;
                m_oTaskPaneBackgroundColor = this.pnlTaskPaneColorChip.BackColor = this.m_oUserAppSettings.TaskPaneBackgroundColor;
                m_bShowQuickHelp = this.chkShowQuickHelp.Checked = this.m_oUserAppSettings.ShowHelp;
                //GLOG : 6597 : jsw
                m_bUpdateZoom = this.chkUpdateZoom.Checked = this.m_oUserAppSettings.UpdateZoom;
                if (m_bUpdateZoom == true)
                    this.spnDefaultZoomPercentage.Enabled = true;
                else
                    this.spnDefaultZoomPercentage.Enabled = false;
                this.spnDefaultZoomPercentage.Value = this.m_oUserAppSettings.DefaultPageZoomPercentage;
                m_iDefaultPageZoomPercentage = Int32.Parse(this.spnDefaultZoomPercentage.Value.ToString());

                m_bWarnBeforeDeactivation = this.chkWarnBeforeDeactivation.Checked = this.m_oUserAppSettings.WarnBeforeDeactivation;
            }

            //enable reset buttons for all but firm records
            //GLOG 6731
            if (m_iEntityID == LMP.Data.ForteConstants.mpFirmRecordID)
            {
                this.btnResetUserForAllEntities.Visible = true;
                this.btnResetUserToFirmDefaults.Visible = false;
            }
            else
            {
                string xEntityType = "Office";
                if (rdGroups.Checked)
                    xEntityType = "Group";
                this.btnResetUserToFirmDefaults.Text = "Reset " + xEntityType + " to Firm Defaults";
                this.btnResetUserToFirmDefaults.Visible = !m_oUserAppSettings.IsDefault;
                this.btnResetUserForAllEntities.Visible = false;
            }
           
            SetUserAppEntityTypeLabel();
        }
        /// <summary>
        /// Display additional adminstrative notes for specific DMS backends
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowDMSAdminMessage(string xDMS)
        {
            if (string.IsNullOrEmpty(xDMS))
                return;

            string xMsg = LMP.Resources.GetLangString("Dialog_AppSettings_DMS" + xDMS + "Note");
            if (xMsg != null)
            {
                string xCaption = LMP.Resources.GetLangString("Dialog_AppSettings_DMS" + xDMS + "Caption");
                if (xCaption == null)
                    xCaption = LMP.ComponentProperties.ProductName + " DMS Integration";
                MessageBox.Show(xMsg, xCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void SaveUserAppSettings()
        {
            if ((m_bDisplayTaskPaneForMPDoc != this.chkDisplayTaskPaneForMPDoc.Checked))
                this.m_oUserAppSettings.ShowTaskPaneWhenOpeningMPDoc =
                    this.chkDisplayTaskPaneForMPDoc.Checked;

            if ((m_bDisplayTaskPaneForNonMPDoc != this.chkDisplayTaskPaneForNonMPDoc.Checked))
                this.m_oUserAppSettings.ShowTaskPaneWhenOpeningNonMPDoc =
                    this.chkDisplayTaskPaneForNonMPDoc.Checked;

            if ((m_bDisplayTaskPaneForNewBlankDoc != this.chkShowTaskPaneOnBlankNew.Checked))
                this.m_oUserAppSettings.ShowTaskPaneWhenCreatingBlankMPDocument =
                    this.chkShowTaskPaneOnBlankNew.Checked;

            //GLOG 15933
            if ((m_bDisplayTaskPaneWhenNoVariables != this.chkShowTaskpaneWhenNoVariables.Checked))
                this.m_oUserAppSettings.ShowTaskPaneWhenNoVariables =
                    this.chkShowTaskpaneWhenNoVariables.Checked;

            if ((m_bFitToPageWidth != this.chkViewPgWidth.Checked))
                this.m_oUserAppSettings.FitToPageWidth = this.chkViewPgWidth.Checked;

            //GLOG : 6597 : JSW
            if ((m_bUpdateZoom != this.chkUpdateZoom.Checked))
                this.m_oUserAppSettings.UpdateZoom = this.chkUpdateZoom.Checked;

            if (m_iDefaultPageZoomPercentage != Int32.Parse(this.spnDefaultZoomPercentage.Value.ToString()))
                this.m_oUserAppSettings.DefaultPageZoomPercentage = Int32.Parse(this.spnDefaultZoomPercentage.Value.ToString());

            if ((m_bUseGradient != this.chkUseGradient.Checked))
                this.m_oUserAppSettings.UseGradientInTaskPane = this.chkUseGradient.Checked;

            if ((m_bCloseTaskPaneOnFinish != this.chkCloseTaskPaneOnFinish.Checked))
                this.m_oUserAppSettings.CloseTaskPaneOnFinish = this.chkCloseTaskPaneOnFinish.Checked;

            //GLOG : 6641 : JSW
            if ((m_bWarnOnFinish != this.chkWarnOnFinish.Checked))
                this.m_oUserAppSettings.WarnOnFinish = this.chkWarnOnFinish.Checked;

            //GLOG : 8308 : JSW
            if ((m_bExpandFindResults != this.chkExpandFindResults.Checked))
                this.m_oUserAppSettings.ExpandFindResults = this.chkExpandFindResults.Checked;
            
            if ((m_bActivateMacPacRibbonTab != this.chkActivateMacPacRibbonTab.Checked))
                this.m_oUserAppSettings.ActivateMacPacRibbonTab = this.chkActivateMacPacRibbonTab.Checked;

            //GLOG : 2620 : CEH
            if ((m_bActivateMacPacRibbonTabOnOpen != this.chkActivateMacPacRibbonTabOnOpen.Checked))
                this.m_oUserAppSettings.ActivateMacPacRibbonTabOnOpen = this.chkActivateMacPacRibbonTabOnOpen.Checked;

            if ((this.m_oTaskPaneBackgroundColor != this.pnlTaskPaneColorChip.BackColor))
            {
                this.m_oUserAppSettings.TaskPaneBackgroundColor = this.pnlTaskPaneColorChip.BackColor;
            }

            if ((m_bShowXMLTags != this.chkShowXMLTags.Checked))
                this.m_oUserAppSettings.ShowXMLTags = this.chkShowXMLTags.Checked;

            if ((m_bShowQuickHelp != this.chkShowQuickHelp.Checked))
                this.m_oUserAppSettings.ShowHelp = this.chkShowQuickHelp.Checked;

            //GLOG : 6597 : jsw
            if ((m_bUpdateZoom != this.chkUpdateZoom.Checked))
                this.m_oUserAppSettings.UpdateZoom = this.chkUpdateZoom.Checked;

            if (m_iDefaultPageZoomPercentage != Int32.Parse(this.spnDefaultZoomPercentage.Value.ToString()))
                this.m_oUserAppSettings.DefaultPageZoomPercentage = Int32.Parse(this.spnDefaultZoomPercentage.Value.ToString());

            if ((m_bWarnBeforeDeactivation != this.chkWarnBeforeDeactivation.Checked))
                this.m_oUserAppSettings.WarnBeforeDeactivation = this.chkWarnBeforeDeactivation.Checked;

            //GLOG item #5116 - dcf -
            ////GLOG : 3079 : CEH
            ////GLOG : 3619 : CEH
            //if (this.chkFileOpen.Checked ||
            //    this.chkFileClose.Checked ||
            //    this.chkFileCloseAll.Checked ||
            //    this.chkFileExit.Checked ||
            //    this.chkFilePrint.Checked ||
            //    this.chkFileSave.Checked ||
            //    this.chkFileSaveAs.Checked ||
            //    this.chkFileSaveAll.Checked ||
            //    this.chkFilePrintDefault.Checked)
            //{
            //    if (this.chkDisplayTrailerDialog.Checked)
            //        this.m_oUserAppSettings.TrailerInsertionOption = LMP.Data.TrailerInsertionOptions.Prompt;
            //    else this.m_oUserAppSettings.TrailerInsertionOption = LMP.Data.TrailerInsertionOptions.Silent;
            //}
            //else this.m_oUserAppSettings.TrailerInsertionOption = LMP.Data.TrailerInsertionOptions.Manual;
            //this.m_oUserAppSettings.TrailerInsertionOption = 0;
        }

        /// <summary>
        /// returns True iff some trailer integration
        /// with file management events has been specified
        /// </summary>
        /// <returns></returns>
        private bool TrailerIntegrationIsSpecified()
        {
            return this.chkFileOpen.Checked ||
                this.chkFileClose.Checked ||
                this.chkFileCloseAll.Checked ||
                this.chkFileExit.Checked ||
                this.chkFilePrint.Checked ||
                this.chkFileSave.Checked ||
                this.chkFileSaveAs.Checked ||
                this.chkFileSaveAll.Checked ||
                this.chkFilePrintDefault.Checked;
        }
        /// <summary>
        /// Update specified Keyset with values from Display tab
        /// </summary>
        /// <param name="oSettings"></param>
        /// <param name="bOverride"></param>
        private void SaveDisplaySettings(FirmApplicationSettings oSettings, bool bOverride)
        {
            if ((m_xPublicFoldersLabel != this.txtPublicFoldersLabel.Text) || bOverride)
                oSettings.PublicFoldersLabel = this.txtPublicFoldersLabel.Text;

            if ((m_xMyFoldersLabel != this.txtMyFoldersLabel.Text) || bOverride)
                oSettings.MyFoldersLabel = this.txtMyFoldersLabel.Text;

            if ((m_xSharedFoldersLabel != this.txtSharedFolderLabel.Text) || bOverride)
                oSettings.SharedFoldersLabel = this.txtSharedFolderLabel.Text;

            if ((m_bExpandChildSegment != this.chkExpandChildSegments.Checked) || bOverride)
                oSettings.ExpandSelectedChildSegment = this.chkExpandChildSegments.Checked;

            //GLOG 8511
            if ((m_bFinishButtonAtEnd != this.chkFinishButtonAtEnd.Checked) || bOverride)
                oSettings.FinishButtonAtEndOfEditor = this.chkFinishButtonAtEnd.Checked;
            //GLOG 15817: Changes for GLOG 8223 have been undone here.
            //The fix for that needed to be done at a lower level in KeySet.GetDefaultValue
            // GLOG : 8223 : JSW
            // save if it's not the firm default since it could be dirty
            if (this.numUpDnPeopleListWarningThreshold.Value != m_iPeopleListThreshold || bOverride) //|| (m_iEntityID != LMP.Data.ForteConstants.mpFirmRecordID) || bOverride)
            {
                oSettings.PeopleListWarningThreshold = (int)numUpDnPeopleListWarningThreshold.Value;
            }

            // GLOG : 8223 : JSW
            // save if it's not the firm default since it may be dirty 
            if (numUpDnMaxPeople.Value != m_iMaxPeople || bOverride) //|| (m_iEntityID != LMP.Data.ForteConstants.mpFirmRecordID) || bOverride)
            {
                oSettings.MaxPeople = (int)numUpDnMaxPeople.Value;
            }

            // GLOG : 3138 : JAB
            // Save the firm application settings for the UserContentLimit.
            // GLOG : 8223 : JSW
            // save if it's not the firm default since it may be dirty 
            if (this.numUpDnUserContentLimit.Value != m_iUserContentLimit || bOverride) //|| (m_iEntityID != LMP.Data.ForteConstants.mpFirmRecordID) || bOverride)
            {
                oSettings.UserContentLimit = (int)this.numUpDnUserContentLimit.Value;
            }

            // GLOG : 3138 : JAB
            // Save the firm application settings for the UserContentWarningTheshold.
            // GLOG : 8223 : JSW
            // save if it's not the firm default since it may be dirty 
            if (this.numUpDnUserContentThreshold.Value != m_iUserContentThreshold || bOverride) //|| (m_iEntityID != LMP.Data.ForteConstants.mpFirmRecordID) || bOverride)
            {
                oSettings.UserContentWarningThreshold = (int)this.numUpDnUserContentThreshold.Value;
            }

            // GLOG : 5358 : JSW
            // Save the firm application settings for the SavedDataLimit.
            // GLOG : 8223 : JSW
            // save if it's not the firm default since it may be dirty 
            if (this.numUpDnSavedDataLimit.Value != m_iSavedDataLimit || bOverride) //|| (m_iEntityID != LMP.Data.ForteConstants.mpFirmRecordID) || bOverride)
            {
                oSettings.SavedDataLimit = (int)this.numUpDnSavedDataLimit.Value;
            }

            // GLOG : 5358 : JSW
            // Save the firm application settings for the SavedDataWarningTheshold.
            // GLOG : 8223 : JSW
            // save if it's not the firm default since it may be dirty 
            if (this.numUpDnSavedDataThreshold.Value != m_iSavedDataThreshold || bOverride) //|| (m_iEntityID != LMP.Data.ForteConstants.mpFirmRecordID) || bOverride)
            {
                oSettings.SavedDataWarningThreshold = (int)this.numUpDnSavedDataThreshold.Value;
            }
            // GLOG : 3315 : JAB
            // Save the help font size and name.
            if ((this.txtHelpFontName.Text != m_xHelpFontName) || bOverride)
            {
                oSettings.HelpFontName = this.txtHelpFontName.Text;
            }

            if ((this.numUpDnHelpFontSize.Value != m_xHelpFontSize) || bOverride)
            {
                try
                {
                    oSettings.HelpFontSize = int.Parse(this.numUpDnHelpFontSize.Value);
                }
                catch
                {
                    // In case of any issue, set the help font size to a default size of 8.
                    oSettings.HelpFontSize = 8;
                }
            }

            // GLOG : 3292 : JAB
            // Save the My/Public/Shared folders help text.
            if ((m_xMyFoldersHelpText != this.txtMyFoldersHelpText.Text) || bOverride)
            {
                oSettings.MyFoldersHelpText = this.txtMyFoldersHelpText.Text;
            }

            if ((m_xPublicFoldersHelpText != this.txtPublicFoldersHelpText.Text) || bOverride)
            {
                oSettings.PublicFoldersHelpText = this.txtPublicFoldersHelpText.Text;
            }

            if ((m_xSharedFoldersHelpText != this.txtSharedFoldersHelpText.Text) || bOverride)
            {
                oSettings.SharedFoldersHelpText = this.txtSharedFoldersHelpText.Text;
            }

        }
        /// <summary>
        /// Update specified Keyset with values from DMS/Trailers tab
        /// </summary>
        /// <param name="oSettings"></param>
        /// <param name="bOverride"></param>
        private void SaveDMSTrailerSettings(FirmApplicationSettings oSettings, bool bOverride)
        {
            //get selected trailer IDs
            int[] aIDs = new int[this.lstTrailers.CheckedIndices.Count];
            for (int i = 0; i < this.lstTrailers.CheckedIndices.Count; i++)
            {
                aIDs[i] = (int)m_oTrailerIndices[this.lstTrailers.CheckedIndices[i]];
            }

            oSettings.DefaultTrailerIDs = aIDs;

            if (!string.IsNullOrEmpty(cmbTrailerDef.SelectedValue.ToString()))
                oSettings.DefaultTrailerID = Int32.Parse(cmbTrailerDef.SelectedValue.ToString());
            else
                oSettings.DefaultTrailerID = 0;

            oSettings.DefaultTrailerBehavior =
                (LMP.Data.TrailerInsertionOptions)(this.cmbDefaultTrailerBehavior.SelectedIndex + 1);

            //GLOG 4243 - No trailer integration for temp locations
            oSettings.SkipTrailerForTempFiles = this.chkSkipTrailerForTempFiles.Checked;

            oSettings.AllowUsersToSetTrailerBehavior = this.chkAllowUsersToSetTrailerBehavior.Checked;
            oSettings.UseMacPac9xStyleTrailer = this.chkUse9xStyleTrailer.Checked;
            oSettings.PromptBeforeRemovingLegacyTrailer = this.chkPromptForLegacyTrailer.Checked; //GLOG 6192 (dm)
            //DMS Type
            oSettings.DMSType = Int32.Parse(this.cmbDocManager.SelectedValue.ToString());
            oSettings.DMSFrontEndProfiling = this.chkUseFrontEndProfiling.Checked;
            //GLOG 5316
            oSettings.DMSCloseOnFEPCancel = this.chkCloseOnCancel.Checked;
            //Get Trailer Integration selections
            FirmAppSettings.mpTrailerIntegrationOptions iOptions = 0;
            if (this.chkFileOpen.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FileOpen;
            if (this.chkFileSave.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FileSave;
            if (this.chkFileSaveAs.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FileSaveAs;
            if (this.chkFileSaveAll.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FileSaveAll;
            if (this.chkFileClose.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FileClose;
            if (this.chkFileCloseAll.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FileCloseAll;
            if (this.chkFilePrint.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FilePrint;
            if (this.chkFilePrintDefault.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FilePrintDefault;
            if (this.chkFileExit.Checked)
                iOptions = iOptions | FirmAppSettings.mpTrailerIntegrationOptions.FileExit;

            oSettings.TrailerIntegrationOptions = iOptions;
            oSettings.DefaultTrailerBehavior =
                (LMP.Data.TrailerInsertionOptions)this.cmbDefaultTrailerBehavior.SelectedIndex + 1;


        }
        /// <summary>
        /// Update specified Keyset with values from Draft Stamp tab
        /// </summary>
        /// <param name="oSettings"></param>
        /// <param name="bOverride"></param>
        private void SaveDraftStampSettings(FirmApplicationSettings oSettings, bool bOverride)
        {
            //get selected draft stamp IDs
            int[] aIDs = new int[this.lstDraftStamps.CheckedIndices.Count];
            for (int i = 0; i < this.lstDraftStamps.CheckedIndices.Count; i++)
            {
                aIDs[i] = (int)m_oDraftStampIndices[this.lstDraftStamps.CheckedIndices[i]];
            }

            oSettings.DefaultDraftStampIDs = aIDs;
            oSettings.SetRemoveAllStampsAsFirstInList = this.chkSetRemoveAllStampsAsFirstInList.Checked; //GLOG : 8170 : ceh

        }
        /// <summary>
        /// Update specified Keyset with values from Network tab
        /// </summary>
        /// <param name="oSettings"></param>
        /// <param name="bOverride"></param>
        private void SaveNetworkSettings(FirmApplicationSettings oSettings, bool bOverride)
        {
            if ((m_xNetworkDBServer != this.txtServer.Text) || bOverride)
                oSettings.NetworkDatabaseServer = this.txtServer.Text;

            if ((m_xNetworkDBName != this.txtDatabase.Text) || bOverride)
                oSettings.NetworkDatabaseName = this.txtDatabase.Text;

            if ((m_xIISServerIPAddress != this.txtIISServerIP.Text) || bOverride)
                oSettings.IISServerIPAddress = this.txtIISServerIP.Text;

            if ((m_xServerAppName != this.txtServerAppName.Text) || bOverride)
                oSettings.ServerApplicationName = this.txtServerAppName.Text;

            //JTS: 03/18/10
            if ((m_xDistributableFolder != this.txtDistributableFolder.Text) || bOverride)
            {
                oSettings.NetworkDistributableFolder = this.txtDistributableFolder.Text;
            }

            if ((m_xFreqUnit != this.cmbFreqUnit.Text) || bOverride)
                oSettings.SyncFrequencyUnit =
                    (LMP.Data.mpFrequencyUnits)Enum.Parse(typeof(LMP.Data.mpFrequencyUnits), this.cmbFreqUnit.Text);

            if ((m_xFreqInterval != this.udFreqInterval.Value.ToString()) || bOverride)
                oSettings.SyncFrequencyInterval = (int)this.udFreqInterval.Value;

            if ((m_xDayOfWeek != this.cmbDay.Text) || bOverride)
                oSettings.SyncFrequencyDayOfWeek =
                (LMP.Data.mpDayOfWeek)Enum.Parse(typeof(LMP.Data.mpDayOfWeek), this.cmbDay.Text);

            if ((m_xTime != this.dtTime.Value.ToString()) || bOverride)
                oSettings.SyncTime = this.dtTime.Value;

            //GLOG 4488
            oSettings.ScheduledSyncBehavior = (FirmAppSettings.mpScheduledSyncBehavior)Int32.Parse(cmbSyncPrompt.SelectedValue.ToString());

            //GLOG 5392
            if (cmbSyncCompletePrompt.SelectedValue.ToString() == "0")
                oSettings.SyncPromptOnCompletion = true;
            else
                oSettings.SyncPromptOnCompletion = false;

            //GLOG 5864
            if ((m_bLogUserSync != this.chkLogUserSync.Checked) || bOverride)
                oSettings.LogUserSyncSuccess = chkLogUserSync.Checked;

        }
        /// <summary>
        /// Update specified Keyset with values from Features tab
        /// </summary>
        /// <param name="oSettings"></param>
        /// <param name="bOverride"></param>
        private void SaveFeaturesSettings(FirmApplicationSettings oSettings, bool bOverride)
        {
            //glog - 3637 - CEH
            oSettings.AllowSharedSegments = this.chkAllowSharedSegments.Checked;
            oSettings.AllowPrivatePeople = this.chkAllowPrivatePeople.Checked;
            oSettings.AllowProxying = this.chkAllowProxying.Checked;

            //oSettings.AllowPauseResume = this.chkAllowPauseResume.Checked;
            oSettings.AllowDocumentTagRemoval = this.chkAllowDocumentTagRemoval.Checked;
            oSettings.AllowSegmentMemberTagRemoval = this.chkAllowSegmentMemberTagRemoval.Checked;
            oSettings.AllowDocumentMemberTagRemoval = this.chkAllowDocumentMemberTagRemoval.Checked;
            oSettings.AllowVariableBlockTagRemoval = this.chkAllowVarBlockTagRemoval.Checked;
            oSettings.DeleteTextSegmentTagsOnFinish = this.chkRemoveTextSegmentTags.Checked;
            oSettings.FinishDocumentsUponOpen = this.chkFinishOnOpen.Checked; //GLOG 6131 (dm)

            oSettings.AllowNormalStyleUpdates = this.chkAllowNormalStyleUpdates.Checked;

            oSettings.ClearUndoStack = this.chkClearUndoStack.Checked;

            oSettings.AlertUnusableTagsRemoved = this.chkAlertTagsRemoved.Checked;
            oSettings.AlertWhenNonForteDocumentOpened = this.chkAlertWhenNonMP10DocumentOpened.Checked;
            oSettings.PromptAboutUnconvertedTablesInDesign = this.chkPromptAboutUnconvertedTablesInDesign.Checked; //GLOG 7873
            oSettings.AllowSavedDataCreationOnDeautomation = this.chkAllowSavedDataCreationOnDeautomation.Checked;
            //GLOG #4291
            oSettings.AllowLegacyDocumentFunctionality = this.chkAllowLegacyDocs.Checked;


        }
        /// <summary>
        /// Update specified Keyset with values from Mail Setup tab
        /// </summary>
        /// <param name="oSettings"></param>
        /// <param name="bOverride"></param>
        private void SaveMailSettings(FirmApplicationSettings oSettings, bool bOverride)
        {
            oSettings.MailAdminEMail = this.txtAdminEMail.Text;
            oSettings.MailHost = this.txtMailHost.Text;
            oSettings.MailUserID = this.txtMailUserID.Text;
            oSettings.MailPassword = this.txtMailPWD.Text;
            oSettings.MailUseNetworkCredentialsToLogin = this.chkUseNetworkCredentials.Checked;
            oSettings.MailUseSsl = this.chkUseSSL.Checked;
            oSettings.MailPort = int.Parse(this.txtMailPort.Text);
            oSettings.OfferMailForErrors = this.chkAllowErrorEmails.Checked;
        }
        /// <summary>
        /// Update specified Keyset with values from Security tab
        /// </summary>
        /// <param name="oSettings"></param>
        /// <param name="bOverride"></param>
        private void SaveSecuritySettings(FirmApplicationSettings oSettings, bool bOverride)
        {
            //GLOG : 6421 : CEH
            //if ((m_xFirmProtectionPassword != this.txtProtectionPassword.Text))
            //oSettings.ProtectedDocumentsPassword = this.txtProtectionPassword.Text;
            oSettings.UseBase64Encoding = this.chkBase64Encoding.Checked;
            //GLOG : 6421 : CEH
            //set firm document protection passwords folders
            BindingSource oBSPass = (BindingSource)this.grdPasswords.DataSource;
            DataTable oDTPass = (DataTable)oBSPass.DataSource;

            string xProtectionPasswords = "";

            foreach (DataRow oRow in oDTPass.Rows)
            {
                xProtectionPasswords = xProtectionPasswords + oRow[0] + LMP.StringArray.mpEndOfSubField + oRow[1] + LMP.StringArray.mpEndOfSubField;
            }

            xProtectionPasswords = xProtectionPasswords.Substring(0, Math.Max(xProtectionPasswords.Length - 1, 0));

            if ((oSettings.ProtectedDocumentsPassword != xProtectionPasswords) || bOverride)
            {
                oSettings.ProtectedDocumentsPassword = xProtectionPasswords;
            }
        }
        /// <summary>
        /// Update specified Keyset with values from Other tab
        /// </summary>
        /// <param name="oSettings"></param>
        /// <param name="bOverride"></param>
        private void SaveOtherSettings(FirmApplicationSettings oSettings, bool bOverride)
        {
            if ((m_bShowSegmentSpecificRibbons != this.chkShowSegmentSpecificRibbons.Checked) || bOverride)
                oSettings.ShowSegmentSpecificRibbons = this.chkShowSegmentSpecificRibbons.Checked;
            //if (m_xCheckboxFont != this.cmbCheckboxFont.Text)
            //    oSettings.CheckboxFont = this.cmbCheckboxFont.Text;

            //if (m_xCheckedCheckboxChar != this.txtCheckedCheckboxChar.Text)
            //    oSettings.CheckedCheckboxASCII = this.txtCheckedCheckboxChar.Text;

            //if (m_xUncheckedCheckboxChar != this.txtUncheckedCheckboxChar.Text)
            //    oSettings.UnCheckedCheckboxASCII = this.txtUncheckedCheckboxChar.Text;

            //GLOG item #6471 - dcf
            if (this.cmbDefaultLabelSegment.SelectedValue != null) //GLOG 7500
                oSettings.DefaultLabelSegment = int.Parse(this.cmbDefaultLabelSegment.SelectedValue.ToString());
            if (this.cmbDefaultEnvelopeSegment.SelectedValue != null) //GLOG 7500
                oSettings.DefaultEnvelopeSegment = int.Parse(this.cmbDefaultEnvelopeSegment.SelectedValue.ToString());

            oSettings.BlankWordDocTemplate = this.txtBlankWordDocTemplate.Text;
            oSettings.AdvancedAddressbookFormatPhonePrefix = this.txtAdvancedAddressBookPhonePrefix.Text;
            oSettings.AdvancedAddressbookFormatFaxPrefix = this.txtAdvancedAddressBookFaxPrefix.Text;
            oSettings.AdvancedAddressbookFormatEmailPrefix = this.txtAdvancedAddressBookEmailPrefix.Text;

            //get default user folders
            BindingSource oBS = (BindingSource)this.grdTypes.DataSource;
            DataTable oDT = (DataTable)oBS.DataSource;

            string xUserFolders = "";

            foreach (DataRow oRow in oDT.Rows)
            {
                xUserFolders = xUserFolders + oRow[0] + ";" + oRow[1] + ";";
            }

            xUserFolders = xUserFolders.Substring(0, Math.Max(xUserFolders.Length - 1, 0));

            if ((oSettings.DefaultUserFolders != xUserFolders) || bOverride)
            {
                oSettings.DefaultUserFolders = xUserFolders;
            }


        }
        private void SaveFirmAppSettings()
        {
            //GLOG 6731: Save settings by tab
            SaveDisplaySettings(this.m_oFirmAppSettings, false);
            SaveDMSTrailerSettings(this.m_oFirmAppSettings, false);
            SaveDraftStampSettings(this.m_oFirmAppSettings, false);
            SaveNetworkSettings(this.m_oFirmAppSettings, false);
            SaveFeaturesSettings(this.m_oFirmAppSettings, false);
            SaveMailSettings(this.m_oFirmAppSettings, false);
            SaveSecuritySettings(this.m_oFirmAppSettings, false);
            SaveOtherSettings(this.m_oFirmAppSettings, false);
        }

        private void ResetToFirmDefaults()
        {
            m_iEntityID = this.SelectedEntityID;

            if (m_iEntityID != 0 && m_iEntityID != LMP.Data.ForteConstants.mpFirmRecordID)
            {
                if (this.m_oFirmAppSettings != null)
                {
                    this.m_oFirmAppSettings.Delete();

                    this.m_oFirmAppSettings = new LMP.Data.FirmApplicationSettings(m_iEntityID);
                    LoadFirmAppSettings();
                }
            }
        }
        private void ResetUserToFirmDefaults()
        {
            m_iEntityID = this.SelectedEntityID;

            if (m_iEntityID != 0 && m_iEntityID != LMP.Data.ForteConstants.mpFirmRecordID)
            {
                //GLOG 6731
                if (this.m_oUserAppSettings != null && !this.m_oUserAppSettings.IsDefault)
                {
                    //GLOG 6731
                    try
                    {
                        this.m_oUserAppSettings.Delete();
                    }
                    catch (LMP.Exceptions.ItemDeleteException)
                    {
                        // GLOG : 2968 : JAB
                        // The ItemDeleteException is caused when attempting to delete a read only keyset.
                        // This is the case during the process of resetting the user app setting and 
                        // it is already set to the firm default.
                    }
                    this.m_oUserAppSettings = new LMP.Data.UserApplicationSettings(m_iEntityID);
                    LoadUserAppSettings();
                }
            }
        }
        private void LoadApplicationSettings()
        {
            m_iEntityID = this.SelectedEntityID;

            if (m_iEntityID != 0)
            {
                this.m_oFirmAppSettings = new LMP.Data.FirmApplicationSettings(m_iEntityID);
                this.m_oUserAppSettings = new LMP.Data.UserApplicationSettings(m_iEntityID);
                LoadFirmAppSettings();
                LoadUserAppSettings();
            }
        }
        private void SaveApplicationSettings()
        {
            if (this.m_oFirmAppSettings != null)
            {
                SaveFirmAppSettings();
            }

            if (this.m_oUserAppSettings != null)
            {
                SaveUserAppSettings();
            }
        }
        /// <summary>
        /// sets the label that shows if the settings
        /// are a default setting or a unique setting
        /// </summary>
        private void SetFirmEntityTypeLabel()
        {
            //GLOG 6731
            this.tabFirmSettings.Text = "Firm Settings";
            if (this.cmbEntity.Text != "Firm Default" && m_oFirmAppSettings != null)
            {
                if (this.m_oFirmAppSettings.IsDefault)
                    this.tabFirmSettings.Text += " - " +
                        m_oFirmAppSettings.SettingType.ToString() + " Defaults";
                else
                    this.tabFirmSettings.Text += " - " + (rdGroups.Checked ? "Group" : "Office") + " Defaults";
            }
        }
        /// <summary>
        /// sets the label that shows if the settings
        /// are a default setting or a unique setting
        /// </summary>
        private void SetUserAppEntityTypeLabel()
        {
            //GLOG 6731
            this.tabDefaultUserSettings.Text = "Default User Settings";
            if (this.cmbEntity.Text != "Firm Default" && m_oUserAppSettings != null)
            {
                if (this.m_oUserAppSettings.IsDefault)
                    this.tabDefaultUserSettings.Text += " - Firm Defaults";
                else
                    this.tabDefaultUserSettings.Text += " - " + (rdGroups.Checked ? "Group" : "Office") + " Defaults";
            }
        }
        public override void SaveCurrentRecord()
        {
            try
            {
                if (this.m_oFirmAppSettings != null)
                {
                    SaveApplicationSettings();
                }
                base.SaveCurrentRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// sets the label that describes the 
        /// </summary>
        private void UpdateSyncScheduleDescription()
        {
            int iInterval = (int)this.udFreqInterval.Value;
            string xFreqUnit = this.cmbFreqUnit.Text.ToLower();
            string xDay = this.cmbDay.Text;
            string xDesc = "";

            if (iInterval > 1)
                xDesc = "Users will synchronize with the network database every " +
                    iInterval.ToString() + " " + xFreqUnit + "s";
            else
                xDesc = "Users will synchronize with the network database every " + xFreqUnit;

            if (xFreqUnit.StartsWith("week"))
                xDesc += " on " + xDay + " at " + this.dtTime.Value.ToShortTimeString();

            if (xFreqUnit.StartsWith("day"))
                xDesc += " at " + this.dtTime.Value.ToShortTimeString();

            xDesc += ".";

            this.lblCurSynScheduleDesc.Text = xDesc;
        }
        private string GetHelpText(string xInitialHelpText, string xFormCaption)
        {
            HelpTextForm oForm = new HelpTextForm(xInitialHelpText, xFormCaption);
            return (oForm.ShowDialog(this) == DialogResult.OK) ? oForm.HelpText : xInitialHelpText;
        }

        //GLOG - 3619 - CEH
        //GLOG - 3079 - CEH
        private void SetTrailerBehaviorControls()
        {
            //GLOG 3929:  State of these checkboxes should not be connected to 
            //integration point checkboxes, because some backends integrate via
            //Startup template instead of using the Word command triggers.
            //if (!this.chkFileOpen.Checked &&
            //    !this.chkFileClose.Checked &&
            //    !this.chkFileCloseAll.Checked &&
            //    !this.chkFileExit.Checked &&
            //    !this.chkFilePrint.Checked &&
            //    !this.chkFileSave.Checked &&
            //    !this.chkFileSaveAs.Checked &&
            //    !this.chkFileSaveAll.Checked &&
            //    !this.chkFilePrintDefault.Checked)
            //{
            //    this.chkDisplayTrailerDialog.Checked = false;
            //    this.chkDisplayTrailerDialog.Enabled = false;
            //    this.chkDisplayTrailerOptionsUserPrefs.Checked = false;
            //    this.chkDisplayTrailerOptionsUserPrefs.Enabled = false;
            //}
            //else
            //{
            //    this.chkDisplayTrailerDialog.Enabled = true;
            //    this.chkDisplayTrailerOptionsUserPrefs.Enabled = true;
            //}

            //bool bTrailerIntegration = TrailerIntegrationIsSpecified();
            //this.chkUseFrontEndProfiling.Enabled = bTrailerIntegration;
            //this.chkSkipTrailerForTempFiles.Enabled = bTrailerIntegration;
            //this.lblDefaultTrailerBehavior.Enabled = bTrailerIntegration;
            //this.cmbDefaultTrailerBehavior.Enabled = bTrailerIntegration;
            //this.chkAllowUsersToSetTrailerBehavior.Enabled = bTrailerIntegration;

            //if (!bTrailerIntegration)
            //{
            //    this.chkUseFrontEndProfiling.Checked = false;
            //    this.chkSkipTrailerForTempFiles.Checked = false;
            //        this.cmbDefaultTrailerBehavior.SelectedIndex = 0;
            //    this.chkAllowUsersToSetTrailerBehavior.Checked = false;
            //}
        }
        /// <summary>
        /// Update specified keyset with Firm settings from current tab 
        /// </summary>
        /// <param name="oSettings"></param>
        private void UpdateCurrentTabValues(FirmApplicationSettings oSettings)
        {
            //GLOG 6731
            switch (tabFirmAppSettings.SelectedTab.Name)
            {
                case "tabDisplay":
                    SaveDisplaySettings(oSettings, true);
                    break;
                case "tabTrailers":
                    SaveDMSTrailerSettings(oSettings, true);
                    break;
                case "tabDraftStamps":
                    SaveDraftStampSettings(oSettings, true);
                    break;
                case "tabSync":
                    SaveNetworkSettings(oSettings, true);
                    break;
                case "tabFeatures":
                    SaveFeaturesSettings(oSettings, true);
                    break;
                case "tabMailSetup":
                    SaveMailSettings(oSettings, true);
                    break;
                case "tabSecurity":
                    SaveSecuritySettings(oSettings, true);
                    break;
                case "tabOther":
                    SaveOtherSettings(oSettings, true);
                    break;
            }
        }
        #endregion
        #region ********************event handlers******************
        private void rdOffices_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //load offices into entities dropdown
                LoadEntitiesList();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void AppSettingsManager_Load(object sender, EventArgs e)
        {
            try
            {
                base.AfterControlLoaded += new AfterControlLoadedHandler(AppSettingsManager_AfterControlLoaded);
                base.BeforeControlUnloaded += new BeforeControlUnLoadedHandler(AppSettingsManager_BeforeControlUnloaded);

                //GLOG - 3619 - ceh
                //get segments of specified type
                LMP.Data.AdminSegmentDefs oDefs = new LMP.Data.AdminSegmentDefs(LMP.Data.mpObjectTypes.Trailer);

                Array oArray = oDefs.ToArray(1, 0);
                foreach (System.Array a in oArray)
                {
                    //add segment display name to the list
                    int iIndex = this.lstTrailers.Items.Add(a.GetValue(0).ToString());

                    //store in field hashtables for ID mapping
                    m_oTrailerIDs.Add(a.GetValue(1), iIndex);
                    m_oTrailerIndices.Add(iIndex, a.GetValue(1));
                }

                //DMS Options
                string[,] aDMSList = new string[5, 2]; //GLOG 7602
                //GLOG 8423: Removed obsolete/unsupported backends
                aDMSList[0, 0] = LMP.Resources.GetLangString("Prompt_DMS_None");
                aDMSList[0, 1] = "0";
                aDMSList[1, 0] = LMP.Resources.GetLangString("Prompt_DMS_WorkSite");
                aDMSList[1, 1] = "3";
                aDMSList[2, 0] = LMP.Resources.GetLangString("Prompt_DMS_Work10");
                aDMSList[2, 1] = "12";
                //aDMSList[2, 0] = LMP.Resources.GetLangString("Prompt_DMS_NetDocuments");
                //aDMSList[2, 1] = "6";
                //GLOG 7602
                aDMSList[3, 0] = LMP.Resources.GetLangString("Prompt_DMS_ndOffice");
                aDMSList[3, 1] = "10";
                //aDMSList[4, 0] = LMP.Resources.GetLangString("Prompt_DMS_DocsOpen");
                //DMSList[4, 1] = "1";
                aDMSList[4, 0] = LMP.Resources.GetLangString("Prompt_DMS_DMCOM");
                aDMSList[4, 1] = "7";
                //aDMSList[6, 0] = LMP.Resources.GetLangString("Prompt_DMS_DMODMA");
                //aDMSList[6, 1] = "5";
                this.cmbDocManager.SetList(aDMSList);
                oDefs = new LMP.Data.AdminSegmentDefs(LMP.Data.mpObjectTypes.DraftStamp);
                oArray = oDefs.ToArray(1, 0);
                foreach (System.Array a in oArray)
                {
                    //add segment display name to the list
                    int iIndex = this.lstDraftStamps.Items.Add(a.GetValue(0).ToString());

                    //store in field hashtables for ID mapping
                    m_oDraftStampIDs.Add(a.GetValue(1), iIndex);
                    m_oDraftStampIndices.Add(iIndex, a.GetValue(1));
                }

                SetTrailerBehaviorControls();

                string[,] aSyncPromptOptions = new string[3, 2];
                aSyncPromptOptions[0, 0] = "prompt User to allow synch to run.";
                aSyncPromptOptions[0, 1] = "0";
                aSyncPromptOptions[1, 0] = "allow synch to run without prompting. (Recommended)";
                aSyncPromptOptions[1, 1] = "1";
                aSyncPromptOptions[2, 0] = "do not allow synch to run.";
                aSyncPromptOptions[2, 1] = "2";
                this.cmbSyncPrompt.SetList(aSyncPromptOptions);

                //GLOG 5392: Options for behavior when Word is running when scheduled sync completes
                string[,] aSyncCompletePromptOptions = new string[2, 2];
                aSyncCompletePromptOptions[0, 0] = "prompt User to restart Word.";
                aSyncCompletePromptOptions[0, 1] = "0";
                aSyncCompletePromptOptions[1, 0] = "install updates next Session without prompting. (Recommended)"; //GLOG : 8667 : jsw
                aSyncCompletePromptOptions[1, 1] = "1";
                this.cmbSyncCompletePrompt.SetList(aSyncCompletePromptOptions);

                //GLOG item #6471 - dcf
                AdminSegmentDefs oLabelDefs = new AdminSegmentDefs(mpObjectTypes.Labels,
                    LMP.Data.Application.User.ID.ToString(), 0, 0, 0, 0, 0, true);

                ArrayList aSegList = oLabelDefs.ToTwoColArrayList(1, 0);
                //GLOG 7952: Create separate ArrayList of unique values
                ArrayList aListItems = new ArrayList();
                string xIDs = "";
                foreach (TwoColumnListObject oItem in aSegList)
                {
                    //Skip if already added
                    if (xIDs.IndexOf("|" + oItem.ValueMember + "|") == -1) //GLOG 7951
                    {
                        aListItems.Add(oItem);
                        //avoid duplicates
                        xIDs += "|" + oItem.ValueMember + "|";
                    }
                }
                this.cmbDefaultLabelSegment.DataSource = aListItems;
                this.cmbDefaultLabelSegment.DisplayMember = "DisplayMember";
                this.cmbDefaultLabelSegment.ValueMember = "ValueMember";

                //GLOG item #6471 - dcf
                AdminSegmentDefs oEnvelopeDefs = new AdminSegmentDefs(mpObjectTypes.Envelopes,
                    LMP.Data.Application.User.ID.ToString(), 0, 0, 0, 0, 0, true);

                aSegList = oEnvelopeDefs.ToTwoColArrayList(1, 0);
                //GLOG 7952: Create separate ArrayList of unique values
                aListItems = new ArrayList();
                xIDs = "";
                foreach (TwoColumnListObject oItem in aSegList)
                {
                    //Skip if already added
                    if (xIDs.IndexOf("|" + oItem.ValueMember + "|") == -1) //GLOG 7951
                    {
                        aListItems.Add(oItem);
                        //avoid duplicates
                        xIDs += "|" + oItem.ValueMember + "|";
                    }
                }

                this.cmbDefaultEnvelopeSegment.DataSource = aListItems;
                this.cmbDefaultEnvelopeSegment.DisplayMember = "DisplayMember";
                this.cmbDefaultEnvelopeSegment.ValueMember = "ValueMember";

                //GLOG : 7998 : ceh
                if (LMP.MacPac.MacPacImplementation.IsToolkit)
                {
                    //hide & remove
                    this.lblOwnerCategory.Visible = false;
                    this.rdOffices.Visible = false;
                    this.rdGroups.Visible = false;
                    this.lblEntity.Visible = false;
                    this.cmbEntity.Visible = false;
                    this.btnResetToFirmDefaults.Visible = false;
                    this.tabControl1.TabPages.Remove(this.tabDefaultUserSettings);
                    this.tabFirmAppSettings.TabPages.Remove(this.tabDisplay);
                    this.tabFirmAppSettings.TabPages.Remove(this.tabSync);
                    this.tabFirmAppSettings.TabPages.Remove(this.tabFeatures);
                    this.tabFirmAppSettings.TabPages.Remove(this.tabMailSetup);
                    this.chkAllowUsersToSetTrailerBehavior.Visible = false;
                    this.tsMain.Visible = false;
                    this.grdTypes.Visible = false;
                    this.lblBlankWordDocTemplate.Visible = false;
                    this.txtBlankWordDocTemplate.Visible = false;

                    //reposition
                    this.tabControl1.Location = new System.Drawing.Point(this.tabControl1.Location.X, 10);
                    this.chkPromptForLegacyTrailer.Location = this.chkSkipTrailerForTempFiles.Location;
                    this.chkSkipTrailerForTempFiles.Location = new System.Drawing.Point(this.chkSkipTrailerForTempFiles.Location.X, this.chkAllowUsersToSetTrailerBehavior.Location.Y);
                    this.lblDefaultLabelSegment.Location = new System.Drawing.Point(this.lblDefaultLabelSegment.Location.X, 18);
                    this.lblDefaultEnvelopeSegment.Location = new System.Drawing.Point(this.lblDefaultEnvelopeSegment.Location.X, 55);
                    this.cmbDefaultLabelSegment.Location = new System.Drawing.Point(this.cmbDefaultLabelSegment.Location.X, 18);
                    this.cmbDefaultEnvelopeSegment.Location = new System.Drawing.Point(this.cmbDefaultEnvelopeSegment.Location.X, 55);
                    this.lblAdvancedAddressBookFormatPrefixes.Location = new System.Drawing.Point(this.lblAdvancedAddressBookFormatPrefixes.Location.X, 92);
                    this.lblAdvancedAddressBookPhonePrefix.Location = new System.Drawing.Point(this.lblAdvancedAddressBookPhonePrefix.Location.X, 92);
                    this.txtAdvancedAddressBookPhonePrefix.Location = new System.Drawing.Point(this.txtAdvancedAddressBookPhonePrefix.Location.X, 92);
                    this.lblAdvancedAddressBookFaxPrefix.Location = new System.Drawing.Point(this.lblAdvancedAddressBookFaxPrefix.Location.X, 92);
                    this.txtAdvancedAddressBookFaxPrefix.Location = new System.Drawing.Point(this.txtAdvancedAddressBookFaxPrefix.Location.X, 92);
                    this.lblAdvancedAddressBookEmailPrefix.Location = new System.Drawing.Point(this.lblAdvancedAddressBookEmailPrefix.Location.X, 92);
                    this.txtAdvancedAddressBookEmailPrefix.Location = new System.Drawing.Point(this.txtAdvancedAddressBookEmailPrefix.Location.X, 92);
                }
                else if (LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                {
                    this.lblSharedFoldersLabel.Visible = false;
                    this.lblSharedFoldersHelpText.Visible = false;
                    this.txtSharedFolderLabel.Visible = false;
                    this.txtSharedFoldersHelpText.Visible = false;
                    this.btnSharedFoldersHelpText.Visible = false;
                    this.label4.Visible = false;
                    this.tabFirmAppSettings.TabPages.Remove(this.tabSync);
                    this.tabFirmAppSettings.TabPages.Remove(this.tabMailSetup);
                    this.chkAllowProxying.Enabled = false;
                    this.chkAllowSharedSegments.Enabled = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void AppSettingsManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {
                this.rdOffices.Checked = true;
                LoadApplicationSettings();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void AppSettingsManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                if (this.m_oFirmAppSettings != null)
                {
                    SaveApplicationSettings();
                }
                base.SaveCurrentRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnResetToFirmDefaults_Click(object sender, EventArgs e)
        {
            try
            {
                ResetToFirmDefaults();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnResetUserToFirmDefaults_Click(object sender, EventArgs e)
        {
            // GLOG : 2968 : JAB
            // This event handler was not connected to the click event. This handles 
            // resetting the firm app setting to the firm defaults.

            try
            {
                ResetUserToFirmDefaults();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbFreqUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.dtTime.Enabled = (this.cmbFreqUnit.Text == "Day" || this.cmbFreqUnit.Text == "Week");
                this.lblTime.Enabled = (this.cmbFreqUnit.Text == "Day" || this.cmbFreqUnit.Text == "Week");
                this.lblDay.Enabled = this.cmbFreqUnit.Text == "Week";
                this.cmbDay.Enabled = this.cmbFreqUnit.Text == "Week";
                UpdateSyncScheduleDescription();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cmbDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                UpdateSyncScheduleDescription();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void udFreqInterval_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                UpdateSyncScheduleDescription();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void dtTime_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                UpdateSyncScheduleDescription();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void numUpDnMaxPeople_ValueChanged(object sender, EventArgs e)
        {
            // Set the warning threshold to not exceed the maximum number of people entries.
            this.numUpDnPeopleListWarningThreshold.Maximum = this.numUpDnMaxPeople.Value;
        }
        private void cmbDocManager_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbDocManager.SelectedValue == null)
                {
                    btnDMSMessage.Enabled = false;
                    return;
                }
                string xDMS = cmbDocManager.SelectedValue.ToString();
                chkUseFrontEndProfiling.Enabled = (xDMS != "0");
                //GLOG 5316
                chkCloseOnCancel.Enabled = (xDMS != "0") && chkUseFrontEndProfiling.Checked;
                //GLOG 3929: Since checkbox is disabled, make sure it isn't stuck in the Checked state
                if (xDMS == "0")
                {
                    chkUseFrontEndProfiling.Checked = false;
                    //GLOG 5316
                    chkCloseOnCancel.Checked = false;
                }

                switch (xDMS)
                {
                    case "3":
                        //Worksite integration handled by startup template only
                        this.chkFileOpen.Enabled = false;
                        this.chkFileOpen.Checked = false;
                        this.chkFileSave.Enabled = false;
                        this.chkFileSave.Checked = false;
                        this.chkFileSaveAs.Enabled = false;
                        this.chkFileSaveAs.Checked = false;
                        this.chkFileSaveAll.Enabled = false;
                        this.chkFileSaveAll.Checked = false;
                        this.chkFileClose.Enabled = false;
                        this.chkFileClose.Checked = false;
                        this.chkFileCloseAll.Enabled = false;
                        this.chkFileCloseAll.Checked = false;
                        this.chkFileClose.Enabled = false;
                        this.chkFileClose.Checked = false;
                        this.chkFileCloseAll.Enabled = false;
                        this.chkFileCloseAll.Checked = false;
                        this.chkFilePrint.Enabled = false;
                        this.chkFilePrint.Checked = false;
                        this.chkFilePrintDefault.Enabled = false;
                        this.chkFilePrintDefault.Checked = false;
                        this.chkFileExit.Enabled = false;
                        this.chkFileExit.Checked = false;
                        btnDMSMessage.Enabled = true;
                        break;
                    case "6":
                    case "10": //GLOG 7602: NetDocuments ndOffice
                        //NetDocuments integration handled by startup tempalte,
                        //except for FilePrint/FilePrintDefault
                        this.chkFilePrint.Enabled = true;
                        this.chkFilePrintDefault.Enabled = true;
                        this.chkFileOpen.Enabled = false;
                        this.chkFileOpen.Checked = false;
                        this.chkFileSave.Enabled = false;
                        this.chkFileSave.Checked = false;
                        this.chkFileSaveAs.Enabled = false;
                        this.chkFileSaveAs.Checked = false;
                        this.chkFileSaveAll.Enabled = false;
                        this.chkFileSaveAll.Checked = false;
                        this.chkFileClose.Enabled = false;
                        this.chkFileClose.Checked = false;
                        this.chkFileCloseAll.Enabled = false;
                        this.chkFileCloseAll.Checked = false;
                        this.chkFileClose.Enabled = false;
                        this.chkFileClose.Checked = false;
                        this.chkFileCloseAll.Enabled = false;
                        this.chkFileCloseAll.Checked = false;
                        this.chkFileExit.Enabled = false;
                        this.chkFileExit.Checked = false;
                        btnDMSMessage.Enabled = true;
                        break;
                    default:
                        this.chkFileOpen.Enabled = true;
                        this.chkFileSave.Enabled = true;
                        this.chkFileSaveAs.Enabled = true;
                        this.chkFileSaveAll.Enabled = true;
                        this.chkFileClose.Enabled = true;
                        this.chkFileCloseAll.Enabled = true;
                        this.chkFileClose.Enabled = true;
                        this.chkFileCloseAll.Enabled = true;
                        this.chkFilePrint.Enabled = true;
                        this.chkFilePrintDefault.Enabled = true;
                        this.chkFileExit.Enabled = true;
                        btnDMSMessage.Enabled = (xDMS == "7");
                        break;
                }
                if (this.ActiveControl == this.cmbDocManager)
                    ShowDMSAdminMessage(xDMS);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnDMSMessage_Click(object sender, EventArgs e)
        {
            try
            {
                string xDMS = cmbDocManager.SelectedValue.ToString();
                ShowDMSAdminMessage(xDMS);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void btnPickColor_Click(object sender, EventArgs e)
        {
            if (this.colorDlgBackColor.ShowDialog() == DialogResult.OK)
            {
                this.pnlTaskPaneColorChip.BackColor = colorDlgBackColor.Color;
            }
        }
        // GLOG : 3292 : JAB
        // Get the My/Public/Shared folders help text.
        private void btnPublicFoldersHelpText_Click(object sender, EventArgs e)
        {
            try
            {
                txtPublicFoldersHelpText.Text = GetHelpText(txtPublicFoldersHelpText.Text, "Public Folders Help Text");
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnMyFoldersHelpText_Click(object sender, EventArgs e)
        {
            try
            {
                txtMyFoldersHelpText.Text = GetHelpText(txtMyFoldersHelpText.Text, "My Folders Help Text");
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnSharedFoldersHelpText_Click(object sender, EventArgs e)
        {
            try
            {
                txtSharedFoldersHelpText.Text = GetHelpText(txtSharedFoldersHelpText.Text, "Shared Folders Help Text");
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkFileOpen_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }
        private void chkFileClose_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }
        private void chkFileCloseAll_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }
        private void chkFilePrint_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }
        private void chkFilePrintDefault_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }
        private void chkFileSave_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }
        private void chkFileSaveAs_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }
        private void chkFileSaveAll_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }
        private void chkFileExit_CheckedChanged(object sender, EventArgs e)
        {
            SetTrailerBehaviorControls();
        }

        //GLOG - 3619 - ceh
        private void lstTrailers_Leave(object sender, EventArgs e)
        {
            LoadDefaultTrailers(m_oFirmAppSettings);
        }

        private void chkUseNetworkCredentials_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.lblMailUserID.Enabled = !this.chkUseNetworkCredentials.Checked;
                this.lblMailPWD.Enabled = !this.chkUseNetworkCredentials.Checked;
                this.txtMailUserID.Enabled = !this.chkUseNetworkCredentials.Checked;
                this.txtMailPWD.Enabled = !this.chkUseNetworkCredentials.Checked;

                if (this.chkUseNetworkCredentials.Checked)
                {
                    this.txtMailUserID.Text = "";
                    this.txtMailPWD.Text = "";
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                BindingSource oBS = (BindingSource)this.grdTypes.DataSource;
                DataTable oDT = (DataTable)oBS.DataSource;

                oDT.Rows.Add("New User Folder", "New User Folder Description");

                this.grdTypes.DataSource = oBS;
                this.grdTypes.CurrentCell = this.grdTypes.Rows[this.grdTypes.Rows.Count - 1].Cells[0];
                this.grdTypes.BeginEdit(true);
                this.tbtnDelete.Enabled = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.grdTypes.CurrentRow == null)
                    return;

                DialogResult iRes = MessageBox.Show("Remove this item from the list of default user folders?",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);

                if (iRes == DialogResult.Yes)
                {
                    int iRow = this.grdTypes.CurrentRow.Index;

                    BindingSource oBS = (BindingSource)this.grdTypes.DataSource;
                    DataTable oDT = (DataTable)oBS.DataSource;

                    oDT.Rows[iRow].Delete();

                    this.grdTypes.DataSource = oBS;

                    if (oDT.Rows.Count > 0)
                    {
                        this.grdTypes.CurrentCell = this.grdTypes
                            .Rows[Math.Min(iRow, this.grdTypes.Rows.Count - 1)].Cells[0];
                    }
                    else
                    {
                        this.tbtnDelete.Enabled = false;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //JTS: 03/18/10
        private void btnDistributableFolder_Click(object sender, EventArgs e)
        {
            try
            {
                this.fbdDistributableFolder.SelectedPath = this.txtDistributableFolder.Text;
                DialogResult iResult = this.fbdDistributableFolder.ShowDialog();
                if (iResult == DialogResult.OK)
                {
                    this.txtDistributableFolder.Text = this.fbdDistributableFolder.SelectedPath;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG 5316
        private void chkUseFrontEndProfiling_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.chkUseFrontEndProfiling.Checked)
                {
                    this.chkCloseOnCancel.Checked = false;
                    this.chkCloseOnCancel.Enabled = false;
                }
                else
                    this.chkCloseOnCancel.Enabled = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 6421 : CEH
        private void tbtnPasswordNew_Click(object sender, EventArgs e)
        {
            try
            {
                BindingSource oBS = (BindingSource)this.grdPasswords.DataSource;
                DataTable oDT = (DataTable)oBS.DataSource;

                oDT.Rows.Add("New Document Password", "New Document Password Description");

                this.grdPasswords.DataSource = oBS;
                this.grdPasswords.CurrentCell = this.grdPasswords.Rows[this.grdPasswords.Rows.Count - 1].Cells[0];
                this.grdPasswords.BeginEdit(true);
                this.tbtnPasswordDelete.Enabled = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void tbtnPasswordDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.grdPasswords.CurrentRow == null)
                    return;

                DialogResult iRes = MessageBox.Show("Remove this item from the list of default document passwords?",
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);

                if (iRes == DialogResult.Yes)
                {
                    int iRow = this.grdPasswords.CurrentRow.Index;

                    BindingSource oBS = (BindingSource)this.grdPasswords.DataSource;
                    DataTable oDT = (DataTable)oBS.DataSource;

                    oDT.Rows[iRow].Delete();

                    this.grdPasswords.DataSource = oBS;

                    if (oDT.Rows.Count > 0)
                    {
                        this.grdPasswords.CurrentCell = this.grdPasswords
                            .Rows[Math.Min(iRow, this.grdPasswords.Rows.Count - 1)].Cells[0];
                    }
                    else
                    {
                        this.tbtnDelete.Enabled = false;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG : 6597 : jsw
        private void chkUpdateZoom_CheckedChanged(object sender, EventArgs e)
        {
            //enable/disable zoom percentage spinner accordingly
            if (this.chkUpdateZoom.Checked == true)
                this.spnDefaultZoomPercentage.Enabled = true;
            else
                this.spnDefaultZoomPercentage.Enabled = false;

        }
        /// <summary>
        /// Reset values on this tab to Firm Default values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnResetTab_Click(object sender, EventArgs e)
        {
            //GLOG 6731
            if (!this.tabFirmAppSettings.Visible || m_oFirmAppSettings == null)
            {
                return;
            }
            FirmAppSettings oSettings = new FirmAppSettings(LMP.Data.ForteConstants.mpFirmRecordID);
            switch (tabFirmAppSettings.SelectedTab.Name)
            {
                case "tabDisplay":
                    LoadDisplaySettings(oSettings);
                    break;
                case "tabTrailers":
                    LoadDMSTrailerSettings(oSettings);
                    break;
                case "tabDraftStamps":
                    LoadDraftStampSettings(oSettings);
                    break;
                case "tabSync":
                    LoadNetworkSettings(oSettings);
                    break;
                case "tabFeatures":
                    LoadFeaturesSettings(oSettings);
                    break;
                case "tabMailSetup":
                    LoadMailSettings(oSettings);
                    break;
                case "tabSecurity":
                    LoadSecuritySettings(oSettings);
                    break;
                case "tabOther":
                    LoadOtherSettings(oSettings);
                    break;
                default:
                    return;
            }
            //Save settings
            UpdateCurrentTabValues(this.m_oFirmAppSettings);
        }

        /// <summary>
        /// Clear individual settings for all offices and groups
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnResetAllEntitiesToFirmDefaults_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult iRet = MessageBox.Show("Are you sure? Individual settings for all offices and groups will be cleared.", "Reset All Settings to Firm Defaults", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (iRet == DialogResult.Yes)
                {
                    int iOfficeCount = 0;
                    int iGroupCount = 0;
                    LMP.Data.Offices oOffices = new LMP.Data.Offices();
                    for (int i = 1; i <= oOffices.Count; i++)
                    {
                        Office oOffice = (Office)oOffices.ItemFromIndex(i);
                        LMP.Data.FirmApplicationSettings oSettings = new LMP.Data.FirmApplicationSettings(oOffice.ID);
                        if (oSettings != null && !oSettings.IsDefault)
                        {
                            oSettings.Delete();
                            iOfficeCount++;
                        }
                    }
                    LMP.Data.PeopleGroups oGroups = new LMP.Data.PeopleGroups();
                    for (int i = 1; i <= oGroups.Count; i++)
                    {
                        PeopleGroup oGroup = (PeopleGroup)oGroups.ItemFromIndex(i);
                        LMP.Data.FirmApplicationSettings oSettings = new LMP.Data.FirmApplicationSettings(oGroup.ID);
                        if (oSettings != null && !oSettings.IsDefault)
                        {
                            oSettings.Delete();
                            iGroupCount++;
                        }
                    }
                    if (iOfficeCount > 0 || iGroupCount > 0)
                    {
                        string xMsg = string.Format("Settings were reset for {0}{1} and {2}{3}", iOfficeCount > 0 ? iOfficeCount.ToString() : "0", iOfficeCount == 1 ? " office" : " offices",
                            iGroupCount > 0 ? iGroupCount.ToString() : "0", iGroupCount == 1 ? " group" : " groups");
                        MessageBox.Show(xMsg, "Reset All Settings to Firm Defaults", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("There were no individual settings to be reset.", "Reset All Settings to Firm Defaults", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Update all individual office and group keysets with Firm settings on current tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnResetTabForAllEntities_Click(object sender, EventArgs e)
        {
            DialogResult iRet = MessageBox.Show("Are you sure? Individual settings on this tab will be cleared for all offices and groups.", "Reset Tab Settings to Firm Defaults", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (iRet == DialogResult.Yes)
            {
                int iOfficeCount = 0;
                int iGroupCount = 0;
                LMP.Data.Offices oOffices = new LMP.Data.Offices();
                for (int i = 1; i <= oOffices.Count; i++)
                {
                    Office oOffice = (Office)oOffices.ItemFromIndex(i);
                    LMP.Data.FirmApplicationSettings oSettings = new LMP.Data.FirmApplicationSettings(oOffice.ID);
                    if (oSettings != null && !oSettings.IsDefault)
                    {
                        UpdateCurrentTabValues(oSettings);
                        iOfficeCount++;
                    }
                }
                LMP.Data.PeopleGroups oGroups = new LMP.Data.PeopleGroups();
                for (int i = 1; i <= oGroups.Count; i++)
                {
                    PeopleGroup oGroup = (PeopleGroup)oGroups.ItemFromIndex(i);
                    LMP.Data.FirmApplicationSettings oSettings = new LMP.Data.FirmApplicationSettings(oGroup.ID);
                    if (oSettings != null && !oSettings.IsDefault)
                    {
                        UpdateCurrentTabValues(oSettings);
                        iGroupCount++;
                    }
                }
                if (iOfficeCount > 0 || iGroupCount > 0)
                {
                    string xMsg = string.Format("Settings were reset for {0}{1} and {2}{3}", iOfficeCount > 0 ? iOfficeCount.ToString() : "0", iOfficeCount == 1 ? " office" : " offices",
                        iGroupCount > 0 ? iGroupCount.ToString() : "0", iGroupCount == 1 ? " group" : " groups");
                    MessageBox.Show(xMsg, "Reset All Settings to Firm Defaults", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("There were no individual office or group settings to be reset.", "Reset All Settings to Firm Defaults", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }
        /// <summary>
        /// Selected entity has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GLOG 6731
            try
            {
                if (this.m_oFirmAppSettings != null)
                {
                    SaveApplicationSettings();
                }

                m_iEntityID = this.SelectedEntityID;
                LoadApplicationSettings();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void btnResetUserForAllEntities_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult iRet = MessageBox.Show("Are you sure? Individual settings for all offices and groups will be cleared.", "Reset All User Application Settings to Firm Defaults", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (iRet == DialogResult.Yes)
                {
                    int iOfficeCount = 0;
                    int iGroupCount = 0;
                    LMP.Data.Offices oOffices = new LMP.Data.Offices();
                    for (int i = 1; i <= oOffices.Count; i++)
                    {
                        Office oOffice = (Office)oOffices.ItemFromIndex(i);
                        LMP.Data.UserApplicationSettings oSettings = new LMP.Data.UserApplicationSettings(oOffice.ID);
                        if (oSettings != null && !oSettings.IsDefault)
                        {
                            oSettings.Delete();
                            iOfficeCount++;
                        }
                    }
                    LMP.Data.PeopleGroups oGroups = new LMP.Data.PeopleGroups();
                    for (int i = 1; i <= oGroups.Count; i++)
                    {
                        PeopleGroup oGroup = (PeopleGroup)oGroups.ItemFromIndex(i);
                        LMP.Data.UserApplicationSettings oSettings = new LMP.Data.UserApplicationSettings(oGroup.ID);
                        if (oSettings != null && !oSettings.IsDefault)
                        {
                            oSettings.Delete();
                            iGroupCount++;
                        }
                    }
                    if (iOfficeCount > 0 || iGroupCount > 0)
                    {
                        string xMsg = string.Format("User Application Settings were reset for {0}{1} and {2}{3}", iOfficeCount > 0 ? iOfficeCount.ToString() : "0", iOfficeCount == 1 ? " office" : " offices",
                            iGroupCount > 0 ? iGroupCount.ToString() : "0", iGroupCount == 1 ? " group" : " groups");
                        MessageBox.Show(xMsg, "Reset All User Application Settings to Firm Defaults", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("There were no individual office or group settings to be reset.", "Reset All User Application Settings to Firm Defaults", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void btnTemplateDefaults_Click(object sender, EventArgs e)
        {
            Form oForm = new TemplateTrailersForm(m_oFirmAppSettings.TemplateTrailerDefaults);
            DialogResult iRet = oForm.ShowDialog();
            if (iRet == DialogResult.OK)
            {
                m_oFirmAppSettings.TemplateTrailerDefaults = ((TemplateTrailersForm)oForm).TemplateTrailerOptions;
            }
        }

    }
}

