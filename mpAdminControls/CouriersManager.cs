using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Controls;
using System.Collections;

namespace LMP.Administration.Controls
{
    public partial class CouriersManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ************fields*************
        private Couriers m_oCouriers;
        private Addresses m_oAddresses;
        private Offices m_oOffices;
        private DataTable m_oCouriersDT;
        private bool m_bFormLoaded = false;
        private bool m_bChangesMade = false;
        private bool m_bIsNewRow = false;
        private const string DEF_COURIER_NAME = "New Courier";
        #endregion
        #region ************constructors**************
        public CouriersManager()
        {
            InitializeComponent();

        }
        #endregion
        #region ************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
                #endregion
        #region ************methods************
        /// <summary>
        /// Display a list of couriers in grdCouriers
        /// </summary>
        private void DataBindGrdCouriers(bool bSortByName)
        {
            m_oCouriers = new Couriers();
            m_oAddresses = new Addresses();
            m_oOffices = new Offices();

            //Get a datatable of all couriers
            m_oCouriersDT = m_oCouriers.ToDataSet().Tables[0];
            
            //Set the DataSource of grdCouriers
            this.grdCouriers.DataSource = m_oCouriersDT;

            //Hide unwanted columns in grdCouriers
            for (int i = 0; i < grdCouriers.Columns.Count; i++)
                if (i != 1)
                    grdCouriers.Columns[i].Visible = false;

            //Set the display properties of the grid
            this.grdCouriers.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //sort column, since names are not in alphabetical order
            if (bSortByName)
                this.grdCouriers.Sort(grdCouriers.Columns[1], ListSortDirection.Ascending);
            else
                this.grdCouriers.Sort(grdCouriers.Columns["ID"], ListSortDirection.Ascending);
            
            m_bFormLoaded = true;
        }
        /// <summary>
        /// Display the properties of the courier
        /// currently selected in grdCouriers
        /// </summary>
        private void DataBindGrdProperties()
        {
            if (!this.bIsNewCourierRow())
            {
                //Get the current courier record
                Courier oCurCourier = (Courier)m_oCouriers.ItemFromID(Int32.Parse(grdCouriers.
                    CurrentRow.Cells["ID"].FormattedValue.ToString()));
                
                //Get the address object corresponding to the 
                //current courier
                Address oCurCourierAddress = (Address)m_oAddresses.
                    ItemFromID(oCurCourier.AddressID);

                //Create a new DataTable to use as the DataSource
                //for grdProperties
                DataTable oPropertiesDT = new DataTable();

                //Add columns to the DataTable
                oPropertiesDT.Columns.Add("Property");
                oPropertiesDT.Columns.Add("Value");

                //An array for adding each new row to the DT
                object[] aRowValues = new object[2];

                //Add rows for each of the properties/values 
                //to the DataTable that is to be bound to 
                //properties

                //Pre City
                aRowValues[0] = "Pre City";
                aRowValues[1] = oCurCourierAddress.PreCity;
                oPropertiesDT.Rows.Add(aRowValues);

                //City
                aRowValues[0] = "City";
                aRowValues[1] = oCurCourierAddress.City;
                oPropertiesDT.Rows.Add(aRowValues);

                //Post City
                aRowValues[0] = "Post City";
                aRowValues[1] = oCurCourierAddress.PostCity;
                oPropertiesDT.Rows.Add(aRowValues);

                //County
                aRowValues[0] = "County";
                aRowValues[1] = oCurCourierAddress.County;
                oPropertiesDT.Rows.Add(aRowValues);

                //State
                aRowValues[0] = "State";
                aRowValues[1] = oCurCourierAddress.State;
                oPropertiesDT.Rows.Add(aRowValues);

                //State Abbreviation
                aRowValues[0] = "State Abbreviation";
                aRowValues[1] = oCurCourierAddress.StateAbbr;
                oPropertiesDT.Rows.Add(aRowValues);

                //Country
                aRowValues[0] = "Country";
                aRowValues[1] = oCurCourierAddress.Country;
                oPropertiesDT.Rows.Add(aRowValues);

                //Zip Code
                aRowValues[0] = "Zip Code";
                aRowValues[1] = oCurCourierAddress.Zip;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 1
                aRowValues[0] = "Address Line 1";
                aRowValues[1] = oCurCourierAddress.Line1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 2
                aRowValues[0] = "Address Line 2";
                aRowValues[1] = oCurCourierAddress.Line2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 3
                aRowValues[0] = "Address Line 3";
                aRowValues[1] = oCurCourierAddress.Line3;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Template
                aRowValues[0] = "Address Template";
                aRowValues[1] = oCurCourierAddress.Template;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 1
                aRowValues[0] = "Phone Line 1";
                aRowValues[1] = oCurCourierAddress.Phone1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 2
                aRowValues[0] = "Phone Line 2";
                aRowValues[1] = oCurCourierAddress.Phone2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 3
                aRowValues[0] = "Phone Line 3";
                aRowValues[1] = oCurCourierAddress.Phone3;
                oPropertiesDT.Rows.Add(aRowValues);

                //Fax Line 1
                aRowValues[0] = "Fax Line 1";
                aRowValues[1] = oCurCourierAddress.Fax1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Fax Line 2
                aRowValues[0] = "Fax Line 2";
                aRowValues[1] = oCurCourierAddress.Fax2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Set grdProperties' datasource to the newly created DT
                grdProperties.DataSource = oPropertiesDT;

                grdProperties.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdProperties.Columns[0].FillWeight = 37;
                grdProperties.Columns[0].ReadOnly = true;
                grdProperties.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            }
            else
            {
                //User has entered the row for new records
                //in grdCouriers

                //Clear data displayed in grdProperties
                DataTable oDT = (DataTable)this.grdProperties.DataSource;
                oDT.Clear();
            }
        }
        /// <summary>
        /// Display all offices, selects current Courier Office
        /// </summary>
        private void DataBindcbxOffices()
        {

            this.m_oOffices = new Offices();

            //An array for use with m_oAllOffices.ToArray
            //to specify the return of only the name and ID fields
            int[] aNameAndIDIndexes = new int[2];
            aNameAndIDIndexes[0] = 0;
            aNameAndIDIndexes[1] = 4;

            //Get an Array of the names and IDs of all Offices
            ArrayList oAllOfficeNamesAndIDsArray = m_oOffices.ToArrayList(aNameAndIDIndexes);

            //bind the cbox
            this.cbxOffices.SetList(oAllOfficeNamesAndIDsArray);

            if (this.bIsNewCourierRow())
            {
                //select first office in list
                this.cbxOffices.SelectedIndex = 0;
            }
            else
            {

                //Get the current courier record
                Courier oCurCourier = (Courier)m_oCouriers.ItemFromID(Int32.Parse(grdCouriers.
                    CurrentRow.Cells["ID"].FormattedValue.ToString()));

                //Set Selected Value to Courier Office ID
                this.cbxOffices.SelectedValue = oCurCourier.OfficeID;
            }
        }
        /// <summary>
        /// Update the properties of the courier record
        /// that the user has just finished editing
        /// </summary>
        private void UpdateCourier()
        {
            //there's no current row - table is empty
            if (grdCouriers.CurrentRow == null)
                return;
            
            //commit any edits to couriers grid
            grdCouriers.EndEdit();
            
            //Get the current courier record
            Courier oCurCourier = (Courier)m_oCouriers.ItemFromID
                (Int32.Parse(this.grdCouriers.CurrentRow.Cells[0].FormattedValue.ToString()));

            //Get the Address record corresponding to the current 
            //Office record
            Address oCurCourierAddress = (Address)m_oAddresses.
                ItemFromID(oCurCourier.AddressID);

            DataGridViewRowCollection oRows = this.grdProperties.Rows;

            //Make sure properties grid is loaded
            if (this.grdProperties.Rows.Count > 0)
            {
                //Update Address Template if the new value is not null
                if (oRows[11].Cells[1].FormattedValue.ToString() != "")
                    oCurCourierAddress.Template = oRows[11].Cells[1].FormattedValue.ToString();

                //Update Courier name
                oCurCourier.Name = grdCouriers.CurrentRow.Cells["Name"].FormattedValue.ToString();

                //Update PreCity
                oCurCourierAddress.PreCity = oRows[0].Cells[1].FormattedValue.ToString();

                //Update City
                oCurCourierAddress.City = oRows[1].Cells[1].FormattedValue.ToString();

                //Update PostCity
                oCurCourierAddress.PostCity = oRows[2].Cells[1].FormattedValue.ToString();

                //Update County
                oCurCourierAddress.County = oRows[3].Cells[1].FormattedValue.ToString();

                //Update State
                oCurCourierAddress.State = oRows[4].Cells[1].FormattedValue.ToString();

                //Update StateAbbr
                oCurCourierAddress.StateAbbr = oRows[5].Cells[1].FormattedValue.ToString();

                //Update Country
                oCurCourierAddress.Country = oRows[6].Cells[1].FormattedValue.ToString();

                //Update Zip
                oCurCourierAddress.Zip = oRows[7].Cells[1].FormattedValue.ToString();

                //Update Line1
                oCurCourierAddress.Line1 = oRows[8].Cells[1].FormattedValue.ToString();

                //Update Line2
                oCurCourierAddress.Line2 = oRows[9].Cells[1].FormattedValue.ToString();

                //Update Line3
                oCurCourierAddress.Line3 = oRows[10].Cells[1].FormattedValue.ToString();

                //Update Phone1
                oCurCourierAddress.Phone1 = oRows[12].Cells[1].FormattedValue.ToString();

                //Update Phone2
                oCurCourierAddress.Phone2 = oRows[13].Cells[1].FormattedValue.ToString();

                //Update Phone3
                oCurCourierAddress.Phone3 = oRows[14].Cells[1].FormattedValue.ToString();

                //Update Fax1
                oCurCourierAddress.Fax1 = oRows[15].Cells[1].FormattedValue.ToString();

                //Update Fax2
                oCurCourierAddress.Fax2 = oRows[16].Cells[1].FormattedValue.ToString();
            }

            //Update OfficeID
            oCurCourier.OfficeID = Convert.ToInt32(this.cbxOffices.Value.ToString());
            try
            {
                //Save the changes to the DB
                m_oCouriers.Save(oCurCourier);
                m_oAddresses.Save(oCurCourierAddress);
                m_bChangesMade = false;
            }
            
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// Delete the currently selected courier record
        /// from the DB and grdCouriers
        /// </summary>
        private void DeleteCourierRecord()
        {
            //exit if there's no current record
            if (grdCouriers.CurrentRow == null || grdCouriers.Rows.Count == 0 ||
                grdCouriers.CurrentRow.IsNewRow)
                return;
            
            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                //Get the currently selected courier record
                Courier oCurCourier = (Courier)m_oCouriers.ItemFromID(Int32.Parse(
                    this.grdCouriers.CurrentRow.Cells[0].FormattedValue.ToString()));

                try
                {
                    //Delete the currently selected courier record
                    m_oCouriers.Delete(oCurCourier.ID);

                    // Glog: 2433
                    // Attempt to delete the address of this courier. The address may not be
                    // deletable if it is still referenced by other entities.
                    try
                    {
                        this.m_oAddresses.Delete(oCurCourier.AddressID);
                    }
                    catch (LMP.Exceptions.StoredProcedureException)
                    {
                        // Do nothing when the exception is due to an attempt at deleting 
                        // an address that is still referenced by other entities.
                    }
                }
                catch
                {
                    //This try/catch block is here because of an error
                    //generated as a reuslt of a missing sproc: spObjectAssignmentsDeleteByObjectIDs
                }

                //Remove the row representing the deleted
                //courier record from grdCouriers
                this.grdCouriers.Rows.Remove(this.grdCouriers.CurrentRow);

                if (grdCouriers.Rows.Count > 0)
                {
                    this.grdCouriers.CurrentCell.Selected = true;
                    DataBindGrdProperties();
                }
                else
                {
                    //clear property grid entirely
                    grdProperties.DataSource = null;
                    grdProperties.Rows.Clear();
                }
            }
        }
        /// <summary>
        /// Create a new courier record and add it to the DB
        /// </summary>
        private bool AddCourierRecord()
        {
            //Create a new courier record.
            Courier oNewCourier = (Courier)m_oCouriers.Create();

            //Create a new address record for the new office
            Address oNewAddress = (Address)m_oAddresses.Create();

            //Set the courier name
            oNewCourier.Name = GetNewCourierName(m_oCouriers.GetLastID() + 1);

            //Set the default properties of the new address record
            oNewAddress.Template = "[Line1]|[Line2]|[Line3]|[City], [State]  [Zip]|[CountryIfForeign]";

            //Save the new Address record
            m_oAddresses.Save(oNewAddress);

            //Set the new address record to the new courier record
            oNewCourier.AddressID = oNewAddress.ID;

            //Set the new office record to the new courier record
            oNewCourier.OfficeID = Convert.ToInt32(this.cbxOffices.SelectedValue.ToString());

            //Save the new Office record
            m_oCouriers.Save(oNewCourier);

            //rebind the couriers grid
            this.DataBindGrdCouriers(false); 
        
            //select the new row in edit mode
            grdCouriers.CurrentCell = grdCouriers.Rows[grdCouriers.Rows.Count - 1].Cells[1];
            grdCouriers.BeginEdit(true);

            //Store office ID of new Courier in grdCouriers
            this.grdCouriers.CurrentRow.Cells[2].Value = oNewCourier.OfficeID.ToString();

            return true;
        }
        /// <summary>
        /// generates new courier name based on constant plus ID
        /// </summary>
        /// <returns></returns>
        private string GetNewCourierName(int iNewID)
        {
            if (grdCouriers.Rows.Count == 0)
                return DEF_COURIER_NAME;

            //search for existing default name
            DataTable oDT = (DataTable)grdCouriers.DataSource;
            DataRow[] oDR = oDT.Select("Name = '" + DEF_COURIER_NAME + "'");
            
            if (oDR.GetLength(0) == 0)
                return DEF_COURIER_NAME;
            else
            {
                return  DEF_COURIER_NAME + "_" +iNewID.ToString();
            }
        }
        /// <summary>
        /// determines if selected row is new row
        /// </summary>
        /// <returns>bool</returns>
        private bool bIsNewCourierRow()
        {
            if (this.grdCouriers.CurrentRow == null || this.grdCouriers.CurrentRow.IsNewRow)
               return true;
            if (this.grdCouriers.SelectedCells.Count > 0)
                if (this.grdCouriers.SelectedCells[0].EditedFormattedValue.ToString() == "")
                    return true;
            return false;
        }
        /// <summary>
        /// Select the first instance of the search
        /// string entered by the user
        /// </summary>
        /// <param name="xSearchString"></param>
        private void FindMatchingRecord()
        {
            string xSearchString = this.ttxtFindCourier.Text;

            if (xSearchString != "")
            {
                //User has entered a search string
                //Get the index of the first matching record
                int iMatchIndex = this.grdCouriers.FindRecord
                    (xSearchString, this.grdCouriers.Columns[1].Name, false);

                if (iMatchIndex > -1)
                    //Match found
                    this.grdCouriers.CurrentCell = this.grdCouriers.Rows[iMatchIndex].Cells[1];
            }
        }
        /// <summary>
        /// returns index from array according to array item value
        /// </summary>
        /// <returns>int</returns>
        private int GetIndexFromArray(int[] iArray, string xValue)
        {
            for (int i = 0; i < iArray.Length; i++)
            {
                if (iArray[i] == Convert.ToInt32(xValue))
                    return i;
            }
            return -1;
        }
        #endregion
        #region ************private functions*************
        /// <summary>
        /// tests for empty or duplicate office name string
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        private bool ValidateCourierName(string xName)
        {
            string xMessage = "";
            bool bReturn = true;

            //check grdOffices DataSource for dupes
            DataTable oDT = (DataTable)grdCouriers.DataSource;
            DataRow[] oRows = oDT.Select(this.grdCouriers.Columns[1].Name + "=" + "'" + xName + "'");
            int iExisting = oRows.Length;

            if (iExisting != 0 && m_bChangesMade)
            {
                bReturn = false;
                xMessage = "Error_DuplicateNameValueNotAllowed";
            }

            if (iExisting != 0 && m_bIsNewRow)
            {
                bReturn = false;
                xMessage = "Error_DuplicateNameValueNotAllowed";
            }

            if (xName == "" && m_bIsNewRow)
            {
                return bReturn;
            }

            if (xName == "")
            {
                xMessage = "Error_EmptyValueNotAllowed";
                bReturn = false;
            }

            if (!bReturn)
            {
                //alert user
                MessageBox.Show(LMP.Resources.GetLangString(xMessage),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return bReturn;
        }
        #endregion
        #region ************event handlers*************
        private void CouriersManager_Load(object sender, EventArgs e)
        {
            try
            {
                this.DataBindGrdCouriers(true);
                this.DataBindcbxOffices(); 
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteCourierRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCouriers_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.DeleteCourierRecord();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCouriers_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!this.IsValid)
                    return;

                this.DataBindGrdProperties();
                this.DataBindcbxOffices();

                m_bIsNewRow = grdCouriers.Rows[e.RowIndex].IsNewRow;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdProperties_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bFormLoaded)
                {
                    DataGridViewRow oCurRow = grdProperties.Rows[e.RowIndex];
                    
                    //test if change made
                    if (oCurRow.Cells[1].FormattedValue.ToString()
                        != oCurRow.Cells[1].EditedFormattedValue.ToString())
                        m_bChangesMade = true;
                    
                    if (e.RowIndex == 0 && m_bChangesMade)    
                    {
                        //prevent entry of duplicate office name
                        string xCourierName = grdProperties.CurrentCell.EditedFormattedValue.ToString();
                        
                        //we'll allow a name to be reset to its original value
                        if (xCourierName == grdCouriers.CurrentCell.FormattedValue.ToString())
                            return;

                        if (!this.ValidateCourierName(xCourierName)) 
                        {
                            grdProperties.CancelEdit();
                            e.Cancel = true;
                            this.IsValid = false;
                            m_bChangesMade = false;
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCouriers_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bFormLoaded)
                {
                    //Validate cell only if form is already loaded
                    if (m_bFormLoaded)
                    {
                        //Get the current row in grdOffices
                        string xOldCourierName = this.grdCouriers.Rows[e.RowIndex].Cells[1]
                                                .FormattedValue.ToString();

                        string xCourierName = e.FormattedValue.ToString();
                        m_bChangesMade = !(xCourierName == xOldCourierName);

                        if (!ValidateCourierName(xCourierName))
                        {
                            e.Cancel = true;
                            this.IsValid = false;
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdProperties_CellValidated(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bChangesMade)
                {
                    this.UpdateCourier();
                    m_bChangesMade = false;
                }
                this.IsValid = true;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCouriers_CellValidated(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bIsNewRow)
                    AddCourierRecord();

                if (m_bChangesMade)
                    UpdateCourier();

                this.IsValid = true;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void ttxtFindCourier_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    //don't attempt to set current cell
                    //if grids are empty, i.e. there are no records
                    if (grdCouriers.Rows.Count == 0)
                        return;
                    
                    //Give focus to grdProperties
                    this.grdProperties.Focus();

                    //Set the current cell in grdProperties
                    this.grdProperties.CurrentCell =
                        this.grdProperties.Rows[0].Cells[1];
                }
                else
                    //Select the first record matching
                    //the search string eneterd by the user
                    this.FindMatchingRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cbxOffices_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (m_bFormLoaded)
                {
                    //Validate cell only if form is already loaded
                    string xOfficeID = this.grdCouriers.CurrentRow.Cells[2].FormattedValue.ToString();
                    if (this.cbxOffices.Value.ToString() != xOfficeID)
                        m_bChangesMade = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdProperties_CellEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (grdProperties.Focused)
                {
                    
                    if (grdProperties.CurrentCellAddress.X == 0 &&
                       grdProperties.CurrentCellAddress.Y == 0)
                        return;

                    //programmatically resetting current cell errors
                    //we can use sendkeys instead
                    if (e.ColumnIndex == 0)
                        System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles adding new records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                grdProperties.EndEdit();
                UpdateCourier();
                AddCourierRecord();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void grdCouriers_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }


        }
        //GLOG : 8241 : JSW
        //tabbing moves from row to row for exising records
        private void grdCouriers_TabPressed(object sender, TabPressedEventArgs e)
        {
            int iRowIndex;

            try
            {
                iRowIndex = this.grdCouriers.CurrentRow.Index;
                if ((grdCouriers.Rows.Count - 1) > iRowIndex)
                    this.grdCouriers.CurrentCell = this.grdCouriers.Rows[iRowIndex + 1].Cells[1];
                else
                    this.grdCouriers.CurrentCell = this.grdCouriers.Rows[0].Cells[1];
            }
            catch { }
        }
    }
}

