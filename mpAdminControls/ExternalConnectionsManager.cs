using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class ExternalConnectionsManager : LMP.Administration.Controls.AdminManagerBase
    {

        #region****************fields*********************
        ExternalConnections m_oConnections;
        private BindingSource m_oBindingSource;
        private DataRow m_oLastRow = null;
        private const string DEF_CONNECTION_NAME = "New Connection";
        private const ExternalConnection.DatabaseTypes DEF_DB_TYPE = ExternalConnection.DatabaseTypes.SQLServer;
        private const string DEF_DB_NAME = "ExternalDBName";
        #endregion
        #region ****************constructors*****************
        public ExternalConnectionsManager()
        {
            InitializeComponent();
        }
        #endregion
        #region ****************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region****************methods***********************
        /// <summary>
        /// Display the ExternalConnections from the DB in grdConnections
        /// </summary>
        private void DataBindGrdConnections()
        {
            //Get an ExternalConnections object representing all the 
            //ExternalConnections in the DB
            m_oConnections = new ExternalConnections();

            //Get a DataTable representative of the data for all ExternalConnections
            DataTable oConnectionsDT = m_oConnections.ToDataSet().Tables[0];

            //set binding source
            m_oBindingSource = new BindingSource();

            //subscribe to position changed event
            m_oBindingSource.PositionChanged += new EventHandler(m_oBindingSource_PositionChanged);

            //set binding source data source = data table
            m_oBindingSource.DataSource = oConnectionsDT;

            //bind grdConnections to binding source
            this.grdConnections.DataSource = m_oBindingSource;

            //Set the display properties of the columns in grdCollections
            this.grdConnections.Columns["ID"].Visible = false;
            this.grdConnections.Columns["LastEditTime"].Visible = false;

            //set Database Type lookup
            DataTable oDT = new DataTable();
            oDT.Columns.Add("dbName");
            oDT.Columns.Add("dbTypeValue");

            oDT.Rows.Add("SQL Server", 1);
            oDT.Rows.Add("Oracle", 2);
            oDT.Rows.Add("Access", 3);

            for (int i = 0; i < grdConnections.Rows.Count; i++)
            {
                grdConnections.Rows[i].Cells["DatabaseType"] = new DataGridViewComboBoxCell();
                DataGridViewComboBoxCell oCombo = (DataGridViewComboBoxCell)grdConnections.Rows[i].Cells["DatabaseType"];
                oCombo.DataSource = oDT;
                oCombo.ValueMember = "dbTypeValue";
                oCombo.DisplayMember = "dbName";
            }
        }
        /// <summary>
        /// handles updating of records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //return if binding source current item is null --
                //this will be the case if all records have been deleted
                //and binding source is updating grid display
                if (((BindingSource)sender).Current == null)
                {
                    m_oLastRow = null;
                    return;
                }

                //set last row variable if it was previously null
                if (m_oLastRow == null)
                    m_oLastRow = ((DataRowView)m_oBindingSource.Current).Row;

                //update record if grid row has been modified
                if (m_oLastRow.RowState == DataRowState.Modified)
                    UpdateExternalConnection(false);
                
                //store current row as last row variable
                if (((BindingSource)sender).Position == -1)
                    //data table has no rows - last row has been deleted
                    m_oLastRow = null;
                else
                    m_oLastRow = ((DataRowView)((BindingSource)sender).Current).Row;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// Updates the DB with the user's edits
        /// </summary>
        private void UpdateExternalConnection(bool bAddNew)
        {
            //Get the current grdConnections cells
            DataGridViewCellCollection oCurCells = null;
            ExternalConnection oConnection;

            if (grdConnections.CurrentRow != null)
                oCurCells = this.grdConnections.CurrentRow.Cells;

            if (bAddNew)
            {
                //The user is adding a record, create a new one
                oConnection = (ExternalConnection)m_oConnections.Create();

                //set the name property - required for creation
                oConnection.Name = GetNewConnectionName();
                oConnection.DatabaseType = DEF_DB_TYPE;
                oConnection.Database = DEF_DB_NAME; 
            }
            else
            {
                //User is modifying a record, get the current record
                oConnection = (ExternalConnection)m_oConnections.ItemFromID
                    ((int)oCurCells["ID"].Value);

                //Set the properties of the dirty ExternalConnection from the data in the grid
                oConnection.Name = oCurCells["Name"].EditedFormattedValue.ToString();
                oConnection.UserID = oCurCells["UserID"].EditedFormattedValue.ToString();
                oConnection.Server = oCurCells["Server"].EditedFormattedValue.ToString();
                oConnection.Database = oCurCells["DatabaseName"].EditedFormattedValue.ToString();
                oConnection.DatabaseType = (ExternalConnection.DatabaseTypes)Int32.Parse(oCurCells["DataBaseType"].Value.ToString());
                oConnection.Password = oCurCells["Password"].EditedFormattedValue.ToString();
                oConnection.IsDirty = true;
            }
            try
            {
                //Attempt to save the changes to the DB
                m_oConnections.Save(oConnection);

                if (bAddNew)
                {
                    //rebind, select last item to refresh.
                    DataBindGrdConnections();
                    grdConnections.CurrentCell = grdConnections.Rows[grdConnections.Rows.Count -1].Cells["Name"];
                }
                else
                    //accept changes for last row - sufficient to reset
                    //rowstate for edits
                    m_oLastRow.AcceptChanges();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// handles updates if containing form is closed
        /// </summary>
        public override void SaveCurrentRecord()
        {
            //user has deleted last last record - nothing to save
            //so just exit
            if (m_oLastRow == null)
                return;

            if (m_oLastRow.RowState == DataRowState.Modified && 
                this.IsValid == true)
                UpdateExternalConnection(false);
        }
        /// <summary>
        /// Deletes the ExternalConnection from the DB that the User has selected 
        /// for deletion.
        /// </summary>
        private void DeleteConnection()
        {
            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                try
                {
                    int iNewRow = Math.Max(grdConnections.CurrentRow.Index - 1, 0);
                    
                    //Delete the ExternalConnection from the DB
                    //the binding source will automatically remove the grid row
                    m_oConnections.Delete((int)this.grdConnections.CurrentRow.Cells["ID"].Value);

                    //rebind grid and reset current cell
                    DataBindGrdConnections();
                    if (grdConnections.Rows.Count > 0)
                        grdConnections.CurrentCell = grdConnections.Rows[iNewRow].Cells["Name"];
                
                }
                catch (System.Exception oE)
                {
                    throw new LMP.Exceptions.DataException
                        (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
                }
            }
        }
        #endregion
        #region****************private functions********************
        /// <summary>
        /// tests for duplicate or empty names
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        private bool ValidateConnectionName(string xName)
        {
            string xMessage = "";
            bool bReturn = true;

            //check if name field is null
            if (System.String.IsNullOrEmpty(xName))
            {
                //message user - name field cannot be empty
                xMessage = "Error_NameFieldCannotBeNull";
                bReturn = false;
            }
            
            //Get a DataTable representative of the data for all ExternalConnections
            DataTable oDT = m_oConnections.ToDataSet().Tables[0];
            DataRow[] oRows = oDT.Select("Name =" + "'" + xName + "'");

            if (oRows.Length > 1)
            {
                //message user - duplicate names not allowed
                xMessage = "Error_DuplicateNameValueNotAllowed";
                bReturn = false;
            }

            if (!System.String.IsNullOrEmpty(xMessage))
            {
                //generate validation message
                MessageBox.Show(LMP.Resources.GetLangString(xMessage),
                    LMP.ComponentProperties.ProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return bReturn;
        }
        /// <summary>
        /// generates new courier name based on constant plus ID
        /// </summary>
        /// <returns></returns>
        private string GetNewConnectionName()
        {
            if (grdConnections.Rows.Count == 0)
                return DEF_CONNECTION_NAME;

            //search for existing default name
            DataTable oDT = (DataTable)m_oBindingSource.DataSource;
            DataRow[] oDR = oDT.Select("Name = '" + DEF_CONNECTION_NAME + "'");
            
            if (oDR.GetLength(0) == 0)
                return DEF_CONNECTION_NAME;
            else
            {
                return  DEF_CONNECTION_NAME + "_" + (m_oConnections.GetLastID() + 1).ToString();
            }
        }        
        #endregion
        #region****************event handlers**********************
        private void ExternalConnectionsManager_Load(object sender, EventArgs e)
        {
            try
            {
                //subscribe to base events
                base.AfterControlLoaded +=
                    new AfterControlLoadedHandler(AdminManager_AfterControlLoaded);
                base.BeforeControlUnloaded += 
                    new BeforeControlUnLoadedHandler(AdminManager_BeforeControlUnloaded);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires after control is loaded into container
        /// enables proper refresh/configuration of property grid cells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {
                //bind property grid after control has been loaded 
                //onto its container
                DataBindGrdConnections();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires if control is about to be unloaded by selecting another
        /// manager - calls SaveCurrentRecord to commit and save any 
        /// pending edits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                //commit any edits to grid
                grdConnections.EndEdit();
                SaveCurrentRecord();             
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdConnections_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //commit any changes
                grdConnections.EndEdit();
                if (m_oBindingSource != null)
                    m_oBindingSource.EndEdit();
                
                if (m_oLastRow.RowState == DataRowState.Unchanged)
                    return;
                
                string xCurValue = e.FormattedValue.ToString();

                if (e.ColumnIndex == grdConnections.Columns["Name"].Index ||
                    e.ColumnIndex == grdConnections.Columns["DatabaseName"].Index)
                {
                    this.IsValid = ValidateConnectionName(xCurValue);
                }

                //cancel navigation if cell data not valid
                e.Cancel = !this.IsValid;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdConnections_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (m_oLastRow.RowState == DataRowState.Unchanged)
                    return;

                if (m_oLastRow.RowState == DataRowState.Modified||
                    !this.IsValid)
                {
                    if (this.grdConnections.CurrentRow.Cells["Name"].EditedFormattedValue.ToString() == ""
                        || this.grdConnections.CurrentRow.Cells["DatabaseName"].EditedFormattedValue.ToString() == "")
                    {
                        //User is attempting to omit required data, warn and cancel cell change
                        this.IsValid = false;
                        MessageBox.Show(
                            LMP.Resources.GetLangString("Error_NameAndDatabaseNameAreRequiredFields"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);

                        e.Cancel = true;
                    }
                    else
                    {
                        this.IsValid = true;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdConnections_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                    //Delete the currently selected record
                    this.DeleteConnection();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdConnections_Leave(object sender, System.EventArgs e)
        {
            try
            {
                //this event fires before cell or row validating events
                //make sure we have valid data in current row
                if (this.grdConnections.CurrentRow.Cells["Name"].EditedFormattedValue.ToString() == ""
                   || this.grdConnections.CurrentRow.Cells["DatabaseName"].EditedFormattedValue.ToString() == "")
                {
                    this.IsValid = false;
                    return;
                }

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdConnections_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                if (e.Exception is IndexOutOfRangeException 
                    || e.Exception is FormatException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteConnection();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tsNew_Click(object sender, EventArgs e)
        {
            try
            {
                grdConnections.EndEdit();
                m_oBindingSource.EndEdit();
                string xName = null;
                if (grdConnections.CurrentCell != null)
                {
                    int iInd = grdConnections.CurrentCell.ColumnIndex;

                    if (iInd == grdConnections.Columns["DatabaseName"].Index ||
                        iInd == grdConnections.Columns["Name"].Index)
                    {
                        xName = grdConnections.CurrentCell.EditedFormattedValue.ToString();
                        this.IsValid = ValidateConnectionName(xName);
                        return;
                    }
                }
                this.SaveCurrentRecord();
                UpdateExternalConnection(true); 
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
#endregion

        private void grdConnections_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }
        }
        //GLOG : 8730 : jsw
        private void grdConnections_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (grdConnections.Columns[e.ColumnIndex].Name == "DatabaseType")
            {
                switch ((ExternalConnection.DatabaseTypes) e.Value)
                {
                    case ExternalConnection.DatabaseTypes.SQLServer:
                        e.Value = "SQLServer";
                        break;
                    case ExternalConnection.DatabaseTypes.Oracle:
                        e.Value = "Oracle";
                        break;
                    case ExternalConnection.DatabaseTypes.Access:
                        e.Value = "Access";
                        break;
                }
            }
        }
    }
}

