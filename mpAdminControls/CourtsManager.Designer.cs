namespace LMP.Administration.Controls
{
    partial class CourtsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CourtsManager));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.scInternalLists = new System.Windows.Forms.SplitContainer();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tsep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tlblJurisdiction = new System.Windows.Forms.ToolStripLabel();
            this.tsep3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.treeJurisdictions = new LMP.Controls.JurisdictionsTree();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnClear = new System.Windows.Forms.ToolStripButton();
            this.grdItems = new System.Windows.Forms.DataGridView();
            this.grdProperties = new System.Windows.Forms.DataGridView();
            this.cmsJurisdictionOps = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPaste = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.scInternalLists)).BeginInit();
            this.scInternalLists.Panel1.SuspendLayout();
            this.scInternalLists.Panel2.SuspendLayout();
            this.scInternalLists.SuspendLayout();
            this.tsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeJurisdictions)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).BeginInit();
            this.cmsJurisdictionOps.SuspendLayout();
            this.SuspendLayout();
            // 
            // scInternalLists
            // 
            this.scInternalLists.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scInternalLists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scInternalLists.Location = new System.Drawing.Point(0, 0);
            this.scInternalLists.Name = "scInternalLists";
            // 
            // scInternalLists.Panel1
            // 
            this.scInternalLists.Panel1.Controls.Add(this.tsMain);
            this.scInternalLists.Panel1.Controls.Add(this.treeJurisdictions);
            // 
            // scInternalLists.Panel2
            // 
            this.scInternalLists.Panel2.Controls.Add(this.toolStrip1);
            this.scInternalLists.Panel2.Controls.Add(this.grdItems);
            this.scInternalLists.Panel2.Controls.Add(this.grdProperties);
            this.scInternalLists.Size = new System.Drawing.Size(616, 561);
            this.scInternalLists.SplitterDistance = 241;
            this.scInternalLists.TabIndex = 29;
            // 
            // tsMain
            // 
            this.tsMain.AutoSize = false;
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsep1,
            this.tlblJurisdiction,
            this.tsep3,
            this.tbtnAdd,
            this.toolStripSeparator1,
            this.tbtnDelete});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(-4, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMain.Size = new System.Drawing.Size(244, 29);
            this.tsMain.Stretch = true;
            this.tsMain.TabIndex = 29;
            // 
            // tsep1
            // 
            this.tsep1.Name = "tsep1";
            this.tsep1.Size = new System.Drawing.Size(6, 29);
            // 
            // tlblJurisdiction
            // 
            this.tlblJurisdiction.AutoSize = false;
            this.tlblJurisdiction.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tlblJurisdiction.Name = "tlblJurisdiction";
            this.tlblJurisdiction.Size = new System.Drawing.Size(80, 22);
            this.tlblJurisdiction.Text = "Jurisdictions";
            this.tlblJurisdiction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsep3
            // 
            this.tsep3.Name = "tsep3";
            this.tsep3.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnAdd
            // 
            this.tbtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("tbtnAdd.Image")));
            this.tbtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAdd.Name = "tbtnAdd";
            this.tbtnAdd.Size = new System.Drawing.Size(33, 26);
            this.tbtnAdd.Text = "&Add";
            this.tbtnAdd.Click += new System.EventHandler(this.tbtnAdd_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(44, 26);
            this.tbtnDelete.Text = "&Delete";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // treeJurisdictions
            // 
            this.treeJurisdictions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeJurisdictions.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.treeJurisdictions.HideSelection = false;
            this.treeJurisdictions.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.treeJurisdictions.IsDirty = false;
            this.treeJurisdictions.LevelDepth = -1;
            this.treeJurisdictions.Location = new System.Drawing.Point(-1, 35);
            this.treeJurisdictions.Name = "treeJurisdictions";
            this.treeJurisdictions.NodeConnectorColor = System.Drawing.SystemColors.ControlDark;
            this.treeJurisdictions.Size = new System.Drawing.Size(239, 524);
            this.treeJurisdictions.SupportingValues = "";
            this.treeJurisdictions.TabIndex = 1;
            this.treeJurisdictions.Tag2 = null;
            this.treeJurisdictions.Value = null;
            this.treeJurisdictions.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeJurisdictions_AfterSelect);
            this.treeJurisdictions.MouseClick += new System.Windows.Forms.MouseEventHandler(this.treeJurisdictions_MouseClick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator4,
            this.toolStripLabel2,
            this.toolStripSeparator7,
            this.tbtnClear});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(-4, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(373, 29);
            this.toolStrip1.TabIndex = 30;
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 29);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.AutoSize = false;
            this.toolStripLabel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(55, 22);
            this.toolStripLabel2.Text = "Court";
            this.toolStripLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnClear
            // 
            this.tbtnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnClear.Image = ((System.Drawing.Image)(resources.GetObject("tbtnClear.Image")));
            this.tbtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnClear.Name = "tbtnClear";
            this.tbtnClear.Size = new System.Drawing.Size(47, 26);
            this.tbtnClear.Text = "&Clear...";
            this.tbtnClear.Click += new System.EventHandler(this.tbtnClear_Click);
            // 
            // grdItems
            // 
            this.grdItems.AllowUserToAddRows = false;
            this.grdItems.AllowUserToResizeRows = false;
            this.grdItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdItems.BackgroundColor = System.Drawing.Color.White;
            this.grdItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdItems.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdItems.Location = new System.Drawing.Point(0, 36);
            this.grdItems.MultiSelect = false;
            this.grdItems.Name = "grdItems";
            this.grdItems.RowHeadersVisible = false;
            this.grdItems.RowHeadersWidth = 25;
            this.grdItems.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdItems.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdItems.Size = new System.Drawing.Size(367, 518);
            this.grdItems.TabIndex = 39;
            this.grdItems.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdItems_CellValidated);
            this.grdItems.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdItems_CellValidating);
            // 
            // grdProperties
            // 
            this.grdProperties.AllowUserToAddRows = false;
            this.grdProperties.AllowUserToDeleteRows = false;
            this.grdProperties.AllowUserToResizeColumns = false;
            this.grdProperties.AllowUserToResizeRows = false;
            this.grdProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProperties.BackgroundColor = System.Drawing.Color.White;
            this.grdProperties.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdProperties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProperties.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.DefaultCellStyle = dataGridViewCellStyle1;
            this.grdProperties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdProperties.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grdProperties.Location = new System.Drawing.Point(3, 36);
            this.grdProperties.MultiSelect = false;
            this.grdProperties.Name = "grdProperties";
            this.grdProperties.RowHeadersVisible = false;
            this.grdProperties.RowTemplate.Height = 20;
            this.grdProperties.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdProperties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdProperties.Size = new System.Drawing.Size(356, 524);
            this.grdProperties.TabIndex = 38;
            // 
            // cmsJurisdictionOps
            // 
            this.cmsJurisdictionOps.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCopy,
            this.tsmiPaste});
            this.cmsJurisdictionOps.Name = "cmsJurisdictionOps";
            this.cmsJurisdictionOps.Size = new System.Drawing.Size(151, 48);
            this.cmsJurisdictionOps.Opening += new System.ComponentModel.CancelEventHandler(this.cmsJurisdictionOps_Opening);
            this.cmsJurisdictionOps.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cmsJurisdictionOps_ItemClicked);
            // 
            // tsmiCopy
            // 
            this.tsmiCopy.Name = "tsmiCopy";
            this.tsmiCopy.Size = new System.Drawing.Size(150, 22);
            this.tsmiCopy.Text = "Copy Children";
            // 
            // tsmiPaste
            // 
            this.tsmiPaste.Name = "tsmiPaste";
            this.tsmiPaste.Size = new System.Drawing.Size(150, 22);
            this.tsmiPaste.Text = "Paste Children";
            // 
            // CourtsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.scInternalLists);
            this.Name = "CourtsManager";
            this.Size = new System.Drawing.Size(616, 561);
            this.Load += new System.EventHandler(this.CourtsManager_Load);
            this.scInternalLists.Panel1.ResumeLayout(false);
            this.scInternalLists.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scInternalLists)).EndInit();
            this.scInternalLists.ResumeLayout(false);
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeJurisdictions)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).EndInit();
            this.cmsJurisdictionOps.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer scInternalLists;
        private System.Windows.Forms.DataGridView grdProperties;
        private LMP.Controls.JurisdictionsTree treeJurisdictions;
        private System.Windows.Forms.DataGridView grdItems;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tbtnClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripSeparator tsep1;
        private System.Windows.Forms.ToolStripLabel tlblJurisdiction;
        private System.Windows.Forms.ToolStripSeparator tsep3;
        private System.Windows.Forms.ToolStripButton tbtnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ContextMenuStrip cmsJurisdictionOps;
        private System.Windows.Forms.ToolStripMenuItem tsmiCopy;
        private System.Windows.Forms.ToolStripMenuItem tsmiPaste;
    }
}
