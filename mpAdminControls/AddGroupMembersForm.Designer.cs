namespace LMP.Administration.Controls
{
    partial class AddGroupMembersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblAllPeople = new System.Windows.Forms.Label();
            this.grdPeople = new System.Windows.Forms.DataGridView();
            this.Add = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lblSearchString = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(253, 287);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnCancel_KeyPress);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(172, 287);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 25);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "O&K";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            this.btnOK.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnOK_KeyPress);
            // 
            // lblAllPeople
            // 
            this.lblAllPeople.AutoSize = true;
            this.lblAllPeople.Location = new System.Drawing.Point(10, 10);
            this.lblAllPeople.Name = "lblAllPeople";
            this.lblAllPeople.Size = new System.Drawing.Size(89, 15);
            this.lblAllPeople.TabIndex = 4;
            this.lblAllPeople.Text = "&Available People:";
            // 
            // grdPeople
            // 
            this.grdPeople.AllowUserToAddRows = false;
            this.grdPeople.AllowUserToDeleteRows = false;
            this.grdPeople.AllowUserToResizeColumns = false;
            this.grdPeople.AllowUserToResizeRows = false;
            this.grdPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdPeople.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.grdPeople.BackgroundColor = System.Drawing.Color.White;
            this.grdPeople.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdPeople.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPeople.ColumnHeadersVisible = false;
            this.grdPeople.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Add});
            this.grdPeople.Location = new System.Drawing.Point(11, 27);
            this.grdPeople.MultiSelect = false;
            this.grdPeople.Name = "grdPeople";
            this.grdPeople.RowHeadersVisible = false;
            this.grdPeople.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdPeople.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdPeople.Size = new System.Drawing.Size(315, 249);
            this.grdPeople.TabIndex = 5;
            this.grdPeople.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdPeople_RowValidated);
            this.grdPeople.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.grdPeople_KeyPress);
            // 
            // Add
            // 
            this.Add.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Add.Frozen = true;
            this.Add.HeaderText = "Add";
            this.Add.MinimumWidth = 20;
            this.Add.Name = "Add";
            this.Add.Width = 20;
            // 
            // lblSearchString
            // 
            this.lblSearchString.AutoSize = true;
            this.lblSearchString.Location = new System.Drawing.Point(12, 292);
            this.lblSearchString.Name = "lblSearchString";
            this.lblSearchString.Size = new System.Drawing.Size(35, 15);
            this.lblSearchString.TabIndex = 6;
            this.lblSearchString.Text = "label1";
            // 
            // AddGroupMembersForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(338, 324);
            this.Controls.Add(this.lblSearchString);
            this.Controls.Add(this.lblAllPeople);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.grdPeople);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddGroupMembersForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Members";
            this.Load += new System.EventHandler(this.AddPeopleToGroupForm_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AddGroupMembersForm_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblAllPeople;
        private System.Windows.Forms.DataGridView grdPeople;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Add;
        private System.Windows.Forms.Label lblSearchString;
    }
}