using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Infragistics.Win.UltraWinTree;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class JurisdictionsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ***************fields*****************

        Jurisdictions m_oCurJurisdictions;
        bool m_bGrdItemsEdited = false;
        bool m_bGrdItemsBound = false;

        #endregion
        #region ***************constructors****************

        public JurisdictionsManager()
        {
            InitializeComponent();
        }
        #endregion
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
                #endregion
        #region **************methods*******************

        /// <summary>
        /// Display the sub-jurisdictions of the selected jurisdiction
        /// in grdItems
        /// </summary>
        private void DataBindGrdItems()
        {
            m_bGrdItemsBound = false;

            //Store the current treeJurisdictions node
            UltraTreeNode oCurNode = (UltraTreeNode)
                this.treeJurisdictions.SelectedNodes.GetItem(0);

            if (oCurNode.Level > 4)
            {
                //A jurisdiction that cannot have 
                //sub-jurisdictions has been selected-
                //clear the jurisdictions displayed in grdItems

                DataTable oCurDT = (DataTable)this.grdItems.DataSource;

                oCurDT.Clear();

                this.grdItems.DataSource = oCurDT;

                //Prevent the user from attempting to add children
                this.grdItems.AllowUserToAddRows = false;
            }
            else
            {
                //Make sure that the user can add rows to grdItems
                this.grdItems.AllowUserToAddRows = true;

                switch (oCurNode.Level)
                {
                    //Get a Jurisdictions object representative of the 
                    //children of the expanding jurisdiction- the cases
                    //specify which level of jurisdictions should be returned

                    case 0:
                        m_oCurJurisdictions = new Jurisdictions(Jurisdictions.Levels.Zero);
                        break;

                    case 1:
                        m_oCurJurisdictions = new Jurisdictions
                             (Jurisdictions.Levels.One, (int)((object[])oCurNode.Tag)[0]);
                        break;

                    case 2:
                        m_oCurJurisdictions = new Jurisdictions
                             (Jurisdictions.Levels.Two, (int)((object[])oCurNode.Tag)[0]);
                        break;

                    case 3:
                        m_oCurJurisdictions = new Jurisdictions
                             (Jurisdictions.Levels.Three, (int)((object[])oCurNode.Tag)[0]);
                        break;

                    case 4:
                        m_oCurJurisdictions = new Jurisdictions
                             (Jurisdictions.Levels.Four, (int)((object[])oCurNode.Tag)[0]);
                        break;
                }

                //Get a DataTable of the child jurisdictions
                DataTable oDT = m_oCurJurisdictions.ToDataSet().Tables[0];

                //Set the DT of child jurisdictions as the DataSource
                //for grdItems
                this.grdItems.DataSource = oDT;

                //Set the display properties of grdItems
                this.grdItems.Columns[0].Visible = false;
                this.grdItems.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                if (this.grdItems.Columns.Count > 2)
                    this.grdItems.Columns[2].Visible = false;

            }

            m_bGrdItemsBound = true;
        }

        /// <summary>
        /// Returns false if the user is attempting to give a 
        /// jurisdiction a blank name- true if not
        /// </summary>
        /// <returns></returns>
        private bool GirdEditIsValid()
        {
            //Get the current node in treeJurisdictions
            UltraTreeNode oCurNode = (UltraTreeNode)
                this.treeJurisdictions.SelectedNodes.GetItem(0);

            //Get the new name the user has entered
            string xCurCellText = this.grdItems.
                CurrentCell.EditedFormattedValue.ToString().Trim();

            if (System.String.IsNullOrEmpty(xCurCellText))
            {
                //The user is attempting to give a jurisdiction
                //a blank name- reset the cur cell value and
                //return false

                this.grdItems.CurrentCell.Value = 
                    this.grdItems.CurrentCell.FormattedValue;

                return false;
            }

            return true;
        }

        /// <summary>
        /// returns true if the current row is 
        /// a new row that has been edited
        /// </summary>
        /// <returns></returns>
        private bool CurrentRowIsNew()
        {
            //get ID
            object oID = this.grdItems.CurrentRow.Cells[0].Value;

            return !this.grdItems.CurrentRow.IsNewRow &&
                (oID == null || oID.ToString() == "");
        }

        /// <summary>
        /// Stores edits made to the currently displayed
        /// jurisdictions in grdItems to the DB
        /// </summary>
        private void UpdateCurJurisdictions()
        {

            if (this.CurrentRowIsNew())
            {
                //The user is adding a new record
                this.AddNewRecord();
            }

            else
            {

                //Get the dirty jurisdiction
                Jurisdiction oDirtyJurisdiction = (Jurisdiction)m_oCurJurisdictions.
                    ItemFromID(Int32.Parse(this.grdItems.CurrentRow.Cells[0].FormattedValue.ToString()));

                //Store the pre-edit name of the current
                //jurisdiction
                string xPreEditName = oDirtyJurisdiction.Name;

                //Edit the name of dirty Jurisdiction to the new name
                //the user entered
                oDirtyJurisdiction.Name = this.grdItems.CurrentCell.
                    EditedFormattedValue.ToString();

                try
                {
                    //Save the current jurisdiction
                    m_oCurJurisdictions.Save(oDirtyJurisdiction);
                }
                catch(System.Exception oE)
                {
                    //Update failed- 
                    //reset the name that was entered
                    this.grdItems.CurrentCell.Value = xPreEditName;

                    //alert
                    throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString(
                        "Error_CouldNotSaveChangesToDB"), oE);
                }

                if ((bool)((object[])this.treeJurisdictions.SelectedNodes[0].Tag)[1])
                    //Change the name of the current jurisdiction in treeJurisdictions
                    this.treeJurisdictions.SelectedNodes[0].Nodes
                        [this.grdItems.CurrentRow.Index].Text = oDirtyJurisdiction.Name;
            }
        }

        /// <summary>
        /// Adds a new jurisdiction to the DB when the 
        /// user creates a new row in grdItems
        /// </summary>
        private void AddNewRecord()
        {
            //Get the current node
            UltraTreeNode oCurNode = this.treeJurisdictions.SelectedNodes[0];

            int iParentJurisdictionID = (int)((object[])oCurNode.Tag)[0];

            //Create a new Jurisdiction record
            Jurisdiction oNewJurisdiction = (Jurisdiction)m_oCurJurisdictions.Create();

            //Set the properties of the new Jurisdiction
            oNewJurisdiction.ParentID = iParentJurisdictionID;
            oNewJurisdiction.Name = this.grdItems.CurrentCell.
                EditedFormattedValue.ToString();

            try
            {
                //Save the new Jurisdiction to the DB
                m_oCurJurisdictions.Save(oNewJurisdiction);
            }
            catch (System.Exception oE)
            {
                //Update failed- 
                //clear the name that was entered
                this.grdItems.CurrentCell.Value = "";

                //alert
                throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString(
                    "Error_CouldNotSaveChangesToDB"), oE);
            }

            //Store the ID of the new jurisdiction in grdItems
            this.grdItems.CurrentRow.Cells[0].Value = oNewJurisdiction.ID;

            if ((bool)((object[])oCurNode.Tag)[1])
            {
                //The selected node has already been
                //expanded and bound- add a node for 
                //the new record

                //Create a node for the new record
                UltraTreeNode oNewNode = new UltraTreeNode("", oNewJurisdiction.Name);

                //Create the tag for the new node
                object[] aNodeIDandIsBound = new object[2];
                aNodeIDandIsBound[0] = oNewJurisdiction.ID;
                aNodeIDandIsBound[1] = false;

                //Set the tag for the new node
                oNewNode.Tag = aNodeIDandIsBound;

                //Add the new node as a child of the selected node
                oCurNode.Nodes.Add(oNewNode);
            }
        }

        /// <summary>
        /// Delete the currently selected jurisdiction in grdItems from the DB
        /// </summary>
        private void DeleteJurisdiction()
        {

            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                //Get the current grdItems row
                DataGridViewRow oCurRow = this.grdItems.CurrentRow;

                //Get the index of the record to be deleted
                int iInd = oCurRow.Index;

                //Delete the current record from the DB - if it has been saved
                if (oCurRow.Cells[0].FormattedValue.ToString() != string.Empty)
                    m_oCurJurisdictions.Delete(Int32.Parse(oCurRow.Cells[0].FormattedValue.ToString()));

                if ((bool)((object[])this.treeJurisdictions.SelectedNodes[0].Tag)[1])
                    //Delete the current record from treeJurisdictions
                    this.treeJurisdictions.SelectedNodes[0].Nodes.RemoveAt(oCurRow.Index);
                
                //Remove the current record from grdItems
                this.grdItems.Rows.RemoveAt(oCurRow.Index);

                //reset the current row
                if (this.grdItems.RowCount > 0)
                {
                    if (iInd == grdItems.Rows.Count - 1)
                        iInd = Math.Max(0, iInd - 1);
                    this.grdItems.CurrentCell = grdItems.Rows[iInd].Cells[1];
                    this.grdItems.CurrentCell.Selected = true;
                }
            }
        }
        #endregion
        #region **************event handlers***************

        private void JurisdictionsManager_Load(object sender, EventArgs e)
        {
            try
            {
                this.treeJurisdictions.LevelDepth = 4;
                this.treeJurisdictions.Focus();
                //this.treeJurisdictions.Value = "0.1";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdItems_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bGrdItemsEdited)
                {
                    //Save edits to grdItems to the DB
                    this.UpdateCurJurisdictions();
                    
                    //Note that the changes have been made
                    m_bGrdItemsEdited = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void treeJurisdictions_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                string xPath = this.treeJurisdictions.SelectedNodes[0].FullPath;
                this.tsPath.Text = xPath.Replace(@"\\", @"\");
                this.DataBindGrdItems();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdItems_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bGrdItemsBound)
                    if (this.grdItems.CurrentCell.FormattedValue.ToString()
                       != this.grdItems.CurrentCell.EditedFormattedValue.ToString())
                        if (this.GirdEditIsValid())
                            //User has entered valid edits-
                            //note that the DB should be updated
                            m_bGrdItemsEdited = true;

                        else
                            //User has entered an invalid Jurisdiction Name-
                            //Cancel selection change
                            e.Cancel = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdItems_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                    this.DeleteJurisdiction();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles deletion of selected Jurisdiction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnJurisdictionDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdItems.CurrentRow != null)
                    DeleteJurisdiction();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
    }
}

