namespace LMP.Administration.Controls
{
    partial class OfficesManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OfficesManager));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tlblOffices = new System.Windows.Forms.ToolStripLabel();
            this.tsep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNew = new System.Windows.Forms.ToolStripButton();
            this.tsep2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.tsep3 = new System.Windows.Forms.ToolStripSeparator();
            this.tlblFindGroup = new System.Windows.Forms.ToolStripLabel();
            this.ttxtFindOffice = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.scInternalLists = new System.Windows.Forms.SplitContainer();
            this.grdOffices = new LMP.Controls.DataGridView();
            this.grdOfficeProperties = new System.Windows.Forms.DataGridView();
            this.tsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scInternalLists)).BeginInit();
            this.scInternalLists.Panel1.SuspendLayout();
            this.scInternalLists.Panel2.SuspendLayout();
            this.scInternalLists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOffices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOfficeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // tsMain
            // 
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblOffices,
            this.tsep1,
            this.tbtnNew,
            this.tsep2,
            this.tbtnDelete,
            this.tsep3,
            this.tlblFindGroup,
            this.ttxtFindOffice,
            this.toolStripSeparator1});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMain.Size = new System.Drawing.Size(671, 25);
            this.tsMain.TabIndex = 19;
            // 
            // tlblOffices
            // 
            this.tlblOffices.AutoSize = false;
            this.tlblOffices.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblOffices.Name = "tlblOffices";
            this.tlblOffices.Size = new System.Drawing.Size(75, 22);
            this.tlblOffices.Text = "Offices";
            this.tlblOffices.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsep1
            // 
            this.tsep1.Name = "tsep1";
            this.tsep1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnNew
            // 
            this.tbtnNew.AutoSize = false;
            this.tbtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNew.Image")));
            this.tbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNew.Name = "tbtnNew";
            this.tbtnNew.Size = new System.Drawing.Size(53, 22);
            this.tbtnNew.Text = "&New...";
            this.tbtnNew.Click += new System.EventHandler(this.tbtnNew_Click);
            // 
            // tsep2
            // 
            this.tsep2.Name = "tsep2";
            this.tsep2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(53, 22);
            this.tbtnDelete.Text = "&Delete...";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // tsep3
            // 
            this.tsep3.Name = "tsep3";
            this.tsep3.Size = new System.Drawing.Size(6, 25);
            // 
            // tlblFindGroup
            // 
            this.tlblFindGroup.AutoSize = false;
            this.tlblFindGroup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tlblFindGroup.Image = ((System.Drawing.Image)(resources.GetObject("tlblFindGroup.Image")));
            this.tlblFindGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlblFindGroup.Name = "tlblFindGroup";
            this.tlblFindGroup.Size = new System.Drawing.Size(50, 22);
            this.tlblFindGroup.Text = "&Find:";
            // 
            // ttxtFindOffice
            // 
            this.ttxtFindOffice.Name = "ttxtFindOffice";
            this.ttxtFindOffice.Size = new System.Drawing.Size(250, 25);
            this.ttxtFindOffice.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ttxtFindOffice_KeyUp);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // scInternalLists
            // 
            this.scInternalLists.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scInternalLists.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scInternalLists.Location = new System.Drawing.Point(0, 25);
            this.scInternalLists.Name = "scInternalLists";
            // 
            // scInternalLists.Panel1
            // 
            this.scInternalLists.Panel1.Controls.Add(this.grdOffices);
            // 
            // scInternalLists.Panel2
            // 
            this.scInternalLists.Panel2.Controls.Add(this.grdOfficeProperties);
            this.scInternalLists.Size = new System.Drawing.Size(671, 439);
            this.scInternalLists.SplitterDistance = 223;
            this.scInternalLists.TabIndex = 25;
            // 
            // grdOffices
            // 
            this.grdOffices.AllowUserToAddRows = false;
            this.grdOffices.AllowUserToDeleteRows = false;
            this.grdOffices.AllowUserToResizeColumns = false;
            this.grdOffices.AllowUserToResizeRows = false;
            this.grdOffices.BackgroundColor = System.Drawing.Color.White;
            this.grdOffices.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdOffices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdOffices.ColumnHeadersHeight = 21;
            this.grdOffices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdOffices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdOffices.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.grdOffices.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdOffices.IsDirty = false;
            this.grdOffices.Location = new System.Drawing.Point(0, 0);
            this.grdOffices.MultiSelect = false;
            this.grdOffices.Name = "grdOffices";
            this.grdOffices.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdOffices.RowHeadersVisible = false;
            this.grdOffices.RowHeadersWidth = 25;
            this.grdOffices.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdOffices.RowTemplate.Height = 20;
            this.grdOffices.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdOffices.Size = new System.Drawing.Size(219, 435);
            this.grdOffices.SupportingValues = "";
            this.grdOffices.TabIndex = 3;
            this.grdOffices.Tag2 = null;
            this.grdOffices.Value = null;
            this.grdOffices.TabPressed += new LMP.Controls.TabPressedHandler(this.grdOffices_TabPressed);
            this.grdOffices.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOffices_CellEnter);
            this.grdOffices.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOffices_CellValidated);
            this.grdOffices.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdOffices_CellValidating);
            this.grdOffices.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdOffices_ColumnAdded);
            this.grdOffices.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdOffices_KeyDown);
            this.grdOffices.Leave += new System.EventHandler(this.grdOffices_Leave);
            // 
            // grdOfficeProperties
            // 
            this.grdOfficeProperties.AllowUserToAddRows = false;
            this.grdOfficeProperties.AllowUserToDeleteRows = false;
            this.grdOfficeProperties.AllowUserToResizeColumns = false;
            this.grdOfficeProperties.AllowUserToResizeRows = false;
            this.grdOfficeProperties.BackgroundColor = System.Drawing.Color.White;
            this.grdOfficeProperties.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdOfficeProperties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdOfficeProperties.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdOfficeProperties.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdOfficeProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdOfficeProperties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdOfficeProperties.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grdOfficeProperties.Location = new System.Drawing.Point(0, 0);
            this.grdOfficeProperties.MultiSelect = false;
            this.grdOfficeProperties.Name = "grdOfficeProperties";
            this.grdOfficeProperties.RowHeadersVisible = false;
            this.grdOfficeProperties.RowTemplate.Height = 20;
            this.grdOfficeProperties.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdOfficeProperties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdOfficeProperties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdOfficeProperties.Size = new System.Drawing.Size(440, 435);
            this.grdOfficeProperties.TabIndex = 35;
            this.grdOfficeProperties.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOfficeProperties_CellEnter);
            this.grdOfficeProperties.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdOfficeProperties_CellValidated);
            this.grdOfficeProperties.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdOfficeProperties_CellValidating);
            // 
            // OfficesManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.scInternalLists);
            this.Controls.Add(this.tsMain);
            this.Name = "OfficesManager";
            this.Size = new System.Drawing.Size(671, 464);
            this.Load += new System.EventHandler(this.OfficesManager_Load);
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.scInternalLists.Panel1.ResumeLayout(false);
            this.scInternalLists.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scInternalLists)).EndInit();
            this.scInternalLists.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdOffices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdOfficeProperties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripLabel tlblOffices;
        private System.Windows.Forms.ToolStripSeparator tsep1;
        private System.Windows.Forms.ToolStripLabel tlblFindGroup;
        private System.Windows.Forms.ToolStripTextBox ttxtFindOffice;
        private System.Windows.Forms.ToolStripSeparator tsep2;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ToolStripSeparator tsep3;
        private System.Windows.Forms.SplitContainer scInternalLists;
        private LMP.Controls.DataGridView grdOffices;
        private System.Windows.Forms.DataGridView grdOfficeProperties;
        private System.Windows.Forms.ToolStripButton tbtnNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;

    }
}
