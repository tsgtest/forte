namespace LMP.Administration.Controls
{
    partial class PeopleManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PeopleManager));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tlblOffices = new System.Windows.Forms.ToolStripLabel();
            this.tsep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnImport = new System.Windows.Forms.ToolStripButton();
            this.tsep2 = new System.Windows.Forms.ToolStripSeparator();
            this.tlblFindGroup = new System.Windows.Forms.ToolStripLabel();
            this.ttxtFindPerson = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.grdPeople = new LMP.Controls.DataGridView();
            this.scPeople = new System.Windows.Forms.SplitContainer();
            this.tabPersonDetail = new System.Windows.Forms.TabControl();
            this.pageDetail = new System.Windows.Forms.TabPage();
            this.chkIsPrimaryOffice = new System.Windows.Forms.CheckBox();
            this.lblOffice = new System.Windows.Forms.Label();
            this.cboxOffices = new System.Windows.Forms.ComboBox();
            this.grdProperties = new System.Windows.Forms.DataGridView();
            this.pageOffices = new System.Windows.Forms.TabPage();
            this.lstOffices = new System.Windows.Forms.CheckedListBox();
            this.pageLicenses = new System.Windows.Forms.TabPage();
            this.tsLicense = new System.Windows.Forms.ToolStrip();
            this.tbtnDeleteLicense = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripSeparator();
            this.grdLicenses = new System.Windows.Forms.DataGridView();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).BeginInit();
            this.scPeople.Panel1.SuspendLayout();
            this.scPeople.Panel2.SuspendLayout();
            this.scPeople.SuspendLayout();
            this.tabPersonDetail.SuspendLayout();
            this.pageDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).BeginInit();
            this.pageOffices.SuspendLayout();
            this.pageLicenses.SuspendLayout();
            this.tsLicense.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLicenses)).BeginInit();
            this.SuspendLayout();
            // 
            // tsMain
            // 
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblOffices,
            this.tsep1,
            this.tbtnNew,
            this.toolStripSeparator1,
            this.tbtnDelete,
            this.toolStripSeparator2,
            this.tbtnImport,
            this.tsep2,
            this.tlblFindGroup,
            this.ttxtFindPerson,
            this.toolStripSeparator3});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMain.Size = new System.Drawing.Size(624, 25);
            this.tsMain.TabIndex = 20;
            // 
            // tlblOffices
            // 
            this.tlblOffices.AutoSize = false;
            this.tlblOffices.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblOffices.Name = "tlblOffices";
            this.tlblOffices.Size = new System.Drawing.Size(70, 22);
            this.tlblOffices.Text = "People";
            this.tlblOffices.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsep1
            // 
            this.tsep1.Name = "tsep1";
            this.tsep1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnNew
            // 
            this.tbtnNew.AutoSize = false;
            this.tbtnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tbtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNew.Image")));
            this.tbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNew.Name = "tbtnNew";
            this.tbtnNew.Size = new System.Drawing.Size(56, 22);
            this.tbtnNew.Text = "&New...";
            this.tbtnNew.Click += new System.EventHandler(this.tbtnNew_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.AutoSize = false;
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(56, 22);
            this.tbtnDelete.Text = "&Delete...";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtnImport
            // 
            this.tbtnImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnImport.Image = ((System.Drawing.Image)(resources.GetObject("tbtnImport.Image")));
            this.tbtnImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnImport.Name = "tbtnImport";
            this.tbtnImport.Size = new System.Drawing.Size(55, 22);
            this.tbtnImport.Text = "&Import...";
            this.tbtnImport.Click += new System.EventHandler(this.tbtnImport_Click);
            // 
            // tsep2
            // 
            this.tsep2.AutoSize = false;
            this.tsep2.Name = "tsep2";
            this.tsep2.Size = new System.Drawing.Size(6, 25);
            // 
            // tlblFindGroup
            // 
            this.tlblFindGroup.AutoSize = false;
            this.tlblFindGroup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tlblFindGroup.Image = ((System.Drawing.Image)(resources.GetObject("tlblFindGroup.Image")));
            this.tlblFindGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlblFindGroup.Name = "tlblFindGroup";
            this.tlblFindGroup.Size = new System.Drawing.Size(80, 22);
            this.tlblFindGroup.Text = "&Find Person:";
            // 
            // ttxtFindPerson
            // 
            this.ttxtFindPerson.Name = "ttxtFindPerson";
            this.ttxtFindPerson.Size = new System.Drawing.Size(250, 25);
            this.ttxtFindPerson.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ttxtFindPerson_KeyUp);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // grdPeople
            // 
            this.grdPeople.AllowUserToAddRows = false;
            this.grdPeople.AllowUserToDeleteRows = false;
            this.grdPeople.AllowUserToResizeColumns = false;
            this.grdPeople.AllowUserToResizeRows = false;
            this.grdPeople.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdPeople.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.grdPeople.BackgroundColor = System.Drawing.Color.White;
            this.grdPeople.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdPeople.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPeople.ColumnHeadersVisible = false;
            this.grdPeople.GridColor = System.Drawing.SystemColors.Menu;
            this.grdPeople.IsDirty = false;
            this.grdPeople.Location = new System.Drawing.Point(0, 0);
            this.grdPeople.MultiSelect = false;
            this.grdPeople.Name = "grdPeople";
            this.grdPeople.RowHeadersVisible = false;
            this.grdPeople.RowTemplate.Height = 20;
            this.grdPeople.Size = new System.Drawing.Size(203, 463);
            this.grdPeople.TabIndex = 44;
            this.grdPeople.Tag2 = null;
            this.grdPeople.Value = null;
            this.grdPeople.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdPeople_CellValidated);
            this.grdPeople.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdPeople_CellValidating);
            this.grdPeople.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdPeople_DataError);
            this.grdPeople.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdPeople_CellEnter);
            // 
            // scPeople
            // 
            this.scPeople.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scPeople.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scPeople.Location = new System.Drawing.Point(0, 25);
            this.scPeople.Name = "scPeople";
            // 
            // scPeople.Panel1
            // 
            this.scPeople.Panel1.Controls.Add(this.grdPeople);
            // 
            // scPeople.Panel2
            // 
            this.scPeople.Panel2.Controls.Add(this.tabPersonDetail);
            this.scPeople.Size = new System.Drawing.Size(624, 467);
            this.scPeople.SplitterDistance = 207;
            this.scPeople.SplitterWidth = 1;
            this.scPeople.TabIndex = 46;
            // 
            // tabPersonDetail
            // 
            this.tabPersonDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabPersonDetail.Controls.Add(this.pageDetail);
            this.tabPersonDetail.Controls.Add(this.pageOffices);
            this.tabPersonDetail.Controls.Add(this.pageLicenses);
            this.tabPersonDetail.Location = new System.Drawing.Point(2, 4);
            this.tabPersonDetail.Name = "tabPersonDetail";
            this.tabPersonDetail.SelectedIndex = 0;
            this.tabPersonDetail.Size = new System.Drawing.Size(410, 459);
            this.tabPersonDetail.TabIndex = 44;
            this.tabPersonDetail.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabPersonDetail_Selecting);
            this.tabPersonDetail.SelectedIndexChanged += new System.EventHandler(this.tabPersonDetail_SelectedIndexChanged);
            // 
            // pageDetail
            // 
            this.pageDetail.Controls.Add(this.chkIsPrimaryOffice);
            this.pageDetail.Controls.Add(this.lblOffice);
            this.pageDetail.Controls.Add(this.cboxOffices);
            this.pageDetail.Controls.Add(this.grdProperties);
            this.pageDetail.Location = new System.Drawing.Point(4, 22);
            this.pageDetail.Name = "pageDetail";
            this.pageDetail.Padding = new System.Windows.Forms.Padding(3);
            this.pageDetail.Size = new System.Drawing.Size(402, 433);
            this.pageDetail.TabIndex = 0;
            this.pageDetail.Text = "Detail";
            this.pageDetail.UseVisualStyleBackColor = true;
            // 
            // chkIsPrimaryOffice
            // 
            this.chkIsPrimaryOffice.AutoSize = true;
            this.chkIsPrimaryOffice.Location = new System.Drawing.Point(281, 21);
            this.chkIsPrimaryOffice.Name = "chkIsPrimaryOffice";
            this.chkIsPrimaryOffice.Size = new System.Drawing.Size(102, 17);
            this.chkIsPrimaryOffice.TabIndex = 39;
            this.chkIsPrimaryOffice.Text = "Is &Primary Office";
            this.chkIsPrimaryOffice.UseVisualStyleBackColor = true;
            this.chkIsPrimaryOffice.Visible = false;
            this.chkIsPrimaryOffice.CheckedChanged += new System.EventHandler(this.chkIsPrimaryOffice_CheckedChanged);
            // 
            // lblOffice
            // 
            this.lblOffice.AutoSize = true;
            this.lblOffice.Location = new System.Drawing.Point(8, 5);
            this.lblOffice.Name = "lblOffice";
            this.lblOffice.Size = new System.Drawing.Size(38, 13);
            this.lblOffice.TabIndex = 38;
            this.lblOffice.Text = "&Office:";
            // 
            // cboxOffices
            // 
            this.cboxOffices.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxOffices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxOffices.FormattingEnabled = true;
            this.cboxOffices.Location = new System.Drawing.Point(8, 19);
            this.cboxOffices.MaximumSize = new System.Drawing.Size(267, 0);
            this.cboxOffices.MinimumSize = new System.Drawing.Size(267, 0);
            this.cboxOffices.Name = "cboxOffices";
            this.cboxOffices.Size = new System.Drawing.Size(267, 21);
            this.cboxOffices.TabIndex = 37;
            this.cboxOffices.SelectedIndexChanged += new System.EventHandler(this.cboxOffices_SelectedIndexChanged);
            // 
            // grdProperties
            // 
            this.grdProperties.AllowUserToAddRows = false;
            this.grdProperties.AllowUserToDeleteRows = false;
            this.grdProperties.AllowUserToResizeColumns = false;
            this.grdProperties.AllowUserToResizeRows = false;
            this.grdProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProperties.BackgroundColor = System.Drawing.Color.White;
            this.grdProperties.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdProperties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProperties.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.DefaultCellStyle = dataGridViewCellStyle1;
            this.grdProperties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdProperties.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grdProperties.Location = new System.Drawing.Point(8, 46);
            this.grdProperties.MinimumSize = new System.Drawing.Size(385, 0);
            this.grdProperties.MultiSelect = false;
            this.grdProperties.Name = "grdProperties";
            this.grdProperties.RowHeadersVisible = false;
            this.grdProperties.RowTemplate.Height = 20;
            this.grdProperties.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdProperties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdProperties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdProperties.Size = new System.Drawing.Size(385, 384);
            this.grdProperties.TabIndex = 36;
            this.grdProperties.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdProperties_CellValidating);
            this.grdProperties.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProperties_CellEnter);
            // 
            // pageOffices
            // 
            this.pageOffices.Controls.Add(this.lstOffices);
            this.pageOffices.Location = new System.Drawing.Point(4, 22);
            this.pageOffices.Name = "pageOffices";
            this.pageOffices.Padding = new System.Windows.Forms.Padding(3);
            this.pageOffices.Size = new System.Drawing.Size(822, 433);
            this.pageOffices.TabIndex = 1;
            this.pageOffices.Text = "Offices";
            this.pageOffices.UseVisualStyleBackColor = true;
            // 
            // lstOffices
            // 
            this.lstOffices.CheckOnClick = true;
            this.lstOffices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstOffices.FormattingEnabled = true;
            this.lstOffices.Location = new System.Drawing.Point(3, 3);
            this.lstOffices.Name = "lstOffices";
            this.lstOffices.Size = new System.Drawing.Size(816, 424);
            this.lstOffices.TabIndex = 41;
            this.lstOffices.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstOffices_ItemCheck);
            // 
            // pageLicenses
            // 
            this.pageLicenses.Controls.Add(this.tsLicense);
            this.pageLicenses.Controls.Add(this.grdLicenses);
            this.pageLicenses.Location = new System.Drawing.Point(4, 22);
            this.pageLicenses.Name = "pageLicenses";
            this.pageLicenses.Size = new System.Drawing.Size(822, 433);
            this.pageLicenses.TabIndex = 2;
            this.pageLicenses.Text = "Licenses";
            this.pageLicenses.UseVisualStyleBackColor = true;
            // 
            // tsLicense
            // 
            this.tsLicense.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsLicense.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtnDeleteLicense,
            this.toolStripButton1});
            this.tsLicense.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsLicense.Location = new System.Drawing.Point(0, 0);
            this.tsLicense.Name = "tsLicense";
            this.tsLicense.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsLicense.Size = new System.Drawing.Size(822, 25);
            this.tsLicense.TabIndex = 21;
            // 
            // tbtnDeleteLicense
            // 
            this.tbtnDeleteLicense.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteLicense.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeleteLicense.Image")));
            this.tbtnDeleteLicense.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteLicense.Name = "tbtnDeleteLicense";
            this.tbtnDeleteLicense.Size = new System.Drawing.Size(54, 22);
            this.tbtnDeleteLicense.Text = "&Delete...";
            this.tbtnDeleteLicense.Click += new System.EventHandler(this.tbtnDeleteLicense_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(6, 25);
            // 
            // grdLicenses
            // 
            this.grdLicenses.AllowUserToResizeColumns = false;
            this.grdLicenses.AllowUserToResizeRows = false;
            this.grdLicenses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdLicenses.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grdLicenses.BackgroundColor = System.Drawing.Color.White;
            this.grdLicenses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdLicenses.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grdLicenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdLicenses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Description,
            this.Number,
            this.Level0,
            this.Level1,
            this.Level2,
            this.Level3,
            this.Level4,
            this.ID});
            this.grdLicenses.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdLicenses.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdLicenses.Location = new System.Drawing.Point(0, 25);
            this.grdLicenses.MultiSelect = false;
            this.grdLicenses.Name = "grdLicenses";
            this.grdLicenses.RowHeadersVisible = false;
            this.grdLicenses.RowHeadersWidth = 25;
            this.grdLicenses.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdLicenses.Size = new System.Drawing.Size(820, 406);
            this.grdLicenses.TabIndex = 2;
            this.grdLicenses.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.grdLicenses_UserAddedRow);
            this.grdLicenses.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdLicenses_KeyDown);
            this.grdLicenses.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdLicenses_RowValidating);
            this.grdLicenses.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdLicenses_CellValidated);
            this.grdLicenses.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdLicenses_CellValidating);
            this.grdLicenses.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdLicenses_CurrentCellDirtyStateChanged);
            this.grdLicenses.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdLicenses_RowValidated);
            this.grdLicenses.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdLicenses_CellEnter);
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.Description.DefaultCellStyle = dataGridViewCellStyle3;
            this.Description.FillWeight = 10.52631F;
            this.Description.HeaderText = "Description";
            this.Description.MinimumWidth = 200;
            this.Description.Name = "Description";
            this.Description.Width = 200;
            // 
            // Number
            // 
            this.Number.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.Number.DefaultCellStyle = dataGridViewCellStyle4;
            this.Number.FillWeight = 109.4737F;
            this.Number.HeaderText = "Number";
            this.Number.MinimumWidth = 100;
            this.Number.Name = "Number";
            // 
            // Level0
            // 
            this.Level0.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Level0.DefaultCellStyle = dataGridViewCellStyle5;
            this.Level0.HeaderText = "Level 0";
            this.Level0.MinimumWidth = 100;
            this.Level0.Name = "Level0";
            this.Level0.ReadOnly = true;
            // 
            // Level1
            // 
            this.Level1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Level1.HeaderText = "Level 1";
            this.Level1.MinimumWidth = 100;
            this.Level1.Name = "Level1";
            this.Level1.ReadOnly = true;
            // 
            // Level2
            // 
            this.Level2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Level2.HeaderText = "Level 2";
            this.Level2.MinimumWidth = 100;
            this.Level2.Name = "Level2";
            this.Level2.ReadOnly = true;
            // 
            // Level3
            // 
            this.Level3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Level3.HeaderText = "Level 3";
            this.Level3.MinimumWidth = 100;
            this.Level3.Name = "Level3";
            this.Level3.ReadOnly = true;
            // 
            // Level4
            // 
            this.Level4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Level4.HeaderText = "Level 4";
            this.Level4.MinimumWidth = 100;
            this.Level4.Name = "Level4";
            this.Level4.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // PeopleManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.scPeople);
            this.Controls.Add(this.tsMain);
            this.Name = "PeopleManager";
            this.Size = new System.Drawing.Size(624, 492);
            this.Load += new System.EventHandler(this.PeopleManager_Load);
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPeople)).EndInit();
            this.scPeople.Panel1.ResumeLayout(false);
            this.scPeople.Panel2.ResumeLayout(false);
            this.scPeople.ResumeLayout(false);
            this.tabPersonDetail.ResumeLayout(false);
            this.pageDetail.ResumeLayout(false);
            this.pageDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProperties)).EndInit();
            this.pageOffices.ResumeLayout(false);
            this.pageLicenses.ResumeLayout(false);
            this.pageLicenses.PerformLayout();
            this.tsLicense.ResumeLayout(false);
            this.tsLicense.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLicenses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripLabel tlblOffices;
        private System.Windows.Forms.ToolStripSeparator tsep1;
        private System.Windows.Forms.ToolStripLabel tlblFindGroup;
        private System.Windows.Forms.ToolStripTextBox ttxtFindPerson;
        private System.Windows.Forms.ToolStripSeparator tsep2;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ToolStripButton tbtnImport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private LMP.Controls.DataGridView grdPeople;
        private System.Windows.Forms.SplitContainer scPeople;
        private System.Windows.Forms.TabControl tabPersonDetail;
        private System.Windows.Forms.TabPage pageDetail;
        private System.Windows.Forms.Label lblOffice;
        private System.Windows.Forms.ComboBox cboxOffices;
        private System.Windows.Forms.DataGridView grdProperties;
        private System.Windows.Forms.TabPage pageOffices;
        private System.Windows.Forms.CheckedListBox lstOffices;
        private System.Windows.Forms.TabPage pageLicenses;
        private System.Windows.Forms.DataGridView grdLicenses;
        private System.Windows.Forms.ToolStrip tsLicense;
        private System.Windows.Forms.ToolStripButton tbtnDeleteLicense;
        private System.Windows.Forms.ToolStripSeparator toolStripButton1;
        private System.Windows.Forms.CheckBox chkIsPrimaryOffice;
        private System.Windows.Forms.ToolStripButton tbtnNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level0;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;

    }
}
