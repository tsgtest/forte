﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class SelectPrimaryOfficeForm : Form
    {
        public SelectPrimaryOfficeForm()
        {
            InitializeComponent();
        }

        public int PrimaryOffice
        {
            get { return int.Parse(this.lstOffices.SelectedValue.ToString()); }
        }

        private void SelectPrimaryOfficeForm_Load(object sender, EventArgs e)
        {
            try
            {
                BindOfficesList();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //populates the offices checked list box
        private void BindOfficesList()
        {
            //create collection of all office records
            Offices m_oAllOffices = new Offices();

            //retrieve all officeIDs and display names
            Array aOffices = m_oAllOffices.ToArray(0, 4);

            int[] m_aAllOfficeIDs = new int[aOffices.GetLength(0)];

            //add office names and IDs to datatable
            DataTable oDT = new DataTable();
            oDT.Columns.Add("DisplayName");
            oDT.Columns.Add("ID");

            for (int i = 0; i < aOffices.GetLength(0); i++)
            {
                object[] oValues = (object[])aOffices.GetValue(i);
                m_aAllOfficeIDs[i] = (int)oValues[0];

                oDT.Rows.Add(oValues[1].ToString(), oValues[0].ToString());
            }
            oDT.AcceptChanges();

            //clear existing listbox datasource
            ListControl oListCtl = (ListControl)this.lstOffices;
            if (oListCtl.DataSource != null)
                ((DataTable)oListCtl.DataSource).Clear();

            //set listbox datasource, valuemember and displaymember
            oListCtl.DataSource = oDT;
            oListCtl.ValueMember = "ID";
            oListCtl.DisplayMember = "DisplayName";
        }
    }
}
