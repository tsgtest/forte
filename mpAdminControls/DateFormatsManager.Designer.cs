namespace LMP.Administration.Controls
{
    partial class DateFormatsManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DateFormatsManager));
            this.tsDateFormats = new System.Windows.Forms.ToolStrip();
            this.tlblCategory = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNewDateFormats = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnMoveUp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnMoveDown = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDeleteDateFormats = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grdDateFormats = new System.Windows.Forms.DataGridView();
            this.tsDateFormats.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDateFormats)).BeginInit();
            this.SuspendLayout();
            // 
            // tsDateFormats
            // 
            this.tsDateFormats.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsDateFormats.AutoSize = false;
            this.tsDateFormats.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsDateFormats.Dock = System.Windows.Forms.DockStyle.None;
            this.tsDateFormats.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsDateFormats.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlblCategory,
            this.toolStripSeparator3,
            this.tbtnNewDateFormats,
            this.toolStripSeparator4,
            this.tbtnMoveUp,
            this.toolStripSeparator2,
            this.tbtnMoveDown,
            this.toolStripSeparator8,
            this.tbtnDeleteDateFormats,
            this.toolStripSeparator1});
            this.tsDateFormats.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsDateFormats.Location = new System.Drawing.Point(1, 0);
            this.tsDateFormats.Name = "tsDateFormats";
            this.tsDateFormats.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsDateFormats.Size = new System.Drawing.Size(580, 29);
            this.tsDateFormats.TabIndex = 3;
            // 
            // tlblCategory
            // 
            this.tlblCategory.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlblCategory.Name = "tlblCategory";
            this.tlblCategory.Size = new System.Drawing.Size(84, 26);
            this.tlblCategory.Text = "Date Formats";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnNewDateFormats
            // 
            this.tbtnNewDateFormats.AutoSize = false;
            this.tbtnNewDateFormats.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNewDateFormats.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNewDateFormats.Image")));
            this.tbtnNewDateFormats.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNewDateFormats.Name = "tbtnNewDateFormats";
            this.tbtnNewDateFormats.Size = new System.Drawing.Size(75, 22);
            this.tbtnNewDateFormats.Text = "&New...";
            this.tbtnNewDateFormats.Click += new System.EventHandler(this.tbtnNewDateFormats_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnMoveUp
            // 
            this.tbtnMoveUp.AutoSize = false;
            this.tbtnMoveUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("tbtnMoveUp.Image")));
            this.tbtnMoveUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnMoveUp.Name = "tbtnMoveUp";
            this.tbtnMoveUp.Size = new System.Drawing.Size(75, 22);
            this.tbtnMoveUp.Text = "Move &Up";
            this.tbtnMoveUp.Click += new System.EventHandler(this.tbtnMoveUp_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnMoveDown
            // 
            this.tbtnMoveDown.AutoSize = false;
            this.tbtnMoveDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("tbtnMoveDown.Image")));
            this.tbtnMoveDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnMoveDown.Name = "tbtnMoveDown";
            this.tbtnMoveDown.Size = new System.Drawing.Size(75, 22);
            this.tbtnMoveDown.Text = "&Move Down";
            this.tbtnMoveDown.Click += new System.EventHandler(this.tbtnMoveDown_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 29);
            // 
            // tbtnDeleteDateFormats
            // 
            this.tbtnDeleteDateFormats.AutoSize = false;
            this.tbtnDeleteDateFormats.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDeleteDateFormats.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDeleteDateFormats.Image")));
            this.tbtnDeleteDateFormats.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDeleteDateFormats.Name = "tbtnDeleteDateFormats";
            this.tbtnDeleteDateFormats.Size = new System.Drawing.Size(75, 22);
            this.tbtnDeleteDateFormats.Text = "D&elete...";
            this.tbtnDeleteDateFormats.Click += new System.EventHandler(this.tbtnDeleteDateFormat_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            this.toolStripSeparator1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.grdDateFormats);
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 582);
            this.panel1.TabIndex = 27;
            // 
            // grdDateFormats
            // 
            this.grdDateFormats.AllowUserToAddRows = false;
            this.grdDateFormats.AllowUserToDeleteRows = false;
            this.grdDateFormats.AllowUserToResizeRows = false;
            this.grdDateFormats.BackgroundColor = System.Drawing.Color.White;
            this.grdDateFormats.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdDateFormats.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDateFormats.ColumnHeadersVisible = false;
            this.grdDateFormats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDateFormats.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdDateFormats.Location = new System.Drawing.Point(0, 0);
            this.grdDateFormats.MultiSelect = false;
            this.grdDateFormats.Name = "grdDateFormats";
            this.grdDateFormats.RowHeadersVisible = false;
            this.grdDateFormats.RowHeadersWidth = 25;
            this.grdDateFormats.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdDateFormats.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdDateFormats.Size = new System.Drawing.Size(574, 578);
            this.grdDateFormats.TabIndex = 1;
            this.grdDateFormats.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdDateFormats_CellBeginEdit);
            this.grdDateFormats.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdDateFormats_CellValidating);
            this.grdDateFormats.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdDateFormats_DataError);
            // 
            // DateFormatsManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tsDateFormats);
            this.Name = "DateFormatsManager";
            this.Size = new System.Drawing.Size(579, 612);
            this.Load += new System.EventHandler(this.DateFormatsManager_Load);
            this.tsDateFormats.ResumeLayout(false);
            this.tsDateFormats.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDateFormats)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsDateFormats;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton tbtnNewDateFormats;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tbtnDeleteDateFormats;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView grdDateFormats;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbtnMoveUp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tbtnMoveDown;
        private System.Windows.Forms.ToolStripLabel tlblCategory;
    }
}
