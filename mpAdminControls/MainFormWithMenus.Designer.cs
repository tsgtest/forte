namespace mpAdmin
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.listsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripMenuItem();
            this.courtLevelsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.level1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.level2ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.level3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.level4ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.level5ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.countiesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.countriesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.peopleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.officesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupMembershipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.courtsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.couriersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 468);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(658, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(658, 468);
            this.panel1.TabIndex = 5;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(658, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.databaseConnectionToolStripMenuItem,
            this.toolStripMenuItem10,
            this.toolStripMenuItem5,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // databaseConnectionToolStripMenuItem
            // 
            this.databaseConnectionToolStripMenuItem.Name = "databaseConnectionToolStripMenuItem";
            this.databaseConnectionToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.databaseConnectionToolStripMenuItem.Text = "&Connect...";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(209, 22);
            this.toolStripMenuItem10.Text = "Co&nnect To Copy Of...";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(209, 22);
            this.toolStripMenuItem5.Text = "&Synchronize Databases...";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(206, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listsToolStripMenuItem1,
            this.toolStripMenuItem6,
            this.toolStripSeparator1,
            this.courtLevelsToolStripMenuItem1,
            this.toolStripMenuItem4,
            this.countiesToolStripMenuItem1,
            this.statesToolStripMenuItem1,
            this.countriesToolStripMenuItem1,
            this.toolStripMenuItem8,
            this.settingsToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem2.Text = "&Edit";
            // 
            // listsToolStripMenuItem1
            // 
            this.listsToolStripMenuItem1.Name = "listsToolStripMenuItem1";
            this.listsToolStripMenuItem1.Size = new System.Drawing.Size(187, 22);
            this.listsToolStripMenuItem1.Text = "&Lists";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem6.Text = "Address &Formats";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(187, 22);
            this.toolStripSeparator1.Text = "&External Connections";
            // 
            // courtLevelsToolStripMenuItem1
            // 
            this.courtLevelsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.level1ToolStripMenuItem1,
            this.level2ToolStripMenuItem1,
            this.level3ToolStripMenuItem1,
            this.level4ToolStripMenuItem1,
            this.level5ToolStripMenuItem1});
            this.courtLevelsToolStripMenuItem1.Name = "courtLevelsToolStripMenuItem1";
            this.courtLevelsToolStripMenuItem1.Size = new System.Drawing.Size(187, 22);
            this.courtLevelsToolStripMenuItem1.Text = "&Court Levels";
            // 
            // level1ToolStripMenuItem1
            // 
            this.level1ToolStripMenuItem1.Name = "level1ToolStripMenuItem1";
            this.level1ToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.level1ToolStripMenuItem1.Text = "Level &1";
            // 
            // level2ToolStripMenuItem1
            // 
            this.level2ToolStripMenuItem1.Name = "level2ToolStripMenuItem1";
            this.level2ToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.level2ToolStripMenuItem1.Text = "Level &2";
            // 
            // level3ToolStripMenuItem1
            // 
            this.level3ToolStripMenuItem1.Name = "level3ToolStripMenuItem1";
            this.level3ToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.level3ToolStripMenuItem1.Text = "Level &3";
            // 
            // level4ToolStripMenuItem1
            // 
            this.level4ToolStripMenuItem1.Name = "level4ToolStripMenuItem1";
            this.level4ToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.level4ToolStripMenuItem1.Text = "Level &4";
            // 
            // level5ToolStripMenuItem1
            // 
            this.level5ToolStripMenuItem1.Name = "level5ToolStripMenuItem1";
            this.level5ToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.level5ToolStripMenuItem1.Text = "Level &5";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(184, 6);
            // 
            // countiesToolStripMenuItem1
            // 
            this.countiesToolStripMenuItem1.Name = "countiesToolStripMenuItem1";
            this.countiesToolStripMenuItem1.Size = new System.Drawing.Size(187, 22);
            this.countiesToolStripMenuItem1.Text = "C&ounties";
            // 
            // statesToolStripMenuItem1
            // 
            this.statesToolStripMenuItem1.Name = "statesToolStripMenuItem1";
            this.statesToolStripMenuItem1.Size = new System.Drawing.Size(187, 22);
            this.statesToolStripMenuItem1.Text = "&States";
            // 
            // countriesToolStripMenuItem1
            // 
            this.countriesToolStripMenuItem1.Name = "countriesToolStripMenuItem1";
            this.countriesToolStripMenuItem1.Size = new System.Drawing.Size(187, 22);
            this.countriesToolStripMenuItem1.Text = "Coun&tries";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(184, 6);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.settingsToolStripMenuItem.Text = "&Application Settings";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.peopleToolStripMenuItem1,
            this.toolStripMenuItem7,
            this.officesToolStripMenuItem1,
            this.groupsToolStripMenuItem,
            this.groupMembershipToolStripMenuItem,
            this.courtsToolStripMenuItem,
            this.couriersToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(57, 20);
            this.toolStripMenuItem3.Text = "&Manage";
            // 
            // peopleToolStripMenuItem1
            // 
            this.peopleToolStripMenuItem1.Name = "peopleToolStripMenuItem1";
            this.peopleToolStripMenuItem1.Size = new System.Drawing.Size(174, 22);
            this.peopleToolStripMenuItem1.Text = "&People";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(174, 22);
            this.toolStripMenuItem7.Text = "Attorney &Licenses";
            // 
            // officesToolStripMenuItem1
            // 
            this.officesToolStripMenuItem1.Name = "officesToolStripMenuItem1";
            this.officesToolStripMenuItem1.Size = new System.Drawing.Size(174, 22);
            this.officesToolStripMenuItem1.Text = "&Offices";
            // 
            // groupsToolStripMenuItem
            // 
            this.groupsToolStripMenuItem.Name = "groupsToolStripMenuItem";
            this.groupsToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.groupsToolStripMenuItem.Text = "&Groups";
            // 
            // groupMembershipToolStripMenuItem
            // 
            this.groupMembershipToolStripMenuItem.Name = "groupMembershipToolStripMenuItem";
            this.groupMembershipToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.groupMembershipToolStripMenuItem.Text = "Group &Membership";
            // 
            // courtsToolStripMenuItem
            // 
            this.courtsToolStripMenuItem.Name = "courtsToolStripMenuItem";
            this.courtsToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.courtsToolStripMenuItem.Text = "&Courts";
            // 
            // couriersToolStripMenuItem
            // 
            this.couriersToolStripMenuItem.Name = "couriersToolStripMenuItem";
            this.couriersToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.couriersToolStripMenuItem.Text = "Cou&riers";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 490);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MacPac Administration";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem listsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem courtLevelsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem level1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem level2ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem level3ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem level4ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem level5ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem countiesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem statesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem countriesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem peopleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem officesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem groupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupMembershipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem courtsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem couriersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;

    }
}

