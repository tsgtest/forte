using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class DateFormatsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region *******************fields***************************
        private BindingSource m_oDateSource;
        private DataRow m_oLastRow = null;
        private OleDbDataAdapter m_oAdapter;
        private OleDbCommandBuilder m_oCmdBuilder;
        private string m_xValue1 = string.Empty;
        private string m_xValue2 = string.Empty;
        private bool m_bLoadInProgress = false;

        #endregion
        #region *******************enums****************************
        private enum Direction
        {
            Up,
            Down
        }
        #endregion
        #region *******************constants************************
        private const string DATEFORMAT_SETID = "-1";
        private const string DATEFORMAT_DEFAULT = "";
        #endregion
        #region *******************constructors*********************
        public DateFormatsManager()
        {
            InitializeComponent();
        }
        #endregion
        #region *******************event handlers*******************
        /// <summary>
        /// handles initializing data grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DateFormatsManager_Load(object sender, EventArgs e)
        {
            try
            {
                m_bLoadInProgress = true;
                DataBindDateFormats();
                //subscribe to base events
                base.AfterControlLoaded +=
                    new AfterControlLoadedHandler(AdminManager_AfterControlLoaded);
                base.BeforeControlUnloaded +=
                    new BeforeControlUnLoadedHandler(AdminManager_BeforeControlUnloaded);
                m_bLoadInProgress = false;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this handler will fire when user changes rows in grid
        /// the datarow state will then determine whether to update
        /// dataset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oDateSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //get current BindingSource datarow object
                BindingSource oBS = (BindingSource)sender;

                if (m_oLastRow == null)
                    m_oLastRow = ((DataRowView)oBS.Current).Row;

                if (grdDateFormats.CurrentRow == null)
                    return;

                //////if (m_oLastRow.RowState == DataRowState.Detached)
                //////    return;

                if (this.IsValid && m_oLastRow.RowState == DataRowState.Modified)
                    if (!ValidateEntry())
                    {
                        grdDateFormats.CancelEdit();
                        this.IsValid = true;
                        return;
                    }

                //update according to rowstate if necessary
                if (m_oLastRow.RowState == DataRowState.Modified)
                    this.UpdateDateFormats(false);
                
                if (m_oLastRow.RowState == DataRowState.Added)
                    this.UpdateDateFormats(true);

                //reset last row variable
                if (oBS.Current != null)
                    m_oLastRow = ((DataRowView)oBS.Current).Row;
            
            }
            catch (Exception oE)
            {
                if (oE is LMP.Exceptions.UINodeException)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Error_CouldNotSaveChangesToValueSet"),
                       LMP.String.MacPacProductName,
                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    grdDateFormats.CancelEdit();
                    m_oLastRow.RejectChanges();  
                    this.IsValid = false;
                }
                else
                    LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires after control is loaded into container
        /// enables proper refresh/configuration of property grid cells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires if control is about to be unloaded by selecting another
        /// manager - calls SaveCurrentRecord to commit and save any 
        /// pending edits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handler captures current value of row cells before any edits
        /// necessary because we are working with ValueSets, and the 
        /// update query requires setID, & 2 current values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdDateFormats_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //value vars are set to empty after save operation
            //do not reset unless they are empty, or about to be edited
            if (e.ColumnIndex == 0)
            {
                m_xValue1 = grdDateFormats.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                if (m_xValue2 == "")
                    m_xValue2 = grdDateFormats.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();
            }
            else
            {
                m_xValue2 = grdDateFormats.Rows[e.RowIndex].Cells[1].FormattedValue.ToString();
                if (m_xValue1 == "")
                    m_xValue1 = grdDateFormats.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
            }
        }
        /// <summary>
        /// handle record deletion from menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <summary>
        /// we require an entry - no duplicates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <summary>
        private void grdDateFormats_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bLoadInProgress)
                    return;

                if (m_oLastRow == null)
                    return;

                bool bValid = true;
                
                if (e.FormattedValue.ToString() != grdDateFormats.CurrentCell.FormattedValue.ToString())
                    bValid = ValidateEntry();

                if ((m_oLastRow.RowState == DataRowState.Unchanged ||
                    m_oLastRow.RowState == DataRowState.Deleted) &&
                    bValid)
                    return;

                e.Cancel = !bValid;

                if (!bValid)
                    grdDateFormats.CancelEdit();

                this.IsValid = bValid;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdDateFormats_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                if (e.Exception is IndexOutOfRangeException
                    || e.Exception is FormatException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnDeleteDateFormat_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteValueSetItem();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// moves record up by calling method to reassign index
        /// value in the hidden sort order column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnMoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                MoveItem(Direction.Up);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// moves record up by calling method to reassign index
        /// value in the hidden sort order column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnMoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                MoveItem(Direction.Down);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// adds new record to grid/saves to db
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnNewDateFormats_Click(object sender, EventArgs e)
        {
            try
            {
               AddNewDateFormat();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *******************private methods******************
        /// <summary>
        /// displays loads value set of ID -1 into grid
        /// </summary>
        private void DataBindDateFormats()
        {
            try
            {
                //we don't want any validation while the grid is loading
                grdDateFormats.CellValidating -= grdDateFormats_CellValidating;
 
                //get data set for Value Set items
                DataSet oDS = GetValueSet(DATEFORMAT_SETID);

                // Remove the prefix that makes the uppper and lower cases of
                // the same date format unique,
                oDS = RemoveUniqueDateFormatPrefix(oDS);

                //get data table
                DataTable oDT = oDS.Tables[0];

                //initialize binding source
                if (m_oDateSource == null)
                    m_oDateSource = new BindingSource();
                m_oDateSource.DataSource = oDT;

                //the dataset column for sort order returns a string, so we must
                //add a duplicate column of type int for proper sorting with the up/down button
                DataColumn colInt32 = new DataColumn("IntSortOrder");
                colInt32.DataType = System.Type.GetType("System.Int32");
                oDT.Columns.Add(colInt32);

                //copy db sort values to temporary integer-type column
                foreach (DataRow  oRow in oDT.Rows)
                    oRow[3] = Int32.Parse(oRow[1].ToString());

                oDT.AcceptChanges();
                
                //bind dataset to value set grid
                this.grdDateFormats.DataSource = m_oDateSource;

                //display column names 
                this.grdDateFormats.Columns[0].HeaderText = "Date Format";
                this.grdDateFormats.Columns[1].HeaderText = "Sort Order";
                this.grdDateFormats.Columns[3].HeaderText = "Int Sort Order";

                //set column attributes
                this.grdDateFormats.Columns[1].Visible = false;
                this.grdDateFormats.Columns[2].Visible = false;
                this.grdDateFormats.Columns[3].Visible = false;

                this.grdDateFormats.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill; 
                this.grdDateFormats.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                this.grdDateFormats.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                this.grdDateFormats.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;

                //sort according to sort column
                this.grdDateFormats.Sort(this.grdDateFormats.Columns[3], ListSortDirection.Ascending);

                //subscribe to position changed event - this will be the caller
                //for the update routine when navigating through grid
                m_oDateSource.PositionChanged += new EventHandler(m_oDateSource_PositionChanged);
                
                //resubscribe to Validating event
                grdDateFormats.CellValidating +=new DataGridViewCellValidatingEventHandler(grdDateFormats_CellValidating);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotBindDataToControl"), oE);
            }
        }
        /// <summary>
        /// sets up new record with default text and new index value
        /// </summary>
        private void AddNewDateFormat()
        {
            //unsubscribe from validation & position_changed events
            grdDateFormats.CellValidating -= grdDateFormats_CellValidating;
            m_oDateSource.PositionChanged -= m_oDateSource_PositionChanged; 
            
            int iNextSortID = GetNextSortValue();

            //make sure the proposed name does not already exist
            //this can happen if they just create a bunch of records via the new
            //button, move them, delete and then attempt to add a new record
            //string xProposedName = DATEFORMAT_DEFAULT + "_" + iNextSortID.ToString();
            string xProposedName = DATEFORMAT_DEFAULT;
            DataTable oDT = ((DataTable)m_oDateSource.DataSource);
            DataRow[] oDR = oDT.Select("Value1 = '" + xProposedName + "'");

            //if (oDR.GetLength(0) > 0)
            //    xProposedName = xProposedName + "_" + iNextSortID.ToString();

            //update the data source, which will update the grid
            ((DataTable)m_oDateSource.DataSource).Rows.Add(xProposedName, 
                iNextSortID.ToString(), null, iNextSortID);
            
            //reset lastrow variable & save the new record
            m_oLastRow = ((DataRowView)m_oDateSource[grdDateFormats.Rows.Count - 1]).Row;
            //SaveCurrentRecord();
            
            //select the new row
            grdDateFormats.CurrentCell = grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[0];
            grdDateFormats.BeginEdit(true);

            //resubscribe to the event handlers
            grdDateFormats.CellValidating +=new DataGridViewCellValidatingEventHandler(grdDateFormats_CellValidating);
            m_oDateSource.PositionChanged += new EventHandler(m_oDateSource_PositionChanged);
        }
        /// <summary>
        /// this adds a new record or updates an existing one
        /// </summary>
        /// <param name="p"></param>
        private void UpdateDateFormats(bool bAddNew)
        {
            //collect the old and new cell values for the currently edited row
            string xEditedValue1 = m_oLastRow.ItemArray.GetValue(0).ToString();
            xEditedValue1 = GetCaseSensitiveUniqueDateFormat(DATEFORMAT_SETID, xEditedValue1);
            string xEditedValue2 = m_oLastRow.ItemArray.GetValue(1).ToString();

            string xPreviousValue1 = m_xValue1.Replace('"'.ToString(), new string('"', 2));
            xEditedValue1 = xEditedValue1.Replace('"'.ToString(), new string('"', 2));

            try
            {
                OleDbCommand oCmd = new OleDbCommand();

                if (!bAddNew)
                {
                    oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                    oCmd.CommandType = CommandType.Text;
                    //GLOG 3401: Because ValueSet items don't have a numeric key,
                    //we need to update LastEditTime for all items in this set,
                    //while adding a Deletions record so entire Set will get recreated in sync
                    //GLOG 6673:  Make sure Date in SQL string is US Format
                    oCmd.CommandText = "UPDATE ValueSets SET LastEditTime = #" + LMP.Data.Application.GetCurrentEditTime(true) +
                        "# WHERE SetID = " + DATEFORMAT_SETID + ";";
                    oCmd.ExecuteNonQuery();
                    SimpleDataCollection.LogDeletions(mpObjectTypes.ValueSets, DATEFORMAT_SETID);
                    //Create update SQL statement
                    string xSQL = "Update ValueSets SET Value1 = \"" + xEditedValue1 + "\", Value2 = \"" + xEditedValue2 +
                        "\" WHERE SetID = " + DATEFORMAT_SETID + " AND Value1 = \"" + xPreviousValue1 + "\" AND ";

                    string xValue2Condition = "Value2 = \"" + m_xValue2 + "\";";
                    if (System.String.IsNullOrEmpty(m_xValue2))
                        xValue2Condition = "(Value2 is null OR Value2 = '');";
                    xSQL = xSQL + xValue2Condition;
                    oCmd.CommandText = xSQL;

                    //create data adapter from command
                    OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                    DataSet oDS = new DataSet();
                    using (oAdapter)
                    {
                        //create and fill data set using adapter
                        oAdapter.Fill(oDS);
                    }
                    m_xValue2 = string.Empty;
                    m_xValue1 = string.Empty;
                }
                else
                {
                    oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                    //GLOG 4426: Make sure new Date record uses Universal Time for LastEditTime
                    //GLOG 6673:  Make sure Date in SQL string is US Format
                    oCmd.CommandText = "Insert INTO ValueSets (SetID, Value1, Value2, LastEditTime) " +
                        "VALUES (" + DATEFORMAT_SETID  + ", \"" + xEditedValue1 + "\", \"" + xEditedValue2 + "\", " + 
                                "#" + LMP.Data.Application.GetCurrentEditTime(true) + "#);";
                    oCmd.CommandType = CommandType.Text;

                    //create data adapter from command
                    OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                    DataSet oDS = new DataSet();
                    //it's possible that someone is moving around default date formats
                    //with old indices appended; therefore, append the new index to the proposed
                    //Value1 var and call this routine recursively
                    using (oAdapter)
                    {
                        //create and fill data set using adapter
                        oAdapter.Fill(oDS);
                    }
                }
                m_oLastRow.AcceptChanges();
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }

        /// <summary>
        /// returns the dataset with list items value set items
        /// </summary>
        /// <param name="xSetID"></param>
        /// <returns>DataSet</returns>
        private DataSet GetValueSet(string xSetID)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = "Select Value1, Value2, SetID FROM ValueSets WHERE SetID = " + xSetID + ";";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                if (m_oAdapter == null)
                    m_oAdapter = new OleDbDataAdapter(oCmd);
                if (m_oCmdBuilder  == null)
                    m_oCmdBuilder = new OleDbCommandBuilder(m_oAdapter);

                DataSet oDS = new DataSet();
                using (m_oAdapter)
                {
                    //create and fill data set using adapter
                    m_oAdapter.Fill(oDS);
                }
                return oDS;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }
        private void DeleteValueSetItem()
        {
            //do nothing if user in new row and no edits made
            if (grdDateFormats.CurrentRow == null || grdDateFormats.CurrentRow.IsNewRow)
                return;

            if (grdDateFormats.CurrentRow.Selected == false)
                grdDateFormats.CurrentRow.Selected = true;

            //warn user they are about to delete a ValueSet item
            DialogResult oChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Message_DeleteEntry"),
                LMP.String.MacPacProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (oChoice == DialogResult.Yes)
            {
                //get values required for deletion query
                string xSetID = DATEFORMAT_SETID;
                
                //////if (m_xValue1 == "") 
                    m_xValue1 = grdDateFormats.CurrentRow.Cells[0].FormattedValue.ToString();
                    m_xValue2 = grdDateFormats.CurrentRow.Cells[1].FormattedValue.ToString();

                //make sure required values are valid
                if (m_xValue1 != "")
                {
                    //delete Value Set item
                    this.DeleteItem(xSetID, m_xValue1, m_xValue2);
                    //remove row
                    this.grdDateFormats.Rows.RemoveAt(this.grdDateFormats.CurrentRow.Index);
                    ((DataTable)m_oDateSource.DataSource).AcceptChanges(); 
                }
                else
                {
                    //user has started a new entry - simply cancel edit
                    grdDateFormats.CancelEdit();
                }
            }
        }
        /// <summary>
        /// deletes a valueset row into db.
        /// </summary>
        /// <param name="xSetID"></param>
        /// <param name="xValue1"></param>
        /// <param name="xValue2"></param>
        private void DeleteItem(string xSetID, string xValue1, string xValue2)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();
                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandType = CommandType.Text;
                //GLOG 3401: Because ValueSet items don't have a unique ID,
                //we need to update LastEditTime for all items in this set,
                //while adding a Deletions record so entire Set will get recreated in sync
                //GLOG 6673:  Make sure Date in SQL string is US Format
                oCmd.CommandText = "UPDATE ValueSets SET LastEditTime = #" + LMP.Data.Application.GetCurrentEditTime(true) +
                    "# WHERE SetID = " + xSetID + ";";
                oCmd.ExecuteNonQuery();
                SimpleDataCollection.LogDeletions(mpObjectTypes.ValueSets, xSetID);
                oCmd.CommandText = "Delete * FROM ValueSets WHERE  Value1 = \"" + xValue1 + "\" AND Value2 = \"" + xValue2 +
                    "\" AND SetID = " + xSetID + ";";

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotDeleteListEntry"), oE);
            }
        }

        private DataSet RemoveUniqueDateFormatPrefix(DataSet oDS)
        {
            // In order to allow for date formats that are identical but of different
            // cases, these date formats are prefixed to make them unique.
            // For example, these are identical except for the case:
            //  This d% day of MMMM, yyyy
            //  this d% day of MMMM, yyyy
            //  this d% DAY of MMMM, yyyy
            // Access case insensitive comparison sees both of these as 
            // identical and the field in which they are stored is the 
            // primary key field. The entries would then be stored as 
            // follows to make them unique.
            //  This d% day of MMMM, yyyy
            //  �this d% day of MMMM, yyyy
            //  ��this d% DAY of MMMM, yyyy
            //
            // Remove the prefixes so that the format are displayed properly.

            if (oDS != null && oDS.Tables.Count > 0)
            {
                foreach (DataRow oRow in oDS.Tables[0].Rows)
                {
                    while (((string)oRow["Value1"]).StartsWith(LMP.StringArray.mpEndOfSubField.ToString()))
                    {
                        oRow["Value1"] = ((string)oRow["Value1"]).Remove(0, 1);
                    }
                }
            }

            return oDS;
        }

        private string GetCaseSensitiveUniqueDateFormat(string xSetID, string xDateFormat)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                // GLOG : 2987 : JAB
                // Fixes quotation marks being doubled in the returned date format.
                // Quotations need to be doubled in order to be used in the sql.
                string xDateFmtForSql = xDateFormat.Replace('"'.ToString(), new string('"', 2));

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = "SELECT * FROM ValueSets WHERE Value1 =\"" +
                    xDateFmtForSql + "\" AND SetID = " + xSetID + ";";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);
                OleDbCommandBuilder oCmdBuilder = new OleDbCommandBuilder(oAdapter);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }

                DataTable oTable = oDS.Tables[0];

                if (oTable.Rows.Count > 0)
                {
                    // Since the field is unique there should only be 1 row.
                    // The select will do the find using a case insensitve search.
                    // Check if the strings are equivalent. If they are not, this indicate 
                    // the strings have a case sensitive difference (ie, HeLLo and hello).

                    if ((string)oTable.Rows[0][1] == xDateFormat)
                    {
                        // We should not have an identical match. This is a true duplicate
                        // as opposed to a case insensitive duplicate.
                        // Return the original so that the duplication check handles things.
                        return xDateFormat;
                    }
                }
                else
                {
                    // This is uniqe as is. Return the original date format value.
                    return xDateFormat;
                }

                // The select's case insensitve comparison returned a row but the value1 
                // field is not case sensitively identical.
                // Make the date format case insensitively unique by adding a prefix.

                DataRow [] oRows;
                string xUniquePrefix = LMP.StringArray.mpEndOfSubField.ToString();

                //create data adapter from command
                oAdapter = new OleDbDataAdapter(oCmd);
                oCmdBuilder = new OleDbCommandBuilder(oAdapter);

                using (oAdapter)
                {
                    do
                    {
                        xDateFormat = xUniquePrefix + xDateFormat;
                        // GLOG : 2987 : JAB
                        // Fixes quotation marks being doubled in the returned date format.
                        xDateFmtForSql = xUniquePrefix + xDateFmtForSql;
                        oCmd.CommandText = "SELECT * FROM ValueSets WHERE Value1 =\"" +
                            xDateFmtForSql + "\" AND SetID = " + xSetID + ";";
                        oAdapter.Fill(oDS);
                        oTable = oDS.Tables[0];
                        string xFilter = "Value1 = '" + xDateFormat + "'";
                        oRows = oTable.Select(xFilter);

                    } while (oRows.Length != 0);
                }
                
                return (xDateFormat);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }  
        
        /// <summary>
        /// checks for duplicate values in value set 
        /// </summary>
        /// <param name="xSetID"></param>
        /// <returns>bool</returns>
        private bool IsDuplicateValueSet(string xSetID, string xValue1)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                xValue1 = xValue1.Replace('"'.ToString(), new string('"', 2));

                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandText = "SELECT count(*) FROM ValueSets WHERE Value1 =\"" +
                    xValue1 + "\" AND SetID = " + xSetID + ";";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);
                OleDbCommandBuilder oCmdBuilder = new OleDbCommandBuilder(oAdapter);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
                DataTable oTable = oDS.Tables[0];
                DataRow oRow = oTable.Rows[0];

                int oExistingRowCount = (int)oRow[0];

                return (oExistingRowCount > 0);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }
        private bool ValidateEntry()
        {
            string xValue = grdDateFormats.CurrentRow.Cells[0].EditedFormattedValue.ToString();

            if (string.IsNullOrEmpty(xValue))
                return false;

            if (m_oLastRow.RowState == DataRowState.Added || m_oLastRow.RowState == DataRowState.Detached)
                m_xValue1 = xValue;

            //check for an existing value set with the same values
            string xValue1 = xValue;
            string xValue2 = grdDateFormats.CurrentRow.Cells[1].EditedFormattedValue.ToString();

            xValue1 = GetCaseSensitiveUniqueDateFormat(DATEFORMAT_SETID, xValue1);

            if (IsDuplicateValueSet(DATEFORMAT_SETID, xValue1))
            {
                //msg user and cancel navigation
                MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateEntryNotAllowed"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.IsValid = false;
                return false;
            }

            //reset IsValid so navigation to another Manager 
            //can be handled by the Main form
            this.IsValid = xValue1 == "";

            if (xValue1 == "")
            {
                //missing value for Column 1, inform user
                MessageBox.Show(LMP.Resources.GetLangString("Msg_ListEntryValue1Required"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.IsValid = false;
                return false;
            }
            this.IsValid = true;
            return true;
        }
        /// <summary>
        /// moves a date item in the list by updating the sort order (Value2) field
        /// </summary>
        /// <param name="direction"></param>
        private void MoveItem(Direction direction)
        {
            if (grdDateFormats.CurrentRow == null)
                return;
            
            int iTempIndex = 99999999;
            int iCurIndex =  grdDateFormats.CurrentRow.Index; 
            int iNewIndex = 0;
            
            switch (direction)
            {
                case Direction.Up:
                    if (grdDateFormats.CurrentRow.Index == 0)
                        return;

                    //turn off validation while moves are happening
                    grdDateFormats.CellValidating -= grdDateFormats_CellValidating;
                    //turn off the position changed handler - we will call the update
                    //routine from here
                    m_oDateSource.PositionChanged -= m_oDateSource_PositionChanged;

                    iNewIndex = iCurIndex - 1;

                    //column 3 is a temporary column with an int type for sorting - the value to be saved to 
                    //the values sets table will be transferred to string column 2

                    //capture current values necessary for update
                    m_oLastRow = ((DataRowView)m_oDateSource[iNewIndex]).Row;
                    m_xValue1 = grdDateFormats.Rows[iNewIndex].Cells[0].FormattedValue.ToString();
                    m_xValue2 = grdDateFormats.Rows[iNewIndex].Cells[1].FormattedValue.ToString();

                    //move preceding record up to end of grd
                    grdDateFormats.Rows[iNewIndex].Cells[1].Value = iTempIndex;
                    grdDateFormats.Rows[iNewIndex].Cells[3].Value = iTempIndex;

                    //request value set update
                    UpdateDateFormats(false);

                    //capture current values necessary for update
                    m_oLastRow = ((DataRowView)m_oDateSource[iNewIndex]).Row;
                    m_xValue1 = grdDateFormats.Rows[iNewIndex].Cells[0].FormattedValue.ToString();
                    m_xValue2 = grdDateFormats.Rows[iNewIndex].Cells[1].FormattedValue.ToString();

                    //reset the index for record to be moved up
                    grdDateFormats.Rows[iNewIndex].Cells[1].Value = iCurIndex;
                    grdDateFormats.Rows[iNewIndex].Cells[3].Value = iCurIndex;

                    //request value set update
                    UpdateDateFormats(false);

                    //capture current values necessary for update
                    m_oLastRow = ((DataRowView)m_oDateSource[grdDateFormats.Rows.Count - 1]).Row;
                    m_xValue1 = grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[0].FormattedValue.ToString();
                    m_xValue2 = grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[1].FormattedValue.ToString();

                    //reset the index for the record to be moved down
                    grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[1].Value = iCurIndex + 1;
                    grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[3].Value = iCurIndex + 1;

                    //request value set update
                    UpdateDateFormats(false);

                    //reset the current row/selection
                    grdDateFormats.CurrentCell = grdDateFormats.Rows[iNewIndex].Cells[0];

                    break;
                case Direction.Down:
                    
                    if (grdDateFormats.Rows.Count < 2)
                        return;

                    if (grdDateFormats.CurrentRow.Index == ((DataTable)m_oDateSource.DataSource).Rows.Count - 1)
                        return;

                    //turn off validation while moves are happening
                    grdDateFormats.CellValidating -= grdDateFormats_CellValidating;
                    //turn off the position changed handler - we will call the update
                    //routine from here
                    m_oDateSource.PositionChanged -= m_oDateSource_PositionChanged;

                    iNewIndex = iCurIndex + 1;

                    //column 3 is a temporary column with an int type for sorting - the value to be saved to 
                    //the values sets table will be transferred to string column 2

                    //capture current values necessary for update
                    m_oLastRow = ((DataRowView)m_oDateSource[iCurIndex]).Row;
                    m_xValue1 = grdDateFormats.Rows[iCurIndex].Cells[0].FormattedValue.ToString();
                    m_xValue2 = grdDateFormats.Rows[iCurIndex].Cells[1].FormattedValue.ToString();

                    //move preceding record up to end of grd
                    grdDateFormats.Rows[iCurIndex].Cells[1].Value = iTempIndex;
                    grdDateFormats.Rows[iCurIndex].Cells[3].Value = iTempIndex;

                    //request value set update
                    UpdateDateFormats(false);

                    //capture current values necessary for update
                    m_oLastRow = ((DataRowView)m_oDateSource[iCurIndex]).Row;
                    m_xValue1 = grdDateFormats.Rows[iCurIndex].Cells[0].FormattedValue.ToString();
                    m_xValue2 = grdDateFormats.Rows[iCurIndex].Cells[1].FormattedValue.ToString();

                    //reset the index for record to be moved down
                    grdDateFormats.Rows[iCurIndex].Cells[1].Value = iCurIndex + 1;
                    grdDateFormats.Rows[iCurIndex].Cells[3].Value = iCurIndex + 1;

                    //request value set update
                    UpdateDateFormats(false);

                    //capture current values necessary for update
                    m_oLastRow = ((DataRowView)m_oDateSource[grdDateFormats.Rows.Count - 1]).Row;
                    m_xValue1 = grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[0].FormattedValue.ToString();
                    m_xValue2 = grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[1].FormattedValue.ToString();

                    //reset the index for the record to be moved down
                    grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[1].Value = iNewIndex + 1;
                    grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].Cells[3].Value = iNewIndex + 1;

                    //request value set update
                    UpdateDateFormats(false);

                    //reset the current row/selection
                    grdDateFormats.CurrentCell = grdDateFormats.Rows[iNewIndex].Cells[0];

                    break;
                default:
                    break;
            }

            //resubscribe to the validating and position_changed events
            grdDateFormats.CellValidating +=new DataGridViewCellValidatingEventHandler(grdDateFormats_CellValidating);
            m_oDateSource.PositionChanged += new EventHandler(m_oDateSource_PositionChanged);
        }
        /// <summary>
        /// gets the next incremented sort value grid datatable
        /// </summary>
        /// <returns></returns>
        private int GetNextSortValue()
        {
            return grdDateFormats.Rows.Count == 0 ? 1 :
                Int32.Parse(grdDateFormats.Rows[grdDateFormats.Rows.Count - 1].
                    Cells[3].FormattedValue.ToString()) + 1;
        }
        #endregion
        #region *******************properties***********************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region *******************overrides************************
        /// <summary>
        /// handles updates if containing form is closed
        /// </summary>
        public override void SaveCurrentRecord()
        {
            //user has deleted last last record - nothing to save
            //so just exit
            if (m_oLastRow == null)
                return;

            //commit any pending edits
            if (m_oLastRow.RowState == DataRowState.Modified)
                UpdateDateFormats(false);
            if (m_oLastRow.RowState == DataRowState.Added)
                UpdateDateFormats(true);
        }
        #endregion
    }
}
