using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LMP.Data;
using LMP.Controls;

namespace LMP.Administration.Controls
{
    public partial class OfficesManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ************fields*************
        private Offices m_oOffices;
        private Addresses m_oAddresses;
        private DataTable m_oOfficesDT;
        private bool m_bFormLoaded = false;
        private bool m_bChangesMade = false;
        private bool m_bIsNewRow = false;
        private const string DEF_OFFICE_NAME = "New Office";
        private const string DEF_FIRM_NAME = "Your Firm Name";
        #endregion
        #region ************constructors**************
        
        public OfficesManager()
        {
            InitializeComponent();
        }
        #endregion
        #region ************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
               #endregion
        #region ************methods************

        /// <summary>
        /// Display a list of offices in grdOffices
        /// </summary>
        private void DataBindGrdOffices(bool bSortByID)
        {
            m_oOffices = new Offices();

            m_oAddresses = new Addresses();

            //Get a datatable of all offices
            m_oOfficesDT = m_oOffices.ToDataSet().Tables[0];

            //Set the DataSource of grdOffices
            this.grdOffices.DataSource = m_oOfficesDT;

            //Hide unwanted columns in grdOffices
            for (int i = 0; i < grdOffices.Columns.Count; i++)
                if (i != 4)
                    grdOffices.Columns[i].Visible = false;

            if (bSortByID)
                grdOffices.Sort(grdOffices.Columns["ID"], ListSortDirection.Descending);
            
            
            //Set the display properties of the grid
            this.grdOffices.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdOffices.Columns["DisplayName"].HeaderText = "Display Name";


            m_bFormLoaded = true;
        }

        /// <summary>
        /// Display the properties of the office
        /// currently selected in grdOffices
        /// </summary>
        private void DataBindGrdProperties()
        {
            //Test to make sure the user has not just entered 
            //the row for new records in grdOffices
            if (!this.grdOffices.CurrentRow.IsNewRow)
            {
                //Get the current office record
                Office oCurOffice = (Office)m_oOffices.ItemFromID(Int32.Parse(grdOffices.
                    CurrentRow.Cells["ID"].FormattedValue.ToString()));

                //Get the address object corresponding to the 
                //current office
                Address oCurOfficeAddress = (Address)m_oAddresses.
                    ItemFromID(oCurOffice.AddressID);

                //Create a new DataTable to use as the DataSource
                //for grdProperties
                DataTable oPropertiesDT = new DataTable();

                //Add columns to the DataTable
                oPropertiesDT.Columns.Add("Property");
                oPropertiesDT.Columns.Add("Value");

                //An array for adding each new row to the DT
                object[] aRowValues = new object[2];

                //Add rows for each of the properties/values 
                //to the DataTable that is to be bound to 
                //properties

                //ID
                aRowValues[0] = "ID";
                aRowValues[1] = oCurOffice.ID;
                DataRow oDR = oPropertiesDT.Rows.Add(aRowValues);

                //Firm Name
                aRowValues[0] = "Firm Name";
                aRowValues[1] = oCurOffice.FirmName;
                oPropertiesDT.Rows.Add(aRowValues);

                //Abbreviation
                aRowValues[0] = "Abbreviation";
                aRowValues[1] = oCurOffice.Abbr;
                oPropertiesDT.Rows.Add(aRowValues);

                //Slogan
                aRowValues[0] = "Slogan";
                aRowValues[1] = oCurOffice.Slogan;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 1
                aRowValues[0] = "Address Line 1";
                aRowValues[1] = oCurOfficeAddress.Line1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 2
                aRowValues[0] = "Address Line 2";
                aRowValues[1] = oCurOfficeAddress.Line2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 3
                aRowValues[0] = "Address Line 3";
                aRowValues[1] = oCurOfficeAddress.Line3;
                oPropertiesDT.Rows.Add(aRowValues);

                //Pre City
                aRowValues[0] = "Pre City";
                aRowValues[1] = oCurOfficeAddress.PreCity;
                oPropertiesDT.Rows.Add(aRowValues);

                //City
                aRowValues[0] = "City";
                aRowValues[1] = oCurOfficeAddress.City;
                oPropertiesDT.Rows.Add(aRowValues);

                //Post City
                aRowValues[0] = "Post City";
                aRowValues[1] = oCurOfficeAddress.PostCity;
                oPropertiesDT.Rows.Add(aRowValues);

                //State
                aRowValues[0] = "State";
                aRowValues[1] = oCurOfficeAddress.State;
                oPropertiesDT.Rows.Add(aRowValues);

                //State Abbreviation
                aRowValues[0] = "State Abbreviation";
                aRowValues[1] = oCurOfficeAddress.StateAbbr;
                oPropertiesDT.Rows.Add(aRowValues);

                //Zip Code
                aRowValues[0] = "Zip Code";
                aRowValues[1] = oCurOfficeAddress.Zip;
                oPropertiesDT.Rows.Add(aRowValues);

                //County
                aRowValues[0] = "County";
                aRowValues[1] = oCurOfficeAddress.County;
                oPropertiesDT.Rows.Add(aRowValues);

                //Country
                aRowValues[0] = "Country";
                aRowValues[1] = oCurOfficeAddress.Country;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 1
                aRowValues[0] = "Phone Line 1";
                aRowValues[1] = oCurOfficeAddress.Phone1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 2
                aRowValues[0] = "Phone Line 2";
                aRowValues[1] = oCurOfficeAddress.Phone2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 3
                aRowValues[0] = "Phone Line 3";
                aRowValues[1] = oCurOfficeAddress.Phone3;
                oPropertiesDT.Rows.Add(aRowValues);

                //Fax Line 1
                aRowValues[0] = "Fax Line 1";
                aRowValues[1] = oCurOfficeAddress.Fax1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Fax Line 2
                aRowValues[0] = "Fax Line 2";
                aRowValues[1] = oCurOfficeAddress.Fax2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Custom Field 1
                aRowValues[0] = "Custom Field 1";
                aRowValues[1] = oCurOffice.GetProperty("CustomField1");
                oPropertiesDT.Rows.Add(aRowValues);

                //Custom Field 2
                aRowValues[0] = "Custom Field 2";
                aRowValues[1] = oCurOffice.GetProperty("CustomField2");
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Template
                aRowValues[0] = "Address Template";
                aRowValues[1] = oCurOfficeAddress.Template;
                oPropertiesDT.Rows.Add(aRowValues);

                //URL
                aRowValues[0] = "URL";
                aRowValues[1] = oCurOffice.URL;
                oPropertiesDT.Rows.Add(aRowValues);

                //Set grdOfficeProperties' datasource to the newly created DT
                grdOfficeProperties.DataSource = oPropertiesDT;

                //Set the display properties of grdOfficeProperties
                grdOfficeProperties.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdOfficeProperties.Columns[0].FillWeight = 37;
                oPropertiesDT.Columns[0].ReadOnly = true;
                grdOfficeProperties.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                grdOfficeProperties.Rows[0].ReadOnly = true;
            }
            else
            {
                //User has entered the row for new records
                //in grdOffices
                //Clear data displayed in grdOfficeProperties
                DataTable oDT = (DataTable)this.grdOfficeProperties.DataSource;
                oDT.Clear();
                this.grdOfficeProperties.DataSource = oDT;
            }
        }
        private void DataBindGrdPropertiesOLD()
        {
            //Test to make sure the user has not just entered 
            //the row for new records in grdOffices
            if (!this.grdOffices.CurrentRow.IsNewRow)
            {
                //Get the current office record
                Office oCurOffice = (Office)m_oOffices.ItemFromID(Int32.Parse(grdOffices.
                    CurrentRow.Cells["ID"].FormattedValue.ToString()));

                //Get the address object corresponding to the 
                //current office
                Address oCurOfficeAddress = (Address)m_oAddresses.
                    ItemFromID(oCurOffice.AddressID);

                //Create a new DataTable to use as the DataSource
                //for grdProperties
                DataTable oPropertiesDT = new DataTable();

                //Add columns to the DataTable
                oPropertiesDT.Columns.Add("Property");
                oPropertiesDT.Columns.Add("Value");

                //An array for adding each new row to the DT
                object[] aRowValues = new object[2];

                //Add rows for each of the properties/values 
                //to the DataTable that is to be bound to 
                //properties

                //Firm Name
                aRowValues[0] = "Firm Name";
                aRowValues[1] = oCurOffice.FirmName;
                oPropertiesDT.Rows.Add(aRowValues);

                //Abbreviation
                aRowValues[0] = "Abbreviation";
                aRowValues[1] = oCurOffice.Abbr;
                oPropertiesDT.Rows.Add(aRowValues);

                //Slogan
                aRowValues[0] = "Slogan";
                aRowValues[1] = oCurOffice.Slogan;
                oPropertiesDT.Rows.Add(aRowValues);

                //Pre City
                aRowValues[0] = "Pre City";
                aRowValues[1] = oCurOfficeAddress.PreCity;
                oPropertiesDT.Rows.Add(aRowValues);

                //City
                aRowValues[0] = "City";
                aRowValues[1] = oCurOfficeAddress.City;
                oPropertiesDT.Rows.Add(aRowValues);

                //Post City
                aRowValues[0] = "Post City";
                aRowValues[1] = oCurOfficeAddress.PostCity;
                oPropertiesDT.Rows.Add(aRowValues);

                //County
                aRowValues[0] = "County";
                aRowValues[1] = oCurOfficeAddress.County;
                oPropertiesDT.Rows.Add(aRowValues);

                //State
                aRowValues[0] = "State";
                aRowValues[1] = oCurOfficeAddress.State;
                oPropertiesDT.Rows.Add(aRowValues);

                //State Abbreviation
                aRowValues[0] = "State Abbreviation";
                aRowValues[1] = oCurOfficeAddress.StateAbbr;
                oPropertiesDT.Rows.Add(aRowValues);

                //Country
                aRowValues[0] = "Country";
                aRowValues[1] = oCurOfficeAddress.Country;
                oPropertiesDT.Rows.Add(aRowValues);

                //Zip Code
                aRowValues[0] = "Zip Code";
                aRowValues[1] = oCurOfficeAddress.Zip;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 1
                aRowValues[0] = "Address Line 1";
                aRowValues[1] = oCurOfficeAddress.Line1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 2
                aRowValues[0] = "Address Line 2";
                aRowValues[1] = oCurOfficeAddress.Line2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Line 3
                aRowValues[0] = "Address Line 3";
                aRowValues[1] = oCurOfficeAddress.Line3;
                oPropertiesDT.Rows.Add(aRowValues);

                //Address Template
                aRowValues[0] = "Address Template";
                aRowValues[1] = oCurOfficeAddress.Template;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 1
                aRowValues[0] = "Phone Line 1";
                aRowValues[1] = oCurOfficeAddress.Phone1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 2
                aRowValues[0] = "Phone Line 2";
                aRowValues[1] = oCurOfficeAddress.Phone2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Phone Line 3
                aRowValues[0] = "Phone Line 3";
                aRowValues[1] = oCurOfficeAddress.Phone3;
                oPropertiesDT.Rows.Add(aRowValues);

                //Fax Line 1
                aRowValues[0] = "Fax Line 1";
                aRowValues[1] = oCurOfficeAddress.Fax1;
                oPropertiesDT.Rows.Add(aRowValues);

                //Fax Line 2
                aRowValues[0] = "Fax Line 2";
                aRowValues[1] = oCurOfficeAddress.Fax2;
                oPropertiesDT.Rows.Add(aRowValues);

                //Custom Field 1
                aRowValues[0] = "Custom Field 1";
                aRowValues[1] = oCurOffice.GetProperty("CustomField1");
                oPropertiesDT.Rows.Add(aRowValues);

                //Custom Field 2
                aRowValues[0] = "Custom Field 2";
                aRowValues[1] = oCurOffice.GetProperty("CustomField2");
                oPropertiesDT.Rows.Add(aRowValues);

                //Set grdOfficeProperties' datasource to the newly created DT
                grdOfficeProperties.DataSource = oPropertiesDT;

                //Set the display properties of grdOfficeProperties
                grdOfficeProperties.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdOfficeProperties.Columns[0].FillWeight = 37;
                oPropertiesDT.Columns[0].ReadOnly = true;
                grdOfficeProperties.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            }
            else
            {
                //User has entered the row for new records
                //in grdOffices
                //Clear data displayed in grdOfficeProperties
                DataTable oDT = (DataTable)this.grdOfficeProperties.DataSource;
                oDT.Clear();
                this.grdOfficeProperties.DataSource = oDT;
            }
        }

        /// <summary>
        /// Update the properties of the Office record
        /// that the user has just finished editing
        /// </summary>
        private void UpdateOfficeOLD()
        {
            //exit if no current row
            if (grdOffices.CurrentRow == null)
                return;

            //Get the current office record
            Office oCurOffice = (Office)m_oOffices.ItemFromID
                (Int32.Parse(this.grdOffices.CurrentRow.Cells[0].FormattedValue.ToString()));

            //Get the Address record corresponding to the current 
            //Office record
            Address oCurOfficeAddress = (Address)m_oAddresses.
                ItemFromID(oCurOffice.AddressID);

            DataGridViewRowCollection oRows = this.grdOfficeProperties.Rows;

            string xName = grdOffices.CurrentRow.Cells["DisplayName"].EditedFormattedValue.ToString();


            //update Name if the new display name value is not null
            oCurOffice.Name = xName.Replace(" ", "");

            //Update DisplayName if the new value is not null
            oCurOffice.DisplayName = xName;

            //Update FirmName if the new value is not null
            if (oRows[0].Cells[1].FormattedValue.ToString() != "")
                oCurOffice.FirmName = oRows[0].Cells[1].FormattedValue.ToString();

            //Update Address Template if the new value is not null
            if (oRows[14].Cells[1].FormattedValue.ToString() != "")
                oCurOfficeAddress.Template = oRows[14].Cells[1].FormattedValue.ToString();

            //Update Abbreviation
            oCurOffice.Abbr = oRows[1].Cells[1].FormattedValue.ToString();

            //Update Slogan
            oCurOffice.Slogan = oRows[2].Cells[1].FormattedValue.ToString();

            //Update PreCity
            oCurOfficeAddress.PreCity = oRows[3].Cells[1].FormattedValue.ToString();

            //Update City
            oCurOfficeAddress.City = oRows[4].Cells[1].FormattedValue.ToString();

            //Update PostCity
            oCurOfficeAddress.PostCity = oRows[5].Cells[1].FormattedValue.ToString();

            //Update County
            oCurOfficeAddress.County = oRows[6].Cells[1].FormattedValue.ToString();

            //Update State
            oCurOfficeAddress.State = oRows[7].Cells[1].FormattedValue.ToString();

            //Update StateAbbr
            oCurOfficeAddress.StateAbbr = oRows[8].Cells[1].FormattedValue.ToString();

            //Update Country
            oCurOfficeAddress.Country = oRows[9].Cells[1].FormattedValue.ToString();

            //Update Zip
            oCurOfficeAddress.Zip = oRows[10].Cells[1].FormattedValue.ToString();

            //Update Line1
            oCurOfficeAddress.Line1 = oRows[11].Cells[1].FormattedValue.ToString();

            //Update Line2
            oCurOfficeAddress.Line2 = oRows[12].Cells[1].FormattedValue.ToString();

            //Update Line3
            oCurOfficeAddress.Line3 = oRows[13].Cells[1].FormattedValue.ToString();

            //Update Phone1
            oCurOfficeAddress.Phone1 = oRows[15].Cells[1].FormattedValue.ToString();

            //Update Phone2
            oCurOfficeAddress.Phone2 = oRows[16].Cells[1].FormattedValue.ToString();

            //Update Phone3
            oCurOfficeAddress.Phone3 = oRows[17].Cells[1].FormattedValue.ToString();

            //Update Fax1
            oCurOfficeAddress.Fax1 = oRows[18].Cells[1].FormattedValue.ToString();

            //Update Fax2
            oCurOfficeAddress.Fax2 = oRows[19].Cells[1].FormattedValue.ToString();

            //Update CustomField1
            oCurOffice.SetCustomProperty("CustomField1",
                oRows[20].Cells[1].FormattedValue.ToString());

            //Update CustomField2
            oCurOffice.SetCustomProperty("CustomField2",
                oRows[21].Cells[1].FormattedValue.ToString());

            try
            {
                //Save the changes to the DB
                m_oOffices.Save(oCurOffice);
                m_oAddresses.Save(oCurOfficeAddress);

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// Update the properties of the Office record
        /// that the user has just finished editing
        /// </summary>
        private void UpdateOffice()
        {
            //TODO:  we might want to load/update from a dictionary list or hash table
            //so the field order isn't hardcoded as here and in databindgrdoffices

            //exit if no current row
            if (grdOffices.CurrentRow == null)
                return;

            //Get the current office record
            Office oCurOffice = (Office)m_oOffices.ItemFromID
                (Int32.Parse(this.grdOffices.CurrentRow.Cells[0].FormattedValue.ToString()));

            //Get the Address record corresponding to the current 
            //Office record
            Address oCurOfficeAddress = (Address)m_oAddresses.
                ItemFromID(oCurOffice.AddressID);

            DataGridViewRowCollection oRows = this.grdOfficeProperties.Rows;

            string xName = grdOffices.CurrentRow.Cells["DisplayName"].EditedFormattedValue.ToString();


            //update Name if the new display name value is not null
            oCurOffice.Name = xName.Replace(" ", "");

            //Update DisplayName if the new value is not null
            oCurOffice.DisplayName = xName;

            //Update FirmName if the new value is not null
            if (oRows[1].Cells[1].FormattedValue.ToString() != "")
                oCurOffice.FirmName = oRows[1].Cells[1].FormattedValue.ToString();

            //Update Address Template if the new value is not null
            if (oRows[21].Cells[1].FormattedValue.ToString() != "")
                oCurOfficeAddress.Template = oRows[22].Cells[1].FormattedValue.ToString();

            //Update Abbreviation
            oCurOffice.Abbr = oRows[2].Cells[1].FormattedValue.ToString();

            //Update Slogan
            oCurOffice.Slogan = oRows[3].Cells[1].FormattedValue.ToString();

            //Update Line1
            oCurOfficeAddress.Line1 = oRows[4].Cells[1].FormattedValue.ToString();

            //Update Line2
            oCurOfficeAddress.Line2 = oRows[5].Cells[1].FormattedValue.ToString();

            //Update Line3
            oCurOfficeAddress.Line3 = oRows[6].Cells[1].FormattedValue.ToString();

            //Update PreCity
            oCurOfficeAddress.PreCity = oRows[7].Cells[1].FormattedValue.ToString();

            //Update City
            oCurOfficeAddress.City = oRows[8].Cells[1].FormattedValue.ToString();

            //Update PostCity
            oCurOfficeAddress.PostCity = oRows[9].Cells[1].FormattedValue.ToString();

            //Update State
            oCurOfficeAddress.State = oRows[10].Cells[1].FormattedValue.ToString();
            
            //Update StateAbbr
            oCurOfficeAddress.StateAbbr = oRows[11].Cells[1].FormattedValue.ToString();

            //Update Zip
            oCurOfficeAddress.Zip = oRows[12].Cells[1].FormattedValue.ToString();

            //Update County
            oCurOfficeAddress.County = oRows[13].Cells[1].FormattedValue.ToString();

            //Update Country
            oCurOfficeAddress.Country = oRows[14].Cells[1].FormattedValue.ToString();

            //Update Phone1
            oCurOfficeAddress.Phone1 = oRows[15].Cells[1].FormattedValue.ToString();

            //Update Phone2
            oCurOfficeAddress.Phone2 = oRows[16].Cells[1].FormattedValue.ToString();

            //Update Phone3
            oCurOfficeAddress.Phone3 = oRows[17].Cells[1].FormattedValue.ToString();

            //Update Fax1
            oCurOfficeAddress.Fax1 = oRows[18].Cells[1].FormattedValue.ToString();

            //Update Fax2
            oCurOfficeAddress.Fax2 = oRows[19].Cells[1].FormattedValue.ToString();

            // GLOG : 3346 : JAB
            // Update Address template field.
            oCurOfficeAddress.Template = oRows[22].Cells[1].FormattedValue.ToString();

            //Update CustomField1
            oCurOffice.SetCustomProperty("CustomField1",
                oRows[20].Cells[1].FormattedValue.ToString().Replace("'","''"));

            //Update CustomField2
            oCurOffice.SetCustomProperty("CustomField2",
                oRows[21].Cells[1].FormattedValue.ToString().Replace("'", "''"));

            //Update URL
            oCurOffice.URL = oRows[23].Cells[1].FormattedValue.ToString();

            try
            {
                //Save the changes to the DB
                m_oOffices.Save(oCurOffice);
                m_oAddresses.Save(oCurOfficeAddress);

            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }

        /// <summary>
        /// Delete the currently selected office record
        /// from the DB and grdOffices
        /// </summary>
        private void DeleteOfficeRecord()
        {
            //exit if there's no current record
            if (grdOffices.CurrentRow == null || grdOffices.Rows.Count == 0 ||
                grdOffices.CurrentRow.IsNewRow)
                return;
            
            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                //Get the currently selected Office record
                Office oCurOffice = (Office)m_oOffices.ItemFromID(Int32.Parse(
                    this.grdOffices.CurrentRow.Cells[0].FormattedValue.ToString()));

                //GLOG - 3666 - ceh
                //message the user to reassign all users linked
                //to the office about to be deleted
                MessageBox.Show(LMP.Resources.GetLangString("Message_ReAssignUsersLinkedToOffice")
                                                            + oCurOffice.DisplayName + ".",
                                                            LMP.ComponentProperties.ProductName,
                                                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                try
                {
                    //Delete the currently selected office record
                    m_oOffices.Delete(oCurOffice.ID);

                    // Glog: 2433
                    // Attempt to delete the address of this courier. The address may not be
                    // deletable if it is still referenced by other entities.
                    try
                    {
                        this.m_oAddresses.Delete(oCurOffice.AddressID);
                    }
                    catch (LMP.Exceptions.StoredProcedureException)
                    {
                        // Allow the attempt to delete the address to fail
                        // as it may still be referenced by other entities.
                    }

                }
                catch
                {
                    //This try/catch block is here because of an error
                    //generated as a reuslt of a missing sproc: spObjectAssignmentsDeleteByObjectIDs
                }

                //Remove the row representing the deleted
                //office record from grdOffices
                this.grdOffices.Rows.Remove(this.grdOffices.CurrentRow);

                if (grdOffices.Rows.Count > 0)
                {
                    this.grdOffices.CurrentCell.Selected = true;
                    DataBindGrdProperties();
                }
                else
                {
                    //clear property grid entirely
                    grdOfficeProperties.DataSource = null;
                    grdOfficeProperties.Rows.Clear();
                }
            }
        }

        /// <summary>
        /// Create a new office record and add it to the DB
        /// </summary>
        private bool AddOfficeRecord()
        {
            //Create a new office record.
            Office oNewOffice = (Office)m_oOffices.Create();
            
            //Create a new address record for the new office
            Address oNewAddress = (Address)m_oAddresses.Create();

            int iNewID = m_oOffices.GetLastID() - 1;
            string xNewOfficeName = GetNewOfficeName(iNewID);

            //Set the name, firm name, and display name properties of the new
            //office record to the name the user has entered.
            oNewOffice.Name = xNewOfficeName.Replace(" ", "");
            oNewOffice.DisplayName = xNewOfficeName;
            oNewOffice.FirmName = DEF_FIRM_NAME;

            //set usage state
            oNewOffice.UsageState = 1; 

            //Set the default properties of the new address record
            oNewAddress.Template = "[Line1]|[Line2]|[Line3]|[City], [State]  [Zip]|[CountryIfForeign]";

            //Save the new Address record
            m_oAddresses.Save(oNewAddress);

            //Set the new address record to the new office record
            oNewOffice.AddressID = oNewAddress.ID;

            //Save the new Office record
            m_oOffices.Save(oNewOffice);

            //rebind the offices grid
            this.DataBindGrdOffices(false);
            //GLOG 6654: Select newly-added Office
            FindMatchingRecord(oNewOffice.DisplayName);
            //select the new row, initiate edit mode
            //grdOffices.CurrentCell = grdOffices.Rows[grdOffices.Rows.Count - 1].Cells["DisplayName"];
            //grdOffices.BeginEdit(true);

            return true;
        }

        /// <summary>
        /// Select the first instance of the search
        /// string entered by the user
        /// </summary>
        /// <param name="xSearchString"></param>
        private void FindMatchingRecord(string xSearchString)
        {
            if (xSearchString != "")
            {
                //User has entered a search string

                //Get the index of the first matching record
                int iMatchIndex = this.grdOffices.FindRecord
                    (xSearchString, this.grdOffices.Columns[4].Name, false);

                if (iMatchIndex > -1)
                    //Match found
                    this.grdOffices.CurrentCell = this.grdOffices.Rows[iMatchIndex].Cells[4];
            }
        }
        /// <summary>
        /// tests for empty or duplicate office name string
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        private bool ValidateOfficeName(string xName, bool bSuppressMessage)
        {
            string xMessage = "";
            bool bReturn = true;

            //check grdOffices DataSource for dupes
            DataTable oDT = (DataTable)grdOffices.DataSource;

            DataRow[] oRows = oDT.Select(this.grdOffices.Columns[4].Name + "='" + xName.Replace("'","''") + "'");
            int iExisting = oRows.Length;

            if (iExisting != 0 && m_bChangesMade)
            {
                bReturn = false;
                xMessage = "Error_DuplicateNameValueNotAllowed";
            }

            if (iExisting != 0 && m_bIsNewRow)
            {
                bReturn = false;
                xMessage = "Error_DuplicateNameValueNotAllowed";
            }

            if (xName == "" && m_bIsNewRow)
            {
                return bReturn;
            }
            
            if (xName == "")
            {
                xMessage = "Error_EmptyValueNotAllowed";
                bReturn = false;
            }

            if (!bReturn && !bSuppressMessage)
            {
                //alert user
                MessageBox.Show(LMP.Resources.GetLangString(xMessage),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            return bReturn;
        }
        /// <summary>
        /// generates new courier name based on constant plus ID
        /// </summary>
        /// <returns></returns>
        private string GetNewOfficeName(int iNewID)
        {
            //GLOG 6654: Prompt for name of new Office
            string xName = DEF_OFFICE_NAME;
            bool bValid = true;
            do
            {
                DialogResult oRet = Dialog.InputBox("New Office", "Enter new Office Name:", ref xName);
                if (oRet == DialogResult.OK)
                {
                    DataTable oDT = (DataTable)grdOffices.DataSource;
                    DataRow[] oDR = oDT.Select("DisplayName = '" + DEF_OFFICE_NAME + "'");

                    if (oDR.GetLength(0) == 0)
                    {
                        bValid = true;
                        return xName;
                    }
                    else
                    {
                        bValid = false;
                        MessageBox.Show(string.Format(LMP.Resources.GetLangString("Prompt_ItemNameAlreadyInUse"), "Office"),
                            LMP.String.MacPacProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                    return "";
            } while (!bValid);
            return "";
            //if (grdOffices.Rows.Count == 0)
            //    return DEF_OFFICE_NAME;

            ////search for existing default name
            //DataTable oDT = (DataTable)grdOffices.DataSource;
            //DataRow[] oDR = oDT.Select("DisplayName = '" + DEF_OFFICE_NAME + "'");

            //if (oDR.GetLength(0) == 0)
            //    return DEF_OFFICE_NAME;
            //else
            //{
            //    int iPos = iNewID * -1;
            //    return DEF_OFFICE_NAME + "_" + iPos.ToString();
            //}
        }
        #endregion 
        #region ************event handlers*************
        private void OfficesManager_Load(object sender, EventArgs e)
        {
            try
            {
                this.DataBindGrdOffices(false);

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteOfficeRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles adding new records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                grdOffices.EndEdit();
                UpdateOffice();
                AddOfficeRecord();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }
        private void grdOffices_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.DeleteOfficeRecord();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdOffices_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.DataBindGrdProperties();
                m_bIsNewRow = grdOffices.Rows[e.RowIndex].IsNewRow;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdOfficeProperties_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bFormLoaded)
                {
                    //Validate cell only if form is already loaded
                    DataGridViewRow oCurRow = grdOfficeProperties.Rows[e.RowIndex];
                    
                    //test if change made
                    if (oCurRow.Cells[1].FormattedValue.ToString()
                        != oCurRow.Cells[1].EditedFormattedValue.ToString())
                        m_bChangesMade = true;
                    
                    if (e.RowIndex == 0 && m_bChangesMade)    
                    {
                        //prevent entry of duplicate office name
                        string xOfficeName = grdOfficeProperties.CurrentCell.EditedFormattedValue.ToString();
                        
                        //we'll allow a name to be reset to its original value
                        if (xOfficeName == grdOffices.CurrentCell.FormattedValue.ToString())
                            return;

                        if (!this.ValidateOfficeName(xOfficeName, false)) 
                        {
                            grdOfficeProperties.CancelEdit();
                            e.Cancel = true;
                            this.IsValid = false;
                            m_bChangesMade = false;
                        }
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// process update code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdOfficeProperties_CellValidated(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bChangesMade)
                {
                    this.UpdateOffice();
                    m_bChangesMade = false;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdOffices_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bFormLoaded)
                {
                    //Get the current row in grdOffices
                    string xOldOfficeName = this.grdOffices.Rows[e.RowIndex].Cells[4]
                                            .FormattedValue.ToString();
                    string xOfficeName = e.FormattedValue.ToString();

                    if (xOldOfficeName == xOfficeName)
                    {
                        m_bChangesMade = false;
                        return;
                    }
                    
                    if (!ValidateOfficeName(xOfficeName, false))
                    {
                        e.Cancel = true;
                        this.IsValid = false;
                        return;
                    }
                    m_bChangesMade = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdOffices_Leave(object sender, System.EventArgs e)
        {
            try
            {
                this.IsValid = ValidateOfficeName(grdOffices.CurrentRow.
                    Cells["DisplayName"].EditedFormattedValue.ToString(), true);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdOffices_CellValidated(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bChangesMade && m_bFormLoaded)
                {
                    UpdateOffice();
                    m_bChangesMade = false;
                }

                this.IsValid = true;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void ttxtFindOffice_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    //don't attempt to set current cell
                    //if grids are empty, i.e. there are no records
                    if (grdOffices.Rows.Count == 0)
                        return;

                    //Give focus to grdProperties
                    this.grdOfficeProperties.Focus();

                    //Set the current cell in grdProperties
                    this.grdOfficeProperties.CurrentCell = 
                        this.grdOfficeProperties.Rows[0].Cells[1];
                }
                else
                    //Select the first record matching
                    //the search string eneterd by the user
                    this.FindMatchingRecord(ttxtFindOffice.Text);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdOfficeProperties_CellEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (grdOfficeProperties.Focused)
                {
                    if (grdOfficeProperties.CurrentCellAddress.X == 0 &&
                       grdOfficeProperties.CurrentCellAddress.Y == 0)
                        return;

                    //programmatically resetting current cell errors
                    //we can use sendkeys instead
                    if (e.ColumnIndex == 0)
                        System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void grdOffices_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }
        }
        //GLOG : 8241 : JSW
        //tabbing moves from row to row for exising records
        private void grdOffices_TabPressed(object sender, TabPressedEventArgs e)
        {
            int iRowIndex;
            try
            {
                iRowIndex = this.grdOffices.CurrentRow.Index;
                if ((grdOffices.Rows.Count - 1) > iRowIndex)
                    this.grdOffices.CurrentCell = this.grdOffices.Rows[iRowIndex + 1].Cells[4];
                else
                    this.grdOffices.CurrentCell = this.grdOffices.Rows[0].Cells[4];
            }
            catch { }
        }
    }
}

