using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.Administration.Controls
{
    public partial class SetLicenseJurisdictionDialog : Form
    {
        #region ***************constructors*****************

        public SetLicenseJurisdictionDialog()
        {
            InitializeComponent();
        }

        #endregion
        #region ***************properties*******************

        /// <summary>
        /// The ID of the level 0 Jurisdiction the user has selected
        /// </summary>
        /// <returns></returns>
        public int Level0JurisdictionID
        {
            get
            {
                int iLevel0ID = 0;
                int iLevel1ID = 0;
                int iLevel2ID = 0;
                int iLevel3ID = 0;
                int iLevel4ID = 0;

                //Get the jurisdiction from the treeJurisdictions control
                this.treeJurisdictions.GetJurisdictionIDs(ref iLevel0ID, 
                    ref iLevel1ID, ref iLevel2ID, ref iLevel3ID, ref iLevel4ID);

                return iLevel0ID;
            }
        }

        /// <summary>
        /// The ID of the level 1 Jurisdiction the user has selected
        /// </summary>
        /// <returns></returns>
        public int Level1JurisdictionID
        {
            get
            {
                int iLevel0ID = 0;
                int iLevel1ID = 0;
                int iLevel2ID = 0;
                int iLevel3ID = 0;
                int iLevel4ID = 0;

                //Get the jurisdiction from the treeJurisdictions control
                this.treeJurisdictions.GetJurisdictionIDs(ref iLevel0ID, 
                    ref iLevel1ID, ref iLevel2ID, ref iLevel3ID, ref iLevel4ID);

                return iLevel1ID;
            }
        }

        /// <summary>
        /// The ID of the level 2 Jurisdiction the user has selected
        /// </summary>
        /// <returns></returns>
        public int Level2JurisdictionID
        {
            get
            {
                int iLevel0ID = 0;
                int iLevel1ID = 0;
                int iLevel2ID = 0;
                int iLevel3ID = 0;
                int iLevel4ID = 0;

                //Get the jurisdiction from the treeJurisdictions control
                this.treeJurisdictions.GetJurisdictionIDs(ref iLevel0ID, 
                    ref iLevel1ID, ref iLevel2ID, ref iLevel3ID, ref iLevel4ID);

                return iLevel2ID;
            }
        }

        /// <summary>
        /// The ID of the level 3 Jurisdiction the user has selected
        /// </summary>
        /// <returns></returns>
        public int Level3JurisdictionID
        {
            get
            {
                int iLevel0ID = 0;
                int iLevel1ID = 0;
                int iLevel2ID = 0;
                int iLevel3ID = 0;
                int iLevel4ID = 0;

                //Get the jurisdiction from the treeJurisdictions control
                this.treeJurisdictions.GetJurisdictionIDs(ref iLevel0ID, 
                    ref iLevel1ID, ref iLevel2ID, ref iLevel3ID, ref iLevel4ID);

                return iLevel3ID;
            }
        }

        /// <summary>
        /// The ID of the level 4 Jurisdiction the user has selected
        /// </summary>
        /// <returns></returns>
        public int Level4JurisdictionID
        {
            get
            {
                int iLevel0ID = 0;
                int iLevel1ID = 0;
                int iLevel2ID = 0;
                int iLevel3ID = 0;
                int iLevel4ID = 0;

                //Get the jurisdiction from the treeJurisdictions control
                this.treeJurisdictions.GetJurisdictionIDs(ref iLevel0ID, 
                    ref iLevel1ID, ref iLevel2ID, ref iLevel3ID, ref iLevel4ID);

                return iLevel4ID;
            }
        }

        /// <summary>
        /// returns integer array of selected
        /// levels
        /// </summary>
        public int[] Jurisdictions
        {
            get
            {
                int iLevel0ID = 0;
                int iLevel1ID = 0;
                int iLevel2ID = 0;
                int iLevel3ID = 0;
                int iLevel4ID = 0;

                //Get the jurisdiction from the treeJurisdictions control
                this.treeJurisdictions.GetJurisdictionIDs(ref iLevel0ID,
                    ref iLevel1ID, ref iLevel2ID, ref iLevel3ID, ref iLevel4ID);
                
                int[] iRet = {  iLevel0ID,
                                iLevel1ID,
                                iLevel2ID,
                                iLevel3ID,
                                iLevel4ID};

                return iRet;
            }
        }
        
        
        /// <summary>
        /// The selected jurisdiction in the Jurisdiction tree in the form
        /// (level + "." + ID)
        /// </summary>
        public string Value
        {
            set
            {
                //Set the selected node in treeJurisdictions
                this.treeJurisdictions.Value = value;
            }
        }

        #endregion  
        #region ***************methods**********************

        #endregion
        #region ***************event handlers***************

        #endregion
    }
}