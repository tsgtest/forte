namespace LMP.Administration.Controls
{
    partial class JurisdictionsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JurisdictionsManager));
            this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.scJurisdictions = new System.Windows.Forms.SplitContainer();
            this.treeJurisdictions = new LMP.Controls.JurisdictionsTree();
            this.lblJurisdiction = new System.Windows.Forms.Label();
            this.tsJurisdiction = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnJurisdictionDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.grdItems = new System.Windows.Forms.DataGridView();
            this.staJurisdictions = new System.Windows.Forms.StatusStrip();
            this.tsPath = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
            this.scJurisdictions.Panel1.SuspendLayout();
            this.scJurisdictions.Panel2.SuspendLayout();
            this.scJurisdictions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeJurisdictions)).BeginInit();
            this.tsJurisdiction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdItems)).BeginInit();
            this.staJurisdictions.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGrid1
            // 
            appearance1.BackColor = System.Drawing.Color.White;
            this.ultraGrid1.DisplayLayout.Appearance = appearance1;
            this.ultraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ultraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ultraGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ultraGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TabRepeat;
            this.ultraGrid1.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.ultraGrid1.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.ultraGrid1.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.ultraGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.ultraGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None;
            this.ultraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.Color.Transparent;
            this.ultraGrid1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid1.DisplayLayout.Override.CellAppearance = appearance8;
            this.ultraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid1.DisplayLayout.Override.CellPadding = 3;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ultraGrid1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ultraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle;
            this.ultraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.ultraGrid1.DisplayLayout.Override.MergedCellContentArea = Infragistics.Win.UltraWinGrid.MergedCellContentArea.VirtualRect;
            this.ultraGrid1.DisplayLayout.Override.MergedCellStyle = Infragistics.Win.UltraWinGrid.MergedCellStyle.Never;
            appearance11.BorderColor = System.Drawing.Color.LightGray;
            appearance11.TextVAlignAsString = "Middle";
            this.ultraGrid1.DisplayLayout.Override.RowAppearance = appearance11;
            this.ultraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.Color.LightSteelBlue;
            appearance12.BorderColor = System.Drawing.Color.Black;
            appearance12.ForeColor = System.Drawing.Color.Black;
            this.ultraGrid1.DisplayLayout.Override.SelectedRowAppearance = appearance12;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ultraGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.ultraGrid1.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.None;
            this.ultraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGrid1.Location = new System.Drawing.Point(612, 495);
            this.ultraGrid1.Name = "ultraGrid1";
            this.ultraGrid1.Size = new System.Drawing.Size(10, 17);
            this.ultraGrid1.TabIndex = 1;
            this.ultraGrid1.Text = "ultraGrid1";
            // 
            // scJurisdictions
            // 
            this.scJurisdictions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.scJurisdictions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scJurisdictions.Location = new System.Drawing.Point(0, 0);
            this.scJurisdictions.Name = "scJurisdictions";
            // 
            // scJurisdictions.Panel1
            // 
            this.scJurisdictions.Panel1.Controls.Add(this.treeJurisdictions);
            // 
            // scJurisdictions.Panel2
            // 
            this.scJurisdictions.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scJurisdictions.Panel2.Controls.Add(this.grdItems);
            this.scJurisdictions.Panel2.Controls.Add(this.lblJurisdiction);
            this.scJurisdictions.Panel2.Controls.Add(this.tsJurisdiction);
            this.scJurisdictions.Size = new System.Drawing.Size(708, 489);
            this.scJurisdictions.SplitterDistance = 364;
            this.scJurisdictions.SplitterWidth = 1;
            this.scJurisdictions.TabIndex = 2;
            // 
            // treeJurisdictions
            // 
            this.treeJurisdictions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeJurisdictions.HideSelection = false;
            this.treeJurisdictions.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.treeJurisdictions.IsDirty = false;
            this.treeJurisdictions.LevelDepth = -1;
            this.treeJurisdictions.Location = new System.Drawing.Point(0, 0);
            this.treeJurisdictions.Name = "treeJurisdictions";
            this.treeJurisdictions.NodeConnectorColor = System.Drawing.SystemColors.ControlDark;
            this.treeJurisdictions.Size = new System.Drawing.Size(360, 485);
            this.treeJurisdictions.TabIndex = 0;
            this.treeJurisdictions.Tag2 = null;
            this.treeJurisdictions.Value = null;
            this.treeJurisdictions.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treeJurisdictions_AfterSelect);
            // 
            // lblJurisdiction
            // 
            this.lblJurisdiction.BackColor = System.Drawing.Color.Transparent;
            this.lblJurisdiction.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJurisdiction.Location = new System.Drawing.Point(3, 5);
            this.lblJurisdiction.Name = "lblJurisdiction";
            this.lblJurisdiction.Size = new System.Drawing.Size(72, 13);
            this.lblJurisdiction.TabIndex = 25;
            this.lblJurisdiction.Text = "&Jurisdiction";
            // 
            // tsJurisdiction
            // 
            this.tsJurisdiction.AutoSize = false;
            this.tsJurisdiction.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsJurisdiction.Dock = System.Windows.Forms.DockStyle.None;
            this.tsJurisdiction.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsJurisdiction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.tbtnJurisdictionDelete,
            this.toolStripSeparator2});
            this.tsJurisdiction.Location = new System.Drawing.Point(82, 0);
            this.tsJurisdiction.Name = "tsJurisdiction";
            this.tsJurisdiction.Size = new System.Drawing.Size(259, 27);
            this.tsJurisdiction.TabIndex = 24;
            this.tsJurisdiction.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // tbtnJurisdictionDelete
            // 
            this.tbtnJurisdictionDelete.AutoSize = false;
            this.tbtnJurisdictionDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnJurisdictionDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnJurisdictionDelete.Image")));
            this.tbtnJurisdictionDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnJurisdictionDelete.Name = "tbtnJurisdictionDelete";
            this.tbtnJurisdictionDelete.Size = new System.Drawing.Size(53, 22);
            this.tbtnJurisdictionDelete.Text = "&Delete...";
            this.tbtnJurisdictionDelete.ToolTipText = "Delete selected county";
            this.tbtnJurisdictionDelete.Click += new System.EventHandler(this.tbtnJurisdictionDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // grdItems
            // 
            this.grdItems.AllowUserToResizeRows = false;
            this.grdItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdItems.BackgroundColor = System.Drawing.Color.White;
            this.grdItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdItems.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdItems.Location = new System.Drawing.Point(0, 26);
            this.grdItems.MultiSelect = false;
            this.grdItems.Name = "grdItems";
            this.grdItems.RowHeadersVisible = false;
            this.grdItems.RowHeadersWidth = 25;
            this.grdItems.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdItems.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdItems.Size = new System.Drawing.Size(339, 458);
            this.grdItems.TabIndex = 1;
            this.grdItems.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdItems_CellValidated);
            this.grdItems.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdItems_CellValidating);
            this.grdItems.KeyUp += new System.Windows.Forms.KeyEventHandler(this.grdItems_KeyUp);
            // 
            // staJurisdictions
            // 
            this.staJurisdictions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsPath});
            this.staJurisdictions.Location = new System.Drawing.Point(0, 490);
            this.staJurisdictions.Name = "staJurisdictions";
            this.staJurisdictions.Size = new System.Drawing.Size(708, 22);
            this.staJurisdictions.SizingGrip = false;
            this.staJurisdictions.TabIndex = 3;
            this.staJurisdictions.Text = "statusStrip1";
            // 
            // tsPath
            // 
            this.tsPath.BackColor = System.Drawing.Color.Transparent;
            this.tsPath.Name = "tsPath";
            this.tsPath.Size = new System.Drawing.Size(0, 17);
            // 
            // JurisdictionsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.staJurisdictions);
            this.Controls.Add(this.scJurisdictions);
            this.Controls.Add(this.ultraGrid1);
            this.Name = "JurisdictionsManager";
            this.Size = new System.Drawing.Size(708, 512);
            this.Load += new System.EventHandler(this.JurisdictionsManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
            this.scJurisdictions.Panel1.ResumeLayout(false);
            this.scJurisdictions.Panel2.ResumeLayout(false);
            this.scJurisdictions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeJurisdictions)).EndInit();
            this.tsJurisdiction.ResumeLayout(false);
            this.tsJurisdiction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdItems)).EndInit();
            this.staJurisdictions.ResumeLayout(false);
            this.staJurisdictions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
        private System.Windows.Forms.SplitContainer scJurisdictions;
        private System.Windows.Forms.DataGridView grdItems;
        private System.Windows.Forms.StatusStrip staJurisdictions;
        private System.Windows.Forms.ToolStripStatusLabel tsPath;
        private LMP.Controls.JurisdictionsTree treeJurisdictions;
        private System.Windows.Forms.ToolStrip tsJurisdiction;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tbtnJurisdictionDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label lblJurisdiction;
    }
}
