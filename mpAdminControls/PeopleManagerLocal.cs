using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using LMP.Data;
using LMP.Controls;


namespace LMP.Administration.Controls
{
    public partial class PeopleManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region *************fields***************
        private LocalPersons m_oBasePeople;
        private LocalPersons m_oPersons;
        private Person m_oCurBasePerson;

        private Offices m_oAllOffices;
        private AttorneyLicenses m_oLicenses;
        private Jurisdictions m_oJurisdictions0 = new Jurisdictions(Jurisdictions.Levels.Zero);
        private Jurisdictions m_oJurisdictions1 = new Jurisdictions(Jurisdictions.Levels.One);
        private Jurisdictions m_oJurisdictions2 = new Jurisdictions(Jurisdictions.Levels.Two);
        private Jurisdictions m_oJurisdictions3 = new Jurisdictions(Jurisdictions.Levels.Three);
        private Jurisdictions m_oJurisdictions4 = new Jurisdictions(Jurisdictions.Levels.Four);
        private Hashtable m_oLevelIDs = new Hashtable(); //GLOG 15942
        private bool m_bDataIsBound = false;
        private bool m_bChangesMade = false;
        private bool m_bCboxOfficesIsBound = false;
        private bool m_bOfficesListIsBound = false;
        private int[] m_aAllOfficeIDs;
        private string m_xGrdLicensesBeforeEditCellValue;
        private bool m_bLicenseChangesMade = false;
        private bool m_bCurrentLicenseRowIsNewRecord = false;
        private bool m_bGrdLicensesIsBound = false;
        private BindingSource m_oPeopleSource;
        private BindingSource m_oPropSource;
        private DataRow m_oLastPeopleRow = null;
        private DataRow m_oLastPropRow = null;
        int m_iRowIndexOfIsAttorney = -1;
        int m_iRowIndexOfIsAuthor = -1;
        private string m_xCustomFields = "";
        private bool m_bDescriptIsEdited = false; //GLOG : 8301 : JSW
        private string m_xSearchString = "";
        private Timer m_oSearchTimer = new Timer();
        #endregion
        #region *************constructors***************
        public PeopleManager()
        {
            //subscribe to base events
            base.AfterControlLoaded += new AfterControlLoadedHandler(AdminManager_AfterControlLoaded);
            base.BeforeControlUnloaded += new BeforeControlUnLoadedHandler(AdminManager_BeforeControlUnloaded);

            InitializeComponent();

            if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
            {
                //GLOG 8220: Display people from ForteLocalPeople.mdb in Administrator
                m_oPersons = new LocalPersons(LMP.Data.mpPeopleListTypes.LocalPublicPeople);

                //Get a persons object consisting of all public
                //records, excluding extra offices
                // GLOG : 3518 : JAB
                // Include only public people (Owner ID == 0) in the local persons collection.
                m_oBasePeople = new LocalPersons(LMP.Data.mpPeopleListTypes.LocalPublicPeople,
                    "LinkedPersonID = 0 AND OwnerID = 0");
            }
            else
            {
                m_oPersons = new LocalPersons(LMP.Data.mpPeopleListTypes.AllActivePeopleInDatabase);

                //Get a persons object consisting of all public
                //records, excluding extra offices
                // GLOG : 3518 : JAB
                // Include only public people (Owner ID == 0) in the local persons collection.
                m_oBasePeople = new LocalPersons(LMP.Data.mpPeopleListTypes.AllActivePeopleInDatabase,
                    "LinkedPersonID = 0 AND OwnerID = 0");
            }
        }
        #endregion
        #region *************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }

        #endregion
        #region *************methods***************
        /// <summary>
        /// Display a list of all public people in 
        /// grdPeople
        /// </summary>
        private void DataBindGrdPeople()
        {
            // GLOG : 6027 : CEH
            DataTable oDT = m_oBasePeople.ToPeopleManagerDataSet().Tables[0];

            //create binding source
            m_oPeopleSource = new BindingSource();
            
            //bind table to bindingsource
            m_oPeopleSource.DataSource = oDT;
            
            //subscribe to positionchanged event - this will handle updates
            m_oPeopleSource.PositionChanged += new EventHandler(m_oPeopleSource_PositionChanged); 

            //Bind the datasource to grdPeople
            this.grdPeople.DataSource = m_oPeopleSource;

            //Set the display properties of grdPeople
            // GLOG : 6027 : CEH
			//ID & Login ID are not visible
            this.grdPeople.Columns[1].Visible = false;
            this.grdPeople.Columns[2].Visible = false;
            this.grdPeople.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //JTS 12/10/08: Store list of custom fields to check if definitions have changed when reloading
            m_xCustomFields = this.GetCustomFieldList();
        }
        /// <summary>
        /// Display the properties of the specified
        /// person in grdProperties
        /// </summary>
        private void BindPropertyGrid(string xPersonID)
        {
            try
            {
                m_bDataIsBound = false;

                if (this.grdPeople.Rows.Count == 0 || xPersonID == null)
                {
                    //user has deleted the last record
                    //clear the data in grdProperties
                    DataTable oDataTable = (DataTable)this.m_oPropSource.DataSource;
                    oDataTable.Clear();
                    this.grdProperties.DataSource = oDataTable;
                    return;
                }

                //Get the currently selected person record
                LocalPersons oPersons = null;
                if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                {
                    oPersons = new LocalPersons(mpPeopleListTypes.LocalPublicPeople);
                }
                else
                {
                    oPersons = new LocalPersons(mpPeopleListTypes.AllActivePeopleInDatabase);
                }

                Person oCurPerson = oPersons.ItemFromID(xPersonID);
                this.chkIsPrimaryOffice.CheckState = 
                    oCurPerson.IsDefaultOffice ? CheckState.Checked : CheckState.Unchecked;

                string xLoginID = m_oCurBasePerson.SystemUserID;

                //Get a 2d array of the property names and values 
                //of the current person record
                object[,] aProperties = oCurPerson.ToArray();

                //Create a hashtable for the properties listed in aProperties
                //that do not have a hard-coded order
                Hashtable oProperties = System.Collections.Specialized
                    .CollectionsUtil.CreateCaseInsensitiveHashtable();

                //transfer properties to hashtable -- this way we can 
                //control order of their presentation in the properties grid
                for (int i = 0; i <= aProperties.GetUpperBound(0); i++)
                {
                    string[] aValues = new string[2];
                    //get property data type name
                    aValues[1] = aProperties[i, 1] == null ? "String" :
                        aProperties[i, 1].GetType().Name.ToString();
                    //get property value
                    aValues[0] = aProperties[i, 1] == null ? "" : 
                        aProperties[i, 1].ToString();
                    //add value/data type name to hashtable
                    oProperties.Add(aProperties[i, 0], aValues);
                }

                //Create a DataTable to populate with the property names 
                //and values
                DataTable oDT = new DataTable();
                oDT.Columns.Add("Property");
                oDT.Columns.Add("Value");
                oDT.Columns.Add("DataType");

                //create storage for hashtable keys
                //their order determines order in property grid -- some are custom,
                //so we need to test whether they actually exist, i.e, have been
                //returned by oPerson.ToArray(), before attempting to
                //add rows to the grid
                //office, shortname, admitted in, and Email have different
                //display names than property names
                string[,] xKeys = { {"Login ID", "System User ID"},
                                    {"Prefix", "Prefix"},
                                    {"First Name", "First Name"},
                                    {"Middle Name", "Middle Name"},
                                    {"Last Name", "Last Name"},
                                    {"Suffix", "Suffix"},
                                    {"Display Name", "Display Name"},
                                    {"Full Name", "Full Name"},
                                    {"Short Name", "ShortName"},
                                    {"Initials", "Initials"},
                                    {"Title", "Title"},
                                    {"Phone", "Phone"},
                                    {"Fax", "Fax"},
                                    {"Email", "EMail"},
                                    {"Is Author", "Is Author"},
                                    {"Is Attorney", "Is Attorney"},
                                    {"Admitted In", "AdmittedIn"},
                                    {"ID", "ID"},
                                    {"Owner ID", "Owner ID"},
                                    {"Linked Person ID", "Linked Person ID"},
                                    {"Office Usage State", "OfficeUsageState"},
                                    {"Default Office Record ID1", "DefaultOfficeRecordID1"}};

                //add rows to datatable only if hashtable item exists
                int iNumKeys = xKeys.GetUpperBound(0);
                for (int i = 0; i <= iNumKeys; i++)
                {
                    string xDisplayName = xKeys[i, 0];
                    string xPropName = xKeys[i, 1];

                    if (oProperties.ContainsKey(xPropName))
                    {
                        if (xKeys[i, 1] == "System User ID")
                            //add login ID to datatable
                            oDT.Rows.Add(xDisplayName, xLoginID, "String");
                        else if (xDisplayName != "Default Office Record ID1" &&
                            xDisplayName != "Office Usage State")
                        {
                            //extract value/data type name array from hashtable
                            string[] aValues = (string[])oProperties[xPropName];

                            //add name, value, and data type name to datatable
                            oDT.Rows.Add(xDisplayName, aValues[0], aValues[1]);
                        }

                        //remove from hashtable once added
                        oProperties.Remove(xKeys[i, 1]);
                    }
                }

                //remove OfficeID - this is not handled in the grid
                oProperties.Remove("Office ID");
                oProperties.Remove("DefaultOfficeRecordID1");
                oProperties.Remove("OfficeUsageState");

                //Add rows for additional custom properties set up by admin
                //they will be any items remaining in the hashtable
                ICollection oKeys = oProperties.Keys;
                foreach (string xKey in oKeys)
                {
                    //extract value/data type name array from hashtable
                    string[] aValues = (string[])oProperties[xKey];

                    //add name, value, and data type name to datatable
                    oDT.Rows.Add(xKey, aValues[0], aValues[1]);
                }

                oDT.AcceptChanges();

                // Subscribe to the table's data row change event to enforce the 
                // IsAttorney IsAuthor relation.
                oDT.RowChanged += new DataRowChangeEventHandler(oDT_RowChanged);

                //create binding source
                m_oPropSource = new BindingSource();

                //unsubscribe to any existing event handler
                m_oPropSource.PositionChanged -= m_oPropSource_PositionChanged;

                //subscribe to position changed handler - this will handle updates to record
                m_oPropSource.PositionChanged += new EventHandler(m_oPropSource_PositionChanged);

                //bind datatable to binding source
                m_oPropSource.DataSource = oDT;
                
                //Bind the DataTable of properties to grdProperties
                this.grdProperties.DataSource = m_oPropSource;

                //Set the display properties of the gird
                this.grdProperties.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
                this.grdProperties.Columns[0].ReadOnly = true;
                this.grdProperties.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                this.grdProperties.Columns[1].FillWeight = 50;
                
                //hide the data type column
                this.grdProperties.Columns[2].Visible = false;

                //Hide ID, SystemUserID, OwnerID, and LinkedPeronID rows
                this.HidePersonIDProperties();

                //create boolean combo lookups
                CreateBooleanCombo();

                // Enforce the IsAttorney IsAuthor relation.
                EnforceIsAttorneyIsAuthorRelation();
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.
                    GetLangString("Error_CouldNotDataBindGrdProperties"), oE);
            }

            m_bDataIsBound = true;
        }

        void oDT_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            // If the changing row is the row of the IsAttorney field.
            if (e.Row.Table.Rows.IndexOf(e.Row) == m_iRowIndexOfIsAttorney)
            {
                EnforceIsAttorneyIsAuthorRelation();
            }
        }

        private void EnforceIsAttorneyIsAuthorRelation()
        {
            DataTable oDT = (DataTable)m_oPropSource.DataSource;

            if (oDT != null && m_iRowIndexOfIsAttorney != -1 && m_iRowIndexOfIsAuthor != -1)
            {
                DataGridViewComboBoxCell oDGVComboBoxCell = (DataGridViewComboBoxCell)this.grdProperties.Rows[m_iRowIndexOfIsAuthor].Cells[1];

                // Set IsAuthor = true whenever IsAttorney is set to true.
                if (((string)(oDT.Rows[m_iRowIndexOfIsAttorney][1])).ToUpper() == "TRUE")
                {
                    oDT.Rows[m_iRowIndexOfIsAuthor][1] = (string)(oDT.Rows[m_iRowIndexOfIsAttorney][1]);
                    string[] aTF = { "True" };
                    oDGVComboBoxCell.DataSource = aTF;
                }
                else
                {
                    string[] aTF = { "True", "False" };
                    oDGVComboBoxCell.DataSource = aTF;
                }
            }
        }
        /// <summary>
        /// configures boolean lookup cell for properties grid
        /// </summary>
        private void CreateBooleanCombo()
        {
            //storage for lookup values
            string[] aTF = { "True", "False" };

            //change all boolean type cells to combo lookups
            for (int i = 0; i < grdProperties.Rows.Count; i++)
            {
                if (grdProperties.Rows[i].Cells[2].FormattedValue.ToString() == "Boolean")
                {
                    this.grdProperties.Rows[i].Cells[1] = new DataGridViewComboBoxCell();

                    //set the data source to lookup array
                    ((DataGridViewComboBoxCell)this.grdProperties.Rows[i].Cells[1]).
                        DataSource = aTF;

                    // Store the IsAttorney and IsAuthor row indices to later be used
                    // when enforcing the IsAttorney IsAuthor relation.

                    if (m_iRowIndexOfIsAuthor == -1 && grdProperties.Rows[i].Cells[0].FormattedValue.ToString() == "Is Author")
                    {
                        m_iRowIndexOfIsAuthor = i;
                    }
                    else
                    {
                        if (m_iRowIndexOfIsAttorney == -1 && grdProperties.Rows[i].Cells[0].FormattedValue.ToString() == "Is Attorney")
                        {
                            m_iRowIndexOfIsAttorney = i;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// hides display of various ID properties in properties grid
        /// </summary>
        private void HidePersonIDProperties()
        {
            for (int i = 0; i < grdProperties.Rows.Count; i++)
            {
                switch (grdProperties.Rows[i].Cells[0].FormattedValue.ToString())
                {
                    //case "Login ID":
                    case "ID":
                    case "Linked Person ID":
                    case "Owner ID":
                        grdProperties.Rows[i].Visible = false;
                        break;
                }
            }
        }
        //populates the offices checked list box
        private void BindOfficesList()
        {
            //create collection of all office records
            m_oAllOffices = new Offices();

            //retrieve all officeIDs and display names
            Array aOffices = m_oAllOffices.ToArray(0, 4);

            m_aAllOfficeIDs = new int[aOffices.GetLength(0)];

            //add office names and IDs to datatable
            DataTable oDT = new DataTable();
            oDT.Columns.Add("DisplayName");
            oDT.Columns.Add("ID");

            for (int i = 0; i < aOffices.GetLength(0); i++)
            {
                object[] oValues = (object[])aOffices.GetValue(i);
                m_aAllOfficeIDs[i] = (int)oValues[0];

                oDT.Rows.Add(oValues[1].ToString(), oValues[0].ToString());
            }
            oDT.AcceptChanges();

            //clear existing listbox datasource
            ListControl oListCtl = (ListControl)this.lstOffices;
            if (oListCtl.DataSource != null)
                ((DataTable)oListCtl.DataSource).Clear();

            //set listbox datasource, valuemember and displaymember
            oListCtl.DataSource = oDT;
            oListCtl.ValueMember = "ID";
            oListCtl.DisplayMember = "DisplayName";
        }
        /// <summary>
        /// checks the offices that used by the current person
        /// </summary>
        private void MarkupOfficesList()
        {
            m_bOfficesListIsBound = false;
            if (m_oCurBasePerson.RelatedPeople != null)
            {
                //Only databind lstOffices if the current 
                //person record has been initialized
                if (this.grdPeople.RowCount == 0)
                {
                    //User entered the row for new records-
                    //Clear the items that are in lstOffices
                    ((ListControl)lstOffices).DataSource = null;
                    return;
                }

                //we rebind the list to ensure that newly
                //added/deleted offices are displayed appropriately
                BindOfficesList();

                //get the field index of the office property -- this could vary
                //if admin has removed any Forte custom properties such as ShortName
                int iOff = GetPersonOfficeFieldIndex();
                
                //Get an array of the IDs of the Offices the 
                //current person has records for
                Array oCurPersonOfficesArray = m_oCurBasePerson.RelatedPeople.ToArray(iOff);

                //Check the offices in lstOffices to which the 
                //current person belongs
                for (int i = 0; i < oCurPersonOfficesArray.GetLength(0); i++)
                {
                    //Get the ID of the current Office
                    int iCurOfficeID = (int)((System.Array)
                        oCurPersonOfficesArray.GetValue(i)).GetValue(0);

                    //Check the Office in lstOffices
                    this.lstOffices.SetItemChecked(Array
                        .IndexOf(m_aAllOfficeIDs, iCurOfficeID), true);

                }
                m_bOfficesListIsBound = true;
                //m_bOfficeRecordsAltered = false;
            }
        }
        /// <summary>
        /// Display the offices that the current person
        /// belongs to in cboxOffices
        /// </summary>
        private void BindOfficeDropdown(string xID)
        {
            if (m_oCurBasePerson.RelatedPeople != null)
            {
                //Only databind lstOffices if the current 
                //person record has been initialized

                if (this.grdPeople.RowCount == 0)
                {
                    //User entered the row for new records-
                    //Clear the items that are in cboxOffices
                    this.cboxOffices.DataSource = null;

                    return;
                }

                m_bCboxOfficesIsBound = false;

                //Clear the offices already in cboxOffices
                this.cboxOffices.DataSource = null;

                //get the field index of offices field in people table
                int iOffInd = GetPersonOfficeFieldIndex();
                
                //Get an array of all the people records
                //person will have one record for each office
                Array oOfficesArray =
                    m_oCurBasePerson.RelatedPeople.ToArray(iOffInd, 0, 1);

                //create datatable to hold office display names and
                //person record IDs
                DataTable oDT = new DataTable();
                oDT.Columns.Add("OfficeDisplayName");
                oDT.Columns.Add("RecordID");

                for (int i = 0; i < oOfficesArray.GetLength(0); i++)
                {
                    //get person record IDs - concatenate
                    string xRecordID = 
                        ((Array)oOfficesArray.GetValue(i)).GetValue(1).ToString() + "." +
                        ((Array)oOfficesArray.GetValue(i)).GetValue(2).ToString();
                    
                    //get office ID for that record
                    string xOfficeID = 
                        ((Array)oOfficesArray.GetValue(i)).GetValue(0).ToString();
                    
                    //get office display name 
                    string xOfficeName = ((Office)m_oAllOffices.ItemFromID(Int32.Parse(xOfficeID))).DisplayName;

                    oDT.Rows.Add(xOfficeName, xRecordID); 
                }

                //sort the datatable by office displayname
                oDT.DefaultView.Sort = "OfficeDisplayName";
                this.cboxOffices.DataSource = oDT;
                this.cboxOffices.ValueMember = "RecordID";
                this.cboxOffices.DisplayMember = "OfficeDisplayName";

                //this is not a new record -
                //select the default office for the selected person
                string xDefOfficeID = m_oCurBasePerson.DefaultOfficeRecordID.ToString();

                if (xDefOfficeID == "0")
                    //no default office specified - use office of base record
                    xDefOfficeID = xID;
                else
                    xDefOfficeID += ".0";

                //force SelectedIndexChanged event to fire
                //when office is selected
                this.cboxOffices.SelectedIndex = -1;

                this.m_bCboxOfficesIsBound = true;

                //select office
                this.cboxOffices.SelectedValue = xDefOfficeID;
            }
        }
        /// <summary>
        /// Display the licenses corresponding to the 
        /// person record that is selected in grdLicenses
        /// </summary>
        public void BindLicenseGrid(string xCurPersonID)
        {
            //Note that grdLicenses is in the process of being bound
            m_bGrdLicensesIsBound = false;

            if (System.String.IsNullOrEmpty(xCurPersonID))
            {
                //User has entered the row for new records, clear grdLicenses
                this.grdLicenses.DataSource = null;
                this.grdLicenses.Rows.Clear();

                //Keep the user from entering invalid data
                this.grdLicenses.ReadOnly = true;

                return;
            }

            //load license grid only for attorneys
            if (((Person)m_oBasePeople.ItemFromID(xCurPersonID)).IsAttorney)
            {
                //Make sure grdLicenses is not ReadOnly
                this.grdLicenses.ReadOnly = false;

                //Clear any records already in the grid
                this.grdLicenses.DataSource = null;
                this.grdLicenses.Rows.Clear();

                m_oLicenses = new AttorneyLicenses(xCurPersonID, (LMP.MacPac.MacPacImplementation.IsFullLocal && LMP.Data.Application.AdminMode)); //GLOG 8316

                //An array to use in creating rows for grdLicenses
                object[] aLicenseProperties = new object[8];

                AttorneyLicense oLicense;
                
                //initialize hashtable for jurisdiction ID storage
                m_oLevelIDs = new Hashtable();

                for (int i = 1; i <= m_oLicenses.Count; i++)
                {
                    oLicense = (AttorneyLicense)m_oLicenses.ItemFromIndex(i);

                    //Fill an array with the properties of the current AttorneyLicense
                    aLicenseProperties[0] = oLicense.Description;
                    aLicenseProperties[1] = oLicense.License;
                    aLicenseProperties[2] = (oLicense.L0 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions0.ItemFromID(oLicense.L0)).Name;
                    aLicenseProperties[3] = (oLicense.L1 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions1.ItemFromID(oLicense.L1)).Name;
                    aLicenseProperties[4] = (oLicense.L2 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions2.ItemFromID(oLicense.L2)).Name;
                    aLicenseProperties[5] = (oLicense.L3 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions3.ItemFromID(oLicense.L3)).Name;
                    aLicenseProperties[6] = (oLicense.L4 == 0) ? null :
                        ((Jurisdiction)m_oJurisdictions4.ItemFromID(oLicense.L4)).Name;
                    aLicenseProperties[7] = oLicense.ID;

                    //Add the row to the grid
                    this.grdLicenses.Rows.Add(aLicenseProperties);

                    DataGridViewCellCollection oCurCells = this.grdLicenses.Rows[i - 1].Cells;

                    //Store the IDs of the Jurisdictions in hashtable
                    //grid will display jurisdiction name
                    int[] iLevels = {   oLicense.L0,
                                        oLicense.L1,
                                        oLicense.L2,
                                        oLicense.L3,
                                        oLicense.L4};

                    string xKey = "License" + i.ToString();
                    m_oLevelIDs.Add(xKey, iLevels);
                }

                //Set the appropriate columns to readonly = true
                for (int f = 2; f <= 6; f++)
                    this.grdLicenses.Columns[f].ReadOnly = true;

                //freeze description column
                grdLicenses.Columns["Description"].Frozen = true;

                //Note that grdLicenses is now bound
                m_bGrdLicensesIsBound = true;
           }
            else
            {
                
                //User is not an attorney, clear grdLicenses
                this.grdLicenses.DataSource = null;
                this.grdLicenses.Rows.Clear();

                //User has selected a non-attorney while grdLicenses
                //is displayed, change the selected tab in tabPersonalDetail
                //to a display relevant data
                if (this.tabPersonDetail.SelectedIndex == 2)
                    this.tabPersonDetail.SelectedIndex = 0;
            }

        }
        /// <summary>
        /// Add a new person record
        /// </summary>
        private void AddPerson()
        {
            //create a new person record
            Person oNewPerson = m_oBasePeople.Create();

            //Set the properties of the new person record
            oNewPerson.DisplayName = "New Person";
            if (m_oAllOffices == null)
                m_oAllOffices = new Offices();

            //GLOG item #5296 - dcf
            //prompt for office before record is created
            int iPrimaryOffice = SelectPrimaryOffice();

            if (iPrimaryOffice == 0)
            {
                return;
            }
            else
            {
                oNewPerson.OfficeID = iPrimaryOffice;
            }

            oNewPerson.UsageState = 1;
            oNewPerson.OfficeUsageState = 1;
            oNewPerson.IsAuthor = true;

            try
            {
                //Save the new person record to the DB
                m_oBasePeople.Save(oNewPerson);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }

            //Create an array of the display values of the new 
            //person
            object[] aDisplayProperties = new object[2];
            aDisplayProperties[0] = oNewPerson.DisplayName;
            aDisplayProperties[1] = oNewPerson.ID;

            //Add a row representing the new person to grdPeople's DataSource
            ((DataTable)m_oPeopleSource.DataSource).Rows.Add(aDisplayProperties);

            //navigate to the new row
            grdPeople.CurrentCell = grdPeople.Rows[grdPeople.RowCount - 1].Cells["DisplayName"];

            //set focus on properties grid for new input
            if (grdProperties.RowCount == 0)
            {
                SetupUIForPerson(oNewPerson);
            }
            
            grdProperties.Focus();
            grdProperties.CurrentCell = grdProperties.Rows[0].Cells["Value"];
            grdProperties.BeginEdit(true);
        }

        /// <summary>
        /// prompts the user to select a primary office for a new person -
        /// returns the ID of the selected primary office
        /// </summary>
        /// <returns></returns>
        private int SelectPrimaryOffice()
        {
            SelectPrimaryOfficeForm oForm = new SelectPrimaryOfficeForm();
            if (oForm.ShowDialog() == DialogResult.OK)
            {
                return oForm.PrimaryOffice;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// sets up the UI to display the specified person
        /// </summary>
        /// <param name="oPerson"></param>
        private void SetupUIForPerson(Person oPerson)
        {
            m_oCurBasePerson = oPerson;
            BindControls(oPerson.ID);
        }
        /// <summary>
        /// Create/Delete secondary office records to
        /// match the list of checked offices that the
        /// user has just editted for this person
        /// </summary>
        private void UpdateOfficePersonRecordsIfNecessary()
        {
            if (m_oCurBasePerson.RelatedPeople == null)
                return;

            //Get the current person record
            Person oCurPerson = m_oBasePeople.ItemFromID
                (this.grdPeople.CurrentRow.Cells[1].FormattedValue.ToString());
            
            //create array list of all secondary officeIDs
            ArrayList oCheckedOfficeIDs = new ArrayList();
            ArrayList oSecondaryOfficeIDs = new ArrayList();
            
            Array aIDs = m_oCurBasePerson.RelatedPeople.ToArray(GetPersonOfficeFieldIndex());
            
            for (int i = 0; i < aIDs.GetLength(0); i++)
                oSecondaryOfficeIDs.Add((int)((Array)aIDs.GetValue(i)).GetValue(0)); 
            
            System.Windows.Forms.CheckedListBox.CheckedIndexCollection
                oCheckedIndices = this.lstOffices.CheckedIndices;

            for (int i = 0; i < oCheckedIndices.Count; i++)
                oCheckedOfficeIDs.Add(m_aAllOfficeIDs[oCheckedIndices[i]]);

            bool bChangesMade = false;

            //Add new records for any offices the user has
            //just checked
            for (int i = 0; i < oCheckedIndices.Count; i++)
            {
                if (!oSecondaryOfficeIDs.Contains(oCheckedOfficeIDs[i]))
                {
                    //get office ID from checked list item
                    DataRowView oDR = (DataRowView)lstOffices.Items[oCheckedIndices[i]];
                    int iID = Int32.Parse(oDR.Row.ItemArray.GetValue(1).ToString());

                    //user has checked new office - add person record
                    AddSecondaryOfficePerson(oCurPerson, iID);

                    //Note that cboxOffices should be rebound
                    bChangesMade = true;
                }
            }

            //Delete any secondary Office records that
            //the user has unchecked -- since we're using 
            //ItemByIndex, delete from end of collection to front

            for (int i = oSecondaryOfficeIDs.Count - 1; i >= 0; i--)
            {
                if (!oCheckedOfficeIDs.Contains((int)oSecondaryOfficeIDs[i]))
                {
                    //Get the current secondary office record
                    Person oSecondaryOfficePerson = m_oCurBasePerson.RelatedPeople
                        .ItemFromIndex(i + 1);

                    //Delete the secondary office person record
                    DeleteOffice(oSecondaryOfficePerson);

                    //Note that cboxOffices should be rebound
                    bChangesMade = true;
                }
            }
            
            //Refresh the list of Offices in cboxOffices if changes made
            if (bChangesMade)
            {
                this.BindOfficeDropdown(m_oCurBasePerson.ID);
            }

            //m_bOfficeRecordsAltered = false;
        }

        /// <summary>
        /// deletes designated secondary office person record
        /// </summary>
        /// <param name="oSecondaryOfficePerson"></param>
        private void DeleteOffice(Person oOfficePerson)
        {
            //Only delete records if they are not the primary user record
            try
            {
                if (oOfficePerson.IsDefaultOffice)
                {
                    //message user and return to original value
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CannotUnselectPrimaryOffice"),
                           LMP.ComponentProperties.ProductName,
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                oOfficePerson.RemoveOffice(oOfficePerson.OfficeID);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// adds a new person record with designated secondary
        /// office ID
        /// </summary>
        /// <param name="oCurPerson"></param>
        private void AddSecondaryOfficePerson(Person oCurPerson, int iNewID)
        {
            Person oNewSecondaryOffice = m_oBasePeople.Create();

            //Get the ID of the office the User is 
            //adding a record for
            int iCurOfficeID = iNewID;

            //check to see if there is already this office record -
            //it might have been "deleted" previously
            Persons oPeople = null;
            if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
            {
                oPeople = new LocalPersons(mpPeopleListTypes.LocalPublicPeople, "LinkedPersonID=" +
                    oCurPerson.ID1 + " AND OfficeID=" + iNewID, UsageStates.Inactive);
            }
            else
            {
                oPeople = new LocalPersons(mpPeopleListTypes.AllActivePeopleInDatabase, "LinkedPersonID=" +
                    oCurPerson.ID1 + " AND OfficeID=" + iNewID, UsageStates.Inactive);
            }
            if (oPeople.Count > 0)
            {
                //such a record was found - use it
                Person oPerson = oPeople.ItemFromIndex(1);

                oPerson.UsageState = 1;

                oPeople.Save(oPerson);
            }
            else
            {
                //no secondary record was found - try the primary record
                if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                {
                    oPeople = new LocalPersons(mpPeopleListTypes.LocalPublicPeople, "ID1=" + oCurPerson.ID1 +
                        " AND ID2=0 AND OfficeID=" + iNewID, UsageStates.Active);
                }
                else
                {
                    oPeople = new LocalPersons(mpPeopleListTypes.AllActivePeopleInDatabase, "ID1=" + oCurPerson.ID1 +
                        " AND ID2=0 AND OfficeID=" + iNewID, UsageStates.Active);
                }

                if (oPeople.Count > 0)
                {
                    //such a record was found - use it
                    Person oPerson = oPeople.ItemFromIndex(1);

                    oPerson.OfficeUsageState = 1;

                    oPeople.Save(oPerson);
                }
                else
                {
                    //Set the properties of the new record that
                    //distinguish it as a seconday office record
                    oNewSecondaryOffice.LinkedPersonID = oCurPerson.ID1;
                    oNewSecondaryOffice.OwnerID = 0;

                    //Set the Office ID of the new record to
                    //the ID of the Office the user has checked
                    oNewSecondaryOffice.OfficeID = iCurOfficeID;

                    //Set the properties of the new record
                    //equal to the original person's
                    oNewSecondaryOffice.NamePrefix = oCurPerson.NamePrefix;
                    oNewSecondaryOffice.DisplayName = oCurPerson.DisplayName;
                    oNewSecondaryOffice.FullName = oCurPerson.FullName;
                    oNewSecondaryOffice.FirstName = oCurPerson.FirstName;
                    oNewSecondaryOffice.MiddleName = oCurPerson.MiddleName;
                    oNewSecondaryOffice.LastName = oCurPerson.LastName;
                    oNewSecondaryOffice.NameSuffix = oCurPerson.NameSuffix;
                    oNewSecondaryOffice.IsAuthor = oCurPerson.IsAuthor;
                    oNewSecondaryOffice.IsAttorney = oCurPerson.IsAttorney;
                    oNewSecondaryOffice.UsageState = 1;

                    //Get a 2d array of the property names and values 
                    //of the current person record
                    object[,] aProperties = oCurPerson.ToArray();

                    //Create a hashtable for the properties listed in aProperties
                    //that do not have a hard-coded order
                    Hashtable oProperties = System.Collections.Specialized.
                        CollectionsUtil.CreateCaseInsensitiveHashtable();

                    //Populate the Hashtable
                    for (int j = 14; j < aProperties.GetLength(0); j++)
                        oProperties.Add(aProperties[j, 0], aProperties[j, 1]);

                    ICollection oKeys = oProperties.Keys;
                    foreach (string oKey in oKeys)
                        oNewSecondaryOffice.SetPropertyValue(oKey, oProperties[oKey]);

                    try
                    {
                        //Save the new record to the DB
                        m_oBasePeople.Save(oNewSecondaryOffice);
                    }
                    catch (System.Exception oE)
                    {
                        throw new LMP.Exceptions.DataException(
                            LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
                    }
                }
            }
        }
        /// <summary>
        /// Delete the selected person record after prompting 
        /// the user to confirm the delete
        /// </summary>
        private void DeletePerson()
        {
            //exit if no people records exist
            if (this.grdPeople.Rows.Count == 0)
                return;
            
            string xMsg = "";
            
            //Confirm that the user intends to delete this record.
            xMsg = LMP.Resources.GetLangString("Message_DeleteEntry");
            
            DialogResult oChoice = MessageBox.Show(xMsg, LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                //GLOG 4546: Delete using Data class method to ensure related records are deleted and logged
                m_oBasePeople.Delete(m_oCurBasePerson.ID);
                ////to delete - set usage state to 0
                //m_oCurBasePerson.UsageState = 0;
                //save
                //m_oBasePeople.Save(m_oCurBasePerson);
                //remove row
                this.grdPeople.Rows.Remove(this.grdPeople.CurrentRow);

                if (grdPeople.Rows.Count > 0)
                {
                    //select new row
                    this.grdPeople.CurrentCell.Selected = true;
                    string xID = grdPeople.CurrentRow.Cells["ID"].FormattedValue.ToString();
                    SetupUIForPerson(LocalPersons.GetPersonFromID(xID));
                }
                else
                    SetupUIForPerson(null);

                //GLOG : 8301 : jsw
                this.m_bDescriptIsEdited = false;

            }
        }
        
        /// <summary>
        /// GLOG : 2233 : JAB
        /// updates properties using the appropriate people collection.
        /// </summary>
        private void UpdatePerson(string xID)
        {
            DataGridViewRowCollection oRows = this.grdProperties.Rows;

            //construct displayname, fullname, and initials,
            //update grid fields only if current people
            //record is new, ie. rowstate == added, and we have not
            //explicitly edited the display name field

            if (m_oLastPeopleRow != null &&
                m_oLastPeopleRow.RowState == DataRowState.Added)
            {
                if (grdProperties.CurrentRow.Index == 5)
                    //suffix field is active - save
                    m_oLastPeopleRow.AcceptChanges();

                UpdateCalculatedFields();
            }

            Person oCurPerson;

            // GLOG : 2233 : JAB
            // Get the current person record according to if the persons
            // is unlinked.
            if (this.IsCurrentPersonUnlinked())
            {
                oCurPerson = m_oBasePeople.ItemFromID(xID);
            }
            else
            {
                oCurPerson = LocalPersons.GetPersonFromID(xID);
            }

            //GLOG 4942
            Person oLinkedPerson = null;
            Persons oLinkedPeople = null;
            ArrayList oUpdatedPeople = new ArrayList();
            if (oCurPerson.LinkedPersonID == 0 && oCurPerson.OwnerID == 0)
            {
                //update login ID
                oCurPerson.SystemUserID = oRows[0].Cells[1].FormattedValue.ToString();
            }
            else
            {
                //person is linked - update the Login ID of the linked person
                if (oCurPerson.LinkedPersonID != 0)
                {
                    try
                    {
                        oLinkedPerson = m_oBasePeople.ItemFromID(oCurPerson.LinkedPersonID);
                    }
                    catch { }

                    if (oLinkedPerson != null)
                        oLinkedPerson.SystemUserID = oRows[0].Cells[1].FormattedValue.ToString();
                }
            }
            //GLOG 4942
            UpdatePersonProperties(oCurPerson);

            if (oLinkedPerson != null)
            {
                //GLOG 4942: Update properties for Main record and all additional office records as well
                UpdatePersonProperties(oLinkedPerson);
                oLinkedPeople = oLinkedPerson.GetAllSecondaryRecords();
                for (int p = 1; p <= oLinkedPeople.Count; p++)
                {
                    //Update secondary People records other than current record
                    if (oLinkedPeople.ItemFromIndex(p).ID != oCurPerson.ID)
                    {
                        Person oSecondary = oLinkedPeople.ItemFromIndex(p);
                        UpdatePersonProperties(oSecondary);
                        oUpdatedPeople.Add(oSecondary);
                    }
                }
            }
            m_bChangesMade = false;

            try
            {
                //Attempt to update the DB
                m_oBasePeople.Save(oCurPerson);

                if (oLinkedPerson != null)
                {
                    //person is a linked person -
                    //save the login ID to the linked person
                    m_oBasePeople.Save(oLinkedPerson);
                    //GLOG 4942: Save additional secondary records that have changed
                    for (int p = 0; p < oUpdatedPeople.Count; p++)
                    {
                        Person oSecondary = (Person)oUpdatedPeople[p];
                        oLinkedPeople.Save(oSecondary);
                    }
                }
                else if (oCurPerson.ID == m_oCurBasePerson.ID)
                {
                    //this person is the current base person - update that object
                    m_oCurBasePerson = oCurPerson;
                }
                //accept changes for global data row variable
                //this will reset row state
                m_oLastPropRow.AcceptChanges();

                //Set the display name in grdPeople if the
                //user has changed it
                if (this.grdPeople.CurrentRow.Cells["DisplayName"].
                    FormattedValue.ToString() != oCurPerson.DisplayName)
                    this.grdPeople.CurrentRow.Cells[0].Value = oCurPerson.DisplayName;
            }

            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        private void UpdatePersonProperties(LMP.Data.Person oCurPerson)
        {
            DataGridViewRowCollection oRows = this.grdProperties.Rows;

            //set values for hard coded standard Forte person properties
            //Update NamePrefix
            oCurPerson.NamePrefix = oRows[1].Cells[1].FormattedValue.ToString();

            //Update FirstName
            oCurPerson.FirstName = oRows[2].Cells[1].FormattedValue.ToString();

            //Update MiddleName
            oCurPerson.MiddleName = oRows[3].Cells[1].FormattedValue.ToString();

            //Update LastName
            oCurPerson.LastName = oRows[4].Cells[1].FormattedValue.ToString();

            //Update NameSuffix
            oCurPerson.NameSuffix = oRows[5].Cells[1].FormattedValue.ToString();

            //Update DisplayName only if the new value
            //is not null
            if (oRows[6].Cells[1].FormattedValue.ToString() != "")
                oCurPerson.DisplayName = oRows[6].Cells[1].FormattedValue.ToString();

            //Update FullName
            oCurPerson.FullName = oRows[7].Cells[1].FormattedValue.ToString();

            //build storage for Forte custom props -- datatable will contain display name
            //which is in some cases different from the actual property name
            string[,] aCustomProps = {  {"Short Name", "ShortName"},
                                        {"Initials", "Initials"},
                                        {"Title", "Title"},
                                        {"Phone", "Phone"},
                                        {"Fax", "Fax"},
                                        {"Email", "EMail"},
                                        {"Admitted In", "AdmittedIn"},
                                        {"Is Attorney", "Is Attorney"},
                                        {"Is Author", "Is Author"}};

            //custom properties may not exist - attempt to select items
            //from properties grid binding source, then set person props accordingly
            DataTable oDT = (DataTable)((BindingSource)grdProperties.DataSource).DataSource;
            DataRow[] oDR = null;

            for (int i = 0; i <= aCustomProps.GetUpperBound(0); i++)
            {
                //select datarow from datatable using property name as shown in grid
                oDR = oDT.Select("Property = '" + aCustomProps[i, 0] + "'");

                //if row exists set property value - get name, value and type from
                //datarow
                int iOffset = 0;
                if (oDR.Length > 0)
                {
                    //check for Is Attorney and Is Author -- these are not
                    //custom props, but have been presented in the grid below 
                    //Forte custom props, so their row number can't be known if 
                    //Forte custom items have been omitted by admin
                    string xProp = aCustomProps[i, 1];
                    string xValue = ((DataRow)oDR.GetValue(0)).ItemArray.GetValue(1).ToString();
                    string xType = ((DataRow)oDR.GetValue(0)).ItemArray.GetValue(2).ToString();

                    if (xProp == "Is Attorney")
                        oCurPerson.IsAttorney = Boolean.Parse(xValue);
                    else if (xProp == "Is Author")
                        oCurPerson.IsAuthor = Boolean.Parse(xValue);
                    else
                        SetPersonCustomProperty(oCurPerson, xProp, xValue, xType);
                }
                else
                {
                    //prop does not exist in the grid - increment an offset count
                    iOffset++;
                }
            }

            //Update any other custom properties set by admin
            //these are stored in additional rows set up when
            //the properties grid was bound
            //get field count of 8 standard mp fields, 
            //count of existing mpcustom fields, plus 3 hidden fields
            int iCount = aCustomProps.GetLength(0) + 8 + 3;

            for (int i = iCount; i < oRows.Count; i++)
            {
                string xValue = oRows[i].Cells[1].FormattedValue.ToString();
                string xProp = oRows[i].Cells[0].FormattedValue.ToString();
                string xType = oRows[i].Cells[2].FormattedValue.ToString();
                SetPersonCustomProperty(oCurPerson, xProp, xValue, xType);
            }

        }
        /// <summary>
        /// sets value of specified custom property
        /// of specified data type
        /// </summary>
        /// <param name="oPerson"></param>
        /// <param name="xName"></param>
        /// <param name="xValue"></param>
        /// <param name="xDataType"></param>
        private void SetPersonCustomProperty(LMP.Data.Person oPerson, string xName,
                                          string xValue, string xDataType)
        {
            if (xDataType == "Boolean")
            {
                bool bValue = Boolean.Parse(xValue);
                oPerson.SetPropertyValue(xName, bValue);
            }
            else
                oPerson.SetPropertyValue(xName, xValue);
        }

        /// <summary>
        /// builds DisplayName, FullName, and Initials based
        /// on input, fills grid values
        /// </summary>
        private void UpdateCalculatedFields()
        {
            DataGridViewRowCollection oRows = this.grdProperties.Rows;

            //get NamePrefix
            string xPrefix = oRows[1].Cells[1].FormattedValue.ToString();
            xPrefix = xPrefix == "" ? xPrefix : xPrefix + " ";

            //get FirstName
            string xFirstName = oRows[2].Cells[1].FormattedValue.ToString();
            xFirstName = xFirstName == "" ? xFirstName : xFirstName + " ";
            string xFirstInitial = xFirstName.Substring(0, Math.Min(1, xFirstName.Length));

            //get MiddleName
            string xMiddleName = oRows[3].Cells[1].FormattedValue.ToString();
            xMiddleName = xMiddleName == "" ? xMiddleName : xMiddleName + " ";
            string xMiddleInitial = "";
            if (xMiddleName != "")
                xMiddleInitial = xMiddleName.Substring(0, 1) + ". ";

            //get LastName
            string xLastName = oRows[4].Cells[1].FormattedValue.ToString();
            string xLastInitial = xLastName.Substring(0, Math.Min(1, xLastName.Length));

            //get NameSuffix
            string xNameSuffix = oRows[5].Cells[1].FormattedValue.ToString();
            xNameSuffix = xNameSuffix == "" ? xNameSuffix : ", " + xNameSuffix;

            //build FullName
            StringBuilder oSB = new StringBuilder();
            string xFullName = oSB.AppendFormat("{0}{1}{2}{3}{4}",
                xPrefix, xFirstName, xMiddleInitial, xLastName, xNameSuffix).ToString();

            //build Initials
            oSB = new StringBuilder();
            string xInitials = oSB.AppendFormat("{0}{1}{2}",
                xFirstInitial, xMiddleInitial.Replace(". ", ""), xLastInitial).ToString();

            //build DisplayName
            oSB = new StringBuilder();
            xNameSuffix = xNameSuffix == "" ? xNameSuffix : " " + xNameSuffix;
            string xDisplayName = oSB.AppendFormat("{0}{1}, {2}",
                xLastName, xNameSuffix.Replace(", ", ""), xFirstName.Replace(" ", "")).ToString();

            //if user has entered nothing in the name fields, don't recalculate
            if (xInitials == "")
                return;

            //edit grid cells with new values
            //set display name
            //GLOG : 8301 : jsw
            if (this.m_bDescriptIsEdited == false)
                oRows[6].Cells[1].Value = xDisplayName;

            oRows[7].Cells[1].Value = xFullName;
            oRows[9].Cells[1].Value = xInitials;
            
            //commit the edits
            grdProperties.EndEdit();
            m_oPropSource.EndEdit();
        }
        /// <summary>
        /// Update the DB with the changes the user has just made
        /// to the current License record
        /// </summary>
        private void UpdateLicense()
        {
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            //Get the license record that the user is editing.
            AttorneyLicense oCurLicense = (AttorneyLicense)m_oLicenses.
                ItemFromIndex(grdLicenses.CurrentRow.Index + 1);

            //Update the properties of the current License record to match the new data
            oCurLicense.Description = oCurCells[0].EditedFormattedValue.ToString();
            oCurLicense.License = oCurCells[1].EditedFormattedValue.ToString();

            //get the current license level IDs from the hashtable
            string xKey = "License" + (grdLicenses.CurrentRow.Index + 1).ToString();
            int[] iIDs = (int[])m_oLevelIDs[xKey];

            oCurLicense.L0 = iIDs[0];
            oCurLicense.L1 = iIDs[1];
            oCurLicense.L2 = iIDs[2];
            oCurLicense.L3 = iIDs[3];
            oCurLicense.L4 = iIDs[4];

            try
            {
                //Save the altered license record
                m_oLicenses.Save(oCurLicense);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// Creates a new license record with the data the user has provided
        /// </summary>
        private void AddLicense()
        {
            string xCurPersonID = this.grdPeople.CurrentRow.Cells[1].FormattedValue.ToString();
            
            if (m_oLicenses == null)
                m_oLicenses = new AttorneyLicenses();
            
            //Create a new license
            AttorneyLicense oNewLicense = (AttorneyLicense)m_oLicenses.Create
                (this.grdPeople.CurrentRow.Cells[1].FormattedValue.ToString());

            //Get the current cells with data for the new license
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            //Set the description for the new license from user input
            oNewLicense.Description = oCurCells[0].EditedFormattedValue.ToString();

            //Set the license name for the new license from user input
            oNewLicense.License = oCurCells[1].EditedFormattedValue.ToString();

            //get the current license level IDs from the hashtable
            string xKey = "License" + (grdLicenses.CurrentRow.Index + 1).ToString();
            int[] iIDs = (int[])m_oLevelIDs[xKey];

            oNewLicense.L0 = iIDs[0];
            oNewLicense.L1 = iIDs[1];
            oNewLicense.L2 = iIDs[2];
            oNewLicense.L3 = iIDs[3];
            oNewLicense.L4 = iIDs[4];

            try
            {
                //Save the new license
                m_oLicenses.Save(oNewLicense);

                //Store the ID of the new license in the grid
                oCurCells[7].Value = oNewLicense.ID;

                //commit the change
                grdLicenses.EndEdit();

                //reset new record flag
                m_bCurrentLicenseRowIsNewRecord = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException
                    (LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE); //display a message box instead and make the user handle it?
            }
        }
        /// <summary>
        /// Deletes the license record the user has chosen for deletion
        /// </summary>
        private void DeleteLicense()
        {
            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (oChoice == DialogResult.Yes)
            {
                //User has confirmed the delete, delete the record
                DataGridViewRow oCurRow = this.grdLicenses.CurrentRow;

                //Get the license to be deleted
                AttorneyLicense oCurLicense = (AttorneyLicense)m_oLicenses.
                    ItemFromID(oCurRow.Cells[7].FormattedValue.ToString());

                //Delete the license
                m_oLicenses.Delete(oCurLicense.ID); //Would set UsageState = 0 instead if it were exposed

                //Remove the row for the deleted license from the grid
                this.grdLicenses.Rows.Remove(oCurRow);
            }
            //unlock controls after successful delete
            EnableDesignatedControls(true); 
        }
        /// <summary>
        /// Tests to make sure the user is not leaving required data blank
        /// in edditing or creating a license
        /// </summary>
        /// <returns></returns>
        private bool CurLicenseRowIsValid()
        {
            //Get the current row
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            if (oCurCells[0].EditedFormattedValue.ToString() == "" ||
                oCurCells[1].EditedFormattedValue.ToString() == "" ||
                oCurCells[2].EditedFormattedValue.ToString() == "")
            {
                //Required fields are blank, the row is invalid
                this.IsValid = false;
                return false;
            }
            else
            {
                //The required fields are set, row is valid
                this.IsValid = true;
                return true;
            }
        }
        /// <summary>
        /// Select the first record in grdPeople matching
        /// the text typed into ttxtFindPerson by the user
        /// </summary>
        private void SelectMatchingRecord(string xSearchString)
        {
            // GLOG : 6027 : CEH
            if (xSearchString != "")
            {
                //User has entered a search string

                //Get the index of the first matching record
                //search by Display Name
                int iMatchIndex = this.grdPeople.FindRecord
                    (xSearchString, this.grdPeople.Columns[0].Name, false);

                //if not found, search by LoginID
                if (iMatchIndex == -1)
                {
                    iMatchIndex = this.grdPeople.FindRecord
                        (xSearchString, this.grdPeople.Columns[2].Name, false, true);
                }

                if (iMatchIndex > -1)
                {
                    //Match found

                    //Select the appropriate record
                    this.grdPeople.CurrentCell =
                        this.grdPeople.Rows[iMatchIndex].Cells[0];
                }
            }
        }
        /// <summary>
        /// Updates the primary office of the current person to match
        /// a change made by the user
        /// </summary>
        private void UpdatePrimaryOffice()
        {
            //Get current Person record ID stored in the properties grid
            Person oCurPerson = m_oCurBasePerson.RelatedPeople.ItemFromID(GetCurrentOfficePersonID());
            m_oCurBasePerson.DefaultOfficeRecordID = oCurPerson.ID1;

            try
            {
                this.m_oBasePeople.Save(m_oCurBasePerson);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        ///// <summary>
        ///// Updates the primary office of the current person to match
        ///// a change made by the user
        ///// </summary>
        //private void UpdatePrimaryOffice()
        //{
        //    //Get current Person record ID stored in the properties grid
        //    Person oCurPerson = m_oCurSecondaryOfficePersons.ItemFromID(GetCurrentPersonID());

        //    //Get the 4 digit ID1 of the current person
        //    string xID1;
        //    string xID2;
        //    String.SplitString(oCurPerson.ID, out xID1, out xID2, ".");
        //    int iID = Int32.Parse(xID1);

        //    try
        //    {
        //        //Change the LinkedPersonIDs of each of the people
        //        //records that will now be ascociated with the new
        //        //primary office record

        //        for (int i = 1; i <= m_oCurSecondaryOfficePersons.Count; i++)
        //        {
        //            //Get one of the records to update
        //            Person oPerson = m_oCurSecondaryOfficePersons.ItemFromIndex(i);

        //            //Set the new LinkedPersonID
        //            oPerson.LinkedPersonID = iID;

        //            //Save the record
        //            m_oCurSecondaryOfficePersons.Save(oPerson);
        //        }

        //        //Set the current Person record to the new primary office record
        //        oCurPerson.LinkedPersonID = 0;

        //        //Save the current Person record
        //        m_oCurSecondaryOfficePersons.Save(oCurPerson);

        //        //reset the ID value of grdPeople current row
        //        this.grdPeople.CurrentRow.Cells[1].Value = oCurPerson.ID;

        //        //accept changes - this will reset row state
        //        if (m_oLastPeopleRow != null)
        //            m_oLastPeopleRow.AcceptChanges();
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.DataException(
        //            LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
        //    }
        //}
        /// <summary>
        /// Displays a dialog box in which the user can edit an existing
        /// license or enter data for a new one.
        /// </summary>
        private void ShowSetLicenseJurisdictionDialog()
        {
            m_xGrdLicensesBeforeEditCellValue = this.grdLicenses.CurrentCell.FormattedValue.ToString();
            
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            //Create a new Dialog
            SetLicenseJurisdictionDialog oJDialog = new SetLicenseJurisdictionDialog();

            //Get the level of the deepest jurisdiction level that is defined
            int iCurLevelDepth = 0;

            for (int i = 2; i <= 6; i++)
                if (oCurCells[i].FormattedValue.ToString() != "")
                    iCurLevelDepth = i - 2;

            //retrieve the jurisdiction levels for the currently selected license row
            //they have been stored in a hashtable
            string xKey = "License" + (grdLicenses.CurrentRow.Index + 1).ToString();
            int[] iLevels = (int[])m_oLevelIDs[xKey];

            if (iCurLevelDepth != 0)
                //There is a jurisdiction defined for this record
                //Set the jurisdiction that will be selected in the dialog
                oJDialog.Value = iCurLevelDepth.ToString() + "."
                    + iLevels[iCurLevelDepth].ToString();

            DialogResult oResult = oJDialog.ShowDialog();

            if (oResult == DialogResult.OK)
            {
                //User did not cancel the dialog, insert the data they entered into the grid
                this.GetJurisdictionsData(oJDialog);

                //set change flag
                m_bLicenseChangesMade = true;
                //we have an uncommitted edit now, so disable navigation
                //it will be reenabled when row is validated
                this.EnableDesignatedControls(false);
            }
        }
       /// <summary>
        /// Inserts the data that the user has entered in the SetLicenseJurisdictionDialog
        /// into the grid and updates the DB
        /// </summary>
        private void GetJurisdictionsData(SetLicenseJurisdictionDialog oJD)
        {
            DataGridViewCellCollection oCurCells = this.grdLicenses.CurrentRow.Cells;

            //get selected jurisdiction IDs
            int[] iJurisdictions = oJD.Jurisdictions;

            //store IDs in hashtable
            string xKey = "License" + (grdLicenses.CurrentRow.Index + 1).ToString();
            m_oLevelIDs[xKey] = iJurisdictions;

            //fill grdJurisdiction cells
            for (int i = 0; i < iJurisdictions.Length; i++)
            {
                int iInd = i + 2;
                string xValue = "";
                int iJID = iJurisdictions[i];
                switch (i)
                {
                    case 0:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions0.ItemFromID(iJID)).Name;
                        break;
                    case 1:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions1.ItemFromID(iJID)).Name;
                        break;
                    case 2:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions2.ItemFromID(iJID)).Name;
                        break;
                    case 3:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions3.ItemFromID(iJID)).Name;
                        break;
                    case 4:
                        if (iJID != 0)
                            xValue = ((Jurisdiction)m_oJurisdictions4.ItemFromID(iJID)).Name;
                        break;
                    default:
                        break;
                }

                oCurCells[iInd].Value = xValue;
            }
        }
        /// <summary>
        /// handles uncommitted changes on close of form
        /// </summary>
        public override void SaveCurrentRecord()
        {
            //user has already deleted last last person record - nothing to save
            if (m_oLastPropRow != null && m_oLastPeopleRow != null)
            {
                //this event fires before cell or row validating events
                //make sure we have valid data for properties -- which 
                //requires at least a display name - updatecalulatedfields
                //will make one if there was new input
                if (m_oLastPeopleRow != null &&
                    m_oLastPeopleRow.RowState == DataRowState.Added)
                    UpdateCalculatedFields();

                if (!ValidatePeopleDisplayName())
                {
                    this.IsValid = false;
                    return;
                }
                else
                {
                    this.IsValid = true;

                    //commit any edits to grid & binding source
                    //this will flip row state to modified
                    grdProperties.EndEdit();
                    m_oPropSource.EndEdit();

                    if (m_oLastPropRow.RowState == DataRowState.Modified)
                    {
                        //update db record
                        UpdatePerson(GetCurrentOfficePersonID());
                        BindPropertyGrid(GetCurrentOfficePersonID());
                    }
                    //if (m_bOfficeRecordsAltered)
                        UpdateOfficePersonRecordsIfNecessary();
                }
            }
        }
        #endregion
        #region*************private functions****************
        /// <summary>
        /// validation for people grid values
        /// </summary>
        /// <returns></returns>
        private bool ValidatePeopleDisplayName()
        {
            if (grdPeople.CurrentCell.FormattedValue.ToString() == "")
            {
                //alert user
                MessageBox.Show(LMP.Resources.GetLangString("Error_EmptyValueNotAllowed"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            else
                return true;
        }
        /// <summary>
        /// returns field index of OfficeID field of Person
        /// </summary>
        /// <returns></returns>
        private int GetPersonOfficeFieldIndex()
        {
            //use any person - cycle through prop array to
            //get index of Office ID field
            Person oPropPerson = m_oBasePeople.ItemFromIndex(1);
            object[,] oProps = oPropPerson.ToArray();

            int iOffInd = -1;
            for (int i = 0; i < oProps.GetLength(0); i++)
            {
                if (oProps[i, 0].ToString() == "Office ID")
                {
                    iOffInd = i + 1;
                    break;
                }
            }
            return iOffInd;
        }

        /// <summary>
        /// GLOG : 2233 : JAB
        /// Determine if the current person is an unlinked person.
        /// </summary>
        /// <returns></returns>
        private bool IsCurrentPersonUnlinked()
        {
            DataTable oDT = ((DataTable)m_oPropSource.DataSource);
            DataRow[] oDR = oDT.Select("Property = 'Linked Person ID'");

            string xID = ((DataRow)oDR.GetValue(0)).ItemArray.GetValue(1).ToString();
            return (xID == "0");
        }

        /// <summary>
        /// retreives ID of current person stored in hidden field
        /// in property grid
        /// </summary>
        /// <returns></returns>
        private string GetCurrentOfficePersonID()
        {
            DataTable oDT = ((DataTable)m_oPropSource.DataSource);
            DataRow[] oDR = oDT.Select("Property = 'ID'");
            string xID = ((DataRow)oDR.GetValue(0)).ItemArray.GetValue(1).ToString();
            return xID;
        }

        /// <summary>
        /// returns the currently referenced office person
        /// </summary>
        private Person CurrentOfficePerson
        {
            get
            {
                return LocalPersons.GetPersonFromID(GetCurrentOfficePersonID());
            }
        }

        /// <summary>
        /// Return all Custom Field names as delimited string
        /// </summary>
        /// <returns></returns>
        private string GetCustomFieldList()
        {
            string xList = "";
            List<string> aList = LMP.Data.Application.GetPeopleCustomFieldList();
            foreach (string xField in aList)
            {
                xList += xField + "|";
            }
            return xList;
        }
        #endregion
        #region *************event handlers***************
        private void PeopleManager_Load(object sender, EventArgs e)
        {
            try
            {
                if (grdPeople.RowCount == 0)
                    this.DataBindGrdPeople();
                m_oSearchTimer.Tick += m_oSearchTimer_Tick;
                m_oSearchTimer.Interval = 300;
                m_oSearchTimer.Stop();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        void m_oSearchTimer_Tick(object sender, EventArgs e)
        {
            //Clear search string after time out
            m_xSearchString = "";
            m_oSearchTimer.Stop();
        }
        /// <summary>
        /// bind various grids after control has been loaded and docked on main
        /// form - this prevents refresh bug where record is displayed but grid
        /// display attribute settings are ignored when form is first loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {

                if (m_xCustomFields != this.GetCustomFieldList())
                {
                    //JTS 12/10/08: Custom Field definitions have changed
                    LocalPersons.ReloadCustomFields();
                    if (LMP.Data.LocalConnection.AdminConnection && LMP.MacPac.MacPacImplementation.IsFullLocal) //GLOG 8220
                    {
                        m_oPersons = new LocalPersons(LMP.Data.mpPeopleListTypes.LocalPublicPeople);
                        //Get a persons object consisting of all public
                        //records, excluding extra offices
                        m_oBasePeople = new LocalPersons(
                            LMP.Data.mpPeopleListTypes.LocalPublicPeople, "LinkedPersonID = 0");
                    }
                    else
                    {
                        m_oPersons = new LocalPersons(LMP.Data.mpPeopleListTypes.AllActivePeopleInDatabase);
                        //Get a persons object consisting of all public
                        //records, excluding extra offices
                        m_oBasePeople = new LocalPersons(
                            LMP.Data.mpPeopleListTypes.AllActivePeopleInDatabase, "LinkedPersonID = 0");
                    }
                    //Rebind property controls to reflect current properties
                    BindPropertyGrid(this.GetCurrentOfficePersonID());
                }

                if (grdPeople.Rows.Count > 0)
                {
                    //bind all the various grids
                    int iRow = grdPeople.CurrentRow != null ? grdPeople.CurrentRow.Index : 0;
                    string xID = grdPeople.Rows[iRow].Cells["ID"].FormattedValue.ToString();
                    SetupUIForPerson(LocalPersons.GetPersonFromID(xID));
                }
                //GLOG : 8333 : jsw
                //highlight cell for current user
                for (int i = 0; i < grdPeople.Rows.Count; i++)
                {
                    string xiD = (string)grdPeople.Rows[i].Cells[1].Value;
                    int iPos = xiD.IndexOf(".");
                    xiD = xiD.Substring(0, iPos);
                    if (xiD == LMP.Architect.Base.Application.CurrentUser.ID.ToString())
                    {
                        grdPeople.Rows[i].DefaultCellStyle.BackColor = Color.Yellow;
                        break;
                    }
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires if control is about to be unloaded by selecting another
        /// manager - calls SaveCurrentRecord to commit and save any 
        /// pending edits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdPeople_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //allow changes only to new row, ie, we don't allow name editing in
                //the offices column
                if (grdPeople.Focused)
                    grdPeople.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = 
                    !grdPeople.Rows[e.RowIndex].IsNewRow;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdPeople_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bChangesMade)
                {
                    //Update the person record that the 
                    //user has just finished editing
                    if (!ValidatePeopleDisplayName())
                    {
                        e.Cancel = true;
                        grdPeople.CancelEdit();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdPeople_CellValidated(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                //if (m_bOfficeRecordsAltered)
                //{
                    //There are unsaved edits to the secondary office
                    //records for the current person record- 
                    //update the secondary office records for the
                    //current person
                    //NOTE: we've removed the condition above
                    //to guarantee that secondary records
                    //are updated - there were bugs all over
                    //due to the fact that these records
                    //were not being saved - this is a temporary
                    //fix only, as this method is called way
                    //too often now.
                    //this.UpdateSecondaryPersonRecordsIfNecessary();
                //}
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles exception if newly added row deleted
        /// from grid via DeletePerson
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdPeople_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                if (e.Exception is IndexOutOfRangeException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdProperties_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //Get the current cell
                DataGridViewCell oCurCell = this.grdProperties.
                    Rows[e.RowIndex].Cells[e.ColumnIndex];

                if (m_bDataIsBound && oCurCell.FormattedValue.ToString()
                    != oCurCell.EditedFormattedValue.ToString())
                {
                    //User has made edits to the current record-
                    //Note that the DB should be updated
                    m_bChangesMade = true;

                    if (oCurCell.RowIndex == 11)
                    {
                        if (oCurCell.EditedFormattedValue.ToString() == "False")
                        {
                            DialogResult oResult = MessageBox.Show(
                                LMP.Resources.GetLangString("Msg_LicensesWillBeDeletedIfIsAttorney=False"),
                                LMP.ComponentProperties.ProductName,
                                MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Warning);

                            if (oResult == DialogResult.Cancel)
                            {
                                //Cancel the change of IsAttorney to false
                                oCurCell.Value = oCurCell.FormattedValue;
                                this.grdProperties.RefreshEdit();

                                e.Cancel = true;

                                //Don't update; the change was reversed
                                m_bChangesMade = false;
                            }
                        }
                        //bind license grid if IsAttorney has been flipped to true
                        else if (!m_bGrdLicensesIsBound && oCurCell.EditedFormattedValue.ToString() == "True")
                            this.BindLicenseGrid(grdPeople.CurrentRow.Cells["ID"].FormattedValue.ToString());
                    }
                    //GLOG : 8301 : jsw
                    if (oCurCell.RowIndex == 6)
                        this.m_bDescriptIsEdited = true;

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// enables tab navigation in data column only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_CellEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (grdProperties.Focused)
                {
                    if (grdProperties.CurrentCellAddress.X == 0 &&
                        grdProperties.CurrentCellAddress.Y == 0)
                        return;

                    //programmatically resetting current cell errors
                    //we can use sendkeys instead
                    if (e.ColumnIndex == 0)
                        System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void cboxOffices_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (m_bCboxOfficesIsBound && this.cboxOffices.SelectedIndex >= 0)
                {
                    if (m_bChangesMade)
                        // GLOG : 2233 : JAB
                        // Specify if the person is linked when updating.
                        //Update the person record that the 
                        //user has just finished editing
                        this.UpdatePerson(GetCurrentOfficePersonID());
                    
                    //DataBind grdProperties with the record
                    //corresponding to the office just selected
                    //in cboxOffices
                    string xID = this.cboxOffices.SelectedValue.ToString();
                    this.BindPropertyGrid(xID);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //private void tabPersonDetail_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //save any uncommitted prop edits
        //        grdProperties.EndEdit();
        //        m_oPropSource.EndEdit();
                
        //        if (m_oLastPropRow.RowState == DataRowState.Modified)  
        //            this.UpdatePerson(GetCurrentOfficePersonID());

        //        //if (m_bOfficeRecordsAltered)
        //        //{
        //            //Update the secondary office records for the
        //            //current person
        //            this.UpdateSecondaryPersonRecordsIfNecessary();
        //        //}
        //    }
        //    catch (System.Exception oE)
        //    {
        //        LMP.Error.Show(oE);
        //    }
        //}
        /// <summary>
        /// enables/disables certain controls
        /// while license tab is active
        /// </summary>
        /// <param name="bEnabled"></param>
        private void EnableDesignatedControls(bool bEnabled)
        {
            this.grdPeople.Enabled = bEnabled;
            this.tbtnDelete.Enabled = bEnabled;
            this.tbtnImport.Enabled = bEnabled;
            this.tbtnNew.Enabled = bEnabled;
            this.ttxtFindPerson.Enabled = bEnabled;
            this.tlblFindGroup.Enabled = bEnabled;
        }
        private void lstOffices_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            try
            {
                if (!m_bOfficesListIsBound)
                    return;

                //do not allow user to uncheck primary office
                //get office ID from value member of listbox
                int iCheckedID = int.Parse(lstOffices.SelectedValue.ToString());

                //get primary office ID for current person
                string xPersonID = GetCurrentOfficePersonID();
                Person oCurPerson = LocalPersons.GetPersonFromID(xPersonID);
                int iPrimaryOfficeID = oCurPerson.GetDefaultOfficeID();

                if (e.NewValue == CheckState.Unchecked &&
                    iCheckedID == iPrimaryOfficeID)
                {
                    //message user and return to original value
                    MessageBox.Show(LMP.Resources.GetLangString("Msg_CannotUnselectPrimaryOffice"),
                           LMP.ComponentProperties.ProductName,
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    e.NewValue = e.CurrentValue;
                    return;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                //do not allow delete if uncommitted edit
                //to grdLicenses exists
                if (grdLicenses.IsCurrentRowDirty)
                    return;

                this.DeletePerson();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void ttxtFindPerson_KeyUp(object sender, KeyEventArgs e)
        {
            
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    //don't attempt to set current cell
                    //if grids are empty, i.e. there are no records
                    if (grdPeople.Rows.Count == 0)
                        return;

                    //Give focus to grdProperties
                    this.grdProperties.Focus();

                    //Set the current cell in grdProperties
                    this.grdProperties.CurrentCell =
                        this.grdProperties.Rows[0].Cells[1];
                }
                else
                    //Select the first record matching
                    //the search string entered by the user
                    this.SelectMatchingRecord(this.ttxtFindPerson.Text);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                //do not allow new record if uncommitted edit
                //to grdLicenses exists
                if (grdLicenses.IsCurrentRowDirty)
                    return;

                //select the detail tab
                this.tabPersonDetail.SelectTab(0);
                this.AddPerson(); 
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnDeleteLicense_Click(object sender, EventArgs e)
        {
            try
            {
                //Test to make sure the user is deleting a valid license
                if (this.grdLicenses.CurrentRow.Cells[7].FormattedValue.ToString() != "")
                {
                    this.DeleteLicense();
                    return;
                }
                StringBuilder oSB = new StringBuilder();
                for (int i = 0; i < 7; i++)
                {
                    oSB.Append(grdLicenses.CurrentRow.Cells[i].FormattedValue.ToString());
                }
                if (oSB.ToString().Length > 0)
                {
                    grdLicenses.CancelEdit();
                    //glog 6253: Move CurrentCell first, to ensure Jurisdiction Chooser isn't displayed when row changes
                    grdLicenses.CurrentCell = grdLicenses.CurrentRow.Cells[0];
                    grdLicenses.Rows.RemoveAt(grdLicenses.CurrentRow.Index);
                    m_bLicenseChangesMade = false;
                    m_bCurrentLicenseRowIsNewRecord = false; //GLOG 5837: Reset New Record Flag
                    EnableDesignatedControls(true); //glog 6253: Re-enable navigation
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                //Store the current value of the cell being edited so it can be used after 
                //the new value is assigned - ignore if we're dealing with levels cells
                //glogal before edit value will be stored prior to selection of new item
                //in locations dialog
                if (e.ColumnIndex < 2)
                    m_xGrdLicensesBeforeEditCellValue = grdLicenses.CurrentCell.FormattedValue.ToString();
                
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
               
                if (!this.grdLicenses.CurrentRow.IsNewRow && m_xGrdLicensesBeforeEditCellValue !=
                        this.grdLicenses.CurrentCell.FormattedValue.ToString())
                    //Edits made to the current license, set flag
                    m_bLicenseChangesMade = true;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (!this.grdLicenses.CurrentRow.IsNewRow && !this.CurLicenseRowIsValid())
                {
                    //invalid row - cancel validation and warn user
                    //grdLicenses.CancelEdit();
                    e.Cancel = true;

                    MessageBox.Show(
                        LMP.Resources.GetLangString("Error_LicenseMustHaveDescriptionNumberAndLevel0Val"),
                        LMP.ComponentProperties.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_RowValidated(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_bLicenseChangesMade)
                {
                    //User has edited the current license, update the DB

                    if (m_bCurrentLicenseRowIsNewRecord)
                    {
                        //The user is adding a new license record, update the DB
                        this.AddLicense();
                        m_bCurrentLicenseRowIsNewRecord = false;
                    }

                    else
                        this.UpdateLicense();

                    m_bLicenseChangesMade = false;
                
                    //edit has been committed, reenable navigation
                    EnableDesignatedControls(true);
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                //Note that the user has added a row for a new record
                m_bCurrentLicenseRowIsNewRecord = true;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// disable navigation while there is a pending edit in grdLicenses
        /// this is due to bug where e.cancel = true in the RowValidating event
        /// handler causes app to hang if people grid is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdLicenses_CurrentCellDirtyStateChanged(object sender, System.EventArgs e)
        {
            try
            {
                //GLOG #4155 - dcf
                //EnableDesignatedControls(false);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex > 1 && !grdLicenses.CurrentRow.IsNewRow)
                    this.ShowSetLicenseJurisdictionDialog();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdLicenses_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.grdLicenses.CurrentCell.ColumnIndex > 1 && e.KeyCode == Keys.Tab)
                {
                    ////////this.grdLicenses.CurrentCell = this.grdLicenses.CurrentRow.Cells[6];
                    ////////e.SuppressKeyPress = true;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// suppresses navigation to license tab if current person
        /// is not attorney
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabPersonDetail_Selecting(object sender, TabControlCancelEventArgs e)
        {
            try
            {
                if (e.TabPageIndex == 2)
                {
                    // GLOG : 2233 : JAB
                    e.Cancel = !m_oCurBasePerson.IsAttorney;
                }
                else if (e.TabPageIndex == 0)
                {
                    if (this.cboxOffices.SelectedIndex == -1)
                        this.cboxOffices.SelectedValue = m_oCurBasePerson.DefaultOfficeRecordID.ToString() + ".0";
                }

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                //do not allow import if uncommitted edit
                //to grdLicenses exists
                if (grdLicenses.IsCurrentRowDirty)
                    return;

                MessageBox.Show("Under Construction",
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles updates to properties/licenses/offices
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oPeopleSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //get current BindingSource datarow object
                BindingSource oBS = (BindingSource)sender;

                if (m_oLastPeopleRow == null && oBS.Current != null)
                    m_oLastPeopleRow = ((DataRowView)oBS.Current).Row;
                
                if (grdPeople.CurrentRow == null)
                    return;

                //commit any pending property changes
                if (m_oLastPropRow != null)
                {
                    grdProperties.EndEdit();
                    m_oPropSource.EndEdit();

                    if (m_oLastPropRow.RowState == DataRowState.Modified)
                    {
                        UpdatePerson(GetCurrentOfficePersonID());
                        
                        //reset row state if necessary
                        if (m_oLastPeopleRow.RowState == DataRowState.Added)
                            m_oLastPeopleRow.AcceptChanges();
                    }
                }

                //reset last row variable
                if (oBS.Current != null)
                    m_oLastPeopleRow = ((DataRowView)oBS.Current).Row;

                //rebind properties grid
                if (oBS.Position > -1)
                {
                    //bind all the various grids
                    string xID = grdPeople.Rows[oBS.Position].Cells["ID"].FormattedValue.ToString();
                    SetupUIForPerson(LocalPersons.GetPersonFromID(xID));
                }
           }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// binds all grids, listboxes and combos for
        /// properties belonging to designated person
        /// </summary>
        /// <param name="xID"></param>
        private void BindControls(string xPersonID)
        {
            //Display the appropriate items in grdProperties
            this.BindPropertyGrid(xPersonID);

            //Display the appropriate items in lstOffices
            this.MarkupOfficesList();

            //Display the appropriate items in cboxOffices
            this.BindOfficeDropdown(xPersonID);

            //Display the Licenses for this record in grdLicenses
            this.BindLicenseGrid(xPersonID);
        }
        private void m_oPropSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //get current BindingSource datarow object
                BindingSource oBS = (BindingSource)sender;

                if (m_oLastPropRow == null)
                    m_oLastPropRow = ((DataRowView)oBS.Current).Row;

                //update format if datarow has been modified
                if (m_oLastPropRow.RowState == DataRowState.Modified)
                {
                    // GLOG : 2233 : JAB
                    // Update the person
                    this.UpdatePerson(GetCurrentOfficePersonID());
                }

                //reset last row variable
                if (oBS.Current != null)
                    m_oLastPropRow = ((DataRowView)oBS.Current).Row;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkIsPrimaryOffice_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.chkIsPrimaryOffice.CheckState == CheckState.Unchecked)
                {
                    this.chkIsPrimaryOffice.CheckState = CheckState.Checked;
                }
                else
                {
                    if (m_bDataIsBound)
                    {
                        //Update which person record is the primary
                        //office record for the current person entity-
                        //do this only if the user is the one making the change
                        this.UpdatePrimaryOffice();
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void grdProperties_Leave(object sender, EventArgs e)
        {
            try
            {
                //save any uncommitted property edits
                grdProperties.EndEdit();
                m_oPropSource.EndEdit();

                if (m_oLastPropRow.RowState == DataRowState.Modified)
                    this.UpdatePerson(GetCurrentOfficePersonID());
                
                //GLOG : 8301 : jsw
                this.m_bDescriptIsEdited = false;

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void lstOffices_Leave(object sender, EventArgs e)
        {
            try
            {
                //save changes to offices
                UpdateOfficePersonRecordsIfNecessary();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        //GLOG #3937 - ceh
        private void grdPeople_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Tab:
                    case Keys.Enter:
                    case Keys.Down:
                    case Keys.Left:
                    case Keys.Up:
                    case Keys.Right:
                        break;
                    default:
                        string xKey = e.KeyCode.ToString();
                        //Ignore non-input keys
                        if (xKey.Length == 1)
                        {
                            m_oSearchTimer.Stop();
                            m_oSearchTimer.Start();
                            m_xSearchString += xKey;
                            //Select the first record matching
                            //the search string entered by the user
                            this.SelectMatchingRecord(m_xSearchString);
                            e.Handled = true;
                        }
                        break;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        //GLOG : 8241 : JSW
        //tabbing moves from row to row for exising records
        private void grdPeople_TabPressed(object sender, TabPressedEventArgs e)
        {

            int iRowIndex;
            try
            {
                iRowIndex = this.grdPeople.CurrentRow.Index;
                if ((grdPeople.Rows.Count - 1) > iRowIndex)
                    this.grdPeople.CurrentCell = this.grdPeople.Rows[iRowIndex + 1].Cells[0];
                else
                    this.grdPeople.CurrentCell = this.grdPeople.Rows[0].Cells[0];
            }
            catch { }
  
        }

    }
}

