using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.Administration.Controls
{
    public partial class FindDialogForm : Form
    {
        #region ***************fields******************
        string m_xSearchString = "";
        bool m_bCaseSensitive = true;
        #endregion
        #region ***************constructors****************

        public FindDialogForm()
        {
            InitializeComponent();
        }
        #endregion
        #region ***************properties******************

        /// <summary>
        /// Returns the search string entered by the user.
        /// </summary>
        public string SearchString
        {
            get { return m_xSearchString; } 
        }

        /// <summary>
        /// Returns true if the user has indicated a
        /// case sensative search, false if not.
        /// </summary>
        public bool CaseSensitive
        {
            get { return m_bCaseSensitive; }
        }
        #endregion
        #region ***************event handlers******************
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                //Store the search string entered by the user.
                m_xSearchString = this.txtFind.Text;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void chkCaseSensitive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //Store whether the user intends a case sensative search.
                m_bCaseSensitive = this.chkCaseSensitive.Checked;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
    }
}