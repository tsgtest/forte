namespace LMP.Administration.Controls
{
    partial class InterrogatoriesManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InterrogatoriesManager));
            this.scGroups = new System.Windows.Forms.SplitContainer();
            this.grdTypes = new LMP.Controls.DataGridView();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tblTypes = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnImport = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnTranslate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tlblFindGroup = new System.Windows.Forms.ToolStripLabel();
            this.ttxtFindType = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.lblGroups = new System.Windows.Forms.Label();
            this.grdStyles = new LMP.Controls.DataGridView();
            this.lblMembers = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scGroups)).BeginInit();
            this.scGroups.Panel1.SuspendLayout();
            this.scGroups.Panel2.SuspendLayout();
            this.scGroups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTypes)).BeginInit();
            this.tsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdStyles)).BeginInit();
            this.SuspendLayout();
            // 
            // scGroups
            // 
            this.scGroups.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.scGroups.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.scGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scGroups.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.scGroups.Location = new System.Drawing.Point(0, 0);
            this.scGroups.Name = "scGroups";
            this.scGroups.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scGroups.Panel1
            // 
            this.scGroups.Panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scGroups.Panel1.Controls.Add(this.grdTypes);
            this.scGroups.Panel1.Controls.Add(this.tsMain);
            this.scGroups.Panel1.Controls.Add(this.lblGroups);
            // 
            // scGroups.Panel2
            // 
            this.scGroups.Panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.scGroups.Panel2.Controls.Add(this.grdStyles);
            this.scGroups.Panel2.Controls.Add(this.lblMembers);
            this.scGroups.Size = new System.Drawing.Size(635, 614);
            this.scGroups.SplitterDistance = 293;
            this.scGroups.SplitterWidth = 2;
            this.scGroups.TabIndex = 0;
            this.scGroups.TabStop = false;
            // 
            // grdTypes
            // 
            this.grdTypes.AllowUserToAddRows = false;
            this.grdTypes.AllowUserToDeleteRows = false;
            this.grdTypes.AllowUserToResizeRows = false;
            this.grdTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdTypes.BackgroundColor = System.Drawing.Color.White;
            this.grdTypes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdTypes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdTypes.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdTypes.IsDirty = false;
            this.grdTypes.Location = new System.Drawing.Point(0, 30);
            this.grdTypes.MultiSelect = false;
            this.grdTypes.Name = "grdTypes";
            this.grdTypes.RowHeadersVisible = false;
            this.grdTypes.RowHeadersWidth = 25;
            this.grdTypes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdTypes.Size = new System.Drawing.Size(631, 259);
            this.grdTypes.SupportingValues = "";
            this.grdTypes.TabIndex = 17;
            this.grdTypes.Tag2 = null;
            this.grdTypes.Value = null;
            this.grdTypes.TabPressed += new LMP.Controls.TabPressedHandler(this.grdTypes_TabPressed);
            this.grdTypes.ValueChanged += new LMP.Controls.ValueChangedHandler(this.grdTypes_ValueChanged);
            this.grdTypes.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdTypes_CellValidating);
            this.grdTypes.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdTypes_ColumnAdded);
            this.grdTypes.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.grdTypes_RowValidating);
            this.grdTypes.Leave += new System.EventHandler(this.grdTypes_Leave);
            // 
            // tsMain
            // 
            this.tsMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsMain.AutoSize = false;
            this.tsMain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tblTypes,
            this.toolStripSeparator8,
            this.tbtnNew,
            this.toolStripSeparator2,
            this.tbtnDelete,
            this.toolStripSeparator11,
            this.tbtnImport,
            this.toolStripSeparator3,
            this.tbtnTranslate,
            this.toolStripSeparator4,
            this.tlblFindGroup,
            this.ttxtFindType,
            this.toolStripSeparator10});
            this.tsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsMain.Location = new System.Drawing.Point(2, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMain.Size = new System.Drawing.Size(632, 31);
            this.tsMain.TabIndex = 18;
            // 
            // tblTypes
            // 
            this.tblTypes.AutoSize = false;
            this.tblTypes.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblTypes.Name = "tblTypes";
            this.tblTypes.Size = new System.Drawing.Size(75, 22);
            this.tblTypes.Text = "Types";
            this.tblTypes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnNew
            // 
            this.tbtnNew.AutoSize = false;
            this.tbtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnNew.Image = ((System.Drawing.Image)(resources.GetObject("tbtnNew.Image")));
            this.tbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnNew.Name = "tbtnNew";
            this.tbtnNew.Size = new System.Drawing.Size(68, 22);
            this.tbtnNew.Text = "&New...";
            this.tbtnNew.Click += new System.EventHandler(this.tbtnNew_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(53, 28);
            this.tbtnDelete.Text = "&Delete...";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 31);
            this.toolStripSeparator11.Visible = false;
            // 
            // tbtnImport
            // 
            this.tbtnImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnImport.Image = ((System.Drawing.Image)(resources.GetObject("tbtnImport.Image")));
            this.tbtnImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnImport.Name = "tbtnImport";
            this.tbtnImport.Size = new System.Drawing.Size(56, 28);
            this.tbtnImport.Text = "&Import...";
            this.tbtnImport.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            this.toolStripSeparator3.Visible = false;
            // 
            // tbtnTranslate
            // 
            this.tbtnTranslate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnTranslate.Image = ((System.Drawing.Image)(resources.GetObject("tbtnTranslate.Image")));
            this.tbtnTranslate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnTranslate.Name = "tbtnTranslate";
            this.tbtnTranslate.Size = new System.Drawing.Size(67, 28);
            this.tbtnTranslate.Text = "&Translate...";
            this.tbtnTranslate.Visible = false;
            this.tbtnTranslate.Click += new System.EventHandler(this.tbtnTranslate_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            this.toolStripSeparator4.Visible = false;
            // 
            // tlblFindGroup
            // 
            this.tlblFindGroup.AutoSize = false;
            this.tlblFindGroup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tlblFindGroup.Image = ((System.Drawing.Image)(resources.GetObject("tlblFindGroup.Image")));
            this.tlblFindGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tlblFindGroup.Name = "tlblFindGroup";
            this.tlblFindGroup.Size = new System.Drawing.Size(50, 22);
            this.tlblFindGroup.Text = "&Find:";
            this.tlblFindGroup.Visible = false;
            // 
            // ttxtFindType
            // 
            this.ttxtFindType.Name = "ttxtFindType";
            this.ttxtFindType.Size = new System.Drawing.Size(250, 31);
            this.ttxtFindType.Visible = false;
            this.ttxtFindType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ttxtFindGroup_KeyUp);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 31);
            // 
            // lblGroups
            // 
            this.lblGroups.AutoSize = true;
            this.lblGroups.BackColor = System.Drawing.Color.Transparent;
            this.lblGroups.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroups.Location = new System.Drawing.Point(3, 2);
            this.lblGroups.Name = "lblGroups";
            this.lblGroups.Size = new System.Drawing.Size(53, 16);
            this.lblGroups.TabIndex = 19;
            this.lblGroups.Text = "Groups";
            // 
            // grdStyles
            // 
            this.grdStyles.AllowUserToAddRows = false;
            this.grdStyles.AllowUserToResizeRows = false;
            this.grdStyles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdStyles.BackgroundColor = System.Drawing.Color.White;
            this.grdStyles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdStyles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdStyles.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdStyles.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdStyles.IsDirty = false;
            this.grdStyles.Location = new System.Drawing.Point(0, 30);
            this.grdStyles.MultiSelect = false;
            this.grdStyles.Name = "grdStyles";
            this.grdStyles.ReadOnly = true;
            this.grdStyles.RowHeadersVisible = false;
            this.grdStyles.RowHeadersWidth = 25;
            this.grdStyles.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdStyles.Size = new System.Drawing.Size(631, 279);
            this.grdStyles.SupportingValues = "";
            this.grdStyles.TabIndex = 21;
            this.grdStyles.Tag2 = null;
            this.grdStyles.Value = null;
            this.grdStyles.ValueChanged += new LMP.Controls.ValueChangedHandler(this.grdStyles_ValueChanged);
            this.grdStyles.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdStyles_ColumnAdded);
            this.grdStyles.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdStyles_DataError);
            this.grdStyles.Leave += new System.EventHandler(this.grdStyles_Leave);
            this.grdStyles.Validated += new System.EventHandler(this.grdStyles_Validated);
            // 
            // lblMembers
            // 
            this.lblMembers.AutoSize = true;
            this.lblMembers.BackColor = System.Drawing.Color.Transparent;
            this.lblMembers.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMembers.Location = new System.Drawing.Point(3, 7);
            this.lblMembers.Name = "lblMembers";
            this.lblMembers.Size = new System.Drawing.Size(41, 14);
            this.lblMembers.TabIndex = 20;
            this.lblMembers.Text = "Styles";
            // 
            // InterrogatoriesManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.scGroups);
            this.Name = "InterrogatoriesManager";
            this.Size = new System.Drawing.Size(635, 614);
            this.Load += new System.EventHandler(this.InterrogatoriesManager_Load);
            this.Leave += new System.EventHandler(this.InterrogatoriesManager_Leave);
            this.Validated += new System.EventHandler(this.InterrogatoriesManager_Validated);
            this.scGroups.Panel1.ResumeLayout(false);
            this.scGroups.Panel1.PerformLayout();
            this.scGroups.Panel2.ResumeLayout(false);
            this.scGroups.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scGroups)).EndInit();
            this.scGroups.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTypes)).EndInit();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdStyles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer scGroups;
        private System.Windows.Forms.Label lblGroups;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbtnTranslate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private LMP.Controls.DataGridView grdTypes;
        private System.Windows.Forms.Label lblMembers;
        private LMP.Controls.DataGridView grdStyles;
        private System.Windows.Forms.ToolStripLabel tlblFindGroup;
        private System.Windows.Forms.ToolStripTextBox ttxtFindType;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel tblTypes;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton tbtnNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton tbtnImport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;

    }
}
