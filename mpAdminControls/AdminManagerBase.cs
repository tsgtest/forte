using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace LMP.Administration.Controls
{
    public delegate void AfterControlLoadedHandler(object sender, EventArgs e);
    public delegate void BeforeControlUnLoadedHandler(object sender, EventArgs e);
    public partial class AdminManagerBase : LMP.Controls.ManagerBase
    {
        SqlConnection m_oCnn;
        bool m_bIsValid = true;
        public event AfterControlLoadedHandler AfterControlLoaded;
        public event BeforeControlUnLoadedHandler BeforeControlUnloaded;

        public AdminManagerBase()
        {
            InitializeComponent();
        }

        internal SqlConnection DatabaseConnection
        {
            get { return m_oCnn; }
            set { m_oCnn = value; }
        }

        public virtual bool IsValid
        {
            get { return m_bIsValid; }
            set { m_bIsValid = value; }
        }
        public virtual void SaveCurrentRecord(bool bPromptIfNecessary)
        {
        }
        public virtual void OnAfterControlLoaded(object sender, EventArgs e)
        {
            if (this.AfterControlLoaded != null)
                this.AfterControlLoaded(sender, e);
        }
        public virtual void OnBeforeControlUnloaded(object sender, EventArgs e)
        {
            if (this.BeforeControlUnloaded != null)
                this.BeforeControlUnloaded(sender, e);
        }
    }
}
