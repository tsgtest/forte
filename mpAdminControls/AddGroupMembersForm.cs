using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class AddGroupMembersForm : Form
    {
        #region *********************fields*********************
        ArrayList m_oPeopleIDsToAdd;
        string m_xPeopleIDsToAdd;

        // GLOG : 3139 : JAB
        StringBuilder m_sbTypedName = new StringBuilder();
        Timer m_ClearTypedNameTimer = null;

        #endregion
        #region *********************properties*********************
        /// <summary>
        /// Gets the comma delimited string of people IDs 
        /// representative of the people checked in clbPeopleToAdd.
        /// </summary>
        public string PeopleIDsToAdd
        {
            get { return m_xPeopleIDsToAdd; }
        }
        #endregion
        #region *********************constructors*********************
        public AddGroupMembersForm()
        {
            InitializeComponent();

            //initialize the ArrayList for checked person IDs.
            m_oPeopleIDsToAdd = new ArrayList();
        }
        #endregion
        #region *********************methods**********************

        /// <summary>
        /// Get a Persons object of all public people and bind its data 
        /// to the grdPeople DataGridView.
        /// </summary>
        private void DataBindGrdPeople()
        {
            try
            {
                //Get a Persons object that includes all public people
                //records.
                //GLOG 8220: Read people from ForteLocalPeople.mdb in Local mode
                LocalPersons oAllPeopleList = null;
                if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                {
                    oAllPeopleList = new LocalPersons(mpPeopleListTypes.LocalPublicPeople,
                         "UserID <> ''");
                }
                else
                {
                    oAllPeopleList =  new LocalPersons(mpPeopleListTypes.AllActivePeopleInDatabase,
                         "UserID <> ''");
                }

                //Get a DataSet of the people in oAllPeopleList with only
                //the display fields.
                DataSet oAllPeopleDataSet = oAllPeopleList.ToDataSet(true);

                // GLOG : 3139 : JAB
                // Use a binding source class to allow navigation to a typed-in name.
                BindingSource oBindingSource = new BindingSource();
                //Bind the DataSet to the DataGridView.
                oBindingSource.DataSource = oAllPeopleDataSet.Tables[0];

                //Bind the DataSet to the DataGridView.
                grdPeople.DataSource = oBindingSource;

                //Set the display properties of the columns automatically
                //added to grdPeople.
                DataGridViewColumnCollection oCols = this.grdPeople.Columns;
                oCols["ID"].Visible = false;
                oCols["DisplayName"].AutoSizeMode = 
                        DataGridViewAutoSizeColumnMode.Fill;
                oCols["DisplayName"].ReadOnly = true;
            }
            catch(System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(LMP.Resources.
                        GetLangString("Error_CouldNotDataBindPeople"), oE);
            }
        }

        /// <summary>
        /// Create a comma delimited string of the checked person IDs.
        /// </summary>
        private void CreateCheckedPersonIDsString()
        {
            StringBuilder oIDsBuilder = new StringBuilder();

            //Build a comma delimited string from the IDs of the 
            //selected people and set the.
            foreach (object oID in m_oPeopleIDsToAdd)
                oIDsBuilder.AppendFormat(",{0}", (string)oID);

            //Set this member field to equal the newly built string.
            m_xPeopleIDsToAdd = oIDsBuilder.ToString();
        }

        /// <summary>
        /// Update the ArrayList of checked person IDs when user checks 
        /// or unchecks one.
        /// </summary>
        private void UpdateIDsArrayList()
        {
            DataGridViewCellCollection oCells = this.grdPeople.CurrentRow.Cells;

            //Store the current person ID.
            object oCurrentID = oCells["ID"].FormattedValue;

            //If the current person is checked and is not already in the 
            //ArrayList of people to add, add the persons ID to the list 
            //of checked IDs.
            if (((bool)oCells["Add"].FormattedValue) &&
                    !m_oPeopleIDsToAdd.Contains(oCurrentID))
                m_oPeopleIDsToAdd.Add(oCurrentID);

            //If the current person is unchecked and is in the
            //ArrayList of checked IDs, remove the ID from the list.
            else if (((!(bool)oCells["Add"].FormattedValue) &&
                    m_oPeopleIDsToAdd.Contains(oCurrentID)))
                m_oPeopleIDsToAdd.Remove(oCurrentID);
        }

        #endregion
        #region *********************event handlers*********************

        /// <summary>
        /// Data bind the gird. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddPeopleToGroupForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.DataBindGrdPeople();

                // GLOG : 3139 : JAB
                // Set focus to the grid.
                this.grdPeople.Select();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Close the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// Create a string out of the checked IDs and close the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.CreateCheckedPersonIDsString();

                this.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// Refresh the ArrayList of checked person IDs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdPeople_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.UpdateIDsArrayList();
            }
            catch(System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        // GLOG : 3139 : JAB
        // Append the typed character to the search string and search for
        // the matching item.
        private void UpdateNameSearch(char cTypedChar)
        {
            if (m_ClearTypedNameTimer == null)
            {
                m_ClearTypedNameTimer = new Timer();
                m_ClearTypedNameTimer.Interval = 1000;
                m_ClearTypedNameTimer.Tick += new EventHandler(m_ClearTypedNameTimer_Tick);
            }

            // Restart the timer.
            m_ClearTypedNameTimer.Stop();
            m_ClearTypedNameTimer.Start();

            lock (m_sbTypedName)
            {
                switch(cTypedChar)
                {
                    case (char)Keys.Back:
                        if (m_sbTypedName.Length > 0)
                        { 
                            m_sbTypedName.Length--; 
                        } 
                        break;

                    default:
                        m_sbTypedName.Append(cTypedChar);
                        break;
                }

                this.lblSearchString.Text = "Search: " + m_sbTypedName.ToString();
                SelectFirstMatchingName(m_sbTypedName);
            }
        }

        // GLOG : 3139 : JAB
        // Clear the search string 3 seconds after the last character
        // for the search string is typed.
        void m_ClearTypedNameTimer_Tick(object sender, EventArgs e)
        {
            lock (m_sbTypedName)
            {
                this.m_sbTypedName.Length = 0;
                this.lblSearchString.Text = m_sbTypedName.ToString();
                this.m_ClearTypedNameTimer.Stop();
            }
        }

        // GLOG : 3139 : JAB
        // Set the selected row in the grid to the item matching the search string.
        private void SelectFirstMatchingName(StringBuilder sbNameToMatch)
        {
            BindingSource oBindingSource = (BindingSource)this.grdPeople.DataSource;
            DataTable dtPeople = (DataTable)oBindingSource.DataSource;
            DataRow[] oRows = dtPeople.Select("DisplayName LIKE '" + sbNameToMatch.ToString() + "%'");

            if (oRows.Length > 0)
            {
                int iMatchPosition = oBindingSource.Find("DisplayName", (string)(oRows[0]["DisplayName"]));
                oBindingSource.Position = iMatchPosition;
            }
        }

        // GLOG : 3139 : JAB
        // Handle key press such that the appropriate keys are applied to the search
        // for the matching name.
        private void HandleKeyPress(KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return || e.KeyChar == (char)Keys.Tab)
            {
                e.Handled = false;
            }
            else
            {
                UpdateNameSearch(e.KeyChar);
                e.Handled = true;
            }
        }

        // GLOG : 3139 : JAB
        // Navigate to the first name that matches a typed in name.
        private void grdPeople_KeyPress(object sender, KeyPressEventArgs e)
        {
            HandleKeyPress(e);
        }

        // GLOG : 3139 : JAB
        // Handle key press for the form.
        private void AddGroupMembersForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            HandleKeyPress(e);
        }

        // GLOG : 3139 : JAB
        // Handle key press for the control.
        private void btnOK_KeyPress(object sender, KeyPressEventArgs e)
        {
            HandleKeyPress(e);
        }

        // GLOG : 3139 : JAB
        // Handle key press for the control.
        private void btnCancel_KeyPress(object sender, KeyPressEventArgs e)
        {
            HandleKeyPress(e);
        }
    }
}