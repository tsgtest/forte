namespace LMP.Administration.Controls
{
    partial class SupportedLanguagesManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupportedLanguagesManager));
            this.grdSupportedLanguages = new System.Windows.Forms.DataGridView();
            this.tsSupportedLanguages = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.grdSupportedLanguages)).BeginInit();
            this.tsSupportedLanguages.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdSupportedLanguages
            // 
            this.grdSupportedLanguages.AllowUserToResizeRows = false;
            this.grdSupportedLanguages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdSupportedLanguages.BackgroundColor = System.Drawing.Color.White;
            this.grdSupportedLanguages.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grdSupportedLanguages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSupportedLanguages.GridColor = System.Drawing.SystemColors.ControlLight;
            this.grdSupportedLanguages.Location = new System.Drawing.Point(0, 25);
            this.grdSupportedLanguages.MultiSelect = false;
            this.grdSupportedLanguages.Name = "grdSupportedLanguages";
            this.grdSupportedLanguages.RowHeadersVisible = false;
            this.grdSupportedLanguages.RowHeadersWidth = 25;
            this.grdSupportedLanguages.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grdSupportedLanguages.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdSupportedLanguages.Size = new System.Drawing.Size(692, 512);
            this.grdSupportedLanguages.TabIndex = 26;
            this.grdSupportedLanguages.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdSupportedLanguages_ColumnAdded);
            this.grdSupportedLanguages.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSupportedLanguages_RowValidated);
            this.grdSupportedLanguages.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdSupportedLanguages_KeyDown);
            // 
            // tsSupportedLanguages
            // 
            this.tsSupportedLanguages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tsSupportedLanguages.AutoSize = false;
            this.tsSupportedLanguages.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsSupportedLanguages.Dock = System.Windows.Forms.DockStyle.None;
            this.tsSupportedLanguages.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsSupportedLanguages.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator5,
            this.tbtnDelete,
            this.toolStripSeparator6});
            this.tsSupportedLanguages.Location = new System.Drawing.Point(0, 2);
            this.tsSupportedLanguages.Name = "tsSupportedLanguages";
            this.tsSupportedLanguages.Size = new System.Drawing.Size(692, 23);
            this.tsSupportedLanguages.TabIndex = 28;
            this.tsSupportedLanguages.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(130, 20);
            this.toolStripLabel1.Text = "Supported Languages";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 23);
            // 
            // tbtnDelete
            // 
            this.tbtnDelete.AutoSize = false;
            this.tbtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDelete.Image")));
            this.tbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDelete.Name = "tbtnDelete";
            this.tbtnDelete.Size = new System.Drawing.Size(53, 22);
            this.tbtnDelete.Text = "&Delete...";
            this.tbtnDelete.ToolTipText = "Delete selected language";
            this.tbtnDelete.Click += new System.EventHandler(this.tbtnDelete_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 23);
            // 
            // SupportedLanguagesManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.grdSupportedLanguages);
            this.Controls.Add(this.tsSupportedLanguages);
            this.Name = "SupportedLanguagesManager";
            this.Size = new System.Drawing.Size(692, 537);
            this.Load += new System.EventHandler(this.LocationsManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdSupportedLanguages)).EndInit();
            this.tsSupportedLanguages.ResumeLayout(false);
            this.tsSupportedLanguages.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.DataGridView grdSupportedLanguages;
        private System.Windows.Forms.ToolStrip tsSupportedLanguages;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tbtnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;

    }
}
