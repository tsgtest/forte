using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using LMP.Controls;
using LMP.Data;
using LMP.Architect.Api;
using Infragistics.Win.UltraWinTree;

namespace LMP.Administration.Controls
{
    public partial class AuthorPreferencesManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region *********************enumerations*********************
        private enum Images
        {
            FolderClosed = 0,
            FolderOpen = 1,
            DocumentSegment = 2,
            SearchResults = 3,
            Prefill = 4,
            ParagraphTextSegment = 5,
            ComponentSegment = 6,
            StyleSheetSegment = 7,
            OwnerFolder = 8,
            LeftArrow = 9,
            AnswerFileSegment = 10,
            DataPacket = 11,
            SentenceTextSegment = 12
        }

        public enum Modes
        {
            Admin = 0,
            User = 1,
            Designer = 2 //GLOG 6148
        }

        public enum mpFolderMemberTypes : short
        {
            UserSegment = 99,
            AdminSegment = 100,
            VariableSet = 104
        }

        #endregion
        #region *********************fields*********************
        private string m_xCurFolderID = "0";
        private string m_xCurSegmentID = "";
        private ArrayList m_oCurOffices = null;
        private ArrayList m_oCurGroups = null;
        private ArrayList m_oCurPeople = null;
        private KeySet m_oPrefKeyset = null;
        private bool m_bOwnersLoading = false;
        private bool m_bLanguagesLoading = false;
        private Modes m_iMode = Modes.Admin;
        private int m_iUserID = 0;
        private const string mpPathSeparator = "\\\\";
        private string m_xCurLanguage = "";
        private ISegmentDef m_oCurSegDef = null;
        private XmlDocument m_oCurSegXML = null;
        private string m_xCurSegObjectData = "";
        private XmlNamespaceManager m_oNamespaceManager = null;
        private string m_xNamespacePrefix = "";
        private int m_iCulture = 1033;
        private string m_xCulture = "English";
        private ArrayList m_aRelatedSegments = new ArrayList();
        private string m_xTopLevelSegmentID = "";
        private Hashtable m_oChooserHash = new Hashtable();
        private Hashtable m_oPreferencesHash = new Hashtable();
        private Hashtable m_oControlsHash = new Hashtable();
        private bool m_bChangingRelated = false;
        #endregion
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        public Size ButtonSize
        {
            get
            {
                return this.btnSave.Size;
            }
        }
        public int ButtonTop
        {
            get { return this.scAuthorPrefs.Top + this.btnSave.Top; }
        }
        #endregion
        #region *********************constructor*********************
        public AuthorPreferencesManager()
        {
            InitializeComponent();
        }
        #endregion
        #region *********************properties*********************
        public Modes Mode
        {
            get { return m_iMode; }
            set
            {
                if (m_iMode != value)
                {
                    m_iMode = value;
                    this.AdjustForMode();
                }
            }
        }
        /// <summary>
        /// gets/sets if the tree is displayed
        /// </summary>
        public bool DisplayTree
        {
            get
            {
                return !this.scAuthorPrefs.Panel1Collapsed;
            }
            set
            {
                this.scAuthorPrefs.Panel1Collapsed = !value;

                if (this.Parent != null)
                {
                    if (value)
                        ((Form)this.Parent).Width = 697;
                    else
                        ((Form)this.Parent).Width = 500;
                }
            }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// handles initial control setup
        /// </summary>
        private void Setup()
        {
            if (m_iMode == Modes.User)
            {
                //enable owners list - in admin mode, folders are static and owners
                //list is dependent on folder, so latter is enabled only when a
                //segment is selected - in user mode, owner list is static and folders
                //are dependent on selected owner, so owner list is always enabled
                this.cmbOwner.Enabled = true;
                this.lblOwner.Enabled = true;

                //set user id
                m_iUserID = LMP.Data.Application.User.ID;
                int iDefaultUserID = m_iUserID;
                if (LMP.Data.Application.User.PersonObject.DefaultOfficeRecordID > 0)
                    iDefaultUserID = LMP.Data.Application.User.PersonObject.DefaultOfficeRecordID;

                //load authors list
                m_bOwnersLoading = true;
                ArrayList oAuthors = SimpleDataCollection.GetArray(
                    "spUserAuthorPreferencePermissions", new object[] { m_iUserID });
                this.cmbOwner.SetList(oAuthors);
                this.cmbOwner.Value = iDefaultUserID.ToString() + ".0";
                m_bOwnersLoading = false;
            }
            else if (m_iMode == Modes.Designer)
            {
                //set user id
                m_iUserID = LMP.Data.Application.User.ID;
            }
            this.pnlRelated.Visible = false;
            this.lblNoPreferences.BringToFront();
            this.cmbRelated.Left = this.lblRelated.Left + this.lblRelated.Width + 15;
            this.cmbRelated.Width = this.pnlRelated.Width - (this.cmbRelated.Left + (int)(10 * LMP.OS.GetScalingFactor()));
            this.lblPrefSep.Top = cmbRelated.Top + cmbRelated.Height + 10;
            this.pnlRelated.Height = this.lblPrefSep.Top + this.lblPrefSep.Height + (int)(3 * LMP.OS.GetScalingFactor());
            //hide language control if project doesn't support multiple languages
            string[] aLanguages = LMP.Data.Application.SupportedLanguagesString.Split(
                LMP.StringArray.mpEndOfField);
            if (aLanguages.Length < 2 && m_iMode == Modes.User)
            {
                this.lblLanguage.Visible = false;
                this.cmbLanguage.TabStop = false;
                this.cmbLanguage.Visible = false;
                this.gbxPreferences.Top -= 41;
                this.gbxPreferences.Height += 36;
            }
            //GLOG 6031: Added Find control
            this.ftvSegments.OwnerID = m_iUserID;
            FolderTreeView.mpFolderTreeViewOptions iOptions = FolderTreeView.mpFolderTreeViewOptions.AdminFolders | FolderTreeView.mpFolderTreeViewOptions.PreferenceSegmentsOnly;
            //GLOG 6148: If in Designer and not AdminMode, user must be a Segment Designer
            if (m_iMode == Modes.Designer && LMP.Data.Application.AdminMode == false)
            {
                iOptions = iOptions | FolderTreeView.mpFolderTreeViewOptions.UserFolders | FolderTreeView.mpFolderTreeViewOptions.DesignerSegments;
            }
            this.ftvSegments.DisplayOptions = iOptions;
            this.ftvSegments.AllowSearching = true;
            this.ftvSegments.Expansion = FolderTreeView.mpFolderTreeViewExpansion.ExpandTopLevel;
            this.ftvSegments.ExecuteFinalSetup();
            this.Refresh();

            ////GLOG 7286: Resize split container to match parent
            this.scAuthorPrefs.Width = this.Width - this.scAuthorPrefs.Left;
            this.scAuthorPrefs.Height = this.Height - this.scAuthorPrefs.Top;
        }

        /// <summary>
        /// implements mode-specific layout adjustments
        /// </summary>
        private void AdjustForMode()
        {
            bool bIsAdmin = (m_iMode == Modes.Admin);
            //GLOG 6148
            bool bIsDesigner = (m_iMode == Modes.Designer);

            //set visibilty of entity type controls
            this.lblOwnerCategory.Visible = bIsAdmin || bIsDesigner;
            this.rdGroups.Visible = bIsAdmin || bIsDesigner;
            this.rdOffices.Visible = bIsAdmin || bIsDesigner;
            //this.rdPeople.Visible = bIsAdmin || bIsDesigner;

            //modify label and get spacing offset
            int iVOffset = 0;
            int iHOffset = 0;
            if (bIsDesigner || bIsAdmin)
            {
                this.lblOwner.Text = "&Entities:";
                iVOffset = 30;
                iHOffset = 97;
            }
            else
            {
                this.lblOwner.Text = "&Authors:";
                iVOffset = -30;
                iHOffset = -84;
            }

            //move other controls
            this.lblOwner.Top += iVOffset;
            this.cmbOwner.Top += iVOffset;
            this.lblLanguage.Top += iVOffset;
            this.cmbLanguage.Top += iVOffset;
            this.gbxPreferences.Top += iVOffset;
            this.gbxPreferences.Height -= iVOffset;
            //this.btnSave.Left += iHOffset;
        }

        /// <summary>
        /// populates the owner dropdown
        /// </summary>
        private void LoadOwners()
        {
            ArrayList oOwners = null;

            m_bOwnersLoading = true;
            if (m_xCurFolderID.Contains("."))
            {
                if (rdOffices.Checked && m_oCurOffices == null)
                {
                    Offices oOffices = new LMP.Data.Offices();
                    oOwners = oOffices.ToArrayList(new int[] {0, 2});
                }
                else if (rdGroups.Checked && m_oCurGroups == null)
                {
                    PeopleGroups oGroups = new PeopleGroups();
                    oOwners = oGroups.ToArrayList(new int[] {0, 1});
                }
            }
            else
            {
                int iCurFolderID = int.Parse(m_xCurFolderID);

                //refresh list if necessary
                if (rdOffices.Checked && m_oCurOffices == null)
                    //get offices
                    oOwners = LMP.Data.AdminPermissions.GetFolderPermissionsForOffices(iCurFolderID);
                else if (rdGroups.Checked && m_oCurGroups == null)
                    //get groups
                    oOwners = LMP.Data.AdminPermissions.GetFolderPermissionsForGroups(iCurFolderID);
                else if (rdPeople.Checked && m_oCurPeople == null)
                    //get people
                    oOwners = LMP.Data.AdminPermissions.GetFolderPermissionsForAuthorUsers(iCurFolderID);
            }
            //if list was refreshed, add firm record at top
            if (oOwners != null)
            {
                string[] aFirmRecord = new string[] { LMP.Data.ForteConstants.mpFirmRecordID.ToString(), "Firm Default" };
                oOwners.Insert(0, aFirmRecord);
            }

            if (rdOffices.Checked)
            {
                //update offices list if necessary
                if (m_oCurOffices == null)
                    m_oCurOffices = oOwners;

                //load offices into control
                cmbOwner.SetList(m_oCurOffices);
            }
            else if (rdGroups.Checked)
            {
                //update groups list if necessary
                if (m_oCurGroups == null)
                    m_oCurGroups = oOwners;

                //load groups into control
                cmbOwner.SetList(m_oCurGroups);
            }
            else if (rdPeople.Checked)
            {
                //update people list if necessary
                if (m_oCurPeople == null)
                    m_oCurPeople = oOwners;

                //load people into control
                cmbOwner.SetList(m_oCurPeople);
            }

            //initialize control
            cmbOwner.SelectedIndex = 0;

            m_bOwnersLoading = false;
        }

        /// <summary>
        /// displays controls for the author prefs belonging to the current segment
        /// </summary>
        /// <param name="oNode"></param>
        private void DisplayPreferenceControls()
        {
            //get xml
            string xXML = m_oCurSegDef.XML;

            //GLOG 2940
            //TODO:  The new functionality here is a stopgap measure
            //before restructuring Content handling to support Language as a Segment property
            //Check if Preferences include a Language setting
            //11/17/08 - retain for backward compatibility
            KeySet oPrefs = null;
            string xLanguage = "";
            try
            {
                oPrefs = new KeySet(mpKeySetTypes.AuthorPref,
                    m_xCurSegmentID, cmbOwner.Value);
                xLanguage = oPrefs.GetValue("Language");
            }
            catch { }
            if (xLanguage != "")
            {
                m_xCurLanguage = xLanguage;
            }
            ArrayList oCurPrefs = (ArrayList)m_oPreferencesHash[m_xCurSegmentID];

            if (oCurPrefs != null && oCurPrefs.Count > 0)
            {
                this.lblNoPreferences.Visible = false;

                //setup controls
                this.SetupPreferenceControls(oCurPrefs);

                //display preference values for selected owner
                this.LoadPreferences(this.cmbOwner.Value, oCurPrefs);

                this.Refresh();
            }
            else
                this.lblNoPreferences.Visible = true;
        }

        /// <summary>
        /// extracts author pref definitions for the current segment
        /// and loads them into an array list
        /// </summary>
        /// <param name="xXML"></param>
        private ArrayList LoadPreferenceDefs(string xSegmentID)
        {
            ArrayList oPrefDefs = new ArrayList();
            if (xSegmentID.EndsWith(".0"))
                xSegmentID = xSegmentID.Substring(0, xSegmentID.Length - 2);

            ISegmentDef oDef = null;
            string xObjectData = "";

            if (!xSegmentID.Contains("."))
            {
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                try
                {
                    oDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(xSegmentID));
                }
                catch { }
            }
            else
            {
                UserSegmentDefs oDefs = new UserSegmentDefs();
                try
                {
                    oDef = (UserSegmentDef)oDefs.ItemFromID(xSegmentID);
                }
                catch { }
            }

            if (oDef != null)
            {
                XmlDocument oXmlDoc = new XmlDocument();
                string xXML = oDef.XML;
                if (LMP.String.IsBase64SegmentString(xXML))
                    xXML = LMP.Architect.Oxml.XmlSegment.GetFlatXmlFromBase64OpcString(xXML);
                oXmlDoc.LoadXml(xXML);
                XmlNamespaceManager oManager = new XmlNamespaceManager(oXmlDoc.NameTable);
                //get all mSEGS so that we can retrieve
                //deleted author preference variables
                if (String.IsWordOpenXML(oXmlDoc.OuterXml))
                {
                    //get segment docvar id
                    oManager.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);

                    string xXPath = @"//w:docVar[contains(@w:val, 'ObjectData=SegmentID=" + xSegmentID + "|')]";
                    XmlNodeList oSegDocVars = oXmlDoc.SelectNodes(xXPath, oManager);
                    foreach (XmlNode oSegVar in oSegDocVars)
                    {
                        if (xObjectData == "")
                        {
                            xObjectData = oSegVar.OuterXml;
                        }
                        //get delete scopes
                        XmlNode oID = oSegVar.SelectSingleNode("@w:name", oManager);
                        string xDeleteScopeVarID = oID.Value.Replace("mpo", "mpd");

                        xXPath = @"//w:docVar[contains(@w:name,'" + xDeleteScopeVarID + "')]";
                        XmlNodeList oDelScopeVars = oXmlDoc.SelectNodes(xXPath, oManager);

                        foreach (XmlNode oDelScopeVar in oDelScopeVars)
                        {
                            //get delete scope object data
                            XmlAttribute oDelScope = oDelScopeVar.Attributes["w:val"];

                            string xDelScopeObjData = null;
                            if (oDelScope != null)
                                xDelScopeObjData += String.Decrypt(oDelScope.Value);

                            //cycle through all sdt tags to get associated doc var ids
                            const string TAG_STRING = "�w:sdt>�w:sdtPr>�w:tag w:val=\"";
                            int iPos = xDelScopeObjData.IndexOf(TAG_STRING);

                            while (iPos > -1)
                            {
                                //there are content controls in this delete scope -
                                //get doc var id
                                string xDelScopeDocVarID = xDelScopeObjData.Substring(iPos + TAG_STRING.Length, 11);
                                xDelScopeDocVarID = xDelScopeDocVarID.Replace("mpv", "mpo");

                                //add doc var to object data
                                xXPath = @"//w:docVar[@w:name='" + xDelScopeDocVarID + "']";
                                XmlNode oVarDocVarNode = oXmlDoc.SelectSingleNode(xXPath, oManager);

                                if (oVarDocVarNode != null)
                                    xObjectData += oVarDocVarNode.Attributes["w:val"].Value;

                                iPos = xDelScopeObjData.IndexOf(TAG_STRING, iPos + 1);
                            }
                        }

                        //get segment content controls
                        xXPath = @"//w:sdt/w:sdtPr/w:tag[starts-with(@w:val,'mps')]";
                        XmlNodeList oSegCCs = oXmlDoc.SelectNodes(xXPath, oManager);

                        foreach (XmlNode oSegCC in oSegCCs)
                        {
                            //check that the cc is part of the 
                            //target segment,and not a child
                            string xSegCTag = oSegCC.Attributes["w:val"].Value;
                            string xDocVarID = xSegCTag.Substring(0, 11).Replace("mps", "mpo");

                            xXPath = @"//w:docVar[@w:name='" + xDocVarID +
                                "' and contains(@w:val, 'ObjectData=SegmentID=" + xSegmentID + "|')]";
                            XmlNode oDocVarNode = oXmlDoc.SelectSingleNode(xXPath, oManager);

                            //right here - need to exclude Variables of child segments
                            if (oDocVarNode != null)
                            {
                                //this is the cc of the target segment - get contained variables
                                xXPath = @"//w:sdt/w:sdtPr/w:tag[starts-with(@w:val,'mpv')]";
                                XmlNodeList oVarCCs = oSegCC.SelectNodes(xXPath, oManager);

                                foreach (XmlNode oVarCC in oVarCCs)
                                {
                                    //check if CC is a var of target segment
                                    xXPath = @"ancestor::w:sdt/w:sdtPr/w:tag[starts-with(@w:val, 'mps')]";
                                    XmlNode oParentSegNode = oVarCC.SelectSingleNode(xXPath, oManager);
                                    XmlNodeList oParentSegNodes = oVarCC.SelectNodes(xXPath, oManager);

                                    if (oParentSegNodes.Count > 0 &&
                                        oParentSegNodes[oParentSegNodes.Count - 1].Attributes["w:val"].Value == xSegCTag)
                                    {
                                        //variable cc belongs to target segment, not
                                        //to a child of the target
                                        //get associated docvar id
                                        string xVarDocVarID = oVarCC.Attributes["w:val"]
                                            .Value.Substring(0, 11).Replace("mpv", "mpo");

                                        xXPath = @"//w:docVar[@w:name='" + xVarDocVarID + "']";
                                        XmlNode oVarDocVarNode = oXmlDoc.SelectSingleNode(xXPath, oManager);

                                        if (oVarDocVarNode != null)
                                            xObjectData += oVarDocVarNode.Attributes["w:val"].Value;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    oManager.AddNamespace(m_xNamespacePrefix,
                        ForteConstants.MacPacNamespace);
                    string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                        xSegmentID + "|')]";
                    string xXPath = @"//" + m_xNamespacePrefix + ":mSEG" + xSegCondition;
                    XmlNodeList oNodes = oXmlDoc.SelectNodes(xXPath, oManager);
                    foreach (XmlNode oSegmentNode in oNodes)
                    {
                        if (xObjectData == "")
                        {
                            XmlAttribute oObjectData = oSegmentNode.Attributes["ObjectData"];
                            xObjectData = String.Decrypt(oObjectData.Value);
                        }
                        XmlAttribute oDeletionScopes = oSegmentNode.Attributes["DeletedScopes"];
                        if (oDeletionScopes != null)
                            xObjectData += String.Decrypt(oDeletionScopes.Value);
                    }

                    //append object data of mVars belonging to target segment
                    xXPath = @"//" + LMP.String.GetDefaultXmlNamespacePrefix() + ":mVar[ancestor::" +
                        LMP.String.GetDefaultXmlNamespacePrefix() + ":mSEG[1]" + xSegCondition + ']';
                    XmlNodeList oVarNodes = oXmlDoc.SelectNodes(xXPath, oManager);
                    foreach (XmlNode oVarNode in oVarNodes)
                    {
                        XmlAttribute oObjectData = oVarNode.Attributes["ObjectData"];
                        xObjectData += oObjectData.Value;
                    }

                    xObjectData = String.RestoreXMLChars(xObjectData);
                }
            }

            //get variable definitions containing author pref codes
            string xPattern = @"VariableDefinition=.*?\|";
            Regex oRegex = new Regex(xPattern, RegexOptions.Singleline);
            MatchCollection oMatches = oRegex.Matches(xObjectData);
            if (oMatches.Count > 0)
            {
                string xDefs = "";
                foreach (Match oMatch in oMatches)
                {
                    string xDef = oMatch.Value;
                    if (((xDef.ToUpper().IndexOf("AUTHORPREFERENCE") > -1) ||
                        (xDef.ToUpper().IndexOf("AUTHORPARENTPREFERENCE") > -1)) &&
                        (xDefs.IndexOf(xDef) == -1))
                    {
                        //parse definition to get only the props we need
                        string[] aDef = xDef.Split('�');

                        //ensure that configured to display in author prefs manager
                        Variable.ControlHosts iDisplayIn =
                            (Variable.ControlHosts)Int32.Parse(aDef[20]);
                        if ((iDisplayIn & Variable.ControlHosts.AuthorPreferenceManager) !=
                            Variable.ControlHosts.AuthorPreferenceManager)
                            continue;

                        //get default value
                        string xDefaultValue = aDef[5];

                        //validate
                        string xDefaultUpper = xDefaultValue.ToUpper();
                        if ((xDefaultUpper.IndexOf("AUTHORPREFERENCE") == -1) &&
                            (xDefaultUpper.IndexOf("AUTHORPARENTPREFERENCE") == -1))
                            //exclude if author pref is not the default value
                            continue;

                        //create array for preference def
                        string[] aProps = new string[5];

                        //get key
                        int iPrefStart = -1;
                        int iPrefEnd = -1;
                        //GLOG 2865: Look for start of Preference fieldcode, 
                        //in case it's within a complex expression
                        iPrefStart = xDefaultUpper.IndexOf("AUTHORPREFERENCE__");
                        if (iPrefStart == -1)
                            iPrefStart = xDefaultUpper.IndexOf("AUTHORPREFERENCE_");
                        if (iPrefStart == -1)
                            iPrefStart = xDefaultUpper.IndexOf("AUTHORPARENTPREFERENCE__");
                        if (iPrefStart == -1)
                            iPrefStart = xDefaultUpper.IndexOf("AUTHORPARENTPREFERENCE_");
                        //Get end of preference fieldcode
                        if (iPrefStart > -1)
                            iPrefEnd = xDefaultUpper.IndexOf(']', iPrefStart);
                        else
                            continue;
                        //Work just with portion of string corresponding to preference fieldcode
                        string xPrefCode = xDefaultValue.Substring(iPrefStart, iPrefEnd - iPrefStart);
                        int iPos = xPrefCode.IndexOf("__") + 2;
                        if (iPos == 1)
                            //fieldcode uses old-format of single underscore
                            iPos = xPrefCode.IndexOf("_") + 1;
                        aProps[0] = xPrefCode.Substring(iPos);

                        //get display name
                        aProps[1] = aDef[2];

                        //get control type
                        aProps[2] = aDef[9];

                        //get control properties
                        aProps[3] = aDef[10];

                        //get execution index
                        aProps[4] = aDef[4];

                        //add to array list
                        oPrefDefs.Add(aProps);

                        //avoid duplicates
                        xDefs += xDef;
                        if (aDef[9] == ((int)mpControlTypes.SegmentChooser).ToString())
                        {
                            //If Preference is a Segment ID, load author's default in Hash table
                            KeySet oPrefs = null;
                            int iChooserSegment = 0;
                            mpObjectTypes iSegmentType = 0;
                            try
                            {
                                oPrefs = new KeySet(mpKeySetTypes.AuthorPref,
                                    xSegmentID, cmbOwner.Value, 0, m_iCulture);
                            }
                            catch { }
                            if (oPrefs != null)
                            {
                                try
                                {
                                    iChooserSegment = Int32.Parse(oPrefs.GetValue(aProps[0]));
                                }
                                catch { }
                            }
                            if (iChooserSegment != 0)
                            {
                                string xControlProps = aDef[10];

                                if (xControlProps != "")
                                {
                                    string[] aCtlProps = xControlProps.Split(StringArray.mpEndOfSubValue);
                                    foreach (string xProp in aCtlProps)
                                    {
                                        string[] aProp = xProp.Split('=');
                                        if (aProp.GetLength(0) == 2)
                                        {
                                            if (aProp[0].ToLower() == "assignedobjecttype")
                                            {
                                                try
                                                {
                                                    iSegmentType = (mpObjectTypes)Enum.Parse(typeof(mpObjectTypes), aProp[1]);
                                                }
                                                catch {}
                                                break;
                                            }
                                        }
                                    }
                                    if (iSegmentType != 0)
                                    {
                                        if (!m_oChooserHash.ContainsKey(iSegmentType))
                                            m_oChooserHash.Add(iSegmentType, iChooserSegment);
                                        else
                                            m_oChooserHash[iSegmentType] = iChooserSegment;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //sort preferences by execution index
            oPrefDefs.Sort(new PreferenceSorter());
            return oPrefDefs;
        }

        /// <summary>
        /// adds controls for the current set of author preferences
        /// </summary>
        private void SetupPreferenceControls(ArrayList oPrefs)
        {
            //Ensure scrollbar won't affect width calculations
            pnlPreferences.AutoScroll = false;
            this.pnlPreferences.SuspendLayout();
            int iTop = 8;
            int iCtlLeft = this.pnlPreferences.Width >= (int)(421 * LMP.OS.GetScalingFactor()) ? this.pnlPreferences.Width - (int)(250 * LMP.OS.GetScalingFactor()) : (int)(.44 * this.pnlPreferences.Width);
            int iCtlWidth = this.pnlPreferences.Width >= 421 ?
                    this.pnlPreferences.Width - (iCtlLeft + (int)(25 * LMP.OS.GetScalingFactor())) :
                    (int)(this.pnlPreferences.Width * .5);
            int iLblWidth = this.pnlPreferences.Width >= 421 ? (this.pnlPreferences.Width - (int)(275 * LMP.OS.GetScalingFactor())) : (int)(.35 * this.pnlPreferences.Width);

            for (int i = 0; i < oPrefs.Count; i++)
            {
                //add label
                string[] aDef = (string[])oPrefs[i];
                Label oLabel = new Label();
                oLabel.Name = "lblPreference" + i.ToString();
                oLabel.Text = '&' + aDef[1] + ':';
                oLabel.Width = iLblWidth; 
                oLabel.Height = 30;
                this.pnlPreferences.Controls.Add(oLabel);
                oLabel.Left = this.pnlPreferences.Width >= 421 ? (int)(15 * LMP.OS.GetScalingFactor()) : (int)(this.pnlPreferences.Width * .03);
                oLabel.Top = iTop;
                this.toolTipPrefLabel.SetToolTip(oLabel, aDef[1]);
                Size oSize = TextRenderer.MeasureText(aDef[1] + ":", oLabel.Font);
                oLabel.AutoEllipsis = true;

                //add value control
                Control oControl = this.SetupControl(aDef);
                oControl.Name = "ctlPreference" + i.ToString();
                this.pnlPreferences.Controls.Add(oControl);
                oControl.Anchor = ((AnchorStyles)((AnchorStyles.Left |
                    AnchorStyles.Top | AnchorStyles.Right)));
                
                oControl.Left = iCtlLeft;
                oControl.Width = iCtlWidth;
                oControl.Top = iTop - 2;

                //If current control is Language dropdown, subscribe to LostFocus event -
                //11/17/08 - retain for backward compatibility
                if (aDef[1].ToUpper() == "LANGUAGE" && oControl is LMP.Controls.ComboBox)
                {
                    ((IControl)oControl).ValueChanged += new ValueChangedHandler(HandleLanguageChanged);
                    //oControl.LostFocus += new EventHandler(HandleLanguageChanged);
                }

                //if value is first item in dropdown list or date combo,
                //text will be selected - deselect now
                if (oControl is LMP.Controls.ComboBox)
                    ((LMP.Controls.ComboBox)oControl).SelectionLength = 0;
                else if (oControl is LMP.Controls.DateCombo)
                    ((LMP.Controls.DateCombo)oControl).SelectionLength = 0;
                else if (oControl is Spinner)
                {
                    ((LMP.Controls.Spinner)oControl).AutoSize = false;
                }
                //set top for next control
                iTop += (oControl.Height + 20);
            }
            this.pnlPreferences.ResumeLayout();
            pnlPreferences.AutoScroll = true;
        }

        /// <summary>
        /// refresh language-specific control properties when language is changed -
        /// this was used prior to adding culture-specific preferences - retain
        /// for backward compatibility
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HandleLanguageChanged(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Application.DoEvents();
                m_xCurLanguage = ((IControl)sender).Value;
                if (string.IsNullOrEmpty(m_xCurLanguage))
                    return;
                UpdateForLanguage(m_xCurLanguage);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        /// <summary>
        /// updates control properties, e.g. lists, when language changes
        /// </summary>
        /// <param name="xLanguage"></param>
        private void UpdateForLanguage(string xLanguage)
        {
            ArrayList oPrefDefs = (ArrayList)m_oPreferencesHash[m_xCurSegmentID];
            for (int i = 0; i < oPrefDefs.Count; i++)
            {
                string[] aDef = (string[])oPrefDefs[i];
                //Look for expressions referencing language variable in control definition
                if (aDef[3].IndexOf("Variable_Language]") > -1 ||
                    aDef[3].IndexOf("Variable__Language]") > -1 ||
                    aDef[3].IndexOf("[SegmentLanguageName]") > -1 ||
                    aDef[3].IndexOf("[SegmentLanguageID]") > -1)
                {
                    Control oControl = this.pnlPreferences.Controls["ctlPreference" + i.ToString()];
                    if (oControl != null)
                        SetControlLanguage(oControl, xLanguage, aDef[3]);
                }
            }
        }

        /// <summary>
        /// Refresh any control properties that reference the Language Variable in an expression
        /// </summary>
        /// <param name="oControl"></param>
        /// <param name="xLanguage"></param>
        /// <param name="xProperties"></param>
        void SetControlLanguage(Control oControl, string xLanguage, string xProperties)
        {
            string[] aProps = null;
            if (xProperties != "")
                aProps = xProperties.Split(StringArray.mpEndOfSubValue);
            else
                aProps = new string[0];

            //cycle through property name/value pairs
            for (int i = 0; i < aProps.Length; i++)
            {
                int iPos = aProps[i].IndexOf("=");
                string xName = aProps[i].Substring(0, iPos);
                string xValue = aProps[i].Substring(iPos + 1);
                //only modify those properties referencing the Language
                if (xValue.IndexOf("Variable_Language]") > -1 ||
                    xValue.IndexOf("Variable__Language]") > -1 ||
                    xValue.IndexOf("[SegmentLanguageName]") > -1 ||
                    xValue.IndexOf("[SegmentLanguageID]") > -1)
                {
                    //Conform to new fieldcode format
                    xValue = xValue.Replace("Variable_Language]", "Variable__Language]");
                    xValue = xValue.Replace("[Parent::Variable__Language]", xLanguage);
                    xValue = xValue.Replace("[Variable__Language]", xLanguage);
                    xValue = xValue.Replace("[SegmentLanguageName]", xLanguage);
                    xValue = xValue.Replace("[SegmentLanguageID]", m_iCulture.ToString());
                    //GLOG 7584:  Allow for more complex expressions such as CHOOSE, REPLACE, etc.
                    xValue = Expression.Evaluate(xValue, null, null);
                    System.Type oCtlType = oControl.GetType();

                    PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                    System.Type oPropType = oPropInfo.PropertyType;

                    object oValue = null;

                    if (oPropType.IsEnum)
                        oValue = Enum.Parse(oPropType, xValue);
                    else
                        oValue = Convert.ChangeType(xValue, 
                            oPropType, LMP.Culture.USEnglishCulture);

                    oPropInfo.SetValue(oControl, oValue, null);
                }
            }
        }
        /// <summary>
        /// gets the author preferences for the specified owner and
        /// initializes the preference controls
        /// </summary>
        /// <param name="xOwnerID"></param>
        private void LoadPreferences(string xOwnerID, ArrayList oPrefDefs)
        {
            //get key set
            m_oPrefKeyset = null;
            try
            {
                m_oPrefKeyset = new KeySet(mpKeySetTypes.AuthorPref,
                    m_xCurSegmentID, xOwnerID, 0, m_iCulture);
            }
            catch { }

            if (m_oPrefKeyset != null)
                //set control values
                this.SetPreferenceControlValues(m_oPrefKeyset, oPrefDefs, true);
            else
                //there's no default record - create it now
                this.SavePreferences(m_iCulture);
        }

        /// <summary>
        /// sets preference control values from specified key set
        /// </summary>
        /// <param name="oPrefs"></param>
        private void SetPreferenceControlValues(KeySet oPrefs, ArrayList oPrefDefs, bool bFirstLoad)
        {
            for (int i = 0; i < oPrefDefs.Count; i++)
            {
                string xCtl = "ctlPreference" + i.ToString();
                IControl oCtl = this.pnlPreferences.Controls[xCtl] as IControl;
                string[] aDefs = (string[])oPrefDefs[i];
                try
                {
                    //GetValue() will err if key is missing or empty, e.g.
                    //with a new default record or key or an existing key
                    //with no value
                    string xValue = oPrefs.GetValue(aDefs[0]);
                    //GLOG 15908: Ensure correct display of date text
                    if (oCtl is DateCombo)
                    {
                        ((DateCombo)oCtl).ResetText();
                    }
                    oCtl.Value = oPrefs.GetValue(aDefs[0]);
                    if (oCtl is Chooser)
                    {
                        Chooser oChooser = oCtl as Chooser;
                        if (m_oChooserHash.ContainsKey(oChooser.AssignedObjectType))
                        {
                            m_oChooserHash[oChooser.AssignedObjectType] = Int32.Parse(oPrefs.GetValue(aDefs[0]));
                        }
                        else
                        {
                            m_oChooserHash.Add(oChooser.AssignedObjectType, Int32.Parse(oPrefs.GetValue(aDefs[0])));
                        }

                    }
                }
                catch { }

                //if value is first item in dropdown list or date combo,
                //text will be selected - deselect now
                if (oCtl is LMP.Controls.ComboBox)
                    ((LMP.Controls.ComboBox)oCtl).SelectionLength = 0;
                else if (oCtl is LMP.Controls.DateCombo)
                    ((LMP.Controls.DateCombo)oCtl).SelectionLength = 0;
                
                if (bFirstLoad)
                    oCtl.IsDirty = false;
            }
        }

        /// <summary>
        /// resets preferences to those of specified entity
        /// </summary>
        /// <param name="iResetID"></param>
        private void ResetPreferences()
        {
            // GLOG : 3012 : JAB
            // Check that the m_oPrefs is not null.
            if (m_oPrefKeyset != null && m_oPrefKeyset.EntityType != mpEntityTypes.Firm && !m_oPrefKeyset.IsDefault)
            {
                m_oPrefKeyset.Delete();

                m_oPrefKeyset = null;
            }

            //load preferences - do so regardless of 
            //whether or not we're deleting a preference set -
            //the user may have changed some values in the UI
            //without yet saving the set
            this.LoadPreferences(this.cmbOwner.Value, (ArrayList)m_oPreferencesHash[m_xCurSegmentID]);
        }

        /// <summary>
        /// saves the current set of preferences
        /// </summary>
        private void SavePreferences(int iCulture)
        {
            //get current owner
            string xOwnerID = this.cmbOwner.Value;

            if (m_oPrefKeyset != null)
            {
                //set values
                bool bForceSave = false;
                int iRetryAttempts = 0;

                ArrayList oPrefDefs = (ArrayList)m_oPreferencesHash[m_xCurSegmentID];
                for (int i = 0; i < oPrefDefs.Count; i++)
                {
                    //get value from control
                    string xCtl = "ctlPreference" + i.ToString();
                    IControl oCtl = this.pnlPreferences.Controls[xCtl] as IControl;

                    //set preference value
                    string[] aDefs = (string[])oPrefDefs[i];
                    try
                    {
                        m_oPrefKeyset.SetValue(aDefs[0], oCtl.Value);
                    }
                    catch
                    {
                        if (iRetryAttempts == 0)
                        {
                            iRetryAttempts++;

                            //default key doesn't exist -  add now
                            KeySet.SetKeyValue(aDefs[0], oCtl.Value,
                                mpKeySetTypes.AuthorPref, m_xCurSegmentID.ToString(),
                                LMP.Data.ForteConstants.mpFirmRecordID, 0, iCulture);

                            //reload key set
                            m_oPrefKeyset = new KeySet(mpKeySetTypes.AuthorPref,
                                m_xCurSegmentID, xOwnerID, 0, iCulture);

                            //set flag to force save - this is necessary because
                            //default values and target values may now match, preventing
                            //the creation of an entity-specific record as expected
                            bForceSave = true;

                            //restart loop
                            i = 0;
                        }
                        else
                        {
                            throw new LMP.Exceptions.NotInCollectionException(
                                string.Format(LMP.Resources.GetLangString("Error_AuthorPreferenceMissing"),aDefs[0]));
                        }
                    }
                    finally
                    {
                        oCtl.IsDirty = false;
                    }
                }

                //dirty the key set artificially
                if (bForceSave)
                {
                    string[] aDefs = (string[])oPrefDefs[0];
                    string xValue = m_oPrefKeyset.GetValue(aDefs[0]);
                    m_oPrefKeyset.SetValue(aDefs[0], "zzmpDummy");
                    m_oPrefKeyset.SetValue(aDefs[0], xValue);
                }

                //save record
                m_oPrefKeyset.Save();
            }
            else
            {
                //there's no default record
                ArrayList oValues = new ArrayList();

                ArrayList oPrefDefs = (ArrayList)m_oPreferencesHash[m_xCurSegmentID];
                //get values from controls
                for (int i = 0; i < oPrefDefs.Count; i++)
                {
                    string xCtl = "ctlPreference" + i.ToString();
                    IControl oCtl = this.pnlPreferences.Controls[xCtl] as IControl;
                    if (oCtl.Value != "")
                    {
                        string[] aDefs = (string[])oPrefDefs[i];
                        oValues.Add(new string[] { aDefs[0], oCtl.Value });
                    }
                    oCtl.IsDirty = false;
                }

                if (oValues.Count > 0)
                {
                    //add firm record to db
                    KeySet.AddAuthorPrefRecord(m_xCurSegmentID,
                        LMP.Data.ForteConstants.mpFirmRecordID.ToString(),
                        iCulture, oValues);

                    ////add record for target owner
                    //if (xOwnerID != LMP.Data.MP10Constants.mpFirmRecordID.ToString())
                    //{
                    //    KeySet.AddAuthorPrefRecord(m_iCurSegmentID.ToString(),
                    //        xOwnerID, iCulture, oValues);
                    //}

                }
                else
                {
                    //add firm record to db
                    KeySet.AddAuthorPrefRecord(m_xCurSegmentID,
                        LMP.Data.ForteConstants.mpFirmRecordID.ToString(),
                        iCulture, null);

                }
                //get new record
                m_oPrefKeyset = new KeySet(mpKeySetTypes.AuthorPref,
                    m_xCurSegmentID, xOwnerID, 0, iCulture);
            }
        }

        /// <summary>
        /// returns a new control as specified
        /// </summary>
        /// <param name="iType"></param>
        /// <param name="xProperties"></param>
        /// <returns></returns>
        private Control SetupControl(string[] aDef)
        {
            try
            {
                Control oCtl = null;

                //create new control
                LMP.Data.mpControlTypes iType =
                    (mpControlTypes)System.Convert.ToByte(aDef[2]);
                switch (iType)
                {
                    case mpControlTypes.Textbox:
                        oCtl = new LMP.Controls.TextBox();
                        ((LMP.Controls.TextBox)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.MultilineTextbox:
                        oCtl = new LMP.Controls.MultilineTextBox();
                        ((LMP.Controls.MultilineTextBox)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.Combo:
                        oCtl = new LMP.Controls.ComboBox();
                        ((LMP.Controls.ComboBox)oCtl).LimitToList = false;
                        ((LMP.Controls.ComboBox)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.DropdownList:
                        oCtl = new LMP.Controls.ComboBox();
                        ((LMP.Controls.ComboBox)oCtl).LimitToList = true;
                        ((LMP.Controls.ComboBox)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.LocationsDropdown:
                        oCtl = new LMP.Controls.LocationsDropdown();
                        break;
                    case mpControlTypes.Checkbox:
                        oCtl = new LMP.Controls.BooleanComboBox();
                        ((LMP.Controls.BooleanComboBox)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.AuthorSelector:
                        oCtl = new LMP.Controls.AuthorSelector();
                        ((LMP.Controls.AuthorSelector)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.List:
                        oCtl = new LMP.Controls.ListBox();
                        break;
                    case mpControlTypes.MultilineCombo:
                        oCtl = new LMP.Controls.MultilineCombo(this);
                        ((LMP.Controls.MultilineCombo)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.DetailGrid:
                        oCtl = new LMP.Controls.Detail();
                        ((LMP.Controls.Detail)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.RelineGrid:
                        oCtl = new LMP.Controls.Reline();
                        ((LMP.Controls.Detail)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.SalutationCombo:
                        oCtl = new LMP.Controls.SalutationCombo();
                        ((LMP.Controls.SalutationCombo)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.DetailList:
                        oCtl = new LMP.Controls.DetailList();
                        ((LMP.Controls.DetailList)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.ClientMatterSelector:
                        //oCtl = new LMP.Controls.ClientMatterSelector();
                        break;
                    case mpControlTypes.SegmentChooser:
                        oCtl = new LMP.Controls.Chooser();

                        //set target object type id and target object id
                        ((Chooser)oCtl).TargetObjectID = Int32.Parse(this.m_xCurSegmentID);
                        ((Chooser)oCtl).TargetObjectType = this.m_oCurSegDef.TypeID;
                        break;
                    case mpControlTypes.DateCombo:
                        oCtl = new LMP.Controls.DateCombo();
                        ((LMP.Controls.DateCombo)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.Spinner:
                        oCtl = new LMP.Controls.Spinner();
                        ((LMP.Controls.Spinner)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.FontList:
                        oCtl = new LMP.Controls.FontList();
                        ((LMP.Controls.FontList)oCtl).AutoSize = true;
                        break;
                    case mpControlTypes.PaperTraySelector:
                        oCtl = new LMP.Controls.PaperTraySelector();
                        ((LMP.Controls.PaperTraySelector)oCtl).AutoSize = true;
                        break;
                    default:
                        break;
                }

                try
                {
                    //set up control properties
                    string xProperties = aDef[3];
                    //GLOG 5287: Replace XML tokens in string
                    xProperties = LMP.String.RestoreXMLChars(xProperties);
                    string[] aProps = null;
                    if (xProperties != "")
                        aProps = xProperties.Split(StringArray.mpEndOfSubValue);
                    else
                        aProps = new string[0];

                    //cycle through property name/value pairs, executing each
                    for (int i = 0; i < aProps.Length; i++)
                    {
                        int iPos = aProps[i].IndexOf("=");
                        string xName = aProps[i].Substring(0, iPos);
                        string xValue = aProps[i].Substring(iPos + 1);

                        if (xValue.IndexOf("Variable_Language]") > -1 ||
                            xValue.IndexOf("Variable__Language]") > -1)
                        {
                            //Skip property for now if language not yet set
                            if (m_xCurLanguage == "")
                                continue;
                            //Conform to new fieldcode format
                            xValue = xValue.Replace("Variable_Language]", "Variable__Language]");
                            //Replace references to Language Variable with current Language string
                            xValue = xValue.Replace("[Parent::Variable__Language]", m_xCurLanguage);
                            xValue = xValue.Replace("[Variable__Language]", m_xCurLanguage);
                        }

                        //replace language field codes
                        xValue = xValue.Replace("[SegmentLanguageName]", m_xCulture);
                        xValue = xValue.Replace("[SegmentLanguageID]", m_iCulture.ToString());
                        //GLOG 7584:  Allow for more complex expressions such as CHOOSE, REPLACE, etc.
                        xValue = Expression.Evaluate(xValue, null, null);

                        System.Type oCtlType = oCtl.GetType();

                        PropertyInfo oPropInfo = oCtlType.GetProperty(xName);
                        System.Type oPropType = oPropInfo.PropertyType;

                        object oValue = null;

                        if (oPropType.IsEnum)
                            oValue = Enum.Parse(oPropType, xValue);
                        else
                            oValue = Convert.ChangeType(xValue, 
                                oPropType, LMP.Culture.USEnglishCulture);

                        try
                        {
                            oPropInfo.SetValue(oCtl, oValue, null);
                        }
                        catch { }
                    }

                    //do any control-specific setup of control
                    IControl oIControl = (IControl)oCtl;
                    oIControl.ExecuteFinalSetup();

                    //initialize controls where necessary
                    if (iType == mpControlTypes.DropdownList)
                        ((LMP.Controls.ComboBox)oCtl).SelectedIndex = 0;
                    else if (iType == mpControlTypes.List)
                        ((LMP.Controls.ListBox)oCtl).SelectedIndex = 0;
                    oIControl.IsDirty = false;
                    return oCtl;
                }
                finally
                {
                    oCtl.ResumeLayout();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Data.mpControlTypes iType =
                    (mpControlTypes)System.Convert.ToByte(aDef[2]);
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeControl") +
                    iType.ToString(), oE);
            }
        }
        private void RefreshFoldersForNewUser()
        {

            UltraTreeNode oNode = null;
            try
            {
                oNode = ftvSegments.Tree.SelectedNodes[0];
            }
            catch { }
            string xFolderID = "0";
            string xSegmentID = "";
            string xKey = "";
            if (oNode != null)
            {
                this.ParseContentNode(oNode, out xFolderID, out xSegmentID);

                xKey = oNode.Key;
            }
            ftvSegments.OwnerID = m_iUserID;

            if (xFolderID != "0")
            {
                //check whether new user has permission to currently selected folder
                bool bPermissionExists = true;
                if (!xFolderID.Contains("."))
                {
                    bPermissionExists = LMP.Data.AdminPermissions.FolderPermissionExists(
                         int.Parse(xFolderID), m_iUserID);
                }
                if (xSegmentID != "")
                {
                    if (bPermissionExists)
                    {
                        //reselect original Segment
                        this.SelectSegment(xSegmentID);
                    }
                    else
                    {
                        //New user doesn't have permission for original folder
                        //clear preferences panel of any existing controls
                        this.pnlPreferences.Controls.Clear();

                        //hide "no preferences" label
                        this.lblNoPreferences.Visible = false;
                    }
                }
            }
            else if (ftvSegments.Mode == FolderTreeView.mpFolderTreeViewModes.FindResults)
            {
                try
                {
                    this.SelectSegment(xSegmentID);
                }
                catch
                {
                }
            }
        }
        /// <summary>
        /// loads selected owners list and displays firm preferences
        /// </summary>
        private void OnOwnersListTypeChanged()
        {
            //disable reset button
            this.btnReset.Enabled = false;

            this.btnSave.Enabled = false;
            //load appropriate owners list
            LoadOwners();

            //load firm preferences
            LoadPreferences(LMP.Data.ForteConstants.mpFirmRecordID.ToString(), (ArrayList)m_oPreferencesHash[m_xCurSegmentID]);
        }

        /// <summary>
        /// gets folder and segment represented by specified node
        /// </summary>
        /// <param name="oNode"></param>
        /// <param name="iFolderID"></param>
        /// <param name="iSegmentID"></param>
        private void ParseContentNode(UltraTreeNode oNode, out string xFolderID,
            out string xSegmentID)
        {
            //GLOG 6148: Selected segment may be Designer Segment
            if (oNode == null)
            {
                xFolderID = "0";
                xSegmentID = "";
                return;
            }
            //parse key into component ids
            string[] xSep = new string[] { mpPathSeparator };
            string[] oIDs = oNode.Key.Split(xSep, StringSplitOptions.None);

            if (oNode.Tag is Folder)
            {
                xFolderID = ((Folder)oNode.Tag).ID.ToString();
                xSegmentID = "";
            }
            else if (oNode.Tag is FolderMember)
            {
                FolderMember oMember = (FolderMember)oNode.Tag;
                //folder node
                xFolderID = oMember.FolderID1 + "." + oMember.FolderID1;
                xSegmentID = oMember.ObjectID1 + "." + oMember.ObjectID2;
                if (xSegmentID.EndsWith(".0"))
                    xSegmentID = xSegmentID.Substring(0, xSegmentID.Length - 2);
            }
            else if (oNode.Tag is object[])
            {
                object[] aItem = (object[])oNode.Tag;
                //segment node
                xFolderID = "0";
                xSegmentID = aItem[1].ToString();
                string xNodePrefix = "A";
                if (xSegmentID.Contains("."))
                {
                    xNodePrefix = "U";
                }

                //GLOG : 7006 : CEH
                //Get the path details to this segment. Details include the node name and key.
                object[] aoSegmentPathDetails = Folder.GetSegmentPathsDetails(xNodePrefix + xSegmentID);
                if (aoSegmentPathDetails.Length > 0)
                {
                    //get First path details
                    object[] aoFirstPathDetail = (object[])aoSegmentPathDetails[0];
                    //get last folder details
                    object[] aoPathNodeDetail = (object[])aoFirstPathDetail[aoFirstPathDetail.Length - 1];
                    //get folder key
                    string xFolderNodeKey = (string)aoPathNodeDetail[1];
                    xFolderID = xFolderNodeKey;
                }
            }
            else
            {
                xFolderID = "0";
                xSegmentID = "";
            }
        }

        public override void SaveCurrentRecord(bool bPromptIfNecessary)
        {
            ArrayList oChooserUpdates = new ArrayList();
            if (this.pnlPreferences.Controls.Count > 0)
            {
                bool bChooserChanged = false;

                bool bSaveNeeded = false;
                for (int i = 0; i < this.pnlPreferences.Controls.Count; i++)
                {
                    IControl oICtl = pnlPreferences.Controls[i] as IControl;
                    if (oICtl != null && oICtl.IsDirty)
                    {
                        bSaveNeeded = true;
                        //No need to check more
                        if (oICtl is Chooser)
                        {
                            Chooser oChooser = oICtl as Chooser;
                            bChooserChanged = true;
                            string xChooserValue = oChooser.SelectedValue.ToString();
                            if (xChooserValue.EndsWith(".0"))
                                xChooserValue = xChooserValue.Substring(0, xChooserValue.Length - 2);
                            oChooserUpdates.Add(new object[] { oChooser.AssignedObjectType, Int32.Parse(xChooserValue)});
                        }
                        //Need to continue checking to get changed choosers
                        //break;
                    }
                }
                if (bPromptIfNecessary)
                {
                if (bSaveNeeded)
                {
                    //GLOG item #3362 - dcf
                    SendKeys.Send("%");

                    DialogResult iRet = MessageBox.Show(this.ParentForm, LMP.Resources.GetLangString("Dialog_AuthorPreferences_SavePreferences"),
                        LMP.ComponentProperties.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (iRet == DialogResult.No)
                        return;
                }
                else
                    return;
                }

                if (m_oPrefKeyset == null)
                {
                    this.SavePreferences(int.Parse(this.cmbLanguage.Value));
                }
                else
                {
                    this.SavePreferences(m_oPrefKeyset.Culture);
                }
                if (m_aRelatedSegments.Count > 1 && bChooserChanged)
                {
                    //If Chooser Segment has changed, update list of related segments
                    int iSelected = cmbRelated.SelectedIndex;
                    mpObjectTypes iSelectedType = (mpObjectTypes)Enum.Parse(typeof(mpObjectTypes), ((string[])m_aRelatedSegments[iSelected])[2]);
                    AdminSegmentDefs oDefs = new AdminSegmentDefs();
                    for (int i = 0; i < oChooserUpdates.Count; i++)
                    {
                        object[] oVals = (object[])oChooserUpdates[i];
                        mpObjectTypes iChangedType = (mpObjectTypes)oVals[0];
                        string xNewSegmentID = oVals[1].ToString();
                        ArrayList oPrefs  = LoadPreferenceDefs(xNewSegmentID);
                        if (!m_oPreferencesHash.ContainsKey(xNewSegmentID))
                            m_oPreferencesHash.Add(xNewSegmentID, oPrefs);
                        AdminSegmentDef oDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(xNewSegmentID));
                        for (int r = 0; r < m_aRelatedSegments.Count; r++)
                        {
                            string[] aRelated = (string[])m_aRelatedSegments[r];
                            if (aRelated[2] == iChangedType.ToString())
                            {
                                m_aRelatedSegments[r] = new string[] { oDef.ID.ToString(), oDef.DisplayName, oDef.TypeID.ToString() };
                                break;
                            }
                        }
                    }
                    m_bChangingRelated = true;
                    this.cmbRelated.SetList(m_aRelatedSegments);
                    for (int i = 0; i < m_aRelatedSegments.Count; i++)
                    {
                        string[] aRelated = (string[])m_aRelatedSegments[i];
                        if (aRelated[2].ToString() == ((int)iSelectedType).ToString())
                            iSelected = i;
                    }
                    cmbRelated.SelectedIndex = iSelected;
                    m_bChangingRelated = false;
                }
            }
        }

        public void SelectSegment(int iSegmentId)
        {
            SelectSegment(iSegmentId.ToString());
        }
        public void SelectSegment(string xSegmentId)
        {
            if (xSegmentId.EndsWith(".0"))
            {
                xSegmentId = xSegmentId.Substring(0, xSegmentId.Length - 2);
            }
            if (xSegmentId == "0")
                return;

            string xNodePrefix = "";
            if (xSegmentId.Contains("."))
            {
                xNodePrefix = "U";
            }
            else if (xSegmentId != "0")
            {
                xNodePrefix = "A";
            }

            if (ftvSegments.Mode == FolderTreeView.mpFolderTreeViewModes.FolderTree)
            {
                int iFirstLevel = (m_iMode == Modes.Designer && LMP.Data.Application.AdminMode == false) ? 0 : 1;
                // Get the path details to this segment. Details include the node name and key.
                object[] aoSegmentPathDetails = new object[] { };
                if (xSegmentId.Contains("."))
                {
                    ArrayList oUserFolderList = UserFolder.GetSegmentPathIds(xNodePrefix + xSegmentId, m_iUserID);
                    object[] aoFirstUserPathDetail = ((ArrayList)oUserFolderList[0]).ToArray();
                    object[] aoReversedPathDetails = new object[aoFirstUserPathDetail.Length];
                    int iNextIndex = 0;
                    for (int a = aoFirstUserPathDetail.Length - 1; a >= 0; a--)
                    {
                        object[] oFolderDetails = (object[])aoFirstUserPathDetail[a];
                        string xFolderID = oFolderDetails[0].ToString();
                        xFolderID = xFolderID + "." + oFolderDetails[1].ToString();
                        string xFolderName = oFolderDetails[2].ToString();
                        aoReversedPathDetails[iNextIndex++] = new object[] {xFolderName, xFolderID};
                    }
                    aoSegmentPathDetails = new object[] { aoReversedPathDetails };
                }
                else
                    aoSegmentPathDetails = Folder.GetSegmentPathsDetails(xNodePrefix + xSegmentId);

                if (aoSegmentPathDetails.Length > 0)
                {
                    // We will select the first node with this segment. Use the first path for the segment.
                    object[] aoFirstPathDetail = (object[])aoSegmentPathDetails[0];

                    TreeNodesCollection oSegmentNodes = this.ftvSegments.Tree.Nodes;

                    if (oSegmentNodes.Count > 0)
                    {
                        string xPreviousNodeKey = "";

                        // Iterate through the nodes using their keys.
                        for (int i = iFirstLevel; i < aoFirstPathDetail.Length; i++)
                        {
                            object[] aoPathNodeDetail = (object[])aoFirstPathDetail[i];
                            string xFolderNodeKey = (string)aoPathNodeDetail[1];
                            UltraTreeNode oSegmentNode = oSegmentNodes[xFolderNodeKey];
                            oSegmentNode.Expanded = true;
                            oSegmentNodes = oSegmentNode.Nodes;
                            if (!xSegmentId.Contains("."))
                                xPreviousNodeKey = xFolderNodeKey + ".";
                        }
                        string xSelectedKey = "";
                        if (xSegmentId.Contains("."))
                        {
                            //If UserSegment, the tree node will have a UserFolderMember ID
                            //Look for the FolderMember that corresponds to this segment
                            for (int i = 0; i < oSegmentNodes.Count; i++)
                            {
                                string xKey = oSegmentNodes[i].Key; 
                                FolderMember oMember = (FolderMember)oSegmentNodes[i].Tag;
                                if (oMember.ObjectID1.ToString() + "." + oMember.ObjectID2.ToString() == xSegmentId)
                                {
                                    xSelectedKey = oMember.ID;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            xSelectedKey = xSegmentId;
                        }

                        for (int i = 0; i < oSegmentNodes.Count; i++)
                        {
                            string xKey = oSegmentNodes[i].Key;
                        }
                        
                        if (oSegmentNodes.Count > 0 && xSelectedKey != "")
                        {
                            // The final node is the node that contains the segment.
                            UltraTreeNode oSegmentFinalNode = oSegmentNodes[xNodePrefix + xPreviousNodeKey + xSelectedKey];
                            // Make the segment's node the selected node.
                            this.ftvSegments.Tree.ActiveNode = oSegmentFinalNode;
                            oSegmentFinalNode.Selected = true;
                            if (m_iMode == Modes.Designer)
                            {
                                ftvSegments.Enabled = false;
                            }
                        }
                    }

                }
                else
                {

                    // The final node is the node that contains the segment.
                    UltraTreeNode oFindNode = ftvSegments.Tree.Nodes[0].Nodes[xNodePrefix + xSegmentId];

                    // Make the segment's node the selected node.
                    this.ftvSegments.Tree.ActiveNode = oFindNode;
                    oFindNode.Selected = true;

                }
            }
        }
        public void SaveRecord()
        {
            try
            {
                SaveCurrentRecord(true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void SetupLanguageControl(string xCurPrefLang, bool bSelectedInTree)
        {
            m_bLanguagesLoading = true;

            //enable control only if segment supports multiple languages
            string xSupportedLanguages = "";
            int iPos1 = m_xCurSegObjectData.IndexOf("|SupportedLanguages=");
            if (iPos1 > -1)
            {
                //segment property was found, get value
                int iPos2 = m_xCurSegObjectData.IndexOf("|", iPos1 + 1);
                int iValueStartPos = iPos1 + 20;
                xSupportedLanguages = m_xCurSegObjectData.Substring(
                    iValueStartPos, iPos2 - iValueStartPos);
            }

            //use default culture
            if (xSupportedLanguages == "")
                xSupportedLanguages = "1033";

            string[] aSupportedLanguages = xSupportedLanguages.Split(
                LMP.StringArray.mpEndOfValue);
            bool bEnabled = (aSupportedLanguages.Length > 1);
            this.lblLanguage.Enabled = bEnabled;
            this.cmbLanguage.Enabled = bEnabled;

            //load list
            string[,] aLanguages = new string[aSupportedLanguages.Length, 2];
            for (int i = 0; i < aSupportedLanguages.Length; i++)
            {
                aLanguages[i, 0] = LMP.Data.Application
                    .GetLanguagesDisplayValue(aSupportedLanguages[i]);
                aLanguages[i, 1] = aSupportedLanguages[i];
            }
            this.cmbLanguage.SetList(aLanguages);

            //set value to default language for segment
            if (bEnabled)
            {
                string xDefault = "";
                if (!bSelectedInTree && xSupportedLanguages.Contains(xCurPrefLang))
                    xDefault = xCurPrefLang;
                else
                {
                    iPos1 = m_xCurSegObjectData.IndexOf("|Culture=");
                    if (iPos1 > -1)
                    {
                        //segment property was found, get value
                        int iPos2 = m_xCurSegObjectData.IndexOf("|", iPos1 + 1);
                        int iValueStartPos = iPos1 + 9;
                        xDefault = m_xCurSegObjectData.Substring(
                            iValueStartPos, iPos2 - iValueStartPos);
                    }
                }
                if (xDefault != "")
                    this.cmbLanguage.Value = xDefault;
                else if (xSupportedLanguages.Contains("1033"))
                    this.cmbLanguage.Value = "1033";
            }

            m_bLanguagesLoading = false;
        }
        private void LoadSegmentPreferences(string xSegmentID, bool bSelectedInTree)
        {
            try
            {
                //Avoid jerkiness when loading controls
                this.pnlPreferences.SuspendLayout();
                this.pnlPreferences.Visible = false;
                this.pnlPreferences.Controls.Clear();
                //load selected segment
                LoadSegmentXml(xSegmentID);
                //setup language dropdown
                if (this.cmbLanguage.Visible)
                {
                    string xCurPrefLang = "";
                    //Maintain selected language if switching to a related segment
                    if (!bSelectedInTree && this.cmbLanguage.Enabled)
                        xCurPrefLang = cmbLanguage.SelectedValue.ToString();
                    SetupLanguageControl(xCurPrefLang, bSelectedInTree);
                }

                if (bSelectedInTree)
                {
                    LoadRelatedSegments(xSegmentID);
                    if (m_aRelatedSegments.Count > 1)
                    {
                        this.cmbRelated.SetList(m_aRelatedSegments);
                        this.pnlRelated.Visible = true;
                    }
                    else
                    {
                        this.pnlRelated.Visible = false;
                    }
                }

                //display preference controls
                DisplayPreferenceControls();


                //enable/disable reset to default buttons
                int iID1;
                int iID2;
                LMP.Data.Application.SplitID(this.cmbOwner.Value, out iID1, out iID2);

                if (!lblNoPreferences.Visible)
                {
                    this.btnReset.Enabled = (iID1 !=
                        LMP.Data.ForteConstants.mpFirmRecordID);
                    this.btnSave.Enabled = true;
                    // GLOG : 3012 : JAB
                    // Enable the owner combo and its label.
                    this.lblOwner.Enabled = true;
                    this.cmbOwner.Enabled = true;
                }
                else
                {
                    // GLOG : 3012 : JAB
                    // Disable the reset button, the owner combo and its label 
                    // if there are no prefs.
                    this.btnReset.Enabled = false;
                    this.lblOwner.Enabled = false;
                    this.cmbOwner.Enabled = false;
                    this.lblLanguage.Enabled = false;
                    this.cmbLanguage.Enabled = false;
                    this.btnSave.Enabled = false;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                this.pnlPreferences.Visible = true;
                this.pnlPreferences.ResumeLayout();
            }
        }

        private void LoadSegmentXml(string xSegmentID)
        {
            if (xSegmentID.EndsWith(".0"))
                xSegmentID = xSegmentID.Substring(0, xSegmentID.Length - 2);
            if (!xSegmentID.Contains("."))
            {
                int iSegmentID = Int32.Parse(xSegmentID);
                //get segment def
                AdminSegmentDefs oSegs = new AdminSegmentDefs();
                AdminSegmentDef oCurSegDef = (AdminSegmentDef)oSegs.ItemFromID(iSegmentID);
                m_xCurSegmentID = oCurSegDef.ID.ToString();
                m_oCurSegDef = oCurSegDef;
            }
            else
            {
                //get segment def
                UserSegmentDefs oSegs = new UserSegmentDefs();
                UserSegmentDef oCurSegDef = (UserSegmentDef)oSegs.ItemFromID(xSegmentID);
                m_xCurSegmentID = oCurSegDef.ID;
                m_oCurSegDef = oCurSegDef;
            }
            //create xml document for xpath searchablity
            XmlDocument oCurSegXML = new XmlDocument();
            string xXML = m_oCurSegDef.XML;
            oCurSegXML.LoadXml(xXML);
            m_oCurSegXML = oCurSegXML;
			//Convert Base64 string to enable XML parsing
            if (LMP.String.IsBase64SegmentString(xXML))
                xXML = LMP.Architect.Oxml.XmlSegment.GetFlatXmlFromBase64OpcString(xXML);


            m_oNamespaceManager = new XmlNamespaceManager(oCurSegXML.NameTable);

            if (String.IsWordOpenXML(xXML))
            {
                m_oNamespaceManager.AddNamespace("w", ForteConstants.WordOpenXMLNamespace);

                string xXPath = @"//w:docVar[contains(@w:val, 'ObjectData=SegmentID=" + xSegmentID + "|')]";
                XmlNode oSegDocVar = m_oCurSegXML.SelectSingleNode(xXPath, m_oNamespaceManager);
                string xObjectData = oSegDocVar.OuterXml;
                int iPos = xObjectData.IndexOf("��ObjectData=") + 13;
                int iPos2 = xObjectData.IndexOf("��", iPos);

                if (iPos2 < 0)
                {
                    //parse to end
                    m_xCurSegObjectData = xObjectData.Substring(iPos);
                }
                else
                {
                    //parse to separator
                    m_xCurSegObjectData = xObjectData.Substring(iPos, iPos2 - iPos);
                }
            }
            else
            {
                m_xNamespacePrefix = String.GetNamespacePrefix(xXML,
                    ForteConstants.MacPacNamespace);
                m_oNamespaceManager.AddNamespace(m_xNamespacePrefix,
                    ForteConstants.MacPacNamespace);

                //get object data
                string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                    m_xCurSegmentID + "|')]";
                string xXPath = @"//" + m_xNamespacePrefix + ":mSEG" + xSegCondition;
                XmlNode oSegNode = m_oCurSegXML.SelectSingleNode(xXPath, m_oNamespaceManager);
                XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
                m_xCurSegObjectData = String.Decrypt(oObjectData.Value);
            }
        }
        private void LoadRelatedSegments(string xParentSegmentID)
        {
            //Only reload related segments when selected item in tree changes
            if (xParentSegmentID!= m_xTopLevelSegmentID)
                return;

            m_oPreferencesHash = new Hashtable();
            m_aRelatedSegments = new ArrayList();
            m_oChooserHash = new Hashtable();

            string xSegmentID = "";
            if (m_oCurSegDef is UserSegmentDef)
            {
                xSegmentID = ((UserSegmentDef)m_oCurSegDef).ID;
            }
            else
                xSegmentID = ((AdminSegmentDef)m_oCurSegDef).ID.ToString();

            m_aRelatedSegments.Add(new string[] { xSegmentID, m_oCurSegDef.DisplayName, m_oCurSegDef.TypeID.ToString() });
            m_oPreferencesHash.Add(xParentSegmentID, LoadPreferenceDefs(xParentSegmentID));
            string xXML = m_oCurSegDef.XML;
            if (LMP.String.IsBase64SegmentString(xXML))
                xXML = LMP.Architect.Oxml.XmlSegment.GetFlatXmlFromBase64OpcString(xXML);

            if (String.IsWordOpenXML(xXML))
            {
                ArrayList aChildSegments = LoadRelatedSegments_CC(m_oCurSegXML, xParentSegmentID, m_xCurSegObjectData.Contains("ExternalChildren=true"));
                if (aChildSegments.Count > 0)
                    m_aRelatedSegments.AddRange(aChildSegments);
            }
            else
            {
                ArrayList aChildSegments = LoadRelatedSegments_Xml(m_oCurSegXML, xParentSegmentID, m_xCurSegObjectData.Contains("ExternalChildren=true"));
                if (aChildSegments.Count > 0)
                    m_aRelatedSegments.AddRange(aChildSegments);
            }   

        }
        private ArrayList LoadRelatedSegments_Xml(XmlDocument oXml, string xParentSegmentID, bool bExternalChildren)
        {
            ArrayList aRelated = new ArrayList();
            string xSegmentID = xParentSegmentID;
            if (xSegmentID.EndsWith(".0"))
                xSegmentID = xSegmentID.Substring(0, xSegmentID.Length - 2);
            
            //Only Load related for Admin Segments
            if (xSegmentID.Contains("."))
                return aRelated;

            string xNSPrefix = LMP.String.GetDefaultXmlNamespacePrefix();


            //Add Variables for Child mSEGs belonging to target segment
            string xSegCondition = @"[contains(@ObjectData,'SegmentID=" +
                xSegmentID + "|')]"; //GLOG 4473
            string xXPath = @"//" + xNSPrefix + ":mSEG" + xSegCondition;
            XmlNode oParentNode = oXml.SelectSingleNode(xXPath, m_oNamespaceManager);
            if (oParentNode == null)
                return aRelated;

            XmlAttribute oParentTagID = oParentNode.Attributes["TagID"];
            string xParentTagID = oParentTagID.Value;


            //Ignore any children nodes with same SegmentID
            xXPath = @"(//" + xNSPrefix + ":mSEG[ancestor::" + xNSPrefix + ":mSEG[1]" + xSegCondition + "]" +
                "[not(contains(@ObjectData,'SegmentID=" + xSegmentID + "|'))])"; //GLOG 4473

            if (bExternalChildren)
            {
                xXPath += @"|(//" + xNSPrefix + ":mSEG[contains(@ObjectData,'ParentTagID=" +
                    xParentTagID + "|')][count(ancestor::" + xNSPrefix + ":mSEG)=0])";
            }
            XmlNodeList oChildren = oXml.SelectNodes(xXPath, m_oNamespaceManager);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                XmlAttribute oObjectData = oSegNode.Attributes["ObjectData"];
                XmlAttribute oTagID = oSegNode.Attributes["TagID"];
                string xObjectData = oObjectData.Value;
                string xTagID = oTagID.Value;
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;

                int iPos = xObjectData.IndexOf("SegmentID=");
                string xChildID = "";
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                int iChildID = Int32.Parse(xChildID);
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oSegmentDef = null;
                try
                {
                    oSegmentDef = (AdminSegmentDef)oDefs.ItemFromID(iChildID);
                }
                catch
                {
                }
                if (oSegmentDef != null)
                {
                    switch (oSegmentDef.TypeID)
                    {
                        case mpObjectTypes.PleadingCaptions:
                        case mpObjectTypes.PleadingCounsels:
                        case mpObjectTypes.AgreementSignatures:
                        case mpObjectTypes.LetterSignatures:
                        case mpObjectTypes.PleadingSignatures:
                        case mpObjectTypes.CollectionTable:
                            break;
                        default:
                            //If Author Preference segment is different from child segment in design, add Preference segment to the related segments dropdown
                            if (m_oChooserHash.ContainsKey(oSegmentDef.TypeID) && m_oChooserHash[oSegmentDef.TypeID].ToString() != iChildID.ToString())
                            {
                                try
                                {
                                    AdminSegmentDef oPrefDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(m_oChooserHash[oSegmentDef.TypeID].ToString()));
                                    //If Preference Segment doesn't exist, leave default
                                    if (oPrefDef != null)
                                    {
                                        oSegmentDef = oPrefDef;
                                    }
                                }
                                catch
                                {
                                }
                            }
                            if (Segment.DefinitionContainsAuthorPreferences(oSegmentDef.XML) || m_oChooserHash.ContainsKey(oSegmentDef.TypeID))
                            {
                                //Add to dropdown if child contains preferences, or if of a type assigned to a Chooser preference
                                aRelated.Add(new string[] { oSegmentDef.ID.ToString(), oSegmentDef.DisplayName, oSegmentDef.TypeID.ToString() });
                                m_oPreferencesHash.Add(oSegmentDef.ID, LoadPreferenceDefs(oSegmentDef.ID.ToString()));
                            }
                            break;
                    }
                    aRelated.AddRange(LoadRelatedSegments_Xml(oXml, oSegmentDef.ID.ToString(), false));
                }
            }
            return aRelated;
        }

        private ArrayList LoadRelatedSegments_CC(XmlDocument oXml, string xParentSegmentID, bool bExternalChildren)
        {
            ArrayList aRelated = new ArrayList();
            string xCCSegmentID = xParentSegmentID;
            if (xCCSegmentID.EndsWith(".0"))
                xCCSegmentID = xCCSegmentID.Substring(0, xCCSegmentID.Length - 2);
            
            //Only load related for Admin Segments
            if (xCCSegmentID.Contains("."))
                return aRelated;

            xCCSegmentID = xCCSegmentID.Replace('.', 'd');
            xCCSegmentID = xCCSegmentID.Replace('-', 'm');
            xCCSegmentID = xCCSegmentID.PadLeft(19, '0');

            //Add Variables for Child mSEGs belonging to target Content Control
            string xSegCondition = "[w:sdtPr/w:tag[contains(@w:val, '" + xCCSegmentID + "')]]";
            //Ignore any children nodes with same SegmentID
            string xXPath = @"(//w:sdt[ancestor::w:sdt[1]" + xSegCondition +
                "][w:sdtPr/w:tag[contains(@w:val,'mps') and not(contains(@w:val,'" + xCCSegmentID + "'))]])";

            if (bExternalChildren)
            {
                xXPath +=  @"|(descendant::w:sdt[count(ancestor::w:sdt)=0]" +
                    "[w:sdtPr/w:tag[contains(@w:val,'mps') and not(contains(@w:val,'" + xCCSegmentID + "'))]])";
            }
            XmlNodeList oChildren = oXml.SelectNodes(xXPath, m_oNamespaceManager);
            string xChildTagList = "";
            foreach (XmlNode oSegNode in oChildren)
            {
                //Parse Document Variable ID from Tag
                XmlAttribute oTagID = oSegNode.SelectSingleNode("w:sdtPr/w:tag", m_oNamespaceManager).Attributes["w:val"];
                string xSegVarID = oTagID.Value.Substring(3, 8);
                XmlNode oDocVarNode = oXml.SelectSingleNode(@"//w:docVar[contains(@w:name,'mpo" + xSegVarID + "')]", m_oNamespaceManager);
                XmlAttribute oVal = oDocVarNode.Attributes["w:val"];
                //Document Variable contains ObjectData for mSEG
                string xObjectData = oVal.Value.Substring(oVal.Value.IndexOf("��ObjectData=") + 13);
                //TagID at start of Doc Variable value
                string xTagID = oVal.Value.Substring(0, oVal.Value.IndexOf("��"));
                //Skip if mSEG with same TagID has already been added
                if (xChildTagList.Contains(xTagID))
                    continue;

                xChildTagList += xTagID;
                int iPos = xObjectData.IndexOf("SegmentID=");
                string xChildID = "";
                if (iPos > -1)
                {
                    iPos = iPos + @"SegmentID=".Length;
                    int iEndPos = xObjectData.IndexOf('|', iPos);
                    if (iEndPos > -1)
                    {
                        xChildID = xObjectData.Substring(iPos, iEndPos - iPos); //GLOG 4473
                    }
                    else
                    {
                        xChildID = xObjectData.Substring(iPos); //GLOG 4473
                    }
                }
                int iChildID = Int32.Parse(xChildID);
                AdminSegmentDefs oDefs = new AdminSegmentDefs();
                AdminSegmentDef oDef = null;
                try
                {
                    oDef = (AdminSegmentDef)oDefs.ItemFromID(iChildID);
                }
                catch 
                {
                }
                if (oDef != null)
                {
                    switch (oDef.TypeID)
                    {
                        case mpObjectTypes.PleadingCaptions:
                        case mpObjectTypes.PleadingCounsels:
                        case mpObjectTypes.AgreementSignatures:
                        case mpObjectTypes.LetterSignatures:
                        case mpObjectTypes.PleadingSignatures:
                        case mpObjectTypes.CollectionTable:
                            break;
                        default:
                            //If Author Preference segment is different from child segment in design, add Preference segment to the related segments dropdown
                            if (m_oChooserHash.ContainsKey(oDef.TypeID) && m_oChooserHash[oDef.TypeID].ToString() != xChildID)
                            {
                                try
                                {
                                    AdminSegmentDef oPrefDef = (AdminSegmentDef)oDefs.ItemFromID(Int32.Parse(m_oChooserHash[oDef.TypeID].ToString()));
                                    //If Preference Segment doesn't exist, leave default
                                    if (oPrefDef != null)
                                    {
                                        oDef = oPrefDef;
                                    }
                                }
                                catch
                                {
                                }
                            }
                            if (Segment.DefinitionContainsAuthorPreferences(oDef.XML) || m_oChooserHash.ContainsKey(oDef.TypeID))
                            {
                                //Add to dropdown if child contains preferences, or if of a type assigned to a Chooser preference
                                aRelated.Add(new string[] { oDef.ID.ToString(), oDef.DisplayName, oDef.TypeID.ToString() });
                                m_oPreferencesHash.Add(oDef.ID.ToString(), LoadPreferenceDefs(oDef.ID.ToString()));
                            }
                            break;
                    }
                    aRelated.AddRange(LoadRelatedSegments_CC(oXml, oDef.ID.ToString(), false));
                }
            }
            return aRelated;
        }
        public void RefreshPreferencesPanel()
        {
            //JTS: this should no longer be necessary after AutoScaling changes

            //if (this.ftvSegments.Tree.SelectedNodes.Count > 0)
            //{
            //    try
            //    {
            //        //clear preferences panel of any existing controls
            //        this.pnlPreferences.Controls.Clear();

            //        //get selected node
            //        UltraTreeNode oNode = this.ftvSegments.Tree.SelectedNodes[0];

            //        //parse key into component ids
            //        int iFolderID;
            //        int iSegmentID;
            //        this.ParseContentNode(oNode, out iFolderID, out iSegmentID);

            //        if (m_iCurSegmentID != 0)
            //        {
            //            DisplayPreferenceControls();

            //            //GLOG #8492
            //            this.cmbLanguage.Width = (this.gbxPreferences.Left + this.gbxPreferences.Width) - this.cmbLanguage.Left;
            //            this.cmbOwner.Width = this.cmbLanguage.Width;
            //        }
            //    }
            //    catch (System.Exception oE)
            //    {
            //        LMP.Error.Show(oE);
            //    }
            //}
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// handler for selection of new node on content tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ftvSegments_AfterSelect(object sender, SelectEventArgs e)
        {
            LMP.Trace.WriteValues("treeAdminSegments_AfterSelect");
            try
            {
                if (e.NewSelections.Count == 0)
                    return;

                SaveCurrentRecord(true);

                //get selected node
                UltraTreeNode oNode = e.NewSelections[0];

                //parse key into component ids
                string xFolderID;
                string xSegmentID;
                this.ParseContentNode(oNode, out xFolderID, out xSegmentID);

                //a segment node is selected
                if (m_iMode == Modes.Admin || m_iMode == Modes.Designer)
                {
                    //if the selected folder has changed,
                    //update the list of owners to display only those to which this
                    //folder has been assigned
                    if (xFolderID != m_xCurFolderID)
                    {
                        m_xCurFolderID = xFolderID;
                        m_oCurOffices = null;
                        m_oCurGroups = null;
                        m_oCurPeople = null;
                        LoadOwners();
                    }

                    //enable owner controls
                    this.cmbOwner.Enabled = true;
                    this.lblOwnerCategory.Enabled = true;
                    this.rdOffices.Enabled = true;
                    this.rdGroups.Enabled = true;
                    this.rdPeople.Enabled = true;
                    this.lblOwner.Enabled = true;
                }


                if (xSegmentID != "")
                {
                    m_xTopLevelSegmentID = xSegmentID;
                    LoadSegmentPreferences(xSegmentID, true);
                }
                else
                {
                    //clear preferences panel of any existing controls
                    this.pnlPreferences.Controls.Clear();
                    //a folder node is selected
                    if (m_iMode == Modes.Admin)
                    {
                        //disable owner controls
                        this.cmbOwner.Enabled = false;
                        this.lblOwnerCategory.Enabled = false;
                        this.rdOffices.Enabled = false;
                        this.rdGroups.Enabled = false;
                        this.rdPeople.Enabled = false;
                        this.lblOwner.Enabled = false;

                        //if the previous selection was not a member of this folder,
                        //reset the owner to firm default
                        if ((xFolderID != m_xCurFolderID) && this.cmbOwner.Enabled)
                            this.cmbOwner.SelectedIndex = 0;

                    }
                    this.btnSave.Enabled = false;
                    this.btnReset.Enabled = false;
                    this.lblLanguage.Enabled = false;
                    this.cmbLanguage.Enabled = false;

                    //hide "no preferences" label
                    this.lblNoPreferences.Visible = false;

                    //clear segment info
                    m_xCurSegmentID = "";
                    m_oCurSegDef = null;
                    m_oCurSegXML = null;
                    m_xCurSegObjectData = "";
                    m_oPrefKeyset = null;
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handles switch to offices
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdOffices_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdOffices.Checked)
                    this.OnOwnersListTypeChanged();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// handles switch to groups
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdGroups_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdGroups.Checked)
                    this.OnOwnersListTypeChanged();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// handles switch to people
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdPeople_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdPeople.Checked)
                    this.OnOwnersListTypeChanged();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// saves current preference set
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlPreferences_Leave(object sender, EventArgs e)
        {
            try
            {
                //this.SavePreferences();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// loads preferences for new owner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbOwner_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord(true);
                if ((!m_bOwnersLoading) && this.cmbOwner.Enabled &&
                    (this.cmbOwner.Value != ""))
                {
                    //get ids of selected entity
                    int iID1;
                    int iID2;
                    LMP.Data.Application.SplitID(this.cmbOwner.Value, out iID1, out iID2);

                    //if this is a new user, refresh folders
                    if (m_iMode == Modes.User)
                    {
                        int iUserID;
                        LMP.Data.Person oPerson = LMP.Data.LocalPersons
                            .GetPersonFromID(this.cmbOwner.Value);
                        if (oPerson.IsPrivate)
                            //selected author is a private person - get the owner
                            iUserID = oPerson.OwnerID;
                        else if (oPerson.IsAlias)
                            //selected author is an alias - get the linked person
                            iUserID = oPerson.LinkedPersonID;
                        else
                            //selected author is a user
                            iUserID = iID1;

                        if (iUserID != m_iUserID)
                        {
                            //refresh folders for new user
                            m_iUserID = iUserID;
                            RefreshFoldersForNewUser();
                        }
                    }

                    //load preferences
                    if (m_xCurSegmentID != "")
                        this.LoadPreferences(this.cmbOwner.Value, (ArrayList)m_oPreferencesHash[m_xCurSegmentID]);

                    // GLOG : 3012 : JAB
                    // Disable the reset button if the m_oPrefs is null.
                    //enable/disable reset buttons
                    this.btnReset.Enabled = (m_oPrefKeyset != null && iID1 !=
                        LMP.Data.ForteConstants.mpFirmRecordID);
                    this.btnSave.Enabled = (m_oPrefKeyset != null);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// loads preferences for new language
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLanguage_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord(true);

                string xCulture = this.cmbLanguage.Value;
                if (xCulture != "")
                {
                    m_iCulture = System.Int32.Parse(xCulture);
                    m_xCulture = LMP.Data.Application.GetLanguagesDisplayValue(xCulture);
                }

                if (!m_bLanguagesLoading)
                {
                    //update pref controls - there may be language field
                    //codes in control properties
                    this.UpdateForLanguage(m_xCulture);

                    //load preferences
                    this.LoadPreferences(this.cmbOwner.Value, (ArrayList)m_oPreferencesHash[m_xCurSegmentID]);

                    //enable/disable reset buttons
                    int iID1;
                    int iID2;
                    LMP.Data.Application.SplitID(this.cmbOwner.Value, out iID1, out iID2);
                    this.btnReset.Enabled = (m_oPrefKeyset != null && iID1 !=
                        LMP.Data.ForteConstants.mpFirmRecordID);
                    this.btnSave.Enabled = (m_oPrefKeyset != null);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void AuthorPreferencesManager_Leave(object sender, EventArgs e)
        {
            try
            {
                // this.SaveCurrentRecord(true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// resets preferences to firm default
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                this.ResetPreferences();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void AuthorPreferencesManager_Load(object sender, EventArgs e)
        {
            //if this is runtime, intialize
            if (!this.DesignMode)
                this.Setup();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord(false);
                if (!this.DisplayTree)
                    this.ParentForm.Close();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void AuthorPreferencesManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord(true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }

        }

        private void scAuthorPrefs_SplitterMoved(object sender, SplitterEventArgs e)
        {
            LMP.Trace.WriteValues("scAuthorPrefs_SplitterMoved");
        }
        private void pnlRelated_VisibleChanged(object sender, EventArgs e)
        {
        }
        private void cmbRelated_ValueChanged(object sender, EventArgs e)
        {
            if (m_bChangingRelated)
                return;
            try
            {
                m_bChangingRelated = true;

                if (pnlRelated.Visible && m_aRelatedSegments.Count > 1 && cmbRelated.SelectedIndex > -1)
                {
                    int iSel = cmbRelated.SelectedIndex;
                    SaveCurrentRecord(true);
					if (iSel > -1 && iSel < cmbRelated.Items.Count)
					{
	                    cmbRelated.SelectedIndex = iSel;
					}
                    string xSegmentID = cmbRelated.SelectedValue.ToString();
                    LoadSegmentPreferences(xSegmentID, false);
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
            finally
            {
                m_bChangingRelated = false;
            }
        }

        private void cmbRelated_DropDown(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord(true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        #endregion
        #region *********************helper classes*********************
        private class PreferenceSorter : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                string[] aPref1 = (string[])x;
                string[] aPref2 = (string[])y;

                int iIndex1 = int.Parse(aPref1[4]);
                int iIndex2 = int.Parse(aPref2[4]);

                if (iIndex1 == iIndex2)
                    return 0;
                else if (iIndex1 > iIndex2)
                    return 1;
                else
                    return -1;
            }

            #endregion
        }
        #endregion


    }
}