using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace LMP.Administration.Controls
{
    public partial class CustomPeopleFieldsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region *******************fields***************************
        private BindingSource m_oCustomFieldSource;
        private DataRow m_oLastRow = null;
        private string m_xValue1 = string.Empty;
        private bool m_bLoadInProgress = false;

        #endregion
        #region *******************enums****************************
        #endregion
        #region *******************constants************************
        private const string CUSTOMFIELD_DEFAULT = "Custom Field ";
        #endregion
        #region *******************constructors*********************
        public CustomPeopleFieldsManager()
        {
            InitializeComponent();
        }
        #endregion
        #region *******************event handlers*******************
        /// <summary>
        /// handles initializing data grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomPeopleFieldsManager_Load(object sender, EventArgs e)
        {
            try
            {
                m_bLoadInProgress = true;
                DataBindCustomFields();
                //subscribe to base events
                base.AfterControlLoaded +=
                    new AfterControlLoadedHandler(AdminManager_AfterControlLoaded);
                base.BeforeControlUnloaded +=
                    new BeforeControlUnLoadedHandler(AdminManager_BeforeControlUnloaded);
                m_bLoadInProgress = false;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// this handler will fire when user changes rows in grid
        /// the datarow state will then determine whether to update
        /// dataset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oCustomFieldSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //get current BindingSource datarow object
                BindingSource oBS = (BindingSource)sender;

                if (oBS.Current == null)
                    return;

                if (m_oLastRow == null)
                    m_oLastRow = ((DataRowView)oBS.Current).Row;

                if (grdCustomFields.CurrentRow == null)
                    return;

                if (this.IsValid && m_oLastRow.RowState == DataRowState.Modified)
                    if (!ValidateEntry())
                    {
                        grdCustomFields.CancelEdit();
                        this.IsValid = true;
                        return;
                    }

                //update according to rowstate if necessary
                if (m_oLastRow.RowState == DataRowState.Modified)
                    this.UpdateCustomFields();
                
                //reset last row variable
                if (oBS.Current != null)
                    m_oLastRow = ((DataRowView)oBS.Current).Row;
            }
            catch (Exception oE)
            {
                if (oE is LMP.Exceptions.UINodeException)
                {
                    MessageBox.Show(LMP.Resources.GetLangString("Error_CouldNotSaveChangesToValueSet"),
                       LMP.String.MacPacProductName,
                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    grdCustomFields.CancelEdit();
                    m_oLastRow.RejectChanges();  
                    this.IsValid = false;
                }
                else
                    LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires after control is loaded into container
        /// enables proper refresh/configuration of property grid cells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires if control is about to be unloaded by selecting another
        /// manager - calls SaveCurrentRecord to commit and save any 
        /// pending edits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handler captures current value of row cells before any edits
        /// necessary because we are working with column names, and the 
        /// update query requires the unedited version
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdCustomFields_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            m_xValue1 = grdCustomFields.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
        }
        /// we require an entry - no duplicates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <summary>
        private void grdCustomFields_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (m_bLoadInProgress)
                    return;

                if (m_oLastRow == null)
                    return;

                bool bValid = true;
                
                if (e.FormattedValue.ToString() != grdCustomFields.CurrentCell.FormattedValue.ToString())
                    bValid = ValidateEntry();

                if ((m_oLastRow.RowState == DataRowState.Unchanged ||
                    m_oLastRow.RowState == DataRowState.Deleted) &&
                    bValid)
                    return;

                e.Cancel = !bValid;

                if (!bValid)
                    grdCustomFields.CancelEdit();

                this.IsValid = bValid;
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdCustomFields_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                if (e.Exception is IndexOutOfRangeException
                    || e.Exception is FormatException)
                    return;
                else
                    e.ThrowException = true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnDeleteCustomFields_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteCustomField();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnNewCustomFields_Click(object sender, EventArgs e)
        {
            try
            {
                //save any edits made to a current row before adding a new one
                if (grdCustomFields.CurrentRow != null && grdCustomFields.IsCurrentRowDirty)
                {
                    //cancel edit if name isn't valid
                    if (!ValidateEntry())
                    {
                        grdCustomFields.CancelEdit();
                        return;
                    }
                    //commit any pending edits
                    grdCustomFields.EndEdit();
                    
                    //reset the global last row pointer & update the db
                    m_oLastRow = ((DataRowView)m_oCustomFieldSource.Current).Row;
                    UpdateCustomFields();
                }
                AddNewCustomField();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *******************private methods******************
        /// <summary>
        /// displays loads value set of ID -1 into grid
        /// </summary>
        private void DataBindCustomFields()
        {
            try
            {
                //we don't want any validation while the grid is loading
                grdCustomFields.CellValidating -= grdCustomFields_CellValidating;
 
                //get data set for Value Set items
                DataSet oDS = GetDataSet();

                //get data table
                DataTable oDT = new DataTable();

                //initialize binding source
                if (m_oCustomFieldSource == null)
                    m_oCustomFieldSource = new BindingSource();

                //add field for custom fields
                DataColumn colCF = new DataColumn("CustomPeopleField");
                colCF.DataType = System.Type.GetType("System.String");
                oDT.Columns.Add(colCF);

                for (int i = 24; i < oDS.Tables[0].Columns.Count; i++)
                {
                    string xColName = oDS.Tables[0].Columns[i].ColumnName;
                    if (!LMP.Data.ForteConstants.mpRequiredDBFields.Contains("|" + xColName + "|"))
                        oDT.Rows.Add(xColName);
                }

                oDT.AcceptChanges();
                
                m_oCustomFieldSource.DataSource = oDT;

                //bind dataset to value set grid
                this.grdCustomFields.DataSource = m_oCustomFieldSource;

                //display column names 
                this.grdCustomFields.Columns[0].HeaderText = "Custom Field Name";

                this.grdCustomFields.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill; 
                this.grdCustomFields.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

                //subscribe to position changed event - this will be the caller
                //for the update routine when navigating through grid
                m_oCustomFieldSource.PositionChanged += new EventHandler(m_oCustomFieldSource_PositionChanged);
                
                //resubscribe to Validating event
                grdCustomFields.CellValidating +=new DataGridViewCellValidatingEventHandler(grdCustomFields_CellValidating);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotBindDataToControl"), oE);
            }
        }
        /// <summary>
        /// sets up new record with default text and new index value
        /// </summary>
        private void AddNewCustomField()
        {
            //unsubscribe from validation & position_changed events
            grdCustomFields.CellValidating -= grdCustomFields_CellValidating;
            m_oCustomFieldSource.PositionChanged -= m_oCustomFieldSource_PositionChanged; 
            
            //make sure the proposed name does not already exist
            string xProposedName = GetNextCustomFieldName();;

            DataSet oDS = GetDataSet();
            if (oDS.Tables[0].Columns.Contains(xProposedName))
                xProposedName = xProposedName + "_" + (oDS.Tables[0].Columns.Count - 23).ToString();

            //GLOG 8220: Connect to ForteLocalPeople.mdb in Local mode
            OleDbConnection oCnn = LMP.MacPac.MacPacImplementation.IsFullLocal ? LMP.Data.PublicPersons.PeopleDBConnection : LMP.Data.LocalConnection.ConnectionObject;
            bool bCloseConnection = false;
            if ((oCnn.State & ConnectionState.Open) == 0)
            {
                oCnn.Open();
                bCloseConnection = true;
            }

            //add the new column
            System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();
            oCmd.Connection = oCnn;
            oCmd.CommandText = "ALTER TABLE " + "People" + " ADD [" + xProposedName + "] Text(255);";
            oCmd.CommandType = CommandType.Text;
            oCmd.ExecuteNonQuery();

            if (bCloseConnection)
                oCnn.Close();

            //update the data source, which will update the grid
            ((DataTable)m_oCustomFieldSource.DataSource).Rows.Add(xProposedName);
            
            //reset lastrow variable & save the new record
            m_oLastRow = ((DataRowView)m_oCustomFieldSource[grdCustomFields.Rows.Count - 1]).Row;

            m_oLastRow.AcceptChanges();

            //select the new row
            grdCustomFields.CurrentCell = grdCustomFields.Rows[grdCustomFields.Rows.Count - 1].Cells[0];
            grdCustomFields.BeginEdit(true);

            //resubscribe to the event handlers
            grdCustomFields.CellValidating +=new DataGridViewCellValidatingEventHandler(grdCustomFields_CellValidating);
            m_oCustomFieldSource.PositionChanged += new EventHandler(m_oCustomFieldSource_PositionChanged);

        }
        /// <summary>
        /// this updates an existing record
        /// </summary>
        /// <param name="p"></param>
        private void UpdateCustomFields()
        {
            //collect the old cell values for the currently edited row
            string xEditedValue1 = m_oLastRow.ItemArray.GetValue(0).ToString();

            try
            {
                DataSet oDS = GetDataSet();
                
                //do nothing if edited value exists
                if (oDS.Tables[0].Columns.Contains(xEditedValue1))
                    return;

                //modify the column name - we can't do this directly so we add a new column
                //copy any data into it and then delete the old column
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();
                //GLOG 8220: Connect to ForteLocalPeople.mdb in Local mode
                OleDbConnection oCnn = LMP.MacPac.MacPacImplementation.IsFullLocal ? LMP.Data.PublicPersons.PeopleDBConnection : LMP.Data.LocalConnection.ConnectionObject;
                bool bCloseConnection = false;
                if ((oCnn.State & ConnectionState.Open) == 0)
                {
                    oCnn.Open();
                    bCloseConnection = true;
                }
                oCmd.Connection = oCnn;
                //add new field to table
                oCmd.CommandText = "ALTER TABLE People ADD [" + xEditedValue1 + "] Text(255);";
                oCmd.CommandType = CommandType.Text;
                oCmd.ExecuteNonQuery();
              
                //copy any existing data
                oCmd.CommandText = "UPDATE People SET [" + xEditedValue1 + "] = [" + m_xValue1 + "];";
                oCmd.ExecuteNonQuery();
               
                //delete the original column
                oCmd.CommandText = "ALTER TABLE People DROP COLUMN [" + m_xValue1 + "];";
                oCmd.ExecuteNonQuery();
               
                m_oLastRow.AcceptChanges();
                if (bCloseConnection)
                    oCnn.Close();

                //JTS 12/09/08: Update People sync queries with current field definitions
                LMP.Data.Application.UpdatePeopleRelatedQueries(LMP.Data.LocalConnection.ConnectionObject);
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// returns a people table dataset with 0 records
        /// </summary>
        /// <param name="xSetID"></param>
        /// <returns>DataSet</returns>
        private DataSet GetDataSet()
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();

                //GLOG 8220
                if (LMP.MacPac.MacPacImplementation.IsFullLocal)
                    oCmd.Connection = LMP.Data.PublicPersons.PeopleDBConnection;
                else
                    oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;

                oCmd.CommandText = "SELECT * FROM People WHERE ID1 = 0;";
                oCmd.CommandType = CommandType.Text;

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);
                OleDbCommandBuilder oCmdBuilder = new OleDbCommandBuilder(oAdapter);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    oAdapter.MissingMappingAction = MissingMappingAction.Passthrough;
                    oAdapter.MissingSchemaAction = MissingSchemaAction.Add;
 
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
                return oDS;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }
        /// <summary>
        /// deletes the currently selected custom field
        /// </summary>
        private void DeleteCustomField()
        {
            //do nothing if user in new row and no edits made
            if (grdCustomFields.CurrentRow == null || grdCustomFields.CurrentRow.IsNewRow)
                return;

            if (grdCustomFields.CurrentRow.Selected == false)
                grdCustomFields.CurrentRow.Selected = true;

            //warn user they are about to delete a ValueSet item
            DialogResult oChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Message_DeleteEntry"),
                LMP.String.MacPacProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (oChoice == DialogResult.Yes)
            {
                m_xValue1 = grdCustomFields.CurrentRow.Cells[0].FormattedValue.ToString();

                //make sure required values are valid
                if (m_xValue1 != "")
                {
                    System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();
                    //GLOG 8220: Connect to ForteLocalPeople.mdb in Local mode
                    OleDbConnection oCnn = LMP.MacPac.MacPacImplementation.IsFullLocal ? LMP.Data.PublicPersons.PeopleDBConnection : LMP.Data.LocalConnection.ConnectionObject;
                    bool bCloseConnection = false;
                    if ((oCnn.State & ConnectionState.Open) == 0)
                    {
                        oCnn.Open();
                        bCloseConnection = true;
                    }

                    oCmd.Connection = oCnn;

                    //delete the specified column
                    oCmd.CommandText = "ALTER TABLE People DROP COLUMN [" + m_xValue1 + "];";
                    oCmd.ExecuteNonQuery();
                    
                    //remove grid row
                    this.grdCustomFields.Rows.RemoveAt(this.grdCustomFields.CurrentRow.Index);
                    
                    //commit changes to binding source
                    ((DataTable)m_oCustomFieldSource.DataSource).AcceptChanges();
                    if (bCloseConnection)
                        oCnn.Close();
                    //JTS 12/09/08: Update People sync queries with current field definitions
                    LMP.Data.Application.UpdatePeopleRelatedQueries(LMP.Data.LocalConnection.ConnectionObject);
                }
                else
                {
                    //user has started a new entry - simply cancel edit
                    grdCustomFields.CancelEdit();
                }
            }
        }
        
        private bool ValidateEntry()
        {
            ////if (m_oLastRow.RowState == DataRowState.Added || m_oLastRow.RowState == DataRowState.Detached)
            string xValue1 = grdCustomFields.CurrentRow.Cells[0].EditedFormattedValue.ToString();

            DataSet oDS = GetDataSet();

            //do nothing if edited value exists
            if (oDS.Tables[0].Columns.Contains(xValue1))
            {
                //msg user and cancel navigation
                MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateEntryNotAllowed"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.IsValid = false;
                return false;
            }

            //reset IsValid so navigation to another Manager 
            //can be handled by the Main form
            if (xValue1 == "")
            {
                //missing value for Column 1, inform user
                MessageBox.Show(LMP.Resources.GetLangString("Msg_ListEntryValue1Required"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.IsValid = false;
                return false;
            }
            
            this.IsValid = true;
            return true;
        }
        private string GetNextCustomFieldName()
        {
            //get data set for Value Set items
            DataSet oDS = GetDataSet();
            return CUSTOMFIELD_DEFAULT + (oDS.Tables[0].Columns.Count - 23).ToString();
        }
        #endregion
        #region *******************properties***********************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region *******************overrides************************
        /// <summary>
        /// handles updates if containing form is closed
        /// </summary>
        public override void SaveCurrentRecord()
        {
            //user has deleted last last record - nothing to save
            //so just exit
            if (m_oLastRow == null)
                return;

            //commit any pending edits
            if (m_oLastRow.RowState == DataRowState.Modified)
                UpdateCustomFields();
        }
        #endregion
    }
}
