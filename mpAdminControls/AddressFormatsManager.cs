using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using LMP.Data;
using System.Windows.Forms;

namespace LMP.Administration.Controls
{
    public partial class AddressFormatsManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region ********************constructors******************
        public AddressFormatsManager()
        {
            InitializeComponent();
        }
        #endregion
        #region ********************fields******************
        private BindingSource m_oFormatsSource;
        private DataRow m_oLastRow = null;
 
        private const string DEF_ADDRESSTOKENSTRING = "[StandardAddress]";
        private const string DEF_FORMATNAME = "New Address Format";

        #endregion
        #region ********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region ********************methods******************
        /// <summary>
        /// binds data to AddressFormats properties grid
        /// </summary>
        public void DataBindGridProperties(int iCurIndex)
        {
            DataTable oDT = new DataTable();
            oDT.Columns.Add("PropertyName");
            oDT.Columns.Add("Value");
            
            //we're adding new record or we have an empty formats table
            //to begin with or because of deleting last record - clear grid
            if (m_oLastRow.RowState == DataRowState.Detached ||
                grdAddressFormats.Rows.Count == 0)
            {
                grdAddressFormats.Focus();
                grdProperties.DataSource = oDT;
                return;
            }

            //refresh for existing records
            oDT.Rows.Add("Show In Address Macro", grdAddressFormats.Rows[iCurIndex].Cells[2].FormattedValue.ToString());
            
            //ensure default value for enum item
            string xFNValue = grdAddressFormats.Rows[iCurIndex].Cells[3].Value == null ? "1" :
                grdAddressFormats.Rows[iCurIndex].Cells[3].Value.ToString();
            
            //add remaining rows
            oDT.Rows.Add("Firm Name Format", xFNValue);
            oDT.Rows.Add("Include Slogan", grdAddressFormats.Rows[iCurIndex].Cells[4].FormattedValue.ToString());

            //ensure default for token stirng
            string xToken = grdAddressFormats.Rows[iCurIndex].Cells[5].FormattedValue.ToString() == "" ? "[StandardAddress]" :
                grdAddressFormats.Rows[iCurIndex].Cells[5].FormattedValue.ToString();
            oDT.Rows.Add("Address Token String", xToken);
            
            oDT.Rows.Add("Delimiter Replacement", grdAddressFormats.Rows[iCurIndex].Cells[6].FormattedValue.ToString());

            //Bind the DataTable of properties to grdProperties
            this.grdProperties.DataSource = oDT;

            //set combo box lookups for boolean values
            string[] aTrueFalse = { "True", "False" };
            
            grdProperties.Rows[0].Cells[1] = new DataGridViewComboBoxCell();
            ((DataGridViewComboBoxCell)grdProperties.Rows[0].Cells[1]).DataSource = aTrueFalse;

            grdProperties.Rows[2].Cells[1] = new DataGridViewComboBoxCell();
            ((DataGridViewComboBoxCell)grdProperties.Rows[2].Cells[1]).DataSource = aTrueFalse;

            //set firm name enum lookup
            oDT = new DataTable();
            oDT.Columns.Add("Display");
            oDT.Columns.Add("Value");

            oDT.Rows.Add("None", "0");
            oDT.Rows.Add("Proper Case", "1");
            oDT.Rows.Add("Upper Case", "2");

            grdProperties.Rows[1].Cells[1] = new DataGridViewComboBoxCell();
            DataGridViewComboBoxCell oCombo = (DataGridViewComboBoxCell)grdProperties.Rows[1].Cells[1];
            oCombo.DataSource = oDT;
            oCombo.DisplayMember = "Display";
            oCombo.ValueMember = "Value";

            //Set the display properties of the grid
            this.grdProperties.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.grdProperties.Columns[0].ReadOnly = true;
            this.grdProperties.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.grdProperties.Columns[1].FillWeight = 50;
        }
        /// <summary>
        /// binds data to AddressFormats grid
        /// </summary>
        public void DataBindGridAddressFormats(bool bSortByID)
        {
            AddressFormats oFormats = new AddressFormats();
            DataTable oDT = new DataTable();
            
            //add binding source
            m_oFormatsSource = new BindingSource();
 
            //subscribe to position changed handler
            //this will handle updating/adding records
            m_oFormatsSource.PositionChanged +=
                new EventHandler(m_oFormatsSource_PositionChanged);

            //Fill DataTable with AddressFormats dataset 
            oDT = oFormats.ToDataSet().Tables[0];

            //Fill BindingSource object
            m_oFormatsSource.DataSource = oDT;

            //Bind the BindingSource to grdAddressFormats
            this.grdAddressFormats.DataSource = m_oFormatsSource;

            //hide unwanted columns
            for (int i = 0; i < grdAddressFormats.Columns.Count; i++)
                if (i != 1)
                    grdAddressFormats.Columns[i].Visible = false;
            
            //Set the display properties of grdAddressFormats
            this.grdAddressFormats.Columns[1].AutoSizeMode = 
                DataGridViewAutoSizeColumnMode.Fill;

            //sort by ID if specified
            if (bSortByID)
                this.grdAddressFormats.Sort(grdAddressFormats.Columns["ID"], ListSortDirection.Ascending);

        }
        /// <summary>
        /// adds/updates format
        /// </summary>
        /// <param name="bCreateNew"></param>
        private void UpdateAddressFormat(bool bCreateNew)
        {
            AddressFormats oFormats = new AddressFormats();
            AddressFormat oFormat = null;
            DataGridViewCellCollection oCells = null;
            
            if (grdAddressFormats.CurrentRow != null)
                oCells = grdAddressFormats.CurrentRow.Cells;

            //create new or retrieve existing format
            if (bCreateNew)
            {
                oFormat = (AddressFormat)oFormats.Create();
                oFormat.Name = DEF_FORMATNAME;
                oFormat.ShowInMacro = false;
                oFormat.IncludeSlogan = false;
                oFormat.DelimiterReplacement = "";
                oFormat.FirmNameFormat = mpFirmNameFormats.ProperCase;
                oFormat.AddressTokenString = DEF_ADDRESSTOKENSTRING;
            }
            else
            {
                //assign values from grid storage
                oFormat = (AddressFormat)oFormats.ItemFromID(Int32.Parse(oCells[0].FormattedValue.ToString()));
                oFormat.Name = oCells[1].FormattedValue.ToString();
                oFormat.ShowInMacro = Boolean.Parse(oCells[2].FormattedValue.ToString());
                oFormat.IncludeSlogan = Boolean.Parse(oCells[4].FormattedValue.ToString());
                oFormat.DelimiterReplacement = oCells[6].FormattedValue.ToString();
                //set default value for firm name format if creating new record
                oFormat.FirmNameFormat = oCells[3].Value.ToString() == "" ? mpFirmNameFormats.ProperCase :
                    (mpFirmNameFormats)(Int32.Parse(oCells[3].Value.ToString()));
                //address token string is required - set default 
                oFormat.AddressTokenString = oCells[5].FormattedValue.ToString() == "" ? DEF_ADDRESSTOKENSTRING :
                    oCells[5].FormattedValue.ToString();
            }

            try
            {
                //save new/updated format
                oFormats.Save(oFormat);

                if (bCreateNew)
                {
                    //reload formats grid, select newly added record for editing
                    DataBindGridAddressFormats(true);
                    grdAddressFormats.CurrentCell = grdAddressFormats.Rows[grdAddressFormats.Rows.Count - 1].Cells["Name"];
                    grdAddressFormats.BeginEdit(true);
                }
                else
                    //accept changes - this will reset row state for edits
                    m_oLastRow.AcceptChanges();
            }
            catch (Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        /// <summary>
        /// Delete the currently selected courier record
        /// from the DB and grdCouriers
        /// </summary>
        private void DeleteFormatRecord()
        {
            //exit if we're in new row and no edits have been made
            if (m_oFormatsSource != null && 
                m_oLastRow.RowState == DataRowState.Detached &&
                grdAddressFormats.CurrentRow.IsNewRow)
                return;
            
            //Confirm that the user intends to delete this record.
            DialogResult oChoice = MessageBox.Show(
                    LMP.Resources.GetLangString("Message_DeleteEntry"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            
            if (oChoice == DialogResult.Yes)
            {
                //if in in new row cancel any edits and simply return without messaging
                if (m_oLastRow.RowState == DataRowState.Detached)
                {
                    if (grdAddressFormats.CurrentCell.EditedFormattedValue.ToString() != "")
                    {
                        grdAddressFormats.CancelEdit();
                    }
                    return;
                }
            
                //Get the currently selected courier record
                AddressFormats oFormats = new AddressFormats();
                AddressFormat oFormat = (AddressFormat)oFormats.ItemFromID(Int32.Parse(
                    this.grdAddressFormats.CurrentRow.Cells[0].FormattedValue.ToString()));

                try
                {
                    //Delete the currently selected courier record
                    oFormats.Delete(oFormat.ID);
                }
                catch
                {
                    //This try/catch block is here because of an error
                    //generated as a result of a missing sproc: spObjectAssignmentsDeleteByObjectIDs
                }

                int iRow = Math.Max(grdAddressFormats.CurrentRow.Index - 1, 0); 
                
                //rebind formats grid 
                DataBindGridAddressFormats(false);

                //we've deleted the last record - position changed handler
                //won't fire, so we'll force property grid to refresh
                if (grdAddressFormats.CurrentRow == null)
                {
                    DataBindGridProperties(0);
                    m_oLastRow = null;
                }
                else
                    //reset current row
                    grdAddressFormats.CurrentCell = 
                        grdAddressFormats.Rows[iRow].Cells[1];
            }
        }
        /// <summary>
        /// handles updates if containing form is closed
        /// </summary>
        public override void SaveCurrentRecord()
        {
            //user has deleted last last record - nothing to save
            //so just exit
            if (m_oLastRow == null)
                return;
            
            //commit any pending edits
            if (m_oLastRow.RowState == DataRowState.Modified)
                UpdateAddressFormat(false);
        }
        #endregion
        #region ********************event handlers******************
        private void AddressFormatsManager_Load(object sender, System.EventArgs e)
        {
            try
            {
                DataBindGridAddressFormats(false);

                //subscribe to base events
                base.AfterControlLoaded += 
                    new AfterControlLoadedHandler(AdminManager_AfterControlLoaded);
                base.BeforeControlUnloaded +=
                    new BeforeControlUnLoadedHandler(AdminManager_BeforeControlUnloaded);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires after control is loaded into container
        /// enables proper refresh/configuration of property grid cells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {
                //set current row for grid and set module level last row variable
                if (grdAddressFormats.Rows.Count > 0)
                {
                    grdAddressFormats.CurrentCell = grdAddressFormats.Rows[0].Cells[1];
                    m_oLastRow = ((DataRowView)m_oFormatsSource.Current).Row;  
                    
                    //bind property grid after control has been loaded 
                    //onto its container
                    DataBindGridProperties(0);
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires if control is about to be unloaded by selecting another
        /// manager - calls SaveCurrentRecord to commit and save any 
        /// pending edits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
                SaveCurrentRecord();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// updates changes to properties
        /// this event will fire after all changes have
        /// been committed to the grids and datasources
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_oFormatsSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                //get current BindingSource datarow object
                BindingSource oBS = (BindingSource)sender;

                if (m_oLastRow == null)
                    m_oLastRow = ((DataRowView)oBS.Current).Row;

                if (grdAddressFormats.CurrentRow == null)
                    return;

                //update format if datarow has been modified
                if (grdAddressFormats.CurrentCell.EditedFormattedValue.ToString() != "" &&
                    !grdAddressFormats.CurrentRow.IsNewRow)  
                {
                    SaveCurrentRecord();
                }

                //reset last row variable
                if (oBS.Current != null) 
                    m_oLastRow = ((DataRowView)oBS.Current).Row;
                
                //rebind properties grid
                if (oBS.Position > -1)
                    DataBindGridProperties(oBS.Position);
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// transfers edits from property grid to 
        /// grdAddressFormats for storage
        /// grdAddressFormats will handle
        /// updating of AddressFormats
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            string xValue = "";

            //set row state flag if there has been an edit - this will ensure
            //record gets saved if UpdateAddress gets called from SaveCurrentRecord
            //when form is closed or another Manager is selected.
            if (grdProperties.CurrentCell.EditedFormattedValue.ToString() != "" &&
                m_oLastRow.RowState == DataRowState.Unchanged)
                m_oLastRow.SetModified();  
            
            //column index of storage grid will be property grid row + 2
            int iColIndex = grdProperties.CurrentCell.RowIndex + 2;

            //check for enum values from Firm Name lookup - cell value will
            //be the enum value rather than the grid cell formatted value
            if (grdProperties.CurrentCell.Value.ToString() !=
                grdProperties.CurrentCell.FormattedValue.ToString())
                xValue = grdProperties.CurrentCell.Value.ToString();
            else
                xValue = grdProperties.CurrentCell.EditedFormattedValue.ToString();

            //store changes in grdAddressFormats
            if (grdAddressFormats.CurrentRow != null)
                grdAddressFormats.CurrentRow.Cells[iColIndex].Value = xValue;
        }
        /// <summary>
        /// this handler enables tabbing through prop data fields only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdProperties_CellEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            try
            {
                if (grdProperties.Focused)
                {
                    if (grdProperties.CurrentCellAddress.X == 0 &&
                       grdProperties.CurrentCellAddress.Y == 0)
                        return;

                    //programmatically resetting current cell errors
                    //we can use sendkeys instead
                    if (e.ColumnIndex == 0)
                        System.Windows.Forms.SendKeys.Send("{TAB}");
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles delete button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteFormatRecord();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// handles creation of new address format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                //commit changes for any outstanding edits to properties grid
                //save to db, then create new record
                if (grdAddressFormats.Rows.Count > 0)
                {
                    grdProperties.EndEdit();
                    ((DataTable)grdProperties.DataSource).AcceptChanges();
                    grdAddressFormats.EndEdit();
                    m_oFormatsSource.EndEdit();
                    this.UpdateAddressFormat(false);
                }
                
                this.UpdateAddressFormat(true);
                if (grdAddressFormats.Rows.Count == 1)
                    DataBindGridProperties(0);

            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// enables deletion via delete key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdAddressFormats_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.DeleteFormatRecord();
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// validate to prevent deletion of format name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdAddressFormats_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (e.FormattedValue.ToString() == "" &&
                    !grdAddressFormats.CurrentRow.IsNewRow)
                {
                    e.Cancel = true;
                    this.IsValid = false;
                    //alert user
                    MessageBox.Show(LMP.Resources.GetLangString("Error_EmptyValueNotAllowed"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    this.IsValid = true;

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion

        private void grdAddressFormats_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }

        }
        //GLOG : 8241 : JSW
        //tabbing moves from row to row for exising records
        private void grdAddressFormats_TabPressed(object sender, LMP.Controls.TabPressedEventArgs e)
        {
            int iRowIndex;

            try
            {
                iRowIndex = this.grdAddressFormats.CurrentRow.Index;
                if ((grdAddressFormats.Rows.Count - 1) > iRowIndex)
                    this.grdAddressFormats.CurrentCell = this.grdAddressFormats.Rows[iRowIndex + 1].Cells[1];
                else
                    this.grdAddressFormats.CurrentCell = this.grdAddressFormats.Rows[0].Cells[1];
            }
            catch { }
        }
     }
}

