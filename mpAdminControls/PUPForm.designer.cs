namespace LMP.Administration
{
    partial class PUPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtSourceServer = new System.Windows.Forms.TextBox();
            this.txtSourceDatabase = new System.Windows.Forms.TextBox();
            this.lblSourceServer = new System.Windows.Forms.Label();
            this.lblSourceDatabase = new System.Windows.Forms.Label();
            this.grpDB = new System.Windows.Forms.GroupBox();
            this.grpTarget = new System.Windows.Forms.GroupBox();
            this.txtTargetDatabase = new System.Windows.Forms.TextBox();
            this.txtTargetServer = new System.Windows.Forms.TextBox();
            this.lblTargetDatabase = new System.Windows.Forms.Label();
            this.lblTargetServer = new System.Windows.Forms.Label();
            this.chkPublishImmediately = new System.Windows.Forms.CheckBox();
            this.chkUpdateLicenses = new System.Windows.Forms.CheckBox();
            this.grpIISServer = new System.Windows.Forms.GroupBox();
            this.txtMacPacApplicationName = new System.Windows.Forms.TextBox();
            this.txtIISServer = new System.Windows.Forms.TextBox();
            this.lblServerAppName = new System.Windows.Forms.Label();
            this.lblIISServer = new System.Windows.Forms.Label();
            this.grpDB.SuspendLayout();
            this.grpTarget.SuspendLayout();
            this.grpIISServer.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(152, 313);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(77, 27);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(235, 313);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 27);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // txtSourceServer
            // 
            this.txtSourceServer.Location = new System.Drawing.Point(79, 33);
            this.txtSourceServer.Name = "txtSourceServer";
            this.txtSourceServer.Size = new System.Drawing.Size(186, 22);
            this.txtSourceServer.TabIndex = 1;
            // 
            // txtSourceDatabase
            // 
            this.txtSourceDatabase.Location = new System.Drawing.Point(79, 72);
            this.txtSourceDatabase.Name = "txtSourceDatabase";
            this.txtSourceDatabase.Size = new System.Drawing.Size(186, 22);
            this.txtSourceDatabase.TabIndex = 3;
            // 
            // lblSourceServer
            // 
            this.lblSourceServer.AutoSize = true;
            this.lblSourceServer.Location = new System.Drawing.Point(12, 37);
            this.lblSourceServer.Name = "lblSourceServer";
            this.lblSourceServer.Size = new System.Drawing.Size(43, 15);
            this.lblSourceServer.TabIndex = 0;
            this.lblSourceServer.Text = "&Server:";
            // 
            // lblSourceDatabase
            // 
            this.lblSourceDatabase.AutoSize = true;
            this.lblSourceDatabase.Location = new System.Drawing.Point(12, 75);
            this.lblSourceDatabase.Name = "lblSourceDatabase";
            this.lblSourceDatabase.Size = new System.Drawing.Size(57, 15);
            this.lblSourceDatabase.TabIndex = 2;
            this.lblSourceDatabase.Text = "&Database:";
            // 
            // grpDB
            // 
            this.grpDB.Controls.Add(this.txtSourceDatabase);
            this.grpDB.Controls.Add(this.txtSourceServer);
            this.grpDB.Controls.Add(this.lblSourceDatabase);
            this.grpDB.Controls.Add(this.lblSourceServer);
            this.grpDB.Location = new System.Drawing.Point(504, 139);
            this.grpDB.Name = "grpDB";
            this.grpDB.Size = new System.Drawing.Size(279, 122);
            this.grpDB.TabIndex = 0;
            this.grpDB.TabStop = false;
            this.grpDB.Text = "Source Database";
            this.grpDB.Visible = false;
            // 
            // grpTarget
            // 
            this.grpTarget.Controls.Add(this.txtTargetDatabase);
            this.grpTarget.Controls.Add(this.txtTargetServer);
            this.grpTarget.Controls.Add(this.lblTargetDatabase);
            this.grpTarget.Controls.Add(this.lblTargetServer);
            this.grpTarget.Enabled = false;
            this.grpTarget.Location = new System.Drawing.Point(20, 65);
            this.grpTarget.Name = "grpTarget";
            this.grpTarget.Size = new System.Drawing.Size(292, 107);
            this.grpTarget.TabIndex = 4;
            this.grpTarget.TabStop = false;
            // 
            // txtTargetDatabase
            // 
            this.txtTargetDatabase.Location = new System.Drawing.Point(79, 65);
            this.txtTargetDatabase.Name = "txtTargetDatabase";
            this.txtTargetDatabase.Size = new System.Drawing.Size(200, 22);
            this.txtTargetDatabase.TabIndex = 5;
            // 
            // txtTargetServer
            // 
            this.txtTargetServer.Location = new System.Drawing.Point(79, 26);
            this.txtTargetServer.Name = "txtTargetServer";
            this.txtTargetServer.Size = new System.Drawing.Size(200, 22);
            this.txtTargetServer.TabIndex = 3;
            // 
            // lblTargetDatabase
            // 
            this.lblTargetDatabase.AutoSize = true;
            this.lblTargetDatabase.Location = new System.Drawing.Point(12, 68);
            this.lblTargetDatabase.Name = "lblTargetDatabase";
            this.lblTargetDatabase.Size = new System.Drawing.Size(57, 15);
            this.lblTargetDatabase.TabIndex = 4;
            this.lblTargetDatabase.Text = "&Database:";
            // 
            // lblTargetServer
            // 
            this.lblTargetServer.AutoSize = true;
            this.lblTargetServer.Location = new System.Drawing.Point(12, 30);
            this.lblTargetServer.Name = "lblTargetServer";
            this.lblTargetServer.Size = new System.Drawing.Size(43, 15);
            this.lblTargetServer.TabIndex = 2;
            this.lblTargetServer.Text = "&Server:";
            // 
            // chkPublishImmediately
            // 
            this.chkPublishImmediately.BackColor = System.Drawing.SystemColors.Control;
            this.chkPublishImmediately.Location = new System.Drawing.Point(29, 39);
            this.chkPublishImmediately.Name = "chkPublishImmediately";
            this.chkPublishImmediately.Size = new System.Drawing.Size(272, 19);
            this.chkPublishImmediately.TabIndex = 1;
            this.chkPublishImmediately.Text = "&Publish updated data to Forte network database";
            this.chkPublishImmediately.UseVisualStyleBackColor = false;
            this.chkPublishImmediately.CheckedChanged += new System.EventHandler(this.chkPublishImmediately_CheckedChanged);
            // 
            // chkUpdateLicenses
            // 
            this.chkUpdateLicenses.AutoSize = true;
            this.chkUpdateLicenses.Location = new System.Drawing.Point(29, 14);
            this.chkUpdateLicenses.Name = "chkUpdateLicenses";
            this.chkUpdateLicenses.Size = new System.Drawing.Size(152, 19);
            this.chkUpdateLicenses.TabIndex = 0;
            this.chkUpdateLicenses.Text = "&Update Attorney Licenses";
            this.chkUpdateLicenses.UseVisualStyleBackColor = true;
            // 
            // grpIISServer
            // 
            this.grpIISServer.Controls.Add(this.txtMacPacApplicationName);
            this.grpIISServer.Controls.Add(this.txtIISServer);
            this.grpIISServer.Controls.Add(this.lblServerAppName);
            this.grpIISServer.Controls.Add(this.lblIISServer);
            this.grpIISServer.Enabled = false;
            this.grpIISServer.Location = new System.Drawing.Point(20, 183);
            this.grpIISServer.Name = "grpIISServer";
            this.grpIISServer.Size = new System.Drawing.Size(292, 118);
            this.grpIISServer.TabIndex = 10;
            this.grpIISServer.TabStop = false;
            this.grpIISServer.Text = "Forte Server Application";
            // 
            // txtMacPacApplicationName
            // 
            this.txtMacPacApplicationName.Location = new System.Drawing.Point(111, 72);
            this.txtMacPacApplicationName.Name = "txtMacPacApplicationName";
            this.txtMacPacApplicationName.Size = new System.Drawing.Size(168, 22);
            this.txtMacPacApplicationName.TabIndex = 9;
            // 
            // txtIISServer
            // 
            this.txtIISServer.Location = new System.Drawing.Point(111, 33);
            this.txtIISServer.Name = "txtIISServer";
            this.txtIISServer.Size = new System.Drawing.Size(168, 22);
            this.txtIISServer.TabIndex = 7;
            // 
            // lblServerAppName
            // 
            this.lblServerAppName.AutoSize = true;
            this.lblServerAppName.Location = new System.Drawing.Point(12, 75);
            this.lblServerAppName.Name = "lblServerAppName";
            this.lblServerAppName.Size = new System.Drawing.Size(93, 15);
            this.lblServerAppName.TabIndex = 8;
            this.lblServerAppName.Text = "Application &Name:";
            // 
            // lblIISServer
            // 
            this.lblIISServer.AutoSize = true;
            this.lblIISServer.Location = new System.Drawing.Point(12, 37);
            this.lblIISServer.Name = "lblIISServer";
            this.lblIISServer.Size = new System.Drawing.Size(59, 15);
            this.lblIISServer.TabIndex = 6;
            this.lblIISServer.Text = "&IIS Server:";
            // 
            // PUPForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(329, 350);
            this.Controls.Add(this.grpIISServer);
            this.Controls.Add(this.chkUpdateLicenses);
            this.Controls.Add(this.chkPublishImmediately);
            this.Controls.Add(this.grpTarget);
            this.Controls.Add(this.grpDB);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PUPForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update People From External Source";
            this.Load += new System.EventHandler(this.PUPForm_Load);
            this.grpDB.ResumeLayout(false);
            this.grpDB.PerformLayout();
            this.grpTarget.ResumeLayout(false);
            this.grpTarget.PerformLayout();
            this.grpIISServer.ResumeLayout(false);
            this.grpIISServer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtSourceServer;
        private System.Windows.Forms.TextBox txtSourceDatabase;
        private System.Windows.Forms.Label lblSourceServer;
        private System.Windows.Forms.Label lblSourceDatabase;
        private System.Windows.Forms.GroupBox grpDB;
        private System.Windows.Forms.GroupBox grpTarget;
        private System.Windows.Forms.TextBox txtTargetDatabase;
        private System.Windows.Forms.TextBox txtTargetServer;
        private System.Windows.Forms.Label lblTargetDatabase;
        private System.Windows.Forms.Label lblTargetServer;
        private System.Windows.Forms.CheckBox chkPublishImmediately;
        private System.Windows.Forms.CheckBox chkUpdateLicenses;
        private System.Windows.Forms.GroupBox grpIISServer;
        private System.Windows.Forms.TextBox txtMacPacApplicationName;
        private System.Windows.Forms.TextBox txtIISServer;
        private System.Windows.Forms.Label lblServerAppName;
        private System.Windows.Forms.Label lblIISServer;
    }
}