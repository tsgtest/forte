using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using AD = System.DirectoryServices;
using LMP.Data;

namespace LMP.Administration.Controls
{
    public partial class InterrogatoriesManager : LMP.Administration.Controls.AdminManagerBase
    {
        #region *********************fields*********************
        private OleDbCommandBuilder m_oCmdBuilder;
        #endregion
        #region *********************constants*********************
        private const string INTERROGATORIES_SETID = "-4";
        private const string STYLES_SETID = "-5";
        private const string INTERROGATORIES_DEFAULT = "";
        #endregion
        #region *********************constructors*********************
        public InterrogatoriesManager()
        {
            //subscribe to base events
            base.AfterControlLoaded += new AfterControlLoadedHandler(AdminManager_AfterControlLoaded);
            base.BeforeControlUnloaded += new BeforeControlUnLoadedHandler(AdminManager_BeforeControlUnloaded);

            InitializeComponent();
        }
        #endregion
        #region *********************properties******************
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
            set
            {
                base.IsValid = value;
            }
        }
        #endregion
        #region *********************methods*********************
        ///// <summary>
        ///// sets up new record with default text and new index value
        ///// </summary>
        //private void AddNewType()
        //{
        //    //unsubscribe from validation & position_changed events
        //    grdTypes.CellValidating -= grdTypes_CellValidating;

        //    BindingSource oBS = ((BindingSource)this.grdTypes.DataSource);

        //    //update the data source, which will update the grid
        //    DataTable oTable = ((DataTable)oBS.DataSource);
        //    DataRow oNewRow = oTable.NewRow();
        //    oNewRow.ItemArray = new object[] {"New Question", "New Response"};

        //    oTable.Rows.InsertAt(oNewRow, this.grdTypes.CurrentRow.Index);

        //    //select the new row
        //    grdTypes.CurrentCell = grdTypes.Rows[this.grdTypes.CurrentRow.Index - 1].Cells[2];
        //    grdTypes.BeginEdit(true);

        //    //resubscribe to the event handlers
        //    grdTypes.CellValidating += new DataGridViewCellValidatingEventHandler(grdTypes_CellValidating);

        //    this.grdTypes.IsDirty = true;
        //}
        /// <summary>
        /// sets up new record with default text and new index value
        /// </summary>
        private void AddNewType()
        {
            //unsubscribe from validation & position_changed events
            grdTypes.CellValidating -= grdTypes_CellValidating;

            BindingSource oBS = ((BindingSource)this.grdTypes.DataSource);

            //update the data source, which will update the grid
            ((DataTable)oBS.DataSource).Rows.Add("New Question", "New Response");

            //select the new row
            grdTypes.CurrentCell = grdTypes.Rows[grdTypes.Rows.Count - 1].Cells[3];
            grdTypes.BeginEdit(true);

            //resubscribe to the event handlers
            grdTypes.CellValidating += new DataGridViewCellValidatingEventHandler(grdTypes_CellValidating);

            this.grdTypes.IsDirty = true;
        }
        /// <summary>
        /// sets up new record with default text and new index value
        /// </summary>
        private void AddNewStyle()
        {
            BindingSource oBS = ((BindingSource)this.grdStyles.DataSource);

            //update the data source, which will update the grid
            ((DataTable)oBS.DataSource).Rows.Add("New Style", 0, 1, 0, 0, 0, 0, 0, 0, 0);

            //select the new row
            grdStyles.CurrentCell = grdStyles.Rows[grdStyles.Rows.Count - 1].Cells[12];
            grdStyles.BeginEdit(true);

            //resubscribe to the event handlers
            grdStyles.CellValidating += new DataGridViewCellValidatingEventHandler(grdStyles_CellValidating);
        }
        private void DeleteValueSetItem()
        {
            //do nothing if user in new row and no edits made
            if (grdTypes.CurrentRow == null || grdTypes.CurrentRow.IsNewRow)
                return;

            if (!grdTypes.CurrentRow.Selected)
                grdTypes.CurrentRow.Selected = true;

            //warn user they are about to delete a ValueSet item
            DialogResult oChoice = MessageBox.Show(
                LMP.Resources.GetLangString("Message_DeleteEntry"),
                LMP.String.MacPacProductName,
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (oChoice == DialogResult.Yes)
            {
                //remove row
                this.grdTypes.Rows.RemoveAt(this.grdTypes.CurrentRow.Index);
                this.grdTypes.IsDirty = true;
                ((DataTable)((BindingSource)this.grdTypes.DataSource).DataSource).AcceptChanges();
            }
        }
        /// <summary>
        /// deletes a valueset row into db.
        /// </summary>
        /// <param name="xSetID"></param>
        /// <param name="xValue1"></param>
        /// <param name="xValue2"></param>
        private void DeleteItem(string xSetID, string xValue1, string xValue2)
        {
            try
            {
                System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand();
                oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                oCmd.CommandType = CommandType.Text;
                //GLOG 3401: Because ValueSet items don't have a unique ID,
                //we need to update LastEditTime for all items in this set,
                //while adding a Deletions record so entire Set will get recreated in sync
                //GLOG 6673:  Make sure Date in SQL string is US Format
                oCmd.CommandText = "UPDATE ValueSets SET LastEditTime = #" + LMP.Data.Application.GetCurrentEditTime(true) +
                    "# WHERE SetID = " + xSetID + ";";
                oCmd.ExecuteNonQuery();
                SimpleDataCollection.LogDeletions(mpObjectTypes.ValueSets, xSetID);
                oCmd.CommandText = "Delete * FROM ValueSets WHERE  Value1 = \"" + xValue1 + "\" AND Value2 = \"" + xValue2 +
                    "\" AND SetID = " + xSetID + ";";

                //create data adapter from command
                OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd);

                DataSet oDS = new DataSet();
                using (oAdapter)
                {
                    //create and fill data set using adapter
                    oAdapter.Fill(oDS);
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString(
                    "Error_CouldNotDeleteListEntry"), oE);
            }
        }
        #endregion
        #region *********************private members*********************
        /// <summary>
        /// displays loads value set of ID -1 into grid
        /// </summary>
        private void DataBindTypes()
        {
            try
            {
                //we don't want any validation while the grid is loading
                grdTypes.CellValidating -= grdTypes_CellValidating;

                //GLOG item #5673 - restructure rog heading storage to
                //allow change of order - move from ValueSets table to firm
                //app keyset
                string xHeadings = null;
                try
                {
                    xHeadings = LMP.Data.KeySet.GetKeyValue("Interrogatories",
                        mpKeySetTypes.FirmApp, "0", ForteConstants.mpFirmRecordID);
                }
                catch { }

                DataTable oDT = null;

                if (string.IsNullOrEmpty(xHeadings))
                {
                    //no keyset-based rog headings - load from ValueSets table
                    DataSet oDS = GetValueSet(INTERROGATORIES_SETID);

                    //get data table
                    oDT = oDS.Tables[0];
                }
                else
                {
                    //get data table for interrogatory heading items
                    oDT = new DataTable();
                    oDT.Columns.Add("Value1");
                    oDT.Columns.Add("Value2");

                    string[] aHeadings = xHeadings.Split('�');
                    for (int i = 0; i < aHeadings.Length; i = i + 2)
                    {
                        oDT.Rows.Add(aHeadings[i], aHeadings[i + 1]);
                    }
                }

                //initialize binding source
                BindingSource oBS = new BindingSource();
                oBS.DataSource = oDT;

                //bind dataset to value set grid
                this.grdTypes.DataSource = oBS;
                this.grdTypes.Columns[0].Visible = false;
                this.grdTypes.Columns[1].Visible = false;
                if (this.grdTypes.Columns.GetColumnCount(DataGridViewElementStates.None) == 3)
                    this.grdTypes.Columns[2].Visible = false;

                DataGridViewTextBoxColumn oNameCol = new DataGridViewTextBoxColumn();
                oNameCol.DataPropertyName = "Value1";
                oNameCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oNameCol.HeaderText = "Question";

                this.grdTypes.Columns.Add(oNameCol);

                DataGridViewTextBoxColumn oResponseCol = new DataGridViewTextBoxColumn();
                oResponseCol.DataPropertyName = "Value2";
                oResponseCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oResponseCol.HeaderText = "Response";

                this.grdTypes.Columns.Add(oResponseCol);

                ////subscribe to position changed event - this will be the caller
                ////for the update routine when navigating through grid
                //m_oTypeSource.PositionChanged += new EventHandler(m_oTypeSource_PositionChanged);

                //resubscribe to Validating event
                grdTypes.CellValidating += new DataGridViewCellValidatingEventHandler(grdTypes_CellValidating);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotBindDataToControl"), oE);
            }
        }

        /// <summary>
        /// displays loads value set of ID -1 into grid
        /// </summary>
        private void DataBindStyles()
        {
            try
            {
                //we don't want any validation while the grid is loading
                grdStyles.CellValidating -= grdStyles_CellValidating;

                DataTable oDT = GetStyleDefinitionsDataTable();

                BindingSource oBS = new BindingSource();
                oBS.DataSource = oDT;

                //bind dataset to value set grid
                this.grdStyles.DataSource = oBS;
                this.grdStyles.ReadOnly = false;

                for (int i = 0; i < 10; i++)
                    this.grdStyles.Columns[i].Visible = false;

                //types colummn
                DataGridViewTextBoxColumn oTypeCol = new DataGridViewTextBoxColumn();
                oTypeCol.DataPropertyName = "Style Type";
                oTypeCol.HeaderText = "Type";
                oTypeCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                oTypeCol.Width = 120;
                oTypeCol.ReadOnly = true;

                this.grdStyles.Columns.Add(oTypeCol);

                //display names colummn
                DataGridViewTextBoxColumn oNameCol = new DataGridViewTextBoxColumn();
                oNameCol.DataPropertyName = "Style Name";
                oNameCol.HeaderText = "Name";
                oNameCol.Width = 100;

                this.grdStyles.Columns.Add(oNameCol);

                //line spacing colummn
                DataGridViewComboBoxColumn oLineSpacingCol = new DataGridViewComboBoxColumn();
                oLineSpacingCol.DataPropertyName = "Line Spacing";
                oLineSpacingCol.HeaderText = "Line Spacing";
                oLineSpacingCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                oLineSpacingCol.Width = 80;

                DataTable oLineSpacingDT = new DataTable();
                oLineSpacingDT.Columns.Add("Name");
                oLineSpacingDT.Columns.Add("Value", typeof(string));

                oLineSpacingDT.Rows.Add("Single", "1");
                oLineSpacingDT.Rows.Add("Double", "2");

                oLineSpacingCol.DataSource = oLineSpacingDT;
                oLineSpacingCol.ValueMember = "Value";
                oLineSpacingCol.DisplayMember = "Name";
                oLineSpacingCol.ReadOnly = false;

                this.grdStyles.Columns.Add(oLineSpacingCol);

                //line 1 indent colummn
                DataGridViewTextBoxColumn oLine1IndentCol = new DataGridViewTextBoxColumn();
                oLine1IndentCol.DataPropertyName = "Line 1 Indent";
                oLine1IndentCol.HeaderText = "Line 1 Indent";
                oLine1IndentCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oLine1IndentCol.ReadOnly = false;

                this.grdStyles.Columns.Add(oLine1IndentCol);

                //space after indent colummn
                DataGridViewTextBoxColumn oSpaceAfterCol = new DataGridViewTextBoxColumn();
                oSpaceAfterCol.DataPropertyName = "Space After";
                oSpaceAfterCol.HeaderText = "Space After";
                oSpaceAfterCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oSpaceAfterCol.ReadOnly = false;

                this.grdStyles.Columns.Add(oSpaceAfterCol);

                //space before indent colummn
                DataGridViewTextBoxColumn oSpaceBeforeCol = new DataGridViewTextBoxColumn();
                oSpaceBeforeCol.DataPropertyName = "Space Before";
                oSpaceBeforeCol.HeaderText = "Space Before";
                oSpaceBeforeCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                this.grdStyles.Columns.Add(oSpaceBeforeCol);

                //bold colummn
                DataGridViewCheckBoxColumn oBoldCol = new DataGridViewCheckBoxColumn();
                oBoldCol.DataPropertyName = "Bold";
                oBoldCol.HeaderText = "Bold";
                oBoldCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oBoldCol.ValueType = typeof(string);
                oBoldCol.TrueValue = "True";
                oBoldCol.FalseValue = "False";

                this.grdStyles.Columns.Add(oBoldCol);

                //Underline colummn
                DataGridViewCheckBoxColumn oUnderlineCol = new DataGridViewCheckBoxColumn();
                oUnderlineCol.DataPropertyName = "Underline";
                oUnderlineCol.HeaderText = "Underline";
                oUnderlineCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oUnderlineCol.MinimumWidth = 55;
                oUnderlineCol.ValueType = typeof(string);
                oUnderlineCol.TrueValue = "True";
                oUnderlineCol.FalseValue = "False";

                this.grdStyles.Columns.Add(oUnderlineCol);

                //AllCaps colummn
                DataGridViewCheckBoxColumn oAllCapsCol = new DataGridViewCheckBoxColumn();
                oAllCapsCol.DataPropertyName = "All Caps";
                oAllCapsCol.HeaderText = "All Caps";
                oAllCapsCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oAllCapsCol.ValueType = typeof(string);
                oAllCapsCol.TrueValue = "True";
                oAllCapsCol.FalseValue = "False";

                this.grdStyles.Columns.Add(oAllCapsCol);

                //SmallCaps colummn
                DataGridViewCheckBoxColumn oSmallCapsCol = new DataGridViewCheckBoxColumn();
                oSmallCapsCol.DataPropertyName = "Small Caps";
                oSmallCapsCol.HeaderText = "Small Caps";
                oSmallCapsCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                oSmallCapsCol.ValueType = typeof(string);
                oSmallCapsCol.TrueValue = "True";
                oSmallCapsCol.FalseValue = "False";

                this.grdStyles.Columns.Add(oSmallCapsCol);

                ////subscribe to position changed event - this will be the caller
                ////for the update routine when navigating through grid
                //m_oStylesSource.PositionChanged += new EventHandler(m_oStylesSource_PositionChanged);

                //resubscribe to Validating event
                grdStyles.CellValidating += new DataGridViewCellValidatingEventHandler(grdStyles_CellValidating);
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotBindDataToControl"), oE);
            }
        }

        private DataTable GetStyleDefinitionsDataTable()
        {
            //get admin firm user app pref keyset
            KeySet oKeySet = new KeySet(mpKeySetTypes.UserAppPref, "0", "-999999999", 0);

            string xFirmStyleDefs = null;

            try
            {
                xFirmStyleDefs = oKeySet.GetValue("RogStyles");
            }
            catch { }

            string[] aFirmStyleDefs = null;

            if (xFirmStyleDefs == null)
            {
                //rog styles not yet set
                xFirmStyleDefs = "QuestionHeading,Discovery No,2,0,0,0,True,True,False,True;QuestionText,Discovery Text,2,.5,0,0,False,False,False,False;ResponseHeading,Response No,2,0,0,0,True,True,False,True;ResponseText,Response Text,2,.5,0,0,False,False,False,False;RunInDiscovery,RunIn Question,2,0,0,0,True,True,False,False;RunInResponse,RunIn Response,2,0,0,0,False,False,False,False";
            }
                
            aFirmStyleDefs = xFirmStyleDefs.Split(';');

            int iStyles = 0;

            try
            {
                iStyles = aFirmStyleDefs.Length;
            }
            catch { }

            DataTable oDT = new DataTable();

            //modify table - parse style data into separate columns
            DataColumn oDC1 = new DataColumn("Style Type");
            DataColumn oDC2 = new DataColumn("Style Name");
            DataColumn oDC3 = new DataColumn("Line Spacing");
            DataColumn oDC4 = new DataColumn("Line 1 Indent");
            DataColumn oDC5 = new DataColumn("Space After");
            DataColumn oDC6 = new DataColumn("Space Before");
            DataColumn oDC7 = new DataColumn("Bold", typeof(string));
            DataColumn oDC8 = new DataColumn("Underline", typeof(string));
            DataColumn oDC9 = new DataColumn("All Caps", typeof(string));
            DataColumn oDC10 = new DataColumn("Small Caps", typeof(string));

            oDT.Columns.AddRange(new DataColumn[]{
                oDC1, oDC2, oDC3, oDC4, oDC5, oDC6, oDC7, oDC8, oDC9, oDC10});

            for (int i = 0; i < iStyles; i++)
            {
                string xValue = aFirmStyleDefs[i];
                string[] aValues = xValue.Split(',');
                DataRow oDR = oDT.Rows.Add();

                oDR[0] = aValues[0];
                oDR[1] = aValues[1];
                oDR[2] = aValues[2];
                oDR[3] = aValues[3];
                oDR[4] = aValues[4];
                oDR[5] = aValues[5];
                oDR[6] = bool.Parse(aValues[6]);
                oDR[7] = bool.Parse(aValues[7]);
                oDR[8] = bool.Parse(aValues[8]);
                oDR[9] = bool.Parse(aValues[9]);
            }

            return oDT;
        }

        private void UpdateStyleDefinitions()
        {
            if (!this.grdStyles.IsDirty)
                return;

            BindingSource oBS = (BindingSource)this.grdStyles.DataSource;
            DataTable oDT = (DataTable)oBS.DataSource;
            string xDef = null;

            if (oDT.Rows.Count == 0)
                return;

            foreach (DataRow oDR in oDT.Rows)
            {
                foreach (object oItem in oDR.ItemArray)
                {
                    xDef += oItem.ToString() + ",";
                }

                xDef = xDef.TrimEnd(',');
                xDef += ";";
            }

            //trim trailing semi-colon
            xDef = xDef.TrimEnd(';');

            //get admin firm user app pref keyset
            KeySet oKeySet = new KeySet(mpKeySetTypes.UserAppPref, "0", "-999999999", 0);

            //save new definitions
            oKeySet.SetValue("RogStyles", xDef);
            oKeySet.Save();

            this.grdStyles.IsDirty = false;
        }
        ///// <summary>
        ///// this handler will fire when user changes rows in grid
        ///// the datarow state will then determine whether to update
        ///// dataset
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void m_oStylesSource_PositionChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        return;

        //        //get current BindingSource datarow object
        //        BindingSource oBS = (BindingSource)sender;

        //        if (m_oStylesLastRow == null)
        //            m_oStylesLastRow = ((DataRowView)oBS.Current).Row;

        //        if (grdStyles.CurrentRow == null)
        //            return;

        //        if (this.IsValid && m_oStylesLastRow.RowState == DataRowState.Modified)
        //            if (!ValidateEntry())
        //            {
        //                grdStyles.CancelEdit();
        //                this.IsValid = true;
        //                return;
        //            }

        //        ////update according to rowstate if necessary
        //        //if (m_oStylesLastRow.RowState == DataRowState.Modified)
        //        //    this.UpdateInterrogatories(false);

        //        //if (m_oStylesLastRow.RowState == DataRowState.Added)
        //        //    this.UpdateInterrogatories(true);

        //        //reset last row variable
        //        if (oBS.Current != null)
        //            m_oStylesLastRow = ((DataRowView)oBS.Current).Row;

        //    }
        //    catch (Exception oE)
        //    {
        //        if (oE is LMP.Exceptions.UINodeException)
        //        {
        //            MessageBox.Show(LMP.Resources.GetLangString("Error_CouldNotSaveChangesToValueSet"),
        //               LMP.String.MacPacProductName,
        //               MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        //            grdStyles.CancelEdit();
        //            m_oStylesLastRow.RejectChanges();
        //            this.IsValid = false;
        //        }
        //        else
        //            LMP.Error.Show(oE);
        //    }
        //}

        //GLOG item #5673 - modified this routine to
        //write to the firm app keyset instead of the ValueSet table
        private void UpdateInterrogatories()
        {
            try
            {
                if (!this.grdTypes.IsDirty)
                    return;

                //save to firm app keyset record
                string xHeadings = null;
                foreach (DataRow oDR in ((DataTable)((BindingSource)this.grdTypes.DataSource).DataSource).Rows)
                {
                    xHeadings += oDR[0].ToString().TrimEnd() +"�" + oDR[1].ToString().TrimEnd() + "�";
                }

                //GLOG item #5935 - dcf
                if (xHeadings == null)
                    xHeadings = "";

                xHeadings = xHeadings.TrimEnd('�');

                KeySet.SetKeyValue("Interrogatories", xHeadings, 
                    mpKeySetTypes.FirmApp, "0", ForteConstants.mpFirmRecordID);

                //get updated interrogatory string
                this.grdTypes.IsDirty = false;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.UINodeException(
                    LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
            }
        }
        //private void UpdateInterrogatories()
        //{
        //    try
        //    {
        //        if (!this.grdTypes.IsDirty)
        //            return;

        //        OleDbCommand oCmd = new OleDbCommand();

        //        oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
        //        oCmd.CommandType = CommandType.Text;

        //        //GLOG 3401: Because ValueSet items don't have a numeric key,
        //        //we delete all interrogatories, and recreate them anew -
        //        //we need to add Deletions records so entire Set will get recreated in sync
        //        oCmd.CommandText = "DELETE FROM ValueSets WHERE SetID = " + INTERROGATORIES_SETID + ";";
        //        oCmd.ExecuteNonQuery();
        //        SimpleDataCollection.LogDeletions(mpObjectTypes.ValueSets, INTERROGATORIES_SETID);

        //        foreach (DataRow oDR in ((DataTable)((BindingSource)this.grdTypes.DataSource).DataSource).Rows)
        //        {
        //            oCmd.CommandText = "Insert INTO ValueSets (SetID, Value1, Value2, LastEditTime) " +
        //                "VALUES (" + INTERROGATORIES_SETID + ", \"" + oDR[0] + "\", \"" + oDR[1] + "\", " +
        //                        "#" + LMP.Data.Application.GetCurrentEditTime() + "#);";
        //            oCmd.CommandType = CommandType.Text;
        //            oCmd.ExecuteNonQuery();
        //        }

        //        this.grdTypes.IsDirty = false;
        //    }
        //    catch (System.Exception oE)
        //    {
        //        throw new LMP.Exceptions.UINodeException(
        //            LMP.Resources.GetLangString("Error_CouldNotSaveChangesToDB"), oE);
        //    }
        //}
        private bool ValidateEntry()
        {
            string xValue = grdTypes.CurrentRow.Cells[0].EditedFormattedValue.ToString();

            if (string.IsNullOrEmpty(xValue))
                return false;

            //reset IsValid so navigation to another Manager 
            //can be handled by the Main form
            this.IsValid = xValue == "";

            if (xValue == "")
            {
                //missing value for Column 1, inform user
                MessageBox.Show(LMP.Resources.GetLangString("Msg_ListEntryValue1Required"),
                    LMP.String.MacPacProductName,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.IsValid = false;
                return false;
            }
            this.IsValid = true;
            return true;
        }

        /// <summary>
        /// returns the dataset with list items value set items
        /// </summary>
        /// <param name="xSetID"></param>
        /// <returns>DataSet</returns>
        private DataSet GetValueSet(string xSetID)
        {
            try
            {
                DataSet oDS = new DataSet();
                using (System.Data.OleDb.OleDbCommand oCmd = new OleDbCommand())
                {

                    oCmd.Connection = LMP.Data.LocalConnection.ConnectionObject;
                    oCmd.CommandText = "Select Value1, Value2, SetID FROM ValueSets WHERE SetID = " + xSetID + ";";
                    oCmd.CommandType = CommandType.Text;

                    //create data adapter from command
                    using (OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd))
                    {
                        if (m_oCmdBuilder == null)
                            m_oCmdBuilder = new OleDbCommandBuilder(oAdapter);

                        //create and fill data set using adapter
                        oAdapter.Fill(oDS);
                    }

                    return oDS;
                }
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.DataException(
                    LMP.Resources.GetLangString("Error_CouldNotCreateDataSet"), oE);
            }
        }
        #endregion
        #region *********************private functions*******************
        /// <summary>
        /// finds the row matching the find text
        /// </summary>
        private void FindMatchingRow(string xSearchText, LMP.Controls.DataGridView oGrid, string xColumnName)
        {
            //note that iMatchIndex == -1 means that no match was found.
            int iMatchIndex = 0;

            if (xSearchText != "")
            {
                //User has entered a string to search for

                //Get the index of the first instance of the 
                //search string the user entered.
                iMatchIndex = oGrid.FindRecord(xSearchText, xColumnName, false);

                if (iMatchIndex > -1)
                    //Match found- select appropriate record.
                    oGrid.CurrentCell = oGrid.Rows[iMatchIndex].Cells[xColumnName];
            }
        }
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// handles initializing data grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InterrogatoriesManager_Load(object sender, EventArgs e)
        {
            try
            {
                DataBindTypes();
                DataBindStyles();
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// bind various grids after control has been loaded and docked on main
        /// form - this prevents refresh bug where record is displayed but grid
        /// display attribute settings are ignored when form is first loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_AfterControlLoaded(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// fires if control is about to be unloaded by selecting another
        /// manager - calls SaveCurrentRecord to commit and save any 
        /// pending edits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminManager_BeforeControlUnloaded(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// ensures name field is unique and non-null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdTypes_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (grdTypes.Columns[e.ColumnIndex].Name == "Name")
                {
                    BindingSource oBS = (BindingSource)this.grdTypes.DataSource;
                    DataTable oDT = (DataTable)oBS.DataSource;
                    DataRow[] oRows = oDT.Select("Name = '" + e.FormattedValue.ToString() + "'");

                    if (oRows.Length == 0)
                    {
                        this.IsValid = true;
                    }
                    else
                    {
                        if (oRows.Length == 1)
                        {
                            // The row that was found could be the row that is being editted. In such a
                            // case the entry is valid. 
                            int iExistingRowID = (int)oRows[0]["ID"];
                            int iEdittedRowID = (int)this.grdTypes.Rows[e.RowIndex].Cells["ID"].Value;
                            this.IsValid = (iExistingRowID == iEdittedRowID);
                        }
                        else
                        {
                            // More than 1 row has been found. There should be a maximum of 2 items: 
                            this.IsValid = false;
                        }
                    }

                    if (!this.IsValid)
                    {
                        //generate validation message
                        MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateNameValueNotAllowed"),
                            LMP.ComponentProperties.ProductName,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.grdTypes.CurrentCell = this.grdTypes.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    }

                    //cancel navigation if cell data not valid
                    e.Cancel = !this.IsValid;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// ensures name field is unique and non-null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdStyles_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                return;
                //if (grdStyles.Columns[e.ColumnIndex].Name == "Name")
                //{
                //    BindingSource oBS = (BindingSource)this.grdStyles.DataSource;
                //    DataTable oDT = (DataTable)oBS.DataSource;
                //    DataRow[] oRows = oDT.Select("Name = '" + e.FormattedValue.ToString() + "'");

                //    if (oRows.Length == 0)
                //    {
                //        this.IsValid = true;
                //    }
                //    else
                //    {
                //        if (oRows.Length == 1)
                //        {
                //            // The row that was found could be the row that is being editted. In such a
                //            // case the entry is valid. 
                //            int iExistingRowID = (int)oRows[0]["ID"];
                //            int iEdittedRowID = (int)this.grdStyles.Rows[e.RowIndex].Cells["ID"].Value;
                //            this.IsValid = (iExistingRowID == iEdittedRowID);
                //        }
                //        else
                //        {
                //            // More than 1 row has been found. There should be a maximum of 2 items: 
                //            this.IsValid = false;
                //        }
                //    }

                //    if (!this.IsValid)
                //    {
                //        //generate validation message
                //        MessageBox.Show(LMP.Resources.GetLangString("Error_DuplicateNameValueNotAllowed"),
                //            LMP.ComponentProperties.ProductName,
                //            MessageBoxButtons.OK, MessageBoxIcon.Error);
                //        this.grdStyles.CurrentCell = this.grdStyles.Rows[e.RowIndex].Cells[e.ColumnIndex];
                //    }

                //    //cancel navigation if cell data not valid
                //    e.Cancel = !this.IsValid;
                //}
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        /// <summary>
        /// handle tab input when focus is on checkbox column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdTypes_TabPressed(object sender, LMP.Controls.TabPressedEventArgs e)
        {

            if (grdTypes.CurrentCell.OwningColumn.Name == "Visible")
            {
                if (e.ShiftPressed)
                {
                    SendKeys.Send("{UP}{RIGHT}^{RIGHT}");
                    grdTypes.CurrentCell = grdTypes.CurrentRow.Cells["Description"];
                    grdTypes.CurrentCell.Selected = true;
                }
                else
                    SendKeys.Send("{RIGHT}");
            }
        }
        /// <summary>
        /// Delete a record.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.DeleteValueSetItem();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// adds new groups
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddNewType();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void tbtnTranslate_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Under Construction",
                        LMP.String.MacPacProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void ttxtFindGroup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                this.FindMatchingRow(this.ttxtFindType.Text, this.grdTypes, "Value1");
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void grdTypes_Leave(object sender, System.EventArgs e)
        {
            try
            {
                if (grdTypes.CurrentRow == null)
                    return;
                
                grdTypes.EndEdit();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdStyles_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            LMP.Error.Show(e.Exception);
        }

        private void grdStyles_Validated(object sender, EventArgs e)
        {
            try
            {
                this.grdStyles.EndEdit();
                UpdateStyleDefinitions();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdTypes_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if (this.grdTypes.IsCurrentRowDirty)
                {
                    if (!this.ValidateEntry())
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void InterrogatoriesManager_Leave(object sender, EventArgs e)
        {
            try
            {
                this.grdTypes.EndEdit();
                this.grdStyles.EndEdit();
                UpdateInterrogatories();
                UpdateStyleDefinitions();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void InterrogatoriesManager_Validated(object sender, EventArgs e)
        {
            try
            {
                this.grdTypes.EndEdit();
                this.grdStyles.EndEdit();
                UpdateInterrogatories();
                UpdateStyleDefinitions();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void grdTypes_ValueChanged(object sender, EventArgs e)
        {
            this.grdTypes.IsDirty = true;
        }

        private void grdStyles_ValueChanged(object sender, EventArgs e)
        {
            this.grdStyles.IsDirty = true;
        }

        private void grdStyles_Leave(object sender, EventArgs e)
        {
            if (this.grdStyles.CurrentRow == null)
                return;

            this.grdStyles.EndEdit();
        }
        private void grdTypes_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //GLOG 6654: Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }
        }
        private void grdStyles_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                //GLOG 6654: Disable Column Header sorting
                e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch { }

        }

        #endregion
    }
}

