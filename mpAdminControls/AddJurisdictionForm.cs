using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LMP.Administration.Controls
{
    public partial class AddJurisdictionForm : Form
    {
        public AddJurisdictionForm()
        {
            InitializeComponent();
        }

        public string JurisdictionName
        {
            get
            {
                return this.txtJurisdictionName.Text;
            }

            set
            {
                this.txtJurisdictionName.Text = value;
            }
        }

        private void txtJurisdictionName_TextChanged(object sender, EventArgs e)
        {
            UpdateView();
        }

        private void UpdateView()
        {
            this.btnOK.Enabled = (JurisdictionName.Length > 0) ;
        }

        private void AddJurisdictionForm_Load(object sender, EventArgs e)
        {
            UpdateView();
        }
    }
}